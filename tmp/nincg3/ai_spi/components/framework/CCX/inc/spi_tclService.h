/*!
 *******************************************************************************
 * \file               spi_tclService.h
 *******************************************************************************
 \verbatim
 PROJECT:        G3G
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    CCA service Spi
 created with CCA skeleton generator.
 COPYRIGHT:      &copy; RBEI
 AUTHOR:
 COPYRIGHT:     (c) 2012 Robert Bosch GmbH, Hildesheim
 HISTORY:
 Date       | Author                 | Modifications
 30.10.2013 | Vinoop U               | Initial Version
 26.12.2013 | Shiva Kumar G          | Updated with few elements for 
                                       HMI Interface
 05.04.2013 | Shiva Kumar G          | Updated New Methods & Properties
 06.04.2014 | Ramya Murthy           | Initialisation sequence implementation
 28.04.2014 | Shihabudheen P M       | Updated for accessory audio context
 10.06.2014 | Ramya Murthy           | Audio policy redesign implementation.
 16.07.2014 | Shiva Kumar G          | Implemented SessionStatusInfo update
 31.07.2014 | Ramya Murthy           | SPI feature configuration via LoadSettings()
 01.10.2014 | Ramya Murthy           | Added Telephone client handler (moved from BT Manager)
 25.10.2014 |  Shihabudheen P M      | added vUpdateSessionStatusInfo
 28.10.2014 | Hari Priya E R         | Added changes for subscription of location data
 31.10.2014 | Ramya Murthy           | Implementation for "SameDevice" param in BluetoothDeviceStatus
 17.11.2014 | Hari Priya E R         | Fix for SUZUKI-20652
 05.11.2014 | Ramya Murthy           | Implementation for Application metadata.
 29.05.2015 | Ramya Murthy           | Implementation for BTPairingRequired property.
 19.06.2015 | Shihabudheen P M       | added vOnDataServiceSubscribeRquest()
 25.06.2015 | Sameer Chandra         | Added ML XDeviceKey Support for PSA
 15.06.2014 | Shihabudheen P M		 | vOnLbBTAddressUpdate()
 07.09.2015 | Dhiraj Asopa           | Added vOnMSSetAccessoryAudioContext()method to set current audio context,
 11.09.2015 | Dhiraj Asopa           | Added SetFeatureRestrictions()method for feature restriction (ParkMode/DriveMode) .
 14.12.2015 | Rachana L Achar        | Modified the prototype of vLaunchAppResult method
 26.02.2016 | Rachana L Achar        | AAP Navigation implementation
 10.03.2016 | Rachana L Achar        | AAP Notification implementation

 \endverbatim
 *******************************************************************************/

#ifndef _SPI_TCLSERVICE_H_
#define _SPI_TCLSERVICE_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
//!Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

//Include public FI interface of this service.
#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_SMARTPHONEINTFI_TYPES
#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_SMARTPHONEINTFI_FUNCTIONIDS
#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_SMARTPHONEINTFI_ERRORCODES
#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_SMARTPHONEINTFI_SERVICEINFO
#include "midw_fi_if.h"

#define ARL_S_IMPORT_INTERFACE_GENERIC
#include "audio_routing_lib.h"

#include "spi_tclRespInterface.h"
#include "spi_tclLifeCycleIntf.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \Forward declarations
 */
class spi_tclCmdInterface;
class spi_tclMainApp;
class spi_tclAudioPolicy;
class spi_tclTelephoneClient;
class spi_tclPosDataClientHandler;
class spi_tclSensorDataClientHandler;
class spi_tclSPMClient;

typedef midw_fi_tcl_e32_KeyCode::tenType spi_tenFiKeyCode;


class spi_tclService: public ahl_tclBaseOneThreadService,
		public spi_tclRespInterface, public spi_tclLifeCycleIntf
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclService::spi_tclService(tVoid)
   ***************************************************************************/
   /*!
   * \fn      spi_tclService(tVoid)
   * \brief   Constructor.
   *
   *          Create an object of the base class
   *          ahl_tclBaseOneThreadService with a pointer to this
   *          application, the offered service identifier and the
   *          service version as parameters.
   * \param   tVoid
   **************************************************************************/
   spi_tclService(tVoid);

   spi_tclService(spi_tclMainApp* poMainAppl);

   /**************************************************************************
   ** FUNCTION   : spi_tclService::~spi_tclService(tVoid)
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclService()
   * \brief: Destructor.
   * \param  : None.
   * \retval : None.
   ***************************************************************************/
   virtual ~spi_tclService();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclService::bInitialize();
   **************************************************************************/
   /*!
   * \fn      bInitialize()
   * \brief   Method to initialize all the pointers used
   * \param   None
   **************************************************************************/
   virtual t_Bool bInitialize();

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclService::bUnInitialize();
    **************************************************************************/
   /*!
    * \fn      bUnInitialize()
    * \brief   Method to de-initialize all the pointers used
    * \param   None
    **************************************************************************/
   virtual t_Bool bUnInitialize();

   /***************************************************************************
    ** FUNCTION:  spi_tclService::vLoadSettings(const trSpiFeatureSupport&...)
    ***************************************************************************/
   /*!
    * \fn      vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
    * \brief   vLoadSettings Method. Invoked during OFF->NORMAL state transition.
    * \sa      vSaveSettings()
    **************************************************************************/
   virtual t_Void vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclService::vSaveSettings()
    ***************************************************************************/
   /*!
    * \fn      vSaveSettings()
    * \brief   vSaveSettings Method. Invoked during  NORMAL->OFF state transition.
    * \sa      vLoadSettings()
    **************************************************************************/
   virtual t_Void vSaveSettings();

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService::vProcessTimer(tU16 u16TimerId)
   ***************************************************************************/
   /*!
   * \fn     tVoid spi_tclService::vProcessTimer(tU16 u16TimerId)
   * \brief  This function is called from the vOnTimer() function of this
   *         CCA application on the expiration of a previously via function
   *         bStartTimer() started timer.
   * \param  [IN] u16TimerId = Identifier of the expired timer.
   * \retval  None.
   *******************************************************************************/
   tVoid vProcessTimer(tU16 u16TimerId);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclService::vOnLoopback(amt_tclSer...
   ***************************************************************************/
   /*!
   * \brief   Loopback service for Smartphone service.
   * \param   [u16ServiceID]:   (I) Service ID
   * \param   [poMessage]:      (->I) Pointer to incoming message.
   * \retval  NONE
   **************************************************************************/
   using ahl_tclBaseOneThreadService::vOnLoopback;
   virtual tVoid vOnLoopback(tU16 u16ServiceID, amt_tclServiceData* poMessage);


   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclService::bRequestAudioActivation(t_U8)
   ***************************************************************************/
   /*!
   * \fn      bRequestAudioActivation(t_U8 u8SourceNum)
   * \brief   Request to the Audio Manager by Component for Starting Audio Playback.
   *          Mandatory Interface to be implemented as per Project Audio Policy.
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          Source Number will be defined for Audio Source by the Audio Component.
   * \retval  Bool value
   **************************************************************************/
   virtual t_Bool bRequestAudioActivation(t_U8 u8SourceNum);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclService::bRequestAudioDeactivation(t_U8)
   ***************************************************************************/
   /*!
   * \fn      bRequestAudioDeactivation(t_U8 u8SourceNum)
   * \brief   Request to the Audio Manager by Component for Stopping Audio Playback.
   *          Mandatory Interface to be implemented as per Project Audio Policy.
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          Source Number will be defined for Audio Source by the Audio Component.
   * \retval  Bool value
   **************************************************************************/
   virtual t_Bool bRequestAudioDeactivation(t_U8 u8SourceNum);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclService::bPauseAudioActivity(t_U8)
   ***************************************************************************/
   /*!
   * \fn      bPauseAudioActivity(t_U8 u8SourceNum)
   * \brief   Request to the Audio Manager by Component for Pausing Audio Playback.
   *          Optional Interface to be implemented if supported and required.
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          Source Number will be defined for Audio Source by the Audio Component.
   * \retval  Bool value
   **************************************************************************/
   virtual t_Bool bPauseAudioActivity(t_U8 u8SourceNum);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclService::vStartSourceActivityResult(t_U8, t_Bool)
   ***************************************************************************/
   /*!
   * \fn      vStartSourceActivityResult(t_U8 u8SourceNum, t_Bool bError)
   * \brief   Acknowledgement from the Source Component to Audio Manager indicating
   *          Successful Start of Audio Playback on the allocated route.
   *        Mandatory Interface to be implemented.
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          [bError]: true for Error Condition, false otherwise
   *          Source Number will be defined for Audio Source by the Audio Component.
   * \retval  NONE
   **************************************************************************/
   virtual t_Void vStartSourceActivityResult(t_U8 u8SourceNum, t_Bool bError = false);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclService::vStopSourceActivityResult(t_U8, t_Bool)
   ***************************************************************************/
   /*!
   * \fn      vStopSourceActivityResult(t_U8 u8SourceNum, t_Bool bError)
   * \brief   Acknowledgement from the Source Component to Audio Manager indicating
   *          Successful Stop of Audio Playback on the allocated route.
   *        Mandatory Interface to be implemented.
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          [bError]: true for Error Condition, false otherwise
   * \retval  NONE
   **************************************************************************/
   virtual t_Void vStopSourceActivityResult(t_U8 u8SourceNum, t_Bool bError = false);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclService::vPauseSourceActivityResult(t_U8, t_Bool)
   ***************************************************************************/
   /*!
   * \fn      vPauseSourceActivityResult(t_U8 u8SourceNum, t_Bool bError)
   * \brief   Acknowledgement from the Source Component to Audio Manager indicating
   *          Successful Pause of Audio Playback on the allocated route.
   *        Optional Interface to be implemented if supported and required
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          [bError]: true for Error Condition, false otherwise
   *          Source Number will be defined for Audio Source by the Audio Component.
   * \retval  Bool value
   **************************************************************************/
   virtual t_Void vPauseSourceActivityResult(t_U8 u8SourceNum, t_Bool bError = false);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclService::bSetSrcAvailability(t_U8,t_Bool)
   ***************************************************************************/
   /*!
   * \fn      bSetSrcAvailability(t_U8 u8SourceNum,t_Bool bAvail)
   * \brief   Register the Availability of State of Source with Audio Manager.
   *        Optional Interface to be implemented if supported and required
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          [bAvail]: true is Source Available, false if Unavailable
   *          Source Number will be defined for Audio Source by the Audio Component.
   * \retval  Bool value
   **************************************************************************/
   virtual t_Bool bSetSrcAvailability(t_U8 u8SourceNum, t_Bool bAvail = true);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclService::vSetServiceAvailable(t_U8,t_Bool)
   ***************************************************************************/
   /*!
   * \fn      vSetServiceAvailable(t_U8 u8SourceNum,t_Bool bAvail)
   * \brief   Set service availability for audio
   * \param   [bAvail]: true is Source Available, false if Unavailable
   **************************************************************************/
   virtual t_Void vSetServiceAvailable(t_Bool bAvail);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclService::bSetAudioDucking()
   ***************************************************************************/
   /*!
   * \fn     bSetAudioDucking
   * \brief  Interface to set audio ducking ON/OFF.
   * \param  cou16RampDuration: Ramp duration in milliseconds
   * \param  cou8VolumeindB: Volume level in dB
   * \param  coenDuckingType: Ducking/ Unducking
   **************************************************************************/
   virtual t_Bool bSetAudioDucking(const tU8 cou8SrcNum,const t_U16 cou16RampDuration, const t_U8 cou8VolumeindB,
         const tenDuckingType coenDuckingType);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclService::bSetSourceMute(t_U8)
   ***************************************************************************/
   /*!
   * \fn      bSetSourceMuteOn(t_U8 u8SourceNum)
   * \brief   Request to Audio Manager to Mute the Source Audio.
   *          Optional Interface to be implemented if supported and required.
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          Source Number will be defined for Audio Source by the Audio Component.
   * \retval  Bool value
   **************************************************************************/
   virtual t_Bool bSetSourceMute(t_U8 u8SourceNum);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclService::bSetSourceDemute(t_U8)
   ***************************************************************************/
   /*!
   * \fn      t_Bool bSetSourceDemute(t_U8 u8SourceNum)
   * \brief   Request to Audio Manager to Demute the Source Audio.
   *        Optional Interface to be implemented if supported and required.
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          Source Number will be defined for Audio Source by the Audio Component.
   * \retval  Bool value
   **************************************************************************/
   virtual t_Bool bSetSourceDemute(tU8 u8SourceNum);

   virtual tBool bOnSrcActivity(arl_tenSource enSrcNum,
            const arl_tSrcActivity& rfcoSrcActivity);

   virtual tBool bOnAllocate(arl_tenSource enSrcNum,
            const arl_tAllocRouteResult& rfcoAllocRoute);

   virtual tBool bOnDeAllocate(arl_tenSource enSrcNum);

   virtual tVoid vOnError(tU8 u8SrcNum, arl_tenISourceError cenError);

   /**************************************************************************
   ** FUNCTION   : t_Void spi_tclService:: vSetRegion(tenRegion enRegion)
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetRegion(tenRegion enRegion)
   * \brief   To set the Region.for the application certification
   * \param   [IN].enRegion - Region Enumeration.
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetRegion(tenRegion enRegion);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclService::vRestoreSettings()
    ***************************************************************************/
   /*!
    * \fn      vRestoreSettings()
    * \brief   This method is called by the Main application when ClearPrivateData
    *          defset event occurs.
    * \param   None
    **************************************************************************/
   t_Void vRestoreSettings();


   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/


protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService::vOnServiceAvailable()
   ***************************************************************************
   * \fn      tVoid spi_tclService::vOnServiceAvailable()
   * \brief   This function is called by the CCA framework when the service
   which is offered by this server has become available.
   * \param   None.
   * \retval  None.
   * Overrides method ahl_tclBaseOneThreadService::vOnServiceAvailable().
   *******************************************************************************/
   virtual tVoid vOnServiceAvailable(tVoid);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService::   vOnServiceUnavailable()
   ***************************************************************************
   * \fn     tVoid spi_tclService::   vOnServiceUnavailable()
   /*!
   * \brief  This function is called by the CCA framework when the service
   *         which is offered by this server has become unavailable.
   * \param  None.
   * \retval None.
   Overrides method ahl_tclBaseOneThreadService::vOnServiceUnavailable().
   *******************************************************************************/
   virtual tVoid vOnServiceUnavailable(tVoid);

   /*******************************************************************************
   ** FUNCTION   : tVoid spi_tclService::   vOnServiceUnavailable()
   ***************************************************************************
   * \fn       tBool spi_tclService::bStatusMessageFactory(tU16 u16FunctionId,
   amt_tclServiceData& roOutMsg,
   amt_tclServiceData* poInMsg)
   /*!
   * \brief    This function is called by the CCA framework to request ANY
   *           property which is offered by this service. For each property
   *           accessed via parameter 'u16FunctionId' the user has to prepare
   *           the corresponding FI data object and to copy it to the
   *           referenced service data object 'roOutMsg'.
   * \param    [IN] u16FunctionId = Function ID of the requested property.
   *           [OUT] roOutMsg = Reference to the service data object to which
   *           the content of the prepared FI data object
   *           should be copied to.
   *           [IN] poInMsg = Selector message which is used to select dedicated
   *           content to be copied to 'roOutMsg' instead of
   *          updating the entire FI data object.
   * \retval   TRUE = For the requested property the FI data object was
   *           successfully created and copied to the referenced service
   *           data object 'roOutMsg'. The CCA framework sends a 'STATUS'
   *           message of the updated property to each registered
   *          clients.
   *           FALSE = Failed to create the FI data object for the requested
   *           property and to update the referenced service data object
   *          roOutMsg' with the new property content. The CCA
   *           framework sends an error message to the requesting
   *          client.

   ********************************************************************************
   * Overrides method ahl_tclBaseOneThreadService::bStatusMessageFactory().
   *******************************************************************************/
   virtual tBool bStatusMessageFactory(tU16 u16FunctionId,
      amt_tclServiceData& roOutMsg, amt_tclServiceData* poInMsg);

   /***************************************************************************
    ** FUNCTION:  tBool spi_tclService::bProcessSet(amt_tclServiceData* p...
    ***************************************************************************/
   /*!
    * \fn      bProcessSet(amt_tclServiceData* poMessage,
   *                 tBool& bPropertyChanged, tU16& u16Error)
    * \brief   This method is called by the CCA framework when it has
   *           received a message for a property with Opcode 'SET' or 'PURESET'
   *           and there is no dedicated handler method defined in the message
   *           map for this pair of FID and opcode. The user has to set the
   *           application specific property to the requested value and the CCA
   *           framework then cares about informing the requesting client
   *           as well as other registered clients.
   * \param    [IN] poMessage : Property to be set.
   * \param    [OUT] bPropertyChanged : Property changed flag to be set to TRUE
   *                 if property has changed. Otherwise to be set
   *                 to FALSE (default).
   * \param    [OUT] u16Error : Error code to be set if a CCA error occurs,
   *                 otherwise don't touch.
    **************************************************************************/
   virtual tBool bProcessSet(amt_tclServiceData* poMessage,
         tBool& bPropertyChanged, tU16& u16Error);

   /***************************************************************************
   * Handler function declarations used by message map.
   ***************************************************************************/

   //!Add your CCA message handler function prototypes here.
   /*******************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSGetDeviceInfoList()
   ***************************************************************************/
   /*!
   * \fn      tVoid spi_tclService:: vOnMSGetDeviceInfoList()
   * \brief   provides a list of connected devices with the information for each device
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   tVoid vOnMSGetDeviceInfoList(amt_tclServiceData* poMessage);

   /*******************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSSelectDevice()
   ***************************************************************************/
   /*!
   * \fn      tVoid spi_tclService:: vOnMSSelectDevice()
   * \brief   To provides a mechanism to select a device from 
   *          Mirror Link device Manager to establish a session
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   tVoid vOnMSSelectDevice(amt_tclServiceData* poMessage);

   /*******************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSLaunchApp()
   ***************************************************************************/
   /*!
   * \fn      tVoid spi_tclService:: vOnMSLaunchApp()
   * \brief    launches a remote application from the selected Mirror Link device
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   tVoid vOnMSLaunchApp(amt_tclServiceData* poMessage);

   /*******************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSTerminateApp()
   ***************************************************************************/
   /*!
   * \fn      tVoid spi_tclService:: vOnMSTerminateApp()
   * \brief   terminates the remote application running on the Mirror Link device
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   tVoid vOnMSTerminateApp(amt_tclServiceData* poMessage);

   /*******************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSGetAppList()
   ***************************************************************************/
   /*!
   * \fn      tVoid spi_tclService:: vOnMSGetAppList()
   * \brief   retrieves list of supported applications for the Device Handle provided
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   tVoid vOnMSGetAppList(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSGetAppIconData()
   ***************************************************************************/
   /*!
   * \fn      tVoid spi_tclService:: vOnMSGetAppIconData()
   * \brief   To retrieve the App Icon dData for the given URL
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSGetAppIconData(amt_tclServiceData* poMessage) const ;

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSSetAppIconAttr()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSSetAppIconAttr()
   * \brief   To Set an applications icon attributes
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSSetAppIconAttr(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSSetVehicleConfig()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSSetVehicleConfig()
   * \brief   To set the vehicle configuration
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSSetVehicleConfig(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSSetVehicleMovementState()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSSetVehicleMovementState()
   * \brief   To set the vehicle Movement State
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSSetVehicleMovementState(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSVehicleMechanicalSpeed()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSVehicleMechanicalSpeed()
   * \brief   To set the vehicle Mechanical Speed
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSVehicleMechanicalSpeed(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSSetVideoBlockingMode()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSSetVideoBlockingMode()
   * \brief   To set the video blcoking mode
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSSetVideoBlockingMode(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSSetAudioBlockingMode()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSSetAudioBlockingMode()
   * \brief   To set the audio blocking mode
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSSetAudioBlockingMode(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSInvokeBTDeviceAction()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSInvokeBTDeviceAction()
   * \brief   To set the BT Device action to be taken
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSInvokeBTDeviceAction(amt_tclServiceData* poMessage);
   
   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSSetOrientationMode()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSSetOrientationMode()
   * \brief   To set the Orientation Mode
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSSetOrientationMode(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSSetScreenSize()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSSetScreenSize()
   * \brief   To set the Screen Size
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSSetScreenSize(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSGetVideoSettings()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSGetVideoSettings()
   * \brief   To Get the video settings
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSGetVideoSettings(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSSetDeviceUsagePreference()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSSetDeviceUsagePreference()
   * \brief   To set the device usage preference
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSSetDeviceUsagePreference(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSGetDeviceUsagePreference()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSGetDeviceUsagePreference()
   * \brief   To get the device usage preference
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSGetDeviceUsagePreference(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSAccessoryDisplayContext()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSAccessoryDisplayContext()
   * \brief   To set the accessory disaply context
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSAccessoryDisplayContext(amt_tclServiceData* poMessage);

      /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSSetAccessoryDisplayContext()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSSetAccessoryDisplayContext()
   * \brief   To set the accessory disaply context
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSSetAccessoryDisplayContext(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSAccessoryAudioContext()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSAccessoryAudioContext()
   * \brief   To set the accessory audio context
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSAccessoryAudioContext(amt_tclServiceData* poMessage);


   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSAccessoryAppState()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSAccessoryAppState()
   * \brief   To set the accessory app state
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSAccessoryAppState(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSSetClientCapabilities()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSSetClientCapabilities()
   * \brief   To set the client capabilities
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSSetClientCapabilities(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSSetMLNotificationEnabledInfo()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSSetMLNotificationEnabledInfo()
   * \brief   To set the ML Notification Enabled Info
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSSetMLNotificationEnabledInfo(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSInvokeNotificationAction()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSInvokeNotificationAction()
   * \brief   To invoke the respective action for the received Notification Event.
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSInvokeNotificationAction(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSSendTouchEvent()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSSendTouchEvent()
   * \brief   To set the Touch or Pointer events.
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSSendTouchEvent(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSSendKeyEvent()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSSendKeyEvent()
   * \brief   To set the Key events.
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSSendKeyEvent(amt_tclServiceData* poMessage);
   


   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSSetVehicleBTAddress()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSSetVehicleBTAddress()
   * \brief   To set the vehicle Blue tooth address.
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSSetVehicleBTAddress(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclService:: vOnMSSetRegion()
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSSetRegion()
   * \brief   To set the Region.for the application certification
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSSetRegion(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION: tVoid spi_tclService::vOnMSDiPoSwitchRequired
   ***************************************************************************/
   /*!
   * \fn     tVoid vOnMSDiPoSwitchRequired
   * \brief  Interface to check if role switch id required for the DiPo Device
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSDiPoSwitchRequired(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION: tVoid spi_tclService::vOnDataServiceSubscribeRquest
   ***************************************************************************/
   /*!
   * \fn     tVoid vOnDataServiceSubscribeRquest
   * \brief  Loopback message to subscribe/unsubscribe the  location data service.
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnDataServiceSubscribeRquest(amt_tclServiceData* poMessage);
   
   /***************************************************************************
   ** FUNCTION: tVoid spi_tclService::vOnMSGetKeyIconData
   ***************************************************************************/
   /*!
   * \fn     tVoid spi_tclService
   * \brief  Interface to get key Icon data for a ML device
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   t_Void vOnMSGetKeyIconData(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION: tVoid spi_tclService::vOnMSSetDisplayAttributes
   ***************************************************************************/
   /*!
   * \fn     tVoid vOnMSSetDisplayAttributes()
   * \brief  Interface to Set the Screen attributes for various technologies
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/
   tVoid vOnMSSetDisplayAttributes(amt_tclServiceData* poMessage);


   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclCmdInterface::vOnVehicleBTAddressUpdate(t_Bool...)
   ***************************************************************************/
   /*!
   * \fn      vOnVehicleBTAddressUpdate(amt_tclServiceData* poMessage)
   * \brief   Interface to update the vehicle BT address info update.
   * \param   [IN] poMessage: Message data
   * \retval  None
   **************************************************************************/
   t_Void vOnLbBTAddressUpdate(amt_tclServiceData* poMessage);
   
   /***************************************************************************
   ** FUNCTION: tVoid spi_tclService::vOnMSSendRotaryCtrlEvent
   ***************************************************************************/
   /*!
   * \fn     tVoid vOnMSSendRotaryCtrlEvent()
   * \brief  Interface to Send Rotary Controller Event.
   * \param   [IN].*poMessage - Pointer  to Service Data message.
   * \retval  tVoid
   **************************************************************************/   
   tVoid vOnMSSendRotaryCtrlEvent(amt_tclServiceData* poMessage);

	/**************************************************************************
	 ** FUNCTION   : tVoid spi_tclService:: vOnMSSetAccessoryAudioContext()
	 ***************************************************************************/
	/*!
	 * \fn      tVoid vOnMSSetAccessoryAudioContext()
	 * \brief   To set the current accessory audio context
	 * \param   [IN].*poMessage - Pointer  to Service Data message.
	 * \retval  tVoid
	 **************************************************************************/
	t_Void vOnMSSetAccessoryAudioContext(amt_tclServiceData* poMessage);

	/**************************************************************************
	 ** FUNCTION   : tVoid spi_tclService:: vOnMSSetFeatureRestrictions()
	 ***************************************************************************/
	/*!
	 * \fn      tVoid vOnMSSetFeatureRestrictions()
	 * \brief   To inform the projection device the feature restrictions to be applied when vehicle is in Park/Drive Mode.
		  Client should set the restrictions irrespective of projection session state
	 * \param   [IN].*poMessage - Pointer  to Service Data message.
	 * \retval  tVoid
	 **************************************************************************/
	t_Void vOnMSSetFeatureRestrictions(amt_tclServiceData* poMessage);

   /**************************************************************************
    ** FUNCTION   : tVoid spi_tclService:: vOnMSSetTechnologyPreference()
    ***************************************************************************/
   /*!
    * \fn      tVoid vOnMSSetTechnologyPreference()
    * \brief   To set the preferred SPI technology for devices which support more than once technology
    * \param   [IN].*poMessage - Pointer  to Service Data message.
    * \retval  tVoid
    **************************************************************************/
   t_Void vOnMSSetTechnologyPreference(amt_tclServiceData* poMessage);

   /**************************************************************************
    ** FUNCTION   : tVoid spi_tclService:: vOnMSSetDeviceSelMode()
    ***************************************************************************/
   /*!
    * \fn      tVoid vOnMSSetDeviceSelMode()
    * \brief   To set the device selection mode
    * \param   [IN].*poMessage - Pointer  to Service Data message.
    * \retval  tVoid
    **************************************************************************/
   t_Void vOnMSSetDeviceSelMode(amt_tclServiceData* poMessage);

   /**************************************************************************
    ** FUNCTION   : tVoid spi_tclService:: vOnMSGetTechnologyPreference()
    ***************************************************************************/
   /*!
    * \fn      tVoid vOnMSGetTechnologyPreference()
    * \brief   To get the last set preferred SPI technology
    * \param   [IN].*poMessage - Pointer  to Service Data message.
    * \retval  tVoid
    **************************************************************************/
   t_Void vOnMSGetTechnologyPreference(amt_tclServiceData* poMessage);



	/***************************************************************************
	 ******************************END OF PROTECTED******************************
	 ***************************************************************************/


private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   * \Assignment Operator, will not be implemented.
   * \Avoids Lint Prio 3 warning: Info 1732: new in constructor for class
   * \'spi_tclService' which has no assignment operator.
   * \NOTE:
   * \This is a technique to disable the assignment operator for this class.
   * \So if an attempt for the assignment is made linker complains.
   ***************************************************************************/
   spi_tclService& operator=(const spi_tclService &oClientHandler);

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclService::bUpdateClients(tCU16 cu16FunID)
   ***************************************************************************/
   /*!
   * \fn      tBool bUpdateClients(tCU16 cu16FunID)
   * \brief   This function is used to update all registered clients.
   *          with respective property status.
   * \param   cu16FunID  : [IN] Property Function ID
   * \retval  tBool  : True if the update is sent successfully
   *                   else False
   **************************************************************************/
   tBool bUpdateClients(tCU16 cu16FunID);

   /***************************************************************************
   ** FUNCTION: tVoid spi_tclService::vPostDeviceStatusInfo(tenDeviceStatusInfo enDeviceStatusInfo)
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceStatusInfo(DeviceStatusInfo enDeviceStatusInfo)
   * \brief  It notifies the client on change in any device attributes.
   *         The client can retrieve the detailed information
   *         via the methods provided.
   * \param  enDeviceStatusInfo : enum value which stores device status
   * \param  enDevConnType: Indicates the type of connection USB, WiFi etc
   * \param  enDeviceStatus: Indicates the status of the device(added, removed etc)
   * \retval  tVoid
   **************************************************************************/
   tVoid vPostDeviceStatusInfo(t_U32 u32DevHandle,
      tenDeviceConnectionType enDevConnType,
      tenDeviceStatusInfo enDeviceStatus);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostDAPStatusInfo
   ***************************************************************************/
   /*!
   * \fn     vPostDAPStatusInfo
   * \brief  It notifies the client about DAP authentication progress information
   *            for a Mirror Link device.
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] enDevConnType   : Identifies the Connection Type.
   * \param  [IN] enDAPStatus : DAP Authentication Progress Status.
   **************************************************************************/
   t_Void vPostDAPStatusInfo(t_U32 u32DeviceHandle,
      tenDeviceConnectionType enDevConnType,
      tenDAPStatus enDAPStatus);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostAppStatusInfo()
   ***************************************************************************/
   /*!
   * \fn     vPostAppStatusInfo(t_U32 u32DeviceHandle, 
   *             tenDeviceConnectionType enDevConnType, tenAppStatusInfo enAppStatus)
   * \brief  It notifies the client upon application list change of a Mirror Link 
   *         device. The client can retrieve the detailed information of the  
   *         applications via the methods provided. If a Mirror Link device is 
   *         disconnected, Device Handle is set to 0xFFFF and DeviceConnectionType 
   *         is set to UNKNOWN_CONNECTION.
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] enDevConnType   : Identifies the Connection Type.
   * \param  [IN] enAppStatus : Provides application Status Information.
   **************************************************************************/
   t_Void vPostAppStatusInfo(t_U32 u32DeviceHandle,
      tenDeviceConnectionType enDevConnType,
      tenAppStatusInfo enAppStatus);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostSessionStatusInfo()
   ***************************************************************************/
   /*!
   * \fn     vPostSessionStatusInfo(t_U32 u32DeviceHandle, 
   *             tenDeviceCategory enDevCat, tenSessionStatus enSessionStatus)
   * \brief  It notifies the client about the ML Session status updates
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] enDevCat   : Identifies the Device category.
   * \param  [IN] enSessionStatus : Session status
   **************************************************************************/
   t_Void vPostSessionStatusInfo(t_U32 u32DeviceHandle,
      tenDeviceCategory enDevCat,
      tenSessionStatus enSessionStatus);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclService::vPopulateDeviceInfoList()
   ***************************************************************************/
   /*!
   * \fn      tVoid vPopulateDeviceInfoList(
   *                bpstl::vector<midw_fi_tcl_DeviceDetails> &rfrvecDeviceInfo,
   *               std::vector<trDeviceInfo> vecrDeviceInfoList)
   * \brief   This function is used populate the midw_fi_DeviceDetails vector
   *          from the trDeviceInfo's vector fetched from the CmdInterface
   * \param   rfrvecDeviceInfo   : [OUT] List of midw_fi_tcl_DeviceDetails
   * \param   vecrDeviceInfoList : [IN] List of trDeviceInfo's of the devices
   * \retval  tVoid
   **************************************************************************/
   tVoid vPopulateDeviceInfoList(bpstl::vector<midw_fi_tcl_DeviceDetails> &rfrvecDeviceInfo,
      std::vector<trDeviceInfo> vecrDeviceInfoList);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclService::vPopulateAppInfoList()
   ***************************************************************************/
   /*!
   * \fn      tVoid vPopulateAppInfoList(tU32 u32DevId,
   *                bpstl::vector<midw_fi_tcl_AppInfo>& rfrmapAppInfoList,
   *                std::vector<trAppDetails> vecrAppDetailsList)
   * \brief   This function is used populate the midw_fi_tcl_AppInfo vector
   *          from the trAppDetails's vector fetched from the CmdInterface
   * \param   u32DevId          : [IN] Device Handle
   * \param   rfrmapAppInfoList : [OUT] List of midw_fi_tcl_AppInfo's 
   * \param   vecrAppDetailsList: [IN] List of trAppDetails supported by
   *                                   the device
   * \retval  tVoid
   **************************************************************************/
   tVoid vPopulateAppInfoList(tU32 u32DevId,
      bpstl::vector<midw_fi_tcl_AppDetails>& rfvecAppDetailsList,
      std::vector<trAppDetails> vecrAppDetailsList) const;

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclService::vPostSelectDeviceResult
    ***************************************************************************/
   /*!
    * \fn     vPostSelectDeviceResult
    * \brief  It provides the result of  select device request
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] enDevConnType   : Identifies the Connection Type.
   * \param  [IN] enDevConnReq    : Identifies the Connection Request.
   * \param  [IN] enDevCat    : Identifies the device category
   * \param  [IN] enRespCode  : Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] bIsPairingReq  : Set when BT pairing is required.
   *         (Not valid for deselection)
   * \param  [IN] rcUsrCntxt  : User Context Details.
   * \sa     spi_tclCmdInterface::vSelectDevice
   **************************************************************************/
   virtual tVoid vPostSelectDeviceResult(t_U32 u32DeviceHandle,
         tenDeviceConnectionType enDevConnType, tenDeviceConnectionReq enDevConnReq,
         tenDeviceCategory enDevCat,
         tenResponseCode enResponseCode, tenErrorCode enErrorCode,
         t_Bool bIsPairingReq, const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vLaunchAppResult()
   ***************************************************************************/
   /*!
   * \fn     vLaunchAppResult(t_U32 u32DeviceHandle, t_U32 u32AppHandle, 
   *            tenDiPOAppType enDiPOAppType, tenResponseCode enResponseCode, 
   *			     tenErrorCode enErrorCode, const trUserContext corfrUsrCntxt)
   * \brief  It provides result of a remote application launch in selected Mirror Link device.
   * \param  [IN] u32DeviceHandle      : Uniquely identifies the target Device.
   * \param  [IN] u32AppHandle : Uniquely identifies an Application on
   *              the target Device. This value will be obtained from AppList Interface.
   *              This value will be set to 0xFFFFFFFF if DeviceCategory = DEV_TYPE_DIPO.
   * \param  [IN] enDiPOAppType : Identifies the application to be launched on a DiPO device.
   *              This value will be set to NOT_USED if DeviceCategory = DEV_TYPE_MIRRORLINK.
   * \param  [IN] enResponseCode  :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] corfrUsrCntxt	 : User Context Details.
   * \sa      spi_tclCmdInterface::vLaunchApp
   **************************************************************************/
   t_Void vLaunchAppResult(t_U32 u32DeviceHandle, 
      t_U32 u32AppHandle, 
      tenDiPOAppType enDiPOAppType,
      tenResponseCode enResponseCode, 
      tenErrorCode enErrorCode, 
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vTerminateAppResult()
   ***************************************************************************/
   /*!
   * \fn     vTerminateAppResult(t_U32 u32DeviceHandle, t_U32 u32AppHandle,
   *             tenResponseCode enResponseCode, tenErrorCode enErrorCode,
   * 				const trUserContext rcUsrCntxt)
   * \brief  It terminates the remote application running on the Mirror Link device.
   * \param  [IN] enResponseCode  :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] u32DeviceHandle		  : Unique handle of the device.
   * \param  [IN] u32AppHandle : Unique handle of the application to be terminated.
   * \param  [IN] rcUsrCntxt : User Context Details.
   * \sa     spi_tclCmdInterface::vTerminateApp
   **************************************************************************/
   t_Void vTerminateAppResult(t_U32 u32DeviceHandle, 
      t_U32 u32AppHandle, 
      tenResponseCode enResponseCode, 
      tenErrorCode enErrorCode, 
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostAppIconDataResult()
   ***************************************************************************/
   /*!
   * \fn     vPostAppIconDataResult(tenIconMimeType enIconMimeType, 
   *            t_Char* pczAppIconData,t_U32 u32Len, const trUserContext rcUsrCntxt)
   * \brief  It retrieves icon data referenced by the AppList.AppIconXXXURLs
   * \param  [IN] enIconMimeType :  Mime Type of the icon pointed by AppIconURL.
   *              If image is not available then this parameter would be set
   *              to NULL (zero length string).
   * \param  [IN] pczAppIconData : Byte Data Stream from the icon image file.
   *              Format of the file is de-fined by IconMimeType parameter.
   * \param  [IN] u32Len : Length the data stream
   * \param  [IN] rcUsrCntxt		: User Context Details.
   * \sa     spi_tclCmdInterface::vGetAppIconData
   **************************************************************************/
   t_Void vPostAppIconDataResult(tenIconMimeType enIconMimeType,
      const t_U8* pcu8AppIconData,
      t_U32 u32Len,
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostSetAppIconAttributesResult()
   ***************************************************************************/
   /*!
   * \fn     vPostSetAppIconAttributesResult(tenResponseCode enResponseCode,
   *                          tenErrorCode enErrorCode, const trUserContext rcUsrCntxt)
   * \brief  sets application icon attributes for retrieval of application icons.
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSetAppIconAttributes
   **************************************************************************/
   t_Void vPostSetAppIconAttributesResult(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode, 
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostSetVideoBlockingModeResult()
   ***************************************************************************/
   /*!
   * \fn     vPostSetVideoBlockingModeResult(tenResponseCode enResponseCode,
   *                          tenErrorCode enErrorCode, const trUserContext& corfrcUsrCntxt)
   * \brief  sets video blocking mode response
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] corfrcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSetAppIconAttributes
   **************************************************************************/
   t_Void vPostSetVideoBlockingModeResult(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode, 
      const trUserContext& corfrcUsrCntxt);

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclService::vPostBluetoothDeviceStatus(...)
    ***************************************************************************/
   /*!
    * \fn     vPostBluetoothDeviceStatus(t_U32 u32BluetoothDevHandle,
    *            t_U32 u32ProjectionDevHandle, tenBTChangeInfo enBTStatus,
    *            t_Bool bCallActive)
    * \brief  It notifies the client when changing from or to a BT device.
    * \param  [IN] u32BluetoothDevHandle  : Uniquely identifies a Bluetooth Device.
    * \param  [IN] u32ProjectionDevHandle : Uniquely identifies a Projection Device.
    * \param  [IN] bSameDevice : Inidcates whether BT & Projection device are same
    *              or different devices.
    * \param  [IN] enBTStatus  : Enum value which stores BT device status
    **************************************************************************/
   virtual t_Void vPostBluetoothDeviceStatus(t_U32 u32BluetoothDevHandle,
         t_U32 u32ProjectionDevHandle,
         tenBTChangeInfo enBTChange,
         t_Bool bSameDevice,
         t_Bool bCallActive);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostSetOrientationModeResult()
   ***************************************************************************/
   /*!
   * \fn     vPostSetOrientationModeResult(tenResponseCode enResponseCode,
   *                  tenErrorCode enErrorCode, const trUserContext& corfrcUsrCntxt)
   * \brief  Interface to set the orientation mode of the projected display.
   * \param  [IN] enResponseCode : Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] corfrcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSetOrientationMode
   **************************************************************************/
   t_Void vPostSetOrientationModeResult(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode, 
      const trUserContext& corfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostDeviceDisplayContext()
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceDisplayContext(t_U32 u32DeviceHandle, t_Bool bDisplayFlag,
   *         tenDisplayContext enDisplayContext, const trUserContext rcUsrCntxt);
   * \brief  This interface is used by Mirror Link/DiPO device to inform the client
   *              about its current display con-text.
   * \param  [IN] u32DeviceHandle  : Uniquely identifies the target Device.
   * \param  [IN] bDisplayFlag     : TRUE � Start Display Projection, FALSE � Stop Display Projection.
   * \param  [IN] enDisplayContext : Display context of the projected device.
   * \param  [IN] rcUsrCntxt       : User Context Details.
   **************************************************************************/
   t_Void vPostDeviceDisplayContext(t_U32 u32DeviceHandle,
      t_Bool bDisplayFlag,
      tenDisplayContext enDisplayContext,
      const trUserContext rcUsrCntxt);

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostDeviceAudioContext
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceAudioContext(t_U32 u32DeviceHandle, t_Bool bDisplayFlag,
   *         tenDisplayContext enDisplayContext, const trUserContext rcUsrCntxt);
   * \brief  this function is used to update the audio context changes to HMI
   * \param  [IN] u32DeviceHandle  : Uniquely identifies the target Device.
   * \param  [IN] bPlayFlag        : TRUE � Start Projection playback, 
   *                                 FALSE � Stop Projection playback.
   * \param  [IN] u8AudioContext   : Audio context of the projected device.
   * \param  [IN] rcUsrCntxt       : User Context Details.
   **************************************************************************/
   t_Void vPostDeviceAudioContext(t_U32 u32DeviceHandle,
      t_Bool bPlayFlag, t_U8 u8AudioContext,
      const trUserContext rcUsrCntxt);

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostDeviceAppState
   **                   (tenSpeechAppState enSpeechAppState,...)
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceAudioContext()
   * \brief  This function used to update HMI about the App state changes
   * \param  [IN] enSpeechAppState : Speech app state
   * \param  [IN] enPhoneAppState  : Phone App state
   * \param  [IN] enNavAppState    : Navigation App state
   * \param  [IN] rcUsrCntxt       : User Context Details.
   **************************************************************************/
   t_Void vPostDeviceAppState(tenSpeechAppState enSpeechAppState, 
      tenPhoneAppState enPhoneAppState, tenNavAppState enNavAppState, 
      const trUserContext rcUsrCntxt);


   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostSendKeyEvent()
   ***************************************************************************/
   /*!
   * \fn     vPostSendKeyEvent(tenResponseCode enResponseCode,tenErrorCode enErrorCode,
   *				const trUserContext rcUsrCntxt)
   * \brief   Interface to send the response to send key events Methodstart.
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *          Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSendKeyEvent
   **************************************************************************/
   t_Void vPostSendKeyEvent(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode, 
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostSendTouchEvent()
   ***************************************************************************/
   /*!
   * \fn     vPostSendTouchEvent(tenResponseCode enResponseCode,tenErrorCode enErrorCode
   *				const trUserContext rcUsrCntxt)
   * \brief  Interface to send the response to the send Touch or Pointer events.
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSendTouchEvent
   **************************************************************************/
   t_Void vPostSendTouchEvent(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode, 
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostInvokeNotificationActionResult()
   ***************************************************************************/
   /*!
   * \fn     vPostInvokeNotificationActionResult(tenResponseCode enResponseCode,
   *            tenErrorCode enErrorCode, const trUserContext rcUsrCntxt)
   * \brief  Interface to send the response to the
   *         received Notification Event.
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt		 : User Context Details.
   * \sa     spi_tclCmdInterface::vInvokeNotificationAction
   **************************************************************************/
   t_Void vPostInvokeNotificationActionResult(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode,
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostSetMLNotificationEnabledInfoResult()
   ***************************************************************************/
   /*!
   * \fn     vPostSetMLNotificationEnabledInfoResult(t_U32 u32DeviceHandle, 
   *            tenResponseCode enResponseCode, tenErrorCode enErrorCode, 
   *            const trUserContext rcUsrCntxt)
   * \brief  Interface to send the response to the device notification preference reqwuest for
   *         applications (only for Mirror Link devices). If notification for
   *         all the applications has to be
   * \param  [IN] u32DeviceHandle   : Uniquely identifies the target Device.
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSetMLNotificationEnabledInfo
   **************************************************************************/
   t_Void vPostSetMLNotificationEnabledInfoResult(t_U32 u32DeviceHandle,
      tenResponseCode enResponseCode, 
      tenErrorCode enErrorCode, 
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostNotificationInfo()
   ***************************************************************************/
   /*!
   * \fn     vPostNotificationInfo(t_U32 u32DeviceHandle, t_U32 u32AppHandle,
   *            NotificationData &rfrNotificationData)
   * \brief  Interface to provide Notification Information received from
   *         the Mirror Link server (only for Mirror Link devices).
   * \param  [IN] u32DeviceHandle   :  Handle uniquely identifies a device.
   * \param  [IN] u32AppHandle      : Handle uniquely identifies an application 
   *              on the device.
   * \param  [IN] corfrNotificationData : Provides notification event details
   * \sa     spi_tclCmdInterface::vSetMLNotificationEnabledInfo
   **************************************************************************/
   t_Void vPostNotificationInfo(t_U32 u32DeviceHandle,
      t_U32 u32AppHandle, 
      const trNotiData& corfrNotificationData);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostApplicationMediaMetaData(t_U32& ...
   ***************************************************************************/
   /*!
   * \fn     vPostApplicationMediaMetaData(const trAppMediaMetaData& rfcorApplicationMediaMetaData,
   *            const trUserContext& rfcorUsrCntxt)
   * \brief  Interface to notify application media metadata to the client.
   * \param  [IN] rfcorApplicationMediaMetaData : Contains the media metadata information
   *              related to an application.
   * \param  [IN] rfcorUsrCntxt    : User Context Details.
   * \sa
   **************************************************************************/
   virtual t_Void vPostApplicationMediaMetaData(const trAppMediaMetaData& rfcorApplicationMediaMetaData,
      const trUserContext& rfcorUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostApplicationPhoneData(...
   ***************************************************************************/
   /*!
   * \fn     vPostApplicationPhoneData(const trAppPhoneData& rfcorApplicationPhoneData,
   *            const trUserContext& rfcorUsrCntxt)
   * \brief  Interface to notify application media metadata to the client.
   * \param  [IN] rfcorApplicationPhoneData : Contains the phone related information.
   * \param  [IN] rfcorUsrCntxt    : User Context Details.
   * \sa
   **************************************************************************/
   virtual t_Void vPostApplicationPhoneData(const trAppPhoneData& rfcorApplicationPhoneData,
      const trUserContext& rfcorUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostApplicationMediaPlaytime(...
   ***************************************************************************/
   /*!
   * \fn     vPostApplicationMediaPlaytime(const trAppMediaPlaytime& rfcorApplicationMediaPlaytime,
   *            const trUserContext& rfcorUsrCntxt)
   * \brief  Interface to notify application media metadata to the client.
   * \param  [IN] rfcorApplicationMediaPlaytime : Contains the media play time of current playing track.
   * \param  [IN] rfcorUsrCntxt    : User Context Details.
   * \sa
   **************************************************************************/
   virtual t_Void vPostApplicationMediaPlaytime(const trAppMediaPlaytime& rfcorApplicationMediaPlaytime,
      const trUserContext& rfcorUsrCntxt);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclService::vPostSubscribeForLocData(t_Bool...)
    ***************************************************************************/
   /*!
   * \fn      vPostSubscribeForLocData(t_Bool bSubscribe, tenLocationDataType enLocDataType)
   * \brief   Subscribes/Unsubscribes for location data notifications from
   *              service-provider (such as POS_FI).
   * \param   [IN] bSubscribe:
   *               TRUE - To subscribe for location data notifications.
   *               FALSE - To unsubscribe for location data notifications.
   * \param   [IN] enLocDataType: Type of data to be subscribed/unsubscribed
   * \retval  None
   **************************************************************************/
   t_Void vPostSubscribeForLocData(t_Bool bSubscribe, tenLocationDataType enLocDataType);

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclService::vPostBTPairingRequired(...)
    ***************************************************************************/
   /*!
    * \fn     vPostBTPairingRequired(const t_String& rfcoszBTAddress, t_Bool bPairingRequired)
    * \brief  It notifies the client when changing from or to a BT device.
    * \param  [IN] rfcoszBTAddress  : BT Address of device.
    * \param  [IN] bPairingRequired : true - if device needs to be paired, else false.
    **************************************************************************/
   t_Void vPostBTPairingRequired(const t_String& rfcoszBTAddress,
         t_Bool bPairingRequired);
		 
  /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostKeyIconDataResult
   **                   (const t_U32 cou32DevId, t_Char* pczAppIconData, ..)
   ***************************************************************************/
   /*!
   * \fn     vPostKeyIconDataResult(tenIconMimeType enIconMimeType,
   *            t_Char* pczAppIconData,t_U32 u32Len, const trUserContext rcUsrCntxt)
   * \brief  It retrieves icon data referenced by the Key Icon URLs
   * \param  [IN] cou32DevId : Device Handle
   * \param  [IN] pczKeyIconData : Byte Data Stream from the icon image file.
   * \param  [IN] u32Len : data stream length
   * \param  [IN] rcUsrCntxt         : User Context Details.
   * \sa     spi_tclCmdInterface::vGetKeyIconData
   **************************************************************************/
   t_Void vPostKeyIconDataResult(const t_U32 cou32DevId,
              const t_U8* pczKeyIconData,
              t_U32 u32Len,
              const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclInputRespIntf::vPostMLClientCapInfo
   **                   (, t_Char* pczAppIconData, ..)
   ***************************************************************************/
   /*!
   * \fn     vPostKeyIconDataResult(tenIconMimeType enIconMimeType,
   *            t_Char* pczAppIconData,t_U32 u32Len, const trUserContext rcUsrCntxt)
   * \brief  It retrieves icon data referenced by the AppList.AppIconXXXURLs
   * \param  [IN] cou32DevId : Device Handle
   * \param  [IN] u16NumXDevices : Number of X-Device Keys supported.
   * \param  [IN] vecrXDeviceKeyDetail : XDevice Key Details.
   **************************************************************************/
   t_Void vPostMLServerCapInfo(const t_U32 cou32DevId,
                              t_U16 u16NumXDevices,
                              trMLSrvKeyCapabilities rSrvKeypabilities);
							  
  /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vUpdateSessionStatusInfo
   **                   (t_U32 u32DeviceHandle,...)
   ***************************************************************************/
   /*!
   * \fn     vUpdateSessionStatusInfo()
   * \brief  Used to update the session status to HMI.
   * \param  [IN] u32DeviceHandle  : Device handle 
   * \param  [IN] enDevCat         : Device category
   * \param  [IN] enSessionStatus  : Session status.
   **************************************************************************/
   t_Void vUpdateSessionStatusInfo(t_U32 u32DeviceHandle,
      tenDeviceCategory enDevCat,
      tenSessionStatus enSessionStatus);

   /**************************************************************************
   ** FUNCTION:  t_Void spi_tclService::vInsertKeyCodeToMap()
   **************************************************************************/
   /*!
   * \fn      vInsertKeyCodeToMap()
   * \brief   Inserts the received key code received from HMI and the corresponding 
              SPI key Code to map
    *\param   NONE
   **************************************************************************/		
   t_Void vInsertKeyCodeToMap();

   /***************************************************************************
   ** FUNCTION: tenKeyCode spi_tclService::enGetKeyCode(...)const
   ***************************************************************************/
   /*!
   * \fn       enGetKeyCode(spi_e32KeyCode e32SpiKeyCode)const
   * \brief   Retrieves the SPI key Code corresponding to the Key Code from HMI
   * \param   e32SpiKeyCode : Key Code from HMI
   **************************************************************************/
   tenKeyCode enGetKeyCode(spi_tenFiKeyCode e32SpiKeyCode)const; 

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostSessionStatusInfo()
   ***************************************************************************/
   /*!
   * \fn     t_Void spi_tclVideoRespInterface(t_U32 u32DeviceHandle, 
   *             tenDeviceCategory enDevCat, tenSessionStatus enSessionStatus)
   * \brief  It notifies the client about the ML Session status updates
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] enDevCat        : Identifies the Device category.
   * \param  [IN] enSessionStatus : Session status
   **************************************************************************/
   t_Void vSendSessionStatusInfo(t_U32 u32DeviceHandle,
      tenDeviceCategory enDevCat,
      tenSessionStatus enSessionStatus);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vUpdateAppBlockingInfo()
   ***************************************************************************/
   /*!
   * \fn     virtual t_Void vUpdateAppBlockingInfo(t_U32 u32DeviceHandle, 
   *             tenDeviceCategory enDevCat, tenSessionStatus enSessionStatus)
   * \brief  It notifies the client about the Application blocking using the 
   *          session status update
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] enDevCat        : Identifies the Device category.
   * \param  [IN] enSessionStatus : Session status
   **************************************************************************/
   t_Void vUpdateAppBlockingInfo(t_U32 u32DeviceHandle,
      tenDeviceCategory enDevCat,
      tenSessionStatus enSessionStatus);

    /***************************************************************************
   ** FUNCTION:  t_Void spi_tclService::vOnGPSData(trGPSData rGPSData)
   ***************************************************************************/
   /*!
   * \fn      vOnGPSData(trGPSData rGPSData)
   * \brief   Interface to receive the GPS Data.
   * \param   [IN] rGPSData: GPS Data.
   * \retval  None
   **************************************************************************/
   t_Void vOnGPSData(trGPSData rGPSData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclService::vOnSensorData(trSensorData rSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnSensorData(trSensorData rSensorData)
   * \brief   Interface to receive the Sensor Data.
   * \param   [IN] rSensorData: Sensor Data
   * \retval  None
   **************************************************************************/
   t_Void vOnSensorData(trSensorData rSensorData);
   
   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclService::vOnAccSensorData
   ** (const std::vector<trAccSensorData>& corfvecrAccSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnAccSensorData(const std::vector<trAccSensorData>& corfvecrAccSensorData)
   * \brief   Interface to receive Acceleration the Sensor Data.
   * \param   [IN] corfvecrAccSensorData: Acceleration Sensor Data
   * \retval  None
   **************************************************************************/
   t_Void vOnAccSensorData(const std::vector<trAccSensorData>& corfvecrAccSensorData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclService::vOnGyroSensorData
   ** (const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnGyroSensorData(const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
   * \brief   Interface to receive the Gyro Sensor Data.
   * \param   [IN] corfvecrGyroSensorData: Gyro Sensor Data
   * \retval  None
   **************************************************************************/
   t_Void vOnGyroSensorData(const std::vector<trGyroSensorData>& corfvecrGyroSensorData);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclService::vOnTelephoneCallActivity()
   ***************************************************************************/
   /*!
   * \fn      vOnTelephoneCallActivity(t_Bool bCallActive)
   * \brief   Interface to recieve Telephone call activity info
   * \param   [IN] bCallActive : true - If any call is active, else false.
   **************************************************************************/
   tVoid vOnTelephoneCallActivity(t_Bool bCallActive);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclService::vPopulateSrvKeyCapabilities()
   ***************************************************************************/
   /*!
   * \fn tVoid vPopulateSrvKeyCapabilities(bpstl::vector<midw_fi_tcl_XDeviceKeys> &rfrvecXDevicenfo,
   *                                  std::vector<trXDeviceKeyDetails> rfrvecXDevicenfo,
   *                                  midw_fi_tcl_KeyCapabilities &rKeyCapabilities,
   *                                  trKeyCapabilities rKeyCapabilities);
   * \brief   This function is used populate the server capabilities.
   * \param   rfrvecXDevicenfo   : [OUT] List of midw_fi_tcl_XDeviceKeys
   * \param   rfrvecXDevicenfo   : [IN]  List of trXDeviceKeyDetails
   * \param   rKeyCapabilities   : [OUT] midw_fi_tcl_KeyCapabilities
   * \param   rKeyCapabilities   : [IN]  trKeyCapabilities
   * \retval  tVoid
   **************************************************************************/
   tVoid vPopulateSrvKeyCapabilities(bpstl::vector<midw_fi_tcl_XDeviceKeys> &rfrvecXDevicenfo,
                                     std::vector<trXDeviceKeyDetails> vecrXDeviceKeyInfo,
                                     midw_fi_tcl_KeyCapabilities &rKeyCapabilities,
                                     trKeyCapabilities rMLKeyCapabilities,
                                     t_U16 u16NumXDevices);

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclService::vPostDeviceUsagePrefResult(t_U32...)
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceUsagePrefResult(t_U32 u32DeviceHandle,
   *            tenErrorCode enErrorCode)
   * \brief  It provides the result of Device Usage Preference Set Request
   * \param  [IN] coU32DeviceHandle: Device handle for which the device usage
   *         preference was set
   * \param  [IN] enErrorCode: Error code if setting device usage preference fails
   * \param  [IN] enDeviceCategory: Device Category
   * \param  [IN] enUsagePref: indicates whether the device usage preference is enabled or not
   * \param  [IN] corfrUsrCtxt : User context
   **************************************************************************/
   virtual t_Void vPostDeviceUsagePrefResult(const t_U32 coU32DeviceHandle,
         tenErrorCode enErrorCode, tenDeviceCategory enDeviceCategory,
         tenEnabledInfo enUsagePref, const trUserContext &corfrUsrCtxt);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclService::vPopulatePhoneMetadata()
   ***************************************************************************/
   /*!
   * \fn tVoid vPopulatePhoneMetadata(bpstl::vector<midw_fi_tcl_ApplicationPhoneCallMetadata> &rfrvecPhoneCallMetadata,
   *                                  std::vector<trPhoneCallMetaData> vecPhoneCallMetaDataList);
   * \brief   This function is used populate the server capabilities.
   * \param   rfrvecPhoneCallMetadata   : [OUT] List of midw_fi_tcl_ApplicationPhoneCallMetadata
   * \param   vecPhoneCallMetaDataList  : [IN]  List of trPhoneCallMetaData
   * \retval  tVoid
   **************************************************************************/
   t_Void vPopulatePhoneMetadata(bpstl::vector<midw_fi_tcl_ApplicationPhoneCallMetadata> &rfrvecPhoneCallMetadata,
                                 std::vector<trPhoneCallMetaData> vecPhoneCallMetaDataList);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vRequestDeviceAuthorization
   ***************************************************************************/
   /*!
   * \fn     vRequestDeviceAuthorization
   * \brief  Interface to send notification to user to authorize the device
   * \param  [IN] rfrvecDevAuthInfo : List of device authorization info
   * \sa
   **************************************************************************/
   t_Void vRequestDeviceAuthorization(std::vector<trDeviceAuthInfo> &rfrvecDevAuthInfo);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vPostDipoRoleSwitchResponse
   ***************************************************************************/
   /*!
   * \fn     vPostDipoRoleSwitchResponse
   * \brief  Post response to DIPO role switch request
   * \param  [IN] bRoleSwitchRequired : true if role switch is required. otherwise false
   * \param  [IN] rfcorUsrCntxt : User context
   * \param  [IN] cou32DeviceHandle : Device Handle
   * \sa
   **************************************************************************/
   t_Void vPostDipoRoleSwitchResponse(t_Bool bRoleSwitchRequired, const t_U32 cou32DeviceHandle,
         const trUserContext& rfcorUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclService::vUpdateDevAuthAndAccessInfo()
   ***************************************************************************/
   /*!
   * \fn     virtual t_Void vUpdateDevAuthAndAccessInfo(const t_U32 cou32DeviceHandle,
   *             const t_Bool cobHandsetInteractionReqd)
   * \brief  Notifies the authorization and access to AAP projection device.
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] cobHandsetInteractionReqd   : Set if interaction required on Handset.
   **************************************************************************/
   virtual t_Void vUpdateDevAuthAndAccessInfo(const t_U32 cou32DeviceHandle,
            const t_Bool cobHandsetInteractionReqd);

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclService::vPostNavigationStatus(...)
    ***************************************************************************/
   /*!
    * \fn     vPostNavigationStatus(const trNavStatusData& corfrNavStatusData)
    * \brief  It notifies the client whenever there is a navigation status
    *         change(ACTIVE/INACTIVE/UNAVAILABLE).
    * \param  corfrNavStatusData  : [IN] Structure containing the device handle,
    *                                    device category and navigation status
    **************************************************************************/
    t_Void vPostNavigationStatus(const trNavStatusData& corfrNavStatusData);

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclService::vPostNavigationNextTurnDataStatus(...)
   ***************************************************************************/
   /*!
    * \fn     vPostNavigationNextTurnDataStatus(
    *          const trNavNextTurnData& corfrNavNexTurnData)
    * \brief  It notifies the client whenever there is a navigation
    *          next turn event information.
    * \param  corfrNavNexTurnData : [IN] Structure containing device handle,
    *                               device category, road name,next turn details
    *                               such as side, event, image, angle and number
    **************************************************************************/
     t_Void vPostNavigationNextTurnDataStatus(const trNavNextTurnData& corfrNavNexTurnData);

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclService::vPostNavigationNextTurnDistanceDataStatus(...)
   ***************************************************************************/
   /*!
    * \fn     vPostNavigationNextTurnDistanceDataStatus(
    *          const trNavNextTurnDistanceData& corfrNavNextTurnDistData)
    * \brief  It notifies the client whenever there is a change in
    *          navigation next turn distance data.
    * \param  corfrNavNextTurnDistData : [IN] Structure containing device handle,
    *                                    device category, distance and
    *                                    time of the next turn
    **************************************************************************/
    t_Void vPostNavigationNextTurnDistanceDataStatus(
            const trNavNextTurnDistanceData& corfrNavNextTurnDistData);

     /*************************************************************************
      ** FUNCTION: t_Void spi_tclService::vPostNotificationData(...)
     *************************************************************************/
     /*
      * \fn     vPostNotificationData(
      *          const trNotificationData& corfrNotificationData)
      * \brief  It notifies the client whenever a notification is received.
      * \param  corfrNotificationData : [IN] Structure containing device handle,
      *                                      category and notification data
      ************************************************************************/
      t_Void vPostNotificationData(
              const trNotificationData& corfrNotificationData);

   /***************************************************************************
   * ! Data members
   ***************************************************************************/

   //! SPI Main Application pointer
   spi_tclMainApp*      m_poMainAppl;

   //! SPI Command Interface Pointer
   spi_tclCmdInterface* m_poSpiCmdIntf;

   //! Audio Policy pointer
   spi_tclAudioPolicy*  m_poAudioPolicy;

   //! Telephone FBlock client-handler pointer
   spi_tclTelephoneClient*   m_poTelephoneClient;

   //!Position FI client handler pointer
   spi_tclPosDataClientHandler*     m_poPosDataClient;

   //!Sensor FI client handler pointer
   spi_tclSensorDataClientHandler*  m_poSensorDataClient;

   //!SPM Client handler pointer
   spi_tclSPMClient* m_poSPMClient;

   //! Device Status
   midw_smartphoneintfi_tclMsgDeviceStatusInfoStatus m_enDevStatusInfo;

   //! DAP Status
   midw_smartphoneintfi_tclMsgDAPStatusInfoStatus m_enDAPStatusInfo;

   //! Application status
   midw_smartphoneintfi_tclMsgAppStatusInfoStatus m_AppInfoStatus;

   //! Bluetooth Device Status
   midw_smartphoneintfi_tclMsgBluetoothDeviceStatusStatus m_BTDeviceStatusMsg;

   //! device display context
   midw_smartphoneintfi_tclMsgDeviceDisplayContextStatus m_DeviceDispCntxt;

   //! Notification Info
   midw_smartphoneintfi_tclMsgNotificationInfoStatus m_NotificationInfo;

   //!Application media MetaData status Info
   midw_smartphoneintfi_tclMsgApplicationMediaMetaDataStatus m_AppMediaMetaData;

   //!Application phone data
   midw_smartphoneintfi_tclMsgApplicationPhoneDataStatus m_AppPhoneData;

   //!Application current playing current time
   midw_smartphoneintfi_tclMsgMediaPlayBackTimeStatus m_AppMediaPlayBacktime;

   //! device audio context
   midw_smartphoneintfi_tclMsgDeviceAudioContextStatus m_DeviceAudioCntxt;

   //! Device App state info
   midw_smartphoneintfi_tclMsgDiPOAppStatusInfoStatus m_DeviceAppState;

   //! Session status update
   midw_smartphoneintfi_tclMsgSessionStatusInfoStatus m_SessionStatus;

   //! Bluetooth pairing required message
   midw_smartphoneintfi_tclMsgBTPairingRequiredStatus m_BTPairingRequiredMsg;
   
   //! ML Server Key Capabilities
   midw_smartphoneintfi_tclMsgMLServerKeyCapabilitiesStatus m_MLSrvKeyCapabilities;

   //! Device authorization status
   midw_smartphoneintfi_tclMsgProjectionDeviceAuthorizationStatus m_DevAuthStatus;

   //! Navigation Status
   midw_smartphoneintfi_tclMsgNavigationStatusInfoStatus m_NavStatus;

   //! Navigation Next Turn Data Status
   midw_smartphoneintfi_tclMsgNavigationNextTurnDataStatus m_NavNextTurnStatus;

   //! Navigation Next Turn Distance Data Status
   midw_smartphoneintfi_tclMsgNavigationNextTurnDistanceDataStatus m_NavNextTurnDistanceStatus;

   //! Notification Event Status
   midw_smartphoneintfi_tclMsgAAPNotificationEventStatus  m_NotificationEventStatus;


   //! Map of key codes
   std::map<spi_tenFiKeyCode,tenKeyCode> m_mapKeyCode;

   //! DiPO feature support indicator
   t_Bool m_bIsDiPOSupported;

   //! For containing vehicle data
   trVehicleData m_rvehicledata;

   /***************************************************************************
   ****************************END OF PRIVATE**********************************
   ***************************************************************************/

   /***************************************************************************
   * Message map definition macro
   ***************************************************************************/
   DECLARE_MSG_MAP( spi_tclService)
};

#endif /* SPI_TCLSERVICE_H_*/

