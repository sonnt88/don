
/***********************************************************************/
/*!
* \file  spi_tclImpTraceStreamable.h
* \brief Generic message Sender
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Message sender
AUTHOR:         VIJETH
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
12.11.2013  | VIJETH      | Initial Version

\endverbatim
*************************************************************************/
#ifndef SPI_TCLIMPTRACESTREAMABLE_H_
#define SPI_TCLIMPTRACESTREAMABLE_H_
/******************************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------------------*/
#include "TraceStreamable.h"
#include "SPITypes.h"
using namespace spi::spitrace;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| forward declarations (scope: global)
|----------------------------------------------------------------------------*/

class ahl_tclBaseOneThreadApp;
/*!
* \class spi_tclImpTraceStreamable
* \brief Implementation of the Trace Streamer
* Trace commands are setup using this class.
*/

class spi_tclImpTraceStreamable: public TraceStreamable {
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /**************************************************************************************
   ** FUNCTION:  tVoid spi_tclImpTraceStreamable::ConvertU8toU32(tU32* cpu32Buffer,tU8* cpu8Buffer)
   ** /*!
   * \brief   Convert a U8 buffer type to U32 type .
   *
   * \param   cpu32Buffer: U32 buffer
   * \param   cpu8Buffer:  U8 buffer for concatenation .
   * \retval  NONE
   ***************************************************************************************/
   tVoid ConvertU8toU32(tU32 cpu32Buffer,tU8* cpu8Buffer);

   /**************************************************************************************
   ** FUNCTION:  tVoid spi_tclImpTraceStreamable::ConvertU8toU16(tU16 cpu16Buffer,tU8* cpu8Buffer)
   ** ** /*!
   * \brief   Convert a U8 buffer type to U16 type .
   *
   * \param   cpu32Buffer: U16 buffer
   * \param   cpu8Buffer:  U8 buffer for concatenation .
   * \retval  NONE
   ***************************************************************************************/
   tVoid ConvertU8toU16(tU16 cpu16Buffer,tU8* cpu8Buffer);

   /***************************************************************************
   ** FUNCTION:  spi_tclImpTraceStreamable::spi_tclImpTraceStreamable(ahl_.
   ***************************************************************************/
   /*!
   * \brief   Parameterized Constructor, based on Dependency Injection
   *          Principle (DIP)
   * \param   [cpoApp]:        (->I) Pointer to the main application
   * \retval  NONE
   **************************************************************************/
   explicit spi_tclImpTraceStreamable(ahl_tclBaseOneThreadApp* const cpoApp);

   /***************************************************************************
   ** FUNCTION:  virtual spi_tclImpTraceStreamable::~spi_tclImpTraceStrea..
   ***************************************************************************/
   /*!
   * \brief   Destructor
   * \param   NONE
   * \retval  NONE
   **************************************************************************/
   virtual ~spi_tclImpTraceStreamable();

   /***************************************************************************
   ** FUNCTION:  virtual tVoid spi_tclImpTraceStreamable::vSetupCmds()
   ***************************************************************************/
   /*!
   * \brief   The function sets up the commands supported via trace.
   *          This is used for mapping trace commands provided via the Trace
   *          input channel interface.
   * \param   NONE
   * \retval  NONE
   **************************************************************************/
   virtual tVoid vSetupCmds();

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *******************************PROTECTED************************************
   ***************************************************************************/

   /*********************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bLaunchApp(tU8 const* const cp..
   *********************************************************************************/
   /*!
   * \brief   Launches the application - Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed.
   **************************************************************************/
   tBool bLaunchApp(tU8 const* const cpu8Buffer) const;

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bTerminateApp(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief   Terminates the application - Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bTerminateApp(tU8 const* const cpu8Buffer) const;

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSelectDevice(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief   Select the device - Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bSelectDevice(tU8 const* const cpu8Buffer) const;

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bGetDeviceInfoList(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief   Gets the Device info list - Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]:TRUE if the command was successfully executed
   **************************************************************************/
   tBool bGetDeviceInfoList(tU8 const* const cpu8Buffer) const;

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bGetAppList(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief   Gets App list - Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]:TRUE if the command was successfully executed
   **************************************************************************/
   tBool bGetAppList(tU8 const* const cpu8Buffer) const;


   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetVehicleConfig()
   ***************************************************************************/
   /*!
   * \brief   SetVehicleConfiguration- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bSetVehicleConfig(tU8 const* const cpu8Buffer) const;

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetOrientationMode()
   ***************************************************************************/
   /*!
   * \brief   bSetOrientationMode- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]:TRUE if the command was successfully executed.
   **************************************************************************/
   tBool bSetOrientationMode(tU8 const* const cpu8Buffer) const;

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetNotificationEnabledInfo()
   ***************************************************************************/
   /*!
   * \brief   bSetNotificationEnabledInfo- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bSetNotificationEnabledInfo(tU8 const* const cpu8Buffer) const;

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetRegion()
   ***************************************************************************/
   /*!
   * \brief   bSetRegion- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bSetRegion(tU8 const* const cpu8Buffer) const;

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetDisplayContext()
   ***************************************************************************/
   /*!
   * \brief   bSetDisplayContext- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bSetDisplayContext(tU8 const* const cpuBuffer) const;

  /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetAudioContext()
   ***************************************************************************/
   /*!
   * \brief   bSetAudioContext- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bSetAudioContext(tU8 const* const cpuBuffer) const;
   
  /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetDiPOAppState()
   ***************************************************************************/
   /*!
   * \brief   bSetDiPOAppState- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bSetDiPOAppState(tU8 const* const cpuBuffer) const;

  /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bInvokeNotificationAction()
   ***************************************************************************/
   /*!
   * \brief   bInvokeNotificationAction- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bInvokeNotificationAction(tU8 const* const cpuBuffer) const;

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetVideoBlocking()
   ***************************************************************************/
   /*!
   * \brief   bSetVideoBlocking- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bSetVideoBlocking(tU8 const* const cpuBuffer) const;

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetDisplayAttr()
   ***************************************************************************/
   /*!
   * \brief   bSetDisplayAttr- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bSetDisplayAttr(tU8 const* const cpu8Buffer) const;

  /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSendKeyEvent()
   ***************************************************************************/
   /*!
   * \brief   bSendKeyEvent - Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bSendKeyEvent(tU8 const* const cpu8Buffer) const;

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetTechnologyPreference(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief   Sets technology preference - Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bSetTechnologyPreference(tU8 const* const cpu8Buffer) const;

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bGetTechnologyPreference(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief   Gets technology preference - Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bGetTechnologyPreference(tU8 const* const cpu8Buffer) const;

   /***************************************************************************
   ** FUNCTION:  spi_tclImpTraceStreamable::spi_tclImpTraceStreamable()
   ***************************************************************************/
   /*!
   * \brief   Default Constructor
   * \param   NONE
   * \retval  NONE
   * \note    Default constructor is declared protected to disable it. So
   *          that any attempt to create without parameter will be caught by
   *          the compiler.
   **************************************************************************/
   spi_tclImpTraceStreamable(); // No definition exists.
    /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetDeviceUsagePreference(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief  bSetDeviceUsagePreference- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed.
   **************************************************************************/
   tBool bSetDeviceUsagePreference(tU8 const* const cpu8Buffer) const;
   
   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSendTouchEvent(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief  bSendTouchEvent - Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bSendTouchEvent(tU8 const* const cpu8Buffer) const;
   
   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSendTouchEvent(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief   bGetDeviceUsagePreference- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bGetDeviceUsagePreference(tU8 const* const cpu8Buffer) const;

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bInvokeBluetoothDeviceAction(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief   bInvokeBluetoothDeviceAction- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bInvokeBluetoothDeviceAction(tU8 const* const cpu8Buffer) const;
   
    /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bDiPORoleSwitchRequired(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief   bDiPORoleSwitchRequired- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bDiPORoleSwitchRequired(tU8 const* const cpu8Buffer) const;
    
    /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bRotaryControllerEvent(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief   bRotaryControllerEvent- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bRotaryControllerEvent(tU8 const* const cpu8Buffer) const;
   
     /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetVehicleMovementState(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief   bSetVehicleMovementState- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bSetVehicleMovementState(tU8 const* const cpu8Buffer) const;
   
     /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetFeatureRestrictions(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief   bSetFeatureRestrictions- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bSetFeatureRestrictions(tU8 const* const cpu8Buffer) const;
   
    /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetEnvironmentData(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief   bSetEnvironmentData- Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if the command was successfully executed
   **************************************************************************/
   tBool bSetEnvironmentData(tU8 const* const cpu8Buffer) const;

   /***************************************************************************
  ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetDeviceSelectionMode(tU8 const* const cp..
  ***************************************************************************/
  /*!
  * \brief   bSetDeviceSelectionMode- Trace command executor
  *          The function converts the trace commands to Loopback message,
  *          which are posted back to the component to be processed using the
  *          normal CCA interface.
  * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
  * \retval  [tBool]: TRUE if the command was successfully executed
  **************************************************************************/
  tBool bSetDeviceSelectionMode(tU8 const* const cpu8Buffer) const;


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ****************************END OF PRIVATE**********************************
   ***************************************************************************/
}; // class spi_tclImpTraceStreamable : public spi_tclImpTraceStreamable

#endif   // #ifndef spi_tclImpTraceStreamable_H_
