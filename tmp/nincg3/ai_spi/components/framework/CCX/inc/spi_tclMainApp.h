/*!
*******************************************************************************
* \file               spi_tclMainApp.h
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    CCA Application Spi_tclMainApp
*                created with CCA skeleton generator.
COPYRIGHT:      &copy; RBEI
HISTORY:
Date       |  Author                   | Modifications
16.10.2013 |                           | Initial Version
06.04.2014 | Ramya Murthy              | Initialisation sequence implementation
12.03.2015 | Ramkumar Muniraj          | Added BTSTL feature switch so that it
									     can also be used in non BTSTL platform.
										 Refer ticket CMG3GB-1715.

\endverbatim
*******************************************************************************/

#ifndef _SPI_TCLMAINAPP_H_
#define _SPI_TCLMAINAPP_H_

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"
#include<signal.h>


/******************************************************************************
| defines
|----------------------------------------------------------------------------*/
#define RELEASE_MEM_OSAL(VAR)      \
   if (OSAL_NULL != VAR)           \
   {                               \
      OSAL_DELETE VAR;             \
      VAR = OSAL_NULL;             \
   }

/******************************************************************************/
 /*!
 * function declarations
 */
#ifdef SYSTEM_S_USE_BPSTL
extern tBool exh_bInitExceptionHandling();
#endif
/*!
* \class definition
*/
class ahl_tclBaseOneThreadApp;
class spi_tclService;
class spi_tclTrace;
class spi_tclDefsetHandler;

class spi_tclMainApp : public ahl_tclBaseOneThreadApp
{
public:

  /********************************************************************************
   ** FUNCTION   : spi_tclMainApp::spi_tclMainApp()
   /*******************************************************************************
   * \fn     spi_tclMainApp::spi_tclMainApp()
   * \brief  Constructor.
   *         Component object is created as Singleton in method theServer().
   * \param   None.
   * \retval  None.
   *******************************************************************************/
   spi_tclMainApp(tVoid);

   /********************************************************************************
   ** FUNCTION   : spi_tclMainApp::~spi_tclMainApp()
   /*******************************************************************************
   * \fn      spi_tclMainApp::~spi_tclMainApp()
   * \brief    Destructor.
               Invalidate static self reference.
   * \param    None.
   * \retval   None.
   *******************************************************************************/
   virtual ~spi_tclMainApp(tVoid);

   /********************************************************************************
   ** FUNCTION   :tBool spi_tclMainApp::bOnInit() 
   /**********************************************************************************
   * \fn       tBool spi_tclMainApp::bOnInit()
   * \brief    This function is called by the CCA framework before starting any
   *           CCA message related communication action. As default this
   *           function always returns TRUE. The user has to implement all his
   *           local application initialization (create client and service
   *           handler, create worker threads, allocate application memory,...).
   *           If returns TRUE, initialization was successfully performed
   *           and framework will register application, else, a reset of the
   *           system is forced immediately.
   *           This function is the counterpart of vOnApplicationClose().
   * \param    None.
   * \retval   TRUE  = Application successfully initialized.
   *           FALSE = Application NOT successfully initialized.
   ********************************************************************************
   * Overrides method ahl_tclBaseOneThreadApp::bOnInit().
   *******************************************************************************/
   virtual tBool bOnInit(tVoid);

   /********************************************************************************
   ** FUNCTION   :tVoid spi_tclMainApp::vOnApplicationClose()
   /*******************************************************************************
   * \fn       tVoid spi_tclMainApp::vOnApplicationClose()
   /*!
   * \brief    This function is called by the CCA framework to indicate the
   *           imminent shutdown of the application. The user has to implement
   *           the de-initialization of all his local application data (destroy
   *           client and service handler, destroy worker threads, free
   *           application memory, ... ). After this function call the
   *           destructor of this application will be called.
   *           This function is the counterpart of bOnInit().
   * \param    None.
   * \retval   None.
   ********************************************************************************
   * Overrides method ahl_tclBaseOneThreadApp::vOnApplicationClose().
   *******************************************************************************/
   virtual tVoid vOnApplicationClose(tVoid);

   /********************************************************************************
   ** FUNCTION   :tVoid spi_tclMainApp::vOnLoadSettings()
   /*******************************************************************************
   * \fn        tVoid spi_tclMainApp::vOnLoadSettings()
   * \brief     This function is called by the CCA framework to trigger the
                loading of last mode settings. The user has to load the
                previously stored last mode settings from persistent memory.
   * \param     None.
   * \retval    None.
   ********************************************************************************
   * Overrides method ahl_tclBaseOneThreadApp::vOnLoadSettings().
   *******************************************************************************/
   virtual tVoid vOnLoadSettings(tVoid);

   /********************************************************************************
   ** FUNCTION   : tVoid spi_tclMainApp::   vOnSaveSettings()
   /*******************************************************************************
   * \fn        tVoid spi_tclMainApp::   vOnSaveSettings()
   * \brief     This function is called by the CCA framework to trigger the
   *            storage of last mode settings. The user has to store the last
   *            mode settings to persistent memory.
   * \param     None.
   * \retval    None.
   ********************************************************************************
   * Overrides method ahl_tclBaseOneThreadApp::vOnSaveSettings().
   *******************************************************************************/
   virtual tVoid vOnSaveSettings(tVoid);

   /********************************************************************************
   ** FUNCTION   : tVoid spi_tclMainApp::vOnTimer(tU16 u16TimerId)
    /*******************************************************************************
   * \fn        tVoid spi_tclMainApp::vOnTimer(tU16 u16TimerId)
   * \brief     This function is called by the CCA framework on the expiration
   *            of a previously started timer via function bStartTimer(). The
   *            expired timer is forwarded to the respective service or
   *            client handlers via a call of vProcessTimer().
   *           The function is called from this applications context and
   *             therefore no interthread programming rules must be considered
   *           and the application methods and/or member variables can be
   *             accessed without using the static self reference
   *            'm_poMainAppInstance'.
   * \param     [IN] u16TimerId = Identifier of the expired timer.
   * \retval     None.
   ********************************************************************************
   * Overrides method ahl_tclBaseOneThreadApp::vOnTimer().
   *******************************************************************************/
   virtual tVoid vOnTimer(tU16 nTimerId);

   /**************************************************************************************************
   ** FUNCTION   : tVoid spi_tclMainApp::vOnLoopback(tU16 u16ServiceID,amt_tclServiceData* poMessage)
   /***************************************************************************************************
   * \fn        tVoid spi_tclMainApp::vOnLoopback(tU16 u16ServiceID,amt_tclServiceData* poMessage)
   * \brief     This function is called by the CCA framework if a message was
   *            sent from one if these applications services or from a
   *            callback-handler of this application (so called self or loopback
   *           messages). The message can be forwarded to the respective service
   *             via a call of vOnLoopbackService(). If there is no service
   *            registered for this message then the user has the option to
   *            directly evaluate the message in the default block of this
   *            methods switch-clause.
   * \param     [IN] u16ServiceID = Identifier of the service from where the
   *            message was sent.
   *            [IN] poMessage = Message object.
   * \retval    None.
   ********************************************************************************
   * Overrides method ahl_tclBaseOneThreadApp::vOnLoopback().
   *******************************************************************************/

   using ahl_tclBaseOneThreadApp::vOnLoopback;
   tVoid vOnLoopback(tU16 u16ServiceID, amt_tclBaseMessage* poMessage){};

   /***************************************************************************
   ** FUNCTION   : t_Void spi_tclMainApp::vSignalHandler()
   ***************************************************************************/
   /* !
   * \fn       t_Void vSignalHandler(t_S32 s32SigNum, siginfo_t *pSiginfo, t_Void *pContext)
   * \brief    Method to Handle the signals sent to SPI Process/ threads created by our process
   * \param    s32SigNum : [IN] Signal ID/Number
   * \param    pSiginfo  : [IN] Siginfo structure, which provides the details of the signal
   * \param    pContext  : [IN] context ptr
   * \retval   None.
   **************************************************************************/
   static t_Void vSignalHandler(t_S32 s32SigNum, siginfo_t *pSiginfo, t_Void *pContext);

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclMainApp::vOnEvent(OSAL_tEventMask nEvent)
   **************************************************************************/
   /* !
   * \fn        vOnEvent(OSAL_tEventMask nEvent)
   * \brief     This method is called by the CCA framework when an event is
   *            signaled on Main application.
   * \param     [IN] nEvent = Identifier of the signaled event
   * \retval    None.
   ***************************************************************************
   * Overrides method ahl_tclBaseOneThreadApp::vOnEvent().
   **************************************************************************/
   virtual tVoid vOnEvent(OSAL_tEventMask nEvent);



protected:
   
    /***************************************************************************
   ** FUNCTION:  tVoid spi_tclMainApp::vHandleTraceCmd(const tPU8 cpu8Buffer)
   ***************************************************************************/
   /*!
    * \brief   Trace Command interface handler.
    * \param   [cpu8Buffer] (->I) Pointer to the Trace command buffer
    * \retval  NONE
    **************************************************************************/
   static tVoid vHandleTraceCmd(tU8 const* const cpu8Buffer);

private:

   /**************************************************************************
   * Assignment Operator, will not be implemented.
   * Avoids Lint Prio 3 warning: Info 1732: new in constructor for class
   * 'spi_tclMainApp' which has no assignment operator.
   * NOTE:
   * This is a technique to disable the assignment operator for this class.
   * So if an attempt for the assignment is made linker complains.
   **************************************************************************/
   spi_tclMainApp& operator= (const spi_tclMainApp &oSpi_tclMainApp);

   /**************************************************************************
   //!Copy Constructor, will not be implemented.
   //!Avoids Lint Prio 3 warning:Info 1733: new in constructor for class
   //!'spi_tclMainApp' which has no copy constructor.
   //!NOTE:
   //! This is a technique to disable the copy constructor for this class.
   //!So if an attempt for the copy constructor is made linker complains.
   **************************************************************************/
   spi_tclMainApp(const spi_tclMainApp &oSpi_tclMainApp);

   /**************************************************************************
   * TTFIS callback register mechanism.
   **************************************************************************/

   /*!
   * Pointer to Trace Command handler.
   */
   //TraceStreamable* m_poTraceStreamer;

   /*!
   * spi_tclService member variable
   */
   spi_tclService* m_poService;

   /*!
   * Static member variable of the class, to use in static member functions
   */
   static spi_tclTrace* m_poCmdHndlr;

   /**************************************************************************
   ** DefSet handler
   **************************************************************************/
   spi_tclDefsetHandler* m_poDefSetHandler;

}; 

#endif /* SPI_TCLMAINAPP_H_*/




