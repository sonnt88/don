/**********************************************************************************************************************
 * \file           SpiDbusTestClient.cpp
 * \brief          Hand-crafted code for DBus test client for CCA DBUS Adapter component
 **********************************************************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    This component implementation provides a DBus test client to test the CCA DBus adapter designed for
                 Smart Phone Integration component
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date            |  Author                                | Modifications
 6.12.2013       |  Pruthvi Thej Nagaraju                 | Initial Version

 \endverbatim
 **********************************************************************************************************************/

/**********************************************************************************************************************
 | includes:
 **********************************************************************************************************************/
#include "SpiDbusTestClient.h"

//! Includes for Trace files
#define SPI_DISABLE_TTFIS_TRACE
#include "Trace.h"

//! Static variables
static const unsigned int cou32TimeIntervalms = 100 ;
static const unsigned int cou32TimerRepeatCount = 20;

/***********************************************************************************************************************
 |namespace usage
 **********************************************************************************************************************/
using namespace dbus_client;

/**********************************************************************************************************************
 ** FUNCTION:  SpiDbusTestClient::SpiDbusTestClient();
 **********************************************************************************************************************/
SpiDbusTestClient::SpiDbusTestClient() :
            //! Create proxy for the DBus service of the CCA-DBus Adapter
            m_pSpiDbusProxy(ApiProxy::createProxy("spiDbusPort", *this)),
            //! Timer to send test commands
            m_oTimer(*this, cou32TimeIntervalms, cou32TimerRepeatCount)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/**********************************************************************************************************************
 ** FUNCTION:  SpiDbusTestClient::~SpiDbusTestClient();
 **********************************************************************************************************************/
SpiDbusTestClient::~SpiDbusTestClient()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

//!  ServiceAvailableIF implementation
/***********************************************************************************************************************
 ** FUNCTION:  virtual void SpiDbusTestClient::onAvailable
 **********************************************************************************************************************/
void SpiDbusTestClient::onAvailable(const ::boost::shared_ptr<asf::core::Proxy>& proxy,
         const ServiceStateChange &stateChange)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (m_pSpiDbusProxy == proxy)
   {
      ETG_TRACE_USR1(("SpiDbusTestClient is connected \n"));
      ETG_TRACE_USR2(("SpiDbusTestClient::Registering for DeviceStatusInfo \n"));

      //! Register for property updates
      m_pSpiDbusProxy->sendDeviceStatusInfoRegister(*this);

      //!TODO Start the timer and call method start on expiry for testing. use m_oTimer.start();

      //! Send method start for testing the adapters
      ETG_TRACE_USR2(("SpiDbusTestClient::Sending Method start for DeviceBTAddress  \n"));
      m_pSpiDbusProxy->sendGetDeviceBTAddressRequest(*this, 0);
      ETG_TRACE_USR2(("SpiDbusTestClient::Sending Method start for LaunchApp  \n"));
      m_pSpiDbusProxy->sendLaunchAppRequest(*this, 0, 0);
   } //if (m_pSpiDbusProxy == proxy)
}

/**********************************************************************************************************************
 ** FUNCTION:  virtual void SpiDbusTestClient::onUnavailable
 **********************************************************************************************************************/
void SpiDbusTestClient::onUnavailable(const ::boost::shared_ptr<asf::core::Proxy>& proxy,
         const ServiceStateChange &stateChange)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

//! TimerCallbackIF implementation
/**********************************************************************************************************************
 ** FUNCTION:  virtual void SpiDbusTestClient::onExpired
 **********************************************************************************************************************/
void SpiDbusTestClient::onExpired(Timer& timer, boost::shared_ptr<TimerPayload> data)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/**********************************************************************************************************************
 ***************************************** PROPERTIES/SIGNALS *********************************************************
 **********************************************************************************************************************/

/**********************************DeviceStatusInfo********************************************************************/
/**********************************************************************************************************************
 ** FUNCTION:  virtual void SpiDbusTestClient::onDeviceStatusInfoError
 **********************************************************************************************************************/
void SpiDbusTestClient::onDeviceStatusInfoError(const ::boost::shared_ptr<ApiProxy>& proxy,
         const ::boost::shared_ptr<DeviceStatusInfoError>& error)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   ETG_TRACE_USR4((" onDeviceStatusInfoError Name = %s \n", error->getName().c_str()));
   ETG_TRACE_USR4((" onDeviceStatusInfoError Message = %s \n", error->getMessage().c_str()));
}


/**********************************************************************************************************************
 ** FUNCTION:  virtual void SpiDbusTestClient::onDeviceStatusInfoSignal
 **********************************************************************************************************************/
void SpiDbusTestClient::onDeviceStatusInfoSignal(const ::boost::shared_ptr<ApiProxy>& proxy,
         const ::boost::shared_ptr<DeviceStatusInfoSignal>& signal)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   ETG_TRACE_USR4((" DeviceStatusInfo = %d \n", signal->getDeviceStatusInfo()));
}

/**********************************************************************************************************************
 ***************************************** METHODS ********************************************************************
 **********************************************************************************************************************/
/**********************************GetDeviceBTAddress******************************************************************/
//!  method error 'GetDeviceBTAddress'
/**********************************************************************************************************************
 ** FUNCTION:  virtual void SpiDbusTestClient::onGetDeviceBTAddressError
 **********************************************************************************************************************/
void SpiDbusTestClient::onGetDeviceBTAddressError(const ::boost::shared_ptr<ApiProxy>& proxy,
         const ::boost::shared_ptr<GetDeviceBTAddressError>& error)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   ETG_TRACE_USR4((" onGetDeviceBTAddressError Name = %s \n", error->getName().c_str()));
   ETG_TRACE_USR4((" onGetDeviceBTAddressError Message = %s \n", error->getMessage().c_str()));
}

//!  method response 'GetDeviceBTAddress'
/**********************************************************************************************************************
 ** FUNCTION:  virtual void SpiDbusTestClient::onGetDeviceBTAddressResponse
 **********************************************************************************************************************/
void SpiDbusTestClient::onGetDeviceBTAddressResponse(const ::boost::shared_ptr<ApiProxy>& proxy,
         const ::boost::shared_ptr<GetDeviceBTAddressResponse>& response)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   ETG_TRACE_USR4((" DeviceBTAddress Response code = %s \n", response->getSBluetoothDeviceAddress().c_str()));
}

/**********************************LaunchApp***************************************************************************/
//!  method error 'LaunchApp'
/**********************************************************************************************************************
 ** FUNCTION:  virtual void SpiDbusTestClient::onLaunchAppError
 **********************************************************************************************************************/
void SpiDbusTestClient::onLaunchAppError(const ::boost::shared_ptr<ApiProxy>& proxy, const ::boost::shared_ptr<
         LaunchAppError>& error)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   ETG_TRACE_USR4((" onLaunchAppError Name = %s \n", error->getName().c_str()));
   ETG_TRACE_USR4((" onLaunchAppError Message = %s \n", error->getMessage().c_str()));
}

//!  method response 'LaunchApp'
/**********************************************************************************************************************
 ** FUNCTION:  virtual void SpiDbusTestClient::onLaunchAppResponse
 **********************************************************************************************************************/
void SpiDbusTestClient::onLaunchAppResponse(const ::boost::shared_ptr<ApiProxy>& proxy, const ::boost::shared_ptr<
         LaunchAppResponse>& response)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   ETG_TRACE_USR4((" LaunchApp Response code = %d \n", response->getResponseCode()));
   ETG_TRACE_USR4((" LaunchApp Error code = %d \n", response->getErrorType()));
}
