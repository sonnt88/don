/**********************************************************************************************************************
 * \file           SpiDbusTestClient.h
 * \brief          Hand-crafted code for DBus test client for CCA DBUS Adapter component
 **********************************************************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    This component implementation provides a DBus test client to test the CCA DBus adapter designed for
                 Smart Phone Integration component
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date            |  Author                                | Modifications
 6.12.2013       |  Pruthvi Thej Nagaraju                 | Initial Version

 \endverbatim
 **********************************************************************************************************************/

#ifndef SPIDBUSTESTCLIENT_H_
#define SPIDBUSTESTCLIENT_H_

/**********************************************************************************************************************
 | includes:
 **********************************************************************************************************************/
//! ASF core includes
#include "asf/core/ApplicationIF.h"
#include "asf/core/BaseComponent.h"
#include "asf/core/Logger.h"
#include "asf/core/Timer.h"

//! DBus API proxy include
#include "org/bosch/cm/psa/apiProxy.h"

namespace dbus_client
{
   using namespace ::asf::core;

   //! DBus API namespace in ASF generated code
   using namespace ::org::bosch::cm::psa::api;

   /*!
    * \class SpiDbusTestClient
    * \brief DBus Test client to implement DBus test client for
    *        SPI DBus API interface of CCA-DBus Adapter
    */

   class SpiDbusTestClient:
            //! Base of ASF Component
            public BaseComponent,
            //! Callback to indicate the availability of DBus server
            public ServiceAvailableIF,
            //! Timer callback: to be used for testing
            public TimerCallbackIF,
            //! Callbacks for Properties and Methods
            public DeviceStatusInfoCallbackIF,
            public GetDeviceBTAddressCallbackIF,
            public LaunchAppCallbackIF
            //! TODO: Extend for other methods and properties when supported
   {
      public:

         /*************************************************************************************************************
          ** FUNCTION:  SpiDbusTestClient::SpiDbusTestClient();
          *************************************************************************************************************/
         /*!
          * \fn     SpiDbusTestClient()
          * \brief  constructor
          * \sa     ~SpiDbusTestClient()
          *************************************************************************************************************/
         SpiDbusTestClient();

         /*************************************************************************************************************
          ** FUNCTION:  SpiDbusTestClient::~SpiDbusTestClient();
          *************************************************************************************************************/
         /*!
          * \fn     ~SpiDbusTestClient()
          * \brief  virtual destructor
          * \sa     SpiDbusTestClient()
          *************************************************************************************************************/
         virtual ~SpiDbusTestClient();

         //!  ServiceAvailableIF implementation
         /**************************************************************************************************************
          ** FUNCTION:  virtual void SpiDbusTestClient::onAvailable
          *************************************************************************************************************/
         /*!
          * \fn     virtual void onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
          *                          const ServiceStateChange &stateChange);
          * \brief  Called when the DBus service of the CCA-DBus adapter is available for SPI
          * \param  proxy : The proxy instance for which the service is available
          * \param  stateChange: Contains previous and current state of the service indicating states Disconnected(0),
          *                      Connected(1), Suspended(2)
          * \sa          onUnavailable
          *************************************************************************************************************/
         void onAvailable(const ::boost::shared_ptr<asf::core::Proxy>& proxy, const ServiceStateChange &stateChange);

         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiDbusTestClient::onUnavailable
          *************************************************************************************************************/
         /*!
          * \fn     virtual void onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
          *                const ServiceStateChange &stateChange);
          * \brief  Called when DBus service of the CCA-DBus adapter becomes unavailable for SPI
          * \param  proxy : The proxy instance for which the service is unavailable
          * \param  stateChange: Contains previous and current state of the service indicating states Disconnected(0),
          *                      Connected(1), Suspended(2)
          * \sa     OnAvailable
          *************************************************************************************************************/
         void onUnavailable(const ::boost::shared_ptr<asf::core::Proxy>& proxy, const ServiceStateChange &stateChange);

         //! TimerCallbackIF implementation
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiDbusTestClient::onExpired
          *************************************************************************************************************/
         /*!
          * \fn     virtual void onExpired(Timer& timer, boost::shared_ptr<TimerPayload> data)
          * \brief  Called when the Timer expires
          * \param  timer Reference to the timer, which is responsible for the invocation.
          * \param  data Informations about the timer callback (repeat count, reason)
          *************************************************************************************************************/
         virtual void onExpired(Timer& timer, boost::shared_ptr<TimerPayload> data);

         /*************************************************************************************************************
          ***************************************** PROPERTIES/SIGNALS ************************************************
          *************************************************************************************************************/

         /**********************************DeviceStatusInfo***********************************************************/
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiDbusTestClient::onDeviceStatusInfoError
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onDeviceStatusInfoError(const ::boost::shared_ptr<ApiProxy>& proxy,
          *          const ::boost::shared_ptr<DeviceStatusInfoError>& error)
          * \brief   Called when the Error is received for this property/signal
          * \param   proxy : The proxy instance from which the property/signal update is received
          * \param   error:  The error received for the property/signal update
          * \sa      onDeviceStatusInfoSignal
          *************************************************************************************************************/
         virtual void onDeviceStatusInfoError(const ::boost::shared_ptr<ApiProxy>& proxy, const ::boost::shared_ptr<
                  DeviceStatusInfoError>& error);

         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiDbusTestClient::onDeviceStatusInfoSignal
          *************************************************************************************************************/
         /*!
          * \fn     virtual void onDeviceStatusInfoSignal(const ::boost::shared_ptr<ApiProxy>& proxy,
          *         const ::boost::shared_ptr<DeviceStatusInfoSignal>& signal
          * \brief   Called when the status update is received for this property/signal
          * \param   proxy : The proxy instance from which the property/signal update is received
          * \param   status:  The status info for this property/signal update
          * \sa      onDeviceStatusInfoError
          *************************************************************************************************************/
         virtual void onDeviceStatusInfoSignal(const ::boost::shared_ptr<ApiProxy>& proxy, const ::boost::shared_ptr<
                  DeviceStatusInfoSignal>& signal);

         /*************************************************************************************************************
          ***************************************** METHODS ***********************************************************
          *************************************************************************************************************/
         /**********************************GetDeviceBTAddress*********************************************************/
         //!  method error 'GetDeviceBTAddress'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiDbusTestClient::onGetDeviceBTAddressError
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onGetDeviceBTAddressError(const ::boost::shared_ptr<ApiProxy>& proxy,
          *          const ::boost::shared_ptr<GetDeviceBTAddressError>& error)
          * \brief   Called by ASF when Error is received for this methods from DBus service. Do not invoke this method
          *          on your own.
          * \param   proxy : the instance of the DBus proxy that invokes this method error
          * \param   error : Error code on failure of Method start
          *************************************************************************************************************/
         virtual void onGetDeviceBTAddressError(const ::boost::shared_ptr<ApiProxy>& proxy,
                  const ::boost::shared_ptr<GetDeviceBTAddressError>& error);

         //!  method response 'GetDeviceBTAddress'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiDbusTestClient::onGetDeviceBTAddressResponse
          *************************************************************************************************************/
         /*!
          * \fn      virtual void oonGetDeviceBTAddressResponse(const ::boost::shared_ptr<ApiProxy>& proxy,
          *          const ::boost::shared_ptr<GetDeviceBTAddressResponse>& response)
          * \brief   Called by ASF when response is received for this methods from from DBus service. Do not invoke
          *          this method on your own.
          * \param   proxy : the instance of the DBus proxy that invokes this method response
          * \param   response : response on invocation of Method start
          *************************************************************************************************************/
         virtual void onGetDeviceBTAddressResponse(const ::boost::shared_ptr<ApiProxy>& proxy,
                  const ::boost::shared_ptr<GetDeviceBTAddressResponse>& response);

         /**********************************LaunchApp******************************************************************/
         //!  method error 'LaunchApp'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiDbusTestClient::onLaunchAppError
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onLaunchAppError(const ::boost::shared_ptr<ApiProxy>& proxy,
          *          const ::boost::shared_ptr<LaunchAppError>& error)
          * \brief   Called by ASF when Error is received for this methods from DBus Service. Do not invoke this method
          *          on your own.us
          * \param   proxy : the instance of the DBus proxy that invokes this method error
          * \param   error : Error code on failure of Method start
          *************************************************************************************************************/
         virtual void onLaunchAppError(const ::boost::shared_ptr<ApiProxy>& proxy, const ::boost::shared_ptr<
                  LaunchAppError>& error);

         //!  method response 'LaunchApp'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiDbusTestClient::onLaunchAppResponse
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onLaunchAppResponse(const ::boost::shared_ptr<ApiProxy>& proxy,
          *          const ::boost::shared_ptr<LaunchAppResponse>& response)
          * \brief   Called by ASF when response is received for this methods from DBus Service. Do not invoke
          *          this method on your own.
          * \param   proxy : the instance of the DBus proxy that invokes this method response
          * \param   response : response on invocation of Method start
          *************************************************************************************************************/
         virtual void onLaunchAppResponse(const ::boost::shared_ptr<ApiProxy>& proxy, const ::boost::shared_ptr<
                  LaunchAppResponse>& response);

      private:

         //! Instance of DBus Proxy. used to call DBus APIs from Test client
         ::boost::shared_ptr<ApiProxy> m_pSpiDbusProxy;

         //! Timer to send test requests to CCA DBus adapter
         Timer m_oTimer;

         //! To get debug logs
         DECLARE_CLASS_LOGGER   ();
   };
}

#endif /* SPIDBUSTESTCLIENT_H_ */
