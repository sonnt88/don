/**********************************************************************************************************************
 * \file           SpiCcaDbusComp.cpp
 * \brief          Hand-crafted code for CCA DBUS Adapter component implementation
 **********************************************************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    This component implementation provides glue code to map CCA messages from smartphoneint
                 CCA service to DBus service messages. The technology differences has taken care while mapping
                 messages from CCA to DBUS (like property and methods in CCA)
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date            |  Author                                | Modifications
 2.12.2013       |  Pruthvi Thej Nagaraju                 | Initial Version

 \endverbatim
 **********************************************************************************************************************/

/**********************************************************************************************************************
 | includes:
 **********************************************************************************************************************/
#include "midw_smartphoneint_fi.h"
#include "SpiCcaDbusComp.h"

//! Includes for Trace files
#define SPI_DISABLE_TTFIS_TRACE
#include "Trace.h"

/**********************************************************************************************************************
 |namespace usage
 **********************************************************************************************************************/
using namespace ::midw_smartphoneint_fi_types;
using namespace ::midw_smartphoneint_fi;
using namespace ::ccx_dbus;

/**********************************************************************************************************************
 ** FUNCTION:  SpiCcaDbusComp::SpiCcaDbusComp();
 **********************************************************************************************************************/
SpiCcaDbusComp::SpiCcaDbusComp() :
            //! Port Name is passed to the Stub implementation for DBus service (Port name defined in cmc file)
            ApiStub("spiDbusStubPort"),
            //! Create proxy for the used CCA service (port name as defined in .cmc file)
            m_pSpiFi(Midw_smartphoneint_fiProxy::createProxy("SpiCcaServerPort", *this))
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/**********************************************************************************************************************
 ** FUNCTION:  SpiCcaDbusComp::~SpiCcaDbusComp();
 **********************************************************************************************************************/
SpiCcaDbusComp::~SpiCcaDbusComp()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onAvailable
 **********************************************************************************************************************/
void SpiCcaDbusComp::onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
         const ServiceStateChange &stateChange)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (m_pSpiFi == proxy)
   {
      //! register for properties
      m_pSpiFi->sendDeviceStatusInfoUpReg(*this);
      //! TODO register for all properties
   } // if (m_pSpiFi == proxy)

}

/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onUnavailable
 **********************************************************************************************************************/
void SpiCcaDbusComp::onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
         const ServiceStateChange &stateChange)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   //! This function releases all property notifications (on all properties)
   if (m_pSpiFi == proxy)
   {
      //! Unregister for properties
      m_pSpiFi->sendRelUpRegAll();
   }// if (m_pSpiFi == proxy)
}

/*******************************Implementation of Overridden methods of CallBackIF classes*****************************/

/**********************************************************************************************************************/

/**********************************************************************************************************************
 ***************************************** PROPERTIES ********************************************************
 *************************************************************************************************************/

/*******************************************DeviceStatusInfo***********************************************************/
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onDeviceStatusInfoError
 **********************************************************************************************************************/
void SpiCcaDbusComp::onDeviceStatusInfoError(const ::boost::shared_ptr<Midw_smartphoneint_fiProxy>& proxy,
         const boost::shared_ptr<midw_smartphoneint_fi::DeviceStatusInfoError>& error)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   ETG_TRACE_USR4((" DeviceStatusInfoError CCA Error code = %d", error->getCcaErrorCode()));
}

/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onDeviceStatusInfoStatus
 **********************************************************************************************************************/
void SpiCcaDbusComp::onDeviceStatusInfoStatus(const ::boost::shared_ptr<Midw_smartphoneint_fiProxy>& proxy,
         const boost::shared_ptr<midw_smartphoneint_fi::DeviceStatusInfoStatus>& status)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   //! Send the property status update
   sendDeviceStatusInfoSignal(status->getDeviceStatusInfo());
}
/**********************************************************************************************************************/

/**********************************************************************************************************************
 ***************************************** METHODS ***********************************************************
 **********************************************************************************************************************/

/*******************************************GetDeviceBTAddress*********************************************************/
//!  method start 'GetDeviceBTAddress'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onGetDeviceBTAddressRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onGetDeviceBTAddressRequest(const ::boost::shared_ptr<GetDeviceBTAddressRequest>& request)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   //! Send method start to CCA
   act_t oCcaAct = m_pSpiFi-> sendGetDeviceBTAddressStart(*this, request->getDeviceHandle());

   //! Store the Asynchronous Completion Token
   m_oActMap.insert(std::pair<act_t, act_t>(oCcaAct, request->getAct()));
}

//!  method error 'GetDeviceBTAddress'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onGetDeviceBTAddressError
 **********************************************************************************************************************/
void SpiCcaDbusComp::onGetDeviceBTAddressError(const ::boost::shared_ptr<Midw_smartphoneint_fiProxy>& proxy,
         const boost::shared_ptr<midw_smartphoneint_fi::GetDeviceBTAddressError>& error)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   //! Retrieve the Asynchronous Completion Token
   ActMap::iterator actPtr = m_oActMap.find(error->getAct());
   if (actPtr != m_oActMap.end())
   {
      ETG_TRACE_USR2(("key is %u and value is %u ", actPtr->first, actPtr->second));
      ETG_TRACE_USR4((" onGetDeviceBTAddressError CCA Errorcode = %d, error->getCcaErrorCode()"));
      //! TODO covert CCA errors to DBus errors using utility

      //! Send the appropriate DBus Error message
      sendGetDeviceBTAddressError("org.freedesktop.DBus.Error.NoReply", "Not implemented");
   }
   else
   {
      ETG_TRACE_ERR(("ACT received does not matches any key \n"));
   } // if (actPtr != m_oActMap.end())
}

//!  method result 'GetDeviceBTAddress'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onGetDeviceBTAddressResult
 **********************************************************************************************************************/
void SpiCcaDbusComp::onGetDeviceBTAddressResult(const ::boost::shared_ptr<Midw_smartphoneint_fiProxy>& proxy,
         const boost::shared_ptr<midw_smartphoneint_fi::GetDeviceBTAddressResult>& result)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   //! Retrieve the Asynchronous Completion Token
   ActMap::iterator actPtr = m_oActMap.find(result->getAct());
   if (actPtr != m_oActMap.end())
   {
      ETG_TRACE_USR2(("key is %u and value is %u ", actPtr->first, actPtr->second));

      //Send the method result to DBus stub
      sendGetDeviceBTAddressResponse(result->getBluetoothDeviceAddress(), result->getErrorType(), actPtr->second);
   }
   else
   {
      ETG_TRACE_ERR(("ACT received does not matches any key \n"));
   } // if (actPtr != m_oActMap.end())
   //todo : check if the signal has to be raised
}

/*******************************************LaunchApp******************************************************************/
//!  method start 'LaunchApp'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onLaunchAppRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onLaunchAppRequest(const ::boost::shared_ptr<LaunchAppRequest>& request)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   //! Send method start to CCA
   act_t oCcaAct = m_pSpiFi->sendLaunchAppStart(*this, request->getDeviceHandle(), request->getAppHandle());

   //! Store the Asynchronous Completion Token
   m_oActMap.insert(std::pair<act_t, act_t>(oCcaAct, request->getAct()));
}

/*************************************************************************************************************
 ** FUNCTION:  virtual void SpiCcaDbusComp::onLaunchAppError
 *************************************************************************************************************/
void SpiCcaDbusComp::onLaunchAppError(const ::boost::shared_ptr<Midw_smartphoneint_fiProxy>& proxy,
         const boost::shared_ptr<midw_smartphoneint_fi::LaunchAppError>& error)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   //! Retrieve the Asynchronous Completion Token
   ActMap::iterator actPtr = m_oActMap.find(error->getAct());
   if (actPtr != m_oActMap.end())
   {
      ETG_TRACE_USR2(("key is %u and value is %u ", actPtr->first, actPtr->second));
      ETG_TRACE_USR4((" onLaunchAppError CCA Errorcode = %d, error->getCcaErrorCode()"));
      //! TODO covert CCA errors to DBus errors using utility

      //! Send the appropriate DBus Error message
      sendLaunchAppError("org.freedesktop.DBus.Error.NoReply", "Not implemented");
   }
   else
   {
      ETG_TRACE_ERR(("ACT received does not matches any key \n"));
   } // if (actPtr != m_oActMap.end())

}

/*************************************************************************************************************
 ** FUNCTION:  virtual void SpiCcaDbusComp::onLaunchAppResult
 *************************************************************************************************************/
void SpiCcaDbusComp::onLaunchAppResult(const ::boost::shared_ptr<Midw_smartphoneint_fiProxy>& proxy,
         const boost::shared_ptr<LaunchAppResult>& result)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   //! Retrieve the Asynchronous Completion Token
   ActMap::iterator actPtr = m_oActMap.find(result->getAct());
   if (actPtr != m_oActMap.end())
   {
      ETG_TRACE_USR2(("key is %u and value is %u ", actPtr->first, actPtr->second));

      //! Send the method result to DBus stub
      sendLaunchAppResponse(result->getResponseCode(), result->getErrorType(), actPtr->second);
   }
   else
   {
      ETG_TRACE_ERR(("ACT received does not matches any key \n"));
   } // if (actPtr != m_oActMap.end())
}

/*******************************************GetAppIconData*************************************************************/
//!  method start 'GetAppIconData'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onGetAppIconDataRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onGetAppIconDataRequest(const ::boost::shared_ptr<GetAppIconDataRequest>& request)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   //! Send method start to CCA
   act_t oCcaAct = m_pSpiFi->sendGetAppIconDataStart(*this, request->getSIconURL());

   //! Store the Asynchronous Completion Token to distinguish subsequent calls
   m_oActMap.insert(std::pair<act_t, act_t>(oCcaAct, request->getAct()));

}

//!  method error 'GetAppIconData'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onGetAppIconDataError
 **********************************************************************************************************************/
void SpiCcaDbusComp::onGetAppIconDataError(const ::boost::shared_ptr<Midw_smartphoneint_fiProxy>& proxy,
         const boost::shared_ptr<midw_smartphoneint_fi::GetAppIconDataError>& error)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   //! Retrieve the Asynchronous Completion Token
   ActMap::iterator actPtr = m_oActMap.find(error->getAct());
   if (actPtr != m_oActMap.end())
   {
      ETG_TRACE_USR2(("key is %u and value is %u ", actPtr->first, actPtr->second));
      ETG_TRACE_USR4((" onGetAppIconDataError CCA Errorcode = %d, error->getCcaErrorCode()"));
      //! TODO covert CCA errors to DBus errors using utility

      //! Send the appropriate DBus Error message
      sendGetAppIconDataError("org.freedesktop.DBus.Error.NoReply", "Not implemented");
   }
   else
   {
      ETG_TRACE_ERR(("ACT received does not matches any key \n"));
   } // if (actPtr != m_oActMap.end())

}

//!  method result 'GetAppIconData'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onGetAppIconDataResult
 **********************************************************************************************************************/
void SpiCcaDbusComp::onGetAppIconDataResult(const ::boost::shared_ptr<Midw_smartphoneint_fiProxy>& proxy,
         const boost::shared_ptr<midw_smartphoneint_fi::GetAppIconDataResult>& result)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   ActMap::iterator actPtr = m_oActMap.find(result->getAct());
   if (actPtr != m_oActMap.end())
   {
      //TODO Convert from CCA type to DBus type with an utility and call method response
   }
   else
   {
      ETG_TRACE_ERR(("ACT received does not matches any key \n"));
   } // if (actPtr != m_oActMap.end())
}

/*******************************************GetAppList*****************************************************************/
//!  method start 'GetAppList'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onGetAppListRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onGetAppListRequest(const ::boost::shared_ptr<GetAppListRequest>& request)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   //! Send method start to CCA
   act_t oCcaAct = m_pSpiFi-> sendGetAppListStart(*this, request->getDeviceHandle());

   //! Store the Asynchronous Completion Token
   m_oActMap.insert(std::pair<act_t, act_t>(oCcaAct, request->getAct()));
}

//!  method error 'GetAppList'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onGetAppListError
 **********************************************************************************************************************/
void SpiCcaDbusComp::onGetAppListError(const ::boost::shared_ptr<Midw_smartphoneint_fiProxy>& proxy,
         const boost::shared_ptr<midw_smartphoneint_fi::GetAppListError>& error)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   //! Retrieve the Asynchronous Completion Token
   ActMap::iterator actPtr = m_oActMap.find(error->getAct());
   if (actPtr != m_oActMap.end())
   {
      ETG_TRACE_USR2(("key is %u and value is %u ", actPtr->first, actPtr->second));
      ETG_TRACE_USR4((" onGetAppListError CCA Errorcode = %d, error->getCcaErrorCode()"));
      //! TODO covert CCA errors to DBus errors using utility

      //! Send the appropriate DBus Error message
      sendGetAppListError("org.freedesktop.DBus.Error.NoReply", "Not implemented");
   }
   else
   {
      ETG_TRACE_ERR(("ACT received does not matches any key \n"));
   } // if (actPtr != m_oActMap.end())
}

//!  method result 'GetAppList'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onGetAppListResult
 **********************************************************************************************************************/
void SpiCcaDbusComp::onGetAppListResult(const ::boost::shared_ptr<Midw_smartphoneint_fiProxy>& proxy,
         const boost::shared_ptr<midw_smartphoneint_fi::GetAppListResult>& result)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   //! Retrieve the Asynchronous Completion Token
   ActMap::iterator actPtr = m_oActMap.find(result->getAct());
   if (actPtr != m_oActMap.end())
   {
      ETG_TRACE_USR2(("key is %u and value is %u ", actPtr->first, actPtr->second));
      std::vector < midw_smartphoneint_fi_types::T_AppInfo > vecCcaAppInfoList = result->getAppInfoList();
      std::vector < GetAppListResponseAppInfoListStruct > vecDbusAppInfoList;
      //TODO Use utility to copy to vecDbusAppInfoList and send
      sendGetAppListResponse(result->getNumAppInfoList(), vecDbusAppInfoList, actPtr->second);
   }
   else
   {
      ETG_TRACE_ERR(("ACT received does not matches any key \n"));
   } // if (actPtr != m_oActMap.end())
}

/*******************************************GetDeviceInfoList*********************************************************/
//!  method start 'GetDeviceInfoList'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onGetDeviceInfoListRequest
 *************************************************************************************************************/
void SpiCcaDbusComp::onGetDeviceInfoListRequest(const ::boost::shared_ptr<GetDeviceInfoListRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************SelectDevice******************************************************************/
//!  method start 'SelectDevice'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onSelectDeviceRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onSelectDeviceRequest(const ::boost::shared_ptr<SelectDeviceRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************TerminateApp***************************************************************/
//!  method start 'TerminateApp'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onTerminateAppRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onTerminateAppRequest(const ::boost::shared_ptr<TerminateAppRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************SetAppIconAttributes*******************************************************/
//!  method start 'SetAppIconAttributes'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onSetAppIconAttributesRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onSetAppIconAttributesRequest(const ::boost::shared_ptr<SetAppIconAttributesRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************SetDeviceUsagePreference***************************************************/
//!  method start 'SetDeviceUsagePreference'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onSetDeviceUsagePreferenceRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onSetDeviceUsagePreferenceRequest(
         const ::boost::shared_ptr<SetDeviceUsagePreferenceRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************GetDeviceUsagePreference***************************************************/
//!  method start 'GetDeviceUsagePreference'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onGetDeviceUsagePreferenceRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onGetDeviceUsagePreferenceRequest(
         const ::boost::shared_ptr<GetDeviceUsagePreferenceRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************SetMLNotificationEnabledInfo***********************************************/
//!  method start 'SetMLNotificationEnabledInfo'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onSetMLNotificationEnabledInfoRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onSetMLNotificationEnabledInfoRequest(const ::boost::shared_ptr<
         SetMLNotificationEnabledInfoRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************InvokeNotificationAction***************************************************/
//!  method start 'InvokeNotificationAction'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onInvokeNotificationActionRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onInvokeNotificationActionRequest(
         const ::boost::shared_ptr<InvokeNotificationActionRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************GetVideoSettings*********************************************************/
//!  method start 'GetVideoSettings'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onGetVideoSettingsRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onGetVideoSettingsRequest(const ::boost::shared_ptr<GetVideoSettingsRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************SetOrientationMode*********************************************************/
//!  method start 'SetOrientationMode'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onSetOrientationModeRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onSetOrientationModeRequest(const ::boost::shared_ptr<SetOrientationModeRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************SetScreenSize**************************************************************/
//!  method start 'SetScreenSize'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onSetScreenSizeRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onSetScreenSizeRequest(const ::boost::shared_ptr<SetScreenSizeRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************SetVideoBlockingMode*******************************************************/
//!  method start 'SetVideoBlockingMode'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onSetVideoBlockingModeRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onSetVideoBlockingModeRequest(const ::boost::shared_ptr<SetVideoBlockingModeRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************SetAudioBlockingMode*******************************************************/
//!  method start 'SetAudioBlockingMode'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onSetAudioBlockingModeRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onSetAudioBlockingModeRequest(const ::boost::shared_ptr<SetAudioBlockingModeRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************SetVehicleMode*************************************************************/
//!  method start 'SetVehicleMode'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onSetVehicleModeRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onSetVehicleModeRequest(const ::boost::shared_ptr<SetVehicleModeRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************SendTouchEvent*************************************************************/
//!  method start 'SendTouchEvent'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onSendTouchEventRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onSendTouchEventRequest(const ::boost::shared_ptr<SendTouchEventRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************SendKeyEvent***************************************************************/
//!  method start 'SendKeyEvent'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onSendKeyEventRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onSendKeyEventRequest(const ::boost::shared_ptr<SendKeyEventRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************SetMirrorLinkServiceUsage**************************************************/
//!  method start 'SetMirrorLinkServiceUsage'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onSetMirrorLinkServiceUsageRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onSetMirrorLinkServiceUsageRequest(
         const ::boost::shared_ptr<SetMirrorLinkServiceUsageRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************UpdateCertificateFile******************************************************/
//!  method start 'UpdateCertificateFile'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onUpdateCertificateFileRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onUpdateCertificateFileRequest(const ::boost::shared_ptr<UpdateCertificateFileRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

/*******************************************SetClientCapabilities******************************************************/
//!  method start 'SetClientCapabilities'
/**********************************************************************************************************************
 ** FUNCTION:  void SpiCcaDbusComp::onSetClientCapabilitiesRequest
 **********************************************************************************************************************/
void SpiCcaDbusComp::onSetClientCapabilitiesRequest(const ::boost::shared_ptr<SetClientCapabilitiesRequest>& request)
{
   ETG_TRACE_ERR((" %s : Method currently not supported \n", __PRETTY_FUNCTION__));
}

//End of File 


