/**********************************************************************************************************************
 * \file           SpiCcaDbusComp.h
 * \brief          Hand-crafted code for CCA DBUS Adapter component implementation
 **********************************************************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    This component implementation provides glue code to map CCA messages from smartphoneint
                 CCA service to DBus service messages. The technology differences has taken care while mapping
                 messages from CCA to DBUS (like property and methods in CCA)
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date            |  Author                                | Modifications
 2.12.2013       |  Pruthvi Thej Nagaraju                 | Initial Version

 \endverbatim
 **********************************************************************************************************************/
#ifndef SPICCADBUSCOMP_H
#define SPICCADBUSCOMP_H

/**********************************************************************************************************************
 | includes:
 **********************************************************************************************************************/
//! std includes
#include <map>

//! ASF core includes
#include "asf/core/Application.h"
#include "asf/core/BaseComponent.h"
#include "asf/core/Logger.h"

//! API inclusions
#include "midw_smartphoneint_fiProxy.h"
#include "org/bosch/cm/psa/apiStub.h"

/**********************************************************************************************************************
 |defines and macros (scope: global)
 **********************************************************************************************************************/

/**********************************************************************************************************************
 |namespace usage
 **********************************************************************************************************************/
using namespace ::asf::core;
using namespace ::org::bosch::cm::psa::api;

namespace ccx_dbus
{
   /*!
    * \class SpiCcaDbusComp
    * \brief CCA to DBus adapter component class implements glue code to bind
    *        CCA messages to DBus messages
    */

   class SpiCcaDbusComp: public BaseComponent,
            //! Derive from the DBus stub of generated code to provide implementation
            public ApiStub,
            //! Informs the availability of CCA service
            public ServiceAvailableIF,
            //! To  Implement callbacks for property updates and method result. Call appropriate DBus call for
            //! this callbacks in implementation
            public ::midw_smartphoneint_fi::GetDeviceBTAddressCallbackIF,
            public ::midw_smartphoneint_fi::LaunchAppCallbackIF,
            public ::midw_smartphoneint_fi::DeviceStatusInfoCallbackIF,
            public ::midw_smartphoneint_fi::GetAppIconDataCallbackIF,
            public ::midw_smartphoneint_fi::GetAppListCallbackIF
   //! Extend the callback inheritance when supported. Can be found in generated file 'midw_smartphoneint_fiProxy.h'
   {
      public:

         /*************************************************************************************************************
          ** FUNCTION:  SpiCcaDbusComp::SpiCcaDbusComp();
          *************************************************************************************************************/
         /*!
          * \fn     SpiCcaDbusComp()
          * \brief  constructor
          * \sa     ~SpiCcaDbusComp()
          *************************************************************************************************************/
         SpiCcaDbusComp();

         /*************************************************************************************************************
          ** FUNCTION:  SpiCcaDbusComp::~SpiCcaDbusComp();
          *************************************************************************************************************/
         /*!
          * \fn     ~SpiCcaDbusComp()
          * \brief  virtual destructor
          * \sa     SpiCcaDbusComp()
          *************************************************************************************************************/
         virtual ~SpiCcaDbusComp();

         //!  ServiceAvailableIF implementation
         /**************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onAvailable
          *************************************************************************************************************/
         /*!
          * \fn     virtual void onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
          *                          const ServiceStateChange &stateChange);
          * \brief  Called when the CCA service is available for SPI
          * \param  proxy : The proxy instance for which the service is available
          * \param  stateChange: Contains previous and current state of the service indicating states Disconnected(0),
          *                      Connected(1), Suspended(2)
          * \sa          onUnavailable
          *************************************************************************************************************/
         virtual void onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
                  const ServiceStateChange &stateChange);

         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onUnavailable
          *************************************************************************************************************/
         /*!
          * \fn     virtual void onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
          *                const ServiceStateChange &stateChange);
          * \brief  Called when the CCA service becomes unavailable for SPI
          * \param  proxy : The proxy instance for which the service is unavailable
          * \param  stateChange: Contains previous and current state of the service indicating states Disconnected(0),
          *                      Connected(1), Suspended(2)
          * \sa     OnAvailable
          *************************************************************************************************************/
         virtual void onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
                  const ServiceStateChange &stateChange);

         /************* Manual Code for CCA interface Callbacks********************************************************/

         /*************************************************************************************************************
          ***************************************** PROPERTIES ********************************************************
          *************************************************************************************************************/

         /**********************************DeviceStatusInfo*************************************************************/
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onDeviceStatusInfoError
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onDeviceStatusInfoError(
          *                   const ::boost::shared_ptr<midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy,
          *                   const boost::shared_ptr<midw_smartphoneint_fi::DeviceStatusInfoError>& error);
          * \brief   Called when the Error is received for this Property
          * \param   proxy : The proxy instance from which the property update is received
          * \param   error:  The error received for the property update
          * \sa          onDeviceStatusInfoStatus
          *************************************************************************************************************/
         virtual void onDeviceStatusInfoError(const ::boost::shared_ptr<
                  midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy, const boost::shared_ptr<
                  midw_smartphoneint_fi::DeviceStatusInfoError>& error);

         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onDeviceStatusInfoStatus
          *************************************************************************************************************/
         /*!
          * \fn     virtual void onDeviceStatusInfoStatus(
          *                   const ::boost::shared_ptr<midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy,
          *                   const boost::shared_ptr<midw_smartphoneint_fi::DeviceStatusInfoStatus>& status);
          * \brief   Called when the status update is received for this Property
          * \param   proxy : The proxy instance from which the property update is received
          * \param   status:  The status info for this property update
          * \sa          onDeviceStatusInfoError
          *************************************************************************************************************/
         virtual void onDeviceStatusInfoStatus(const ::boost::shared_ptr<
                  midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy, const boost::shared_ptr<
                  midw_smartphoneint_fi::DeviceStatusInfoStatus>& status);

         /*************************************************************************************************************
          ***************************************** METHODS ***********************************************************
          *************************************************************************************************************/

         /**********************************GetDeviceBTAddress*********************************************************/
         //!  method start 'GetDeviceBTAddress'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onGetDeviceBTAddressRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onGetDeviceBTAddressRequest(const ::boost::shared_ptr<GetDeviceBTAddressRequest>& request)
          * \brief   Called by ASF when a client invokes the method 'GetDeviceBTAddress'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onGetDeviceBTAddressError, onGetDeviceBTAddressResult
          *************************************************************************************************************/
         virtual void onGetDeviceBTAddressRequest(const ::boost::shared_ptr<GetDeviceBTAddressRequest>& request);

         //!  method error 'GetDeviceBTAddress'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onGetDeviceBTAddressError
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onGetDeviceBTAddressError(
          *                   const ::boost::shared_ptr<midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy,
          *                   const boost::shared_ptr<midw_smartphoneint_fi::GetDeviceBTAddressError>& error);
          * \brief   Called by ASF when Error is received for this methods from CCA. Do not invoke this method
          *          on your own. send the corresponding DBus interface method error.
          * \param   proxy : the instance of the cca proxy that invokes this method error
          * \param   error : Error code on failure of Method start
          *************************************************************************************************************/
         virtual void onGetDeviceBTAddressError(const ::boost::shared_ptr<
                  midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy, const boost::shared_ptr<
                  midw_smartphoneint_fi::GetDeviceBTAddressError>& error);

         //!  method result 'GetDeviceBTAddress'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onGetDeviceBTAddressResult
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onGetDeviceBTAddressResult(
          *                   const ::boost::shared_ptr<midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy,
          *                   const boost::shared_ptr<midw_smartphoneint_fi::GetDeviceBTAddressResult>& result);
          * \brief   Called by ASF when response is received for this methods from CCA. Do not invoke this method
          *          on your own. send the corresponding DBus interface method response.
          * \param   proxy : the instance of the cca proxy that invokes this method result
          * \param   result : Result on invocation of Method start
          *************************************************************************************************************/
         virtual void onGetDeviceBTAddressResult(const ::boost::shared_ptr<
                  midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy, const boost::shared_ptr<
                  midw_smartphoneint_fi::GetDeviceBTAddressResult>& result);

         /**********************************LaunchApp*********************************************************/
         //!  method start 'LaunchApp'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onLaunchAppRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onLaunchAppRequest(const ::boost::shared_ptr<LaunchAppRequest>& request);
          * \brief   Called by ASF when a client invokes the method 'LaunchApp'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onLaunchAppError, onLaunchAppResult
          *************************************************************************************************************/
         virtual void onLaunchAppRequest(const ::boost::shared_ptr<LaunchAppRequest>& request);

         //!  method error 'LaunchApp'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onLaunchAppError
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onLaunchAppError(
          *                   const ::boost::shared_ptr<midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy,
          *                   const boost::shared_ptr<midw_smartphoneint_fi::LaunchAppError>& error);
          * \brief   Called by ASF when Error is received for this methods from CCA. Do not invoke this method
          *          on your own. send the corresponding DBus interface method error.
          * \param   proxy : the instance of the cca proxy that invokes this method error
          * \param   error : Error code on failure of Method start
          *************************************************************************************************************/
         virtual void onLaunchAppError(
                  const ::boost::shared_ptr<midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy,
                  const boost::shared_ptr<midw_smartphoneint_fi::LaunchAppError>& error);

         //!  method result 'LaunchApp'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onLaunchAppResult
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onLaunchAppResult(
          *                   const ::boost::shared_ptr<midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy,
          *                   const boost::shared_ptr<midw_smartphoneint_fi::LaunchAppResult>& result);
          * \brief   Called by ASF when response is received for this methods from CCA. Do not invoke this method
          *          on your own. send the corresponding DBus interface method response.
          * \param   proxy : the instance of the cca proxy that invokes this method result
          * \param   result : Result on invocation of Method start
          *************************************************************************************************************/
         virtual void onLaunchAppResult(
                  const ::boost::shared_ptr<midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy,
                  const boost::shared_ptr<midw_smartphoneint_fi::LaunchAppResult>& result);

         // TODO Implement Following Methods

         /**********************************GetAppIconData*************************************************************/
         //!  method start 'GetAppIconData'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onGetAppIconDataRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onGetAppIconDataRequest(const ::boost::shared_ptr<GetAppIconDataRequest>& request);
          * \brief   Called by ASF when a client invokes the method 'GetAppIconData'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to
          *          the response methods.
          * \sa      onGetAppIconDataError, onGetAppIconDataResult
          *************************************************************************************************************/
         virtual void onGetAppIconDataRequest(const ::boost::shared_ptr<GetAppIconDataRequest>& request);

         //!  method error 'GetAppIconData'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onGetAppIconDataError
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onGetAppIconDataError(
          *                   const ::boost::shared_ptr<midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy,
          *                   const boost::shared_ptr<midw_smartphoneint_fi::GetAppIconDataError>& error);
          * \brief   Called by ASF when Error is received for this methods from CCA. Do not invoke this method
          *          on your own. send the corresponding DBus interface method error.
          * \param   proxy : the instance of the cca proxy that invokes this method error
          * \param   error : Error code on failure of Method start
          *************************************************************************************************************/
         virtual void onGetAppIconDataError(
                  const ::boost::shared_ptr<midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy,
                  const boost::shared_ptr<midw_smartphoneint_fi::GetAppIconDataError>& error);

         //!  method result 'GetAppIconData'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onGetAppIconDataResult
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onGetAppIconDataResult(
          *                   const ::boost::shared_ptr<midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy,
          *                   const boost::shared_ptr<midw_smartphoneint_fi::GetAppIconDataResult>& result);
          * \brief   Called by ASF when response is received for this methods from CCA. Do not invoke this method
          *          on your own. send the corresponding DBus interface method response.
          * \param   proxy : the instance of the cca proxy that invokes this method result
          * \param   result : Result on invocation of Method start
          *************************************************************************************************************/
         virtual void onGetAppIconDataResult(const ::boost::shared_ptr<
                  midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy, const boost::shared_ptr<
                  midw_smartphoneint_fi::GetAppIconDataResult>& result);

         /**********************************GetAppList*****************************************************************/
         //!  method start 'GetAppList'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onGetAppListRequest
          *************************************************************************************************************/
         /*!
          * \fn     virtual void onGetAppListRequest(const ::boost::shared_ptr<GetAppListRequest>& request)
          * \brief  Called by ASF when a client invokes the method 'GetAppList'. Do not invoke this method on
          *         your own.A method start for CCA server must be invoked from this method.
          * \param  request : The parameter request provides the method call parameters. Additionally the request
          *         provides the field act (Asynchronous Completion Token) which can later be passed to the
          *         response methods.
          * \sa     onGetAppListError, onGetAppListResult
          *************************************************************************************************************/
         virtual void onGetAppListRequest(const ::boost::shared_ptr<GetAppListRequest>& request);

         //!  method error 'GetAppList'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onGetAppListError
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onGetAppListError(
          *                   const ::boost::shared_ptr<midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy,
          *                   const boost::shared_ptr<midw_smartphoneint_fi::GetAppListError>& error);
          * \brief   Called by ASF when Error is received for this methods from CCA. Do not invoke this method
          *          on your own. send the corresponding DBus interface method error.
          * \param   proxy : the instance of the cca proxy that invokes this method error
          * \param   error : Error code on failure of Method start
          *************************************************************************************************************/
         virtual void onGetAppListError(
                  const ::boost::shared_ptr<midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy,
                  const boost::shared_ptr<midw_smartphoneint_fi::GetAppListError>& error);

         //!  method result 'GetAppList'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onGetAppListResult
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onGetAppListResult(
          *                   const ::boost::shared_ptr<midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy,
          *                   const boost::shared_ptr<midw_smartphoneint_fi::GetAppListResult>& result);
          * \brief   Called by ASF when response is received for this methods from CCA. Do not invoke this method
          *          on your own. send the corresponding DBus interface method response.
          * \param   proxy : the instance of the cca proxy that invokes this method result
          * \param   result : Result on invocation of Method start
          *************************************************************************************************************/
         virtual void onGetAppListResult(
                  const ::boost::shared_ptr<midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& proxy,
                  const boost::shared_ptr<midw_smartphoneint_fi::GetAppListResult>& result);


         /**********************************GetDeviceInfoList*********************************************************/
         //!  method start 'GetDeviceInfoList'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onGetDeviceInfoListRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onGetDeviceInfoListRequest(const ::boost::shared_ptr<GetDeviceInfoListRequest>& request)
          * \brief   Called by ASF when a client invokes the method 'GetDeviceInfoList'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onGetDeviceInfoListError, onGetDeviceInfoListResult
          *************************************************************************************************************/
         virtual void onGetDeviceInfoListRequest(const ::boost::shared_ptr<GetDeviceInfoListRequest>& request);

         /**********************************SelectDevice*********************************************************/
         //!  method start 'SelectDevice'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onSelectDeviceRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onSelectDeviceRequest(const ::boost::shared_ptr<SelectDeviceRequest>& request)
          * \brief   Called by ASF when a client invokes the method 'SelectDevice'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onSelectDeviceError, onSelectDeviceResult
          *************************************************************************************************************/
         virtual void onSelectDeviceRequest(const ::boost::shared_ptr<SelectDeviceRequest>& request);


         /**********************************TerminateApp*********************************************************/
         //!  method start 'TerminateApp'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onTerminateAppRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onTerminateAppRequest(const ::boost::shared_ptr<TerminateAppRequest>& request);
          * \brief   Called by ASF when a client invokes the method 'TerminateApp'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onTerminateAppError, onTerminateAppResult
          *************************************************************************************************************/
         virtual void onTerminateAppRequest(const ::boost::shared_ptr<TerminateAppRequest>& request);

         /**********************************SetAppIconAttributes*******************************************************/
         //!  method start 'SetAppIconAttributes'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onSetAppIconAttributesRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onSetAppIconAttributesRequest(const ::boost::shared_ptr<SetAppIconAttributesRequest>& request)
          * \brief   Called by ASF when a client invokes the method 'SetAppIconAttributes'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onSetAppIconAttributesError, onSetAppIconAttributesResult
          *************************************************************************************************************/
         virtual void onSetAppIconAttributesRequest(const ::boost::shared_ptr<SetAppIconAttributesRequest>& request);

         /**********************************SetDeviceUsagePreference***************************************************/
         //!  method start 'SetDeviceUsagePreference'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onSetDeviceUsagePreferenceRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onSetDeviceUsagePreferenceRequest(
          *             const ::boost::shared_ptr<SetDeviceUsagePreferenceRequest>& request);
          * \brief   Called by ASF when a client invokes the method 'SetDeviceUsagePreference'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onSetDeviceUsagePreferenceError, onSetDeviceUsagePreferenceResult
          *************************************************************************************************************/
         virtual void onSetDeviceUsagePreferenceRequest(
                  const ::boost::shared_ptr<SetDeviceUsagePreferenceRequest>& request);

         /**********************************GetDeviceUsagePreference***************************************************/
         //!  method start 'GetDeviceUsagePreference'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onGetDeviceUsagePreferenceRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onGetDeviceUsagePreferenceRequest(
          *             const ::boost::shared_ptr<GetDeviceUsagePreferenceRequest>& request);
          * \brief   Called by ASF when a client invokes the method 'GetDeviceUsagePreference'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onGetDeviceUsagePreferenceError, onGetDeviceUsagePreferenceResult
          *************************************************************************************************************/
         virtual void onGetDeviceUsagePreferenceRequest(
                  const ::boost::shared_ptr<GetDeviceUsagePreferenceRequest>& request);

         /**********************************SetMLNotificationEnabledInfo***********************************************/
         //!  method start 'SetMLNotificationEnabledInfo'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onSetMLNotificationEnabledInfoRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onSetMLNotificationEnabledInfoRequest(
          *             const ::boost::shared_ptr<SetMLNotificationEnabledInfoRequest>& request);
          * \brief   Called by ASF when a client invokes the method 'SetMLNotificationEnabledInfo'. Do not invoke
          *          this method on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onSetMLNotificationEnabledInfoError, onSetMLNotificationEnabledInfoResult
          *************************************************************************************************************/
         virtual void onSetMLNotificationEnabledInfoRequest(const ::boost::shared_ptr<
                  SetMLNotificationEnabledInfoRequest>& request);

         /**********************************InvokeNotificationAction***************************************************/
         //!  method start 'InvokeNotificationAction'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onInvokeNotificationActionRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onInvokeNotificationActionRequest(
          *             const ::boost::shared_ptr<InvokeNotificationActionRequest>& request);
          * \brief   Called by ASF when a client invokes the method 'InvokeNotificationAction'. Do not invoke this
          *          method on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onInvokeNotificationActionError, onInvokeNotificationActionResult
          *************************************************************************************************************/
         virtual void onInvokeNotificationActionRequest(
                  const ::boost::shared_ptr<InvokeNotificationActionRequest>& request);

         /**********************************GetVideoSettings*********************************************************/
         //!  method start 'GetVideoSettings'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onGetVideoSettingsRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onGetVideoSettingsRequest(const ::boost::shared_ptr<GetVideoSettingsRequest>& request);
          * \brief   Called by ASF when a client invokes the method 'GetVideoSettings'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onGetVideoSettingsError, onGetVideoSettingsResult
          *************************************************************************************************************/
         virtual void onGetVideoSettingsRequest(const ::boost::shared_ptr<GetVideoSettingsRequest>& request);

         /**********************************SetOrientationMode*********************************************************/
         //!  method start 'SetOrientationMode'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onSetOrientationModeRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onSetOrientationModeRequest(const ::boost::shared_ptr<SetOrientationModeRequest>& request);
          * \brief   Called by ASF when a client invokes the method 'SetOrientationMode'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onSetOrientationModeError, onSetOrientationModeResult
          *************************************************************************************************************/
         virtual void onSetOrientationModeRequest(const ::boost::shared_ptr<SetOrientationModeRequest>& request);

         /**********************************SetScreenSize*********************************************************/
         //!  method start 'SetScreenSize'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onSetScreenSizeRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onSetScreenSizeRequest(const ::boost::shared_ptr<SetScreenSizeRequest>& request);
          * \brief   Called by ASF when a client invokes the method 'SetScreenSize'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onSetScreenSizeError, onSetScreenSizeResult
          *************************************************************************************************************/
         virtual void onSetScreenSizeRequest(const ::boost::shared_ptr<SetScreenSizeRequest>& request);

         /**********************************SetVideoBlockingMode*******************************************************/
         //!  method start 'SetVideoBlockingMode'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onSetVideoBlockingModeRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onSetVideoBlockingModeRequest(const ::boost::shared_ptr<SetVideoBlockingModeRequest>& request)
          * \brief   Called by ASF when a client invokes the method 'SetVideoBlockingMode'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onSetVideoBlockingModeError, onSetVideoBlockingModeResult
          *************************************************************************************************************/
         virtual void onSetVideoBlockingModeRequest(const ::boost::shared_ptr<SetVideoBlockingModeRequest>& request);

         /**********************************SetAudioBlockingMode*******************************************************/
         //!  method start 'SetAudioBlockingMode'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onSetAudioBlockingModeRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onSetAudioBlockingModeRequest(const ::boost::shared_ptr<SetAudioBlockingModeRequest>& request)
          * \brief   Called by ASF when a client invokes the method 'SetAudioBlockingMode'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onSetAudioBlockingModeError, onSetAudioBlockingModeResult
          *************************************************************************************************************/
         virtual void onSetAudioBlockingModeRequest(const ::boost::shared_ptr<SetAudioBlockingModeRequest>& request);

         /**********************************SetVehicleMode*********************************************************/
         //!  method start 'SetVehicleMode'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onSetVehicleModeRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onSetVehicleModeRequest(const ::boost::shared_ptr<SetVehicleModeRequest>& request);
          * \brief   Called by ASF when a client invokes the method 'SetVehicleMode'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onSetVehicleModeError, onSetVehicleModeResult
          *************************************************************************************************************/
         virtual void onSetVehicleModeRequest(const ::boost::shared_ptr<SetVehicleModeRequest>& request);

         /**********************************SendTouchEvent*************************************************************/
         //!  method start 'SendTouchEvent'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onSendTouchEventRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onSendTouchEventRequest(const ::boost::shared_ptr<SendTouchEventRequest>& request);
          * \brief   Called by ASF when a client invokes the method 'SendTouchEvent'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onSendTouchEventError, onSendTouchEventResult
          *************************************************************************************************************/
         virtual void onSendTouchEventRequest(const ::boost::shared_ptr<SendTouchEventRequest>& request);

         /**********************************SendKeyEvent*********************************************************/
         //!  method start 'SendKeyEvent'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onSendKeyEventRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onSendKeyEventRequest(const ::boost::shared_ptr<SendKeyEventRequest>& request);
          * \brief   Called by ASF when a client invokes the method 'SendKeyEvent'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onSendKeyEventError, onSendKeyEventResult
          *************************************************************************************************************/
         virtual void onSendKeyEventRequest(const ::boost::shared_ptr<SendKeyEventRequest>& request);

         /**********************************SetMirrorLinkServiceUsage**************************************************/
         //!  method start 'SetMirrorLinkServiceUsage'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onSetMirrorLinkServiceUsageRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onSetMirrorLinkServiceUsageRequest(
          *             const ::boost::shared_ptr<SetMirrorLinkServiceUsageRequest>& request);
          * \brief   Called by ASF when a client invokes the method 'SetMirrorLinkServiceUsage'. Do not invoke this
          *          method on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onSetMirrorLinkServiceUsageError, onSetMirrorLinkServiceUsageResult
          *************************************************************************************************************/
         virtual void onSetMirrorLinkServiceUsageRequest(
                  const ::boost::shared_ptr<SetMirrorLinkServiceUsageRequest>& request);

         /**********************************UpdateCertificateFile******************************************************/
         //!  method start 'UpdateCertificateFile'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onUpdateCertificateFileRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onUpdateCertificateFileRequest(
          *             const ::boost::shared_ptr<UpdateCertificateFileRequest>& request);
          * \brief   Called by ASF when a client invokes the method 'UpdateCertificateFile'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onUpdateCertificateFileError, onUpdateCertificateFileResult
          *************************************************************************************************************/
         virtual void onUpdateCertificateFileRequest(const ::boost::shared_ptr<UpdateCertificateFileRequest>& request);

         /**********************************SetClientCapabilities******************************************************/
         //!  method start 'SetClientCapabilities'
         /*************************************************************************************************************
          ** FUNCTION:  virtual void SpiCcaDbusComp::onSetClientCapabilitiesRequest
          *************************************************************************************************************/
         /*!
          * \fn      virtual void onSetClientCapabilitiesRequest(
          *             const ::boost::shared_ptr<SetClientCapabilitiesRequest>& request);
          * \brief   Called by ASF when a client invokes the method 'SetClientCapabilities'. Do not invoke this method
          *          on your own. A method start for CCA server must be invoked from this method.
          * \param   request : The parameter request provides the method call parameters. Additionally the request
          *          provides the field act (Asynchronous Completion Token) which can later be passed to the
          *          response methods.
          * \sa      onSetClientCapabilitiesError, onSetClientCapabilitiesResult
          *************************************************************************************************************/
         virtual void onSetClientCapabilitiesRequest(const ::boost::shared_ptr<SetClientCapabilitiesRequest>& request);

      private:

         //! Instance of ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy
         boost::shared_ptr<midw_smartphoneint_fi::Midw_smartphoneint_fiProxy> m_pSpiFi;

         //! map to store the asynchronous token received on invoking methods
         typedef std::map<act_t, act_t> ActMap;
         ActMap m_oActMap;

         DECLARE_CLASS_LOGGER   ();

   };

}

#endif // SPICCADBUSCOMP_H
//End of File 

