/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#include "BaseComponent.h"
#include "Midw_smartphoneint_fiAdapterComponent.h"
#include "Midw_smartphoneint_fiAdapterComponentStub.h"
#include "asf/core/Logger.h"
#include "boost/shared_ptr.hpp"
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace ::asf::core;

namespace midw_smartphoneint_fiComponent {
namespace adapter {

using namespace ::midw_smartphoneint_fi;
using namespace ::midw_smartphoneint_fi_types;

DEFINE_CLASS_LOGGER_AND_LEVEL ("midw_smartphoneint_fiComponent/adapter/Midw_smartphoneint_fiAdapterComponent", Midw_smartphoneint_fiAdapterComponent, Info);


Midw_smartphoneint_fiAdapterComponent::Midw_smartphoneint_fiAdapterComponent () : m_pomidw_smartphoneint_fiProxy\
                            (::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy::\
                                createProxy (
                                "midw_smartphoneint_fiRequiredPort", *this
                                )
                            )
{
    LOG_INFO ("Midw_smartphoneint_fiAdapterComponent constructor...");
    m_poMidw_smartphoneint_fiAdapterComponentInfrastructureStub.reset \
    (new Midw_smartphoneint_fiAdapterComponentInfrastructureStub ());
}

Midw_smartphoneint_fiAdapterComponent::~Midw_smartphoneint_fiAdapterComponent ()
{
    LOG_INFO ("Midw_smartphoneint_fiAdapterComponent destructor...");
}

void Midw_smartphoneint_fiAdapterComponent::onAvailable\
(const ::boost::shared_ptr< ::asf::core::Proxy >& rfoProxy, const ::asf::core::ServiceStateChange& corfoStateChange){
    LOG_INFO ("onAvailable ()...");
    if (m_pomidw_smartphoneint_fiProxy == rfoProxy) {
        LOG_INFO ("Service is available, starting JSON Stub Component ...");
        m_poMidw_smartphoneint_fiAdapterComponentStub.reset (new Midw_smartphoneint_fiAdapterComponentStub (this));
    }
}
void Midw_smartphoneint_fiAdapterComponent::onUnavailable\
(const ::boost::shared_ptr< ::asf::core::Proxy >& rfoProxy, const ::asf::core::ServiceStateChange& corfoStateChange){
    LOG_INFO ("onUnavailable ()...");
}
void Midw_smartphoneint_fiAdapterComponent::setPropertyStubAvailable (bool enable) {
    LOG_INFO("Midw_smartphoneint_fiAdapterComponent::setPropertyStubAvailable ()...\
    enable: %d",enable);
    m_poMidw_smartphoneint_fiAdapterComponentInfrastructureStub->\
    setPropertyStubAvailable (enable);
}

// Callback 'AccessoryAppStateCallbackIF'

// Glue function vGlueAccessoryAppStateRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueAccessoryAppStateRequest (const ::midw_smartphoneint_fi_types::T_e8_SpeechAppState& AppStateSpeech, const ::midw_smartphoneint_fi_types::T_e8_PhoneAppState& AppStatePhone, const ::midw_smartphoneint_fi_types::T_e8_NavigationAppState& AppStateNavigation,act_t actJson){
    LOG_INFO ("vGlueAccessoryAppStateRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendAccessoryAppStateStart (AppStateSpeech, AppStatePhone, AppStateNavigation);
    }
}

// Callback 'AccessoryAudioContextCallbackIF'

// Glue function vGlueAccessoryAudioContextRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueAccessoryAudioContextRequest (uint32 DeviceHandle, bool AudioFlag, const ::midw_smartphoneint_fi_types::T_e8_AudioContext& AudioContext,act_t actJson){
    LOG_INFO ("vGlueAccessoryAudioContextRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendAccessoryAudioContextStart (DeviceHandle, AudioFlag, AudioContext);
    }
}

// Callback 'AccessoryDisplayContextCallbackIF'

// Glue function vGlueAccessoryDisplayContextRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueAccessoryDisplayContextRequest (uint32 DeviceHandle, bool DisplayFlag, const ::midw_smartphoneint_fi_types::T_e8_DisplayContext& DisplayContext,act_t actJson){
    LOG_INFO ("vGlueAccessoryDisplayContextRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendAccessoryDisplayContextStart (DeviceHandle, DisplayFlag, DisplayContext);
    }
}

// Callback 'AppStatusInfoCallbackIF'

// Glue function vGlueAppStatusInfoDeregister
        void Midw_smartphoneint_fiAdapterComponent::vGlueAppStatusInfoDeregister (){
        LOG_INFO ("vGlueAppStatusInfoDeregister ()...");
        map<string, act_t>::iterator actPropRegNamePtr = _actPropRegNameMap.find ("AppStatusInfo");
        if (actPropRegNamePtr != _actPropRegNameMap.end ()){
            LOG_DEBUG ("key is %s and value is %u ", actPropRegNamePtr->first.c_str(), actPropRegNamePtr->second);
            _actPropRegMap.erase (actPropRegNamePtr->second);
            _actPropRegNameMap.erase ("AppStatusInfo");
        }
        else {
        LOG_FATAL ("Name received does not match any key in _actPropRegNameMap");
        }
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            m_pomidw_smartphoneint_fiProxy->sendAppStatusInfoRelUpRegAll ();
        }
    }

// Glue function vGlueAppStatusInfoRegister
void Midw_smartphoneint_fiAdapterComponent::vGlueAppStatusInfoRegister (act_t actJson){
        LOG_INFO ("vGlueAppStatusInfoRegister ()...");
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendAppStatusInfoUpReg (*this);
            _actPropRegMap.insert (pair<act_t, act_t> (ccaAct, actJson) );
            _actPropRegNameMap.insert (pair<string, act_t> ("AppStatusInfo", ccaAct) );
        }
}

// onAppStatusInfoError
void Midw_smartphoneint_fiAdapterComponent::onAppStatusInfoError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::AppStatusInfoError >& corfoError){
    LOG_INFO ("onAppStatusInfoError...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoError->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        switch (corfoError->getField ()){
        case ::midw_smartphoneint_fi::AppStatusInfoError::E_CCA_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueAppStatusInfoRegisterError \
            (corfoError->getCcaErrorCode (), actRegPtr->second);
        break;
        case ::midw_smartphoneint_fi::AppStatusInfoError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueAppStatusInfoRegisterError \
            (corfoError->getSystemErrorCode (), actRegPtr->second);
        break;
        default:
            LOG_FATAL ("Invalid error field");
        }
        u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// onAppStatusInfoStatus
void Midw_smartphoneint_fiAdapterComponent::onAppStatusInfoStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< AppStatusInfoStatus >& corfoUpdate){
    LOG_INFO ("onAppStatusInfoStatus...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoUpdate->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueAppStatusInfoPropertyUpdate \
        (corfoUpdate->getDeviceHandle (), corfoUpdate->getDeviceConnectionType (), corfoUpdate->getAppStatus (), actRegPtr->second);
         u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// Callback 'ApplicationMetaDataCallbackIF'

// Glue function vGlueApplicationMetaDataDeregister
        void Midw_smartphoneint_fiAdapterComponent::vGlueApplicationMetaDataDeregister (){
        LOG_INFO ("vGlueApplicationMetaDataDeregister ()...");
        map<string, act_t>::iterator actPropRegNamePtr = _actPropRegNameMap.find ("ApplicationMetaData");
        if (actPropRegNamePtr != _actPropRegNameMap.end ()){
            LOG_DEBUG ("key is %s and value is %u ", actPropRegNamePtr->first.c_str(), actPropRegNamePtr->second);
            _actPropRegMap.erase (actPropRegNamePtr->second);
            _actPropRegNameMap.erase ("ApplicationMetaData");
        }
        else {
        LOG_FATAL ("Name received does not match any key in _actPropRegNameMap");
        }
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            m_pomidw_smartphoneint_fiProxy->sendApplicationMetaDataRelUpRegAll ();
        }
    }

// Glue function vGlueApplicationMetaDataRegister
void Midw_smartphoneint_fiAdapterComponent::vGlueApplicationMetaDataRegister (act_t actJson){
        LOG_INFO ("vGlueApplicationMetaDataRegister ()...");
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendApplicationMetaDataUpReg (*this);
            _actPropRegMap.insert (pair<act_t, act_t> (ccaAct, actJson) );
            _actPropRegNameMap.insert (pair<string, act_t> ("ApplicationMetaData", ccaAct) );
        }
}

// onApplicationMetaDataError
void Midw_smartphoneint_fiAdapterComponent::onApplicationMetaDataError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::ApplicationMetaDataError >& corfoError){
    LOG_INFO ("onApplicationMetaDataError...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoError->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        switch (corfoError->getField ()){
        case ::midw_smartphoneint_fi::ApplicationMetaDataError::E_CCA_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueApplicationMetaDataRegisterError \
            (corfoError->getCcaErrorCode (), actRegPtr->second);
        break;
        case ::midw_smartphoneint_fi::ApplicationMetaDataError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueApplicationMetaDataRegisterError \
            (corfoError->getSystemErrorCode (), actRegPtr->second);
        break;
        default:
            LOG_FATAL ("Invalid error field");
        }
        u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// onApplicationMetaDataStatus
void Midw_smartphoneint_fiAdapterComponent::onApplicationMetaDataStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ApplicationMetaDataStatus >& corfoUpdate){
    LOG_INFO ("onApplicationMetaDataStatus...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoUpdate->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueApplicationMetaDataPropertyUpdate \
        (corfoUpdate->getDeviceHandle (), corfoUpdate->getMetaDataValid (), corfoUpdate->getApplicationMetaData (), actRegPtr->second);
         u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// Callback 'BluetoothDeviceStatusCallbackIF'

// Glue function vGlueBluetoothDeviceStatusDeregister
        void Midw_smartphoneint_fiAdapterComponent::vGlueBluetoothDeviceStatusDeregister (){
        LOG_INFO ("vGlueBluetoothDeviceStatusDeregister ()...");
        map<string, act_t>::iterator actPropRegNamePtr = _actPropRegNameMap.find ("BluetoothDeviceStatus");
        if (actPropRegNamePtr != _actPropRegNameMap.end ()){
            LOG_DEBUG ("key is %s and value is %u ", actPropRegNamePtr->first.c_str(), actPropRegNamePtr->second);
            _actPropRegMap.erase (actPropRegNamePtr->second);
            _actPropRegNameMap.erase ("BluetoothDeviceStatus");
        }
        else {
        LOG_FATAL ("Name received does not match any key in _actPropRegNameMap");
        }
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            m_pomidw_smartphoneint_fiProxy->sendBluetoothDeviceStatusRelUpRegAll ();
        }
    }

// Glue function vGlueBluetoothDeviceStatusRegister
void Midw_smartphoneint_fiAdapterComponent::vGlueBluetoothDeviceStatusRegister (act_t actJson){
        LOG_INFO ("vGlueBluetoothDeviceStatusRegister ()...");
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendBluetoothDeviceStatusUpReg (*this);
            _actPropRegMap.insert (pair<act_t, act_t> (ccaAct, actJson) );
            _actPropRegNameMap.insert (pair<string, act_t> ("BluetoothDeviceStatus", ccaAct) );
        }
}

// onBluetoothDeviceStatusError
void Midw_smartphoneint_fiAdapterComponent::onBluetoothDeviceStatusError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::BluetoothDeviceStatusError >& corfoError){
    LOG_INFO ("onBluetoothDeviceStatusError...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoError->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        switch (corfoError->getField ()){
        case ::midw_smartphoneint_fi::BluetoothDeviceStatusError::E_CCA_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueBluetoothDeviceStatusRegisterError \
            (corfoError->getCcaErrorCode (), actRegPtr->second);
        break;
        case ::midw_smartphoneint_fi::BluetoothDeviceStatusError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueBluetoothDeviceStatusRegisterError \
            (corfoError->getSystemErrorCode (), actRegPtr->second);
        break;
        default:
            LOG_FATAL ("Invalid error field");
        }
        u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// onBluetoothDeviceStatusStatus
void Midw_smartphoneint_fiAdapterComponent::onBluetoothDeviceStatusStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< BluetoothDeviceStatusStatus >& corfoUpdate){
    LOG_INFO ("onBluetoothDeviceStatusStatus...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoUpdate->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueBluetoothDeviceStatusPropertyUpdate \
        (corfoUpdate->getBluetoothDeviceHandle (), corfoUpdate->getProjectionDeviceHandle (), corfoUpdate->getBTChangeInfo (), corfoUpdate->getCallActiveStatus (), actRegPtr->second);
         u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// Callback 'DAPStatusInfoCallbackIF'

// Glue function vGlueDAPStatusInfoDeregister
        void Midw_smartphoneint_fiAdapterComponent::vGlueDAPStatusInfoDeregister (){
        LOG_INFO ("vGlueDAPStatusInfoDeregister ()...");
        map<string, act_t>::iterator actPropRegNamePtr = _actPropRegNameMap.find ("DAPStatusInfo");
        if (actPropRegNamePtr != _actPropRegNameMap.end ()){
            LOG_DEBUG ("key is %s and value is %u ", actPropRegNamePtr->first.c_str(), actPropRegNamePtr->second);
            _actPropRegMap.erase (actPropRegNamePtr->second);
            _actPropRegNameMap.erase ("DAPStatusInfo");
        }
        else {
        LOG_FATAL ("Name received does not match any key in _actPropRegNameMap");
        }
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            m_pomidw_smartphoneint_fiProxy->sendDAPStatusInfoRelUpRegAll ();
        }
    }

// Glue function vGlueDAPStatusInfoRegister
void Midw_smartphoneint_fiAdapterComponent::vGlueDAPStatusInfoRegister (act_t actJson){
        LOG_INFO ("vGlueDAPStatusInfoRegister ()...");
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendDAPStatusInfoUpReg (*this);
            _actPropRegMap.insert (pair<act_t, act_t> (ccaAct, actJson) );
            _actPropRegNameMap.insert (pair<string, act_t> ("DAPStatusInfo", ccaAct) );
        }
}

// onDAPStatusInfoError
void Midw_smartphoneint_fiAdapterComponent::onDAPStatusInfoError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DAPStatusInfoError >& corfoError){
    LOG_INFO ("onDAPStatusInfoError...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoError->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        switch (corfoError->getField ()){
        case ::midw_smartphoneint_fi::DAPStatusInfoError::E_CCA_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDAPStatusInfoRegisterError \
            (corfoError->getCcaErrorCode (), actRegPtr->second);
        break;
        case ::midw_smartphoneint_fi::DAPStatusInfoError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDAPStatusInfoRegisterError \
            (corfoError->getSystemErrorCode (), actRegPtr->second);
        break;
        default:
            LOG_FATAL ("Invalid error field");
        }
        u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// onDAPStatusInfoStatus
void Midw_smartphoneint_fiAdapterComponent::onDAPStatusInfoStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< DAPStatusInfoStatus >& corfoUpdate){
    LOG_INFO ("onDAPStatusInfoStatus...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoUpdate->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDAPStatusInfoPropertyUpdate \
        (corfoUpdate->getDeviceHandle (), corfoUpdate->getDeviceConnectionType (), corfoUpdate->getDAPStatus (), actRegPtr->second);
         u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// Callback 'DataServiceInfoCallbackIF'

// Glue function vGlueDataServiceInfoDeregister
        void Midw_smartphoneint_fiAdapterComponent::vGlueDataServiceInfoDeregister (){
        LOG_INFO ("vGlueDataServiceInfoDeregister ()...");
        map<string, act_t>::iterator actPropRegNamePtr = _actPropRegNameMap.find ("DataServiceInfo");
        if (actPropRegNamePtr != _actPropRegNameMap.end ()){
            LOG_DEBUG ("key is %s and value is %u ", actPropRegNamePtr->first.c_str(), actPropRegNamePtr->second);
            _actPropRegMap.erase (actPropRegNamePtr->second);
            _actPropRegNameMap.erase ("DataServiceInfo");
        }
        else {
        LOG_FATAL ("Name received does not match any key in _actPropRegNameMap");
        }
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            m_pomidw_smartphoneint_fiProxy->sendDataServiceInfoRelUpRegAll ();
        }
    }

// Glue function vGlueDataServiceInfoRegister
void Midw_smartphoneint_fiAdapterComponent::vGlueDataServiceInfoRegister (act_t actJson){
        LOG_INFO ("vGlueDataServiceInfoRegister ()...");
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendDataServiceInfoUpReg (*this);
            _actPropRegMap.insert (pair<act_t, act_t> (ccaAct, actJson) );
            _actPropRegNameMap.insert (pair<string, act_t> ("DataServiceInfo", ccaAct) );
        }
}

// onDataServiceInfoError
void Midw_smartphoneint_fiAdapterComponent::onDataServiceInfoError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DataServiceInfoError >& corfoError){
    LOG_INFO ("onDataServiceInfoError...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoError->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        switch (corfoError->getField ()){
        case ::midw_smartphoneint_fi::DataServiceInfoError::E_CCA_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDataServiceInfoRegisterError \
            (corfoError->getCcaErrorCode (), actRegPtr->second);
        break;
        case ::midw_smartphoneint_fi::DataServiceInfoError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDataServiceInfoRegisterError \
            (corfoError->getSystemErrorCode (), actRegPtr->second);
        break;
        default:
            LOG_FATAL ("Invalid error field");
        }
        u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// onDataServiceInfoStatus
void Midw_smartphoneint_fiAdapterComponent::onDataServiceInfoStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< DataServiceInfoStatus >& corfoUpdate){
    LOG_INFO ("onDataServiceInfoStatus...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoUpdate->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDataServiceInfoPropertyUpdate \
        (corfoUpdate->getDeviceHandle (), corfoUpdate->getDeviceCategory (), corfoUpdate->getDataServiceType (), corfoUpdate->getDataServiceInfoType (), actRegPtr->second);
         u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// Callback 'DeviceAudioContextCallbackIF'

// Glue function vGlueDeviceAudioContextDeregister
        void Midw_smartphoneint_fiAdapterComponent::vGlueDeviceAudioContextDeregister (){
        LOG_INFO ("vGlueDeviceAudioContextDeregister ()...");
        map<string, act_t>::iterator actPropRegNamePtr = _actPropRegNameMap.find ("DeviceAudioContext");
        if (actPropRegNamePtr != _actPropRegNameMap.end ()){
            LOG_DEBUG ("key is %s and value is %u ", actPropRegNamePtr->first.c_str(), actPropRegNamePtr->second);
            _actPropRegMap.erase (actPropRegNamePtr->second);
            _actPropRegNameMap.erase ("DeviceAudioContext");
        }
        else {
        LOG_FATAL ("Name received does not match any key in _actPropRegNameMap");
        }
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            m_pomidw_smartphoneint_fiProxy->sendDeviceAudioContextRelUpRegAll ();
        }
    }

// Glue function vGlueDeviceAudioContextRegister
void Midw_smartphoneint_fiAdapterComponent::vGlueDeviceAudioContextRegister (act_t actJson){
        LOG_INFO ("vGlueDeviceAudioContextRegister ()...");
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendDeviceAudioContextUpReg (*this);
            _actPropRegMap.insert (pair<act_t, act_t> (ccaAct, actJson) );
            _actPropRegNameMap.insert (pair<string, act_t> ("DeviceAudioContext", ccaAct) );
        }
}

// onDeviceAudioContextError
void Midw_smartphoneint_fiAdapterComponent::onDeviceAudioContextError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceAudioContextError >& corfoError){
    LOG_INFO ("onDeviceAudioContextError...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoError->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        switch (corfoError->getField ()){
        case ::midw_smartphoneint_fi::DeviceAudioContextError::E_CCA_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDeviceAudioContextRegisterError \
            (corfoError->getCcaErrorCode (), actRegPtr->second);
        break;
        case ::midw_smartphoneint_fi::DeviceAudioContextError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDeviceAudioContextRegisterError \
            (corfoError->getSystemErrorCode (), actRegPtr->second);
        break;
        default:
            LOG_FATAL ("Invalid error field");
        }
        u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// onDeviceAudioContextStatus
void Midw_smartphoneint_fiAdapterComponent::onDeviceAudioContextStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< DeviceAudioContextStatus >& corfoUpdate){
    LOG_INFO ("onDeviceAudioContextStatus...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoUpdate->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDeviceAudioContextPropertyUpdate \
        (corfoUpdate->getDeviceHandle (), corfoUpdate->getAudioFlag (), corfoUpdate->getAudioContext (), actRegPtr->second);
         u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// Callback 'DeviceDisplayContextCallbackIF'

// Glue function vGlueDeviceDisplayContextDeregister
        void Midw_smartphoneint_fiAdapterComponent::vGlueDeviceDisplayContextDeregister (){
        LOG_INFO ("vGlueDeviceDisplayContextDeregister ()...");
        map<string, act_t>::iterator actPropRegNamePtr = _actPropRegNameMap.find ("DeviceDisplayContext");
        if (actPropRegNamePtr != _actPropRegNameMap.end ()){
            LOG_DEBUG ("key is %s and value is %u ", actPropRegNamePtr->first.c_str(), actPropRegNamePtr->second);
            _actPropRegMap.erase (actPropRegNamePtr->second);
            _actPropRegNameMap.erase ("DeviceDisplayContext");
        }
        else {
        LOG_FATAL ("Name received does not match any key in _actPropRegNameMap");
        }
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            m_pomidw_smartphoneint_fiProxy->sendDeviceDisplayContextRelUpRegAll ();
        }
    }

// Glue function vGlueDeviceDisplayContextRegister
void Midw_smartphoneint_fiAdapterComponent::vGlueDeviceDisplayContextRegister (act_t actJson){
        LOG_INFO ("vGlueDeviceDisplayContextRegister ()...");
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendDeviceDisplayContextUpReg (*this);
            _actPropRegMap.insert (pair<act_t, act_t> (ccaAct, actJson) );
            _actPropRegNameMap.insert (pair<string, act_t> ("DeviceDisplayContext", ccaAct) );
        }
}

// onDeviceDisplayContextError
void Midw_smartphoneint_fiAdapterComponent::onDeviceDisplayContextError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceDisplayContextError >& corfoError){
    LOG_INFO ("onDeviceDisplayContextError...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoError->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        switch (corfoError->getField ()){
        case ::midw_smartphoneint_fi::DeviceDisplayContextError::E_CCA_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDeviceDisplayContextRegisterError \
            (corfoError->getCcaErrorCode (), actRegPtr->second);
        break;
        case ::midw_smartphoneint_fi::DeviceDisplayContextError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDeviceDisplayContextRegisterError \
            (corfoError->getSystemErrorCode (), actRegPtr->second);
        break;
        default:
            LOG_FATAL ("Invalid error field");
        }
        u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// onDeviceDisplayContextStatus
void Midw_smartphoneint_fiAdapterComponent::onDeviceDisplayContextStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< DeviceDisplayContextStatus >& corfoUpdate){
    LOG_INFO ("onDeviceDisplayContextStatus...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoUpdate->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDeviceDisplayContextPropertyUpdate \
        (corfoUpdate->getDeviceHandle (), corfoUpdate->getDisplayFlag (), corfoUpdate->getDisplayContext (), actRegPtr->second);
         u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// Callback 'DeviceStatusInfoCallbackIF'

// Glue function vGlueDeviceStatusInfoDeregister
        void Midw_smartphoneint_fiAdapterComponent::vGlueDeviceStatusInfoDeregister (){
        LOG_INFO ("vGlueDeviceStatusInfoDeregister ()...");
        map<string, act_t>::iterator actPropRegNamePtr = _actPropRegNameMap.find ("DeviceStatusInfo");
        if (actPropRegNamePtr != _actPropRegNameMap.end ()){
            LOG_DEBUG ("key is %s and value is %u ", actPropRegNamePtr->first.c_str(), actPropRegNamePtr->second);
            _actPropRegMap.erase (actPropRegNamePtr->second);
            _actPropRegNameMap.erase ("DeviceStatusInfo");
        }
        else {
        LOG_FATAL ("Name received does not match any key in _actPropRegNameMap");
        }
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            m_pomidw_smartphoneint_fiProxy->sendDeviceStatusInfoRelUpRegAll ();
        }
    }

// Glue function vGlueDeviceStatusInfoRegister
void Midw_smartphoneint_fiAdapterComponent::vGlueDeviceStatusInfoRegister (act_t actJson){
        LOG_INFO ("vGlueDeviceStatusInfoRegister ()...");
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendDeviceStatusInfoUpReg (*this);
            _actPropRegMap.insert (pair<act_t, act_t> (ccaAct, actJson) );
            _actPropRegNameMap.insert (pair<string, act_t> ("DeviceStatusInfo", ccaAct) );
        }
}

// onDeviceStatusInfoError
void Midw_smartphoneint_fiAdapterComponent::onDeviceStatusInfoError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceStatusInfoError >& corfoError){
    LOG_INFO ("onDeviceStatusInfoError...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoError->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        switch (corfoError->getField ()){
        case ::midw_smartphoneint_fi::DeviceStatusInfoError::E_CCA_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDeviceStatusInfoRegisterError \
            (corfoError->getCcaErrorCode (), actRegPtr->second);
        break;
        case ::midw_smartphoneint_fi::DeviceStatusInfoError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDeviceStatusInfoRegisterError \
            (corfoError->getSystemErrorCode (), actRegPtr->second);
        break;
        default:
            LOG_FATAL ("Invalid error field");
        }
        u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// onDeviceStatusInfoStatus
void Midw_smartphoneint_fiAdapterComponent::onDeviceStatusInfoStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< DeviceStatusInfoStatus >& corfoUpdate){
    LOG_INFO ("onDeviceStatusInfoStatus...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoUpdate->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDeviceStatusInfoPropertyUpdate \
        (corfoUpdate->getDeviceHandle (), corfoUpdate->getDeviceConnectionType (), corfoUpdate->getDeviceStatus (), actRegPtr->second);
         u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// Callback 'DiPOAppStatusInfoCallbackIF'

// Glue function vGlueDiPOAppStatusInfoDeregister
        void Midw_smartphoneint_fiAdapterComponent::vGlueDiPOAppStatusInfoDeregister (){
        LOG_INFO ("vGlueDiPOAppStatusInfoDeregister ()...");
        map<string, act_t>::iterator actPropRegNamePtr = _actPropRegNameMap.find ("DiPOAppStatusInfo");
        if (actPropRegNamePtr != _actPropRegNameMap.end ()){
            LOG_DEBUG ("key is %s and value is %u ", actPropRegNamePtr->first.c_str(), actPropRegNamePtr->second);
            _actPropRegMap.erase (actPropRegNamePtr->second);
            _actPropRegNameMap.erase ("DiPOAppStatusInfo");
        }
        else {
        LOG_FATAL ("Name received does not match any key in _actPropRegNameMap");
        }
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            m_pomidw_smartphoneint_fiProxy->sendDiPOAppStatusInfoRelUpRegAll ();
        }
    }

// Glue function vGlueDiPOAppStatusInfoRegister
void Midw_smartphoneint_fiAdapterComponent::vGlueDiPOAppStatusInfoRegister (act_t actJson){
        LOG_INFO ("vGlueDiPOAppStatusInfoRegister ()...");
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendDiPOAppStatusInfoUpReg (*this);
            _actPropRegMap.insert (pair<act_t, act_t> (ccaAct, actJson) );
            _actPropRegNameMap.insert (pair<string, act_t> ("DiPOAppStatusInfo", ccaAct) );
        }
}

// onDiPOAppStatusInfoError
void Midw_smartphoneint_fiAdapterComponent::onDiPOAppStatusInfoError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DiPOAppStatusInfoError >& corfoError){
    LOG_INFO ("onDiPOAppStatusInfoError...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoError->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        switch (corfoError->getField ()){
        case ::midw_smartphoneint_fi::DiPOAppStatusInfoError::E_CCA_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDiPOAppStatusInfoRegisterError \
            (corfoError->getCcaErrorCode (), actRegPtr->second);
        break;
        case ::midw_smartphoneint_fi::DiPOAppStatusInfoError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDiPOAppStatusInfoRegisterError \
            (corfoError->getSystemErrorCode (), actRegPtr->second);
        break;
        default:
            LOG_FATAL ("Invalid error field");
        }
        u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// onDiPOAppStatusInfoStatus
void Midw_smartphoneint_fiAdapterComponent::onDiPOAppStatusInfoStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< DiPOAppStatusInfoStatus >& corfoUpdate){
    LOG_INFO ("onDiPOAppStatusInfoStatus...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoUpdate->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDiPOAppStatusInfoPropertyUpdate \
        (corfoUpdate->getAppStateSpeech (), corfoUpdate->getAppStatePhone (), corfoUpdate->getAppStateNavigation (), actRegPtr->second);
         u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// Callback 'DiPORoleSwitchRequiredCallbackIF'

// Glue function vGlueDiPORoleSwitchRequiredRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueDiPORoleSwitchRequiredRequest (uint8 u8DeviceTag,act_t actJson){
    LOG_INFO ("vGlueDiPORoleSwitchRequiredRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendDiPORoleSwitchRequiredStart (*this,u8DeviceTag);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onDiPORoleSwitchRequiredError
void Midw_smartphoneint_fiAdapterComponent::onDiPORoleSwitchRequiredError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DiPORoleSwitchRequiredError >& corfoError){
LOG_INFO ("onDiPORoleSwitchRequiredError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::DiPORoleSwitchRequiredError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDiPORoleSwitchRequiredError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::DiPORoleSwitchRequiredError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDiPORoleSwitchRequiredError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onDiPORoleSwitchRequiredResult
void Midw_smartphoneint_fiAdapterComponent::onDiPORoleSwitchRequiredResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< DiPORoleSwitchRequiredResult >& corfoResult){
    LOG_INFO ("onDiPORoleSwitchRequiredResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueDiPORoleSwitchRequiredResponse \
        (corfoResult->getU8DeviceTag (), corfoResult->getE8DiPOSwitchReqResponse (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'GetAppIconDataCallbackIF'

// Glue function vGlueGetAppIconDataRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueGetAppIconDataRequest (string AppIconURL,act_t actJson){
    LOG_INFO ("vGlueGetAppIconDataRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendGetAppIconDataStart (*this,AppIconURL);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onGetAppIconDataError
void Midw_smartphoneint_fiAdapterComponent::onGetAppIconDataError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetAppIconDataError >& corfoError){
LOG_INFO ("onGetAppIconDataError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::GetAppIconDataError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueGetAppIconDataError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::GetAppIconDataError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueGetAppIconDataError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onGetAppIconDataResult
void Midw_smartphoneint_fiAdapterComponent::onGetAppIconDataResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< GetAppIconDataResult >& corfoResult){
    LOG_INFO ("onGetAppIconDataResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueGetAppIconDataResponse \
        (corfoResult->getIconMimeType (), corfoResult->getAppIconData (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'GetAppListCallbackIF'

// Glue function vGlueGetAppListRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueGetAppListRequest (uint32 DeviceHandle,act_t actJson){
    LOG_INFO ("vGlueGetAppListRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendGetAppListStart (*this,DeviceHandle);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onGetAppListError
void Midw_smartphoneint_fiAdapterComponent::onGetAppListError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetAppListError >& corfoError){
LOG_INFO ("onGetAppListError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::GetAppListError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueGetAppListError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::GetAppListError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueGetAppListError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onGetAppListResult
void Midw_smartphoneint_fiAdapterComponent::onGetAppListResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< GetAppListResult >& corfoResult){
    LOG_INFO ("onGetAppListResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueGetAppListResponse \
        (corfoResult->getDeviceHandle (), corfoResult->getNumAppDetailsList (), corfoResult->getAppDetailsList (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'GetDeviceInfoListCallbackIF'

// Glue function vGlueGetDeviceInfoListRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueGetDeviceInfoListRequest (act_t actJson){
    LOG_INFO ("vGlueGetDeviceInfoListRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendGetDeviceInfoListStart (*this);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onGetDeviceInfoListError
void Midw_smartphoneint_fiAdapterComponent::onGetDeviceInfoListError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetDeviceInfoListError >& corfoError){
LOG_INFO ("onGetDeviceInfoListError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::GetDeviceInfoListError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueGetDeviceInfoListError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::GetDeviceInfoListError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueGetDeviceInfoListError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onGetDeviceInfoListResult
void Midw_smartphoneint_fiAdapterComponent::onGetDeviceInfoListResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< GetDeviceInfoListResult >& corfoResult){
    LOG_INFO ("onGetDeviceInfoListResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueGetDeviceInfoListResponse \
        (corfoResult->getNumDevices (), corfoResult->getDeviceInfoList (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'GetDeviceUsagePreferenceCallbackIF'

// Glue function vGlueGetDeviceUsagePreferenceRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueGetDeviceUsagePreferenceRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_DeviceCategory& DeviceCategory,act_t actJson){
    LOG_INFO ("vGlueGetDeviceUsagePreferenceRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendGetDeviceUsagePreferenceStart (*this,DeviceHandle, DeviceCategory);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onGetDeviceUsagePreferenceError
void Midw_smartphoneint_fiAdapterComponent::onGetDeviceUsagePreferenceError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetDeviceUsagePreferenceError >& corfoError){
LOG_INFO ("onGetDeviceUsagePreferenceError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::GetDeviceUsagePreferenceError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueGetDeviceUsagePreferenceError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::GetDeviceUsagePreferenceError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueGetDeviceUsagePreferenceError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onGetDeviceUsagePreferenceResult
void Midw_smartphoneint_fiAdapterComponent::onGetDeviceUsagePreferenceResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< GetDeviceUsagePreferenceResult >& corfoResult){
    LOG_INFO ("onGetDeviceUsagePreferenceResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueGetDeviceUsagePreferenceResponse \
        (corfoResult->getDeviceHandle (), corfoResult->getDeviceCategory (), corfoResult->getEnabledInfo (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'GetVideoSettingsCallbackIF'

// Glue function vGlueGetVideoSettingsRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueGetVideoSettingsRequest (uint32 DeviceHandle,act_t actJson){
    LOG_INFO ("vGlueGetVideoSettingsRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendGetVideoSettingsStart (*this,DeviceHandle);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onGetVideoSettingsError
void Midw_smartphoneint_fiAdapterComponent::onGetVideoSettingsError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetVideoSettingsError >& corfoError){
LOG_INFO ("onGetVideoSettingsError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::GetVideoSettingsError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueGetVideoSettingsError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::GetVideoSettingsError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueGetVideoSettingsError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onGetVideoSettingsResult
void Midw_smartphoneint_fiAdapterComponent::onGetVideoSettingsResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< GetVideoSettingsResult >& corfoResult){
    LOG_INFO ("onGetVideoSettingsResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueGetVideoSettingsResponse \
        (corfoResult->getDeviceHandle (), corfoResult->getVideoAttributes (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'InvokeBluetoothDeviceActionCallbackIF'

// Glue function vGlueInvokeBluetoothDeviceActionRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueInvokeBluetoothDeviceActionRequest (uint32 BluetoothDeviceHandle, uint32 ProjectionDeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_BTChangeInfo& BTChangeInfo,act_t actJson){
    LOG_INFO ("vGlueInvokeBluetoothDeviceActionRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendInvokeBluetoothDeviceActionStart (BluetoothDeviceHandle, ProjectionDeviceHandle, BTChangeInfo);
    }
}

// Callback 'InvokeNotificationActionCallbackIF'

// Glue function vGlueInvokeNotificationActionRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueInvokeNotificationActionRequest (uint32 DeviceHandle, uint32 AppHandle, uint16 NotificationID, uint16 NotificationActionID,act_t actJson){
    LOG_INFO ("vGlueInvokeNotificationActionRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendInvokeNotificationActionStart (*this,DeviceHandle, AppHandle, NotificationID, NotificationActionID);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onInvokeNotificationActionError
void Midw_smartphoneint_fiAdapterComponent::onInvokeNotificationActionError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::InvokeNotificationActionError >& corfoError){
LOG_INFO ("onInvokeNotificationActionError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::InvokeNotificationActionError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueInvokeNotificationActionError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::InvokeNotificationActionError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueInvokeNotificationActionError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onInvokeNotificationActionResult
void Midw_smartphoneint_fiAdapterComponent::onInvokeNotificationActionResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< InvokeNotificationActionResult >& corfoResult){
    LOG_INFO ("onInvokeNotificationActionResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueInvokeNotificationActionResponse \
        (corfoResult->getResponseCode (), corfoResult->getErrorCode (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'LaunchAppCallbackIF'

// Glue function vGlueLaunchAppRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueLaunchAppRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_DeviceCategory& DeviceCategory, uint32 AppHandle, const ::midw_smartphoneint_fi_types::T_e8_DiPOAppType& DiPOAppType, string TelephoneNumber, const ::midw_smartphoneint_fi_types::T_e8_EcnrSetting& EcnrSetting,act_t actJson){
    LOG_INFO ("vGlueLaunchAppRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendLaunchAppStart (*this,DeviceHandle, DeviceCategory, AppHandle, DiPOAppType, TelephoneNumber, EcnrSetting);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onLaunchAppError
void Midw_smartphoneint_fiAdapterComponent::onLaunchAppError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::LaunchAppError >& corfoError){
LOG_INFO ("onLaunchAppError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::LaunchAppError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueLaunchAppError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::LaunchAppError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueLaunchAppError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onLaunchAppResult
void Midw_smartphoneint_fiAdapterComponent::onLaunchAppResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< LaunchAppResult >& corfoResult){
    LOG_INFO ("onLaunchAppResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueLaunchAppResponse \
        (corfoResult->getDeviceHandle (), corfoResult->getAppHandle (), corfoResult->getDiPOAppType (), corfoResult->getResponseCode (), corfoResult->getErrorCode (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'NotificationInfoCallbackIF'

// Glue function vGlueNotificationInfoDeregister
        void Midw_smartphoneint_fiAdapterComponent::vGlueNotificationInfoDeregister (){
        LOG_INFO ("vGlueNotificationInfoDeregister ()...");
        map<string, act_t>::iterator actPropRegNamePtr = _actPropRegNameMap.find ("NotificationInfo");
        if (actPropRegNamePtr != _actPropRegNameMap.end ()){
            LOG_DEBUG ("key is %s and value is %u ", actPropRegNamePtr->first.c_str(), actPropRegNamePtr->second);
            _actPropRegMap.erase (actPropRegNamePtr->second);
            _actPropRegNameMap.erase ("NotificationInfo");
        }
        else {
        LOG_FATAL ("Name received does not match any key in _actPropRegNameMap");
        }
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            m_pomidw_smartphoneint_fiProxy->sendNotificationInfoRelUpRegAll ();
        }
    }

// Glue function vGlueNotificationInfoRegister
void Midw_smartphoneint_fiAdapterComponent::vGlueNotificationInfoRegister (act_t actJson){
        LOG_INFO ("vGlueNotificationInfoRegister ()...");
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendNotificationInfoUpReg (*this);
            _actPropRegMap.insert (pair<act_t, act_t> (ccaAct, actJson) );
            _actPropRegNameMap.insert (pair<string, act_t> ("NotificationInfo", ccaAct) );
        }
}

// onNotificationInfoError
void Midw_smartphoneint_fiAdapterComponent::onNotificationInfoError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::NotificationInfoError >& corfoError){
    LOG_INFO ("onNotificationInfoError...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoError->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        switch (corfoError->getField ()){
        case ::midw_smartphoneint_fi::NotificationInfoError::E_CCA_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueNotificationInfoRegisterError \
            (corfoError->getCcaErrorCode (), actRegPtr->second);
        break;
        case ::midw_smartphoneint_fi::NotificationInfoError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueNotificationInfoRegisterError \
            (corfoError->getSystemErrorCode (), actRegPtr->second);
        break;
        default:
            LOG_FATAL ("Invalid error field");
        }
        u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// onNotificationInfoStatus
void Midw_smartphoneint_fiAdapterComponent::onNotificationInfoStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< NotificationInfoStatus >& corfoUpdate){
    LOG_INFO ("onNotificationInfoStatus...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoUpdate->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueNotificationInfoPropertyUpdate \
        (corfoUpdate->getDeviceHandle (), corfoUpdate->getAppHandle (), corfoUpdate->getNotificationData (), actRegPtr->second);
         u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// Callback 'SelectDeviceCallbackIF'

// Glue function vGlueSelectDeviceRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueSelectDeviceRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionType& DeviceConnectionType, const ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionReq& DeviceConnectionReq, const ::midw_smartphoneint_fi_types::T_e8_EnabledInfo& DAPUsage, const ::midw_smartphoneint_fi_types::T_e8_EnabledInfo& CDBUsage,act_t actJson){
    LOG_INFO ("vGlueSelectDeviceRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSelectDeviceStart (*this,DeviceHandle, DeviceConnectionType, DeviceConnectionReq, DAPUsage, CDBUsage);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onSelectDeviceError
void Midw_smartphoneint_fiAdapterComponent::onSelectDeviceError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SelectDeviceError >& corfoError){
LOG_INFO ("onSelectDeviceError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::SelectDeviceError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSelectDeviceError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::SelectDeviceError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSelectDeviceError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onSelectDeviceResult
void Midw_smartphoneint_fiAdapterComponent::onSelectDeviceResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SelectDeviceResult >& corfoResult){
    LOG_INFO ("onSelectDeviceResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSelectDeviceResponse \
        (corfoResult->getDeviceHandle (), corfoResult->getDeviceConnectionType (), corfoResult->getDeviceConnectionReq (), corfoResult->getResponseCode (), corfoResult->getErrorCode (), corfoResult->getBTPairingRequired (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'SendKeyEventCallbackIF'

// Glue function vGlueSendKeyEventRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueSendKeyEventRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_KeyMode& KeyMode, const ::midw_smartphoneint_fi_types::T_e32_KeyCode& KeyCode,act_t actJson){
    LOG_INFO ("vGlueSendKeyEventRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSendKeyEventStart (*this,DeviceHandle, KeyMode, KeyCode);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onSendKeyEventError
void Midw_smartphoneint_fiAdapterComponent::onSendKeyEventError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SendKeyEventError >& corfoError){
LOG_INFO ("onSendKeyEventError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::SendKeyEventError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSendKeyEventError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::SendKeyEventError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSendKeyEventError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onSendKeyEventResult
void Midw_smartphoneint_fiAdapterComponent::onSendKeyEventResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SendKeyEventResult >& corfoResult){
    LOG_INFO ("onSendKeyEventResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSendKeyEventResponse \
        (corfoResult->getResponseCode (), corfoResult->getErrorCode (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'SendTouchEventCallbackIF'

// Glue function vGlueSendTouchEventRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueSendTouchEventRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_TouchData& TouchData,act_t actJson){
    LOG_INFO ("vGlueSendTouchEventRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSendTouchEventStart (*this,DeviceHandle, TouchData);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onSendTouchEventError
void Midw_smartphoneint_fiAdapterComponent::onSendTouchEventError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SendTouchEventError >& corfoError){
LOG_INFO ("onSendTouchEventError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::SendTouchEventError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSendTouchEventError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::SendTouchEventError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSendTouchEventError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onSendTouchEventResult
void Midw_smartphoneint_fiAdapterComponent::onSendTouchEventResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SendTouchEventResult >& corfoResult){
    LOG_INFO ("onSendTouchEventResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSendTouchEventResponse \
        (corfoResult->getResponseCode (), corfoResult->getErrorCode (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'SessionStatusInfoCallbackIF'

// Glue function vGlueSessionStatusInfoDeregister
        void Midw_smartphoneint_fiAdapterComponent::vGlueSessionStatusInfoDeregister (){
        LOG_INFO ("vGlueSessionStatusInfoDeregister ()...");
        map<string, act_t>::iterator actPropRegNamePtr = _actPropRegNameMap.find ("SessionStatusInfo");
        if (actPropRegNamePtr != _actPropRegNameMap.end ()){
            LOG_DEBUG ("key is %s and value is %u ", actPropRegNamePtr->first.c_str(), actPropRegNamePtr->second);
            _actPropRegMap.erase (actPropRegNamePtr->second);
            _actPropRegNameMap.erase ("SessionStatusInfo");
        }
        else {
        LOG_FATAL ("Name received does not match any key in _actPropRegNameMap");
        }
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            m_pomidw_smartphoneint_fiProxy->sendSessionStatusInfoRelUpRegAll ();
        }
    }

// Glue function vGlueSessionStatusInfoRegister
void Midw_smartphoneint_fiAdapterComponent::vGlueSessionStatusInfoRegister (act_t actJson){
        LOG_INFO ("vGlueSessionStatusInfoRegister ()...");
        if (m_pomidw_smartphoneint_fiProxy != NULL)
        {
            act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSessionStatusInfoUpReg (*this);
            _actPropRegMap.insert (pair<act_t, act_t> (ccaAct, actJson) );
            _actPropRegNameMap.insert (pair<string, act_t> ("SessionStatusInfo", ccaAct) );
        }
}

// onSessionStatusInfoError
void Midw_smartphoneint_fiAdapterComponent::onSessionStatusInfoError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SessionStatusInfoError >& corfoError){
    LOG_INFO ("onSessionStatusInfoError...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoError->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        switch (corfoError->getField ()){
        case ::midw_smartphoneint_fi::SessionStatusInfoError::E_CCA_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSessionStatusInfoRegisterError \
            (corfoError->getCcaErrorCode (), actRegPtr->second);
        break;
        case ::midw_smartphoneint_fi::SessionStatusInfoError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSessionStatusInfoRegisterError \
            (corfoError->getSystemErrorCode (), actRegPtr->second);
        break;
        default:
            LOG_FATAL ("Invalid error field");
        }
        u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// onSessionStatusInfoStatus
void Midw_smartphoneint_fiAdapterComponent::onSessionStatusInfoStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SessionStatusInfoStatus >& corfoUpdate){
    LOG_INFO ("onSessionStatusInfoStatus...");
    uint32 u32Flag = 0;
    ActMap::iterator actRegPtr = _actPropRegMap.find (corfoUpdate->getAct ());
    if (actRegPtr != _actPropRegMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actRegPtr->first, actRegPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSessionStatusInfoPropertyUpdate \
        (corfoUpdate->getDeviceHandle (), corfoUpdate->getDeviceCategory (), corfoUpdate->getSessionStatus (), actRegPtr->second);
         u32Flag = 1;
    } 
    else {
        LOG_DEBUG ("ACT received does not match any key in _actPropRegMap");
    }
}

// Callback 'SetAppIconAttributesCallbackIF'

// Glue function vGlueSetAppIconAttributesRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueSetAppIconAttributesRequest (uint32 DeviceHandle, uint32 AppHandle, const ::midw_smartphoneint_fi_types::T_IconAttributes& IconAttributes,act_t actJson){
    LOG_INFO ("vGlueSetAppIconAttributesRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSetAppIconAttributesStart (*this,DeviceHandle, AppHandle, IconAttributes);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onSetAppIconAttributesError
void Midw_smartphoneint_fiAdapterComponent::onSetAppIconAttributesError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetAppIconAttributesError >& corfoError){
LOG_INFO ("onSetAppIconAttributesError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::SetAppIconAttributesError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetAppIconAttributesError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::SetAppIconAttributesError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetAppIconAttributesError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onSetAppIconAttributesResult
void Midw_smartphoneint_fiAdapterComponent::onSetAppIconAttributesResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SetAppIconAttributesResult >& corfoResult){
    LOG_INFO ("onSetAppIconAttributesResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetAppIconAttributesResponse \
        (corfoResult->getResponseCode (), corfoResult->getErrorCode (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'SetAudioBlockingModeCallbackIF'

// Glue function vGlueSetAudioBlockingModeRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueSetAudioBlockingModeRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_BlockingMode& BlockingMode,act_t actJson){
    LOG_INFO ("vGlueSetAudioBlockingModeRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSetAudioBlockingModeStart (*this,DeviceHandle, BlockingMode);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onSetAudioBlockingModeError
void Midw_smartphoneint_fiAdapterComponent::onSetAudioBlockingModeError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetAudioBlockingModeError >& corfoError){
LOG_INFO ("onSetAudioBlockingModeError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::SetAudioBlockingModeError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetAudioBlockingModeError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::SetAudioBlockingModeError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetAudioBlockingModeError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onSetAudioBlockingModeResult
void Midw_smartphoneint_fiAdapterComponent::onSetAudioBlockingModeResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SetAudioBlockingModeResult >& corfoResult){
    LOG_INFO ("onSetAudioBlockingModeResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetAudioBlockingModeResponse \
        (corfoResult->getDeviceHandle (), corfoResult->getResponseCode (), corfoResult->getErrorCode (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'SetClientCapabilitiesCallbackIF'

// Glue function vGlueSetClientCapabilitiesRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueSetClientCapabilitiesRequest (const ::midw_smartphoneint_fi_types::T_ClientCapabilities& ClientCapabilities,act_t actJson){
    LOG_INFO ("vGlueSetClientCapabilitiesRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSetClientCapabilitiesStart (ClientCapabilities);
    }
}

// Callback 'SetDataServiceFilterCallbackIF'

// Glue function vGlueSetDataServiceFilterRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueSetDataServiceFilterRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_DeviceCategory& DeviceCategory, const ::midw_smartphoneint_fi_types::T_e8_DataServiceType& DataServiceType, const ::midw_smartphoneint_fi_types::T_e8_EnabledInfo& EnabledInfo,act_t actJson){
    LOG_INFO ("vGlueSetDataServiceFilterRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSetDataServiceFilterStart (DeviceHandle, DeviceCategory, DataServiceType, EnabledInfo);
    }
}

// Callback 'SetDataServiceUsageCallbackIF'

// Glue function vGlueSetDataServiceUsageRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueSetDataServiceUsageRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_DeviceCategory& DeviceCategory, const ::midw_smartphoneint_fi_types::T_e8_DataServiceType& DataServiceType, const ::midw_smartphoneint_fi_types::T_e8_DataServiceUsage& DataServiceUsage,act_t actJson){
    LOG_INFO ("vGlueSetDataServiceUsageRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSetDataServiceUsageStart (*this,DeviceHandle, DeviceCategory, DataServiceType, DataServiceUsage);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onSetDataServiceUsageError
void Midw_smartphoneint_fiAdapterComponent::onSetDataServiceUsageError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetDataServiceUsageError >& corfoError){
LOG_INFO ("onSetDataServiceUsageError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::SetDataServiceUsageError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetDataServiceUsageError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::SetDataServiceUsageError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetDataServiceUsageError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onSetDataServiceUsageResult
void Midw_smartphoneint_fiAdapterComponent::onSetDataServiceUsageResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SetDataServiceUsageResult >& corfoResult){
    LOG_INFO ("onSetDataServiceUsageResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetDataServiceUsageResponse \
        (corfoResult->getResponseCode (), corfoResult->getErrorCode (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'SetDeviceUsagePreferenceCallbackIF'

// Glue function vGlueSetDeviceUsagePreferenceRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueSetDeviceUsagePreferenceRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_DeviceCategory& DeviceCategory, const ::midw_smartphoneint_fi_types::T_e8_EnabledInfo& EnabledInfo,act_t actJson){
    LOG_INFO ("vGlueSetDeviceUsagePreferenceRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSetDeviceUsagePreferenceStart (DeviceHandle, DeviceCategory, EnabledInfo);
    }
}

// Callback 'SetMLNotificationEnabledInfoCallbackIF'

// Glue function vGlueSetMLNotificationEnabledInfoRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueSetMLNotificationEnabledInfoRequest (uint32 DeviceHandle, uint16 NumNotificationEnableList, const ::std::vector< ::midw_smartphoneint_fi_types::T_NotificationEnable >& NotificationEnableList,act_t actJson){
    LOG_INFO ("vGlueSetMLNotificationEnabledInfoRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSetMLNotificationEnabledInfoStart (*this,DeviceHandle, NumNotificationEnableList, NotificationEnableList);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onSetMLNotificationEnabledInfoError
void Midw_smartphoneint_fiAdapterComponent::onSetMLNotificationEnabledInfoError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetMLNotificationEnabledInfoError >& corfoError){
LOG_INFO ("onSetMLNotificationEnabledInfoError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::SetMLNotificationEnabledInfoError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetMLNotificationEnabledInfoError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::SetMLNotificationEnabledInfoError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetMLNotificationEnabledInfoError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onSetMLNotificationEnabledInfoResult
void Midw_smartphoneint_fiAdapterComponent::onSetMLNotificationEnabledInfoResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SetMLNotificationEnabledInfoResult >& corfoResult){
    LOG_INFO ("onSetMLNotificationEnabledInfoResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetMLNotificationEnabledInfoResponse \
        (corfoResult->getDeviceHandle (), corfoResult->getResponseCode (), corfoResult->getErrorCode (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'SetOrientationModeCallbackIF'

// Glue function vGlueSetOrientationModeRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueSetOrientationModeRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_OrientationMode& OrientationMode,act_t actJson){
    LOG_INFO ("vGlueSetOrientationModeRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSetOrientationModeStart (*this,DeviceHandle, OrientationMode);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onSetOrientationModeError
void Midw_smartphoneint_fiAdapterComponent::onSetOrientationModeError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetOrientationModeError >& corfoError){
LOG_INFO ("onSetOrientationModeError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::SetOrientationModeError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetOrientationModeError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::SetOrientationModeError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetOrientationModeError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onSetOrientationModeResult
void Midw_smartphoneint_fiAdapterComponent::onSetOrientationModeResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SetOrientationModeResult >& corfoResult){
    LOG_INFO ("onSetOrientationModeResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetOrientationModeResponse \
        (corfoResult->getResponseCode (), corfoResult->getErrorCode (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'SetRegionCallbackIF'

// Glue function vGlueSetRegionRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueSetRegionRequest (const ::midw_smartphoneint_fi_types::T_e8_Region& Region,act_t actJson){
    LOG_INFO ("vGlueSetRegionRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSetRegionStart (Region);
    }
}

// Callback 'SetScreenSizeCallbackIF'

// Glue function vGlueSetScreenSizeRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueSetScreenSizeRequest (const ::midw_smartphoneint_fi_types::T_ScreenAttributes& ScreenAttributes,act_t actJson){
    LOG_INFO ("vGlueSetScreenSizeRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSetScreenSizeStart (ScreenAttributes);
    }
}

// Callback 'SetVehicleBTAddressCallbackIF'

// Glue function vGlueSetVehicleBTAddressRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueSetVehicleBTAddressRequest (string BTAddress,act_t actJson){
    LOG_INFO ("vGlueSetVehicleBTAddressRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSetVehicleBTAddressStart (BTAddress);
    }
}

// Callback 'SetVehicleConfigurationCallbackIF'

// Glue function vGlueSetVehicleConfigurationRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueSetVehicleConfigurationRequest (const ::midw_smartphoneint_fi_types::T_e8_Vehicle_Configuration& VehicleConfiguration, bool SetConfiguration,act_t actJson){
    LOG_INFO ("vGlueSetVehicleConfigurationRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSetVehicleConfigurationStart (VehicleConfiguration, SetConfiguration);
    }
}

// Callback 'SetVideoBlockingModeCallbackIF'

// Glue function vGlueSetVideoBlockingModeRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueSetVideoBlockingModeRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_BlockingMode& BlockingMode,act_t actJson){
    LOG_INFO ("vGlueSetVideoBlockingModeRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendSetVideoBlockingModeStart (*this,DeviceHandle, BlockingMode);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onSetVideoBlockingModeError
void Midw_smartphoneint_fiAdapterComponent::onSetVideoBlockingModeError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetVideoBlockingModeError >& corfoError){
LOG_INFO ("onSetVideoBlockingModeError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::SetVideoBlockingModeError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetVideoBlockingModeError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::SetVideoBlockingModeError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetVideoBlockingModeError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onSetVideoBlockingModeResult
void Midw_smartphoneint_fiAdapterComponent::onSetVideoBlockingModeResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SetVideoBlockingModeResult >& corfoResult){
    LOG_INFO ("onSetVideoBlockingModeResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueSetVideoBlockingModeResponse \
        (corfoResult->getDeviceHandle (), corfoResult->getResponseCode (), corfoResult->getErrorCode (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'TerminateAppCallbackIF'

// Glue function vGlueTerminateAppRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueTerminateAppRequest (uint32 DeviceHandle, uint32 AppHandle,act_t actJson){
    LOG_INFO ("vGlueTerminateAppRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendTerminateAppStart (*this,DeviceHandle, AppHandle);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onTerminateAppError
void Midw_smartphoneint_fiAdapterComponent::onTerminateAppError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::TerminateAppError >& corfoError){
LOG_INFO ("onTerminateAppError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::TerminateAppError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueTerminateAppError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::TerminateAppError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueTerminateAppError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onTerminateAppResult
void Midw_smartphoneint_fiAdapterComponent::onTerminateAppResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< TerminateAppResult >& corfoResult){
    LOG_INFO ("onTerminateAppResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueTerminateAppResponse \
        (corfoResult->getDeviceHandle (), corfoResult->getAppHandle (), corfoResult->getResponseCode (), corfoResult->getErrorCode (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

// Callback 'UpdateCertificateFileCallbackIF'

// Glue function vGlueUpdateCertificateFileRequest
void Midw_smartphoneint_fiAdapterComponent::vGlueUpdateCertificateFileRequest (string CertificateFilePath,act_t actJson){
    LOG_INFO ("vGlueUpdateCertificateFileRequest...");
    if (m_pomidw_smartphoneint_fiProxy != NULL)
    {
        act_t ccaAct = m_pomidw_smartphoneint_fiProxy->sendUpdateCertificateFileStart (*this,CertificateFilePath);
        _actRequestMap.insert (pair<act_t, act_t> (ccaAct, actJson));
    }
}

// onUpdateCertificateFileError
void Midw_smartphoneint_fiAdapterComponent::onUpdateCertificateFileError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::UpdateCertificateFileError >& corfoError){
LOG_INFO ("onUpdateCertificateFileError...");
ActMap::iterator actPtr = _actRequestMap.find (corfoError->getAct ());
if (actPtr != _actRequestMap.end ()){
    LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
    switch (corfoError->getField ()){
    case ::midw_smartphoneint_fi::UpdateCertificateFileError::E_CCA_ERROR_CODE:
       m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueUpdateCertificateFileError \
       (corfoError->getCcaErrorCode (), actPtr->second);
    break;
    case ::midw_smartphoneint_fi::UpdateCertificateFileError::E_SYSTEM_ERROR_CODE:
            m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueUpdateCertificateFileError \
            (corfoError->getSystemErrorCode (), actPtr->second);
    break;
    default:
        LOG_FATAL ("Invalid error field");
    }
    _actRequestMap.erase (actPtr);
} 
else {
LOG_FATAL ("ACT received does not match any key");
}
}

// onUpdateCertificateFileResult
void Midw_smartphoneint_fiAdapterComponent::onUpdateCertificateFileResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< UpdateCertificateFileResult >& corfoResult){
    LOG_INFO ("onUpdateCertificateFileResult...");
    ActMap::iterator actPtr = _actRequestMap.find (corfoResult->getAct ());
    if (actPtr != _actRequestMap.end ()){
        LOG_DEBUG ("key is %u and value is %u ", actPtr->first, actPtr->second);
        m_poMidw_smartphoneint_fiAdapterComponentStub->vGlueUpdateCertificateFileResponse \
        (corfoResult->getResponseCode (), corfoResult->getErrorCode (), actPtr->second);
        _actRequestMap.erase (actPtr);
    } else {
    LOG_FATAL ("ACT received does not match any key");
    }
}

} // namespace adapter
} // namespace midw_smartphoneint_fiComponent
