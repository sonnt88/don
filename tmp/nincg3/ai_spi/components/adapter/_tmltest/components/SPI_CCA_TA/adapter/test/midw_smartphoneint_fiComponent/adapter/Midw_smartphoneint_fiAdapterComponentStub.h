/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#ifndef MIDW_SMARTPHONEINT_FIADAPTERCOMPONENTSTUB_H
#define MIDW_SMARTPHONEINT_FIADAPTERCOMPONENTSTUB_H

#include "BaseComponent.h"
#include "Midw_smartphoneint_fiAdapterComponent.h"
#include "Midw_smartphoneint_fiServiceStub.h"
#include "asf/core/Logger.h"
#include "asf/core/Types.h"
#include "boost/ptr_container/ptr_vector.hpp"
#include "boost/shared_ptr.hpp"
#include "midw_smartphoneint_fi.h"
#include "midw_smartphoneint_fi/Midw_smartphoneint_fiService.h"
#include "midw_smartphoneint_fi/midw_smartphoneint_fiAuxiliaryTypes.h"
#include "midw_smartphoneint_fi/midw_smartphoneint_fiAuxiliaryTypesConst.h"
#include "midw_smartphoneint_fi/midw_smartphoneint_fi_typesTypes.h"
#include "midw_smartphoneint_fi/midw_smartphoneint_fi_typesTypesConst.h"
#include <string>
#include <vector>

using namespace ::midw_smartphoneint_fi::Midw_smartphoneint_fiService;
using namespace ::asf::core;
using namespace ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes;
using namespace ::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes;
using namespace std;

namespace midw_smartphoneint_fiComponent {
namespace adapter {

/**
 * The Midw_smartphoneint_fiAdapterComponentStub class extends
 * Midw_smartphoneint_fiServiceStub is the hook to implement the
 * Midw_smartphoneint_fiService. All the virtual functions in the class
 * Midw_smartphoneint_fiServiceStub are overridden here.
 */
class Midw_smartphoneint_fiAdapterComponentStub :public BaseComponent, Midw_smartphoneint_fiServiceStub {
    public:
        /**
         * ****Constructor *****
         */
        Midw_smartphoneint_fiAdapterComponentStub (Midw_smartphoneint_fiAdapterComponent* const rfoProxy);
        /**
         * ****Destructor****
         */
        virtual ~Midw_smartphoneint_fiAdapterComponentStub ();
        
        /***************Midw_smartphoneint_fiService Interface*******************/
        // request GetDeviceInfoList
        
        /**
         * This function will be called if the component receives the client
         * request "GetDeviceInfoListRequest". The implementation has to redirect
         * the the request using Glue Function vGlueSendGetDeviceInfoListRequest
         * (...) and send the response back using the function
         * sendGetDeviceInfoListResponse (...)
         *
         * <b>Documentation of 'GetDeviceInfoList'</b>:
         *
         * If the meaning of "GetDeviceInfoList" isn't clear, then there should be
         * a description here.
         *
         */
        
        void onGetDeviceInfoListRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::GetDeviceInfoListRequest >& corfoRequest);
        // Glue Function vGlueSendGetDeviceInfoListResponse ()
        /**
         * This function will redirect the response GetDeviceInfoListResponse to
         * the client. It uses sendGetDeviceInfoListResponse (...) function for
         * that
         *
         * <b>Documentation of 'GetDeviceInfoList'</b>:
         *
         * If the meaning of "GetDeviceInfoList" isn't clear, then there should be
         * a description here.
         *
         */
        
        void vGlueGetDeviceInfoListResponse (uint16 NumDevices,const ::std::vector< ::midw_smartphoneint_fi_types::T_DeviceDetails >& DeviceInfoList,act_t actJson);
        
        // Glue Function vGlueSendGetDeviceInfoListError ()
        /**
         * This function will redirect the error  GetDeviceInfoListError to the
         * client. It uses sendGetDeviceInfoListError (...) function for that
         *
         * <b>Documentation of 'GetDeviceInfoList'</b>:
         *
         * If the meaning of "GetDeviceInfoList" isn't clear, then there should be
         * a description here.
         *
         */
        
        void vGlueGetDeviceInfoListError (uint32 data, act_t actJson = 0);
        // request SelectDevice
        
        /**
         * This function will be called if the component receives the client
         * request "SelectDeviceRequest". The implementation has to redirect the
         * the request using Glue Function vGlueSendSelectDeviceRequest (...) and
         * send the response back using the function sendSelectDeviceResponse
         * (...)
         *
         * <b>Documentation of 'SelectDevice'</b>:
         *
         * If the meaning of "SelectDevice" isn't clear, then there should be a
         * description here.
         *
         */
        
        void onSelectDeviceRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SelectDeviceRequest >& corfoRequest);
        // Glue Function vGlueSendSelectDeviceResponse ()
        /**
         * This function will redirect the response SelectDeviceResponse to the
         * client. It uses sendSelectDeviceResponse (...) function for that
         *
         * <b>Documentation of 'SelectDevice'</b>:
         *
         * If the meaning of "SelectDevice" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueSelectDeviceResponse (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionType& DeviceConnectionType,const ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionReq& DeviceConnectionReq,const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode,bool BTPairingRequired,act_t actJson);
        
        // Glue Function vGlueSendSelectDeviceError ()
        /**
         * This function will redirect the error  SelectDeviceError to the client.
         * It uses sendSelectDeviceError (...) function for that
         *
         * <b>Documentation of 'SelectDevice'</b>:
         *
         * If the meaning of "SelectDevice" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueSelectDeviceError (uint32 data, act_t actJson = 0);
        // request LaunchApp
        
        /**
         * This function will be called if the component receives the client
         * request "LaunchAppRequest". The implementation has to redirect the the
         * request using Glue Function vGlueSendLaunchAppRequest (...) and send
         * the response back using the function sendLaunchAppResponse (...)
         *
         * <b>Documentation of 'LaunchApp'</b>:
         *
         * If the meaning of "LaunchApp" isn't clear, then there should be a
         * description here.
         *
         */
        
        void onLaunchAppRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::LaunchAppRequest >& corfoRequest);
        // Glue Function vGlueSendLaunchAppResponse ()
        /**
         * This function will redirect the response LaunchAppResponse to the
         * client. It uses sendLaunchAppResponse (...) function for that
         *
         * <b>Documentation of 'LaunchApp'</b>:
         *
         * If the meaning of "LaunchApp" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueLaunchAppResponse (uint32 DeviceHandle,uint32 AppHandle,const ::midw_smartphoneint_fi_types::T_e8_DiPOAppType& DiPOAppType,const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode,act_t actJson);
        
        // Glue Function vGlueSendLaunchAppError ()
        /**
         * This function will redirect the error  LaunchAppError to the client. It
         * uses sendLaunchAppError (...) function for that
         *
         * <b>Documentation of 'LaunchApp'</b>:
         *
         * If the meaning of "LaunchApp" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueLaunchAppError (uint32 data, act_t actJson = 0);
        // request TerminateApp
        
        /**
         * This function will be called if the component receives the client
         * request "TerminateAppRequest". The implementation has to redirect the
         * the request using Glue Function vGlueSendTerminateAppRequest (...) and
         * send the response back using the function sendTerminateAppResponse
         * (...)
         *
         * <b>Documentation of 'TerminateApp'</b>:
         *
         * If the meaning of "TerminateApp" isn't clear, then there should be a
         * description here.
         *
         */
        
        void onTerminateAppRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::TerminateAppRequest >& corfoRequest);
        // Glue Function vGlueSendTerminateAppResponse ()
        /**
         * This function will redirect the response TerminateAppResponse to the
         * client. It uses sendTerminateAppResponse (...) function for that
         *
         * <b>Documentation of 'TerminateApp'</b>:
         *
         * If the meaning of "TerminateApp" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueTerminateAppResponse (uint32 DeviceHandle,uint32 AppHandle,const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode,act_t actJson);
        
        // Glue Function vGlueSendTerminateAppError ()
        /**
         * This function will redirect the error  TerminateAppError to the client.
         * It uses sendTerminateAppError (...) function for that
         *
         * <b>Documentation of 'TerminateApp'</b>:
         *
         * If the meaning of "TerminateApp" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueTerminateAppError (uint32 data, act_t actJson = 0);
        // request GetAppList
        
        /**
         * This function will be called if the component receives the client
         * request "GetAppListRequest". The implementation has to redirect the the
         * request using Glue Function vGlueSendGetAppListRequest (...) and send
         * the response back using the function sendGetAppListResponse (...)
         *
         * <b>Documentation of 'GetAppList'</b>:
         *
         * If the meaning of "GetAppList" isn't clear, then there should be a
         * description here.
         *
         */
        
        void onGetAppListRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::GetAppListRequest >& corfoRequest);
        // Glue Function vGlueSendGetAppListResponse ()
        /**
         * This function will redirect the response GetAppListResponse to the
         * client. It uses sendGetAppListResponse (...) function for that
         *
         * <b>Documentation of 'GetAppList'</b>:
         *
         * If the meaning of "GetAppList" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueGetAppListResponse (uint32 DeviceHandle,uint16 NumAppDetailsList,const ::std::vector< ::midw_smartphoneint_fi_types::T_AppDetails >& AppDetailsList,act_t actJson);
        
        // Glue Function vGlueSendGetAppListError ()
        /**
         * This function will redirect the error  GetAppListError to the client.
         * It uses sendGetAppListError (...) function for that
         *
         * <b>Documentation of 'GetAppList'</b>:
         *
         * If the meaning of "GetAppList" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueGetAppListError (uint32 data, act_t actJson = 0);
        // request GetAppIconData
        
        /**
         * This function will be called if the component receives the client
         * request "GetAppIconDataRequest". The implementation has to redirect the
         * the request using Glue Function vGlueSendGetAppIconDataRequest (...)
         * and send the response back using the function
         * sendGetAppIconDataResponse (...)
         *
         * <b>Documentation of 'GetAppIconData'</b>:
         *
         * If the meaning of "GetAppIconData" isn't clear, then there should be a
         * description here.
         *
         */
        
        void onGetAppIconDataRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::GetAppIconDataRequest >& corfoRequest);
        // Glue Function vGlueSendGetAppIconDataResponse ()
        /**
         * This function will redirect the response GetAppIconDataResponse to the
         * client. It uses sendGetAppIconDataResponse (...) function for that
         *
         * <b>Documentation of 'GetAppIconData'</b>:
         *
         * If the meaning of "GetAppIconData" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueGetAppIconDataResponse (::std::string IconMimeType,const ::std::vector< uint8 >& AppIconData,act_t actJson);
        
        // Glue Function vGlueSendGetAppIconDataError ()
        /**
         * This function will redirect the error  GetAppIconDataError to the
         * client. It uses sendGetAppIconDataError (...) function for that
         *
         * <b>Documentation of 'GetAppIconData'</b>:
         *
         * If the meaning of "GetAppIconData" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueGetAppIconDataError (uint32 data, act_t actJson = 0);
        // request SetAppIconAttributes
        
        /**
         * This function will be called if the component receives the client
         * request "SetAppIconAttributesRequest". The implementation has to
         * redirect the the request using Glue Function
         * vGlueSendSetAppIconAttributesRequest (...) and send the response back
         * using the function sendSetAppIconAttributesResponse (...)
         *
         * <b>Documentation of 'SetAppIconAttributes'</b>:
         *
         * If the meaning of "SetAppIconAttributes" isn't clear, then there should
         * be a description here.
         *
         */
        
        void onSetAppIconAttributesRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetAppIconAttributesRequest >& corfoRequest);
        // Glue Function vGlueSendSetAppIconAttributesResponse ()
        /**
         * This function will redirect the response SetAppIconAttributesResponse
         * to the client. It uses sendSetAppIconAttributesResponse (...) function
         * for that
         *
         * <b>Documentation of 'SetAppIconAttributes'</b>:
         *
         * If the meaning of "SetAppIconAttributes" isn't clear, then there should
         * be a description here.
         *
         */
        
        void vGlueSetAppIconAttributesResponse (const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode,act_t actJson);
        
        // Glue Function vGlueSendSetAppIconAttributesError ()
        /**
         * This function will redirect the error  SetAppIconAttributesError to the
         * client. It uses sendSetAppIconAttributesError (...) function for that
         *
         * <b>Documentation of 'SetAppIconAttributes'</b>:
         *
         * If the meaning of "SetAppIconAttributes" isn't clear, then there should
         * be a description here.
         *
         */
        
        void vGlueSetAppIconAttributesError (uint32 data, act_t actJson = 0);
        // request SetDeviceUsagePreference
        
        /**
         * This function will be called if the component receives the client
         * request "SetDeviceUsagePreferenceRequest". The implementation has to
         * redirect the the request using Glue Function
         * vGlueSendSetDeviceUsagePreferenceRequest (...) and send the response
         * back using the function sendSetDeviceUsagePreferenceResponse (...)
         *
         * <b>Documentation of 'SetDeviceUsagePreference'</b>:
         *
         * If the meaning of "SetDeviceUsagePreference" isn't clear, then there
         * should be a description here.
         *
         */
        
        void onSetDeviceUsagePreferenceRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetDeviceUsagePreferenceRequest >& corfoRequest);
        // Glue Function vGlueSendSetDeviceUsagePreferenceResponse ()
        /**
         * This function will redirect the response
         * SetDeviceUsagePreferenceResponse to the client. It uses
         * sendSetDeviceUsagePreferenceResponse (...) function for that
         *
         * <b>Documentation of 'SetDeviceUsagePreference'</b>:
         *
         * If the meaning of "SetDeviceUsagePreference" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueSetDeviceUsagePreferenceResponse (act_t actJson);
        
        // Glue Function vGlueSendSetDeviceUsagePreferenceError ()
        /**
         * This function will redirect the error  SetDeviceUsagePreferenceError to
         * the client. It uses sendSetDeviceUsagePreferenceError (...) function
         * for that
         *
         * <b>Documentation of 'SetDeviceUsagePreference'</b>:
         *
         * If the meaning of "SetDeviceUsagePreference" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueSetDeviceUsagePreferenceError (uint32 data, act_t actJson = 0);
        // request SetMLNotificationEnabledInfo
        
        /**
         * This function will be called if the component receives the client
         * request "SetMLNotificationEnabledInfoRequest". The implementation has
         * to redirect the the request using Glue Function
         * vGlueSendSetMLNotificationEnabledInfoRequest (...) and send the
         * response back using the function
         * sendSetMLNotificationEnabledInfoResponse (...)
         *
         * <b>Documentation of 'SetMLNotificationEnabledInfo'</b>:
         *
         * If the meaning of "SetMLNotificationEnabledInfo" isn't clear, then
         * there should be a description here.
         *
         */
        
        void onSetMLNotificationEnabledInfoRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetMLNotificationEnabledInfoRequest >& corfoRequest);
        // Glue Function vGlueSendSetMLNotificationEnabledInfoResponse ()
        /**
         * This function will redirect the response
         * SetMLNotificationEnabledInfoResponse to the client. It uses
         * sendSetMLNotificationEnabledInfoResponse (...) function for that
         *
         * <b>Documentation of 'SetMLNotificationEnabledInfo'</b>:
         *
         * If the meaning of "SetMLNotificationEnabledInfo" isn't clear, then
         * there should be a description here.
         *
         */
        
        void vGlueSetMLNotificationEnabledInfoResponse (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode,act_t actJson);
        
        // Glue Function vGlueSendSetMLNotificationEnabledInfoError ()
        /**
         * This function will redirect the error
         * SetMLNotificationEnabledInfoError to the client. It uses
         * sendSetMLNotificationEnabledInfoError (...) function for that
         *
         * <b>Documentation of 'SetMLNotificationEnabledInfo'</b>:
         *
         * If the meaning of "SetMLNotificationEnabledInfo" isn't clear, then
         * there should be a description here.
         *
         */
        
        void vGlueSetMLNotificationEnabledInfoError (uint32 data, act_t actJson = 0);
        // request InvokeNotificationAction
        
        /**
         * This function will be called if the component receives the client
         * request "InvokeNotificationActionRequest". The implementation has to
         * redirect the the request using Glue Function
         * vGlueSendInvokeNotificationActionRequest (...) and send the response
         * back using the function sendInvokeNotificationActionResponse (...)
         *
         * <b>Documentation of 'InvokeNotificationAction'</b>:
         *
         * If the meaning of "InvokeNotificationAction" isn't clear, then there
         * should be a description here.
         *
         */
        
        void onInvokeNotificationActionRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::InvokeNotificationActionRequest >& corfoRequest);
        // Glue Function vGlueSendInvokeNotificationActionResponse ()
        /**
         * This function will redirect the response
         * InvokeNotificationActionResponse to the client. It uses
         * sendInvokeNotificationActionResponse (...) function for that
         *
         * <b>Documentation of 'InvokeNotificationAction'</b>:
         *
         * If the meaning of "InvokeNotificationAction" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueInvokeNotificationActionResponse (const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode,act_t actJson);
        
        // Glue Function vGlueSendInvokeNotificationActionError ()
        /**
         * This function will redirect the error  InvokeNotificationActionError to
         * the client. It uses sendInvokeNotificationActionError (...) function
         * for that
         *
         * <b>Documentation of 'InvokeNotificationAction'</b>:
         *
         * If the meaning of "InvokeNotificationAction" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueInvokeNotificationActionError (uint32 data, act_t actJson = 0);
        // request GetVideoSettings
        
        /**
         * This function will be called if the component receives the client
         * request "GetVideoSettingsRequest". The implementation has to redirect
         * the the request using Glue Function vGlueSendGetVideoSettingsRequest
         * (...) and send the response back using the function
         * sendGetVideoSettingsResponse (...)
         *
         * <b>Documentation of 'GetVideoSettings'</b>:
         *
         * If the meaning of "GetVideoSettings" isn't clear, then there should be
         * a description here.
         *
         */
        
        void onGetVideoSettingsRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::GetVideoSettingsRequest >& corfoRequest);
        // Glue Function vGlueSendGetVideoSettingsResponse ()
        /**
         * This function will redirect the response GetVideoSettingsResponse to
         * the client. It uses sendGetVideoSettingsResponse (...) function for
         * that
         *
         * <b>Documentation of 'GetVideoSettings'</b>:
         *
         * If the meaning of "GetVideoSettings" isn't clear, then there should be
         * a description here.
         *
         */
        
        void vGlueGetVideoSettingsResponse (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_VideoAttributes& VideoAttributes,act_t actJson);
        
        // Glue Function vGlueSendGetVideoSettingsError ()
        /**
         * This function will redirect the error  GetVideoSettingsError to the
         * client. It uses sendGetVideoSettingsError (...) function for that
         *
         * <b>Documentation of 'GetVideoSettings'</b>:
         *
         * If the meaning of "GetVideoSettings" isn't clear, then there should be
         * a description here.
         *
         */
        
        void vGlueGetVideoSettingsError (uint32 data, act_t actJson = 0);
        // request SetOrientationMode
        
        /**
         * This function will be called if the component receives the client
         * request "SetOrientationModeRequest". The implementation has to redirect
         * the the request using Glue Function vGlueSendSetOrientationModeRequest
         * (...) and send the response back using the function
         * sendSetOrientationModeResponse (...)
         *
         * <b>Documentation of 'SetOrientationMode'</b>:
         *
         * If the meaning of "SetOrientationMode" isn't clear, then there should
         * be a description here.
         *
         */
        
        void onSetOrientationModeRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetOrientationModeRequest >& corfoRequest);
        // Glue Function vGlueSendSetOrientationModeResponse ()
        /**
         * This function will redirect the response SetOrientationModeResponse to
         * the client. It uses sendSetOrientationModeResponse (...) function for
         * that
         *
         * <b>Documentation of 'SetOrientationMode'</b>:
         *
         * If the meaning of "SetOrientationMode" isn't clear, then there should
         * be a description here.
         *
         */
        
        void vGlueSetOrientationModeResponse (const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode,act_t actJson);
        
        // Glue Function vGlueSendSetOrientationModeError ()
        /**
         * This function will redirect the error  SetOrientationModeError to the
         * client. It uses sendSetOrientationModeError (...) function for that
         *
         * <b>Documentation of 'SetOrientationMode'</b>:
         *
         * If the meaning of "SetOrientationMode" isn't clear, then there should
         * be a description here.
         *
         */
        
        void vGlueSetOrientationModeError (uint32 data, act_t actJson = 0);
        // request SetScreenSize
        
        /**
         * This function will be called if the component receives the client
         * request "SetScreenSizeRequest". The implementation has to redirect the
         * the request using Glue Function vGlueSendSetScreenSizeRequest (...) and
         * send the response back using the function sendSetScreenSizeResponse
         * (...)
         *
         * <b>Documentation of 'SetScreenSize'</b>:
         *
         * If the meaning of "SetScreenSize" isn't clear, then there should be a
         * description here.
         *
         */
        
        void onSetScreenSizeRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetScreenSizeRequest >& corfoRequest);
        // Glue Function vGlueSendSetScreenSizeResponse ()
        /**
         * This function will redirect the response SetScreenSizeResponse to the
         * client. It uses sendSetScreenSizeResponse (...) function for that
         *
         * <b>Documentation of 'SetScreenSize'</b>:
         *
         * If the meaning of "SetScreenSize" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueSetScreenSizeResponse (act_t actJson);
        
        // Glue Function vGlueSendSetScreenSizeError ()
        /**
         * This function will redirect the error  SetScreenSizeError to the
         * client. It uses sendSetScreenSizeError (...) function for that
         *
         * <b>Documentation of 'SetScreenSize'</b>:
         *
         * If the meaning of "SetScreenSize" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueSetScreenSizeError (uint32 data, act_t actJson = 0);
        // request SetVideoBlockingMode
        
        /**
         * This function will be called if the component receives the client
         * request "SetVideoBlockingModeRequest". The implementation has to
         * redirect the the request using Glue Function
         * vGlueSendSetVideoBlockingModeRequest (...) and send the response back
         * using the function sendSetVideoBlockingModeResponse (...)
         *
         * <b>Documentation of 'SetVideoBlockingMode'</b>:
         *
         * If the meaning of "SetVideoBlockingMode" isn't clear, then there should
         * be a description here.
         *
         */
        
        void onSetVideoBlockingModeRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetVideoBlockingModeRequest >& corfoRequest);
        // Glue Function vGlueSendSetVideoBlockingModeResponse ()
        /**
         * This function will redirect the response SetVideoBlockingModeResponse
         * to the client. It uses sendSetVideoBlockingModeResponse (...) function
         * for that
         *
         * <b>Documentation of 'SetVideoBlockingMode'</b>:
         *
         * If the meaning of "SetVideoBlockingMode" isn't clear, then there should
         * be a description here.
         *
         */
        
        void vGlueSetVideoBlockingModeResponse (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode,act_t actJson);
        
        // Glue Function vGlueSendSetVideoBlockingModeError ()
        /**
         * This function will redirect the error  SetVideoBlockingModeError to the
         * client. It uses sendSetVideoBlockingModeError (...) function for that
         *
         * <b>Documentation of 'SetVideoBlockingMode'</b>:
         *
         * If the meaning of "SetVideoBlockingMode" isn't clear, then there should
         * be a description here.
         *
         */
        
        void vGlueSetVideoBlockingModeError (uint32 data, act_t actJson = 0);
        // request SetAudioBlockingMode
        
        /**
         * This function will be called if the component receives the client
         * request "SetAudioBlockingModeRequest". The implementation has to
         * redirect the the request using Glue Function
         * vGlueSendSetAudioBlockingModeRequest (...) and send the response back
         * using the function sendSetAudioBlockingModeResponse (...)
         *
         * <b>Documentation of 'SetAudioBlockingMode'</b>:
         *
         * If the meaning of "SetAudioBlockingMode" isn't clear, then there should
         * be a description here.
         *
         */
        
        void onSetAudioBlockingModeRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetAudioBlockingModeRequest >& corfoRequest);
        // Glue Function vGlueSendSetAudioBlockingModeResponse ()
        /**
         * This function will redirect the response SetAudioBlockingModeResponse
         * to the client. It uses sendSetAudioBlockingModeResponse (...) function
         * for that
         *
         * <b>Documentation of 'SetAudioBlockingMode'</b>:
         *
         * If the meaning of "SetAudioBlockingMode" isn't clear, then there should
         * be a description here.
         *
         */
        
        void vGlueSetAudioBlockingModeResponse (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode,act_t actJson);
        
        // Glue Function vGlueSendSetAudioBlockingModeError ()
        /**
         * This function will redirect the error  SetAudioBlockingModeError to the
         * client. It uses sendSetAudioBlockingModeError (...) function for that
         *
         * <b>Documentation of 'SetAudioBlockingMode'</b>:
         *
         * If the meaning of "SetAudioBlockingMode" isn't clear, then there should
         * be a description here.
         *
         */
        
        void vGlueSetAudioBlockingModeError (uint32 data, act_t actJson = 0);
        // request SetVehicleConfiguration
        
        /**
         * This function will be called if the component receives the client
         * request "SetVehicleConfigurationRequest". The implementation has to
         * redirect the the request using Glue Function
         * vGlueSendSetVehicleConfigurationRequest (...) and send the response
         * back using the function sendSetVehicleConfigurationResponse (...)
         *
         * <b>Documentation of 'SetVehicleConfiguration'</b>:
         *
         * If the meaning of "SetVehicleConfiguration" isn't clear, then there
         * should be a description here.
         *
         */
        
        void onSetVehicleConfigurationRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetVehicleConfigurationRequest >& corfoRequest);
        // Glue Function vGlueSendSetVehicleConfigurationResponse ()
        /**
         * This function will redirect the response
         * SetVehicleConfigurationResponse to the client. It uses
         * sendSetVehicleConfigurationResponse (...) function for that
         *
         * <b>Documentation of 'SetVehicleConfiguration'</b>:
         *
         * If the meaning of "SetVehicleConfiguration" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueSetVehicleConfigurationResponse (act_t actJson);
        
        // Glue Function vGlueSendSetVehicleConfigurationError ()
        /**
         * This function will redirect the error  SetVehicleConfigurationError to
         * the client. It uses sendSetVehicleConfigurationError (...) function for
         * that
         *
         * <b>Documentation of 'SetVehicleConfiguration'</b>:
         *
         * If the meaning of "SetVehicleConfiguration" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueSetVehicleConfigurationError (uint32 data, act_t actJson = 0);
        // request SendTouchEvent
        
        /**
         * This function will be called if the component receives the client
         * request "SendTouchEventRequest". The implementation has to redirect the
         * the request using Glue Function vGlueSendSendTouchEventRequest (...)
         * and send the response back using the function
         * sendSendTouchEventResponse (...)
         *
         * <b>Documentation of 'SendTouchEvent'</b>:
         *
         * If the meaning of "SendTouchEvent" isn't clear, then there should be a
         * description here.
         *
         */
        
        void onSendTouchEventRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SendTouchEventRequest >& corfoRequest);
        // Glue Function vGlueSendSendTouchEventResponse ()
        /**
         * This function will redirect the response SendTouchEventResponse to the
         * client. It uses sendSendTouchEventResponse (...) function for that
         *
         * <b>Documentation of 'SendTouchEvent'</b>:
         *
         * If the meaning of "SendTouchEvent" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueSendTouchEventResponse (const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode,act_t actJson);
        
        // Glue Function vGlueSendSendTouchEventError ()
        /**
         * This function will redirect the error  SendTouchEventError to the
         * client. It uses sendSendTouchEventError (...) function for that
         *
         * <b>Documentation of 'SendTouchEvent'</b>:
         *
         * If the meaning of "SendTouchEvent" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueSendTouchEventError (uint32 data, act_t actJson = 0);
        // request SendKeyEvent
        
        /**
         * This function will be called if the component receives the client
         * request "SendKeyEventRequest". The implementation has to redirect the
         * the request using Glue Function vGlueSendSendKeyEventRequest (...) and
         * send the response back using the function sendSendKeyEventResponse
         * (...)
         *
         * <b>Documentation of 'SendKeyEvent'</b>:
         *
         * If the meaning of "SendKeyEvent" isn't clear, then there should be a
         * description here.
         *
         */
        
        void onSendKeyEventRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SendKeyEventRequest >& corfoRequest);
        // Glue Function vGlueSendSendKeyEventResponse ()
        /**
         * This function will redirect the response SendKeyEventResponse to the
         * client. It uses sendSendKeyEventResponse (...) function for that
         *
         * <b>Documentation of 'SendKeyEvent'</b>:
         *
         * If the meaning of "SendKeyEvent" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueSendKeyEventResponse (const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode,act_t actJson);
        
        // Glue Function vGlueSendSendKeyEventError ()
        /**
         * This function will redirect the error  SendKeyEventError to the client.
         * It uses sendSendKeyEventError (...) function for that
         *
         * <b>Documentation of 'SendKeyEvent'</b>:
         *
         * If the meaning of "SendKeyEvent" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueSendKeyEventError (uint32 data, act_t actJson = 0);
        // request UpdateCertificateFile
        
        /**
         * This function will be called if the component receives the client
         * request "UpdateCertificateFileRequest". The implementation has to
         * redirect the the request using Glue Function
         * vGlueSendUpdateCertificateFileRequest (...) and send the response back
         * using the function sendUpdateCertificateFileResponse (...)
         *
         * <b>Documentation of 'UpdateCertificateFile'</b>:
         *
         * If the meaning of "UpdateCertificateFile" isn't clear, then there
         * should be a description here.
         *
         */
        
        void onUpdateCertificateFileRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::UpdateCertificateFileRequest >& corfoRequest);
        // Glue Function vGlueSendUpdateCertificateFileResponse ()
        /**
         * This function will redirect the response UpdateCertificateFileResponse
         * to the client. It uses sendUpdateCertificateFileResponse (...) function
         * for that
         *
         * <b>Documentation of 'UpdateCertificateFile'</b>:
         *
         * If the meaning of "UpdateCertificateFile" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueUpdateCertificateFileResponse (const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode,act_t actJson);
        
        // Glue Function vGlueSendUpdateCertificateFileError ()
        /**
         * This function will redirect the error  UpdateCertificateFileError to
         * the client. It uses sendUpdateCertificateFileError (...) function for
         * that
         *
         * <b>Documentation of 'UpdateCertificateFile'</b>:
         *
         * If the meaning of "UpdateCertificateFile" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueUpdateCertificateFileError (uint32 data, act_t actJson = 0);
        // request SetClientCapabilities
        
        /**
         * This function will be called if the component receives the client
         * request "SetClientCapabilitiesRequest". The implementation has to
         * redirect the the request using Glue Function
         * vGlueSendSetClientCapabilitiesRequest (...) and send the response back
         * using the function sendSetClientCapabilitiesResponse (...)
         *
         * <b>Documentation of 'SetClientCapabilities'</b>:
         *
         * If the meaning of "SetClientCapabilities" isn't clear, then there
         * should be a description here.
         *
         */
        
        void onSetClientCapabilitiesRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetClientCapabilitiesRequest >& corfoRequest);
        // Glue Function vGlueSendSetClientCapabilitiesResponse ()
        /**
         * This function will redirect the response SetClientCapabilitiesResponse
         * to the client. It uses sendSetClientCapabilitiesResponse (...) function
         * for that
         *
         * <b>Documentation of 'SetClientCapabilities'</b>:
         *
         * If the meaning of "SetClientCapabilities" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueSetClientCapabilitiesResponse (act_t actJson);
        
        // Glue Function vGlueSendSetClientCapabilitiesError ()
        /**
         * This function will redirect the error  SetClientCapabilitiesError to
         * the client. It uses sendSetClientCapabilitiesError (...) function for
         * that
         *
         * <b>Documentation of 'SetClientCapabilities'</b>:
         *
         * If the meaning of "SetClientCapabilities" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueSetClientCapabilitiesError (uint32 data, act_t actJson = 0);
        // request AccessoryDisplayContext
        
        /**
         * This function will be called if the component receives the client
         * request "AccessoryDisplayContextRequest". The implementation has to
         * redirect the the request using Glue Function
         * vGlueSendAccessoryDisplayContextRequest (...) and send the response
         * back using the function sendAccessoryDisplayContextResponse (...)
         *
         * <b>Documentation of 'AccessoryDisplayContext'</b>:
         *
         * If the meaning of "AccessoryDisplayContext" isn't clear, then there
         * should be a description here.
         *
         */
        
        void onAccessoryDisplayContextRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::AccessoryDisplayContextRequest >& corfoRequest);
        // Glue Function vGlueSendAccessoryDisplayContextResponse ()
        /**
         * This function will redirect the response
         * AccessoryDisplayContextResponse to the client. It uses
         * sendAccessoryDisplayContextResponse (...) function for that
         *
         * <b>Documentation of 'AccessoryDisplayContext'</b>:
         *
         * If the meaning of "AccessoryDisplayContext" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueAccessoryDisplayContextResponse (act_t actJson);
        
        // Glue Function vGlueSendAccessoryDisplayContextError ()
        /**
         * This function will redirect the error  AccessoryDisplayContextError to
         * the client. It uses sendAccessoryDisplayContextError (...) function for
         * that
         *
         * <b>Documentation of 'AccessoryDisplayContext'</b>:
         *
         * If the meaning of "AccessoryDisplayContext" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueAccessoryDisplayContextError (uint32 data, act_t actJson = 0);
        // request SetDataServiceUsage
        
        /**
         * This function will be called if the component receives the client
         * request "SetDataServiceUsageRequest". The implementation has to
         * redirect the the request using Glue Function
         * vGlueSendSetDataServiceUsageRequest (...) and send the response back
         * using the function sendSetDataServiceUsageResponse (...)
         *
         * <b>Documentation of 'SetDataServiceUsage'</b>:
         *
         * If the meaning of "SetDataServiceUsage" isn't clear, then there should
         * be a description here.
         *
         */
        
        void onSetDataServiceUsageRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetDataServiceUsageRequest >& corfoRequest);
        // Glue Function vGlueSendSetDataServiceUsageResponse ()
        /**
         * This function will redirect the response SetDataServiceUsageResponse to
         * the client. It uses sendSetDataServiceUsageResponse (...) function for
         * that
         *
         * <b>Documentation of 'SetDataServiceUsage'</b>:
         *
         * If the meaning of "SetDataServiceUsage" isn't clear, then there should
         * be a description here.
         *
         */
        
        void vGlueSetDataServiceUsageResponse (const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode,act_t actJson);
        
        // Glue Function vGlueSendSetDataServiceUsageError ()
        /**
         * This function will redirect the error  SetDataServiceUsageError to the
         * client. It uses sendSetDataServiceUsageError (...) function for that
         *
         * <b>Documentation of 'SetDataServiceUsage'</b>:
         *
         * If the meaning of "SetDataServiceUsage" isn't clear, then there should
         * be a description here.
         *
         */
        
        void vGlueSetDataServiceUsageError (uint32 data, act_t actJson = 0);
        // request GetDeviceUsagePreference
        
        /**
         * This function will be called if the component receives the client
         * request "GetDeviceUsagePreferenceRequest". The implementation has to
         * redirect the the request using Glue Function
         * vGlueSendGetDeviceUsagePreferenceRequest (...) and send the response
         * back using the function sendGetDeviceUsagePreferenceResponse (...)
         *
         * <b>Documentation of 'GetDeviceUsagePreference'</b>:
         *
         * If the meaning of "GetDeviceUsagePreference" isn't clear, then there
         * should be a description here.
         *
         */
        
        void onGetDeviceUsagePreferenceRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::GetDeviceUsagePreferenceRequest >& corfoRequest);
        // Glue Function vGlueSendGetDeviceUsagePreferenceResponse ()
        /**
         * This function will redirect the response
         * GetDeviceUsagePreferenceResponse to the client. It uses
         * sendGetDeviceUsagePreferenceResponse (...) function for that
         *
         * <b>Documentation of 'GetDeviceUsagePreference'</b>:
         *
         * If the meaning of "GetDeviceUsagePreference" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueGetDeviceUsagePreferenceResponse (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_DeviceCategory& DeviceCategory,const ::midw_smartphoneint_fi_types::T_e8_EnabledInfo& EnabledInfo,act_t actJson);
        
        // Glue Function vGlueSendGetDeviceUsagePreferenceError ()
        /**
         * This function will redirect the error  GetDeviceUsagePreferenceError to
         * the client. It uses sendGetDeviceUsagePreferenceError (...) function
         * for that
         *
         * <b>Documentation of 'GetDeviceUsagePreference'</b>:
         *
         * If the meaning of "GetDeviceUsagePreference" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueGetDeviceUsagePreferenceError (uint32 data, act_t actJson = 0);
        // request SetVehicleBTAddress
        
        /**
         * This function will be called if the component receives the client
         * request "SetVehicleBTAddressRequest". The implementation has to
         * redirect the the request using Glue Function
         * vGlueSendSetVehicleBTAddressRequest (...) and send the response back
         * using the function sendSetVehicleBTAddressResponse (...)
         *
         * <b>Documentation of 'SetVehicleBTAddress'</b>:
         *
         * If the meaning of "SetVehicleBTAddress" isn't clear, then there should
         * be a description here.
         *
         */
        
        void onSetVehicleBTAddressRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetVehicleBTAddressRequest >& corfoRequest);
        // Glue Function vGlueSendSetVehicleBTAddressResponse ()
        /**
         * This function will redirect the response SetVehicleBTAddressResponse to
         * the client. It uses sendSetVehicleBTAddressResponse (...) function for
         * that
         *
         * <b>Documentation of 'SetVehicleBTAddress'</b>:
         *
         * If the meaning of "SetVehicleBTAddress" isn't clear, then there should
         * be a description here.
         *
         */
        
        void vGlueSetVehicleBTAddressResponse (act_t actJson);
        
        // Glue Function vGlueSendSetVehicleBTAddressError ()
        /**
         * This function will redirect the error  SetVehicleBTAddressError to the
         * client. It uses sendSetVehicleBTAddressError (...) function for that
         *
         * <b>Documentation of 'SetVehicleBTAddress'</b>:
         *
         * If the meaning of "SetVehicleBTAddress" isn't clear, then there should
         * be a description here.
         *
         */
        
        void vGlueSetVehicleBTAddressError (uint32 data, act_t actJson = 0);
        // request SetDataServiceFilter
        
        /**
         * This function will be called if the component receives the client
         * request "SetDataServiceFilterRequest". The implementation has to
         * redirect the the request using Glue Function
         * vGlueSendSetDataServiceFilterRequest (...) and send the response back
         * using the function sendSetDataServiceFilterResponse (...)
         *
         * <b>Documentation of 'SetDataServiceFilter'</b>:
         *
         * If the meaning of "SetDataServiceFilter" isn't clear, then there should
         * be a description here.
         *
         */
        
        void onSetDataServiceFilterRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetDataServiceFilterRequest >& corfoRequest);
        // Glue Function vGlueSendSetDataServiceFilterResponse ()
        /**
         * This function will redirect the response SetDataServiceFilterResponse
         * to the client. It uses sendSetDataServiceFilterResponse (...) function
         * for that
         *
         * <b>Documentation of 'SetDataServiceFilter'</b>:
         *
         * If the meaning of "SetDataServiceFilter" isn't clear, then there should
         * be a description here.
         *
         */
        
        void vGlueSetDataServiceFilterResponse (act_t actJson);
        
        // Glue Function vGlueSendSetDataServiceFilterError ()
        /**
         * This function will redirect the error  SetDataServiceFilterError to the
         * client. It uses sendSetDataServiceFilterError (...) function for that
         *
         * <b>Documentation of 'SetDataServiceFilter'</b>:
         *
         * If the meaning of "SetDataServiceFilter" isn't clear, then there should
         * be a description here.
         *
         */
        
        void vGlueSetDataServiceFilterError (uint32 data, act_t actJson = 0);
        // request InvokeBluetoothDeviceAction
        
        /**
         * This function will be called if the component receives the client
         * request "InvokeBluetoothDeviceActionRequest". The implementation has to
         * redirect the the request using Glue Function
         * vGlueSendInvokeBluetoothDeviceActionRequest (...) and send the response
         * back using the function sendInvokeBluetoothDeviceActionResponse (...)
         *
         * <b>Documentation of 'InvokeBluetoothDeviceAction'</b>:
         *
         * If the meaning of "InvokeBluetoothDeviceAction" isn't clear, then there
         * should be a description here.
         *
         */
        
        void onInvokeBluetoothDeviceActionRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::InvokeBluetoothDeviceActionRequest >& corfoRequest);
        // Glue Function vGlueSendInvokeBluetoothDeviceActionResponse ()
        /**
         * This function will redirect the response
         * InvokeBluetoothDeviceActionResponse to the client. It uses
         * sendInvokeBluetoothDeviceActionResponse (...) function for that
         *
         * <b>Documentation of 'InvokeBluetoothDeviceAction'</b>:
         *
         * If the meaning of "InvokeBluetoothDeviceAction" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueInvokeBluetoothDeviceActionResponse (act_t actJson);
        
        // Glue Function vGlueSendInvokeBluetoothDeviceActionError ()
        /**
         * This function will redirect the error  InvokeBluetoothDeviceActionError
         * to the client. It uses sendInvokeBluetoothDeviceActionError (...)
         * function for that
         *
         * <b>Documentation of 'InvokeBluetoothDeviceAction'</b>:
         *
         * If the meaning of "InvokeBluetoothDeviceAction" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueInvokeBluetoothDeviceActionError (uint32 data, act_t actJson = 0);
        // request AccessoryAudioContext
        
        /**
         * This function will be called if the component receives the client
         * request "AccessoryAudioContextRequest". The implementation has to
         * redirect the the request using Glue Function
         * vGlueSendAccessoryAudioContextRequest (...) and send the response back
         * using the function sendAccessoryAudioContextResponse (...)
         *
         * <b>Documentation of 'AccessoryAudioContext'</b>:
         *
         * If the meaning of "AccessoryAudioContext" isn't clear, then there
         * should be a description here.
         *
         */
        
        void onAccessoryAudioContextRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::AccessoryAudioContextRequest >& corfoRequest);
        // Glue Function vGlueSendAccessoryAudioContextResponse ()
        /**
         * This function will redirect the response AccessoryAudioContextResponse
         * to the client. It uses sendAccessoryAudioContextResponse (...) function
         * for that
         *
         * <b>Documentation of 'AccessoryAudioContext'</b>:
         *
         * If the meaning of "AccessoryAudioContext" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueAccessoryAudioContextResponse (act_t actJson);
        
        // Glue Function vGlueSendAccessoryAudioContextError ()
        /**
         * This function will redirect the error  AccessoryAudioContextError to
         * the client. It uses sendAccessoryAudioContextError (...) function for
         * that
         *
         * <b>Documentation of 'AccessoryAudioContext'</b>:
         *
         * If the meaning of "AccessoryAudioContext" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueAccessoryAudioContextError (uint32 data, act_t actJson = 0);
        // request AccessoryAppState
        
        /**
         * This function will be called if the component receives the client
         * request "AccessoryAppStateRequest". The implementation has to redirect
         * the the request using Glue Function vGlueSendAccessoryAppStateRequest
         * (...) and send the response back using the function
         * sendAccessoryAppStateResponse (...)
         *
         * <b>Documentation of 'AccessoryAppState'</b>:
         *
         * If the meaning of "AccessoryAppState" isn't clear, then there should be
         * a description here.
         *
         */
        
        void onAccessoryAppStateRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::AccessoryAppStateRequest >& corfoRequest);
        // Glue Function vGlueSendAccessoryAppStateResponse ()
        /**
         * This function will redirect the response AccessoryAppStateResponse to
         * the client. It uses sendAccessoryAppStateResponse (...) function for
         * that
         *
         * <b>Documentation of 'AccessoryAppState'</b>:
         *
         * If the meaning of "AccessoryAppState" isn't clear, then there should be
         * a description here.
         *
         */
        
        void vGlueAccessoryAppStateResponse (act_t actJson);
        
        // Glue Function vGlueSendAccessoryAppStateError ()
        /**
         * This function will redirect the error  AccessoryAppStateError to the
         * client. It uses sendAccessoryAppStateError (...) function for that
         *
         * <b>Documentation of 'AccessoryAppState'</b>:
         *
         * If the meaning of "AccessoryAppState" isn't clear, then there should be
         * a description here.
         *
         */
        
        void vGlueAccessoryAppStateError (uint32 data, act_t actJson = 0);
        // request SetRegion
        
        /**
         * This function will be called if the component receives the client
         * request "SetRegionRequest". The implementation has to redirect the the
         * request using Glue Function vGlueSendSetRegionRequest (...) and send
         * the response back using the function sendSetRegionResponse (...)
         *
         * <b>Documentation of 'SetRegion'</b>:
         *
         * If the meaning of "SetRegion" isn't clear, then there should be a
         * description here.
         *
         */
        
        void onSetRegionRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetRegionRequest >& corfoRequest);
        // Glue Function vGlueSendSetRegionResponse ()
        /**
         * This function will redirect the response SetRegionResponse to the
         * client. It uses sendSetRegionResponse (...) function for that
         *
         * <b>Documentation of 'SetRegion'</b>:
         *
         * If the meaning of "SetRegion" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueSetRegionResponse (act_t actJson);
        
        // Glue Function vGlueSendSetRegionError ()
        /**
         * This function will redirect the error  SetRegionError to the client. It
         * uses sendSetRegionError (...) function for that
         *
         * <b>Documentation of 'SetRegion'</b>:
         *
         * If the meaning of "SetRegion" isn't clear, then there should be a
         * description here.
         *
         */
        
        void vGlueSetRegionError (uint32 data, act_t actJson = 0);
        // request DiPORoleSwitchRequired
        
        /**
         * This function will be called if the component receives the client
         * request "DiPORoleSwitchRequiredRequest". The implementation has to
         * redirect the the request using Glue Function
         * vGlueSendDiPORoleSwitchRequiredRequest (...) and send the response back
         * using the function sendDiPORoleSwitchRequiredResponse (...)
         *
         * <b>Documentation of 'DiPORoleSwitchRequired'</b>:
         *
         * If the meaning of "DiPORoleSwitchRequired" isn't clear, then there
         * should be a description here.
         *
         */
        
        void onDiPORoleSwitchRequiredRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::DiPORoleSwitchRequiredRequest >& corfoRequest);
        // Glue Function vGlueSendDiPORoleSwitchRequiredResponse ()
        /**
         * This function will redirect the response DiPORoleSwitchRequiredResponse
         * to the client. It uses sendDiPORoleSwitchRequiredResponse (...)
         * function for that
         *
         * <b>Documentation of 'DiPORoleSwitchRequired'</b>:
         *
         * If the meaning of "DiPORoleSwitchRequired" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueDiPORoleSwitchRequiredResponse (uint8 U8DeviceTag,const ::midw_smartphoneint_fi_types::T_e8_DiPOSwitchReqResponse& E8DiPOSwitchReqResponse,act_t actJson);
        
        // Glue Function vGlueSendDiPORoleSwitchRequiredError ()
        /**
         * This function will redirect the error  DiPORoleSwitchRequiredError to
         * the client. It uses sendDiPORoleSwitchRequiredError (...) function for
         * that
         *
         * <b>Documentation of 'DiPORoleSwitchRequired'</b>:
         *
         * If the meaning of "DiPORoleSwitchRequired" isn't clear, then there
         * should be a description here.
         *
         */
        
        void vGlueDiPORoleSwitchRequiredError (uint32 data, act_t actJson = 0);
            /**
             * This function will be called if the component receives a "registration"
             * message for the property DeviceStatusInfo. The default implementation
             * does nothing. Redirection of registration request has to be done
             *
             * <b>Documentation of 'DeviceStatusInfo'</b>:
             *
             * If the meaning of "DeviceStatusInfo" isn't clear, then there should be
             * a description here.
             *
             * @param isFirst true if the count of the registerd clients is one
             *
             */
            void onDeviceStatusInfoRegister (bool isFirst, act_t actJson);
        
            /**
             * This function will be called if the component receives a
             * "deregistration" message for the property  DeviceStatusInfo. The
             * default implementation does nothing. Redirection of registration
             * request has to be done
             *
             * <b>Documentation of 'DeviceStatusInfo'</b>:
             *
             * If the meaning of "DeviceStatusInfo" isn't clear, then there should be
             * a description here.
             *
             * @param isLast true if no client is registered anymore
             *
             */
            void onDeviceStatusInfoDeregister (bool isLast);
            
                
            // Glue Function vGlueSendDeviceStatusInfoRegisterError ()
            /**
             * This function will redirect the error  DeviceStatusInfoRegisterError to
             * the client. It uses sendDeviceStatusInfoRegisterError (...) function
             * for that
             *
             * <b>Documentation of 'DeviceStatusInfo'</b>:
             *
             * If the meaning of "DeviceStatusInfo" isn't clear, then there should be
             * a description here.
             *
             */
            
            void vGlueDeviceStatusInfoRegisterError (uint32 data, act_t actJson);
            // Glue Function vGlueSendDeviceStatusInfoPropertyUpdate ()
            /**
             * This function will redirect the property notification
             * DeviceStatusInfoUpdate to the client. It uses setDeviceStatusInfo (...)
             * function for that
             *
             * <b>Documentation of 'DeviceStatusInfo'</b>:
             *
             * If the meaning of "DeviceStatusInfo" isn't clear, then there should be
             * a description here.
             *
             */
            
            void vGlueDeviceStatusInfoPropertyUpdate (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionType& DeviceConnectionType,const ::midw_smartphoneint_fi_types::T_e8_DeviceStatusInfo& DeviceStatus, act_t actJson = 0);
            
            /**
             * This function will be called if the component receives a "registration"
             * message for the property AppStatusInfo. The default implementation does
             * nothing. Redirection of registration request has to be done
             *
             * <b>Documentation of 'AppStatusInfo'</b>:
             *
             * If the meaning of "AppStatusInfo" isn't clear, then there should be a
             * description here.
             *
             * @param isFirst true if the count of the registerd clients is one
             *
             */
            void onAppStatusInfoRegister (bool isFirst, act_t actJson);
        
            /**
             * This function will be called if the component receives a
             * "deregistration" message for the property  AppStatusInfo. The default
             * implementation does nothing. Redirection of registration request has to
             * be done
             *
             * <b>Documentation of 'AppStatusInfo'</b>:
             *
             * If the meaning of "AppStatusInfo" isn't clear, then there should be a
             * description here.
             *
             * @param isLast true if no client is registered anymore
             *
             */
            void onAppStatusInfoDeregister (bool isLast);
            
                
            // Glue Function vGlueSendAppStatusInfoRegisterError ()
            /**
             * This function will redirect the error  AppStatusInfoRegisterError to
             * the client. It uses sendAppStatusInfoRegisterError (...) function for
             * that
             *
             * <b>Documentation of 'AppStatusInfo'</b>:
             *
             * If the meaning of "AppStatusInfo" isn't clear, then there should be a
             * description here.
             *
             */
            
            void vGlueAppStatusInfoRegisterError (uint32 data, act_t actJson);
            // Glue Function vGlueSendAppStatusInfoPropertyUpdate ()
            /**
             * This function will redirect the property notification
             * AppStatusInfoUpdate to the client. It uses setAppStatusInfo (...)
             * function for that
             *
             * <b>Documentation of 'AppStatusInfo'</b>:
             *
             * If the meaning of "AppStatusInfo" isn't clear, then there should be a
             * description here.
             *
             */
            
            void vGlueAppStatusInfoPropertyUpdate (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionType& DeviceConnectionType,const ::midw_smartphoneint_fi_types::T_e8_AppStatusInfo& AppStatus, act_t actJson = 0);
            
            /**
             * This function will be called if the component receives a "registration"
             * message for the property DAPStatusInfo. The default implementation does
             * nothing. Redirection of registration request has to be done
             *
             * <b>Documentation of 'DAPStatusInfo'</b>:
             *
             * If the meaning of "DAPStatusInfo" isn't clear, then there should be a
             * description here.
             *
             * @param isFirst true if the count of the registerd clients is one
             *
             */
            void onDAPStatusInfoRegister (bool isFirst, act_t actJson);
        
            /**
             * This function will be called if the component receives a
             * "deregistration" message for the property  DAPStatusInfo. The default
             * implementation does nothing. Redirection of registration request has to
             * be done
             *
             * <b>Documentation of 'DAPStatusInfo'</b>:
             *
             * If the meaning of "DAPStatusInfo" isn't clear, then there should be a
             * description here.
             *
             * @param isLast true if no client is registered anymore
             *
             */
            void onDAPStatusInfoDeregister (bool isLast);
            
                
            // Glue Function vGlueSendDAPStatusInfoRegisterError ()
            /**
             * This function will redirect the error  DAPStatusInfoRegisterError to
             * the client. It uses sendDAPStatusInfoRegisterError (...) function for
             * that
             *
             * <b>Documentation of 'DAPStatusInfo'</b>:
             *
             * If the meaning of "DAPStatusInfo" isn't clear, then there should be a
             * description here.
             *
             */
            
            void vGlueDAPStatusInfoRegisterError (uint32 data, act_t actJson);
            // Glue Function vGlueSendDAPStatusInfoPropertyUpdate ()
            /**
             * This function will redirect the property notification
             * DAPStatusInfoUpdate to the client. It uses setDAPStatusInfo (...)
             * function for that
             *
             * <b>Documentation of 'DAPStatusInfo'</b>:
             *
             * If the meaning of "DAPStatusInfo" isn't clear, then there should be a
             * description here.
             *
             */
            
            void vGlueDAPStatusInfoPropertyUpdate (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionType& DeviceConnectionType,const ::midw_smartphoneint_fi_types::T_e8_DAPStatus& DAPStatus, act_t actJson = 0);
            
            /**
             * This function will be called if the component receives a "registration"
             * message for the property NotificationInfo. The default implementation
             * does nothing. Redirection of registration request has to be done
             *
             * <b>Documentation of 'NotificationInfo'</b>:
             *
             * If the meaning of "NotificationInfo" isn't clear, then there should be
             * a description here.
             *
             * @param isFirst true if the count of the registerd clients is one
             *
             */
            void onNotificationInfoRegister (bool isFirst, act_t actJson);
        
            /**
             * This function will be called if the component receives a
             * "deregistration" message for the property  NotificationInfo. The
             * default implementation does nothing. Redirection of registration
             * request has to be done
             *
             * <b>Documentation of 'NotificationInfo'</b>:
             *
             * If the meaning of "NotificationInfo" isn't clear, then there should be
             * a description here.
             *
             * @param isLast true if no client is registered anymore
             *
             */
            void onNotificationInfoDeregister (bool isLast);
            
                
            // Glue Function vGlueSendNotificationInfoRegisterError ()
            /**
             * This function will redirect the error  NotificationInfoRegisterError to
             * the client. It uses sendNotificationInfoRegisterError (...) function
             * for that
             *
             * <b>Documentation of 'NotificationInfo'</b>:
             *
             * If the meaning of "NotificationInfo" isn't clear, then there should be
             * a description here.
             *
             */
            
            void vGlueNotificationInfoRegisterError (uint32 data, act_t actJson);
            // Glue Function vGlueSendNotificationInfoPropertyUpdate ()
            /**
             * This function will redirect the property notification
             * NotificationInfoUpdate to the client. It uses setNotificationInfo (...)
             * function for that
             *
             * <b>Documentation of 'NotificationInfo'</b>:
             *
             * If the meaning of "NotificationInfo" isn't clear, then there should be
             * a description here.
             *
             */
            
            void vGlueNotificationInfoPropertyUpdate (uint32 DeviceHandle,uint32 AppHandle,const ::midw_smartphoneint_fi_types::T_NotificationData& NotificationData, act_t actJson = 0);
            
            /**
             * This function will be called if the component receives a "registration"
             * message for the property ApplicationMetaData. The default
             * implementation does nothing. Redirection of registration request has to
             * be done
             *
             * <b>Documentation of 'ApplicationMetaData'</b>:
             *
             * If the meaning of "ApplicationMetaData" isn't clear, then there should
             * be a description here.
             *
             * @param isFirst true if the count of the registerd clients is one
             *
             */
            void onApplicationMetaDataRegister (bool isFirst, act_t actJson);
        
            /**
             * This function will be called if the component receives a
             * "deregistration" message for the property  ApplicationMetaData. The
             * default implementation does nothing. Redirection of registration
             * request has to be done
             *
             * <b>Documentation of 'ApplicationMetaData'</b>:
             *
             * If the meaning of "ApplicationMetaData" isn't clear, then there should
             * be a description here.
             *
             * @param isLast true if no client is registered anymore
             *
             */
            void onApplicationMetaDataDeregister (bool isLast);
            
                
            // Glue Function vGlueSendApplicationMetaDataRegisterError ()
            /**
             * This function will redirect the error  ApplicationMetaDataRegisterError
             * to the client. It uses sendApplicationMetaDataRegisterError (...)
             * function for that
             *
             * <b>Documentation of 'ApplicationMetaData'</b>:
             *
             * If the meaning of "ApplicationMetaData" isn't clear, then there should
             * be a description here.
             *
             */
            
            void vGlueApplicationMetaDataRegisterError (uint32 data, act_t actJson);
            // Glue Function vGlueSendApplicationMetaDataPropertyUpdate ()
            /**
             * This function will redirect the property notification
             * ApplicationMetaDataUpdate to the client. It uses setApplicationMetaData
             * (...) function for that
             *
             * <b>Documentation of 'ApplicationMetaData'</b>:
             *
             * If the meaning of "ApplicationMetaData" isn't clear, then there should
             * be a description here.
             *
             */
            
            void vGlueApplicationMetaDataPropertyUpdate (uint32 DeviceHandle,bool MetaDataValid,const ::midw_smartphoneint_fi_types::T_ApplicationMetaData& ApplicationMetaData, act_t actJson = 0);
            
            /**
             * This function will be called if the component receives a "registration"
             * message for the property DeviceDisplayContext. The default
             * implementation does nothing. Redirection of registration request has to
             * be done
             *
             * <b>Documentation of 'DeviceDisplayContext'</b>:
             *
             * If the meaning of "DeviceDisplayContext" isn't clear, then there should
             * be a description here.
             *
             * @param isFirst true if the count of the registerd clients is one
             *
             */
            void onDeviceDisplayContextRegister (bool isFirst, act_t actJson);
        
            /**
             * This function will be called if the component receives a
             * "deregistration" message for the property  DeviceDisplayContext. The
             * default implementation does nothing. Redirection of registration
             * request has to be done
             *
             * <b>Documentation of 'DeviceDisplayContext'</b>:
             *
             * If the meaning of "DeviceDisplayContext" isn't clear, then there should
             * be a description here.
             *
             * @param isLast true if no client is registered anymore
             *
             */
            void onDeviceDisplayContextDeregister (bool isLast);
            
                
            // Glue Function vGlueSendDeviceDisplayContextRegisterError ()
            /**
             * This function will redirect the error
             * DeviceDisplayContextRegisterError to the client. It uses
             * sendDeviceDisplayContextRegisterError (...) function for that
             *
             * <b>Documentation of 'DeviceDisplayContext'</b>:
             *
             * If the meaning of "DeviceDisplayContext" isn't clear, then there should
             * be a description here.
             *
             */
            
            void vGlueDeviceDisplayContextRegisterError (uint32 data, act_t actJson);
            // Glue Function vGlueSendDeviceDisplayContextPropertyUpdate ()
            /**
             * This function will redirect the property notification
             * DeviceDisplayContextUpdate to the client. It uses
             * setDeviceDisplayContext (...) function for that
             *
             * <b>Documentation of 'DeviceDisplayContext'</b>:
             *
             * If the meaning of "DeviceDisplayContext" isn't clear, then there should
             * be a description here.
             *
             */
            
            void vGlueDeviceDisplayContextPropertyUpdate (uint32 DeviceHandle,bool DisplayFlag,const ::midw_smartphoneint_fi_types::T_e8_DisplayContext& DisplayContext, act_t actJson = 0);
            
            /**
             * This function will be called if the component receives a "registration"
             * message for the property DataServiceInfo. The default implementation
             * does nothing. Redirection of registration request has to be done
             *
             * <b>Documentation of 'DataServiceInfo'</b>:
             *
             * If the meaning of "DataServiceInfo" isn't clear, then there should be a
             * description here.
             *
             * @param isFirst true if the count of the registerd clients is one
             *
             */
            void onDataServiceInfoRegister (bool isFirst, act_t actJson);
        
            /**
             * This function will be called if the component receives a
             * "deregistration" message for the property  DataServiceInfo. The default
             * implementation does nothing. Redirection of registration request has to
             * be done
             *
             * <b>Documentation of 'DataServiceInfo'</b>:
             *
             * If the meaning of "DataServiceInfo" isn't clear, then there should be a
             * description here.
             *
             * @param isLast true if no client is registered anymore
             *
             */
            void onDataServiceInfoDeregister (bool isLast);
            
                
            // Glue Function vGlueSendDataServiceInfoRegisterError ()
            /**
             * This function will redirect the error  DataServiceInfoRegisterError to
             * the client. It uses sendDataServiceInfoRegisterError (...) function for
             * that
             *
             * <b>Documentation of 'DataServiceInfo'</b>:
             *
             * If the meaning of "DataServiceInfo" isn't clear, then there should be a
             * description here.
             *
             */
            
            void vGlueDataServiceInfoRegisterError (uint32 data, act_t actJson);
            // Glue Function vGlueSendDataServiceInfoPropertyUpdate ()
            /**
             * This function will redirect the property notification
             * DataServiceInfoUpdate to the client. It uses setDataServiceInfo (...)
             * function for that
             *
             * <b>Documentation of 'DataServiceInfo'</b>:
             *
             * If the meaning of "DataServiceInfo" isn't clear, then there should be a
             * description here.
             *
             */
            
            void vGlueDataServiceInfoPropertyUpdate (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_DeviceCategory& DeviceCategory,const ::midw_smartphoneint_fi_types::T_e8_DataServiceType& DataServiceType,const ::midw_smartphoneint_fi_types::T_e8_DataServiceInfoType& DataServiceInfoType, act_t actJson = 0);
            
            /**
             * This function will be called if the component receives a "registration"
             * message for the property BluetoothDeviceStatus. The default
             * implementation does nothing. Redirection of registration request has to
             * be done
             *
             * <b>Documentation of 'BluetoothDeviceStatus'</b>:
             *
             * If the meaning of "BluetoothDeviceStatus" isn't clear, then there
             * should be a description here.
             *
             * @param isFirst true if the count of the registerd clients is one
             *
             */
            void onBluetoothDeviceStatusRegister (bool isFirst, act_t actJson);
        
            /**
             * This function will be called if the component receives a
             * "deregistration" message for the property  BluetoothDeviceStatus. The
             * default implementation does nothing. Redirection of registration
             * request has to be done
             *
             * <b>Documentation of 'BluetoothDeviceStatus'</b>:
             *
             * If the meaning of "BluetoothDeviceStatus" isn't clear, then there
             * should be a description here.
             *
             * @param isLast true if no client is registered anymore
             *
             */
            void onBluetoothDeviceStatusDeregister (bool isLast);
            
                
            // Glue Function vGlueSendBluetoothDeviceStatusRegisterError ()
            /**
             * This function will redirect the error
             * BluetoothDeviceStatusRegisterError to the client. It uses
             * sendBluetoothDeviceStatusRegisterError (...) function for that
             *
             * <b>Documentation of 'BluetoothDeviceStatus'</b>:
             *
             * If the meaning of "BluetoothDeviceStatus" isn't clear, then there
             * should be a description here.
             *
             */
            
            void vGlueBluetoothDeviceStatusRegisterError (uint32 data, act_t actJson);
            // Glue Function vGlueSendBluetoothDeviceStatusPropertyUpdate ()
            /**
             * This function will redirect the property notification
             * BluetoothDeviceStatusUpdate to the client. It uses
             * setBluetoothDeviceStatus (...) function for that
             *
             * <b>Documentation of 'BluetoothDeviceStatus'</b>:
             *
             * If the meaning of "BluetoothDeviceStatus" isn't clear, then there
             * should be a description here.
             *
             */
            
            void vGlueBluetoothDeviceStatusPropertyUpdate (uint32 BluetoothDeviceHandle,uint32 ProjectionDeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_BTChangeInfo& BTChangeInfo,bool CallActiveStatus, act_t actJson = 0);
            
            /**
             * This function will be called if the component receives a "registration"
             * message for the property DeviceAudioContext. The default implementation
             * does nothing. Redirection of registration request has to be done
             *
             * <b>Documentation of 'DeviceAudioContext'</b>:
             *
             * If the meaning of "DeviceAudioContext" isn't clear, then there should
             * be a description here.
             *
             * @param isFirst true if the count of the registerd clients is one
             *
             */
            void onDeviceAudioContextRegister (bool isFirst, act_t actJson);
        
            /**
             * This function will be called if the component receives a
             * "deregistration" message for the property  DeviceAudioContext. The
             * default implementation does nothing. Redirection of registration
             * request has to be done
             *
             * <b>Documentation of 'DeviceAudioContext'</b>:
             *
             * If the meaning of "DeviceAudioContext" isn't clear, then there should
             * be a description here.
             *
             * @param isLast true if no client is registered anymore
             *
             */
            void onDeviceAudioContextDeregister (bool isLast);
            
                
            // Glue Function vGlueSendDeviceAudioContextRegisterError ()
            /**
             * This function will redirect the error  DeviceAudioContextRegisterError
             * to the client. It uses sendDeviceAudioContextRegisterError (...)
             * function for that
             *
             * <b>Documentation of 'DeviceAudioContext'</b>:
             *
             * If the meaning of "DeviceAudioContext" isn't clear, then there should
             * be a description here.
             *
             */
            
            void vGlueDeviceAudioContextRegisterError (uint32 data, act_t actJson);
            // Glue Function vGlueSendDeviceAudioContextPropertyUpdate ()
            /**
             * This function will redirect the property notification
             * DeviceAudioContextUpdate to the client. It uses setDeviceAudioContext
             * (...) function for that
             *
             * <b>Documentation of 'DeviceAudioContext'</b>:
             *
             * If the meaning of "DeviceAudioContext" isn't clear, then there should
             * be a description here.
             *
             */
            
            void vGlueDeviceAudioContextPropertyUpdate (uint32 DeviceHandle,bool AudioFlag,const ::midw_smartphoneint_fi_types::T_e8_AudioContext& AudioContext, act_t actJson = 0);
            
            /**
             * This function will be called if the component receives a "registration"
             * message for the property DiPOAppStatusInfo. The default implementation
             * does nothing. Redirection of registration request has to be done
             *
             * <b>Documentation of 'DiPOAppStatusInfo'</b>:
             *
             * If the meaning of "DiPOAppStatusInfo" isn't clear, then there should be
             * a description here.
             *
             * @param isFirst true if the count of the registerd clients is one
             *
             */
            void onDiPOAppStatusInfoRegister (bool isFirst, act_t actJson);
        
            /**
             * This function will be called if the component receives a
             * "deregistration" message for the property  DiPOAppStatusInfo. The
             * default implementation does nothing. Redirection of registration
             * request has to be done
             *
             * <b>Documentation of 'DiPOAppStatusInfo'</b>:
             *
             * If the meaning of "DiPOAppStatusInfo" isn't clear, then there should be
             * a description here.
             *
             * @param isLast true if no client is registered anymore
             *
             */
            void onDiPOAppStatusInfoDeregister (bool isLast);
            
                
            // Glue Function vGlueSendDiPOAppStatusInfoRegisterError ()
            /**
             * This function will redirect the error  DiPOAppStatusInfoRegisterError
             * to the client. It uses sendDiPOAppStatusInfoRegisterError (...)
             * function for that
             *
             * <b>Documentation of 'DiPOAppStatusInfo'</b>:
             *
             * If the meaning of "DiPOAppStatusInfo" isn't clear, then there should be
             * a description here.
             *
             */
            
            void vGlueDiPOAppStatusInfoRegisterError (uint32 data, act_t actJson);
            // Glue Function vGlueSendDiPOAppStatusInfoPropertyUpdate ()
            /**
             * This function will redirect the property notification
             * DiPOAppStatusInfoUpdate to the client. It uses setDiPOAppStatusInfo
             * (...) function for that
             *
             * <b>Documentation of 'DiPOAppStatusInfo'</b>:
             *
             * If the meaning of "DiPOAppStatusInfo" isn't clear, then there should be
             * a description here.
             *
             */
            
            void vGlueDiPOAppStatusInfoPropertyUpdate (const ::midw_smartphoneint_fi_types::T_e8_SpeechAppState& AppStateSpeech,const ::midw_smartphoneint_fi_types::T_e8_PhoneAppState& AppStatePhone,const ::midw_smartphoneint_fi_types::T_e8_NavigationAppState& AppStateNavigation, act_t actJson = 0);
            
            /**
             * This function will be called if the component receives a "registration"
             * message for the property SessionStatusInfo. The default implementation
             * does nothing. Redirection of registration request has to be done
             *
             * <b>Documentation of 'SessionStatusInfo'</b>:
             *
             * If the meaning of "SessionStatusInfo" isn't clear, then there should be
             * a description here.
             *
             * @param isFirst true if the count of the registerd clients is one
             *
             */
            void onSessionStatusInfoRegister (bool isFirst, act_t actJson);
        
            /**
             * This function will be called if the component receives a
             * "deregistration" message for the property  SessionStatusInfo. The
             * default implementation does nothing. Redirection of registration
             * request has to be done
             *
             * <b>Documentation of 'SessionStatusInfo'</b>:
             *
             * If the meaning of "SessionStatusInfo" isn't clear, then there should be
             * a description here.
             *
             * @param isLast true if no client is registered anymore
             *
             */
            void onSessionStatusInfoDeregister (bool isLast);
            
                
            // Glue Function vGlueSendSessionStatusInfoRegisterError ()
            /**
             * This function will redirect the error  SessionStatusInfoRegisterError
             * to the client. It uses sendSessionStatusInfoRegisterError (...)
             * function for that
             *
             * <b>Documentation of 'SessionStatusInfo'</b>:
             *
             * If the meaning of "SessionStatusInfo" isn't clear, then there should be
             * a description here.
             *
             */
            
            void vGlueSessionStatusInfoRegisterError (uint32 data, act_t actJson);
            // Glue Function vGlueSendSessionStatusInfoPropertyUpdate ()
            /**
             * This function will redirect the property notification
             * SessionStatusInfoUpdate to the client. It uses setSessionStatusInfo
             * (...) function for that
             *
             * <b>Documentation of 'SessionStatusInfo'</b>:
             *
             * If the meaning of "SessionStatusInfo" isn't clear, then there should be
             * a description here.
             *
             */
            
            void vGlueSessionStatusInfoPropertyUpdate (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_DeviceCategory& DeviceCategory,const ::midw_smartphoneint_fi_types::T_e8_SessionStatus& SessionStatus, act_t actJson = 0);
            
    private:
    /*Proxy Referential Pointer*/
    ::boost::shared_ptr< Midw_smartphoneint_fiAdapterComponent > m_poMidw_smartphoneint_fiAdapterComponent;
    vector<act_t> _registerPropertyBuffer;
    DECLARE_CLASS_LOGGER (); 
};

} // namespace adapter
} // namespace midw_smartphoneint_fiComponent

#endif // MIDW_SMARTPHONEINT_FIADAPTERCOMPONENTSTUB_H
