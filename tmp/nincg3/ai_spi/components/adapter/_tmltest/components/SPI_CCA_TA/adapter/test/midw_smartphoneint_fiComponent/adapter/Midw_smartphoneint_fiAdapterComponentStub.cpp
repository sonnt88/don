/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#include "BaseComponent.h"
#include "Midw_smartphoneint_fiAdapterComponent.h"
#include "Midw_smartphoneint_fiAdapterComponentStub.h"
#include "asf/core/Logger.h"
#include "asf/core/Types.h"
#include "boost/ptr_container/ptr_vector.hpp"
#include "boost/shared_ptr.hpp"
#include "midw_smartphoneint_fi.h"
#include "midw_smartphoneint_fi/Midw_smartphoneint_fiService.h"
#include "midw_smartphoneint_fi/midw_smartphoneint_fiAuxiliaryTypes.h"
#include "midw_smartphoneint_fi/midw_smartphoneint_fiAuxiliaryTypesConst.h"
#include "midw_smartphoneint_fi/midw_smartphoneint_fi_typesTypes.h"
#include "midw_smartphoneint_fi/midw_smartphoneint_fi_typesTypesConst.h"
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace ::asf::core;
using namespace std;

namespace midw_smartphoneint_fiComponent {
namespace adapter {

using namespace ::midw_smartphoneint_fi::Midw_smartphoneint_fiService;

DEFINE_CLASS_LOGGER_AND_LEVEL ("midw_smartphoneint_fiComponent/adapter/Midw_smartphoneint_fiAdapterComponent", Midw_smartphoneint_fiAdapterComponentStub, Info);


Midw_smartphoneint_fiAdapterComponentStub::Midw_smartphoneint_fiAdapterComponentStub (Midw_smartphoneint_fiAdapterComponent* const rfoProxy) : BaseComponent (), \
    Midw_smartphoneint_fiServiceStub ("midw_smartphoneint_fiServiceProvidedPort"), \
    m_poMidw_smartphoneint_fiAdapterComponent (rfoProxy) {
    LOG_INFO ("Midw_smartphoneint_fiAdapterComponentStub constructor...");
    m_poMidw_smartphoneint_fiAdapterComponent->setPropertyStubAvailable (true);
}

Midw_smartphoneint_fiAdapterComponentStub::~Midw_smartphoneint_fiAdapterComponentStub () {
    LOG_INFO ("Midw_smartphoneint_fiAdapterComponentStub destructor...");
    m_poMidw_smartphoneint_fiAdapterComponent->setPropertyStubAvailable (false);
}

// request GetDeviceInfoList
void Midw_smartphoneint_fiAdapterComponentStub::onGetDeviceInfoListRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::GetDeviceInfoListRequest >& corfoRequest)
{
    LOG_INFO ("onGetDeviceInfoListRequest ()...");

    m_poMidw_smartphoneint_fiAdapterComponent->vGlueGetDeviceInfoListRequest (corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueGetDeviceInfoListResponse (uint16 NumDevices,const ::std::vector< ::midw_smartphoneint_fi_types::T_DeviceDetails >& DeviceInfoList, act_t actJson)
{
    LOG_INFO ("vGlueGetDeviceInfoListResponse ()...");
    
    LOG_INFO ("NumDevices:%u", NumDevices);
    ::boost::ptr_vector< ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_DeviceDetails > rPtrVector2DeviceInfoList;
    rPtrVector2DeviceInfoList.clear ();
    if (0 == DeviceInfoList.size ()){
        LOG_INFO ("DeviceInfoListList  is empty...");
    } else {
        uint32 u32i = 0;
    
        for (::std::vector< ::midw_smartphoneint_fi_types::T_DeviceDetails >::const_iterator \
        itT_DeviceDetails = DeviceInfoList.begin (); itT_DeviceDetails != DeviceInfoList.end (); ++itT_DeviceDetails)
        {
            LOG_INFO ("-------DeviceInfoList---------------");
            LOG_INFO ("Current Index : %d", u32i);
            ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_DeviceDetails *rT_DeviceDetails;
            rT_DeviceDetails = new ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_DeviceDetails;
    
            LOG_INFO ("U32DeviceHandle : %u", itT_DeviceDetails->getU32DeviceHandle ());
    
       rT_DeviceDetails->setU32DeviceHandle (itT_DeviceDetails->getU32DeviceHandle ());
    
            LOG_INFO ("SzDeviceName : %s", itT_DeviceDetails->getSzDeviceName ().c_str ());
    
       rT_DeviceDetails->setSzDeviceName (itT_DeviceDetails->getSzDeviceName ());
    
            rT_DeviceDetails->setEnDeviceCategory (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceCategory (itT_DeviceDetails->getEnDeviceCategory ()));
            LOG_INFO ("SzDeviceModelName : %s", itT_DeviceDetails->getSzDeviceModelName ().c_str ());
    
       rT_DeviceDetails->setSzDeviceModelName (itT_DeviceDetails->getSzDeviceModelName ());
    
            LOG_INFO ("SzDeviceManufacturerName : %s", itT_DeviceDetails->getSzDeviceManufacturerName ().c_str ());
    
       rT_DeviceDetails->setSzDeviceManufacturerName (itT_DeviceDetails->getSzDeviceManufacturerName ());
    
            rT_DeviceDetails->setEnDeviceConnectionStatus (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceConnectionStatus (itT_DeviceDetails->getEnDeviceConnectionStatus ()));
            rT_DeviceDetails->setEnDeviceConnectionType (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceConnectionType (itT_DeviceDetails->getEnDeviceConnectionType ()));
            rT_DeviceDetails->setRVersionInfo (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_VersionInfo (itT_DeviceDetails->getRVersionInfo ().getSzMinorVersion (),
            itT_DeviceDetails->getRVersionInfo ().getSzMajorVersion (),
            itT_DeviceDetails->getRVersionInfo ().getSzPatchVersion ()));
            rT_DeviceDetails->setRServerCapabilities (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_ServerCapabilities (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_KeyCapabilities (itT_DeviceDetails->getRServerCapabilities ().getRKeyCapabilities ().getU16KnobKeySupport (),
            itT_DeviceDetails->getRServerCapabilities ().getRKeyCapabilities ().getU16DeviceKeySupport (),
            itT_DeviceDetails->getRServerCapabilities ().getRKeyCapabilities ().getU16MultimediaKeySupport (),
            itT_DeviceDetails->getRServerCapabilities ().getRKeyCapabilities ().getU16MiscKeySupport (),
            itT_DeviceDetails->getRServerCapabilities ().getRKeyCapabilities ().getU16MultiTouchSupport (),
            itT_DeviceDetails->getRServerCapabilities ().getRKeyCapabilities ().getU16PointerTouchSupport ()),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_AudioCapabilities (itT_DeviceDetails->getRServerCapabilities ().getRAudioCapabilities ().getU16BTHFPSupport (),
            itT_DeviceDetails->getRServerCapabilities ().getRAudioCapabilities ().getU16BTA2DPSupport (),
            itT_DeviceDetails->getRServerCapabilities ().getRAudioCapabilities ().getU16RTPInSupport (),
            itT_DeviceDetails->getRServerCapabilities ().getRAudioCapabilities ().getU16RTPOutSupport ()),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_DisplayCapabilities (itT_DeviceDetails->getRServerCapabilities ().getRDisplayCapabilities ().getU16FrameBufferConfiguration (),
            itT_DeviceDetails->getRServerCapabilities ().getRDisplayCapabilities ().getU16PixelFormat ())));
            LOG_INFO ("BDeviceUsageEnabled : %u", itT_DeviceDetails->getBDeviceUsageEnabled ());
    
       rT_DeviceDetails->setBDeviceUsageEnabled (itT_DeviceDetails->getBDeviceUsageEnabled ());
    
            LOG_INFO ("BSelectedDevice : %u", itT_DeviceDetails->getBSelectedDevice ());
    
       rT_DeviceDetails->setBSelectedDevice (itT_DeviceDetails->getBSelectedDevice ());
    
            LOG_INFO ("BDAPSupport : %u", itT_DeviceDetails->getBDAPSupport ());
    
       rT_DeviceDetails->setBDAPSupport (itT_DeviceDetails->getBDAPSupport ());
    
            LOG_INFO ("SzBTAddress : %s", itT_DeviceDetails->getSzBTAddress ().c_str ());
    
       rT_DeviceDetails->setSzBTAddress (itT_DeviceDetails->getSzBTAddress ());
    
    
            rPtrVector2DeviceInfoList.push_back (rT_DeviceDetails);
            u32i++;
            LOG_INFO ("-------End of DeviceInfoList---------------");
        }
    }
    
    sendGetDeviceInfoListResponse (NumDevices,rPtrVector2DeviceInfoList, actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueGetDeviceInfoListError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueGetDeviceInfoListError ()...");
    LOG_INFO ("data:%u", data);

    sendGetDeviceInfoListError (getdeviceinfolistErrorCode (data), actJson);
}
// request SelectDevice
void Midw_smartphoneint_fiAdapterComponentStub::onSelectDeviceRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SelectDeviceRequest >& corfoRequest)
{
    LOG_INFO ("onSelectDeviceRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSelectDeviceRequest (corfoRequest->getDeviceHandle (),::midw_smartphoneint_fi_types::T_e8_DeviceConnectionType (corfoRequest->getDeviceConnectionType ()),::midw_smartphoneint_fi_types::T_e8_DeviceConnectionReq (corfoRequest->getDeviceConnectionReq ()),::midw_smartphoneint_fi_types::T_e8_EnabledInfo (corfoRequest->getDAPUsage ()),::midw_smartphoneint_fi_types::T_e8_EnabledInfo (corfoRequest->getCDBUsage ()),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSelectDeviceResponse (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionType& DeviceConnectionType,const ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionReq& DeviceConnectionReq,const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode,bool BTPairingRequired, act_t actJson)
{
    LOG_INFO ("vGlueSelectDeviceResponse ()...");
    
    LOG_INFO ("DeviceHandle:%u", DeviceHandle);
    LOG_INFO ("BTPairingRequired:%u", BTPairingRequired);
    sendSelectDeviceResponse (DeviceHandle,::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceConnectionType (DeviceConnectionType),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceConnectionReq (DeviceConnectionReq),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ResponseCode (ResponseCode),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ErrorType (ErrorCode),BTPairingRequired, actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSelectDeviceError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSelectDeviceError ()...");
    LOG_INFO ("data:%u", data);

    sendSelectDeviceError (selectdeviceErrorCode (data), actJson);
}
// request LaunchApp
void Midw_smartphoneint_fiAdapterComponentStub::onLaunchAppRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::LaunchAppRequest >& corfoRequest)
{
    LOG_INFO ("onLaunchAppRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    LOG_INFO ("AppHandle:%u", corfoRequest->getAppHandle ());
    LOG_INFO ("TelephoneNumber:%s", corfoRequest->getTelephoneNumber ().c_str ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueLaunchAppRequest (corfoRequest->getDeviceHandle (),::midw_smartphoneint_fi_types::T_e8_DeviceCategory (corfoRequest->getDeviceCategory ()),corfoRequest->getAppHandle (),::midw_smartphoneint_fi_types::T_e8_DiPOAppType (corfoRequest->getDiPOAppType ()),corfoRequest->getTelephoneNumber (),::midw_smartphoneint_fi_types::T_e8_EcnrSetting (corfoRequest->getEcnrSetting ()),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueLaunchAppResponse (uint32 DeviceHandle,uint32 AppHandle,const ::midw_smartphoneint_fi_types::T_e8_DiPOAppType& DiPOAppType,const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode, act_t actJson)
{
    LOG_INFO ("vGlueLaunchAppResponse ()...");
    
    LOG_INFO ("DeviceHandle:%u", DeviceHandle);
    LOG_INFO ("AppHandle:%u", AppHandle);
    sendLaunchAppResponse (DeviceHandle,AppHandle,::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DiPOAppType (DiPOAppType),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ResponseCode (ResponseCode),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ErrorType (ErrorCode), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueLaunchAppError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueLaunchAppError ()...");
    LOG_INFO ("data:%u", data);

    sendLaunchAppError (launchappErrorCode (data), actJson);
}
// request TerminateApp
void Midw_smartphoneint_fiAdapterComponentStub::onTerminateAppRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::TerminateAppRequest >& corfoRequest)
{
    LOG_INFO ("onTerminateAppRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    LOG_INFO ("AppHandle:%u", corfoRequest->getAppHandle ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueTerminateAppRequest (corfoRequest->getDeviceHandle (),corfoRequest->getAppHandle (),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueTerminateAppResponse (uint32 DeviceHandle,uint32 AppHandle,const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode, act_t actJson)
{
    LOG_INFO ("vGlueTerminateAppResponse ()...");
    
    LOG_INFO ("DeviceHandle:%u", DeviceHandle);
    LOG_INFO ("AppHandle:%u", AppHandle);
    sendTerminateAppResponse (DeviceHandle,AppHandle,::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ResponseCode (ResponseCode),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ErrorType (ErrorCode), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueTerminateAppError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueTerminateAppError ()...");
    LOG_INFO ("data:%u", data);

    sendTerminateAppError (terminateappErrorCode (data), actJson);
}
// request GetAppList
void Midw_smartphoneint_fiAdapterComponentStub::onGetAppListRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::GetAppListRequest >& corfoRequest)
{
    LOG_INFO ("onGetAppListRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueGetAppListRequest (corfoRequest->getDeviceHandle (),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueGetAppListResponse (uint32 DeviceHandle,uint16 NumAppDetailsList,const ::std::vector< ::midw_smartphoneint_fi_types::T_AppDetails >& AppDetailsList, act_t actJson)
{
    LOG_INFO ("vGlueGetAppListResponse ()...");
    
    LOG_INFO ("DeviceHandle:%u", DeviceHandle);
    LOG_INFO ("NumAppDetailsList:%u", NumAppDetailsList);
    ::boost::ptr_vector< ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_AppDetails > rPtrVector3AppDetailsList;
    rPtrVector3AppDetailsList.clear ();
    if (0 == AppDetailsList.size ()){
        LOG_INFO ("AppDetailsListList  is empty...");
    } else {
        uint32 u32i = 0;
    
        for (::std::vector< ::midw_smartphoneint_fi_types::T_AppDetails >::const_iterator \
        itT_AppDetails = AppDetailsList.begin (); itT_AppDetails != AppDetailsList.end (); ++itT_AppDetails)
        {
            LOG_INFO ("-------AppDetailsList---------------");
            LOG_INFO ("Current Index : %d", u32i);
            ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_AppDetails *rT_AppDetails;
            rT_AppDetails = new ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_AppDetails;
    
            LOG_INFO ("U32AppHandle : %u", itT_AppDetails->getU32AppHandle ());
    
       rT_AppDetails->setU32AppHandle (itT_AppDetails->getU32AppHandle ());
    
            LOG_INFO ("SzAppName : %s", itT_AppDetails->getSzAppName ().c_str ());
    
       rT_AppDetails->setSzAppName (itT_AppDetails->getSzAppName ());
    
            rT_AppDetails->setEnAppStatus (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_AppStatus (itT_AppDetails->getEnAppStatus ()));
            LOG_INFO ("SzAppVariant : %s", itT_AppDetails->getSzAppVariant ().c_str ());
    
       rT_AppDetails->setSzAppVariant (itT_AppDetails->getSzAppVariant ());
    
            LOG_INFO ("SzAppProviderName : %s", itT_AppDetails->getSzAppProviderName ().c_str ());
    
       rT_AppDetails->setSzAppProviderName (itT_AppDetails->getSzAppProviderName ());
    
            LOG_INFO ("SzAppProviderURL : %s", itT_AppDetails->getSzAppProviderURL ().c_str ());
    
       rT_AppDetails->setSzAppProviderURL (itT_AppDetails->getSzAppProviderURL ());
    
            LOG_INFO ("SzAppDescription : %s", itT_AppDetails->getSzAppDescription ().c_str ());
    
       rT_AppDetails->setSzAppDescription (itT_AppDetails->getSzAppDescription ());
    
    ::boost::ptr_vector< string > rPtrVector3AppAllowedProfiles;
    rPtrVector3AppAllowedProfiles.clear ();
    if (0 == itT_AppDetails->getAppAllowedProfiles ().size ()){
        LOG_INFO ("itT_AppDetails->getAppAllowedProfiles ()List  is empty...");
    } else {
        uint32 u32i = 0;
    
        for (::std::vector< string >::const_iterator \
        itstring = itT_AppDetails->getAppAllowedProfiles ().begin (); itstring != itT_AppDetails->getAppAllowedProfiles ().end (); ++itstring)
        {
            LOG_INFO ("-------AppAllowedProfiles---------------");
            LOG_INFO ("Current Index : %d", u32i);
    
    
            rPtrVector3AppAllowedProfiles.push_back (new string (*itstring));
            u32i++;
            LOG_INFO ("-------End of AppAllowedProfiles---------------");
        }
    }
    
            rT_AppDetails->setAppAllowedProfiles (rPtrVector3AppAllowedProfiles);
    
            LOG_INFO ("SzAppCertificateURL : %s", itT_AppDetails->getSzAppCertificateURL ().c_str ());
    
       rT_AppDetails->setSzAppCertificateURL (itT_AppDetails->getSzAppCertificateURL ());
    
            rT_AppDetails->setEnAppCategory (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e32_AppCategory (itT_AppDetails->getEnAppCategory ()));
            rT_AppDetails->setEnTrustLevel (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e16_TrustLevel (itT_AppDetails->getEnTrustLevel ()));
            rT_AppDetails->setE8AppCertificationInfo (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_AppCertificationInfo (itT_AppDetails->getE8AppCertificationInfo ()));
            rT_AppDetails->setRAppDisplayInfo (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_AppDisplayInfo (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e32_AppDisplayCategory (itT_AppDetails->getRAppDisplayInfo ().getEnAppDisplayCategory ()),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e16_TrustLevel (itT_AppDetails->getRAppDisplayInfo ().getEnTrustLevel ()),itT_AppDetails->getRAppDisplayInfo ().getU32AppDisplayRules (),
            itT_AppDetails->getRAppDisplayInfo ().getSzAppDisplayOrientation ()));
            rT_AppDetails->setRAppAudioInfo (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_AppAudioInfo (itT_AppDetails->getRAppAudioInfo ().getSzAppAudioType (),
            ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e32_AppAudioCategory (itT_AppDetails->getRAppAudioInfo ().getEnAppAudioCategory ()),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e16_TrustLevel (itT_AppDetails->getRAppAudioInfo ().getEnTrustLevel ()),itT_AppDetails->getRAppAudioInfo ().getU32AppAudioRules ()));
            rT_AppDetails->setRAppRemotingInfo (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_AppRemotingInfo (itT_AppDetails->getRAppRemotingInfo ().getSzRemotingProtocolID (),
            itT_AppDetails->getRAppRemotingInfo ().getSzRemotingFormat (),
            itT_AppDetails->getRAppRemotingInfo ().getSzRemotingDirection (),
            itT_AppDetails->getRAppRemotingInfo ().getU32RemotingAudioIPL (),
            itT_AppDetails->getRAppRemotingInfo ().getU32RemotingAudioMPL ()));
            LOG_INFO ("U16NumAppIcons : %u", itT_AppDetails->getU16NumAppIcons ());
    
       rT_AppDetails->setU16NumAppIcons (itT_AppDetails->getU16NumAppIcons ());
    
    ::boost::ptr_vector< ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_IconAttributes > rPtrVector3AppIconList;
    rPtrVector3AppIconList.clear ();
    if (0 == itT_AppDetails->getAppIconList ().size ()){
        LOG_INFO ("itT_AppDetails->getAppIconList ()List  is empty...");
    } else {
        uint32 u32i = 0;
    
        for (::std::vector< ::midw_smartphoneint_fi_types::T_IconAttributes >::const_iterator \
        itT_IconAttributes = itT_AppDetails->getAppIconList ().begin (); itT_IconAttributes != itT_AppDetails->getAppIconList ().end (); ++itT_IconAttributes)
        {
            LOG_INFO ("-------AppIconList---------------");
            LOG_INFO ("Current Index : %d", u32i);
            ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_IconAttributes *rT_IconAttributes;
            rT_IconAttributes = new ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_IconAttributes;
    
            LOG_INFO ("U32IconWidth : %u", itT_IconAttributes->getU32IconWidth ());
    
       rT_IconAttributes->setU32IconWidth (itT_IconAttributes->getU32IconWidth ());
    
            LOG_INFO ("U32IconHeight : %u", itT_IconAttributes->getU32IconHeight ());
    
       rT_IconAttributes->setU32IconHeight (itT_IconAttributes->getU32IconHeight ());
    
            LOG_INFO ("U32IconDepth : %u", itT_IconAttributes->getU32IconDepth ());
    
       rT_IconAttributes->setU32IconDepth (itT_IconAttributes->getU32IconDepth ());
    
            rT_IconAttributes->setEnIconMimeType (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_IconMimeType (itT_IconAttributes->getEnIconMimeType ()));
            LOG_INFO ("SzIconURL : %s", itT_IconAttributes->getSzIconURL ().c_str ());
    
       rT_IconAttributes->setSzIconURL (itT_IconAttributes->getSzIconURL ());
    
    
            rPtrVector3AppIconList.push_back (rT_IconAttributes);
            u32i++;
            LOG_INFO ("-------End of AppIconList---------------");
        }
    }
    
            rT_AppDetails->setAppIconList (rPtrVector3AppIconList);
    
    
            rPtrVector3AppDetailsList.push_back (rT_AppDetails);
            u32i++;
            LOG_INFO ("-------End of AppDetailsList---------------");
        }
    }
    
    sendGetAppListResponse (DeviceHandle,NumAppDetailsList,rPtrVector3AppDetailsList, actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueGetAppListError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueGetAppListError ()...");
    LOG_INFO ("data:%u", data);

    sendGetAppListError (getapplistErrorCode (data), actJson);
}
// request GetAppIconData
void Midw_smartphoneint_fiAdapterComponentStub::onGetAppIconDataRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::GetAppIconDataRequest >& corfoRequest)
{
    LOG_INFO ("onGetAppIconDataRequest ()...");

    LOG_INFO ("AppIconURL:%s", corfoRequest->getAppIconURL ().c_str ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueGetAppIconDataRequest (corfoRequest->getAppIconURL (),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueGetAppIconDataResponse (::std::string IconMimeType,const ::std::vector< uint8 >& AppIconData, act_t actJson)
{
    LOG_INFO ("vGlueGetAppIconDataResponse ()...");
    
    LOG_INFO ("IconMimeType:%s", IconMimeType.c_str ());
    for (uint32 u32Index = 0; u32Index<AppIconData.size (); u32Index++)
        LOG_INFO ("AppIconData[%u]:", u32Index, AppIconData[u32Index]);
    
    sendGetAppIconDataResponse (IconMimeType,AppIconData, actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueGetAppIconDataError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueGetAppIconDataError ()...");
    LOG_INFO ("data:%u", data);

    sendGetAppIconDataError (getappicondataErrorCode (data), actJson);
}
// request SetAppIconAttributes
void Midw_smartphoneint_fiAdapterComponentStub::onSetAppIconAttributesRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetAppIconAttributesRequest >& corfoRequest)
{
    LOG_INFO ("onSetAppIconAttributesRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    LOG_INFO ("AppHandle:%u", corfoRequest->getAppHandle ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSetAppIconAttributesRequest (corfoRequest->getDeviceHandle (),corfoRequest->getAppHandle (),::midw_smartphoneint_fi_types::T_IconAttributes (corfoRequest->getIconAttributes ().getU32IconWidth (),
    corfoRequest->getIconAttributes ().getU32IconHeight (),
    corfoRequest->getIconAttributes ().getU32IconDepth (),
    ::midw_smartphoneint_fi_types::T_e8_IconMimeType (corfoRequest->getIconAttributes ().getEnIconMimeType ()),corfoRequest->getIconAttributes ().getSzIconURL ()),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSetAppIconAttributesResponse (const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode, act_t actJson)
{
    LOG_INFO ("vGlueSetAppIconAttributesResponse ()...");
    
    sendSetAppIconAttributesResponse (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ResponseCode (ResponseCode),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ErrorType (ErrorCode), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSetAppIconAttributesError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSetAppIconAttributesError ()...");
    LOG_INFO ("data:%u", data);

    sendSetAppIconAttributesError (setappiconattributesErrorCode (data), actJson);
}
// request SetDeviceUsagePreference
void Midw_smartphoneint_fiAdapterComponentStub::onSetDeviceUsagePreferenceRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetDeviceUsagePreferenceRequest >& corfoRequest)
{
    LOG_INFO ("onSetDeviceUsagePreferenceRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSetDeviceUsagePreferenceRequest (corfoRequest->getDeviceHandle (),::midw_smartphoneint_fi_types::T_e8_DeviceCategory (corfoRequest->getDeviceCategory ()),::midw_smartphoneint_fi_types::T_e8_EnabledInfo (corfoRequest->getEnabledInfo ()),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSetDeviceUsagePreferenceResponse ( act_t actJson)
{
    LOG_INFO ("vGlueSetDeviceUsagePreferenceResponse ()...");
    
    sendSetDeviceUsagePreferenceResponse (actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSetDeviceUsagePreferenceError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSetDeviceUsagePreferenceError ()...");
    LOG_INFO ("data:%u", data);

    sendSetDeviceUsagePreferenceError (setdeviceusagepreferenceErrorCode (data), actJson);
}
// request SetMLNotificationEnabledInfo
void Midw_smartphoneint_fiAdapterComponentStub::onSetMLNotificationEnabledInfoRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetMLNotificationEnabledInfoRequest >& corfoRequest)
{
    LOG_INFO ("onSetMLNotificationEnabledInfoRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    LOG_INFO ("NumNotificationEnableList:%u", corfoRequest->getNumNotificationEnableList ());
    ::std::vector< ::midw_smartphoneint_fi_types::T_NotificationEnable > rStdVector3NotificationEnableList;
    rStdVector3NotificationEnableList.clear ();
    if (0 == corfoRequest->getNotificationEnableList ().size ()) {
        LOG_INFO ("corfoRequest->getNotificationEnableList ()List  is empty...");
    }
    else {
        uint32 u32i = 0;
    
        for (::boost::ptr_vector< ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_NotificationEnable >::const_iterator \
        itT_NotificationEnable = corfoRequest->getNotificationEnableList ().begin (); itT_NotificationEnable != corfoRequest->getNotificationEnableList ().end (); ++itT_NotificationEnable)
        {
            LOG_INFO ("-------NotificationEnableList---------------");
            LOG_INFO ("Current Index : %d", u32i);
            ::midw_smartphoneint_fi_types::T_NotificationEnable rT_NotificationEnable;
    
            LOG_INFO ("U32AppHandle : %u", itT_NotificationEnable->getU32AppHandle ());
    
       rT_NotificationEnable.setU32AppHandle (itT_NotificationEnable->getU32AppHandle () );
    
            rT_NotificationEnable.setEnEnabledInfo (::midw_smartphoneint_fi_types::T_e8_EnabledInfo (itT_NotificationEnable->getEnEnabledInfo ()));
    
            rStdVector3NotificationEnableList.push_back (rT_NotificationEnable);
            u32i++;
            LOG_INFO ("-------End of NotificationEnableList---------------");
        }
    }
    
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSetMLNotificationEnabledInfoRequest (corfoRequest->getDeviceHandle (),corfoRequest->getNumNotificationEnableList (),rStdVector3NotificationEnableList,corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSetMLNotificationEnabledInfoResponse (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode, act_t actJson)
{
    LOG_INFO ("vGlueSetMLNotificationEnabledInfoResponse ()...");
    
    LOG_INFO ("DeviceHandle:%u", DeviceHandle);
    sendSetMLNotificationEnabledInfoResponse (DeviceHandle,::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ResponseCode (ResponseCode),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ErrorType (ErrorCode), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSetMLNotificationEnabledInfoError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSetMLNotificationEnabledInfoError ()...");
    LOG_INFO ("data:%u", data);

    sendSetMLNotificationEnabledInfoError (setmlnotificationenabledinfoErrorCode (data), actJson);
}
// request InvokeNotificationAction
void Midw_smartphoneint_fiAdapterComponentStub::onInvokeNotificationActionRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::InvokeNotificationActionRequest >& corfoRequest)
{
    LOG_INFO ("onInvokeNotificationActionRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    LOG_INFO ("AppHandle:%u", corfoRequest->getAppHandle ());
    LOG_INFO ("NotificationID:%u", corfoRequest->getNotificationID ());
    LOG_INFO ("NotificationActionID:%u", corfoRequest->getNotificationActionID ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueInvokeNotificationActionRequest (corfoRequest->getDeviceHandle (),corfoRequest->getAppHandle (),corfoRequest->getNotificationID (),corfoRequest->getNotificationActionID (),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueInvokeNotificationActionResponse (const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode, act_t actJson)
{
    LOG_INFO ("vGlueInvokeNotificationActionResponse ()...");
    
    sendInvokeNotificationActionResponse (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ResponseCode (ResponseCode),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ErrorType (ErrorCode), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueInvokeNotificationActionError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueInvokeNotificationActionError ()...");
    LOG_INFO ("data:%u", data);

    sendInvokeNotificationActionError (invokenotificationactionErrorCode (data), actJson);
}
// request GetVideoSettings
void Midw_smartphoneint_fiAdapterComponentStub::onGetVideoSettingsRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::GetVideoSettingsRequest >& corfoRequest)
{
    LOG_INFO ("onGetVideoSettingsRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueGetVideoSettingsRequest (corfoRequest->getDeviceHandle (),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueGetVideoSettingsResponse (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_VideoAttributes& VideoAttributes, act_t actJson)
{
    LOG_INFO ("vGlueGetVideoSettingsResponse ()...");
    
    LOG_INFO ("DeviceHandle:%u", DeviceHandle);
    sendGetVideoSettingsResponse (DeviceHandle,::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_VideoAttributes (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_OrientationMode (VideoAttributes.getEnOrientationMode ()),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_ScreenAttributes (VideoAttributes.getRScreenAttributes ().getU16ScreenHeight (),
    VideoAttributes.getRScreenAttributes ().getU16ScreenWidth (),
    ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ScreenAspectRatio (VideoAttributes.getRScreenAttributes ().getEnScreenAspectRatio ()))), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueGetVideoSettingsError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueGetVideoSettingsError ()...");
    LOG_INFO ("data:%u", data);

    sendGetVideoSettingsError (getvideosettingsErrorCode (data), actJson);
}
// request SetOrientationMode
void Midw_smartphoneint_fiAdapterComponentStub::onSetOrientationModeRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetOrientationModeRequest >& corfoRequest)
{
    LOG_INFO ("onSetOrientationModeRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSetOrientationModeRequest (corfoRequest->getDeviceHandle (),::midw_smartphoneint_fi_types::T_e8_OrientationMode (corfoRequest->getOrientationMode ()),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSetOrientationModeResponse (const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode, act_t actJson)
{
    LOG_INFO ("vGlueSetOrientationModeResponse ()...");
    
    sendSetOrientationModeResponse (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ResponseCode (ResponseCode),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ErrorType (ErrorCode), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSetOrientationModeError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSetOrientationModeError ()...");
    LOG_INFO ("data:%u", data);

    sendSetOrientationModeError (setorientationmodeErrorCode (data), actJson);
}
// request SetScreenSize
void Midw_smartphoneint_fiAdapterComponentStub::onSetScreenSizeRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetScreenSizeRequest >& corfoRequest)
{
    LOG_INFO ("onSetScreenSizeRequest ()...");

    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSetScreenSizeRequest (::midw_smartphoneint_fi_types::T_ScreenAttributes (corfoRequest->getScreenAttributes ().getU16ScreenHeight (),
    corfoRequest->getScreenAttributes ().getU16ScreenWidth (),
    ::midw_smartphoneint_fi_types::T_e8_ScreenAspectRatio (corfoRequest->getScreenAttributes ().getEnScreenAspectRatio ())),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSetScreenSizeResponse ( act_t actJson)
{
    LOG_INFO ("vGlueSetScreenSizeResponse ()...");
    
    sendSetScreenSizeResponse (actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSetScreenSizeError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSetScreenSizeError ()...");
    LOG_INFO ("data:%u", data);

    sendSetScreenSizeError (setscreensizeErrorCode (data), actJson);
}
// request SetVideoBlockingMode
void Midw_smartphoneint_fiAdapterComponentStub::onSetVideoBlockingModeRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetVideoBlockingModeRequest >& corfoRequest)
{
    LOG_INFO ("onSetVideoBlockingModeRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSetVideoBlockingModeRequest (corfoRequest->getDeviceHandle (),::midw_smartphoneint_fi_types::T_e8_BlockingMode (corfoRequest->getBlockingMode ()),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSetVideoBlockingModeResponse (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode, act_t actJson)
{
    LOG_INFO ("vGlueSetVideoBlockingModeResponse ()...");
    
    LOG_INFO ("DeviceHandle:%u", DeviceHandle);
    sendSetVideoBlockingModeResponse (DeviceHandle,::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ResponseCode (ResponseCode),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ErrorType (ErrorCode), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSetVideoBlockingModeError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSetVideoBlockingModeError ()...");
    LOG_INFO ("data:%u", data);

    sendSetVideoBlockingModeError (setvideoblockingmodeErrorCode (data), actJson);
}
// request SetAudioBlockingMode
void Midw_smartphoneint_fiAdapterComponentStub::onSetAudioBlockingModeRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetAudioBlockingModeRequest >& corfoRequest)
{
    LOG_INFO ("onSetAudioBlockingModeRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSetAudioBlockingModeRequest (corfoRequest->getDeviceHandle (),::midw_smartphoneint_fi_types::T_e8_BlockingMode (corfoRequest->getBlockingMode ()),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSetAudioBlockingModeResponse (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode, act_t actJson)
{
    LOG_INFO ("vGlueSetAudioBlockingModeResponse ()...");
    
    LOG_INFO ("DeviceHandle:%u", DeviceHandle);
    sendSetAudioBlockingModeResponse (DeviceHandle,::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ResponseCode (ResponseCode),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ErrorType (ErrorCode), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSetAudioBlockingModeError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSetAudioBlockingModeError ()...");
    LOG_INFO ("data:%u", data);

    sendSetAudioBlockingModeError (setaudioblockingmodeErrorCode (data), actJson);
}
// request SetVehicleConfiguration
void Midw_smartphoneint_fiAdapterComponentStub::onSetVehicleConfigurationRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetVehicleConfigurationRequest >& corfoRequest)
{
    LOG_INFO ("onSetVehicleConfigurationRequest ()...");

    LOG_INFO ("SetConfiguration:%u", corfoRequest->getSetConfiguration ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSetVehicleConfigurationRequest (::midw_smartphoneint_fi_types::T_e8_Vehicle_Configuration (corfoRequest->getVehicleConfiguration ()),corfoRequest->getSetConfiguration (),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSetVehicleConfigurationResponse ( act_t actJson)
{
    LOG_INFO ("vGlueSetVehicleConfigurationResponse ()...");
    
    sendSetVehicleConfigurationResponse (actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSetVehicleConfigurationError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSetVehicleConfigurationError ()...");
    LOG_INFO ("data:%u", data);

    sendSetVehicleConfigurationError (setvehicleconfigurationErrorCode (data), actJson);
}
// request SendTouchEvent
void Midw_smartphoneint_fiAdapterComponentStub::onSendTouchEventRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SendTouchEventRequest >& corfoRequest)
{
    LOG_INFO ("onSendTouchEventRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    ::std::vector< ::std::vector< ::midw_smartphoneint_fi_types::T_TouchCoordinates > > rStdVector2TouchInfoList;
    rStdVector2TouchInfoList.clear ();
    if (0 == corfoRequest->getTouchData ().getTouchInfoList ().size ()) {
        LOG_INFO ("corfoRequest->getTouchData ().getTouchInfoList ()List  is empty...");
    }
    else {
        uint32 u32i = 0;
    
        for (::boost::ptr_vector< ::boost::ptr_vector< ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_TouchCoordinates > >::const_iterator \
        it1 = corfoRequest->getTouchData ().getTouchInfoList ().begin (); it1 != corfoRequest->getTouchData ().getTouchInfoList ().end (); ++it1)
        {
            LOG_INFO ("-------TouchInfoList---------------");
            LOG_INFO ("Current Index : %d", u32i);
            ::std::vector< ::midw_smartphoneint_fi_types::T_TouchCoordinates > rT_TouchCoordinates1;
            for (::boost::ptr_vector< ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_TouchCoordinates >::const_iterator \
            itT_TouchCoordinates = it1->begin (); itT_TouchCoordinates != it1->end (); ++itT_TouchCoordinates)
            {
                ::midw_smartphoneint_fi_types::T_TouchCoordinates rT_TouchCoordinates;
    
                rT_TouchCoordinates.setEnTouchMode (::midw_smartphoneint_fi_types::T_e8_TouchMode (itT_TouchCoordinates->getEnTouchMode ()));
                LOG_INFO ("U16XCoordinate : %u", itT_TouchCoordinates->getU16XCoordinate ());
    
       rT_TouchCoordinates.setU16XCoordinate (itT_TouchCoordinates->getU16XCoordinate () );
    
                LOG_INFO ("U16YCoordinate : %u", itT_TouchCoordinates->getU16YCoordinate ());
    
       rT_TouchCoordinates.setU16YCoordinate (itT_TouchCoordinates->getU16YCoordinate () );
    
    
                rT_TouchCoordinates1.push_back (rT_TouchCoordinates); 
            }
            rStdVector2TouchInfoList.push_back (rT_TouchCoordinates1);
            u32i++;
            LOG_INFO ("-------End of TouchInfoList---------------");
        }
    }
    
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSendTouchEventRequest (corfoRequest->getDeviceHandle (),::midw_smartphoneint_fi_types::T_TouchData (corfoRequest->getTouchData ().getU16TouchDescriptors (),
    rStdVector2TouchInfoList),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSendTouchEventResponse (const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode, act_t actJson)
{
    LOG_INFO ("vGlueSendTouchEventResponse ()...");
    
    sendSendTouchEventResponse (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ResponseCode (ResponseCode),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ErrorType (ErrorCode), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSendTouchEventError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSendTouchEventError ()...");
    LOG_INFO ("data:%u", data);

    sendSendTouchEventError (sendtoucheventErrorCode (data), actJson);
}
// request SendKeyEvent
void Midw_smartphoneint_fiAdapterComponentStub::onSendKeyEventRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SendKeyEventRequest >& corfoRequest)
{
    LOG_INFO ("onSendKeyEventRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSendKeyEventRequest (corfoRequest->getDeviceHandle (),::midw_smartphoneint_fi_types::T_e8_KeyMode (corfoRequest->getKeyMode ()),::midw_smartphoneint_fi_types::T_e32_KeyCode (corfoRequest->getKeyCode ()),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSendKeyEventResponse (const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode, act_t actJson)
{
    LOG_INFO ("vGlueSendKeyEventResponse ()...");
    
    sendSendKeyEventResponse (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ResponseCode (ResponseCode),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ErrorType (ErrorCode), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSendKeyEventError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSendKeyEventError ()...");
    LOG_INFO ("data:%u", data);

    sendSendKeyEventError (sendkeyeventErrorCode (data), actJson);
}
// request UpdateCertificateFile
void Midw_smartphoneint_fiAdapterComponentStub::onUpdateCertificateFileRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::UpdateCertificateFileRequest >& corfoRequest)
{
    LOG_INFO ("onUpdateCertificateFileRequest ()...");

    LOG_INFO ("CertificateFilePath:%s", corfoRequest->getCertificateFilePath ().c_str ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueUpdateCertificateFileRequest (corfoRequest->getCertificateFilePath (),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueUpdateCertificateFileResponse (const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode, act_t actJson)
{
    LOG_INFO ("vGlueUpdateCertificateFileResponse ()...");
    
    sendUpdateCertificateFileResponse (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ResponseCode (ResponseCode),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ErrorType (ErrorCode), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueUpdateCertificateFileError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueUpdateCertificateFileError ()...");
    LOG_INFO ("data:%u", data);

    sendUpdateCertificateFileError (updatecertificatefileErrorCode (data), actJson);
}
// request SetClientCapabilities
void Midw_smartphoneint_fiAdapterComponentStub::onSetClientCapabilitiesRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetClientCapabilitiesRequest >& corfoRequest)
{
    LOG_INFO ("onSetClientCapabilitiesRequest ()...");

    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSetClientCapabilitiesRequest (::midw_smartphoneint_fi_types::T_ClientCapabilities (::midw_smartphoneint_fi_types::T_KeyCapabilities (corfoRequest->getClientCapabilities ().getRKeyCapabilities ().getU16KnobKeySupport (),
    corfoRequest->getClientCapabilities ().getRKeyCapabilities ().getU16DeviceKeySupport (),
    corfoRequest->getClientCapabilities ().getRKeyCapabilities ().getU16MultimediaKeySupport (),
    corfoRequest->getClientCapabilities ().getRKeyCapabilities ().getU16MiscKeySupport (),
    corfoRequest->getClientCapabilities ().getRKeyCapabilities ().getU16MultiTouchSupport (),
    corfoRequest->getClientCapabilities ().getRKeyCapabilities ().getU16PointerTouchSupport ())),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSetClientCapabilitiesResponse ( act_t actJson)
{
    LOG_INFO ("vGlueSetClientCapabilitiesResponse ()...");
    
    sendSetClientCapabilitiesResponse (actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSetClientCapabilitiesError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSetClientCapabilitiesError ()...");
    LOG_INFO ("data:%u", data);

    sendSetClientCapabilitiesError (setclientcapabilitiesErrorCode (data), actJson);
}
// request AccessoryDisplayContext
void Midw_smartphoneint_fiAdapterComponentStub::onAccessoryDisplayContextRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::AccessoryDisplayContextRequest >& corfoRequest)
{
    LOG_INFO ("onAccessoryDisplayContextRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    LOG_INFO ("DisplayFlag:%u", corfoRequest->getDisplayFlag ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueAccessoryDisplayContextRequest (corfoRequest->getDeviceHandle (),corfoRequest->getDisplayFlag (),::midw_smartphoneint_fi_types::T_e8_DisplayContext (corfoRequest->getDisplayContext ()),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueAccessoryDisplayContextResponse ( act_t actJson)
{
    LOG_INFO ("vGlueAccessoryDisplayContextResponse ()...");
    
    sendAccessoryDisplayContextResponse (actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueAccessoryDisplayContextError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueAccessoryDisplayContextError ()...");
    LOG_INFO ("data:%u", data);

    sendAccessoryDisplayContextError (accessorydisplaycontextErrorCode (data), actJson);
}
// request SetDataServiceUsage
void Midw_smartphoneint_fiAdapterComponentStub::onSetDataServiceUsageRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetDataServiceUsageRequest >& corfoRequest)
{
    LOG_INFO ("onSetDataServiceUsageRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSetDataServiceUsageRequest (corfoRequest->getDeviceHandle (),::midw_smartphoneint_fi_types::T_e8_DeviceCategory (corfoRequest->getDeviceCategory ()),::midw_smartphoneint_fi_types::T_e8_DataServiceType (corfoRequest->getDataServiceType ()),::midw_smartphoneint_fi_types::T_e8_DataServiceUsage (corfoRequest->getDataServiceUsage ()),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSetDataServiceUsageResponse (const ::midw_smartphoneint_fi_types::T_e8_ResponseCode& ResponseCode,const ::midw_smartphoneint_fi_types::T_e8_ErrorType& ErrorCode, act_t actJson)
{
    LOG_INFO ("vGlueSetDataServiceUsageResponse ()...");
    
    sendSetDataServiceUsageResponse (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ResponseCode (ResponseCode),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_ErrorType (ErrorCode), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSetDataServiceUsageError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSetDataServiceUsageError ()...");
    LOG_INFO ("data:%u", data);

    sendSetDataServiceUsageError (setdataserviceusageErrorCode (data), actJson);
}
// request GetDeviceUsagePreference
void Midw_smartphoneint_fiAdapterComponentStub::onGetDeviceUsagePreferenceRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::GetDeviceUsagePreferenceRequest >& corfoRequest)
{
    LOG_INFO ("onGetDeviceUsagePreferenceRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueGetDeviceUsagePreferenceRequest (corfoRequest->getDeviceHandle (),::midw_smartphoneint_fi_types::T_e8_DeviceCategory (corfoRequest->getDeviceCategory ()),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueGetDeviceUsagePreferenceResponse (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_DeviceCategory& DeviceCategory,const ::midw_smartphoneint_fi_types::T_e8_EnabledInfo& EnabledInfo, act_t actJson)
{
    LOG_INFO ("vGlueGetDeviceUsagePreferenceResponse ()...");
    
    LOG_INFO ("DeviceHandle:%u", DeviceHandle);
    sendGetDeviceUsagePreferenceResponse (DeviceHandle,::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceCategory (DeviceCategory),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_EnabledInfo (EnabledInfo), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueGetDeviceUsagePreferenceError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueGetDeviceUsagePreferenceError ()...");
    LOG_INFO ("data:%u", data);

    sendGetDeviceUsagePreferenceError (getdeviceusagepreferenceErrorCode (data), actJson);
}
// request SetVehicleBTAddress
void Midw_smartphoneint_fiAdapterComponentStub::onSetVehicleBTAddressRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetVehicleBTAddressRequest >& corfoRequest)
{
    LOG_INFO ("onSetVehicleBTAddressRequest ()...");

    LOG_INFO ("BTAddress:%s", corfoRequest->getBTAddress ().c_str ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSetVehicleBTAddressRequest (corfoRequest->getBTAddress (),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSetVehicleBTAddressResponse ( act_t actJson)
{
    LOG_INFO ("vGlueSetVehicleBTAddressResponse ()...");
    
    sendSetVehicleBTAddressResponse (actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSetVehicleBTAddressError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSetVehicleBTAddressError ()...");
    LOG_INFO ("data:%u", data);

    sendSetVehicleBTAddressError (setvehiclebtaddressErrorCode (data), actJson);
}
// request SetDataServiceFilter
void Midw_smartphoneint_fiAdapterComponentStub::onSetDataServiceFilterRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetDataServiceFilterRequest >& corfoRequest)
{
    LOG_INFO ("onSetDataServiceFilterRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSetDataServiceFilterRequest (corfoRequest->getDeviceHandle (),::midw_smartphoneint_fi_types::T_e8_DeviceCategory (corfoRequest->getDeviceCategory ()),::midw_smartphoneint_fi_types::T_e8_DataServiceType (corfoRequest->getDataServiceType ()),::midw_smartphoneint_fi_types::T_e8_EnabledInfo (corfoRequest->getEnabledInfo ()),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSetDataServiceFilterResponse ( act_t actJson)
{
    LOG_INFO ("vGlueSetDataServiceFilterResponse ()...");
    
    sendSetDataServiceFilterResponse (actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSetDataServiceFilterError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSetDataServiceFilterError ()...");
    LOG_INFO ("data:%u", data);

    sendSetDataServiceFilterError (setdataservicefilterErrorCode (data), actJson);
}
// request InvokeBluetoothDeviceAction
void Midw_smartphoneint_fiAdapterComponentStub::onInvokeBluetoothDeviceActionRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::InvokeBluetoothDeviceActionRequest >& corfoRequest)
{
    LOG_INFO ("onInvokeBluetoothDeviceActionRequest ()...");

    LOG_INFO ("BluetoothDeviceHandle:%u", corfoRequest->getBluetoothDeviceHandle ());
    LOG_INFO ("ProjectionDeviceHandle:%u", corfoRequest->getProjectionDeviceHandle ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueInvokeBluetoothDeviceActionRequest (corfoRequest->getBluetoothDeviceHandle (),corfoRequest->getProjectionDeviceHandle (),::midw_smartphoneint_fi_types::T_e8_BTChangeInfo (corfoRequest->getBTChangeInfo ()),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueInvokeBluetoothDeviceActionResponse ( act_t actJson)
{
    LOG_INFO ("vGlueInvokeBluetoothDeviceActionResponse ()...");
    
    sendInvokeBluetoothDeviceActionResponse (actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueInvokeBluetoothDeviceActionError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueInvokeBluetoothDeviceActionError ()...");
    LOG_INFO ("data:%u", data);

    sendInvokeBluetoothDeviceActionError (invokebluetoothdeviceactionErrorCode (data), actJson);
}
// request AccessoryAudioContext
void Midw_smartphoneint_fiAdapterComponentStub::onAccessoryAudioContextRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::AccessoryAudioContextRequest >& corfoRequest)
{
    LOG_INFO ("onAccessoryAudioContextRequest ()...");

    LOG_INFO ("DeviceHandle:%u", corfoRequest->getDeviceHandle ());
    LOG_INFO ("AudioFlag:%u", corfoRequest->getAudioFlag ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueAccessoryAudioContextRequest (corfoRequest->getDeviceHandle (),corfoRequest->getAudioFlag (),::midw_smartphoneint_fi_types::T_e8_AudioContext (corfoRequest->getAudioContext ()),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueAccessoryAudioContextResponse ( act_t actJson)
{
    LOG_INFO ("vGlueAccessoryAudioContextResponse ()...");
    
    sendAccessoryAudioContextResponse (actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueAccessoryAudioContextError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueAccessoryAudioContextError ()...");
    LOG_INFO ("data:%u", data);

    sendAccessoryAudioContextError (accessoryaudiocontextErrorCode (data), actJson);
}
// request AccessoryAppState
void Midw_smartphoneint_fiAdapterComponentStub::onAccessoryAppStateRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::AccessoryAppStateRequest >& corfoRequest)
{
    LOG_INFO ("onAccessoryAppStateRequest ()...");

    m_poMidw_smartphoneint_fiAdapterComponent->vGlueAccessoryAppStateRequest (::midw_smartphoneint_fi_types::T_e8_SpeechAppState (corfoRequest->getAppStateSpeech ()),::midw_smartphoneint_fi_types::T_e8_PhoneAppState (corfoRequest->getAppStatePhone ()),::midw_smartphoneint_fi_types::T_e8_NavigationAppState (corfoRequest->getAppStateNavigation ()),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueAccessoryAppStateResponse ( act_t actJson)
{
    LOG_INFO ("vGlueAccessoryAppStateResponse ()...");
    
    sendAccessoryAppStateResponse (actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueAccessoryAppStateError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueAccessoryAppStateError ()...");
    LOG_INFO ("data:%u", data);

    sendAccessoryAppStateError (accessoryappstateErrorCode (data), actJson);
}
// request SetRegion
void Midw_smartphoneint_fiAdapterComponentStub::onSetRegionRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::SetRegionRequest >& corfoRequest)
{
    LOG_INFO ("onSetRegionRequest ()...");

    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSetRegionRequest (::midw_smartphoneint_fi_types::T_e8_Region (corfoRequest->getRegion ()),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSetRegionResponse ( act_t actJson)
{
    LOG_INFO ("vGlueSetRegionResponse ()...");
    
    sendSetRegionResponse (actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSetRegionError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSetRegionError ()...");
    LOG_INFO ("data:%u", data);

    sendSetRegionError (setregionErrorCode (data), actJson);
}
// request DiPORoleSwitchRequired
void Midw_smartphoneint_fiAdapterComponentStub::onDiPORoleSwitchRequiredRequest (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiService::DiPORoleSwitchRequiredRequest >& corfoRequest)
{
    LOG_INFO ("onDiPORoleSwitchRequiredRequest ()...");

    LOG_INFO ("u8DeviceTag:%u", corfoRequest->getU8DeviceTag ());
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueDiPORoleSwitchRequiredRequest (corfoRequest->getU8DeviceTag (),corfoRequest->getAct () );
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueDiPORoleSwitchRequiredResponse (uint8 U8DeviceTag,const ::midw_smartphoneint_fi_types::T_e8_DiPOSwitchReqResponse& E8DiPOSwitchReqResponse, act_t actJson)
{
    LOG_INFO ("vGlueDiPORoleSwitchRequiredResponse ()...");
    
    LOG_INFO ("u8DeviceTag:%u", U8DeviceTag);
    sendDiPORoleSwitchRequiredResponse (U8DeviceTag,::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DiPOSwitchReqResponse (E8DiPOSwitchReqResponse), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub:: vGlueDiPORoleSwitchRequiredError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueDiPORoleSwitchRequiredError ()...");
    LOG_INFO ("data:%u", data);

    sendDiPORoleSwitchRequiredError (diporoleswitchrequiredErrorCode (data), actJson);
}
// property DeviceStatusInfo
void Midw_smartphoneint_fiAdapterComponentStub::onDeviceStatusInfoRegister (bool isFirst, act_t actJson) {
    LOG_INFO ("onDeviceStatusInfoRegister ()...");
    _registerPropertyBuffer.push_back (actJson);
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueDeviceStatusInfoRegister (actJson);
}   

void Midw_smartphoneint_fiAdapterComponentStub::onDeviceStatusInfoDeregister (bool isLast){
    LOG_INFO ("onDeviceStatusInfoDeregister ()...");
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueDeviceStatusInfoDeregister ();
}


void Midw_smartphoneint_fiAdapterComponentStub:: vGlueDeviceStatusInfoRegisterError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueDeviceStatusInfoRegisterError ()...");
    LOG_INFO ("data:%u", data);

    sendDeviceStatusInfoRegisterError (devicestatusinfoErrorCode (data), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueDeviceStatusInfoPropertyUpdate (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionType& DeviceConnectionType,const ::midw_smartphoneint_fi_types::T_e8_DeviceStatusInfo& DeviceStatus, act_t actJson) {
    LOG_INFO ("vGlueDeviceStatusInfoPropertyUpdate ()...");
    
    /*Check for property register notification*/
    vector<uint32>::iterator it = find (_registerPropertyBuffer.begin (), _registerPropertyBuffer.end (), actJson);
    if (it != _registerPropertyBuffer.end () ) {
        _registerPropertyBuffer.erase (it);
        sendDeviceStatusInfoUpdate (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::devicestatusinfoInfoValueStruct (DeviceHandle,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceConnectionType (DeviceConnectionType),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceStatusInfo (DeviceStatus)), actJson);
    }
    else{
        setDeviceStatusInfo (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::devicestatusinfoInfoValueStruct (DeviceHandle,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceConnectionType (DeviceConnectionType),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceStatusInfo (DeviceStatus)));
    }
}


// property AppStatusInfo
void Midw_smartphoneint_fiAdapterComponentStub::onAppStatusInfoRegister (bool isFirst, act_t actJson) {
    LOG_INFO ("onAppStatusInfoRegister ()...");
    _registerPropertyBuffer.push_back (actJson);
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueAppStatusInfoRegister (actJson);
}   

void Midw_smartphoneint_fiAdapterComponentStub::onAppStatusInfoDeregister (bool isLast){
    LOG_INFO ("onAppStatusInfoDeregister ()...");
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueAppStatusInfoDeregister ();
}


void Midw_smartphoneint_fiAdapterComponentStub:: vGlueAppStatusInfoRegisterError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueAppStatusInfoRegisterError ()...");
    LOG_INFO ("data:%u", data);

    sendAppStatusInfoRegisterError (appstatusinfoErrorCode (data), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueAppStatusInfoPropertyUpdate (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionType& DeviceConnectionType,const ::midw_smartphoneint_fi_types::T_e8_AppStatusInfo& AppStatus, act_t actJson) {
    LOG_INFO ("vGlueAppStatusInfoPropertyUpdate ()...");
    
    /*Check for property register notification*/
    vector<uint32>::iterator it = find (_registerPropertyBuffer.begin (), _registerPropertyBuffer.end (), actJson);
    if (it != _registerPropertyBuffer.end () ) {
        _registerPropertyBuffer.erase (it);
        sendAppStatusInfoUpdate (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::appstatusinfoInfoValueStruct (DeviceHandle,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceConnectionType (DeviceConnectionType),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_AppStatusInfo (AppStatus)), actJson);
    }
    else{
        setAppStatusInfo (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::appstatusinfoInfoValueStruct (DeviceHandle,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceConnectionType (DeviceConnectionType),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_AppStatusInfo (AppStatus)));
    }
}


// property DAPStatusInfo
void Midw_smartphoneint_fiAdapterComponentStub::onDAPStatusInfoRegister (bool isFirst, act_t actJson) {
    LOG_INFO ("onDAPStatusInfoRegister ()...");
    _registerPropertyBuffer.push_back (actJson);
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueDAPStatusInfoRegister (actJson);
}   

void Midw_smartphoneint_fiAdapterComponentStub::onDAPStatusInfoDeregister (bool isLast){
    LOG_INFO ("onDAPStatusInfoDeregister ()...");
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueDAPStatusInfoDeregister ();
}


void Midw_smartphoneint_fiAdapterComponentStub:: vGlueDAPStatusInfoRegisterError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueDAPStatusInfoRegisterError ()...");
    LOG_INFO ("data:%u", data);

    sendDAPStatusInfoRegisterError (dapstatusinfoErrorCode (data), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueDAPStatusInfoPropertyUpdate (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionType& DeviceConnectionType,const ::midw_smartphoneint_fi_types::T_e8_DAPStatus& DAPStatus, act_t actJson) {
    LOG_INFO ("vGlueDAPStatusInfoPropertyUpdate ()...");
    
    /*Check for property register notification*/
    vector<uint32>::iterator it = find (_registerPropertyBuffer.begin (), _registerPropertyBuffer.end (), actJson);
    if (it != _registerPropertyBuffer.end () ) {
        _registerPropertyBuffer.erase (it);
        sendDAPStatusInfoUpdate (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::dapstatusinfoInfoValueStruct (DeviceHandle,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceConnectionType (DeviceConnectionType),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DAPStatus (DAPStatus)), actJson);
    }
    else{
        setDAPStatusInfo (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::dapstatusinfoInfoValueStruct (DeviceHandle,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceConnectionType (DeviceConnectionType),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DAPStatus (DAPStatus)));
    }
}


// property NotificationInfo
void Midw_smartphoneint_fiAdapterComponentStub::onNotificationInfoRegister (bool isFirst, act_t actJson) {
    LOG_INFO ("onNotificationInfoRegister ()...");
    _registerPropertyBuffer.push_back (actJson);
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueNotificationInfoRegister (actJson);
}   

void Midw_smartphoneint_fiAdapterComponentStub::onNotificationInfoDeregister (bool isLast){
    LOG_INFO ("onNotificationInfoDeregister ()...");
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueNotificationInfoDeregister ();
}


void Midw_smartphoneint_fiAdapterComponentStub:: vGlueNotificationInfoRegisterError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueNotificationInfoRegisterError ()...");
    LOG_INFO ("data:%u", data);

    sendNotificationInfoRegisterError (notificationinfoErrorCode (data), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueNotificationInfoPropertyUpdate (uint32 DeviceHandle,uint32 AppHandle,const ::midw_smartphoneint_fi_types::T_NotificationData& NotificationData, act_t actJson) {
    LOG_INFO ("vGlueNotificationInfoPropertyUpdate ()...");
    
    ::boost::ptr_vector< ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_IconAttributes > rPtrVector1NotificationIconList;
    rPtrVector1NotificationIconList.clear ();
    if (0 == NotificationData.getNotificationIconList ().size ()){
        LOG_INFO ("NotificationData.getNotificationIconList ()List  is empty...");
    } else {
        uint32 u32i = 0;
    
        for (::std::vector< ::midw_smartphoneint_fi_types::T_IconAttributes >::const_iterator \
        itT_IconAttributes = NotificationData.getNotificationIconList ().begin (); itT_IconAttributes != NotificationData.getNotificationIconList ().end (); ++itT_IconAttributes)
        {
            LOG_INFO ("-------NotificationIconList---------------");
            LOG_INFO ("Current Index : %d", u32i);
            ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_IconAttributes *rT_IconAttributes;
            rT_IconAttributes = new ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_IconAttributes;
    
            LOG_INFO ("U32IconWidth : %u", itT_IconAttributes->getU32IconWidth ());
    
       rT_IconAttributes->setU32IconWidth (itT_IconAttributes->getU32IconWidth ());
    
            LOG_INFO ("U32IconHeight : %u", itT_IconAttributes->getU32IconHeight ());
    
       rT_IconAttributes->setU32IconHeight (itT_IconAttributes->getU32IconHeight ());
    
            LOG_INFO ("U32IconDepth : %u", itT_IconAttributes->getU32IconDepth ());
    
       rT_IconAttributes->setU32IconDepth (itT_IconAttributes->getU32IconDepth ());
    
            rT_IconAttributes->setEnIconMimeType (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_IconMimeType (itT_IconAttributes->getEnIconMimeType ()));
            LOG_INFO ("SzIconURL : %s", itT_IconAttributes->getSzIconURL ().c_str ());
    
       rT_IconAttributes->setSzIconURL (itT_IconAttributes->getSzIconURL ());
    
    
            rPtrVector1NotificationIconList.push_back (rT_IconAttributes);
            u32i++;
            LOG_INFO ("-------End of NotificationIconList---------------");
        }
    }
    
    ::boost::ptr_vector< ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_NotificationAction > rPtrVector1NotificationActionList;
    rPtrVector1NotificationActionList.clear ();
    if (0 == NotificationData.getNotificationActionList ().size ()){
        LOG_INFO ("NotificationData.getNotificationActionList ()List  is empty...");
    } else {
        uint32 u32i = 0;
    
        for (::std::vector< ::midw_smartphoneint_fi_types::T_NotificationAction >::const_iterator \
        itT_NotificationAction = NotificationData.getNotificationActionList ().begin (); itT_NotificationAction != NotificationData.getNotificationActionList ().end (); ++itT_NotificationAction)
        {
            LOG_INFO ("-------NotificationActionList---------------");
            LOG_INFO ("Current Index : %d", u32i);
            ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_NotificationAction *rT_NotificationAction;
            rT_NotificationAction = new ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_NotificationAction;
    
            LOG_INFO ("U16NotificationActionID : %u", itT_NotificationAction->getU16NotificationActionID ());
    
       rT_NotificationAction->setU16NotificationActionID (itT_NotificationAction->getU16NotificationActionID ());
    
            LOG_INFO ("SzNotificationActionName : %s", itT_NotificationAction->getSzNotificationActionName ().c_str ());
    
       rT_NotificationAction->setSzNotificationActionName (itT_NotificationAction->getSzNotificationActionName ());
    
            LOG_INFO ("U16NotificationActionIconCount : %u", itT_NotificationAction->getU16NotificationActionIconCount ());
    
       rT_NotificationAction->setU16NotificationActionIconCount (itT_NotificationAction->getU16NotificationActionIconCount ());
    
    ::boost::ptr_vector< ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_IconAttributes > rPtrVector1NotificationActionIconList;
    rPtrVector1NotificationActionIconList.clear ();
    if (0 == itT_NotificationAction->getNotificationActionIconList ().size ()){
        LOG_INFO ("itT_NotificationAction->getNotificationActionIconList ()List  is empty...");
    } else {
        uint32 u32i = 0;
    
        for (::std::vector< ::midw_smartphoneint_fi_types::T_IconAttributes >::const_iterator \
        itT_IconAttributes = itT_NotificationAction->getNotificationActionIconList ().begin (); itT_IconAttributes != itT_NotificationAction->getNotificationActionIconList ().end (); ++itT_IconAttributes)
        {
            LOG_INFO ("-------NotificationActionIconList---------------");
            LOG_INFO ("Current Index : %d", u32i);
            ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_IconAttributes *rT_IconAttributes;
            rT_IconAttributes = new ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_IconAttributes;
    
            LOG_INFO ("U32IconWidth : %u", itT_IconAttributes->getU32IconWidth ());
    
       rT_IconAttributes->setU32IconWidth (itT_IconAttributes->getU32IconWidth ());
    
            LOG_INFO ("U32IconHeight : %u", itT_IconAttributes->getU32IconHeight ());
    
       rT_IconAttributes->setU32IconHeight (itT_IconAttributes->getU32IconHeight ());
    
            LOG_INFO ("U32IconDepth : %u", itT_IconAttributes->getU32IconDepth ());
    
       rT_IconAttributes->setU32IconDepth (itT_IconAttributes->getU32IconDepth ());
    
            rT_IconAttributes->setEnIconMimeType (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_IconMimeType (itT_IconAttributes->getEnIconMimeType ()));
            LOG_INFO ("SzIconURL : %s", itT_IconAttributes->getSzIconURL ().c_str ());
    
       rT_IconAttributes->setSzIconURL (itT_IconAttributes->getSzIconURL ());
    
    
            rPtrVector1NotificationActionIconList.push_back (rT_IconAttributes);
            u32i++;
            LOG_INFO ("-------End of NotificationActionIconList---------------");
        }
    }
    
            rT_NotificationAction->setNotificationActionIconList (rPtrVector1NotificationActionIconList);
    
    
            rPtrVector1NotificationActionList.push_back (rT_NotificationAction);
            u32i++;
            LOG_INFO ("-------End of NotificationActionList---------------");
        }
    }
    
    
    /*Check for property register notification*/
    vector<uint32>::iterator it = find (_registerPropertyBuffer.begin (), _registerPropertyBuffer.end (), actJson);
    if (it != _registerPropertyBuffer.end () ) {
        _registerPropertyBuffer.erase (it);
        sendNotificationInfoUpdate (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::notificationinfoInfoValueStruct (DeviceHandle,
        AppHandle,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_NotificationData (NotificationData.getU16NotificationID (),
        NotificationData.getSzNotificationTitle (),
        NotificationData.getSzNotificationBody (),
        NotificationData.getU16NotificationIconCount (),
        rPtrVector1NotificationIconList,NotificationData.getU16NotificationAppID (),
        rPtrVector1NotificationActionList)), actJson);
    }
    else{
        setNotificationInfo (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::notificationinfoInfoValueStruct (DeviceHandle,
        AppHandle,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_NotificationData (NotificationData.getU16NotificationID (),
        NotificationData.getSzNotificationTitle (),
        NotificationData.getSzNotificationBody (),
        NotificationData.getU16NotificationIconCount (),
        rPtrVector1NotificationIconList,NotificationData.getU16NotificationAppID (),
        rPtrVector1NotificationActionList)));
    }
}


// property ApplicationMetaData
void Midw_smartphoneint_fiAdapterComponentStub::onApplicationMetaDataRegister (bool isFirst, act_t actJson) {
    LOG_INFO ("onApplicationMetaDataRegister ()...");
    _registerPropertyBuffer.push_back (actJson);
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueApplicationMetaDataRegister (actJson);
}   

void Midw_smartphoneint_fiAdapterComponentStub::onApplicationMetaDataDeregister (bool isLast){
    LOG_INFO ("onApplicationMetaDataDeregister ()...");
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueApplicationMetaDataDeregister ();
}


void Midw_smartphoneint_fiAdapterComponentStub:: vGlueApplicationMetaDataRegisterError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueApplicationMetaDataRegisterError ()...");
    LOG_INFO ("data:%u", data);

    sendApplicationMetaDataRegisterError (applicationmetadataErrorCode (data), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueApplicationMetaDataPropertyUpdate (uint32 DeviceHandle,bool MetaDataValid,const ::midw_smartphoneint_fi_types::T_ApplicationMetaData& ApplicationMetaData, act_t actJson) {
    LOG_INFO ("vGlueApplicationMetaDataPropertyUpdate ()...");
    
    /*Check for property register notification*/
    vector<uint32>::iterator it = find (_registerPropertyBuffer.begin (), _registerPropertyBuffer.end (), actJson);
    if (it != _registerPropertyBuffer.end () ) {
        _registerPropertyBuffer.erase (it);
        sendApplicationMetaDataUpdate (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::applicationmetadataInfoValueStruct (DeviceHandle,
        MetaDataValid,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_ApplicationMetaData (ApplicationMetaData.getArtist (),
        ApplicationMetaData.getTitle (),
        ApplicationMetaData.getAlbum (),
        ApplicationMetaData.getGenre (),
        ApplicationMetaData.getTotalPlayTime (),
        ApplicationMetaData.getElapsedPlayTime (),
        ApplicationMetaData.getImageUrl (),
        ApplicationMetaData.getImageSize (),
        ApplicationMetaData.getImageMIMEType (),
        ApplicationMetaData.getAppName (),
        ApplicationMetaData.getPhoneCaller (),
        ApplicationMetaData.getPhoneCallInfo ())), actJson);
    }
    else{
        setApplicationMetaData (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::applicationmetadataInfoValueStruct (DeviceHandle,
        MetaDataValid,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_ApplicationMetaData (ApplicationMetaData.getArtist (),
        ApplicationMetaData.getTitle (),
        ApplicationMetaData.getAlbum (),
        ApplicationMetaData.getGenre (),
        ApplicationMetaData.getTotalPlayTime (),
        ApplicationMetaData.getElapsedPlayTime (),
        ApplicationMetaData.getImageUrl (),
        ApplicationMetaData.getImageSize (),
        ApplicationMetaData.getImageMIMEType (),
        ApplicationMetaData.getAppName (),
        ApplicationMetaData.getPhoneCaller (),
        ApplicationMetaData.getPhoneCallInfo ())));
    }
}


// property DeviceDisplayContext
void Midw_smartphoneint_fiAdapterComponentStub::onDeviceDisplayContextRegister (bool isFirst, act_t actJson) {
    LOG_INFO ("onDeviceDisplayContextRegister ()...");
    _registerPropertyBuffer.push_back (actJson);
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueDeviceDisplayContextRegister (actJson);
}   

void Midw_smartphoneint_fiAdapterComponentStub::onDeviceDisplayContextDeregister (bool isLast){
    LOG_INFO ("onDeviceDisplayContextDeregister ()...");
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueDeviceDisplayContextDeregister ();
}


void Midw_smartphoneint_fiAdapterComponentStub:: vGlueDeviceDisplayContextRegisterError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueDeviceDisplayContextRegisterError ()...");
    LOG_INFO ("data:%u", data);

    sendDeviceDisplayContextRegisterError (devicedisplaycontextErrorCode (data), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueDeviceDisplayContextPropertyUpdate (uint32 DeviceHandle,bool DisplayFlag,const ::midw_smartphoneint_fi_types::T_e8_DisplayContext& DisplayContext, act_t actJson) {
    LOG_INFO ("vGlueDeviceDisplayContextPropertyUpdate ()...");
    
    /*Check for property register notification*/
    vector<uint32>::iterator it = find (_registerPropertyBuffer.begin (), _registerPropertyBuffer.end (), actJson);
    if (it != _registerPropertyBuffer.end () ) {
        _registerPropertyBuffer.erase (it);
        sendDeviceDisplayContextUpdate (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::devicedisplaycontextInfoValueStruct (DeviceHandle,
        DisplayFlag,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DisplayContext (DisplayContext)), actJson);
    }
    else{
        setDeviceDisplayContext (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::devicedisplaycontextInfoValueStruct (DeviceHandle,
        DisplayFlag,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DisplayContext (DisplayContext)));
    }
}


// property DataServiceInfo
void Midw_smartphoneint_fiAdapterComponentStub::onDataServiceInfoRegister (bool isFirst, act_t actJson) {
    LOG_INFO ("onDataServiceInfoRegister ()...");
    _registerPropertyBuffer.push_back (actJson);
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueDataServiceInfoRegister (actJson);
}   

void Midw_smartphoneint_fiAdapterComponentStub::onDataServiceInfoDeregister (bool isLast){
    LOG_INFO ("onDataServiceInfoDeregister ()...");
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueDataServiceInfoDeregister ();
}


void Midw_smartphoneint_fiAdapterComponentStub:: vGlueDataServiceInfoRegisterError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueDataServiceInfoRegisterError ()...");
    LOG_INFO ("data:%u", data);

    sendDataServiceInfoRegisterError (dataserviceinfoErrorCode (data), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueDataServiceInfoPropertyUpdate (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_DeviceCategory& DeviceCategory,const ::midw_smartphoneint_fi_types::T_e8_DataServiceType& DataServiceType,const ::midw_smartphoneint_fi_types::T_e8_DataServiceInfoType& DataServiceInfoType, act_t actJson) {
    LOG_INFO ("vGlueDataServiceInfoPropertyUpdate ()...");
    
    /*Check for property register notification*/
    vector<uint32>::iterator it = find (_registerPropertyBuffer.begin (), _registerPropertyBuffer.end (), actJson);
    if (it != _registerPropertyBuffer.end () ) {
        _registerPropertyBuffer.erase (it);
        sendDataServiceInfoUpdate (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::dataserviceinfoInfoValueStruct (DeviceHandle,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceCategory (DeviceCategory),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DataServiceType (DataServiceType),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DataServiceInfoType (DataServiceInfoType)), actJson);
    }
    else{
        setDataServiceInfo (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::dataserviceinfoInfoValueStruct (DeviceHandle,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceCategory (DeviceCategory),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DataServiceType (DataServiceType),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DataServiceInfoType (DataServiceInfoType)));
    }
}


// property BluetoothDeviceStatus
void Midw_smartphoneint_fiAdapterComponentStub::onBluetoothDeviceStatusRegister (bool isFirst, act_t actJson) {
    LOG_INFO ("onBluetoothDeviceStatusRegister ()...");
    _registerPropertyBuffer.push_back (actJson);
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueBluetoothDeviceStatusRegister (actJson);
}   

void Midw_smartphoneint_fiAdapterComponentStub::onBluetoothDeviceStatusDeregister (bool isLast){
    LOG_INFO ("onBluetoothDeviceStatusDeregister ()...");
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueBluetoothDeviceStatusDeregister ();
}


void Midw_smartphoneint_fiAdapterComponentStub:: vGlueBluetoothDeviceStatusRegisterError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueBluetoothDeviceStatusRegisterError ()...");
    LOG_INFO ("data:%u", data);

    sendBluetoothDeviceStatusRegisterError (bluetoothdevicestatusErrorCode (data), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueBluetoothDeviceStatusPropertyUpdate (uint32 BluetoothDeviceHandle,uint32 ProjectionDeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_BTChangeInfo& BTChangeInfo,bool CallActiveStatus, act_t actJson) {
    LOG_INFO ("vGlueBluetoothDeviceStatusPropertyUpdate ()...");
    
    /*Check for property register notification*/
    vector<uint32>::iterator it = find (_registerPropertyBuffer.begin (), _registerPropertyBuffer.end (), actJson);
    if (it != _registerPropertyBuffer.end () ) {
        _registerPropertyBuffer.erase (it);
        sendBluetoothDeviceStatusUpdate (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::bluetoothdevicestatusInfoValueStruct (BluetoothDeviceHandle,
        ProjectionDeviceHandle,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_BTChangeInfo (BTChangeInfo),CallActiveStatus), actJson);
    }
    else{
        setBluetoothDeviceStatus (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::bluetoothdevicestatusInfoValueStruct (BluetoothDeviceHandle,
        ProjectionDeviceHandle,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_BTChangeInfo (BTChangeInfo),CallActiveStatus));
    }
}


// property DeviceAudioContext
void Midw_smartphoneint_fiAdapterComponentStub::onDeviceAudioContextRegister (bool isFirst, act_t actJson) {
    LOG_INFO ("onDeviceAudioContextRegister ()...");
    _registerPropertyBuffer.push_back (actJson);
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueDeviceAudioContextRegister (actJson);
}   

void Midw_smartphoneint_fiAdapterComponentStub::onDeviceAudioContextDeregister (bool isLast){
    LOG_INFO ("onDeviceAudioContextDeregister ()...");
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueDeviceAudioContextDeregister ();
}


void Midw_smartphoneint_fiAdapterComponentStub:: vGlueDeviceAudioContextRegisterError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueDeviceAudioContextRegisterError ()...");
    LOG_INFO ("data:%u", data);

    sendDeviceAudioContextRegisterError (deviceaudiocontextErrorCode (data), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueDeviceAudioContextPropertyUpdate (uint32 DeviceHandle,bool AudioFlag,const ::midw_smartphoneint_fi_types::T_e8_AudioContext& AudioContext, act_t actJson) {
    LOG_INFO ("vGlueDeviceAudioContextPropertyUpdate ()...");
    
    /*Check for property register notification*/
    vector<uint32>::iterator it = find (_registerPropertyBuffer.begin (), _registerPropertyBuffer.end (), actJson);
    if (it != _registerPropertyBuffer.end () ) {
        _registerPropertyBuffer.erase (it);
        sendDeviceAudioContextUpdate (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::deviceaudiocontextInfoValueStruct (DeviceHandle,
        AudioFlag,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_AudioContext (AudioContext)), actJson);
    }
    else{
        setDeviceAudioContext (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::deviceaudiocontextInfoValueStruct (DeviceHandle,
        AudioFlag,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_AudioContext (AudioContext)));
    }
}


// property DiPOAppStatusInfo
void Midw_smartphoneint_fiAdapterComponentStub::onDiPOAppStatusInfoRegister (bool isFirst, act_t actJson) {
    LOG_INFO ("onDiPOAppStatusInfoRegister ()...");
    _registerPropertyBuffer.push_back (actJson);
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueDiPOAppStatusInfoRegister (actJson);
}   

void Midw_smartphoneint_fiAdapterComponentStub::onDiPOAppStatusInfoDeregister (bool isLast){
    LOG_INFO ("onDiPOAppStatusInfoDeregister ()...");
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueDiPOAppStatusInfoDeregister ();
}


void Midw_smartphoneint_fiAdapterComponentStub:: vGlueDiPOAppStatusInfoRegisterError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueDiPOAppStatusInfoRegisterError ()...");
    LOG_INFO ("data:%u", data);

    sendDiPOAppStatusInfoRegisterError (dipoappstatusinfoErrorCode (data), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueDiPOAppStatusInfoPropertyUpdate (const ::midw_smartphoneint_fi_types::T_e8_SpeechAppState& AppStateSpeech,const ::midw_smartphoneint_fi_types::T_e8_PhoneAppState& AppStatePhone,const ::midw_smartphoneint_fi_types::T_e8_NavigationAppState& AppStateNavigation, act_t actJson) {
    LOG_INFO ("vGlueDiPOAppStatusInfoPropertyUpdate ()...");
    
    /*Check for property register notification*/
    vector<uint32>::iterator it = find (_registerPropertyBuffer.begin (), _registerPropertyBuffer.end (), actJson);
    if (it != _registerPropertyBuffer.end () ) {
        _registerPropertyBuffer.erase (it);
        sendDiPOAppStatusInfoUpdate (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::dipoappstatusinfoInfoValueStruct (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_SpeechAppState (AppStateSpeech),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_PhoneAppState (AppStatePhone),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_NavigationAppState (AppStateNavigation)), actJson);
    }
    else{
        setDiPOAppStatusInfo (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::dipoappstatusinfoInfoValueStruct (::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_SpeechAppState (AppStateSpeech),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_PhoneAppState (AppStatePhone),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_NavigationAppState (AppStateNavigation)));
    }
}


// property SessionStatusInfo
void Midw_smartphoneint_fiAdapterComponentStub::onSessionStatusInfoRegister (bool isFirst, act_t actJson) {
    LOG_INFO ("onSessionStatusInfoRegister ()...");
    _registerPropertyBuffer.push_back (actJson);
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSessionStatusInfoRegister (actJson);
}   

void Midw_smartphoneint_fiAdapterComponentStub::onSessionStatusInfoDeregister (bool isLast){
    LOG_INFO ("onSessionStatusInfoDeregister ()...");
    m_poMidw_smartphoneint_fiAdapterComponent->vGlueSessionStatusInfoDeregister ();
}


void Midw_smartphoneint_fiAdapterComponentStub:: vGlueSessionStatusInfoRegisterError (uint32 data, act_t actJson){
    LOG_INFO ("vGlueSessionStatusInfoRegisterError ()...");
    LOG_INFO ("data:%u", data);

    sendSessionStatusInfoRegisterError (sessionstatusinfoErrorCode (data), actJson);
}

void Midw_smartphoneint_fiAdapterComponentStub::vGlueSessionStatusInfoPropertyUpdate (uint32 DeviceHandle,const ::midw_smartphoneint_fi_types::T_e8_DeviceCategory& DeviceCategory,const ::midw_smartphoneint_fi_types::T_e8_SessionStatus& SessionStatus, act_t actJson) {
    LOG_INFO ("vGlueSessionStatusInfoPropertyUpdate ()...");
    
    /*Check for property register notification*/
    vector<uint32>::iterator it = find (_registerPropertyBuffer.begin (), _registerPropertyBuffer.end (), actJson);
    if (it != _registerPropertyBuffer.end () ) {
        _registerPropertyBuffer.erase (it);
        sendSessionStatusInfoUpdate (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::sessionstatusinfoInfoValueStruct (DeviceHandle,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceCategory (DeviceCategory),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_SessionStatus (SessionStatus)), actJson);
    }
    else{
        setSessionStatusInfo (::midw_smartphoneint_fi::midw_smartphoneint_fiAuxiliaryTypes::sessionstatusinfoInfoValueStruct (DeviceHandle,
        ::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_DeviceCategory (DeviceCategory),::midw_smartphoneint_fi::midw_smartphoneint_fi_typesTypes::T_e8_SessionStatus (SessionStatus)));
    }
}

} // namespace adapter
} // namespace midw_smartphoneint_fiComponent
