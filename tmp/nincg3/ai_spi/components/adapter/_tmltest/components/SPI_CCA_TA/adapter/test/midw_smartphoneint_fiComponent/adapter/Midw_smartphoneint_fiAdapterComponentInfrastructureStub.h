/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#ifndef MIDW_SMARTPHONEINT_FIADAPTERCOMPONENTINFRASTRUCTURESTUB_H
#define MIDW_SMARTPHONEINT_FIADAPTERCOMPONENTINFRASTRUCTURESTUB_H

#include "InfrastructureMidw_smartphoneint_fiServiceStub.h"
#include "asf/core/Logger.h"
#include <iostream>

namespace midw_smartphoneint_fiComponent {
namespace adapter {

class Midw_smartphoneint_fiAdapterComponentInfrastructureStub : public ::midw_smartphoneint_fi::InfrastructureMidw_smartphoneint_fiService::InfrastructureMidw_smartphoneint_fiServiceStub {
    
    public:
    Midw_smartphoneint_fiAdapterComponentInfrastructureStub ();
    ~Midw_smartphoneint_fiAdapterComponentInfrastructureStub ();
    void setPropertyStubAvailable (bool enable);
    
    private:
    bool stubAvailable;
    
    DECLARE_CLASS_LOGGER ();
    
};

} // namespace adapter
} // namespace midw_smartphoneint_fiComponent

#endif // MIDW_SMARTPHONEINT_FIADAPTERCOMPONENTINFRASTRUCTURESTUB_H
