/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#ifndef MIDW_SMARTPHONEINT_FIADAPTERCOMPONENT_H
#define MIDW_SMARTPHONEINT_FIADAPTERCOMPONENT_H

#include "BaseComponent.h"
#include "Midw_smartphoneint_fiAdapterComponentInfrastructureStub.h"
#include "Midw_smartphoneint_fiService.h"
#include "Midw_smartphoneint_fiServiceStub.h"
#include "asf/core/Logger.h"
#include "boost/shared_ptr.hpp"
#include "midw_smartphoneint_fiClientBase.h"
#include "midw_smartphoneint_fiProxy.h"
#include "midw_smartphoneint_fi_types.h"
#include <map>

using namespace ::asf::core;
using namespace std;
using namespace ::midw_smartphoneint_fi;

namespace midw_smartphoneint_fiComponent {
namespace adapter {

using namespace ::midw_smartphoneint_fi;
using namespace ::midw_smartphoneint_fi_types;

class Midw_smartphoneint_fiAdapterComponentStub;

class Midw_smartphoneint_fiAdapterComponent :public BaseComponent, \
ServiceAvailableIF, \
::midw_smartphoneint_fi::midw_smartphoneint_fiClientBase
{
public:
Midw_smartphoneint_fiAdapterComponent ();
virtual ~Midw_smartphoneint_fiAdapterComponent ();

/*ServiceAvailableIF*/
void onAvailable\
(const ::boost::shared_ptr< ::asf::core::Proxy >& rfoProxy, const ::asf::core::ServiceStateChange& corfoStateChange);
void onUnavailable\
(const ::boost::shared_ptr< ::asf::core::Proxy >& rfoProxy, const ::asf::core::ServiceStateChange& corfoStateChange);
void setPropertyStubAvailable (bool enable);

// Callback 'AccessoryAppStateCallbackIF'
// Glue function vGlueAccessoryAppStateRequest
/**
 * This function will redirect the request  AccessoryAppStateRequest to
 * the service. It uses glue function vGlueAccessoryAppStateError (...)
 * function for that<b>Documentation of 'AccessoryAppState'</b>:If the
 * meaning of "AccessoryAppState" isn't clear, then there should be a
 * description here.
 */
void vGlueAccessoryAppStateRequest (const ::midw_smartphoneint_fi_types::T_e8_SpeechAppState& AppStateSpeech, const ::midw_smartphoneint_fi_types::T_e8_PhoneAppState& AppStatePhone, const ::midw_smartphoneint_fi_types::T_e8_NavigationAppState& AppStateNavigation,act_t actJson);
// Callback 'AccessoryAudioContextCallbackIF'
// Glue function vGlueAccessoryAudioContextRequest
/**
 * This function will redirect the request  AccessoryAudioContextRequest
 * to the service. It uses glue function vGlueAccessoryAudioContextError
 * (...) function for that<b>Documentation of
 * 'AccessoryAudioContext'</b>:If the meaning of "AccessoryAudioContext"
 * isn't clear, then there should be a description here.
 */
void vGlueAccessoryAudioContextRequest (uint32 DeviceHandle, bool AudioFlag, const ::midw_smartphoneint_fi_types::T_e8_AudioContext& AudioContext,act_t actJson);
// Callback 'AccessoryDisplayContextCallbackIF'
// Glue function vGlueAccessoryDisplayContextRequest
/**
 * This function will redirect the request  AccessoryDisplayContextRequest
 * to the service. It uses glue function vGlueAccessoryDisplayContextError
 * (...) function for that<b>Documentation of
 * 'AccessoryDisplayContext'</b>:If the meaning of
 * "AccessoryDisplayContext" isn't clear, then there should be a
 * description here.
 */
void vGlueAccessoryDisplayContextRequest (uint32 DeviceHandle, bool DisplayFlag, const ::midw_smartphoneint_fi_types::T_e8_DisplayContext& DisplayContext,act_t actJson);
// Callback 'AppStatusInfoCallbackIF'
// Glue function vGlueAppStatusInfoDeregister
/**
 * This function will redirect the deregistration request for property
 * AppStatusInfo to the service. It uses the function
 * sendAppStatusInfoRelUpRegAll (...) function for that<b>Documentation of
 * 'AppStatusInfo'</b>:If the meaning of "AppStatusInfo" isn't clear, then
 * there should be a description here.
 */
void vGlueAppStatusInfoDeregister ();
// Glue function vGlueAppStatusInfoRegister
/**
 * This function will redirect the registration request for property
 * AppStatusInfo to the service. It uses the function
 * sendAppStatusInfoUpReg (...) function for that<b>Documentation of
 * 'AppStatusInfo'</b>:If the meaning of "AppStatusInfo" isn't clear, then
 * there should be a description here.
 */
void vGlueAppStatusInfoRegister (act_t actJson);
// onAppStatusInfoError
/**
 * This function will redirect the signal error  AppStatusInfoError from
 * the service. It uses glue function vGlueAppStatusInfoRegisterError
 * (...) function for that<b>Documentation of 'AppStatusInfo'</b>:If the
 * meaning of "AppStatusInfo" isn't clear, then there should be a
 * description here.
 */
void onAppStatusInfoError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::AppStatusInfoError >& corfoError);
// onAppStatusInfoUpdate
/**
 * This function will redirect the property update  AppStatusInfoStatus
 * from the service. It uses glue function
 * vGlueAppStatusInfoPropertyUpdate (...) function for
 * that<b>Documentation of 'AppStatusInfo'</b>:If the meaning of
 * "AppStatusInfo" isn't clear, then there should be a description here.
 */
void onAppStatusInfoStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< AppStatusInfoStatus >& corfoStatus);
// Callback 'ApplicationMetaDataCallbackIF'
// Glue function vGlueApplicationMetaDataDeregister
/**
 * This function will redirect the deregistration request for property
 * ApplicationMetaData to the service. It uses the function
 * sendApplicationMetaDataRelUpRegAll (...) function for
 * that<b>Documentation of 'ApplicationMetaData'</b>:If the meaning of
 * "ApplicationMetaData" isn't clear, then there should be a description
 * here.
 */
void vGlueApplicationMetaDataDeregister ();
// Glue function vGlueApplicationMetaDataRegister
/**
 * This function will redirect the registration request for property
 * ApplicationMetaData to the service. It uses the function
 * sendApplicationMetaDataUpReg (...) function for that<b>Documentation of
 * 'ApplicationMetaData'</b>:If the meaning of "ApplicationMetaData" isn't
 * clear, then there should be a description here.
 */
void vGlueApplicationMetaDataRegister (act_t actJson);
// onApplicationMetaDataError
/**
 * This function will redirect the signal error  ApplicationMetaDataError
 * from the service. It uses glue function
 * vGlueApplicationMetaDataRegisterError (...) function for
 * that<b>Documentation of 'ApplicationMetaData'</b>:If the meaning of
 * "ApplicationMetaData" isn't clear, then there should be a description
 * here.
 */
void onApplicationMetaDataError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::ApplicationMetaDataError >& corfoError);
// onApplicationMetaDataUpdate
/**
 * This function will redirect the property update
 * ApplicationMetaDataStatus from the service. It uses glue function
 * vGlueApplicationMetaDataPropertyUpdate (...) function for
 * that<b>Documentation of 'ApplicationMetaData'</b>:If the meaning of
 * "ApplicationMetaData" isn't clear, then there should be a description
 * here.
 */
void onApplicationMetaDataStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ApplicationMetaDataStatus >& corfoStatus);
// Callback 'BluetoothDeviceStatusCallbackIF'
// Glue function vGlueBluetoothDeviceStatusDeregister
/**
 * This function will redirect the deregistration request for property
 * BluetoothDeviceStatus to the service. It uses the function
 * sendBluetoothDeviceStatusRelUpRegAll (...) function for
 * that<b>Documentation of 'BluetoothDeviceStatus'</b>:If the meaning of
 * "BluetoothDeviceStatus" isn't clear, then there should be a description
 * here.
 */
void vGlueBluetoothDeviceStatusDeregister ();
// Glue function vGlueBluetoothDeviceStatusRegister
/**
 * This function will redirect the registration request for property
 * BluetoothDeviceStatus to the service. It uses the function
 * sendBluetoothDeviceStatusUpReg (...) function for that<b>Documentation
 * of 'BluetoothDeviceStatus'</b>:If the meaning of
 * "BluetoothDeviceStatus" isn't clear, then there should be a description
 * here.
 */
void vGlueBluetoothDeviceStatusRegister (act_t actJson);
// onBluetoothDeviceStatusError
/**
 * This function will redirect the signal error
 * BluetoothDeviceStatusError from the service. It uses glue function
 * vGlueBluetoothDeviceStatusRegisterError (...) function for
 * that<b>Documentation of 'BluetoothDeviceStatus'</b>:If the meaning of
 * "BluetoothDeviceStatus" isn't clear, then there should be a description
 * here.
 */
void onBluetoothDeviceStatusError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::BluetoothDeviceStatusError >& corfoError);
// onBluetoothDeviceStatusUpdate
/**
 * This function will redirect the property update
 * BluetoothDeviceStatusStatus from the service. It uses glue function
 * vGlueBluetoothDeviceStatusPropertyUpdate (...) function for
 * that<b>Documentation of 'BluetoothDeviceStatus'</b>:If the meaning of
 * "BluetoothDeviceStatus" isn't clear, then there should be a description
 * here.
 */
void onBluetoothDeviceStatusStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< BluetoothDeviceStatusStatus >& corfoStatus);
// Callback 'DAPStatusInfoCallbackIF'
// Glue function vGlueDAPStatusInfoDeregister
/**
 * This function will redirect the deregistration request for property
 * DAPStatusInfo to the service. It uses the function
 * sendDAPStatusInfoRelUpRegAll (...) function for that<b>Documentation of
 * 'DAPStatusInfo'</b>:If the meaning of "DAPStatusInfo" isn't clear, then
 * there should be a description here.
 */
void vGlueDAPStatusInfoDeregister ();
// Glue function vGlueDAPStatusInfoRegister
/**
 * This function will redirect the registration request for property
 * DAPStatusInfo to the service. It uses the function
 * sendDAPStatusInfoUpReg (...) function for that<b>Documentation of
 * 'DAPStatusInfo'</b>:If the meaning of "DAPStatusInfo" isn't clear, then
 * there should be a description here.
 */
void vGlueDAPStatusInfoRegister (act_t actJson);
// onDAPStatusInfoError
/**
 * This function will redirect the signal error  DAPStatusInfoError from
 * the service. It uses glue function vGlueDAPStatusInfoRegisterError
 * (...) function for that<b>Documentation of 'DAPStatusInfo'</b>:If the
 * meaning of "DAPStatusInfo" isn't clear, then there should be a
 * description here.
 */
void onDAPStatusInfoError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DAPStatusInfoError >& corfoError);
// onDAPStatusInfoUpdate
/**
 * This function will redirect the property update  DAPStatusInfoStatus
 * from the service. It uses glue function
 * vGlueDAPStatusInfoPropertyUpdate (...) function for
 * that<b>Documentation of 'DAPStatusInfo'</b>:If the meaning of
 * "DAPStatusInfo" isn't clear, then there should be a description here.
 */
void onDAPStatusInfoStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< DAPStatusInfoStatus >& corfoStatus);
// Callback 'DataServiceInfoCallbackIF'
// Glue function vGlueDataServiceInfoDeregister
/**
 * This function will redirect the deregistration request for property
 * DataServiceInfo to the service. It uses the function
 * sendDataServiceInfoRelUpRegAll (...) function for that<b>Documentation
 * of 'DataServiceInfo'</b>:If the meaning of "DataServiceInfo" isn't
 * clear, then there should be a description here.
 */
void vGlueDataServiceInfoDeregister ();
// Glue function vGlueDataServiceInfoRegister
/**
 * This function will redirect the registration request for property
 * DataServiceInfo to the service. It uses the function
 * sendDataServiceInfoUpReg (...) function for that<b>Documentation of
 * 'DataServiceInfo'</b>:If the meaning of "DataServiceInfo" isn't clear,
 * then there should be a description here.
 */
void vGlueDataServiceInfoRegister (act_t actJson);
// onDataServiceInfoError
/**
 * This function will redirect the signal error  DataServiceInfoError from
 * the service. It uses glue function vGlueDataServiceInfoRegisterError
 * (...) function for that<b>Documentation of 'DataServiceInfo'</b>:If the
 * meaning of "DataServiceInfo" isn't clear, then there should be a
 * description here.
 */
void onDataServiceInfoError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DataServiceInfoError >& corfoError);
// onDataServiceInfoUpdate
/**
 * This function will redirect the property update  DataServiceInfoStatus
 * from the service. It uses glue function
 * vGlueDataServiceInfoPropertyUpdate (...) function for
 * that<b>Documentation of 'DataServiceInfo'</b>:If the meaning of
 * "DataServiceInfo" isn't clear, then there should be a description here.
 */
void onDataServiceInfoStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< DataServiceInfoStatus >& corfoStatus);
// Callback 'DeviceAudioContextCallbackIF'
// Glue function vGlueDeviceAudioContextDeregister
/**
 * This function will redirect the deregistration request for property
 * DeviceAudioContext to the service. It uses the function
 * sendDeviceAudioContextRelUpRegAll (...) function for
 * that<b>Documentation of 'DeviceAudioContext'</b>:If the meaning of
 * "DeviceAudioContext" isn't clear, then there should be a description
 * here.
 */
void vGlueDeviceAudioContextDeregister ();
// Glue function vGlueDeviceAudioContextRegister
/**
 * This function will redirect the registration request for property
 * DeviceAudioContext to the service. It uses the function
 * sendDeviceAudioContextUpReg (...) function for that<b>Documentation of
 * 'DeviceAudioContext'</b>:If the meaning of "DeviceAudioContext" isn't
 * clear, then there should be a description here.
 */
void vGlueDeviceAudioContextRegister (act_t actJson);
// onDeviceAudioContextError
/**
 * This function will redirect the signal error  DeviceAudioContextError
 * from the service. It uses glue function
 * vGlueDeviceAudioContextRegisterError (...) function for
 * that<b>Documentation of 'DeviceAudioContext'</b>:If the meaning of
 * "DeviceAudioContext" isn't clear, then there should be a description
 * here.
 */
void onDeviceAudioContextError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceAudioContextError >& corfoError);
// onDeviceAudioContextUpdate
/**
 * This function will redirect the property update
 * DeviceAudioContextStatus from the service. It uses glue function
 * vGlueDeviceAudioContextPropertyUpdate (...) function for
 * that<b>Documentation of 'DeviceAudioContext'</b>:If the meaning of
 * "DeviceAudioContext" isn't clear, then there should be a description
 * here.
 */
void onDeviceAudioContextStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< DeviceAudioContextStatus >& corfoStatus);
// Callback 'DeviceDisplayContextCallbackIF'
// Glue function vGlueDeviceDisplayContextDeregister
/**
 * This function will redirect the deregistration request for property
 * DeviceDisplayContext to the service. It uses the function
 * sendDeviceDisplayContextRelUpRegAll (...) function for
 * that<b>Documentation of 'DeviceDisplayContext'</b>:If the meaning of
 * "DeviceDisplayContext" isn't clear, then there should be a description
 * here.
 */
void vGlueDeviceDisplayContextDeregister ();
// Glue function vGlueDeviceDisplayContextRegister
/**
 * This function will redirect the registration request for property
 * DeviceDisplayContext to the service. It uses the function
 * sendDeviceDisplayContextUpReg (...) function for that<b>Documentation
 * of 'DeviceDisplayContext'</b>:If the meaning of "DeviceDisplayContext"
 * isn't clear, then there should be a description here.
 */
void vGlueDeviceDisplayContextRegister (act_t actJson);
// onDeviceDisplayContextError
/**
 * This function will redirect the signal error  DeviceDisplayContextError
 * from the service. It uses glue function
 * vGlueDeviceDisplayContextRegisterError (...) function for
 * that<b>Documentation of 'DeviceDisplayContext'</b>:If the meaning of
 * "DeviceDisplayContext" isn't clear, then there should be a description
 * here.
 */
void onDeviceDisplayContextError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceDisplayContextError >& corfoError);
// onDeviceDisplayContextUpdate
/**
 * This function will redirect the property update
 * DeviceDisplayContextStatus from the service. It uses glue function
 * vGlueDeviceDisplayContextPropertyUpdate (...) function for
 * that<b>Documentation of 'DeviceDisplayContext'</b>:If the meaning of
 * "DeviceDisplayContext" isn't clear, then there should be a description
 * here.
 */
void onDeviceDisplayContextStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< DeviceDisplayContextStatus >& corfoStatus);
// Callback 'DeviceStatusInfoCallbackIF'
// Glue function vGlueDeviceStatusInfoDeregister
/**
 * This function will redirect the deregistration request for property
 * DeviceStatusInfo to the service. It uses the function
 * sendDeviceStatusInfoRelUpRegAll (...) function for that<b>Documentation
 * of 'DeviceStatusInfo'</b>:If the meaning of "DeviceStatusInfo" isn't
 * clear, then there should be a description here.
 */
void vGlueDeviceStatusInfoDeregister ();
// Glue function vGlueDeviceStatusInfoRegister
/**
 * This function will redirect the registration request for property
 * DeviceStatusInfo to the service. It uses the function
 * sendDeviceStatusInfoUpReg (...) function for that<b>Documentation of
 * 'DeviceStatusInfo'</b>:If the meaning of "DeviceStatusInfo" isn't
 * clear, then there should be a description here.
 */
void vGlueDeviceStatusInfoRegister (act_t actJson);
// onDeviceStatusInfoError
/**
 * This function will redirect the signal error  DeviceStatusInfoError
 * from the service. It uses glue function
 * vGlueDeviceStatusInfoRegisterError (...) function for
 * that<b>Documentation of 'DeviceStatusInfo'</b>:If the meaning of
 * "DeviceStatusInfo" isn't clear, then there should be a description
 * here.
 */
void onDeviceStatusInfoError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceStatusInfoError >& corfoError);
// onDeviceStatusInfoUpdate
/**
 * This function will redirect the property update  DeviceStatusInfoStatus
 * from the service. It uses glue function
 * vGlueDeviceStatusInfoPropertyUpdate (...) function for
 * that<b>Documentation of 'DeviceStatusInfo'</b>:If the meaning of
 * "DeviceStatusInfo" isn't clear, then there should be a description
 * here.
 */
void onDeviceStatusInfoStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< DeviceStatusInfoStatus >& corfoStatus);
// Callback 'DiPOAppStatusInfoCallbackIF'
// Glue function vGlueDiPOAppStatusInfoDeregister
/**
 * This function will redirect the deregistration request for property
 * DiPOAppStatusInfo to the service. It uses the function
 * sendDiPOAppStatusInfoRelUpRegAll (...) function for
 * that<b>Documentation of 'DiPOAppStatusInfo'</b>:If the meaning of
 * "DiPOAppStatusInfo" isn't clear, then there should be a description
 * here.
 */
void vGlueDiPOAppStatusInfoDeregister ();
// Glue function vGlueDiPOAppStatusInfoRegister
/**
 * This function will redirect the registration request for property
 * DiPOAppStatusInfo to the service. It uses the function
 * sendDiPOAppStatusInfoUpReg (...) function for that<b>Documentation of
 * 'DiPOAppStatusInfo'</b>:If the meaning of "DiPOAppStatusInfo" isn't
 * clear, then there should be a description here.
 */
void vGlueDiPOAppStatusInfoRegister (act_t actJson);
// onDiPOAppStatusInfoError
/**
 * This function will redirect the signal error  DiPOAppStatusInfoError
 * from the service. It uses glue function
 * vGlueDiPOAppStatusInfoRegisterError (...) function for
 * that<b>Documentation of 'DiPOAppStatusInfo'</b>:If the meaning of
 * "DiPOAppStatusInfo" isn't clear, then there should be a description
 * here.
 */
void onDiPOAppStatusInfoError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DiPOAppStatusInfoError >& corfoError);
// onDiPOAppStatusInfoUpdate
/**
 * This function will redirect the property update
 * DiPOAppStatusInfoStatus from the service. It uses glue function
 * vGlueDiPOAppStatusInfoPropertyUpdate (...) function for
 * that<b>Documentation of 'DiPOAppStatusInfo'</b>:If the meaning of
 * "DiPOAppStatusInfo" isn't clear, then there should be a description
 * here.
 */
void onDiPOAppStatusInfoStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< DiPOAppStatusInfoStatus >& corfoStatus);
// Callback 'DiPORoleSwitchRequiredCallbackIF'
// Glue function vGlueDiPORoleSwitchRequiredRequest
/**
 * This function will redirect the request  DiPORoleSwitchRequiredRequest
 * to the service. It uses glue function vGlueDiPORoleSwitchRequiredError
 * (...) function for that<b>Documentation of
 * 'DiPORoleSwitchRequired'</b>:If the meaning of "DiPORoleSwitchRequired"
 * isn't clear, then there should be a description here.
 */
void vGlueDiPORoleSwitchRequiredRequest (uint8 u8DeviceTag,act_t actJson);
// onDiPORoleSwitchRequiredError
/**
 * This function will redirect the error  DiPORoleSwitchRequiredError from
 * the service. It uses glue function vGlueDiPORoleSwitchRequiredError
 * (...) function for that<b>Documentation of
 * 'DiPORoleSwitchRequired'</b>:If the meaning of "DiPORoleSwitchRequired"
 * isn't clear, then there should be a description here.
 */
void onDiPORoleSwitchRequiredError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DiPORoleSwitchRequiredError >& corfoError);
// onDiPORoleSwitchRequiredResult
/**
 * This function will redirect the result  DiPORoleSwitchRequiredResult
 * from the service. It uses glue function
 * vGlueDiPORoleSwitchRequiredResponse (...) function for
 * that<b>Documentation of 'DiPORoleSwitchRequired'</b>:If the meaning of
 * "DiPORoleSwitchRequired" isn't clear, then there should be a
 * description here.
 */
void onDiPORoleSwitchRequiredResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< DiPORoleSwitchRequiredResult >& corfoResult);
// Callback 'GetAppIconDataCallbackIF'
// Glue function vGlueGetAppIconDataRequest
/**
 * This function will redirect the request  GetAppIconDataRequest to the
 * service. It uses glue function vGlueGetAppIconDataError (...) function
 * for that<b>Documentation of 'GetAppIconData'</b>:If the meaning of
 * "GetAppIconData" isn't clear, then there should be a description here.
 */
void vGlueGetAppIconDataRequest (string AppIconURL,act_t actJson);
// onGetAppIconDataError
/**
 * This function will redirect the error  GetAppIconDataError from the
 * service. It uses glue function vGlueGetAppIconDataError (...) function
 * for that<b>Documentation of 'GetAppIconData'</b>:If the meaning of
 * "GetAppIconData" isn't clear, then there should be a description here.
 */
void onGetAppIconDataError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetAppIconDataError >& corfoError);
// onGetAppIconDataResult
/**
 * This function will redirect the result  GetAppIconDataResult from the
 * service. It uses glue function vGlueGetAppIconDataResponse (...)
 * function for that<b>Documentation of 'GetAppIconData'</b>:If the
 * meaning of "GetAppIconData" isn't clear, then there should be a
 * description here.
 */
void onGetAppIconDataResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< GetAppIconDataResult >& corfoResult);
// Callback 'GetAppListCallbackIF'
// Glue function vGlueGetAppListRequest
/**
 * This function will redirect the request  GetAppListRequest to the
 * service. It uses glue function vGlueGetAppListError (...) function for
 * that<b>Documentation of 'GetAppList'</b>:If the meaning of "GetAppList"
 * isn't clear, then there should be a description here.
 */
void vGlueGetAppListRequest (uint32 DeviceHandle,act_t actJson);
// onGetAppListError
/**
 * This function will redirect the error  GetAppListError from the
 * service. It uses glue function vGlueGetAppListError (...) function for
 * that<b>Documentation of 'GetAppList'</b>:If the meaning of "GetAppList"
 * isn't clear, then there should be a description here.
 */
void onGetAppListError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetAppListError >& corfoError);
// onGetAppListResult
/**
 * This function will redirect the result  GetAppListResult from the
 * service. It uses glue function vGlueGetAppListResponse (...) function
 * for that<b>Documentation of 'GetAppList'</b>:If the meaning of
 * "GetAppList" isn't clear, then there should be a description here.
 */
void onGetAppListResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< GetAppListResult >& corfoResult);
// Callback 'GetDeviceInfoListCallbackIF'
// Glue function vGlueGetDeviceInfoListRequest
/**
 * This function will redirect the request  GetDeviceInfoListRequest to
 * the service. It uses glue function vGlueGetDeviceInfoListError (...)
 * function for that<b>Documentation of 'GetDeviceInfoList'</b>:If the
 * meaning of "GetDeviceInfoList" isn't clear, then there should be a
 * description here.
 */
void vGlueGetDeviceInfoListRequest (act_t actJson);
// onGetDeviceInfoListError
/**
 * This function will redirect the error  GetDeviceInfoListError from the
 * service. It uses glue function vGlueGetDeviceInfoListError (...)
 * function for that<b>Documentation of 'GetDeviceInfoList'</b>:If the
 * meaning of "GetDeviceInfoList" isn't clear, then there should be a
 * description here.
 */
void onGetDeviceInfoListError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetDeviceInfoListError >& corfoError);
// onGetDeviceInfoListResult
/**
 * This function will redirect the result  GetDeviceInfoListResult from
 * the service. It uses glue function vGlueGetDeviceInfoListResponse (...)
 * function for that<b>Documentation of 'GetDeviceInfoList'</b>:If the
 * meaning of "GetDeviceInfoList" isn't clear, then there should be a
 * description here.
 */
void onGetDeviceInfoListResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< GetDeviceInfoListResult >& corfoResult);
// Callback 'GetDeviceUsagePreferenceCallbackIF'
// Glue function vGlueGetDeviceUsagePreferenceRequest
/**
 * This function will redirect the request
 * GetDeviceUsagePreferenceRequest to the service. It uses glue function
 * vGlueGetDeviceUsagePreferenceError (...) function for
 * that<b>Documentation of 'GetDeviceUsagePreference'</b>:If the meaning
 * of "GetDeviceUsagePreference" isn't clear, then there should be a
 * description here.
 */
void vGlueGetDeviceUsagePreferenceRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_DeviceCategory& DeviceCategory,act_t actJson);
// onGetDeviceUsagePreferenceError
/**
 * This function will redirect the error  GetDeviceUsagePreferenceError
 * from the service. It uses glue function
 * vGlueGetDeviceUsagePreferenceError (...) function for
 * that<b>Documentation of 'GetDeviceUsagePreference'</b>:If the meaning
 * of "GetDeviceUsagePreference" isn't clear, then there should be a
 * description here.
 */
void onGetDeviceUsagePreferenceError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetDeviceUsagePreferenceError >& corfoError);
// onGetDeviceUsagePreferenceResult
/**
 * This function will redirect the result  GetDeviceUsagePreferenceResult
 * from the service. It uses glue function
 * vGlueGetDeviceUsagePreferenceResponse (...) function for
 * that<b>Documentation of 'GetDeviceUsagePreference'</b>:If the meaning
 * of "GetDeviceUsagePreference" isn't clear, then there should be a
 * description here.
 */
void onGetDeviceUsagePreferenceResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< GetDeviceUsagePreferenceResult >& corfoResult);
// Callback 'GetVideoSettingsCallbackIF'
// Glue function vGlueGetVideoSettingsRequest
/**
 * This function will redirect the request  GetVideoSettingsRequest to the
 * service. It uses glue function vGlueGetVideoSettingsError (...)
 * function for that<b>Documentation of 'GetVideoSettings'</b>:If the
 * meaning of "GetVideoSettings" isn't clear, then there should be a
 * description here.
 */
void vGlueGetVideoSettingsRequest (uint32 DeviceHandle,act_t actJson);
// onGetVideoSettingsError
/**
 * This function will redirect the error  GetVideoSettingsError from the
 * service. It uses glue function vGlueGetVideoSettingsError (...)
 * function for that<b>Documentation of 'GetVideoSettings'</b>:If the
 * meaning of "GetVideoSettings" isn't clear, then there should be a
 * description here.
 */
void onGetVideoSettingsError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetVideoSettingsError >& corfoError);
// onGetVideoSettingsResult
/**
 * This function will redirect the result  GetVideoSettingsResult from the
 * service. It uses glue function vGlueGetVideoSettingsResponse (...)
 * function for that<b>Documentation of 'GetVideoSettings'</b>:If the
 * meaning of "GetVideoSettings" isn't clear, then there should be a
 * description here.
 */
void onGetVideoSettingsResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< GetVideoSettingsResult >& corfoResult);
// Callback 'InvokeBluetoothDeviceActionCallbackIF'
// Glue function vGlueInvokeBluetoothDeviceActionRequest
/**
 * This function will redirect the request
 * InvokeBluetoothDeviceActionRequest to the service. It uses glue
 * function vGlueInvokeBluetoothDeviceActionError (...) function for
 * that<b>Documentation of 'InvokeBluetoothDeviceAction'</b>:If the
 * meaning of "InvokeBluetoothDeviceAction" isn't clear, then there should
 * be a description here.
 */
void vGlueInvokeBluetoothDeviceActionRequest (uint32 BluetoothDeviceHandle, uint32 ProjectionDeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_BTChangeInfo& BTChangeInfo,act_t actJson);
// Callback 'InvokeNotificationActionCallbackIF'
// Glue function vGlueInvokeNotificationActionRequest
/**
 * This function will redirect the request
 * InvokeNotificationActionRequest to the service. It uses glue function
 * vGlueInvokeNotificationActionError (...) function for
 * that<b>Documentation of 'InvokeNotificationAction'</b>:If the meaning
 * of "InvokeNotificationAction" isn't clear, then there should be a
 * description here.
 */
void vGlueInvokeNotificationActionRequest (uint32 DeviceHandle, uint32 AppHandle, uint16 NotificationID, uint16 NotificationActionID,act_t actJson);
// onInvokeNotificationActionError
/**
 * This function will redirect the error  InvokeNotificationActionError
 * from the service. It uses glue function
 * vGlueInvokeNotificationActionError (...) function for
 * that<b>Documentation of 'InvokeNotificationAction'</b>:If the meaning
 * of "InvokeNotificationAction" isn't clear, then there should be a
 * description here.
 */
void onInvokeNotificationActionError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::InvokeNotificationActionError >& corfoError);
// onInvokeNotificationActionResult
/**
 * This function will redirect the result  InvokeNotificationActionResult
 * from the service. It uses glue function
 * vGlueInvokeNotificationActionResponse (...) function for
 * that<b>Documentation of 'InvokeNotificationAction'</b>:If the meaning
 * of "InvokeNotificationAction" isn't clear, then there should be a
 * description here.
 */
void onInvokeNotificationActionResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< InvokeNotificationActionResult >& corfoResult);
// Callback 'LaunchAppCallbackIF'
// Glue function vGlueLaunchAppRequest
/**
 * This function will redirect the request  LaunchAppRequest to the
 * service. It uses glue function vGlueLaunchAppError (...) function for
 * that<b>Documentation of 'LaunchApp'</b>:If the meaning of "LaunchApp"
 * isn't clear, then there should be a description here.
 */
void vGlueLaunchAppRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_DeviceCategory& DeviceCategory, uint32 AppHandle, const ::midw_smartphoneint_fi_types::T_e8_DiPOAppType& DiPOAppType, string TelephoneNumber, const ::midw_smartphoneint_fi_types::T_e8_EcnrSetting& EcnrSetting,act_t actJson);
// onLaunchAppError
/**
 * This function will redirect the error  LaunchAppError from the service.
 * It uses glue function vGlueLaunchAppError (...) function for
 * that<b>Documentation of 'LaunchApp'</b>:If the meaning of "LaunchApp"
 * isn't clear, then there should be a description here.
 */
void onLaunchAppError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::LaunchAppError >& corfoError);
// onLaunchAppResult
/**
 * This function will redirect the result  LaunchAppResult from the
 * service. It uses glue function vGlueLaunchAppResponse (...) function
 * for that<b>Documentation of 'LaunchApp'</b>:If the meaning of
 * "LaunchApp" isn't clear, then there should be a description here.
 */
void onLaunchAppResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< LaunchAppResult >& corfoResult);
// Callback 'NotificationInfoCallbackIF'
// Glue function vGlueNotificationInfoDeregister
/**
 * This function will redirect the deregistration request for property
 * NotificationInfo to the service. It uses the function
 * sendNotificationInfoRelUpRegAll (...) function for that<b>Documentation
 * of 'NotificationInfo'</b>:If the meaning of "NotificationInfo" isn't
 * clear, then there should be a description here.
 */
void vGlueNotificationInfoDeregister ();
// Glue function vGlueNotificationInfoRegister
/**
 * This function will redirect the registration request for property
 * NotificationInfo to the service. It uses the function
 * sendNotificationInfoUpReg (...) function for that<b>Documentation of
 * 'NotificationInfo'</b>:If the meaning of "NotificationInfo" isn't
 * clear, then there should be a description here.
 */
void vGlueNotificationInfoRegister (act_t actJson);
// onNotificationInfoError
/**
 * This function will redirect the signal error  NotificationInfoError
 * from the service. It uses glue function
 * vGlueNotificationInfoRegisterError (...) function for
 * that<b>Documentation of 'NotificationInfo'</b>:If the meaning of
 * "NotificationInfo" isn't clear, then there should be a description
 * here.
 */
void onNotificationInfoError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::NotificationInfoError >& corfoError);
// onNotificationInfoUpdate
/**
 * This function will redirect the property update  NotificationInfoStatus
 * from the service. It uses glue function
 * vGlueNotificationInfoPropertyUpdate (...) function for
 * that<b>Documentation of 'NotificationInfo'</b>:If the meaning of
 * "NotificationInfo" isn't clear, then there should be a description
 * here.
 */
void onNotificationInfoStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< NotificationInfoStatus >& corfoStatus);
// Callback 'SelectDeviceCallbackIF'
// Glue function vGlueSelectDeviceRequest
/**
 * This function will redirect the request  SelectDeviceRequest to the
 * service. It uses glue function vGlueSelectDeviceError (...) function
 * for that<b>Documentation of 'SelectDevice'</b>:If the meaning of
 * "SelectDevice" isn't clear, then there should be a description here.
 */
void vGlueSelectDeviceRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionType& DeviceConnectionType, const ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionReq& DeviceConnectionReq, const ::midw_smartphoneint_fi_types::T_e8_EnabledInfo& DAPUsage, const ::midw_smartphoneint_fi_types::T_e8_EnabledInfo& CDBUsage,act_t actJson);
// onSelectDeviceError
/**
 * This function will redirect the error  SelectDeviceError from the
 * service. It uses glue function vGlueSelectDeviceError (...) function
 * for that<b>Documentation of 'SelectDevice'</b>:If the meaning of
 * "SelectDevice" isn't clear, then there should be a description here.
 */
void onSelectDeviceError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SelectDeviceError >& corfoError);
// onSelectDeviceResult
/**
 * This function will redirect the result  SelectDeviceResult from the
 * service. It uses glue function vGlueSelectDeviceResponse (...) function
 * for that<b>Documentation of 'SelectDevice'</b>:If the meaning of
 * "SelectDevice" isn't clear, then there should be a description here.
 */
void onSelectDeviceResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SelectDeviceResult >& corfoResult);
// Callback 'SendKeyEventCallbackIF'
// Glue function vGlueSendKeyEventRequest
/**
 * This function will redirect the request  SendKeyEventRequest to the
 * service. It uses glue function vGlueSendKeyEventError (...) function
 * for that<b>Documentation of 'SendKeyEvent'</b>:If the meaning of
 * "SendKeyEvent" isn't clear, then there should be a description here.
 */
void vGlueSendKeyEventRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_KeyMode& KeyMode, const ::midw_smartphoneint_fi_types::T_e32_KeyCode& KeyCode,act_t actJson);
// onSendKeyEventError
/**
 * This function will redirect the error  SendKeyEventError from the
 * service. It uses glue function vGlueSendKeyEventError (...) function
 * for that<b>Documentation of 'SendKeyEvent'</b>:If the meaning of
 * "SendKeyEvent" isn't clear, then there should be a description here.
 */
void onSendKeyEventError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SendKeyEventError >& corfoError);
// onSendKeyEventResult
/**
 * This function will redirect the result  SendKeyEventResult from the
 * service. It uses glue function vGlueSendKeyEventResponse (...) function
 * for that<b>Documentation of 'SendKeyEvent'</b>:If the meaning of
 * "SendKeyEvent" isn't clear, then there should be a description here.
 */
void onSendKeyEventResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SendKeyEventResult >& corfoResult);
// Callback 'SendTouchEventCallbackIF'
// Glue function vGlueSendTouchEventRequest
/**
 * This function will redirect the request  SendTouchEventRequest to the
 * service. It uses glue function vGlueSendTouchEventError (...) function
 * for that<b>Documentation of 'SendTouchEvent'</b>:If the meaning of
 * "SendTouchEvent" isn't clear, then there should be a description here.
 */
void vGlueSendTouchEventRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_TouchData& TouchData,act_t actJson);
// onSendTouchEventError
/**
 * This function will redirect the error  SendTouchEventError from the
 * service. It uses glue function vGlueSendTouchEventError (...) function
 * for that<b>Documentation of 'SendTouchEvent'</b>:If the meaning of
 * "SendTouchEvent" isn't clear, then there should be a description here.
 */
void onSendTouchEventError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SendTouchEventError >& corfoError);
// onSendTouchEventResult
/**
 * This function will redirect the result  SendTouchEventResult from the
 * service. It uses glue function vGlueSendTouchEventResponse (...)
 * function for that<b>Documentation of 'SendTouchEvent'</b>:If the
 * meaning of "SendTouchEvent" isn't clear, then there should be a
 * description here.
 */
void onSendTouchEventResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SendTouchEventResult >& corfoResult);
// Callback 'SessionStatusInfoCallbackIF'
// Glue function vGlueSessionStatusInfoDeregister
/**
 * This function will redirect the deregistration request for property
 * SessionStatusInfo to the service. It uses the function
 * sendSessionStatusInfoRelUpRegAll (...) function for
 * that<b>Documentation of 'SessionStatusInfo'</b>:If the meaning of
 * "SessionStatusInfo" isn't clear, then there should be a description
 * here.
 */
void vGlueSessionStatusInfoDeregister ();
// Glue function vGlueSessionStatusInfoRegister
/**
 * This function will redirect the registration request for property
 * SessionStatusInfo to the service. It uses the function
 * sendSessionStatusInfoUpReg (...) function for that<b>Documentation of
 * 'SessionStatusInfo'</b>:If the meaning of "SessionStatusInfo" isn't
 * clear, then there should be a description here.
 */
void vGlueSessionStatusInfoRegister (act_t actJson);
// onSessionStatusInfoError
/**
 * This function will redirect the signal error  SessionStatusInfoError
 * from the service. It uses glue function
 * vGlueSessionStatusInfoRegisterError (...) function for
 * that<b>Documentation of 'SessionStatusInfo'</b>:If the meaning of
 * "SessionStatusInfo" isn't clear, then there should be a description
 * here.
 */
void onSessionStatusInfoError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SessionStatusInfoError >& corfoError);
// onSessionStatusInfoUpdate
/**
 * This function will redirect the property update
 * SessionStatusInfoStatus from the service. It uses glue function
 * vGlueSessionStatusInfoPropertyUpdate (...) function for
 * that<b>Documentation of 'SessionStatusInfo'</b>:If the meaning of
 * "SessionStatusInfo" isn't clear, then there should be a description
 * here.
 */
void onSessionStatusInfoStatus (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SessionStatusInfoStatus >& corfoStatus);
// Callback 'SetAppIconAttributesCallbackIF'
// Glue function vGlueSetAppIconAttributesRequest
/**
 * This function will redirect the request  SetAppIconAttributesRequest to
 * the service. It uses glue function vGlueSetAppIconAttributesError (...)
 * function for that<b>Documentation of 'SetAppIconAttributes'</b>:If the
 * meaning of "SetAppIconAttributes" isn't clear, then there should be a
 * description here.
 */
void vGlueSetAppIconAttributesRequest (uint32 DeviceHandle, uint32 AppHandle, const ::midw_smartphoneint_fi_types::T_IconAttributes& IconAttributes,act_t actJson);
// onSetAppIconAttributesError
/**
 * This function will redirect the error  SetAppIconAttributesError from
 * the service. It uses glue function vGlueSetAppIconAttributesError (...)
 * function for that<b>Documentation of 'SetAppIconAttributes'</b>:If the
 * meaning of "SetAppIconAttributes" isn't clear, then there should be a
 * description here.
 */
void onSetAppIconAttributesError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetAppIconAttributesError >& corfoError);
// onSetAppIconAttributesResult
/**
 * This function will redirect the result  SetAppIconAttributesResult from
 * the service. It uses glue function vGlueSetAppIconAttributesResponse
 * (...) function for that<b>Documentation of
 * 'SetAppIconAttributes'</b>:If the meaning of "SetAppIconAttributes"
 * isn't clear, then there should be a description here.
 */
void onSetAppIconAttributesResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SetAppIconAttributesResult >& corfoResult);
// Callback 'SetAudioBlockingModeCallbackIF'
// Glue function vGlueSetAudioBlockingModeRequest
/**
 * This function will redirect the request  SetAudioBlockingModeRequest to
 * the service. It uses glue function vGlueSetAudioBlockingModeError (...)
 * function for that<b>Documentation of 'SetAudioBlockingMode'</b>:If the
 * meaning of "SetAudioBlockingMode" isn't clear, then there should be a
 * description here.
 */
void vGlueSetAudioBlockingModeRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_BlockingMode& BlockingMode,act_t actJson);
// onSetAudioBlockingModeError
/**
 * This function will redirect the error  SetAudioBlockingModeError from
 * the service. It uses glue function vGlueSetAudioBlockingModeError (...)
 * function for that<b>Documentation of 'SetAudioBlockingMode'</b>:If the
 * meaning of "SetAudioBlockingMode" isn't clear, then there should be a
 * description here.
 */
void onSetAudioBlockingModeError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetAudioBlockingModeError >& corfoError);
// onSetAudioBlockingModeResult
/**
 * This function will redirect the result  SetAudioBlockingModeResult from
 * the service. It uses glue function vGlueSetAudioBlockingModeResponse
 * (...) function for that<b>Documentation of
 * 'SetAudioBlockingMode'</b>:If the meaning of "SetAudioBlockingMode"
 * isn't clear, then there should be a description here.
 */
void onSetAudioBlockingModeResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SetAudioBlockingModeResult >& corfoResult);
// Callback 'SetClientCapabilitiesCallbackIF'
// Glue function vGlueSetClientCapabilitiesRequest
/**
 * This function will redirect the request  SetClientCapabilitiesRequest
 * to the service. It uses glue function vGlueSetClientCapabilitiesError
 * (...) function for that<b>Documentation of
 * 'SetClientCapabilities'</b>:If the meaning of "SetClientCapabilities"
 * isn't clear, then there should be a description here.
 */
void vGlueSetClientCapabilitiesRequest (const ::midw_smartphoneint_fi_types::T_ClientCapabilities& ClientCapabilities,act_t actJson);
// Callback 'SetDataServiceFilterCallbackIF'
// Glue function vGlueSetDataServiceFilterRequest
/**
 * This function will redirect the request  SetDataServiceFilterRequest to
 * the service. It uses glue function vGlueSetDataServiceFilterError (...)
 * function for that<b>Documentation of 'SetDataServiceFilter'</b>:If the
 * meaning of "SetDataServiceFilter" isn't clear, then there should be a
 * description here.
 */
void vGlueSetDataServiceFilterRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_DeviceCategory& DeviceCategory, const ::midw_smartphoneint_fi_types::T_e8_DataServiceType& DataServiceType, const ::midw_smartphoneint_fi_types::T_e8_EnabledInfo& EnabledInfo,act_t actJson);
// Callback 'SetDataServiceUsageCallbackIF'
// Glue function vGlueSetDataServiceUsageRequest
/**
 * This function will redirect the request  SetDataServiceUsageRequest to
 * the service. It uses glue function vGlueSetDataServiceUsageError (...)
 * function for that<b>Documentation of 'SetDataServiceUsage'</b>:If the
 * meaning of "SetDataServiceUsage" isn't clear, then there should be a
 * description here.
 */
void vGlueSetDataServiceUsageRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_DeviceCategory& DeviceCategory, const ::midw_smartphoneint_fi_types::T_e8_DataServiceType& DataServiceType, const ::midw_smartphoneint_fi_types::T_e8_DataServiceUsage& DataServiceUsage,act_t actJson);
// onSetDataServiceUsageError
/**
 * This function will redirect the error  SetDataServiceUsageError from
 * the service. It uses glue function vGlueSetDataServiceUsageError (...)
 * function for that<b>Documentation of 'SetDataServiceUsage'</b>:If the
 * meaning of "SetDataServiceUsage" isn't clear, then there should be a
 * description here.
 */
void onSetDataServiceUsageError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetDataServiceUsageError >& corfoError);
// onSetDataServiceUsageResult
/**
 * This function will redirect the result  SetDataServiceUsageResult from
 * the service. It uses glue function vGlueSetDataServiceUsageResponse
 * (...) function for that<b>Documentation of 'SetDataServiceUsage'</b>:If
 * the meaning of "SetDataServiceUsage" isn't clear, then there should be
 * a description here.
 */
void onSetDataServiceUsageResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SetDataServiceUsageResult >& corfoResult);
// Callback 'SetDeviceUsagePreferenceCallbackIF'
// Glue function vGlueSetDeviceUsagePreferenceRequest
/**
 * This function will redirect the request
 * SetDeviceUsagePreferenceRequest to the service. It uses glue function
 * vGlueSetDeviceUsagePreferenceError (...) function for
 * that<b>Documentation of 'SetDeviceUsagePreference'</b>:If the meaning
 * of "SetDeviceUsagePreference" isn't clear, then there should be a
 * description here.
 */
void vGlueSetDeviceUsagePreferenceRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_DeviceCategory& DeviceCategory, const ::midw_smartphoneint_fi_types::T_e8_EnabledInfo& EnabledInfo,act_t actJson);
// Callback 'SetMLNotificationEnabledInfoCallbackIF'
// Glue function vGlueSetMLNotificationEnabledInfoRequest
/**
 * This function will redirect the request
 * SetMLNotificationEnabledInfoRequest to the service. It uses glue
 * function vGlueSetMLNotificationEnabledInfoError (...) function for
 * that<b>Documentation of 'SetMLNotificationEnabledInfo'</b>:If the
 * meaning of "SetMLNotificationEnabledInfo" isn't clear, then there
 * should be a description here.
 */
void vGlueSetMLNotificationEnabledInfoRequest (uint32 DeviceHandle, uint16 NumNotificationEnableList, const ::std::vector< ::midw_smartphoneint_fi_types::T_NotificationEnable >& NotificationEnableList,act_t actJson);
// onSetMLNotificationEnabledInfoError
/**
 * This function will redirect the error
 * SetMLNotificationEnabledInfoError from the service. It uses glue
 * function vGlueSetMLNotificationEnabledInfoError (...) function for
 * that<b>Documentation of 'SetMLNotificationEnabledInfo'</b>:If the
 * meaning of "SetMLNotificationEnabledInfo" isn't clear, then there
 * should be a description here.
 */
void onSetMLNotificationEnabledInfoError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetMLNotificationEnabledInfoError >& corfoError);
// onSetMLNotificationEnabledInfoResult
/**
 * This function will redirect the result
 * SetMLNotificationEnabledInfoResult from the service. It uses glue
 * function vGlueSetMLNotificationEnabledInfoResponse (...) function for
 * that<b>Documentation of 'SetMLNotificationEnabledInfo'</b>:If the
 * meaning of "SetMLNotificationEnabledInfo" isn't clear, then there
 * should be a description here.
 */
void onSetMLNotificationEnabledInfoResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SetMLNotificationEnabledInfoResult >& corfoResult);
// Callback 'SetOrientationModeCallbackIF'
// Glue function vGlueSetOrientationModeRequest
/**
 * This function will redirect the request  SetOrientationModeRequest to
 * the service. It uses glue function vGlueSetOrientationModeError (...)
 * function for that<b>Documentation of 'SetOrientationMode'</b>:If the
 * meaning of "SetOrientationMode" isn't clear, then there should be a
 * description here.
 */
void vGlueSetOrientationModeRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_OrientationMode& OrientationMode,act_t actJson);
// onSetOrientationModeError
/**
 * This function will redirect the error  SetOrientationModeError from the
 * service. It uses glue function vGlueSetOrientationModeError (...)
 * function for that<b>Documentation of 'SetOrientationMode'</b>:If the
 * meaning of "SetOrientationMode" isn't clear, then there should be a
 * description here.
 */
void onSetOrientationModeError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetOrientationModeError >& corfoError);
// onSetOrientationModeResult
/**
 * This function will redirect the result  SetOrientationModeResult from
 * the service. It uses glue function vGlueSetOrientationModeResponse
 * (...) function for that<b>Documentation of 'SetOrientationMode'</b>:If
 * the meaning of "SetOrientationMode" isn't clear, then there should be a
 * description here.
 */
void onSetOrientationModeResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SetOrientationModeResult >& corfoResult);
// Callback 'SetRegionCallbackIF'
// Glue function vGlueSetRegionRequest
/**
 * This function will redirect the request  SetRegionRequest to the
 * service. It uses glue function vGlueSetRegionError (...) function for
 * that<b>Documentation of 'SetRegion'</b>:If the meaning of "SetRegion"
 * isn't clear, then there should be a description here.
 */
void vGlueSetRegionRequest (const ::midw_smartphoneint_fi_types::T_e8_Region& Region,act_t actJson);
// Callback 'SetScreenSizeCallbackIF'
// Glue function vGlueSetScreenSizeRequest
/**
 * This function will redirect the request  SetScreenSizeRequest to the
 * service. It uses glue function vGlueSetScreenSizeError (...) function
 * for that<b>Documentation of 'SetScreenSize'</b>:If the meaning of
 * "SetScreenSize" isn't clear, then there should be a description here.
 */
void vGlueSetScreenSizeRequest (const ::midw_smartphoneint_fi_types::T_ScreenAttributes& ScreenAttributes,act_t actJson);
// Callback 'SetVehicleBTAddressCallbackIF'
// Glue function vGlueSetVehicleBTAddressRequest
/**
 * This function will redirect the request  SetVehicleBTAddressRequest to
 * the service. It uses glue function vGlueSetVehicleBTAddressError (...)
 * function for that<b>Documentation of 'SetVehicleBTAddress'</b>:If the
 * meaning of "SetVehicleBTAddress" isn't clear, then there should be a
 * description here.
 */
void vGlueSetVehicleBTAddressRequest (string BTAddress,act_t actJson);
// Callback 'SetVehicleConfigurationCallbackIF'
// Glue function vGlueSetVehicleConfigurationRequest
/**
 * This function will redirect the request  SetVehicleConfigurationRequest
 * to the service. It uses glue function vGlueSetVehicleConfigurationError
 * (...) function for that<b>Documentation of
 * 'SetVehicleConfiguration'</b>:If the meaning of
 * "SetVehicleConfiguration" isn't clear, then there should be a
 * description here.
 */
void vGlueSetVehicleConfigurationRequest (const ::midw_smartphoneint_fi_types::T_e8_Vehicle_Configuration& VehicleConfiguration, bool SetConfiguration,act_t actJson);
// Callback 'SetVideoBlockingModeCallbackIF'
// Glue function vGlueSetVideoBlockingModeRequest
/**
 * This function will redirect the request  SetVideoBlockingModeRequest to
 * the service. It uses glue function vGlueSetVideoBlockingModeError (...)
 * function for that<b>Documentation of 'SetVideoBlockingMode'</b>:If the
 * meaning of "SetVideoBlockingMode" isn't clear, then there should be a
 * description here.
 */
void vGlueSetVideoBlockingModeRequest (uint32 DeviceHandle, const ::midw_smartphoneint_fi_types::T_e8_BlockingMode& BlockingMode,act_t actJson);
// onSetVideoBlockingModeError
/**
 * This function will redirect the error  SetVideoBlockingModeError from
 * the service. It uses glue function vGlueSetVideoBlockingModeError (...)
 * function for that<b>Documentation of 'SetVideoBlockingMode'</b>:If the
 * meaning of "SetVideoBlockingMode" isn't clear, then there should be a
 * description here.
 */
void onSetVideoBlockingModeError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetVideoBlockingModeError >& corfoError);
// onSetVideoBlockingModeResult
/**
 * This function will redirect the result  SetVideoBlockingModeResult from
 * the service. It uses glue function vGlueSetVideoBlockingModeResponse
 * (...) function for that<b>Documentation of
 * 'SetVideoBlockingMode'</b>:If the meaning of "SetVideoBlockingMode"
 * isn't clear, then there should be a description here.
 */
void onSetVideoBlockingModeResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< SetVideoBlockingModeResult >& corfoResult);
// Callback 'TerminateAppCallbackIF'
// Glue function vGlueTerminateAppRequest
/**
 * This function will redirect the request  TerminateAppRequest to the
 * service. It uses glue function vGlueTerminateAppError (...) function
 * for that<b>Documentation of 'TerminateApp'</b>:If the meaning of
 * "TerminateApp" isn't clear, then there should be a description here.
 */
void vGlueTerminateAppRequest (uint32 DeviceHandle, uint32 AppHandle,act_t actJson);
// onTerminateAppError
/**
 * This function will redirect the error  TerminateAppError from the
 * service. It uses glue function vGlueTerminateAppError (...) function
 * for that<b>Documentation of 'TerminateApp'</b>:If the meaning of
 * "TerminateApp" isn't clear, then there should be a description here.
 */
void onTerminateAppError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::TerminateAppError >& corfoError);
// onTerminateAppResult
/**
 * This function will redirect the result  TerminateAppResult from the
 * service. It uses glue function vGlueTerminateAppResponse (...) function
 * for that<b>Documentation of 'TerminateApp'</b>:If the meaning of
 * "TerminateApp" isn't clear, then there should be a description here.
 */
void onTerminateAppResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< TerminateAppResult >& corfoResult);
// Callback 'UpdateCertificateFileCallbackIF'
// Glue function vGlueUpdateCertificateFileRequest
/**
 * This function will redirect the request  UpdateCertificateFileRequest
 * to the service. It uses glue function vGlueUpdateCertificateFileError
 * (...) function for that<b>Documentation of
 * 'UpdateCertificateFile'</b>:If the meaning of "UpdateCertificateFile"
 * isn't clear, then there should be a description here.
 */
void vGlueUpdateCertificateFileRequest (string CertificateFilePath,act_t actJson);
// onUpdateCertificateFileError
/**
 * This function will redirect the error  UpdateCertificateFileError from
 * the service. It uses glue function vGlueUpdateCertificateFileError
 * (...) function for that<b>Documentation of
 * 'UpdateCertificateFile'</b>:If the meaning of "UpdateCertificateFile"
 * isn't clear, then there should be a description here.
 */
void onUpdateCertificateFileError (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::UpdateCertificateFileError >& corfoError);
// onUpdateCertificateFileResult
/**
 * This function will redirect the result  UpdateCertificateFileResult
 * from the service. It uses glue function
 * vGlueUpdateCertificateFileResponse (...) function for
 * that<b>Documentation of 'UpdateCertificateFile'</b>:If the meaning of
 * "UpdateCertificateFile" isn't clear, then there should be a description
 * here.
 */
void onUpdateCertificateFileResult (const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< UpdateCertificateFileResult >& corfoResult);
private:

    ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy > m_pomidw_smartphoneint_fiProxy;
    /*Stub Referential Pointer*/
    ::boost::shared_ptr< Midw_smartphoneint_fiAdapterComponentStub > m_poMidw_smartphoneint_fiAdapterComponentStub;
    /*InfraStructure Stub Referential Pointer*/
    ::boost::shared_ptr< Midw_smartphoneint_fiAdapterComponentInfrastructureStub > m_poMidw_smartphoneint_fiAdapterComponentInfrastructureStub;
    typedef map<act_t, act_t> ActMap;
    ActMap _actRequestMap;
    ActMap _actPropRegMap;
    
    map<string, act_t> _actPropRegNameMap;
    ActMap _actPropGetMap;
    ActMap _actPropSetMap;
    DECLARE_CLASS_LOGGER ();
};

} // namespace adapter
} // namespace midw_smartphoneint_fiComponent

#endif // MIDW_SMARTPHONEINT_FIADAPTERCOMPONENT_H
