/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#include "Midw_smartphoneint_fiAdapterComponentInfrastructureStub.h"

namespace midw_smartphoneint_fiComponent {
namespace adapter {

DEFINE_CLASS_LOGGER_AND_LEVEL ("midw_smartphoneint_fiComponent/adapter", Midw_smartphoneint_fiAdapterComponentInfrastructureStub, Info);

Midw_smartphoneint_fiAdapterComponentInfrastructureStub::Midw_smartphoneint_fiAdapterComponentInfrastructureStub () :
::midw_smartphoneint_fi::InfrastructureMidw_smartphoneint_fiService::InfrastructureMidw_smartphoneint_fiServiceStub ("infrastructureMidw_smartphoneint_fiServiceProvidedPort") {
    LOG_INFO ("infrastructureServiceStubClassName Constructor...");
    stubAvailable = false;
}

Midw_smartphoneint_fiAdapterComponentInfrastructureStub::~Midw_smartphoneint_fiAdapterComponentInfrastructureStub () {
    LOG_INFO ("infrastructureServiceStubClassName Destructor...");
}

void Midw_smartphoneint_fiAdapterComponentInfrastructureStub::setPropertyStubAvailable (bool enable) {
    LOG_INFO ("Midw_smartphoneint_fiAdapterComponentInfrastructureStub::\
    setPropertyStubAvailable...enable: %d", enable);
    
    if(enable != stubAvailable){
        stubAvailable = enable;
        setStubAvailable (stubAvailable);
    }
}

} // namespace adapter
} // namespace midw_smartphoneint_fiComponent
