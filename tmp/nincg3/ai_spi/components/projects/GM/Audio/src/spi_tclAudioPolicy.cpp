/*!
 *******************************************************************************
 * \file             spi_tclAudioPolicy.cpp
 * \brief            GM Audio Policy class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   SmartPhone Integration
 DESCRIPTION:    Audio Implementation for Mirror Link
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                                  | Modifications
 29.10.2013 |  Hari Priya E R(RBEI/ECP2)               | Initial Version
 04.12.2013 |  Raghavendra S (RBEI/ECP2)               | Implemented redefined
 interfaces
 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include <ahl_if.h>

//!Include public FI interface of this service
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_SERVICEINFO
#include "most_fi_if.h"

#define MIDW_FI_S_IMPORT_INTERFACE_FI_TYPES
#include "midw_fi_if.h"

#include "spi_tclAudioIntf.h"
#include "spi_tclAudioPolicy.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclAudioPolicy.cpp.trc.h"
#endif

#define CHAR_SET_UTF8   midw_fi_tclString::FI_EN_UTF8

//!Version defines for offered service
#define DEVPROJ_SERVICE_FI_MAJOR_VERSION  MOST_DEVPRJFI_C_U16_SERVICE_MAJORVERSION
#define DEVPROJ_SERVICE_FI_MINOR_VERSION  MOST_DEVPRJFI_C_U16_SERVICE_MINORVERSION
#define DEVPROJ_SERVICE_FI_PATCH_VERSION  0

/***************************************************************************
 ** FUNCTION:  spi_tclAudioPolicy::spi_tclAudioPolicy
 (spi_tclAudioIntf* poAudioIntf,ahl_tclBaseOneThreadApp* poMainApp)
 ***************************************************************************/
spi_tclAudioPolicy::spi_tclAudioPolicy(spi_tclAudioIntf* poAudioIntf,
         ahl_tclBaseOneThreadApp* poMainApp) :
   iil_tclISource((ahl_tclBaseOneThreadApp*) poMainApp,
            MOST_DEVPRJFI_C_U16_SERVICE_ID,
            DEVPROJ_SERVICE_FI_MAJOR_VERSION,
            DEVPROJ_SERVICE_FI_MINOR_VERSION), m_poAudioIntf(poAudioIntf)
{
   ETG_TRACE_USR1(("spi_tclAudioPolicy::spi_tclAudioPolicy"));
}// spi_tclAudioPolicy::spi_tclAudioPolicy()

/***************************************************************************
 ** FUNCTION:  spi_tclAudioPolicy::~spi_tclAudioPolicy();
 ***************************************************************************/
spi_tclAudioPolicy::~spi_tclAudioPolicy()
{
   ETG_TRACE_USR1(("spi_tclAudioPolicy::~spi_tclAudioPolicy"));

}//spi_tclAudioPolicy::~spi_tclAudioPolicy()

/***************************************************************************
 ** FUNCTION:  bool spi_tclAudioPolicy::bRequestAudioActivation(tU8 u8SourceNum)
 ***************************************************************************/
tBool spi_tclAudioPolicy::bRequestAudioActivation(tU8 u8SourceNum)
{
   ETG_TRACE_USR1((" bRequestAudioActivation entered \n"));

   tBool bRetVal = bTriggerAVAction(u8SourceNum, IIL_EN_ISRC_REQACT);

   return bRetVal;

}//spi_tclAudioPolicy::bRequestAudioActivation(tU8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  bool spi_tclAudioPolicy::bRequestAudioDeactivation(tU8 u8SourceNum)
 ***************************************************************************/
tBool spi_tclAudioPolicy::bRequestAudioDeactivation(tU8 u8SourceNum)
{
   ETG_TRACE_USR1((" bRequestAudioDeactivation entered \n"));

   tBool bRetVal = bTriggerAVAction(u8SourceNum, IIL_EN_ISRC_REQDEACT);

   return bRetVal;

}//spi_tclAudioPolicy::bRequestAudioDeactivation(tU8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  tVoid spi_tclAudioPolicy::vOnStartAllocate(tU8* pu8MsgAlloc)
 ***************************************************************************/
tVoid spi_tclAudioPolicy::vOnStartAllocate(tU8* pu8MsgAlloc)
{
   ETG_TRACE_USR1((" vOnStartAllocate entered \n"));

   if (NULL != pu8MsgAlloc)
   {
      amt_tclServiceData* poMessage = (amt_tclServiceData*) (pu8MsgAlloc);
      vOnMsISource(poMessage);

   }//if(NULL != pu8MsgAlloc)

}//tVoid spi_tclAudioPolicy::vOnStartAllocate(tU8* pu8MsgAlloc)

/***************************************************************************
 ** FUNCTION:  void spi_tclAudioPolicy::vOnStartDeAllocate(tU8* pu8MsgDeAlloc)
 ***************************************************************************/
tVoid spi_tclAudioPolicy::vOnStartDeAllocate(tU8* pu8MsgDeAlloc)
{
   ETG_TRACE_USR1((" vOnStartDeAllocate entered \n"));

   if (NULL != pu8MsgDeAlloc)
   {
      amt_tclServiceData* poMessage = (amt_tclServiceData*) (pu8MsgDeAlloc);
      vOnMsISource(poMessage);

   }//if(NULL != pu8MsgDeAlloc)

}//tVoid spi_tclAudioPolicy::vOnStartDeAllocate(tU8* pu8MsgDeAlloc)

/***************************************************************************
 ** FUNCTION:  tVoid spi_tclAudioPolicy::vOnSourceActivity(tU8* pu8MsgSrcAct)
 ***************************************************************************/
tVoid spi_tclAudioPolicy::vOnSourceActivity(tU8* pu8MsgSrcAct)
{
   ETG_TRACE_USR1((" vOnSourceActivity entered \n"));

   if (NULL != pu8MsgSrcAct)
   {
      amt_tclServiceData* poMessage = (amt_tclServiceData*) (pu8MsgSrcAct);
      vOnMsISource(poMessage);

   }//if(NULL != pu8MsgSrcAct)

}//tVoid spi_tclAudioPolicy::vOnSourceActivity(tU8* pu8MsgSrcAct)

/***************************************************************************
 ** FUNCTION:  tVoid spi_tclAudioPolicy::vOnRequestSourceInfo(tU8* pu8MsgSrcInfo)
 ***************************************************************************/
tVoid spi_tclAudioPolicy::vOnRequestSourceInfo(tU8* pu8MsgSrcInfo)
{
   ETG_TRACE_USR1((" vOnRequestSourceInfo entered \n"));

   if (NULL != pu8MsgSrcInfo)
   {
      amt_tclServiceData* poMessage = (amt_tclServiceData*) (pu8MsgSrcInfo);
      vOnMsISource(poMessage);

   }//if(NULL != pu8MsgSrcInfo)

}//tVoid spi_tclAudioPolicy::vOnRequestSourceInfo(tU8* pu8MsgSrcInfo)

/***************************************************************************
 ** FUNCTION:  tVoid spi_tclAudioPolicy::vStartSourceActivityResult(tU8,tBool)
 ***************************************************************************/
tVoid spi_tclAudioPolicy::vStartSourceActivityResult(tU8 u8SourceNum,
         tBool bError)
{
   SPI_INTENTIONALLY_UNUSED(bError);
   ETG_TRACE_USR1((" vStartSourceActivityResult entered \n"));
   ETG_TRACE_USR4((" Source Activity Start Acknowledgement received for Source Number: %d",
                     u8SourceNum));

   /* NOTE: IIL does not require the streaming application to notify Start of
    Source Activity. Hence the trigger is unused */

}//spi_tclAudioPolicy::vStartSourceActivityResult(tU8 tBool)

/***************************************************************************
 ** FUNCTION:  void spi_tclAudioPolicy::vStopSourceActivityResult(tU8,tBool)
 ***************************************************************************/
tVoid spi_tclAudioPolicy::vStopSourceActivityResult(tU8 u8SourceNum,
         tBool bError)
{
   SPI_INTENTIONALLY_UNUSED(bError);
   ETG_TRACE_USR1((" vStopSourceActivityResult entered \n"));
   ETG_TRACE_USR4((" Source Activity Stop Acknowledgement received for Source Number: %d",
                     u8SourceNum));

   /* NOTE: IIL does not require the streaming application to notify End of
    Source Activity. Hence the trigger is unused */

}//spi_tclAudioPolicy::vStopSourceActivityResult(tU8 tBool)

/***************************************************************************
 ** FUNCTION:  tVoid spi_tclAudioPolicy::bSetSrcAvailability(tU8,tBool)
 ***************************************************************************/
tBool spi_tclAudioPolicy::bSetSrcAvailability(tU8 u8SourceNum, tBool bAvail)
{
   SPI_INTENTIONALLY_UNUSED(u8SourceNum);
   tBool bRetVal = true;
   ETG_TRACE_USR1((" bSetSourceAvailability entered \n"));
   vSetSrcAvailable(bAvail);

   return bRetVal;

}//spi_tclAudioPolicy::bSetSourceAvailability(tU8 u8SourceNum)


/***************************************************************************
 ** FUNCTION:  tBool spi_tclAudioPolicy::bOnSrcActivityStart(
 arl_tenSource enSrcNum, const iil_tSrcActivity& rfcoSrcActivity)
 ***************************************************************************/
tBool spi_tclAudioPolicy::bOnSrcActivityStart(tCU8 cu8SrcNum,
         const iil_tSrcActivity& rfcoSrcActivity)
{
   ETG_TRACE_USR1((" bOnSrcActivityStart entered \n"));

   tBool bRetVal = (NULL != m_poAudioIntf);

   // Pointer NULL check kept for LINT Compliance
   if (NULL != m_poAudioIntf)
   {
      switch (rfcoSrcActivity.enType)
      {
         case IIL_SRCACT_EN_ON:
         {
            ETG_TRACE_USR4((" Source Activty ON recived for Src Num: %d", cu8SrcNum));
            m_poAudioIntf->vOnStartSourceActivity(cu8SrcNum);
         }
            break;

         case IIL_SRCACT_EN_PAUSE:
         case IIL_SRCACT_EN_OFF:
         {
            ETG_TRACE_USR4((" Source Activty OFF recived for Src Num: %d", cu8SrcNum));
            m_poAudioIntf->vOnStopSourceActivity(cu8SrcNum);
         }
            break;

         default:
         {
            ETG_TRACE_USR4((" Unknown Source Activty recived for Src Num: %d", cu8SrcNum));
            bRetVal = false;
         }
            break;

      }//switch(rfcoSrcActivity.enType)

   }//if(NULL != m_poAudioIntf)

   return bRetVal;

} //spi_tclAudioPolicy::bOnSrcActivityStart(arl_tenSource,const arl_tSrcActivity&)

/***************************************************************************
 ** FUNCTION:  tBool spi_tclAudioPolicy::bOnAllocateResult(tCU8 cu8SrcNum,
 const iil_tAllocRouteResult& rfcoAllocRoute)
 ***************************************************************************/
tBool spi_tclAudioPolicy::bOnAllocateResult(tCU8 cu8SrcNum,
         const iil_tAllocRouteResult& rfcoAllocRoute)
{
   ETG_TRACE_USR1((" bOnAllocateResult for GM entered \n"));

   tBool bRetVal = true;
   trAudSrcInfo rSrcInfo;

   if (NULL != m_poAudioIntf)
   {
      ETG_TRACE_USR4((" Output device vector size %d\n", rfcoAllocRoute.listOutputDev.strALSADev.size()));
      ETG_TRACE_USR4((" Input device vector size %d\n", rfcoAllocRoute.listInputDev.strALSADev.size()));

      /* In case of Main Audio and Mix Audio source , we get only one ALSA device names.*/
      if ((1 == rfcoAllocRoute.listOutputDev.strALSADev.size())&&
               (0 == rfcoAllocRoute.listInputDev.strALSADev.size()))
      {
         GET_STRINGDATA_FROM_FI_STRINGOBJ((rfcoAllocRoute.listOutputDev.strALSADev.front()), CHAR_SET_UTF8, rSrcInfo.rMainAudDevNames.szOutputDev);
         ETG_TRACE_USR4(("bOnAllocateResult: ALSAOutDev : %s \n", rSrcInfo.rMainAudDevNames.szOutputDev.c_str()));
      }
      /* In case of Phone Audio and VR Audio source , we get only four ALSA device names(2-ECNR and 2- Main Audio).*/
      else if((2 == rfcoAllocRoute.listOutputDev.strALSADev.size()) &&
               (2 == rfcoAllocRoute.listInputDev.strALSADev.size()))
      {
         GET_STRINGDATA_FROM_FI_STRINGOBJ((rfcoAllocRoute.listInputDev.strALSADev.at(0)), CHAR_SET_UTF8, rSrcInfo.rMainAudDevNames.szInputDev);
         ETG_TRACE_USR4(("bOnAllocate: ALSAInDev : %s \n", rSrcInfo.rMainAudDevNames.szInputDev.c_str()));

         GET_STRINGDATA_FROM_FI_STRINGOBJ((rfcoAllocRoute.listInputDev.strALSADev.at(1)), CHAR_SET_UTF8, rSrcInfo.rEcnrAudDevNames.szInputDev);
         ETG_TRACE_USR4(("bOnAllocate: ECNR ALSAInDev : %s \n", rSrcInfo.rEcnrAudDevNames.szInputDev.c_str()));

         GET_STRINGDATA_FROM_FI_STRINGOBJ((rfcoAllocRoute.listOutputDev.strALSADev.at(0)), CHAR_SET_UTF8, rSrcInfo.rEcnrAudDevNames.szOutputDev);
         ETG_TRACE_USR4(("bOnAllocate: ECNR ALSAOutDev : %s \n", rSrcInfo.rEcnrAudDevNames.szOutputDev.c_str()));

         GET_STRINGDATA_FROM_FI_STRINGOBJ((rfcoAllocRoute.listOutputDev.strALSADev.at(1)), CHAR_SET_UTF8, rSrcInfo.rMainAudDevNames.szOutputDev);
         ETG_TRACE_USR4(("bOnAllocate: ALSAOutDev : %s \n", rSrcInfo.rMainAudDevNames.szOutputDev.c_str()));
      }
      else
      {
         ETG_TRACE_ERR(("bOnAllocateResult: Invalid ALSA Device Names\n"));
         bRetVal = false;
      }

      rSrcInfo.u16UCID = rfcoAllocRoute.u16UCID;
      rSrcInfo.u32STMMsgBufID = rfcoAllocRoute.u32STM_MsgBufID;
      rSrcInfo.u16MOSTConnectionLabel = rfcoAllocRoute.u16MOSTConnectionLabel;
      rSrcInfo.u16MLBChannelAddress = rfcoAllocRoute.u16MLBChannelAddress;
      rSrcInfo.u8BlkWidth = rfcoAllocRoute.u8BlkWidth;
      rSrcInfo.u32SmHandle = rfcoAllocRoute.u32SmHandle;
      ETG_TRACE_USR4((" UCID : %d \n", rSrcInfo.u16UCID));
      ETG_TRACE_USR4((" STM_MsgBufID : %d \n", rSrcInfo.u32STMMsgBufID));
      ETG_TRACE_USR4((" MOSTConnectionLabel : %d \n",rSrcInfo.u16MOSTConnectionLabel));
      ETG_TRACE_USR4((" MLBChannelAddress : %d \n",rSrcInfo.u16MLBChannelAddress));
      ETG_TRACE_USR4((" Blk Width : %d \n", rSrcInfo.u8BlkWidth));
      ETG_TRACE_USR4((" SmHandle : %d \n", rSrcInfo.u32SmHandle));

      m_poAudioIntf->bOnRouteAllocateResult(cu8SrcNum, rSrcInfo);

   }//if(NULL != m_poAudioIntf)

   return bRetVal;

}// tBool spi_tclAudioPolicy::bOnAllocateResult(tCU8 cu8SrcNum, ..)

/************************************************************************************
 ** FUNCTION:  tBool spi_tclAudioPolicy::bOnDeAllocateResult(tCU8 cu8SrcNum)
 *************************************************************************************/
tBool spi_tclAudioPolicy::bOnDeAllocateResult(tCU8 cu8SrcNum)
{
   ETG_TRACE_USR1((" bOnDeAllocateResult entered \n"));

   tBool bRetVal = (NULL != m_poAudioIntf);

   if (NULL != m_poAudioIntf)
   {
      m_poAudioIntf->vOnRouteDeAllocateResult(cu8SrcNum);

   }//if(NULL != m_poAudioIntf)

   return bRetVal;

}//tBool spi_tclAudioPolicy::bOnDeAllocateResult(tCU8 cu8SrcNum)

/***************************************************************************
 ** FUNCTION:  tVoid spi_tclAudioPolicy::vOnError(tCU8, iil_tenISourceError..)
 ***************************************************************************/
tVoid spi_tclAudioPolicy::vOnError(tCU8 cu8SrcNum, iil_tenISourceError enError)
{
   ETG_TRACE_USR1((" spi_tclAudioPolicy::vOnError entered \n"));

   if (NULL != m_poAudioIntf)
   {
      switch (enError)
      {
         case IIL_EN_ISRC_REQAVACT_ERR:
         {
            ETG_TRACE_USR4(("Request Audio Activation Error for Source Number %d ",
                              cu8SrcNum));
            m_poAudioIntf-> vOnErrorReqAudioActivation(cu8SrcNum);
         }
            break;

         case IIL_EN_ISRC_REQAVDEACT_ERR:
         {
            ETG_TRACE_USR4(("Request Audio Activation Error for Source Number %d ",
                              cu8SrcNum));
            m_poAudioIntf->vOnErrorReqAudioDeactivation(cu8SrcNum);
         }
            break;

         case IIL_EN_ISRC_ALLOC_ERR:
         {
            ETG_TRACE_USR4(("Audio Route Allocation Error for Source Number %d ",
                              cu8SrcNum));
            m_poAudioIntf->vOnErrorAllocate(cu8SrcNum);
         }
            break;

         case IIL_EN_ISRC_DEALLOC_ERR:
         {
            ETG_TRACE_USR4(("Audio Route Deallocation Error for Source Number %d ",
                              cu8SrcNum));
            m_poAudioIntf->vOnErrorDeallocate(cu8SrcNum);
         }
            break;

         case IIL_EN_ISRC_SRCACT_ON_ERR:
         {
            ETG_TRACE_USR4(("Start Source Activity Error for Source Number %d ",
                              cu8SrcNum));
            m_poAudioIntf->vOnErrorStartSourceActivity(cu8SrcNum);
         }
            break;

         case IIL_EN_ISRC_SRCACT_OFF_ERR:
         {
            ETG_TRACE_USR4(("Stop Source Activity Error for Source Number %d ",
                              cu8SrcNum));
            m_poAudioIntf->vOnErrorStopSourceActivity(cu8SrcNum);
         }
            break;

         default:
            ETG_TRACE_USR4(("Unknown Audio Error for Source Number %d ",
                              cu8SrcNum));
            m_poAudioIntf->vOnErrorGenericAudio(cu8SrcNum);
            break;

      }//switch(cenError)
   }//if (NULL != m_poAudioIntf)

}// spi_tclAudioPolicy::vOnError(tU8 u8SrcNum,arl_tenISourceError enError)

/***************************************************************************
** FUNCTION:  virtual tBool spi_tclAudioPolicy::bOnReqAVDeActResult(tCU8 cu8Sr..
***************************************************************************/
tBool spi_tclAudioPolicy::bOnReqAVDeActResult(tCU8 cu8SrcNum)
{
   tBool bRetVal = false;
   if (NULL != m_poAudioIntf)
   {
      bRetVal = m_poAudioIntf->bOnReqAVDeActivationResult(cu8SrcNum);
   }
   ETG_TRACE_USR1((" spi_tclAudioPolicy::bOnReqAVDeActResult cu8SrcNum = %d bRetVal =%d\n", cu8SrcNum, bRetVal));
   return bRetVal;
}
