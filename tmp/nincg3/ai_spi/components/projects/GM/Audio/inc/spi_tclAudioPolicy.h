/*!
 *******************************************************************************
 * \file             spi_tclAudioPolicy.h
 * \brief            GM Audio Policy class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   SmartPhone Integration
 DESCRIPTION:    GM Audio Policy class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                                  | Modifications
 29.10.2013 |  Hari Priya E R(RBEI/ECP2)               | Initial Version
 04.12.2013 |  Raghavendra S (RBEI/ECP2)               | Implemented redefined 
                                                         interfaces

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef SPI_TCLAUDIOPOLICY_H
#define SPI_TCLAUDIOPOLICY_H

#define IIL_S_IMPORT_INTERFACE_GENERIC
#include <iil_if.h>

#include "SPITypes.h"
#include "spi_tclAudioPolicyBase.h"

/**
 *  class definitions.
 */
class spi_tclAudioIntf;
class ahl_tclBaseOneThreadApp;


/**
 * GM Policy class that realises the audio interface based on IIL.
 */

class spi_tclAudioPolicy :public spi_tclAudioPolicyBase, public iil_tclISource
{
public:
	/***************************************************************************
     *********************************PUBLIC*************************************
     ***************************************************************************/

    /***************************************************************************
    ** FUNCTION:  spi_tclAudioPolicy::spi_tclAudioPolicy(spi_tclAudio* poAudio,
	               ahl_tclBaseOneThreadApp* poMainApp);
    ***************************************************************************/
    /*!
    * \fn      spi_tclAudioPolicy(spi_tclAudio* poAudio,ahl_tclBaseOneThreadApp* poMainApp)
    * \brief   Constructor
    **************************************************************************/
	spi_tclAudioPolicy(spi_tclAudioIntf* poAudioIntf, ahl_tclBaseOneThreadApp* poMainApp);

	/***************************************************************************
    ** FUNCTION:  spi_tclAudioPolicy::~spi_tclAudioPolicy();
    ***************************************************************************/
    /*!
    * \fn      ~spi_tclAudioPolicy()
    * \brief   Virtual Destructor
    **************************************************************************/
	virtual ~spi_tclAudioPolicy();

    
    // SPI Policy Base Functions
   	/***************************************************************************
    ** FUNCTION:  tBool spi_tclAudioPolicy::bRequestAudioActivation(tU8)
    ***************************************************************************/
    /*!
    * \fn      bRequestAudioActivation(tU8 u8SourceNum)
    * \brief   Request to the Audio Manager by Component for Starting Audio Playback. 
	*          Mandatory Interface to be implemented as per Project Audio Policy.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  Bool value
    **************************************************************************/
	virtual tBool bRequestAudioActivation(tU8 u8SourceNum);

	/***************************************************************************
    ** FUNCTION:  tBool spi_tclAudioPolicy::bRequestAudioDeactivation(tU8)
    ***************************************************************************/
    /*!
    * \fn      bRequestAudioDeactivation(tU8 u8SourceNum)
    * \brief   Request to the Audio Manager by Component for Stopping Audio Playback.
	*          Mandatory Interface to be implemented as per Project Audio Policy.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  Bool value
    **************************************************************************/
	virtual tBool bRequestAudioDeactivation(tU8 u8SourceNum);

	/***************************************************************************
    ** FUNCTION:  tBool spi_tclAudioPolicy::bPauseAudioActivity(tU8)
    ***************************************************************************/
    /*!
    * \fn      bPauseAudioActivity(tU8 u8SourceNum)
    * \brief   Request to the Audio Manager by Component for Pausing Audio Playback.
	*          Optional Interface to be implemented if supported and required.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  Bool value
    **************************************************************************/
	virtual tBool bPauseAudioActivity(tU8 u8SourceNum){return false;};

	/***************************************************************************
    ** FUNCTION:  tVoid spi_tclAudioPolicy::vStartSourceActivityResult(tU8, tBool)
    ***************************************************************************/
    /*!
    * \fn      vStartSourceActivityResult(tU8 u8SourceNum, tBool bError)
    * \brief   Acknowledgement from the Source Component to Audio Manager indicating
	*          Successful Start of Audio Playback on the allocated route. 
	*		   Mandatory Interface to be implemented.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          [bError]: TRUE for Error Condition, FALSE otherwise
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  NONE
    **************************************************************************/
	virtual tVoid vStartSourceActivityResult(tU8 u8SourceNum, tBool bError = FALSE);

	/***************************************************************************
    ** FUNCTION:  tVoid spi_tclAudioPolicy::vStopSourceActivityResult(tU8, tBool)
    ***************************************************************************/
    /*!
    * \fn      vStopSourceActivityResult(tU8 u8SourceNum, tBool bError)
    * \brief   Acknowledgement from the Source Component to Audio Manager indicating
	*          Successful Stop of Audio Playback on the allocated route. 
	*		   Mandatory Interface to be implemented.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          [bError]: TRUE for Error Condition, FALSE otherwise
	* \retval  NONE
    **************************************************************************/
	virtual tVoid vStopSourceActivityResult(tU8 u8SourceNum, tBool bError = FALSE);

	/***************************************************************************
    ** FUNCTION:  tVoid spi_tclAudioPolicy::vPauseSourceActivityResult(tU8, tBool)
    ***************************************************************************/
    /*!
    * \fn      vPauseSourceActivityResult(tU8 u8SourceNum, tBool bError)
    * \brief   Acknowledgement from the Source Component to Audio Manager indicating
	*          Successful Pause of Audio Playback on the allocated route. 
	*		   Optional Interface to be implemented if supported and required
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          [bError]: TRUE for Error Condition, FALSE otherwise
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  Bool value
    **************************************************************************/
	virtual tVoid vPauseSourceActivityResult(tU8 u8SourceNum, tBool bError = FALSE){};

	/***************************************************************************
    ** FUNCTION:  tBool spi_tclAudioPolicy::bSetSrcAvailability(tU8,tBool)
    ***************************************************************************/
    /*!
    * \fn      bSetSrcAvailability(tU8 u8SourceNum,tBool bAvail)
    * \brief   Register the Availability of State of Source with Audio Manager. 
	*   	   Optional Interface to be implemented if supported and required
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          [bAvail]: TRUE is Source Available, FALSE if Unavailable
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  Bool value
    **************************************************************************/
	virtual tBool bSetSrcAvailability(tU8 u8SourceNum, tBool bAvail = TRUE);

	/***************************************************************************
    ** FUNCTION:  tBool spi_tclAudioPolicy::bSetSourceMute(tU8)
    ***************************************************************************/
    /*!
    * \fn      bSetSourceMuteOn(tU8 u8SourceNum)
    * \brief   Request to Audio Manager to Mute the Source Audio.
	*          Optional Interface to be implemented if supported and required.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  Bool value
    **************************************************************************/
	virtual tBool bSetSourceMute(tU8 u8SourceNum){return false;};

	/***************************************************************************
    ** FUNCTION:  tBool spi_tclAudioPolicy::bSetSourceDemute(tU8)
    ***************************************************************************/
    /*!
    * \fn      tBool bSetSourceDemute(tU8 u8SourceNum)
    * \brief   Request to Audio Manager to Demute the Source Audio. 
	*		   Optional Interface to be implemented if supported and required.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  Bool value
    **************************************************************************/
	virtual tBool bSetSourceDemute(tU8 u8SourceNum){return false;};   


   // IIL Functions
   /***************************************************************************
   ** FUNCTION:  virtual tVoid spi_tclAudioPolicy::vOnError(tU8,const iil_tenISource)
   ***************************************************************************/
   /*!
    * \fn      tVoid vOnError(tU8 u8SourceNum, const iil_tenISourceError)
    * \brief   Function callback to trigger actions on request AV Activation 
    *          error
    * \param   [u8SrcNum]:  (I) Source Number.
    * \param   [enError]:  (I) ISource Error type.
    * \retval  NONE
    **************************************************************************/
    virtual tVoid vOnError(tU8 u8SrcNum, const iil_tenISourceError enError);

	/***************************************************************************
   ** FUNCTION:  virtual tBool spi_tclAudioPolicy::bOnSrcActivityStart(const iil..
   ***************************************************************************/
   /*!
    * \fn      tBool bOnSrcActivityStart(tCU8 cu8SrcNum, const iil_tSrcActivity& rfcoSrcActivity)
    * \brief   Application specific function on Source Activity start. 
    * \param   [cu8SrcNum]:  (I) Source Number.
    * \param   [rfcoSrcActivity]: (I) Source Activity
    * \retval  [tBool]: TRUE, if source activity was successful, FALSE otherwise
    **************************************************************************/
   virtual tBool bOnSrcActivityStart(tCU8 cu8SrcNum, const iil_tSrcActivity& rfcoSrcActivity); 

    /***************************************************************************
   ** FUNCTION:  virtual tBool spi_tclAudioPolicy::bSrcActivityResult(const iil_t..
   ***************************************************************************/
   /*!
    * \fn      tBool bSrcActivityResult(tCU8 cu8SrcNum, const iil_tSrcActivity& rfcoSrcActivity)
    * \brief   Application specific function before Source Activity result. 
    * \param   [u8SrcNum]:  (I) Source Number.
    * \param   [rfcoSrcActivity]: (I) Source Activity
    * \retval  [tBool]: TRUE, if source activity was successful, FALSE otherwise
    **************************************************************************/
   virtual tBool bSrcActivityResult(tCU8 cu8SrcNum ,
	             const iil_tSrcActivity& rfcoSrcActivity) {return true;}; 

   /***************************************************************************
   ** FUNCTION:  virtual tBool spi_tclAudioPolicy::bOnAllocateResult(tCU8 cu8SrcN..
   ***************************************************************************/
   /*!
    * \fn      tBool bOnAllocateResult(tCU8 cu8SrcNum, const iil_tSrcActivity& rfcoSrcActivity)
    * \brief   Application specific function after Allocate is processed.
    * \param   [u8SrcNum]:  (I) Source Number.
    * \param   [rfcoAllocRoute]: (I) Reference to Allocate route result
    * \retval  [tBool]: TRUE, if Application performed operations successfully,
    *          FALSE otherwise
    **************************************************************************/
   virtual tBool bOnAllocateResult(tCU8 cu8SrcNum, 
	             const iil_tAllocRouteResult& rfcoAllocRoute);

   /***************************************************************************
   ** FUNCTION:  virtual tBool spi_tclAudioPolicy::bOnDeAllocateResult(tCU8 cu8Sr..
   ***************************************************************************/
   /*!
    * \fn      tBool bOnDeAllocateResult(tCU8 cu8SrcNum)
    * \brief   Application specific function after DeAllocate is processed.
    * \param   [u8SrcNum]:  (I) Source Number.
    * \retval  [tBool]: TRUE, if Application performed operations successfully,
    *          FALSE otherwise
    **************************************************************************/
   virtual tBool bOnDeAllocateResult(tCU8 cu8SrcNum);

   /***************************************************************************
   ** FUNCTION:  virtual tBool spi_tclAudioPolicy::bOnReqAVActResult(tCU8 cu8Sr..
   ***************************************************************************/
   /*!
    * \fn      tBool bOnReqAVActResult(tCU8 cu8SrcNum, const iil_tAVReqResult& coAVReqResult)
    * \brief   Application specific function after RequestAVAct result can be 
    *          processed.
    * \param   [u8SrcNum]:  (I) Source Number.
    * \param   [coAVReqResult]:  (I) AV Activation result.
    * \retval  [tBool]: TRUE, if Application performed operations successfully,
    *          FALSE otherwise
    **************************************************************************/
   virtual tBool bOnReqAVActResult(tCU8 cu8SrcNum
                 , const iil_tAVReqResult& coAVReqResult){return true;};

   /***************************************************************************
   ** FUNCTION:  virtual tBool spi_tclAudioPolicy::bOnReqAVDeActResult(tCU8 cu8Sr..
   ***************************************************************************/
   /*!
    * \fn      tBool bOnReqAVDeActResult(tCU8 cu8SrcNum)
    * \brief   Application specific function after RequestAVAct result can be 
    *          processed.
    * \param   [u8SrcNum]:  (I) Source Number.
    * \retval  [tBool]: TRUE, if Application performed operations successfully,
    *          FALSE otherwise
    **************************************************************************/
   virtual tBool bOnReqAVDeActResult(tCU8 cu8SrcNum);

	/***************************************************************************
    ** FUNCTION:  tVoid spi_tclAudioPolicy::vOnStartAllocate(tU8* pu8MsgAlloc)
    ***************************************************************************/
    /*!
    * \fn      vOnStartAllocate(tU8* pu8MsgAlloc)
    * \brief   Command from the external Audio Manager to the Source component
	*          to Allocate the Audio Route for required for playback. 
	*		   Optional Interface to be implemented.
	* \param   [pu8MsgSrcAct]: Message Data containing Allocate Command.
	* \retval  NONE
    **************************************************************************/
	virtual tVoid vOnStartAllocate(tU8* pu8MsgAlloc);

    /***************************************************************************
    ** FUNCTION:  tVoid spi_tclAudioPolicy::vOnStartDeAllocate(tU8* pu8MsgDeAlloc)
    ***************************************************************************/
    /*!
    * \fn      vOnStartDeAllocate(tU8* pu8MsgDeAlloc)
    * \brief   Command from the external Audio Manager to the Source component
	*          to DeAllocate the Audio Route after playback. 
	*		   Optional Interface to be implemented.
	* \param   [pu8MsgSrcAct]: Message Data containing DeAllocate Command.
	* \retval  NONE
    **************************************************************************/
	virtual tVoid vOnStartDeAllocate(tU8* pu8MsgDeAlloc);

	/***************************************************************************
    ** FUNCTION:  tVoid spi_tclAudioPolicy::vOnSourceActivity(tU8* pu8MsgSrcAct)
    ***************************************************************************/
    /*!
    * \fn      vOnSourceActivity(tU8* pu8MsgSrcAct)
    * \brief   Command from the external Audio Manager to the Source component
	*          to carry out action associated with Source Activity On/Off. 
	*		   Optional Interface to be implemented.
	* \param   [pu8MsgSrcAct]: Message Data containing Source Activity Command.
	* \retval  NONE
    **************************************************************************/
	virtual tVoid vOnSourceActivity(tU8* pu8MsgSrcAct);

	/***************************************************************************
    ** FUNCTION:  tVoid spi_tclAudioPolicy::vOnRequestSourceInfo(tU8*)
    ***************************************************************************/
    /*!
    * \fn      vOnRequestSourceInfo(tU8* pu8MsgSrcAct)
    * \brief   Request from the external Audio Manager to the get the attributes
	*          associated with Audio Source. 
	*		   Optional Interface to be implemented.
	* \param   [pu8MsgSrcAct]: Message Data containing Source Info Request.
	* \retval  NONE
    **************************************************************************/
	virtual tVoid vOnRequestSourceInfo(tU8* pu8MsgSrcInfo);

   private:

	/***************************************************************************
    *********************************PRIVATE************************************
    ***************************************************************************/

	//!Instance of the Audio Interface class
	spi_tclAudioIntf* m_poAudioIntf;

};
#endif   // #ifndef SPI_TCLAUDIOPOLICY_H
