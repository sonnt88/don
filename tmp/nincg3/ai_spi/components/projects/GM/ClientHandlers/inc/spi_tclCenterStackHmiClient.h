  /*!
 *******************************************************************************
 * \file         spi_tclCenterStackHmiClient.h
 * \brief        CenterStackHMI client handler class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    CenterStackHMI client handler class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 10.05.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 27.06.2014 |  Ramya Murthy (RBEI/ECP2)         | Renamed class

 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCL_CENTERSTACKHMICLIENT_H_
#define _SPI_TCL_CENTERSTACKHMICLIENT_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
//!Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

#include "BaseTypes.h"
#include "spi_tclClientHandlerBase.h"

/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/* Forward declarations */

/*! 
* \class spi_tclCenterStackHmiClient
* \brief CenterStackHMI client handler class 
*/

class spi_tclCenterStackHmiClient
   : public ahl_tclBaseOneThreadClientHandler, public spi_tclClientHandlerBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclCenterStackHmiClient::spi_tclCenterStackHmiClient(...)
   ***************************************************************************/
   /*!
   * \fn      spi_tclCenterStackHmiClient()
   * \brief   Parameterized Constructor
   * \param   [IN] cpoMainApp: Pointer to main application
   * \sa     ~spi_tclCenterStackHmiClient()
   ***************************************************************************/
   explicit spi_tclCenterStackHmiClient(ahl_tclBaseOneThreadApp* const cpoMainApp);

   /***************************************************************************
   ** FUNCTION:  virtual spi_tclCenterStackHmiClient::~spi_tclCenterStackHmiClient()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclCenterStackHmiClient()
   * \brief   Destructor
   * \param   spi_tclCenterStackHmiClient()
   **************************************************************************/
   virtual ~spi_tclCenterStackHmiClient();

   /*********Start of functions overridden from spi_tclClientHandlerBase*****/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclCenterStackHmiClient::vRegisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers for all interested properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vUnregisterForProperties()
   **************************************************************************/
   virtual t_Void vRegisterForProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclCenterStackHmiClient::vUnregisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Un-registers all subscribed properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vRegisterForProperties()
   **************************************************************************/
   virtual t_Void vUnregisterForProperties();

   /*********End of functions overridden from spi_tclClientHandlerBase*******/

   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /** Start of functions overridden from ahl_tclBaseOneThreadClientHandler **/

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclCenterStackHmiClient::vOnServiceAvailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceAvailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes available, e.g. server has been started.
   * \sa      vOnServiceUnavailable()
   **************************************************************************/
   virtual tVoid vOnServiceAvailable();

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclCenterStackHmiClient::vOnServiceUnavailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceUnavailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes unavailable, e.g. server has been shut down.
   * \sa      vOnServiceAvailable()
   **************************************************************************/
   virtual tVoid vOnServiceUnavailable();

   /*** End of functions overridden from ahl_tclBaseOneThreadClientHandler ****/

   /***************************************************************************
   ******************************END OF PROTECTED******************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   * Handler method declarations used by message map.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclCenterStackHmiClient::vOnStatusDayNightModeOverrideSetting(...
   **************************************************************************/
   /*!
   * \fn      vOnStatusDayNightModeOverrideSetting()
   * \brief   Called by framework when SystemState status message is
   *          sent by SystemState Service.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnStatusDayNightModeOverrideSetting(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclCenterStackHmiClient::spi_tclCenterStackHmiClient()
   ***************************************************************************/
   /*!
   * \brief   Default Constructor
   * \param   None
   * \retval  None
   * \note    Default constructor is declared private to disable it. So
   *          that any attempt to create without parameter will be caught by
   *          the compiler.
   **************************************************************************/
   spi_tclCenterStackHmiClient();

   /***************************************************************************
   ** FUNCTION:  spi_tclCenterStackHmiClient::spi_tclCenterStackHmiClient(...)
   ***************************************************************************/
   /*!
   * \brief   Copy Constructor
   * \param   [IN] oClient : Const reference to object to be copied
   * \retval  None
   * \note    Default copy constructor is declared private to disable it. So
   *          that any attempt to copy will be caught by the compiler.
   **************************************************************************/
   spi_tclCenterStackHmiClient(
         const spi_tclCenterStackHmiClient &oClient);

   /***************************************************************************
   ** FUNCTION:  spi_tclCenterStackHmiClient& spi_tclCenterStackHmiClient
   **                ::operator=(const spi_tclCenterStackHmiClient &oClient)
   ***************************************************************************/
   /*!
   * \brief   Assignment Operator
   * \param   [IN] oClient : Const reference to object to be copied
   * \retval  [spi_tclCenterStackHmiClient&]: Reference to this pointer.
   * \note    Assignment operator is declared private to disable it. So
   *          that any attempt for assignment will be caught by the compiler.
   **************************************************************************/
   spi_tclCenterStackHmiClient& operator=(
         const spi_tclCenterStackHmiClient &oClient);


   /***************************************************************************
   *! Data members
   ***************************************************************************/

   /*
   * Main application pointer
   */
   ahl_tclBaseOneThreadApp*   m_poMainApp;

   /***************************************************************************
   ****************************END OF PRIVATE**********************************
   ***************************************************************************/

   /***************************************************************************
   * Declare Msg Map - Utilizing the Framework for message map abstraction.
   ***************************************************************************/
   DECLARE_MSG_MAP(spi_tclCenterStackHmiClient)
};

#endif   // _SPI_TCL_CENTERSTACKHMICLIENT_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
