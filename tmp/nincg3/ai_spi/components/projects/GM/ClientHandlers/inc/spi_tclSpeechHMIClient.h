/*!
 *******************************************************************************
 * \file         spi_tclSpeechHMIClient.h
 * \brief        Speech HMI Client handler class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Speech HMI Client handler class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 06.05.2014 |  Hari Priya E R              | Initial Version
 27.06.2014 |  Ramya Murthy                | Renamed class and added loopback msg implementation.

 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCL_SPEECHHMICLIENT_H_
#define _SPI_TCL_SPEECHHMICLIENT_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
//!Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

//!Include common fi interface
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

//!Include Speech HMI FI type defines
#define MOST_FI_S_IMPORT_INTERFACE_MOST_SPCHMIFI_FUNCTIONIDS
#define MOST_FI_S_IMPORT_INTERFACE_MOST_SPCHMIFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_SPCHMIFI_ERRORCODES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_SPCHMIFI_SERVICEINFO
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_SERVICEINFO
#include "most_fi_if.h"

#include "BaseTypes.h"
#include "spi_tclClientHandlerBase.h"

/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
typedef most_spchmifi_tclMsgBaseMessage spchmi_FiMsgBase;

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/* Forward declarations */

/*!
 * \class spi_tclSpeechHMIClient
 * \brief 
 */

class spi_tclSpeechHMIClient
   : public ahl_tclBaseOneThreadClientHandler, public spi_tclClientHandlerBase
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclSpeechHMIClient::spi_tclSpeechHMIClient(...)
   **************************************************************************/
   /*!
   * \fn      spi_tclSpeechHMIClient(ahl_tclBaseOneThreadApp* poMainAppl)
   * \brief   Overloaded Constructor
   * \param   poMainAppl : [IN] Pointer to main CCA application
   **************************************************************************/
   spi_tclSpeechHMIClient(ahl_tclBaseOneThreadApp* poMainAppl);

   /***************************************************************************
   ** FUNCTION:  spi_tclSpeechHMIClient::~spi_tclSpeechHMIClient()
   **************************************************************************/
   /*!
   * \fn      ~spi_tclSpeechHMIClient()
   * \brief   Destructor
   **************************************************************************/
   virtual ~spi_tclSpeechHMIClient();

   /*********Start of functions overridden from spi_tclClientHandlerBase*****/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSpeechHMIClient::vRegisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers for all interested properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vUnregisterForProperties()
   **************************************************************************/
   virtual t_Void vRegisterForProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSpeechHMIClient::vUnregisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Un-registers all subscribed properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vRegisterForProperties()
   **************************************************************************/
   virtual t_Void vUnregisterForProperties();

   /*********End of functions overridden from spi_tclClientHandlerBase*******/

   /**************************************************************************
   ** FUNCTION:  tBool spi_tclSpeechHMIClient::bSendSRButtonEvent(...
   **************************************************************************/
   /*!
   * \fn      bSendSRButtonEvent(tenKeyCode enKeyCode)
   * \param   enKeyCode : [IN]  Key Code
   * \brief   Method Start message for SR Button Event
   **************************************************************************/
   tBool bSendSRButtonEvent(tenKeyCode enKeyCode);

   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /** Start of functions overridden from ahl_tclBaseOneThreadClientHandler **/

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclSpeechHMIClient::vOnServiceAvailable();
   **************************************************************************/
   /*!
   * \fn      vOnServiceAvailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes available, e.g. server has been started.
   * \sa      vOnServiceUnavailable()
   **************************************************************************/
   virtual tVoid vOnServiceAvailable();

   /**************************************************************************
    ** FUNCTION:  tVoid spi_tclSpeechHMIClient::vOnServiceUnavailable();
   **************************************************************************/
   /*!
   * \fn      vOnServiceUnavailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes unavailable, e.g. server has been shut down.
   * \sa      vOnServiceAvailable()
   **************************************************************************/
   virtual tVoid vOnServiceUnavailable();

   /*** End of functions overridden from ahl_tclBaseOneThreadClientHandler ****/

   /***************************************************************************
   ******************************END OF PROTECTED******************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   * Handler function declarations used by message map.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclSpeechHMIClient::vOnMRSRButtonEvent...
   ***************************************************************************/
   /*!
   * \fn      vOnMRSRButtonEvent(amt_tclServiceData* poMessage)
   * \brief   Method to handle SRButtonEvent.MethodResult message
   * \param   poMessage : [IN] Pointer to message
   **************************************************************************/
   tVoid vOnMRSRButtonEvent(amt_tclServiceData* poMessage) const;

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclSpeechHMIClient::vOnErrorSRButtonEvent(amt_...
   **************************************************************************/
   /*!
   * \fn      vOnErrorSRButtonEvent(amt_tclServiceData* poMessage)
   * \brief   Method to handle SRButtonEvent.MethodError message
   * \param   poMessage : [IN] Pointer to message
   **************************************************************************/
   tVoid vOnErrorSRButtonEvent(amt_tclServiceData* poMessage) const;

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclSpeechHMIClient::vOnSRStatus...
   ***************************************************************************/
   /*!
   * \fn      vOnSRStatus(amt_tclServiceData* poMessage)
   * \brief   Method to handle SRStatus.Status message
   * \param   poMessage : [IN] Pointer to message
   **************************************************************************/
   tVoid vOnSRStatus(amt_tclServiceData* poMessage);

   /**************************************************************************
   * ! Other methods 
   **************************************************************************/

   /**************************************************************************
   ** FUNCTION:  tBool spi_tclSpeechHMIClient::bPostMethodStart(const...
   **************************************************************************/
   /*!
   * \fn      bPostMethodStart(const spchmi_FiMsgBase& rfcooBTSetMS,
   *           t_U32 u32CmdCounter)
   * \brief   Posts a MethodStart message.
   * \param   [IN] spchmi_FiMsgBase : Speech HMI Method Start message type
   * \param   [IN] u32CmdCounter : Command counter value to be set in the message.
   * \retval  tBool: TRUE - if message posting is successful, else FALSE.
   **************************************************************************/
   tBool bPostMethodStart(const spchmi_FiMsgBase& rfcooSpcHMIMS) const;

   /**************************************************************************
   ** FUNCTION:  spi_tclSpeechHMIClient::spi_tclSpeechHMIClient...
   **************************************************************************/
   /*!
   * \fn      spi_tclSpeechHMIClient()
   * \brief   Default Constructor, will not be implemented.
   *          NOTE: This is a technique to disable the Default Constructor for 
   *          this class. So if an attempt for the constructor is made compiler
   *          complains.
    **************************************************************************/
   spi_tclSpeechHMIClient();

   /**************************************************************************
   ** FUNCTION:  spi_tclSpeechHMIClient::spi_tclSpeechHMIClient(const...
   **************************************************************************/
   /*!
   * \fn      spi_tclSpeechHMIClient(const spi_tclSpeechHMIClient& oClient)
   * \brief   Copy Consturctor, will not be implemented.
   *          Avoids Lint Prio 3 warning: Info 1732: new in constructor for 
   *          spi_tclSpeechHMIClient' which has no Copy Consturctor.
   *          NOTE: This is a technique to disable the Copy Consturctor for this
   *          class. So if an attempt for the copying is made linker complains.
   * \param   [IN] oClient : Const reference to object to be copied
    **************************************************************************/
   spi_tclSpeechHMIClient(const spi_tclSpeechHMIClient& oClient);

   /**************************************************************************
   ** FUNCTION:  spi_tclSpeechHMIClient::spi_tclSpeechHMIClient...
   **************************************************************************/
   /*!
   * \fn      spi_tclSpeechHMIClient& operator=(
   *          const spi_tclSpeechHMIClient& oClient)
   * \brief   Assingment Operater, will not be implemented.
   *          Avoids Lint Prio 3 warning: Info 1732: new in constructor for 
   *          class 'spi_tclSpeechHMIClient' which has no assignment operator.
   *          NOTE: This is a technique to disable the assignment operator for this
   *          class. So if an attempt for the assignment is made compiler complains.
    **************************************************************************/
   spi_tclSpeechHMIClient& operator=(const spi_tclSpeechHMIClient& oClient);


   /***************************************************************************
   *! Data members
   ***************************************************************************/

   /*
   * Main application pointer
   */
   ahl_tclBaseOneThreadApp*   m_poMainApp;

   /***************************************************************************
   ****************************END OF PRIVATE**********************************
   ***************************************************************************/

   /***************************************************************************
   * Message map definition macro
   ***************************************************************************/
   DECLARE_MSG_MAP(spi_tclSpeechHMIClient)
};

#endif // _SPI_TCL_SPEECHHMICLIENT_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
