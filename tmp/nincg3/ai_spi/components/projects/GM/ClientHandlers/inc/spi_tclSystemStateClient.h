  /*!
 *******************************************************************************
 * \file         spi_tclSystemStateClient.h
 * \brief        System State client handler class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    System State client handler class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 10.05.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 27.06.2014 |  Ramya Murthy (RBEI/ECP2)         | Renamed class and added loopback msg implementation.
 14.10.2014 |  Hari Priya E R (RBEI/ECP2)       | Added changes to get TimeofDay Property updates

 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCL_SYSTEMSTATECLIENT_H_
#define _SPI_TCL_SYSTEMSTATECLIENT_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
//!Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

//!Include common fi interface
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

#include "BaseTypes.h"
#include "spi_tclClientHandlerBase.h"

/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/* Forward declarations */

/*! 
* \class spi_tclSystemStateClient
* \brief System State client handler class
* 
*/

class spi_tclSystemStateClient
   : public ahl_tclBaseOneThreadClientHandler, public spi_tclClientHandlerBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclSystemStateClient::spi_tclSystemStateClient(...)
   ***************************************************************************/
   /*!
   * \fn      spi_tclSystemStateClient()
   * \brief   Parameterized Constructor
   * \param   [IN] cpoMainApp: Pointer to main application
   * \sa      ~spi_tclSystemStateClient()
   ***************************************************************************/
   explicit spi_tclSystemStateClient(ahl_tclBaseOneThreadApp* const cpoMainApp);

   /***************************************************************************
   ** FUNCTION:  virtual spi_tclSystemStateClient::~spi_tclSystemStateClient()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclSystemStateClient()
   * \brief   Destructor
   * \param   spi_tclSystemStateClient()
   **************************************************************************/
   virtual ~spi_tclSystemStateClient();

   /***************************************************************************
   ** FUNCTION: t_PosixTime spi_tclSystemStateClient::tGetPosixTime( )
   **************************************************************************/
   /*!
   * \fn      tGetPosixTime()
   * \brief   Method used to get the Posix Time
   * \param   None
   * \retval  t_PosixTime
   **************************************************************************/
   t_PosixTime tGetPosixTime();

   /*********Start of functions overridden from spi_tclClientHandlerBase*****/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSystemStateClient::vRegisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers for all interested properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vUnregisterForProperties()
   **************************************************************************/
   virtual t_Void vRegisterForProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSystemStateClient::vUnregisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Un-registers all subscribed properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vRegisterForProperties()
   **************************************************************************/
   virtual t_Void vUnregisterForProperties();

   /*********End of functions overridden from spi_tclClientHandlerBase*******/

   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***** Start of functions overridden from ahl_tclBaseOneThreadService *****/

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclSystemStateClient::vOnServiceAvailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceAvailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes available, e.g. server has been started.
   * \sa      vOnServiceUnavailable()
   **************************************************************************/
   virtual tVoid vOnServiceAvailable();

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclSystemStateClient::vOnServiceUnavailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceUnavailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes unavailable, e.g. server has been shut down.
   * \sa      vOnServiceAvailable()
   **************************************************************************/
   virtual tVoid vOnServiceUnavailable();

   /*** End of functions overridden from ahl_tclBaseOneThreadClientHandler ****/

   /***************************************************************************
   ******************************END OF PROTECTED******************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   * Handler method declarations used by message map.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclSystemStateClient::vOnStatusDayNightMode(amt_tclServiceData...
   **************************************************************************/
   /*!
   * \fn      vOnStatusDayNightMode()
   * \brief   Called by framework when SystemState status message is
   *          sent by SystemState Service.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnStatusDayNightMode(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclSystemStateClient::vOnStatusTimeOfDay(amt_tclServiceData...
   **************************************************************************/
   /*!
   * \fn      vOnStatusTimeOfDay()
   * \brief   Called by framework when SystemTime status message is
   *          sent by SystemState Service.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnStatusTimeOfDay(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclSystemStateClient::vOnStatusValetModeEnabled(...)
   **************************************************************************/
   /*!
   * \fn      vOnStatusValetModeEnabled()
   * \brief   Called by framework when ValetModeEnabled status message is
   *          sent by SystemState Service.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnStatusValetModeEnabled(amt_tclServiceData* poMessage);

   /**************************************************************************
   * ! Other methods
   **************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclSystemStateClient::spi_tclSystemStateClient()
   ***************************************************************************/
   /*!
   * \brief   Default Constructor
   * \param   None
   * \retval  None
   * \note    Default constructor is declared private to disable it. So
   *          that any attempt to create without parameter will be caught by
   *          the compiler.
   **************************************************************************/
   spi_tclSystemStateClient();

   /***************************************************************************
   ** FUNCTION:  spi_tclSystemStateClient::spi_tclSystemStateClient(const..
   ***************************************************************************/
   /*!
   * \brief   Copy Constructor
   * \param   [IN] oClient : Const reference to object to be copied
   * \retval  None
   * \note    Default copy constructor is declared private to disable it. So
   *          that any attempt to copy will be caught by the compiler.
   **************************************************************************/
   spi_tclSystemStateClient(const spi_tclSystemStateClient &oClient);

   /***************************************************************************
   ** FUNCTION:  spi_tclSystemStateClient& spi_tclSystemStateClient::operator=(...
   ***************************************************************************/
   /*!
   * \brief   Assignment Operator
   * \param   [IN] oClient : Const reference to object to be copied
   * \retval  [spi_tclSystemStateClient&]: Reference to this pointer.
   * \note    Assignment operator is declared private to disable it. So
   *          that any attempt for assignment will be caught by the compiler.
   **************************************************************************/
   spi_tclSystemStateClient& operator=(const spi_tclSystemStateClient &oClient);


   /***************************************************************************
   *! Data members
   ***************************************************************************/

   /*
   * Main application pointer
   */
   ahl_tclBaseOneThreadApp*   m_poMainApp;

   /*
   * Posix Time Value
   */
   t_PosixTime m_tPosixTime;

   /***************************************************************************
   ****************************END OF PRIVATE**********************************
   ***************************************************************************/

   /***************************************************************************
   * Declare Msg Map - Utilizing the Framework for message map abstraction.
   ***************************************************************************/
   DECLARE_MSG_MAP(spi_tclSystemStateClient)
};

#endif   // _SPI_TCL_SYSTEMSTATECLIENT_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
