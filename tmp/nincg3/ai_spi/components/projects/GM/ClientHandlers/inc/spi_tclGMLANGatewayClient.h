/*!
 *******************************************************************************
 * \file              spi_tclGMLANGatewayClient.h
 * \brief             GMLAN Gateway Client handler class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    GMLAN Gateway Client handler class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 10.10.2014 |  Hari Priya E R                | Initial Version
 30.06.2015 |  Ramya Murthy                | Changes to read Gear & ParkBrake info from VehicleMovementState,
                                             ElectricParkBrake & ParkBrakeSwitchState properties.

 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCL_GMLANGATEWAYCLIENT_H_
#define _SPI_TCL_GMLANGATEWAYCLIENT_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
//!Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

//!Include common fi interface
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

//!Include GM LAN GateWay FI type defines
#define MOST_FI_S_IMPORT_INTERFACE_MOST_GMLNGWFI_FUNCTIONIDS
#define MOST_FI_S_IMPORT_INTERFACE_MOST_GMLNGWFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_GMLNGWFI_ERRORCODES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_GMLNGWFI_SERVICEINFO
#include "most_fi_if.h"

#include "SPITypes.h"
#include "spi_tclClientHandlerBase.h"

/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/


/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/



/* Forward declarations */

/*!
 * \class spi_tclGMLANGatewayClient
 * \brief GMLAN Gateway Client handler class
 */

class spi_tclGMLANGatewayClient
      : public ahl_tclBaseOneThreadClientHandler
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclGMLANGatewayClient::spi_tclGMLANGatewayClient(...)
   **************************************************************************/
   /*!
   * \fn      spi_tclGMLANGatewayClient(ahl_tclBaseOneThreadApp* poMainAppl)
   * \brief   Overloaded Constructor
   * \param   poMainAppl : [IN] Pointer to main CCA application
   **************************************************************************/
   spi_tclGMLANGatewayClient(ahl_tclBaseOneThreadApp* poMainAppl);

   /***************************************************************************
   ** FUNCTION:  spi_tclGMLANGatewayClient::~spi_tclGMLANGatewayClient()
   **************************************************************************/
   /*!
   * \fn      ~spi_tclGMLANGatewayClient()
   * \brief   Destructor
   **************************************************************************/
   virtual ~spi_tclGMLANGatewayClient();

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vRequestVehicleDataUpdate()
   **************************************************************************/
   /*!
   * \fn     vRequestVehicleDataUpdate()
   * \brief   Function that is used to trigger the sending of the vehicle data 
                           via callback function
   * \retval NONE
   **************************************************************************/
   tVoid vRequestVehicleDataUpdate();

   /*********Start of functions overridden from spi_tclClientHandlerBase*****/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclGMLANGatewayClient::vRegisterForLocDataProperties(
                                   trLocationDataCallbacks rDataSvcLocDataCb,
                                   trSensorDataCallbacks rDataSvcSensorDataCb)
   **************************************************************************/
   /*!
   * \fn      vRegisterForLocDataProperties(
                         trLocationDataCallbacks rDataSvcLocDataCb,
                       trSensorDataCallbacks rDataSvcSensorDataCb)
   * \brief   Registers for Location Data related properties to respective service
   *          Mandatory interface to be implemented.
   * \param   rDataSvcLocDataCb: [IN]Callback function for GPS data
   * \param   rDataSvcSensorDataCb: [IN]Callback function for Sensor data
   * \sa      vUnregisterLocDataForProperties()
   **************************************************************************/
   virtual t_Void vRegisterForLocDataProperties(trLocationDataCallbacks rDataSvcLocDataCb,
                                   trSensorDataCallbacks rDataSvcSensorDataCb);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclGMLANGatewayClient::vRegisterForVehicleDataProperties(
   trVehicleDataCallbacks rVehicleDataCb)
   **************************************************************************/
   /*!
   * \fn      vRegisterForVehicleDataProperties(trVehicleDataCallbacks rVehicleDataCb)
   * \brief   Registers for Vehicle Data related properties to respective service
   *          Mandatory interface to be implemented.
   * \param   rVehicleDataCb: [IN]Callback function for Vehicle data
   * \sa      vUnregisterLocDataForProperties()
   **************************************************************************/
   virtual t_Void vRegisterForVehicleDataProperties(trVehicleDataCallbacks rVehicleDataCb);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclGMLANGatewayClient::vUnregisterForLocDataProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForLocDataProperties()
   * \brief   Un-registers all subscribed properties for Location Data
   *          Mandatory interface to be implemented.
   * \sa      vRegisterForLocDataProperties()
   **************************************************************************/
   virtual t_Void vUnregisterForLocDataProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclGMLANGatewayClient::vUnregisterForVehicleDataProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForVehicleDataProperties()
   * \brief   Un-registers all subscribed properties for Vehicle Data
   *          Mandatory interface to be implemented.
   * \sa      vRegisterForVehicleDataProperties()
   **************************************************************************/
   virtual t_Void vUnregisterForVehicleDataProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclGMLANGatewayClient::vRegForEnvDataUpdates()
   **************************************************************************/
   /*!
   * \fn      t_Void vRegForEnvDataUpdates(trEnvironmentDataCbs rEnvDataCbs)
   * \brief   Registers for Environment Data related properties to respective service
   *          Mandatory interface to be implemented.
   * \param   rEnvDataCbs: [IN]Callback function for Environment data
   * \sa      vUnregisterLocDataForProperties()
   **************************************************************************/
   t_Void vRegForEnvDataUpdates(trEnvironmentDataCbs rEnvDataCbs);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclGMLANGatewayClient::vUnregForEnvDataUpdates()
   **************************************************************************/
   /*!
   * \fn      t_Void vUnregForEnvDataUpdates()
   * \brief   Un-registers all subscribed properties for Environment Data
   *          Mandatory interface to be implemented.
   * \sa      vRegisterForLocDataProperties()
   **************************************************************************/
   t_Void vUnregForEnvDataUpdates();


   /*********End of functions overridden from spi_tclClientHandlerBase*******/

   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /** Start of functions overridden from ahl_tclBaseOneThreadClientHandler **/

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnServiceAvailable();
   **************************************************************************/
   /*!
   * \fn      vOnServiceAvailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes available, e.g. server has been started.
   * \sa      vOnServiceUnavailable()
   **************************************************************************/
   virtual tVoid vOnServiceAvailable() ;
     
   /**************************************************************************
    ** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnServiceUnavailable();
   **************************************************************************/
   /*!
   * \fn      vOnServiceUnavailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes unavailable, e.g. server has been shut down.
   * \sa      vOnServiceAvailable()
   **************************************************************************/
   virtual tVoid vOnServiceUnavailable() ;

   /*** End of functions overridden from ahl_tclBaseOneThreadClientHandler ****/

   /***************************************************************************
   ******************************END OF PROTECTED******************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   * ! Handler method declarations used by message map.
   ***************************************************************************/

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusGPSConfig(amt_tcl...
   **************************************************************************/
   /*!
   * \fn      vOnStatusGPSConfig(amt_tclServiceData* poMessage)
   * \brief   Hand;es the status received for the GPS Configuration property
   * \param   poMessage : [IN] CCA Status Message
   **************************************************************************/
   tVoid vOnStatusGPSConfig(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusGPSGeoPosition(amt_tcl...
   **************************************************************************/
   /*!
   * \fn      vOnStatusGPSGeoPosition(amt_tclServiceData* poMessage)
   * \brief   Handles the status received for the GPS Geographocal Position property
   * \param   poMessage : [IN] CCA Status Message
   **************************************************************************/
   tVoid vOnStatusGPSGeoPosition(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusGPSElevationAndHeading(amt_tcl...
   **************************************************************************/
   /*!
   * \fn      vOnStatusGPSElevationAndHeading(amt_tclServiceData* poMessage)
   * \brief   Handles the status received for the GPS Elevation And Heading property
   * \param   poMessage : [IN] CCA Status Message
   **************************************************************************/
   tVoid vOnStatusGPSElevationAndHeading(amt_tclServiceData* poMessage);

    /**************************************************************************
   ** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusVehicleSpeed(amt_tcl...
   **************************************************************************/
   /*!
   * \fn      vOnStatusVehicleSpeed(amt_tclServiceData* poMessage)
   * \brief   Handles the status received for the GPS Vehicle Speed property
   * \param   poMessage : [IN] CCA Status Message
   **************************************************************************/
   tVoid vOnStatusVehicleSpeed(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusParkBrakeSwitch(amt_tcl...
   **************************************************************************/
   /*!
   * \fn      vOnStatusParkBrakeSwitch(amt_tclServiceData* poMessage)
   * \brief   Handles the status received for the parking brake property
   * \param   poMessage : [IN] CCA Status Message
   **************************************************************************/
   tVoid vOnStatusParkBrakeSwitch(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusGPSDateAndTime(amt_tcl...
   **************************************************************************/
   /*!
   * \fn      vOnStatusGPSDateAndTime(amt_tclServiceData* poMessage)
   * \brief   Handles the status received for the GPS Date And Time property
   * \param   poMessage : [IN] CCA Status Message
   **************************************************************************/
   tVoid vOnStatusGPSDateAndTime(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusVehicleMovementState(amt_tcl...
   **************************************************************************/
   /*!
   * \fn      vOnStatusVehicleMovementState(amt_tclServiceData* poMessage)
   * \brief   Handles the status received for the VehicleMovementState property
   * \param   poMessage : [IN] CCA Status Message
   **************************************************************************/
   tVoid vOnStatusVehicleMovementState(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusElectricParkBrake(amt_tcl...
   **************************************************************************/
   /*!
   * \fn      vOnStatusElectricParkBrake(amt_tclServiceData* poMessage)
   * \brief   Handles the status received for the ElectricParkBrake property
   * \param   poMessage : [IN] CCA Status Message
   **************************************************************************/
   tVoid vOnStatusElectricParkBrake(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusOutsideAirTemperature(amt_tcl...
   **************************************************************************/
   /*!
   * \fn      vOnStatusOutsideAirTemperature(amt_tclServiceData* poMessage)
   * \brief   Handles the status received for the Outside temperature property
   * \param   poMessage : [IN] CCA Status Message
   **************************************************************************/
   tVoid vOnStatusOutsideAirTemperature(amt_tclServiceData* poMessage);

   /**************************************************************************
    ** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusVINDigits10To17(amt_tcl...
    **************************************************************************/
    /*!
    * \fn      vOnStatusVINDigits10To17(amt_tclServiceData* poMessage)
    * \brief   Handles the status received for the VINDigits10To17
    * \param   poMessage : [IN] CCA Status Message
    **************************************************************************/
    tVoid vOnStatusVINDigits10To17(amt_tclServiceData* poMessage);

   /**************************************************************************
   * ! Other methods 
   **************************************************************************/

   /**************************************************************************
   ** FUNCTION:  spi_tclGMLANGatewayClient::spi_tclGMLANGatewayClient()
   **************************************************************************/
   /*!
   * \fn      spi_tclGMLANGatewayClient()
   * \brief   Default Constructor, will not be implemented.
   *          NOTE: This is a technique to disable the Default Constructor for 
   *          this class. So if an attempt for the constructor is made compiler
   *          complains.
    **************************************************************************/
   spi_tclGMLANGatewayClient();

   /**************************************************************************
   ** FUNCTION:  spi_tclGMLANGatewayClient::spi_tclGMLANGatewayClient(const ...)
   **************************************************************************/
   /*!
   * \fn      spi_tclGMLANGatewayClient(const spi_tclGMLANGatewayClient& oClient)
   * \brief   Copy Consturctor, will not be implemented.
   *          Avoids Lint Prio 3 warning: Info 1732: new in constructor for 
   *          class'spi_tclGMLANGatewayClient' which has no Copy Consturctor.
   *          NOTE: This is a technique to disable the Copy Consturctor for this
   *          class. So if an attempt for the copying is made linker complains.
   * \param   poMessage : [IN] Property to be set.
    **************************************************************************/
   spi_tclGMLANGatewayClient(const spi_tclGMLANGatewayClient& oClient);

   /**************************************************************************
   ** FUNCTION:  spi_tclGMLANGatewayClient::spi_tclGMLANGatewayClient(const ...)
   **************************************************************************/
   /*!
   * \fn      spi_tclGMLANGatewayClient& operator=(
   *          const spi_tclGMLANGatewayClient& oClient)
   * \brief   Assingment Operater, will not be implemented.
   *          Avoids Lint Prio 3 warning: Info 1732: new in constructor for 
   *          class 'spi_tclGMLANGatewayClient' which has no assignment operator.
   *          NOTE: This is a technique to disable the assignment operator for this
   *          class. So if an attempt for the assignment is made compiler complains.
    **************************************************************************/
   spi_tclGMLANGatewayClient& operator=(const spi_tclGMLANGatewayClient& oClient);


   /***************************************************************************
   *! Data members
   ***************************************************************************/

   /*
   * Main application pointer
   */
   ahl_tclBaseOneThreadApp*   m_poMainApp;

   /*
   * GPS Data
   */
   trGPSData m_rGPSData;

   /*
   * Sensor Data
   */
   trSensorData m_rSensorData;

   /*
   *Vehicle Data
   */
   trVehicleData m_rVehicleData;

   /*
   *Callbacks for Location data
   */
   trLocationDataCallbacks m_rDataSvcLocDataCb;

   /*
   *Callbacks for Sensor data
   */
   trSensorDataCallbacks m_rDataSvcSensorDataCb;

   /*
   *Callbacks for Vehicle data
   */
   trVehicleDataCallbacks m_rVehicleDataCb;

   /*
   *ParkBrake state reported in ElectricParkBrake property
   */
   t_Bool m_bElectricParkBrakeActive;

   /*
   *ParkBrake state reported in ParkBrakeSwitchState property
   */
   t_Bool m_bParkBrakeSwitchActive;

   /*
   *Callbacks for Environment data
   */
   trEnvironmentDataCbs m_rEnvironmentDataCbs;

   /***************************************************************************
   ****************************END OF PRIVATE**********************************
   ***************************************************************************/

   /***************************************************************************
   * Message map definition macro
   ***************************************************************************/
   DECLARE_MSG_MAP(spi_tclGMLANGatewayClient)
};

#endif // _SPI_TCL_GMLANGATEWAYCLIENT_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
