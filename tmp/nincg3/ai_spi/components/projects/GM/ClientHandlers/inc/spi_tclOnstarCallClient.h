/*!
*******************************************************************************
* \file         spi_tclOnstarCallClient.h
* \brief        Onstar Call Settings Client handler class
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Onstar Call Settings Client handler class 
COPYRIGHT:      &copy; RBEI

HISTORY:
 Date       |  Author                      | Modifications
 27.05.2014 |  Vinoop U                    | Initial Version
 18.06.2014 |  Hari Priya E R              | Updated with Methods for Call Accept and HangUp
 27.06.2014 |  Ramya Murthy                | Renamed class and added loopback msg implementation.
 08.01.2015 |  Shihabudheen P M            | Added 1. vOnStatusEmergencyCallState
                                                   2. vOnStatusC1CallState

\endverbatim
******************************************************************************/

#ifndef _SPI_TCL_ONSCALLCLIENT_H_
#define _SPI_TCL_ONSCALLCLIENT_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
//!Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

//!Include common fi interface
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

//!Include Alert Manager FI type defines
#define MOST_FI_S_IMPORT_INTERFACE_MOST_ONSCALFI_FUNCTIONIDS
#define MOST_FI_S_IMPORT_INTERFACE_MOST_ONSCALFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_ONSCALFI_ERRORCODES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_ONSCALFI_SERVICEINFO
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_SERVICEINFO
#include "most_fi_if.h"

#include "BaseTypes.h"
#include "spi_tclClientHandlerBase.h"

/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
typedef most_onscalfi_tclMsgBaseMessage onscal_FiMsgBase;

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/* Forward declarations */

/*!
 * \class spi_tclOnstarCallClient
 * \brief OnstarData Client handler class
 */

class spi_tclOnstarCallClient
   : public ahl_tclBaseOneThreadClientHandler, public spi_tclClientHandlerBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclOnstarCallClient::spi_tclOnstarCallClient(..)
   ***************************************************************************/
   /*!
   * \fn     spi_tclOnstarCallClient(ahl_tclBaseOneThreadApp* poMainApp)
   * \brief  Parametrized Constructor
   * \param  poMainApp : [IN] Pointer to main app.
   * \sa     ~spi_tclOnstarCallClient()
   **************************************************************************/
   spi_tclOnstarCallClient(ahl_tclBaseOneThreadApp* poMainApp);

   /***************************************************************************
   ** FUNCTION:  virtual spi_tclOnstarCallClient::~spi_tclOnstarCallClient()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclOnstarCallClient()
   * \brief   Destructor
   * \sa      spi_tclOnstarCallClient()
   **************************************************************************/
   virtual ~spi_tclOnstarCallClient();

   /*********Start of functions overridden from spi_tclClientHandlerBase*****/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclOnstarCallClient::vRegisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers for all interested properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vUnregisterForProperties()
   **************************************************************************/
   virtual t_Void vRegisterForProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclOnstarCallClient::vUnregisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Un-registers all subscribed properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vRegisterForProperties()
   **************************************************************************/
   virtual t_Void vUnregisterForProperties();

   /*********End of functions overridden from spi_tclClientHandlerBase*******/

   /**************************************************************************
   ** FUNCTION:  tBool spi_tclOnstarCallClient::bSendCallAcceptTrigger(...
   **************************************************************************/
   /*!
   * \fn      bSendCallAcceptTrigger(tU32 u32NullActionValue)
   * \param  u32NullActionValue: [IN]This Action does not contain an ActionValue. 
             This dummy parameter is included only to simplify ODI gateway  
             internal processing and shall always be transmitted as 0x0000
   * \brief   Triggers the Method start for Onstar Incoming Call Accept 
   **************************************************************************/
   tBool bSendCallAcceptTrigger(tU32 u32NullActionValue = 0);

   /**************************************************************************
   ** FUNCTION:  tBool spi_tclOnstarCallClient::bSendCallHangUpTrigger(...
   **************************************************************************/
   /*!
   * \fn      bSendCallHangUpTrigger(tU32 u32NullActionValue)
   * \param  u32NullActionValue: [IN]This Action does not contain an ActionValue. 
             This dummy parameter is included only to simplify ODI gateway  
             internal processing and shall always be transmitted as 0x0000
   * \brief   Triggers the Method start for Onstar Call HangUp 
   **************************************************************************/
   tBool bSendCallHangUpTrigger(tU32 u32NullActionValue = 0);

   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /** Start of functions overridden from ahl_tclBaseOneThreadClientHandler **/

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclOnstarCallClient::vOnServiceAvailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceAvailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes available, e.g. server has been started.
   * \sa      vOnServiceUnavailable()
   **************************************************************************/
   virtual tVoid vOnServiceAvailable();

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclOnstarCallClient::vOnServiceUnavailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceUnavailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes unavailable, e.g. server has been shut down.
   * \sa      vOnServiceAvailable()
   **************************************************************************/
   virtual tVoid vOnServiceUnavailable();

   /*** End of functions overridden from ahl_tclBaseOneThreadClientHandler ****/

   /***************************************************************************
   ******************************END OF PROTECTED******************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   * Handler function declarations used by message map.
   ***************************************************************************/

  /***************************************************************************
   ** FUNCTION:  tVoid spi_tclOnstarCallClient::vOnStatusC1CallState() ()
   ***************************************************************************/
   /*!
   * \fn      vOnStatusC1CallState(amt_tclServiceData* poMessage)
   * \brief   This Property represents the call state status of the device.
   *          It reflects the status of the call1.
   * \param   poMessage: [IN] Pointer to message
   * \sa
   **************************************************************************/
   tVoid vOnStatusC1CallState(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclOnstarCallClient::vOnStatusC2CallState() ()
   ***************************************************************************/
   /*!
   * \fn      vOnStatusC2CallState(amt_tclServiceData* poMessage)
   * \brief   This Property represents the call state status of the device.
   * \param   poMessage: [IN] Pointer to message
   * \sa
   **************************************************************************/
   tVoid vOnStatusC2CallState(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclOnstarCallClient::vOnStatusEmergencyCallState() ()
   ***************************************************************************/
   /*!
   * \fn      vOnStatusEmergencyCallState(amt_tclServiceData* poMessage)
   * \brief   This Property represents the status of Onstar Emergency call.
   * \param   poMessage: [IN] Pointer to message
   * \sa
   **************************************************************************/
   tVoid vOnStatusEmergencyCallState(amt_tclServiceData* poMessage);
   
   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclOnstarCallClient::vOnErrorCallAccept(amt_...
   **************************************************************************/
   /*!
   * \fn      vOnErrorCallAccept(amt_tclServiceData* poMessage)
   * \brief   Handles the error condition of the Call_AcceptIncoming method
   * \param   poMessage : [IN] Pointer to message
   **************************************************************************/
   tVoid vOnErrorCallAccept(amt_tclServiceData* poMessage) const;

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclOnstarCallClient::vOnErrorCallHangUp(amt_...
   **************************************************************************/
   /*!
   * \fn      vOnErrorCallHangUp(amt_tclServiceData* poMessage)
   * \brief   Handles the error condition of Call_HangUp Method
   * \param   poMessage : [IN] Pointer to message
   **************************************************************************/
   tVoid vOnErrorCallHangUp(amt_tclServiceData* poMessage) const;

   /**************************************************************************
   * ! Other methods 
   **************************************************************************/

   /**************************************************************************
   ** FUNCTION:  tBool spi_tclOnstarCallClient::bPostMethodStart(const...
   **************************************************************************/
   /*!
   * \fn      bPostMethodStart(const onscal_FiMsgBase& rfcooOnsCalMS)
   * \brief   Posts a MethodStart message.
   * \param   [IN] onscal_FiMsgBase : Onstar Call Method Start message type
   * \retval  tBool: TRUE - if message posting is successful, else FALSE.
   **************************************************************************/
   tBool bPostMethodStart(const onscal_FiMsgBase& rfcooOnsCalMS) const;

   /***************************************************************************
   ** FUNCTION:  spi_tclOnstarCallClient::spi_tclOnstarCallClient()
   ***************************************************************************/
   /*!
   * \fn     spi_tclOnstarCallClient()
   * \brief  Default Constructor, will not be implemented.
   **************************************************************************/
   spi_tclOnstarCallClient();

   /***************************************************************************
   ** FUNCTION:  spi_tclOnstarCallClient::spi_tclOnstarCallClient(const ...)
   ***************************************************************************/
   /*!
   * \brief   Copy Constructor
   * \param   [IN] oClient : Const reference to object to be copied
   * \retval  None
   * \note    Default copy constructor is declared private to disable it. So
   *          that any attempt to copy will be caught by the compiler.
   **************************************************************************/
   spi_tclOnstarCallClient(const spi_tclOnstarCallClient &oClient);

   /**************************************************************************
   * Assingment Operater, will not be implemented.
   * Avoids Lint Prio 3 warning: Info 1732: new in constructor for class
   * 'spi_tclOnstarCallClient' which has no assignment operator.
   * NOTE: This is a technique to disable the assignment operator for this
   * class. So if an attempt for the assignment is made compiler complains.
   **************************************************************************/
   spi_tclOnstarCallClient& operator=(const spi_tclOnstarCallClient &oClient);


   /***************************************************************************
   *! Data members
   ***************************************************************************/

   /*
   * Main application pointer
   */
   ahl_tclBaseOneThreadApp*   m_poMainApp;

   /***************************************************************************
   ****************************END OF PRIVATE**********************************
   ***************************************************************************/

   /***************************************************************************
   * Message map definition macro
   ***************************************************************************/
   DECLARE_MSG_MAP(spi_tclOnstarCallClient)
};

#endif // _SPI_TCL_ONSCALLCLIENT_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
