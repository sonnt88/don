  /*!
 *******************************************************************************
 * \file         spi_tclAVManagerClient.h
 * \brief        AVManager client handler class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    AVManager client handler class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 10.12.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCL_AVMANAGERCLIENT_H_
#define _SPI_TCL_AVMANAGERCLIENT_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
//!Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

//! Include AVManager interface
#define MOST_FI_S_IMPORT_INTERFACE_MOST_AVMANFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_AVMANFI_FUNCTIONIDS
#define MOST_FI_S_IMPORT_INTERFACE_MOST_AVMANFI_ERRORCODES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_AVMANFI_SERVICEINFO
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_SERVICEINFO
#include "most_fi_if.h"

#include "BaseTypes.h"
#include "spi_tclClientHandlerBase.h"

/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
typedef most_avmanfi_tclMsgBaseMessage avman_FiMsgBase;

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/* Forward declarations */

/*! 
* \class spi_tclAVManagerClient
* \brief AVManager client handler class
*/

class spi_tclAVManagerClient
   : public ahl_tclBaseOneThreadClientHandler, public spi_tclClientHandlerBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAVManagerClient::spi_tclAVManagerClient(...)
   ***************************************************************************/
   /*!
   * \fn      spi_tclAVManagerClient()
   * \brief   Parameterized Constructor
   * \param   [IN] cpoMainApp: Pointer to main application
   * \sa     ~spi_tclAVManagerClient()
   ***************************************************************************/
   explicit spi_tclAVManagerClient(ahl_tclBaseOneThreadApp* const cpoMainApp);

   /***************************************************************************
   ** FUNCTION:  virtual spi_tclAVManagerClient::~spi_tclAVManagerClient()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAVManagerClient()
   * \brief   Destructor
   * \param   spi_tclAVManagerClient()
   **************************************************************************/
   virtual ~spi_tclAVManagerClient();

   /*********Start of functions overridden from spi_tclClientHandlerBase*****/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAVManagerClient::vRegisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers for all interested properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vUnregisterForProperties()
   **************************************************************************/
   virtual t_Void vRegisterForProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAVManagerClient::vUnregisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Un-registers all subscribed properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vRegisterForProperties()
   **************************************************************************/
   virtual t_Void vUnregisterForProperties();

   /*********End of functions overridden from spi_tclClientHandlerBase*******/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAVManagerClient::vRequestAVActivation(amt_tclServiceData* ..).
   ***************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Sends a RequestAVActivation MethodStart message to AVManager service
   * \sa      vRegisterForProperties()
   **************************************************************************/
   t_Void vRequestAVActivation(const most_fi_tcl_AVManBaseStatusItem& rfcoBaseStatusInfo);

   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /** Start of functions overridden from ahl_tclBaseOneThreadClientHandler **/

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclAVManagerClient::vOnServiceAvailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceAvailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes available, e.g. server has been started.
   * \sa      vOnServiceUnavailable()
   **************************************************************************/
   virtual tVoid vOnServiceAvailable();

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclAVManagerClient::vOnServiceUnavailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceUnavailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes unavailable, e.g. server has been shut down.
   * \sa      vOnServiceAvailable()
   **************************************************************************/
   virtual tVoid vOnServiceUnavailable();

   /*** End of functions overridden from ahl_tclBaseOneThreadClientHandler ****/

   /***************************************************************************
   ******************************END OF PROTECTED******************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   * Handler method declarations used by message map.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAVManagerClient::vOnMRReqAVActivation(amt_tclServiceData* ..).
   ***************************************************************************/
   /*!
   * \fn      vOnMRReqAVActivation
   * \brief   Method to get the result of RequestAVActivation
   * \param   poMessage:  [IN] CCA message data
   * \retval  None
   **************************************************************************/
   t_Void vOnMRReqAVActivation(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAVManagerClient::vOnErrReqAVActivation(amt_tclServiceData* ..).
   ***************************************************************************/
   /*!
   * \fn      vOnErrReqAVActivation
   * \brief   Method to get the error of RequestAVActivation
   * \param   poMessage:  [IN] CCA message data
   * \retval  None
   **************************************************************************/
   t_Void vOnErrReqAVActivation(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION:  tBool spi_tclAVManagerClient::bPostMethodStart(const...
   **************************************************************************/
   /*!
   * \fn      bPostMethodStart(const avman_FiMsgBase& rfcooAVManMS,
   *           t_U32 u32CmdCounter)
   * \brief   Posts a MethodStart message.
   * \param   [IN] rfcooAVManMS : AVManager Method Start message type
   * \param   [IN] u32CmdCounter : Command counter value to be set in the message.
   * \retval  tBool: TRUE - if message posting is successful, else FALSE.
   **************************************************************************/
   tBool bPostMethodStart(const avman_FiMsgBase& rfcooAVManMS) const;

   /***************************************************************************
   ** FUNCTION:  spi_tclAVManagerClient::spi_tclAVManagerClient()
   ***************************************************************************/
   /*!
   * \brief   Default Constructor
   * \param   None
   * \retval  None
   * \note    Default constructor is declared private to disable it. So
   *          that any attempt to create without parameter will be caught by
   *          the compiler.
   **************************************************************************/
   spi_tclAVManagerClient();

   /***************************************************************************
   ** FUNCTION:  spi_tclAVManagerClient::spi_tclAVManagerClient(...)
   ***************************************************************************/
   /*!
   * \brief   Copy Constructor
   * \param   [IN] oClient : Const reference to object to be copied
   * \retval  None
   * \note    Default copy constructor is declared private to disable it. So
   *          that any attempt to copy will be caught by the compiler.
   **************************************************************************/
   spi_tclAVManagerClient(
         const spi_tclAVManagerClient &oClient);

   /***************************************************************************
   ** FUNCTION:  spi_tclAVManagerClient& spi_tclAVManagerClient
   **                ::operator=(const spi_tclAVManagerClient &oClient)
   ***************************************************************************/
   /*!
   * \brief   Assignment Operator
   * \param   [IN] oClient : Const reference to object to be copied
   * \retval  [spi_tclAVManagerClient&]: Reference to this pointer.
   * \note    Assignment operator is declared private to disable it. So
   *          that any attempt for assignment will be caught by the compiler.
   **************************************************************************/
   spi_tclAVManagerClient& operator=(
         const spi_tclAVManagerClient &oClient);


   /***************************************************************************
   *! Data members
   ***************************************************************************/

   /*
   * Main application pointer
   */
   ahl_tclBaseOneThreadApp*   m_poMainApp;

   /***************************************************************************
   ****************************END OF PRIVATE**********************************
   ***************************************************************************/

   /***************************************************************************
   * Declare Msg Map - Utilizing the Framework for message map abstraction.
   ***************************************************************************/
   DECLARE_MSG_MAP(spi_tclAVManagerClient)
};

#endif   // _SPI_TCL_AVMANAGERCLIENT_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
