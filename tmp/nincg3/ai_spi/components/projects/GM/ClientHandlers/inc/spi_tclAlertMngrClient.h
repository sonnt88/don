/*!
 *******************************************************************************
 * \file              spi_tclAlertMngrClient.h
 * \brief             Alert Manager Client handler class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Alert Manager Client handler class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 22.10.2013 |  Ramya Murthy                | Initial Version
 27.06.2014 |  Ramya Murthy                | Renamed class
 11.08.2014 |  Ramya Murthy                | Logic for ML Notifications

 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCL_ALERTMANAGERCLIENT_H_
#define _SPI_TCL_ALERTMANAGERCLIENT_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
//!Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

//!Include common fi interface
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

//!Include Alert Manager FI type defines
#define MOST_FI_S_IMPORT_INTERFACE_MOST_ATMANFI_FUNCTIONIDS
#define MOST_FI_S_IMPORT_INTERFACE_MOST_ATMANFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_ATMANFI_ERRORCODES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_ATMANFI_SERVICEINFO
#include "most_fi_if.h"

#include "SPITypes.h"
#include "spi_tclClientHandlerBase.h"
#include "Lock.h"

/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/
//! Invalid alert ID
static const tU32 cou32INVALID_ALERT_ID      = 0xFFFFFFFF;

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
//! Alert manager message base
typedef most_atmanfi_tclMsgBaseMessage      alrtmngr_FiMsgBase;

//! Alert manager ActivateGrowthAlert MethodStart msg
typedef most_atmanfi_tclMsgActivateGrowthAlertMethodStart  alrtmngr_tFiMSActGrowthAlert;

//! Provides Notification details details
struct trNotiDetails
{
   //! Triggered Alert ID
   tU32 u32AlertID;
   //! Notification data
   trNotiData rNotiData;

   //! Initialize structure members to default
   trNotiDetails() : u32AlertID(cou32INVALID_ALERT_ID)
   {
   }
};



/* Forward declarations */

/*!
 * \class spi_tclAlertMngrClient
 * \brief AlertManager client handler class
 */

class spi_tclAlertMngrClient
      : public ahl_tclBaseOneThreadClientHandler, public spi_tclClientHandlerBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAlertMngrClient::spi_tclAlertMngrClient(...)
   **************************************************************************/
   /*!
   * \fn      spi_tclAlertMngrClient(ahl_tclBaseOneThreadApp* poMainAppl)
   * \brief   Overloaded Constructor
   * \param   poMainAppl : [IN] Pointer to main CCA application
   **************************************************************************/
   spi_tclAlertMngrClient(ahl_tclBaseOneThreadApp* poMainAppl);

   /***************************************************************************
   ** FUNCTION:  spi_tclAlertMngrClient::~spi_tclAlertMngrClient()
   **************************************************************************/
   /*!
   * \fn      ~spi_tclAlertMngrClient()
   * \brief   Destructor
   **************************************************************************/
   virtual ~spi_tclAlertMngrClient();

   /*********Start of functions overridden from spi_tclClientHandlerBase*****/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAlertMngrClient::vRegisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers for all interested properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vUnregisterForProperties()
   **************************************************************************/
   virtual t_Void vRegisterForProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAlertMngrClient::vUnregisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Un-registers all subscribed properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vRegisterForProperties()
   **************************************************************************/
   virtual t_Void vUnregisterForProperties();

   /*********End of functions overridden from spi_tclClientHandlerBase*******/

   /**************************************************************************
   ** FUNCTION:  tBool spi_tclAlertMngrClient::bTriggerNotificationAlert(tU32...
   **************************************************************************/
   /*!
   * \fn      bTriggerNotificationAlert(tU32 u32AppHandle,
   *              const trNotiData& corfrNotiData)
   * \brief   Sends the notification event as an alert and stores the
   *             notification event data.
   * \param   corfrNotiData  : [IN] Notification event data
   **************************************************************************/
   tBool bTriggerNotificationAlert(const trNotiData& corfrNotiData);

   /***************************************************************************
   ** FUNCTION: tVoid spi_tclAlertMngrClient::vSendNotificationAction()
   ***************************************************************************/
   /*!
   * \fn      vGetNotificationData(tU32 u32AlertID, tU32& rfu32AppHandle,
   *              trNotiData& rfrNotiData)
   * \brief   Interface to retrieve Notification data for an Alert.
   * \param   u32AlertID : [IN] Alert ID of the notification
   * \param   rfrNotiData  : [OUT] Notification data structure
   **************************************************************************/
   tVoid vGetNotificationData(tU32 u32AlertID,
         trNotiData& rfrNotiData);

   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /** Start of functions overridden from ahl_tclBaseOneThreadClientHandler **/

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclAlertMngrClient::vOnServiceAvailable();
   **************************************************************************/
   /*!
   * \fn      vOnServiceAvailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes available, e.g. server has been started.
   * \sa      vOnServiceUnavailable()
   **************************************************************************/
   virtual tVoid vOnServiceAvailable() const;
     
   /**************************************************************************
    ** FUNCTION:  tVoid spi_tclAlertMngrClient::vOnServiceUnavailable();
   **************************************************************************/
   /*!
   * \fn      vOnServiceUnavailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes unavailable, e.g. server has been shut down.
   * \sa      vOnServiceAvailable()
   **************************************************************************/
   virtual tVoid vOnServiceUnavailable() const;

   /*** End of functions overridden from ahl_tclBaseOneThreadClientHandler ****/

   /***************************************************************************
   ******************************END OF PROTECTED******************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   * ! Handler method declarations used by message map.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclAlertMngrClient::vOnMRActivateGrowthAlert(...)
   ***************************************************************************/
   /*!
   * \fn      vOnMSActivateGrowthAlert(amt_tclServiceData* poMessage)
   * \brief   Method to handle ActivateGrowthAlert.MethodResult message
   * \param   poMessage : [IN] Pointer to message
   **************************************************************************/
   tVoid vOnMRActivateGrowthAlert(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclAlertMngrClient::vOnMRUpdateGrowthAlert(...)
   **************************************************************************/
   /*!
   * \fn      vOnMSUpdateGrowthAlert(amt_tclServiceData* poMessage)
   * \brief   Method to handle UpdateGrowthAlert.MethodResult message
   * \param   poMessage : [IN] Pointer to message
   **************************************************************************/
   tVoid vOnMRUpdateGrowthAlert(amt_tclServiceData* poMessage) const;

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclAlertMngrClient::vOnErrorActivateGrowthAlert(...)
   **************************************************************************/
   /*!
   * \fn      vOnErrorActivateGrowthAlert(amt_tclServiceData* poMessage)
   * \brief   Method to handle ActivateGrowthAlert.MethodError messages
   * \param   poMessage : [IN] Pointer to message
   **************************************************************************/
   tVoid vOnErrorActivateGrowthAlert(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclAlertMngrClient::vOnErrorUpdateGrowthAlert(...)
   **************************************************************************/
   /*!
   * \fn      vOnErrorUpdateGrowthAlert(amt_tclServiceData* poMessage)
   * \brief   Method to handle UpdateGrowthAlert.MethodError messages
   * \param   poMessage : [IN] Pointer to message
   **************************************************************************/
   tVoid vOnErrorUpdateGrowthAlert(amt_tclServiceData* poMessage) const;

   /**************************************************************************
   * ! Other methods 
   **************************************************************************/

   /**************************************************************************
   ** FUNCTION:  tBool spi_tclAlertMngrClient::bPostMethodStart(const...
   **************************************************************************/
   /*!
   * \fn      bPostMethodStart(const alrtmngr_FiMsgBase& rfcooAtManMS,
   *          const trMsgContext& rfcorMsgCtxt)
   * \brief   Posts the alert manager MethodStart message which is passed to
   *          rfcooAtManMS.
   * \param   rfcooAtManMS : [IN] Alert manager Method Start message type
   * \param   u32CmdCounter : [IN] CmdCounter value to be set in MethodStart message.
   **************************************************************************/
   tBool bPostMethodStart(const alrtmngr_FiMsgBase& rfcooAtManMS,
         tU32 u32CmdCounter);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclAlertMngrClient::vStoreNotiDetails(...)
   ***************************************************************************/
   /*!
   * \fn      vStoreNotiDetails(const trNotiData& corfrNotiData)
   * \brief   Stores the notification data internally.
   * \param   corfrNotiData  : [IN] Structure containing notification data
   **************************************************************************/
   tVoid vStoreNotiDetails(const trNotiData& corfrNotiData);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclAlertMngrClient::vStoreNotiDetails(...)
   ***************************************************************************/
   /*!
   * \fn      vStoreNotiDetails(tU32 u32NotificationID, tU32 u32AlertID)
   * \brief   Updates the AlertID of u32NotificationID in internally stored data.
   * \param   u32NotificationID  : [IN] Unique notification ID
   * \param   u32AlertID  : [IN] Unique Alert ID of notification
   **************************************************************************/
   tVoid vUpdateNotiDetails(tU32 u32NotificationID,
         tU32 u32AlertID);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclAlertMngrClient::vDeleteNotiDetails(...)
   ***************************************************************************/
   /*!
   * \fn      vDeleteNotiDetails(tU32 u32NotificationID)
   * \brief   Removes notification data of u32NotificationID stored internally.
   * \param   u32NotificationID  : [IN] Unique notification ID
   **************************************************************************/
   tVoid vDeleteNotiDetails(tU32 u32NotificationID);

   /***************************************************************************
   ** FUNCTION: tVoid spi_tclAlertMngrClient::vPopulateGrowthAlertBtnInfo()
   ***************************************************************************/
   /*!
   * \fn      vPopulateGrowthAlertBtnInfo(alrtmngr_tFiMSActGrowthAlert& rfAlertMsg,
   *             const std::vector<trNotiAction>& corfrNotiActionList)
   * \brief   Adds alert button(s) info in GrowthAlert MethodStart message.
   * \param   NumAlertActions  : [IN] No. of actions in alert
   * \param   rfAlertMsg  : [IN] GrowthAlert MethodStart message
   * \param   corfrNotiActionList  : [IN] Alert action information list
   **************************************************************************/
   tVoid vPopulateGrowthAlertBtnInfo(t_U8 NumAlertActions,
         alrtmngr_tFiMSActGrowthAlert& rfAlertMsg,
         const std::vector<trNotiAction>& corfrNotiActionList);

   /**************************************************************************
   ** FUNCTION:  spi_tclAlertMngrClient::spi_tclAlertMngrClient()
   **************************************************************************/
   /*!
   * \fn      spi_tclAlertMngrClient()
   * \brief   Default Constructor, will not be implemented.
   *          NOTE: This is a technique to disable the Default Constructor for 
   *          this class. So if an attempt for the constructor is made compiler
   *          complains.
    **************************************************************************/
   spi_tclAlertMngrClient();

   /**************************************************************************
   ** FUNCTION:  spi_tclAlertMngrClient::spi_tclAlertMngrClient(const ...)
   **************************************************************************/
   /*!
   * \fn      spi_tclAlertMngrClient(const spi_tclAlertMngrClient& oClient)
   * \brief   Copy Consturctor, will not be implemented.
   *          Avoids Lint Prio 3 warning: Info 1732: new in constructor for 
   *          class'spi_tclAlertMngrClient' which has no Copy Consturctor.
   *          NOTE: This is a technique to disable the Copy Consturctor for this
   *          class. So if an attempt for the copying is made linker complains.
   * \param   poMessage : [IN] Property to be set.
    **************************************************************************/
   spi_tclAlertMngrClient(const spi_tclAlertMngrClient& oClient);

   /**************************************************************************
   ** FUNCTION:  spi_tclAlertMngrClient::spi_tclAlertMngrClient(const ...)
   **************************************************************************/
   /*!
   * \fn      spi_tclAlertMngrClient& operator=(
   *          const spi_tclAlertMngrClient& oClient)
   * \brief   Assingment Operater, will not be implemented.
   *          Avoids Lint Prio 3 warning: Info 1732: new in constructor for 
   *          class 'spi_tclAlertMngrClient' which has no assignment operator.
   *          NOTE: This is a technique to disable the assignment operator for this
   *          class. So if an attempt for the assignment is made compiler complains.
    **************************************************************************/
   spi_tclAlertMngrClient& operator=(const spi_tclAlertMngrClient& oClient);


   /***************************************************************************
   *! Data members
   ***************************************************************************/

   /*
   * Main application pointer
   */
   ahl_tclBaseOneThreadApp*   m_poMainApp;

   /*
   * Notification details Map (Key = NotiID, Value = NotiDetails)
   */
   std::map<tU32, trNotiDetails>   m_mapNotiDetails;

   /*
   * Lock object
   */
   Lock  m_oLock;

   /***************************************************************************
   ****************************END OF PRIVATE**********************************
   ***************************************************************************/

   /***************************************************************************
   * Message map definition macro
   ***************************************************************************/
   DECLARE_MSG_MAP(spi_tclAlertMngrClient)
};

#endif // _SPI_TCL_ALERTMANAGERCLIENT_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
