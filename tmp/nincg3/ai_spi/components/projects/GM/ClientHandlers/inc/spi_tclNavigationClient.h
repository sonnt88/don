/*!
*******************************************************************************
* \file              spi_tclNavigationClient.h
* \brief             Navigation info Client handler class
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Navigation info Client handler class 
COPYRIGHT:      &copy; RBEI

HISTORY:
 Date       |  Author                      | Modifications
 27.6.2014  |  Vinoop U                    | Initial Version
 27.06.2014 |  Ramya Murthy                | Renamed class and added loopback msg implementation.

\endverbatim
******************************************************************************/

#ifndef _SPI_TCL_NAVIGATIONCLIENT_H_
#define _SPI_TCL_NAVIGATIONCLIENT_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
//!Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

//!Include common fi interface
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

#include "BaseTypes.h"
#include "spi_tclClientHandlerBase.h"

// Forward declaration.
class most_navfi_tclMsgBaseMessage;

/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/* Forward declarations */

/*!
 * \class spi_tclNavigationClient
 * \brief Navigation Client handler class
 */

class spi_tclNavigationClient
   : public ahl_tclBaseOneThreadClientHandler, public spi_tclClientHandlerBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclNavigationClient::spi_tclNavigationClient(..)
   ***************************************************************************/
   /*!
   * \fn     spi_tclNavigationClient(ahl_tclBaseOneThreadApp* poMainApp)
   * \brief  Parametrized Constructor
   * \param  poMainApp : [IN] Pointer to main app.
   * \sa
   **************************************************************************/
   spi_tclNavigationClient(ahl_tclBaseOneThreadApp* poMainApp);

   /***************************************************************************
   ** FUNCTION:  virtual spi_tclNavigationClient::~spi_tclNavigationClient()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclNavigationClient()
   * \brief   Destructor
   * \sa
   **************************************************************************/
   virtual ~spi_tclNavigationClient();

   /*********Start of functions overridden from spi_tclClientHandlerBase*****/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclNavigationClient::vRegisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers for all interested properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vUnregisterForProperties()
   **************************************************************************/
   virtual t_Void vRegisterForProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclNavigationClient::vUnregisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Un-registers all subscribed properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vRegisterForProperties()
   **************************************************************************/
   virtual t_Void vUnregisterForProperties();

   /*********End of functions overridden from spi_tclClientHandlerBase*******/

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclNavigationClient::vSetNavGuidanceStatus()
   **************************************************************************/
   /*!
   * \fn      vSetNavGuidanceStatus()
   * \brief   Function which set the guidance status in the fi.
   * \param   enGuidanceStatus : [IN] Guidance status to be updateed
   * \sa      
   **************************************************************************/
   t_Void vSetNavGuidanceStatus(tenGuidanceStatus enGuidanceStatus);

   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /** Start of functions overridden from ahl_tclBaseOneThreadClientHandler **/

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclNavigationClient::vOnServiceAvailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceAvailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes available, e.g. server has been started.
   * \sa      vOnServiceUnavailable()
   **************************************************************************/
   virtual tVoid vOnServiceAvailable();

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclNavigationClient::vOnServiceUnavailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceUnavailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes unavailable, e.g. server has been shut down.
   * \sa      vOnServiceAvailable()
   **************************************************************************/
   virtual tVoid vOnServiceUnavailable();

   /*** End of functions overridden from ahl_tclBaseOneThreadClientHandler ****/

   /***************************************************************************
   ******************************END OF PROTECTED******************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   * Handler function declarations used by message map.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclNavigationClient::vOnGuidanceStatus() ()
   ***************************************************************************/
   /*!
   * \fn      vOnGuidanceStatus(amt_tclServiceData* poMessage)
   * \brief   This Property represents the current GuidanceStatus.
   * \param   poMessage: [IN] Pointer to GuidanceStatus message
   * \sa
   **************************************************************************/
   tVoid vOnGuidanceStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclNavigationClient::spi_tclNavigationClient()
   ***************************************************************************/
   /*!
   * \fn     spi_tclNavigationClient()
   * \brief  Default Constructor, will not be implemented.
   **************************************************************************/
   spi_tclNavigationClient();

   /***************************************************************************
   ** FUNCTION:  spi_tclNavigationClient::spi_tclNavigationClient(const ...)
   ***************************************************************************/
   /*!
   * \brief   Copy Constructor
   * \param   [IN] oClient : Const reference to object to be copied
   * \retval  None
   * \note    Default copy constructor is declared private to disable it. So
   *          that any attempt to copy will be caught by the compiler.
   **************************************************************************/
   spi_tclNavigationClient(const spi_tclNavigationClient &oClient);

   /**************************************************************************
   * Assingment Operater, will not be implemented.
   * Avoids Lint Prio 3 warning: Info 1732: new in constructor for class
   * 'spi_tclNavigationClient' which has no assignment operator.
   * NOTE: This is a technique to disable the assignment operator for this
   * class. So if an attempt for the assignment is made compiler complains.
   **************************************************************************/
   spi_tclNavigationClient& operator=(const spi_tclNavigationClient &oClientHandler);

   /***************************************************************************
   ** FUNCTION:  spi_tclNavigationClient::bPostMethodStart(const ...)
   ***************************************************************************/
   /*!
   * \brief   bPostMethodStart
   * \param   [IN] rfcoNavMasgBase : Object of Nav fi message base
   * \retval  None
   * \note    Function to trigger the method start cakll to the Nav fi.
   **************************************************************************/
   tBool bPostMethodStart(const most_navfi_tclMsgBaseMessage& rfcoNavMasgBase);


   /***************************************************************************
   *! Data members
   ***************************************************************************/

   /*
   * Main application pointer
   */
   ahl_tclBaseOneThreadApp*   m_poMainApp;


   /***************************************************************************
   ****************************END OF PRIVATE**********************************
   ***************************************************************************/

   /***************************************************************************
   * Message map definition macro
   ***************************************************************************/
   DECLARE_MSG_MAP(spi_tclNavigationClient)
};

#endif // _SPI_TCL_NAVIGATIONCLIENT_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
