/*!
*******************************************************************************
* \file              spi_tclOnstarNavClient.h
* \brief             Onstar Navigation client handler.
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Onstar Navigation client handler
COPYRIGHT:      &copy; RBEI

HISTORY:
 Date        |  Author                      | Modifications
 29.10.2014  |  Shihabudheen P M            | Initial Version

\endverbatim
******************************************************************************/

#ifndef _SPI_TCL_ONSTARNAVCLIENT_H_
#define _SPI_TCL_ONSTARNAVCLIENT_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
//!Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

//!Include common fi interface
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

#include "BaseTypes.h"
#include "spi_tclClientHandlerBase.h"

/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

class most_onsnavfi_tclMsgBaseMessage;

/*!
 * \class spi_tclNavigationClient
 * \brief Navigation Client handler class
 */
class spi_tclOnstarNavClient : 
   public ahl_tclBaseOneThreadClientHandler, public spi_tclClientHandlerBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

  /***************************************************************************
   ** FUNCTION:  spi_tclOnstarNavClient::spi_tclOnstarNavClient(..)
   ***************************************************************************/
   /*!
   * \fn     spi_tclOnstarNavClient(ahl_tclBaseOneThreadApp* poMainApp)
   * \brief  Parametrized Constructor
   * \param  poMainApp : [IN] Pointer to main app.
   * \sa
   **************************************************************************/
   spi_tclOnstarNavClient(ahl_tclBaseOneThreadApp* poMainApp);

  /***************************************************************************
   ** FUNCTION:  spi_tclOnstarNavClient::~spi_tclOnstarNavClient()
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclOnstarNavClient()
   * \brief  Destructor
   * \param
   **************************************************************************/
   ~spi_tclOnstarNavClient();

   /*********Start of functions overridden from spi_tclClientHandlerBase*****/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclOnstarNavClient::vRegisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers for all interested properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vUnregisterForProperties()
   **************************************************************************/
   virtual t_Void vRegisterForProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclOnstarNavClient::vUnregisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Un-registers all subscribed properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vRegisterForProperties()
   **************************************************************************/
   virtual t_Void vUnregisterForProperties();

   /*********End of functions overridden from spi_tclClientHandlerBase*******/
   
   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclOnstarNavClient::bCancelOnstarNavigation()
   ***************************************************************************/
   /*!
   * \fn      bCancelOnstarNavigation()
   * \brief   Function to cancel the Onstar navigation. 
   * \param   NONE
   * \retval  t_Bool : true if canceled successfully, false otherwise.
   * \sa      vOnServiceAvailable()
   **************************************************************************/
   t_Bool bCancelOnstarNavigation();

   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/

protected:

   /**************************************************************************
   ******************************PROTECTED************************************
   ***************************************************************************/

  /** Start of functions overridden from ahl_tclBaseOneThreadClientHandler **/

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclOnstarNavClient::vOnServiceAvailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceAvailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes available, e.g. server has been started.
   * \sa      vOnServiceUnavailable()
   **************************************************************************/
   virtual tVoid vOnServiceAvailable();

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclOnstarNavClient::vOnServiceUnavailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceUnavailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes unavailable, e.g. server has been shut down.
   * \sa      vOnServiceAvailable()
   **************************************************************************/
   virtual tVoid vOnServiceUnavailable();

   /*** End of functions overridden from ahl_tclBaseOneThreadClientHandler ****/

  /***************************************************************************
   ******************************END OF PROTECTED*****************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

  /***************************************************************************
   ** FUNCTION:  spi_tclOnstarNavClient::bPostMethodStart(const ...)
   ***************************************************************************/
   /*!
   * \brief   bPostMethodStart
   * \param   [IN] rfcoNavMasgBase : Object of Onstar Nav fi message base
   * \retval  None
   * \note    Function to trigger the method start cakll to the Nav fi.
   **************************************************************************/
   tBool bPostMethodStart(const most_onsnavfi_tclMsgBaseMessage& rfcoNavMasgBase);

   //! Main application pointer
   ahl_tclBaseOneThreadApp*   m_poMainApp;

  /***************************************************************************
   * Message map definition macro
   ***************************************************************************/
   DECLARE_MSG_MAP(spi_tclOnstarNavClient)

   /***************************************************************************
   **************************END OF PRIVATE************************************
   ***************************************************************************/
};

#endif