/*!
 *******************************************************************************
 * \file         spi_tclOnstarCallClient.cpp
 * \brief        Onstar Call Settings Client handler class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Onstar Call Settings Client handler class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 27.05.2014 |  Vinoop U                    | Initial Version
 18.06.2014 |  Hari Priya E R              | Updated with Methods for Call Accept and HangUp
 27.06.2014 |  Ramya Murthy                | Renamed class and added loopback msg implementation.
 08.01.2015 |  Shihabudheen P M            | Added 1. vOnStatusEmergencyCallState
                                                   2. vOnStatusC1CallState

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_LoopbackTypes.h"
#include "XFiObjHandler.h"
#include "FIMsgDispatch.h"
using namespace shl::msgHandler;
#include "spi_tclOnstarCallClient.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
#include "trcGenProj/Header/spi_tclOnstarCallClient.cpp.trc.h"
#endif

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

typedef XFiObjHandler<most_onscalfi_tclMsgC2_CallStateStatus>
      spi_tXFiOnstarCallStateStatus;
typedef XFiObjHandler<most_onscalfi_tclMsgCall_AcceptIncomingError>
      spi_tXFiOnstarAcceptIncomingErr;
typedef XFiObjHandler<most_onscalfi_tclMsgCall_HangUpError>
      spi_tXFiOnstarHangUpErr;
typedef XFiObjHandler<most_onscalfi_tclMsgC1_CallStateStatus>
      spi_tXFiOnstarC1CallStateStatus;
typedef XFiObjHandler<most_onscalfi_tclMsgEmergencyCall_CallStateStatus>
      spi_tXFiOnstarEmergencyCallStateStatus;
       

typedef most_onscalfi_tclMsgCall_AcceptIncomingMethodStart onscal_tFiAcceptIncomingMS;
typedef most_onscalfi_tclMsgCall_HangUpMethodStart onscal_tFiCallHangUpMS;

/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/
#define ONSCAL_FI_MAJOR_VERSION  MOST_ONSCALFI_C_U16_SERVICE_MAJORVERSION
#define ONSCAL_FI_MINOR_VERSION  MOST_ONSCALFI_C_U16_SERVICE_MINORVERSION

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/
/******************************************************************************/
/*                                                                            */
/* CCA MESSAGE MAP                                                            */
/*                                                                            */
/******************************************************************************/

BEGIN_MSG_MAP(spi_tclOnstarCallClient, ahl_tclBaseWork)
   // Add your ON_MESSAGE_SVCDATA() macros here to define which corresponding
   // method should be called on receiving a specific message.
   ON_MESSAGE_SVCDATA(MOST_ONSCALFI_C_U16_C1_CALLSTATE,
         AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusC1CallState)
   ON_MESSAGE_SVCDATA(MOST_ONSCALFI_C_U16_C2_CALLSTATE,
         AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusC2CallState)
   ON_MESSAGE_SVCDATA(MOST_ONSCALFI_C_U16_EMERGENCYCALL_CALLSTATE,
         AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusEmergencyCallState)
   ON_MESSAGE_SVCDATA( MOST_ONSCALFI_C_U16_CALL_ACCEPTINCOMING ,
         AMT_C_U8_CCAMSG_OPCODE_ERROR, vOnErrorCallAccept)
   ON_MESSAGE_SVCDATA( MOST_ONSCALFI_C_U16_CALL_HANGUP ,
         AMT_C_U8_CCAMSG_OPCODE_ERROR, vOnErrorCallHangUp)
END_MSG_MAP()

/******************************************************************************/
/*                                                                            */
/* METHODS                                                                    */
/*                                                                            */
/******************************************************************************/

/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/*******************************************************************************
 * FUNCTION: spi_tclOnstarCallClient::
 *             spi_tclOnstarCallClient(ahl_tclBaseOneThreadApp* poMainAppl)
 *******************************************************************************/
spi_tclOnstarCallClient::spi_tclOnstarCallClient(ahl_tclBaseOneThreadApp* poMainApp)
: ahl_tclBaseOneThreadClientHandler(
      poMainApp, /* Application Pointer          */
      MOST_ONSCALFI_C_U16_SERVICE_ID, /* ID of used Service           */
      ONSCAL_FI_MAJOR_VERSION, /* MajorVersion of used Service */
      ONSCAL_FI_MINOR_VERSION), /* MinorVersion of used Service */
  m_poMainApp(poMainApp)
{
   ETG_TRACE_USR1(("spi_tclOnstarCallClient() entered "));
   SPI_NORMAL_ASSERT(OSAL_NULL == m_poMainApp);
}

/*******************************************************************************
 * FUNCTION: spi_tclOnstarCallClient::~spi_tclOnstarCallClient()
 *******************************************************************************/
spi_tclOnstarCallClient::~spi_tclOnstarCallClient()
{
   ETG_TRACE_USR1(("~spi_tclOnstarCallClient() entered "));
   m_poMainApp = OSAL_NULL;
}

/*******************************************************************************
 * FUNCTION: tVoid spi_tclOnstarCallClient::vOnServiceAvailable()
 *******************************************************************************/
tVoid spi_tclOnstarCallClient::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("spi_tclOnstarCallClient::vOnServiceAvailable entered "));
}

/*******************************************************************************
 * FUNCTION: tVoid spi_tclOnstarCallClient::vOnServiceUnavailable(
 *******************************************************************************/
tVoid spi_tclOnstarCallClient::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("spi_tclOnstarCallClient::vOnServiceUnavailable entered "));
}

/***************************************************************************
 * FUNCTION:  tVoid spi_tclOnstarCallClient::vRegisterForProperties()
 ***************************************************************************/
tVoid spi_tclOnstarCallClient::vRegisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclOnstarCallClient::vRegisterForProperties entered "));

   //! Register for interested properties
   vAddAutoRegisterForProperty( MOST_ONSCALFI_C_U16_C2_CALLSTATE);
   vAddAutoRegisterForProperty( MOST_ONSCALFI_C_U16_C1_CALLSTATE);
   vAddAutoRegisterForProperty( MOST_ONSCALFI_C_U16_EMERGENCYCALL_CALLSTATE);
   ETG_TRACE_USR2(("[DESC]:vRegisterForProperties: Registered for FuncIDs = 0x%x, 0x%x, 0x%x ",
         MOST_ONSCALFI_C_U16_C2_CALLSTATE,
         MOST_ONSCALFI_C_U16_C1_CALLSTATE,
         MOST_ONSCALFI_C_U16_EMERGENCYCALL_CALLSTATE));
}

/***************************************************************************
 * FUNCTION:  tVoid spi_tclOnstarCallClient::vUnregisterForProperties()
 ***************************************************************************/
tVoid spi_tclOnstarCallClient::vUnregisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclOnstarCallClient::vUnregisterForProperties entered "));

   //! Unregister subscribed properties
   vRemoveAutoRegisterForProperty( MOST_ONSCALFI_C_U16_C2_CALLSTATE);
   vRemoveAutoRegisterForProperty( MOST_ONSCALFI_C_U16_C1_CALLSTATE);
   vRemoveAutoRegisterForProperty( MOST_ONSCALFI_C_U16_EMERGENCYCALL_CALLSTATE);
   ETG_TRACE_USR2(("[DESC]:vUnregisterForProperties: Unregistered for FuncIDs = 0x%x, 0x%x, 0x%x ",
         MOST_ONSCALFI_C_U16_C2_CALLSTATE,
         MOST_ONSCALFI_C_U16_C1_CALLSTATE,
         MOST_ONSCALFI_C_U16_EMERGENCYCALL_CALLSTATE));
}

/***************************************************************************
 ** FUNCTION:  spi_tclOnstarCallClient::vOnErrorCallAccept(...)
 **************************************************************************/
tVoid spi_tclOnstarCallClient::vOnErrorCallAccept(amt_tclServiceData* poMessage) const
{
   ETG_TRACE_USR1(("spi_tclOnstarCallClient::vOnErrorCallAccept entered "));

   spi_tXFiOnstarAcceptIncomingErr oAcceptCallError(*poMessage,
         MOST_ONSCALFI_C_U16_SERVICE_MAJORVERSION);

   if ((true == oAcceptCallError.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      //! Forward received message info as a Loopback message
      tLbOnstarAcceptCallError oAcceptCallErrLbMsg(
            m_poMainApp->u16GetAppId(), // Source AppID
            m_poMainApp->u16GetAppId(), // Target AppID
            0, // RegisterID
            0, // CmdCounter
            MOST_DEVPRJFI_C_U16_SERVICE_ID,        // ServiceID
            SPI_C_U16_IFID_ONSTAR_ACCEPTCALLERROR, // Function ID
            AMT_C_U8_CCAMSG_OPCODE_STATUS          // Opcode
            );

      //!@Note: There is no data to be added to Loopback message.

      if (oAcceptCallErrLbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oAcceptCallErrLbMsg, TRUE))
         {
            ETG_TRACE_ERR(("[ERR]:vOnErrorCallAccept: Loopback message posting failed "));
         }
      } //if (oAcceptCallErrLbMsg().bIsValid())
      else
      {
         ETG_TRACE_ERR(("[ERR]:vOnErrorCallAccept: Loopback message creation failed "));
      }
   }//if ((true == oAcceptCallError.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnErrorCallAccept: Invalid message/Null pointer "));
   }
   ETG_TRACE_USR1(("spi_tclOnstarCallClient::vOnErrorCallAccept left "));
}

/***************************************************************************
 ** FUNCTION:  spi_tclOnstarCallClient::vOnErrorCallHangUp(...)
 **************************************************************************/
tVoid spi_tclOnstarCallClient::vOnErrorCallHangUp(amt_tclServiceData* poMessage) const
{
   ETG_TRACE_USR1(("spi_tclOnstarCallClient::vOnErrorCallHangUp entered "));

   spi_tXFiOnstarHangUpErr oHangUpError(*poMessage, MOST_ONSCALFI_C_U16_SERVICE_MAJORVERSION);

   if ((true == oHangUpError.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      //! Forward received message info as a Loopback message
      tLbOnstarHangUpError oHangUpErrLbMsg(
            m_poMainApp->u16GetAppId(), // Source AppID
            m_poMainApp->u16GetAppId(), // Target AppID
            0, // RegisterID
            0, // CmdCounter
            MOST_DEVPRJFI_C_U16_SERVICE_ID,    // ServiceID
            SPI_C_U16_IFID_ONSTAR_HANGUPERROR, // Function ID
            AMT_C_U8_CCAMSG_OPCODE_STATUS      // Opcode
            );

      //!@Note: There is no data to be added to Loopback message.

      if (oHangUpErrLbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oHangUpErrLbMsg, TRUE))
         {
            ETG_TRACE_ERR(("[ERR]:vOnErrorCallHangUp: Loopback message posting failed "));
         }
      } //if (oHangUpErrLbMsg().bIsValid())
      else
      {
         ETG_TRACE_ERR(("[ERR]:vOnErrorCallHangUp: Loopback message creation failed "));
      }
   }//if ((true == oHangUpError.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnErrorCallHangUp: Invalid message/Null pointer "));
   }
   ETG_TRACE_USR1(("spi_tclOnstarCallClient::vOnErrorCallHangUp left "));
}


/*******************************************************************************
 ** FUNCTION:   spi_tclOnstarCallClient::vOnStatusC1CallState(amt_tclServiceData...
 *******************************************************************************/
tVoid spi_tclOnstarCallClient::vOnStatusC1CallState(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclOnstarCallClient::vOnStatusC1CallState entered "));

   spi_tXFiOnstarC1CallStateStatus oC1CallStateStatus(*poMessage,
         MOST_ONSCALFI_C_U16_SERVICE_MAJORVERSION);

   if ((true == oC1CallStateStatus.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      tenOnstarCallState enCallState =
            static_cast<tenOnstarCallState>(oC1CallStateStatus.e8C1_CallStateValue.enType);

      ETG_TRACE_USR2(("[DESC]:vOnStatusC1CallState: Received C1 call status = %d ",
            ETG_ENUM(ONSTAR_CALL_STATE, enCallState)));

      //! Forward received message info as a Loopback message
      tLbOnStarC1CallState oC1CallStateLbMsg(
            m_poMainApp->u16GetAppId(), // Source AppID
            m_poMainApp->u16GetAppId(), // Target AppID
            0, // RegisterID
            0, // CmdCounter
            MOST_DEVPRJFI_C_U16_SERVICE_ID,     // ServiceID
            SPI_C_U16_IFID_ONSTAR_C1_CALLSTATE, // Function ID
            AMT_C_U8_CCAMSG_OPCODE_STATUS       // Opcode
            );

      //! Add data to message
      oC1CallStateLbMsg.vSetByte(static_cast<tU8> (enCallState));

      if (oC1CallStateLbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oC1CallStateLbMsg, TRUE))
         {
            ETG_TRACE_ERR(("[ERR]:vOnStatusC1CallState: Loopback message posting failed "));
         }
      } //if (oC1CallStateLbMsg().bIsValid())
      else
      {
         ETG_TRACE_ERR(("[ERR]:vOnStatusC1CallState: Loopback message creation failed "));
      }
   }//if ((true == oC1CallStateLbMsg.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnStatusC1CallState: Invalid message/Null pointer "));
   }
}


/*******************************************************************************
 ** FUNCTION:   spi_tclOnstarCallClient::vOnStatusC2CallState(amt_tclServiceData...
 *******************************************************************************/
tVoid spi_tclOnstarCallClient::vOnStatusC2CallState(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclOnstarCallClient::vOnStatusC2CallState entered "));

   spi_tXFiOnstarCallStateStatus oCallStateStatus(*poMessage,
         MOST_ONSCALFI_C_U16_SERVICE_MAJORVERSION);

   if ((true == oCallStateStatus.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      tenOnstarCallState enCallState =
            static_cast<tenOnstarCallState>(oCallStateStatus.e8C2_CallStateValue.enType);

      ETG_TRACE_USR2(("[DESC]:vOnStatusC2CallState: Received C2 call status = %d ",
            ETG_ENUM(ONSTAR_CALL_STATE, enCallState)));

      //! Forward received message info as a Loopback message
      tLbOnstarCallState oCallStateLbMsg(
            m_poMainApp->u16GetAppId(), // Source AppID
            m_poMainApp->u16GetAppId(), // Target AppID
            0, // RegisterID
            0, // CmdCounter
            MOST_DEVPRJFI_C_U16_SERVICE_ID,     // ServiceID
            SPI_C_U16_IFID_ONSTAR_C2_CALLSTATE, // Function ID
            AMT_C_U8_CCAMSG_OPCODE_STATUS       // Opcode
            );

      //! Add data to message
      oCallStateLbMsg.vSetByte(static_cast<tU8> (enCallState));

      if (oCallStateLbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oCallStateLbMsg, TRUE))
         {
            ETG_TRACE_ERR(("[ERR]:vOnStatusC2CallState: Loopback message posting failed "));
         }
      } //if (oCallStateLbMsg().bIsValid())
      else
      {
         ETG_TRACE_ERR(("[ERR]:vOnStatusC2CallState: Loopback message creation failed "));
      }
   }//if ((true == oCallStateStatus.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnStatusC2CallState: Invalid message/Null pointer "));
   }
}

/*******************************************************************************
 ** FUNCTION:   spi_tclOnstarCallClient::vOnStatusEmergencyCallState(amt_tclServiceData...
 *******************************************************************************/
tVoid spi_tclOnstarCallClient::vOnStatusEmergencyCallState(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclOnstarCallClient::vOnStatusEmergencyCallState entered "));

   spi_tXFiOnstarEmergencyCallStateStatus oEmerCallStateStatus(*poMessage,
         MOST_ONSCALFI_C_U16_SERVICE_MAJORVERSION);

   if ((true == oEmerCallStateStatus.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      tenOnstarCallState enCallState =
            static_cast<tenOnstarCallState>(oEmerCallStateStatus.e8EmergencyCall_CallStateValue.enType);

      ETG_TRACE_USR2(("[DESC]:vOnStatusEmergencyCallState: Received Emergency call status = %d ",
            ETG_ENUM(ONSTAR_CALL_STATE, enCallState)));

      //! Forward received message info as a Loopback message
      tLbOnStarEmergencyCallState oEmergencyCallStateLbMsg(
            m_poMainApp->u16GetAppId(), // Source AppID
            m_poMainApp->u16GetAppId(), // Target AppID
            0, // RegisterID
            0, // CmdCounter
            MOST_DEVPRJFI_C_U16_SERVICE_ID,     // ServiceID
            SPI_C_U16_IFID_ONSTAR_EMERGENCY_CALLSTATE, // Function ID
            AMT_C_U8_CCAMSG_OPCODE_STATUS       // Opcode
            );

      //! Add data to message
      oEmergencyCallStateLbMsg.vSetByte(static_cast<tU8> (enCallState));

      if (oEmergencyCallStateLbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oEmergencyCallStateLbMsg, TRUE))
         {
            ETG_TRACE_ERR(("[ERR]:vOnStatusEmergencyCallState: Loopback message posting failed "));
         }
      } //if (oEmergencyCallStateLbMsg().bIsValid())
      else
      {
         ETG_TRACE_ERR(("[ERR]:vOnStatusEmergencyCallState: Loopback message creation failed "));
      }
   }//if ((true == oEmerCallStateStatus.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnStatusEmergencyCallState: Invalid message/Null pointer "));
   }
}

/**************************************************************************
 ** FUNCTION:  tBool spi_tclOnstarCallClient::bSendCallAcceptTrigger(...
 **************************************************************************/
tBool spi_tclOnstarCallClient::bSendCallAcceptTrigger(tU32 u32NullActionValue)
{
   ETG_TRACE_USR1(("spi_tclOnstarCallClient::bSendCallAcceptTrigger entered "));

   tBool bRetVal = TRUE;
   onscal_tFiAcceptIncomingMS oCall_AcceptIncomingMS;

   oCall_AcceptIncomingMS.u32NullActionValue = u32NullActionValue;

   if (FALSE == bPostMethodStart(oCall_AcceptIncomingMS))
   {
      ETG_TRACE_ERR(("[ERR]:bSendCallAcceptTrigger:Sending Call_AcceptIncoming MethodStart failed "));
      bRetVal = FALSE;
   }

   return bRetVal;
}

/**************************************************************************
 ** FUNCTION:  tBool spi_tclOnstarCallClient::bSendCallHangUpTrigger(...
 **************************************************************************/
tBool spi_tclOnstarCallClient::bSendCallHangUpTrigger(tU32 u32NullActionValue)
{
   ETG_TRACE_USR1(("spi_tclOnstarCallClient::bSendCallHangUpTrigger entered "));
   tBool bRetVal = TRUE;

   onscal_tFiCallHangUpMS oCallHangUpMS;

   oCallHangUpMS.u32NullActionValue = u32NullActionValue;

   if (FALSE == bPostMethodStart(oCallHangUpMS))
   {
      ETG_TRACE_ERR(("[ERR]:bSendCallHangUpTrigger:Sending CallHangUp MethodStart failed "));
      bRetVal = FALSE;
   }

   return bRetVal;
}

/**************************************************************************
 ** FUNCTION:  tBool spi_tclOnstarCallClient::bPostMethodStart(const...)
 **************************************************************************/
tBool spi_tclOnstarCallClient::bPostMethodStart(
      const onscal_FiMsgBase& rfcooOnsCalMS) const
{
   tBool bSuccess = FALSE;

   if (OSAL_NULL != m_poMainApp)
   {
      //! Create Msg context
      trMsgContext rMsgCtxt;
      rMsgCtxt.rUserContext.u32SrcAppID = CCA_C_U16_APP_SMARTPHONEINTEGRATION;
      rMsgCtxt.rUserContext.u32DestAppID = u16GetServerAppID();
      rMsgCtxt.rUserContext.u32RegID = u16GetRegID();
      rMsgCtxt.rUserContext.u32CmdCtr = 0;

      //!Post BT settings MethodStart
      FIMsgDispatch oMsgDispatcher(m_poMainApp);
      bSuccess = oMsgDispatcher.bSendMessage(rfcooOnsCalMS, rMsgCtxt, ONSCAL_FI_MAJOR_VERSION);
   }

   ETG_TRACE_USR2(("bPostMethodStart() left with: Message Post Success = %u (FID = 0x%x) ",
         ETG_ENUM(BOOL, bSuccess), rfcooOnsCalMS.u16GetFunctionID()));
   return bSuccess;

}//! end of bPostMethodResult()

///////////////////////////////////////////////////////////////////////////////
// <EOF>
