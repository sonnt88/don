/*!
*******************************************************************************
* \file              spi_tclOnstarNavClient.cpp
* \brief             Onstar navigation client handler
*******************************************************************************
\verbatim
 PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Onstar navigation client handler
COPYRIGHT:      &copy; RBEI

HISTORY:
 Date       |  Author                      | Modifications
29.10.2014  |  Shihabudheen P M            | Initial version

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_LoopbackTypes.h"
#include "XFiObjHandler.h"
#include "FIMsgDispatch.h"
using namespace shl::msgHandler;
#include "spi_tclOnstarNavClient.h"

#define MOST_FI_S_IMPORT_INTERFACE_MOST_ONSNAVFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_ONSNAVFI_SERVICEINFO
#define MOST_FI_S_IMPORT_INTERFACE_MOST_ONSNAVFI_FUNCTIONIDS
#define MOST_FI_S_IMPORT_INTERFACE_MOST_ONSNAVFI_ERRORCODES
#include "most_fi_if.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
#include "trcGenProj/Header/spi_tclOnstarNavClient.cpp.trc.h"
#endif


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

//! Onstar Nav cancel method start
typedef most_onsnavfi_tclMsgCancelNavigationMethodStart OnstarNavCancelMS;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

BEGIN_MSG_MAP(spi_tclOnstarNavClient, ahl_tclBaseWork)
END_MSG_MAP()

/*******************************************************************************
* FUNCTION: spi_tclOnstarNavClient::
*             spi_tclOnstarNavClient(ahl_tclBaseOneThreadApp* poMainAppl)
*******************************************************************************/
spi_tclOnstarNavClient::spi_tclOnstarNavClient(ahl_tclBaseOneThreadApp* poMainApp)
   : ahl_tclBaseOneThreadClientHandler(
   poMainApp,                           /* Application Pointer          */
   MOST_ONSNAVFI_C_U16_SERVICE_ID,              /* ID of used Service           */
   MOST_ONSNAVFI_C_U16_SERVICE_MAJORVERSION,    /* MajorVersion of used Service */
   MOST_ONSNAVFI_C_U16_SERVICE_MINORVERSION),   /* MinorVersion of used Service */
   m_poMainApp(poMainApp)
{
   ETG_TRACE_USR1(("spi_tclOnstarNavClient() entered "));
   SPI_NORMAL_ASSERT(OSAL_NULL == m_poMainApp);
}


/*******************************************************************************
* FUNCTION: spi_tclOnstarNavClient::~spi_tclOnstarNavClient(tVoid)
*******************************************************************************/
spi_tclOnstarNavClient::~spi_tclOnstarNavClient()
{
   ETG_TRACE_USR1(("~spi_tclOnstarNavClient() entered "));
   m_poMainApp = NULL;
}

/*******************************************************************************
* FUNCTION: spi_tclOnstarNavClient::vRegisterForProperties()
*******************************************************************************/
t_Void spi_tclOnstarNavClient::vRegisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclOnstarNavClient::vRegisterForProperties entered "));
}

/*******************************************************************************
* FUNCTION: spi_tclOnstarNavClient::vUnregisterForProperties()
*******************************************************************************/
t_Void spi_tclOnstarNavClient::vUnregisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclOnstarNavClient::vUnregisterForProperties entered "));
}

//! Protected functions
/*******************************************************************************
* FUNCTION: spi_tclOnstarNavClient::vOnServiceAvailable()
*******************************************************************************/
tVoid spi_tclOnstarNavClient::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("spi_tclOnstarNavClient::vOnServiceAvailable entered "));
}

/*******************************************************************************
* FUNCTION: spi_tclOnstarNavClient::vOnServiceUnavailable()
*******************************************************************************/
tVoid spi_tclOnstarNavClient::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("spi_tclOnstarNavClient::vOnServiceUnavailable entered "));
}

/*******************************************************************************
* FUNCTION: spi_tclOnstarNavClient::bCancelOnstarNavigation()
*******************************************************************************/
t_Bool spi_tclOnstarNavClient::bCancelOnstarNavigation()
{
   ETG_TRACE_USR1(("spi_tclOnstarNavClient::bCancelOnstarNavigation entered "));

   t_Bool bRetVal = true;
   OnstarNavCancelMS oOnstarNavCancelMS;
   oOnstarNavCancelMS.u32NullActionValue = 0;

   if (FALSE == bPostMethodStart(oOnstarNavCancelMS))
   {
      bRetVal = false;
      ETG_TRACE_ERR(("[ERR]:bCancelOnstarNavigation: Posting CancelNavigation MethodStart failed "));
   }//if (FALSE == bPostMethodStart(oNavGuidanceStatusSet))
   return bRetVal;
}


/*******************************************************************************
** FUNCTION:   spi_tclOnstarNavClient::bPostMethodStart(most_onsnavfi_tclMsgBaseMessage...
*******************************************************************************/
tBool spi_tclOnstarNavClient::bPostMethodStart(const most_onsnavfi_tclMsgBaseMessage& rfcoNavMasgBase)
{
   tBool bSuccess = FALSE;

   if (OSAL_NULL != m_poMainApp)
   {
      //! Create Msg context
      trMsgContext rMsgCtxt;
      rMsgCtxt.rUserContext.u32SrcAppID  = CCA_C_U16_APP_SMARTPHONEINTEGRATION;
      rMsgCtxt.rUserContext.u32DestAppID = u16GetServerAppID();
      rMsgCtxt.rUserContext.u32RegID     = u16GetRegID();
      rMsgCtxt.rUserContext.u32CmdCtr    = 0;

      //!Post BT settings MethodStart
      FIMsgDispatch oMsgDispatcher(m_poMainApp);
      bSuccess = oMsgDispatcher.bSendMessage(rfcoNavMasgBase, rMsgCtxt, MOST_ONSNAVFI_C_U16_SERVICE_MAJORVERSION);
   }

   ETG_TRACE_USR2(("spi_tclOnstarNavClient::bPostMethodStart() left with: Message Post Success = %u (FID = 0x%x) ",
         ETG_ENUM(BOOL, bSuccess), rfcoNavMasgBase.u16GetFunctionID()));
   return bSuccess;
}
