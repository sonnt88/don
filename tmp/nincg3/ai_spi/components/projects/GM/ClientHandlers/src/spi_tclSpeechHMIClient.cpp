/*!
 *******************************************************************************
 * \file         spi_tclSpeechHMIClient.cpp
 * \brief        Speech HMI Client handler class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Speech HMI Client handler class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 06.05.2014 |  Hari Priya E R              | Initial Version
 27.06.2014 |  Ramya Murthy                | Renamed class and added loopback msg implementation.

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_LoopbackTypes.h"
#include "XFiObjHandler.h"
#include "FIMsgDispatch.h"
using namespace shl::msgHandler;
#include "spi_tclSpeechHMIClient.h"

#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
#include "trcGenProj/Header/spi_tclSpeechHMIClient.cpp.trc.h"
#endif
#endif

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
#define SPCHMI_FI_MAJOR_VERSION  MOST_SPCHMIFI_C_U16_SERVICE_MAJORVERSION
#define SPCHMI_FI_MINOR_VERSION  MOST_SPCHMIFI_C_U16_SERVICE_MINORVERSION


/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
typedef XFiObjHandler<most_spchmifi_tclMsgSRButtonEventMethodResult>  spchmi_XFiSRButtonMR;
typedef XFiObjHandler<most_spchmifi_tclMsgSRButtonEventError>  spchmi_XFiSRButtonEventErr;

typedef most_spchmifi_tclMsgSRButtonEventMethodStart  spchmi_XFiSRButtonMS;

typedef most_fi_tcl_e8_SpcHMISRButton  SpcHMISRButton;

typedef XFiObjHandler<most_spchmifi_tclMsgSRStatusStatus>
   spchmi_XFiSRStatus;

#define SPCHMI_SWC_PTT             (SpcHMISRButton::FI_EN_E8SWC_PTT_BUTTON)
#define SPCHMI_SWC_END             (SpcHMISRButton::FI_EN_E8END_BUTTON)

/******************************************************************************
** CCA MESSAGE MAP
******************************************************************************/
BEGIN_MSG_MAP(spi_tclSpeechHMIClient, ahl_tclBaseWork)

   /* -------------------------------Methods.---------------------------------*/
   ON_MESSAGE_SVCDATA(MOST_SPCHMIFI_C_U16_SRBUTTONEVENT,
   AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnMRSRButtonEvent)
   ON_MESSAGE_SVCDATA( MOST_SPCHMIFI_C_U16_SRBUTTONEVENT,
   AMT_C_U8_CCAMSG_OPCODE_ERROR, vOnErrorSRButtonEvent)
    /* -------------------------------Methods.---------------------------------*/

    /* -------------------------------Property.---------------------------------*/
   ON_MESSAGE_SVCDATA( MOST_SPCHMIFI_C_U16_SRSTATUS,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnSRStatus)
   /* -------------------------------Property.---------------------------------*/

END_MSG_MAP()



/***************************************************************************
 ** FUNCTION:  spi_tclSpeechHMIClient::spi_tclSpeechHMIClient(...)
 **************************************************************************/
spi_tclSpeechHMIClient::
   spi_tclSpeechHMIClient(ahl_tclBaseOneThreadApp* poMainAppl)
      : ahl_tclBaseOneThreadClientHandler(
         poMainAppl,                      /* Application Pointer */
         MOST_SPCHMIFI_C_U16_SERVICE_ID,  /* ID of used Service */
         SPCHMI_FI_MAJOR_VERSION,         /* MajorVersion of used Service */
         SPCHMI_FI_MINOR_VERSION),        /* MinorVersion of used Service */
         m_poMainApp(poMainAppl)
{
   ETG_TRACE_USR1(("spi_tclSpeechHMIClient() entered "));
   SPI_NORMAL_ASSERT(OSAL_NULL == m_poMainApp);
}

/***************************************************************************
 ** FUNCTION:  virtual spi_tclSpeechHMIClient::~spi_tclSpeechHMIClient()
 **************************************************************************/
spi_tclSpeechHMIClient::~spi_tclSpeechHMIClient()
{
   ETG_TRACE_USR1(("~spi_tclSpeechHMIClient() entered "));
   m_poMainApp = OSAL_NULL;
}

/***************************************************************************
 ** FUNCTION:  spi_tclSpeechHMIClient::vOnServiceAvailable();
 **************************************************************************/
tVoid spi_tclSpeechHMIClient::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("spi_tclSpeechHMIClient::vOnServiceAvailable entered "));
   //! Add code here
}

/***************************************************************************
 ** FUNCTION:  spi_tclSpeechHMIClient::vOnServiceUnavailable();
 **************************************************************************/
tVoid spi_tclSpeechHMIClient::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("spi_tclSpeechHMIClient::vOnServiceUnavailable entered "));
   //! Add code here
}

/***************************************************************************
* FUNCTION:  t_Void spi_tclSpeechHMIClient::vRegisterForProperties()
***************************************************************************/
t_Void spi_tclSpeechHMIClient::vRegisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclSpeechHMIClient::vRegisterForProperties entered "));
   //! Register for interested properties
   vAddAutoRegisterForProperty(MOST_SPCHMIFI_C_U16_SRSTATUS);
   ETG_TRACE_USR2(("[DESC]:vRegisterForProperties: Registered for FuncID = 0x%x ",
         MOST_SPCHMIFI_C_U16_SRSTATUS));
}

/***************************************************************************
* FUNCTION:  t_Void spi_tclSpeechHMIClient::vUnregisterForProperties()
***************************************************************************/
t_Void spi_tclSpeechHMIClient::vUnregisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclSpeechHMIClient::vUnregisterForProperties entered "));
   //! Unregister subscribed properties
   vRemoveAutoRegisterForProperty(MOST_SPCHMIFI_C_U16_SRSTATUS);
   ETG_TRACE_USR2(("[DESC]:vUnregisterForProperties: Unregistered for FuncID = 0x%x ",
         MOST_SPCHMIFI_C_U16_SRSTATUS));
}

/***************************************************************************
 ** FUNCTION:  spi_tclSpeechHMIClient::vOnMRSRButtonEvent(...)
 **************************************************************************/
tVoid spi_tclSpeechHMIClient::vOnMRSRButtonEvent(amt_tclServiceData* poMessage) const
{
   ETG_TRACE_USR1(("spi_tclSpeechHMIClient::vOnMRSRButtonEvent entered "));

   spchmi_XFiSRButtonMR oXFiSRButtonResult(*poMessage,SPCHMI_FI_MAJOR_VERSION);

   if ((true == oXFiSRButtonResult.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      tenSpeechRecBtnEventResult enSRBtnEventResult =
            static_cast<tenSpeechRecBtnEventResult>(
                  oXFiSRButtonResult.bNewSpeechSessionStarted);

      ETG_TRACE_USR2(("[DESC]vOnMRSRButtonEvent: Received result = %d ",
            ETG_ENUM(SR_BTNEVENT_RESULT, enSRBtnEventResult)));

      //! Forward received message info as a Loopback message
      tLbSpeechRecBtnEventResult oBtnEventResultbMsg(
            m_poMainApp->u16GetAppId(), // Source AppID
            m_poMainApp->u16GetAppId(), // Target AppID
            0, // RegisterID
            0, // CmdCounter
            MOST_DEVPRJFI_C_U16_SERVICE_ID,          // ServiceID
            SPI_C_U16_IFID_SPEECHREC_BTNEVENTRESULT, // Function ID
            AMT_C_U8_CCAMSG_OPCODE_STATUS            // Opcode
            );

      //! Add data to message
      oBtnEventResultbMsg.vSetByte(static_cast<tU8>(enSRBtnEventResult));

      if (oBtnEventResultbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oBtnEventResultbMsg, TRUE))
         {
            ETG_TRACE_ERR(("[ERR]:vOnMRSRButtonEvent: Loopback message posting failed "));
         }
      } //if (oBtnEventResultbMsg().bIsValid())
      else
      {
         ETG_TRACE_ERR(("[ERR]:vOnMRSRButtonEvent: Loopback message creation failed "));
      }
   }//if ((true == oXFiSRButtonResult.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnMRSRButtonEvent: Invalid message/Null pointer "));
   }
   ETG_TRACE_USR1(("spi_tclSpeechHMIClient::vOnMRSRButtonEvent left "));
}

/***************************************************************************
 ** FUNCTION:  spi_tclSpeechHMIClient::vOnErrorSRButtonEvent(...)
 **************************************************************************/
tVoid spi_tclSpeechHMIClient::vOnErrorSRButtonEvent(amt_tclServiceData* poMessage) const
{
   ETG_TRACE_USR1(("spi_tclSpeechHMIClient::vOnErrorSRButtonEvent entered "));

   spchmi_XFiSRButtonEventErr oXFiSRButtonEventErr(*poMessage,SPCHMI_FI_MAJOR_VERSION);

   if ((true == oXFiSRButtonEventErr.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      //! Forward received message info as a Loopback message
      tLbSpeechRecBtnEventResult oBtnEventResultbMsg(
            m_poMainApp->u16GetAppId(), // Source AppID
            m_poMainApp->u16GetAppId(), // Target AppID
            0, // RegisterID
            0, // CmdCounter
            MOST_DEVPRJFI_C_U16_SERVICE_ID,          // ServiceID
            SPI_C_U16_IFID_SPEECHREC_BTNEVENTRESULT, // Function ID
            AMT_C_U8_CCAMSG_OPCODE_STATUS            // Opcode
            );

      //! Add data to message
      oBtnEventResultbMsg.vSetByte(static_cast<tU8>(e8NEW_SESSION_NOT_STARTED));

      if (oBtnEventResultbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oBtnEventResultbMsg, TRUE))
         {
            ETG_TRACE_ERR(("[ERR]:vOnErrorSRButtonEvent: Loopback message posting failed "));
         }
      } //if (oBtnEventResultbMsg.bIsValid())
      else
      {
         ETG_TRACE_ERR(("[ERR]:vOnErrorSRButtonEvent: Loopback message creation failed "));
      }
   }//if ((true == oXFiSRButtonEventErr.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnErrorSRButtonEvent: Invalid message/Null pointer "));
   }
   ETG_TRACE_USR1(("spi_tclSpeechHMIClient::vOnErrorSRButtonEvent left "));
}

/**************************************************************************
** FUNCTION:  tBool spi_tclSpeechHMIClient::vOnSRStatus(...)
**************************************************************************/
tVoid spi_tclSpeechHMIClient::vOnSRStatus(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclSpeechHMIClient::vOnSRStatus entered "));

   spchmi_XFiSRStatus oSRStatus(*poMessage, SPCHMI_FI_MAJOR_VERSION);

   if ((true == oSRStatus.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      tenSpeechRecStatus enSRStatus =
            static_cast<tenSpeechRecStatus> (oSRStatus.e8SRStatus.enType);

      ETG_TRACE_USR2(("[DESC]vOnSRStatus: Received SpeechRecognition Status = %d ",
            ETG_ENUM(SR_STATUS, enSRStatus)));

      //! Forward received message info as a Loopback message
      tLbSpeechRecStatus oSRStatusLbMsg(
            m_poMainApp->u16GetAppId(), // Source AppID
            m_poMainApp->u16GetAppId(), // Target AppID
            0, // RegisterID
            0, // CmdCounter
            MOST_DEVPRJFI_C_U16_SERVICE_ID,    // ServiceID
            SPI_C_U16_IFID_SPEECHREC_SRSTATUS, // Function ID
            AMT_C_U8_CCAMSG_OPCODE_STATUS      // Opcode
            );

      //! Add data to message
      oSRStatusLbMsg.vSetByte(static_cast<tU8> (enSRStatus));

      if (oSRStatusLbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oSRStatusLbMsg, TRUE))
         {
            ETG_TRACE_ERR(("[ERR]:vOnSRStaus: Loopback message posting failed "));
         }
      } //if (oSRStatusLbMsg.bIsValid())
      else
      {
         ETG_TRACE_ERR(("[ERR]:vOnSRStaus: Loopback message creation failed "));
      }
   }//if ((true == oSRStatus.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnSRStaus: Invalid message/Null pointer "));
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclSpeechHMIClient::bSendSRButtonEvent(...)
 **************************************************************************/
tBool spi_tclSpeechHMIClient::bSendSRButtonEvent(tenKeyCode enKeyCode)
{
   ETG_TRACE_USR1(("spi_tclSpeechHMIClient::bSendSRButtonEvent Key Code -%d", ETG_ENUM(KEY_CODE, enKeyCode)));

   tBool bRetVal = FALSE;
   spchmi_XFiSRButtonMS oSRButtonEventMS;

   if(enKeyCode == e32DEV_PTT)
   {
      oSRButtonEventMS.e8SRButton.enType= SPCHMI_SWC_PTT;
   }
   else if(enKeyCode == e32DEV_PHONE_END)
   {
      oSRButtonEventMS.e8SRButton.enType= SPCHMI_SWC_END;
   }
   if (FALSE == bPostMethodStart(oSRButtonEventMS))
   {
      ETG_TRACE_ERR(("[ERR]:bSendSRButtonEvent: Posting SRButtonEvent MethodStart failed "));
   }

   return bRetVal;
}

/**************************************************************************
** FUNCTION:  tBool spi_tclSpeechHMIClient::bPostMethodStart(const...)
**************************************************************************/
tBool spi_tclSpeechHMIClient::bPostMethodStart(
      const spchmi_FiMsgBase& rfcooSpcHMIMS) const
{
   tBool bSuccess = FALSE;

   if (OSAL_NULL != m_poMainApp)
   {
      //! Create Msg context
      trMsgContext rMsgCtxt;
      rMsgCtxt.rUserContext.u32SrcAppID  = CCA_C_U16_APP_SMARTPHONEINTEGRATION;
      rMsgCtxt.rUserContext.u32DestAppID = u16GetServerAppID();
      rMsgCtxt.rUserContext.u32RegID     = u16GetRegID();
      rMsgCtxt.rUserContext.u32CmdCtr    = 0;

      //!Post BT settings MethodStart
      FIMsgDispatch oMsgDispatcher(m_poMainApp);
      bSuccess = oMsgDispatcher.bSendMessage(rfcooSpcHMIMS, rMsgCtxt, SPCHMI_FI_MAJOR_VERSION);
   }

   ETG_TRACE_USR2(("spi_tclSpeechHMIClient::bPostMethodStart() left with: Message Post Success = %u (FID = 0x%x) ",
         ETG_ENUM(BOOL, bSuccess), rfcooSpcHMIMS.u16GetFunctionID()));
   return bSuccess;

}//! end of bPostMethodStart()

///////////////////////////////////////////////////////////////////////////////
// <EOF>

