  /*!
 *******************************************************************************
 * \file         spi_tclSystemStateClient.cpp
 * \brief        System State client handler class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    System State client handler class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 10.05.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 27.06.2014 |  Ramya Murthy (RBEI/ECP2)         | Renamed class
 14.10.2014 |  Hari Priya E R (RBEI/ECP2)       | Added changes to get TimeofDay Property updates
 30.12.2014 |  Ramya Murthy (RBEI/ECP2)         | Fix for GMMY16-22433
 27.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors
 \endverbatim
 ******************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <ctime>
#include "SPITypes.h"
#include "spi_LoopbackTypes.h"
#include "XFiObjHandler.h"
using namespace shl::msgHandler;
#include "spi_tclSystemStateClient.h"

//! Include SystemState interface
#define MOST_FI_S_IMPORT_INTERFACE_MOST_SYSTAFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_SYSTAFI_FUNCTIONIDS
#define MOST_FI_S_IMPORT_INTERFACE_MOST_SYSTAFI_ERRORCODES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_SYSTAFI_SERVICEINFO
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_SERVICEINFO
#include "most_fi_if.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
      #include "trcGenProj/Header/spi_tclSystemStateClient.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/
#define SYSTATE_FI_MAJOR_VERSION  MOST_SYSTAFI_C_U16_SERVICE_MAJORVERSION
#define SYSTATE_FI_MINOR_VERSION  MOST_SYSTAFI_C_U16_SERVICE_MINORVERSION

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
typedef XFiObjHandler<most_systafi_tclMsgDayNightModeStatus>      sysst_tXFiStDayNightMode;
typedef XFiObjHandler<most_systafi_tclMsgTimeOfDayStatus>         sysst_tXFiStTimeOfDay;
typedef XFiObjHandler<most_systafi_tclMsgValetModeEnabledStatus>  sysst_tXFiStValetModeEnabled;

typedef tenSystemStateDayNightMode  tenDayNightMode;

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| Utilizing the Framework for message map abstraction.
|----------------------------------------------------------------------------*/

BEGIN_MSG_MAP(spi_tclSystemStateClient, ahl_tclBaseOneThreadClientHandler)

    ON_MESSAGE_SVCDATA(MOST_SYSTAFI_C_U16_DAYNIGHTMODE,
    AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusDayNightMode)
    ON_MESSAGE_SVCDATA(MOST_SYSTAFI_C_U16_TIMEOFDAY,
    AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusTimeOfDay)
    ON_MESSAGE_SVCDATA(MOST_SYSTAFI_C_U16_VALETMODEENABLED,
    AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusValetModeEnabled)

END_MSG_MAP()


//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/******************************************************************************
| function implementation (scope: external-interfaces)
|----------------------------------------------------------------------------*/

/******************************************************************************
** FUNCTION:  spi_tclSystemStateClient::spi_tclSystemStateClient(ahl_..
******************************************************************************/
/*explicit*/
spi_tclSystemStateClient::spi_tclSystemStateClient(ahl_tclBaseOneThreadApp* const cpoMainApp)
   : ahl_tclBaseOneThreadClientHandler(
         cpoMainApp,
         MOST_SYSTAFI_C_U16_SERVICE_ID,
         SYSTATE_FI_MAJOR_VERSION,
         SYSTATE_FI_MINOR_VERSION),
     m_poMainApp(cpoMainApp)
{
   ETG_TRACE_USR1(("spi_tclSystemStateClient() entered "));
   SPI_NORMAL_ASSERT(OSAL_NULL == m_poMainApp);
}//! end of spi_tclSystemStateClient()

/******************************************************************************
** FUNCTION:  virtual spi_tclSystemStateClient::~spi_tclSystemStateClient()
******************************************************************************/
spi_tclSystemStateClient::~spi_tclSystemStateClient()
{
   ETG_TRACE_USR1(("~spi_tclSystemStateClient() entered "));
   m_poMainApp = OSAL_NULL;
}//! end of ~spi_tclSystemStateClient()

/******************************************************************************
** FUNCTION:  virtual tVoid spi_tclSystemStateClient::vOnServiceAvailable()
******************************************************************************/
tVoid spi_tclSystemStateClient::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("spi_tclSystemStateClient::vOnServiceAvailable entered "));
   vAddAutoRegisterForProperty(MOST_SYSTAFI_C_U16_DAYNIGHTMODE);
   vAddAutoRegisterForProperty(MOST_SYSTAFI_C_U16_VALETMODEENABLED);
}//! end of vOnServiceAvailable()

/******************************************************************************
** FUNCTION:  virtual tVoid spi_tclSystemStateClient::vOnServiceUnavailable()
******************************************************************************/
/*virtual*/
tVoid spi_tclSystemStateClient::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("spi_tclSystemStateClient::vOnServiceUnavailable entered "));
   vRemoveAutoRegisterForProperty(MOST_SYSTAFI_C_U16_DAYNIGHTMODE);
   vRemoveAutoRegisterForProperty(MOST_SYSTAFI_C_U16_VALETMODEENABLED);
}//! end of vOnServiceUnavailable()

/***************************************************************************
** FUNCTION:  tVoid spi_tclSystemStateClient::vRegisterForProperties()
**************************************************************************/
tVoid spi_tclSystemStateClient::vRegisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclSystemStateClient::vRegisterForProperties entered "));

   //! Register for interested properties
   vAddAutoRegisterForProperty(MOST_SYSTAFI_C_U16_TIMEOFDAY);
   ETG_TRACE_USR2(("[DESC]:vRegisterForProperties: Registered for FuncID = 0x%x ",
         MOST_SYSTAFI_C_U16_TIMEOFDAY));
}//! end of vRegisterForProperties()

/***************************************************************************
** FUNCTION:  tVoid spi_tclSystemStateClient::vUnregisterForProperties()
**************************************************************************/
tVoid spi_tclSystemStateClient::vUnregisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclSystemStateClient::vUnregisterForProperties entered "));

   //! Unregister subscribed properties
   vRemoveAutoRegisterForProperty(MOST_SYSTAFI_C_U16_TIMEOFDAY);
   ETG_TRACE_USR2(("[DESC]:vUnregisterForProperties: Unregistered for FuncID = 0x%x ",
         MOST_SYSTAFI_C_U16_TIMEOFDAY));
}//! end of vUnregisterForProperties()

/***************************************************************************
** FUNCTION:  spi_tclSystemStateClient::vOnStatusDayNightMode(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclSystemStateClient::vOnStatusDayNightMode(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclSystemStateClient::vOnStatusDayNightMode entered "));

   sysst_tXFiStDayNightMode oXFiStDayNightMode(*poMessage, SYSTATE_FI_MAJOR_VERSION);

   if ((oXFiStDayNightMode.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      tenDayNightMode enDayNightMode =
            (true == oXFiStDayNightMode.bDayNightMode) ? (e8NIGHT_MODE) : (e8DAY_MODE);

      ETG_TRACE_USR2(("[DESC]:vOnStatusDayNightMode: Received DayNight Mode = %u ",
            ETG_ENUM(DAYNIGHT_MODE, enDayNightMode)));

      //! Forward received message info as a Loopback message
      tLbSysStaDayNightMode oDayNightModeLbMsg
         (
            m_poMainApp->u16GetAppId(), // Source AppID
            m_poMainApp->u16GetAppId(), // Target AppID
            0, // RegisterID
            0, // CmdCounter
            MOST_DEVPRJFI_C_U16_SERVICE_ID,       // ServiceID
            SPI_C_U16_IFID_SYSSTATE_DAYNIGHTMODE, // Function ID
            AMT_C_U8_CCAMSG_OPCODE_STATUS         // Opcode
         );

      //! Add data to message
      oDayNightModeLbMsg.vSetByte(static_cast<tU8>(enDayNightMode));

      //! Post loopback message
      if (oDayNightModeLbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oDayNightModeLbMsg, TRUE))
         {
            ETG_TRACE_ERR(("[ERR]:vOnStatusDayNightMode: Loopback message posting failed "));
         }
      } //if (oDayNightModeLbMsg.bIsValid())
      else
      {
         ETG_TRACE_ERR(("[ERR]:vOnStatusDayNightMode: Loopback message creation failed "));
      }
   } //if ((oXFiStDayNightMode.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnStatusDayNightMode: Invalid message/Null pointer "));
   }

   ETG_TRACE_USR1(("spi_tclSystemStateClient::vOnStatusDayNightMode left "));

}//! end of vOnStatusDayNightMode()

/***************************************************************************
** FUNCTION:  spi_tclSystemStateClient::vOnStatusDayNightMode(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclSystemStateClient::vOnStatusTimeOfDay(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclSystemStateClient::vOnStatusTimeOfDay entered "));

   sysst_tXFiStTimeOfDay oXFiStTimeOfDay(*poMessage, SYSTATE_FI_MAJOR_VERSION);

   if ((oXFiStTimeOfDay.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      ETG_TRACE_USR4(("[DESC]:vOnStatusTimeOfDay: Received TimeofDay is "
            "Hours:%d, Minutes:%d, Seconds:%d, Day:%d, Month:%d, Year:%d ",
            oXFiStTimeOfDay.oTimeOfDayStream.u8HourOfDay,
            oXFiStTimeOfDay.oTimeOfDayStream.u8MinuteOfHour,
            oXFiStTimeOfDay.oTimeOfDayStream.u8SecondsOfMinute,
            oXFiStTimeOfDay.oTimeOfDayStream.u8CalendarDay,
            oXFiStTimeOfDay.oTimeOfDayStream.e8CalendarMonth.enType,
            oXFiStTimeOfDay.oTimeOfDayStream.u8CalendarYear));

      //time_t timer;   //Commented to remove Lint Warning
      struct tm rTime;

      rTime.tm_mday = oXFiStTimeOfDay.oTimeOfDayStream.u8CalendarDay;

      tU8 u8Month = static_cast<tU8>(oXFiStTimeOfDay.oTimeOfDayStream.e8CalendarMonth.enType);
      /*In struct tm,the month value should be months since January.
      Hence if the month is greater than 1,subtract 19 from it to get the required value.*/
      rTime.tm_mon = (1<= u8Month)?(u8Month -1):u8Month;

      /*According to FCAT,we need to do the following calculation to get the actual value:E=N*1 + 2000,
      where N is the value received by the property update
      In struct tm,the year value should be years since 1900.
      Hence if the year is greater than 1900,subtract 1900 from it to get the required value.
      So the value will be (oXFiStTimeOfDay.oTimeOfDayStream.u8CalendarYear)+2000-1900*/
      rTime.tm_year = (oXFiStTimeOfDay.oTimeOfDayStream.u8CalendarYear)+100;

      tU8 u8Hour = oXFiStTimeOfDay.oTimeOfDayStream.u8HourOfDay;
      rTime.tm_hour = (u8Hour<= 23)? u8Hour:0;

      tU8 u8Minutes = oXFiStTimeOfDay.oTimeOfDayStream.u8MinuteOfHour;
      rTime.tm_min = (u8Minutes <=59)?u8Minutes:0;

      tU8 u8Seconds = oXFiStTimeOfDay.oTimeOfDayStream.u8SecondsOfMinute;
      rTime.tm_sec = (u8Seconds <= 61)? u8Seconds:0;

      //Convert the Date and time to Posix format
      m_tPosixTime = static_cast<t_PosixTime>(mktime(&rTime));

      ETG_TRACE_USR4(("[PARAM]:vOnStatusTimeOfDay: Calculated Posix Time:%d ",m_tPosixTime));

      //! Forward received message info as a Loopback message
      tLbSysStaTimeOfDay oTimeOfDayLbMsg
         (
            m_poMainApp->u16GetAppId(), // Source AppID
            m_poMainApp->u16GetAppId(), // Target AppID
            0, // RegisterID
            0, // CmdCounter
            MOST_DEVPRJFI_C_U16_SERVICE_ID,    // ServiceID
            SPI_C_U16_IFID_SYSSTATE_TIMEOFDAY, // Function ID
            AMT_C_U8_CCAMSG_OPCODE_STATUS      // Opcode
         );

      //! Post loopback message
      if (oTimeOfDayLbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oTimeOfDayLbMsg, TRUE))
         {
            ETG_TRACE_ERR(("[ERR]:vOnStatusTimeOfDay: Loopback message posting failed "));
         }
      } //if (oTimeOfDayLbMsg.bIsValid())
      else
      {
         ETG_TRACE_ERR(("[ERR]:vOnStatusTimeOfDay: Loopback message creation failed "));
      }
   } //if ((oXFiStTimeOfDay.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnStatusTimeOfDay: Invalid message/Null pointer "));
   }

   ETG_TRACE_USR1(("spi_tclSystemStateClient::vOnStatusTimeOfDay left "));

}//! end of vOnStatusDayNightMode()

/***************************************************************************
** FUNCTION:  spi_tclSystemStateClient::vOnStatusValetModeEnabled(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclSystemStateClient::vOnStatusValetModeEnabled(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclSystemStateClient::vOnStatusValetModeEnabled entered "));

   sysst_tXFiStValetModeEnabled oXFiStValetModeEnabled(*poMessage, SYSTATE_FI_MAJOR_VERSION);

   if ((oXFiStValetModeEnabled.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      ETG_TRACE_USR2(("[DESC]:vOnStatusValetModeEnabled: ValetMode is enabled = %u ",
            ETG_ENUM(BOOL, oXFiStValetModeEnabled.bValetModeFlag)));

      //! Forward received message info as a Loopback message
      tLbSysStaValetModeEnabled oValetModeLbMsg
         (
            m_poMainApp->u16GetAppId(), // Source AppID
            m_poMainApp->u16GetAppId(), // Target AppID
            0, // RegisterID
            0, // CmdCounter
            MOST_DEVPRJFI_C_U16_SERVICE_ID,    // ServiceID
            SPI_C_U16_IFID_SYSSTATE_VALETMODE, // Function ID
            AMT_C_U8_CCAMSG_OPCODE_STATUS      // Opcode
         );

      //! Add data to message
      oValetModeLbMsg.vSetByte(static_cast<tU8>(oXFiStValetModeEnabled.bValetModeFlag));

      //! Post loopback message
      if (oValetModeLbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oValetModeLbMsg, TRUE))
         {
            ETG_TRACE_ERR(("[ERR]:vOnStatusValetModeEnabled: Loopback message posting failed "));
         }
      } //if (oValetModeLbMsg.bIsValid())
      else
      {
         ETG_TRACE_ERR(("[ERR]:vOnStatusValetModeEnabled: Loopback message creation failed "));
      }
   } //if ((oXFiStValetModeEnabled.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnStatusValetModeEnabled: Invalid message/Null pointer "));
   }

   ETG_TRACE_USR1(("spi_tclSystemStateClient::vOnStatusValetModeEnabled left "));

}//! end of vOnStatusValetModeEnabled()

/***************************************************************************
** FUNCTION: t_PosixTime spi_tclSystemStateClient::tGetPosixTime().
**************************************************************************/
t_PosixTime spi_tclSystemStateClient::tGetPosixTime()
{
   return m_tPosixTime;
}

//lint –restore
////////////////////////////////////////////////////////////////////////////////
// <EOF>
