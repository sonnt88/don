  /*!
 *******************************************************************************
 * \file         spi_tclCenterStackHmiClient.cpp
 * \brief        CenterStackHMI client handler class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    CenterStackHMI client handler class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 10.05.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 27.06.2014 |  Ramya Murthy (RBEI/ECP2)         | Renamed class

 \endverbatim
 ******************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_LoopbackTypes.h"
#include "XFiObjHandler.h"
using namespace shl::msgHandler;
#include "spi_tclCenterStackHmiClient.h"

//! Include CenterstackHMI interface
#define MOST_FI_S_IMPORT_INTERFACE_MOST_CNTSTKFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_CNTSTKFI_FUNCTIONIDS
#define MOST_FI_S_IMPORT_INTERFACE_MOST_CNTSTKFI_ERRORCODES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_CNTSTKFI_SERVICEINFO
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_SERVICEINFO
#include "most_fi_if.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
      #include "trcGenProj/Header/spi_tclCenterStackHmiClient.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/
#define CNTRSTCKHMI_FI_MAJOR_VERSION  MOST_CNTSTKFI_C_U16_SERVICE_MAJORVERSION
#define CNTRSTCKHMI_FI_MINOR_VERSION  MOST_CNTSTKFI_C_U16_SERVICE_MINORVERSION

/******************************************************************************
| typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
typedef XFiObjHandler<most_cntstkfi_tclMsgDayNightModeOverrideSettingStatus>
      cntrstck_tXFiStDayNightModeOvrSet;

typedef tenCenterStackHmiDayNightOverride    tenDayNightOverrideStatus;

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| Utilizing the Framework for message map abstraction.
|----------------------------------------------------------------------------*/

BEGIN_MSG_MAP(spi_tclCenterStackHmiClient, ahl_tclBaseOneThreadClientHandler)

    ON_MESSAGE_SVCDATA(MOST_CNTSTKFI_C_U16_DAYNIGHTMODEOVERRIDESETTING,
    AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusDayNightModeOverrideSetting)

END_MSG_MAP()


/******************************************************************************
| function implementation (scope: external-interfaces)
|----------------------------------------------------------------------------*/

/******************************************************************************
** FUNCTION:  spi_tclCenterStackHmiClient::spi_tclCenterStackHmiClient(ahl_..
******************************************************************************/
/*explicit*/
spi_tclCenterStackHmiClient::spi_tclCenterStackHmiClient(ahl_tclBaseOneThreadApp* const cpoMainApp)
   : ahl_tclBaseOneThreadClientHandler(
         cpoMainApp,
         MOST_CNTSTKFI_C_U16_SERVICE_ID,
         CNTRSTCKHMI_FI_MAJOR_VERSION,
         CNTRSTCKHMI_FI_MINOR_VERSION),
      m_poMainApp(cpoMainApp)
{
   ETG_TRACE_USR1(("spi_tclCenterStackHmiClient() entered "));
   SPI_NORMAL_ASSERT(OSAL_NULL == m_poMainApp);
}//! end of spi_tclCenterStackHmiClient()

/******************************************************************************
** FUNCTION:  virtual spi_tclCenterStackHmiClient::~spi_tclCenterStackHmiClient()
******************************************************************************/
spi_tclCenterStackHmiClient::~spi_tclCenterStackHmiClient()
{
   ETG_TRACE_USR1(("~spi_tclCenterStackHmiClient() entered "));
   m_poMainApp = OSAL_NULL;
}//! end of ~spi_tclCenterStackHmiClient()

/******************************************************************************
** FUNCTION:  virtual tVoid spi_tclCenterStackHmiClient::vOnServiceAvailable()
******************************************************************************/
tVoid spi_tclCenterStackHmiClient::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("spi_tclCenterStackHmiClient::vOnServiceAvailable entered "));
   vRegisterForProperties();
}//! end of vOnServiceAvailable()

/******************************************************************************
** FUNCTION:  virtual tVoid spi_tclCenterStackHmiClient::vOnServiceUnavailable()
******************************************************************************/
tVoid spi_tclCenterStackHmiClient::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("spi_tclCenterStackHmiClient::vOnServiceUnavailable entered "));
   vUnregisterForProperties();
}//! end of vOnServiceUnavailable()

/***************************************************************************
** FUNCTION:  tVoid spi_tclCenterStackHmiClient::vRegisterForProperties()
**************************************************************************/
tVoid spi_tclCenterStackHmiClient::vRegisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclCenterStackHmiClient::vRegisterForProperties entered "));

   //! Register for interested properties
   vAddAutoRegisterForProperty(MOST_CNTSTKFI_C_U16_DAYNIGHTMODEOVERRIDESETTING);
   ETG_TRACE_USR2(("[DESC]:vRegisterForProperties: Registered for FuncID = 0x%x ",
         MOST_CNTSTKFI_C_U16_DAYNIGHTMODEOVERRIDESETTING));
}//! end of vRegisterForProperties()

/***************************************************************************
** FUNCTION:  tVoid spi_tclCenterStackHmiClient::vUnregisterForProperties()
**************************************************************************/
tVoid spi_tclCenterStackHmiClient::vUnregisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclCenterStackHmiClient::vUnregisterForProperties entered "));

   //! Unregister subscribed properties
   vRemoveAutoRegisterForProperty(MOST_CNTSTKFI_C_U16_DAYNIGHTMODEOVERRIDESETTING);
   ETG_TRACE_USR2(("[DESC]:vUnregisterForProperties: Unregistered for FuncID = 0x%x ",
         MOST_CNTSTKFI_C_U16_DAYNIGHTMODEOVERRIDESETTING));
}//! end of vUnregisterForProperties()

/***************************************************************************
** FUNCTION:  spi_tclCenterStackHmiClient::vOnStatusDayNightModeOverrideSetting(...
**************************************************************************/
tVoid spi_tclCenterStackHmiClient::vOnStatusDayNightModeOverrideSetting(
      amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclCenterStackHmiClient::vOnStatusDayNightModeOverrideSetting entered "));

   cntrstck_tXFiStDayNightModeOvrSet oXFiStDayNightModeOvrSet(*poMessage, CNTRSTCKHMI_FI_MAJOR_VERSION);

   if ((oXFiStDayNightModeOvrSet.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      tenCenterStackHmiDayNightOverride enDayNightOvdStatus
            = static_cast<tenDayNightOverrideStatus>
               (oXFiStDayNightModeOvrSet.e8DayNightModeOverrideSetting.enType);

      ETG_TRACE_USR2(("[DESC]:vOnStatusDayNightModeOverrideSetting: Received DayNightMode = %u ",
            ETG_ENUM(DAYNIGHT_MODE_HMI_OVERRIDE, enDayNightOvdStatus)));

      //! Forward received message info as a Loopback message
      tLbCntrStckHmiDayNightMode oDayNightModeLbMsg
              (
                 m_poMainApp->u16GetAppId(),             // Source AppID
                 m_poMainApp->u16GetAppId(),             // Target AppID
                 0,                                      // RegisterID
                 0,                                      // CmdCounter
                 MOST_DEVPRJFI_C_U16_SERVICE_ID,         // ServiceID
                 SPI_C_U16_IFID_CNTRSTCKHMI_DAYNIGHTMODE,// Function ID
                 AMT_C_U8_CCAMSG_OPCODE_STATUS           // Opcode
              );

      //! Add data to message
      oDayNightModeLbMsg.vSetByte(static_cast<tU8>(enDayNightOvdStatus));

      //! Post loopback message
      if (oDayNightModeLbMsg.bIsValid())
      {
        if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oDayNightModeLbMsg, TRUE))
        {
           ETG_TRACE_ERR(("[ERR]:vOnStatusDayNightModeOverrideSetting: "
                 "Loopback message posting failed "));
        }
      } //if (oDayNightModeLbMsg().bIsValid())
      else
      {
        ETG_TRACE_ERR(("[ERR]:vOnStatusDayNightModeOverrideSetting: "
              "Loopback message creation failed "));
      }
   } //if ((oXFiStDayNightModeOvrSet.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnStatusDayNightModeOverrideSetting: Invalid message/Null pointer "));
   }

   ETG_TRACE_USR1(("spi_tclCenterStackHmiClient::vOnStatusDayNightModeOverrideSetting left "));

}//! end of vOnStatusDayNightModeOverrideSetting()


////////////////////////////////////////////////////////////////////////////////
// <EOF>
