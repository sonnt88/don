  /*!
 *******************************************************************************
 * \file         spi_tclAVManagerClient.cpp
 * \brief        AVManager client handler class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    AVManager client handler class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 10.12.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "XFiObjHandler.h"
#include "FIMsgDispatch.h"
using namespace shl::msgHandler;
#include "spi_tclAVManagerClient.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
      #include "trcGenProj/Header/spi_tclAVManagerClient.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/
#define AVMAN_FI_MAJOR_VERSION  MOST_AVMANFI_C_U16_SERVICE_MAJORVERSION
#define AVMAN_FI_MINOR_VERSION  MOST_AVMANFI_C_U16_SERVICE_MINORVERSION

/******************************************************************************
| typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
typedef most_avmanfi_tclMsgRequestAVActivationMethodStart   avman_tMSReqAVActivation;

typedef XFiObjHandler<most_avmanfi_tclMsgRequestAVActivationMethodResult>  avman_tXFiMRReqAVActivation;
//typedef XFiObjHandler<most_avmanfi_tclMsgRequestAVActivationError>  avman_tXFiErrReqAVActivation;

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| Utilizing the Framework for message map abstraction.
|----------------------------------------------------------------------------*/

BEGIN_MSG_MAP(spi_tclAVManagerClient, ahl_tclBaseOneThreadClientHandler)

   ON_MESSAGE_SVCDATA( MOST_AVMANFI_C_U16_REQUESTAVACTIVATION,
   AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnMRReqAVActivation)
   ON_MESSAGE_SVCDATA( MOST_AVMANFI_C_U16_REQUESTAVACTIVATION,
   AMT_C_U8_CCAMSG_OPCODE_ERROR, vOnErrReqAVActivation)

END_MSG_MAP()


/******************************************************************************
| function implementation (scope: external-interfaces)
|----------------------------------------------------------------------------*/

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/******************************************************************************
** FUNCTION:  spi_tclAVManagerClient::spi_tclAVManagerClient(ahl_..
******************************************************************************/
/*explicit*/
spi_tclAVManagerClient::spi_tclAVManagerClient(ahl_tclBaseOneThreadApp* const cpoMainApp)
   : ahl_tclBaseOneThreadClientHandler(
         cpoMainApp,
         MOST_AVMANFI_C_U16_SERVICE_ID,
         AVMAN_FI_MAJOR_VERSION,
         AVMAN_FI_MINOR_VERSION),
      m_poMainApp(cpoMainApp)
{
   ETG_TRACE_USR1(("spi_tclAVManagerClient() entered "));
   SPI_NORMAL_ASSERT(OSAL_NULL == m_poMainApp);
}//! end of spi_tclAVManagerClient()

/******************************************************************************
** FUNCTION:  virtual spi_tclAVManagerClient::~spi_tclAVManagerClient()
******************************************************************************/
spi_tclAVManagerClient::~spi_tclAVManagerClient()
{
   ETG_TRACE_USR1(("~spi_tclAVManagerClient() entered "));
   m_poMainApp = OSAL_NULL;
}//! end of ~spi_tclAVManagerClient()

/***************************************************************************
** FUNCTION:  tVoid spi_tclAVManagerClient::vRegisterForProperties()
**************************************************************************/
tVoid spi_tclAVManagerClient::vRegisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclAVManagerClient::vRegisterForProperties entered "));
   //! Register for interested properties
}//! end of vRegisterForProperties()

/***************************************************************************
** FUNCTION:  tVoid spi_tclAVManagerClient::vUnregisterForProperties()
**************************************************************************/
tVoid spi_tclAVManagerClient::vUnregisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclAVManagerClient::vUnregisterForProperties entered "));
   //! Unregister subscribed properties
}//! end of vUnregisterForProperties()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAVManagerClient::vRequestAVActivation(amt_tclServiceData* ..).
***************************************************************************/
t_Void spi_tclAVManagerClient::vRequestAVActivation(
      const most_fi_tcl_AVManBaseStatusItem& rfcoBaseStatusInfo)
{
   ETG_TRACE_USR1(("spi_tclAVManagerClient::vRequestAVActivation entered "));

   //! Activate last audio source
   avman_tMSReqAVActivation oMSReqAVAct;
   oMSReqAVAct.u8SourceFBlock = rfcoBaseStatusInfo.u8SourceFBlock;
   oMSReqAVAct.u8SourceInstID = rfcoBaseStatusInfo.u8SourceInstID;
   oMSReqAVAct.u8SourceNr = rfcoBaseStatusInfo.u8SourceNr;
   oMSReqAVAct.e8LogicalAVChannel.enType = rfcoBaseStatusInfo.e8LogicalAVChannel.enType;
   ETG_TRACE_USR4(("[PARAM]:vRequestAVActivation: FBlockID:%d, Src Number:%d, Src Instance:%d, Logical Channel:%d",
            oMSReqAVAct.u8SourceFBlock, oMSReqAVAct.u8SourceNr, oMSReqAVAct.u8SourceInstID,
            oMSReqAVAct.e8LogicalAVChannel.enType));

   //!Post RequestAVActivation MethodStart
   if (FALSE == bPostMethodStart(oMSReqAVAct))
   {
      ETG_TRACE_ERR(("[ERR]:vRequestAVActivation: Sending RequestAVActivation MethodStart failed "));
   }//if (false == bPostMethodStart(oMSReqAVAct))
}

/***************************************************************************
*********************************PROTECTED**********************************
***************************************************************************/

/******************************************************************************
** FUNCTION:  virtual tVoid spi_tclAVManagerClient::vOnServiceAvailable()
******************************************************************************/
tVoid spi_tclAVManagerClient::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("spi_tclAVManagerClient::vOnServiceAvailable entered "));
}//! end of vOnServiceAvailable()

/******************************************************************************
** FUNCTION:  virtual tVoid spi_tclAVManagerClient::vOnServiceUnavailable()
******************************************************************************/
tVoid spi_tclAVManagerClient::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("spi_tclAVManagerClient::vOnServiceUnavailable entered "));
}//! end of vOnServiceUnavailable()

/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  t_Void spi_tclAVManagerClient::vOnMRReqAVActivation(amt_tclServiceData* ..).
***************************************************************************/
t_Void spi_tclAVManagerClient::vOnMRReqAVActivation(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclAVManagerClient::vOnMRReqAVActivation entered "));

   avman_tXFiMRReqAVActivation oXFiMRReqAVActivation(*poMessage);
   if (true == oXFiMRReqAVActivation.bIsValid())
   {
      ETG_TRACE_USR2(("[DESC]:vOnMRReqAVActivation: Received audio activation result = %d "
            "(for SrcFBlock = %d, SrcInstID = %d, SrcNum = %d, LogicalAVChannel = %d) ",
            oXFiMRReqAVActivation.e8RequestResult.enType,
            oXFiMRReqAVActivation.u8SourceFBlock, oXFiMRReqAVActivation.u8SourceInstID,
            oXFiMRReqAVActivation.u8SourceNr, oXFiMRReqAVActivation.e8LogicalAVChannel.enType));
   }//if (true == oXFiMRReqAVActivation.bIsValid())
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnMRReqAVActivation: Msg extraction failed! "));
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAVManagerClient::vOnErrReqAVActivation(amt_tclServiceData* ..).
***************************************************************************/
t_Void spi_tclAVManagerClient::vOnErrReqAVActivation(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclAVManagerClient::vOnErrReqAVActivation entered "));
   SPI_INTENTIONALLY_UNUSED(poMessage)
   //@Note: Nothing to be done
}

/**************************************************************************
** FUNCTION:  tBool spi_tclAVManagerClient::bPostMethodStart(const...)
**************************************************************************/
tBool spi_tclAVManagerClient::bPostMethodStart(
      const avman_FiMsgBase& rfcooAVManMS) const
{
   tBool bSuccess = FALSE;

   if (OSAL_NULL != m_poMainApp)
   {
      //! Create Msg context
      trMsgContext rMsgCtxt;
      rMsgCtxt.rUserContext.u32SrcAppID  = CCA_C_U16_APP_SMARTPHONEINTEGRATION;
      rMsgCtxt.rUserContext.u32DestAppID = u16GetServerAppID();
      rMsgCtxt.rUserContext.u32RegID     = u16GetRegID();
      rMsgCtxt.rUserContext.u32CmdCtr    = 0;

      //!Post BT settings MethodStart
      FIMsgDispatch oMsgDispatcher(m_poMainApp);
      bSuccess = oMsgDispatcher.bSendMessage(rfcooAVManMS, rMsgCtxt, MOST_AVMANFI_C_U16_SERVICE_MAJORVERSION);
   }

   ETG_TRACE_USR2(("spi_tclAVManagerClient::bPostMethodStart() left with: Message Post Success = %u (FID = 0x%x) ",
         ETG_ENUM(BOOL, bSuccess), rfcooAVManMS.u16GetFunctionID()));
   return bSuccess;
}//! end of bPostMethodStart()

////////////////////////////////////////////////////////////////////////////////
// <EOF>
