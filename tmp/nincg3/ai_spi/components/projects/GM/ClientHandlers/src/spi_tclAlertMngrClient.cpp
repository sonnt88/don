/*!
 *******************************************************************************
 * \file              spi_tclAlertMngrClient.cpp
 * \brief             Alert Manager Client handler class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Alert Manager Client handler class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 22.10.2013 |  Ramya Murthy                | Initial Version
 27.06.2014 |  Ramya Murthy                | Renamed class
 11.08.2014 |  Ramya Murthy                | Logic for ML Notifications
 06.05.2015  |Tejaswini HB                 |Lint Fix

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "XFiObjHandler.h"
#include "FIMsgDispatch.h"
using namespace shl::msgHandler;
#include "spi_tclAlertMngrClient.h"

#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
#include "trcGenProj/Header/spi_tclAlertMngrClient.cpp.trc.h"
#endif
#endif

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
#define ALERTMAN_FI_MAJOR_VERSION  MOST_ATMANFI_C_U16_SERVICE_MAJORVERSION
#define ALERTMAN_FI_MINOR_VERSION  MOST_ATMANFI_C_U16_SERVICE_MINORVERSION

//! Character set type in MOST FI
#define MOSTFI_CHAR_SET_UTF8   (most_fi_tcl_String::FI_EN_UTF8)

//! Device projection alert button actions
#define DEV_PRJ_ACTION_1   (most_fi_tcl_e8_AtManButtonAction::FI_EN_E8DEVICE_PROJECTION_APPLICATION_ACTION_1)
#define DEV_PRJ_ACTION_2   (most_fi_tcl_e8_AtManButtonAction::FI_EN_E8DEVICE_PROJECTION_APPLICATION_ACTION_2)
#define DEV_PRJ_ACTION_3   (most_fi_tcl_e8_AtManButtonAction::FI_EN_E8DEVICE_PROJECTION_APPLICATION_ACTION_3)
//#define DISMISS_ACTION     (most_fi_tcl_e8_AtManButtonAction::FI_EN_E8DISMISS_ACTION)

#define ACK_TYPE_AUTO_MANUAL   (most_fi_tcl_e8_AtManAckType::FI_EN_E8ACK_TYPE_AUTO_MANUAL)
#define ACK_TYPE_MANUAL_ONLY   (most_fi_tcl_e8_AtManAckType::FI_EN_E8ACK_TYPE_MANUAL_ONLY)

//! CenterStack display type
#define CNTRSTACK_DISPLAY  (most_fi_tcl_e8_AtManDisplayType::FI_EN_E8DISPLAY_TYPE_CENTERSTACK)

#define ADD_DISPLAY_TYPE(FI_DISPLAY_STREAM_OBJ, FI_DISPLAY_TYPE) \
      alrtmngr_tFiDisplayType oFiDisplayType;    \
      oFiDisplayType.enType = FI_DISPLAY_TYPE;   \
      FI_DISPLAY_STREAM_OBJ.e8Items.push_back(oFiDisplayType);

//! Macro to add button action types to ButtonActionStream object
#define ADD_BTN_ACTION(FI_ACTION_STREAM_OBJ, FI_ACTION)  \
      alrtmngr_tFiBtnAction oFiBtnAction; \
      oFiBtnAction.enType = FI_ACTION;   \
      FI_ACTION_STREAM_OBJ.e8Items.push_back(oFiBtnAction);

#define STRING_NOT_EMPTY(STRING)  ("" != STRING)

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
typedef most_fi_tcl_e8_AtManButtonAction  alrtmngr_tFiBtnAction;
typedef most_fi_tcl_e8_AtManDisplayType   alrtmngr_tFiDisplayType;

typedef XFiObjHandler<most_atmanfi_tclMsgActivateGrowthAlertMethodResult>  alrtmngr_tXFiMRActGrowthAlert;
//typedef XFiObjHandler<most_atmanfi_tclMsgUpdateGrowthAlertMethodResult>    alrtmngr_tXFiMRUpdGrowthAlert;
typedef XFiObjHandler<most_atmanfi_tclMsgActivateGrowthAlertError>         alrtmngr_tXFiErrActGrowthAlert;
//typedef XFiObjHandler<most_atmanfi_tclMsgUpdateGrowthAlertError>           alrtmngr_tXFiErrUpdGrowthAlert;

//! Iterator to NotiDetails map
typedef std::map<tU32, trNotiDetails>::iterator    tItrNotiDetailsMap;

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

//! Maximum no. of actions that can be present in an alert
static const tU32 cou32MAX_ALERT_ACTIONS     = 3;
//! Maximum time for auto-dismissal of alert
static const tU8 coU8MAX_AUTO_TIMEOUT = 10;

/******************************************************************************
** CCA MESSAGE MAP
******************************************************************************/
BEGIN_MSG_MAP(spi_tclAlertMngrClient, ahl_tclBaseWork)

   /* -------------------------------Methods.---------------------------------*/
   ON_MESSAGE_SVCDATA(MOST_ATMANFI_C_U16_ACTIVATEGROWTHALERT,
   AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnMRActivateGrowthAlert)
   ON_MESSAGE_SVCDATA( MOST_ATMANFI_C_U16_ACTIVATEGROWTHALERT,
   AMT_C_U8_CCAMSG_OPCODE_ERROR, vOnErrorActivateGrowthAlert)
   ON_MESSAGE_SVCDATA( MOST_ATMANFI_C_U16_UPDATEGROWTHALERT,
   AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnMRUpdateGrowthAlert)
   ON_MESSAGE_SVCDATA(MOST_ATMANFI_C_U16_UPDATEGROWTHALERT,
   AMT_C_U8_CCAMSG_OPCODE_ERROR, vOnErrorUpdateGrowthAlert)

   /* -------------------------------Methods.---------------------------------*/

END_MSG_MAP()



/***************************************************************************
 ** FUNCTION:  spi_tclAlertMngrClient::spi_tclAlertMngrClient(...
 **************************************************************************/
spi_tclAlertMngrClient::spi_tclAlertMngrClient(ahl_tclBaseOneThreadApp* poMainAppl)
      : ahl_tclBaseOneThreadClientHandler(
         poMainAppl,                     /* Application Pointer */
         MOST_ATMANFI_C_U16_SERVICE_ID,  /* ID of used Service */
         ALERTMAN_FI_MAJOR_VERSION,      /* MajorVersion of used Service */
         ALERTMAN_FI_MINOR_VERSION),     /* MinorVersion of used Service */
         m_poMainApp(poMainAppl)
{
   ETG_TRACE_USR1(("spi_tclAlertMngrClient() entered \n"));
   SPI_NORMAL_ASSERT(OSAL_NULL == m_poMainApp);
}

/***************************************************************************
 ** FUNCTION:  virtual spi_tclAlertMngrClient::~spi_tclAlertMngrClient()
 **************************************************************************/
spi_tclAlertMngrClient::~spi_tclAlertMngrClient()
{
   ETG_TRACE_USR1(("~spi_tclAlertMngrClient() entered \n"));
   m_mapNotiDetails.clear();
   m_poMainApp = OSAL_NULL;
}

/***************************************************************************
 ** FUNCTION:  spi_tclAlertMngrClient::vOnServiceAvailable();
 **************************************************************************/
tVoid spi_tclAlertMngrClient::vOnServiceAvailable() const
{
   ETG_TRACE_USR1(("spi_tclAlertMngrClient::vOnServiceAvailable entered \n"));
   //! Add code here
}

/***************************************************************************
 ** FUNCTION:  spi_tclAlertMngrClient::vOnServiceUnavailable();
 **************************************************************************/
tVoid spi_tclAlertMngrClient::vOnServiceUnavailable() const
{
   ETG_TRACE_USR1(("spi_tclAlertMngrClient::vOnServiceUnavailable entered \n"));
   //! Add code here
}

/***************************************************************************
* FUNCTION:  tVoid spi_tclAlertMngrClient::vRegisterForProperty()
***************************************************************************/
tVoid spi_tclAlertMngrClient::vRegisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclAlertMngrClient::vRegisterForProperties entered \n"));
   //! Add code here
}

/***************************************************************************
* FUNCTION:  tVoid spi_tclAlertMngrClient::vUnregisterForProperties()
***************************************************************************/
tVoid spi_tclAlertMngrClient::vUnregisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclAlertMngrClient::vUnregisterForProperties entered \n"));
   //! Add code here
}


/**************************************************************************
** FUNCTION:  tBool spi_tclAlertMngrClient::bTriggerNotificationAlert(const...
**************************************************************************/
tBool spi_tclAlertMngrClient::bTriggerNotificationAlert(const trNotiData& corfrNotiData)
{
   ETG_TRACE_USR1(("spi_tclAlertMngrClient::bTriggerNotificationAlert entered: Num of NotiActions = %d \n",
         corfrNotiData.tvecNotiActionList.size()));

   tBool bAlertSent = FALSE;
   tBool bStoreAlertInfo = FALSE;
   t_U8 u8NumOfActions = (cou32MAX_ALERT_ACTIONS < (corfrNotiData.tvecNotiActionList.size())) ?
         (cou32MAX_ALERT_ACTIONS) : (corfrNotiData.tvecNotiActionList.size());

   //! Create ActivateGrowthAlert MethodStart msg & initailize its data
   alrtmngr_tFiMSActGrowthAlert oActGrowthAlertMS;
   oActGrowthAlertMS.e8Priority.enType = most_fi_tcl_e8_AtManPriority::FI_EN_E8HIGH;
   oActGrowthAlertMS.u8AlertSound = 0;

   //! Define display types
   ADD_DISPLAY_TYPE(oActGrowthAlertMS.oDisplayTypeStream, CNTRSTACK_DISPLAY);

    //! Add alert title
   if (STRING_NOT_EMPTY(corfrNotiData.szNotiTitle))
   {
      oActGrowthAlertMS.sAlertTitle.bSet(corfrNotiData.szNotiTitle.c_str(), MOSTFI_CHAR_SET_UTF8);
   }//if (OSAL_NULL != pcoszAlertText)

   //! Add alert text
   if (STRING_NOT_EMPTY(corfrNotiData.szNotiBody))
   {
      oActGrowthAlertMS.sAlertText.bSet(corfrNotiData.szNotiBody.c_str(), MOSTFI_CHAR_SET_UTF8);
   }//if (OSAL_NULL != pcoszAlertText)

   if (0 == u8NumOfActions)
   {
      //Notification has no actions. Hence define alert as auto-dismiss acknowledge type & without any buttons.
      oActGrowthAlertMS.e8AckType.enType = ACK_TYPE_AUTO_MANUAL;
      oActGrowthAlertMS.u16AutoTimeout = coU8MAX_AUTO_TIMEOUT;
   }
   else
   {
      //Alert info needs to be stored only for Manual acknowledge type alerts, since
      //we would receive action from HMI only for these.
      bStoreAlertInfo = TRUE;

      //Notification has one or more actions. Hence define alert as Manual acknowledge type & with buttons
      //as defined in corfrNotiData actions
      oActGrowthAlertMS.e8AckType.enType = ACK_TYPE_MANUAL_ONLY;
      oActGrowthAlertMS.u16AutoTimeout = 0;

      //! Define alert buttons
      vPopulateGrowthAlertBtnInfo(u8NumOfActions, oActGrowthAlertMS, corfrNotiData.tvecNotiActionList);
   }

   ETG_TRACE_USR4((" bTriggerNotificationAlert: Alert AckType = %u, AutoTimeout = %u, Num of alert buttons = %u ",
         oActGrowthAlertMS.e8AckType.enType,
         oActGrowthAlertMS.u16AutoTimeout,
         u8NumOfActions));

   //! Send alert with Notification ID as CmdCounter
   bAlertSent = bPostMethodStart(oActGrowthAlertMS, corfrNotiData.u32NotiID);

   //! Store notification info in NotiDetails map.
   if ((TRUE == bAlertSent) && (TRUE == bStoreAlertInfo))
   {
      vStoreNotiDetails(corfrNotiData);
   }//if (TRUE == bStoreAlertInfo)

   return bAlertSent;
}//! end of bTriggerNotificationAlert()

/***************************************************************************
** FUNCTION: tVoid spi_tclAlertMngrClient::vSendNotificationAction()
***************************************************************************/
tVoid spi_tclAlertMngrClient::vGetNotificationData(tU32 u32AlertID,
      trNotiData& rfrNotiData)
{
   ETG_TRACE_USR1(("spi_tclAlertMngrClient::vGetNotificationData entered: u32AlertID = 0x%x \n",
         u32AlertID));

   tBool bFoundNotiData = FALSE;
   tU32 u32NotiID = 0;

   m_oLock.s16Lock();
   if ((SPI_MAP_NOT_EMPTY(m_mapNotiDetails)) && (cou32INVALID_ALERT_ID != u32AlertID))
   {
      for (tItrNotiDetailsMap itrNotiDetailsMap = m_mapNotiDetails.begin();
           itrNotiDetailsMap != m_mapNotiDetails.end();
           ++itrNotiDetailsMap)
      {
         if (u32AlertID == (itrNotiDetailsMap->second).u32AlertID)
         {
            rfrNotiData = (itrNotiDetailsMap->second).rNotiData;
            u32NotiID = itrNotiDetailsMap->first;
            bFoundNotiData = TRUE;
            //@Note: vDeleteNotiDetails() is not called here to delete the NotificationData entry
            //because vDeleteNotiDetails() function uses the same lock object is used to enclose this for loop.
            break;
         }//if (u32AlertID == (itrNotiDetailsMap->second).u32AlertID)
      }//for (tItrNotiDetailsMap itrNotiDetailsMap...)
   }//if (SPI_MAP_NOT_EMPTY(m_mapNotiDetails))
   m_oLock.vUnlock();

   if (TRUE == bFoundNotiData)
   {
      //! Remove Notification entry from NotiDetails map.
      vDeleteNotiDetails(u32NotiID);
   }
   else
   {
      ETG_TRACE_ERR(("spi_tclAlertMngrClient::vGetNotificationData: Notification details not found! \n"));
   }
}


/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclAlertMngrClient::vOnMRActivateGrowthAlert(...)
 **************************************************************************/
tVoid spi_tclAlertMngrClient::vOnMRActivateGrowthAlert(amt_tclServiceData* poMessage)
{
   alrtmngr_tXFiMRActGrowthAlert oActGrowthAlertMR(*poMessage, ALERTMAN_FI_MAJOR_VERSION);

   if (TRUE == oActGrowthAlertMR.bIsValid())
   {
      //! Extract notification ID from CmdCounter
      trUserContext rUsrCtxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCtxt);
      tU32 u32NotificationID = rUsrCtxt.u32CmdCtr;

      ETG_TRACE_USR2(("spi_tclAlertMngrClient::vOnMRActivateGrowthAlert: "
            "Received u32AlertID = 0x%x for u32NotificationID = 0x%x \n",
            oActGrowthAlertMR.u32AlertID, u32NotificationID));

      //! Store received AlertID in map.
      vUpdateNotiDetails(u32NotificationID, oActGrowthAlertMR.u32AlertID);
   }
   else
   {
      ETG_TRACE_ERR(("spi_tclAlertMngrClient::vOnMRActivateGrowthAlert: "
            "Invalid message received! \n"));
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclAlertMngrClient::vOnErrorActivateGrowthAlert(...)
 **************************************************************************/
tVoid spi_tclAlertMngrClient::vOnErrorActivateGrowthAlert(amt_tclServiceData* poMessage)
{
   alrtmngr_tXFiErrActGrowthAlert oActGrowthAlertErr(*poMessage, ALERTMAN_FI_MAJOR_VERSION);

   if (TRUE == oActGrowthAlertErr.bIsValid())
   {
      //! Extract notification ID from CmdCounter
      trUserContext rUsrCtxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCtxt);
      tU32 u32NotificationID = rUsrCtxt.u32CmdCtr;

      ETG_TRACE_USR2(("spi_tclAlertMngrClient::vOnErrorActivateGrowthAlert: "
            "Received error for u32NotificationID = 0x%x \n",
            u32NotificationID));

      //! Remove Notification info from map.
      vDeleteNotiDetails(u32NotificationID);
   }
   else
   {
      ETG_TRACE_ERR(("spi_tclAlertMngrClient::vOnErrorActivateGrowthAlert: "
            "Invalid msg received! \n"));
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclAlertMngrClient::vOnMRUpdateGrowthAlert(...)
 **************************************************************************/
tVoid spi_tclAlertMngrClient::vOnMRUpdateGrowthAlert(amt_tclServiceData* poMessage) const
{
   SPI_INTENTIONALLY_UNUSED(poMessage);
   ETG_TRACE_USR1(("spi_tclAlertMngrClient::vOnMRUpdateGrowthAlert entered \n"));
   //! Add code here
}

/***************************************************************************
 ** FUNCTION:  spi_tclAlertMngrClient::vOnErrorActivateGrowthAlert(...)
 **************************************************************************/
tVoid spi_tclAlertMngrClient::vOnErrorUpdateGrowthAlert(amt_tclServiceData* poMessage) const
{
   SPI_INTENTIONALLY_UNUSED(poMessage);
   ETG_TRACE_USR1(("spi_tclAlertMngrClient::vOnErrorUpdateGrowthAlert entered \n"));
   //! Add code here
}
/**************************************************************************
** FUNCTION:  tBool spi_tclAlertMngrClient::bPostMethodStart(const...
**************************************************************************/
tBool spi_tclAlertMngrClient::bPostMethodStart(const alrtmngr_FiMsgBase& rfcooAtManMS,
      tU32 u32CmdCounter)
{
   tBool bSuccess = FALSE;

   if (OSAL_NULL != m_poMainApp)
   {
      FIMsgDispatch oMsgDispatcher(m_poMainApp);

      //! Create Msg context
      trMsgContext rMsgCtxt;
      rMsgCtxt.rUserContext.u32SrcAppID = CCA_C_U16_APP_SMARTPHONEINTEGRATION;
      rMsgCtxt.rUserContext.u32DestAppID = u16GetServerAppID();
      rMsgCtxt.rUserContext.u32CmdCtr = u32CmdCounter;
      rMsgCtxt.rUserContext.u32RegID = u16GetRegID();

      //! Post Alert Manager MethodStart
      bSuccess = oMsgDispatcher.bSendMessage(rfcooAtManMS, rMsgCtxt, ALERTMAN_FI_MAJOR_VERSION);

      if (FALSE == bSuccess)
      {
         ETG_TRACE_ERR(("spi_tclAlertMngrClient::bPostMethodStart(): "
               "Sending of message with 'FID = 0x%x' failed. \n",
               rfcooAtManMS.u16GetFunctionID()));
      }//if (FALSE == bSuccess)
   }//if (OSAL_NULL != m_poMainApp)

   ETG_TRACE_USR4(("spi_tclAlertMngrClient::bPostMethodStart() left with success: %u \n",
         ETG_ENUM(BOOL, bSuccess)));
   return bSuccess;

}//! end of bPostMethodStart()

/***************************************************************************
** FUNCTION:  tVoid spi_tclAlertMngrClient::vStoreNotiDetails(...)
***************************************************************************/
tVoid spi_tclAlertMngrClient::vStoreNotiDetails(const trNotiData& corfrNotiData)
{
   trNotiDetails rNotiDetails;
   rNotiDetails.rNotiData = corfrNotiData;
   //@Note: Other data in trNotiDetails are initialized to default values in its ctor.

   m_oLock.s16Lock();
   tItrNotiDetailsMap itrNotiDetailsMap = m_mapNotiDetails.find(corfrNotiData.u32NotiID);
   if (m_mapNotiDetails.end() != itrNotiDetailsMap)
   {
      //Notification details already exist in map. Overwrite it.
      itrNotiDetailsMap->second = rNotiDetails;
      ETG_TRACE_USR2(("spi_tclAlertMngrClient::vStoreNotiDetails: "
            "Notification details (for NotificationID = 0x%x) already exists! "
            "Hence overwriting the details. \n",
            corfrNotiData.u32NotiID));
   }
   else
   {
      //Notification details does not exist in map. Add it.
      m_mapNotiDetails.insert(std::pair<tU32, trNotiDetails>(corfrNotiData.u32NotiID, rNotiDetails));
      ETG_TRACE_USR2(("spi_tclAlertMngrClient::vStoreNotiDetails: "
         "Notification details (for NotificationID = 0x%x) inserted",
         corfrNotiData.u32NotiID));
   }
   m_oLock.vUnlock();

}

/***************************************************************************
** FUNCTION:  tVoid spi_tclAlertMngrClient::vUpdateNotiDetails(...)
***************************************************************************/
tVoid spi_tclAlertMngrClient::vUpdateNotiDetails(tU32 u32NotificationID,
      tU32 u32AlertID)
{
   tBool bFoundNotiData = FALSE;

   //Update the AlertID in NotiDetails map.
   m_oLock.s16Lock();
   if (SPI_MAP_NOT_EMPTY(m_mapNotiDetails))
   {
      tItrNotiDetailsMap itrNotiDetailsMap = m_mapNotiDetails.find(u32NotificationID);
      if (m_mapNotiDetails.end() != itrNotiDetailsMap)
      {
         (itrNotiDetailsMap->second).u32AlertID = u32AlertID;
         bFoundNotiData = TRUE;
      }
   }//if (SPI_MAP_NOT_EMPTY(m_mapNotiDetails))
   m_oLock.vUnlock();

   if (FALSE == bFoundNotiData)
   {
      ETG_TRACE_ERR(("spi_tclAlertMngrClient::vUpdateNotiDetails: "
            "Notification details (for u32NotificationID = 0x%x) not found! "
            "Hence AlertID (0x%x) not stored. \n",
            u32NotificationID, u32AlertID));
   }
}

/***************************************************************************
** FUNCTION:  tVoid spi_tclAlertMngrClient::vDeleteNotiDetails(...)
***************************************************************************/
tVoid spi_tclAlertMngrClient::vDeleteNotiDetails(tU32 u32NotificationID)
{
   tBool bFoundNotiData = FALSE;

   m_oLock.s16Lock();
   if (SPI_MAP_NOT_EMPTY(m_mapNotiDetails))
   {
      tItrNotiDetailsMap itrNotiDetailsMap = m_mapNotiDetails.find(u32NotificationID);
      if (m_mapNotiDetails.end() != itrNotiDetailsMap)
      {
         m_mapNotiDetails.erase(itrNotiDetailsMap);
         bFoundNotiData = TRUE;
      }
   }//if (SPI_MAP_NOT_EMPTY(m_mapNotiDetails))
   m_oLock.vUnlock();

   if (FALSE == bFoundNotiData)
   {
      ETG_TRACE_ERR(("spi_tclAlertMngrClient::vDeleteNotiDetails: "
            "Notification details (for u32NotificationID = 0x%x) not found! "
            "Hence doing nothing. \n",
            u32NotificationID));
   }
}

/***************************************************************************
** FUNCTION: tVoid spi_tclAlertMngrClient::vPopulateGrowthAlertBtnInfo()
***************************************************************************/
tVoid spi_tclAlertMngrClient::vPopulateGrowthAlertBtnInfo(
      t_U8 NumAlertActions,
      alrtmngr_tFiMSActGrowthAlert& rfAlertMsg,
      const std::vector<trNotiAction>& corfrNotiActionList)
{
   //! Define alert buttons
   for (t_U8 u8ActnLstIndx = 0; u8ActnLstIndx < NumAlertActions; ++u8ActnLstIndx)
   {
      t_String szNotiActionName = corfrNotiActionList[u8ActnLstIndx].szNotiActionName;
      if (STRING_NOT_EMPTY(szNotiActionName))
      {
         switch (u8ActnLstIndx)
         {
            case 0:
            {
               rfAlertMsg.sButton1Text.bSet(szNotiActionName.c_str(), MOSTFI_CHAR_SET_UTF8);
               ADD_BTN_ACTION(rfAlertMsg.oButtonActionStream, DEV_PRJ_ACTION_1);
            }
               break;
            case 1:
            {
               rfAlertMsg.sButton2Text.bSet(szNotiActionName.c_str(), MOSTFI_CHAR_SET_UTF8);
               ADD_BTN_ACTION(rfAlertMsg.oButtonActionStream, DEV_PRJ_ACTION_2);
            }
               break;
            case 2:
            {
               rfAlertMsg.sButton3Text.bSet(szNotiActionName.c_str(), MOSTFI_CHAR_SET_UTF8);
               ADD_BTN_ACTION(rfAlertMsg.oButtonActionStream, DEV_PRJ_ACTION_3);
            }
               break;
            default:
               break;
         }//switch (u8ActnLstIndx)
      }//if (STRING_NOT_EMPTY(szNotiActionName))
   }//for (t_U8 u8ActnLstIndx = 0; u8ActnLstIndx < NumAlertActions; ++u8ActnLstIndx)
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
