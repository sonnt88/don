/*!
*******************************************************************************
* \file              spi_tclNavigationClient.cpp
* \brief             Navigation Data Client handler class
*******************************************************************************
\verbatim
 PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Position Data Client handler class 
COPYRIGHT:      &copy; RBEI

HISTORY:
 Date       |  Author                      | Modifications
 27.06.2014 |  Vinoop U                    | Initial Version
 27.06.2014 |  Ramya Murthy                | Renamed class and added loopback msg implementation.

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_LoopbackTypes.h"
#include "XFiObjHandler.h"
#include "FIMsgDispatch.h"
using namespace shl::msgHandler;
#include "spi_tclNavigationClient.h"

//Include FI interface of used service
#define MOST_FI_S_IMPORT_INTERFACE_MOST_NAVFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_NAVFI_FUNCTIONIDS
#define MOST_FI_S_IMPORT_INTERFACE_MOST_NAVFI_ERRORCODES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_NAVFI_SERVICEINFO
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_SERVICEINFO
#include "most_fi_if.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
#include "trcGenProj/Header/spi_tclNavigationClient.cpp.trc.h"
#endif


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

typedef XFiObjHandler<most_navfi_tclMsgGuidanceStatusStatus>
   spi_XFiGuidanceStatus;

//! Navigation giudance status pure set object
typedef most_navfi_tclMsgGuidanceStatusPureSet NavGuidanceStatusSet;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
/******************************************************************************/
/*                                                                            */
/* CCA MESSAGE MAP                                                            */
/*                                                                            */
/******************************************************************************/

BEGIN_MSG_MAP(spi_tclNavigationClient, ahl_tclBaseWork)
   // Add your ON_MESSAGE_SVCDATA() macros here to define which corresponding 
   // method should be called on receiving a specific message.
   ON_MESSAGE_SVCDATA(MOST_NAVFI_C_U16_GUIDANCESTATUS,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnGuidanceStatus)
END_MSG_MAP()


/******************************************************************************/
/*                                                                            */
/* METHODS                                                                    */
/*                                                                            */
/******************************************************************************/

/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/*******************************************************************************
* FUNCTION: spi_tclNavigationClient::
*             spi_tclNavigationClient(ahl_tclBaseOneThreadApp* poMainAppl)
*******************************************************************************/
spi_tclNavigationClient::spi_tclNavigationClient(ahl_tclBaseOneThreadApp* poMainApp)
   : ahl_tclBaseOneThreadClientHandler(
   poMainApp,                           /* Application Pointer          */
   CCA_C_U16_SRV_FB_NAVIGATION,              /* ID of used Service           */
   MOST_NAVFI_C_U16_SERVICE_MAJORVERSION,    /* MajorVersion of used Service */
   MOST_NAVFI_C_U16_SERVICE_MINORVERSION),   /* MinorVersion of used Service */
   m_poMainApp(poMainApp)
{
   ETG_TRACE_USR1(("spi_tclNavigationClient() entered "));
   SPI_NORMAL_ASSERT(OSAL_NULL == m_poMainApp);
}

/*******************************************************************************
* FUNCTION: spi_tclNavigationClient::~spi_tclNavigationClient(tVoid)
*******************************************************************************/
spi_tclNavigationClient::~spi_tclNavigationClient()
{
   ETG_TRACE_USR1(("~spi_tclNavigationClient() entered "));
   m_poMainApp = OSAL_NULL;
}

/*******************************************************************************
* FUNCTION: tVoid spi_tclNavigationClient::vOnServiceAvailable()
*******************************************************************************/
tVoid spi_tclNavigationClient::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("spi_tclNavigationClient::vOnServiceAvailable entered "));
}

/*******************************************************************************
* FUNCTION: tVoid spi_tclNavigationClient::vOnServiceUnavailable(
*******************************************************************************/
tVoid spi_tclNavigationClient::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("spi_tclNavigationClient::vOnServiceUnavailable entered "));
}

/***************************************************************************
 * FUNCTION:  tVoid spi_tclNavigationClient::vRegisterForProperties()
 ***************************************************************************/
tVoid spi_tclNavigationClient::vRegisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclNavigationClient::vRegisterForProperties entered "));

   //! Register subscribed properties
   vAddAutoRegisterForProperty(MOST_NAVFI_C_U16_GUIDANCESTATUS);
   ETG_TRACE_USR2(("[DESC]:vRegisterForProperties: Registered for FuncID = 0x%x ",
         MOST_NAVFI_C_U16_GUIDANCESTATUS));
}

/***************************************************************************
 * FUNCTION:  tVoid spi_tclNavigationClient::vUnregisterForProperties()
 ***************************************************************************/
tVoid spi_tclNavigationClient::vUnregisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclNavigationClient::vUnregisterForProperties entered "));

   //! Unregister subscribed properties
   vRemoveAutoRegisterForProperty(MOST_NAVFI_C_U16_GUIDANCESTATUS);
   ETG_TRACE_USR2(("[DESC]:vUnregisterForProperties: Unregistered for FuncID = 0x%x ",
         MOST_NAVFI_C_U16_GUIDANCESTATUS));
}

/*******************************************************************************
** FUNCTION:   spi_tclNavigationClient::vOnGuidanceStatus(amt_tclServiceData...
*******************************************************************************/
tVoid spi_tclNavigationClient::vOnGuidanceStatus(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclNavigationClient::vOnGuidanceStatus entered "));

   spi_XFiGuidanceStatus oXFiGuidanceStatus(*poMessage, MOST_NAVFI_C_U16_SERVICE_MAJORVERSION);

   if ((oXFiGuidanceStatus.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      tenGuidanceStatus enNavGuidanceStatus =
            static_cast<tenGuidanceStatus>(oXFiGuidanceStatus.e8GuidanceStatus.enType);

      ETG_TRACE_USR2(("[DESC]:vOnGuidanceStatus: Received Guidance Status = %d ",
            ETG_ENUM(SPI_GUIDANCE_STATUS, enNavGuidanceStatus)));

       //! Forward received message info as a Loopback message
      tLbNavGuidanceStatus oGuidanceStatusLbMsg
               (
                  m_poMainApp->u16GetAppId(), // Source AppID
                  m_poMainApp->u16GetAppId(), // Target AppID
                  0, // RegisterID
                  0, // CmdCounter
                  MOST_DEVPRJFI_C_U16_SERVICE_ID,    // ServiceID
                  SPI_C_U16_IFID_NAV_GUIDANCESTATUS, // Function ID
                  AMT_C_U8_CCAMSG_OPCODE_STATUS      // Opcode
               );

       //! Add data to message
      oGuidanceStatusLbMsg.vSetByte(static_cast<tU8>(enNavGuidanceStatus));

      //! Post loopback message
      if (oGuidanceStatusLbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oGuidanceStatusLbMsg, TRUE))
         {
            ETG_TRACE_ERR(("[ERR]:vOnGuidanceStatus: Loopback message posting failed "));
         }
      } //if (oDayNightModeLbMsg().bIsValid())
      else
      {
         ETG_TRACE_ERR(("[ERR]:vOnGuidanceStatus: Loopback message creation failed "));
      }
   } //if ((oXFiGuidanceStatus.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnGuidanceStatus: Invalid message/Null pointer "));
   }
}


/*******************************************************************************
** FUNCTION:   spi_tclNavigationClient::vSetNavGuidanceStatus(tenGuidanceStatus...
*******************************************************************************/
t_Void spi_tclNavigationClient::vSetNavGuidanceStatus(tenGuidanceStatus enGuidanceStatus)
{
   ETG_TRACE_USR1(("spi_tclNavigationClient::vSetNavGuidanceStatus entered with status: %d",
         ETG_ENUM(SPI_GUIDANCE_STATUS, enGuidanceStatus)));

   NavGuidanceStatusSet oNavGuidanceStatusSet;
   switch(enGuidanceStatus)
   {
   case e8GUIDANCE_ACTIVE:
      {
         oNavGuidanceStatusSet.e8GuidanceStatus.enType = most_fi_tcl_e8_NavGuidanceStatus::FI_EN_E8GA_GUIDANCE_ACTIVE;
      }
      break;
   case e8GUIDANCE_INACTIVE:
      {
         oNavGuidanceStatusSet.e8GuidanceStatus.enType = most_fi_tcl_e8_NavGuidanceStatus::FI_EN_E8GA_GUIDANCE_INACTIVE;
      }
      break;
   case e8GUIDANCE_SUSPENDED:
      {
         oNavGuidanceStatusSet.e8GuidanceStatus.enType = most_fi_tcl_e8_NavGuidanceStatus::FI_EN_E8GA_GUIDANCE_SUSPENDED;
         oNavGuidanceStatusSet.u32DestinationId = 0;
      }
      break;
   default:
      {
         ETG_TRACE_ERR(("[ERR]:vSetNavGuidanceStatus: Invalid Guidance status enum "));
      }
   }//switch(enGuidanceStatus)

   if (FALSE == bPostMethodStart(oNavGuidanceStatusSet))
   {
      ETG_TRACE_ERR(("[ERR]:vSetNavGuidanceStatus: Posting GuidanceStatus.Set message failed "));
   }//if (FALSE == bPostMethodStart(oNavGuidanceStatusSet))
}

/*******************************************************************************
** FUNCTION:   spi_tclNavigationClient::bPostMethodStart(most_navfi_tclMsgBaseMessage...
*******************************************************************************/
tBool spi_tclNavigationClient::bPostMethodStart(const most_navfi_tclMsgBaseMessage& rfcoNavMasgBase)
{
   tBool bSuccess = FALSE;

   if (OSAL_NULL != m_poMainApp)
   {
      //! Create Msg context
      trMsgContext rMsgCtxt;
      rMsgCtxt.rUserContext.u32SrcAppID  = CCA_C_U16_APP_SMARTPHONEINTEGRATION;
      rMsgCtxt.rUserContext.u32DestAppID = u16GetServerAppID();
      rMsgCtxt.rUserContext.u32RegID     = u16GetRegID();
      rMsgCtxt.rUserContext.u32CmdCtr    = 0;

      //!Post BT settings MethodStart
      FIMsgDispatch oMsgDispatcher(m_poMainApp);
      bSuccess = oMsgDispatcher.bSendMessage(rfcoNavMasgBase, rMsgCtxt, MOST_NAVFI_C_U16_SERVICE_MAJORVERSION);
   }

   ETG_TRACE_USR2(("spi_tclNavigationClient::bPostMethodStart() left with: Message Post Success = %u (FID = 0x%x) ",
         ETG_ENUM(BOOL, bSuccess), rfcoNavMasgBase.u16GetFunctionID()));
   return bSuccess;
}

////////////////////////////////////////////////////////////////////////////////
// <EOF>
