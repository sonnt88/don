/*!
*******************************************************************************
* \file         spi_tclOnstarDataClient.cpp
* \brief        OnstatDataSettings Client handler class
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Onstar DataSettings Client handler class
COPYRIGHT:      &copy; RBEI

HISTORY:
 Date       |  Author                      | Modifications
 27.05.2014 |  Vinoop U                    | Initial Version
 27.06.2014 |  Ramya Murthy                | Renamed class and added loopback msg implementation.
 17.03.2015 |  Shihabudheen P M            | Added vOnDataSettingsEventStatus

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_LoopbackTypes.h"
#include "XFiObjHandler.h"
using namespace shl::msgHandler;
#include "spi_tclOnstarDataClient.h"

//Include FI interface of used service
#define MOST_FI_S_IMPORT_INTERFACE_MOST_ONSDATFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_ONSDATFI_FUNCTIONIDS
#define MOST_FI_S_IMPORT_INTERFACE_MOST_ONSDATFI_ERRORCODES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_ONSDATFI_SERVICEINFO
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_SERVICEINFO
#include "most_fi_if.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
#include "trcGenProj/Header/spi_tclOnstarDataClient.cpp.trc.h"
#endif


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
typedef XFiObjHandler<most_onsdatfi_tclMsgOnStarTBTActiveStatus>  spi_XFiTBTActiveStatus;
typedef XFiObjHandler<most_onsdatfi_tclMsgOnStarDataSettingsEventStatus>  spi_XFiDataSettingsStatus;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
#define ONSDAT_FI_MAJORVERSION MOST_ONSDATFI_C_U16_SERVICE_MAJORVERSION 
#define ONSDAT_FI_MINORVERSION MOST_ONSDATFI_C_U16_SERVICE_MINORVERSION

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
/******************************************************************************/
/*                                                                            */
/* CCA MESSAGE MAP                                                            */
/*                                                                            */
/******************************************************************************/

BEGIN_MSG_MAP(spi_tclOnstarDataClient, ahl_tclBaseWork)
   // Add your ON_MESSAGE_SVCDATA() macros here to define which corresponding 
   // method should be called on receiving a specific message.
   ON_MESSAGE_SVCDATA(MOST_ONSDATFI_C_U16_ONSTARTBTACTIVE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusTBTActive)
   ON_MESSAGE_SVCDATA(MOST_ONSDATFI_C_U16_ONSTARDATASETTINGSEVENT,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnDataSettingsEventStatus)
   
END_MSG_MAP()


/******************************************************************************/
/*                                                                            */
/* METHODS                                                                    */
/*                                                                            */
/******************************************************************************/

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/*******************************************************************************
* FUNCTION: spi_tclOnstarDataClient::spi_tclOnstarDataClient(ahl_tclBaseOneThreadApp* poMainAppl)
*******************************************************************************/
spi_tclOnstarDataClient::spi_tclOnstarDataClient(ahl_tclBaseOneThreadApp* poMainApp)
   : ahl_tclBaseOneThreadClientHandler(
   poMainApp,                           /* Application Pointer          */
   MOST_ONSDATFI_C_U16_SERVICE_ID,      /* ID of used Service           */
   ONSDAT_FI_MAJORVERSION,              /* MajorVersion of used Service */
   ONSDAT_FI_MINORVERSION),             /* MinorVersion of used Service */
   m_poMainApp(poMainApp)
{
   ETG_TRACE_USR1(("spi_tclOnstarDataClient() entered "));
   SPI_NORMAL_ASSERT(OSAL_NULL == m_poMainApp);
}

/*******************************************************************************
* FUNCTION: spi_tclOnstarDataClient::~spi_tclOnstarDataClient()
*******************************************************************************/
spi_tclOnstarDataClient::~spi_tclOnstarDataClient()
{
   ETG_TRACE_USR1(("~spi_tclOnstarDataClient() entered "));
   m_poMainApp = OSAL_NULL;
}

/*******************************************************************************
* FUNCTION: tVoid spi_tclOnstarDataClient::vOnServiceAvailable()
*******************************************************************************/
tVoid spi_tclOnstarDataClient::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("spi_tclOnstarDataClient::vOnServiceAvailable entered "));
   vAddAutoRegisterForProperty(MOST_ONSDATFI_C_U16_ONSTARDATASETTINGSEVENT);
}

/*******************************************************************************
* FUNCTION: tVoid spi_tclOnstarDataClient::vOnServiceUnavailable(
*******************************************************************************/
tVoid spi_tclOnstarDataClient::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("spi_tclOnstarDataClient::vOnServiceUnavailable entered "));
   vRemoveAutoRegisterForProperty(MOST_ONSDATFI_C_U16_ONSTARDATASETTINGSEVENT);
}

/***************************************************************************
* FUNCTION:  tVoid spi_tclOnstarDataClient::vRegisterForProperties()
***************************************************************************/
tVoid spi_tclOnstarDataClient::vRegisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclOnstarDataClient::vRegisterForProperties entered "));

   //! Register for interested properties
   vAddAutoRegisterForProperty(MOST_ONSDATFI_C_U16_ONSTARTBTACTIVE);
   ETG_TRACE_USR2(("[DESC]:vRegisterForProperties: Registered for FuncID = 0x%x ",
         MOST_ONSDATFI_C_U16_ONSTARTBTACTIVE));
}

/***************************************************************************
* FUNCTION:  tVoid spi_tclOnstarDataClient::vUnregisterForProperties()
***************************************************************************/
tVoid spi_tclOnstarDataClient::vUnregisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclOnstarDataClient::vRegisterForProperties entered "));

   //! Unregister subscribed properties
   vRemoveAutoRegisterForProperty(MOST_ONSDATFI_C_U16_ONSTARTBTACTIVE);
   ETG_TRACE_USR2(("[DESC]:vUnregisterForProperties: Unregistered for FuncID = 0x%x ",
         MOST_ONSDATFI_C_U16_ONSTARTBTACTIVE));
}

/*******************************************************************************
** FUNCTION:   spi_tclOnstarDataClient::vOnStatusTBTActive(amt_tclServiceData...
*******************************************************************************/
tVoid spi_tclOnstarDataClient::vOnStatusTBTActive(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclOnstarDataClient::vOnStatusTBTActive entered "));

   spi_XFiTBTActiveStatus oTBTActiveStatus(*poMessage, ONSDAT_FI_MAJORVERSION);

   if ((true == oTBTActiveStatus.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      ETG_TRACE_USR2(("[DESC]:vOnStatusTBTActive: TBT is active = %d ",
            ETG_ENUM(BOOL, oTBTActiveStatus.bOnStarTBTActiveFlag)));

      //! Forward received message info as a Loopback message
      tLbOnstarTBTActiveState oTBTActiveLbMsg(
            m_poMainApp->u16GetAppId(), // Source AppID
            m_poMainApp->u16GetAppId(), // Target AppID
            0, // RegisterID
            0, // CmdCounter
            MOST_DEVPRJFI_C_U16_SERVICE_ID,       // ServiceID
            SPI_C_U16_IFID_ONSTAR_TBTACTIVESTATE, // Function ID
            AMT_C_U8_CCAMSG_OPCODE_STATUS         // Opcode
            );

      //! Add data to message
      oTBTActiveLbMsg.vSetByte(static_cast<tU8>(oTBTActiveStatus.bOnStarTBTActiveFlag));

      if (oTBTActiveLbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oTBTActiveLbMsg, TRUE))
         {
            ETG_TRACE_ERR(("[ERR]:vOnStatusTBTActive: Loopback message posting failed "));
         }
      } //if (oTBTActiveLbMsg().bIsValid())
      else
      {
         ETG_TRACE_ERR(("[ERR]:vOnStatusTBTActive: Loopback message creation failed "));
      }
   }//if ((true == oTBTActiveStatus.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnStatusTBTActive: Invalid message/Null pointer "));
   }
}

/*******************************************************************************
** FUNCTION:   spi_tclOnstarDataClient::vOnDataSettingsEventStatus(amt_tclServiceData...
*******************************************************************************/
tVoid spi_tclOnstarDataClient::vOnDataSettingsEventStatus(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclOnstarDataClient::vOnDataSettingsEventStatus entered "));

   spi_XFiDataSettingsStatus oDataSettingsStatus(*poMessage, ONSDAT_FI_MAJORVERSION);

   if ((true == oDataSettingsStatus.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      tenOnstarSettingsEvents enOnstarSettingsEvent =
         static_cast<tenOnstarSettingsEvents>(oDataSettingsStatus.e8OnStarDataSettingsEventEnum.enType);

      ETG_TRACE_USR2(("[DESC]:vOnDataSettingsEventStatus: Received event type = %d ",
            ETG_ENUM(ONSTAR_DATASET_EVENT, enOnstarSettingsEvent)));

      tLbOnStarDataSettingsUpdate oDataSettingsLbMsg(
            m_poMainApp->u16GetAppId(), // Source AppID
            m_poMainApp->u16GetAppId(), // Target AppID
            0, // RegisterID
            0, // CmdCounter
            MOST_DEVPRJFI_C_U16_SERVICE_ID,       // ServiceID
            SPI_C_U16_IFID_ONSTAR_SETTINGS_UPDATE, // Function ID
            AMT_C_U8_CCAMSG_OPCODE_STATUS         // Opcode
            );

      //! Add data to message
      oDataSettingsLbMsg.vSetByte(static_cast<tU8>(enOnstarSettingsEvent));
      
      if (oDataSettingsLbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oDataSettingsLbMsg, TRUE))
         {
            ETG_TRACE_ERR(("[ERR]:vOnDataSettingsEventStatus: Loopback message posting failed "));
         }
      } //if (oTBTActiveLbMsg().bIsValid())
      else
      {
         ETG_TRACE_ERR(("[ERR]:vOnDataSettingsEventStatus: Loopback message creation failed "));
      }
   }//if ((true == oTBTActiveStatus.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnDataSettingsEventStatus: Invalid message/Null pointer "));
   }
}

////////////////////////////////////////////////////////////////////////////////
// <EOF>
