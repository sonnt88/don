/*!
 *******************************************************************************
 * \file              spi_tclGMLANGatewayClient.cpp
 * \brief             GMLAN Gateway Client handler class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    GMLAN Gateway Client handler class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                       | Modifications
 10.10.2014 |  Hari Priya E R               | Initial Version
 06.05.2015  |Tejaswini HB                  |Lint Fix
 28.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors
 30.06.2015 |  Ramya Murthy                 | Changes to read Gear & ParkBrake info from VehicleMovementState,
                                              ElectricParkBrake & ParkBrakeSwitchState properties.
30.10.2015  | Shiva Kumar G                 | Implemented ReportEnvironment Data feature
 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include <ctime>
#include "NmeaEncoder.h"
#include "spi_LoopbackTypes.h"
#include "XFiObjHandler.h"
#include "FIMsgDispatch.h"
using namespace shl::msgHandler;
#include "spi_tclGMLANGatewayClient.h"

#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
#include "trcGenProj/Header/spi_tclGMLANGatewayClient.cpp.trc.h"
#endif
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	
/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
#define GMLNGW_FI_MAJOR_VERSION  MOST_GMLNGWFI_C_U16_SERVICE_MAJORVERSION
#define GMLNGW_FI_MINOR_VERSION  MOST_GMLNGWFI_C_U16_SERVICE_MINORVERSION

static const tFloat scf16SpeedConversionValue = 27.78;
//static const tU16 scu16SatellitesUsed = 4;                //To remove Lint Warning
//static const tU16 scu16Millisecondconversion = 1000;      //To remove Lint Warning
static const t_Double scdLatLongResolution = (3600.0 * 1000.0);
static const t_Float scfCentimeterToMeterConversion = 0.01;

static const trLocationDataCallbacks rEmptyLocCallbacks;
static const trSensorDataCallbacks rEmptySensorCallbacks;
static const trVehicleDataCallbacks rEmptyVehicleCallbacks;
static const trEnvironmentDataCbs scorEmptyEnvDataCbs;

//! Data ranges specified by GMLANGAteway FI
static const t_U32 cou32_FI_MAX_SPEED = 255;
static const t_U32 cou32_FI_MAX_HDOP = 1023;
static const t_U32 cou32_FI_MAX_ELEVATION = 2097151;
static const t_U32 cou32_FI_MAX_HEADING = 4095;

static const t_S32 cou32_FI_MIN_LATITUDE = -536870912;
static const t_S32 cou32_FI_MAX_LATITUDE = 536870911;
static const t_S32 cou32_FI_MIN_LONGITUDE = -1073741824;
static const t_S32 cou32_FI_MAX_LONGITUDE = 1073741823;

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
typedef XFiObjHandler<most_gmlngwfi_tclMsgGMLANGPSConfigurationStatus>  gmlngw_tXFiStatusGPSConfig;
typedef XFiObjHandler<most_gmlngwfi_tclMsgGPSGeographicalPositionStatus> gmlngw_tXFiStatusGeoPosition;
typedef XFiObjHandler<most_gmlngwfi_tclMsgGPSElevationAndHeadingStatus> gmlngw_tXFiStatusElevationAndHeading;
typedef XFiObjHandler<most_gmlngwfi_tclMsgVehicleSpeedStatus> gmlngw_tXFiStatusVehicleSpeed;
typedef XFiObjHandler<most_gmlngwfi_tclMsgGPSDateAndTimeStatus> gmlngw_tXFiStatusDateAndTime;
typedef XFiObjHandler<most_gmlngwfi_tclMsgParkBrakeSwitchStateStatus> gmlngw_tXFiStatusParkBrakeSwitch;
typedef XFiObjHandler<most_gmlngwfi_tclMsgVehicleMovementStateStatus> gmlngw_tXFiStatusVehMovementState;
typedef XFiObjHandler<most_gmlngwfi_tclMsgElectricParkBrakeStatusStatus> gmlngw_tXFiStatusElecParkBrake;

typedef most_fi_tcl_e8_GMLnGWElectricParkBrakeStatus gmlngw_tFiElecParkBrkStatus;
typedef XFiObjHandler<most_gmlngwfi_tclMsgVINDigits10To17Status> gmlngw_tXFiStatusVINDigits10To17;
typedef XFiObjHandler<most_gmlngwfi_tclMsgOutsideAirTemperatureStatus> gmlngw_tXFiStatusOutsideAirTemperature;


/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
** CCA MESSAGE MAP
******************************************************************************/
BEGIN_MSG_MAP(spi_tclGMLANGatewayClient, ahl_tclBaseWork)

   /* -------------------------------Properties.---------------------------------*/
   ON_MESSAGE_SVCDATA(MOST_GMLNGWFI_C_U16_GMLANGPSCONFIGURATION,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusGPSConfig)
   ON_MESSAGE_SVCDATA(MOST_GMLNGWFI_C_U16_GPSGEOGRAPHICALPOSITION,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusGPSGeoPosition)
   ON_MESSAGE_SVCDATA(MOST_GMLNGWFI_C_U16_GPSELEVATIONANDHEADING,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusGPSElevationAndHeading)
   ON_MESSAGE_SVCDATA(MOST_GMLNGWFI_C_U16_VEHICLESPEED,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusVehicleSpeed)
   ON_MESSAGE_SVCDATA(MOST_GMLNGWFI_C_U16_GPSDATEANDTIME,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusGPSDateAndTime)
   ON_MESSAGE_SVCDATA(MOST_GMLNGWFI_C_U16_PARKBRAKESWITCHSTATE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusParkBrakeSwitch)
   ON_MESSAGE_SVCDATA(MOST_GMLNGWFI_C_U16_VEHICLEMOVEMENTSTATE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusVehicleMovementState)
   ON_MESSAGE_SVCDATA(MOST_GMLNGWFI_C_U16_ELECTRICPARKBRAKESTATUS,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusElectricParkBrake)
   ON_MESSAGE_SVCDATA(MOST_GMLNGWFI_C_U16_OUTSIDEAIRTEMPERATURE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusOutsideAirTemperature)
   ON_MESSAGE_SVCDATA(MOST_GMLNGWFI_C_U16_VINDIGITS10TO17,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusVINDigits10To17)


   /* -------------------------------Properties.---------------------------------*/

END_MSG_MAP()



/***************************************************************************
 ** FUNCTION:  spi_tclGMLANGatewayClient::spi_tclGMLANGatewayClient(...
 **************************************************************************/
spi_tclGMLANGatewayClient::spi_tclGMLANGatewayClient(ahl_tclBaseOneThreadApp* poMainAppl)
      : ahl_tclBaseOneThreadClientHandler(
         poMainAppl,                     /* Application Pointer */
         MOST_GMLNGWFI_C_U16_SERVICE_ID,  /* ID of used Service */
         GMLNGW_FI_MAJOR_VERSION,      /* MajorVersion of used Service */
         GMLNGW_FI_MINOR_VERSION),      /* MinorVersion of used Service */     
         m_poMainApp(poMainAppl),
         m_bElectricParkBrakeActive(false),
         m_bParkBrakeSwitchActive(false)
{
   ETG_TRACE_USR1(("spi_tclGMLANGatewayClient() entered "));
   SPI_NORMAL_ASSERT(OSAL_NULL == m_poMainApp);

   /*Set the parameters of the GPS Data and Sensor Data to required values,
    wherever data is not available from the client handler*/
   m_rGPSData.dLatLongResolution = scdLatLongResolution;
   //m_rSensorData.u16SatellitesUsed = scu16SatellitesUsed; //@Note: Info not sent sicne it is not madatory.
   m_rGPSData.enHeadingDir = e8CLOCKWISE;
   m_rGPSData.enSensorType = e8SENSOR_TYPE_COMBINED_LEFT_RIGHT_WHEEL;
}

/***************************************************************************
 ** FUNCTION:  virtual spi_tclGMLANGatewayClient::~spi_tclGMLANGatewayClient()
 **************************************************************************/
spi_tclGMLANGatewayClient::~spi_tclGMLANGatewayClient()
{
   ETG_TRACE_USR1(("~spi_tclGMLANGatewayClient() entered "));
   m_poMainApp = OSAL_NULL;
}

/***************************************************************************
 ** FUNCTION:  spi_tclGMLANGatewayClient::vOnServiceAvailable();
 **************************************************************************/
tVoid spi_tclGMLANGatewayClient::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("spi_tclGMLANGatewayClient::vOnServiceAvailable entered "));
   vAddAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_GMLANGPSCONFIGURATION);
   vAddAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_PARKBRAKESWITCHSTATE);
   vAddAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_ELECTRICPARKBRAKESTATUS);
   vAddAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_VEHICLEMOVEMENTSTATE);
   vAddAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_VINDIGITS10TO17);
}

/***************************************************************************
 ** FUNCTION:  spi_tclGMLANGatewayClient::vOnServiceUnavailable();
 **************************************************************************/
tVoid spi_tclGMLANGatewayClient::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("spi_tclGMLANGatewayClient::vOnServiceUnavailable entered "));
   vRemoveAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_GMLANGPSCONFIGURATION);
   vRemoveAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_PARKBRAKESWITCHSTATE);
   vRemoveAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_ELECTRICPARKBRAKESTATUS);
   vRemoveAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_VEHICLEMOVEMENTSTATE);
   vRemoveAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_VINDIGITS10TO17);
}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclGMLANGatewayClient::vRegisterForLocDataProperties(
                                   trLocationDataCallbacks rDataSvcLocDataCb,
                                   trSensorDataCallbacks rDataSvcSensorDataCb)
   **************************************************************************/
tVoid spi_tclGMLANGatewayClient::vRegisterForLocDataProperties(trLocationDataCallbacks rDataSvcLocDataCb,
                                                               trSensorDataCallbacks rDataSvcSensorDataCb)
{
   ETG_TRACE_USR1(("spi_tclGMLANGatewayClient::vRegisterForLocDataProperties entered "));
   //! Register for the location data related properties
   vAddAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_GPSGEOGRAPHICALPOSITION);
   vAddAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_GPSELEVATIONANDHEADING);
   vAddAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_GPSDATEANDTIME);

   m_rDataSvcLocDataCb = rDataSvcLocDataCb;
   m_rDataSvcSensorDataCb = rDataSvcSensorDataCb;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclGMLANGatewayClient::vRegisterForVehicleDataProperties(
               trVehicleDataCallbacks rVehicleDataCb)
**************************************************************************/
tVoid spi_tclGMLANGatewayClient::vRegisterForVehicleDataProperties(trVehicleDataCallbacks rVehicleDataCb)
{
   ETG_TRACE_USR1(("spi_tclGMLANGatewayClient::vRegisterForVehicleDataProperties entered "));

   //send update for vehicle mode
   if (OSAL_NULL != m_rVehicleDataCb.fvOnVehicleData)
   {
      m_rVehicleDataCb.fvOnVehicleData(m_rVehicleData, false);
   }
  
   //! Register for the vehicle data related properties
   vAddAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_VEHICLESPEED);
   m_rVehicleDataCb = rVehicleDataCb;
}

/***************************************************************************
   ** FUNCTION:  t_Void spi_tclGMLANGatewayClient::vUnregisterForLocDataProperties()
**************************************************************************/
tVoid spi_tclGMLANGatewayClient::vUnregisterForLocDataProperties()
{
   ETG_TRACE_USR1(("spi_tclGMLANGatewayClient::vUnregisterForLocDataProperties entered "));

   //! Unregister from the registered location data properties
   vRemoveAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_GPSGEOGRAPHICALPOSITION);
   vRemoveAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_GPSELEVATIONANDHEADING);
   vRemoveAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_GPSDATEANDTIME);

   m_rDataSvcLocDataCb = rEmptyLocCallbacks;
   m_rDataSvcSensorDataCb = rEmptySensorCallbacks;
}

/***************************************************************************
   ** FUNCTION:  t_Void spi_tclGMLANGatewayClient::vUnregisterForVehicleDataProperties()
**************************************************************************/
tVoid spi_tclGMLANGatewayClient::vUnregisterForVehicleDataProperties()
{
   ETG_TRACE_USR1(("spi_tclGMLANGatewayClient::vUnregisterForVehicleDataProperties entered "));

   //! Unregister from the registered vehicle data properties
   vRemoveAutoRegisterForProperty( MOST_GMLNGWFI_C_U16_VEHICLESPEED);
   m_rVehicleDataCb = rEmptyVehicleCallbacks;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclGMLANGatewayClient::vRegForEnvDataUpdates()
**************************************************************************/
t_Void spi_tclGMLANGatewayClient::vRegForEnvDataUpdates(trEnvironmentDataCbs rEnvDataCbs)
{
   ETG_TRACE_USR1(("spi_tclGMLANGatewayClient::vRegForEnvDataUpdates() "));
   vAddAutoRegisterForProperty(MOST_GMLNGWFI_C_U16_OUTSIDEAIRTEMPERATURE);
   m_rEnvironmentDataCbs = rEnvDataCbs ;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclGMLANGatewayClient::vUnregForEnvDataUpdates()
**************************************************************************/
t_Void spi_tclGMLANGatewayClient::vUnregForEnvDataUpdates()
{
   ETG_TRACE_USR1(("spi_tclGMLANGatewayClient::vUnregForEnvDataUpdates() "));
   m_rEnvironmentDataCbs = scorEmptyEnvDataCbs;
   vRemoveAutoRegisterForProperty(MOST_GMLNGWFI_C_U16_OUTSIDEAIRTEMPERATURE);
}

/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/**************************************************************************
** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusGPSConfig()
**************************************************************************/
tVoid spi_tclGMLANGatewayClient::
vOnStatusGPSConfig(amt_tclServiceData* poMessage)
{
   //! Extract the Status msg details
   gmlngw_tXFiStatusGPSConfig oXFiStGPSConfig(*poMessage, GMLNGW_FI_MAJOR_VERSION);

   if ((true == oXFiStGPSConfig.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      tenGPSConfig enGPSConfig = static_cast<tenGPSConfig>(oXFiStGPSConfig.e8GMLANGPSConfiguration.enType);
      ETG_TRACE_USR2(("[DESC]:spi_tclGMLANGatewayClient::vOnStatusGPSConfig: GPS Config: %d",
         ETG_ENUM(GMLNGW_GPSCONFIG, enGPSConfig)));

      //! Forward received message info as a Loopback message
      tLbGMLANGPSConfig oGPSConfigStatusLbMsg
         (
         m_poMainApp->u16GetAppId(),   // Source AppID
         m_poMainApp->u16GetAppId(),   // Target AppID
         0,                             // RegisterID
         0,               // CmdCounter
         CCA_C_U16_SRV_FB_DEVICEPROJECTION,  // ServiceID
         SPI_C_U16_IFID_GMLAN_GPSCONFIG, // Function ID
         AMT_C_U8_CCAMSG_OPCODE_STATUS  // Opcode
         );

      //! Add data to message
      oGPSConfigStatusLbMsg.vSetByte(static_cast<tU8>(enGPSConfig));

      //! Post loopback message
      if (oGPSConfigStatusLbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oGPSConfigStatusLbMsg, TRUE))
         {
            ETG_TRACE_ERR(("[ERR]: vOnStatusGPSConfig: Loopback message posting for GPS Config failed!"));
         }
      } //if (oGPSConfigStatusLbMsg.bIsValid())
   }//if (true == oXFiStGPSConfig.bIsValid())
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclGMLANGatewayClient::vOnStatusGPSConfig: Invalid message received! "));
   }
}//! end of vOnStatusGPSConfig()

/**************************************************************************
** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusGPSGeoPosition()
**************************************************************************/
tVoid spi_tclGMLANGatewayClient::
vOnStatusGPSGeoPosition(amt_tclServiceData* poMessage)
{
   //! Extract the Status msg details
   gmlngw_tXFiStatusGeoPosition oXFiStGeoPosition(*poMessage, GMLNGW_FI_MAJOR_VERSION);

   if (true == oXFiStGeoPosition.bIsValid())
   {
      /*The latitude and longitude values as received from the Fi are in ms arc.
       Convert this into decimal degrees and use it.
       1 arc millisecond = (0.000278/1000) degrees.*/
      m_rGPSData.s32Latitude = static_cast<tS32>(oXFiStGeoPosition.s32PositioningSystemLatitude);
      m_rGPSData.s32Longitude = static_cast<tS32>(oXFiStGeoPosition.s32PositioningSystemLongitude);

      //! Validate latitude availability
      m_rGPSData.bLatitudeAvailable =
            ((cou32_FI_MIN_LATITUDE <= m_rGPSData.s32Latitude) && (cou32_FI_MAX_LATITUDE >= m_rGPSData.s32Latitude)) ?
            (oXFiStGeoPosition.bLatitudeValidity) : (false);
      if (oXFiStGeoPosition.bLatitudeValidity != m_rGPSData.bLatitudeAvailable)
      {
         ETG_TRACE_ERR(("[ERR]:spi_tclGMLANGatewayClient::vOnStatusGPSGeoPosition: Latitude data out of range! "));
      }//if (oXFiStGeoPosition.bLatitudeValidity != m_rGPSData.bLatitudeAvailable)

      //! Validate longitude availability
      m_rGPSData.bLongitudeAvailable =
            ((cou32_FI_MIN_LONGITUDE <= m_rGPSData.s32Longitude) && (cou32_FI_MAX_LONGITUDE >= m_rGPSData.s32Longitude)) ?
            (oXFiStGeoPosition.bLongitudeValidity) : (false);
      if (oXFiStGeoPosition.bLatitudeValidity != m_rGPSData.bLatitudeAvailable)
      {
         ETG_TRACE_ERR(("[ERR]:spi_tclGMLANGatewayClient::vOnStatusGPSGeoPosition: Longitude data out of range! "));
      }//if (oXFiStGeoPosition.bLatitudeValidity != m_rGPSData.bLatitudeAvailable)

      //! Validate GPS fix
      if ((m_rGPSData.bLatitudeAvailable) && (m_rGPSData.bLongitudeAvailable))
      {
         m_rSensorData.enGnssMode = e8GNSSMODE_3DFIX;
         m_rSensorData.enGnssQuality = e8GNSSQUALITY_AUTONOMOUS;
      }
      else
      {
         m_rSensorData.enGnssMode = e8GNSSMODE_UNKNOWN;
         m_rSensorData.enGnssQuality = e8GNSSQUALITY_UNKNOWN;
      }

      ETG_TRACE_USR4(("[PARAM]:spi_tclGMLANGatewayClient::vOnStatusGPSGeoPosition: "
            "Latitude: %d (Valid: %u), Longitude : %d (Valid: %u)",
            m_rGPSData.s32Latitude, ETG_ENUM(BOOL, m_rGPSData.bLatitudeAvailable),
            m_rGPSData.s32Longitude, ETG_ENUM(BOOL, m_rGPSData.bLongitudeAvailable)));
   } //if (true == oXFiStGeoPosition.bIsValid())
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclGMLANGatewayClient::vOnStatusGPSGeoPosition: Invalid message received! "));
   }
}//! end of vOnStatusGPSGeoPosition()

/**************************************************************************
** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusGPSElevationAndHeading()
**************************************************************************/
tVoid spi_tclGMLANGatewayClient::
vOnStatusGPSElevationAndHeading(amt_tclServiceData* poMessage)
{
   //! Extract the Status msg details
   gmlngw_tXFiStatusElevationAndHeading oXFiStElevationAndHeading(*poMessage, GMLNGW_FI_MAJOR_VERSION);
   
   if (true == oXFiStElevationAndHeading.bIsValid())
   {
      ETG_TRACE_USR4(("[PARAM]:spi_tclGMLANGatewayClient::vOnStatusGPSElevationAndHeading: "
            "Received Speed: %u (Valid: %u), HDOP: %u (Valid: %u), "
            "Elevation: %u (Valid: %u), Heading: %u (Valid: %u) ",
            oXFiStElevationAndHeading.u8PositioningSystemCalculatedSpeed,
            ETG_ENUM(BOOL, oXFiStElevationAndHeading.bCalculatedSpeedValidity),
            oXFiStElevationAndHeading.u16PositioningSystemDilutionOfPrecision,
            ETG_ENUM(BOOL, oXFiStElevationAndHeading.bDilutionOfPrecisionValidity),
            oXFiStElevationAndHeading.u32PositioningSystemElevation,
            ETG_ENUM(BOOL, oXFiStElevationAndHeading.bElevationValidity),
            oXFiStElevationAndHeading.u16PositioningSystemHeading,
            ETG_ENUM(BOOL, oXFiStElevationAndHeading.bHeadingValidity)));

      tFloat fSpeed = (oXFiStElevationAndHeading.u8PositioningSystemCalculatedSpeed) * scf16SpeedConversionValue;
      m_rGPSData.s16Speed = static_cast<t_S16>(fSpeed);
      m_rGPSData.bSpeedAvailable = (cou32_FI_MAX_SPEED >= oXFiStElevationAndHeading.u8PositioningSystemCalculatedSpeed) ?
            (oXFiStElevationAndHeading.bCalculatedSpeedValidity) : (false);

      /*According to FCAT,we need to do the following calculation to get the actual value:E = N * 0.1,
      where N is the value received by the property update*/
      tDouble dHDOP = (oXFiStElevationAndHeading.u16PositioningSystemDilutionOfPrecision)*0.1;
      m_rSensorData.dHDOP = static_cast<tDouble>(dHDOP);
      m_rSensorData.bHDOPAvailable = (cou32_FI_MAX_HDOP >= oXFiStElevationAndHeading.u16PositioningSystemDilutionOfPrecision) ?
            (oXFiStElevationAndHeading.bDilutionOfPrecisionValidity) : (false);

      /*According to FCAT,we need to do the following calculation to get the actual value:E = N * 1-100000,
      where N is the value received by the property update, in units = cm.*/
      t_U32 u32Elevation = oXFiStElevationAndHeading.u32PositioningSystemElevation;
      tDouble dElevation = (u32Elevation - 100000.0) * (scfCentimeterToMeterConversion); //Unit: meters
      m_rGPSData.s32Altitude = static_cast<t_S32>(dElevation);
      m_rGPSData.bAltitudeAvailable = (cou32_FI_MAX_ELEVATION >= oXFiStElevationAndHeading.u32PositioningSystemElevation) ?
            (oXFiStElevationAndHeading.bElevationValidity) : (false);

      /*According to FCAT,we need to do the following calculation to get the actual value:E = N * 0.1,
      where N is the value received by the property update*/
      tDouble dHeading = (oXFiStElevationAndHeading.u16PositioningSystemHeading)*(0.1);
      m_rGPSData.dHeading = static_cast<t_Double>(dHeading);
      m_rGPSData.bHeadingAvailable = (cou32_FI_MAX_HEADING >= oXFiStElevationAndHeading.u16PositioningSystemHeading) ?
            (oXFiStElevationAndHeading.bHeadingValidity) : (false);

      ETG_TRACE_USR4(("[PARAM]:spi_tclGMLANGatewayClient::vOnStatusGPSElevationAndHeading: "
            "Calculated Speed(cmps): %d (Valid: %u), HDOP: %f (Valid: %u), "
            "Elevation: %d (Valid: %u), Heading: %f (Valid: %u) ",
            m_rGPSData.s16Speed, ETG_ENUM(BOOL, m_rGPSData.bSpeedAvailable),
            m_rSensorData.dHDOP, ETG_ENUM(BOOL, m_rSensorData.bHDOPAvailable),
            m_rGPSData.s32Altitude, ETG_ENUM(BOOL, m_rGPSData.bAltitudeAvailable),
            m_rGPSData.dHeading, ETG_ENUM(BOOL, m_rGPSData.bHeadingAvailable)));

   } //if (true == oXFiStElevationAndHeading.bIsValid())
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclGMLANGatewayClient::vOnStatusGPSElevationAndHeading: Invalid message received! "));
   }
}//! end of vOnStatusGPSElevationAndHeading()

/**************************************************************************
** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusVehicleSpeed()
**************************************************************************/
tVoid spi_tclGMLANGatewayClient::
vOnStatusVehicleSpeed(amt_tclServiceData* poMessage)
{
   //! Extract the Status msg details
   gmlngw_tXFiStatusVehicleSpeed oXFiStVehicleSpeed(*poMessage, GMLNGW_FI_MAJOR_VERSION);

   if (true == oXFiStVehicleSpeed.bIsValid())
   {
      /*According to FCAT,we need to do the following calculation to get the actual value:E = N*0.015625,
       where N is the value received by the property update
       Vehicle Speed received from the Fi is in km/hr.
       Converting this into cm/sec and using it.*/
      m_rVehicleData.s16Speed = static_cast<tU16>(
            (oXFiStVehicleSpeed.u16VehicleSpeed) * (0.015625) * (scf16SpeedConversionValue));
      m_rVehicleData.bSpeedAvailable = oXFiStVehicleSpeed.bValidityFlag;

      ETG_TRACE_USR4(("[PARAM]:spi_tclGMLANGatewayClient::vOnStatusVehicleSpeed: VehicleSpeed - "
            "Received: %u (Valid %u), Calculated(cmps): %d ",
            oXFiStVehicleSpeed.u16VehicleSpeed, ETG_ENUM(BOOL, m_rVehicleData.bSpeedAvailable),
            m_rVehicleData.s16Speed));

      if (OSAL_NULL != m_rVehicleDataCb.fvOnVehicleData)
      {
         m_rVehicleDataCb.fvOnVehicleData(m_rVehicleData, false);
      }

   } //if (oXFiStGPSConfig.bIsValid())
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclGMLANGatewayClient::vOnStatusVehicleSpeed: Invalid message received! "));
   }

}//! end of vOnStatusVehicleSpeed()

/**************************************************************************
** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusVehicleMovementState()
**************************************************************************/
tVoid spi_tclGMLANGatewayClient::
vOnStatusVehicleMovementState(amt_tclServiceData* poMessage)
{
   //! Extract the Status msg details
   gmlngw_tXFiStatusVehMovementState oXFiStVehMovState(*poMessage, GMLNGW_FI_MAJOR_VERSION);

   if ((true == oXFiStVehMovState.bIsValid()) && (OSAL_NULL != m_poMainApp))
   {
      m_rVehicleData.enVehMovState = static_cast<tenVehicleMovementState>(
            oXFiStVehMovState.e8VehicleMovementState.enType);

      ETG_TRACE_USR4(("[PARAM]:spi_tclGMLANGatewayClient::vOnStatusVehicleMovementState: Received state: %d ",
            ETG_ENUM(VEH_MOV_STATE, m_rVehicleData.enVehMovState)));

      if ((OSAL_NULL != m_rVehicleDataCb.fvOnVehicleData) && (e8VEHICLE_MOVEMENT_STATE_INVALID != m_rVehicleData.enVehMovState))
      {
         m_rVehicleDataCb.fvOnVehicleData(m_rVehicleData, false);
      }
   } //if (true == oXFiStVehMovState.bIsValid())
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclGMLANGatewayClient::vOnStatusVehicleMovementState: Invalid message received! "));
   }
}//! end of vOnStatusVehicleMovementState()

/**************************************************************************
** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusElectricParkBrake()
**************************************************************************/
tVoid spi_tclGMLANGatewayClient::
vOnStatusElectricParkBrake(amt_tclServiceData* poMessage)
{
   //! Extract the Status msg details
   gmlngw_tXFiStatusElecParkBrake oXFiStElecParkBrake(*poMessage, GMLNGW_FI_MAJOR_VERSION);

   if (true == oXFiStElecParkBrake.bIsValid())
   {
      m_bElectricParkBrakeActive =
            (gmlngw_tFiElecParkBrkStatus::FI_EN_E8APPLIED == oXFiStElecParkBrake.e8ElectricParkBrakeStatus.enType);

      m_rVehicleData.bParkBrakeActive = (m_bElectricParkBrakeActive || m_bParkBrakeSwitchActive);

      ETG_TRACE_USR4(("[PARAM]:spi_tclGMLANGatewayClient::vOnStatusElectricParkBrake: Received state : %d, Calculated state : %d ",
            ETG_ENUM(PARK_BRAKE, oXFiStElecParkBrake.e8ElectricParkBrakeStatus.enType),
            ETG_ENUM(BOOL, m_rVehicleData.bParkBrakeActive)));

      if (OSAL_NULL != m_rVehicleDataCb.fvOnVehicleData)
      {
         m_rVehicleDataCb.fvOnVehicleData(m_rVehicleData, false);
      }
   } //if (true == oXFiStElecParkBrake.bIsValid())
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclGMLANGatewayClient::vOnStatusElectricParkBrake: Invalid message received! "));
   }
}//! end of vOnStatusElectricParkBrake()

/**************************************************************************
** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusParkBrakeSwitch()
**************************************************************************/
tVoid spi_tclGMLANGatewayClient::
vOnStatusParkBrakeSwitch(amt_tclServiceData* poMessage)
{
   //! Extract the Status msg details
	gmlngw_tXFiStatusParkBrakeSwitch oXFiStParkBrakeSwitch(*poMessage, GMLNGW_FI_MAJOR_VERSION);

   if (true == oXFiStParkBrakeSwitch.bIsValid())
   {
      m_bParkBrakeSwitchActive = oXFiStParkBrakeSwitch.bParkBrakeSwitchActive;

      m_rVehicleData.bParkBrakeActive = (m_bElectricParkBrakeActive || m_bParkBrakeSwitchActive);

	   ETG_TRACE_USR4(("[PARAM]:spi_tclGMLANGatewayClient::vOnStatusParkBrakeSwitch: Received state : %d, Calculated state : %d ",
	         ETG_ENUM(BOOL, oXFiStParkBrakeSwitch.bParkBrakeSwitchActive),
	         ETG_ENUM(BOOL, m_rVehicleData.bParkBrakeActive)));

      if (OSAL_NULL != m_rVehicleDataCb.fvOnVehicleData)
      {
         m_rVehicleDataCb.fvOnVehicleData(m_rVehicleData, false);
      }
   } //if (true == oXFiStParkBrakeSwitch.bIsValid())
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclGMLANGatewayClient::vOnStatusParkBrakeSwitch: Invalid message received! "));
   }
}//! end of vOnStatusParkBrakeSwitch()

/**************************************************************************
** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusGPSDateAndTime()
**************************************************************************/
tVoid spi_tclGMLANGatewayClient::
vOnStatusGPSDateAndTime(amt_tclServiceData* poMessage)
{
   //! Extract the Status msg details
	/*lint -esym(40,fvOnGpsData)fvOnGpsData Undeclared identifier */
	/*lint -esym(40,fvOnSensorData)fvOnSensorData Undeclared identifier */
	/*lint -esym(40,fvOnVehicleData)fvOnVehicleData Undeclared identifier */
   gmlngw_tXFiStatusDateAndTime oXFiStDateAndTime(*poMessage, GMLNGW_FI_MAJOR_VERSION);
   //time_t timer;  //Commented to remove Lint Warning
   struct tm rTime;

   if ((true == oXFiStDateAndTime.bIsValid())
       &&
      (OSAL_NULL!= m_rDataSvcLocDataCb.fvOnGpsData )
      &&
      (OSAL_NULL != m_rDataSvcSensorDataCb.fvOnSensorData))
   {
      if(
         (true ==  oXFiStDateAndTime.bHoursValidity)&&
         (true == oXFiStDateAndTime.bMinutesValidity)&&
         (true == oXFiStDateAndTime.bSecondsValidity)
         )
      {
         rTime.tm_mday = oXFiStDateAndTime.u8CalendarDay;

         tU8 u8Month = static_cast<tU8>(oXFiStDateAndTime.e8CalendarMonth.enType);
         rTime.tm_mon =(1<= u8Month)?(u8Month-1):u8Month;

         /*According to FCAT,we need to do the following calculation to get the actual value:E=N*1 + 2000,
          where N is the value received by the property update*/
         /* In struct tm,the year value should be years since 1900.
         Hence if the year is greater than 1900,subtract 1900 from it to get the required value.*/
         rTime.tm_year = (oXFiStDateAndTime.u8CalendarYear)+100;

         rTime.tm_hour =((oXFiStDateAndTime.u8Hours )<= 23)? (oXFiStDateAndTime.u8Hours):0;
         rTime.tm_min = ((oXFiStDateAndTime.u8Minutes) <=59)?(oXFiStDateAndTime.u8Minutes):0;
         rTime.tm_sec = ((oXFiStDateAndTime.u8Seconds) <= 61)? (oXFiStDateAndTime.u8Seconds):0;
         
         //Convert the Date and time to Posix format
         m_rGPSData.PosixTime = static_cast<t_PosixTime>(mktime(&rTime));
         m_rSensorData.PosixTime = static_cast<t_PosixTime>(mktime(&rTime));
         m_rGPSData.u16ExactTime = 0;

         ETG_TRACE_USR4(("[PARAM]:spi_tclGMLANGatewayClient::vOnStatusGPSDateAndTime: "
               "Received Time-H:%d M:%d S:%d, Received Date-d:%d m:%d y:%d, Calculated Posixtime: %d",
               oXFiStDateAndTime.u8Hours,oXFiStDateAndTime.u8Minutes,oXFiStDateAndTime.u8Seconds,
               oXFiStDateAndTime.u8CalendarDay,oXFiStDateAndTime.e8CalendarMonth.enType,
               oXFiStDateAndTime.u8CalendarYear, m_rGPSData.PosixTime));

         m_rDataSvcLocDataCb.fvOnGpsData(m_rGPSData);
         m_rDataSvcSensorDataCb.fvOnSensorData(m_rSensorData);
      }
   } //if (true == oXFiStDateAndTime.bIsValid())
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclGMLANGatewayClient::vOnStatusGPSDateAndTime: Invalid message received! "));
   }
}//! end of vOnStatusGPSDateAndTime()

/***************************************************************************
** FUNCTION:  tVoid spi_tclGMLANGatewayClient::tVoid vRequestVehicleDataUpdate()()
**************************************************************************/
tVoid spi_tclGMLANGatewayClient::vRequestVehicleDataUpdate()
{
   //! Send vehicle data 
   if (OSAL_NULL != m_rVehicleDataCb.fvOnVehicleData)
   {
      m_rVehicleDataCb.fvOnVehicleData(m_rVehicleData, true);
   }
}


/**************************************************************************
** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusOutsideAirTemperature(amt_tcl...
**************************************************************************/
tVoid spi_tclGMLANGatewayClient::vOnStatusOutsideAirTemperature(amt_tclServiceData* poMessage)
{
   /*lint -esym(40,fvOutsideTempUpdate)fvOutsideTempUpdate Undeclared identifier */

   gmlngw_tXFiStatusOutsideAirTemperature oXFiStOutsideAirTemp(*poMessage, GMLNGW_FI_MAJOR_VERSION);

   if (true == oXFiStOutsideAirTemp.bIsValid())
   {
      tDouble dTemp = (oXFiStOutsideAirTemp.u8OutsideAirTemperature)*(0.5) - 40 ;

      ETG_TRACE_USR4(("[PARAM]:spi_tclGMLANGatewayClient::vOnStatusOutsideAirTemperature: Valid: %d, Temp in Celcius : %f ",
         ETG_ENUM(BOOL, oXFiStOutsideAirTemp.bValidityFlag),dTemp));

      if (OSAL_NULL != m_rEnvironmentDataCbs.fvOutsideTempUpdate)
      {
         m_rEnvironmentDataCbs.fvOutsideTempUpdate(oXFiStOutsideAirTemp.bValidityFlag, dTemp);
      }//if (OSAL_NULL != m_rEnvironmentDataCbs)
   } //if (true == oXFiStOutsideAirTemp.bIsValid())
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclGMLANGatewayClient::vOnStatusOutsideAirTemperature: Invalid message received! "));
   }

}

/**************************************************************************
** FUNCTION:  tVoid spi_tclGMLANGatewayClient::vOnStatusVINDigits10To17(amt_tcl...
**************************************************************************/
tVoid spi_tclGMLANGatewayClient::vOnStatusVINDigits10To17(amt_tclServiceData* poMessage)
{
   gmlngw_tXFiStatusVINDigits10To17 oXFiStVINDigits10To17(*poMessage, GMLNGW_FI_MAJOR_VERSION);
   tU32 u32ListSize = oXFiStVINDigits10To17.oVINDigitStream.u8Items.size();
   if ((true == oXFiStVINDigits10To17.bIsValid())&& (OSAL_NULL != m_poMainApp) && (u32ListSize>0))
   {
      for (t_U8 u8Index = 0; u8Index < u32ListSize; u8Index++)
         {
            ETG_TRACE_USR4(("[PARAM]:spi_tclGMLANGatewayClient::vOnStatusVINDigits10To17:Values coming from GMLAN =%c",oXFiStVINDigits10To17.oVINDigitStream.u8Items.at(u8Index)));
         }
      t_U8 u8ModelYear = oXFiStVINDigits10To17.oVINDigitStream.u8Items.at(0);
      ETG_TRACE_USR4(("[PARAM]:spi_tclGMLANGatewayClient::vOnStatusVINDigits10To17:Value of Model Year =%c",u8ModelYear));

      //! Forward received message info as a Loopback message
     tLbVINDigits10To17 oVINDigits10To17StatusLbMsg
      (
         m_poMainApp->u16GetAppId(), // Source AppID
         m_poMainApp->u16GetAppId(), // Target AppID
         0, // RegisterID
         0, // CmdCounter
         CCA_C_U16_SRV_FB_DEVICEPROJECTION, // ServiceID
         SPI_C_U16_IFID_VINDIGITS10TO17, // Function ID
         AMT_C_U8_CCAMSG_OPCODE_STATUS // Opcode
       );
      //! Add data to message
       oVINDigits10To17StatusLbMsg.vSetByte(u8ModelYear);
       //! Post loopback message
      if (oVINDigits10To17StatusLbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oVINDigits10To17StatusLbMsg, TRUE))
         {
            ETG_TRACE_ERR(("[ERR]: vOnStatusVINDigits10To17: Loopback message posting for VIN Digits10to17 failed!"));
         }
      }
   } //if (oVINDigits10To17StatusLbMsg.bIsValid())
     //if (true ==oXFiStVINDigits10To17.bIsValid())
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclGMLANGatewayClient::vOnStatusVINDigits10To17: Invalid message received! "));
   }
}
//lint –restore

///////////////////////////////////////////////////////////////////////////////
// <EOF>
