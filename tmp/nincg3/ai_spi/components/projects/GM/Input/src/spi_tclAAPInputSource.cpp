/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdInputSource.cpp
 * \brief             Android Auto GM Enpoint wrapper for Input
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Input wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 24.04.2015 |  Sameer Chandra		   	   | Initial Version
 11.07.2015 | Ramya Murthy                 | Fix for same session ID being used for multiple Endpoints
 16.07.2015 |  Sameer Chandra              | Added method vReportKnobkey to support Knob
                                             Encoder.


 \endverbatim
 ******************************************************************************/
#ifndef GEN3X86
// Below code should only compile in case of Gen3arm Make
#include "spi_tclAAPInputSource.h"
#include <set>
#include <errno.h>
#include "SPITypes.h"
//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
      #include "trcGenProj/Header/spi_tclAAPInputSource.cpp.trc.h"
   #endif
#endif


//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	
/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::spi_tclAAPCmdInput()
***************************************************************************/
spi_tclAAPInputSource::spi_tclAAPInputSource():m_poInputSource(NULL)
{

   ETG_TRACE_USR1((" spi_tclAAPInputSource::spi_tclAAPInputSource entered "));

}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::~spi_tclAAPCmdInput()
***************************************************************************/
spi_tclAAPInputSource::~spi_tclAAPInputSource()
{

    ETG_TRACE_USR1((" spi_tclAAPInputSource::~spi_tclAAPInputSource entered "));
    m_poInputSource = NULL;
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::bInitialize()
***************************************************************************/
t_Bool spi_tclAAPInputSource::bInitialize(const trAAPInputConfig& rAAPInputConfig, const set<t_S32>& sKeyCodes)
{
   t_Bool bRetVal = false;

   //! Get Galreciever using Session Data Interface
   spi_tclAAPSessionDataIntf oSessionDataIntf;

   shared_ptr < GalReceiver > shpoGalReceiver
         = oSessionDataIntf.poGetGalReceiver();

   if ( shpoGalReceiver != nullptr )
   {
      m_poInputSource = new (std::nothrow) InputSource(e32SESSIONID_AAPTOUCH,
         shpoGalReceiver->messageRouter());
   }

   if ( NULL != m_poInputSource )
   {
      //! Register Touch Screen with InputSource Endpoint.
      ETG_TRACE_USR4(("spi_tclAAPInputSource::Register TouchScreen- Width:%d, Height:%d ",
                  rAAPInputConfig.u32DisplayHeight, rAAPInputConfig.u32DisplayWidth));

      m_poInputSource->registerTouchScreen(rAAPInputConfig.u32DisplayHeight,
            rAAPInputConfig.u32DisplayWidth, static_cast<TouchScreenType>(rAAPInputConfig.enAAPTouchScreenType));
      //!Register Key Codes that would be used.
      ETG_TRACE_USR2(("spi_tclAAPInputSource::Register key codes "));

      m_poInputSource->registerKeycodes(sKeyCodes);

      //! Register the Input Source Endpoint with the GalReceiver.
      ETG_TRACE_USR2(
            ("spi_tclAAPInputSource::Register Input Source End-point with GAL Receiver"));

      if ( shpoGalReceiver != nullptr )
      {
         /*lint -esym(40,nullptr)nullptr Undeclared identifier */
         bRetVal = shpoGalReceiver->registerService(m_poInputSource);
         ETG_TRACE_USR2(
               ("spi_tclAAPInputSource::Registration with GalReceiver : %d\n", ETG_ENUM(
                     BOOL, bRetVal)));
      }

   }
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  spi_tclAAPInputSource::bUnInitialize()
***************************************************************************/
t_Void spi_tclAAPInputSource::bUnInitialize()
{
   ETG_TRACE_USR1(("spi_tclAAPCmdInput::bUnInitialize() entered "));

   if ( NULL != m_poInputSource )
   {
      //! Delete the end-point since it is created for every connection/disconnection.
      delete m_poInputSource;
   }

}

/***************************************************************************
** FUNCTION:  spi_tclAAPInputSource::vReportTouch()
***************************************************************************/
t_Void spi_tclAAPInputSource::vReportTouch(t_U64 u64timestamp, t_S32 s32numPointers,
                                           const t_U32* u32pointerIds, t_U32* u32XCoord,
                                           t_U32* u32YCoord, t_U32 u32Action, t_U32 u32ActionIndex)
{
   if ( NULL != m_poInputSource )
   {
      t_S32 s32ReportTouchStatus = m_poInputSource->reportTouch(u64timestamp,
            s32numPointers, u32pointerIds, u32XCoord, u32YCoord, u32Action,
            u32ActionIndex);

      ETG_TRACE_USR4(("Reporting Touch event Status : %d \n", s32ReportTouchStatus));
   }
}
/***************************************************************************
** FUNCTION:  spi_tclAAPInputSource::vReportTouch()
***************************************************************************/
t_Void spi_tclAAPInputSource::vReportkey(t_U64 u64TimeStamp, t_U32 u32KeyCode,
      t_Bool bIsDown, t_U32 u32MetaState)
{
   if ( NULL != m_poInputSource )
   {
      t_S32 s32ReportKeyStatus = m_poInputSource->reportKey(u64TimeStamp,
            u32KeyCode, bIsDown, u32MetaState);

      ETG_TRACE_USR4(("Reporting Key event Status : %d \n", s32ReportKeyStatus));
   }
}

/***************************************************************************
** FUNCTION:  spi_tclAAPInputSource::vReportKnobkey()
***************************************************************************/
t_Void spi_tclAAPInputSource:: vReportKnobkey(t_U64 u64TimeStamp, t_U32 u32KeyCode, t_S32 s32DeltaCnts)
{
   if (NULL != m_poInputSource)
   {
      t_S32 s32ReportKonbKeyStatus = m_poInputSource->reportRelative(u64TimeStamp, u32KeyCode,s32DeltaCnts);

      ETG_TRACE_USR4(("Reporting Knob Key event Status [%d] \n", s32ReportKonbKeyStatus ));
   }
}
#endif /*GEN3X86*/
//lint –restore
