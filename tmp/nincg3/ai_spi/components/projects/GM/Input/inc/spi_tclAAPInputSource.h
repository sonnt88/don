/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdInputSource.h
 * \brief             Input wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Input wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 24.04.2015 |  Sameer Chandra		   	   | Initial Version
 16.07.2015 |  Sameer Chandra              | Added method vReportKnobkey to support Knob
                                             Encoder.

 \endverbatim
 ******************************************************************************/
 #ifndef GEN3X86
// Below code should only compile in case of Gen3arm Make
#ifndef SPI_TCLAAPINPUTSOURCE_H_
#define SPI_TCLAAPINPUTSOURCE_H_

#include "SPITypes.h"
#include "AAPTypes.h"
#include "RespBase.h"
#include "spi_tclAAPSessionDataIntf.h"
#include "InputSource.h"

class spi_tclAAPInputSource
{
   public:

  /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

  /***************************************************************************
   ** FUNCTION:  spi_tclAAPInputSource::spi_tclAAPInputSource();
   ***************************************************************************/
  /*!
   * \fn     spi_tclAAPCmdInput()
   * \brief  Default Constructor
   * \sa     spi_tclAAPCmdInput()
   **************************************************************************/
	spi_tclAAPInputSource();

  /***************************************************************************
   ** FUNCTION:  virtual spi_tclAAPInputSource::~spi_tclAAPInputSource()
   ***************************************************************************/
  /*!
   * \fn      virtual ~spi_tclAAPCmdInput()
   * \brief   Virtual Destructor
   * \sa      spi_tclAAPCmdInput()
   **************************************************************************/
  virtual  ~spi_tclAAPInputSource();

  /***************************************************************************
   ** FUNCTION:  virtual spi_tclAAPInputSource::bInitialize()
   **************************************************************************/
  /*!
   * \fn      bInitialize()
   * \brief   Initializes the InputSource Endpoint, registers keycodes and touch
   * 	      screen.
   * \sa      bInitialize()
   **************************************************************************/
  t_Bool bInitialize(const trAAPInputConfig& rAAPInputConfig, const set<t_S32>& sKeyCodes);

  /***************************************************************************
   ** FUNCTION:  virtual spi_tclAAPInputSource::bUnInitialize()
   ***************************************************************************/
  /*!
   * \fn      bUnInitialize()
   * \brief   Uninitializes the InputSource Endpoint.
   * \sa      bUnInitialize()
   **************************************************************************/
   t_Void bUnInitialize();

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclAAPInputSource::vReportTouch
   ***************************************************************************/
  /*!
   * \fn      vReportTouch(t_U64 u64timestamp, t_S32 s32numPointers,
   *                       const t_U32* u32pointerIds, const t_U32* u32XCoord,
   *                       const t_U32* u32YCoord, t_U32 u32Action,
   *                       t_U32 u32ActionIndex)
   * \brief   Receives the Touch events and forwards it to AAP Input Source Endpoint
   * 		  wrapper.
   * \param   u64timestamp  : [IN] unique identifier to AAP device
   * \param   s32numPointers     : [IN] reference to touch data structure which contains
   *          touch details received /ref trTouchData
   * \param   u32pointerIds
   * \param   u32XCoord : [IN] X Coordinate value for touch point
   * \param   u32YCoord : [IN] Y Coordinate value for touch point
   * \param   u32Action : [IN] Action state weather it is press, release etc.
   * \param   u32ActionIndex : [IN] Index of the touch point for which the change is reported.
   * \retval  NONE
   **************************************************************************/
   t_Void vReportTouch(t_U64 u64timestamp, t_S32 s32numPointers,
         const t_U32* u32pointerIds, t_U32* u32XCoord,
         t_U32* u32YCoord, t_U32 u32Action, t_U32 u32ActionIndex);

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPInputHandler::vReportkey
   ***************************************************************************/
  /*!
   * \fn      vReportkey(t_U64 u64TimeStamp, t_U32 u32KeyCode, t_Bool bIsDown, t_U32 u32MetaState))
   * \brief   Receives hard key events and forwards it to AAP InputSource Endpoint
   * 		  wrapper.
   * \param   u64TimeStamp : [IN] unique identifier to AAP device
   * \param   u32KeyCode       : [IN] indicates keypress or keyrelease
   * \param   bIsDown       : [IN] unique key code identifier
   * \param   u32MetaState  : [IN] Used for special keys
   * \retval  NONE
   **************************************************************************/
   t_Void   vReportkey(t_U64 u64TimeStamp, t_U32 u32KeyCode, t_Bool bIsDown, t_U32 u32MetaState);

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPInputHandler::vReportKnobkey
   ***************************************************************************/
  /*!
   * \fn      vReportKnobkey(t_U64 u64TimeStamp, t_U32 u32KeyCode,
   *                         t_S32 s32DeltaCnts))
   * \brief   Receives Knob Encoder Change and forwards it to AAP InputSource Endpoint
   *       wrapper.
   * \param   u64TimeStamp : [IN] unique identifier to AAP device
   * \param   u32KeyCode   : [IN] unique code for knob rotary controller
   * \param   s32DeltaCnts   : [IN] Encoder Delta Change.
   * \retval  NONE
   **************************************************************************/
   t_Void   vReportKnobkey(t_U64 u64TimeStamp, t_U32 u32KeyCode, t_S32 s32DeltaCnts);

   protected :

	/***************************************************************************
	*********************************PROTECTED**********************************
	***************************************************************************/

	/***************************************************************************
	** FUNCTION:  spi_tclAAPInputSource(const spi_tclAAPInputSource...
	***************************************************************************/
	/*!
	* \fn      spi_tclAAPInputSource(const spi_tclAAPInputSource& corfoSrc)
	* \brief   Copy constructor - Do not allow the creation of copy constructor
	* \param   corfoSrc : [IN] reference to source data interface object
	* \retval
	* \sa      spi_tclAAPInputSource()
	***************************************************************************/
	spi_tclAAPInputSource (const spi_tclAAPInputSource& corfoSrc);


	/***************************************************************************
	** FUNCTION:  spi_tclAAPInputSource& operator=( const spi_tclAAPInput...
	***************************************************************************/
	/*!
	* \fn      spi_tclAAPInputSource& operator=(const spi_tclAAPInputSource& corfoSrc))
	* \brief   Assignment operator
	* \param   corfoSrc : [IN] reference to source data interface object
	* \retval
	* \sa      spi_tclAAPInputSource(const spi_tclAAPInputSource& otrSrc)
	***************************************************************************/
	spi_tclAAPInputSource& operator=(const spi_tclAAPInputSource& corfoSrc);

  private:

	/***************************************************************************
	** FUNCTION:  t_Void spi_tclAAPInputSource::vSetInputConfig
	***************************************************************************/
	/*!
	* \fn      enGetGestureMode()
	* \brief   Retrieves AAP gesture code for corresponding SPI gesture code.
	* \param   NONE
	* \retval  NONE
	**************************************************************************/
	t_Void vSetInputConfig(const trAAPInputConfig& rAAPInputConfig )
	{
		SPI_INTENTIONALLY_UNUSED(rAAPInputConfig);
	}
  //! InputSource Endpoint
  InputSource* m_poInputSource;

};
#endif /* SPI_TCLAAPINPUTSOURCE_H_ */
#endif