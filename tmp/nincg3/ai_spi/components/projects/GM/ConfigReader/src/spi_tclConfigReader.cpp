/***********************************************************************/
/*!
* \file  spi_tclConfigReader.cpp
* \brief Class to get the Variant specific Info for GM
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to get the Variant specific Info for GM
AUTHOR:         Hari Priya E R
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
19.05.2013  | Hari Priya E R        | Initial Version
31.07.2013  | Ramya Murthy          | Implementation for generic config usage and
                                      SPI feature configuration
05.08.2013  | Ramya Murthy          | Added DriveSide info
06.08.2013  | Ramya Murthy          | Added MLNotification setting
14.08.2013  | Ramya Murthy          | Reading DriveSide info from EOL
10.10.2014  | Vinoop U		         | Fetching OEM Icon details For CarPlay
06.11.2014  |  Hari Priya E R       | Added changes to get the GM Vehicle brand
17.11.2014  | Ramya Murthy          | Added System variant info
28.11.2014  | Ramya Murthy          | Updated SPI Feature support info
08.05.2015  | Shiva Kumar G         | Extended for ML Client Profile configs
30.06.2015  | Tejaswini HB          | SetSpiFeatureEnabled only when feature is supported

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "Trace.h"
#include "EolHandler.h"
#include "DiPOTypes.h"
#include "AAPTypes.h"
#include "Datapool.h"
#include "spi_tclConfigReader.h"
#include "RandomIdGenerator.h"

#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
#include "trcGenProj/Header/spi_tclConfigReader.cpp.trc.h"
#endif
#endif

#define REG_S_IMPORT_INTERFACE_GENERIC
#include "reg_if.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
#define DIPO_SCREEN_CONFIG
static trVideoConfigData aVideoConfig[]=
#include "spi_tclScreenSettings.cfg"
#undef DIPO_SCREEN_CONFIG

#define DIPO_VEHICLEBRAND_CONFIG
static trVehicleBrandInfo aVehicleConfig[]=
#include "spi_tclScreenSettings.cfg"
#undef DIPO_VEHICLEBRAND_CONFIG

#define GM_MANUFACTURER_MODEL_CONFIG
static trVehicleInfo aManufacturerModelConfig[]=
#include "spi_tclScreenSettings.cfg"
#undef GM_MANUFACTURER_MODEL_CONFIG
#define MANUF_NAME_STRING "GM"
#define OEM_ICON_NAME "CarMode"
#define OEM_ICON_PATH ""
#define STR_LENGTH 250
#define IS_CADILLAC 6


/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/
/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/
//! Drive Side EOL values
//static const t_U8 cou8EOL_DRIVESIDE_LEFT = 0x00;
static const t_U8 cou8EOL_DRIVESIDE_RIGHT = 0x01;

//Default Client Friendly Name
static const t_String sczClientFriendlyName = "" ;
//Client Manufacturer Name
static const t_String sczClientManfacturerName = "GM";
static const t_String sczRegistryPath = "/dev/registry/LOCAL_MACHINE/SOFTWARE/BLAUPUNKT/VERSIONS";
static const t_U32 scou32MAX_SIZE = 255;
//Vehicle Manufacturer Name
static t_String szHeadUnitManfacturerName = "GM";
//Vehicle Model Name
static t_String szHeadUnitModelName = "GM";
// Vehicle Model Year
static t_String sczVehicleModelYear = "2017";

/*!
* \typedef struct rModelYearMap
* \brief Structure to map the VIN 10th bit values with model year
*/
typedef struct rModelYearMap
{
   //! Model year Hex value with respect to alphabets.
   t_U8 u8ModelYearFromMap;
   //! Equivalent string value for model year
   t_String szModelYearFromMap;
}trModelYearMap;

static const trModelYearMap saModelYearLookUpTbl[]={{0x45,"2014"},{0x46,"2015"},{0x47,"2016"},{0x48,"2017"},
{0x4a,"2018"},{0x4b,"2019"},{0x4c,"2020"},{0x4d,"2021"},{0x4e,"2022"},{0x50,"2023"},{0x52,"2024"}};

/***************************************************************************
** FUNCTION:  spi_tclConfigReader::spi_tclConfigReader()
***************************************************************************/
spi_tclConfigReader::spi_tclConfigReader():
m_enDriveModeInfo(e8PARK_MODE),
m_enNightModeInfo(e8_DAY_MODE)
{
   ETG_TRACE_USR1(("spi_tclConfigReader() entered \n"));
}

/***************************************************************************
** FUNCTION:  spi_tclConfigReader::~spi_tclConfigReader()
***************************************************************************/
spi_tclConfigReader::~spi_tclConfigReader()
{
   ETG_TRACE_USR1(("~spi_tclConfigReader() entered \n"));
}

/***************************************************************************
** FUNCTION   :t_Void spi_tclConfigReader::vGetVideoConfigData()
***************************************************************************/
t_Void spi_tclConfigReader::vGetVideoConfigData(tenDeviceCategory enDevCat,
              trVideoConfigData& rfrVideoConfig)
{
   ETG_TRACE_USR1(("vGetVideoConfigData() entered."));
   SPI_INTENTIONALLY_UNUSED(enDevCat);
   EolHandler oEolHandler;    // EOL handler

   t_U8 u8SysVariant = static_cast<t_U8>(e8NAVISYSTEM8INCH);//By Default set to 8 inch Nav System

   //Read Initial SID Value
   if ( TRUE == oEolHandler.bRead( EOLLIB_TABLE_ID_SYSTEM, 
      EOLLIB_OFFSET_SYSTEM_CONFIGURATION, 
      &u8SysVariant,
      sizeof(t_U8)))
   {
      ETG_TRACE_USR4(("System Variant type :%d ",u8SysVariant));
   }
   else
   {
      ETG_TRACE_USR4(("Assigned System Variant type :%d.", u8SysVariant));
   }

   t_U8 u8ArraySize = (sizeof(aVideoConfig)/sizeof(trVideoConfigData));
   for(t_U8 u8Index = 0;u8Index < u8ArraySize;u8Index++)
   {

      if (u8SysVariant == aVideoConfig[u8Index].enSysVariant)
      {
         rfrVideoConfig.u32Screen_Width= aVideoConfig[u8Index].u32Screen_Width;
         rfrVideoConfig.u32Screen_Height= aVideoConfig[u8Index].u32Screen_Height;
         rfrVideoConfig.u32LayerId  = aVideoConfig[u8Index].u32LayerId;
         rfrVideoConfig.u32SurfaceId = aVideoConfig[u8Index].u32SurfaceId;
         rfrVideoConfig.u32TouchLayerId = aVideoConfig[u8Index].u32TouchLayerId;
         rfrVideoConfig.u32TouchSurfaceId = aVideoConfig[u8Index].u32TouchSurfaceId;
         rfrVideoConfig.u32Screen_Width_Millimeter = aVideoConfig[u8Index].u32Screen_Width_Millimeter;
         rfrVideoConfig.u32Screen_Height_Millimeter = aVideoConfig[u8Index].u32Screen_Height_Millimeter;
         rfrVideoConfig.u32ProjScreen_Width = aVideoConfig[u8Index].u32ProjScreen_Width;
         rfrVideoConfig.u32ProjScreen_Height = aVideoConfig[u8Index].u32ProjScreen_Height;
         rfrVideoConfig.u32ProjScreen_Width_Mm =  aVideoConfig[u8Index].u32ProjScreen_Width_Mm;
         rfrVideoConfig.u32ProjScreen_Height_Mm = aVideoConfig[u8Index].u32ProjScreen_Height_Mm;
         rfrVideoConfig.u32ResolutionSupported = aVideoConfig[u8Index].u32ResolutionSupported;

         break;
      }
   }

   ETG_TRACE_USR4(("Screen Width = %d",rfrVideoConfig.u32Screen_Width));
   ETG_TRACE_USR4(("Screen Height = %d",rfrVideoConfig.u32Screen_Height));
   ETG_TRACE_USR4(("Screen Width(mm) = %d",rfrVideoConfig.u32Screen_Width_Millimeter));
   ETG_TRACE_USR4(("Screen Height(mm) = %d",rfrVideoConfig.u32Screen_Height_Millimeter));
   ETG_TRACE_USR4(("Layer Id = %d",rfrVideoConfig.u32LayerId));
   ETG_TRACE_USR4(("Surface Id = %d",rfrVideoConfig.u32SurfaceId));
   ETG_TRACE_USR4(("Touch Layer Id = %d",rfrVideoConfig.u32TouchLayerId));
   ETG_TRACE_USR4(("Touch Surface Id  = %d",rfrVideoConfig.u32TouchSurfaceId));
   ETG_TRACE_USR4(("Proj Screen Width = %d",rfrVideoConfig.u32ProjScreen_Width));
   ETG_TRACE_USR4(("Proj Screen Height = %d",rfrVideoConfig.u32ProjScreen_Height));
   ETG_TRACE_USR4(("Proj Screen Width(mm) = %d",rfrVideoConfig.u32ProjScreen_Width_Mm));
   ETG_TRACE_USR4(("Proj Screen Height(mm) = %d",rfrVideoConfig.u32ProjScreen_Height_Mm));

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclConfigReader::vReadVehicleBrand(t_U8& rfu8VehicleBand)
***************************************************************************/
t_Void spi_tclConfigReader::vReadVehicleBrand(t_U8& rfu8VehicleBand)
{
   ETG_TRACE_USR1(("vReadVehicleBrand() entered."));

   EolHandler oEolHandler;    // EOL handler
   if ( TRUE == oEolHandler.bRead( EOLLIB_TABLE_ID_BRAND, 
      EOLLIB_OFFSET_VEHICLE_ANIMATION_WELCOME_TYPE,
      &rfu8VehicleBand,
      sizeof(t_U8)))
   {
      ETG_TRACE_USR4(("Vehicle brand :%d ",rfu8VehicleBand));
   }
   else
   {
      ETG_TRACE_USR4(("Default Vehicle brand :%d.", rfu8VehicleBand));
   }
}

/***************************************************************************
** FUNCTION   :t_Void spi_tclConfigReader::vGetOemIconData(
              trVehicleBrandInfo& rfrVehicleBrandInfo)
***************************************************************************/
t_Void spi_tclConfigReader::vGetOemIconData(trVehicleBrandInfo& rfrVehicleBrandInfo)
{
   ETG_TRACE_USR1(("vGetOemIconData() entered."));
   t_U8 u8VehicleBrand = 0;

   vReadVehicleBrand(u8VehicleBrand);
    t_U8 u8ArraySize = (sizeof(aVehicleConfig)/sizeof(trVehicleBrandInfo));

    memset(rfrVehicleBrandInfo.szOemIconPath, 0, STR_LENGTH);
    memset(rfrVehicleBrandInfo.szOemName, 0, STR_LENGTH);
	memset(rfrVehicleBrandInfo.szManufacturer, 0, STR_LENGTH);
    memset(rfrVehicleBrandInfo.szModel, 0, STR_LENGTH);
	strncpy(rfrVehicleBrandInfo.szOemIconPath, OEM_ICON_PATH, STR_LENGTH);
	strncpy(rfrVehicleBrandInfo.szOemName, OEM_ICON_NAME, STR_LENGTH);
    strncpy(rfrVehicleBrandInfo.szManufacturer, MANUF_NAME_STRING, STR_LENGTH);

    //! Headunit manufacturer and  model name is same as vehicle model for GM
    szHeadUnitModelName = rfrVehicleBrandInfo.szModel;
    szHeadUnitManfacturerName = rfrVehicleBrandInfo.szManufacturer;

   for(t_U8 u8Index = 0; u8Index < u8ArraySize;u8Index++)
   {
      if((u8VehicleBrand >=aVehicleConfig[u8Index].u8GMVehicleBrandStartRange) && (u8VehicleBrand<=aVehicleConfig[u8Index].u8GMVehicleBrandEndRange))
      //if(u8VehicleBrand == aVehicleConfig[u8Index].u8GMVehicleBrand)
      {
         strncpy(rfrVehicleBrandInfo.szOemIconPath, aVehicleConfig[u8Index].szOemIconPath, STR_LENGTH);
         strncpy(rfrVehicleBrandInfo.szOemName, aVehicleConfig[u8Index].szOemName, STR_LENGTH);
         strncpy(rfrVehicleBrandInfo.szModel, aVehicleConfig[u8Index].szOemName, STR_LENGTH);
         ETG_TRACE_USR4(("path :%s", rfrVehicleBrandInfo.szOemIconPath));
         ETG_TRACE_USR4(("value :%s", rfrVehicleBrandInfo.szOemName));
         break;
      }
   }
   ETG_TRACE_USR4(("Array Size :%d", u8ArraySize));

}


/***************************************************************************
** FUNCTION:  t_Void spi_tclConfigReader::vGetSpiFeatureSupport(...)
***************************************************************************/
t_Void spi_tclConfigReader::vGetSpiFeatureSupport(trSpiFeatureSupport& rfrSpiFeatureSupport)
{
   EolHandler oEolHandler;
   t_U8 u8EolMLEnableSetting = 0;
   t_U8 u8EolDipoEnableSetting = 0;
   t_U8 u8EolAAPEnableSetting = 0;

   //! Read MirrorLink & DiPO settings from EOL
#ifdef VARIANT_S_FTR_ENABLE_SPI_MIRRORLINK
  // ML is enabled as a feature, hence ML enabling or disabling during runtime is based on EOL values.
   if (FALSE == oEolHandler.bRead(EOLLIB_TABLE_ID_BRAND,
         EOLLIB_OFFSET_ENABLE_APPLICATION_MIRRORLINK, &u8EolMLEnableSetting, sizeof(t_U8)))
   {
      ETG_TRACE_ERR(("vGetSpiFeatureSupport: Error reading MirrorLink setting from EOL Table! \n"));
   }
#endif


   if (FALSE == oEolHandler.bRead(EOLLIB_TABLE_ID_BRAND,
         EOLLIB_OFFSET_ENABLE_APPLICATION_DIGITAL_IPOD_OUT, &u8EolDipoEnableSetting, sizeof(t_U8)))
   {
      ETG_TRACE_ERR(("vGetSpiFeatureSupport: Error reading DiPO setting from EOL Table! \n"));
   }

#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO
  // AAP is enabled as a feature, hence AAP enabling or disabling during runtime is based on EOL values.
   if (FALSE == oEolHandler.bRead(EOLLIB_TABLE_ID_BRAND,
            EOLLIB_OFFSET_RESERVED_BYTE_9_BRAND, &u8EolAAPEnableSetting, sizeof(t_U8)))
   {
      ETG_TRACE_ERR(("vGetSpiFeatureSupport: Error reading Projection setting from EOL Table! \n"));
   }
#endif
   //! Set supported SPI features
   rfrSpiFeatureSupport.vSetMirrorLinkSupport(0x01 == u8EolMLEnableSetting);
   rfrSpiFeatureSupport.vSetDipoSupport(0x01 == u8EolDipoEnableSetting);
   t_Bool bAndroidAutoEnabled = (0x01 ==(0x01 && u8EolAAPEnableSetting));
   rfrSpiFeatureSupport.vSetAndroidAutoSupport(bAndroidAutoEnabled);

   ETG_TRACE_USR1(("spi_tclConfigReader::vGetSpiFeatureSupport: MIRRORLINK supported = %d \n",
         ETG_ENUM(BOOL, u8EolMLEnableSetting)));
   ETG_TRACE_USR1(("spi_tclConfigReader::vGetSpiFeatureSupport: DIPO supported = %d \n",
         ETG_ENUM(BOOL, u8EolDipoEnableSetting)));
   ETG_TRACE_USR1(("spi_tclConfigReader::vGetSpiFeatureSupport: ANDROID AUTO supported = %d u8EolAAPEnableSetting = %d\n",
         ETG_ENUM(BOOL, bAndroidAutoEnabled), u8EolAAPEnableSetting));
}

/***************************************************************************
** FUNCTION:  tenDriveSideInfo spi_tclConfigReader::enGetDriveSideInfo(...)
***************************************************************************/
tenDriveSideInfo spi_tclConfigReader::enGetDriveSideInfo()
{
   //! Read DriveSide setting from EOL
   EolHandler oEolHandler;
   t_U8 u8EolDriveSideSetting = 0;

   if (FALSE == oEolHandler.bRead(EOLLIB_TABLE_ID_COUNTRY,
         EOLLIB_OFFSET_DRIVE_SIDE, &u8EolDriveSideSetting, sizeof(t_U8)))
   {
      ETG_TRACE_ERR(("spi_tclConfigReader::enGetDriveSideInfo: Error reading from EOL Table! \n"));
   }

   //! Evaluate Drive Side
   tenDriveSideInfo enDriveSideInfo = (cou8EOL_DRIVESIDE_RIGHT == u8EolDriveSideSetting)?
         (e8RIGHT_HAND_DRIVE): (e8LEFT_HAND_DRIVE);

   ETG_TRACE_USR1(("spi_tclConfigReader::enGetDriveSideInfo() left with enDriveSideInfo = %d \n",
         ETG_ENUM(DRIVE_SIDE_TYPE, enDriveSideInfo)));

   return enDriveSideInfo;

}

/***************************************************************************
 ** FUNCTION:  t_U8 spi_tclConfigReader::u8GetAAPParkRestrictionInfo()
 ***************************************************************************/
t_U8 spi_tclConfigReader::u8GetAAPParkRestrictionInfo()
{
	ETG_TRACE_USR1(("spi_tclConfigReader::u8GetAAPParkRestrictionInfo "));
	//Note: This function will not be used for GM as it does not have two restriction info(Park/Drive)separately.
	t_U8 u8EolParkRestrictionInfo = 0;
	return u8EolParkRestrictionInfo;
}

/***************************************************************************
** FUNCTION:  tenDriveSideInfo spi_tclConfigReader::enGetDriveSideInfo(...)
***************************************************************************/
t_U8 spi_tclConfigReader::u8GetAAPDriveRestrictionInfo()
{
   //! Read DriveSide setting from EOL
   EolHandler oEolHandler;
   t_U8 u8EolDriveRestrictionInfo = 0;

   if (FALSE == oEolHandler.bRead(EOLLIB_TABLE_ID_COUNTRY,
		   EOLLIB_OFFSET_DWL_EMAIL_MASK_BYTE1, &u8EolDriveRestrictionInfo, sizeof(t_U8)))
   {
      ETG_TRACE_ERR(("spi_tclConfigReader::u8GetAAPDriveRestrictionInfo: Error reading from EOL Table! \n"));
   }

   ETG_TRACE_USR1(("spi_tclConfigReader::u8GetAAPDriveRestrictionInfo()  u8EolDriveRestrictionInfo = %d \n",
		   u8EolDriveRestrictionInfo));

   u8EolDriveRestrictionInfo = (u8EolDriveRestrictionInfo & (0x1F));

   ETG_TRACE_USR2(("spi_tclConfigReader::u8GetAAPDriveRestrictionInfo() after masking u8EolDriveRestrictionInfo = %d \n",
		   u8EolDriveRestrictionInfo));

   return u8EolDriveRestrictionInfo;
}

/***************************************************************************
 ** FUNCTION:  t_U8 spi_tclConfigReader::u8GeDiPODriveRestrictionInfo(...)
 ***************************************************************************/
t_U8 spi_tclConfigReader::u8GeDiPOParkRestrictionInfo()
{
	ETG_TRACE_USR1(("spi_tclConfigReader::u8GeDiPOParkRestrictionInfo "));
	//Note: This function will not be used for GM as it does not have two restriction info(Park/Drive)separately.
	t_U8 u8EolParkRestrictionInfo = 0;
	return u8EolParkRestrictionInfo;
}
/***************************************************************************
** FUNCTION:  tenDriveSideInfo spi_tclConfigReader::u8GeDiPODriveRestrictionInfo(...)
***************************************************************************/
t_U8 spi_tclConfigReader::u8GeDiPODriveRestrictionInfo()
{
   EolHandler oEolHandler;
   t_U8 u8EolDriveRestrictionInfo = 0;

   //@Note :  In GM EOLLIB_OFFSET_DWL_EMAIL_MASK_BYTE1 is the id used to represent the Drive restriction.
   if (FALSE == oEolHandler.bRead(EOLLIB_TABLE_ID_COUNTRY,
		   EOLLIB_OFFSET_DWL_EMAIL_MASK_BYTE1, &u8EolDriveRestrictionInfo, sizeof(t_U8)))
   {
	  ETG_TRACE_ERR(("spi_tclConfigReader::u8GeDiPODriveRestrictionInfo: Error reading from EOL Table! \n"));
   }

   //! If the 7th bit is set to 0 nothing to block. if it is set block the music list, non music list and softkeyboard.
   u8EolDriveRestrictionInfo = u8EolDriveRestrictionInfo & 0x80;

   ETG_TRACE_USR1(("spi_tclConfigReader::u8GeDiPODriveRestrictionInfo()  u8EolDriveRestrictionInfo = %d \n",
		   u8EolDriveRestrictionInfo));

   if(0x80 == u8EolDriveRestrictionInfo)
   {
	   //! Lock only music list, non music list and softkeyboard.
	   u8EolDriveRestrictionInfo = 0x0D;
   }
   else
   {
	   //Nothing to lock in this case.
	   u8EolDriveRestrictionInfo = 0x00;
   }
   return u8EolDriveRestrictionInfo;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclConfigReader::bGetMLNotificationSetting()
***************************************************************************/
t_Bool spi_tclConfigReader::bGetMLNotificationSetting()
{
   //! Read ML Notification setting from EOL
   EolHandler oEolHandler;
   t_U8 u8EolMLNotifSetting = 0;

   if (FALSE == oEolHandler.bRead(EOLLIB_TABLE_ID_BRAND,
         EOLLIB_OFFSET_DEFAULT_MIRRORLINK_NOTIFICATION_SETTING,
         &u8EolMLNotifSetting, sizeof(t_U8)))
   {
      ETG_TRACE_ERR(("spi_tclConfigReader::bGetMLNotificationSetting: Error reading from EOL Table! \n"));
   }

   t_Bool bMLNotifOnOff = (0x01 == u8EolMLNotifSetting)? true : false;

   ETG_TRACE_USR1(("spi_tclConfigReader::bGetMLNotificationSetting() left with MLNotification = %d \n",
         ETG_ENUM(BOOL, bMLNotifOnOff)));
   return bMLNotifOnOff;
}

/***************************************************************************
** FUNCTION:  tenSystemVariant spi_tclConfigReader::enGetSystemVariant()
***************************************************************************/
tenSystemVariant spi_tclConfigReader::enGetSystemVariant()
{
   EolHandler oEolHandler;    // EOL handler
   t_U8 u8SysVariant = 0;
   tenSystemVariant enSysVariant = e8VARIANT_COLOR; //default

   //Read system configuration from EOL table
   if (TRUE == oEolHandler.bRead( EOLLIB_TABLE_ID_SYSTEM,
      EOLLIB_OFFSET_SYSTEM_CONFIGURATION,
      &u8SysVariant,
      sizeof(t_U8)))
   {
      tenGMSystemVariant enGMVariant = static_cast<tenGMSystemVariant>(u8SysVariant);
      enSysVariant = ((e8NAVISYSTEM8INCH == enGMVariant) || (e8NAVISYSTEM10DOT2INCH == enGMVariant)) ?
            (e8VARIANT_NAVIGATION) : (e8VARIANT_COLOR);
   }
   else
   {
      ETG_TRACE_USR4(("spi_tclConfigReader::enGetSystemVariant: Error reading from EOL table! \n"));
   }

   ETG_TRACE_USR2(("spi_tclConfigReader::enGetSystemVariant() left with %d \n",
         ETG_ENUM(SYSTEM_VARIANT, enSysVariant)));
   return enSysVariant;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetClientID()
***************************************************************************/
t_String spi_tclConfigReader::szGetClientID()
{

   ETG_TRACE_USR1(("spi_tclConfigReader::szGetClientID() left with ClientID = %s",
      cszClientProfileId.c_str()));

   return cszClientProfileId.c_str();
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetClientFriendlyName()
***************************************************************************/
t_String spi_tclConfigReader::szGetClientFriendlyName()
{

   ETG_TRACE_USR1(("spi_tclConfigReader::szGetClientFriendlyName() left with FriendlyName = %s",
      sczClientFriendlyName.c_str()));

   return sczClientFriendlyName.c_str();
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetClientManufacturerName()
***************************************************************************/
t_String spi_tclConfigReader::szGetClientManufacturerName()
{
   return sczClientManfacturerName.c_str();
}
/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetModelYear()
***************************************************************************/
t_String spi_tclConfigReader::szGetModelYear()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::szGetModelYear() left with Model Year= %s",
         sczVehicleModelYear.c_str()));
   return sczVehicleModelYear;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetHeadUnitManufacturerName()
***************************************************************************/
t_String spi_tclConfigReader::szGetHeadUnitManufacturerName()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::szGetHeadUnitManufacturerName() left with Head unit manufacturer name = %s",
      szHeadUnitManfacturerName.c_str()));
   return szHeadUnitManfacturerName;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetHeadUnitModelName()
***************************************************************************/
t_String spi_tclConfigReader::szGetHeadUnitModelName()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::szGetHeadUnitModelName() left with Head unit model name = %s",
      szHeadUnitModelName.c_str()));
   return szHeadUnitModelName;
}
/***************************************************************************
** FUNCTION:  t_Void spi_tclConfigReader::vReadScreenConfigs()
***************************************************************************/
t_Void spi_tclConfigReader::vReadScreenConfigs()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::vReadScreenConfigs"));
   //@todo - Read EOL values from here. Call this method from constructor
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclConfigReader::vSetScreenConfigs()
***************************************************************************/
t_Void spi_tclConfigReader::vSetScreenConfigs(const trDisplayAttributes& corfrDispLayerAttr)
{
   SPI_INTENTIONALLY_UNUSED(corfrDispLayerAttr);
   ETG_TRACE_USR1(("spi_tclConfigReader::vSetScreenConfigs"));
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclConfigReader::bGetRotaryCtrlSupport()
***************************************************************************/
t_Bool spi_tclConfigReader::bGetRotaryCtrlSupport()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::vSetScreenConfigs"));

   t_Bool bIsRotCtrlSupport = true;
   t_U8 rfu8VehicleBand =0;
   vReadVehicleBrand(rfu8VehicleBand);
   
   ETG_TRACE_USR4(("Vehicle Brand: %d ",rfu8VehicleBand));

   if (rfu8VehicleBand < IS_CADILLAC)
   {
      bIsRotCtrlSupport = false;
   }
   return bIsRotCtrlSupport;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetSoftwareVersion()
***************************************************************************/
t_String spi_tclConfigReader::szGetSoftwareVersion()
{
   //! Read software version using datapool mock for registry (refer reg_mock.h)
   reg_tclRegKey oReg;
   t_Char     szBuildCustomerVersion[scou32MAX_SIZE];
   t_String   szSwVersion;

   if (oReg.bOpen(sczRegistryPath.c_str()))
   {
      if (oReg.bQueryString("BUILDVERSION_CUSTVERSTRING", (tChar*) &szBuildCustomerVersion, sizeof(szBuildCustomerVersion)))
      {
         szSwVersion = szBuildCustomerVersion;
      }
   }

   ETG_TRACE_USR1(("spi_tclConfigReader::szGetSoftwareVersion  szSwVersion = %s", szSwVersion.c_str()));
   return szSwVersion;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vSetDriveModeInfo()
***************************************************************************/
t_Void spi_tclConfigReader::vSetDriveModeInfo(const tenVehicleConfiguration enDriveModeInfo)
{
	//The data will access from different threads. Hence locked before use.
	m_oLock.s16Lock();
	m_enDriveModeInfo = enDriveModeInfo;
	m_oLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vGetDriveModeInfo()
***************************************************************************/
t_Void spi_tclConfigReader::vGetDriveModeInfo(tenVehicleConfiguration &rfoenDriveModeInfo)
{
	m_oLock.s16Lock();
	rfoenDriveModeInfo =  m_enDriveModeInfo;
	m_oLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vSetNightModeInfo()
***************************************************************************/
t_Void spi_tclConfigReader::vSetNightModeInfo(const tenVehicleConfiguration enNightModeInfo)
{
	m_oLock.s16Lock();
	m_enNightModeInfo = enNightModeInfo;
	m_oLock.vUnlock();

}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vGetNightModeInfo()
***************************************************************************/
t_Void spi_tclConfigReader::vGetNightModeInfo(tenVehicleConfiguration &rfoenNightModeInfo)
{
	m_oLock.s16Lock();
	rfoenNightModeInfo = m_enNightModeInfo;
	m_oLock.vUnlock();

}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vSetDriveSideInfo()
***************************************************************************/
t_Void spi_tclConfigReader::vSetDriveSideInfo(const tenVehicleConfiguration enVehicleConfig)
{
   //@Note: Nothing to do here. Reading data from EOL
   SPI_INTENTIONALLY_UNUSED(enVehicleConfig);
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vGetDisplayInputParam()
***************************************************************************/
t_Void spi_tclConfigReader::vGetDisplayInputParam(t_U8 &rfu8DisplayInput)
{
	if(bGetRotaryCtrlSupport())
	{
		rfu8DisplayInput = LOW_FIDELITY_TOUCH_WITH_KNOB;
	}
	else
	{
		rfu8DisplayInput = LOW_FIDELITY_TOUCH;
	}
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclConfigReader::vGetVehicleInfoDataAAP(trVehicleInfo& rfrVehicleInfo)
***************************************************************************/

t_Void spi_tclConfigReader::vGetVehicleInfoDataAAP(trVehicleInfo& rfrVehicleInfo)
{
  ETG_TRACE_USR1(("vGetVehicleInfoDataAAP() entered."));
  t_U8 u8VehicleBrand = 0;

  vReadVehicleBrand(u8VehicleBrand);
  t_U8 u8ArraySize = (sizeof(aManufacturerModelConfig)/sizeof(trVehicleInfo));
  for(t_U8 u8Index = 0; u8Index < u8ArraySize;u8Index++)
    {
      if((u8VehicleBrand >=aManufacturerModelConfig[u8Index].u8GMVehicleBrandStartRange) && (u8VehicleBrand<=aManufacturerModelConfig[u8Index].u8GMVehicleBrandEndRange))
        {
          rfrVehicleInfo.szManufacturer=aManufacturerModelConfig[u8Index].szManufacturer;
          rfrVehicleInfo.szModel= aManufacturerModelConfig[u8Index].szModel;
          ETG_TRACE_USR4(("Manufacturer Name :%s", rfrVehicleInfo.szManufacturer.c_str()));
          ETG_TRACE_USR4(("Model Name :%s", rfrVehicleInfo.szModel.c_str()));
          break;
        }
    }
  szHeadUnitModelName = rfrVehicleInfo.szModel;
  szHeadUnitManfacturerName = rfrVehicleInfo.szManufacturer;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclConfigReader::vGetModelYearData(const t_U8 cou8VehicleModelYear)
***************************************************************************/
t_Void spi_tclConfigReader::vSetVehicleModelYearInfo(t_U8 cou8VehicleModelYear)
{
   ETG_TRACE_USR1(("vSetVehicleModelYearInfo() entered."));
   t_U8 u8ArraySize = (sizeof(saModelYearLookUpTbl)/sizeof(trModelYearMap));
   for (t_U8 u8Index = 0; u8Index < u8ArraySize; u8Index++)
   {
       if (saModelYearLookUpTbl[u8Index].u8ModelYearFromMap == (cou8VehicleModelYear))
       {
          sczVehicleModelYear = saModelYearLookUpTbl[u8Index].szModelYearFromMap;
          ETG_TRACE_USR4(("Model year :%s", sczVehicleModelYear.c_str()));
           break;
       }// if(saModelYearLookUpTbl[u8Index].u8ModelYearFromMap
   }//for(tU8 u8Index=0;saModelYearLookUpTbl[u8Index]
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclConfigReader::vGetDefaultProjUsageSettings
 ***************************************************************************/
t_Void spi_tclConfigReader::vGetDefaultProjUsageSettings(tenDeviceCategory enSPIType, tenEnabledInfo &enEnabledInfo)
{
   SPI_INTENTIONALLY_UNUSED(enSPIType);
   enEnabledInfo = e8USAGE_ENABLED;
}


/***************************************************************************
 ** FUNCTION:  t_Void spi_tclConfigReaderBase::vClearVehicleId()
 ** Setting Vehicle Id to DataPool.
 ** This function will be called on defset and it will set the string to empty.
 ***************************************************************************/
t_Void spi_tclConfigReader::vClearVehicleId()
{
   ETG_TRACE_USR1(("vClearVehicleId() entered \n"));
   Datapool oDatapool;
   t_String szVehicleIdentifier;
   oDatapool.bWriteVehicleId(szVehicleIdentifier);
   ETG_TRACE_USR1(("vClearVehicleId() entered and value of vehicle Id after writing in datapool is :%s",szVehicleIdentifier.c_str()));
}

/***************************************************************************
 ** FUNCTION:  t_String spi_tclConfigReaderBase::szGetVehicleId()
 ** Function returns Vehicle Id after reading it from DataPool.
 ***************************************************************************/
t_String spi_tclConfigReader::szGetVehicleId()
{
   ETG_TRACE_USR1(("szGetVehicleId() entered \n"));
   Datapool oDatapool;
   t_String szVehicleIdentifier;
   //Read Vehicle Id if available.
   oDatapool.bReadVehicleId(szVehicleIdentifier);
   ETG_TRACE_USR1(("szGetVehicleId() entered and value of vehicle Id after reading from datapool is :%s",szVehicleIdentifier.c_str()));
   //If Vehicle Id is not available,then generate the vehicle Id using Random Generator Algorithm
   //and write it to DataPool,Read and return it.
   ETG_TRACE_USR1(("length of Vehicle Id string after reading from datapool is :%d",szVehicleIdentifier.length()));
   if (0 == szVehicleIdentifier.length())
   {
      RandomIdGenerator oRandomIdGenerator;
      szVehicleIdentifier=oRandomIdGenerator.szgenerateRandomId();
      ETG_TRACE_USR1((" value of vehicle Id after using random function is :%s",szVehicleIdentifier.c_str()));
      oDatapool.bWriteVehicleId(szVehicleIdentifier);
      ETG_TRACE_USR1(("szGetVehicleId() entered and value of vehicle Id after writing in datapool and then reading is :%s",szVehicleIdentifier.c_str()));
   }
   return szVehicleIdentifier;
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
