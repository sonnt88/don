/***********************************************************************/
/*!
* \file  spi_tclConfigReader.h
* \brief Class to get the Variant specific Info for GM
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to get the Variant specific Info for GM
AUTHOR:         Hari Priya E R
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
19.05.2013  | Hari Priya E R        | Initial Version
31.07.2013  | Ramya Murthy          | Implementation for generic config usage and
                                      SPI feature configuration
05.08.2013  | Ramya Murthy          | Added DriveSide info
06.08.2013  | Ramya Murthy          | Added MLNotification setting
06.11.2014  |  Hari Priya E R       | Added changes to get the GM Vehicle brand
17.11.2014  | Ramya Murthy          | Added System variant info
28.11.2014  | Ramya Murthy          | Updated SPI Feature support info
08.05.2015  | Shiva Kumar G         | Extended for ML Client Profile configs

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLCONFIGREADER_H_
#define _SPI_TCLCONFIGREADER_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "GenericSingleton.h"
#include "Lock.h"
#include "SPITypes.h"
#include "spi_tclConfigReaderBase.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclConfigReader
* \brief Class to get the Variant specific Info for GM
****************************************************************************/

class spi_tclConfigReader:public GenericSingleton<spi_tclConfigReader>, public spi_tclConfigReaderBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/
   //spi_tclConfigReader();
   /***************************************************************************
   ** FUNCTION:  spi_tclConfigReader::~spi_tclConfigReader()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclConfigReader()
   * \brief   Destructor
   * \sa      spi_tclConfigReader()
   **************************************************************************/
   virtual ~spi_tclConfigReader();

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclConfigReader::vGetVideoConfigData()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetVideoConfigData(tenDeviceCategory enDevCat,
   *                 trVideoConfigData& rfrVideoConfig)
   * \brief   Method to read the EOL value and get the video configuration data 
              from the look up table
   * \param   enDevCat      : [IN] Provide configurations fro the requested technology
   * \param   rfrVideoConfig: [IN] Video Config Data
   * \retval  NONE
   **************************************************************************/
   t_Void vGetVideoConfigData(tenDeviceCategory enDevCat,
      trVideoConfigData& rfrVideoConfig);

      /***************************************************************************
   ** FUNCTION: t_Void spi_tclConfigReader::vGetOemIconData(
                     trVehicleBrandInfo& rfrVehicleBrandInfo)
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetOemIconData(trVehicleBrandInfo& rfrVehicleBrandInfo)
   * \brief   Method to read the EOL value and get the Brand configuration data 
              from the look up table
   * \param   rfrVideoConfig: [IN]Vehicle Brand Data
   * \retval  NONE
   **************************************************************************/
   t_Void vGetOemIconData(trVehicleBrandInfo& rfrVehicleBrandInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclConfigReader::vGetSpiFeatureSupport(...)
   ***************************************************************************/
   /*!
   * \fn      vGetSpiFeatureSupport(trSpiFeatureSupport& rfrSpiFeatureSupport)
   * \brief   Method to read supported SPI features info.
   * \param   rfrSpiFeatureSupport: [OUT] Provides supported SPI features
   * \retval  NONE
   **************************************************************************/
   t_Void vGetSpiFeatureSupport(trSpiFeatureSupport& rfrSpiFeatureSupport);

   /***************************************************************************
   ** FUNCTION:  tenDriveSideInfo spi_tclConfigReader::enGetDriveSideInfo(...)
   ***************************************************************************/
   /*!
   * \fn      enGetDriveSideInfo()
   * \brief   Method to read Drive side info.
   * \param   NONE
   * \retval  Drive Side enum
   **************************************************************************/
   tenDriveSideInfo enGetDriveSideInfo();


	/***************************************************************************
	 ** FUNCTION:  t_U8 spi_tclConfigReader::u8GetAAPParkRestrictionInfo(...)
	 ***************************************************************************/
	/*!
	 * \fn      u8GetAAPParkRestrictionInfo()
	 * \brief   Method to read Park Restriction info for Android Auto.
	 * \param   NONE
	 * \retval  8-bit integer
	 **************************************************************************/
	t_U8 u8GetAAPParkRestrictionInfo();

	/***************************************************************************
	 ** FUNCTION:  tenDriveSideInfo spi_tclConfigReader::u8GetAAPDriveRestrictionInfo(...)
	 ***************************************************************************/
	/*!
	 * \fn      u8GetAAPDriveRestrictionInfo()
	 * \brief   Method to read Drive Restriction info for Android Auto.
	 * \param   NONE
	 * \retval  8-bit integer
	 **************************************************************************/

	t_U8 u8GetAAPDriveRestrictionInfo();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclConfigReader::bGetMLNotificationSetting()
   ***************************************************************************/
   /*!
   * \fn      bGetMLNotificationSetting()
   * \brief   Method to read MLNotification On/Off setting from EOL.
   * \param   NONE
   * \retval  t_Bool: true - if MLNotification is ON, else false.
   **************************************************************************/
   t_Bool bGetMLNotificationSetting();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclConfigReader::vReadVehicleBrand(t_U8& rfu8VehicleBand)
   ***************************************************************************/
   /*!
   * \fn      vReadVehicleBrand()
   * \brief   Method to read the vehicle brand value from EOL
   * \param   rfu8VehicleBand: Vehicle Brand value
   * \retval  NONE
   **************************************************************************/
   t_Void vReadVehicleBrand(t_U8& rfu8VehicleBand);
   
   /***************************************************************************
   ** FUNCTION:  tenSystemVariant spi_tclConfigReader::enGetSystemVariant()
   ***************************************************************************/
   /*!
   * \fn      enGetSystemVariant()
   * \brief   Method to read System variant type
   * \param   NONE
   * \retval  tenSystemVariant
   **************************************************************************/
   tenSystemVariant enGetSystemVariant();

   /***************************************************************************
   ** FUNCTION:  t_String spi_tclConfigReader::szGetClientID()
   ***************************************************************************/
   /*!
   * \fn      t_String szGetClientID()
   * \brief   Method to get the client ID.
   * \param   NONE
   * \retval  t_String
   **************************************************************************/
   t_String szGetClientID();

   /***************************************************************************
   ** FUNCTION:  t_String spi_tclConfigReader::szGetClientFriendlyName()
   ***************************************************************************/
   /*!
   * \fn      t_String szGetClientFriendlyName()
   * \brief   Method to get the client friendly name.
   * \param   NONE
   * \retval  t_String
   **************************************************************************/
   t_String szGetClientFriendlyName();

   /***************************************************************************
   ** FUNCTION:  t_String spi_tclConfigReader::szGetClientManufacturerName()
   ***************************************************************************/
   /*!
   * \fn      t_String szGetClientManufacturerName()
   * \brief   Method to get the client manufacturer name.
   * \param   NONE
   * \retval  t_String
   **************************************************************************/
   t_String szGetClientManufacturerName();


   /***************************************************************************
   ** FUNCTION:  t_String spi_tclConfigReader::szGetModelYear()
   ***************************************************************************/
   /*!
   * \fn      t_String szGetModelYear()
   * \brief   Method to get the Model year of the vehicle
   * \param   NONE
   * \retval  t_String
   **************************************************************************/
   t_String szGetModelYear();

   /***************************************************************************
   ** FUNCTION:  t_String spi_tclConfigReader::szGetHeadUnitManufacturerName()
   ***************************************************************************/
   /*!
   * \fn      t_String szGetHeadUnitManufacturerName()
   * \brief   Method to get the Headunit manufacturer name.
   * \param   NONE
   * \retval  t_String
   **************************************************************************/
   t_String szGetHeadUnitManufacturerName();

   /***************************************************************************
   ** FUNCTION:  t_String spi_tclConfigReader::szGetHeadUnitModelName()
   ***************************************************************************/
   /*!
   * \fn      t_String szGetHeadUnitModelName()
   * \brief   Method to get the headunit model name.
   * \param   NONE
   * \retval  t_String
   **************************************************************************/
   t_String szGetHeadUnitModelName();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclConfigReader::vReadScreenConfigs()
   ***************************************************************************/
   /*!
   * \fn      t_Void vReadScreenConfigs(const trDisplayAttributes& corfrDispLayerAttr)
   * \brief   Method to set screen attributes
   * \param   corfrDispLayerAttr : [IN] Display attributes
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetScreenConfigs(const trDisplayAttributes& corfrDispLayerAttr);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclConfigReader::bGetRotaryCtrlSupport()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bGetRotaryCtrlSupport()
   * \brief   Method to know if Rotary Controller is supported
   * \param   NONE
   * \retval  t_String
   **************************************************************************/
   t_Bool bGetRotaryCtrlSupport();

	/***************************************************************************
	 ** FUNCTION:  t_U8 spi_tclConfigReader::u8GeDiPOParkRestrictionInfo()
	 ***************************************************************************/
	/*!
	 * \fn      t_U8 u8GeDiPOParkRestrictionInfo()
	 * \brief   Method to return the DiPO park restriction info.
	 * \param   NONE
	 * \retval  t_U8
	 **************************************************************************/
	t_U8 u8GeDiPOParkRestrictionInfo();

	/***************************************************************************
	 ** FUNCTION:  t_Bool spi_tclConfigReader::u8GeDiPODriveRestrictionInfo()
	 ***************************************************************************/
	/*!
	 * \fn      t_Bool u8GeDiPODriveRestrictionInfo()
	 * \brief   Method to return the DiPO drive restriction info.
	 * \param   NONE
	 * \retval  t_U8
	 **************************************************************************/
	t_U8 u8GeDiPODriveRestrictionInfo();

	/***************************************************************************
	 ** FUNCTION:  t_String spi_tclConfigReader::szGetSoftwareVersion()
	 ***************************************************************************/
	/*!
	 * \fn      t_String szGetSoftwareVersion()
	 * \brief   Method to get the software version
	 * \param   NONE
	 * \retval  t_String
	 **************************************************************************/
	t_String szGetSoftwareVersion();

   /***************************************************************************
   ** FUNCTION:  t_Void vSetFeatureRestrictions(tenDeviceCategory enDevCategory,
   ** const t_U8 cou8ParkModeRestrictionInfo,const t_U8 cou8DriveModeRestrictionInfo)
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetFeatureRestrictions(tenDeviceCategory enDevCategory,
   *           const t_U8 cou8ParkModeRestrictionInfo,
   *           const t_U8 cou8DriveModeRestrictionInfo)
   * \brief   Method to set Vehicle Park/Drive Mode Restriction
   * \param   NONE
   * \retval  t_Void
   **************************************************************************/
   //Needs to be revisited when ConfigReaderDevBase is implemented
   t_Void vSetFeatureRestrictions(tenDeviceCategory enDevCategory,
            const t_U8 cou8ParkModeRestrictionInfo,
            const t_U8 cou8DriveModeRestrictionInfo){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclConfigReader::vSetDriveModeInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetDriveModeInfo()
   * \brief   Method set the drive mode information
   * \param   enDriveModeInfo : [IN] Drive mode info.
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetDriveModeInfo(const tenVehicleConfiguration enDriveModeInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclConfigReader::vGetDriveModeInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetDriveModeInfo()
   * \brief   Method set the drive mode information
   * \param   enDriveModeInfo : [OUT] Drive mode info.
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetDriveModeInfo(tenVehicleConfiguration &rfoenDriveModeInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclConfigReader::vSetNightModeInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetNightModeInfo()
   * \brief   Method set the night  mode information
   * \param   enNightModeInfo : [IN] Night mode info
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetNightModeInfo(const tenVehicleConfiguration enNightModeInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclConfigReader::vGetNightModeInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetNightModeInfo()
   * \brief   Method get the night  mode information
   * \param   rfoenNightModeInfo : [OUT] Night mode info
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetNightModeInfo(tenVehicleConfiguration &rfoenNightModeInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclConfigReader::vSetDriveSideInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetDriveSideInfo()
   * \brief   Method to set the night mode info
   * \param   enVehicleConfig : [IN] Drive side info.
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetDriveSideInfo(const tenVehicleConfiguration enVehicleConfig);
   
   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclConfigReader::vGetDisplayInputParam()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetDisplayInputParam()
   * \brief   Method get the display input configuration.Input methods enabled.
   * \param   rfu8DisplayInput : [OUT] Display input parameter for DiPO
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetDisplayInputParam(t_U8 &rfu8DisplayInput);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclConfigReader::vGetVehicleInfoDataAAP(
                     trVehicleBrandInfo& rfrVehicleBrandInfo)
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetVehicleInfoDataAAP(trVehicleBrandInfo& rfrVehicleBrandInfo)
   * \brief   Method to read vehicle information (Model,manufacturer name)
   * \param   rfrVehicleInfo: [IN]Vehicle Info Data
   * \retval  NONE
   **************************************************************************/
   t_Void vGetVehicleInfoDataAAP(trVehicleInfo& rfrVehicleInfo);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclConfigReader::vSetVehicleModelYearInfo(const t_U8 cou8VehicleModelYear)
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetVehicleModelYearInfo(const t_U8 cou8VehicleModelYear)
   * \brief   Method to set model year.
   * \param   cou8VehicleModelYear: [IN]Model year Data
   * \retval  NONE
   **************************************************************************/
   t_Void vSetVehicleModelYearInfo(const t_U8 cou8VehicleModelYear);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclConfigReader::vGetDefaultProjUsageSettings
    ***************************************************************************/
   /*!
    * \fn     t_Bool vGetDefaultProjUsageSettings()
    * \brief  Method to retrieve default setting for projection usage
    * \param [OUT] : returns the current value of device projection enable
    * \enSPIType  : indicates the type of SPI technology. e8DEV_TYPE_UNKNOWN indicates default value for any SPI technology
    * \retval t_Void
       **************************************************************************/
   t_Void vGetDefaultProjUsageSettings(tenDeviceCategory enSPIType, tenEnabledInfo &enEnabledInfo);
   /***************************************************************************
    ** FUNCTION:  t_String spi_tclConfigReaderBase::szGetVehicleId()
    ***************************************************************************/
   /*!
    * \fn      szGetVehicleId()
    * \brief   Method to get VehicleId information
    * \retval  t_String
    **************************************************************************/
   t_String szGetVehicleId();

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclConfigReaderBase::vClearVehicleId()
    ***************************************************************************/
   /*!
    * \fn      szGetVehicleId()
    * \brief   Method to set VehicleId information
    * \retval  t_Void
    **************************************************************************/
   t_Void vClearVehicleId();


   /*!
   * \brief   Generic Singleton class
   */
   friend class GenericSingleton<spi_tclConfigReader>;


   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclConfigReader::spi_tclConfigReader()
   ***************************************************************************/
   /*!
   * \fn      spi_tclConfigReader()
   * \brief   Default Constructor
   * \sa      ~spi_tclConfigReader()
   **************************************************************************/
   spi_tclConfigReader();

   /***************************************************************************
   ** FUNCTION:  spi_tclConfigReader& spi_tclConfigReader::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclConfigReader& operator= (const spi_tclConfigReader &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclConfigReader& operator= (const spi_tclConfigReader &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclConfigReader::spi_tclConfigReader(const ..
   ***************************************************************************/
   /*!
   * \fn      spi_tclConfigReader(const spi_tclConfigReader &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclConfigReader(const spi_tclConfigReader &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclConfigReader::vReadScreenConfigs()
   ***************************************************************************/
   /*!
   * \fn      t_Void vReadScreenConfigs()
   * \brief   Method to read screen attributes from EOL
   * \param   NONE
   * \retval  t_Void
   **************************************************************************/
   t_Void vReadScreenConfigs();

	//! Drive mode information
	tenVehicleConfiguration m_enDriveModeInfo;

	//! Light information
	tenVehicleConfiguration m_enNightModeInfo;
	
	Lock m_oLock;
   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //spi_tclConfigReader

#endif //_SPI_TCLCONFIGREADER_H_


///////////////////////////////////////////////////////////////////////////////
// <EOF>


