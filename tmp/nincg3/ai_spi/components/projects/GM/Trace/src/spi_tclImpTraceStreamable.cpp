/***********************************************************************/
/*!
* \file  devprj_tclImpTraceStreamable.cpp
* \brief Generic message Sender
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Message sender
AUTHOR:         Vinoop
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
17.02.2014  |  Vinoop      			| Added 1.bDiPODeviceConnection
                                      2.bDiPODeviceSelectResult

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_TCLSERVICE
#include "trcGenProj/Header/spi_tclImpTraceStreamable.cpp.trc.h"
#endif
#endif

#define GENERICMSGS_S_IMPORT_INTERFACE_GENERIC
#include "generic_msgs_if.h"

//!Include public FI interface of this service
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_FUNCTIONIDS
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_ERRORCODES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_SERVICEINFO
#include "most_fi_if.h"

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/
#include "spi_tclImpTraceStreamable.h"
#include "TraceStreamable.h"


/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/

#define CONVERT_32(u8_data1,u8_data2,u8_data3,u8_data4) ((u8_data1) | (u8_data2 << 8) | (u8_data3 << 16) | (u8_data4 << 24))
//#define CONVERT_16(u8_data1,u8_data2) ((u8_data1) | (u8_data2 << 8)) - currently unused
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/*!
* \typedef TraceCmdFunctor<spi_tclImpTraceStreamable> spi_tCommand
* \brief Trace Command object definition.
*/
typedef TraceCmdFunctor<spi_tclImpTraceStreamable> spi_tCommand;
/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| function implementation (scope: external-interfaces)
|----------------------------------------------------------------------------*/

/******************************************************************************
** FUNCTION:  spi_tclImpTraceStreamable::spi_tclImpTraceStreamable(ahl_.
******************************************************************************/

/*explicit*/
spi_tclImpTraceStreamable::spi_tclImpTraceStreamable(
   ahl_tclBaseOneThreadApp * const cpoApp) :
TraceStreamable(cpoApp)
{
   //add code
}

/******************************************************************************
** FUNCTION:  virtual spi_tclImpTraceStreamable::~dev_tclImpTraceStrea..
******************************************************************************/

/*virtual*/
spi_tclImpTraceStreamable::~spi_tclImpTraceStreamable()
{
   //add code
}

/******************************************************************************
** FUNCTION:  virtual tVoid spi_tclImpTraceStreamable::vSetupCmds()
******************************************************************************/

/*virtual*/
tVoid spi_tclImpTraceStreamable::vSetupCmds()
{
   // Add the command
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::vSetupCmds"));
   vAddCmd(MOST_DEVPRJFI_C_U16_LAUNCHAPP, new spi_tCommand(this,
      &spi_tclImpTraceStreamable::bLaunchApp));

   vAddCmd(MOST_DEVPRJFI_C_U16_TERMINATEAPP, new spi_tCommand(this,
      &spi_tclImpTraceStreamable::bTerminateApp));

   vAddCmd(MOST_DEVPRJFI_C_U16_SELECTDEVICE, new spi_tCommand(this,
      &spi_tclImpTraceStreamable::bSelectDevice));

} // tVoid spi_tclImpTraceStreamable::vSetupCmds()

/**************************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bLaunchApp(tU8 const* const cp..
***************************************************************************************/
tBool spi_tclImpTraceStreamable::bLaunchApp(tU8 const* const cpu8Buffer)const
{
   ETG_TRACE_USR4(("spi_tclImpTraceStreamable::bLaunchApp"));
   most_devprjfi_tclMsgLaunchAppMethodStart oLaunchApp;

   if(NULL != cpu8Buffer )
   {
      tU8 u8NumValidBytes = cpu8Buffer[0];
      //Launch App command should be recieved with 14bytes of data and 10 valid Bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - Device ID - 4 Bytes
      // cpu8Buffer[7] to cpu8Buffer[10] - App ID - 4 Bytes
    if ( 10 <= u8NumValidBytes)
     {
      oLaunchApp.u32DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));
      oLaunchApp.u32ApplicationHandle = CONVERT_32((cpu8Buffer[7]),(cpu8Buffer[8]),(cpu8Buffer[9]),(cpu8Buffer[10]));
      ETG_TRACE_USR4(("spi_tclImpTraceStreamable::bLaunchApp:Device- %x App- %x", oLaunchApp.u32DeviceHandle,oLaunchApp.u32ApplicationHandle));
    }

   }
      return (bSendMsg(oLaunchApp));
}


/**************************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetAnimStatus(tU8 const* const cp..
***************************************************************************************/
tBool spi_tclImpTraceStreamable::bSelectDevice(tU8 const* const cpu8Buffer)const
{
   ETG_TRACE_USR4(("spi_tclImpTraceStreamable::bSelectDevice - %d ",cpu8Buffer[0]));
   most_devprjfi_tclMsgSelectDeviceMethodStart oSelectDevice;

   if(NULL != cpu8Buffer)
   {
      tU8 u8NumValidBytes = cpu8Buffer[0];
      //bSelectDevice command should be recieved with 14bytes of data and 8 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - Device ID - 4 Bytes
      // cpu8Buffer[7] - Connection type - 1 Byte
      // cpu8Buffer[8] - Connection Req- 1 Byte.
      if(6 <= u8NumValidBytes )
      {
         oSelectDevice.u32DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));
         ETG_TRACE_USR4(("spi_tclImpTraceStreamable::bSelectDevice:Device- %x ", oSelectDevice.u32DeviceHandle));
      }
   }
   return (bSendMsg(oSelectDevice));
}

/**************************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bTerminateApp(tU8 const* const cp..
***************************************************************************************/
tBool spi_tclImpTraceStreamable::bTerminateApp(tU8 const* const cpu8Buffer)const
{
   most_devprjfi_tclMsgTerminateAppMethodStart oTerminateApp;

   if(NULL!=cpu8Buffer)
   {
      tU8 u8NumValidBytes = cpu8Buffer[0];
      if (10 <= u8NumValidBytes)
      {
         oTerminateApp.u32DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));
         oTerminateApp.u32ApplicationHandle =  CONVERT_32((cpu8Buffer[7]),(cpu8Buffer[8]),(cpu8Buffer[9]),(cpu8Buffer[10]));
         ETG_TRACE_USR4(("spi_tclImpTraceStreamable::bTerminateApp:Device- %x App- %x", oTerminateApp.u32DeviceHandle,oTerminateApp.u32ApplicationHandle));
      } //if (10 <= u8NumValidBytes)
   } //if(NULL != cpu8Buffer)
   return (bSendMsg(oTerminateApp));

}






