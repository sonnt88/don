/***********************************************************************/
/*!
* \file  dev_tclImpTraceStreamable.h
* \brief Generic message Sender
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Message sender
AUTHOR:         Vinoop
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
12.11.2013  | Vinoop      | Initial Version

\endverbatim
*************************************************************************/
#ifndef SPI_TCLIMPTRACESTREAMABLE_H_
#define SPI_TCLIMPTRACESTREAMABLE_H_

/******************************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------------------*/
#include "TraceStreamable.h"
#include "SPITypes.h"
using namespace spi::spitrace;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
//!Include Application Help Library.
//!Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

//!Include common fi interface
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#define MOST_FI_S_IMPORT_INTERFACE_MOST_SYSTAFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_SYSTAFI_FUNCTIONIDS
#include <most_fi_if.h>

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| forward declarations (scope: global)
|----------------------------------------------------------------------------*/

class ahl_tclBaseOneThreadApp;
//class spi_tclImpTraceStreamable;
/*!
* \class spi_tclImpTraceStreamable
* \brief Implementation of the Trace Streamer
* Trace commands are setup using this class.
*/

class spi_tclImpTraceStreamable: public TraceStreamable {
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /**************************************************************************************
   ** FUNCTION:  tVoid spi_tclImpTraceStreamable::ConvertU8toU32(tU32* cpu32Buffer,tU8* cpu8Buffer)
   ** /*!
   * \brief   Convert a U8 buffer type to U32 type .
   *
   * \param   cpu32Buffer: U32 buffer
   * \param   cpu8Buffer:  U8 buffer for concatenation .
   * \retval  NONE
   ***************************************************************************************/
   tVoid ConvertU8toU32(tU32 cpu32Buffer,tU8* cpu8Buffer);

   /**************************************************************************************
   ** FUNCTION:  tVoid spi_tclImpTraceStreamable::ConvertU8toU16(tU16 cpu16Buffer,tU8* cpu8Buffer)
   ** ** /*!
   * \brief   Convert a U8 buffer type to U16 type .
   *
   * \param   cpu32Buffer: U16 buffer
   * \param   cpu8Buffer:  U8 buffer for concatenation .
   * \retval  NONE
   ***************************************************************************************/
   tVoid ConvertU8toU16(tU16 cpu16Buffer,tU8* cpu8Buffer);

   /***************************************************************************
   ** FUNCTION:  spi_tclImpTraceStreamable::spi_tclImpTraceStreamable(ahl_.
   ***************************************************************************/
   /*!
   * \brief   Parameterized Constructor, based on Dependency Injection
   *          Principle (DIP)
   * \param   [cpoApp]:        (->I) Pointer to the main application
   * \retval  NONE
   **************************************************************************/
   explicit spi_tclImpTraceStreamable(ahl_tclBaseOneThreadApp* const cpoApp);

   /***************************************************************************
   ** FUNCTION:  virtual spi_tclImpTraceStreamable::~spi_tclImpTraceStrea..
   ***************************************************************************/
   /*!
   * \brief   Destructor
   * \param   NONE
   * \retval  NONE
   **************************************************************************/
   virtual ~spi_tclImpTraceStreamable();

   /***************************************************************************
   ** FUNCTION:  virtual tVoid spi_tclImpTraceStreamable::vSetupCmds()
   ***************************************************************************/
   /*!
   * \brief   The function sets up the commands supported via trace.
   *          This is used for mapping trace commands provided via the Trace
   *          input channel interface.
   * \param   NONE
   * \retval  NONE
   **************************************************************************/
   virtual tVoid vSetupCmds();

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/
protected:

   /***************************************************************************
   *******************************PROTECTED************************************
   ***************************************************************************/

   /*********************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bLaunchApp(tU8 const* const cp..
   *********************************************************************************/
   /*!
   * \brief   Launches the application - Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if Play cue command was successfully executed,
   *          FALSE otherwise.
   **************************************************************************/
   tBool bLaunchApp(tU8 const* const cpu8Buffer) const;

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bTerminateApp(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief   Terminates the application - Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if Stop cue command was successfully executed,
   *          FALSE otherwise.
   **************************************************************************/
   tBool bTerminateApp(tU8 const* const cpu8Buffer) const;

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclImpTraceStreamable::bSelectDevice(tU8 const* const cp..
   ***************************************************************************/
   /*!
   * \brief   Select the device - Trace command executor
   *          The function converts the trace commands to Loopback message,
   *          which are posted back to the component to be processed using the
   *          normal CCA interface.
   * \param   [cpu8Buffer]:  (->I) Input trace channel buffer.
   * \retval  [tBool]: TRUE if Stop cue command was successfully executed,
   *          FALSE otherwise.
   **************************************************************************/
   tBool bSelectDevice(tU8 const* const cpu8Buffer) const;

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ****************************END OF PRIVATE**********************************
   ***************************************************************************/
}; // class spi_tclImpTraceStreamable : public spi_tclImpTraceStreamable
//typedef devprj_tclImpTraceStreamable spi_tclImpTraceStreamable ;


#endif /* DEV_TCLIMPTRACESTREAMABLE_H_ */
