/*!
*******************************************************************************
* \file              devprj_tclMainApp.h
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Device Projection main application class
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                       | Modifications
16.10.2013 |							   | Initial Version
06.11.2013 |  Ramya Murthy			       | Modified for Device Projection class
03.01.2014 |  Hari Priya E R(RBEI/ECP2)    | Included changes for handling 
                                             device selection and launch application
12.02.2014 |  Hari Priya E R(RBEI/ECP2)    | Included changes for EOL handling 
06.04.2014 |  Ramya Murthy                 | Initialisation sequence implementation
18.05.2014 |  Ramya Murthy                 | DayNight mode implementation 
12.03.2015 |  Ramkumar Muniraj             | Added BTSTL feature switch so that it
											 can also be used in non BTSTL platform.
											 Refer ticket CMG3GB-1715.
Note: Initial version taken from SPI Main application class 
\endverbatim
******************************************************************************/

#ifndef _DEVPRJ_MAINAPP_H_
#define _DEVPRJ_MAINAPP_H_

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"
#include <signal.h>
#include "TraceStreamable.h"
using namespace spi::spitrace;

#include "spi_tclTrace.h"

/******************************************************************************
| defines
|----------------------------------------------------------------------------*/
#define RELEASE_MEM_OSAL(VAR)      \
   if (OSAL_NULL != VAR)           \
   {                               \
      OSAL_DELETE VAR;             \
      VAR = OSAL_NULL;             \
   }

//! Forward declarations
#ifdef SYSTEM_S_USE_BPSTL
extern tBool exh_bInitExceptionHandling();
#endif
class devprj_tclService;
class spi_tclDefsetHandler;

/*!
* \class definition
*
*/
class devprj_tclMainApp : public ahl_tclBaseOneThreadApp
{
public:

   /***************************************************************************
   ** FUNCTION:  devprj_tclMainApp::devprj_tclMainApp()
   ***************************************************************************/
   /*!
   * \fn      devprj_tclMainApp()
   * \brief   Constructor
   **************************************************************************/
   devprj_tclMainApp();

   /***************************************************************************
   ** FUNCTION   : virtual devprj_tclMainApp::~devprj_tclMainApp()
   ***************************************************************************/
   /* !
   * \fn	   ~devprj_tclMainApp()
   * \brief    Destructor.
   Invalidate static self reference.
   * \param    None.
   **************************************************************************/
   virtual ~devprj_tclMainApp();

   /***************************************************************************
   ** FUNCTION   : tBool devprj_tclMainApp::bOnInit()
   ***************************************************************************/
   /* !
   * \fn       bOnInit()
   * \brief    This method is called by the CCA framework before starting any
   *           CCA message related communication action. As default this
   *           method always returns TRUE. The user has to implement all his
   *           local application initialization (create client and service
   *           handler, create worker threads, allocate application memory,...).
   *           If returns TRUE, initialization was successfully performed
   *           and framework will register application, else, a reset of the
   *           system is forced immediately.
   *           This method is the counterpart of vOnApplicationClose().
   * \param    None.
   * \retval   TRUE  = Application successfully initialized.
   *           FALSE = Application NOT successfully initialized.
   ***************************************************************************
   * Overrides method ahl_tclBaseOneThreadApp::bOnInit().
   **************************************************************************/
   virtual tBool bOnInit();

   /**************************************************************************
   ** FUNCTION   : tVoid devprj_tclMainApp::vOnApplicationClose()
   **************************************************************************/
   /* !
   * !
   * \fn       vOnApplicationClose()
   * \brief    This method is called by the CCA framework to indicate the
   *           imminent shutdown of the application. The user has to implement
   *           the de-initialization of all his local application data (destroy
   *           client and service handler, destroy worker threads, free
   *           application memory, ... ). After this method call the
   *           destructor of this application will be called.
   *           This method is the counterpart of bOnInit().
   * \param    None.
   * \retval   None.
   ***************************************************************************
   * Overrides method ahl_tclBaseOneThreadApp::vOnApplicationClose().
   **************************************************************************/
   virtual tVoid vOnApplicationClose();

   /**************************************************************************
   ** FUNCTION   : tVoid devprj_tclMainApp::vOnLoadSettings()
   **************************************************************************/
   /* !
   * !
   * \fn        vOnLoadSettings()
   * \brief     This method is called by the CCA framework to trigger the
   loading of last mode settings. The user has to load the
   previously stored last mode settings from persistent memory.
   * \param     None.
   * \retval    None.
   ***************************************************************************
   * Overrides method ahl_tclBaseOneThreadApp::vOnLoadSettings().
   **************************************************************************/
   virtual tVoid vOnLoadSettings();

   /**************************************************************************
   ** FUNCTION   : tVoid devprj_tclMainApp::vOnSaveSettings()
   **************************************************************************/
   /* !
   * \fn        vOnSaveSettings()
   * \brief     This method is called by the CCA framework to trigger the
   *            storage of last mode settings. The user has to store the last
   *            mode settings to persistent memory.
   * \param     None.
   * \retval    None.
   ***************************************************************************
   * Overrides method ahl_tclBaseOneThreadApp::vOnSaveSettings().
   **************************************************************************/
   virtual tVoid vOnSaveSettings();

   /**************************************************************************
   ** FUNCTION   : tVoid devprj_tclMainApp::vOnTimer(tU16 nTimerId)
   **************************************************************************/
   /* !
   * \fn        vOnTimer(tU16 u16TimerId)
   * \brief     This method is called by the CCA framework on the expiration
   *            of a previously started timer via method bStartTimer(). The
   *            expired timer is forwarded to the respective service or
   *            client handlers via a call of vProcessTimer().
   *            The method is called from this applications context and
   *            therefore no interthread programming rules must be considered
   *            and the application methods and/or member variables can be
   *            accessed without using the static self reference
   *            'm_poMainAppInstance'.
   * \param     [IN] u16TimerId = Identifier of the expired timer.
   * \retval    None.
   ***************************************************************************
   * Overrides method ahl_tclBaseOneThreadApp::vOnTimer().
   **************************************************************************/
   virtual tVoid vOnTimer(tU16 nTimerId);

   /**************************************************************************
   ** FUNCTION   : tVoid devprj_tclMainApp::vOnEvent(OSAL_tEventMask nEvent)
   **************************************************************************/
   /* !
   * \fn        vOnEvent(OSAL_tEventMask nEvent)
   * \brief     This method is called by the CCA framework when an event is
   *            signaled on Main application.
   * \param     [IN] nEvent = Identifier of the signaled event
   * \retval    None.
   ***************************************************************************
   * Overrides method ahl_tclBaseOneThreadApp::vOnEvent().
   **************************************************************************/
   virtual tVoid vOnEvent(OSAL_tEventMask nEvent);

   /**************************************************************************
   * //!TTFIS Trace related
   * ************************************************************************/

   /**************************************************************************
   ** FUNCTION   : virtual devprj_tclMainApp::cb_vDefaultHandlerTraceCmds...
   **************************************************************************/
   /* !
   * \fn        cb_vDefaultHandlerTraceCmds(const tU8* pcu8Data)
   * \brief     Handle manually coded TTFIS commands.
   * \param     [IN] pcu8Data = Pointer to buffer with Trace Command and
   *            command parameters.
   *            pcu8Data[0]    = number of valid bytes in the passed buffer
   *            pcu8Data[1]    = trace command
   *            pcu8Data[2..n] = command parameters 1..k
   * \retval    None.
   **************************************************************************/
   static tVoid cb_vDefaultHandlerTraceCmds(const tU8* pcu8Data);


   /***************************************************************************
   ** FUNCTION   : tVoid devprj_tclMainApp::vOnLoopback(tU16 u16ServiceID,...)
   ***************************************************************************/
   /* !
   * \fn        vOnLoopback(tU16 u16ServiceID, amt_tclServiceData* poMessage)
   * \brief     This function is called by the CCA framework if a message was
   *            sent from one if these applications services or from a
   *            callback-handler of this application (so called self or loopback
   *            messages). The message can be forwarded to the respective service
   *            via a call of vOnLoopbackService(). If there is no service
   *            registered for this message then the user has the option to
   *            directly evaluate the message in the default block of this
   *            methods switch-clause.
   * \param     [IN] u16ServiceID = Identifier of the service from where the
   *            message was sent.
   *            [IN] poMessage = Message object.
   * \retval    None.
   ****************************************************************************
   * Overrides method ahl_tclBaseOneThreadApp::vOnLoopback().
   ***************************************************************************/
   using ahl_tclBaseOneThreadApp::vOnLoopback;
   tVoid vOnLoopback(tU16 u16ServiceID, amt_tclBaseMessage* poMessage);


protected:



private:

   /***************************************************************************
   ** FUNCTION   : virtual devprj_tclMainApp::devprj_tclMainApp& operator= (...
   ***************************************************************************/
   /* !
   * \fn		devprj_tclMainApp& operator= (const devprj_tclMainApp& oDevprj_tclMainApp)
   * \brief 	Assignment Operator, will not be implemented.
   * 			Avoids Lint Prio 3 warning: Info 1732: new in constructor for class
   * 			'devprj_tclMainApp' which has no assignment operator.
   * 			NOTE:
   * 			This is a technique to disable the assignment operator for this class.
   * 			So if an attempt for the assignment is made linker complains.
   ***************************************************************************/
   devprj_tclMainApp& operator=(const devprj_tclMainApp& oDevprj_tclMainApp);

   /***************************************************************************
   ** FUNCTION   : devprj_tclMainApp::devprj_tclMainApp(const devprj_tcl...
   ***************************************************************************/
   /* !
   * \fn		devprj_tclMainApp(const devprj_tclMainApp& oDevprj_tclMainApp)
   * \brief 	Copy Constructor, will not be implemented.
   * 			Avoids Lint Prio 3 warning:Info 1733: new in constructor for class
   * 			'devprj_tclMainApp' which has no copy constructor.
   * 			NOTE:
   * 			This is a technique to disable the copy constructor for this class.
   * 			So if an attempt for the copy constructor is made linker complains.
   **************************************************************************/
   devprj_tclMainApp(const devprj_tclMainApp& oDevprj_tclMainApp);

   /***************************************************************************
   ** FUNCTION   : devprj_tclMainApp::vReadEOLCalibration()
   ***************************************************************************/
   /* !
   * \fn		vReadEOLCalibration(tVoid)
   * \brief 	Method to read the system calibration values
   * \param    None
   * \retval   None.
   **************************************************************************/	
   tVoid vReadEOLCalibration();

   /***************************************************************************
   ** FUNCTION   : t_Void devprj_tclMainApp::vSignalHandler()
   ***************************************************************************/
   /* !
   * \fn       t_Void vSignalHandler(t_S32 s32SigNum, siginfo_t *pSiginfo, t_Void *pContext)
   * \brief    Method to Handle the signals sent to SPI Process/ threads created by our process
   * \param    s32SigNum : [IN] Signal ID/Number
   * \param    pSiginfo  : [IN] Siginfo structure, which provides the details of the signal
   * \param    pContext  : [IN] context ptr
   * \retval   None.
   **************************************************************************/
   static t_Void vSignalHandler(t_S32 s32SigNum, siginfo_t *pSiginfo, t_Void *pContext);

   /**************************************************************************
   ** Services
   **************************************************************************/
   devprj_tclService* m_poDevPrjService;

   /**************************************************************************
   ** DefSet handler
   **************************************************************************/
   spi_tclDefsetHandler* m_poDefSetHandler;
   
   /**************************************************************************
   ** Trace handler
   **************************************************************************/
   static spi_tclTrace* m_poCmdHndlr;

}; 

#endif /* _DEVPRJ_MAINAPP_H_ */

///////////////////////////////////////////////////////////////////////////////
// <EOF>


