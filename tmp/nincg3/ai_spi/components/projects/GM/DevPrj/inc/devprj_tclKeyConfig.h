/***********************************************************************/
/*!
* \file  devprj_tclKeyConfig.h
* \brief Class to handle the key configurations for Device Projection
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to handle the key configurations for Device Projection
AUTHOR:         Hari Priya E R
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
27.09.2014  | Hari Priya E R        | Initial Version
06.11.2014  |  Hari Priya E R       | Added changes to set Client key capabilities

\endverbatim
*************************************************************************/

#ifndef _DEVPRJ_TCLKEYCONFIG_H_
#define _DEVPRJ_TCLKEYCONFIG_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

#include "Timer.h"
#include "SPITypes.h"
#include "devprj_TypeDefs.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/


/****************************************************************************/
/*!
* \class devprj_tclKeyConfig
* \brief Class to handle the key configurations for GM
****************************************************************************/
class spi_tclCmdInterface;
class spi_tclSpeechHMIClient;
class spi_tclTelephoneClient;

class devprj_tclKeyConfig
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/
   
   /***************************************************************************
   ** FUNCTION:  devprj_tclKeyConfig::devprj_tclKeyConfig(
                 spi_tclCmdInterface* poSpiCmdInterface,
                 spi_tclSpeechHMIClient* poSpeechHMI,
				 spi_tclTelephoneClient* poTelephoneClient)
   ***************************************************************************/
   /*!
   * \fn      devprj_tclKeyConfig(spi_tclCmdInterface* poSpiCmdInterface)
   * \brief   Parameterised Constructor
   * \param   poSpiCmdInterface: [IN] Cmd Interface Pointer
   * \param   poSpeechHMI:       [IN] Speech HMI Client pointer
   * \param   poTelephoneClient: [IN] Telephone Client pointer
   * \sa      ~devprj_tclKeyConfig()
   **************************************************************************/
   devprj_tclKeyConfig(spi_tclCmdInterface* poSpiCmdInterface,
      spi_tclSpeechHMIClient* poSpeechHMI,
      spi_tclTelephoneClient* poTelephoneClient);

   /***************************************************************************
   ** FUNCTION:  devprj_tclKeyConfig::~devprj_tclKeyConfig()
   ***************************************************************************/
   /*!
   * \fn      virtual ~devprj_tclKeyConfig()
   * \brief   Destructor
   * \sa      devprj_tclKeyConfig()
   **************************************************************************/
   virtual ~devprj_tclKeyConfig();

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclKeyConfig::vConfigDevPrjKeys(tenKeyMode enKeyMode,
                     tenKeyCode enKeyCode,
                     tBool& bNativeSRTrigger,t_Bool& bSetMute,
                     trUserContext rcUsrCntxt
   ***************************************************************************/
   /*!
   * \fn      vConfigDevPrjKeys(tU32 u32DeviceHandle
                     tenKeyMode enKeyMode,tenKeyCode enKeyCode)
   * \brief   Method to configure the keys for Device Projection
   * \param   enKeyMode :        [IN] Key Mode- Key pressed or released
   * \param   enKeyCode :        [IN] Unique Identifier of the Key
   * \param   bSetMute :         [IN] Flag that indicates that Mute state 
                                  should be set
   * \param  [IN] rcUsrCntxt    : User Context Details
   * \retval  NONE
   **************************************************************************/
   tVoid vConfigDevPrjKeys(tenKeyMode enKeyMode,tenKeyCode enKeyCode,
                     tBool& bSetMute,
                     trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclKeyConfig::vSetSpeechRecStatus(
                 tenSpeechRecStatus enSRStatus)
   ***************************************************************************/
   /*!
   * \fn      vSetSpeechRecStatus(tenSpeechRecStatus enSRStatus)
   * \brief   Method to set the native speech recognition status
   * \param   enSRStatus : [IN] Native SR status
   * \retval  NONE
   **************************************************************************/
   tVoid vSetSpeechRecStatus(tenSpeechRecStatus enSRStatus);

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclKeyConfig::vInsertKeyCodeToMap()
   ***************************************************************************/
   /*!
   * \fn      vInsertKeyCodeToMap()
   * \brief   Method to insert the Device Projection Switch types and 
               the internal Key codes to a map
   * \param   NONE
   * \retval  NONE
   **************************************************************************/
   tVoid vInsertKeyCodeToMap();

   /***************************************************************************
   ** FUNCTION: tenKeyCode devprj_tclKeyConfig::enGetKeyCode
              (devprj_tenSwitch enDevPrjSwitch)const
   ***************************************************************************/
   /*!
   * \fn      enGetKeyCode(devprj_tenSwitch enDevPrjSwitch)const
   * \brief   Method to get the internal Key Code corresponding to
                  the Device Projection key 
   * \param   enDevPrjSwitch: [IN]Device Projection Switch type
   * \retval  tenKeyCode: Internal Key Code
   **************************************************************************/
   tenKeyCode enGetKeyCode(devprj_tenSwitch enDevPrjSwitch)const;   

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclKeyConfig::vSetBTCallStatus(
                 tenCallStatus enCallStatus,tU16 u16CallInstance)
   ***************************************************************************/
   /*!
   * \fn      vSetBTCallStatus( tenCallStatus enCallStatus)
   * \brief   Method to set the BT Call status
   * \param   enCallStatus : [IN] BT Call status
   * \param   u16CallInstance : [IN]Call Instance
   * \retval  NONE
   **************************************************************************/
   tVoid vSetBTCallStatus(const std::vector<trTelCallStatusInfo> coTelCallStatusList);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclKeyConfig::vSetClientCapabilities()
   ***************************************************************************/
   /*!
   * \fn     vSetClientCapabilities()
   * \brief  Method that sets the client capabilities
   * \param  NONE
   * \retVal NONE
   **************************************************************************/
   t_Void vSetClientCapabilities();



   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/
   /***************************************************************************
   ** FUNCTION:  devprj_tclKeyConfig& devprj_tclKeyConfig::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      devprj_tclKeyConfig& operator= (const devprj_tclKeyConfig &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   devprj_tclKeyConfig& operator= (const devprj_tclKeyConfig &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  devprj_tclKeyConfig::devprj_tclKeyConfig(const ..
   ***************************************************************************/
   /*!
   * \fn      devprj_tclKeyConfig(const devprj_tclKeyConfig &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   devprj_tclKeyConfig(const devprj_tclKeyConfig &corfrSrc);


   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclKeyConfig::vConfigCarPlayKeys
                 (tenKeyMode enKeyMode,tenKeyCode enKeyCode,t_Bool& bSetMute)
   ***************************************************************************/
   /*!
   * \fn      vConfigCarPlayKeys(tenKeyMode enKeyMode,tenKeyCode enKeyCode
                               ,t_Bool& bSetMute)
   * \brief   Method to configure the CarPlay keys
   * \param   u32DeviceHandle:   [IN]Device Handle
   * \param   enKeyMode :        [IN] Key Mode- Key pressed or released
   * \param   enKeyCode :        [IN] Unique Identifier of the Key
   * \param   bSetMute :         [IN] Flag that indicates that Mute state 
                                  should be set
   * \param  [IN] rcUsrCntxt    : User Context Details.
   * \retval  NONE
   **************************************************************************/
   tVoid vConfigCarPlayKeys(tenKeyMode enKeyMode,tenKeyCode enKeyCode,
      tBool& bSetMute);

 

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclKeyConfig::vConfigCarPlaySWCKeys
                 (tenKeyMode enKeyMode,tenKeyCode enKeyCode,
                 tBool& bNativeSRTrigger,t_Bool& bSetMute)
   ***************************************************************************/
   /*!
   * \fn      vConfigCarPlaySWCKeys(tenKeyMode enKeyMode,tenKeyCode enKeyCode,
                           tBool& bNativeSRTrigger,t_Bool& bSetMute)
   * \brief   Method to configure the CarPlay SWC keys
   * \param   enKeyMode :        [IN] Key Mode- Key pressed or released
   * \param   enKeyCode :        [IN] Unique Identifier of the Key
   * \param   bSetMute :         [IN] Flag that indicates that Mute state 
                                  should be set
   * \retval  NONE
   **************************************************************************/
   tVoid vConfigCarPlaySWCKeys(tenKeyMode enKeyMode,tenKeyCode enKeyCode,
      tBool& bSetMute);

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclKeyConfig::vConfigMLKeys
                 (tenKeyMode enKeyMode,tenKeyCode enKeyCode,t_Bool& bSetMute)
   ***************************************************************************/
   /*!
   * \fn      vConfigMLKeys(tenKeyMode enKeyMode,tenKeyCode enKeyCode,t_Bool& bSetMute)
   * \brief   Method to configure the ML keys
   * \param   enKeyMode :        [IN] Key Mode- Key pressed or released
   * \param   enKeyCode :        [IN] Unique Identifier of the Key
   * \param   bSetMute :         [IN] Flag that indicates that Mute state 
                                  should be set
   * \retval  NONE
   **************************************************************************/
   tVoid vConfigMLKeys(tenKeyMode enKeyMode,tenKeyCode enKeyCode,
      tBool& bSetMute);

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclKeyConfig::vConfigMLSWCKeys(tenKeyMode enKeyMode,
                          tenKeyCode enKeyCode,
                          tBool& bSetMute)
   ***************************************************************************/
   /*!
   * \fn      vConfigMLSWCKeys(tenKeyMode enKeyMode,
                          tenKeyCode enKeyCode,
                          tBool& bSetMute)
   * \brief   Method to configure the ML SWC keys
   * \param   enKeyMode :        [IN] Key Mode- Key pressed or released
   * \param   enKeyCode :        [IN] Unique Identifier of the Key
   * \param   bSetMute :         [IN] Flag that indicates that Mute state 
                                  should be set
   * \retval  NONE
   **************************************************************************/
   tVoid vConfigMLSWCKeys(tenKeyMode enKeyMode,
                          tenKeyCode enKeyCode,
                          tBool& bSetMute);

    /***************************************************************************
   ** FUNCTION: tBool devprj_tclKeyConfig::bCbLongPressTimeOut(
                    timer_t timerID , tVoid *pObject, 
                         const tVoid *pcoUserData)
   ***************************************************************************/
   /*!
   * \fn     bCbLongPressTimeOut(timer_t timerID , tVoid *pObject, 
                         const tVoid *pcoUserData)
   * \brief  Callback Function that is called upon long press timer expiry
   * \param  timerID : [IN]Timer ID
   * \param  pObject : [IN]Object context for timer callback call
   * \param  pcoUserData:[IN] additional pointer to user supplied data which 
   * \       will be send back via user callback function when the timer expires 
   * \retVal  tBool
   * \sa     
   **************************************************************************/
   static t_Bool bCbLongPressTimeOut(timer_t timerID , tVoid *pObject, 
                         const tVoid *pcoUserData);

   /***************************************************************************
   ** FUNCTION: tBool devprj_tclKeyConfig::bSiriSustainTimerCallback(
                       timer_t timerID , tVoid *pObject, 
                         const tVoid *pcoUserData);
   ***************************************************************************/
   /*!
   * \fn     bSiriSustainTimerCallback(timer_t timerID , tVoid *pObject, 
                         const tVoid *pcoUserData)
   * \brief  Callback function that is called upon Siri sustain timer expiry
   * \param  timerID : [IN]Timer ID
   * \param  pObject : [IN]Object context for timer callback call
   * \param  pcoUserData:[IN] additional pointer to user supplied data which 
   * \       will be send back via user callback function when the timer expires 
   * \retVal  tBool
   * \sa     
   **************************************************************************/
   static t_Bool bSiriSustainTimerCallback(timer_t timerID , tVoid *pObject, 
                         const tVoid *pcoUserData);

    /***************************************************************************
   ** FUNCTION: t_Void devprj_tclKeyConfig::vHandleCarPlayPTTKey(
      tenSpeechAppState enSpeechAppState,
      tenKeyMode enKeyMode,tenKeyCode enKeyCode)
   ***************************************************************************/
   /*!
   * \fn     vHandleCarPlayPTTKey(tenSpeechAppState enSpeechAppState,
              tenKeyMode enKeyMode,tenKeyCode enKeyCode,
               tBool& bNativeSRTrigger)
   * \brief  Method that handles the PTT key events
   * \param  enSpeechAppState : [IN]Speech App State
   * \param  enKeyMode :        [IN] Key Mode- Key pressed or released
   * \param  enKeyCode :        [IN] Unique Identifier of the Key
   * \retVal  NONE
   **************************************************************************/
   t_Void vHandleCarPlayPTTKey(tenSpeechAppState enSpeechAppState,
      tenKeyMode enKeyMode,tenKeyCode enKeyCode);

       /***************************************************************************
   ** FUNCTION: t_Void devprj_tclKeyConfig::vHandleCarPlayENDKey(
      tenSpeechAppState enSpeechAppState,
      tenKeyMode enKeyMode,tenKeyCode enKeyCode,t_Bool& bSetMute)
   ***************************************************************************/
   /*!
   * \fn     vHandleCarPlayENDKey(tenSpeechAppState enSpeechAppState,
              tenKeyMode enKeyMode,tenKeyCode enKeyCode,
               tBool& bNativeSRTrigger,t_Bool& bSetMute)
   * \brief  Method that handles the END key events
   * \param  enSpeechAppState : [IN]Speech App State
   * \param  enKeyMode :        [IN] Key Mode- Key pressed or released
   * \param  enKeyCode :        [IN] Unique Identifier of the Key
   * \param   bSetMute :        [IN] Flag that indicates that Mute state 
                                  should be set
   * \retVal  NONE
   **************************************************************************/
   t_Void vHandleCarPlayENDKey(tenSpeechAppState enSpeechAppState,
      tenKeyMode enKeyMode,tenKeyCode enKeyCode,
      tBool& bSetMute);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclKeyConfig::vHandleMLPTTKey(
      tenKeyMode enKeyMode,tenKeyCode enKeyCode)
   ***************************************************************************/
   /*!
   * \fn     vHandleMLPTTKey(tenKeyMode enKeyMode,tenKeyCode enKeyCode)
   * \brief  Method that handles the ML PTT key events
   * \param  enKeyMode :        [IN] Key Mode- Key pressed or released
   * \param  enKeyCode :        [IN] Unique Identifier of the Key
   * \retVal  NONE
   **************************************************************************/
   t_Void vHandleMLPTTKey(tenKeyMode enKeyMode,tenKeyCode enKeyCode);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclKeyConfig::vHandleMLENDKey(
      tenKeyMode enKeyMode,tenKeyCode enKeyCode,tBool& bSetMute)
   ***************************************************************************/
   /*!
   * \fn     vHandleMLENDKey(tenKeyMode enKeyMode,tenKeyCode enKeyCode)
   * \brief  Method that handles the ML PTT key events
   * \param  enKeyMode :        [IN] Key Mode- Key pressed or released
   * \param  enKeyCode :        [IN] Unique Identifier of the Key
   * \param   bSetMute :        [IN] Flag that indicates that Mute state 
                                  should be set
   * \retVal  NONE
   **************************************************************************/
   t_Void vHandleMLENDKey(tenKeyMode enKeyMode,tenKeyCode enKeyCode,tBool& bSetMute);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclKeyConfig::vHandleBTCallOnKey()
   ***************************************************************************/
   /*!
   * \fn     vHandleBTCallOnKey(tenBTCallAction enBTCallAction = e8_BT_CALLEND)
   * \brief  Method that handles the ML PTT & END key events
   * \param  bCallAccept :        [IN] Whether to accept call or not.
   * \retVal NONE
   **************************************************************************/
   t_Void vHandleBTCallOnKey (tenBTCallAction enBTCallAction = e8_BT_CALLEND);

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclKeyConfig::vConfigAAPKeys
                 (tenKeyMode enKeyMode,tenKeyCode enKeyCode,t_Bool& bSetMute)
   ***************************************************************************/
   /*!
   * \fn      vConfigMLKeys(tenKeyMode enKeyMode,tenKeyCode enKeyCode,t_Bool& bSetMute)
   * \brief   Method to configure the ML keys
   * \param   enKeyMode :        [IN] Key Mode- Key pressed or released
   * \param   enKeyCode :        [IN] Unique Identifier of the Key
   * \param   bSetMute :         [IN] Flag that indicates that Mute state
                                  should be set
   * \retval  NONE
   **************************************************************************/
   tVoid vConfigAAPKeys(tenKeyMode enKeyMode,tenKeyCode enKeyCode,
      tBool& bSetMute);

   /***************************************************************************
    ** FUNCTION: tVoid devprj_tclKeyConfig::vConfigAAPSWCKeys(tenKeyMode enKeyMode,
    tenKeyCode enKeyCode,
    tBool& bSetMute)
    ***************************************************************************/
   /*!
    * \fn      vConfigMLSWCKeys(tenKeyMode enKeyMode,
    tenKeyCode enKeyCode,
    tBool& bSetMute)
    * \brief   Method to configure the ML SWC keys
    * \param   enKeyMode :        [IN] Key Mode- Key pressed or released
    * \param   enKeyCode :        [IN] Unique Identifier of the Key
    * \param   bSetMute :         [IN] Flag that indicates that Mute state
    should be set
    * \retval  NONE
    **************************************************************************/
   tVoid vConfigAAPSWCKeys(tenKeyMode enKeyMode, tenKeyCode enKeyCode,
         tBool& bSetMute);
		 
   /***************************************************************************
    ** FUNCTION: t_Void devprj_tclKeyConfig::vHandleAAPPTTKey(
    tenKeyMode enKeyMode,tenKeyCode enKeyCode)
    ***************************************************************************/
   /*!
    * \fn     vHandleMLPTTKey(tenKeyMode enKeyMode,tenKeyCode enKeyCode)
    * \brief  Method that handles the ML PTT key events
    * \param  enSpeechAppState:  [IN] Phone Speech App state
    * \param  enKeyMode :        [IN] Key Mode- Key pressed or released
    * \param  enKeyCode :        [IN] Unique Identifier of the Key
    * \retVal  NONE
    **************************************************************************/
   t_Void vHandleAAPPTTKey(tenSpeechAppState enSpeechAppState, tenKeyMode enKeyMode,
         tenKeyCode enKeyCode);

   /***************************************************************************
    ** FUNCTION: t_Void devprj_tclKeyConfig::vHandleAAPENDKey(
    tenKeyMode enKeyMode,tenKeyCode enKeyCode,tBool& bSetMute)
    ***************************************************************************/
   /*!
    * \fn     vHandleMLENDKey(tenKeyMode enKeyMode,tenKeyCode enKeyCode)
    * \brief  Method that handles the ML PTT key events
    * \param  enSpeechAppState:  [IN] Phone Speech App state
    * \param  enKeyMode :        [IN] Key Mode- Key pressed or released
    * \param  enKeyCode :        [IN] Unique Identifier of the Key
    * \param   bSetMute :        [IN] Flag that indicates that Mute state
    should be set
    * \retVal  NONE
    **************************************************************************/
   t_Void vHandleAAPENDKey(tenSpeechAppState enSpeechAppState, tenKeyMode enKeyMode,
         tenKeyCode enKeyCode, tBool& bSetMute);

   //!Map of key codes
   std::map<devprj_tenSwitch,tenKeyCode> m_mapKeyCode;

   //!Device Handle
   static tU32 m_u32DeviceHandle;

   //! Flag to keep track of the timer expiry 
   static tBool m_bIsTimerExpired;

   //! Cmd Interface Pointer
   static spi_tclCmdInterface* m_poSpiCmdInterface;

   //! Speech HMI Client  Pointer
   static spi_tclSpeechHMIClient* m_poSpeechHMIClient;

   //! Telephone Client  Pointer
   spi_tclTelephoneClient* m_poTelephoneClient;

   //! Native SR Status	
   static tenSpeechRecStatus m_enSRStatus;

   //! Call Instanaces List
   std::vector<trTelCallStatusInfo> m_vTelCallStatusList;

   //! BT Activity Status
   tenBTActivityStatus m_enBtActivityStatus;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //devprj_tclKeyConfig

#endif //_DEVPRJ_TCLKEYCONFIG_H_


///////////////////////////////////////////////////////////////////////////////
// <EOF>


