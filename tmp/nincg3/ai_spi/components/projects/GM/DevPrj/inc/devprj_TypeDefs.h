/*!
 *******************************************************************************
 * \file              devprj_TypeDefs.h
 * \brief             Device Projection service type defines
 *******************************************************************************
 \verbatim
 PROJECT:        G3G
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Device Projection service type defines
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 11.11.2013 |  Ramya Murthy			       | Initial Version
 02.07.2014 |  Shihabudheen P M            | Modified for IIl extension integration

 \endverbatim
 ******************************************************************************/

#ifndef _DEVPRJ_TYPEDEFINES_H_
#define _DEVPRJ_TYPEDEFINES_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
//!Include public FI interface of this service

#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#define MOST_FI_S_IMPORT_INTERFACE_FI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_AVMANFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_AVMANFI_FUNCTIONIDS
#define MOST_FI_S_IMPORT_INTERFACE_MOST_AVMANFI_ERRORCODES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_AVMANFI_SERVICEINFO
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_FUNCTIONIDS
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_ERRORCODES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_SERVICEINFO
#include "most_fi_if.h"

#include "XFiObjHandler.h"
using namespace shl::msgHandler;


/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

// ! Type defines for enums 
typedef most_fi_tcl_e8_DevPrjDeviceType            devprj_tFiDevType;
typedef most_fi_tcl_e8_DevPrjDeviceType::tenType   devprj_tenDevType;

typedef most_fi_tcl_e8_DevPrjDeviceInterfaceType            devprj_tFiDevIntfType;
typedef most_fi_tcl_e8_DevPrjDeviceInterfaceType::tenType   devprj_tenDevIntfType;

typedef most_fi_tcl_e8_DevPrjRequestedDisplayContext           devprj_tFiReqDispCntxt;
typedef most_fi_tcl_e8_DevPrjRequestedDisplayContext::tenType  devprj_tenReqDispCntxt;

typedef most_fi_tcl_e8_DevPrjDigitalIPodOutApplication            devprj_tFiDipoApp;
typedef most_fi_tcl_e8_DevPrjDigitalIPodOutApplication::tenType   devprj_tenDipoApp;

typedef most_fi_tcl_e8_DevPrjEchoCancellationNoiseReductionSetting            devprj_tFiEcnrSetting;
typedef most_fi_tcl_e8_DevPrjEchoCancellationNoiseReductionSetting::tenType   devprj_tenEcnrSetting;

typedef most_fi_tcl_e8_DevPrjLaunchResult             devprj_tFiLaunchResult;
typedef most_fi_tcl_e8_DevPrjLaunchResult::tenType    devprj_tenLaunchResult;

typedef most_fi_tcl_e8_DevPrjApplicationCertificateType           devprj_tFiAppCertType;
typedef most_fi_tcl_e8_DevPrjApplicationCertificateType::tenType  devprj_tenAppCertType;

typedef most_fi_tcl_e8_DevPrjSwitchEnumeration           devprj_tFiSwitch;
typedef most_fi_tcl_e8_DevPrjSwitchEnumeration::tenType  devprj_tenSwitch;

typedef most_fi_tcl_e8_DevPrjSwitchEventTypeEnumeration           devprj_tFiSwitchEventType;
typedef most_fi_tcl_e8_DevPrjSwitchEventTypeEnumeration::tenType  devprj_tenSwitchEventType;

typedef most_fi_tcl_e8_DevPrjButtonAction             devprj_tFiBtnAction;
typedef most_fi_tcl_e8_DevPrjButtonAction::tenType    devprj_tenBtnAction;

typedef most_fi_tcl_e8_DevPrjRequestedDisplayContext             devprj_tFiPrjctdDispCntxt;
typedef most_fi_tcl_e8_DevPrjRequestedDisplayContext::tenType    devprj_tenPrjctdDispCntxt;

typedef most_fi_tcl_e8_DevPrjBTChangeInfo                   devprj_tFiBTChangeInfo;
typedef most_fi_tcl_e8_DevPrjBTChangeInfo::tenType          devprj_tenBTChangeInfo;

typedef most_fi_tcl_e8_DevPrjSpeechAppState::tenType        devprj_tenSpeechAppState;
typedef most_fi_tcl_e8_DevPrjPhoneAppState::tenType         devprj_tenPhoneAppState;
typedef most_fi_tcl_e8_DevPrjNavigationAppState::tenType    devprj_tenNavAppState;

// ! Type defines for structures, containers, etc.
typedef most_fi_tcl_DevPrjDeviceList                  devprj_tFiDevLst;
typedef most_fi_tcl_DevPrjDeviceListItem              devprj_tFiDevLstItem;
typedef most_fi_tcl_DevPrjApplicationList             devprj_tFiAppLst;
typedef most_fi_tcl_DevPrjApplicationListItem         devprj_tFiAppLstItem;
typedef most_fi_tcl_DevPrjDeviceUserParametersItem    devprj_tFiDevUserParamItem;

typedef most_fi_tcl_e8_DevPrjButtonAction       devprj_tFiBtnAction;

#define DEVPRJ_ACTION_1  (devprj_tFiBtnAction::FI_EN_E8DEVICE_PROJECTION_APPLICATION_ACTION_1)
#define DEVPRJ_ACTION_2  (devprj_tFiBtnAction::FI_EN_E8DEVICE_PROJECTION_APPLICATION_ACTION_2)
#define DEVPRJ_ACTION_3  (devprj_tFiBtnAction::FI_EN_E8DEVICE_PROJECTION_APPLICATION_ACTION_3)


// ! Type define for DevPrj Fi types message base
typedef most_devprjfi_tclMsgBaseMessage      devprj_FiMsgBase;

// ! Type defines for XFiObjHandler Msg Response types
typedef XFiObjHandler<most_devprjfi_tclMsgSourceInfoGet>                                  devprj_tXFiGetAudSrcInfo;

typedef XFiObjHandler<most_devprjfi_tclMsgDeviceNotificationsEnabledPureSet>              devprj_tXFiPSDevNotifEnabled;
typedef XFiObjHandler<most_devprjfi_tclMsgDriveModePureSet>                               devprj_tXFiPSDriveMode;
typedef XFiObjHandler<most_devprjfi_tclMsgMirrorLinkEnablePureSet>                        devprj_tXFiPSMLEnable;
typedef XFiObjHandler<most_devprjfi_tclMsgDIPOEnablePureSet>                              devprj_tXFiPSDipoEnable;
typedef XFiObjHandler<most_devprjfi_tclMsgGoogleAutoLinkEnablePureSet>                    devprj_tXFiPSGoogleAutoEnable;
typedef XFiObjHandler<most_devprjfi_tclMsgProjectionDeviceAuthAndAccessibilityPureSet>    devprj_tXFiPSProjDevAuthAndAccessibility;

typedef XFiObjHandler<most_devprjfi_tclMsgChangeDeviceProjectionEnableMethodStart>        devprj_tXFiMSChangeDevPrjEnable;
typedef XFiObjHandler<most_devprjfi_tclMsgLaunchAppMethodStart>                           devprj_tXFiMSLaunchApp;
typedef XFiObjHandler<most_devprjfi_tclMsgTerminateAppMethodStart>                        devprj_tXFiMSTerminateApp;
typedef XFiObjHandler<most_devprjfi_tclMsgGetApplicationIconMethodStart>                  devprj_tXFiMSGetAppIcon;
typedef XFiObjHandler<most_devprjfi_tclMsgSelectDeviceMethodStart>                        devprj_tXFiMSSelectDev;
typedef XFiObjHandler<most_devprjfi_tclMsgDispatchSwitchEventMethodStart>                 devprj_tXFiMSDispatchSwitchEv;
typedef XFiObjHandler<most_devprjfi_tclMsgDispatchMenuEncoderChangeMethodStart>           devprj_tXFiMSDispatchMenuEncChange;
typedef XFiObjHandler<most_devprjfi_tclMsgGetApplicationImageMethodStart>                 devprj_tXFiMSGetAppImage;
typedef XFiObjHandler<most_devprjfi_tclMsgDeviceProjectionAlertButtonEventMethodStart>    devprj_tXFiMSDevPrjAlertBtnEv;
typedef XFiObjHandler<most_devprjfi_tclMsgRequestAccessoryDisplayContext_MethodStart>     devprj_tXFiMSReqAccDispCtxt;
typedef XFiObjHandler<most_devprjfi_tclMsgDiPORoleSwitchRequiredMethodStart>              devprj_tXFiMSRoleSwitchReq;
typedef XFiObjHandler<most_devprjfi_tclMsgInvokeBluetoothDeviceActionMethodStart>         devprj_tXFiMSInvokeBTDevAction;
typedef XFiObjHandler<most_devprjfi_tclMsgStopMediaPlaybackMethodStart>                   devprj_tXFiMSStopMediaPb;
typedef XFiObjHandler<most_devprjfi_tclMsgResumeMediaPlaybackMethodStart>                 devprj_tXFiMSResumeMediaPb;

// ! Type defines for DevPrj Msg Request types
typedef most_devprjfi_tclMsgChangeDeviceProjectionEnableMethodResult    devprj_tMRChangeDevPrjEnable;
typedef most_devprjfi_tclMsgLaunchAppMethodResult                       devprj_tMRLaunchApp;
typedef most_devprjfi_tclMsgGetApplicationIconMethodResult              devprj_tMRGetAppIcon;
typedef most_devprjfi_tclMsgSelectDeviceMethodResult                    devprj_tMRSelectDev;
typedef most_devprjfi_tclMsgGetApplicationImageMethodResult             devprj_tMRGetAppImage;
typedef most_devprjfi_tclMsgSourceActivityMethodResult                  devprj_tMRSrcActivity;
typedef most_devprjfi_tclMsgDiPORoleSwitchRequiredMethodResult          devprj_tMRDiPoSwitchReq;
typedef most_devprjfi_tclMsgStopMediaPlaybackMethodResult               devprj_tMRStopMediaPb;
typedef most_devprjfi_tclMsgResumeMediaPlaybackMethodResult             devprj_tMRResumeMediaPb;

typedef most_devprjfi_tclMsgDeviceConnectionListStatus                           devprj_tStDevConnList;
typedef most_devprjfi_tclMsgDeviceNotificationsEnabledStatus                     devprj_tStDevNotifEnabled;
typedef most_devprjfi_tclMsgDisplayProjectionRequest_Status                      devprj_tStDispProjReq;
typedef most_devprjfi_tclMsgApplicationListStatus                                devprj_tStAppList;
typedef most_devprjfi_tclMsgDriveModeStatus                                      devprj_tStDriveMode;
typedef most_devprjfi_tclMsgApplicationMetaDataStatus                            devprj_tStAppMetaData;
typedef most_devprjfi_tclMsgMirrorLinkEnableStatus                               devprj_tStMLEnable;
typedef most_devprjfi_tclMsgDIPOEnableStatus                                     devprj_tStDIPOEnable;
typedef most_devprjfi_tclMsgGoogleAutoLinkEnableStatus                           devprj_tStGoogleAutoEnable;
typedef most_devprjfi_tclMsgSourceInfoStatus                                     devprj_tStSourceInfo;
typedef most_devprjfi_tclMsgAppStatusInfoStatus                                  devprj_tStAppStateInfo;
typedef most_devprjfi_tclMsgSourceAvailableStatus                                devprj_tStSourceAvailable;
typedef most_devprjfi_tclMsgBluetoothDeviceStatusStatus                          devprj_tStBTDeviceStatus;
typedef most_devprjfi_tclMsgMLActiveAppStatus                                    devprj_tStMLActiveApp;
typedef most_devprjfi_tclMsgBTPairingRequiredStatus                              devprj_tStBTPairRequired;
typedef most_devprjfi_tclMsgProjectionDeviceAuthAndAccessibilityStatus           devprj_tStProjDevAuthAndAccessibility;

typedef most_devprjfi_tclMsgDeviceNotificationsEnabledError          devprj_tErrDevNotifEnabled;
typedef most_devprjfi_tclMsgGetApplicationIconError                  devprj_tErrGetAppIcon;
typedef most_devprjfi_tclMsgDispatchMenuEncoderChangeError           devprj_tErrDispatchMenuEncChange;
typedef most_devprjfi_tclMsgGetApplicationImageError                 devprj_tErrGetAppImage;
typedef most_devprjfi_tclMsgDeviceProjectionAlertButtonEventError    devprj_tErrDevPrjAlertBtnEvent;
typedef most_devprjfi_tclMsgChangeDeviceProjectionEnableError        devprj_tErrChangeDevPrjEnable;
typedef most_devprjfi_tclMsgRequestAccessoryDisplayContext_Error     devprj_tErrRequestAccDispCtxt;
typedef most_devprjfi_tclMsgSelectDeviceError                        devprj_tErrSelectDevice;
typedef most_devprjfi_tclMsgLaunchAppError                           devprj_tErrLaunchApp;
typedef most_devprjfi_tclMsgTerminateAppError                        devprj_tErrTerminateApp;
typedef most_devprjfi_tclMsgDispatchSwitchEventError                 devprj_tErrDispSwitchEvent;
typedef most_devprjfi_tclMsgAllocateError                            devprj_tErrAllocate;
typedef most_devprjfi_tclMsgDeAllocateError                          devprj_tErrDeallocate;
typedef most_devprjfi_tclMsgSourceActivityError                      devprj_tErrSrcActivity;
typedef most_devprjfi_tclMsgDiPORoleSwitchRequiredError              devprj_tErrDiPoSwitchReq;
typedef most_devprjfi_tclMsgInvokeBluetoothDeviceActionError         devprj_tErrInvokeBTDevAction;
typedef most_devprjfi_tclMsgStopMediaPlaybackError                   devprj_tErrStopMediaPb;


typedef XFiObjHandler<most_avmanfi_tclMsgBaseChannelStatusStatus> devprj_tBaseChannelStatus;

typedef XFiObjHandler<most_avmanfi_tclMsgMuteStatus> devprj_tMuteStatus;

typedef most_avmanfi_tclMsgMutePureSet devprj_tMutePureSet;

typedef struct
{
   //! Switch Code which we will receive from the DeviceProjection FBlock
   devprj_tenSwitch enDevPrjSwitch;
   //! Corresponding ML Switch Code
   tenKeyCode enKeyCode;

}trKeyCode;

/*!
* \typedef struct rEOLRegionMap
* \brief Structure to map the EOL values with the respective Region Enumeration code
*/
typedef struct rEOLRegionMap
{
   //! EOL Value
   t_U8 u8EOLValue;
   //! Equivalent Region enumeration for the EOL Value
   tenRegion enRegion;
}trEOLRegionMap;

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
#define MOSTFI_CHAR_SET_UTF8        (most_fi_tcl_String::FI_EN_UTF8)
#define MIDWFI_CHAR_SET_UTF8        (midw_fi_tclString::FI_EN_UTF8)

#define DEVPRJ_DISP_CTXT_UNKNOWN    (devprj_tFiPrjctdDispCntxt::FI_EN_E8DISPLAY_CONTEXT_UNKNOWN)

#define AVMAN_CHN_LCMAIN            (most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_MAIN_AUDIO)
#define AVMAN_CHN_NONE              (most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_NONE)
#define AVMAN_CHN_STATUS_ACTIVE     (most_fi_tcl_e8_AVManChannelStatus::FI_EN_E8CS_ACTIVE)
#define AVMAN_CHN_STATUS_SUSPENDED  (most_fi_tcl_e8_AVManChannelStatus::FI_EN_E8CS_SUSPENDED)
#define AVMAN_CHN_STATUS_INACTIVE   (most_fi_tcl_e8_AVManChannelStatus::FI_EN_E8CS_INACTIVE)


#endif /* _DEVPRJ_TYPEDEFINES_H_ */

