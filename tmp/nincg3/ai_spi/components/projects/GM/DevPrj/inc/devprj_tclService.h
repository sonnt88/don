/*!
 *******************************************************************************
 * \file              devprj_tclService.h
 * \brief             Device Projection Service class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Device Projection Service class to implement the service 
                 provided by Device Projection component. 
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 21.10.2013 |  Ramya Murthy			       | Initial Version
 26.12.2013 |  Ramya Murthy                | Included AlertManager client-handler
                                             and few RespIntf methods implementation.
 03.01.2014 |  Hari Priya E R(RBEI/ECP2)   | Included changes for handling device selection and launch application
 30.01.2014 |  Hari Priya E R(RBEI/ECP2)   | Included changes for hard key handling
 12.02.2014 |  Ramya Murthy                | Included BT Settings client-handler & changed
                                             vGetAudioSrcInfo() to bHandoverMsgAudioSrcInfo().
 14.02.2014 |  Ramya Murthy                | Adapted to FCat v4.5.2 and SPI 
                                             HMI API document v1.5 changes.
 06.04.2014 |  Ramya Murthy                | Initialisation sequence implementation
 29.04.2014 |  Shiva Kumar G               | Locale Mapping for Application Filtering
 18.05.2014 |  Ramya Murthy                | DayNight mode implementation
 25.05.2014 |  Hari Priya E R              | Removed function for setting screen variant
 10.06.2014 |  Ramya Murthy                | Audio policy redesign implementation.
 27.06.2014 |  Ramya Murthy                | Changes for Client handlers - creation,
                                             subscription and collecting data via loopback.
 02.07.2014 |  Shihabudheen P M            | Modified for IIl extension integration.
 03.07.2014 |  Hari Priya E R              | Added changes for SWC key handling		
 31.07.2014 |  Ramya Murthy                | SPI feature configuration via LoadSettings()
 14.08.2014 |  Ramya Murthy                | Logic for ML Notifications via alerts
 27.09.2014 |  Shihabudheen P M            | CarPaly app state handling added.
 25.09.2014 |  Hari Priya E R              | Changes for PTT and END key handling
 01.10.2014 |  Ramya Murthy                | Added Telephone client handler (moved from BT Manager)
 07.10.2014 |  Ramya Murthy                | Implemented BTPairingRequired property
 13.10.2014 |  Hari Priya E R              | Added changes related to GMLAN gateway client interface
 17.11.2014 | Ramya Murthy                 | LocationData registration based on EOL and GMLANGPSConfiguration
 17.11.2014 |  Shihabudheen P M            | Added session status update for CarPlay 
 05.11.2014 |  Ramya Murthy                | Implementation of revised CarPlay media concept 
                                             and Application metadata.
 17.03.2015 | Shihabudheen P M             | Added vOnOnstarDataSettingsUpdate()
 03.06.2015 | Ramya Murthy                 | Fix for AAP music not resuming after Native speech issue. (GMMY17-3041)
 09.07.2015 | Ramya Murthy                 | Fix for No iPod audio while Android Auto active (GMMY17-3482)
 29.09.2015 | Ramya Murthy                 | Revised logic for providing vehicle data
 14.12.2015 | Rachana L Achar              | Logiscope improvements
 29.02.2016 | Rachana L Achar              | Legal Disclaimer implementation

 \endverbatim
 ******************************************************************************/

#ifndef _DEVPROJ_SERVICE_H_
#define _DEVPROJ_SERVICE_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include <map>
#include <stack>

//!Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

//!Include common fi interface
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

#define IIL_S_IMPORT_INTERFACE_GENERIC
#include <iil_if.h>

#include "SPITypes.h"
#include "devprj_TypeDefs.h"
#include "spi_LoopbackTypes.h"
#include "spi_tclRespInterface.h"
#include "spi_tclLifeCycleIntf.h"
#include "Lock.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
//! enum for the current audio state
enum tenCurrAudioState
{
   e8_AUDIO_IDLE = 0x00,
   e8_AUDIO_ACTIVATING = 0x01,
   e8_AUDIO_DEACTIVATING = 0x02
};

//! audio request
struct trAudioRequest
{
   t_U8 u8SourceNumber;
   t_Bool bActivate;
   trAudioRequest(): u8SourceNumber(0), bActivate(false)
   {

   }
};

//! Audio state
struct trAudioStatus
{
   tenCurrAudioState enCurrAudioState;
   t_U8 u8SrcNumber;

   trAudioStatus():enCurrAudioState(e8_AUDIO_IDLE), u8SrcNumber(0)
   {

   }
};


/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class devprj_tclService
 * \brief 
 */

/* Forward Declarations. */
class devprj_tclMainApp;
class devprj_tclKeyConfig;
class spi_tclCmdInterface;
class spi_tclSystemStateClient;
class spi_tclCenterStackHmiClient;
class spi_tclAlertMngrClient;
class spi_tclOnstarDataClient;
class spi_tclOnstarCallClient;
class spi_tclSpeechHMIClient;
class spi_tclNavigationClient;
class spi_tclGestureService;
class spi_tclTelephoneClient;
class spi_tclOnstarNavClient;
class spi_tclGMLANGatewayClient;
class spi_tclPosDataClientHandler;
class spi_tclSensorDataClientHandler;
class spi_tclAVManagerClient;

class devprj_tclService: public iil_tclISource, public spi_tclRespInterface, 
         public spi_tclLifeCycleIntf
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  devprj_tclService::devprj_tclService(devprj_tclMainApp* po...
   ***************************************************************************/
   /*!
   * \fn      devprj_tclService(devprj_tclMainApp* poMainAppl)
   * \brief   Overloaded Constructor
   * \param   [IN] poMainAppl : Pointer to main application
   **************************************************************************/
   devprj_tclService(devprj_tclMainApp* poMainAppl);

   /***************************************************************************
   ** FUNCTION:  virtual devprj_tclService::~devprj_tclService();
   **************************************************************************/
   /*!
   * \fn      ~devprj_tclService()
   * \brief   Destructor
   **************************************************************************/
   virtual ~devprj_tclService();

   /***************************************************************************
   ** FUNCTION:  t_Bool devprj_tclService::bInitialize();
   **************************************************************************/
   /*!
   * \fn      bInitialize()
   * \brief   Method to initialize all the pointers used
   * \param   None
   **************************************************************************/
   virtual t_Bool bInitialize();

   /***************************************************************************
    ** FUNCTION:  t_Bool devprj_tclService::bUnInitialize();
    **************************************************************************/
   /*!
    * \fn      bUnInitialize()
    * \brief   Method to de-initialize all the pointers used
    * \param   None
    **************************************************************************/
   virtual t_Bool bUnInitialize();

   /***************************************************************************
    ** FUNCTION:  t_Void devprj_tclService::vLoadSettings(const trSpiFeatureSupport&...)
    ***************************************************************************/
   /*!
    * \fn      vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
    * \brief   vLoadSettings Method. Invoked during OFF->NORMAL state transition.
    * \sa      vSaveSettings()
    **************************************************************************/
   virtual t_Void vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp);

   /***************************************************************************
    ** FUNCTION:  t_Void devprj_tclService::vSaveSettings()
    ***************************************************************************/
   /*!
    * \fn      vSaveSettings()
    * \brief   vSaveSettings Method. Invoked during  NORMAL->OFF state transition.
    * \sa      vLoadSettings()
    **************************************************************************/
   virtual t_Void vSaveSettings();

   /***************************************************************************
    ** FUNCTION:  t_Void devprj_tclService::vRestoreSettings()
    ***************************************************************************/
   /*!
    * \fn      vRestoreSettings()
    * \brief   This method is called by the Main application when ClearPrivateData
    *          defset event occurs.
    * \param   None
    **************************************************************************/
   t_Void vRestoreSettings();

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vSetMLNotificationOnOff()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetMLNotificationOnOff(t_Bool bSetMLNotificationsOn)
   * \brief  To Set the Notifications to On/Off
   * \param  bSetMLNotificationsOn : [IN] True - Set Notifications to ON
   *                                    False - Set Notifications to OFF
   * \retval t_Void
   **************************************************************************/
   t_Void vSetMLNotificationOnOff(t_Bool bSetMLNotificationsOn);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vSetRegion()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetRegion(t_U8 u8EOLMarketingRegion)
   * \brief  This is to set the current vehicle sale region for the application filtering
   * \param  [IN] u8EOLMarketingRegion : EOL Value
   * \retval t_Void
   **************************************************************************/
   t_Void vSetRegion(t_U8 u8EOLMarketingRegion);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vOnCarPlayAudioResult()
   ***************************************************************************/
   /*!
   * \fn     vOnCarPlayAudioResult(t_String szAudioDevice, tenAudioError enAudioError)
   * \brief  Handles AudioDevice result from Mediaplayer
   * \param  [IN] szAudioDevice: ALSA output audio device name
   * \param  [IN] enAudioError: received audio error from mediaplayer
   * \retval t_Void
   **************************************************************************/
   t_Void vOnCarPlayAudioResult(t_String szAudioDevice, tenAudioError enAudioError);

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnLoopback(amt_tclSer...
   ***************************************************************************/
   /*!
   * \brief   Loopback service for Smartphone service.
   * \param   [u16ServiceID]:   (I) Service ID
   * \param   [poMessage]:      (->I) Pointer to incoming message.
   * \retval  NONE
   **************************************************************************/
   using ahl_tclBaseOneThreadService::vOnLoopback;
   virtual tVoid vOnLoopback(tU16 u16ServiceID, amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION   : tVoid devprj_tclService::vOnTimer(tU16 nTimerId)
   **************************************************************************/
   /* !
   * \fn        vOnTimer(tU16 u16TimerId)
   * \brief     This method is called by the CCA framework on the expiration
   *            of a previously started timer via method bStartTimer(). The
   *            expired timer is forwarded to the respective service or
   *            client handlers via a call of vProcessTimer().
   *            The method is called from this applications context and
   *            therefore no interthread programming rules must be considered
   *            and the application methods and/or member variables can be
   *            accessed without using the static self reference
   *            'm_poMainAppInstance'.
   * \param     [IN] u16TimerId = Identifier of the expired timer.
   * \retval    None.
   **************************************************************************/
   virtual tVoid vOnTimer(tU16 nTimerId);

   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /****************************************************************************
   * Overriding Base class SpiRespInterface's methods
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclService::vPostDeviceStatusInfo(t_U32...)
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceStatusInfo(t_U32 u32DeviceHandle,
   *            tenDeviceConnectionType enDevConnType,
   *            tenDeviceStatusInfo enDeviceStatusInfo)
   * \brief  It notifies the client on change in any device attributes.
   *         The client can retrieve the detailed information
   *         via the methods provided.
   * \param  [IN] u32DeviceHandle   : Unique device handle
   * \param  [IN] enDevConnType  : Connection type of device
   * \param  [IN] enDeviceStatus : Device status info enumeration
   **************************************************************************/
   virtual tVoid vPostDeviceStatusInfo(t_U32 u32DeviceHandle,
         tenDeviceConnectionType enDevConnType, tenDeviceStatusInfo enDeviceStatus);

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclService::vPostAppStatusInfo(t_U32...)
   ***************************************************************************/
   /*!
   * \fn     vPostAppStatusInfo(t_U32 u32DeviceHandle,
   *            tenDeviceConnectionType enDevConnType,
   *            tenAppStatusInfo enAppStatusInfo)
   * \brief  It notifies the client on change in any device attributes.
   *         The client can retrieve the detailed information
   *         via the methods provided.
   * \param  [IN] u32DeviceHandle  : Unique device handle
   * \param  [IN] enDevConnType : Connection type of device
   * \param  [IN] enAppStatus   : Application status unfo enumeration
   **************************************************************************/
   virtual tVoid vPostAppStatusInfo(t_U32 u32DeviceHandle,
         tenDeviceConnectionType enDevConnType, tenAppStatusInfo enAppStatus);

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclService::vPostDeviceUsagePrefResult(t_U32...)
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceUsagePrefResult(t_U32 u32DeviceHandle,
   *            tenErrorCode enErrorCode)
   * \brief  It provides the result of Device Usage Preference Set Request
   * \param  [IN] coU32DeviceHandle: Device handle for which the device usage
   *         preference was set
   * \param  [IN] enErrorCode: Error code if setting device usage preference fails
   * \param  [IN] enDeviceCategory: Device Category
   * \param  [IN] enUsagePref: indicates whether the device usage preference is enabled or not
   * \param  [IN] corfrUsrCtxt : User context
   **************************************************************************/
   virtual t_Void vPostDeviceUsagePrefResult(const t_U32 coU32DeviceHandle,
         tenErrorCode enErrorCode, tenDeviceCategory enDeviceCategory,
         tenEnabledInfo enUsagePref, const trUserContext &corfrUsrCtxt);

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclService::vLaunchAppResult(tenResponseCod...
   ***************************************************************************/
   /*!
   * \fn     vLaunchAppResult(tenResponseCode enResponseCode, tenErrorCode enErrorCode,
   *              const trUserContext rcUsrCntxt)
   * \brief  It provides the result of remote application launch in selected
   *         Mirror Link device.
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] u32AppHandle    : Uniquely identifies an Application on
   *              the target Device. This value will be obtained from AppList Interface.
   *              This value will be set to 0xFFFFFFFF if DeviceCategory = DEV_TYPE_DIPO.
   * \param  [IN] enDiPOAppType : Identifies the application to be launched on a DiPO device.
   *              This value will be set to NOT_USED if DeviceCategory = DEV_TYPE_MIRRORLINK.
   * \param  [IN] enResponseCode  :  Provides result from the operation.
   * \param  [IN] enErrorCode     : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \sa     spi_tclCmdInterface::vLaunchApp
   **************************************************************************/
   virtual tVoid vLaunchAppResult(t_U32 u32DeviceHandle, 
         t_U32 u32AppHandle, tenDiPOAppType enDiPOAppType,
         tenResponseCode enResponseCode, tenErrorCode enErrorCode,
         const trUserContext& rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclService::vTerminateAppResult(tenRespons...
   ***************************************************************************/
   /*!
   * \fn     vTerminateAppResult(tenResponseCode enResponseCode,
   *              tenErrorCode enErrorCode, const trUserContext rcUsrCntxt)
   * \brief  It provides the result of remote application termination on the
   *         Mirror Link device.
   * \param  [IN] u32DeviceHandle : Unique handle of the device.
   * \param  [IN] u32AppHandle    : Unique handle of the application to be terminated.   
   * \param  [IN] enResponseCode  : Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt  : User Context Details.
   * \sa     spi_tclCmdInterface::vTerminateApp
   **************************************************************************/
   virtual tVoid vTerminateAppResult(t_U32 u32DeviceHandle, 
         t_U32 u32AppHandle, tenResponseCode enResponseCode,
         tenErrorCode enErrorCode, const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vPostApplicationMediaMetaData(t_U32& ...
   ***************************************************************************/
   /*!
   * \fn     vPostApplicationMediaMetaData(const trAppMediaMetaData& rfcorApplicationMediaMetaData,
   *            const trUserContext& rfcorUsrCntxt)
   * \brief  Interface to notify application media metadata to the client.
   * \param  [IN] rfcorApplicationMediaMetaData : Contains the media metadata information
   *              related to an application.
   * \param  [IN] rfcorUsrCntxt    : User Context Details.
   * \sa
   **************************************************************************/
   virtual t_Void vPostApplicationMediaMetaData(const trAppMediaMetaData& rfcorApplicationMediaMetaData,
      const trUserContext& rfcorUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vPostApplicationPhoneData(...
   ***************************************************************************/
   /*!
   * \fn     vPostApplicationPhoneData(const trAppPhoneData& rfcorApplicationPhoneData,
   *            const trUserContext& rfcorUsrCntxt)
   * \brief  Interface to notify application media metadata to the client.
   * \param  [IN] rfcorApplicationPhoneData : Contains the phone related information.
   * \param  [IN] rfcorUsrCntxt    : User Context Details.
   * \sa
   **************************************************************************/
   virtual t_Void vPostApplicationPhoneData(const trAppPhoneData& rfcorApplicationPhoneData,
      const trUserContext& rfcorUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vPostApplicationMediaPlaytime(...
   ***************************************************************************/
   /*!
   * \fn     vPostApplicationMediaPlaytime(const trAppMediaPlaytime& rfcorApplicationMediaPlaytime,
   *            const trUserContext& rfcorUsrCntxt)
   * \brief  Interface to notify application media metadata to the client.
   * \param  [IN] rfcorApplicationMediaPlaytime : Contains the media play time of current playing track.
   * \param  [IN] rfcorUsrCntxt    : User Context Details.
   * \sa
   **************************************************************************/
   virtual t_Void vPostApplicationMediaPlaytime(const trAppMediaPlaytime& rfcorApplicationMediaPlaytime,
      const trUserContext& rfcorUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vPostDeviceDisplayContext
   **                   (t_U32 u32DeviceHandle, tenDisplayContext enDisplayContext,..)
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceDisplayContext(t_U32 u32DeviceHandle, t_Bool bDisplayFlag,
   *         tenDisplayContext enDisplayContext, const trUserContext rcUsrCntxt);
   * \brief  This interface is used by Mirror Link/DiPO device to inform the client
   *              about its current display con-text.
   * \param  [IN] u32DeviceHandle  : Uniquely identifies the target Device.
   * \param  [IN] bDisplayFlag     : TRUE – Start Display Projection,
   *              FALSE – Stop Display Projection.
   * \param  [IN] enDisplayContext : Display context of the projected device.
   * \param  [IN] rcUsrCntxt       : User Context Details.
   * \sa
   **************************************************************************/
   virtual t_Void vPostDeviceDisplayContext(t_U32 u32DeviceHandle,
         t_Bool bDisplayFlag, tenDisplayContext enDisplayContext,
         const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclService::vPostSelectDeviceResult
   **                (tenResponseCode enResponseCode, tenErrorCode enErrorCode, ..)
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceStatusInfo(DeviceStatusInfo enDeviceStatusInfo,
   *              const trUserContext rcUsrCntxt)
   * \brief  It provides a mechanism to
   *         select a device from Mirror Link device Manager
   *         to establish a session.
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] enDevConnType   : Identifies the Connection Type.
   * \param  [IN] enDevConnReq    : Identifies the Connection Request.
   * \param  [IN] enDevCat    : Identifies the device category
   * \param  [IN] enRespCode  : Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] bIsPairingReq  : Set when BT pairing is required.
   *         (Not valid for deselection)
   * \param  [IN] rcUsrCntxt  : User Context Details.
   * \sa     spi_tclCmdInterface::vSelectDevice
   **************************************************************************/
   virtual tVoid vPostSelectDeviceResult(t_U32 u32DeviceHandle,
         tenDeviceConnectionType enDevConnType, tenDeviceConnectionReq enDevConnReq, 
         tenDeviceCategory enDevCat,
         tenResponseCode enResponseCode, tenErrorCode enErrorCode,
         t_Bool bIsPairingReq, const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vPostDeviceSelectStatus
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceSelectStatus
   * \brief  Respond with the device selection status.
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] enDevCategory   : Device category.
   * \param  [IN] enDevConnReq	  : Identifies the Connection Request.
   * \param  [IN] enRespCode      : Response code
   **************************************************************************/
   virtual t_Void vPostDeviceSelectStatus(t_U32 u32DeviceHandle, 
      tenDeviceCategory enDevCategory,
      tenDeviceConnectionReq enDevConnReq,
      tenResponseCode enRespCode);

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclService::vPostGetAppIconData(t_U32 u32Ico...
   ***************************************************************************/
   /*!
   * \fn     vPostAppIconDataResult(t_U32 u32IconMimeType, t_Char* pczAppIconData,
   *              t_U32 u32Len,const trUserContext rcUsrCntxt)
   * \brief  It retrieves icon data referenced by the AppList.AppIconXXXURLs
   * \param  [IN] u32IconMimeType :  Mime Type of the icon pointed by AppIconURL.
   *              If image is not available then this parameter would be set
   *              to NULL (zero length string).
   * \param  [IN] pczAppIconData  : Byte Data Stream from the icon image file.
   *              Format of the file is de-fined by IconMimeType parameter.
   * \param  [IN] u32Len : Length the data stream
   * \param  [IN] rcUsrCntxt      : User Context Details.
   * \sa     spi_tclCmdInterface::vGetAppIconData
   **************************************************************************/
   virtual tVoid vPostAppIconDataResult(tenIconMimeType enIconMimeType,
         const t_U8* pcu8AppIconData, t_U32 u32Len, const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclService::vPostSendKeyEvent(tenResponseCode e...
   ***************************************************************************/
   /*!
   * \fn     vPostSendKeyEvent(tenResponseCode enResponseCode,tenErrorCode enErrorCode,
   *            const trUserContext rcUsrCntxt)
   * \brief  Interface to set the key events.
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode    : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt		: User Context Details.
   * \sa     spi_tclCmdInterface::vSendKeyEvent
   **************************************************************************/
   virtual tVoid vPostSendKeyEvent(tenResponseCode enResponseCode,
         tenErrorCode enErrorCode, const trUserContext rcUsrCntxt);

  /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vPostDeviceAppState
   **                   (tenSpeechAppState enSpeechAppState,...)
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceAudioContext()
   * \brief  This function used to update HMI about the App state changes
   * \param  [IN] enSpeechAppState : Speech app state
   * \param  [IN] enPhoneAppState  : Phone App state
   * \param  [IN] enNavAppState    : Navigation App state
   * \param  [IN] rcUsrCntxt       : User Context Details.
   **************************************************************************/
   virtual t_Void vPostDeviceAppState(tenSpeechAppState enSpeechAppState, 
      tenPhoneAppState enPhoneAppState,  tenNavAppState enNavAppState, 
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vPostNotificationInfo()
   ***************************************************************************/
   /*!
   * \fn     vPostNotificationInfo(t_U32 u32DeviceHandle, t_U32 u32AppHandle,
   *            NotificationData &rfrNotificationData)
   * \brief  Interface to provide Notification Information received from
   *         the Mirror Link server (only for Mirror Link devices).
   * \param  [IN] u32DeviceHandle   :  Handle uniquely identifies a device.
   * \param  [IN] u32AppHandle      : Handle uniquely identifies an application
   *              on the device.
   * \param  [IN] corfrNotificationData : Provides notification event details
   **************************************************************************/
   t_Void vPostNotificationInfo(t_U32 u32DeviceHandle,
      t_U32 u32AppHandle,
      const trNotiData& corfrNotificationData);

   /***************************************************************************
    ** FUNCTION: t_Void devprj_tclService::vPostBluetoothDeviceStatus(...)
    ***************************************************************************/
   /*!
    * \fn     vPostBluetoothDeviceStatus(t_U32 u32BluetoothDevHandle,
    *            t_U32 u32ProjectionDevHandle, tenBTChangeInfo enBTStatus,
    *            t_Bool bCallActive)
    * \brief  Interface used by SPI to notify clients when changing from or to a BT device.
    * \param  [IN] u32BluetoothDevHandle  : Uniquely identifies a Bluetooth Device.
    * \param  [IN] u32ProjectionDevHandle : Uniquely identifies a Projection Device.
    * \param  [IN] bSameDevice : Inidcates whether BT & Projection device are same
    *              or different devices.
    * \param  [IN] enBTStatus  : Enum value which stores BT device status
    **************************************************************************/
   virtual t_Void vPostBluetoothDeviceStatus(t_U32 u32BluetoothDevHandle,
         t_U32 u32ProjectionDevHandle,
         tenBTChangeInfo enBTChange,
         t_Bool bSameDevice,
         t_Bool bCallActive);

   /***************************************************************************
    ** FUNCTION: t_Void devprj_tclService::vPostBTPairingRequired(...)
    ***************************************************************************/
   /*!
    * \fn     vPostBTPairingRequired(const t_String& rfcoszBTAddress, t_Bool bPairingRequired)
    * \brief  It notifies the client when Projection device is required to be paired via Bluetooth.
    * \param  [IN] rfcoszBTAddress  : BT Address of device.
    * \param  [IN] bPairingRequired : true - if device needs to be paired, else false.
    **************************************************************************/
   virtual t_Void vPostBTPairingRequired(const t_String& rfcoszBTAddress,
         t_Bool bPairingRequired);

   /***************************************************************************
    ** FUNCTION:  t_Void devprj_tclService::vPostSubscribeForLocData(t_Bool...)
    ***************************************************************************/
   /*!
   * \fn      vPostSubscribeForLocData(t_Bool bSubscribe, tenLocationDataType enLocDataType)
   * \brief   Subscribes/Unsubscribes for location data notifications from
   *              service-provider (such as POS_FI).
   * \param   [IN] bSubscribe:
   *               TRUE - To subscribe for location data notifications.
   *               FALSE - To unsubscribe for location data notifications.
   * \param   [IN] enLocDataType: Type of data to be subscribed/unsubscribed
   * \retval  None
   **************************************************************************/
   virtual t_Void vPostSubscribeForLocData(t_Bool bSubscribe, tenLocationDataType enLocDataType);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vSendSessionStatusInfo()
   ***************************************************************************/
   /*!
   * \fn     vSendSessionStatusInfo(t_U32 u32DeviceHandle,
   *             tenDeviceCategory enDevCat, tenSessionStatus enSessionStatus)
   * \brief  It notifies the client about the ML Session status updates
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] enDevCat        : Identifies the Device category.
   * \param  [IN] enSessionStatus : Session status
   **************************************************************************/
   virtual t_Void vSendSessionStatusInfo(t_U32 u32DeviceHandle,
      tenDeviceCategory enDevCat,
      tenSessionStatus enSessionStatus);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vUpdateAppBlockingInfo()
   ***************************************************************************/
   /*!
   * \fn     vUpdateAppBlockingInfo(t_U32 u32DeviceHandle,
   *             tenDeviceCategory enDevCat, tenSessionStatus enSessionStatus)
   * \brief  It notifies the client about the Application blocking using the
   *          session status update
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] enDevCat        : Identifies the Device category.
   * \param  [IN] enSessionStatus : Session status
   **************************************************************************/
   virtual t_Void vUpdateAppBlockingInfo(t_U32 u32DeviceHandle,
      tenDeviceCategory enDevCat,
      tenSessionStatus enSessionStatus);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vUpdateActiveAppInfo()
   ***************************************************************************/
   /*!
   * \fn     t_Void vUpdateActiveAppInfo(const t_U32 cou32DevId,
   *            const tenDeviceCategory coenDevCat,
   *            const t_U32 cou32AppId,
   *            const tenAppCertificationInfo coenAppCertInfo)
   * \brief  method to send the Active App update to HMI 
   * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
   * \param  coenDevCat       : [IN] Device category
   * \param  cou32AppId       : [IN] AppId
   * \param  coenAppCertInfo  : [IN] Certification Status
   * \retval t_Void
   **************************************************************************/
   virtual t_Void vUpdateActiveAppInfo(t_U32 u32DeviceHandle,
      tenDeviceCategory enDevCat,
      const t_U32 cou32AppId,
      const tenAppCertificationInfo coenAppCertInfo);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vUpdateSessionStatusInfo
   **                   (t_U32 u32DeviceHandle,...)
   ***************************************************************************/
   /*!
   * \fn     vUpdateSessionStatusInfo()
   * \brief  Used to update the session status to HMI.
   * \param  [IN] u32DeviceHandle  : Device handle 
   * \param  [IN] enDevCat         : Device category
   * \param  [IN] enSessionStatus  : Session status.
   **************************************************************************/
   virtual t_Void vUpdateSessionStatusInfo(t_U32 u32DeviceHandle,
      tenDeviceCategory enDevCat,
      tenSessionStatus enSessionStatus);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vUpdateDevAuthAndAccessInfo()
   ***************************************************************************/
   /*!
   * \fn     virtual t_Void vUpdateDevAuthAndAccessInfo(const t_U32 cou32DeviceHandle,
   *             const t_Bool cobHandsetInteractionReqd)
   * \brief  Notifies the authorization and access to AAP projection device.
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] cobHandsetInteractionReqd   : Set if interaction required on Handset.
   **************************************************************************/
   virtual t_Void vUpdateDevAuthAndAccessInfo(const t_U32 cou32DeviceHandle,
            const t_Bool cobHandsetInteractionReqd);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vUpdateDevAuthStatus()
   ***************************************************************************/
   /*!
   * \fn     virtual t_Void vUpdateDevAuthStatus(const t_U32 cou32DeviceHandle,
   *             const t_Bool cobUserAuthStatus)
   * \brief  Notifies the authorization status to AAP projection device.
   * \param  [IN] u32DeviceHandle   : Uniquely identifies the target Device.
   * \param  [IN] cobUserAuthStatus : authorization status
   **************************************************************************/
   virtual t_Void vUpdateDevAuthStatus(const t_U32 cou32DeviceHandle,
            const t_Bool cobUserAuthStatus);

   /*************************Start of ISource methods*************************/

   /***************************************************************************
    ** FUNCTION: t_Void devprj_tclService::vRestoreLastMediaAudSrc()
    ***************************************************************************/
   /*!
    * \fn     t_Void vRestoreLastMediaAudSrc
    * \brief  Interface to restore last stored audio source.
    * \param  NONE
    * \retval NONE
    **************************************************************************/
   virtual t_Void vRestoreLastMediaAudSrc();

   /***************************************************************************
   ** FUNCTION:  tBool devprj_tclService::bSetAudioDucking(const tU8 cou16RampDuration,
			const tU8 cou8VolumeLevelindB, const tenDuckingType coenDuckingType);
   ***************************************************************************/
   /*!
   * \fn      tBool bSetAudioDucking(const tU16 cou16RampDuration, 
			  const tU8 cou8VolumeLevelindB, const tenDuckingType coenDuckingType);
   * \brief  Interface to set audio ducking ON/OFF.
   * \param  cou8SrcNum: Source Number.
   * \param  cou16RampDuration: Ramp duration in milliseconds
   * \param  cou8VolumeLevelindB: Volume level in dB
   * \param  coenDuckingType: Ducking/ Unducking
   * \retval Bool value
   **************************************************************************/
   virtual t_Bool bSetAudioDucking(const tU8 cou8SrcNum, const tU16 cou16RampDuration,
            const tU8 cou8VolumeLevelindB, const tenDuckingType coenDuckingType);

   /***************************************************************************
   ** FUNCTION:  t_Bool devprj_tclService::bRequestAudioActivation(t_U8)
   ***************************************************************************/
   /*!
   * \fn      bRequestAudioActivation(t_U8 u8SourceNum)
   * \brief   Request to the Audio Manager by Component for Starting Audio Playback.
   *          Mandatory Interface to be implemented as per Project Audio Policy.
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          Source Number will be defined for Audio Source by the Audio Component.
   * \retval  Bool value
   **************************************************************************/
   virtual t_Bool bRequestAudioActivation(t_U8 u8SourceNum);

   /***************************************************************************
   ** FUNCTION:  t_Bool devprj_tclService::bRequestAudioDeactivation(t_U8)
   ***************************************************************************/
   /*!
   * \fn      bRequestAudioDeactivation(t_U8 u8SourceNum)
   * \brief   Request to the Audio Manager by Component for Stopping Audio Playback.
   *          Mandatory Interface to be implemented as per Project Audio Policy.
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          Source Number will be defined for Audio Source by the Audio Component.
   * \retval  Bool value
   **************************************************************************/
   virtual t_Bool bRequestAudioDeactivation(t_U8 u8SourceNum);

   /***************************************************************************
   ** FUNCTION:  t_Bool devprj_tclService::bPauseAudioActivity(t_U8)
   ***************************************************************************/
   /*!
   * \fn      bPauseAudioActivity(t_U8 u8SourceNum)
   * \brief   Request to the Audio Manager by Component for Pausing Audio Playback.
   *          Optional Interface to be implemented if supported and required.
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          Source Number will be defined for Audio Source by the Audio Component.
   * \retval  Bool value
   **************************************************************************/
   virtual t_Bool bPauseAudioActivity(t_U8 u8SourceNum) {return false;};

   /***************************************************************************
   ** FUNCTION:  t_Void devprj_tclService::vStartSourceActivityResult(t_U8, t_Bool)
   ***************************************************************************/
   /*!
   * \fn      vStartSourceActivityResult(t_U8 u8SourceNum, t_Bool bError)
   * \brief   Acknowledgement from the Source Component to Audio Manager indicating
   *          Successful Start of Audio Playback on the allocated route.
   *          Mandatory Interface to be implemented.
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          [bError]: true for Error Condition, false otherwise
   *          Source Number will be defined for Audio Source by the Audio Component.
   * \retval  NONE
   **************************************************************************/
   virtual t_Void vStartSourceActivityResult(t_U8 u8SourceNum, t_Bool bError = false);

   /***************************************************************************
   ** FUNCTION:  t_Void devprj_tclService::vStopSourceActivityResult(t_U8, t_Bool)
   ***************************************************************************/
   /*!
   * \fn      vStopSourceActivityResult(t_U8 u8SourceNum, t_Bool bError)
   * \brief   Acknowledgement from the Source Component to Audio Manager indicating
   *          Successful Stop of Audio Playback on the allocated route.
   *          Mandatory Interface to be implemented.
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          [bError]: true for Error Condition, false otherwise
   * \retval  NONE
   **************************************************************************/
   virtual t_Void vStopSourceActivityResult(t_U8 u8SourceNum, t_Bool bError = false);

   /***************************************************************************
   ** FUNCTION:  t_Void devprj_tclService::vPauseSourceActivityResult(t_U8, t_Bool)
   ***************************************************************************/
   /*!
   * \fn      vPauseSourceActivityResult(t_U8 u8SourceNum, t_Bool bError)
   * \brief   Acknowledgement from the Source Component to Audio Manager indicating
   *          Successful Pause of Audio Playback on the allocated route.
   *          Optional Interface to be implemented if supported and required
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          [bError]: true for Error Condition, false otherwise
   *          Source Number will be defined for Audio Source by the Audio Component.
   * \retval  Bool value
   **************************************************************************/
   virtual t_Void vPauseSourceActivityResult(t_U8 u8SourceNum, t_Bool bError = false){};

   /***************************************************************************
   ** FUNCTION:  t_Bool devprj_tclService::bSetSrcAvailability(t_U8,t_Bool)
   ***************************************************************************/
   /*!
   * \fn      bSetSrcAvailability(t_U8 u8SourceNum,t_Bool bAvail)
   * \brief   Register the Availability of State of Source with Audio Manager.
   *          Optional Interface to be implemented if supported and required
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          [bAvail]: true is Source Available, false if Unavailable
   *          Source Number will be defined for Audio Source by the Audio Component.
   * \retval  Bool value
   **************************************************************************/
   virtual t_Bool bSetSrcAvailability(t_U8 u8SourceNum, t_Bool bAvail = true);

   /***************************************************************************
   ** FUNCTION:  t_Bool devprj_tclService::bSetSourceMute(t_U8)
   ***************************************************************************/
   /*!
   * \fn      bSetSourceMuteOn(t_U8 u8SourceNum)
   * \brief   Request to Audio Manager to Mute the Source Audio.
   *          Optional Interface to be implemented if supported and required.
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          Source Number will be defined for Audio Source by the Audio Component.
   * \retval  Bool value
   **************************************************************************/
   virtual t_Bool bSetSourceMute(t_U8 u8SourceNum){return false;};

   /***************************************************************************
   ** FUNCTION:  t_Bool devprj_tclService::bSetSourceDemute(t_U8)
   ***************************************************************************/
   /*!
   * \fn      t_Bool bSetSourceDemute(t_U8 u8SourceNum)
   * \brief   Request to Audio Manager to Demute the Source Audio.
   *        Optional Interface to be implemented if supported and required.
   * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
   *          Source Number will be defined for Audio Source by the Audio Component.
   * \retval  Bool value
   **************************************************************************/
   virtual t_Bool bSetSourceDemute(t_U8 u8SourceNum) {return false;};

   // IIL Functions
   /***************************************************************************
   ** FUNCTION:  virtual tVoid devprj_tclService::vOnError(tU8,const iil_tenISource)
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnError(tU8 u8SourceNum, const iil_tenISourceError)
   * \brief   Function callback to trigger actions on request AV Activation
   *          error
   * \param   [u8SrcNum]:  (I) Source Number.
   * \param   [enError]:  (I) ISource Error type.
   * \retval  NONE
   **************************************************************************/
   virtual tVoid vOnError(tU8 u8SrcNum, const iil_tenISourceError enError);

   /***************************************************************************
   ** FUNCTION:  virtual tBool devprj_tclService::bOnSrcActivityStart(const...
   ***************************************************************************/
   /*!
   * \fn      tBool bOnSrcActivityStart(const tU8 cu8SrcNum,
   *              const iil_tSrcActivity& rfcoSrcActivity)
   * \brief   Application specific function on Source Activity start.
   * \param   [cu8SrcNum]:  (I) Source Number.
   * \param   [rfcoSrcActivity]: (I) Source Activity
   * \retval  [tBool]: true, if source activity was successful, false otherwise
   **************************************************************************/
   virtual tBool bOnSrcActivityStart(const tU8 cu8SrcNum, const iil_tSrcActivity& rfcoSrcActivity);

   /***************************************************************************
   ** FUNCTION:  virtual tBool devprj_tclService::bSrcActivityResult(const iil_t..
   ***************************************************************************/
   /*!
   * \fn      tBool bSrcActivityResult(const tU8 cu8SrcNum,
   *              const iil_tSrcActivity& rfcoSrcActivity)
   * \brief   Application specific function before Source Activity result.
   * \param   [u8SrcNum]:  (I) Source Number.
   * \param   [rfcoSrcActivity]: (I) Source Activity
   * \retval  [tBool]: true, if source activity was successful, false otherwise
   **************************************************************************/
   virtual tBool bSrcActivityResult(const tU8 cu8SrcNum ,
             const iil_tSrcActivity& rfcoSrcActivity);

   /***************************************************************************
   ** FUNCTION:  virtual tBool devprj_tclService::bOnAllocateResult(const...
   ***************************************************************************/
   /*!
   * \fn      tBool bOnAllocateResult(const tU8 cu8SrcNum, const iil_tSrcActivity& rfcoSrcActivity)
   * \brief   Application specific function after Allocate is processed.
   * \param   [u8SrcNum]:  (I) Source Number.
   * \param   [rfcoAllocRoute]: (I) Reference to Allocate route result
   * \retval  [tBool]: true, if Application performed operations successfully,
   *          false otherwise
   **************************************************************************/
   virtual tBool bOnAllocateResult(const tU8 cu8SrcNum,
             const iil_tAllocRouteResult& rfcoAllocRoute);

   /***************************************************************************
   ** FUNCTION:  virtual tBool devprj_tclService::bOnDeAllocateResult(const...
   ***************************************************************************/
   /*!
   * \fn      tBool bOnDeAllocateResult(const tU8 cu8SrcNum)
   * \brief   Application specific function after DeAllocate is processed.
   * \param   [u8SrcNum]:  (I) Source Number.
   * \retval  [tBool]: true, if Application performed operations successfully,
   *          false otherwise
   **************************************************************************/
   virtual tBool bOnDeAllocateResult(const tU8 cu8SrcNum);

   /***************************************************************************
   ** FUNCTION:  virtual tBool devprj_tclService::bOnReqAVActResult(const...
   ***************************************************************************/
   /*!
   * \fn      tBool bOnReqAVActResult(const tU8 cu8SrcNum,
   *              const iil_tAVReqResult& coAVReqResult)
   * \brief   Application specific function after RequestAVAct result can be
   *          processed.
   * \param   [u8SrcNum]:  (I) Source Number.
   * \param   [coAVReqResult]:  (I) AV Activation result.
   * \retval  [tBool]: true, if Application performed operations successfully,
   *          false otherwise
   **************************************************************************/
   virtual tBool bOnReqAVActResult(const tU8 cu8SrcNum
              , const iil_tAVReqResult& coAVReqResult);

   /***************************************************************************
   ** FUNCTION:  virtual tBool devprj_tclService::bOnReqAVDeActResult(const tU8 cu8Sr..
   ***************************************************************************/
   /*!
   * \fn      tBool bOnReqAVDeActResult(const tU8 cu8SrcNum)
   * \brief   Application specific function after RequestAVAct result can be
   *          processed.
   * \param   [u8SrcNum]:  (I) Source Number.
   * \retval  [tBool]: true, if Application performed operations successfully,
   *          false otherwise
   **************************************************************************/
   virtual tBool bOnReqAVDeActResult(const tU8 cu8SrcNum);

  /***************************************************************************
   ** FUNCTION:  virtual tBool devprj_tclService::vOnBaseChannelStatusUpdate
   ***************************************************************************/
   /*!
   * \fn      tBool vOnBaseChannelStatusUpdate
   * \brief   AFunction to get tha base channel status updates from AV manager          
   * \param   poMessage:  [IN] CCA message data
   * \retval  None
   **************************************************************************/
   t_Void vOnBaseChannelStatusUpdate(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  virtual t_Void devprj_tclService::vOnMuteStatus
   ***************************************************************************/
   /*!
   * \fn      vOnMuteStatus
   * \brief   Method to get the Mute status updates from AV manager
   * \param   poMessage:  [IN] CCA message data
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnMuteStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  virtual t_Void devprj_tclService::vOnSetMute
   ***************************************************************************/
   /*!
   * \fn      vOnSetMute(t_Bool bMuteState)
   * \brief   Method to set the Mute status to ON/OFF
   * \param   bMuteState:  [IN] Mute Status-ON/OFF
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnSetMute(t_Bool bMuteState);

   /***************************************************************************
    ** FUNCTION:  t_Void devprj_tclService::vSendAudioStatusChange(...)
    ***************************************************************************/
    /*!
    * \fn      vSendAudioStatusChange(tenAudioStatus enAudioStatus)
    * \brief   Interface to provide audio status change info
    * \param   [enAudioStatus]: Current audio status
    * \retval  t_Void
    **************************************************************************/
   virtual t_Void vSendAudioStatusChange(tenAudioStatus enAudioStatus);

   /*************************End of ISource methods**************************/

   /***************************************************************************
   * Overriding ahl_tclBaseOneThreadService methods.
   **************************************************************************/

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclVideoPolicy::vOnServiceAvailable()
   ***************************************************************************/
   /*!
   * \fn      virtual tVoid vOnServiceAvailable()
   * \brief   This function is called by the CCA framework when the service
   *          which is offered by this server has become available.
   * \retval  tVoid
   * \sa      vOnServiceUnavailable()
   **************************************************************************/
   virtual tVoid vOnServiceAvailable();

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclVideoPolicy::vOnServiceUnavailable()
   ***************************************************************************/
   /*!
   * \fn      virtual tVoid vOnServiceUnavailable()
   * \brief   This function is called by the CCA framework when the service
   *          which is offered by this server has become unavailable.
   * \retval  tVoid
   * \sa      vOnServiceAvailable()
   **************************************************************************/
   virtual tVoid vOnServiceUnavailable();

	/***************************************************************************
   ** FUNCTION:  tBool devprj_tclService::bStatusMessageFactory(tU16 u16Fun...
   **************************************************************************/
   /*!
   * \fn      bStatusMessageFactory(tU16 u16FunctionId, 
   *                        amt_tclServiceData& roOutMsg, 
   *                        amt_tclServiceData* poInMsg)
   * \brief   This method shall create for every supported property a status
   *          message, which then is used by the framework to update registered
   *          clients. Typically this method is called when the service issues
   *          a method call like eUpdateClients(..) or eUpdateRequestingClient(..)
   *          or for framework related reasons (e.g. our service becomes available 
   *          again).
   * \param   [IN] u16FunctionId : Function ID of the requested property.
   * \param   [OUT] roOutMsg : Reference to the service data object to which the
   *                content of the prepared FI data object should be copied to.
   * \param   [IN] poInMsg   : Selector message which is used to select dedicated
   *               content to be copied to 'roOutMsg' instead of updating
   *               the entire FI data object.
   **************************************************************************/
   virtual tBool bStatusMessageFactory(tU16 u16FunctionId,
         amt_tclServiceData& roOutMsg, amt_tclServiceData* poInMsg);

   /***************************************************************************
    ** FUNCTION:  tBool devprj_tclService::bProcessSet(amt_tclServiceData* p...
    ***************************************************************************/
   /*!
    * \fn      bProcessSet(amt_tclServiceData* poMessage,
   *                 tBool& bPropertyChanged, tU16& u16Error)
    * \brief   This method is called by the CCA framework when it has 
   *           received a message for a property with Opcode 'SET' or 'PURESET'
   *           and there is no dedicated handler method defined in the message 
   *           map for this pair of FID and opcode. The user has to set the 
   *           application specific property to the requested value and the CCA
   *           framework then cares about informing the requesting client 
   *           as well as other registered clients.
   * \param    [IN] poMessage : Property to be set.
   * \param    [OUT] bPropertyChanged : Property changed flag to be set to TRUE 
   *                 if property has changed. Otherwise to be set 
   *                 to FALSE (default).
   * \param    [OUT] u16Error : Error code to be set if a CCA error occurs,
   *                 otherwise don't touch.
    **************************************************************************/
   virtual tBool bProcessSet(amt_tclServiceData* poMessage, 
         tBool& bPropertyChanged, tU16& u16Error);

   /***************************************************************************
   ******************************END OF PROTECTED******************************
   ***************************************************************************/


private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  devprj_tclService::devprj_tclService()
   **************************************************************************/
   /*!
   * \fn      devprj_tclService()
   * \brief   Defualt Constructor, will not be implemented.
   **************************************************************************/
   devprj_tclService();

   /***************************************************************************
   ** FUNCTION:  devprj_tclService::devprj_tclService(const devprj_tclServi...
   **************************************************************************/
   /*!
   * \fn      devprj_tclService(const devprj_tclService& oDevPrj_tclService)
   * \brief   Copy Consturctor, will not be implemented.
   **************************************************************************/
   devprj_tclService(const devprj_tclService& oDevPrj_tclService);

   /***************************************************************************
   ** FUNCTION:  devprj_tclService& devprj_tclService::operator=(const devp...
   **************************************************************************/
   /*!
   * \fn      devprj_tclService& operator=(const devprj_tclService& oDevPrj_tclService)
   * \brief   Assignment Operator, will not be implemented.
   **************************************************************************/
   devprj_tclService& operator=(const devprj_tclService& oDevPrj_tclService);

   /****************************************************************************
   * Handler method declarations used by message map.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnMSChangeDevProjEnable(amt_tclS...
   **************************************************************************/
   /*!
   * \fn      vOnMSChangeDevProjEnable(amt_tclServiceData* poMessage)
   * \brief   Extracts information from ChangeDevProjEnable MethodStart message
   *          and forwards request to SPI.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnMSChangeDevProjEnable(amt_tclServiceData* poMessage) const;

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnMSLaunchApp(amt_tclServiceData...
   **************************************************************************/
   /*!
   * \fn      vOnMSLaunchApp(amt_tclServiceData* poMessage)
   * \brief   Extracts information from LaunchApp MethodStart message and 
   *          forwards request to SPI.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnMSLaunchApp(amt_tclServiceData* poMessage);
     
   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnMSTerminateApp(amt_tclServiceD...
   **************************************************************************/
   /*!
   * \fn      vOnMSTerminateApp(amt_tclServiceData* poMessage)
   * \brief   Extracts information from TerminateApp MethodStart message and
   *          forwards request to SPI.
   * \param   [IN] poMessage : Pointer to message
    **************************************************************************/
   tVoid vOnMSTerminateApp(amt_tclServiceData* poMessage);
     
   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnMSGetAppIcon(amt_tclServiceDat...
   **************************************************************************/
   /*!
   * \fn      vOnMSGetAppIcon(amt_tclServiceData* poMessage)
   * \brief   Extracts information from GetApplicationIcon MethodStart message
   *          and forwards the request to SPI.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnMSGetAppIcon(amt_tclServiceData* poMessage) const;
     
   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnMSGetAppImage(amt_tclServiceDat...
   **************************************************************************/
   /*!
   * \fn      vOnMSGetAppImage(amt_tclServiceData* poMessage)
   * \brief   Extracts information from GetApplicationImage MethodStart message
   *          and forwards the request to SPI.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnMSGetAppImage(amt_tclServiceData* poMessage) const;

   /**************************************************************************
   ** FUNCTION:  tBool devprj_tclService::vOnMSSelectDevice(devprj_tStAp...
   **************************************************************************/
   /*!
   * \fn      vOnMSSelectDevice(amt_tclServiceData* poMessage)
   * \brief   Extracts information from SelectDevice MethodStart message
   *          and forwards request to SPI.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnMSSelectDevice(amt_tclServiceData* poMessage) const;

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnMSDispatchSwitchEvent(amt_tclS...
   **************************************************************************/
   /*!
   * \fn      vOnMSDispatchSwitchEvent(amt_tclServiceData* poMessage)
   * \brief   Extracts information from DispatchSwitchEvent MethodStart message 
   *          and forwards request to SPI.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnMSDispatchSwitchEvent(amt_tclServiceData* poMessage);
     
   /**************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnMSDispatchMenuEncdrChange(amt...
   *************************************************************************/
   /*!
   * \fn      vOnMSDispatchMenuEncdrChange(amt_tclServiceData* poMessage)
   * \brief   Extracts information from DispatchMenuEncdrChange MethodStart
   *          message and forwards request to SPI.
   * \param   [IN] poMessage : Pointer to message
   *************************************************************************/
   tVoid vOnMSDispatchMenuEncdrChange(amt_tclServiceData* poMessage) const;
     
   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnMSDevProjAlertBtnEvent(amt_tcl...
   **************************************************************************/
   /*!
   * \fn      vOnMSDevProjAlertBtnEvent(amt_tclServiceData* poMessage)
   * \brief   Extracts information from DevProjAlertButtoEvent MethodStart
   *          message and forwards request to SPI.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnMSDevProjAlertBtnEvent(amt_tclServiceData* poMessage);
     
   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnMSRequestAccDispCntxt(amt_tclS...
   ***************************************************************************/
   /*!
   * \fn      vOnMSSelectDevice(amt_tclServiceData* poMessage)
   * \brief   Extracts information from tclMsgRequestAccessoryDisplayContext 
   *          MethodStart message and forwards request to SPI.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnMSRequestAccDispCntxt(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnMSDiPoSwitchRequired(amt_tclS...
   ***************************************************************************/
   /*!
   * \fn      vOnMSDiPoSwitchRequired(amt_tclServiceData* poMessage)
   * \brief   Interface to check if role switch id required for the DiPo Device
   *          MethodStart message and forwards request to SPI.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnMSDiPoSwitchRequired(amt_tclServiceData* poMessage) const;

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnMSInvokeBTDeviceAction(amt_tclS...
   ***************************************************************************/
   /*!
   * \fn      vOnMSInvokeBTDeviceAction(amt_tclServiceData* poMessage)
   * \brief   Extracts information from InvokeBluetoothDeviceAction
   *          MethodStart message and forwards request to SPI.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnMSInvokeBTDeviceAction(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnMSStopMediaPlayback(amt_tclS...
   ***************************************************************************/
   /*!
   * \fn      vOnMSStopMediaPlayback(amt_tclServiceData* poMessage)
   * \brief   Extracts information from StopMediaPlayback
   *          MethodStart message and forwards request to SPI.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnMSStopMediaPlayback(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnMSResumeMediaPlayback(amt_tclS...
   ***************************************************************************/
   /*!
   * \fn      vOnMSResumeMediaPlayback(amt_tclServiceData* poMessage)
   * \brief   Extracts information from ResumeMediaPlayback
   *          MethodStart message and forwards request to SPI.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnMSResumeMediaPlayback(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnLoopbackMessage(...)
   ***************************************************************************/
   /*!
   * \fn      vOnLoopbackMessage(amt_tclServiceData* poMessage)
   * \brief   Generic loopback message handler.
   *          Extracts loopback messages and forwards its data to SPI.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnLoopbackMessage(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnSRStatus(...)
   ***************************************************************************/
   /*!
   * \fn      vOnSRStatus(tenSpeechRecStatus enSRStatus)
   * \brief   Interface to inform about SpeechRecognition status.
   * \param   [IN] enSRStatus : Speech recogntition enum
   **************************************************************************/
   tVoid vOnSRStatus(tenSpeechRecStatus enSRStatus);

   /**************************************************************************
   * ! Internal methods
   **************************************************************************/

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vGenerateError(const amt_tclServic...
   ***************************************************************************/
   /*!
   * \fn      vGenerateError(const amt_tclServiceData* pcooMessage)
   * \brief   Sends a MethodError message based on the the input message's FunctionID.
   * \param   [IN] pcooMessage : Pointer to message
   **************************************************************************/
   tVoid vGenerateError(const amt_tclServiceData* pcooMessage) const;

   /**************************************************************************
   ** FUNCTION:  tBool devprj_tclService::bPostResponse(const devprj_Fi...
   **************************************************************************/
   /*!
   * \fn      bPostResponse(const devprj_FiMsgBase& rfcooDevPrjMsg,
   *             const trUserContext& rfcorUsrCtxt)
   * \brief   Posts CCA message response for a DevPrj FI type message.
   * \param   [IN] rfcooDevPrjMsg : DevPrj FI message object.
   * \param   [IN] rfcorUsrCtxt : DevPrj message context.
   * \result  TRUE if method is posted successfully
   **************************************************************************/
   tBool bPostResponse(const devprj_FiMsgBase& rfcooDevPrjMsg,
         const trUserContext& rfcorUsrCtxt) const;
      
   /***************************************************************************
   ** FUNCTION:  tBool devprj_tclService::bHandoverStatusMsg(const devprj_Fi...
   **************************************************************************/
   /*!
   * \fn      bHandoverStatusMsg(const devprj_FiMsgBase& rfcooDevPrjStatusMsg,
   *             amt_tclServiceData& roOutMsg)
   * \brief   Creates a visitor message out of DevPrj status message in 
   *          rfcooDevPrjStatusMsg and hands it over to roOutMsg.
   * \param   [IN] rfcooDevPrjStatusMsg : DevPrj Error FI object.
   * \param   [OUT] roOutMsg    : Service data message
   * \param   [IN] rfcorUsrCtxt : DevPrj message context.
   * \result  TRUE if DevPrj Status message is handed over successfully.
   * \sa      bStatusMessageFactory()
   **************************************************************************/
   tBool bHandoverStatusMsg(const devprj_FiMsgBase& rfcooDevPrjStatusMsg,
         amt_tclServiceData& roOutMsg) const;

   /**************************************************************************
   ** FUNCTION:  tBool devprj_tclService::bHandoverMsgDevConnListStatus(amt...
   **************************************************************************/
   /*!
   * \fn      bHandoverMsgDevConnListStatus(amt_tclServiceData& rfoOutMsg)
   * \brief   Retrieves DeviceInfoList from SpiCmdInterface and creates a
   *          DevPrj DeviceConnectionList Status message. Hands over the 
   *          DevPrj status message to service data message.
   * \param   [OUT] rfoOutMsg : Service data message.
   * \sa      bStatusMessageFactory() & bHandoverStatusMsg()
   **************************************************************************/
   tBool bHandoverMsgDevConnListStatus(amt_tclServiceData& rfoOutMsg) const;

   /**************************************************************************
   ** FUNCTION:  tBool devprj_tclService::bHandoverMsgAppListStatus(amt_tcl...
   **************************************************************************/
   /*!
   * \fn      bHandoverMsgAppListStatus(amt_tclServiceData& rfoOutMsg)
   * \brief   Retrieves AppInfoList from SpiCmdInterface and creates a
   *          DevPrj ApplicationList Status message. Hands over the 
   *          DevPrj status message to service data message.
   * \param   [OUT] rfoOutMsg : Service data message.
   * \sa      bStatusMessageFactory() & bHandoverStatusMsg()
   **************************************************************************/
   tBool bHandoverMsgAppListStatus(amt_tclServiceData& rfoOutMsg);
   
   /***************************************************************************
   ** FUNCTION: tBool devprj_tclService::bHandoverMsgDevNotiEnableInfoStatus(amt...
   ***************************************************************************/
   /*!
   * \fn      bHandoverMsgDevNotiEnableInfoStatus(amt_tclServiceData& roOutMsg)
   * \brief   Retrieves Notification Info from SpiCmdInterface and creates a 
   *          DevPrj DeviceNotificationEnabledInfo Status message. Hands over  
   *          the DevPrj status message to service data message.
   * \param   [OUT] roOutMsg : Service data message.
   * \sa      bStatusMessageFactory() & bHandoverStatusMsg()
   **************************************************************************/
   tBool bHandoverMsgDevNotiEnableInfoStatus(amt_tclServiceData& roOutMsg) const;

   /***************************************************************************
   ** FUNCTION: tBool devprj_tclService::bHandoverMsgAppMetadataStatus(amt_t...
   ***************************************************************************/
   /*!
   * \fn      bHandoverMsgAppMetadataStatus(amt_tclServiceData& rfoOutMsg)
   * \brief   Retrieves Application Metadata information from SpiCmdInterface 
   *          and creates a DevPrj ApplicationMetaData Status message. Hands over  
   *          the DevPrj status message to service data message.
   * \param   [OUT] rfoOutMsg : Service data message.
   * \sa      bStatusMessageFactory() & bHandoverStatusMsg()
   **************************************************************************/
   tBool bHandoverMsgAppMetadataStatus(amt_tclServiceData& rfoOutMsg) const;
   
   /**************************************************************************
   ** FUNCTION:  tBool devprj_tclService::bHandoverMsgAudioSrcInfo(devprj..).
   **************************************************************************/
   /*!
   * \fn      bHandoverMsgAudioSrcInfo(amt_tclServiceData* poInMsg,
   *           amt_tclServiceData& rfoSourceInfo)
   * \brief   Retrieves Source Info from Audio Policy and creates a DevPrj
   *          SourceInfo Status message. Hands over the DevPrj status message 
   *          to service data message.
   * \param   [IN] poInMsg : Service data incoming message.
   * \param   [OUT] roOutMsg : Service data message.
   * \sa	  bStatusMessageFactory()
   **************************************************************************/	
   tBool bHandoverMsgAudioSrcInfo(amt_tclServiceData* poInMsg,
         amt_tclServiceData& roOutMsg) const;

   /**************************************************************************
   ** FUNCTION:  tBool devprj_tclService::bHandoverMsgAudioSrcAvailable(...
   **************************************************************************/
   /*!
   * \fn      bHandoverMsgAudioSrcAvailable(amt_tclServiceData& roOutMsg)
   * \brief   Creates a DevPrj SourceAvailable Status message.
   *          Hands over the DevPrj status message to service data mssage.
   * \param   [OUT] roOutMsg : Service data message.
   * \sa   bStatusMessageFactory()
   **************************************************************************/
   tBool bHandoverMsgAudioSrcAvailable(amt_tclServiceData& roOutMsg) const;

   /***************************************************************************
   ** FUNCTION: tBool devprj_tclService::bHandoverDevProjEnableStatus(amt_tclSe...
   ***************************************************************************/
   /*!
   * \fn      bHandoverDevProjEnableStatus(amt_tclServiceData& roOutMsg,
   *             tenDeviceCategory enDevCategory)
   * \brief   Retrieves ML/DiPO/AAP setting info from SpiCmdInterface and creates a
   *          DevPrj ML/DiPO/AAP Enable Status message. Hands over
   *          the DevPrj status message to service data message.
   * \param   [OUT] roOutMsg     : Service data message
   * \param   [IN] enDevCategory : Indicates device type (ML/Dipo/AAP).
   * \result  TRUE if DevPrj Status message is handed over successfully.
   * \sa      bStatusMessageFactory()
   **************************************************************************/
   tBool bHandoverDevProjEnableStatus(amt_tclServiceData& roOutMsg,
         tenDeviceCategory enDevCategory) const;

   /***************************************************************************
   ** FUNCTION: tBool devprj_tclService::bHandoverDriveModeStatus(amt_tclSe...
   ***************************************************************************/
   /*!
   * \fn      bHandoverDriveModeStatus(amt_tclServiceData& roOutMsg)
   * \brief   Retrieves Drive mode info from SpiCmdInterface and creates a
   *          DevPrj DriveMode Status message. Hands over  
   *          the DevPrj status message to service data message.
   * \param   [OUT] roOutMsg     : Service data message
   * \result  TRUE if DevPrj Status message is handed over successfully.
   * \sa      bStatusMessageFactory()
   **************************************************************************/
   tBool bHandoverDriveModeStatus(amt_tclServiceData& roOutMsg) const;

   /***************************************************************************
   ** FUNCTION: tBool devprj_tclService::bHandoverDeviceAppStateStatus(amt_tclSe...
   ***************************************************************************/
   /*!
   * \fn      bHandoverDeviceAppStateStatus(amt_tclServiceData& roOutMsg)
   * \brief   Retrieves Device App state info from SpiCmdInterface 
   *          the DevPrj status message to service data message.
   * \param   [OUT] roOutMsg     : Service data message
   * \result  TRUE if DevPrj Status message is handed over successfully.
   * \sa      bStatusMessageFactory()
   **************************************************************************/
   tBool bHandoverDeviceAppStateStatus(amt_tclServiceData& roOutMsg) const;

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vUpdateDayNightMode()
   ***************************************************************************/
   /*!
   * \fn      vUpdateDayNightMode()
   * \brief   Forwards Day/Night mode info to SPI.
   * \param   None.
   **************************************************************************/
   tVoid vUpdateDayNightMode();

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vPostSessionStatusInfo()
   ***************************************************************************/
   /*!
   * \fn     vPostSessionStatusInfo(t_U32 u32DeviceHandle,
   *             tenDeviceCategory enDevCat, tenSessionStatus enSessionStatus)
   * \brief  It notifies the client about the ML Session status updates
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] enDevCat   : Identifies the Device category.
   * \param  [IN] enSessionStatus : Session status
   **************************************************************************/
   t_Void vPostSessionStatusInfo(t_U32 u32DeviceHandle,
         tenDeviceCategory enDevCat,
         tenSessionStatus enSessionStatus);

   /***************************************************************************
   ** FUNCTION: tenKeyCode devprj_tclService::vInitializeBaseChannelMap
   ***************************************************************************/
   /*!
   * \fn      vInitializeBaseChannelMap()
   * \brief   Function to initialize the Base channel map with AV manager channels
   *          and the corresponding SPI internally used audio channels
   * 
   **************************************************************************/
   tVoid vInitializeBaseChannelMap();
   
   /***************************************************************************
   ** FUNCTION: tenKeyCode devprj_tclService::bGetSPIAudioContext
   ***************************************************************************/
   /*!
   * \fn      bGetSPIAudioContext()
   * \brief   Function to search the SPI Audio conttext corresponding to AV
   *          manager base channel
   * \param   enBaseChannel : [IN] AV Manager base channel 
   * \param   rfoAudioCntxt : [OUT] SPI audio context
   * \retval  bool : True, if search successfull. False otherwise.
   **************************************************************************/
   tBool bGetSPIAudioContext(most_fi_tcl_e8_AVManLogicalAVChannel::tenType enBaseChannel,
      tenAudioContext & rfoAudioCntxt);

   /***************************************************************************
   ** FUNCTION: t_Bool devprj_tclService::bRequestAltAudActivation
   ***************************************************************************/
   /*!
   * \fn      bRequestAltAudActivation()
   * \brief   Function to request for alternate Audio activation from AVM.
   * \param   cou8SrcNum : [IN] Source Number
   * \retval  bool : True, if activation successful. False otherwise.
   **************************************************************************/
   t_Bool bRequestAltAudActivation(const tU8 cou8SrcNum);

   /***************************************************************************
   ** FUNCTION: t_Bool devprj_tclService::bRequestAltAudDeactivation
   ***************************************************************************/
   /*!
   * \fn      bRequestAltAudDeactivation()
   * \brief   Function to request for alternate Audio deactivation from AVM.
   * \param   cou8SrcNum : [IN] Source Number
   * \retval  bool : True, if activation successful. False otherwise.
   **************************************************************************/
   t_Bool bRequestAltAudDeactivation(const tU8 cou8SrcNum);

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vRegisterForProperties()
   ***************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers to client-handlers for property notifications.
   * \param   None
   * \sa      vUnregisterForProperties()
   **************************************************************************/
   tVoid vRegisterForProperties();

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vUnregisterForProperties()
   ***************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Unregisters for property notifications from client-handlers
   * \param   None
   * \sa      vRegisterForProperties()
   **************************************************************************/
   tVoid vUnregisterForProperties();
   
   /***************************************************************************
   ** FUNCTION:  t_Bool devprj_tclService::u32GetNotificationActionID(devprj_tFiBtnAction...
   ***************************************************************************/
   /*!
   * \fn      u32GetNotificationActionID(devprj_tFiBtnAction oFiBtnAction,
   *              const trNotiData& rfcorNotiData)
   * \brief   Retrieves notification action ID of a notification event from
   *              AlertManager.
   * \param   [IN] oFiBtnAction :  DeviceProjection AlertButtonEvent action
   * \param   [IN] rfcorNotiData : Notification data
   **************************************************************************/
   t_U32 u32GetNotificationActionID(devprj_tFiBtnAction oFiBtnAction,
         const trNotiData& rfcorNotiData) const;

   /***************************************************************************
   ** FUNCTION:  t_Void devprj_tclService::vRegisterLocData( 
                           tenLocationDataType enLocDataType)
   ***************************************************************************/
   /*!
   * \fn      vRegisterLocData( tenLocationDataType enLocDataType)
   * \brief   Registers for the required location data/vehicle data properties
   * \param   [IN] enLocDataType :  Type of Location Data
   **************************************************************************/
   t_Void vRegisterLocData( tenLocationDataType enLocDataType);
   
   /***************************************************************************
   ** FUNCTION:  t_Void devprj_tclService::vUnregisterLocData( 
                           tenLocationDataType enLocDataType)
   ***************************************************************************/
   /*!
   * \fn      vUnregisterLocData( tenLocationDataType enLocDataType)
   * \brief   Unregisters from the required location data/vehicle data properties
   * \param   [IN] enLocDataType :  Type of Location Data
   **************************************************************************/
   t_Void vUnregisterLocData( tenLocationDataType enLocDataType);

   /***************************************************************************
   ** FUNCTION:  t_Void devprj_tclService::vOnGPSData(trGPSData rGPSData)
   ***************************************************************************/
   /*!
   * \fn      vOnGPSData(trGPSData rGPSData)
   * \brief   Interface to receive the GPS Data.
   * \param   [IN] rGPSData: GPS Data.
   * \retval  None
   **************************************************************************/
   t_Void vOnGPSData(trGPSData rGPSData);

   /***************************************************************************
   ** FUNCTION:  t_Void devprj_tclService::vOnSensorData(trSensorData rSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnSensorData(trSensorData rSensorData)
   * \brief   Interface to receive the Sensor Data.
   * \param   [IN] rSensorData: Sensor Data
   * \retval  None
   **************************************************************************/
   t_Void vOnSensorData(trSensorData rSensorData);

   /***************************************************************************
   ** FUNCTION:  t_Void devprj_tclService::vOnAccSensorData
   ** (const std::vector<trAccSensorData>& corfvecrAccSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnAccSensorData(const std::vector<trAccSensorData>& corfvecrAccSensorData)
   * \brief   Interface to receive the Acceleration Sensor Data.
   * \param   [IN] corfvecrAccSensorData: Acceleration Sensor Data
   * \retval  None
   **************************************************************************/
   t_Void vOnAccSensorData(const std::vector<trAccSensorData>& corfvecrAccSensorData);

   /***************************************************************************
   ** FUNCTION:  t_Void devprj_tclService::vOnGyroSensorData
   ** (const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnGyroSensorData(const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
   * \brief   Interface to receive the Gyro Sensor Data.
   * \param   [IN] corfvecrGyroSensorData: Gyro Sensor Data
   * \retval  None
   **************************************************************************/
   t_Void vOnGyroSensorData(const std::vector<trGyroSensorData>& corfvecrGyroSensorData);

   /***************************************************************************
   ** FUNCTION:  t_Void devprj_tclService::vOnGmlanData(trVehicleData rVehicleData)
   ***************************************************************************/
   /*!
   * \fn      vOnGmlanData(trVehicleData rVehicleData)
   * \brief   Interface to receive the Vehicle Data.
   * \param   [IN] rVehicleData: Vehicle Data.
   * \param   [IN] bSolicited: Indicates whether data is solicited or changed data
   * \retval  None
   **************************************************************************/
   t_Void vOnGmlanData(trVehicleData rVehicleData, t_Bool bSolicited);

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnTelephoneCallActivity()
   ***************************************************************************/
   /*!
   * \fn      vOnTelephoneCallActivity(t_Bool bCallActive)
   * \brief   Interface to recieve Telephone call activity info
   * \param   [IN] bCallActive : true - If any call is active, else false.
   **************************************************************************/
   tVoid vOnTelephoneCallActivity(t_Bool bCallActive);

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnTelephoneCallStatus()
   ***************************************************************************/
   /*!
   * \fn      vOnTelephoneCallStatus(
   *             const std::vector<trTelCallStatusInfo>& rfcoTelCallStatusList)
   * \brief   Interface to recieve info on each Telephone call
   * \param   [IN] rfcoTelCallStatusList : Telephone call info list
   **************************************************************************/
   tVoid vOnTelephoneCallStatus(
         std::vector<trTelCallStatusInfo> rfcoTelCallStatusList);

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::bQueueAlternateAudioReq()
   ***************************************************************************/
   /*!
   * \fn      bQueueAlternateAudioReq(t_U8 u8SrcNumber,  t_Bool bActivate)
   * \brief   Request to process audio ducking or queue it if busy
   * \param   [IN] u8SrcNumber : Telephone call info list
   **************************************************************************/
   tBool bQueueAlternateAudioReq(const tU8 cou8SrcNumber,  tBool bActivate);

   /***************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnOnstarDataSettingsUpdate()
   ***************************************************************************/
   /*!
   * \fn      vOnOnstarDataSettingsUpdate()
   * \brief   function to process the data settings update
   * \param   [IN] enDtataSetEvent : Data settings event
   **************************************************************************/
   t_Void vOnOnstarDataSettingsUpdate(tenOnstarSettingsEvents enDtataSetEvent);

   /***************************************************************************
   ** FUNCTION:  t_Void devprj_tclService::vUpdateAAPAudioContext(...)
   ***************************************************************************/
   /*!
   * \fn      vUpdateAAPAudioContext()
   * \brief   function to process audio context changes during AAP session
   * \param   [IN] rfcoBaseChannelMsg : BaseChannelStatus msg
   **************************************************************************/
   t_Void vUpdateAAPAudioContext(const devprj_tBaseChannelStatus& rfcoBaseChannelMsg);

   /***************************************************************************
   ** FUNCTION:  t_Void devprj_tclService::vOnOutsideTempUpdate()
   ***************************************************************************/
   /*!
   * \fn      t_Void vOnOutsideTempUpdate(t_Bool bValidityFlag,t_Double dTemp)
   * \brief   Interface to receive the Outside Temperature updates.
   * \param   [IN] bValidityFlag: Valid update
   * \param   [IN] dTemp : Temperature in Celcius
   * \retval  None
   **************************************************************************/
   t_Void vOnOutsideTempUpdate(t_Bool bValidityFlag,t_Double dTemp);

   /***************************************************************************
    ** FUNCTION:  t_Void devprj_tclService::vSubscribeForEnvData()
    ***************************************************************************/
   /*!
   * \fn      t_Void vSubscribeForEnvData(t_Bool bSubscribe)
   * \brief   Subscribe/Unsubscribe for Environment updates 
   * \param   [IN] bSubscribe: TRUE - Subscribe
   *                           FALSE - Unsubscribe
   * \retval  None
   **************************************************************************/
   t_Void vSubscribeForEnvData(t_Bool bSubscribe);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vRequestDeviceAuthorization
   ***************************************************************************/
   /*!
   * \fn     vRequestDeviceAuthorization
   * \brief  Interface to send notification to user to authorize the device
   * \param  [IN] rfrvecDevAuthInfo : List of device authorization info
   * \sa
   **************************************************************************/
   t_Void vRequestDeviceAuthorization(std::vector<trDeviceAuthInfo> &rfrvecDevAuthInfo);

   /***************************************************************************
   ** FUNCTION: t_Void devprj_tclService::vPostDipoRoleSwitchResponse
   ***************************************************************************/
   /*!
   * \fn     vPostDipoRoleSwitchResponse
   * \brief  Post response to DIPO role switch request
   * \param  [IN] bRoleSwitchRequired : true if role switch is required. otherwise false
   * \param  [IN] rfcorUsrCntxt : User context
   * \param  [IN] cou32DeviceHandle : Device Handle
   * \sa
   **************************************************************************/
   t_Void vPostDipoRoleSwitchResponse(t_Bool bRoleSwitchRequired,const t_U32 cou32DeviceHandle,
         const trUserContext& rfcorUsrCntxt);
		 
   /***************************************************************************
   ** FUNCTION: tVoid devprj_tclService::vUnblockVideo(t_U32 u32Dev...
   ***************************************************************************/
   /*!
   * \fn     vUnblockVideo(t_U32 u32DeviceHandle, tenResponseCode enResponseCode)
   * \brief  Unblocks the video for MirrorLink.
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] enResponseCode  :  Provides result from the operation.
   * \sa     vLaunchAppResult()
   **************************************************************************/   
   tVoid vUnblockVideo(t_U32 u32DeviceHandle, tenResponseCode enResponseCode);
   
   /***************************************************************************
   ** FUNCTION: t_U32 devprj_tclService::vEvaluateSelectDevice
   **           (t_U32 u32DeviceHandle, tenDeviceConnectionReq enDevConnReq, ..)
   ***************************************************************************/
   /*!
   * \fn     vEvaluateSelectDevice(t_U32 u32DeviceHandle,
   *         tenDeviceConnectionReq enDevConnReq, tenResponseCode enResponseCode,
			 t_Bool bIsPairingReq)     
   * \brief  Evaluates selected DeviceHandle to be sent.
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] enDevConnReq	  : Identifies the Connection Request.
   * \param  [IN] enResponseCode  :  Provides result from the operation.
   * \param	 [IN] bIsPairingReq   : indicates if BT pairing required
   * \result Evaluated device handle Id.   
   * \sa     vPostSelectDeviceResult()
   **************************************************************************/   
   t_U32 vEvaluateSelectDevice(t_U32 u32DeviceHandle, 
	  tenDeviceConnectionReq enDevConnReq, tenResponseCode enResponseCode,
	  t_Bool bIsPairingReq);
	  
   /**************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vGenerateAppList(devprj_tStAppLis...
   **************************************************************************/
   /*!
   * \fn      vGenerateAppList(devprj_tStAppList& rfoStAppList,
              const std::vector<trAppDetails>& corfvecrSpiAppDetailsList)
   * \brief   Generates DevPrj AppList using SPI AppInfoList.
   * \param   [OUT] rfoStAppList : DevPrj application list.
   * \param   [IN] corfvecrSpiAppDetailsList : SPI AppDetails vector.
   * \sa      bHandoverMsgAppListStatus(amt_tclServiceData& roOutMsg)
   **************************************************************************/   
   tVoid vGenerateAppList(devprj_tStAppList& rfoStAppList,
	  const std::vector<trAppDetails>& corfvecrSpiAppDetailsList);

   /**************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vCopyAppMetadata(devprj_tStAppMet...
   **************************************************************************/
   /*!
   * \fn      vCopyAppMetadata(devprj_tStAppMetaData& rfoStMetaData)
   * \brief   Populates DevPrj AppMetadata with AppMetatdata details 
   * \param   [OUT] rfoStMetaData : DevPrj AppMetadata.
   * \sa      bHandoverMsgAppMetadataStatus(amt_tclServiceData& roOutMsg)
   **************************************************************************/
   tVoid vCopyAppMetadata(devprj_tStAppMetaData& rfoStMetaData) const;
   
   /**************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vCopyDeviceInfoList(const std::vec...
   **************************************************************************/
   /*!
   * \fn      vCopyDeviceInfoList(const std::vector<trDeviceInfo>& corfvecrDeviceInfoList,
              devprj_tStDevConnList& rfoStDevList) const
   * \brief   Copies SPI DeviceInfoList details into DevPrj DeviceConnectionList.
   *          Adds the generated DeviceListItem in DeviceList.
   * \param   [IN] corfvecrDeviceInfoList : SPI device info list.
   * \param   [OUT] rfoStDevList : DevPrj device list.
   * \sa      bHandoverMsgDevConnListStatus(amt_tclServiceData& roOutMsg)
   **************************************************************************/
   tVoid vCopyDeviceInfoList(const std::vector<trDeviceInfo>& corfvecrDeviceInfoList, devprj_tStDevConnList& rfoStDevList) const;
   
   /**************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vSetDeviceVersion(const trDeviceI...
   **************************************************************************/
   /*!
   * \fn      vSetDeviceVersion(const trDeviceInfo& corfrDevInfo,
              devprj_tFiDevLstItem& rfoDevLstItem) const
   * \brief   Retrieves the device version information.
   *          Sets the device version of the DeviceListItem to the 
   *          retrieved version.
   * \param   [IN] corfrDevInfo : Device Info object for retrieving the version.
   * \param   [OUT] rfoDevLstItem : Device list item.
   * \sa      vCopyDeviceInfoList()
   **************************************************************************/
   tVoid vSetDeviceVersion(const trDeviceInfo& corfrDevInfo, devprj_tFiDevLstItem& rfoDevLstItem) const;

   /**************************************************************************
   ** FUNCTION:  tVoid devprj_tclService::vOnMsSrcActivity(amt_tclServiceData*...
   **************************************************************************/
   /*!
   * \fn      vOnMsSrcActivity(amt_tclServiceData* poMessage)
   * \brief   Interface to receive SrcActvity MethodStart message
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnMsSrcActivity(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void devprj_tclService::vOnValetModeChange()
   **************************************************************************/
   /*!
   * \fn      vOnValetModeChange(t_Bool bValetModeActive)
   * \brief   Interface to handle Valet mode changes
   * \param   [IN] bValetModeActive : True if Valet mode is active, else False
   **************************************************************************/
   t_Void vOnValetModeChange(t_Bool bValetModeActive);

   /***************************************************************************
   * ! Data members
   ***************************************************************************/
      
   /*
    * Main application pointer
    */
   devprj_tclMainApp*      m_poMainAppl;
	
   /*
    * Gesture Service client-handler
    */
   spi_tclGestureService*  m_poGestureServc;

   /*
    * SPI Command Interface pointer
    */
   spi_tclCmdInterface*    m_poSpiCmdInterface;

   /*
    * Alert Manager FBlock client-handler pointer
    */
   spi_tclAlertMngrClient*    m_poAlertMngrClient;

   /*
    * SytemState FBlock client-handler
    */
   spi_tclSystemStateClient*  m_poSystemStateClient;

   /*
   * CenterStackHMI FBlock client-handler
   */
   spi_tclCenterStackHmiClient*  m_poCenterStackHmiClient;

   /*
    * Onstar Data FBlock client-handler
    */
   spi_tclOnstarDataClient*  m_poOnstarDataClient;

   /*
    * Onstar Personal Calling FBlock client-handler
    */
   spi_tclOnstarCallClient*  m_poOnstarCallClient;

   /*
    * Speech HMI FBlock client-handler pointer
    */
   spi_tclSpeechHMIClient*   m_poSpeechHMIClient;

   /*
    * Navigation FBlock client-handler pointer
    */
   spi_tclNavigationClient*  m_poNavigationClient;

   /*
    * Telephone FBlock client-handler pointer
    */
   spi_tclTelephoneClient*   m_poTelephoneClient;

   /*
    * Device Projection Key Configuration pointer
    */
   devprj_tclKeyConfig* m_poKeyConfig;

   /*
   * GM LAN Gateway pointer
   */
   spi_tclGMLANGatewayClient*  m_poGMLANClient;

   /*
    * Position FI client handler pointer
    */
   spi_tclPosDataClientHandler*  m_poPosDataClient;

   /*
    * Sensor FI client handler pointer
    */
   spi_tclSensorDataClientHandler*  m_poSensorDataClient;
   
   /*
    * Onstar Nav FBlock client-handler pointer
    */
   spi_tclOnstarNavClient*  m_poOnstarNavClient;

   /*
    * Onstar AVManager FBlock client-handler pointer
    */
   spi_tclAVManagerClient*  m_poAVManagerClient;

   /*
    * Member variable to maintain the current vehicle mode
    */
   t_Bool m_bDriveModeEnabled;
	
   /*
    * SystemState Service Day/Night mode status
    */
   tenSystemStateDayNightMode   m_enSysStateDayNightMode;

   /*
    * CenterStackHMI Service Day/Night mode override status
    */
   tenCenterStackHmiDayNightOverride   m_enCntrStackHmiDayNightMode;

   /*
    * Speech Recognition Session Status - Started/Not started
    */
   tenSpeechRecBtnEventResult   m_enSpeechRecSessionStatus;
   
   /*
    * Map of AV manager base channel with SPI internal channels
    */
   std::map<most_fi_tcl_e8_AVManLogicalAVChannel::tenType, tenAudioContext> m_mapBaseChannel;

   //! To keep the last allocated channel
   most_fi_tcl_e8_AVManLogicalAVChannel::tenType m_CurrAllocatedChannel;

   /*
    * Source F-Block Number
    */
   tU8 m_u8AudioSourceFBlock;

   /*
    * DevPrj BluetoothDeviceStatus message object
    */
   devprj_tStBTDeviceStatus m_oStBTDevStatusMsg;
   
   /*
    * DevPrj DisplayProjectionRequest message object
    */
   devprj_tStDispProjReq  m_oStDispProjRequest;

   /*
    * DevPrj MLActiveApp message object
    */
   devprj_tStMLActiveApp  m_oStMLActiveAppMsg;

   /*
    * DevPrj BTPairingRequired message object
    */
   devprj_tStBTPairRequired   m_oStBTPairRequired;

   /*
    * DevPrj devprj_tStProjDevAuthAndAccessibility message object
    */
   devprj_tStProjDevAuthAndAccessibility   m_oProjDevAuthAndAccess;

   /*
    * Speech app state
    */
   tenSpeechAppState m_enSpeechAppState;

   /*
    * Phone app state
    */
   tenPhoneAppState m_enPhoneAppState;

   /*
    * Nav app state
    */
   tenNavAppState m_enNavAppState;

   /*
    *Embedded Navigation status
    */
   t_Bool m_bNavStatus;

   /*
    *Onstar Navigation status.
    */
   t_Bool m_bOnStarNavStatus;
   
   /*
   *Onstar Phone status.
   */   
   t_Bool m_bOnstarPhoneActive;
   
   /*
   *Native Phone status.
   */  
   t_Bool m_bNativePhoneActive;
   
   /*
   *Onstar Speech status.
   */
   t_Bool m_bOnstarSpeechState;
   
   /*
   *Navigation Speech status.
   */
   t_Bool m_bNativeSpeechActive;
   
   /*
    * Flag that indicates the MuteState
   */
   tBool m_bMuteFlag;

   /*
    * GPS Configuration-Onstar,Nav or None
    */
   tenGPSConfig m_enGPSConfig;

   /*
    * System variant type (color/nav)
    */
   tenSystemVariant  m_enSystemVariant;

   /*
   *Device session status
   */
   tenSessionStatus m_enDeviceSessionStatus;

   //! To maintain the src availability flag
   t_Bool m_bSrcAvailSet;

   /*
   *DiPO feature support indicator
   */
   t_Bool m_bIsDiPOSupported;

   /*
    * Flag that indicates whether CarPlay media is active
    */
   t_Bool  m_bCarPlayMediaActive;
   
   /*
    * CarPlay audio device name
    */
   t_String m_szCarPlayMediaAudDevice;

   /*
    * Lock object for CarPlayMediaActive flag
    */
   Lock    m_oCarPlayMediaStatusLock;

   /*
    * Lock object for CarPlayMediaAudDevice data
    */
   Lock    m_oCarPlayMediaAudDeviceLock;

   /*
    * Lock object for Duck/Alternate Audio flag
    */
   Lock    m_oAudioDuckLock;
   
   /*
    * Lock object for Display Projection /Black Screen flag
    */
   Lock    m_oDispProjLock;

   //! media metadata
   trAppMediaMetaData m_rAppMediaMetaData;

   //! media playtime
   trAppMediaPlaytime m_rAppMediaPlaytime;

   //! Phone data
   trAppPhoneData m_rAppPhoneData;


   /*
    * Last main audio source info
    */
   most_fi_tcl_AVManBaseStatusItem   m_oAvManLastMainAudioSrc;

   //! Stores current status of alternate audio
   trAudioStatus m_rAltAudioStatus;

   //! Stores queued requests for alternalte audio
   trAudioRequest m_rAltAudioRequest;

   //! Lock for Audio status
   Lock  m_oAltAudioStatusLock;

   //! Lock for Audio Request
   Lock  m_oAltAudioRequestLock;

   //! Indicates whether Main channel is allocated to Device Projection
   t_Bool m_bIsDevPrjMainAudioActive;

   //! Indicates whether Main channel is allocated to Device Projection, but in suspended state
   //! (occurs during Speech, Phone call, etc.)
   t_Bool m_bIsDevPrjMainAudioSuspended;

   /*
    * Lock object for m_bIsDevPrjMainAudioActive and m_bIsDevPrjMainAudioSuspended flags
    */
   Lock    m_oDevPrjMainAudioStatusLock;

   //! Indicates whether Speech channel activation is requested by SPI
   t_Bool m_bSpeechChnRequested;

   //! Lock object for m_bSpeechChnRequested flag
   Lock   m_oSpeechChnReqLock;

   std::stack<tenAudioContext> m_enAudioCntxtUpdateStack;

   //! Device list item containing authorization and access information
   devprj_tFiDevUserParamItem m_oDevUserParamItem;

   /***************************************************************************
   ****************************END OF PRIVATE**********************************
   ***************************************************************************/

   /****************************************************************************
   * Message map definition macro
   ***************************************************************************/
   DECLARE_MSG_MAP(devprj_tclService)

};

#endif /* _DEVPROJ_SERVICE_H_ */

///////////////////////////////////////////////////////////////////////////////
// <EOF>
