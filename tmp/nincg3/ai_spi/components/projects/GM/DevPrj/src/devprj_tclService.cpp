/*!
 *******************************************************************************
 * \file              devprj_tclService.cpp
 * \brief             Device Projection Service class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Device Projection Service class to implement the service 
                 provided by Device Projection component. 
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 21.10.2013 |  Ramya Murthy                | Initial Version
 06.12.2013 |  Ramya Murthy                | Included AlertManager client-handler
                                             and few RespIntf methods implementation.
 30.01.2014 |  Hari Priya E R(RBEI/ECP2)   | Included changes for hard key handling
 12.02.2014 |  Ramya Murthy                | Included BT Settings client-handler
 14.02.2014 |  Ramya Murthy                | Adapted to FCat v4.5.2 and SPI
                                             HMI API document v1.5 changes.
 16.03.2014 |  Ramya Murthy                | Included SelectDevice & GetApplicationIcon/Image
                                             error handling, and printing enum names in traces.
 26.03.2014 |  Ramya Murthy                | Removed OSAL Asserts and included
                                             SetDeviceUsagePreference response handling.
 06.04.2014 |  Ramya Murthy                | Initialisation sequence implementation
 29.04.2014 |  Shiva Kumar G               | Locale Mapping for Application Filtering
 18.05.2014 |  Ramya Murthy                | DayNight mode implementation
 25.05.2014 |  Hari Priya E R              | Removed function for setting screen variant
 10.06.2014 |  Ramya Murthy                | Audio policy redesign implementation.
 27.06.2014 |  Ramya Murthy                | Changes for Client handlers - creation,
                                             subscription and collecting data via loopback.
 02.07.2014 |  Shihabudheen P M            | Modified for IIl extension integration
 03.07.2014 |  Hari Priya E R              | Included changes for SWC key Handling
 31.07.2014 |  Ramya Murthy                | SPI feature configuration via LoadSettings()
 14.08.2014 |  Ramya Murthy                | Logic for ML Notifications via alerts
 11.09.2014 |  Ramya Murthy                | Implementation for of MLActiveApp, and adapted 
                                             RequestAccessoryDisplayContext & DisplayProjectionRequest for ML
 27.09.2014 |  Shihabudheen P M            | CarPaly app state handling added.
 01.10.2014 |  Ramya Murthy                | Added Telephone client handler (moved from BT Manager)
 07.10.2014 |  Ramya Murthy                | Implemented BTPairingRequired property
 13.10.2014 |  Hari Priya E R              | Added changes related to GMLAN gateway client interface
 06.11.2014 |  Hari Priya E R              | Added changes to set Client key capabilities
 17.11.2014 |  Ramya Murthy                | LocationData registration based on EOL and GMLANGPSConfiguration
 17.11.2014 |  Shihabudheen P M            | Added session status update for CarPlay 
 01.12.2014 |  Ramya Murthy                | Fix for ML LaunchApp (GMMY16-21149) and 
                                             implementation of ML & DiPO enable calibrations.
 05.12.2014 |  Ramya Murthy                | Implementation of revised CarPlay media concept 
                                             and Application metadata.
 22.12.2014 |  Ramya Murthy                | Fix for USB audio restoration after Siri (GMMY16-22219)
 10.02.2015 |  Shihabudheen P M            | Added method to receive onstar call and onstar emergency call  
 02.03.2015 |  Ramya Murthy                | Added clearing of audio SrcActive Flag on media release 
                                             (Fix for GMMY16-25651, GMMY16-25594)
 17.03.2015 | Shihabudheen P M             | Added vOnOnstarDataSettingsUpdate()
 16.04.2015 | Shitanshu Shekhar            | Added Android Auto specific conditions 
                                             in vRegisterLocData() for vehicle data
 27.05.2015 | Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors
 29.05.2015 | Vinoop U                     |implementation for forwarding Media metadata to cluster  in Android auto	
 03.06.2015 | Ramya Murthy                 | Fix for AAP music not resuming after Native speech issue. (GMMY17-3041)
 30.06.2015 | Ramya Murthy                 | Changes to read Gear & ParkBrake info from VehicleMovementState,
                                             ElectricParkBrake & ParkBrakeSwitchState properties.
 09.07.2015 | Ramya Murthy                 | Fix for No iPod audio while Android Auto active (GMMY17-3482)
 17.07.2015 | Sameer Chandra               | Implemented vOnMSDispatchMenuEncdrChange method
 09.07.2015 | Ramya Murthy                 | Fix for restoring audio context after phone call (GMMY17-6634)
 29.09.2015 | Ramya Murthy                 | Revised logic for subscribing location data for different SPI technologies,
                                            and disabling sending Onstar data for AndroidAuto
30.10.2015  | Shiva Kumar G                | Implemented ReportEnvironment Data feature
14.12.2015  | Ramya Murthy                 | Fix for GMMY17-11425 and GMMY17-12040
25.01.2016  | Rachana L Achar              | Logiscope improvements
14.12.2015  | Ramya Murthy                 | Changes to deactivate projection device during Valet mode (GMMY17-10540)

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "devprj_tclMainApp.h"
#include "devprj_tclKeyConfig.h"
#include "spi_tclAlertMngrClient.h"
#include "spi_tclSystemStateClient.h"
#include "spi_tclCenterStackHmiClient.h"
#include "spi_tclOnstarDataClient.h"
#include "spi_tclOnstarCallClient.h"
#include "spi_tclSpeechHMIClient.h"
#include "spi_tclNavigationClient.h"
#include "spi_tclTelephoneClient.h"
#include "spi_tclOnstarNavClient.h"
#include "spi_tclPosDataClientHandler.h"
#include "spi_tclSensorDataClientHandler.h"
#include "spi_tclGMLANGatewayClient.h"
#include "spi_tclConfigReader.h"
#include "spi_tclMPlayClientHandler.h"
#include "spi_tclAVManagerClient.h"

#include "spi_tclCmdInterface.h"
#include "FIMsgDispatch.h"
using namespace shl::msgHandler;
#include "StringHandler.h"
#include "spi_tclGestureService.h"

#include "devprj_tclService.h"

#define MIDW_FI_S_IMPORT_INTERFACE_FI_TYPES
#include "midw_fi_if.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_TCLSERVICE
#include "trcGenProj/Header/devprj_tclService.cpp.trc.h"
#endif
#endif

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
#define DEVPROJ_SERVICE_FI_MAJOR_VERSION  MOST_DEVPRJFI_C_U16_SERVICE_MAJORVERSION
#define DEVPROJ_SERVICE_FI_MINOR_VERSION  MOST_DEVPRJFI_C_U16_SERVICE_MINORVERSION
#define DEVPROJ_SERVICE_FI_PATCH_VERSION  0

#define DEVPRJ_FBLOCK_ID                  218
//#define ONSTAR_PERSONALCALLING_FBLOCK_ID  199
#define MPLAYER_FBLOCK_ID                 171

#define UNDUCK_TIMER_ID                         01
#define UNDUCK_TIMER_FIRST_TICK    ((OSAL_tMSecond)500)
#define UNDUCK_TIMER_INTERVAL       0

#define DIPO_BLACKSCREEN_RECOVERY_TIMER_ID                  02
#define DIPO_BLACKSCREEN_RECOVERY_TIMER_FIRST_TICK          ((OSAL_tMSecond)500)
#define DIPO_BLACKSCREEN_RECOVERY_TIMER_TIMER_INTERVAL       0

//Macro to set BluetoothDeviceStatus message data to default
#define CLEAR_BTDEVSTATUS_MSG(MESSAGE_OBJ) \
      MESSAGE_OBJ.u8BTDeviceHandle = 0;     \
      MESSAGE_OBJ.u32DeviceHandle = 0;   \
      MESSAGE_OBJ.e8BTChangeInfo.enType = devprj_tFiBTChangeInfo::FI_EN_E8NO_CHANGE;   \
      MESSAGE_OBJ.bCallActiveStatus = false;

//Macro to set BTPairingRequired message data to default
#define CLEAR_BTPAIR_REQUIRED_MSG(MESSAGE_OBJ)   \
      MESSAGE_OBJ.vDestroy();  \
      MESSAGE_OBJ.bPairingReqd = false;
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \most_fi_tcl_DevPrjDeviceUserParametersItem devUserParamItem
 * \brief Item of the device list.
 */
typedef most_fi_tcl_DevPrjDeviceUserParametersItem tDevUserParamItem;

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/
/*!
* \brief Mapped the elements as mentioned in the GIS337 specification.
* All the t_U8 values(0x00 - 0xFF) which are not mentioned here, are by default equivalent to 0xFF - e8WORLD.
* 0xFF is used to check the end of the array. If any new elements needs to be added,
* insert before that
*
* EOL Value  -- Vehicle sale region -- Guidelines to be followed (GIS 337 )
*   0        --   USA || Canada     -- NA Guidelines
*   2        --   Mexico            -- NA Guidelines
*   5        --   Europe            -- EU Guidelines
*   9        --   Japan             -- JAPAN Guidelines
*   C        --   Philippines       -- NA Guidelines
*   others   --    XXXX             -- GLOBAL Guidelines
*/
static const trEOLRegionMap saEOLRegionMap[]={{0x0,e8_USA},{0x2,e8_AMERICA},{0x5,e8_EU},{0x8,e8_CHN},
{0x9,e8_JPN},{0xC,e8_AMERICA},{0xFF,e8_WORLD}};

//! Maximum value for U32 data type
static const t_U32 cou32InvalidHandle         = 0;
//! Device handle value to indicate all devices
static const t_U32 cou32DevHandle_AllDevices  = 0xFFFFFFFF;
//! Device handle for a select request error (32-bit)
static const t_U32 cou32DevHandle_SelectError  = 0x7FFFFFFF;
//! Device handle for a deselect request (16-bit)
static const t_U16 cou16DevHandle_Deselect    = 0xFFFF;
//! Device handle for a deselect request (32-bit)
static const t_U32 cou32DevHandle_Deselect    = 0xFFFFFFFF;
//! Mask bytes for retrieving lower 16 bits
static const t_U16 cou16DevHandle_Mask        = 0xFFFF;
//! Mask bytes for setting BT pairing flag (32-bit)
static const t_U32 cou32BTPairingFlag         = 0x80000000;
static const t_U16 scou16SingleConnectedDevices = 1;
static const t_U16 scou16NoConnectedDevice = 0;

static const t_U8 scu8AudMainSrcNum    = 0x01;
static const t_U8 scu8AudSpeechSrcNum  = 0x03;
static const t_U8 scu8AltAudSrcNum     = 0x06;
static const t_U8 scu8DuckAudSrcNum    = 0x04;

static const t_U8 scou8FirstPhoneIndex = 0;
static const t_U8 scou8PhoneListSize = 1;

static t_Bool sbAudDuckEnabled      = false;
static t_Bool sbAltAudActivated     = false;

static t_Bool sbDipoProjTimerRun    = false;

//! Empty location data callback structures
static trLocationDataCallbacks rDataSvcLocDataCb;
static trSensorDataCallbacks rDataSvcSensorDataCb;
static trVehicleDataCallbacks rDataSvcVehicleDataCb;
static trEnvironmentDataCbs rEnvironmentDataCbs;

//! Structure to store context of StopMediaPlayback request from MediaPlayer
static trUserContext rStopMediaPlaybackCtxt;

//! Stores Projection device speech app state
static tenSpeechAppState senDeviceSpeechAppState = e8SPI_SPEECH_UNKNOWN;

//! Source Number for Audio Ducking Feature
static t_U8 su8AudDuckSrcNum      = 0;

static t_Bool bUnduckTimerRunning = false;

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported



/******************************************************************************
 ** CCA MESSAGE MAP                                                
******************************************************************************/
BEGIN_IIL_MSG_MAP(devprj_tclService)

   /* -------------------------------Methods.---------------------------------*/
   ON_MESSAGE_SVCDATA( MOST_DEVPRJFI_C_U16_LAUNCHAPP, 
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART, vOnMSLaunchApp)
   ON_MESSAGE_SVCDATA( MOST_DEVPRJFI_C_U16_TERMINATEAPP, 
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART, vOnMSTerminateApp)
   ON_MESSAGE_SVCDATA( MOST_DEVPRJFI_C_U16_GETAPPLICATIONICON,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART, vOnMSGetAppIcon)
   ON_MESSAGE_SVCDATA( MOST_DEVPRJFI_C_U16_DISPATCHSWITCHEVENT, 
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART, vOnMSDispatchSwitchEvent)
   ON_MESSAGE_SVCDATA( MOST_DEVPRJFI_C_U16_DISPATCHMENUENCODERCHANGE,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART, vOnMSDispatchMenuEncdrChange)
   /*ON_MESSAGE_SVCDATA( MOST_DEVPRJFI_C_U16_GETAPPLICATIONIMAGE,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART, vOnMSGetAppImage)*/
   ON_MESSAGE_SVCDATA( MOST_DEVPRJFI_C_U16_DEVICEPROJECTIONALERTBUTTONEVENT,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART, vOnMSDevProjAlertBtnEvent)
   ON_MESSAGE_SVCDATA( MOST_DEVPRJFI_C_U16_CHANGEDEVICEPROJECTIONENABLE,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART, vOnMSChangeDevProjEnable)
   ON_MESSAGE_SVCDATA( MOST_DEVPRJFI_C_U16_SELECTDEVICE,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART, vOnMSSelectDevice)
   ON_MESSAGE_SVCDATA( MOST_DEVPRJFI_C_U16_REQUESTACCESSORYDISPLAYCONTEXT_,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART, vOnMSRequestAccDispCntxt)
   ON_MESSAGE_SVCDATA( MOST_DEVPRJFI_C_U16_DIPOROLESWITCHREQUIRED,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART, vOnMSDiPoSwitchRequired)
   ON_MESSAGE_SVCDATA( MOST_DEVPRJFI_C_U16_INVOKEBLUETOOTHDEVICEACTION,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART, vOnMSInvokeBTDeviceAction)

   /*Internal (loopback) messages mapping*/
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_SYSSTATE_DAYNIGHTMODE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_CNTRSTCKHMI_DAYNIGHTMODE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_ONSTAR_C2_CALLSTATE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_ONSTAR_ACCEPTCALLERROR,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_ONSTAR_HANGUPERROR,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_ONSTAR_TBTACTIVESTATE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_SPEECHREC_BTNEVENTRESULT,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_SPEECHREC_SRSTATUS,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_NAV_GUIDANCESTATUS,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_GMLAN_GPSCONFIG,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_SYSSTATE_TIMEOFDAY,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_ONSTAR_C1_CALLSTATE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_ONSTAR_EMERGENCY_CALLSTATE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_ONSTAR_SETTINGS_UPDATE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_DATA_SERVICE_SUBSCRIBE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_VEHICLE_BTADDRESS_UPDATE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_VINDIGITS10TO17,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_SYSSTATE_VALETMODE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
   
#ifdef VARIANT_S_FTR_ENABLE_GM_CARPLAY_MEDIA
   ON_MESSAGE_SVCDATA( MOST_DEVPRJFI_C_U16_STOPMEDIAPLAYBACK,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART, vOnMSStopMediaPlayback)
   ON_MESSAGE_SVCDATA( MOST_DEVPRJFI_C_U16_RESUMEMEDIAPLAYBACK,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART, vOnMSResumeMediaPlayback)
   ON_MESSAGE_SVCDATA( SPI_C_U16_IFID_MPLAY_REQAUDIODEV_RESULT,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnLoopbackMessage)
#endif

   ON_MESSAGE_SVCDATA( MOST_AVMANFI_C_U16_BASECHANNELSTATUS,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnBaseChannelStatusUpdate)
   ON_MESSAGE_SVCDATA( MOST_AVMANFI_C_U16_MUTE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnMuteStatus)
   ON_MESSAGE_SVCDATA( IIL_EN_SOURCEACTIVITY,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART, vOnMsSrcActivity)
   
   //!@TODO: To be removed when messages are implemented in CmdInterface
   ON_MESSAGE_SVCDATA( MOST_DEVPRJFI_C_U16_GETAPPLICATIONIMAGE,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART, vGenerateError)
   

   /* -------------------------------Methods.---------------------------------*/

END_MSG_MAP()


/***************************************************************************
 ** FUNCTION:  devprj_tclService::devprj_tclService(devprj_tclMainApp* po...
 **************************************************************************/
devprj_tclService::devprj_tclService(devprj_tclMainApp* poMainAppl)
   : iil_tclISource(
      poMainAppl,                          /* Application Pointer */
      CCA_C_U16_SRV_FB_DEVICEPROJECTION,   /* ID of offered Service */
      DEVPROJ_SERVICE_FI_MAJOR_VERSION,    /* MajorVersion of offered Service */
      DEVPROJ_SERVICE_FI_MINOR_VERSION,   /* MinorVersion of offered Service */
      DEVPROJ_SERVICE_FI_PATCH_VERSION),
     m_poMainAppl(poMainAppl),
     m_poGestureServc(OSAL_NULL),
     m_poSpiCmdInterface(OSAL_NULL),
     m_poAlertMngrClient(OSAL_NULL),
     m_poSystemStateClient(OSAL_NULL),
     m_poCenterStackHmiClient(OSAL_NULL),
     m_poOnstarDataClient(OSAL_NULL),
     m_poOnstarCallClient(OSAL_NULL),
     m_poSpeechHMIClient(OSAL_NULL),
     m_poNavigationClient(OSAL_NULL),
     m_poTelephoneClient(OSAL_NULL),
     m_poOnstarNavClient(OSAL_NULL),
     m_poAVManagerClient(OSAL_NULL),
     m_poKeyConfig(OSAL_NULL),
     m_poGMLANClient(OSAL_NULL),
     m_poPosDataClient(OSAL_NULL),
     m_poSensorDataClient(OSAL_NULL),
     m_enSysStateDayNightMode(e8DAY_MODE),
     m_enCntrStackHmiDayNightMode(e8OVERRIDE_AUTOMATIC),
     m_bDriveModeEnabled(false),
     m_CurrAllocatedChannel(most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_NONE),
     m_u8AudioSourceFBlock(0),
     m_enSpeechAppState(e8SPI_SPEECH_UNKNOWN),
     m_enPhoneAppState(e8SPI_PHONE_UNKNOWN),
     m_enNavAppState(e8SPI_NAV_UNKNOWN),
     m_bNavStatus(false),
     m_bOnStarNavStatus(false),
     m_bMuteFlag(false),
     m_enGPSConfig(e8GMLAN_GPS_NONE),
     m_enSystemVariant(e8VARIANT_COLOR),
     m_bSrcAvailSet(true),//this must be set to TRUE, since it is by default set to TRUE in IIL
     m_enDeviceSessionStatus(e8_SESSION_INACTIVE),
     m_bCarPlayMediaActive(false),
     m_bIsDiPOSupported(false),
     m_bIsDevPrjMainAudioActive(false),
     m_bSpeechChnRequested(false),
     m_bNativePhoneActive(false),
     m_bOnstarPhoneActive(false),
     m_bOnstarSpeechState(false),
     m_bNativeSpeechActive(false)
{
   ETG_TRACE_USR1(("devprj_tclService::devprj_tclService entered "));
   SPI_NORMAL_ASSERT(OSAL_NULL == m_poMainAppl);

}//! end of devprj_tclService()

/***************************************************************************
 ** FUNCTION:  virtual devprj_tclService::~devprj_tclService();
 **************************************************************************/
devprj_tclService::~devprj_tclService()
{
   ETG_TRACE_USR1(("devprj_tclService::~devprj_tclService entered "));
   m_poAVManagerClient = OSAL_NULL;
   m_poOnstarNavClient = OSAL_NULL;
   m_poSensorDataClient = OSAL_NULL;
   m_poPosDataClient = OSAL_NULL;
   m_poGMLANClient = OSAL_NULL;
   m_poKeyConfig = OSAL_NULL;
   m_poTelephoneClient = OSAL_NULL;
   m_poNavigationClient = OSAL_NULL;
   m_poSpeechHMIClient = OSAL_NULL;
   m_poOnstarCallClient = OSAL_NULL;
   m_poOnstarDataClient = OSAL_NULL;
   m_poCenterStackHmiClient = OSAL_NULL;
   m_poSystemStateClient = OSAL_NULL;
   m_poAlertMngrClient = OSAL_NULL;
   m_poSpiCmdInterface = OSAL_NULL;
   m_poGestureServc = OSAL_NULL;
   m_poMainAppl = OSAL_NULL;
}//! end of ~devprj_tclService()

/**************************************************************************
** FUNCTION:  t_Bool devprj_tclService::bInitialize();
**************************************************************************/
t_Bool devprj_tclService::bInitialize()
{
   ETG_TRACE_USR1(("devprj_tclService::vInitialize entered "));

   t_Bool bInit = false;

   /* Set the source availability flag to false, so that AV Manager
   will not allocate route for SPI, it was the last source */
   bSetSrcAvailability(e8AUD_MAIN_OUT,false);

   SPI_NORMAL_ASSERT(OSAL_NULL == m_poMainAppl);
   if (OSAL_NULL != m_poMainAppl)
   {
     //! Add Gesture Service client handler
      m_poGestureServc = OSAL_NEW spi_tclGestureService(m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poGestureServc);

      //! Get instance of SpiCmdInterface class and pass SpiRespInterface & MainApp pointers
      m_poSpiCmdInterface = spi_tclCmdInterface::getInstance(this, m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poSpiCmdInterface);


      //! Add client handlers
      m_poAlertMngrClient = OSAL_NEW spi_tclAlertMngrClient(m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poAlertMngrClient);
      m_poSystemStateClient = OSAL_NEW spi_tclSystemStateClient(m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poSystemStateClient);
      m_poCenterStackHmiClient = OSAL_NEW spi_tclCenterStackHmiClient(m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poCenterStackHmiClient);
      m_poOnstarDataClient = OSAL_NEW spi_tclOnstarDataClient(m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poOnstarDataClient);
      m_poOnstarCallClient = OSAL_NEW spi_tclOnstarCallClient(m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poOnstarCallClient);
      m_poSpeechHMIClient = OSAL_NEW spi_tclSpeechHMIClient(m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poSpeechHMIClient);
      m_poNavigationClient = OSAL_NEW spi_tclNavigationClient(m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poNavigationClient);
      m_poTelephoneClient = OSAL_NEW spi_tclTelephoneClient(m_poMainAppl, MOST_TELFI_C_U16_SERVICE_ID);
      NORMAL_M_ASSERT(OSAL_NULL != m_poTelephoneClient);
      m_poGMLANClient = OSAL_NEW spi_tclGMLANGatewayClient(m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poGMLANClient);
      m_poPosDataClient = OSAL_NEW spi_tclPosDataClientHandler(m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poPosDataClient);
      m_poSensorDataClient = OSAL_NEW spi_tclSensorDataClientHandler(m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poSensorDataClient);
      m_poOnstarNavClient = OSAL_NEW spi_tclOnstarNavClient(m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poOnstarNavClient);
      m_poAVManagerClient = OSAL_NEW spi_tclAVManagerClient(m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poAVManagerClient);

      if ((OSAL_NULL != m_poSpiCmdInterface) && (OSAL_NULL != m_poSpeechHMIClient) &&
            (OSAL_NULL != m_poTelephoneClient))
      {
         m_poKeyConfig = OSAL_NEW devprj_tclKeyConfig(m_poSpiCmdInterface,m_poSpeechHMIClient,
            m_poTelephoneClient);
         NORMAL_M_ASSERT(OSAL_NULL != m_poKeyConfig);
      }

      bInit = ((OSAL_NULL != m_poGestureServc) &&
            (OSAL_NULL != m_poSpiCmdInterface) &&
            (OSAL_NULL != m_poAlertMngrClient) &&
            (OSAL_NULL != m_poSystemStateClient) &&
            (OSAL_NULL != m_poCenterStackHmiClient) &&
            (OSAL_NULL != m_poOnstarDataClient) &&
            (OSAL_NULL != m_poOnstarCallClient) &&
            (OSAL_NULL != m_poSpeechHMIClient) &&
            (OSAL_NULL != m_poNavigationClient) &&
            (OSAL_NULL != m_poTelephoneClient) &&
	         (OSAL_NULL != m_poKeyConfig) &&
	         (OSAL_NULL != m_poGMLANClient)&&
	         (OSAL_NULL != m_poPosDataClient) &&
	         (OSAL_NULL != m_poSensorDataClient) &&
            (OSAL_NULL != m_poOnstarNavClient) &&
            (OSAL_NULL != m_poAVManagerClient) &&
            (m_poSpiCmdInterface->bInitialize()));

   } //if (OSAL_NULL != m_poMainAppl)

   return bInit;
}//! end of bInitialize()

/**************************************************************************
** FUNCTION:  t_Bool devprj_tclService::bUnInitialize();
**************************************************************************/
t_Bool devprj_tclService::bUnInitialize()
{
   ETG_TRACE_USR1(("devprj_tclService::bUnInitialize entered "));

   t_Bool bUnInit = false;

   //! Release resources
   RELEASE_MEM_OSAL(m_poAVManagerClient);
   RELEASE_MEM_OSAL(m_poOnstarNavClient);
   RELEASE_MEM_OSAL(m_poSensorDataClient);
   RELEASE_MEM_OSAL(m_poPosDataClient);
   RELEASE_MEM_OSAL(m_poGMLANClient);
   RELEASE_MEM_OSAL(m_poKeyConfig);
   RELEASE_MEM_OSAL(m_poTelephoneClient);
   RELEASE_MEM_OSAL(m_poNavigationClient);
   RELEASE_MEM_OSAL(m_poSpeechHMIClient);
   RELEASE_MEM_OSAL(m_poOnstarCallClient);
   RELEASE_MEM_OSAL(m_poOnstarDataClient);
   RELEASE_MEM_OSAL(m_poCenterStackHmiClient);
   RELEASE_MEM_OSAL(m_poSystemStateClient);
   RELEASE_MEM_OSAL(m_poAlertMngrClient);

   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      bUnInit = m_poSpiCmdInterface->bUnInitialize();
   }
   m_poSpiCmdInterface = OSAL_NULL;

   RELEASE_MEM_OSAL(m_poGestureServc);

   return bUnInit;
}//! end of bUnInitialize()

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vLoadSettings(const trSpiFeatureSupport&...)
***************************************************************************/
t_Void devprj_tclService::vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
{

   /*lint -esym(40,fvOnGpsData)fvOnGpsData Undeclared identifier */
   /*lint -esym(40,fvOnSensorData)fvOnSensorData Undeclared identifier */
   /*lint -esym(40,fvOnVehicleData)fvOnVehicleData Undeclared identifier */
   /*lint -esym(40,fvOnTelephoneCallActivity)fvOnTelephoneCallActivity Undeclared identifier */
   /*lint -esym(40,fvOnTelephoneCallStatus)fvOnTelephoneCallStatus Undeclared identifier */
   /*lint -esym(40,fvOutsideTempUpdate)fvOutsideTempUpdate Undeclared identifier */
   /*lint -esym(40,_1)_1 Undeclared identifier */
   ETG_TRACE_USR1(("devprj_tclService::vLoadSettings entered "));
   
   //!Initialize the Base channel map with appropriate values 
   vInitializeBaseChannelMap();

   //! Store DiPO feature support status
   m_bIsDiPOSupported = rfcrSpiFeatureSupp.bDipoSupported();
   
   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      m_poSpiCmdInterface->vLoadSettings(rfcrSpiFeatureSupp);
   }

   if(OSAL_NULL!= m_poKeyConfig)
   {
      //!Insert the device projection switch codes and the corresponding ML Key codes to map
	  m_poKeyConfig->vInsertKeyCodeToMap();
      //Set the required client capabilities
      m_poKeyConfig->vSetClientCapabilities();
   }
   
   //! Read system variant type
   spi_tclConfigReader* poConfigReader = spi_tclConfigReader::getInstance();
   m_enSystemVariant = (OSAL_NULL != poConfigReader)?
         (poConfigReader->enGetSystemVariant()) : (e8VARIANT_COLOR);
   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      t_Bool bLocDataAvailable = (e8VARIANT_NAVIGATION == m_enSystemVariant);
      t_Bool bIsDRLocData = (e8VARIANT_NAVIGATION != m_enSystemVariant);
      m_poSpiCmdInterface->vSetLocDataAvailablility(
            bLocDataAvailable, bIsDRLocData);
   }

   //Initialise the callback structures required for Location data and Vehicle Data
   rDataSvcLocDataCb.fvOnGpsData =
      std::bind(&devprj_tclService::vOnGPSData,
      this,
      SPI_FUNC_PLACEHOLDERS_1);

   rDataSvcSensorDataCb.fvOnSensorData =
      std::bind(&devprj_tclService::vOnSensorData,
      this,
      SPI_FUNC_PLACEHOLDERS_1);

   rDataSvcSensorDataCb.fvOnAccSensorData =
      std::bind(&devprj_tclService::vOnAccSensorData,
      this,
      SPI_FUNC_PLACEHOLDERS_1);

   rDataSvcSensorDataCb.fvOnGyroSensorData =
      std::bind(&devprj_tclService::vOnGyroSensorData,
      this,
      SPI_FUNC_PLACEHOLDERS_1);

   rDataSvcVehicleDataCb.fvOnVehicleData =
         std::bind(&devprj_tclService::vOnGmlanData,
         this,SPI_FUNC_PLACEHOLDERS_2);

   //! Register for Telephone events callbacks
   if (OSAL_NULL != m_poTelephoneClient)
   {
      trTelephoneCallbacks rTelephoneCallbacks;
      rTelephoneCallbacks.fvOnTelephoneCallActivity =
            std::bind(&devprj_tclService::vOnTelephoneCallActivity,
                  this,
                  SPI_FUNC_PLACEHOLDERS_1);
      rTelephoneCallbacks.fvOnTelephoneCallStatus =
            std::bind(&devprj_tclService::vOnTelephoneCallStatus,
                  this,
                  SPI_FUNC_PLACEHOLDERS_1);
      m_poTelephoneClient->vRegisterCallbacks(rTelephoneCallbacks);
   }

   rEnvironmentDataCbs.fvOutsideTempUpdate = 
      std::bind(&devprj_tclService::vOnOutsideTempUpdate,
      this,
      SPI_FUNC_PLACEHOLDERS_2);

}

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vSaveSettings()
***************************************************************************/
t_Void devprj_tclService::vSaveSettings()
{
   ETG_TRACE_USR1(("devprj_tclService::vSaveSettings entered "));
   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      m_poSpiCmdInterface->vSaveSettings();
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void devprj_tclService::vRestoreSettings()
 ***************************************************************************/
t_Void devprj_tclService::vRestoreSettings()
{
   ETG_TRACE_USR1(("devprj_tclService::vRestoreSettings entered "));

   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      m_poSpiCmdInterface->vRestoreSettings();
   }//if (OSAL_NULL != m_pSpiCmdIntf)
}

/***************************************************************************
** FUNCTION: t_Void devprj_tclService::vSetMLNotificationOnOff()
***************************************************************************/
t_Void devprj_tclService::vSetMLNotificationOnOff(t_Bool bSetMLNotificationsOn)
{
   ETG_TRACE_USR1(("devprj_tclService::vSetMLNotificationOnOff entered: bSetMLNotificationsOn = %d ",
      ETG_ENUM(BOOL, bSetMLNotificationsOn)));

   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      m_poSpiCmdInterface->vSetMLNotificationOnOff(bSetMLNotificationsOn);
   }//if (OSAL_NULL != m_poSpiCmdInterface)
}

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnServiceAvailable()
***************************************************************************/
tVoid devprj_tclService::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("devprj_tclService::vOnServiceAvailable entered "));
   //Add code
}

/***************************************************************************
** FUNCTION:  tVoid spi_tclVideoPolicy::vOnServiceUnavailable()
***************************************************************************/
tVoid devprj_tclService::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("devprj_tclService::vOnServiceUnavailable entered "));
   //Add code
}

/***************************************************************************
 ** FUNCTION:  tBool devprj_tclService::bStatusMessageFactory(tU16 u16Fun...
 **************************************************************************/
tBool devprj_tclService::bStatusMessageFactory(tU16 u16FunctionId,
      amt_tclServiceData& roOutMsg, amt_tclServiceData* poInMsg)
{
   SPI_INTENTIONALLY_UNUSED(poInMsg);
   tBool bSuccess = FALSE; 

   ETG_TRACE_USR1(("devprj_tclService::bStatusMessageFactory entered: FID = 0x%4x ",
      u16FunctionId));

   //! Create status message for required FunctionID and
   //! handover the status message to roOutMsg.
   switch (u16FunctionId)
   {
      case MOST_DEVPRJFI_C_U16_DEVICECONNECTIONLIST:
      {
         bSuccess = bHandoverMsgDevConnListStatus(roOutMsg);
      }
         break;
      case MOST_DEVPRJFI_C_U16_DEVICENOTIFICATIONSENABLED:
      {
         bSuccess = bHandoverMsgDevNotiEnableInfoStatus(roOutMsg);
      }
         break;
      case MOST_DEVPRJFI_C_U16_DISPLAYPROJECTIONREQUEST_:
      {
         bSuccess = bHandoverStatusMsg(m_oStDispProjRequest, roOutMsg);
      }
         break;
      case MOST_DEVPRJFI_C_U16_APPLICATIONLIST:
      {
         bSuccess = bHandoverMsgAppListStatus(roOutMsg);
      }
         break;
      case MOST_DEVPRJFI_C_U16_DRIVEMODE:
      {
         bSuccess = bHandoverDriveModeStatus(roOutMsg);
      }
         break;
      case MOST_DEVPRJFI_C_U16_MIRRORLINKENABLE: 
      {
         bSuccess = bHandoverDevProjEnableStatus(roOutMsg, e8DEV_TYPE_MIRRORLINK);
      }
         break;
      case MOST_DEVPRJFI_C_U16_DIPOENABLE:
      {
         bSuccess = bHandoverDevProjEnableStatus(roOutMsg, e8DEV_TYPE_DIPO);
      }
         break;
      case MOST_DEVPRJFI_C_U16_GOOGLEAUTOLINKENABLE:
      {
         bSuccess = bHandoverDevProjEnableStatus(roOutMsg, e8DEV_TYPE_ANDROIDAUTO);
      }
         break;
      case MOST_DEVPRJFI_C_U16_APPLICATIONMETADATA:
      {
         bSuccess = bHandoverMsgAppMetadataStatus(roOutMsg);
      }
         break;
      case MOST_DEVPRJFI_C_U16_APPSTATUSINFO:
      {
         bSuccess = bHandoverDeviceAppStateStatus(roOutMsg);
      }
         break;
      case MOST_DEVPRJFI_C_U16_BLUETOOTHDEVICESTATUS:
      {
         //Clear msg info once handover is complete, since the event should
         //not be triggered again if the property is subscribed after the event trigger.
         bSuccess = bHandoverStatusMsg(m_oStBTDevStatusMsg, roOutMsg);
         CLEAR_BTDEVSTATUS_MSG(m_oStBTDevStatusMsg);
      }
         break;
      case MOST_DEVPRJFI_C_U16_MLACTIVEAPP:
      {
         bSuccess = bHandoverStatusMsg(m_oStMLActiveAppMsg, roOutMsg);
      }
         break;
      case MOST_DEVPRJFI_C_U16_BTPAIRINGREQUIRED:
      {
         //Clear msg info once handover is complete, since the event should
         //not be triggered again if the property is subscribed after the event trigger.
         bSuccess = bHandoverStatusMsg(m_oStBTPairRequired, roOutMsg);
         CLEAR_BTPAIR_REQUIRED_MSG(m_oStBTPairRequired);
      }
         break;
      case MOST_DEVPRJFI_C_U16_PROJECTIONDEVICEAUTHANDACCESSIBILITY:
      {
         bSuccess = bHandoverStatusMsg(m_oProjDevAuthAndAccess, roOutMsg);
      }
         break;
      default:
         ETG_TRACE_ERR((" bStatusMessageFactory: Invalid Func ID : 0x%4x", u16FunctionId));
         break;
   } //switch (u16FunctionId)

   if (FALSE == bSuccess)
   {
      ETG_TRACE_ERR((" bStatusMessageFactory(): Creation of message with 'FID = 0x%4x' failed! ", u16FunctionId));
   }

   return bSuccess;

}//! end of bStatusMessageFactory()

/***************************************************************************
 ** FUNCTION:  tBool devprj_tclService::bProcessSet(amt_tclServiceData* p...
 **************************************************************************/
tBool devprj_tclService::bProcessSet(amt_tclServiceData* poMessage,
      tBool& bPropertyChanged, tU16& u16Error)
{
   SPI_INTENTIONALLY_UNUSED(bPropertyChanged);
   SPI_INTENTIONALLY_UNUSED(u16Error);

   tU16 u16FunctionId = poMessage->u16GetFunctionID();
   ETG_TRACE_USR1(("devprj_tclService::bProcessSet entered: FID = 0x%4x ", u16FunctionId));

   //Extract user context details
   trUserContext rUsrCtxt;
   CPY_TO_USRCNTXT(poMessage, rUsrCtxt);

   SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);

   tBool bMsgValid = FALSE;

   //! Call appropriate method in SPI to process the DevPrj Set message.
   switch (u16FunctionId)
   {
      case MOST_DEVPRJFI_C_U16_DEVICENOTIFICATIONSENABLED:
      {
         devprj_tXFiPSDevNotifEnabled oXFiDevNotif(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);
         bMsgValid = oXFiDevNotif.bIsValid();
         if ((true == bMsgValid) && (OSAL_NULL != m_poSpiCmdInterface))
         {
            ETG_TRACE_USR2((" bProcessSet: Received DeviceNotificationsEnabled = %u ",
                  ETG_ENUM(BOOL, oXFiDevNotif.bDeviceNotificationsEnabled)));

            m_poSpiCmdInterface->vSetMLNotificationOnOff(oXFiDevNotif.bDeviceNotificationsEnabled);

            //! Update clients about DeviceNotificationsEnabled change.
            if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_DEVICENOTIFICATIONSENABLED))
            {
               ETG_TRACE_ERR((" bProcessSet: Updating DeviceNotificationsEnabled.Status failed! "));
            }
         } //if (OSAL_NULL != m_poSpiCmdInterface)
      }
         break;
      case MOST_DEVPRJFI_C_U16_DRIVEMODE:
      {
         devprj_tXFiPSDriveMode oXFiDriveMode(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);
         bMsgValid = oXFiDriveMode.bIsValid();
         if ((true == bMsgValid) && (OSAL_NULL != m_poSpiCmdInterface))
         {
            ETG_TRACE_USR2((" bProcessSet: Received DriveMode = %u ", ETG_ENUM(VEHICLE_MODE, oXFiDriveMode.bDriveMode)));

            m_bDriveModeEnabled = oXFiDriveMode.bDriveMode;
            tenVehicleConfiguration enVehicleConfig = (TRUE == oXFiDriveMode.bDriveMode) ? e8DRIVE_MODE : e8PARK_MODE;
            m_poSpiCmdInterface->vSetVehicleConfig(enVehicleConfig,true,rUsrCtxt);

            //! Update clients about DriveMode change.
            if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_DRIVEMODE))
            {
               ETG_TRACE_ERR((" bProcessSet: Updating DriveMode.Status failed! "));
            }
         } //if (OSAL_NULL != m_poSpiCmdInterface)
      }
         break;
      case MOST_DEVPRJFI_C_U16_MIRRORLINKENABLE: 
      {
         devprj_tXFiPSMLEnable oXFiMLEnable(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);
         bMsgValid = oXFiMLEnable.bIsValid();
         if ((true == bMsgValid) && (OSAL_NULL != m_poSpiCmdInterface))
         {
            ETG_TRACE_USR2((" bProcessSet: Received ProjectionEnabled = %u ",
                  ETG_ENUM(BOOL, oXFiMLEnable.bProjectionEnabled)));

            tenEnabledInfo enEnableInfo =
               (TRUE == oXFiMLEnable.bProjectionEnabled) ? e8USAGE_ENABLED : e8USAGE_DISABLED;

            m_poSpiCmdInterface->vSetDeviceUsagePreference(cou32DevHandle_AllDevices,
                  e8DEV_TYPE_MIRRORLINK, enEnableInfo, rUsrCtxt);
         } //if (OSAL_NULL != m_poSpiCmdInterface)
      }
         break;
      case MOST_DEVPRJFI_C_U16_DIPOENABLE:
      {
         devprj_tXFiPSDipoEnable oXFiDipoEnable(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);
         bMsgValid = oXFiDipoEnable.bIsValid();
         if ((true == bMsgValid) && (OSAL_NULL != m_poSpiCmdInterface))
         {
            ETG_TRACE_USR2((" bProcessSet: Received ProjectionEnabled = %u ",
                  ETG_ENUM(BOOL, oXFiDipoEnable.bProjectionEnabled)));

            tenEnabledInfo enEnableInfo =
               (TRUE == oXFiDipoEnable.bProjectionEnabled) ? e8USAGE_ENABLED : e8USAGE_DISABLED;

            m_poSpiCmdInterface->vSetDeviceUsagePreference(cou32DevHandle_AllDevices,
                  e8DEV_TYPE_DIPO, enEnableInfo, rUsrCtxt);
         } //if (OSAL_NULL != m_poSpiCmdInterface)
      }
         break;
      case MOST_DEVPRJFI_C_U16_GOOGLEAUTOLINKENABLE:
      {
         devprj_tXFiPSGoogleAutoEnable oXFiGoogleAutoEnable(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);
         bMsgValid = oXFiGoogleAutoEnable.bIsValid();
         if ((true == bMsgValid) && (OSAL_NULL != m_poSpiCmdInterface))
         {
            ETG_TRACE_USR2((" bProcessSet: Received ProjectionEnabled = %u ",
                  ETG_ENUM(BOOL, oXFiGoogleAutoEnable.bProjectionEnabled)));
            tenEnabledInfo enEnableInfo =
               (TRUE == oXFiGoogleAutoEnable.bProjectionEnabled) ? e8USAGE_ENABLED : e8USAGE_DISABLED;
            m_poSpiCmdInterface->vSetDeviceUsagePreference(cou32DevHandle_AllDevices,
                  e8DEV_TYPE_ANDROIDAUTO, enEnableInfo, rUsrCtxt);
         } //if (OSAL_NULL != m_poSpiCmdInterface)
      }
         break;
      case MOST_DEVPRJFI_C_U16_PROJECTIONDEVICEAUTHANDACCESSIBILITY:
      {
         devprj_tXFiPSProjDevAuthAndAccessibility oDevAuthAndAccessibility(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);
         bMsgValid = oDevAuthAndAccessibility.bIsValid();
         if ((true == bMsgValid) && (OSAL_NULL != m_poSpiCmdInterface))
         {
            ETG_TRACE_USR2((" bProcessSet: Received Projection Device handle = %d, Authorization status = %u ",
                  oDevAuthAndAccessibility.u32DeviceHandle, ETG_ENUM(BOOL, oDevAuthAndAccessibility.bUserAuthorizationStatus)));
            tenUserAuthorizationStatus enUserAuthStatus = e8_USER_AUTH_UNKNOWN;
            enUserAuthStatus = m_oDevUserParamItem.bUserAuthorizationStatus ? e8_USER_AUTH_AUTHORIZED : e8_USER_AUTH_UNAUTHORIZED;
            m_poSpiCmdInterface->vSetDeviceAuthorization(oDevAuthAndAccessibility.u32DeviceHandle, enUserAuthStatus);
         } //if (OSAL_NULL != m_poSpiCmdInterface)
      }
         break;
      default:
         ETG_TRACE_ERR((" bProcessSet: Invalid Func ID : 0x%x ", u16FunctionId));
         break;
   } //switch (u16FunctionId)

   return bMsgValid;

} //! end of bProcessSet()

/******************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnLoopback(tU16 u16ServiceID,...
******************************************************************************/
tVoid devprj_tclService::vOnLoopback(tU16 u16ServiceID, amt_tclServiceData *poMessage)
{
   ETG_TRACE_USR1(("devprj_tclService::vOnLoopback() entered with service Id = %d", u16ServiceID));

   if (FALSE == ahl_tclBaseOneThreadService::bDefaultSvcDataHandler(this, poMessage))
   {
      // Trace: Nothing else to do since it is a loopback message.
      // Message will be deleted by the framework.
      // No error message to be sent back if loop back could not find a mapping
      // function - just perform the trace.
      ETG_TRACE_ERR((" vOnLoopback() could not find a mapping function (Opcode: %d)",
            ETG_ENUM(OP_CODES, poMessage->u8GetOpCode())));
   }
}

/******************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnTimer(tU16 nTimerID)
******************************************************************************/
tVoid devprj_tclService::vOnTimer(tU16 nTimerID)
{
    ETG_TRACE_USR1(("devprj_tclService::vOnTimer with Timer ID = %d", nTimerID));

   if (OSAL_NULL != m_poMainAppl)
   {
      switch (nTimerID)
      {
         case UNDUCK_TIMER_ID:
         {
            ETG_TRACE_USR4(("Unduck Timer stopped \n"));
            m_poMainAppl->bStopTimer(UNDUCK_TIMER_ID);
            bUnduckTimerRunning = false;
            //! de-activate for Duck(LC_MIX_ALERT_MSG);
            m_oAudioDuckLock.s16Lock();
            if (true == sbAudDuckEnabled)
            {
               ETG_TRACE_USR4(("Deactivating LC_MIX_ALERT_MSG \n"));
               bQueueAlternateAudioReq(su8AudDuckSrcNum, false);
               //bTriggerAVAction(su8AudDuckSrcNum , IIL_EN_ISRC_REQDEACT);
               sbAudDuckEnabled = false;
            }//End of if(true == sbAudDuckEnabled)
//! Fix for GMMY16-8908: below flow commented
            //! If alternate channel(LC_ALERT_TONE) is de-activated, activate it.
//            if (false == sbAltAudActivated)
//            {
//               ETG_TRACE_USR4(("Activating LC_ALERT_TONE \n"));
//               //bQueueAlternateAudioReq(scu8AltAudSrcNum, true);
//               //bTriggerAVAction(scu8AltAudSrcNum , IIL_EN_ISRC_REQACT);
//               sbAltAudActivated = true;
//            }//End of if(false == sbAltAudActivated)
            m_oAudioDuckLock.vUnlock();

         } // case UNDUCK_TIMER_ID
            break;
         case DIPO_BLACKSCREEN_RECOVERY_TIMER_ID:
         {
		    // Supervision timer expired, Carplay Device has not taken the Display resource
			// Send the current projection status to give HMI another chance to process the message
		    m_oDispProjLock.s16Lock();
            sbDipoProjTimerRun = (true == sbDipoProjTimerRun) ? 
			                      (!(m_poMainAppl->bStopTimer(DIPO_BLACKSCREEN_RECOVERY_TIMER_ID))):(sbDipoProjTimerRun);
            ETG_TRACE_USR4(("Dipo blackscreen recovery Timer stopped \n"));

            //! Update Clients about ProjectedDisplayContext change
            if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_DISPLAYPROJECTIONREQUEST_))
            {
               ETG_TRACE_ERR((" vOnTimer: Updating DisplayProjectionRequest.Status failed! "));
            }
			m_oDispProjLock.vUnlock();

         } // case DIPO_BLACKSCREEN_RECOVERY_TIMER_ID
            break;
         default:
         {
         }
            break;
      }//End of Switch
   }
}

/***************************************************************************
* ! Overriding Base class SpiRespInterface methods
***************************************************************************/

/***************************************************************************
** FUNCTION: tVoid devprj_tclService::vPostDeviceStatusInfo(tenDeviceStat...
***************************************************************************/
tVoid devprj_tclService::vPostDeviceStatusInfo(t_U32 u32DeviceHandle,
      tenDeviceConnectionType enDevConnType, 
      tenDeviceStatusInfo enDeviceStatus)
{
   SPI_INTENTIONALLY_UNUSED(enDevConnType);
   
   ETG_TRACE_USR1(("devprj_tclService::vPostDeviceStatusInfo entered: DeviceHandle = 0x%x, DeviceStatus = %d ",
         u32DeviceHandle, ETG_ENUM(DEVICE_STATUS_INFO, enDeviceStatus)));
   //@Note: The below part of the code is not used currently. Keep it for future reference. 
   /*
   t_Bool bUpdateRequired = true;

   if((NULL != m_poSpiCmdInterface)
      &&(e8DEV_TYPE_DIPO == m_poSpiCmdInterface->enGetDeviceCategory(u32DeviceHandle)))
   {
      t_U32 u32NumDevices = 0;
      std::vector<trDeviceInfo> vecrDeviceInfoList;
      
      typedef std::vector<trDeviceInfo>::iterator SpiDevInfoIter;
      m_poSpiCmdInterface->vGetDeviceInfoList(u32NumDevices, vecrDeviceInfoList);
  
      //! This logic is used to synchronize the device selection
      // and CarPlay session creation to update the session active status to 
      // HMI. With this the device active status will update HMI only after the 
      // session startup at CarPlay plugin side is completed.

      for(SpiDevInfoIter devListIter = vecrDeviceInfoList.begin();
         devListIter != vecrDeviceInfoList.end(); ++devListIter)
      {
         if((u32DeviceHandle == devListIter->u32DeviceHandle) && 
            (e8DEV_TYPE_DIPO == devListIter->enDeviceCategory) &&
            (true == devListIter->bSelectedDevice) &&
            (e8_SESSION_INACTIVE == m_enDeviceSessionStatus))
         {
            bUpdateRequired = false;
         }
      } // End of for loop
   }//if(NULL != m_poSpiCmdInterface)
   if(true == bUpdateRequired)
   {
   
   }//if(true = bUpdateRequired)
   */
   //! Update clients about DeviceConnectionList change.
   if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_DEVICECONNECTIONLIST))
   {
      ETG_TRACE_ERR((" vPostDeviceStatusInfo: Updating DeviceConnectionList.Status failed! "));
   }
   
}//! end of vPostDeviceStatusInfo()

/***************************************************************************
** FUNCTION: tVoid devprj_tclService::vPostAppStatusInfo(tenDeviceStat...
***************************************************************************/
tVoid devprj_tclService::vPostAppStatusInfo(t_U32 u32DeviceHandle,
      tenDeviceConnectionType enDevConnType, 
      tenAppStatusInfo enAppStatus)
{
   SPI_INTENTIONALLY_UNUSED(enDevConnType);
   
   ETG_TRACE_USR1(("devprj_tclService::vPostAppStatusInfo entered: DeviceHandle = 0x%x, ApplicationStatus = %d ",
         u32DeviceHandle, ETG_ENUM(APP_STATUS_INFO, enAppStatus)));
  
   //! Update clients about AppicationList change.
   if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_APPLICATIONLIST))
   {
      ETG_TRACE_ERR((" vPostAppStatusInfo: Updating ApplicationList.Status failed! "));
   }
}//! end of vPostAppStatusInfo()

/***************************************************************************
** FUNCTION: tVoid devprj_tclService::vPostDeviceUsagePrefResult(t_U32 u32DeviceHandle...
***************************************************************************/
t_Void devprj_tclService::vPostDeviceUsagePrefResult(const t_U32 coU32DeviceHandle,
      tenErrorCode enErrorCode,
      tenDeviceCategory enDeviceCategory,
      tenEnabledInfo enUsagePref,
      const trUserContext &corfrUsrCtxt)
{
   ETG_TRACE_USR1(("devprj_tclService::vPostDeviceUsagePrefResult entered "));
   ETG_TRACE_USR2((" vPostDeviceUsagePrefResult: Received DeviceHandle = 0x%x ", coU32DeviceHandle));
   ETG_TRACE_USR2((" vPostDeviceUsagePrefResult: Received EnableInfo = %d ", ETG_ENUM(ENABLED_INFO, enUsagePref)));
   ETG_TRACE_USR2((" vPostDeviceUsagePrefResult: Received ErrorCode = %d ", ETG_ENUM(ERROR_CODE, enErrorCode)));
   ETG_TRACE_USR2((" vPostDeviceUsagePrefResult: Received DeviceCategory = %d ", ETG_ENUM(DEVICE_CATEGORY, enDeviceCategory)));

   //@Note: SetDeviceUsagePreference function is used by two APIs in DeviceProjection i.e,
   //a) To set global ML/DiPOEnable property (used with DevHandle = cou32DevHandle_AllDevices), AND
   //b) By ChangeDeviceUsagePreference method. (used with a valid Device handle).
   //Hence we identify the result of each by evaluating the DeviceHandle.

   //! Handle ChangeDeviceProjectionEnable result
   if (cou32DevHandle_AllDevices != coU32DeviceHandle)
   {
      //! Send ChangeDeviceProjectionEnable method result
      if (e8NO_ERRORS == enErrorCode)
      {
         devprj_tMRChangeDevPrjEnable oChnDevPrjMR;
         oChnDevPrjMR.u32DeviceHandle = coU32DeviceHandle;
         oChnDevPrjMR.bDeviceProjectionEnabled = (e8USAGE_ENABLED == enUsagePref) ? TRUE : FALSE;
         if (FALSE == bPostResponse(oChnDevPrjMR, corfrUsrCtxt))
         {
            ETG_TRACE_ERR((" vPostDeviceUsagePrefResult: ChangeDeviceProjectionEnable MR post failed! "));
         }
      }
      //! Send ChangeDeviceProjectionEnable method error
      else
      {
         devprj_tErrChangeDevPrjEnable oChngDevProjEnableErr;
         if (FALSE == bPostResponse(oChngDevProjEnableErr, corfrUsrCtxt))
         {
            ETG_TRACE_ERR((" vPostDeviceUsagePrefResult: ChangeDeviceProjectionEnable.Error post failed! "));
         }
      }
   } //if (cou32DevHandle_AllDevices != u32DeviceHandle)

   //! Handle MirrorLinkEnable/DiPOEnable result
   else if (e8DEV_TYPE_UNKNOWN != enDeviceCategory)
   {
      tU16 u16MLDiPOEnableFunctID = 0;
      switch(enDeviceCategory)
      {
         case e8DEV_TYPE_MIRRORLINK : u16MLDiPOEnableFunctID = MOST_DEVPRJFI_C_U16_MIRRORLINKENABLE ; break;
         case e8DEV_TYPE_DIPO : u16MLDiPOEnableFunctID = MOST_DEVPRJFI_C_U16_DIPOENABLE ; break;
         case e8DEV_TYPE_ANDROIDAUTO : u16MLDiPOEnableFunctID = MOST_DEVPRJFI_C_U16_GOOGLEAUTOLINKENABLE ; break;
         default : break;
      }

      //! Update clients about MirrorLinkEnable/DiPOEnable change.
      if (AIL_EN_N_NO_ERROR != eUpdateClients(u16MLDiPOEnableFunctID))
      {
         ETG_TRACE_ERR((" vPostDeviceUsagePrefResult: Updating MirrorLinkEnable/DiPOEnable.Status failed! "));
      }
   } //else if (e8DEV_TYPE_UNKNOWN != enDeviceCategory)

}//! end of vPostDeviceUsagePrefResult()

/***************************************************************************
** FUNCTION: tVoid devprj_tclService::vLaunchAppResult(tenResponseCode...
***************************************************************************/
tVoid devprj_tclService::vLaunchAppResult(t_U32 u32DeviceHandle,
      t_U32 u32AppHandle,
      tenDiPOAppType enDiPOAppType, 
      tenResponseCode enResponseCode,
      tenErrorCode enErrorCode,
      const trUserContext& corfrUsrCntxt)
{
   ETG_TRACE_USR1((" vLaunchAppResult: DevId= 0x%x, AppHandle =0x%x, DiPOAppType = %d",
					u32DeviceHandle, u32AppHandle, ETG_ENUM(DIPO_APP_TYPE, enDiPOAppType)));
   ETG_TRACE_USR1((" vLaunchAppResult: ResponseCode = %d, ErrorCode = %d",
					ETG_ENUM(RESPONSE_CODE, enResponseCode), ETG_ENUM(ERROR_CODE, enErrorCode)));

   //! Initialize and send LaunchApp method result
   devprj_tMRLaunchApp oLaunchAppMR;

   oLaunchAppMR.e8LaunchResult.enType = (e8SUCCESS == enResponseCode) ?
      (devprj_tFiLaunchResult::FI_EN_E8LAUNCH_SUCCESSFUL) : 
      (devprj_tFiLaunchResult::FI_EN_E8LAUNCH_FAILED);
	
   if (FALSE == bPostResponse(oLaunchAppMR, corfrUsrCntxt))
   {
      ETG_TRACE_ERR((" vLaunchAppResult: LaunchApp MR post failed! "));
   }

   vUnblockVideo(u32DeviceHandle, enResponseCode);

}//! end of vPostLaunchAppResult()

/***************************************************************************
** FUNCTION: tVoid devprj_tclService::vUnblockVideo(t_U32 u32Dev...
***************************************************************************/
tVoid devprj_tclService::vUnblockVideo(t_U32 u32DeviceHandle,
      tenResponseCode enResponseCode)
{
   ETG_TRACE_USR1(("devprj_tclService: vUnblockVideo entered"));
   //! Unblock video for MirrorLink - workaround in GM since SessionStatus will not be sent to HMI
   //! received until video is unblocked.
   if (
      (e8SUCCESS == enResponseCode)
      &&
      (OSAL_NULL != m_poSpiCmdInterface)
      &&
      (e8DEV_TYPE_MIRRORLINK == m_poSpiCmdInterface->enGetDeviceCategory(u32DeviceHandle))
      )
   {
      m_poSpiCmdInterface->vSetVideoBlockingMode(u32DeviceHandle, e8DISABLE_BLOCKING, corEmptyUsrContext);
   }
}//! end of vUnblockVideo()

/***************************************************************************
** FUNCTION: tVoid devprj_tclService::vPostTerminateAppResult(tenResponse...
***************************************************************************/
tVoid devprj_tclService::vTerminateAppResult(t_U32 u32DeviceHandle, 
      t_U32 u32AppHandle, 
      tenResponseCode enResponseCode,
      tenErrorCode enErrorCode,
      const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);

   ETG_TRACE_USR1(("devprj_tclService::vTerminateAppResult entered "));
   ETG_TRACE_USR2((" vTerminateAppResult: Received DeviceHandle = 0x%x ", u32DeviceHandle));
   ETG_TRACE_USR2((" vTerminateAppResult: Received ApplicationHandle = 0x%x ", u32AppHandle));
   ETG_TRACE_USR2((" vTerminateAppResult: Received ResponseCode = %d ", ETG_ENUM(RESPONSE_CODE, enResponseCode)));
   ETG_TRACE_USR2((" vTerminateAppResult: Received ErrorCode = %d ", ETG_ENUM(ERROR_CODE, enErrorCode)));

   //! @Note: Do nothing. DevPrj FI does not define a MethodResult for TerminateApp.

}//! end of vPostTerminateAppResult()

/***************************************************************************
** FUNCTION: t_Void devprj_tclService::vPostApplicationMediaMetaData(...
***************************************************************************/
t_Void devprj_tclService::vPostApplicationMediaMetaData(
      const trAppMediaMetaData& rfcorApplicationMediaMetaData,
      const trUserContext& rfcorUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(rfcorUsrCntxt);

   ETG_TRACE_USR1(("devprj_tclService::vPostApplicationMediaMetaData entered: bMediaMetadataValid = %u ",
         ETG_ENUM(BOOL, rfcorApplicationMediaMetaData.bMediaMetadataValid)));

   //Store media meta data to member variable
   m_rAppMediaMetaData = rfcorApplicationMediaMetaData;
   
   //! Update clients about Application media Metadata change.
   if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_APPLICATIONMETADATA))
   {
      ETG_TRACE_ERR((" vPostApplicationMediaMetaData: Updating Application media Metadata.Status failed! "));
   }
}//! end of vPostApplicationMediaMetaData()



/***************************************************************************
** FUNCTION: t_Void devprj_tclService::vPostApplicationMediaPlaytime(...
***************************************************************************/
t_Void devprj_tclService::vPostApplicationMediaPlaytime(
      const trAppMediaPlaytime& rfcorApplicationMediaPlaytime,
            const trUserContext& rfcorUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(rfcorUsrCntxt);

   ETG_TRACE_USR1(("devprj_tclService::vPostApplicationMediaPlaytime entered"));

   //Store media current playing time info to member variable
   m_rAppMediaPlaytime = rfcorApplicationMediaPlaytime;

   //! Update clients about current playing media current time change.
   if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_APPLICATIONMETADATA))
   {
      ETG_TRACE_ERR((" vPostApplicationMediaPlaytime: Updating Media playtime, Status failed! "));
   }
}//! end of vPostApplicationMediaPlaytime()


/***************************************************************************
** FUNCTION: t_Void devprj_tclService::vPostApplicationPhoneData(...
***************************************************************************/
t_Void devprj_tclService::vPostApplicationPhoneData(
      const trAppPhoneData& rfcorApplicationPhoneData,
            const trUserContext& rfcorUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(rfcorUsrCntxt);

   ETG_TRACE_USR1(("devprj_tclService::vPostApplicationPhoneData entered: bPhoneMetadataValid = %u ",
         ETG_ENUM(BOOL, rfcorApplicationPhoneData.bPhoneMetadataValid)));

   //Store Phone data to member variable
   m_rAppPhoneData = rfcorApplicationPhoneData;

   //! Update clients about Phone data .
   if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_APPLICATIONMETADATA))
   {
      ETG_TRACE_ERR((" vPostApplicationPhoneData: Updating Phone data.Status failed! "));
   }
}//! end of vPostApplicationPhoneData()


/***************************************************************************
** FUNCTION: tVoid devprj_tclService::vPostSelectDeviceResult(tenResponse...
***************************************************************************/
tVoid devprj_tclService::vPostSelectDeviceResult(t_U32 u32DeviceHandle,
        tenDeviceConnectionType enDevConnType,
        tenDeviceConnectionReq enDevConnReq,
        tenDeviceCategory enDevCat,
        tenResponseCode enResponseCode,
        tenErrorCode enErrorCode,
        t_Bool bIsPairingReq,
        const trUserContext corfrUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(enDevCat);
   ETG_TRACE_USR1(("devprj_tclService::vPostSelectDeviceResult entered: DevId = 0x%x, DevConnType = %d",
					u32DeviceHandle, ETG_ENUM(CONNECTION_TYPE, enDevConnType)));
   ETG_TRACE_USR1(("vPostSelectDeviceResult: DevConnRequest = %d, IsPairingReq = %d", ETG_ENUM(CONNECTION_REQ, enDevConnReq),
					ETG_ENUM(BOOL, bIsPairingReq)));
   ETG_TRACE_USR1(("vPostSelectDeviceResult: ResponseCode = %d, ErrorCode = %d", ETG_ENUM(RESPONSE_CODE, enResponseCode), 
					ETG_ENUM(ERROR_CODE, enErrorCode)));
   
   SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);

   if(OSAL_NULL != m_poSpiCmdInterface)
   {
      //! Send SelectDevice.MethodResult if Select/Deselect operation is:
      // a) Successful, OR
      // b) Failed - because the device is not plugged-in.
      t_Bool bIsDeviceUnavailable = (e8FAILURE == enResponseCode) &&
            ((e8DEVICE_NOT_CONNECTED == enErrorCode) || (e8UNSUPPORTED_OPERATION == enErrorCode));
      if ((e8SUCCESS == enResponseCode) || (true == bIsDeviceUnavailable))
      {
         //! Evaluate selected DeviceHandle to be sent.
         //! Create & post SelectDevice MethodResult msg.
         devprj_tMRSelectDev oSelectDevMR;
		 oSelectDevMR.u32DeviceHandle = static_cast<tU32>(vEvaluateSelectDevice(u32DeviceHandle, enDevConnReq, enResponseCode, bIsPairingReq));

		 if (FALSE == bPostResponse(oSelectDevMR, corfrUsrCntxt))
         {
            ETG_TRACE_ERR((" vPostSelectDeviceResult: SelectDevice MR post failed! "));
         }
      }

      //! For all other cases, send SelectDevice.MethodError
      else
      {
         devprj_tErrSelectDevice oSelectDevErr;
         if (FALSE == bPostResponse(oSelectDevErr, corfrUsrCntxt))
         {
            ETG_TRACE_ERR((" vPostSelectDeviceResult: SelectDevice.Error post failed! "));
         }
      }
   } //if(OSAL_NULL != m_poSpiCmdInterface)

}//! end of vPostSelectDeviceResult()

/***************************************************************************
** FUNCTION: t_U32 devprj_tclService::vEvaluateSelectDevice(t_U32 u32Devi...
***************************************************************************/
t_U32 devprj_tclService::vEvaluateSelectDevice(t_U32 u32DeviceHandle, 
	  tenDeviceConnectionReq enDevConnReq,
	  tenResponseCode enResponseCode,
	  t_Bool bIsPairingReq)
{
    ETG_TRACE_USR1(("devprj_tclService::vEvaluateSelectDevice entered"));
	t_U32 u32SelDevHandle = 0;

    if (e8SUCCESS == enResponseCode)
    {
        if (e8DEVCONNREQ_SELECT == enDevConnReq)
        {
            //If BT pairing is required, set the MSB bit of DeviceHandle.
            u32SelDevHandle =  (true == bIsPairingReq) ?
            (cou32BTPairingFlag | u32DeviceHandle) : (u32DeviceHandle);
        }
        else if (e8DEVCONNREQ_DESELECT == enDevConnReq)
        {
            //For deselect request, set DeviceHandle = 0xFFFFFFFF
            u32SelDevHandle = cou32DevHandle_Deselect;
        }
    }
    //In case of error, set DeviceHandle = 0x7FFFFFFF
    else if (e8FAILURE == enResponseCode)
    {
        u32SelDevHandle = cou32DevHandle_SelectError;
    }
	
	ETG_TRACE_USR2((" vEvaluateSelectDevice: Calculated DeviceHandle = 0x%x ", u32SelDevHandle));
	
	return u32SelDevHandle;
}//! end of vEvaluateSelectDevice()

/***************************************************************************
** FUNCTION: tVoid devprj_tclService::vPostDeviceSelectStatus(t_U32...
***************************************************************************/
t_Void devprj_tclService::vPostDeviceSelectStatus(t_U32 u32DeviceHandle, 
                               tenDeviceCategory enDevCategory,
                               tenDeviceConnectionReq enDevConnReq,
                               tenResponseCode enRespCode)
{
   ETG_TRACE_USR1(("devprj_tclService::vPostDeviceSelectStatus entered "));
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   SPI_INTENTIONALLY_UNUSED(enDevCategory);

   if (e8SUCCESS == enRespCode)
   {
      (e8DEVCONNREQ_SELECT == enDevConnReq) ?
               vRegisterForProperties() : vUnregisterForProperties();
   }//if (e8SUCCESS == enRespCode)
}

/***************************************************************************
** FUNCTION: tVoid devprj_tclService::vPostAppIconDataResult(tenIconMimeType...
***************************************************************************/
tVoid devprj_tclService::vPostAppIconDataResult(tenIconMimeType enIconMimeType,
      const t_U8* pcu8AppIconData,
      t_U32 u32Len,
      const trUserContext rcUsrCntxt)
{
   ETG_TRACE_USR1(("devprj_tclService::vPostAppIconDataResult entered: IconMimeType = %d ",
      ETG_ENUM(ICON_MIME_TYPE, enIconMimeType)));

   SPI_NORMAL_ASSERT(OSAL_NULL == pcu8AppIconData);

   std::string szMimeType;
   tBool bPostSuccess = FALSE;

   switch (enIconMimeType)
   {
      case e8ICON_PNG:
         szMimeType.assign("image/png");
         break;
      case e8ICON_JPEG:
         szMimeType.assign("image/jpeg");
         break;
      case e8ICON_INVALID:
      default:
         szMimeType.assign("image/invalid");
         break;
   } //switch (enIconMimeType)

   switch (rcUsrCntxt.u32FuncID)
   {
      case MOST_DEVPRJFI_C_U16_GETAPPLICATIONICON:
      {
         devprj_tMRGetAppIcon oGetAppIconMR;
         oGetAppIconMR.sIconMIMEType.bSet(szMimeType.c_str());
         if (OSAL_NULL != pcu8AppIconData)
         {
            oGetAppIconMR.oApplicationIconData.vSetData(const_cast<t_U8*> (pcu8AppIconData), u32Len);
         }//if(OSAL_NULL != pcu8AppIconData)          

         bPostSuccess = bPostResponse(oGetAppIconMR, rcUsrCntxt);
         oGetAppIconMR.vDestroy();
      }
         break;
      case MOST_DEVPRJFI_C_U16_GETAPPLICATIONIMAGE:
      {
         devprj_tMRGetAppImage oGetAppImageMR;
         oGetAppImageMR.sImageMIMEType.bSet(szMimeType.c_str());
         if (OSAL_NULL != pcu8AppIconData)
         {
            oGetAppImageMR.oImageData.vSetData(const_cast<t_U8*>  (pcu8AppIconData), u32Len);
         }//if(OSAL_NULL != pcu8AppIconData)
         bPostSuccess = bPostResponse(oGetAppImageMR, rcUsrCntxt);
         oGetAppImageMR.vDestroy();
      }
         break;
      default:
         ETG_TRACE_ERR((" vPostAppIconDataResult: Invalid Function ID : 0x%x ",
               rcUsrCntxt.u32FuncID));
         break;
   } //switch (rcUsrCntxt.u32FuncID)

   if (FALSE == bPostSuccess)
   {
      ETG_TRACE_ERR((" vPostAppIconDataResult: GetApplicationIcon or GetApplicationImage MR post failed! "));
   }//if (FALSE == bPostSuccess) 
}//! end of vPostAppIconDataResult()

/***************************************************************************
** FUNCTION: tVoid devprj_tclService::vPostSendKeyEvent(tenResponseCode e...
***************************************************************************/
tVoid devprj_tclService::vPostSendKeyEvent(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode,
      const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);

   ETG_TRACE_USR1(("devprj_tclService::vPostSendKeyEvent entered: Received ResponseCode = %d, ErrorCode = %d ",
         ETG_ENUM(RESPONSE_CODE, enResponseCode), ETG_ENUM(ERROR_CODE, enErrorCode)));

   //! @Note: Do nothing. DispatchSwitchEvent method does not have a method result.

}//! end of vPostSendKeyEvent()

/***************************************************************************
** FUNCTION: tVoid devprj_tclService::vPostDeviceDisplayContext(t_U32 ...
***************************************************************************/
tVoid devprj_tclService::vPostDeviceDisplayContext(t_U32 u32DeviceHandle,
      t_Bool bDisplayFlag,
      tenDisplayContext enDisplayContext,
      const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);

   ETG_TRACE_USR1(("devprj_tclService::vPostDeviceDisplayContext() entered: "
         "Received DeviceHandle = 0x%x, DisplayFlag = %u, DisplayContext = %u ",
         u32DeviceHandle,
         ETG_ENUM(BOOL, bDisplayFlag),
         ETG_ENUM(DISPLAY_CONTEXT, enDisplayContext)));

   m_oDispProjLock.s16Lock();
   if(NULL != m_poMainAppl)
   {
      sbDipoProjTimerRun = (true == sbDipoProjTimerRun) ? 
	                       (!(m_poMainAppl->bStopTimer(DIPO_BLACKSCREEN_RECOVERY_TIMER_ID))):(sbDipoProjTimerRun);
      ETG_TRACE_USR4(("Dipo blackscreen recovery Timer stopped \n"));
   }

   //Store info in DisplayProjectionRequest msg
   //@Note: DeviceDisplay context is currently used for DiPO
   m_oStDispProjRequest.u32DeviceHandle = u32DeviceHandle;
   m_oStDispProjRequest.bStartStopFlag  = bDisplayFlag;
   m_oStDispProjRequest.e8RequestedDisplayContext.enType = static_cast<devprj_tenPrjctdDispCntxt>(enDisplayContext);

   //! Update Clients about ProjectedDisplayContext change
   if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_DISPLAYPROJECTIONREQUEST_))
   {
      ETG_TRACE_ERR((" vPostDeviceDisplayContext: Updating DisplayProjectionRequest.Status failed! "));
   }
   m_oDispProjLock.vUnlock();
   
}//! end of vPostDeviceDisplayContext()

/***************************************************************************
** FUNCTION: t_Void devprj_tclService::vPostSessionStatusInfo()
***************************************************************************/
t_Void devprj_tclService::vPostSessionStatusInfo(t_U32 u32DeviceHandle,
       tenDeviceCategory enDevCat,
       tenSessionStatus enSessionStatus)
{
   ETG_TRACE_USR1(("devprj_tclService::vPostSessionStatusInfo() entered: "
         "Received DeviceHandle = 0x%x, DevCat = %u, SessionStatus = %u ",
         u32DeviceHandle,
         ETG_ENUM(DEVICE_CATEGORY, enDevCat),
         ETG_ENUM(SESSION_STATUS, enSessionStatus)));

   tBool bStartStopFlag = (e8_SESSION_ACTIVE == enSessionStatus);
   ETG_TRACE_USR4((" vPostSessionStatusInfo: StartStopFlag = %d (Previous StartStopFlag = %d) ",
         bStartStopFlag, m_oStDispProjRequest.bStartStopFlag));

   if ((e8_SESSION_SUSPENDED_NON_ML_APP != enSessionStatus)
      &&
      (e8_SESSION_SUSPENDED_NON_DRIVE_APP != enSessionStatus)
      &&
      (bStartStopFlag != m_oStDispProjRequest.bStartStopFlag))
   {
      //Store info in DisplayProjectionRequest msg
      //@Note: SessionStatus is currently used for ML
      m_oStDispProjRequest.u32DeviceHandle = u32DeviceHandle;
      m_oStDispProjRequest.bStartStopFlag = bStartStopFlag;
      m_oStDispProjRequest.e8RequestedDisplayContext.enType = DEVPRJ_DISP_CTXT_UNKNOWN; //data unused for ML

      //! Update Clients about ProjectedDisplayContext change
      if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_DISPLAYPROJECTIONREQUEST_))
      {
         ETG_TRACE_ERR((" vPostSessionStatusInfo: Updating DisplayProjectionRequest.Status failed! "));
      }
   }//if ((e8_SESSION_SUSPENDED_NON_ML_APP != enSessionStatus)&&...)
}

/***************************************************************************
** FUNCTION: t_Void devprj_tclService::vPostNotificationInfo()
***************************************************************************/
t_Void devprj_tclService::vPostNotificationInfo(t_U32 u32DeviceHandle,
      t_U32 u32AppHandle,
      const trNotiData& corfrNotificationData)
{
   ETG_TRACE_USR1(("devprj_tclService::vPostNotificationInfo() entered "));
   ETG_TRACE_USR4((" vPostNotificationInfo: DeviceHandle = 0x%x, AppHandle = 0x%x, NotiID = 0x%x, NotiAppID = 0x%x ",
         u32DeviceHandle,u32AppHandle,corfrNotificationData.u32NotiID,corfrNotificationData.u32NotiAppID));
   ETG_TRACE_USR4((" vPostNotificationInfo: Notification Title = %s ",
         corfrNotificationData.szNotiTitle.c_str()));
   ETG_TRACE_USR4((" vPostNotificationInfo: Notification Body = %s ",
         corfrNotificationData.szNotiBody.c_str()));
   ETG_TRACE_USR4((" vPostNotificationInfo: Number of NotificationActions = %d ",
         corfrNotificationData.tvecNotiActionList.size()));

   //Print Notification actions list info
   for (t_U8 u8LstIndx = 0; u8LstIndx < corfrNotificationData.tvecNotiActionList.size(); ++u8LstIndx)
   {
      t_U32 u32NotiActionID = corfrNotificationData.tvecNotiActionList[u8LstIndx].u32NotiActionID;
      t_String szNotiActionName = corfrNotificationData.tvecNotiActionList[u8LstIndx].szNotiActionName.c_str();
      ETG_TRACE_USR4((" vPostNotificationInfo: NotificationAction %d: ActionID = 0x%x, ActionName = %s ",
            (u8LstIndx + 1), u32NotiActionID, szNotiActionName.c_str()));
   }//for (t_U8 u8LstIndx = 0; ...)
   
   if (OSAL_NULL != m_poAlertMngrClient)
   {
      //! Send notification event as an alert
      tBool bAlertSent = m_poAlertMngrClient->bTriggerNotificationAlert(corfrNotificationData);

      //! Need to send default notification action to clear the event for below usecases:
      //! 1. If notification event is not processed (alert not sent)
      //! 2. If notification event is processed (alert sent), but the notification has no actions 
      //!    (since alert button event would not be received for this).
      if (
         (OSAL_NULL != m_poSpiCmdInterface)
         &&
         ((FALSE == bAlertSent) || (0 == corfrNotificationData.tvecNotiActionList.size()))
         )
      {
          ETG_TRACE_USR1((" Clearing event by sending default notification action. "));

          m_poSpiCmdInterface->vInvokeNotificationAction(u32DeviceHandle,
               u32AppHandle, corfrNotificationData.u32NotiID, 0, corEmptyUsrContext);

      }//if (((TRUE == bAlertSent) && ...))
   }//if (OSAL_NULL != m_poAlertMngrClient)
} //! end of vPostNotificationInfo()

/***************************************************************************
** FUNCTION: t_Void devprj_tclService::vPostBluetoothDeviceStatus(...)
***************************************************************************/
t_Void devprj_tclService::vPostBluetoothDeviceStatus(t_U32 u32BluetoothDevHandle,
      t_U32 u32ProjectionDevHandle,
      tenBTChangeInfo enBTChange,
      t_Bool bSameDevice,
      t_Bool bCallActive)
{
   ETG_TRACE_USR1(("devprj_tclService::vPostBluetoothDeviceStatus entered "));
   ETG_TRACE_USR4((" vPostBluetoothDeviceStatus: BluetoothDevHandle = 0x%x ", u32BluetoothDevHandle));
   ETG_TRACE_USR4((" vPostBluetoothDeviceStatus: ProjectionDevHandle = 0x%x ", u32ProjectionDevHandle));
   ETG_TRACE_USR4((" vPostBluetoothDeviceStatus: BTChange = %d ", ETG_ENUM(BT_CHANGE_INFO, enBTChange)));
   ETG_TRACE_USR4((" vPostBluetoothDeviceStatus: SameDevice = %d ", ETG_ENUM(BOOL, bSameDevice)));
   ETG_TRACE_USR4((" vPostBluetoothDeviceStatus: CallActive = %d ", ETG_ENUM(BOOL, bCallActive)));

   //! If BT & Projection devices are same, accept the switch
   if (true == bSameDevice)
   {
      SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);
      if (OSAL_NULL != m_poSpiCmdInterface)
      {
         m_poSpiCmdInterface->vInvokeBluetoothDeviceAction(u32BluetoothDevHandle,
               u32ProjectionDevHandle, enBTChange);
      }
   }//if (true == bSameDevice)
   //! If BT & Projection devices are different, trigger device switch
   else
   {
      //Store BluetoothDeviceStatus details.
      m_oStBTDevStatusMsg.u8BTDeviceHandle = u32BluetoothDevHandle;
      m_oStBTDevStatusMsg.u32DeviceHandle = u32ProjectionDevHandle;
      m_oStBTDevStatusMsg.e8BTChangeInfo.enType = static_cast<devprj_tenBTChangeInfo>(enBTChange);
      m_oStBTDevStatusMsg.bCallActiveStatus = bCallActive;

      //! Update Clients about BluetoothDeviceStatus change
      if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_BLUETOOTHDEVICESTATUS))
      {
         ETG_TRACE_ERR((" vPostBluetoothDeviceStatus: Updating BluetoothDeviceStatus.Status failed! "));
      }
   }
}//! end of vPostBluetoothDeviceStatus()

/***************************************************************************
 ** FUNCTION: t_Void devprj_tclService::vPostBTPairingRequired(...)
 ***************************************************************************/
t_Void devprj_tclService::vPostBTPairingRequired(const t_String& rfcoszBTAddress,
      t_Bool bPairingRequired)
{
   ETG_TRACE_USR1(("devprj_tclService::vPostBTPairingRequired() entered: "
         "bPairingRequired = %d, szBTAddress = %s ",
         ETG_ENUM(BOOL, bPairingRequired), rfcoszBTAddress.c_str()));

   m_oStBTPairRequired.bPairingReqd = bPairingRequired;
   m_oStBTPairRequired.sDevBTAddr.bSet(rfcoszBTAddress.c_str());

   //! Update Clients about BTPairingRequired change
   if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_BTPAIRINGREQUIRED))
   {
      ETG_TRACE_ERR((" vPostBTPairingRequired: Updating BTPairingRequired.Status failed! "));
   }
}

/***************************************************************************
** FUNCTION: t_Void devprj_tclService::vSendSessionStatusInfo()
***************************************************************************/
t_Void devprj_tclService::vSendSessionStatusInfo(t_U32 u32DeviceHandle,
      tenDeviceCategory enDevCat,
      tenSessionStatus enSessionStatus)
{
   ETG_TRACE_USR1(("devprj_tclService::vSendSessionStatusInfo() entered: "
         "Received DeviceHandle = 0x%x, DevCat = %u, SessionStatus = %u ",
         u32DeviceHandle,
         ETG_ENUM(DEVICE_CATEGORY, enDevCat),
         ETG_ENUM(SESSION_STATUS, enSessionStatus)));

   //Update the status to HMI
   vPostSessionStatusInfo(u32DeviceHandle,enDevCat,enSessionStatus);
}

/***************************************************************************
** FUNCTION: t_Void devprj_tclService::vUpdateAppBlockingInfo()
***************************************************************************/
t_Void devprj_tclService::vUpdateAppBlockingInfo(t_U32 u32DeviceHandle,
                                                 tenDeviceCategory enDevCat,
                                                 tenSessionStatus enSessionStatus)
{
   ETG_TRACE_USR1(("devprj_tclService::vUpdateAppBlockingInfo() entered: "
         "Received DeviceHandle = 0x%x, DevCat = %u, SessionStatus = %u "
         ,u32DeviceHandle, ETG_ENUM(DEVICE_CATEGORY, enDevCat),
         ETG_ENUM(SESSION_STATUS, enSessionStatus)));

   //Update the status to HMI
   vPostSessionStatusInfo(u32DeviceHandle,enDevCat,enSessionStatus);
}


/***************************************************************************
** FUNCTION: t_Void devprj_tclService::vUpdateActiveAppInfo()
***************************************************************************/
t_Void devprj_tclService::vUpdateActiveAppInfo(t_U32 u32DeviceHandle,
                                               tenDeviceCategory enDevCat,
                                               const t_U32 cou32AppId,
                                               const tenAppCertificationInfo coenAppCertInfo)
{
   ETG_TRACE_USR1(("devprj_tclService::vUpdateActiveAppInfo: "
      "Received DeviceHandle = 0x%x, DevCat = %u"
      "AppHandle=0x%x AppCertType-%d",
      u32DeviceHandle,ETG_ENUM(DEVICE_CATEGORY, enDevCat),
      cou32AppId,ETG_ENUM(APP_CERTIFICATION_STATUS,coenAppCertInfo)));

   devprj_tenAppCertType enAppCertType = devprj_tFiAppCertType::FI_EN_E8PARK_ONLY;
   if((e8DRIVE_CERTIFIED_ONLY == coenAppCertInfo) || (e8DRIVE_CERTIFIED == coenAppCertInfo))
   {
      enAppCertType = devprj_tFiAppCertType::FI_EN_E8NO_RESTRICTION;
   }//if((e8DRIVE_CERTIFIED_ONLY == coenAppCertInfo)

   if ((m_oStMLActiveAppMsg.u32ApplicationHandle != cou32AppId) ||
      (m_oStMLActiveAppMsg.e8ApplicationCertificateType.enType != enAppCertType))
   {
      m_oStMLActiveAppMsg.u32ApplicationHandle = cou32AppId;
      m_oStMLActiveAppMsg.e8ApplicationCertificateType.enType = enAppCertType;


      ETG_TRACE_USR4((" devprj_tclService::vUpdateActiveAppInfo() A new application is moved to FG"
         " AppHandle=0x%x AppCertType-%d", cou32AppId,enAppCertType));

      //! Update Clients about MLActiveApp status
      if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_MLACTIVEAPP))
      {
         ETG_TRACE_ERR((" devprj_tclService::vUpdateActiveAppInfo(): Updating MLActiveApp.Status failed! "));
      }//if (AIL_EN_N_NO_ERROR !=...)
   }//if ((m_oStMLActiveAppMsg.u
}

/***************************************************************************
** FUNCTION: t_Void devprj_tclService::vUpdateDevAuthAndAccessInfo()
***************************************************************************/
t_Void devprj_tclService::vUpdateDevAuthAndAccessInfo(const t_U32 cou32DeviceHandle,
         const t_Bool cobHandsetInteractionReqd)
{
   ETG_TRACE_USR1(("devprj_tclService::vUpdateDevAuthAndAccessInfo: "
            "Received DeviceHandle = 0x%x, Handset Interaction Required = %d",
      cou32DeviceHandle, ETG_ENUM(BOOL,cobHandsetInteractionReqd)));

   if(0 != cou32DeviceHandle)
   {
      m_oProjDevAuthAndAccess.u16NumConnectedDevices = scou16SingleConnectedDevices;

      m_oDevUserParamItem.u32DeviceHandle             = cou32DeviceHandle;
      m_oDevUserParamItem.bHandsetInteractionRequired = cobHandsetInteractionReqd;

      //! Add DeviceListItem in DeviceList

      std::vector<tDevUserParamItem> &corfvecDeviceList = m_oProjDevAuthAndAccess.oDeviceUserParameters.oItems;
      std::vector<tDevUserParamItem>::iterator coitDevList;
      t_Bool bDevFound = false;
      for(coitDevList = corfvecDeviceList.begin(); coitDevList != corfvecDeviceList.end(); ++coitDevList)
      {
         if (coitDevList->u32DeviceHandle == cou32DeviceHandle)
         {
            coitDevList->bHandsetInteractionRequired = cobHandsetInteractionReqd;
            bDevFound = true;
            break;
         }
      }
      if (false == bDevFound)
      {
         m_oProjDevAuthAndAccess.oDeviceUserParameters.oItems.push_back(m_oDevUserParamItem);
      }
   }
   //If device is not active , the list should be empty
   else
   {
      m_oProjDevAuthAndAccess.u16NumConnectedDevices = scou16NoConnectedDevice;
      m_oProjDevAuthAndAccess.oDeviceUserParameters.oItems.clear();
   }

   //! Update Clients about PROJECTIONDEVICEAUTHANDACCESSIBILITY status
   if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_PROJECTIONDEVICEAUTHANDACCESSIBILITY))
   {
      ETG_TRACE_ERR((" devprj_tclService::vUpdateDevAuthAndAccessInfo(): Updating Authorization Status failed! "));
   }//if (AIL_EN_N_NO_ERROR !=...)
}

/***************************************************************************
** FUNCTION: t_Void devprj_tclService::vUpdateDevAuthStatus()
***************************************************************************/
t_Void devprj_tclService::vUpdateDevAuthStatus(const t_U32 cou32DeviceHandle,
         const t_Bool cobUserAuthStatus)
{
   ETG_TRACE_USR1(("devprj_tclService::vUpdateDevAuthStatus: "
            "Received DeviceHandle = 0x%x, Authorization status = %d",
      cou32DeviceHandle, ETG_ENUM(BOOL,cobUserAuthStatus)));

   if(0 != cou32DeviceHandle)
   {
      m_oProjDevAuthAndAccess.u16NumConnectedDevices = scou16SingleConnectedDevices;

      m_oDevUserParamItem.u32DeviceHandle             = cou32DeviceHandle;
      m_oDevUserParamItem.bUserAuthorizationStatus    = cobUserAuthStatus;

      //! Add DeviceListItem in DeviceList
      std::vector<tDevUserParamItem> &corfvecDeviceList = m_oProjDevAuthAndAccess.oDeviceUserParameters.oItems;
      std::vector<tDevUserParamItem>::iterator coitDevList;
      t_Bool bDevFound = false;
      for(coitDevList = corfvecDeviceList.begin(); coitDevList != corfvecDeviceList.end(); ++coitDevList)
      {
         if (coitDevList->u32DeviceHandle == cou32DeviceHandle)
         {
            coitDevList->bUserAuthorizationStatus = cobUserAuthStatus;
            bDevFound = true;
            break;
         }
      }
      if (false == bDevFound)
      {
         m_oProjDevAuthAndAccess.oDeviceUserParameters.oItems.push_back(m_oDevUserParamItem);
      }
   }
   //If device is not active , the list should be empty
   else
   {
      m_oProjDevAuthAndAccess.u16NumConnectedDevices = scou16NoConnectedDevice;
      m_oProjDevAuthAndAccess.oDeviceUserParameters.oItems.clear();
   }

   //! Update Clients about PROJECTIONDEVICEAUTHANDACCESSIBILITY status
   if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_PROJECTIONDEVICEAUTHANDACCESSIBILITY))
   {
      ETG_TRACE_ERR((" devprj_tclService::vUpdateDevAuthStatus(): Updating Authorization Status failed! "));
   }//if (AIL_EN_N_NO_ERROR !=...)
}

/***************************************************************************
** FUNCTION: t_Void devprj_tclService::vUpdateSessionStatusInfo()
***************************************************************************/
t_Void devprj_tclService::vUpdateSessionStatusInfo(t_U32 u32DeviceHandle,
                                tenDeviceCategory enDevCat,
                                tenSessionStatus enSessionStatus)
{
   ETG_TRACE_USR1(("devprj_tclService::vUpdateSessionStatusInfo() entered: "
         "Received DeviceHandle = 0x%x, DevCat = %u, Received SessionStatus = %u current session status = %u",
         u32DeviceHandle,
         ETG_ENUM(DEVICE_CATEGORY, enDevCat),
         ETG_ENUM(SESSION_STATUS, enSessionStatus),
         ETG_ENUM(SESSION_STATUS, enSessionStatus)));

   if(((e8DEV_TYPE_DIPO == enDevCat)||(e8DEV_TYPE_ANDROIDAUTO == enDevCat)) &&
      (e8_SESSION_ACTIVE != m_enDeviceSessionStatus) &&
      (e8_SESSION_ACTIVE == enSessionStatus))
   {
      m_enDeviceSessionStatus = enSessionStatus;
	  // Clear the Source Number used for Audio ducking at the start of Session.
	  su8AudDuckSrcNum = 0;
      //! Update clients about DeviceConnectionList change.
      if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_DEVICECONNECTIONLIST))
      {
         ETG_TRACE_ERR((" vUpdateSessionStatusInfo: Updating DeviceConnectionList.Status failed! "));
      }
   } //if(e8DEV_TYPE_DIPO == enDevCat)
   if((e8DEV_TYPE_DIPO == enDevCat) && (e8_SESSION_ACTIVE == enSessionStatus))
   {
      vUpdateDayNightMode();
   }
#ifdef VARIANT_S_FTR_ENABLE_GM_CARPLAY_MEDIA
   else if((e8DEV_TYPE_DIPO == enDevCat) &&
      (e8_SESSION_INACTIVE == enSessionStatus))
   {
      //! This logic is used to send the response of stop media playback 
      //  to media player in case of session termination without any teardown.
      //  Added to solve the CarPlay to Pandora switch issue.

      //! Send StopMediaPlayback result, if it was triggered.
      if ((corEmptyUsrContext != rStopMediaPlaybackCtxt) && 
         (OSAL_NULL != m_poSpiCmdInterface))
      {
         devprj_tMRStopMediaPb oStopMediaPbMR;
         oStopMediaPbMR.u8DeviceTag = m_poSpiCmdInterface->u32GetSelectedDeviceHandle();
         if (FALSE == bPostResponse(oStopMediaPbMR, rStopMediaPlaybackCtxt))
         {
            ETG_TRACE_ERR((" vUpdateSessionStatusInfo: StopMediaPlayback MR post failed! "));
         }
         //! Clear context
         rStopMediaPlaybackCtxt = corEmptyUsrContext;
      }//if (corEmptyUsrContext != rStopMediaPlaybackCtxt)
   }
#endif
   m_enDeviceSessionStatus = enSessionStatus;
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
}

/***************************************************************************
* !Handler method declarations used by message map.
***************************************************************************/

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnMSChangeDevProjEnable(amt_tclS...
**************************************************************************/
tVoid devprj_tclService::
      vOnMSChangeDevProjEnable(amt_tclServiceData* poMessage) const
{
   ETG_TRACE_USR1(("devprj_tclService::vOnMSChangeDevProjEnable entered "));

   //! Create an XFiObjHandler object of DevPrj ChangeDevProjEnable MS type to extract data from poMessage   
   devprj_tXFiMSChangeDevPrjEnable oXFiChngDevPrj(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);

   //! Call matching ChangeDevProjEnable method in SpiCmdInterface.
   if (true == oXFiChngDevPrj.bIsValid())
   {
      ETG_TRACE_USR4((" vOnMSChangeDevProjEnable: Received DeviceHandle = 0x%x, DeviceProjectionEnabled = %u ",
            oXFiChngDevPrj.u32DeviceHandle, ETG_ENUM(BOOL, oXFiChngDevPrj.bDeviceProjectionEnabled)));

      SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);

      if (OSAL_NULL != m_poSpiCmdInterface)
      {
          //! Extract the DevPrj user context details
         trUserContext rUsrCtxt;
         CPY_TO_USRCNTXT(poMessage, rUsrCtxt);

         t_U32 u32DevHandle = static_cast<t_U32>(oXFiChngDevPrj.u32DeviceHandle);
         tenEnabledInfo enEnableInfo = (TRUE == oXFiChngDevPrj.bDeviceProjectionEnabled) ?
            e8USAGE_ENABLED : e8USAGE_DISABLED;

         m_poSpiCmdInterface->vSetDeviceUsagePreference(u32DevHandle,
               e8DEV_TYPE_UNKNOWN, enEnableInfo, rUsrCtxt);

      } // if (OSAL_NULL != m_poSpiCmdInterface)
   }
   else
   {
      ETG_TRACE_ERR((" vOnMSChangeDevProjEnable: Invalid message received. "));
   }

}//! end of vOnMSChangeDevProjEnable()

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnMSLaunchApp(amt_tclServiceData*...
***************************************************************************/
tVoid devprj_tclService::
      vOnMSLaunchApp(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("devprj_tclService::vOnMSLaunchApp entered "));

   //! Create an XFiObjHandler object of DevPrj LaunchApp MS type to extract data from poMessage   
   devprj_tXFiMSLaunchApp oXFiLaunchApp(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);

   //! Call corresponding LaunchApp method in SpiCmdInterface.
   if (true == oXFiLaunchApp.bIsValid())
   {
      //Get Telephone number string
      std::string szTelephoneNum;
      GET_STRINGDATA_FROM_FI_STRINGOBJ(oXFiLaunchApp.sTelephoneNumber, MOSTFI_CHAR_SET_UTF8, szTelephoneNum);

      ETG_TRACE_USR4((" vOnMSLaunchApp: Received DeviceHandle = 0x%x ", oXFiLaunchApp.u32DeviceHandle));
      ETG_TRACE_USR4((" vOnMSLaunchApp: Received ApplicationHandle = 0x%x ", oXFiLaunchApp.u32ApplicationHandle));
      ETG_TRACE_USR4((" vOnMSLaunchApp: Received DigitalIPodOutApplication = %u ", (tU8)(oXFiLaunchApp.e8DigitalIPodOutApplication.enType)));
      ETG_TRACE_USR4((" vOnMSLaunchApp: Received TelephoneNumber = %s ", szTelephoneNum.c_str()));
      ETG_TRACE_USR4((" vOnMSLaunchApp: Received ECNRSetting = %u ", (tU8)(oXFiLaunchApp.e8EchoCancellationNoiseReductionSetting.enType)));

      SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);

      if (OSAL_NULL != m_poSpiCmdInterface)
      {
         //! Extract the DevPrj user context details
         trUserContext rUsrCtxt;
         CPY_TO_USRCNTXT(poMessage, rUsrCtxt);

         tenDiPOAppType enDiPOAppType = static_cast<tenDiPOAppType>(oXFiLaunchApp.e8DigitalIPodOutApplication.enType);
         tenEcnrSetting enEcnrSetting = static_cast<tenEcnrSetting>(oXFiLaunchApp.e8EchoCancellationNoiseReductionSetting.enType);

         m_poSpiCmdInterface->vLaunchApp(oXFiLaunchApp.u32DeviceHandle, e8DEV_TYPE_UNKNOWN,
               oXFiLaunchApp.u32ApplicationHandle, enDiPOAppType,
               szTelephoneNum.c_str(), enEcnrSetting, rUsrCtxt);

      } //if (OSAL_NULL != m_poSpiCmdInterface)
   }
   else
   {
      ETG_TRACE_ERR((" vOnMSLaunchApp: Invalid message received. "));
   }

}//! end of vOnMSLaunchApp()
     
/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnMSTerminateApp(amt_tclServiceDa...
***************************************************************************/
tVoid devprj_tclService::
      vOnMSTerminateApp(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("devprj_tclService::vOnMSTerminateApp entered "));

   //! Create an XFiObjHandler object of DevPrj TerminateApp MS type to extract data from poMessage   
   devprj_tXFiMSTerminateApp oXFiTermApp(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);

   //! Call corresponding TerminateApp method in SpiCmdInterface.
   if (true == oXFiTermApp.bIsValid())
   {
      ETG_TRACE_USR4((" vOnMSTerminateApp: DeviceHandle = 0x%x, ApplicationHandle = 0x%x ",
            oXFiTermApp.u32DeviceHandle, oXFiTermApp.u32ApplicationHandle));

      SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);
      
      if (OSAL_NULL != m_poSpiCmdInterface)
      {
         //! Extract the DevPrj user context details
         trUserContext rUsrCtxt;
         CPY_TO_USRCNTXT(poMessage, rUsrCtxt);

         m_poSpiCmdInterface->vTerminateApp(oXFiTermApp.u32DeviceHandle,
            oXFiTermApp.u32ApplicationHandle, rUsrCtxt);
      }
   }
   else
   {
      ETG_TRACE_ERR((" vOnMSTerminateApp: Invalid message received. "));
   }

}//! end of vOnMSTerminateApp()

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnMSGetAppIcon(amt_tclServiceData...
***************************************************************************/
tVoid devprj_tclService::vOnMSGetAppIcon(amt_tclServiceData* poMessage) const
{
   ETG_TRACE_USR1(("devprj_tclService::vOnMSGetAppIcon entered "));

   //! Create an XFiObjHandler object of DevPrj GetApplicationIconMS type to extract data from poMessage
   devprj_tXFiMSGetAppIcon oXFiGetAppIcon(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);

   if (true == oXFiGetAppIcon.bIsValid())
   {
      //! Get Application Icon URL string
      std::string szAppIconURL;
      GET_STRINGDATA_FROM_FI_STRINGOBJ(oXFiGetAppIcon.sApplicationIconURL, MOSTFI_CHAR_SET_UTF8, szAppIconURL);
      ETG_TRACE_USR2((" vOnMSGetAppIcon: Received Application Icon URL = %s ", szAppIconURL.c_str()));

      SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);

      //! Call corresponding GetAppIcon method in SpiCmdInterface.
      if ((FALSE == szAppIconURL.empty()) && (OSAL_NULL != m_poSpiCmdInterface))
      {
         //! Extract the DevPrj user context details
         trUserContext rUsrCtxt;
         CPY_TO_USRCNTXT(poMessage, rUsrCtxt);

         m_poSpiCmdInterface->vGetAppIconData(szAppIconURL, rUsrCtxt);
      }
      //! Post error msg data is invalid
      else if (TRUE == szAppIconURL.empty())
      {
         vGenerateError(poMessage);
      }
   }
   else
   {
      ETG_TRACE_ERR((" vOnMSGetAppIcon: Invalid message received. "));
   }

}//! end of vOnMSGetAppIcon()

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnMSGetAppImage(amt_tclServiceData...
***************************************************************************/
tVoid devprj_tclService::vOnMSGetAppImage(amt_tclServiceData* poMessage) const
{
   ETG_TRACE_USR1(("devprj_tclService::vOnMSGetAppImage entered "));

    //! Create an XFiObjHandler object of DevPrj GetApplicationImage MS type to extract data from poMessage
   devprj_tXFiMSGetAppImage oXFiGetAppImage(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);

   if (true == oXFiGetAppImage.bIsValid())
   {
      //! Get Application Image URL string
      std::string szAppImageURL;
      GET_STRINGDATA_FROM_FI_STRINGOBJ(oXFiGetAppImage.sImageURL, MOSTFI_CHAR_SET_UTF8, szAppImageURL);
      ETG_TRACE_USR2((" vOnMSGetAppImage: Received Application Image URL = %s ", szAppImageURL.c_str()));

      SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);

      //! Call corresponding GetAppImage method in SpiCmdInterface.
      if ((FALSE == szAppImageURL.empty()) && (OSAL_NULL != m_poSpiCmdInterface))
      {
         //! Extract the DevPrj user context details
         trUserContext rUsrCtxt;
         CPY_TO_USRCNTXT(poMessage, rUsrCtxt);

         m_poSpiCmdInterface->vGetAppIconData(szAppImageURL, rUsrCtxt);
      }
      //! Post error msg data is invalid
      else if (TRUE == szAppImageURL.empty())
      {
         vGenerateError(poMessage);
      }
   }
   else
   {
      ETG_TRACE_ERR((" vOnMSGetAppImage: Invalid message received. "));
   }

}//! end of vOnMSGetAppImage()

/**************************************************************************
** FUNCTION:  tBool devprj_tclService::vOnMSSelectDevice(devprj_tStAp...
**************************************************************************/
tVoid devprj_tclService::
      vOnMSSelectDevice(amt_tclServiceData* poMessage) const
{
   ETG_TRACE_USR1(("devprj_tclService::vOnMSSelectDevice() entered "));

   //! Create an XFiObjHandler object of DevPrj SelectDevice MS type to extract data from poMessage
   devprj_tXFiMSSelectDev oXFiSelectDev(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);

   //! Call corresponding SelectDevice method in SpiCmdInterface.
   if (true == oXFiSelectDev.bIsValid())
   {
      ETG_TRACE_USR4((" vOnMSSelectDevice: Received DeviceHandle = 0x%x ", oXFiSelectDev.u32DeviceHandle));

      SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);

      if (OSAL_NULL != m_poSpiCmdInterface)
      {
         //! Extract the DevPrj user context details
         trUserContext rUsrCtxt;
         CPY_TO_USRCNTXT(poMessage, rUsrCtxt);

         t_U32 u32SelDevHandle = (cou16DevHandle_Mask) & (static_cast<t_U32> (oXFiSelectDev.u32DeviceHandle));
         //@Note:Upper 16 bits of device handle is masked since its currently not used.

         tenDeviceConnectionReq enDevConnReq = e8DEVCONNREQ_SELECT;

         //! For a DeselectDevice request, initialize appropriate values
         if (cou16DevHandle_Deselect == u32SelDevHandle)
         {
            enDevConnReq = e8DEVCONNREQ_DESELECT;
            u32SelDevHandle = m_poSpiCmdInterface->u32GetSelectedDeviceHandle();
            ETG_TRACE_USR3((" vOnMSSelectDevice: Retrieved Selected DeviceHandle = 0x%x ", u32SelDevHandle));
         }

         m_poSpiCmdInterface->vSelectDevice(u32SelDevHandle,
               e8USB_CONNECTED, /*Device Connection Type*/
               enDevConnReq,
               e8USAGE_ENABLED, /*DAP Usage - not used*/
               e8USAGE_ENABLED, /*CDB Usage - not used*/
               //! Send default value ,as category will not be sent from HMI
               e8DEV_TYPE_UNKNOWN,
               rUsrCtxt);

      } // if (OSAL_NULL != m_poSpiCmdInterface)
   }
   else
   {
      ETG_TRACE_ERR((" vOnMSSelectDevice: Invalid message received. "));
   }

}//! end of vOnMSSelectDevice()

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnMSDispatchSwitchEvent(amt_tclSe...
***************************************************************************/
tVoid devprj_tclService::vOnMSDispatchSwitchEvent(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("devprj_tclService::vOnMSDispatchSwitchEvent entered "));

   //! Create an XFiObjHandler object of devprj DispatchSwitchEvent MS type to extract data from poMessage
   devprj_tXFiMSDispatchSwitchEv oXFiDispSwitchEv(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);

   //! Call corresponding Switch Event method in SpiCmdInterface.
   if (true == oXFiDispSwitchEv.bIsValid())
   {
      ETG_TRACE_USR4((" vOnMSDispatchSwitchEvent: Received SwitchEventTypeEnumeration = %u, SwitchEnumeration = %u ",
         oXFiDispSwitchEv.e8SwitchEventTypeEnumeration.enType,
         oXFiDispSwitchEv.e8SwitchEnumeration.enType));

      //! Extract the DevPrj user context details
      trUserContext rUsrCtxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCtxt);
      tBool bSetMute = false;

      SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);

      if ((OSAL_NULL != m_poSpiCmdInterface)&& (OSAL_NULL != m_poKeyConfig))
      {
         tenKeyMode enKeyMode = static_cast<tenKeyMode>(oXFiDispSwitchEv.e8SwitchEventTypeEnumeration.enType);
         tenKeyCode enKeyCode = m_poKeyConfig->enGetKeyCode(oXFiDispSwitchEv.e8SwitchEnumeration.enType);

         ETG_TRACE_USR4((" vOnMSDispatchSwitchEvent: KeyMode = %d, KeyCode = %d ",
                  ETG_ENUM(KEY_MODE, enKeyMode), ETG_ENUM(KEY_CODE, enKeyCode)));

         if(e32INVALID_KEY!= enKeyCode)
         {
            m_poKeyConfig ->vConfigDevPrjKeys(enKeyMode,enKeyCode,bSetMute,rUsrCtxt);

            if(true == bSetMute)
            {
               (true == m_bMuteFlag)?vOnSetMute(false):vOnSetMute(true);
               bSetMute = false;
            }
         }
      }//if (OSAL_NULL != m_poSpiCmdInterface)
   }
   else
   {
      ETG_TRACE_ERR((" vOnMSDispatchSwitchEvent: Invalid message received. "));
   }
}//! end of vOnMSDispatchSwitchEvent()


/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnMSDispatchMenuEncdrChange(amt_t...
***************************************************************************/
tVoid devprj_tclService::vOnMSDispatchMenuEncdrChange(amt_tclServiceData* poMessage) const
{
   SPI_INTENTIONALLY_UNUSED(poMessage);

   ETG_TRACE_USR1(("devprj_tclService::vOnMSDispatchMenuEncdrChange entered "));

   //! Create an XFiObjHandler object of devprj MenuEncodeChange MS type
   //! to extract data from poMessage
   devprj_tXFiMSDispatchMenuEncChange oXFiMenuEncdrChange(*poMessage,DEVPROJ_SERVICE_FI_MAJOR_VERSION);

   //! Call corresponding Switch Event method in SpiCmdInterface.
   if (true == oXFiMenuEncdrChange.bIsValid())
   {
      ETG_TRACE_USR4((" Received EncoderDeltaCounts = %d",oXFiMenuEncdrChange.s8EncoderDeltaCounts));

      trUserContext rUsrCtxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCtxt);

      if (NULL != m_poSpiCmdInterface)
      {
         t_U32 u32SelDevHandle = m_poSpiCmdInterface->u32GetSelectedDeviceHandle();
         m_poSpiCmdInterface->vSendKnobKeyEvent(u32SelDevHandle,oXFiMenuEncdrChange.s8EncoderDeltaCounts, rUsrCtxt);
      }
   }
   else
   {
      ETG_TRACE_ERR((" vOnMSDispatchMenuEncdrChange: Invalid message received. "));
   }

}//! end of vOnMSDispatchMenuEncdrChange()
     
/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnMSDevProjAlertBtnEvent(amt_tclS...
***************************************************************************/
tVoid devprj_tclService::vOnMSDevProjAlertBtnEvent(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("devprj_tclService::vOnMSDevProjAlertBtnEvent entered "));

   //! Create an XFiObjHandler object of devprj DeviceProjectionAlertButtonEvent MS type
   //! to extract data from poMessage
   devprj_tXFiMSDevPrjAlertBtnEv oXFiAlertBtnEvent(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);

   //! Call corresponding Switch Event method in SpiCmdInterface.
   if (true == oXFiAlertBtnEvent.bIsValid())
   {
      //Extract Alert ID from CmdCounter
      trUserContext rUsrCtxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCtxt);
      tU32 u32AlertID = rUsrCtxt.u32CmdCtr;

      ETG_TRACE_USR2((" vOnMSDevProjAlertBtnEvent: Received e8ButtonAction = %d (for u32AlertID = %d) ",
            ETG_ENUM(DEVPRJ_BTN_ACTION, oXFiAlertBtnEvent.e8ButtonAction.enType),
            u32AlertID));

      if (OSAL_NULL != m_poAlertMngrClient)
      {
         //Retrieve notification info stored in AlertManager
         trNotiData rNotiData;
         m_poAlertMngrClient->vGetNotificationData(u32AlertID, rNotiData);

         //Send notification action to SPI
         if (OSAL_NULL != m_poSpiCmdInterface)
         {
            t_U32 u32SelDevHandle = m_poSpiCmdInterface->u32GetSelectedDeviceHandle();
            ETG_TRACE_USR4((" vOnMSDevProjAlertBtnEvent: Retrieved SelectedDevHandle = 0x%x ", u32SelDevHandle));

            if (cou32InvalidHandle != u32SelDevHandle)
            {
               t_U32 u32NotiActionID = u32GetNotificationActionID(oXFiAlertBtnEvent.e8ButtonAction, rNotiData);
               //@Note: u32NotiActionID is not evaluated here, because it is ok if u32NotiActionID = 0.
               //A zero value would clear the notification event, which is as per requirement.

               m_poSpiCmdInterface->vInvokeNotificationAction(u32SelDevHandle,
                     rNotiData.u32NotiAppID, rNotiData.u32NotiID, u32NotiActionID, rUsrCtxt);
            }
            else
            {
               ETG_TRACE_ERR((" vOnMSDevProjAlertBtnEvent: No device active!!! "
                     "Hence notification action is not processed. "));
            }
         }//if (OSAL_NULL != m_poSpiCmdInterface)
         else
         {
            ETG_TRACE_ERR((" vOnMSDevProjAlertBtnEvent: Received invalid AlertID! "
                  "Hence notification action is not processed. "));
         }
      }//if (OSAL_NULL != m_poAlertMngrClient)
   }
   else
   {
      ETG_TRACE_ERR((" vOnMSDevProjAlertBtnEvent: Invalid message received. "));
   }

}//! end of vOnMSDevProjAlertBtnEvent()

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnMSRequestAccDispCntxt(amt_tclS...
***************************************************************************/
tVoid devprj_tclService::
      vOnMSRequestAccDispCntxt(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("vdevprj_tclService::vOnMSRequestAccDispCntxt entered "));

   //! Create an XFiObjHandler object of DevPrj RequestAccessoryDisplayContext MS type to extract data from poMessage
   devprj_tXFiMSReqAccDispCtxt oXFiAccDispCtxt(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);

   if (true == oXFiAccDispCtxt.bIsValid())
   {
      ETG_TRACE_USR4((" vOnMSRequestAccDispCntxt: "
            "Received StartStopFlag = %u, RequestedDisplayContext = %u ",
            ETG_ENUM(BOOL, oXFiAccDispCtxt.bStartStopFlag),
            ETG_ENUM(DISPLAY_CONTEXT, oXFiAccDispCtxt.e8RequestedDisplayContext.enType)));
      
      SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);
      
      //! Call corresponding RequestAccessoryDisplayContext method in SpiCmdInterface.
      if (OSAL_NULL != m_poSpiCmdInterface)
      {
         //! Extract the DevPrj user context details
         trUserContext rUsrCtxt;
         CPY_TO_USRCNTXT(poMessage, rUsrCtxt);

         t_U32 u32DevHandle = m_poSpiCmdInterface->u32GetSelectedDeviceHandle();
         ETG_TRACE_USR4((" vOnMSRequestAccDispCntxt: Retrieved SelectedDevHandle = 0x%x ", u32DevHandle));

         //! Get current active device's category
         tenDeviceCategory enDevCat = m_poSpiCmdInterface->enGetDeviceCategory(u32DevHandle);

         switch (enDevCat)
         {
            case e8DEV_TYPE_MIRRORLINK:
            {
               //send message to Video manager
               tenBlockingMode enBlockingMode = (true == oXFiAccDispCtxt.bStartStopFlag) ?
                     (e8ENABLE_BLOCKING) : (e8DISABLE_BLOCKING);
               m_poSpiCmdInterface->vSetVideoBlockingMode(u32DevHandle, enBlockingMode, rUsrCtxt);
            }
               break;
            case e8DEV_TYPE_DIPO:
            case e8DEV_TYPE_ANDROIDAUTO:
            {
               tenDisplayContext enDispCtxt =
                  static_cast<tenDisplayContext>(oXFiAccDispCtxt.e8RequestedDisplayContext.enType);
               m_poSpiCmdInterface->vSetAccessoryDisplayContext(u32DevHandle,
                     oXFiAccDispCtxt.bStartStopFlag, enDispCtxt, rUsrCtxt);

               // Start the timer when HMI releases the Display resource which was borrowed/taken.
               // Supervise the message from Carplay Device to take the Display for a time window
               m_oDispProjLock.s16Lock();
               if((OSAL_NULL != m_poMainAppl) && (false == oXFiAccDispCtxt.bStartStopFlag)
                        && (false == m_oStDispProjRequest.bStartStopFlag))
               {
                  ETG_TRACE_USR4((" Starting blackscreen supervision timer \n"));
                  sbDipoProjTimerRun = m_poMainAppl->bStartTimer(DIPO_BLACKSCREEN_RECOVERY_TIMER_ID,
                           DIPO_BLACKSCREEN_RECOVERY_TIMER_FIRST_TICK, DIPO_BLACKSCREEN_RECOVERY_TIMER_TIMER_INTERVAL);
               }
               m_oDispProjLock.vUnlock();

            }
               break;
            default:
            {
               tenDisplayContext enDispCtxt =
                   static_cast<tenDisplayContext>(oXFiAccDispCtxt.e8RequestedDisplayContext.enType);
               m_poSpiCmdInterface->vSetAccessoryDisplayContext(u32DevHandle,
                      oXFiAccDispCtxt.bStartStopFlag, enDispCtxt, rUsrCtxt);
            }
            break;
         } //switch(enDevCat)
      } //if (OSAL_NULL != m_poSpiCmdInterface)
   } //if (true == oXFiAccDispCtxt.bIsValid())
   else
   {
      ETG_TRACE_ERR((" vOnMSRequestAccDispCntxt: Invalid message received. "));
   }
}

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnMSDiPoSwitchRequired(amt_tclS...
***************************************************************************/
tVoid devprj_tclService::vOnMSDiPoSwitchRequired(amt_tclServiceData* poMessage) const
{
   ETG_TRACE_USR1(("devprj_tclService::vOnMSDiPoSwitchRequired entered "));

   //! Create an XFiObjHandler object of DevPrj DiPoRoleSwitchRequiredt MS type to extract data from poMessage
   devprj_tXFiMSRoleSwitchReq oXFiRoleSwitchReq(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);

   if (true == oXFiRoleSwitchReq.bIsValid())
   {
      ETG_TRACE_USR4((" vOnMSDiPoSwitchRequired: Device Handle = 0x%x ", oXFiRoleSwitchReq.u8DeviceTag));
      SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);

      //! Extract the DevPrj user context details
      trUserContext rUsrCtxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCtxt);

      if ((true == m_bIsDiPOSupported) && (OSAL_NULL != m_poSpiCmdInterface))
      {
         //! Call corresponding DiPoRoleSwitchRequiredt method in SpiCmdInterface.
         t_U32 u32DeviceHandle = oXFiRoleSwitchReq.u8DeviceTag;
         m_poSpiCmdInterface->bIsDiPoRoleSwitchRequired(u32DeviceHandle,rUsrCtxt);
      }
   }
   else
   {
      ETG_TRACE_ERR((" vOnMSDiPoSwitchRequired: Invalid message received. "));
   }
}

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnMSInvokeBTDeviceAction(amt_tclS...
***************************************************************************/
tVoid devprj_tclService::vOnMSInvokeBTDeviceAction(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("devprj_tclService::vOnMSInvokeBTDeviceAction entered "));

   //! Create an XFiObjHandler object of DevPrj InvokeBluetoothDeviceAction MS
   //! type to extract data from poMessage
   devprj_tXFiMSInvokeBTDevAction oXFiInvokeBTDevAction(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);

   if (true == oXFiInvokeBTDevAction.bIsValid())
   {
      ETG_TRACE_USR4((" vOnMSInvokeBTDeviceAction: Received BluetoothDeviceHandle = 0x%x ",
            oXFiInvokeBTDevAction.u8BTDeviceHandle));
      ETG_TRACE_USR4((" vOnMSInvokeBTDeviceAction: Received ProjectionDeviceHandle = 0x%x ",
            oXFiInvokeBTDevAction.u32DeviceHandle));
      ETG_TRACE_USR4((" vOnMSInvokeBTDeviceAction: Received BTChangeInfo = %u ",
            ETG_ENUM(BT_CHANGE_INFO, oXFiInvokeBTDevAction.e8BTChangeInfo.enType)));

      SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);

      tBool bIsInputDataValid = (
            (cou32InvalidHandle != oXFiInvokeBTDevAction.u8BTDeviceHandle) &&
            (cou32InvalidHandle != oXFiInvokeBTDevAction.u32DeviceHandle));

      //! Call corresponding InvokeBluetoothDeviceAction method in SpiCmdInterface.
      if ((TRUE == bIsInputDataValid) && (OSAL_NULL != m_poSpiCmdInterface))
      {
         m_poSpiCmdInterface->vInvokeBluetoothDeviceAction(
               oXFiInvokeBTDevAction.u8BTDeviceHandle,
               oXFiInvokeBTDevAction.u32DeviceHandle,
               static_cast<tenBTChangeInfo>(oXFiInvokeBTDevAction.e8BTChangeInfo.enType));
      }
      //! Send InvokeBluetoothDeviceAction error
      else if (FALSE == bIsInputDataValid)
      {
         vGenerateError(poMessage);
      }
   }
   else
   {
      ETG_TRACE_ERR((" vOnMSInvokeBTDeviceAction: Invalid message received. "));
   }
}//! end of vOnMSInvokeBTDeviceAction()

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnMSStopMediaPlayback(amt_tclS...
***************************************************************************/
tVoid devprj_tclService::vOnMSStopMediaPlayback(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("devprj_tclService::vOnMSStopMediaPlayback() entered "));

   //! Create an XFiObjHandler object of DevPrj StopMediaPlayback MS
   //! type to extract data from poMessage
   devprj_tXFiMSStopMediaPb oXFiStopMediaPb(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);

   SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);
   if ((true == oXFiStopMediaPb.bIsValid()) && (OSAL_NULL != m_poSpiCmdInterface))
   {
      t_U32 u32SelDevHandle = m_poSpiCmdInterface->u32GetSelectedDeviceHandle();
      ETG_TRACE_USR1((" vOnMSStopMediaPlayback: Received DeviceTag = 0x%x, Stop reason = %d",
            oXFiStopMediaPb.u8DeviceTag, ETG_ENUM(MEDIA_PB_STOP_REASON, oXFiStopMediaPb.e8PlaybackStopReason.enType)));
      SPI_NORMAL_ASSERT(u32SelDevHandle != (oXFiStopMediaPb.u8DeviceTag));

      //! Extract the DevPrj user context details
      CPY_TO_USRCNTXT(poMessage, rStopMediaPlaybackCtxt);

      m_oCarPlayMediaStatusLock.s16Lock();
      t_Bool bIsCarPlayMediaActive = m_bCarPlayMediaActive;
      m_oCarPlayMediaStatusLock.vUnlock();

      //! Clear Audio device name
      m_oCarPlayMediaAudDeviceLock.s16Lock();
      m_szCarPlayMediaAudDevice.clear();
      m_oCarPlayMediaAudDeviceLock.vUnlock();

      t_Bool bSendResponse = true;

      if (
         (true == bIsCarPlayMediaActive)
         &&
         (u32SelDevHandle == (oXFiStopMediaPb.u8DeviceTag))
         &&
         (e8DEV_TYPE_DIPO == m_poSpiCmdInterface->enGetDeviceCategory(u32SelDevHandle))
         &&
         (most_fi_tcl_e8_PlaybackStopReason::FI_EN_E8REASON_MEDIADEVICESWITCH ==
               oXFiStopMediaPb.e8PlaybackStopReason.enType)
         )
      {
         bSendResponse = false;

         //! Take the audio resource since user has switched out of CarPlay media.
         //@Note: StopMediaPlayback result will be sent once teardown occurs.
         m_poSpiCmdInterface->vSetAccessoryAudioContext(u32SelDevHandle,
               e8SPI_AUDIO_MAIN, true, corEmptyUsrContext,e8DEV_TYPE_DIPO);

         //! Mock Source Activity OFF and DeAllocate result
         m_poSpiCmdInterface->vOnStopSourceActivity(scu8AudMainSrcNum);
         m_poSpiCmdInterface->vOnRouteDeAllocateResult(scu8AudMainSrcNum);
         //@TODO - Source Number hard-coded for CarPlay. To be revised.
      }

      if (true == bSendResponse)
      {
         devprj_tMRStopMediaPb oStopMediaPbMR;
         //@Note: Fix for GMMY17-11123 - provide same DeviceHandle as in request, since it is possible
         //to receive the request when CarPlay session is active, but device selection is not complete.
         oStopMediaPbMR.u8DeviceTag = oXFiStopMediaPb.u8DeviceTag;
         if (FALSE == bPostResponse(oStopMediaPbMR, rStopMediaPlaybackCtxt))
         {
            ETG_TRACE_ERR((" vOnMSStopMediaPlayback: StopMediaPlayback MR post failed! "));
         }
         //Clear context
         rStopMediaPlaybackCtxt = corEmptyUsrContext;
      }//if (true == bSendResponse)

   }//if ((true == oXFiStopMediaPb.bIsValid()) &&...)
   else if (false == oXFiStopMediaPb.bIsValid())
   {
      ETG_TRACE_ERR((" vOnMSStopMediaPlayback: Invalid message received. "));
   }
}

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnMSResumeMediaPlayback(amt_tclS...
***************************************************************************/
tVoid devprj_tclService::vOnMSResumeMediaPlayback(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("devprj_tclService::vOnMSResumeMediaPlayback() entered "));

   //! Create an XFiObjHandler object of DevPrj StopMediaPlayback MS
   //! type to extract data from poMessage
   devprj_tXFiMSResumeMediaPb oXFiResumeMediaPb(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);

   SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);
   if ((true == oXFiResumeMediaPb.bIsValid()) && (OSAL_NULL != m_poSpiCmdInterface))
   {
      //! Extract the DevPrj user context details
      trUserContext rUsrCtxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCtxt);

      m_oCarPlayMediaStatusLock.s16Lock();
      t_Bool bIsCarPlayMediaActive = m_bCarPlayMediaActive;
      m_oCarPlayMediaStatusLock.vUnlock();

      t_U32 u32SelDevHandle = m_poSpiCmdInterface->u32GetSelectedDeviceHandle();
      ETG_TRACE_USR1((" vOnMSResumeMediaPlayback: Received DeviceTag = 0x%x, IsCarPlayMediaActive = %d ",
            oXFiResumeMediaPb.u8DeviceTag, ETG_ENUM(BOOL, bIsCarPlayMediaActive)));
      SPI_NORMAL_ASSERT(u32SelDevHandle != (oXFiResumeMediaPb.u8DeviceTag));

      //! If media setup is not yet received for CarPlay media, unborrow the audio resource
      if (
         (false == bIsCarPlayMediaActive)
         &&
         (u32SelDevHandle == (oXFiResumeMediaPb.u8DeviceTag))
         &&
         (e8DEV_TYPE_DIPO == m_poSpiCmdInterface->enGetDeviceCategory(u32SelDevHandle))
         )
      {

		 m_oCarPlayMediaStatusLock.s16Lock();
     	 while(!m_enAudioCntxtUpdateStack.empty())
         {     		
            m_poSpiCmdInterface->vSetAccessoryAudioContext(u32SelDevHandle, m_enAudioCntxtUpdateStack.top(), false, corEmptyUsrContext,e8DEV_TYPE_DIPO);
            m_enAudioCntxtUpdateStack.pop();          
          }
		  m_oCarPlayMediaStatusLock.vUnlock();

         //! Set the flag to avoid doing a take if BaseChannelStatusUpdate is updated before
         //! phone sends media setup
         m_oCarPlayMediaStatusLock.s16Lock();
         m_bCarPlayMediaActive = true;
         m_oCarPlayMediaStatusLock.vUnlock();
      }
      else
      {
         ETG_TRACE_ERR((" vOnMSResumeMediaPlayback: No action taken since CarPlay is inactive/Invalid handle received! "));
      }

      //! Acknowledge ResumeMediaPlayback request
      devprj_tMRResumeMediaPb oResumeMediaPbMR;
      //@Note: Provide same DeviceHandle as in request, since it is possible
      //to receive the request when CarPlay session is active, but device selection is not complete.
      oResumeMediaPbMR.u8DeviceTag = oXFiResumeMediaPb.u8DeviceTag;
      if (FALSE == bPostResponse(oResumeMediaPbMR, rUsrCtxt))
      {
         ETG_TRACE_ERR((" vOnMSResumeMediaPlayback: ResumeMediaPlayback MR post failed! "));
      }
   }//if ((true == oXFiResumeMediaPb.bIsValid()) &&...)
   else if (false == oXFiResumeMediaPb.bIsValid())
   {
      ETG_TRACE_ERR((" vOnMSResumeMediaPlayback: Invalid message received. "));
   }
}

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnLoopbackMessage(...)
***************************************************************************/
tVoid devprj_tclService::vOnLoopbackMessage(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("devprj_tclService::vOnLoopbackMessage entered "));

   if ((OSAL_NULL != poMessage) && (OSAL_NULL != m_poSpiCmdInterface))
   {
      tU16 u16LoopbackFuncID = poMessage->u16GetFunctionID();
      ETG_TRACE_USR2((" vOnLoopbackMessage: Internal Function ID - %d", ETG_ENUM( LOOPBACK_MESSAGE, u16LoopbackFuncID)));

      //! Extract loopback message and forward info.
      switch (u16LoopbackFuncID)
      {
         case SPI_C_U16_IFID_SYSSTATE_DAYNIGHTMODE:
         {
            tLbSysStaDayNightMode oDayNightModeLbMsg(poMessage);
            m_enSysStateDayNightMode =
                  static_cast<tenSystemStateDayNightMode>(oDayNightModeLbMsg.u8GetByte());
            vUpdateDayNightMode();
         }
            break;
         case SPI_C_U16_IFID_CNTRSTCKHMI_DAYNIGHTMODE:
         {
            tLbCntrStckHmiDayNightMode oDayNightModeLbMsg(poMessage);
            m_enCntrStackHmiDayNightMode =
                  static_cast<tenCenterStackHmiDayNightOverride>(oDayNightModeLbMsg.u8GetByte());
            vUpdateDayNightMode();
         }
            break;
         case SPI_C_U16_IFID_ONSTAR_C2_CALLSTATE:
         {
            //! Update Phone app state
            tLbOnstarCallState oCallStateLbMsg(poMessage);
            m_enPhoneAppState = (e8ONSCALL_IDLE == static_cast<tenOnstarCallState>(oCallStateLbMsg.u8GetByte())) ?
                  (e8SPI_PHONE_NOT_ACTIVE) : (e8SPI_PHONE_ACTIVE);

            //! Sending the speech app state also along with the phone state to indicate that the accessory is 
            //  doing speech related operation
            m_enSpeechAppState = (e8SPI_PHONE_ACTIVE == m_enPhoneAppState) ? e8SPI_SPEECH_SPEAKING :
               e8SPI_SPEECH_END;
            m_poSpiCmdInterface->vSetAccessoryAppState(
                  m_enSpeechAppState, m_enPhoneAppState, m_enNavAppState, corEmptyUsrContext);
         }
            break;
         case SPI_C_U16_IFID_ONSTAR_C1_CALLSTATE:
            {
               //! Update Phone app state
               tLbOnStarC1CallState oC1CallStateLbMsg(poMessage);
               m_enPhoneAppState = (e8ONSCALL_IDLE == static_cast<tenOnstarCallState>(oC1CallStateLbMsg.u8GetByte())) ?
                  (e8SPI_PHONE_NOT_ACTIVE) : (e8SPI_PHONE_ACTIVE);

               //! Sending the speech app state also along with the phone state to indicate that the accessory is 
               //  doing speech related operation
               m_enSpeechAppState = (e8SPI_PHONE_ACTIVE == m_enPhoneAppState) ? e8SPI_SPEECH_SPEAKING :
                  e8SPI_SPEECH_END;
               m_poSpiCmdInterface->vSetAccessoryAppState(
                  m_enSpeechAppState, m_enPhoneAppState, m_enNavAppState, corEmptyUsrContext);
            }
            break;
         case SPI_C_U16_IFID_ONSTAR_EMERGENCY_CALLSTATE:
            {
               //! Update Phone app state
               tLbOnStarEmergencyCallState oEmerCallStateLbMsg(poMessage);
               m_enPhoneAppState = (e8ONSCALL_IDLE == static_cast<tenOnstarCallState>(oEmerCallStateLbMsg.u8GetByte())) ?
                  (e8SPI_PHONE_NOT_ACTIVE) : (e8SPI_PHONE_ACTIVE);

               //! Sending the speech app state also along with the phone state to indicate that the accessory is 
               //  doing speech related operation
               m_bNativeSpeechActive = (e8SPI_PHONE_ACTIVE == m_enPhoneAppState);
				  
			   m_enSpeechAppState = (m_bOnstarSpeechState || m_bNativeSpeechActive) ? e8SPI_SPEECH_SPEAKING : e8SPI_SPEECH_END;
               m_poSpiCmdInterface->vSetAccessoryAppState(
                  m_enSpeechAppState, m_enPhoneAppState, m_enNavAppState, corEmptyUsrContext);
            }
            break;
         /* @Note: Currently these IDs are not processed. Code retained for future use
         case SPI_C_U16_IFID_ONSTAR_ACCEPTCALLERROR:
         {
         }
            break;
         case SPI_C_U16_IFID_ONSTAR_HANGUPERROR:
         {
         }*/
            //break;
         case SPI_C_U16_IFID_ONSTAR_TBTACTIVESTATE:
         {
            //! Update Navigation app state
            tLbOnstarTBTActiveState oTBTActiveLbMsg(poMessage);
            m_bOnStarNavStatus = (TRUE == static_cast<tBool>(oTBTActiveLbMsg.u8GetByte())) ?
                  true : false;
            m_enNavAppState = (m_bOnStarNavStatus || m_bNavStatus) ?
                  (e8SPI_NAV_ACTIVE) : (e8SPI_NAV_NOT_ACTIVE);
            m_poSpiCmdInterface->vSetAccessoryAppState(
                  m_enSpeechAppState, m_enPhoneAppState, m_enNavAppState, corEmptyUsrContext);
         }
            break;
         case SPI_C_U16_IFID_SPEECHREC_BTNEVENTRESULT:
         {
            tLbSpeechRecBtnEventResult oSRBtnEventResLbMsg(poMessage);
            m_enSpeechRecSessionStatus = static_cast<tenSpeechRecBtnEventResult>(oSRBtnEventResLbMsg.u8GetByte());
         }
            break;
         case SPI_C_U16_IFID_SPEECHREC_SRSTATUS:
         {
            //! Update Speech app state
            tLbSpeechRecStatus oSRStatusLbMsg(poMessage);
            vOnSRStatus(static_cast<tenSpeechRecStatus>(oSRStatusLbMsg.u8GetByte()));
         }
            break;
         case SPI_C_U16_IFID_NAV_GUIDANCESTATUS:
         {
            //! Update Navigation app state
            tLbNavGuidanceStatus oGuidStatusLbMsg(poMessage);
            m_bNavStatus = (e8GUIDANCE_ACTIVE == static_cast<tenGuidanceStatus>(oGuidStatusLbMsg.u8GetByte())) ?
               true : false;
            m_enNavAppState = (m_bNavStatus || m_bOnStarNavStatus) ?
                  (e8SPI_NAV_ACTIVE) : (e8SPI_NAV_NOT_ACTIVE);
            m_poSpiCmdInterface->vSetAccessoryAppState(
                  m_enSpeechAppState, m_enPhoneAppState, m_enNavAppState, corEmptyUsrContext);
         }
            break;
         case SPI_C_U16_IFID_GMLAN_GPSCONFIG:
            {
               //! Store GPS configuration
               tLbGMLANGPSConfig oGPSConfigLbMsg(poMessage);
               m_enGPSConfig = static_cast<tenGPSConfig>(oGPSConfigLbMsg.u8GetByte());
               t_Bool bLocDataAvailable = ((e8VARIANT_NAVIGATION == m_enSystemVariant) ||
                     (e8GMLAN_GPS_ONSTAR == m_enGPSConfig));
               t_Bool bIsDRLocData = (e8VARIANT_NAVIGATION == m_enSystemVariant) ? (false):(e8GMLAN_GPS_ONSTAR == m_enGPSConfig);
               m_poSpiCmdInterface->vSetLocDataAvailablility(bLocDataAvailable, bIsDRLocData);
            }
            break;
         case SPI_C_U16_IFID_SYSSTATE_TIMEOFDAY:
            {
               //Get the Vehicle data from GMLANGateway client
               if (OSAL_NULL!= m_poGMLANClient)
               {
                  m_poGMLANClient->vRequestVehicleDataUpdate();
               }
            }
            break;
         case SPI_C_U16_IFID_MPLAY_REQAUDIODEV_RESULT:
            {
               // Extract AudioDevice name & process it
               tLbMplayAudioDevice oAudioDevLbMsg(poMessage);
               trUserContext rUsrCtxt;
               CPY_TO_USRCNTXT(poMessage, rUsrCtxt);
               t_String szAudioDevName = (OSAL_NULL != oAudioDevLbMsg.pcocGetData())? (oAudioDevLbMsg.pcocGetData()) : ("");
               vOnCarPlayAudioResult(szAudioDevName, static_cast<tenAudioError>(rUsrCtxt.u32CmdCtr));
            }
            break;
         case SPI_C_U16_IFID_ONSTAR_SETTINGS_UPDATE:
            {
               tLbOnStarDataSettingsUpdate oDataSettingsLbMsg(poMessage);
               tenOnstarSettingsEvents enDataSetEvent =
                  static_cast<tenOnstarSettingsEvents>(oDataSettingsLbMsg.u8GetByte());
               vOnOnstarDataSettingsUpdate(enDataSetEvent);
            }
            break;
         case SPI_C_U16_IFID_DATA_SERVICE_SUBSCRIBE:
         	{
         		tLbDataServiceSubscribeMsg oDataSrvMsg(poMessage);
         		t_U16 u16SubscribeInfo = oDataSrvMsg.u16GetWord();
         		//! taking the first bits (9-16) as a boolean value.
         		t_Bool bStatus = (t_Bool)(u16SubscribeInfo & 0x0100);
         		//! taking the second byte (1-8) as a status value
         		tenLocationDataType enLocationDataType = static_cast<tenLocationDataType>(u16SubscribeInfo & 0x00FF);
         		ETG_TRACE_USR1((" SPI_C_U16_IFID_DATA_SERVICE_SUBSCRIBE bStatus : %d  and DataValue : %d", bStatus,
         				ETG_ENUM(LOCATION_DATA_TYPE, enLocationDataType)));
										
   				(true == bStatus) ? (vRegisterLocData(enLocationDataType)) : (vUnregisterLocData(enLocationDataType));
         	}
         	break;
         case SPI_C_U16_IFID_VEHICLE_BTADDRESS_UPDATE:
         	{
         		tLbOnVehicleBTAdressUpdate oVehicleBTAddMsg(poMessage);
         		t_String szBtAddress = (OSAL_NULL != oVehicleBTAddMsg.pcocGetData())? (oVehicleBTAddMsg.pcocGetData()) : ("");
         	    m_poSpiCmdInterface->vSetVehicleBTAddress(szBtAddress);
         	}
         	break;
         case SPI_C_U16_IFID_VINDIGITS10TO17:
            {
              tLbVINDigits10To17 oVINDigits10To17LbMsg(poMessage);
              t_U8 u8ModelYear =oVINDigits10To17LbMsg.u8GetByte();
               spi_tclConfigReader* poConfigReader = spi_tclConfigReader::getInstance();
               if (OSAL_NULL != poConfigReader)
               {
                   poConfigReader->vSetVehicleModelYearInfo(u8ModelYear);
               }
            }
            break;
         case SPI_C_U16_IFID_SYSSTATE_VALETMODE:
            {
               tLbSysStaValetModeEnabled oValetModeLbMsg(poMessage);
               t_Bool bIsValetModeActive = static_cast<t_Bool>(oValetModeLbMsg.u8GetByte());
               vOnValetModeChange(bIsValetModeActive);
            }
            break;
         default:
            ETG_TRACE_ERR((" vOnLoopbackMessage: Invalid Function ID : 0x%x ", u16LoopbackFuncID));
            break;
      }//switch (u16LoopbackFuncID)
   }//if ((OSAL_NULL != poMessage) && (OSAL_NULL != m_poSpiCmdInterface))
}//! end of vOnLoopbackMessage()

/***************************************************************************
* ! Internal methods
***************************************************************************/

/***************************************************************************
** FUNCTION:  tBool devprj_tclService::bPostResponse(const devprj_Fi...
**************************************************************************/
tBool devprj_tclService::bPostResponse(const devprj_FiMsgBase& rfcooDevPrjMsg,
      const trUserContext& rfcorUsrCtxt) const
{
   //! Create Msg context using User context
   trMsgContext rMsgCtxt;
   rMsgCtxt.rUserContext = rfcorUsrCtxt;

   //! Post DevPrj MethodResult
   FIMsgDispatch oMsgDispatcher(m_poMainAppl);
   tBool bSuccess = oMsgDispatcher.bSendResMessage(rfcooDevPrjMsg, rMsgCtxt,
         DEVPROJ_SERVICE_FI_MAJOR_VERSION);

   ETG_TRACE_USR2(("devprj_tclService::bPostResponse() left with success: %u (for FID = 0x%x, Opcode = %d) ",
         ETG_ENUM(BOOL, bSuccess), rfcorUsrCtxt.u32FuncID, rfcooDevPrjMsg.u8GetOpCode()));

   return bSuccess;

}//! end of bPostResponse()

/***************************************************************************
** FUNCTION:  tBool devprj_tclService::bHandoverStatusMsg(const devprj_Fi...
**************************************************************************/
tBool devprj_tclService::bHandoverStatusMsg(
      const devprj_FiMsgBase& rfcooDevPrjStatusMsg, 
      amt_tclServiceData& roOutMsg) const
{
   //! Handover the DeviceProjection Status message to Framework output message
   fi_tclVisitorMessage oVisitorMsg(rfcooDevPrjStatusMsg, DEVPROJ_SERVICE_FI_MAJOR_VERSION);
   return (oVisitorMsg.bHandOver(&roOutMsg));

}//! end of bHandoverStatusMsg()

/***************************************************************************
** FUNCTION: tBool devprj_tclService::bHandoverMsgDevConnListStatus(amt_t...
***************************************************************************/
tBool devprj_tclService::
      bHandoverMsgDevConnListStatus(amt_tclServiceData& rfoOutMsg) const
{
   tBool bSuccess = FALSE;

   SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);

   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      //! Get DeviceInfoList from Spi CmdInterface and
      //! create DevPrj DeviceConnectionList Status msg.
      devprj_tStDevConnList oStDevList;

      t_U32 u32NumDevices = 0;
      std::vector<trDeviceInfo> vecrDeviceInfoList;
      
      m_poSpiCmdInterface->vGetDeviceInfoList(u32NumDevices, vecrDeviceInfoList);

      ETG_TRACE_USR4(("devprj_tclService::bHandoverMsgDevConnListStatus: Retrieved NumDevices = %u ", u32NumDevices));

      //! Copy SPI DeviceInfoList details into DevPrj DeviceConnectionList
      oStDevList.u16NumConnectedDevices = static_cast<tU16>(u32NumDevices);

      if (0 != u32NumDevices)
      {
         vCopyDeviceInfoList(vecrDeviceInfoList, oStDevList);
      } //if (0 != oStDevList.u16NumConnectedDevices)

      bSuccess = bHandoverStatusMsg(oStDevList, rfoOutMsg);
      
      //! Deallocate memory used by DeviceList msg.
      oStDevList.vDestroy();

   } //if (OSAL_NULL != m_poSpiCmdInterface)

   return bSuccess;

}//! end of bHandoverMsgDevConnListStatus()

/***************************************************************************
** FUNCTION: tVoid devprj_tclService::vCopyDeviceInfoList(std::vector...
***************************************************************************/
tVoid devprj_tclService::vCopyDeviceInfoList(const std::vector<trDeviceInfo>& corfvecrDeviceInfoList, devprj_tStDevConnList& rfoStDevList) const
{
	ETG_TRACE_USR1(("devprj_tclService::vCopyDeviceInfoList entered"));
	typedef std::vector<trDeviceInfo>::const_iterator SpiDevInfoVecIter;

	for (SpiDevInfoVecIter SpiLstIter = corfvecrDeviceInfoList.begin();
	SpiLstIter != corfvecrDeviceInfoList.end(); ++SpiLstIter)
         {
            //!Get device details
            devprj_tFiDevLstItem oDevLstItem;
            trDeviceInfo rDevInfo = (*SpiLstIter);

            oDevLstItem.u32DeviceHandle = rDevInfo.u32DeviceHandle;
            oDevLstItem.e8DeviceType.enType =
               static_cast<devprj_tenDevType>(rDevInfo.enDeviceCategory);
            oDevLstItem.sDeviceName.bSet(rDevInfo.szDeviceName.c_str());

            //! This logic is used to synchronize the device selection
            // and CarPlay session creation to update the session active status to 
            // HMI. With this the device active status will update HMI only after the 
            // session startup at CarPlay plugin side is completed.
            if(((e8DEV_TYPE_DIPO == rDevInfo.enDeviceCategory) || (e8DEV_TYPE_ANDROIDAUTO == rDevInfo.enDeviceCategory)) &&
               (e8_SESSION_ACTIVE != m_enDeviceSessionStatus))
            {
               oDevLstItem.bActiveDevice = false;
            }
            else
            {
               oDevLstItem.bActiveDevice = rDevInfo.bSelectedDevice;
            }
            oDevLstItem.bDeviceProjectionEnabled = rDevInfo.bDeviceUsageEnabled;

		//Set the Device Version in oDevLstItem
		vSetDeviceVersion(rDevInfo, oDevLstItem);
            
            //Get Device Connection Type 
            switch (rDevInfo.enDeviceConnectionType)
            {
               case e8USB_CONNECTED:
                  oDevLstItem.e8DeviceInterfaceType.enType =
                     devprj_tFiDevIntfType::FI_EN_E8DEV_IFACE_WIRED_USB;
                  break;

               case e8WIFI_CONNECTED:
                  oDevLstItem.e8DeviceInterfaceType.enType =
                     devprj_tFiDevIntfType::FI_EN_E8DEV_IFACE_WIFI;
                  break;
               default:
                  ETG_TRACE_ERR((" vCopyDeviceInfoList: Invalid enum value encountered : %u",
                        (tU8)(rDevInfo.enDeviceConnectionType)));
                  break;
            }

            //! Add DeviceListItem in DeviceList
        rfoStDevList.oDeviceList.oItems.push_back(oDevLstItem);

         }//for (std::vector<trDeviceInfo>::iterator...)
}//! end of vCopyDeviceInfoList()

/***************************************************************************
** FUNCTION: tBool devprj_tclService::vSetDeviceVersion(const trDeviceInf...
***************************************************************************/
tVoid devprj_tclService::vSetDeviceVersion(const trDeviceInfo& corfrDevInfo, devprj_tFiDevLstItem& rfoDevLstItem) const
{
	ETG_TRACE_USR1(("devprj_tclService::vSetDeviceVersion entered"));
	//Get the Device Version from corfrDevInfo
	trVersionInfo rVersionInfo = corfrDevInfo.rVersionInfo;

	//Convert the device version to string format
	std::string szVersion;
	StringHandler oStrHdler;
	oStrHdler.vConvertIntToStr(rVersionInfo.u32MajorVersion, szVersion, DECIMAL_STRING);
	std::string szDevVer(szVersion);
	oStrHdler.vConvertIntToStr(rVersionInfo.u32MinorVersion, szVersion, DECIMAL_STRING);
	szDevVer.append("." + szVersion);
	oStrHdler.vConvertIntToStr(rVersionInfo.u32PatchVersion, szVersion, DECIMAL_STRING);
	szDevVer.append("." + szVersion);
	rfoDevLstItem.sDeviceVersion.bSet(szDevVer.c_str());
}//! end of vSetDeviceVersion()

/***************************************************************************
** FUNCTION: tBool devprj_tclService::bHandoverMsgAppListStatus(amt_tclSe...
***************************************************************************/
tBool devprj_tclService::bHandoverMsgAppListStatus(amt_tclServiceData& rfoOutMsg)
{
   tBool bSuccess = FALSE;

   SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);

   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      //! Get Selected Device's Handle and AppInfoList from Spi CmdInterface 
      //! and create DevPrj ApplicationList Status msg.
      devprj_tStAppList oStAppList;

      oStAppList.u32DeviceHandle =
         static_cast<tU32>(m_poSpiCmdInterface->u32GetSelectedDeviceHandle());
      
      ETG_TRACE_USR4(("devprj_tclService::bHandoverMsgAppListStatus: Retrieved SelectedDevHandle = 0x%x ",
	     oStAppList.u32DeviceHandle));

      //! Check for DeviceHandle validity   
      if (cou32InvalidHandle != oStAppList.u32DeviceHandle)
      {
         t_U32 u32SpiNumApps = 0;
         std::vector<trAppDetails> vecrSpiAppDetailsList;
      
         m_poSpiCmdInterface->vGetAppList(oStAppList.u32DeviceHandle,
            u32SpiNumApps, vecrSpiAppDetailsList);

         ETG_TRACE_USR4(("devprj_tclService::bHandoverMsgAppListStatus: Retrieved NumApps = %u ", u32SpiNumApps));

         //! Copy SPI AppInfoList details into DevPrj AppList
         oStAppList.u16NumApps = static_cast<tU16>(u32SpiNumApps);

         if (0 != oStAppList.u16NumApps)
         {
            vGenerateAppList(oStAppList, vecrSpiAppDetailsList);
         } //if (0 != oStAppList.u16NumApps)

         bSuccess = bHandoverStatusMsg(oStAppList, rfoOutMsg);
         
         //! Deallocate memory used by AppList msg.
         oStAppList.vDestroy();

      }//if (cou32InvalidHandle != oStAppList.u32DeviceHandle)
      else
      {
         oStAppList.u16NumApps = 0;
         bSuccess = bHandoverStatusMsg(oStAppList, rfoOutMsg);
         //@Note: Not required to destroy AppList msg, since it is empty.
      }
   } //if (OSAL_NULL != m_poSpiCmdInterface)
   return bSuccess;

}//! end of bHandoverMsgAppListStatus()

/**************************************************************************
** FUNCTION:  tVoid devprj_tclService::vGenerateAppList(devprj_tStAppLis...
**************************************************************************/
tVoid devprj_tclService::vGenerateAppList(devprj_tStAppList& rfoStAppList,
	  const std::vector<trAppDetails>& corfvecrSpiAppDetailsList)
{
	ETG_TRACE_USR1(("devprj_tclService::vGenerateAppList entered"));
	typedef std::vector<trAppDetails>::const_iterator SpiAppDetVecIter;
         
    for (SpiAppDetVecIter SpiLstIter = corfvecrSpiAppDetailsList.begin();
        SpiLstIter != corfvecrSpiAppDetailsList.end(); ++SpiLstIter)
            {
               //Get App details
               devprj_tFiAppLstItem oAppLstItem;
               trAppDetails rAppDetail = (*SpiLstIter); 
               
               t_U32 u32AppHandle = rAppDetail.u32AppHandle;
               devprj_tenAppCertType enAppCertType = devprj_tFiAppCertType::FI_EN_E8PARK_ONLY;
               tenAppCertificationInfo enAppCertInfo = rAppDetail.enAppCertificationInfo;
               tenAppStatus enAppStatus = rAppDetail.enAppStatus;
            
               oAppLstItem.u32ApplicationHandle = u32AppHandle;
               oAppLstItem.sApplicationName.bSet(rAppDetail.szAppName.c_str());

               if (0 != rAppDetail.tvecAppIconList.size())
               {
                  oAppLstItem.sApplicationIconURL.bSet(
                     rAppDetail.tvecAppIconList[0].szIconURL.c_str());
               }

		//Set the App certificate type based on enAppCertInfo
               if((e8DRIVE_CERTIFIED_ONLY == enAppCertInfo) || (e8DRIVE_CERTIFIED == enAppCertInfo))
               {
                  enAppCertType = devprj_tFiAppCertType::FI_EN_E8NO_RESTRICTION;
               }
               oAppLstItem.e8ApplicationCertificateType.enType = enAppCertType;

        ETG_TRACE_USR4((" vGenerateAppList: "
                     "AppHandle = 0x%x, AppStatus = %d, AppCertificationInfo = %d, AppName = %s ",
                     u32AppHandle,
                     ETG_ENUM(APP_STATUS, enAppStatus),
                     ETG_ENUM(APP_CERTIFICATION_STATUS, enAppCertInfo),
                     rAppDetail.szAppName.c_str()));

               //! Add DevPrj AppListItem in AppList
        rfoStAppList.oApplicationList.oItems.push_back(oAppLstItem);

            }//for (std::vector<trAppDetails>::iterator...)
}//! end of vGenerateAppList()

/***************************************************************************
** FUNCTION: tBool devprj_tclService::bHandoverMsgDevNotiEnableInfoStatus(am...
***************************************************************************/
tBool devprj_tclService::
      bHandoverMsgDevNotiEnableInfoStatus(amt_tclServiceData& roOutMsg) const
{
   ETG_TRACE_USR4(("devprj_tclService::bHandoverMsgDevNotiEnableInfoStatus() entered "));

   tBool bSuccess = FALSE;

   SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);

   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      devprj_tStDevNotifEnabled oStNotifEnabled;
      oStNotifEnabled.bDeviceNotificationsEnabled = m_poSpiCmdInterface->bGetMLNotificationEnabledInfo();
      bSuccess = bHandoverStatusMsg(oStNotifEnabled, roOutMsg);
   } //if (OSAL_NULL != m_poSpiCmdInterface)

   return bSuccess;

}//! end of bHandoverMsgDevNotiEnableInfoStatus()

/***************************************************************************
** FUNCTION: tBool devprj_tclService::bHandoverMsgAppMetadataStatus(amt_t...
***************************************************************************/
tBool devprj_tclService::
      bHandoverMsgAppMetadataStatus(amt_tclServiceData& rfoOutMsg) const
{
   tBool bSuccess = FALSE;

   SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);

   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      //! Create DevPrj ApplicationMetaData Status msg.

      ETG_TRACE_USR4((" bHandoverMsgAppMetadataStatus: Artist = %s", m_rAppMediaMetaData.szArtist.c_str()));
      ETG_TRACE_USR4((" bHandoverMsgAppMetadataStatus: Title = %s", m_rAppMediaMetaData.szTitle.c_str()));
      ETG_TRACE_USR4((" bHandoverMsgAppMetadataStatus: Album =  %s", m_rAppMediaMetaData.szAlbum.c_str()));
      ETG_TRACE_USR4((" bHandoverMsgAppMetadataStatus: Genre = %s", m_rAppMediaMetaData.szGenre.c_str()));
      ETG_TRACE_USR4((" bHandoverMsgAppMetadataStatus: TotalPlayTime = %u , ElapsedPlayTime = %u",
					  m_rAppMediaPlaytime.u32TotalPlayTime, m_rAppMediaPlaytime.u32ElapsedPlayTime));
      ETG_TRACE_USR4((" bHandoverMsgAppMetadataStatus: ImageUrl =  %s ", m_rAppMediaMetaData.szImageUrl.c_str()));
      ETG_TRACE_USR4((" bHandoverMsgAppMetadataStatus: ImageSize =  %u ", m_rAppMediaMetaData.u32ImageSize));
      ETG_TRACE_USR4((" bHandoverMsgAppMetadataStatus: ImageMIMEType =  %s ", m_rAppMediaMetaData.szImageMIMEType.c_str()));
      ETG_TRACE_USR4((" bHandoverMsgAppMetadataStatus: AppName =  %s ", m_rAppMediaMetaData.szAppName.c_str()));


      devprj_tStAppMetaData oStMetaData;
      oStMetaData.bMetaDataValid = static_cast<tBool>(m_rAppMediaMetaData.bMediaMetadataValid);

	  //populate oStMetaData with AppMetatdata details
      vCopyAppMetadata(oStMetaData);
      ETG_TRACE_USR4((" bHandoverMsgAppMetadataStatus: MetadataValid = %d", ETG_ENUM(BOOL, oStMetaData.bMetaDataValid)));
	  
      bSuccess = bHandoverStatusMsg(oStMetaData, rfoOutMsg);
      
      //!Deallocate memory used by Metadata msg.
      oStMetaData.vDestroy();

   } //if (OSAL_NULL != m_poSpiCmdInterface)

   return bSuccess;

}//! end of bHandoverMsgAppMetadataStatus()

/**************************************************************************
** FUNCTION:  tVoid devprj_tclService::vCopyAppMetadata(devprj_tStAppMet...
**************************************************************************/
tVoid devprj_tclService::vCopyAppMetadata(devprj_tStAppMetaData& rfoStMetaData) const
{
   rfoStMetaData.u32TotalPlaytime    = m_rAppMediaPlaytime.u32TotalPlayTime;
   rfoStMetaData.u32ElapsedPlaytime  = m_rAppMediaPlaytime.u32ElapsedPlayTime;
   rfoStMetaData.u32ImageSize        = m_rAppMediaMetaData.u32ImageSize;
   rfoStMetaData.sArtist.bSet(m_rAppMediaMetaData.szArtist.c_str());
   rfoStMetaData.sTitle.bSet(m_rAppMediaMetaData.szTitle.c_str());
   rfoStMetaData.sAlbum.bSet(m_rAppMediaMetaData.szAlbum.c_str());
   rfoStMetaData.sGenre.bSet(m_rAppMediaMetaData.szGenre.c_str());
   rfoStMetaData.sImageMIMEType.bSet(m_rAppMediaMetaData.szImageMIMEType.c_str());
   rfoStMetaData.sAppName.bSet(m_rAppMediaMetaData.szAppName.c_str());

   if(true == m_rAppPhoneData.bPhoneMetadataValid)
   {
      rfoStMetaData.bMetaDataValid = static_cast<tBool>(m_rAppPhoneData.bPhoneMetadataValid);

      t_U8 u8PhoneListSize = m_rAppPhoneData.tvecPhoneCallMetaDataList.size();
      ETG_TRACE_USR4(("[PARAM]: vCopyAppMetadata: Phone Call MetaData List size = %d ", u8PhoneListSize));

      // If phone call list size is one, then forward the data
      if(scou8PhoneListSize == u8PhoneListSize)
      {
         tenPhoneCallState enPhoneCallState = m_rAppPhoneData.tvecPhoneCallMetaDataList[scou8FirstPhoneIndex].enPhoneCallState;
         if((e8CALL_CONNECTING == enPhoneCallState) || (e8CALL_RINGING == enPhoneCallState) || (e8CALL_ACTIVE == enPhoneCallState))
         {
            rfoStMetaData.sPhoneCaller.bSet(m_rAppPhoneData.tvecPhoneCallMetaDataList[scou8FirstPhoneIndex].szPhoneNumber.c_str());
            rfoStMetaData.sPhoneCallInfo.bSet(m_rAppPhoneData.tvecPhoneCallMetaDataList[scou8FirstPhoneIndex].szDisplayName.c_str());
            ETG_TRACE_USR4(("[PARAM]: vCopyAppMetadata: PhoneCaller = %s ", m_rAppPhoneData.tvecPhoneCallMetaDataList[scou8FirstPhoneIndex].szPhoneNumber.c_str()));
            ETG_TRACE_USR4(("[PARAM]: vCopyAppMetadata: PhoneCallInfo = %s ", m_rAppPhoneData.tvecPhoneCallMetaDataList[scou8FirstPhoneIndex].szDisplayName.c_str()));
         }
      }
      //!Multiple call scenario
      else if(scou8PhoneListSize < u8PhoneListSize)
      {
         // If phone call list size is more than one, then forward the data
         // Phone list is iterated in reverse order to mimic the earlier media player behavior
         // where media player was sending the latest data
         t_U8 u8Index = u8PhoneListSize;
         while(0 != u8Index)
         {
            tenPhoneCallState enPhoneCallState = m_rAppPhoneData.tvecPhoneCallMetaDataList[u8Index-1].enPhoneCallState;
            if((e8CALL_CONNECTING == enPhoneCallState) || (e8CALL_RINGING == enPhoneCallState))
            {
               rfoStMetaData.sPhoneCaller.bSet(m_rAppPhoneData.tvecPhoneCallMetaDataList[u8Index-1].szPhoneNumber.c_str());
               rfoStMetaData.sPhoneCallInfo.bSet(m_rAppPhoneData.tvecPhoneCallMetaDataList[u8Index-1].szDisplayName.c_str());
               ETG_TRACE_USR4(("[PARAM]: vCopyAppMetadata: PhoneCaller = %s ", m_rAppPhoneData.tvecPhoneCallMetaDataList[u8Index-1].szPhoneNumber.c_str()));
               ETG_TRACE_USR4(("[PARAM]: vCopyAppMetadata: PhoneCallInfo = %s ", m_rAppPhoneData.tvecPhoneCallMetaDataList[u8Index-1].szDisplayName.c_str()));
               break;
            }
            else if(e8CALL_ACTIVE == enPhoneCallState)
            {
               rfoStMetaData.sPhoneCaller.bSet(m_rAppPhoneData.tvecPhoneCallMetaDataList[u8Index-1].szPhoneNumber.c_str());
               rfoStMetaData.sPhoneCallInfo.bSet(m_rAppPhoneData.tvecPhoneCallMetaDataList[u8Index-1].szDisplayName.c_str());
               ETG_TRACE_USR4(("[PARAM]: vCopyAppMetadata: PhoneCaller = %s ", m_rAppPhoneData.tvecPhoneCallMetaDataList[u8Index-1].szPhoneNumber.c_str()));
               ETG_TRACE_USR4(("[PARAM]: vCopyAppMetadata: PhoneCallInfo = %s ", m_rAppPhoneData.tvecPhoneCallMetaDataList[u8Index-1].szDisplayName.c_str()));
               break;
            }
            u8Index--;
         }
      }
   }
   ETG_TRACE_USR4((" vCopyAppMetadata: MetadataValid = %d", ETG_ENUM(BOOL, rfoStMetaData.bMetaDataValid)));
}//! end of vCopyAppMetadata()

/***************************************************************************
** FUNCTION: tBool devprj_tclService::bHandoverDevProjEnableStatus(amt_tclSe...
***************************************************************************/
tBool devprj_tclService::bHandoverDevProjEnableStatus(
      amt_tclServiceData& roOutMsg,
      tenDeviceCategory enDevCategory) const
{
   tBool bSuccess = FALSE;

   SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);

   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      //! Get ML setting preference from Spi CmdInterface 
      tenEnabledInfo enEnableInfo = e8USAGE_DISABLED;
      if (true ==  m_poSpiCmdInterface->bGetDeviceUsagePreference(
               cou32DevHandle_AllDevices, enDevCategory, enEnableInfo))
      {
         t_Bool bProjEnabled = (e8USAGE_ENABLED == enEnableInfo) ? TRUE : FALSE;

         ETG_TRACE_USR4(("devprj_tclService::bHandoverDevProjEnableStatus: "
               "Retrieved Device Proj EnableInfo = %d (for DeviceCategory = %d)",
               ETG_ENUM(ENABLED_INFO, enEnableInfo),
               ETG_ENUM(DEVICE_CATEGORY, enDevCategory)));

         switch(enDevCategory)
         {
            case e8DEV_TYPE_MIRRORLINK:
         {
            //! Create DevPrj MirrorLinkEnable Status msg.
            devprj_tStMLEnable oStMLEnable;
            oStMLEnable.bProjectionEnabled = bProjEnabled;
            bSuccess = bHandoverStatusMsg(oStMLEnable, roOutMsg);
               break;
         }
            case e8DEV_TYPE_DIPO:
         {
            //! Create DevPrj DiPOEnable Status msg.
            devprj_tStDIPOEnable oStDipoEnable;
            oStDipoEnable.bProjectionEnabled = bProjEnabled;
            bSuccess = bHandoverStatusMsg(oStDipoEnable, roOutMsg);
               break;
            }
            case e8DEV_TYPE_ANDROIDAUTO:
            {
               //! Create DevPrj AAPEnable Status msg.
               devprj_tStGoogleAutoEnable oStGoogleAutoEnable;
               oStGoogleAutoEnable.bProjectionEnabled = bProjEnabled;
               bSuccess = bHandoverStatusMsg(oStGoogleAutoEnable, roOutMsg);
               break;
            }
            default:
            {
               break;
            }
         }
      } //if (true ==  m_poSpiCmdInterface->bGetDeviceUsagePreference(...))
   } //if (OSAL_NULL != m_poSpiCmdInterface)

   return bSuccess;

}//! end of bHandoverDevProjEnableStatus()

/***************************************************************************
** FUNCTION: tBool devprj_tclService::bHandoverDriveModeStatus(amt_tclSe...
***************************************************************************/
tBool devprj_tclService::
      bHandoverDriveModeStatus(amt_tclServiceData& roOutMsg) const
{
   ETG_TRACE_USR4(("devprj_tclService::bHandoverDriveModeStatus: Retrieved ML/DiPO VehicleMode = %d ",
            ETG_ENUM(VEHICLE_MODE, m_bDriveModeEnabled)));

   devprj_tStDriveMode oStDriveMode;
   oStDriveMode.bDriveMode = m_bDriveModeEnabled;
   tBool bSuccess = bHandoverStatusMsg(oStDriveMode, roOutMsg);

   return bSuccess;

}//! end of bHandoverDriveModeStatus()
/***************************************************************************
   ** FUNCTION: tenKeyCode devprj_tclService::vInitializeBaseChannelMap()
***************************************************************************/
tVoid devprj_tclService::vInitializeBaseChannelMap()
{
   ETG_TRACE_USR1(("devprj_tclService::vInitializeBaseChannelMap entered "));

   //! The AV manager channels are mapped to the SPI internally used channels for processing.
   //  The AV manager channels of SPI interest(which cause the change in the ownership of the audio resource)
   //  is used in the map. 
   m_mapBaseChannel[most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_MAIN_AUDIO] = e8SPI_AUDIO_MAIN;
   m_mapBaseChannel[most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_PHONE] = e8SPI_AUDIO_PHONE;
   m_mapBaseChannel[most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_EMER_PHONE] = e8SPI_AUDIO_EMER_PHONE;
   m_mapBaseChannel[most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_ADVISOR_PHONE] = e8SPI_AUDIO_ADVISOR_PHONE;
   m_mapBaseChannel[most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_INCOMING_RING_TONE] = e8SPI_AUDIO_INCOM_TONE;
   m_mapBaseChannel[most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_SPEECH_REC] = e8SPI_AUDIO_SPEECH_REC;
   m_mapBaseChannel[most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_INTERNET_APP_AUDIO] = e8SPI_AUDIO_INTERNET_APP;
   m_mapBaseChannel[most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_TRAFFIC] = s8SPI_AUDIO_TRAFFIC;
   m_mapBaseChannel[most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_EMER_MSG] = e8SPI_AUDIO_EMER_MSG;
   m_mapBaseChannel[most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_SYNCH_MSG ] = e8SPI_AUDIO_SYNC_MSG;
   m_mapBaseChannel[most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_ASYNCH_MSG ] = e8SPI_AUDIO_ASYNC_MSG;
   m_mapBaseChannel[most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_LVM ] = e8SPI_AUDIO_LVM;
   m_mapBaseChannel[most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_AUDIO_CUE ] = e8SPI_AUDIO_CUE;

}

/***************************************************************************
   ** FUNCTION: tenKeyCode devprj_tclService::bGetSPIAudioContext()
***************************************************************************/
tBool devprj_tclService::bGetSPIAudioContext(most_fi_tcl_e8_AVManLogicalAVChannel::tenType enBaseChannel,
      tenAudioContext & rfoAudioCntxt)
{
   std::map<most_fi_tcl_e8_AVManLogicalAVChannel::tenType, tenAudioContext>::iterator it =
      m_mapBaseChannel.find(enBaseChannel);
   t_Bool bRetVal = false;
   if(it != m_mapBaseChannel.end())
   {
      bRetVal = true;
      rfoAudioCntxt = it->second;
   }
   ETG_TRACE_USR1(("devprj_tclService::bGetSPIAudioContext left with Result = %d, DiPOAudioCntxt = %d (for ChannelType %d)",
         ETG_ENUM(BOOL, bRetVal), rfoAudioCntxt, enBaseChannel));
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vGenerateError(const amt_tclServiceData...
***************************************************************************/
tVoid devprj_tclService::vGenerateError(const amt_tclServiceData* pcooMessage) const
{
   t_Bool bSuccess = FALSE;

   SPI_NORMAL_ASSERT(OSAL_NULL == pcooMessage);
   if (OSAL_NULL != pcooMessage)
   {
      //! Extract the DevPrj user context details
      trUserContext rUsrCtxt;
      CPY_TO_USRCNTXT(pcooMessage, rUsrCtxt);

      tU16 u16FuncID = pcooMessage->u16GetFunctionID();
      ETG_TRACE_USR1(("devprj_tclService::vGenerateError() entered: Function ID = 0x%x ", u16FuncID));

      //! Create & send error message
      switch (u16FuncID)
      {
         case MOST_DEVPRJFI_C_U16_GETAPPLICATIONICON:
         {
            devprj_tErrGetAppIcon oGetAppIconErr;
            bSuccess = bPostResponse(oGetAppIconErr, rUsrCtxt);
         }
            break;
         case MOST_DEVPRJFI_C_U16_GETAPPLICATIONIMAGE:
         {
            devprj_tErrGetAppImage oGetAppImageErr;
            bSuccess = bPostResponse(oGetAppImageErr, rUsrCtxt);
         }
            break;
         case MOST_DEVPRJFI_C_U16_DEVICEPROJECTIONALERTBUTTONEVENT:
         {
            devprj_tErrDevPrjAlertBtnEvent oAlertBtnEventErr;
            bSuccess = bPostResponse(oAlertBtnEventErr, rUsrCtxt);
         }
            break;
         case MOST_DEVPRJFI_C_U16_REQUESTACCESSORYDISPLAYCONTEXT_:
         {
            devprj_tErrRequestAccDispCtxt oReqAccDispErr;
            bSuccess = bPostResponse(oReqAccDispErr, rUsrCtxt);
         }
            break;
         case MOST_DEVPRJFI_C_U16_INVOKEBLUETOOTHDEVICEACTION:
         {
            devprj_tErrInvokeBTDevAction oInvBTDevActionErr;
            bSuccess = bPostResponse(oInvBTDevActionErr, rUsrCtxt);
         }
            break;
         case MOST_DEVPRJFI_C_U16_STOPMEDIAPLAYBACK:
         {
            devprj_tErrStopMediaPb oStopMediaPbErr;
            bSuccess = bPostResponse(oStopMediaPbErr, rUsrCtxt);
         }
            break;
         default:
            ETG_TRACE_ERR((" vGenerateError: Invalid Function ID : 0x%x ", u16FuncID));
            break;

      } //switch (u16FuncID)
   } //if (OSAL_NULL != pcooMessage)

   if (FALSE == bSuccess)
   {
      ETG_TRACE_ERR((" vGenerateError(): Method Error post failed! "));
   }
}//! end of vGenerateError()



/***************************************************************************
** FUNCTION: t_Void devprj_tclService::vSetRegion()
***************************************************************************/
t_Void devprj_tclService::vSetRegion(t_U8 u8EOLMarketingRegion)
{
   ETG_TRACE_USR1(("devprj_tclService::vSetRegion = %d", u8EOLMarketingRegion));

   tenRegion enRegion = e8_WORLD;
   for(t_U8 u8Index=0;saEOLRegionMap[u8Index].u8EOLValue != 0xFF;u8Index++)
   {
      if(saEOLRegionMap[u8Index].u8EOLValue == u8EOLMarketingRegion)
      {
         enRegion = saEOLRegionMap[u8Index].enRegion;
         break;
      }// if(saEOLRegionMap[u8Index].u8EOLValue 
   }//for(tU8 u8Index=0;saEOLRegionMap[u8Index]

   if(OSAL_NULL != m_poSpiCmdInterface)
   {
      m_poSpiCmdInterface->vSetRegion(enRegion);
   } //if(OSAL_NULL != m_poSpiCmdInterface)
}

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vUpdateDayNightMode()
***************************************************************************/
tVoid devprj_tclService::vUpdateDayNightMode()
{
   //! Evaluate DayNight mode based on current setting in SystemState and
   //! CenterstackHMI FBlocks.
   //! If CenterStackHMI.DayNightModeOverrideSetting is 
   //! DAY_NIGHT_OVERRIDE_NOT_SUPPORTED or DAY_NIGHT_OVERRIDE_AUTOMATIC then 
   //! mode is according to SystemState.DayNightMode. 
   //! Else, mode is as per CenterStackHMI.DayNightModeOverrideSetting.

   tBool bIsNightMode = false;
   if (
      (e8OVERRIDE_NOT_SUPPORTED == m_enCntrStackHmiDayNightMode)
      ||
      (e8OVERRIDE_AUTOMATIC == m_enCntrStackHmiDayNightMode)
      )
   {
      //! DayNight mode is as per SystemState DayNightMode
      bIsNightMode = (e8NIGHT_MODE == m_enSysStateDayNightMode);
   }
   else
   {
      //! DayNight mode is as per CenterStackHMI DayNightMode Override
      bIsNightMode = (e8OVERRIDE_NIGHTMODE == m_enCntrStackHmiDayNightMode);
   }

   ETG_TRACE_USR2((" devprj_tclService::vUpdateDayNightMode: (SystemState : %d, CenterStackHMI : %d) Evaluated current mode = %u ",
         ETG_ENUM(DAYNIGHT_MODE, m_enSysStateDayNightMode),
         ETG_ENUM(DAYNIGHT_MODE_HMI_OVERRIDE, m_enCntrStackHmiDayNightMode),
         ETG_ENUM(DAYNIGHT_MODE, bIsNightMode)));

   //! Forward DayNight mode info to SPI.
   SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);
   if (OSAL_NULL != m_poSpiCmdInterface)
   {
       tenVehicleConfiguration enVehicleConfig =
             (true == bIsNightMode) ? (e8_NIGHT_MODE) : (e8_DAY_MODE);

       m_poSpiCmdInterface->vSetVehicleConfig(enVehicleConfig, true, corEmptyUsrContext);
   } //if (OSAL_NULL != m_poSpiCmdInterface)
}


/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vPostDeviceAppState()
***************************************************************************/
t_Void devprj_tclService::vPostDeviceAppState(tenSpeechAppState enSpeechAppState, 
      tenPhoneAppState enPhoneAppState,  tenNavAppState enNavAppState, 
      const trUserContext rcUsrCntxt)
{
   ETG_TRACE_USR1(("devprj_tclService::vPostDeviceAppState() entered with Speech:%d, Nav:%d, Phone:%d",
      ETG_ENUM(SPEECH_APP_STATE, enSpeechAppState),
      ETG_ENUM(NAV_APP_STATE, enNavAppState),
      ETG_ENUM(PHONE_APP_STATE, enPhoneAppState)));

   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);

   //! Store Device speech app state
   senDeviceSpeechAppState = enSpeechAppState;

   if ((e8SPI_NAV_ACTIVE == m_enNavAppState) && (e8SPI_NAV_ACTIVE == enNavAppState))
   {
      //enAccNavAppState = e8SPI_NAV_NOT_ACTIVE;
      if((true == m_bNavStatus) &&(OSAL_NULL != m_poNavigationClient))
      {
         m_poNavigationClient->vSetNavGuidanceStatus(e8GUIDANCE_INACTIVE);
      }
      if((true == m_bOnStarNavStatus) && (OSAL_NULL != m_poOnstarNavClient))
      {
         m_poOnstarNavClient->bCancelOnstarNavigation();
      }
   }//if(e8SPI_NAV_ACTIVE == enNavAppState)
   

   //TODO - Update property only for DiPO device?
   //! Update Clients about ProjectedDisplayContext change
   if (AIL_EN_N_NO_ERROR != eUpdateClients(MOST_DEVPRJFI_C_U16_APPSTATUSINFO))
   {
      ETG_TRACE_ERR((" vPostDeviceAppState: ""Updating AppStatusInfo.Status failed! "));
   }//if (AIL_EN_N_NO_ERROR !=...)
}

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::bHandoverDeviceAppStateStatus()
***************************************************************************/
tBool devprj_tclService::bHandoverDeviceAppStateStatus(amt_tclServiceData& roOutMsg) const
{
   ETG_TRACE_USR1(("devprj_tclService::bHandoverDeviceAppStateStatus() entered "));
   tBool bSuccess = FALSE;
   tenSpeechAppState enSpeechAppState = e8SPI_SPEECH_UNKNOWN;
   tenPhoneAppState enPhoneAppState = e8SPI_PHONE_UNKNOWN;
   tenNavAppState enNavAppState = e8SPI_NAV_UNKNOWN;
   SPI_NORMAL_ASSERT(OSAL_NULL == m_poSpiCmdInterface);
   
   if(OSAL_NULL != m_poSpiCmdInterface)
   {
      m_poSpiCmdInterface->vGetAppStateInfo(enSpeechAppState, enPhoneAppState, enNavAppState);

      devprj_tStAppStateInfo oStAppState;
      oStAppState.e8SpeechAppState.enType = static_cast<devprj_tenSpeechAppState>(enSpeechAppState);
      oStAppState.e8PhoneAppState.enType = static_cast<devprj_tenPhoneAppState>(enPhoneAppState);
      oStAppState.e8NavigationAppState.enType = static_cast<devprj_tenNavAppState>(enNavAppState);

      bSuccess = bHandoverStatusMsg(oStAppState, roOutMsg);
   } // if(OSAL_NULL != m_poSpiCmdInterface)
  
   return bSuccess;
}

/***************************************************************************
 ** FUNCTION:  t_Bool devprj_tclService::bRequestAltAudActivation(const tU8 cou8SrcNum)
 ***************************************************************************/
t_Bool devprj_tclService::bRequestAltAudActivation(const tU8 cou8SrcNum)
{
   ETG_TRACE_USR1(("bRequestAltAudActivation entered Src Num %d",cou8SrcNum));

   tBool bRetVal = true;

   //! If ducking is enabled, LC_MIX_ALERT_MSG would have been activated ,
   //! so do not activate Alternate Audio channel(LC_ALERT_TONE)
   m_oAudioDuckLock.s16Lock();
   if(false == sbAudDuckEnabled)
   {
      ETG_TRACE_USR4(("Activating LC_ALERT_TONE \n"));
      //bRetVal = bTriggerAVAction(cou8SrcNum , IIL_EN_ISRC_REQACT);
      bRetVal = bQueueAlternateAudioReq(cou8SrcNum, true);
      sbAltAudActivated = true;
   }
   else if (NULL != m_poSpiCmdInterface)
   {
      //! @Note: Source activity ON is mocked so that audio prepare request from
      //! phone is answered when duck request is received before prepare.
      //! bOnAllocate is not mocked as both LC_ALERT_TONE and LC_MIX_ALERT
      //! use the same alsa device
      ETG_TRACE_USR4(("Mocking source activity as route is already allocated \n"));
      m_poSpiCmdInterface->vOnStartSourceActivity(cou8SrcNum);
   }
   m_oAudioDuckLock.vUnlock();
   return bRetVal;

}//devprj_tclService::bRequestAltAudActivation(const tU8 cou8SrcNum)

/***************************************************************************
 ** FUNCTION:  t_Bool devprj_tclService::bRequestAltAudDeactivation(const tU8 cou8SrcNum)
 ***************************************************************************/
t_Bool devprj_tclService::bRequestAltAudDeactivation(const tU8 cou8SrcNum)
{
   ETG_TRACE_USR1(("bRequestAltAudDeactivation entered Src Num %d",cou8SrcNum));

   tBool bRetVal = true;

   if((OSAL_NULL != m_poMainAppl) && (true == bUnduckTimerRunning))
   {
      ETG_TRACE_USR4(("Unduck Timer stopped \n"));
      m_poMainAppl->bStopTimer(UNDUCK_TIMER_ID);
      bUnduckTimerRunning = false;
   }

   //! de-activate for Duck(LC_MIX_ALERT_MSG)
   m_oAudioDuckLock.s16Lock();
   if(true == sbAudDuckEnabled)
   {
      ETG_TRACE_USR4(("Deactivating LC_MIX_ALERT_MSG \n"));
      //bRetVal = bTriggerAVAction(cou8SrcNum , IIL_EN_ISRC_REQDEACT);
      bRetVal = bQueueAlternateAudioReq(scu8DuckAudSrcNum, false);
      sbAudDuckEnabled = false;
   }//End of if(true == sbAudDuckEnabled)

   //! de-activate for Alternate Audio(LC_ALERT_TONE)
   if(true == sbAltAudActivated)
   {
      ETG_TRACE_USR4(("Deactivating LC_ALERT_TONE \n"));
      //bRetVal = bTriggerAVAction(scu8AltAudSrcNum , IIL_EN_ISRC_REQDEACT);
      bRetVal = bQueueAlternateAudioReq(scu8AltAudSrcNum, false);
      sbAltAudActivated = false;
   }//End of if(false == sbAltAudActivated)
   m_oAudioDuckLock.vUnlock();

   return bRetVal;

}//devprj_tclService::bRequestAltAudDeActivation(const tU8 cou8SrcNum)

/*************************Start of ISource methods*************************/

/***************************************************************************
 ** FUNCTION:  t_Bool devprj_tclService::bSetAudioDucking(const tU8 cou8SrcNum,
 ** const tU16 cou16RampDuration, const tU8 cou8VolumeLevelindB, const tenDuckingType coenDuckingType)
 ***************************************************************************/
t_Bool devprj_tclService::bSetAudioDucking(const tU8 cou8SrcNum, const tU16 cou16RampDuration,
         const tU8 cou8VolumeLevelindB, const tenDuckingType coenDuckingType)
{
   ETG_TRACE_USR1(("devprj_tclService::bSetAudioDucking entered Src Num %d, Alt Src Num %d ducking val %d",cou8SrcNum,
            scu8AltAudSrcNum, ETG_ENUM(AUDIO_DUCK_TYPE, coenDuckingType)));

   SPI_INTENTIONALLY_UNUSED(cou16RampDuration);
   SPI_INTENTIONALLY_UNUSED(cou8VolumeLevelindB);

   tBool bRetVal = false;
   t_U32 u32DeviceHandle = 0xffff;
   tenDeviceCategory enDeviceCategory = e8DEV_TYPE_UNKNOWN;
   
   if(OSAL_NULL != m_poSpiCmdInterface)
   {
      u32DeviceHandle = m_poSpiCmdInterface->u32GetSelectedDeviceHandle();
      enDeviceCategory = m_poSpiCmdInterface->enGetDeviceCategory(u32DeviceHandle);
	  bRetVal = (e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory);
   }//if(OSAL_NULL != m_poSpiCmdInterface)
   
   su8AudDuckSrcNum = (e8DEV_TYPE_DIPO == enDeviceCategory) ? cou8SrcNum:su8AudDuckSrcNum;

   if((e8_DUCKINGTYPE_DUCK == coenDuckingType) && (e8DEV_TYPE_DIPO == enDeviceCategory))
   {
      if((OSAL_NULL != m_poMainAppl) && (true == bUnduckTimerRunning))
      {
         ETG_TRACE_USR4(("Unduck Timer stopped \n"));
         m_poMainAppl->bStopTimer(UNDUCK_TIMER_ID);
         bUnduckTimerRunning = false;
      }//End of if((OSAL_NULL != m_poMainAppl) && (true == bUnduckTimerRunning))

      m_oAudioDuckLock.s16Lock();
      //! If alternate channel(LC_ALERT_TONE) is activated, de-activate it.
      if(true == sbAltAudActivated)
      {
         ETG_TRACE_USR4(("Deactivating LC_ALERT_TONE \n"));
         //bRetVal = bTriggerAVAction(scu8AltAudSrcNum , IIL_EN_ISRC_REQDEACT);
         bRetVal = bQueueAlternateAudioReq(scu8AltAudSrcNum, false);
         sbAltAudActivated = false;
      }//End of if(true == sbAltAudActivated)

      //! activate for Duck(LC_MIX_ALERT_MSG);
      if(false == sbAudDuckEnabled)
      {
         ETG_TRACE_USR4(("Activating LC_MIX_ALERT_MSG \n"));
         //bRetVal = bTriggerAVAction(cou8SrcNum , IIL_EN_ISRC_REQACT);
         bRetVal = bQueueAlternateAudioReq(cou8SrcNum, true);
         sbAudDuckEnabled = true;
      }//End of if(false == sbAudDuckEnabled)
      m_oAudioDuckLock.vUnlock();
   }//End of if(e8_DUCKINGTYPE_DUCK == coenDuckingType)

   else if (e8DEV_TYPE_DIPO == enDeviceCategory)
   {
      if((OSAL_NULL != m_poMainAppl) && (false == bUnduckTimerRunning))
      {
         ETG_TRACE_USR4(("Unduck Timer started \n"));
         bUnduckTimerRunning = m_poMainAppl->bStartTimer(UNDUCK_TIMER_ID,
                  UNDUCK_TIMER_FIRST_TICK, UNDUCK_TIMER_INTERVAL);
      }//End of if((OSAL_NULL != m_poMainAppl) && (false == bUnduckTimerRunning))
      else
      {
         //! de-activate for Duck(LC_MIX_ALERT_MSG);
         m_oAudioDuckLock.s16Lock();
         if(true == sbAudDuckEnabled)
         {
            ETG_TRACE_USR4(("Deactivating LC_MIX_ALERT_MSG \n"));
            //bRetVal = bTriggerAVAction(cou8SrcNum , IIL_EN_ISRC_REQDEACT);
            bRetVal = bQueueAlternateAudioReq(cou8SrcNum, false);
            sbAudDuckEnabled = false;
         }//End of if(true == sbAudDuckEnabled)

         //! If alternate channel(LC_ALERT_TONE) is de-activated, activate it.
         if(false == sbAltAudActivated)
         {
            ETG_TRACE_USR4(("Activating LC_ALERT_TONE \n"));
            //bRetVal = bTriggerAVAction(scu8AltAudSrcNum , IIL_EN_ISRC_REQACT);
            bRetVal = bQueueAlternateAudioReq(scu8AltAudSrcNum, true);
            sbAltAudActivated = true;
         }//End of if(false == sbAltAudActivated)
         m_oAudioDuckLock.vUnlock();
      }//End of else
   }//End of else e8_DUCKINGTYPE_UNDUCK == coenDuckingType

   return bRetVal;

}//devprj_tclService::bSetAudioDucking(const tU16 cou16RampDuration, ..)

/***************************************************************************
 ** FUNCTION:  t_Bool devprj_tclService::bRequestAudioActivation(t_U8 u8SourceNum)
 ***************************************************************************/
t_Bool devprj_tclService::bRequestAudioActivation(tU8 u8SourceNum)
{
   ETG_TRACE_USR1(("devprj_tclService::bRequestAudioActivation entered = %d",u8SourceNum));

   tBool bRetVal = false;

   //! Retrieve Selected Device Handle  Category
   tenDeviceCategory enSelDevCat = e8DEV_TYPE_UNKNOWN;
   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      enSelDevCat = m_poSpiCmdInterface->enGetDeviceCategory(
            m_poSpiCmdInterface->u32GetSelectedDeviceHandle());
   }//if (OSAL_NULL != m_poSpiCmdInterface)

   //@Note: Request for audio only if a device is active.
   //TODO - post audio error for cleanup if device is not active?
   if (e8DEV_TYPE_UNKNOWN != enSelDevCat)
   {
      //! If source activation request for Alternate Audio is received.
      if((scu8AltAudSrcNum == u8SourceNum) && (e8DEV_TYPE_DIPO == enSelDevCat))
      {
         bRetVal = bRequestAltAudActivation(u8SourceNum);
      }//End of ((scu8AltAudSrcNum == u8SourceNum) && (e8DEV_TYPE_DIPO == enSelDevCat))
      else
      {
         t_Bool bSendAudReq = true;

         //! Validate whether audio should be requested for CarPlay device.
         if (e8DEV_TYPE_DIPO == enSelDevCat)
         {
#ifdef VARIANT_S_FTR_ENABLE_GM_CARPLAY_MEDIA
            //! @Note: Do not request audio activation for CarPlay Media (since MediaPlayer handles source allocation)
            bSendAudReq = (scu8AudMainSrcNum != u8SourceNum);
#endif
         }//if (e8DEV_TYPE_DIPO == enSelDevCat)
         //! Validate whether audio should be requested for AAP device.
         else if (e8DEV_TYPE_ANDROIDAUTO == enSelDevCat)
         {
            //! @Note: Do not request main audio activation if:
            //! (1) DeviceProjection main source is already active (to prevent activating an already active channel)
            //! (2) DeviceProjection main source will be restored by Audio manager (i.e. is in suspended state)
            m_oDevPrjMainAudioStatusLock.s16Lock();
            if ((scu8AudMainSrcNum == u8SourceNum) &&
                  ((true == m_bIsDevPrjMainAudioActive) || (true == m_bIsDevPrjMainAudioSuspended)))
            {
               bSendAudReq = false;
            }
            else if(scu8AudSpeechSrcNum == u8SourceNum)
            {
               senDeviceSpeechAppState = e8SPI_SPEECH_SPEAKING;
            }
            m_oDevPrjMainAudioStatusLock.vUnlock();
         }//else if (e8DEV_TYPE_ANDROIDAUTO == enSelDevCat)

         if (true == bSendAudReq)
         {
            m_oSpeechChnReqLock.s16Lock();
            m_bSpeechChnRequested = (scu8AudSpeechSrcNum == u8SourceNum) ? (true) : m_bSpeechChnRequested;
            m_oSpeechChnReqLock.vUnlock();

            //We are requesting for AV Activation. so set the source availability to true
            //so that AV Manager will allocate the route for SPI.
            //by default source availability is set to FALSE
            bSetSrcAvailability(e8AUD_MAIN_OUT, true);
            //@Note: Source availability is set to FALSE, from tcl_Audio on device de selection.
            //So this is not done here. This needs to be revisited later
            bRetVal = bTriggerAVAction(u8SourceNum, IIL_EN_ISRC_REQACT);
         }//if (true == bSendAudReq)
         else
         {
            ETG_TRACE_USR3(("devprj_tclService::bRequestAudioActivation: Nothing to be done! "));
         }
      }//End of else
   }//if (e8DEV_TYPE_UNKNOWN != enSelDevCat)

   return bRetVal;

}//devprj_tclService::bRequestAudioActivation(tU8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  t_Bool devprj_tclService::bRequestAudioDeactivation(tU8 u8SourceNum)
 ***************************************************************************/
t_Bool devprj_tclService::bRequestAudioDeactivation(t_U8 u8SourceNum)
{
   ETG_TRACE_USR1(("devprj_tclService::bRequestAudioDeactivation entered = %d",u8SourceNum));

   tBool bRetVal = false;

   //! Retrieve Selected Device Handle  Category
   tenDeviceCategory enSelDevCat = e8DEV_TYPE_UNKNOWN;
   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      enSelDevCat = m_poSpiCmdInterface->enGetDeviceCategory(
            m_poSpiCmdInterface->u32GetSelectedDeviceHandle());
   }

   //! If source deactivation request for Alternate Audio is received.
   if((scu8AltAudSrcNum == u8SourceNum) && (e8DEV_TYPE_DIPO == enSelDevCat))
   {
      bRetVal = bRequestAltAudDeactivation(u8SourceNum);
   }//End of ((scu8AltAudSrcNum == u8SourceNum) && (e8DEV_TYPE_DIPO == enSelDevCat))
   else
   {
      senDeviceSpeechAppState = ((e8DEV_TYPE_ANDROIDAUTO == enSelDevCat) && (scu8AudSpeechSrcNum == u8SourceNum)) ?
            (e8SPI_SPEECH_END) : (senDeviceSpeechAppState);

      //! Never deactivate main audio channel (GMMY17-12341)
      if (scu8AudMainSrcNum != u8SourceNum)
      {
         //! Forward request to IIL
         bRetVal = bTriggerAVAction(u8SourceNum, IIL_EN_ISRC_REQDEACT);
      }//if (true == bSendAudReq)
      else
      {
         ETG_TRACE_USR3(("devprj_tclService::bRequestAudioDeactivation: Nothing to be done! "));
      }
   }//End of else

   return bRetVal;

}//devprj_tclService::bRequestAudioDeactivation(tU8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  t_Void devprj_tclService::vStartSourceActivityResult(t_U8,t_Bool)
 ***************************************************************************/
t_Void devprj_tclService::vStartSourceActivityResult(t_U8 u8SourceNum,
      t_Bool bError)
{
   SPI_INTENTIONALLY_UNUSED(bError);
   ETG_TRACE_USR1(("devprj_tclService::vStartSourceActivityResult entered "));
   ETG_TRACE_USR4((" Source Activity Start Acknowledgment received for Source Number: %d",
                     u8SourceNum));

   /* NOTE: IIL does not require the streaming application to notify Start of
    Source Activity. Hence the trigger is unused */

}//devprj_tclService::vStartSourceActivityResult(t_U8 t_Bool)

/***************************************************************************
 ** FUNCTION:  t_Void devprj_tclService::vStopSourceActivityResult(t_U8,t_Bool)
 ***************************************************************************/
t_Void devprj_tclService::vStopSourceActivityResult(t_U8 u8SourceNum,
      t_Bool bError)
{
   SPI_INTENTIONALLY_UNUSED(bError);
   ETG_TRACE_USR1(("devprj_tclService::vStopSourceActivityResult entered "));
   ETG_TRACE_USR4((" Source Activity Stop Acknowledgment received for Source Number: %d",
                     u8SourceNum));

   /* NOTE: IIL does not require the streaming application to notify End of
    Source Activity. Hence the trigger is unused */

}//devprj_tclService::vStopSourceActivityResult(t_U8 t_Bool)

/***************************************************************************
 ** FUNCTION:  t_Bool devprj_tclService::bSetSrcAvailability(t_U8,t_Bool)
 ***************************************************************************/
t_Bool devprj_tclService::bSetSrcAvailability(t_U8 u8SourceNum, t_Bool bAvail)
{
   SPI_INTENTIONALLY_UNUSED(u8SourceNum);

   ETG_TRACE_USR1(("devprj_tclService::bSetSourceAvailability:m_bSrcAvailSet-%d bAvail-%d",
      ETG_ENUM(BOOL,m_bSrcAvailSet),ETG_ENUM(BOOL,bAvail)));

   t_Bool bRetVal = true;

   if(bAvail != m_bSrcAvailSet)
   {  
      vSetSrcAvailable(bAvail);
      m_bSrcAvailSet = bAvail;
   }//if(bAvail != m_bSrcAvailSet)
   

   return bRetVal;
}//devprj_tclService::bSetSourceAvailability(t_U8 t_Bool)


/**************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnMsSrcActivity(amt_tclServiceData*...
**************************************************************************/
tVoid devprj_tclService::vOnMsSrcActivity(amt_tclServiceData *poMessage)
{
   ETG_TRACE_USR1(("devprj_tclService::vOnMsSrcActivity entered "));

   //! SourceActivity is received by SPI first and then forwarded to IIL (Fix for GMMY17-10880)
   //! This is used only to process SA_OFF, since need to release audio resources asap.
   //! SA_ON is processed by SPI after IIL finishes processing it (in bOnSrcActivityStart or bOnSrcActivityResult)
   iil_tISrcActivityStart oSrcActivity(*poMessage, DEVPROJ_SERVICE_FI_MAJOR_VERSION);

   if (TRUE == oSrcActivity.bIsValid())
   {
      t_U8 u8SrcNum = oSrcActivity.u8SourceNr;
      ETG_TRACE_USR4(("Source Activity invoked for SrcNum: %d, Activity: %d",
            u8SrcNum,  ETG_ENUM(IIL_SRC_ACT, oSrcActivity.e8Activity.enType)));

      m_oSpeechChnReqLock.s16Lock();
      m_bSpeechChnRequested = (scu8AudSpeechSrcNum == u8SrcNum)? (false) : m_bSpeechChnRequested;
      t_Bool bIsSpeechChnRequested = m_bSpeechChnRequested;
      m_oSpeechChnReqLock.vUnlock();

      if ((IIL_SRCACT_EN_OFF == oSrcActivity.e8Activity.enType) && (NULL != m_poSpiCmdInterface))
      {
         m_poSpiCmdInterface->vOnStopSourceActivity(u8SrcNum);

         //! Set audio context as invalid (will be set to correct state once BaseChannelStatus update is received)
         //! Note: Fix for GMMY17-3482. SPI should take/borrow audio focus to MD asap, to stop it from streaming music.
         //! Fix for GMMY17-10158 - VR icon hanging due to audio focus change from GAIN to GAIN_TRANSIENT when speech channel is requested
         if ((scu8AudMainSrcNum == u8SrcNum) && (e8SPI_SPEECH_SPEAKING != senDeviceSpeechAppState) && (false == bIsSpeechChnRequested))
         {
            m_poSpiCmdInterface->vSetAccessoryAudioContext(m_poSpiCmdInterface->u32GetSelectedDeviceHandle(),
                  e8SPI_AUDIO_UNKNOWN, true, corEmptyUsrContext, e8DEV_TYPE_ANDROIDAUTO);
         }

         //! Clear flag if Duck channel is deactivated by SBX (happens if Speech channel becomes active)
         m_oAudioDuckLock.s16Lock();
         sbAudDuckEnabled = (su8AudDuckSrcNum == u8SrcNum)?(false):(sbAudDuckEnabled);
         m_oAudioDuckLock.vUnlock();
      }
   }//if (TRUE == oSrcActivity.bIsValid())
   else
   {
      ETG_TRACE_ERR(("vOnMsSrcActivity: Invalid message received "));
   }

   //! Dispatch the request to IIL
   vOnMsISource(poMessage);

}//devprj_tclService::vOnMsSrcActivity(amt_tclServiceData *poMessage)

// IIL Functions
/***************************************************************************
 ** FUNCTION:  tBool devprj_tclService::bOnSrcActivityStart(
 arl_tenSource enSrcNum, const iil_tSrcActivity& rfcoSrcActivity)
 ***************************************************************************/
tBool devprj_tclService::bOnSrcActivityStart(const tU8 cu8SrcNum,
         const iil_tSrcActivity& rfcoSrcActivity)
{
   ETG_TRACE_USR1(("devprj_tclService::bOnSrcActivityStart entered "));

   tBool bRetVal = (OSAL_NULL != m_poSpiCmdInterface);

   // Pointer NULL check kept for LINT Compliance
   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      switch (rfcoSrcActivity.enType)
      {
         case IIL_SRCACT_EN_ON:
         {
            ETG_TRACE_USR4((" Source Activity ON Start received for Src Num: %d", cu8SrcNum));
            //@Note: SA_ON is triggered from here only for CarPlay, to comply with audio latency requirements.
            if (e8DEV_TYPE_DIPO == m_poSpiCmdInterface->enGetDeviceCategory(
                        m_poSpiCmdInterface->u32GetSelectedDeviceHandle()))
            {
               m_poSpiCmdInterface->vOnStartSourceActivity(cu8SrcNum);
            }
         }
            break;

         case IIL_SRCACT_EN_PAUSE:
         case IIL_SRCACT_EN_OFF:
         {
            ETG_TRACE_USR4((" Source Activity OFF Start received for Src Num: %d", cu8SrcNum));
         }
            break;

         default:
         {
            ETG_TRACE_USR4((" Unknown Source Activity Start received for Src Num: %d", cu8SrcNum));
            bRetVal = FALSE;
         }
            break;
      }//switch(rfcoSrcActivity.enType)
   }//if (OSAL_NULL != m_poSpiCmdInterface)

   return bRetVal;
} //devprj_tclService::bOnSrcActivityStart(arl_tenSource,const arl_tSrcActivity&)

/***************************************************************************
** FUNCTION:  virtual tBool devprj_tclService::bSrcActivityResult(const iil_t..
***************************************************************************/
tBool devprj_tclService::bSrcActivityResult(const tU8 cu8SrcNum ,
          const iil_tSrcActivity& rfcoSrcActivity)
{
   ETG_TRACE_USR1(("devprj_tclService::bSrcActivityResult entered "));

   tBool bRetVal = (OSAL_NULL != m_poSpiCmdInterface);

   // Pointer NULL check kept for LINT Compliance
   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      switch (rfcoSrcActivity.enType)
      {
         case IIL_SRCACT_EN_ON:
         {
            ETG_TRACE_USR4((" Source Activity ON Result received for Src Num: %d", cu8SrcNum));
            //@Note: Except for CarPlay, SA_ON is triggered from here for main audio channel
            if ((scu8AudMainSrcNum == cu8SrcNum) && (e8DEV_TYPE_DIPO != m_poSpiCmdInterface->enGetDeviceCategory(
                  m_poSpiCmdInterface->u32GetSelectedDeviceHandle())))
            {
               m_poSpiCmdInterface->vOnStartSourceActivity(cu8SrcNum);
            }
         }
            break;

         case IIL_SRCACT_EN_PAUSE:
         case IIL_SRCACT_EN_OFF:
         {
            ETG_TRACE_USR4((" Source Activity OFF Result received for Src Num: %d", cu8SrcNum));
         }
            break;

         default:
         {
            ETG_TRACE_USR4((" Unknown Source Activity Result received for Src Num: %d", cu8SrcNum));
            bRetVal = FALSE;
         }
            break;
      }//switch(rfcoSrcActivity.enType)
   }//if (OSAL_NULL != m_poSpiCmdInterface)

   return bRetVal;
} //devprj_tclService::bSrcActivityResult(arl_tenSource,const arl_tSrcActivity&)

/***************************************************************************
 ** FUNCTION:  tBool devprj_tclService::bOnAllocateResult(const tU8 cu8SrcNum,
 const iil_tAllocRouteResult& rfcoAllocRoute)
 ***************************************************************************/
tBool devprj_tclService::bOnAllocateResult(const tU8 cu8SrcNum,
         const iil_tAllocRouteResult& rfcoAllocRoute)
{
   ETG_TRACE_USR1(("devprj_tclService::bOnAllocateResult for SrcNum %d",cu8SrcNum ));

   tBool bRetVal = true;
   trAudSrcInfo rSrcInfo;

   if ((OSAL_NULL != m_poSpiCmdInterface) && (su8AudDuckSrcNum != cu8SrcNum))
   {
      t_U8 u8InListSize = rfcoAllocRoute.listInputDev.strALSADev.size();
	  t_U8 u8OutListSize = rfcoAllocRoute.listOutputDev.strALSADev.size();
	  
      ETG_TRACE_USR4((" Audio Device List Size: Input [%d], Output [%d]", u8InListSize, u8OutListSize));

      /* In case of Main Audio and Mix Audio source , we get only one ALSA device names.*/
      if ((1 == u8OutListSize)&&(0 == u8InListSize))
      {
         GET_STRINGDATA_FROM_FI_STRINGOBJ((rfcoAllocRoute.listOutputDev.strALSADev.front()), MIDWFI_CHAR_SET_UTF8, rSrcInfo.rMainAudDevNames.szOutputDev);
         ETG_TRACE_USR4((" Audio Output Device: %s ", rSrcInfo.rMainAudDevNames.szOutputDev.c_str()));
      }
      /* In case of Phone Audio and VR Audio source , we get only four ALSA device names(2-ECNR and 2- Main Audio).*/
      else if((3 == u8OutListSize) && (3 == u8InListSize))
      {
         GET_STRINGDATA_FROM_FI_STRINGOBJ((rfcoAllocRoute.listInputDev.strALSADev.at(0)), MIDWFI_CHAR_SET_UTF8, rSrcInfo.rMainAudDevNames.szInputDev);
         ETG_TRACE_USR4(("bOnAllocate: ALSAInDev : %s ", rSrcInfo.rMainAudDevNames.szInputDev.c_str()));

         GET_STRINGDATA_FROM_FI_STRINGOBJ((rfcoAllocRoute.listInputDev.strALSADev.at(1)), MIDWFI_CHAR_SET_UTF8, rSrcInfo.rEcnrAudDevNames.szInputDev);
         ETG_TRACE_USR4(("bOnAllocate: ECNR ALSAInDev : %s ", rSrcInfo.rEcnrAudDevNames.szInputDev.c_str()));

         GET_STRINGDATA_FROM_FI_STRINGOBJ((rfcoAllocRoute.listOutputDev.strALSADev.at(1)), MIDWFI_CHAR_SET_UTF8, rSrcInfo.rEcnrAudDevNames.szOutputDev);
         ETG_TRACE_USR4(("bOnAllocate: ECNR ALSAOutDev : %s ", rSrcInfo.rEcnrAudDevNames.szOutputDev.c_str()));

         GET_STRINGDATA_FROM_FI_STRINGOBJ((rfcoAllocRoute.listOutputDev.strALSADev.at(2)), MIDWFI_CHAR_SET_UTF8, rSrcInfo.rMainAudDevNames.szOutputDev);
         ETG_TRACE_USR4(("bOnAllocate: ALSAOutDev : %s ", rSrcInfo.rMainAudDevNames.szOutputDev.c_str()));	 
      }
      else
      {
         ETG_TRACE_ERR(("devprj_tclService::bOnAllocateResult: Invalid ALSA Device Names"));
         bRetVal = false;
      }

      bRetVal = (true == bRetVal) ? m_poSpiCmdInterface->bOnRouteAllocateResult(cu8SrcNum, rSrcInfo):(bRetVal);

   }//if((OSAL_NULL != m_poSpiCmdInterface) && (su8AudDuckSrcNum != cu8SrcNum))

   return bRetVal;

}// tBool devprj_tclService::bOnAllocateResult(const tU8 cu8SrcNum, ..)

/************************************************************************************
 ** FUNCTION:  tBool devprj_tclService::bOnDeAllocateResult(const tU8 cu8SrcNum)
 *************************************************************************************/
tBool devprj_tclService::bOnDeAllocateResult(const tU8 cu8SrcNum)
{
   ETG_TRACE_USR1(("devprj_tclService::bOnDeAllocateResult entered for Src Num %d ", cu8SrcNum));

   tBool bRetVal = (OSAL_NULL != m_poSpiCmdInterface);

   if ((OSAL_NULL != m_poSpiCmdInterface) && (su8AudDuckSrcNum != cu8SrcNum))
   {
      m_poSpiCmdInterface->vOnRouteDeAllocateResult(cu8SrcNum);
   }//if ((OSAL_NULL != m_poSpiCmdInterface) && (su8AudDuckSrcNum != cu8SrcNum))

   return bRetVal;

}//tBool devprj_tclService::bOnDeAllocateResult(const tU8 cu8SrcNum)

/***************************************************************************
 ** FUNCTION:  tBool devprj_tclService::bOnReqAVActResult(const tU8 cu8SrcNum..)
 ***************************************************************************/
tBool devprj_tclService::bOnReqAVActResult(const tU8 cu8SrcNum, const iil_tAVReqResult& coAVReqResult)
{
   ETG_TRACE_USR1(("devprj_tclService::bOnReqAVActResult cu8SrcNum = %d coAVReqResult = %d",cu8SrcNum, coAVReqResult.enType));
   if((IIL_REQAVACT_EN_GRANTED != coAVReqResult.enType) && (su8AudDuckSrcNum != cu8SrcNum))
   {
      vOnError(cu8SrcNum, IIL_EN_ISRC_REQAVACT_ERR);
   }
   else if ((NULL != m_poSpiCmdInterface) && (IIL_REQAVACT_EN_GRANTED == coAVReqResult.enType) && (scu8AudMainSrcNum != cu8SrcNum)
         && (e8DEV_TYPE_DIPO != m_poSpiCmdInterface->enGetDeviceCategory(m_poSpiCmdInterface->u32GetSelectedDeviceHandle())))
   {
      //!@Note: Except for CarPlay, SA_ON is moved here to prevent requesting for de-activation of channel when a channel activation is in progress.
      //This ensures SPI waits for channel activation result, before requesting for deactivation of the channel.
      //Main audio channel is not handled here since main audio is not always requested (can be restored by AVManager)
      m_poSpiCmdInterface->vOnStartSourceActivity(cu8SrcNum);
   }
   if ((cu8SrcNum == m_rAltAudioStatus.u8SrcNumber) && (e8_AUDIO_ACTIVATING == m_rAltAudioStatus.enCurrAudioState))
   {
      //! Set the current alaternate audio state to idle once result is received
      m_oAltAudioStatusLock.s16Lock();
      m_rAltAudioStatus.u8SrcNumber = cu8SrcNum;
      m_rAltAudioStatus.enCurrAudioState = e8_AUDIO_IDLE;
      m_oAltAudioStatusLock.vUnlock();
   }
   if(cu8SrcNum == m_rAltAudioRequest.u8SourceNumber)
   {
      //! Process the request pending for cu8SrcNum
      bQueueAlternateAudioReq(m_rAltAudioRequest.u8SourceNumber, m_rAltAudioRequest.bActivate);
      m_oAltAudioRequestLock.s16Lock();
      m_rAltAudioRequest.u8SourceNumber = 0;
      m_rAltAudioRequest.bActivate = false;
      m_oAltAudioRequestLock.vUnlock();
   }
   return true;
}

/***************************************************************************
 ** FUNCTION:  tVoid devprj_tclService::vOnError(const tU8, iil_tenISourceError..)
 ***************************************************************************/
tVoid devprj_tclService::vOnError(const tU8 cu8SrcNum, iil_tenISourceError enError)
{
   tenAudioError enAudiError = e8_AUDIOERROR_UNKNOWN;

   m_oCarPlayMediaStatusLock.s16Lock();
   t_Bool bIsCarPlayMediaActive = m_bCarPlayMediaActive;
   m_oCarPlayMediaStatusLock.vUnlock();

   t_Bool bProcessError = ((false == bIsCarPlayMediaActive) || ((true == bIsCarPlayMediaActive) && (scu8AudMainSrcNum != cu8SrcNum)));
   ETG_TRACE_USR2(("devprj_tclService::vOnError() entered: SrcNum %d, IIL SrcError = %d, bCarPlayMediaActive = %d ",
            cu8SrcNum, enError, ETG_ENUM(BOOL, bIsCarPlayMediaActive)));

   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      switch (enError)
      {
         case IIL_EN_ISRC_REQAVACT_ERR:
         {
            enAudiError = e8_AUDIOERROR_AVACTIVATION;
            //! If req for activating Duck channel results into an error
            m_oAudioDuckLock.s16Lock();
            sbAudDuckEnabled = (su8AudDuckSrcNum == cu8SrcNum)?(false):(sbAudDuckEnabled);
            m_oAudioDuckLock.vUnlock();
         }
            break;
         case IIL_EN_ISRC_REQAVDEACT_ERR:
         {
            enAudiError = e8_AUDIOERROR_AVDEACTIVATION;
         }
            break;
         case IIL_EN_ISRC_ALLOC_ERR:
         {
            enAudiError = e8_AUDIOERROR_ALLOCATE;
         }
            break;
         case IIL_EN_ISRC_DEALLOC_ERR:
         {
            enAudiError = e8_AUDIOERROR_DEALLOCATE;
         }
            break;
         case IIL_EN_ISRC_SRCACT_ON_ERR:
         {
            enAudiError = e8_AUDIOERROR_STARTSOURCEACT;
         }
            break;
         case IIL_EN_ISRC_SRCACT_OFF_ERR:
         {
            enAudiError = e8_AUDIOERROR_STOPSOURCEACT;
         }
            break;
         case IIL_EN_ISRC_AVDEACT_SEQERR:
         {
            //! @Note: This error is received when AVDeactivationResult is received before
            //! SourceActivityOFF and Deallocate (usually seen with Delphi Tuners)
            ETG_TRACE_USR4(("Triggering Internal Audio Deactivation"));
            m_poSpiCmdInterface->vOnStopSourceActivity(cu8SrcNum);
            m_poSpiCmdInterface->vOnRouteDeAllocateResult(cu8SrcNum);
         }
            break;
         default:
         {
            enAudiError = e8_AUDIOERROR_GENERIC;
         }
            break;
      }//switch(enError)

      //! Store main audio source as inactive in case of activation errors (GMMY17-6630, GMMY17-10158 - AA VR icon hanging issue)
      t_Bool bIsSourceInactive = ((e8_AUDIOERROR_AVACTIVATION == enAudiError) ||
            (e8_AUDIOERROR_ALLOCATE == enAudiError) ||  (e8_AUDIOERROR_STARTSOURCEACT == enAudiError));
      if (true == bIsSourceInactive)
      {
         m_oSpeechChnReqLock.s16Lock();
         m_bSpeechChnRequested = (scu8AudSpeechSrcNum == cu8SrcNum)? (false) : m_bSpeechChnRequested;
         t_Bool bIsSpeechChnRequested = m_bSpeechChnRequested;
         m_oSpeechChnReqLock.vUnlock();

         m_poSpiCmdInterface->vOnStopSourceActivity(cu8SrcNum);

         //! Set audio context as invalid (will be set to correct state once BaseChannelStatus update is received)
         //! Note: SPI should take/borrow audio focus to MD asap, to stop it from streaming music.
         if ((scu8AudMainSrcNum == cu8SrcNum) && (e8SPI_SPEECH_SPEAKING != senDeviceSpeechAppState) && (false == bIsSpeechChnRequested))
         {
            m_poSpiCmdInterface->vSetAccessoryAudioContext(m_poSpiCmdInterface->u32GetSelectedDeviceHandle(),
                  e8SPI_AUDIO_UNKNOWN, true, corEmptyUsrContext, e8DEV_TYPE_ANDROIDAUTO);
         }
      }//if (true == bIsSourceInactive)

      if ((IIL_EN_ISRC_AVDEACT_SEQERR != enError) && (su8AudDuckSrcNum != cu8SrcNum) && (true == bProcessError))
      {
         ETG_TRACE_USR4(("devprj_tclService::vOnError: Audio Error for Source Number %d Error = %d",
                  cu8SrcNum, ETG_ENUM(AUDIO_ERROR, enAudiError)));
         m_poSpiCmdInterface->vOnAudioError(cu8SrcNum,enAudiError);
      }//if ((IIL_EN_ISRC_AVDEACT_SEQERR != enError) &&...)

   }//if (OSAL_NULL != m_poSpiCmdInterface)
}// devprj_tclService::vOnError(tU8 u8SrcNum,arl_tenISourceError enError)

/***************************************************************************
** FUNCTION:  virtual tBool devprj_tclService::bOnReqAVDeActResult(const tU8 cu8Sr..
***************************************************************************/
tBool devprj_tclService::bOnReqAVDeActResult(const tU8 cu8SrcNum)
{
   ETG_TRACE_USR1(("devprj_tclService::bOnReqAVDeActResult Src Number = %d", cu8SrcNum));
   if((cu8SrcNum == m_rAltAudioStatus.u8SrcNumber) && (e8_AUDIO_DEACTIVATING == m_rAltAudioStatus.enCurrAudioState))
   {
      //! Set the current alternate audio state to idle if result is received
      m_oAltAudioStatusLock.s16Lock();
      m_rAltAudioStatus.u8SrcNumber = cu8SrcNum;
      m_rAltAudioStatus.enCurrAudioState = e8_AUDIO_IDLE;
      m_oAltAudioStatusLock.vUnlock();
   }
   if(cu8SrcNum == m_rAltAudioRequest.u8SourceNumber)
   {
      //! Process the pending request
      bQueueAlternateAudioReq(m_rAltAudioRequest.u8SourceNumber, m_rAltAudioRequest.bActivate);
      m_oAltAudioRequestLock.s16Lock();
      m_rAltAudioRequest.u8SourceNumber = 0;
      m_rAltAudioRequest.bActivate = false;
      m_oAltAudioRequestLock.vUnlock();
   }
   return TRUE;
}


/***************************************************************************
 ** FUNCTION:  t_Void devprj_tclService::vRestoreLastMediaAudSrc()
 ***************************************************************************/
t_Void devprj_tclService::vRestoreLastMediaAudSrc()
{
   ETG_TRACE_USR1(("devprj_tclService::vOnRestoreLastAudioSource entered "));

   //! Clear audio device name since we switch out of CarPlay media
   m_oCarPlayMediaAudDeviceLock.s16Lock();
   m_szCarPlayMediaAudDevice.clear();
   m_oCarPlayMediaAudDeviceLock.vUnlock();

   //!Do not request for activation if FBlock ID and Source number are invalid
   if((0 != m_oAvManLastMainAudioSrc.u8SourceFBlock) && (0 != m_oAvManLastMainAudioSrc.u8SourceNr))
   {
      if (NULL != m_poSpiCmdInterface)
      {
         //! Clear SourceActivity flag for main audio (which will be set if
         //! dummy media prepare was received after Siri/Phone call)
         m_poSpiCmdInterface->vOnStopSourceActivity(scu8AudMainSrcNum);
      }//if (NULL != m_poSpiCmdInterface)

      if (OSAL_NULL != m_poAVManagerClient)
      {
         m_poAVManagerClient->vRequestAVActivation(m_oAvManLastMainAudioSrc);
      }//if (OSAL_NULL != m_poAVManagerClient)
   }//End of if((0 != m_oAvManLastMainAudioSrc.u8SourceFBlock) &&…
}//t_Void devprj_tclService::vOnRestoreLastAudioSource()


/*************************End of ISource methods**************************/


/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vRegisterForProperties()
***************************************************************************/
tVoid devprj_tclService::vRegisterForProperties()
{
   ETG_TRACE_USR1(("devprj_tclService::vRegisterForProperties "));

   if (
      (OSAL_NULL != m_poOnstarCallClient)
      &&
      (OSAL_NULL != m_poOnstarDataClient)
      &&
      (OSAL_NULL != m_poSpeechHMIClient)
      &&
      (OSAL_NULL != m_poNavigationClient)
      )
   {
      //@Note: Telephone FBlock properties are by default registered on start-up.
      //Hence they need not be registered here again.
      m_poOnstarCallClient->vRegisterForProperties();
      m_poOnstarDataClient->vRegisterForProperties();
      m_poSpeechHMIClient->vRegisterForProperties();
      m_poNavigationClient->vRegisterForProperties();
   }//if ((OSAL_NULL != m_poOnstarCallClient)&&..)
}

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vUnregisterForProperties()
***************************************************************************/
tVoid devprj_tclService::vUnregisterForProperties()
{
   ETG_TRACE_USR1(("devprj_tclService::vUnregisterForProperties "));

   if (
      (OSAL_NULL != m_poOnstarCallClient)
      &&
      (OSAL_NULL != m_poOnstarDataClient)
      &&
      (OSAL_NULL != m_poSpeechHMIClient)
      &&
      (OSAL_NULL != m_poNavigationClient)
      )
   {
      //@Note: Telephone FBlock properties are not unregistered here since
      //they are required throughout ignition cycle.
      m_poOnstarCallClient->vUnregisterForProperties();
      m_poOnstarDataClient->vUnregisterForProperties();
      m_poSpeechHMIClient->vUnregisterForProperties();
      m_poNavigationClient->vUnregisterForProperties();
   }//if ((OSAL_NULL != m_poOnstarCallClient)&&..)
}

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vOnCarPlayAudioResult()
***************************************************************************/
t_Void devprj_tclService::vOnCarPlayAudioResult(t_String szAudioDevice,
      tenAudioError enAudioError)
{
   ETG_TRACE_USR1(("devprj_tclService::vOnCarPlayAudioResult() entered: "
         "enAudioError = %d, szAudioDevice = %s ",
         ETG_ENUM(AUDIO_ERROR, enAudioError), szAudioDevice.c_str()));

   //! Mock Source Activity ON and Allocate result
   if (
      (OSAL_NULL != m_poSpiCmdInterface)
      &&
      (e8DEV_TYPE_DIPO == m_poSpiCmdInterface->enGetDeviceCategory(
            m_poSpiCmdInterface->u32GetSelectedDeviceHandle()))
      )
   {
      //@TODO - Source Number hard-coded for CarPlay. To be revised.
      if (e8_AUDIOERROR_NONE == enAudioError)
      {
         //@Note: Audio device name is requested & stored after first media prepare.
         //This is used to prevent sending multiple requests to mediaplayer when 
         //CarPlay sends multiple media preapre messages.
         m_oCarPlayMediaAudDeviceLock.s16Lock();
         m_szCarPlayMediaAudDevice = szAudioDevice;
         m_oCarPlayMediaAudDeviceLock.vUnlock();

         trAudSrcInfo rSrcInfo;
         rSrcInfo.rMainAudDevNames.szOutputDev.assign(szAudioDevice);
         m_poSpiCmdInterface->bOnRouteAllocateResult(scu8AudMainSrcNum, rSrcInfo);
         m_poSpiCmdInterface->vOnStartSourceActivity(scu8AudMainSrcNum);
      }
      else
      {
         m_oCarPlayMediaAudDeviceLock.s16Lock();
         m_szCarPlayMediaAudDevice.clear();
         m_oCarPlayMediaAudDeviceLock.vUnlock();
         m_poSpiCmdInterface->vOnAudioError(scu8AudMainSrcNum, enAudioError);
      }
   }//if ((OSAL_NULL != m_poSpiCmdInterface) && ...)
}

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnSRStatus(tenSpeechRecStatus...)
***************************************************************************/
tVoid devprj_tclService::vOnSRStatus(tenSpeechRecStatus enSRStatus)
{
   ETG_TRACE_USR1(("devprj_tclService::vOnSRStatus entered %d ",
         ETG_ENUM(SR_STATUS, enSRStatus)));
   if(OSAL_NULL!= m_poKeyConfig)
   {
      m_poKeyConfig ->vSetSpeechRecStatus(enSRStatus);
   }
   trUserContext rUserContext; //Dummy
   switch(enSRStatus)
   {
   case e8SR_ACTIVE:
      {
         m_bNativeSpeechActive = true;
      }
      break;
   case e8SR_SUSPEND:
   case e8SR_INACTIVE:
   case e8SR_INITIALIZING:
      {
         m_bNativeSpeechActive = false;
      }
      break;
   default:
      {
         ETG_TRACE_USR1(("Invalid case in vOnSRStatus"));
      }
   } //switch(enSRStatus)
   if(OSAL_NULL != m_poSpiCmdInterface)
   {
      m_enSpeechAppState = (m_bOnstarSpeechState || m_bNativeSpeechActive) ? e8SPI_SPEECH_RECOGNIZING : e8SPI_SPEECH_END; 
      m_poSpiCmdInterface->vSetAccessoryAppState(m_enSpeechAppState, 
         m_enPhoneAppState, m_enNavAppState, rUserContext);
   }//if(OSAL_NULL != m_poSpiCmdInterface)
   //!Add code
}

#ifdef VARIANT_S_FTR_ENABLE_GM_CARPLAY_MEDIA
/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vOnBaseChannelStatusUpdate
***************************************************************************/
t_Void devprj_tclService::vOnBaseChannelStatusUpdate(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("devprj_tclService::vOnBaseChannelStatusUpdate entered "));
   devprj_tBaseChannelStatus oBaseChannelMsg(*poMessage);

   if ((true == oBaseChannelMsg.bIsValid()) && (NULL != m_poSpiCmdInterface))
   {
      //! Process base channel update for DiPO
      tenAudioContext enDiPOAudioCntxt;

      for (t_U8 u8Index = 0; u8Index < oBaseChannelMsg.oBaseStatus.oItems.size(); ++u8Index)
      {
         // Check for Active Logical channel in list of Audio Channels.
         if (AVMAN_CHN_STATUS_ACTIVE == oBaseChannelMsg.oBaseStatus.oItems[u8Index].e8ChannelStatus.enType)
         {
            // Check if the channel type is relevant
        t_Bool bRet = bGetSPIAudioContext(oBaseChannelMsg.oBaseStatus.oItems[u8Index].e8LogicalAVChannel.enType,
                                             enDiPOAudioCntxt);
            if (TRUE == bRet)
            {
               m_oCarPlayMediaStatusLock.s16Lock();
               t_Bool bIsCarPlayMediaActive = m_bCarPlayMediaActive;
               m_oCarPlayMediaStatusLock.vUnlock();

               // Store last non-CarPlay main audio source info
               //@Note: Workaround for apple bug - dummy audio prepare sent after phone call or Siri.
               if (
                   (AVMAN_CHN_LCMAIN == oBaseChannelMsg.oBaseStatus.oItems[u8Index].e8LogicalAVChannel.enType)
                   &&
                   ((MPLAYER_FBLOCK_ID != oBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock)
                   ||
                   ((MPLAYER_FBLOCK_ID == oBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock)
                         && (false == bIsCarPlayMediaActive)))
                   )
               {
                  m_oAvManLastMainAudioSrc = oBaseChannelMsg.oBaseStatus.oItems[u8Index];
                  ETG_TRACE_USR4(("Stored Source: FBlockID %d, Src Number %d , Src Instance %d, Logical Channel %d",
                           m_oAvManLastMainAudioSrc.u8SourceFBlock, m_oAvManLastMainAudioSrc.u8SourceNr, m_oAvManLastMainAudioSrc.u8SourceInstID,
                           m_oAvManLastMainAudioSrc.e8LogicalAVChannel.enType));
               }

               // Evaluate for change in Audio Context. Active channel Type or FBlock owning active channel must have changed.
               if (
                  ((AVMAN_CHN_NONE != m_CurrAllocatedChannel)
                  &&
                  (oBaseChannelMsg.oBaseStatus.oItems[u8Index].e8LogicalAVChannel.enType != m_CurrAllocatedChannel))
                  ||
                  (oBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock != m_u8AudioSourceFBlock)
                  )
               {
                  t_Bool bAudioRequest = true;

                  /* If the Audio resource is allocated to Device Projection or MediaPlayer FBlock (when CarPlay media is active),
                     send the Audio channel release information with the last context.
                     If the Channel is already with Device Projection Fblock, Ignore update */
                  if (
                     (DEVPRJ_FBLOCK_ID == oBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock)
                     ||
                     ((MPLAYER_FBLOCK_ID == oBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock)
                              && (true == bIsCarPlayMediaActive))
                     )
                  {
                     bAudioRequest = false;
                     bRet = bGetSPIAudioContext(m_CurrAllocatedChannel, enDiPOAudioCntxt);
                     m_CurrAllocatedChannel = most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_NONE;
                  } //if(DEVPRJ_FBLOCK_ID == oBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock)
                  else
                  {
                     /* If the channel is allocated to accessory, set the current audio accessory context */
                     m_CurrAllocatedChannel = oBaseChannelMsg.oBaseStatus.oItems[u8Index].e8LogicalAVChannel.enType;
                  }// end of if-else..if(DEVPRJ_FBLOCK_ID == oBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock)

                  m_u8AudioSourceFBlock = oBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock;

                  if (true == bRet)
                  {
                     ETG_TRACE_USR4(("Base Channel Status Update: Audio Context: AccessoryRequest[%d], AudioContext[%d], FBlockID[%d]",
                            ETG_ENUM(BOOL, bAudioRequest),ETG_ENUM(AUDIO_CONTEXT, enDiPOAudioCntxt),
                            m_u8AudioSourceFBlock));
                     t_U32 u32DeviceID = m_poSpiCmdInterface->u32GetSelectedDeviceHandle();
                     if(true == bAudioRequest)
                     {
					     // ! Send the audio context with true flag and also add the same to stack.
                  		 m_oCarPlayMediaStatusLock.s16Lock();
                    	 m_enAudioCntxtUpdateStack.push(enDiPOAudioCntxt);
                    	 m_poSpiCmdInterface->vSetAccessoryAudioContext(u32DeviceID,enDiPOAudioCntxt, bAudioRequest, corEmptyUsrContext,e8DEV_TYPE_DIPO);
                  		 m_oCarPlayMediaStatusLock.vUnlock();
                     }
                     else
                     {
					    //!Free up the stack and send all the context in reverse order of the context.
                  		m_oCarPlayMediaStatusLock.s16Lock();
                    	while(!m_enAudioCntxtUpdateStack.empty())
                    	{
                    	   m_poSpiCmdInterface->vSetAccessoryAudioContext(u32DeviceID,m_enAudioCntxtUpdateStack.top(), bAudioRequest, corEmptyUsrContext,e8DEV_TYPE_DIPO);
                    	   m_enAudioCntxtUpdateStack.pop();
                    	}
                  		m_oCarPlayMediaStatusLock.vUnlock();
                     }
                  }
               } // End of if((oBaseChannelMsg.oBaseStatus.oItems[u8Index].e8LogicalAVChannel.enType != m_CurrAllocatedChannel)...
               break;
            } // End of if((TRUE == bRet) && (OSAL_NULL != m_poSpiCmdInterface))
         } // End of if(( most_fi_tcl_e8_AVManChannelStatus::FI_EN_E8CS_ACTIVE == ...)
      }// End of for(t_U8 u8Index = 0; u8Index < oBaseChannelMsg.oBaseStatus.oItems.size() ; ++u8Index)

      //! Process base channel update for AAP session
      vUpdateAAPAudioContext(oBaseChannelMsg);

   }//if ((true == oBaseChannelMsg.bIsValid()) && (NULL != m_poSpiCmdInterface))
}// End of t_Void devprj_tclService::vOnBaseChannelStatusUpdate(amt_tclServiceData* poMessage)

#else
/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vOnBaseChannelStatusUpdate
***************************************************************************/
t_Void devprj_tclService::vOnBaseChannelStatusUpdate(amt_tclServiceData* poMessage)
{
    ETG_TRACE_USR1(("devprj_tclService::vOnBaseChannelStatusUpdate entered "));
    devprj_tBaseChannelStatus oBaseChannelMsg(*poMessage);

    if (true == oBaseChannelMsg.bIsValid())
    {
       tenAudioContext enDiPOAudioCntxt;
       tBool bUpdateReqd = FALSE;

       for(t_U8 u8Index = 0; u8Index < oBaseChannelMsg.oBaseStatus.oItems.size() ; ++u8Index)
       {
           // Check for Active Logical channel in list of Audio Channels.
           if(( most_fi_tcl_e8_AVManChannelStatus::FI_EN_E8CS_ACTIVE ==
               oBaseChannelMsg.oBaseStatus.oItems[u8Index].e8ChannelStatus.enType))
           {
               //! Store last non-CarPlay main audio source info
               //! @Note: Workaround for apple bug - dummy audio prepare sent after phone call or Siri.
               if((DEVPRJ_FBLOCK_ID != oBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock)
                       &&
                  (AVMAN_CHN_LCMAIN == oBaseChannelMsg.oBaseStatus.oItems[u8Index].e8LogicalAVChannel.enType))
               {
                  m_oAvManLastMainAudioSrc = oBaseChannelMsg.oBaseStatus.oItems[u8Index];
                  ETG_TRACE_USR4(("Stored Source: FBlockID %d, Src Number %d , Src Instance %d, Logical Channel %d",
                           m_oAvManLastMainAudioSrc.u8SourceFBlock, m_oAvManLastMainAudioSrc.u8SourceNr, m_oAvManLastMainAudioSrc.u8SourceInstID,
                           m_oAvManLastMainAudioSrc.e8LogicalAVChannel.enType));
               }

               // Check if the channel type is relevant
               t_Bool bRet = bGetSPIAudioContext(oBaseChannelMsg.oBaseStatus.oItems[u8Index].e8LogicalAVChannel.enType,
                   enDiPOAudioCntxt);
               if((TRUE == bRet) && (OSAL_NULL != m_poSpiCmdInterface))
               {
                   // Evaluate for change in Audio Context. Active channel Type or FBlock owning active channel must have changed.
                   if((oBaseChannelMsg.oBaseStatus.oItems[u8Index].e8LogicalAVChannel.enType != m_CurrAllocatedChannel)
                       || (oBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock != m_u8AudioSourceFBlock))
                   {
                       /* If the Audio resource is allocated to Device Projection, send the Audio channel release
                          information with the last context.
                          If the Channel is already with Device Projection Fblock, Ignore update */
                       if((DEVPRJ_FBLOCK_ID == oBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock)&&
                           (most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_NONE != m_CurrAllocatedChannel))
                       {
                           bGetSPIAudioContext(m_CurrAllocatedChannel,enDiPOAudioCntxt);
                           m_CurrAllocatedChannel = most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_NONE;
                           bUpdateReqd = TRUE;
                       } //if(DEVPRJ_FBLOCK_ID == oBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock)

                       /* If the Audio resource is allocated to any FBlock other than Device Projection */
                       else if (DEVPRJ_FBLOCK_ID != oBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock)
                       {
                           /* If the channel is allocated to accessory, set the current audio accessory context */
                           m_CurrAllocatedChannel = oBaseChannelMsg.oBaseStatus.oItems[u8Index].e8LogicalAVChannel.enType;
                           bUpdateReqd = TRUE;
                       }// end of if-else..if(DEVPRJ_FBLOCK_ID == oBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock)


                       //Check if the channel is assigned to Device Projection FBlock
                       if(TRUE == bUpdateReqd)
                       {
                           m_u8AudioSourceFBlock = oBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock;
                           t_Bool bAudioRequest = (DEVPRJ_FBLOCK_ID != oBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock);
                           t_U32 u32DeviceID = m_poSpiCmdInterface->u32GetSelectedDeviceHandle();

                           ETG_TRACE_USR4(("Base Channel Status Update: Audio Context: AccessoryRequest[%d], AudioContext[%d], FBlockID[%d]",
                               ETG_ENUM(BOOL, bAudioRequest),ETG_ENUM(AUDIO_CONTEXT, enDiPOAudioCntxt),
                               m_u8AudioSourceFBlock));

                           m_poSpiCmdInterface->vSetAccessoryAudioContext(u32DeviceID,enDiPOAudioCntxt, bAudioRequest, corEmptyUsrContext,e8DEV_TYPE_DIPO);
                       }
                   } // End of if((oBaseChannelMsg.oBaseStatus.oItems[u8Index].e8LogicalAVChannel.enType != m_CurrAllocatedChannel)...
                   break;
               } // End of if((TRUE == bRet) && (OSAL_NULL != m_poSpiCmdInterface))
           } // End of if(( most_fi_tcl_e8_AVManChannelStatus::FI_EN_E8CS_ACTIVE == ...)
       }// End of for(t_U8 u8Index = 0; u8Index < oBaseChannelMsg.oBaseStatus.oItems.size() ; ++u8Index)

       //! Process base channel update for AAP session
       vUpdateAAPAudioContext(oBaseChannelMsg);
    }//if (true == oBaseChannelMsg.bIsValid())
}// End of t_Void devprj_tclService::vOnBaseChannelStatusUpdate(amt_tclServiceData* poMessage)
#endif

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vUpdateAAPAudioContext(...)
***************************************************************************/
t_Void devprj_tclService::vUpdateAAPAudioContext(
      const devprj_tBaseChannelStatus& rfcoBaseChannelMsg)
{
   if (NULL != m_poSpiCmdInterface)
   {
      t_U32 u32DeviceID = m_poSpiCmdInterface->u32GetSelectedDeviceHandle();

      //! Flag to indicate whether native Navigation alert is playing
      t_Bool bIsNativeNavAlertPlaying = false;

      //! Initialise flag to false, since "m_bIsDevPrjMainAudioActive" flag should be cleared when there is no channel active
      t_Bool bIsDevPrjMainAudioActive = false;
      t_Bool bIsDevPrjMainAudioSuspended = false;

      //! Flag to indicate whether any audio channel is currently active
      t_Bool bIsAnyAudioChannelActive = false;

      for (t_U8 u8Index = 0; u8Index < rfcoBaseChannelMsg.oBaseStatus.oItems.size(); ++u8Index)
      {
         most_fi_tcl_e8_AVManLogicalAVChannel::tenType enAudioChannel =
               rfcoBaseChannelMsg.oBaseStatus.oItems[u8Index].e8LogicalAVChannel.enType;
         most_fi_tcl_e8_AVManChannelStatus::tenType enChannelStatus =
               rfcoBaseChannelMsg.oBaseStatus.oItems[u8Index].e8ChannelStatus.enType;
         tU8 u8AudioSourceFBlock = rfcoBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock;

         //! Print status of channels if it is not inactive
         if (AVMAN_CHN_STATUS_INACTIVE != enChannelStatus)
         {
            ETG_TRACE_USR4(("vUpdateAAPAudioContext: AudioChannel %d, ChannelStatus %d, SourceFBlock %d ",
                  enAudioChannel, enChannelStatus, u8AudioSourceFBlock));
         }//if (AVMAN_CHN_STATUS_INACTIVE != enChannelStatus)

         if (AVMAN_CHN_STATUS_ACTIVE == enChannelStatus)
         {
            bIsAnyAudioChannelActive = true;
         }//if (AVMAN_CHN_STATUS_ACTIVE == enChannelStatus)

         switch (enAudioChannel)
         {
            case most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_MAIN_AUDIO:
            {
               //@Note: This flag is used to prevent triggering main audio activation when DeviceProjection source
               //is already in the process of being activated by AVManager. (example, AAP media restoration after native VR)
               bIsDevPrjMainAudioActive = (DEVPRJ_FBLOCK_ID == u8AudioSourceFBlock) && (AVMAN_CHN_STATUS_ACTIVE == enChannelStatus);
               bIsDevPrjMainAudioSuspended = (DEVPRJ_FBLOCK_ID == u8AudioSourceFBlock) && (AVMAN_CHN_STATUS_SUSPENDED == enChannelStatus);
            }
            break;
            case most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_MIX_ALERT_MSG:
            {
               if ((AVMAN_CHN_STATUS_ACTIVE == enChannelStatus) && (DEVPRJ_FBLOCK_ID != u8AudioSourceFBlock))
               {
                  m_poSpiCmdInterface->vSetAccessoryAudioContext(u32DeviceID, e8SPI_AUDIO_MIX_ALERT_MSG, true,
                        corEmptyUsrContext, e8DEV_TYPE_ANDROIDAUTO);
                  bIsNativeNavAlertPlaying = true;
               }
            }
            break;
            case most_fi_tcl_e8_AVManLogicalAVChannel::FI_EN_E8LC_INTERNET_APP_AUDIO:
            {
               //!Fix for Internet Audio and AAP audio playing together.
               //!In this case LC_MAIN goes to suspended state instead of INACTIVE when LC_INTERNET_APP_AUDIO is activated.
               //!Therefore need to clear the bIsDevPrjMainAudioSuspended flag, so that device proj requests for activation of LC_MAIN.
               bIsDevPrjMainAudioSuspended = (AVMAN_CHN_STATUS_ACTIVE == enChannelStatus) ?
                     (false) : (bIsDevPrjMainAudioSuspended);
            }
            break;
            default:
            {
               //! Nothing to do
            }
               break;
         }//switch (enAudioChannel)
      }//End of for loop

      ETG_TRACE_USR3(("vUpdateAAPAudioContext: DeviceProjection main audio channel status: Active %d, Suspended %d ",
            ETG_ENUM(BOOL, bIsDevPrjMainAudioActive), ETG_ENUM(BOOL, bIsDevPrjMainAudioSuspended)));

      m_oDevPrjMainAudioStatusLock.s16Lock();
      m_bIsDevPrjMainAudioActive = bIsDevPrjMainAudioActive;
      m_bIsDevPrjMainAudioSuspended = bIsDevPrjMainAudioSuspended;
      m_oDevPrjMainAudioStatusLock.vUnlock();

      //! Reset audio context if there is no audio channel active. (GMMY17-6634)
      if (false == bIsAnyAudioChannelActive)
      {
         ETG_TRACE_USR3(("vUpdateAAPAudioContext: Clearing audio context since there is no audio channel active "));
         m_poSpiCmdInterface->vSetAccessoryAudioContext(u32DeviceID, e8SPI_AUDIO_NONE, true,
               corEmptyUsrContext, e8DEV_TYPE_ANDROIDAUTO);
      }
      //! Validate other channels & update audio context only if Native Navigation alert is not playing
      else if (false == bIsNativeNavAlertPlaying)
      {
         for (t_U8 u8Index = 0; u8Index < rfcoBaseChannelMsg.oBaseStatus.oItems.size(); ++u8Index)
         {
            // Check for Active Logical channel in list of Audio Channels.
            if (AVMAN_CHN_STATUS_ACTIVE == rfcoBaseChannelMsg.oBaseStatus.oItems[u8Index].e8ChannelStatus.enType)
            {
               most_fi_tcl_e8_AVManLogicalAVChannel::tenType enAAPCurrAllocatedChannel =
                     rfcoBaseChannelMsg.oBaseStatus.oItems[u8Index].e8LogicalAVChannel.enType;
               tU8 u8AAPAudioSourceFBlock = rfcoBaseChannelMsg.oBaseStatus.oItems[u8Index].u8SourceFBlock;

               tenAudioContext enCurAudioCntxt = e8SPI_AUDIO_UNKNOWN;
               t_Bool bRet = bGetSPIAudioContext(enAAPCurrAllocatedChannel, enCurAudioCntxt);
               t_Bool bAudioRequest = (DEVPRJ_FBLOCK_ID != u8AAPAudioSourceFBlock);

               if (true == bRet)
               {
                  ETG_TRACE_USR3(("vUpdateAAPAudioContext: AccessoryRequest[%d], AudioContext[%d], FBlockID[%d]",
                         ETG_ENUM(BOOL, bAudioRequest),ETG_ENUM(AUDIO_CONTEXT, enCurAudioCntxt), u8AAPAudioSourceFBlock));
                  m_poSpiCmdInterface->vSetAccessoryAudioContext(u32DeviceID,enCurAudioCntxt, bAudioRequest,
                        corEmptyUsrContext, e8DEV_TYPE_ANDROIDAUTO);
                  break;
               }
            }//if (AVMAN_CHN_STATUS_ACTIVE == ...)
         }//End of for loop
      }//else if (false == bIsNativeNavAlertPlaying)

   }//if (NULL != m_poSpiCmdInterface)
}

/***************************************************************************
** FUNCTION:  t_Bool devprj_tclService::u32GetNotificationActionID(devprj_tFiBtnAction...
***************************************************************************/
t_U32 devprj_tclService::u32GetNotificationActionID(devprj_tFiBtnAction oFiBtnAction,
      const trNotiData& rfcorNotiData) const
{
   t_U32 u32NotiActionID = 0;

   switch (oFiBtnAction.enType)
   {
      case DEVPRJ_ACTION_1:
         if (1 <= rfcorNotiData.tvecNotiActionList.size())
         {
            u32NotiActionID = rfcorNotiData.tvecNotiActionList[0].u32NotiActionID;
         }
         break;
      case DEVPRJ_ACTION_2:
         if (2 <= rfcorNotiData.tvecNotiActionList.size())
         {
            u32NotiActionID = rfcorNotiData.tvecNotiActionList[1].u32NotiActionID;
         }
         break;
      case DEVPRJ_ACTION_3:
         if (3 <= rfcorNotiData.tvecNotiActionList.size())
         {
            u32NotiActionID = rfcorNotiData.tvecNotiActionList[2].u32NotiActionID;
         }
         break;
      default:
      {
         ETG_TRACE_ERR((" vSendNotificationAction: Invalid action %d! ",
               ETG_ENUM(DEVPRJ_BTN_ACTION, oFiBtnAction.enType)));
      }
         break;
   }//switch (oFiBtnAction.enType)

   ETG_TRACE_USR3(("devprj_tclService::u32GetNotificationActionID: left with u32NotiActionID = %d ",
         u32NotiActionID));

   return u32NotiActionID;
}

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vOnMuteStatus(amt_tclServiceData* ..).
***************************************************************************/
t_Void devprj_tclService::vOnMuteStatus(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("devprj_tclService::vOnMuteStatus entered "));
   devprj_tMuteStatus oMsgMuteStatus(*poMessage);

   m_bMuteFlag = oMsgMuteStatus.bMuteFlag;

   ETG_TRACE_USR4(("Mute Status : %d ",m_bMuteFlag));
}

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vOnSetMute(t_Bool bMuteState).
***************************************************************************/
t_Void devprj_tclService::vOnSetMute(t_Bool bMuteState)
{
   ETG_TRACE_USR4(("devprj_tclService::vOnSetMute Entered"));

      //devprj_tMutePureSet oMsgMuteSet;
   iil_tMutePureSet oMsgMuteSet;


   oMsgMuteSet.bMuteFlag = bMuteState;

   //Check if AV Manager service is available and then set the Mute state
   if( bIfServiceAvailable())
   {
      if (bOnSetMuteState(oMsgMuteSet) == FALSE)
      {
         ETG_TRACE_USR4(("devprj_tclService::\
                         vOnSetMute() : Unable to post message."));
      }
   }
   else
   {
      ETG_TRACE_ERR(("devprj_tclService::\
                      vOnSetMute() : Reqd service unavailable"));
   }
   oMsgMuteSet.vDestroy();  
}

/***************************************************************************
 ** FUNCTION:  t_Void devprj_tclService::vSendAudioStatusChange(...)
 ***************************************************************************/
t_Void devprj_tclService::vSendAudioStatusChange(tenAudioStatus enAudioStatus)
{
#ifdef VARIANT_S_FTR_ENABLE_GM_CARPLAY_MEDIA

   ETG_TRACE_USR1(("devprj_tclService::vSendAudioStatusChange() entered: enAudioStatus = %d ", ETG_ENUM(AUDIO_STATUS, enAudioStatus)));

   //! Set flag since audio prepare/teardown message is received for CarPlay Media
   m_oCarPlayMediaStatusLock.s16Lock();
   m_bCarPlayMediaActive = (e8AUDIO_STATUS_MEDIA_SETUP == enAudioStatus);
   m_oCarPlayMediaStatusLock.vUnlock();

   //! Process audio status if a device is active (currently required only for DiPO)
   if (OSAL_NULL != m_poSpiCmdInterface)
      /*&&
      (e8DEV_TYPE_DIPO == m_poSpiCmdInterface->enGetDeviceCategory(
            m_poSpiCmdInterface->u32GetSelectedDeviceHandle()))
      )*/
   {
      switch (enAudioStatus)
      {
         case e8AUDIO_STATUS_MEDIA_SETUP:
         {
            //!If audio device name is already received (from first media prepare), do not request it again.
            //@Note: In cases where SPI needs to restore last audio source (i.e. after Siri/Phone call), 
            //device name should not be requested more than once (since mediaplayer immediately starts music playback).
            m_oCarPlayMediaAudDeviceLock.s16Lock();
            t_String szCarPlayAudioDevName = m_szCarPlayMediaAudDevice;
            m_oCarPlayMediaAudDeviceLock.vUnlock();

            if ((true == szCarPlayAudioDevName.empty()) && (OSAL_NULL != m_poMainAppl))
            {
               spi_tclMPlayClientHandler* poMPlayClient = spi_tclMPlayClientHandler::getInstance(m_poMainAppl);
               if (OSAL_NULL != poMPlayClient)
               {
                  poMPlayClient->bRequestAudioDevice(m_poSpiCmdInterface->u32GetSelectedDeviceHandle());
               }
            }//if (OSAL_NULL != m_poMainAppl)
         }//case e8AUDIO_STATUS_SETUP:
            break;
         case e8AUDIO_STATUS_MEDIA_TEARDOWN:
         {
            //! Send StopMediaPlayback result, if it was triggered.
            if (corEmptyUsrContext != rStopMediaPlaybackCtxt)
            {
               devprj_tMRStopMediaPb oStopMediaPbMR;
               oStopMediaPbMR.u8DeviceTag = m_poSpiCmdInterface->u32GetSelectedDeviceHandle();
               if (FALSE == bPostResponse(oStopMediaPbMR, rStopMediaPlaybackCtxt))
               {
                  ETG_TRACE_ERR((" vSendAudioStatusChange: StopMediaPlayback MR post failed! "));
               }
               //! Clear context
               rStopMediaPlaybackCtxt = corEmptyUsrContext;
            }//if (corEmptyUsrContext != rStopMediaPlaybackCtxt)
         }//case e8AUDIO_STATUS_TEARDOWN:
            break;
         case e8AUDIO_STATUS_MEDIA_RELEASE:
         {
            //! Clear audio device name (this cleanup is done in case Audio
            //! restoration logic has failed due to delayed mode change msg
            //! and/or audio source change has occurred without StopMediaPlayback)
            m_oCarPlayMediaAudDeviceLock.s16Lock();
            m_szCarPlayMediaAudDevice.clear();
            m_oCarPlayMediaAudDeviceLock.vUnlock();
            //! Clear SourceActivity flag for main audio
            m_poSpiCmdInterface->vOnStopSourceActivity(scu8AudMainSrcNum);
         }//case e8AUDIO_STATUS_MEDIA_RELEASE:
            break;
         case e8AUDIO_STATUS_RELEASE_AUDIO_DEVICE:
         {
            if (OSAL_NULL != m_poMainAppl)
            {
               spi_tclMPlayClientHandler* poMPlayClient = spi_tclMPlayClientHandler::getInstance(m_poMainAppl);
               if (OSAL_NULL != poMPlayClient)
               {
                  poMPlayClient->bReleaseAudioDevice(m_poSpiCmdInterface->u32GetSelectedDeviceHandle());
               }
            }//if (OSAL_NULL != m_poMainAppl)
         }//case e8AUDIO_STATUS_RELEASE_AUDIO_DEVICE:
            break;
         default:
            ETG_TRACE_ERR((" vSendAudioStatusChange: Invalid audio status! "));
            break;
      }//switch (enAudioStatus)
   }//if (OSAL_NULL != m_poSpiCmdInterface)
   
#endif
}

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vPostSubscribeForLocData(t_Bool bSubscribe, 
                               tenLocationDataType enLocDataType).
***************************************************************************/
t_Void devprj_tclService::vPostSubscribeForLocData(t_Bool bSubscribe, tenLocationDataType enLocDataType)
{
   ETG_TRACE_USR1(("devprj_tclService::vPostSubscribeForLocData() entered"));

   t_U16 u16SubscribeMode = 0;
   u16SubscribeMode  = (t_U16)bSubscribe;
   u16SubscribeMode = u16SubscribeMode << 8; // Set the first 1 byte as a flag value
   u16SubscribeMode = u16SubscribeMode | (t_U16)enLocDataType; // Set the second byte as status value/

   if(OSAL_NULL != m_poMainAppl)
   {
      // the subscription message is posted as a loop back message to SPI service.
      // This is to avoid the register/unregister of properties triggered from different
      // thread context other than SPI CCA context.
      tLbDataServiceSubscribeMsg oSSubscribeMsg(
            m_poMainAppl->u16GetAppId(), // Source AppID
            m_poMainAppl->u16GetAppId(), // Target AppID
              0, // RegisterID
              0, // CmdCounter
              CCA_C_U16_SRV_FB_DEVICEPROJECTION,
              SPI_C_U16_IFID_DATA_SERVICE_SUBSCRIBE,
              AMT_C_U8_CCAMSG_OPCODE_STATUS);

      oSSubscribeMsg.vSetWord(u16SubscribeMode);

      //! Post loopback message
      if (oSSubscribeMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainAppl->enPostMessage(&oSSubscribeMsg, TRUE))
         {
            ETG_TRACE_ERR(("devprj_tclService::vOnSubscribeForLocData Loopback message posting failed!"));
         }
      } //if (oDayNightModeLbMsg().bIsValid())
      else
      {
         ETG_TRACE_ERR(("devprj_tclService::vOnSubscribeForLocData Loopback message creation failed!"));
      }
   }//End of if(OSAL_NULL != m_poMainAppl)

}

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vRegisterLocData( 
                               tenLocationDataType enLocDataType).
***************************************************************************/
t_Void devprj_tclService::vRegisterLocData( tenLocationDataType enLocDataType)
{
   ETG_TRACE_USR1(("devprj_tclService::vRegisterLocData() entered: enSystemVariant = %d, enGPSConfig = %d ",
         ETG_ENUM(SYSTEM_VARIANT, m_enSystemVariant),
         ETG_ENUM(GMLNGW_GPSCONFIG, m_enGPSConfig)));

   //! Register for vehicle data for all SPI technologies
   if (OSAL_NULL != m_poGMLANClient)
   {
      m_poGMLANClient->vRegisterForVehicleDataProperties(rDataSvcVehicleDataCb);
   }

   tenDeviceCategory enSelDevCat = e8DEV_TYPE_UNKNOWN;
   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      enSelDevCat = m_poSpiCmdInterface->enGetDeviceCategory(m_poSpiCmdInterface->u32GetSelectedDeviceHandle());
   }

   switch (enSelDevCat)
   {
      case e8DEV_TYPE_ANDROIDAUTO:
         if ((e8VARIANT_NAVIGATION == m_enSystemVariant) && (OSAL_NULL!= m_poSensorDataClient))
         {
            m_poSensorDataClient->vRegisterForGyroAccProperty(rDataSvcSensorDataCb);
            m_poSensorDataClient->vRegisterForProperty(rDataSvcSensorDataCb);
         }
         break;
      default:
         {
            //! Register for Location data properties as below:
            //! If system variant(EOL) is NAVIGATION - register for GPS data in Position & Sensor FIs
            //! Else, if GMLANGPSConfig is ONSTAR - register for GPS data in GMLANGAteway
            //! Else, consider as non-Navigation & non-Onstar variant, and register for Vehicle data in GMLANGateway
            if (e8VARIANT_NAVIGATION == m_enSystemVariant)
            {
               if (OSAL_NULL!= m_poPosDataClient)
               {
                  m_poPosDataClient->vRegisterForProperties(enLocDataType, rDataSvcLocDataCb);
               }
               if (OSAL_NULL!= m_poSensorDataClient)
               {
                  m_poSensorDataClient->vRegisterForProperty(rDataSvcSensorDataCb);
               }
            }
            else if ((e8GMLAN_GPS_ONSTAR == m_enGPSConfig) && (OSAL_NULL != m_poGMLANClient))
            {
               m_poGMLANClient->vRegisterForLocDataProperties(rDataSvcLocDataCb,rDataSvcSensorDataCb);
            }
            else if (OSAL_NULL != m_poSystemStateClient)
            {
               m_poSystemStateClient->vRegisterForProperties();
            }
         }
         break;
   }//switch (enSelDevCat)
}

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vUnregisterLocData( 
                               tenLocationDataType enLocDataType).
***************************************************************************/
t_Void devprj_tclService::vUnregisterLocData(tenLocationDataType enLocDataType)
{
   ETG_TRACE_USR1(("devprj_tclService::vUnregisterLocData() entered: enSystemVariant = %d, enGPSConfig = %d ",
         ETG_ENUM(SYSTEM_VARIANT, m_enSystemVariant),
         ETG_ENUM(GMLNGW_GPSCONFIG, m_enGPSConfig)));

   if ((OSAL_NULL != m_poGMLANClient)
      &&
      (OSAL_NULL!= m_poSensorDataClient))
   {
      m_poGMLANClient->vUnregisterForVehicleDataProperties();
      m_poSensorDataClient->vUnregisterForGyroAccProperty();
   }

   //! Unregister for Location data properties as below:
   //! If system variant(EOL) is NAVIGATION - unregister for GPS data in Position & Sensor FIs
   //! Else, if GMLANGPSConfig is ONSTAR - unregister for GPS data in GMLANGAteway
   //! Else, consider as non-Navigation & non-Onstar variant, and unregister for Vehicle data in GMLANGateway
   if (e8VARIANT_NAVIGATION == m_enSystemVariant)
   {
      if (OSAL_NULL != m_poPosDataClient)
      {
         m_poPosDataClient->vUnregisterForProperties(enLocDataType);
      }
      if (OSAL_NULL != m_poSensorDataClient)
      {
         m_poSensorDataClient->vUnregisterForProperty();
      }
   }
   else if ((e8GMLAN_GPS_ONSTAR == m_enGPSConfig) && (OSAL_NULL != m_poGMLANClient))
   {
      m_poGMLANClient->vUnregisterForLocDataProperties();
   }
   else if ((OSAL_NULL != m_poGMLANClient) && (OSAL_NULL != m_poSystemStateClient))
   {
      m_poSystemStateClient->vUnregisterForProperties();
   }
}

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vOnGPSData(trGPSData rGPSData)
***************************************************************************/
t_Void devprj_tclService::vOnGPSData(trGPSData rGpsData)
{
   if (OSAL_NULL!= m_poSpiCmdInterface)
   {
      m_poSpiCmdInterface->vOnGPSData(rGpsData);
   }
}

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vOnSensorData(trSensorData rSensorData)
***************************************************************************/
t_Void devprj_tclService::vOnSensorData(trSensorData rSensorData)
{
   if(OSAL_NULL!= m_poSpiCmdInterface)
   {
      m_poSpiCmdInterface->vOnSensorData(rSensorData);
   }
}

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vOnAccSensorData
** (std::vector<trAccSensorData> vecrAccSensorData)
***************************************************************************/
t_Void devprj_tclService::vOnAccSensorData(const std::vector<trAccSensorData>& corfvecrAccSensorData)
{
   if(OSAL_NULL!= m_poSpiCmdInterface)
   {
      m_poSpiCmdInterface->vOnAccSensorData(corfvecrAccSensorData);
   }
}

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vOnGyroSensorData
** (const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
***************************************************************************/
t_Void devprj_tclService::vOnGyroSensorData(const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
{
   if(OSAL_NULL!= m_poSpiCmdInterface)
   {
      m_poSpiCmdInterface->vOnGyroSensorData(corfvecrGyroSensorData);
   }
}

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vOnGmlanData(trVehicleData rVehicleData)
***************************************************************************/
t_Void devprj_tclService::vOnGmlanData(trVehicleData rVehicleData, t_Bool bSolicited)
{
   if((OSAL_NULL != m_poSystemStateClient) &&(OSAL_NULL!= m_poSpiCmdInterface))
   {
      rVehicleData.PosixTime = m_poSystemStateClient->tGetPosixTime();
      //@Note: Only time data is received from SystemState

      m_poSpiCmdInterface->vOnVehicleData(rVehicleData, bSolicited);
   }
}

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnTelephoneCallActivity()
***************************************************************************/
tVoid devprj_tclService::vOnTelephoneCallActivity(t_Bool bCallActive)
{
   ETG_TRACE_USR4(("devprj_tclService::vOnTelephoneCallActivity() entered: bIsCallActive = %d ",
         ETG_ENUM(BOOL, bCallActive)));
   //! Forward call actiivty to BT Manager & set phone app state
   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      m_poSpiCmdInterface->vOnCallStatus(bCallActive);

      if ((false == bCallActive) && (e8SPI_PHONE_ACTIVE == m_enPhoneAppState))
      {
         //! Send a unborrow request from the accessory side since the call is ended.
         //! This is explicitly send to iPhone to clear the audio blocking constraint.
         //! This is required only for CarPlay.
         t_U32 u32DeviceID = m_poSpiCmdInterface->u32GetSelectedDeviceHandle();
         tenDeviceCategory enSelDevCat = m_poSpiCmdInterface->enGetDeviceCategory(m_poSpiCmdInterface->u32GetSelectedDeviceHandle());

         if (e8DEV_TYPE_DIPO == enSelDevCat)
         {
            m_poSpiCmdInterface->vSetAccessoryAudioContext(u32DeviceID,
                     e8SPI_AUDIO_PHONE, false,
                     corEmptyUsrContext, e8DEV_TYPE_DIPO);
         }
      } //if(false == bCallActive)

   }//if (OSAL_NULL != m_poSpiCmdInterface)
}

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnTelephoneCallStatus()
***************************************************************************/
tVoid devprj_tclService::vOnTelephoneCallStatus(
       std::vector<trTelCallStatusInfo> rfcoTelCallStatusList)
{
   ETG_TRACE_USR4(("devprj_tclService::vOnTelephoneCallStatus() entered "));
   //! Forward call(s) info to Key handler
    if (OSAL_NULL != m_poKeyConfig)
   {
     m_poKeyConfig->vSetBTCallStatus(rfcoTelCallStatusList);

   }//if (OSAL_NULL!= m_poKeyConfig)
    std::vector<trTelCallStatusInfo>::iterator it = rfcoTelCallStatusList.begin();
    tenCallStatus rCallStatus = e8IDLE;
    for(; it != rfcoTelCallStatusList.end(); ++it)
    {
       if(e8IDLE !=  it->enCallStatus)
       {
          m_bNativePhoneActive = true;
          rCallStatus = it->enCallStatus;
          break;
       }
       else
       {
          m_bNativePhoneActive = false;
       }
    }
    m_enPhoneAppState = (m_bNativePhoneActive || m_bOnstarPhoneActive) ? (e8SPI_PHONE_ACTIVE) : (e8SPI_PHONE_NOT_ACTIVE);

    if((true == m_bNativePhoneActive) &&
       (rCallStatus != e8RINGTONE))
    {
       //! If the call is active set the speech app state to speeking.
       m_bNativeSpeechActive = true;
    }
    else
    {
       m_bNativeSpeechActive = false;
    }
    m_enSpeechAppState = (m_bOnstarSpeechState || m_bNativeSpeechActive) ? e8SPI_SPEECH_SPEAKING : e8SPI_SPEECH_END;

    if (OSAL_NULL != m_poSpiCmdInterface)
    {
       m_poSpiCmdInterface->vSetAccessoryAppState(m_enSpeechAppState,
             m_enPhoneAppState, m_enNavAppState, corEmptyUsrContext);
    }
}

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::bQueueAlternateAudioReq()
***************************************************************************/
tBool devprj_tclService::bQueueAlternateAudioReq(const tU8 u8SrcNumber, tBool bActivate)
{
   t_Bool bRetval = false;
   ETG_TRACE_USR4(("devprj_tclService::vQueueAVActivationReq() u8SrcNumber = %d bActivate = %d \n",u8SrcNumber, bActivate));
   ETG_TRACE_USR4(("devprj_tclService::vQueueAVActivationReq() m_rAltAudioStatus.u8SrcNumber = %d "
            "m_rAltAudioStatus.enCurrAudioState = %d \n",m_rAltAudioStatus.u8SrcNumber, m_rAltAudioStatus.enCurrAudioState));

   if((u8SrcNumber == m_rAltAudioStatus.u8SrcNumber) && (e8_AUDIO_IDLE != m_rAltAudioStatus.enCurrAudioState))
   {
      //! If current state is not idle store the request
      bRetval = true;
      m_oAltAudioRequestLock.s16Lock();
      m_rAltAudioRequest.bActivate = bActivate;
      m_rAltAudioRequest.u8SourceNumber = u8SrcNumber;
      m_oAltAudioRequestLock.vUnlock();
   }
   else if (0 != u8SrcNumber)
   {
      //! Normal Flow
      if(true == bActivate)
      {
         //! Send activation request
         bRetval = bTriggerAVAction(u8SrcNumber , IIL_EN_ISRC_REQACT);
         m_oAltAudioStatusLock.s16Lock();
         m_rAltAudioStatus.u8SrcNumber = u8SrcNumber;
         m_rAltAudioStatus.enCurrAudioState = e8_AUDIO_ACTIVATING;
         m_oAltAudioStatusLock.vUnlock();
      }
      else
      {
         //! Send Deactivation request
         bRetval = bTriggerAVAction(u8SrcNumber , IIL_EN_ISRC_REQDEACT);
         m_oAltAudioStatusLock.s16Lock();
         m_rAltAudioStatus.u8SrcNumber = u8SrcNumber;
         m_rAltAudioStatus.enCurrAudioState = e8_AUDIO_DEACTIVATING;
         m_oAltAudioStatusLock.vUnlock();
      }
   }
   return bRetval;
}

/***************************************************************************
** FUNCTION:  tVoid devprj_tclService::vOnOnstarDataSettingsUpdate()
***************************************************************************/
t_Void devprj_tclService::vOnOnstarDataSettingsUpdate(tenOnstarSettingsEvents enDataSetEvent)
{
   ETG_TRACE_USR4(("devprj_tclService::vOnOnstarDataSettingsUpdate() entered: Event type %d ",
         ETG_ENUM(ONSTAR_DATASET_EVENT, enDataSetEvent)));

   t_Bool bUpdateNeeded = true;

   switch(enDataSetEvent)
   {
   case e8_ONSTAR_CALL_CONNECTING:
   case e8_ONSTAR_CALL_CONNECTED:
      {
		 m_bOnstarPhoneActive = true;
		 m_bOnstarSpeechState = true;
      }
      break;
   case e8_ONSTAR_INCOMING_CALL:
   	  {
	    m_bOnstarPhoneActive = true; 
   		m_bOnstarSpeechState = false;
   	  }
   	  break;
   case e8_ONSTAR_CALL_ENDED:
   case e8_ONSTAR_CALL_CONNECTION_FAILED:
      {
		 m_bOnstarPhoneActive = false;
		 m_bOnstarSpeechState = false;
      }
      break;
   case e8_ONSTAR_TITILED_MENU_TEMPLATE:
      {
		 m_bOnstarSpeechState = true;
      }
      break;
   case e8_ONSTAR_DEACTIVATE_MENU:
      {
		 m_bOnstarSpeechState = false;
      }
      break;
   default:
      {
         bUpdateNeeded = false;
      }
      break;
   }//switch(enDataSetEvent)

   if((true == bUpdateNeeded) && (NULL != m_poSpiCmdInterface))
   {
      m_enPhoneAppState = (m_bOnstarPhoneActive || m_bNativePhoneActive) ? e8SPI_PHONE_ACTIVE : e8SPI_PHONE_NOT_ACTIVE;
	  m_enSpeechAppState = (m_bOnstarSpeechState || m_bNativeSpeechActive) ? e8SPI_SPEECH_SPEAKING : e8SPI_SPEECH_END;
      m_poSpiCmdInterface->vSetAccessoryAppState( m_enSpeechAppState,
               m_enPhoneAppState, m_enNavAppState, corEmptyUsrContext);
   }// if(true == bUpdateNeeded)
}

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vOnOutsideTempUpdate()
***************************************************************************/
t_Void devprj_tclService::vOnOutsideTempUpdate(t_Bool bValidityFlag,t_Double dTemp)
{
   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      m_poSpiCmdInterface->vReportEnvironmentData(bValidityFlag,dTemp,false,(t_Double)(0));
   }//if (OSAL_NULL != m_poSpiCmdInterface)
}

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vSubscribeForEnvData()
***************************************************************************/
t_Void devprj_tclService::vSubscribeForEnvData(t_Bool bSubscribe)
{
   if(OSAL_NULL != m_poGMLANClient)
   {
      (true == bSubscribe)?(m_poGMLANClient->vRegForEnvDataUpdates(rEnvironmentDataCbs)):
         (m_poGMLANClient->vUnregForEnvDataUpdates());
   }//if(OSAL_NULL != m_poGMLANClient)
}

/***************************************************************************
** FUNCTION: t_Void devprj_tclService::vRequestDeviceAuthorization
***************************************************************************/
t_Void devprj_tclService::vRequestDeviceAuthorization(std::vector<trDeviceAuthInfo> &rfrvecDevAuthInfo)
{
   ETG_TRACE_USR1(("devprj_tclService::vRequestDeviceAuthorization() not implemented "));
   SPI_INTENTIONALLY_UNUSED(rfrvecDevAuthInfo);
}

/***************************************************************************
** FUNCTION: t_Void devprj_tclService::vPostDipoRoleSwitchResponse
***************************************************************************/
t_Void devprj_tclService::vPostDipoRoleSwitchResponse(t_Bool bRoleSwitchRequired, const t_U32 cou32DeviceHandle,
      const trUserContext& rfcorUsrCntxt)
{
   ETG_TRACE_USR1(("devprj_tclService::vPostDipoRoleSwitchResponse() bRoleSwitchRequired = %d ", ETG_ENUM(BOOL, bRoleSwitchRequired)));

   //! Send DiPoRoleSwitchRequiredt method result
   devprj_tMRDiPoSwitchReq oDiPoSwitchReqMR;
   oDiPoSwitchReqMR.u8DeviceTag = cou32DeviceHandle;

   most_fi_tcl_e8_DiPOSwitchReqResponse::tenType enDiPoSwitchRes =
            (true == bRoleSwitchRequired) ?
                  most_fi_tcl_e8_DiPOSwitchReqResponse::FI_EN_E8DIPO_ROLE_SWITCH_REQUIRED
                : most_fi_tcl_e8_DiPOSwitchReqResponse::FI_EN_E8DIPO_ROLE_SWITCH_NOT_REQUIRED;
   oDiPoSwitchReqMR.e8DiPOSwitchReqResponse.enType = enDiPoSwitchRes;

   if (FALSE == bPostResponse(oDiPoSwitchReqMR, rfcorUsrCntxt))
   {
      ETG_TRACE_ERR((" vPostDipoRoleSwitchResponse: DiPoRoleSwitchRequired MR post failed! "));
   }

   //! Currently method error is not required
   //! Send DiPoRoleSwitchRequiredt method error
  /* {
      devprj_tErrDiPoSwitchReq oDiPoSwitchReqErr;
      if (FALSE == bPostResponse(oDiPoSwitchReqErr, corfrUsrCtxt))
      {
         ETG_TRACE_ERR(("vOnMSDiPoSwitchRequired: "
            "DiPoRoleSwitchRequiredt.Error post failed! "));
      }
   }*/
}

/***************************************************************************
** FUNCTION:  t_Void devprj_tclService::vOnValetModeChange()
**************************************************************************/
t_Void devprj_tclService::vOnValetModeChange(t_Bool bValetModeActive)
{
   ETG_TRACE_USR1(("devprj_tclService::vOnValetModeChange entered "));

   if (OSAL_NULL != m_poSpiCmdInterface)
   {
      //! Set Device selection mode
      tenDeviceSelectionMode enDevSelectionMode = (bValetModeActive) ?
            (e16DEVICESEL_MANUAL) : (e16DEVICESEL_AUTOMATIC);
      m_poSpiCmdInterface->vSetDeviceSelectionMode(enDevSelectionMode);

      //! Deselect any active projection device if Valet mode is active
      if (true == bValetModeActive)
      {
         t_U32 u32SelDevHandle = m_poSpiCmdInterface->u32GetSelectedDeviceHandle();
         if (cou32InvalidHandle != u32SelDevHandle)
         {
            m_poSpiCmdInterface->vSelectDevice(u32SelDevHandle,
                  e8USB_CONNECTED, /*Device Connection Type*/
                  e8DEVCONNREQ_DESELECT,
                  e8USAGE_ENABLED, /*DAP Usage - not used*/
                  e8USAGE_ENABLED, /*CDB Usage - not used*/
                  e8DEV_TYPE_UNKNOWN,
                  corEmptyUsrContext,
                  false); /*Not HMI deselection*/
         }
      }//if (true == bValetModeActive)
   }//if (OSAL_NULL != m_poSpiCmdInterface)
}

//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>
