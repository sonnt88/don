/***********************************************************************/
/*!
* \file  devprj_tclKeyConfig.cpp
* \brief Class to handle the key configurations for Device Projection
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to handle the key configurations for Device Projection
AUTHOR:         Hari Priya E R
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
27.09.2014  | Hari Priya E R        | Initial Version
06.11.2014  | Hari Priya E R        | Added changes to set Client key capabilities based on
                                      GM Vehicle Brand vlaue
03.04.2015  | Sameer Chandra        | GMMY16-26376 Fix,Improvements in ML PTT key handling.
06.05.2015  | Ram                   | Made logic handling AAP PPT start and PTT end similar to ML.
06.05.2015  | Shiva kumar G         | Changes in carplay PTT Key handling(GMMY17-2964)

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

#include "Trace.h"
#include "SPITypes.h"
#include "devprj_tclKeyConfig.h"
#include "spi_tclCmdInterface.h"
#include "spi_tclConfigReader.h"
#include "spi_tclSpeechHMIClient.h"
#include "spi_tclTelephoneClient.h"

#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_TCLSERVICE
#include "trcGenProj/Header/devprj_tclKeyConfig.cpp.trc.h"
#endif
#endif

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/
//Long press Timer method should be called for every 1sec
#define TIMER_PTT_LONGPRESS_MSEC (tCU32)900
//Siri Sustain timer for 1 sec.
//#define TIMER_SIRI_SUSTAIN_MSEC (tCU32)1000
static timer_t rLongPressTimerID = 0;
static timer_t rSiriSustainTimerID = 0;

tU32 devprj_tclKeyConfig::m_u32DeviceHandle = 0;
static const tU32 scu32AppHandle = 0;
static trUserContext scrUsrCntxt;
tBool devprj_tclKeyConfig::m_bIsTimerExpired = false;
spi_tclCmdInterface* devprj_tclKeyConfig:: m_poSpiCmdInterface = NULL;
spi_tclSpeechHMIClient* devprj_tclKeyConfig::m_poSpeechHMIClient = NULL;
tenSpeechRecStatus devprj_tclKeyConfig::m_enSRStatus = e8SR_INITIALIZING;

static const trKeyCode aKeyCode[] =
{
   {devprj_tFiSwitch::FI_EN_E8DS_BACK_SWITCH,e32DEV_BACKWARD},
   {devprj_tFiSwitch::FI_EN_E8DS_MENU_SWITCH,e32DEV_MENU},
   {devprj_tFiSwitch::FI_EN_E8DS_NEXT_SWITCH,e32MULTIMEDIA_NEXT},
   {devprj_tFiSwitch::FI_EN_E8DS_PREV_SWITCH,e32MULTIMEDIA_PREVIOUS},
   {devprj_tFiSwitch::FI_EN_E8DS_SWC_RIGHT_SWITCH,e32MULTIMEDIA_NEXT},
   {devprj_tFiSwitch::FI_EN_E8DS_SWC_LEFT_SWITCH,e32MULTIMEDIA_PREVIOUS},
   {devprj_tFiSwitch::FI_EN_E8DS_SWC_UP_SWITCH,e32MULTIMEDIA_NEXT},
   {devprj_tFiSwitch::FI_EN_E8DS_SWC_DOWN_SWITCH,e32MULTIMEDIA_PREVIOUS},
   {devprj_tFiSwitch::FI_EN_E8DS_FAVORITES_UP_SWITCH,e32MULTIMEDIA_NEXT},
   {devprj_tFiSwitch::FI_EN_E8DS_FAVORITES_DOWN_SWITCH,e32MULTIMEDIA_PREVIOUS},
   {devprj_tFiSwitch::FI_EN_E8DS_SWC_END_SWITCH,e32DEV_PHONE_END},
   {devprj_tFiSwitch::FI_EN_E8DS_SWC_PTT_SWITCH,e32DEV_PTT}
};


/***************************************************************************
** FUNCTION:  devprj_tclKeyConfig::devprj_tclKeyConfig()
***************************************************************************/
devprj_tclKeyConfig::devprj_tclKeyConfig(spi_tclCmdInterface* poSpiCmdInterface,
                                         spi_tclSpeechHMIClient* poSpeechHMI,
                                         spi_tclTelephoneClient* poTelephoneClient):
m_poTelephoneClient(poTelephoneClient),m_enBtActivityStatus(e8_BT_NOACTIVITY)
{
   ETG_TRACE_USR1(("devprj_tclKeyConfig() entered \n"));

   m_poSpeechHMIClient = poSpeechHMI;
   m_poSpiCmdInterface = poSpiCmdInterface;

   SPI_NORMAL_ASSERT(NULL == m_poSpiCmdInterface);

   SPI_NORMAL_ASSERT(NULL == m_poSpeechHMIClient);
   SPI_NORMAL_ASSERT(NULL == m_poTelephoneClient);
}

/***************************************************************************
** FUNCTION:  devprj_tclKeyConfig::~devprj_tclKeyConfig()
***************************************************************************/
devprj_tclKeyConfig::~devprj_tclKeyConfig()
{
   ETG_TRACE_USR1(("~devprj_tclKeyConfig() entered \n"));
   m_mapKeyCode.clear();
   m_vTelCallStatusList.clear();
   m_poSpiCmdInterface = NULL;
   m_poSpeechHMIClient = NULL;
   m_poTelephoneClient = NULL;
}

/***************************************************************************
** FUNCTION   :tVoid devprj_tclKeyConfig::vConfigDevPrjKeys(
                     tenKeyMode enKeyMode,tenKeyCode enKeyCode,
                     tBool& bNativeSRTrigger,
                     t_Bool& bSetMute,
                     trUserContext rcUsrCntxt)
***************************************************************************/
tVoid devprj_tclKeyConfig::vConfigDevPrjKeys(
                     tenKeyMode enKeyMode,tenKeyCode enKeyCode,
                     tBool& bSetMute,
                     trUserContext rcUsrCntxt)
{
   ETG_TRACE_USR1(("vConfigDevPrjKeys() entered."));

   scrUsrCntxt = rcUsrCntxt;

   if (NULL != m_poSpiCmdInterface)
   {
      m_u32DeviceHandle = m_poSpiCmdInterface->u32GetSelectedDeviceHandle();
      tenDeviceCategory enDevCat = m_poSpiCmdInterface ->enGetDeviceCategory(m_u32DeviceHandle);

      //Based on the device category,configure the CarPlay and ML keys appropriately
      switch (enDevCat)
        {
      case e8DEV_TYPE_DIPO:
        {
          vConfigCarPlayKeys(enKeyMode, enKeyCode, bSetMute);
        }
        break;
      case e8DEV_TYPE_MIRRORLINK:
        {
          vConfigMLKeys(enKeyMode, enKeyCode, bSetMute);
        }
        break;
      case e8DEV_TYPE_ANDROIDAUTO:
        {
          vConfigAAPKeys(enKeyMode, enKeyCode, bSetMute);
        }
        break;
      default:
        {
           ETG_TRACE_ERR(("vConfigDevPrjKeys()-Invalid Device Category!"));
        }
        break;
        }
   }
}

/***************************************************************************
** FUNCTION   :tVoid devprj_tclKeyConfig::vConfigCarPlayKeys(tenKeyMode enKeyMode,
                                               tenKeyCode enKeyCode,
                                               t_Bool& bSetMute)
***************************************************************************/
tVoid devprj_tclKeyConfig::vConfigCarPlayKeys(tenKeyMode enKeyMode,tenKeyCode enKeyCode,
                                               tBool& bSetMute)
{
   ETG_TRACE_USR1(("vConfigCarPlayKeys() entered."));

   if (NULL != m_poSpiCmdInterface)
   {
      /*Since the PTT and END keys requires special handling based on App State,
      it is handled in a separate function*/
      if((e32DEV_PTT == enKeyCode) || (e32DEV_PHONE_END == enKeyCode))
      {
         vConfigCarPlaySWCKeys(enKeyMode,enKeyCode,bSetMute);
      }
      else
      {
         //For keys other than PTT and END,use the Key Event interface directly
         m_poSpiCmdInterface->vSendKeyEvent(m_u32DeviceHandle, 
            enKeyMode,enKeyCode, scrUsrCntxt);
      }
   }
}

/***************************************************************************
** FUNCTION   :tVoid devprj_tclKeyConfig::vConfigMLKeys(tenKeyMode enKeyMode,
                                         tenKeyCode enKeyCode,
                                         t_Bool& bSetMute,
                                         trBTCallFlag& rBTCallFlag)
***************************************************************************/
tVoid devprj_tclKeyConfig::vConfigMLKeys(tenKeyMode enKeyMode,tenKeyCode enKeyCode,
                                         tBool& bSetMute)
{
   ETG_TRACE_USR1(("vConfigMLKeys() entered."));

   if (NULL != m_poSpiCmdInterface)
   {
      /*Since the PTT and END keys requires special handling based on App State,
      it is handled in a separate function*/
      if((e32DEV_PTT == enKeyCode) || (e32DEV_PHONE_END == enKeyCode))
      {
         vConfigMLSWCKeys(enKeyMode,enKeyCode,bSetMute);
      }
      else
      {
         //For keys other than PTT and END,use the Key Event interface directly
         m_poSpiCmdInterface->vSendKeyEvent(m_u32DeviceHandle, 
            enKeyMode,enKeyCode, scrUsrCntxt);
      }
   }
}

/***************************************************************************
** FUNCTION   :tVoid devprj_tclKeyConfig::vConfigMLSWCKeys(tenKeyMode enKeyMode,
                                            tenKeyCode enKeyCode,
                                            tBool& bSetMute)
***************************************************************************/
tVoid devprj_tclKeyConfig::vConfigMLSWCKeys(tenKeyMode enKeyMode,
                                            tenKeyCode enKeyCode,
                                            tBool& bSetMute)
{
   ETG_TRACE_USR1(("vConfigMLSWCKeys() entered."));

   switch(enKeyCode)
   {
   case e32DEV_PTT:
      {
         vHandleMLPTTKey(enKeyMode,enKeyCode);
      }
      break;
   case e32DEV_PHONE_END:
      {
         vHandleMLENDKey(enKeyMode,enKeyCode,bSetMute);
      }
      break;
   default:
      {
         ETG_TRACE_USR2(("Key pressed is neither PTT nor END"));
      }
      break;
   }
}

/***************************************************************************
** FUNCTION   :tVoid devprj_tclKeyConfig::vHandleMLPTTKey(
                         tenKeyMode enKeyMode,tenKeyCode enKeyCode)
***************************************************************************/
t_Void devprj_tclKeyConfig::vHandleMLPTTKey(tenKeyMode enKeyMode,tenKeyCode enKeyCode)
{
   ETG_TRACE_USR1(("vHandleMLPTTKey() entered."));

   if(e8KEY_PRESS == enKeyMode )
   {
      vHandleBTCallOnKey(e8_BT_CALLACCEPT);

      if((NULL!= m_poSpeechHMIClient) && (e8_BT_NOACTIVITY == m_enBtActivityStatus) &&
         ((e8SR_INACTIVE == m_enSRStatus)||(e8SR_ACTIVE == m_enSRStatus)))
      {
         //Indicate the Device Projection Service that Native Speech Rec needs to be triggered
         /*Initially when no Native SR is active,the SR status will be Inactive.
         At this point pressing the PTT key should start the Native SR.
         Once the Native SR starts,the SR status becomes Active.
         Then pressing the PTT key should cancel the voice prompt*/
         m_poSpeechHMIClient->bSendSRButtonEvent(enKeyCode);
      }
   }
}

/***************************************************************************
** FUNCTION   :tVoid devprj_tclKeyConfig::vHandleMLENDKey(tenKeyMode enKeyMode,
                                            tenKeyCode enKeyCode,
                                            tBool& bSetMute)
***************************************************************************/
t_Void devprj_tclKeyConfig::vHandleMLENDKey(tenKeyMode enKeyMode,tenKeyCode enKeyCode,
                                            tBool& bSetMute)
{
  ETG_TRACE_USR1(("vHandleMLENDKey() entered."));

  if (e8KEY_PRESS == enKeyMode)
    {
      vHandleBTCallOnKey(e8_BT_CALLEND);

      if ( e8SR_ACTIVE == m_enSRStatus && (e8_BT_NOACTIVITY == m_enBtActivityStatus) )
        {
          /**
           * Since no calls were handled, forward the request to native speech rec if
           *  it is active already.
           */
          ETG_TRACE_USR2(("SR is active,trigger SR end"));
          m_poSpeechHMIClient->bSendSRButtonEvent(enKeyCode);
        }
      else if (e8_BT_NOACTIVITY == m_enBtActivityStatus)
        {
          /**
           * If the END key is pressed when there is no phone call or native speech session,
           * trigger the Global Mute of the AV Manager
           */
    	  ETG_TRACE_USR2(
              ("SR is Inactive & no calls handled, set global mute"));
          bSetMute = true;
        }
    }
  /*
   * KEY_RELEASE event is not handled since there is nothing particular
   * to be performed. There is no possible use case to handle for long
   * press of END key either.
   */
}

/***************************************************************************
** FUNCTION   :tVoid devprj_tclKeyConfig::vConfigCarPlaySWCKeys(tenKeyMode enKeyMode,
                                                 tenKeyCode enKeyCode,
                                                 t_Bool& bSetMute)
***************************************************************************/
tVoid devprj_tclKeyConfig::vConfigCarPlaySWCKeys(tenKeyMode enKeyMode,
                                                 tenKeyCode enKeyCode,
                                                 tBool& bSetMute)
{
   ETG_TRACE_USR1(("vConfigCarPlaySWCKeys() entered."));

   tenSpeechAppState enSpeechAppState = e8SPI_SPEECH_UNKNOWN;
   tenPhoneAppState enPhoneAppState = e8SPI_PHONE_UNKNOWN;
   tenNavAppState enNavAppState = e8SPI_NAV_UNKNOWN;

   if (NULL != m_poSpiCmdInterface)
   {
      m_poSpiCmdInterface->vGetAppStateInfo(enSpeechAppState,enPhoneAppState,enNavAppState);

         if(e8SPI_PHONE_ACTIVE == enPhoneAppState)
         {
            if(e32DEV_PTT == enKeyCode)
            {
                ETG_TRACE_USR2(("Active Phone call "));
               /*In case of GM, the same key code is received for both PTT and Phone Call Accept, whereas in Suzuki, 
               there are separate codes for PTT and Phone Call.
               While mapping the phone call functionality of the Apple Device to the received key event, one common code needs to be used.
               Hence, converting the received PTT key code in case of phone context to Phone Call Key Code and then using it.*/
               enKeyCode = e32DEV_PHONE_CALL;
            }//if(e32DEV_PTT == enKeyCode)
            m_poSpiCmdInterface->vSendKeyEvent(m_u32DeviceHandle, 
               enKeyMode,enKeyCode, scrUsrCntxt);
         } ////if(e8SPI_PHONE_ACTIVE != enPhoneAppState)
         else 
         {
            switch(enKeyCode)
            {
            case e32DEV_PTT:
               {
                  vHandleCarPlayPTTKey(enSpeechAppState,enKeyMode,enKeyCode);
               }
               break;
            case e32DEV_PHONE_END:
               {
                  vHandleCarPlayENDKey(enSpeechAppState,enKeyMode,enKeyCode,bSetMute);
               }
               break;
            default:
               {
                  ETG_TRACE_USR2(("Key pressed is neither PTT nor END"));
               }
               break;
            }//switch(enKeyCode)
         }//else
   }//if (NULL != m_poSpiCmdInterface)
}


/***************************************************************************
** FUNCTION   :tVoid devprj_tclKeyConfig::vHandleCarPlayPTTKey(
                         tenSpeechAppState enSpeechAppState,
                         tenKeyMode enKeyMode,tenKeyCode enKeyCode)
***************************************************************************/
t_Void devprj_tclKeyConfig::vHandleCarPlayPTTKey(tenSpeechAppState enSpeechAppState,
                                                 tenKeyMode enKeyMode,tenKeyCode enKeyCode)
{
   ETG_TRACE_USR1(("devprj_tclKeyConfig::vHandleCarPlayPTTKey entered \n"));

   Timer* poTimer = Timer::getInstance();

   if ( (NULL != m_poSpiCmdInterface) && (NULL != poTimer) )
   {
      if ( e8KEY_PRESS == enKeyMode )
      {
         //!First check if there are any active calls on BT
         //!If there is any active incoming BT call accept it,else
         //!if there is any BT call in any other state do not process any
         //!request after checking for it.
         vHandleBTCallOnKey(e8_BT_CALLACCEPT);

         if ( e8_BT_NOACTIVITY == m_enBtActivityStatus )
         {

            ETG_TRACE_USR2(("CarPlay PTT key Pressed"));

             if ( ((e8SPI_SPEECH_SPEAKING == enSpeechAppState)
                  || (e8SPI_SPEECH_RECOGNIZING == enSpeechAppState)))
            {
                ETG_TRACE_USR4(("Siri is in [%d] State",ETG_ENUM(SPI_SPEECH_STATE,enSpeechAppState)));

               m_poSpiCmdInterface->vLaunchApp(m_u32DeviceHandle,
                     e8DEV_TYPE_DIPO, scu32AppHandle, e8DIPO_SIRI_BUTTONDOWN,
                     "", e8ECNR_NOCHANGE, scrUsrCntxt);
            }
             else
            {
               //!Since there is no BT call activity, process the incoming request.
               // If Siri is not active start the timer as soon as the PTT Key Press event
               // is received.
               //@Note: Timer is used to to track long press of PTT key event

                ETG_TRACE_USR4(("Siri is in [%d] State",ETG_ENUM(SPI_SPEECH_STATE,enSpeechAppState)));
               poTimer->StartTimer(rLongPressTimerID, TIMER_PTT_LONGPRESS_MSEC,
                     0, this, bCbLongPressTimeOut, NULL);
               //Send the Siri Pre-Warm trigger
               m_poSpiCmdInterface->vLaunchApp(m_u32DeviceHandle,
                     e8DEV_TYPE_DIPO, scu32AppHandle, e8DIPO_SIRI_PREWARN, "",
                     e8ECNR_NOCHANGE, scrUsrCntxt);
            }
         }
      }
      else if ( (e8KEY_RELEASE == enKeyMode) && (e8_BT_NOACTIVITY == m_enBtActivityStatus) )
      {
         ETG_TRACE_USR2(("Carplay PTT key Released"));
         /* If the key release event is received,then stop the timer and if the earlier started timer has expired,
          then send the Siri Button Up trigger.
          Else,if the timer has not expired,then it is a short press and
          hence trigger the SR Button Event trigger of Speech HMI*/
         poTimer->CancelTimer(rLongPressTimerID);
         rLongPressTimerID = 0;

         if ( true == m_bIsTimerExpired )
         {
            ETG_TRACE_USR4(("Carplay PTT key Timer expired"));
            if ( e8SR_ACTIVE != m_enSRStatus )
            {
               ETG_TRACE_USR4(("No Acive Native SR Launch Siri"));
               //Timer expired and no Native Speech Session Active ,hence send Siri Button Up trigger
               m_poSpiCmdInterface->vLaunchApp(m_u32DeviceHandle,
                     e8DEV_TYPE_DIPO, scu32AppHandle, e8DIPO_SIRI_BUTTONUP, "",
                     e8ECNR_NOCHANGE, scrUsrCntxt);
            }
            else if ( NULL != m_poSpeechHMIClient )
            {
               //Timer expired but Speech Session Active
               /*Indicate the Device Projection Service that SR Button event trigger has to be sent to Speech HMI
                to cancel voice prompt*/
               m_poSpeechHMIClient->bSendSRButtonEvent(enKeyCode);
            }
            m_bIsTimerExpired = false;
         }

         /*At this point,if Siri session is active,pressing the PTT button should end Siri as per new
          GM & Apple requirements. Ref: GMMY17-4696*/
         else if ( (e8SPI_SPEECH_SPEAKING == enSpeechAppState)
               || (e8SPI_SPEECH_RECOGNIZING == enSpeechAppState) )
         {
            ETG_TRACE_USR4(("Siri is in [%d] State",ETG_ENUM(SPI_SPEECH_STATE,enSpeechAppState)));

            m_poSpiCmdInterface->vLaunchApp(m_u32DeviceHandle, e8DEV_TYPE_DIPO,
                  scu32AppHandle, e8DIPO_SIRI_BUTTONUP, "", e8ECNR_NOCHANGE,
                  scrUsrCntxt);

         }
         else if ( (NULL != m_poSpeechHMIClient) && ((e8SR_INACTIVE
               == m_enSRStatus) || (e8SR_ACTIVE == m_enSRStatus)) )
         {
            //Indicate the Device Projection Service that Native Speech Rec needs to be triggered
            /*Initially when no Native SR is active,the SR status will be Inactive.
             At this point pressing the PTT key should start the Native SR.
             Once the Native SR starts,the SR status becomes Active.
             Then pressing the PTT key should cancel the voice prompt*/
            ETG_TRACE_USR4(("Toggle SR button"));
            m_poSpeechHMIClient->bSendSRButtonEvent(enKeyCode);
         }
      }
   }
}

/***************************************************************************
** FUNCTION   :tVoid devprj_tclKeyConfig::vHandleCarPlayENDKey(
                         tenSpeechAppState enSpeechAppState,
                         tenKeyMode enKeyMode,tenKeyCode enKeyCode)
***************************************************************************/
t_Void devprj_tclKeyConfig::vHandleCarPlayENDKey(tenSpeechAppState enSpeechAppState,
                                               tenKeyMode enKeyMode,tenKeyCode enKeyCode,
                                               tBool& bSetMute)
{
   ETG_TRACE_USR1(("devprj_tclKeyConfig::vHandleCarPlayENDKey entered \n"));

   if ( e8KEY_PRESS == enKeyMode )
   {
      //!First check if there are any active calls on BT
      //!If there is any active incoming/ongoing/outgoing/on hold BT call end/reject it and
      //!do not process any request after acting on BT calls.
      vHandleBTCallOnKey(e8_BT_CALLEND);

      /*If end button is pressed during an active Siri Session,then irrespective of short/long press,
       Siri session should end.
       Since Apple ends the Siri Session only upon sending the Button Down and Button Up triggers with a short delay,
       consider even a long press as a short press and send the triggers accordingly*/
      if ( e8_BT_NOACTIVITY == m_enBtActivityStatus )
      {
         if (((e8SPI_SPEECH_RECOGNIZING == enSpeechAppState)
               || (e8SPI_SPEECH_SPEAKING == enSpeechAppState)))

         {
            ETG_TRACE_USR4(("Siri is in [%d] State",ETG_ENUM(SPI_SPEECH_STATE,enSpeechAppState)));
            ETG_TRACE_USR2(("Sent Siri : %d\n",e8DIPO_SIRI_BUTTONDOWN));
            m_poSpiCmdInterface->vLaunchApp(m_u32DeviceHandle, e8DEV_TYPE_DIPO,
                  scu32AppHandle, e8DIPO_SIRI_BUTTONDOWN, "", e8ECNR_NOCHANGE,
                  scrUsrCntxt);

         }
         else if ((NULL != m_poSpeechHMIClient) && (e8SR_ACTIVE
               == m_enSRStatus))
         {
            //END Key press when Native SR active,hence end Native SR
            ETG_TRACE_USR2((" End Native SR\n"));

            m_poSpeechHMIClient->bSendSRButtonEvent(enKeyCode);
         }
         else
         {
            /*If no Siri session is active and no Native speech session is active,pressing
             the END button should trigger a Global Mute*/
            ETG_TRACE_USR2(("Set global mute\n"));
            bSetMute = true;
         }
      }

   }
   else if ( (e8KEY_RELEASE == enKeyMode) && (e8_BT_NOACTIVITY == m_enBtActivityStatus) )
   {
      //! Send the Siri Button UP
      ETG_TRACE_USR4(("Sent Siri : %d\n",e8DIPO_SIRI_BUTTONUP));
      m_poSpiCmdInterface->vLaunchApp(m_u32DeviceHandle, e8DEV_TYPE_DIPO,
            scu32AppHandle, e8DIPO_SIRI_BUTTONUP, "", e8ECNR_NOCHANGE,
            scrUsrCntxt);
   }
}

/***************************************************************************
   ** FUNCTION: tBool devprj_tclKeyConfig::bCbLongPressTimeOut()
***************************************************************************/
t_Bool devprj_tclKeyConfig::bCbLongPressTimeOut(timer_t timerID , 
                                               tVoid *pObject, 
                                               const tVoid *pcoUserData)
{

   ETG_TRACE_USR1(("devprj_tclKeyConfig::bCbLongPressTimeOut entered \n"));
   SPI_INTENTIONALLY_UNUSED(pcoUserData);
   SPI_INTENTIONALLY_UNUSED(pObject);
   tenDeviceCategory enDevCat = m_poSpiCmdInterface ->enGetDeviceCategory(
         m_u32DeviceHandle);

   if ((enDevCat == e8DEV_TYPE_DIPO) && (NULL != m_poSpiCmdInterface)
         && (rLongPressTimerID == timerID))
      {
         m_bIsTimerExpired = true;
         m_poSpiCmdInterface->vLaunchApp(m_u32DeviceHandle, e8DEV_TYPE_DIPO,
               scu32AppHandle, e8DIPO_SIRI_BUTTONDOWN, "", e8ECNR_NOCHANGE,
               scrUsrCntxt);
      }
   else if ((enDevCat == e8DEV_TYPE_ANDROIDAUTO) && (NULL
         != m_poSpiCmdInterface) && (rLongPressTimerID == timerID))
      {

         //! Trigger Key event for search functionality.
         //! Send PRESS and RELEASE for SEARCH key to trigger
         //! Android auto Voice rec session.
         if (e8SR_ACTIVE != m_enSRStatus)
            {
               ETG_TRACE_USR1(("Send Search key Event"));

               m_bIsTimerExpired = true;
               m_poSpiCmdInterface->vSendKeyEvent(m_u32DeviceHandle,
                     e8KEY_PRESS, e32DEV_SEARCH, scrUsrCntxt);

               m_poSpiCmdInterface->vSendKeyEvent(m_u32DeviceHandle,
                     e8KEY_RELEASE, e32DEV_SEARCH, scrUsrCntxt);
            }
         else if (e8SR_ACTIVE == m_enSRStatus && NULL != m_poSpeechHMIClient)
            {
               //! Native Speech rec is active and it should be
               //! ended.
               ETG_TRACE_USR1(("Send Search key Event"));

               //! End Native Speech rec
               m_bIsTimerExpired = true;
               m_poSpeechHMIClient->bSendSRButtonEvent(e32DEV_PHONE_END);

               //! Activate Android auto Speech Rec.
               //! Send the Android Auto Speech Rec key.

               m_poSpiCmdInterface->vSendKeyEvent(m_u32DeviceHandle,
                     e8KEY_PRESS, e32DEV_SEARCH, scrUsrCntxt);

               m_poSpiCmdInterface->vSendKeyEvent(m_u32DeviceHandle,
                     e8KEY_RELEASE, e32DEV_SEARCH, scrUsrCntxt);

            }

      }

   //Then Cancel the timer
   Timer* poTimer = Timer::getInstance();
   if (NULL != poTimer)
      {
         poTimer->CancelTimer(rLongPressTimerID);
         rLongPressTimerID = 0;
      }
   return true;
}

//!Static
/***************************************************************************
 ** FUNCTION:  devprj_tclKeyConfig::bSiriSustainTimerCallback
 ***************************************************************************/
t_Bool devprj_tclKeyConfig::bSiriSustainTimerCallback(timer_t rTimerID, 
                                                      t_Void *pvObject,
                                                      const t_Void *pvUserData)
{  //TODO this function is deprecated now!
   SPI_INTENTIONALLY_UNUSED(pvUserData);
   SPI_INTENTIONALLY_UNUSED(pvObject);
   ETG_TRACE_USR1((" devprj_tclKeyConfig::bSiriSustainTimerCallback entered \n"));

   //In order to sustain Siri session ,Siri Button Down and Button Up should be sent with a delay.
   //The button down would be sent as soon as the Key Press is received
   //Hence sending the ButtonUp upon timer expiry after 1sec.

   if ((NULL != m_poSpiCmdInterface)&& (rSiriSustainTimerID == rTimerID))
   {
      m_poSpiCmdInterface->vLaunchApp(m_u32DeviceHandle,e8DEV_TYPE_DIPO,scu32AppHandle,
         e8DIPO_SIRI_BUTTONUP,"",e8ECNR_NOCHANGE,scrUsrCntxt);
   }

   //Then Cancel the timer
   Timer* poTimer = Timer::getInstance();
   if (NULL != poTimer)
   {
      poTimer->CancelTimer(rSiriSustainTimerID);
      rSiriSustainTimerID = 0;
   }
   return true;
}

/***************************************************************************
   ** FUNCTION: tVoid devprj_tclKeyConfig::vSetSpeechRecStatus(
                   tenSpeechRecStatus enSRStatus)
***************************************************************************/
tVoid devprj_tclKeyConfig::vSetSpeechRecStatus(tenSpeechRecStatus enSRStatus)
{
   ETG_TRACE_USR1(("devprj_tclKeyConfig::vSetSpeechRecStatus entered with Speech Rec status %d \n",ETG_ENUM(SR_STATUS, enSRStatus)));

   m_enSRStatus = enSRStatus;
}

/***************************************************************************
   ** FUNCTION: tVoid devprj_tclKeyConfig::vSetBTCallStatus(
   tenCallStatus enCallStatus,tU16 u16CallInstance)
***************************************************************************/
tVoid devprj_tclKeyConfig::vSetBTCallStatus(const std::vector<trTelCallStatusInfo> coTelCallStatusList)
{
   ETG_TRACE_USR1(("devprj_tclKeyConfig::vSetBTCallStatus entered "));
   //! Call list used to decide on action for PTT keys.
   m_vTelCallStatusList.clear();
   m_vTelCallStatusList = coTelCallStatusList;
}



/***************************************************************************
** FUNCTION: tVoid devprj_tclKeyConfig::vInsertKeyCodeToMap()
***************************************************************************/
tVoid devprj_tclKeyConfig::vInsertKeyCodeToMap()
{
   ETG_TRACE_USR1(("devprj_tclService::vInsertKeyCodeToMap entered "));
   ETG_TRACE_USR4(("(sizeof(aKeyCode): %d ",(sizeof(aKeyCode))));
   ETG_TRACE_USR4(("(sizeof(aKeyCode))/sizeof(tU32): %d ",(sizeof(aKeyCode))/sizeof(tU32)));

   for(tU32 u32Index=0; u32Index< ((sizeof(aKeyCode))/sizeof(trKeyCode));u32Index++)
   {
      m_mapKeyCode.insert(std::pair<devprj_tenSwitch,tenKeyCode>
         (aKeyCode[u32Index].enDevPrjSwitch,aKeyCode[u32Index].enKeyCode));
   }
}

/***************************************************************************
   ** FUNCTION: tenKeyCode devprj_tclKeyConfig::enGetKeyCode(devprj...)const
***************************************************************************/
tenKeyCode devprj_tclKeyConfig::enGetKeyCode(devprj_tenSwitch enDevPrjSwitch)const
{
   //Default KeyCode
   tenKeyCode enKeyCode = e32INVALID_KEY;
   //Iterator to iterate through the Switch map
   std::map<devprj_tenSwitch,tenKeyCode>::const_iterator itKeyCode;

   //Find the requested value and return the associated enum value with that.
   itKeyCode = m_mapKeyCode.find(enDevPrjSwitch);
   if (m_mapKeyCode.end() != itKeyCode )
   {
      enKeyCode = itKeyCode->second ;
   }
   return enKeyCode;
}


/***************************************************************************
** FUNCTION: t_Void devprj_tclKeyConfig::vSetClientCapabilities()
***************************************************************************/
t_Void devprj_tclKeyConfig::vSetClientCapabilities()
{
   spi_tclConfigReader* poConfigReader = spi_tclConfigReader::getInstance();
   if(NULL != poConfigReader)
   {
      t_U8 u8VehicleBrand = 0;
      poConfigReader->vReadVehicleBrand(u8VehicleBrand);
      tenVehicleBrand enVehicleBrand = static_cast <tenVehicleBrand>(u8VehicleBrand);
      ETG_TRACE_USR4(("Vehicle Brand: %d ",enVehicleBrand));
      trClientCapabilities rClientCapabilities ;
      if(e8CADILLAC == enVehicleBrand)
      {
         rClientCapabilities.rKeyCapabilities.u32DeviceKeySupport  = 0x00;
      }
      else
      {
         rClientCapabilities.rKeyCapabilities.u32DeviceKeySupport  = 0x9000;
      }
      rClientCapabilities.rKeyCapabilities.u32MultimediaKeySupport = 0x60;

      if(NULL!= m_poSpiCmdInterface)
      {
         trUserContext rUsrCntxt;
         m_poSpiCmdInterface->vSetClientCapabilities(rClientCapabilities,rUsrCntxt);
      }
   }
}
/***************************************************************************
** FUNCTION   :tVoid devprj_tclKeyConfig::vConfigAAPKeys(tenKeyMode enKeyMode,
                                               tenKeyCode enKeyCode,
                                               t_Bool& bSetMute)
***************************************************************************/
tVoid devprj_tclKeyConfig::vConfigAAPKeys(tenKeyMode enKeyMode,tenKeyCode enKeyCode,
                                               tBool& bSetMute)
{
  ETG_TRACE_USR1(("vConfigAAPKeys() entered."));

  if (NULL != m_poSpiCmdInterface)
    {
      if ((e32DEV_PTT == enKeyCode) || (e32DEV_PHONE_END == enKeyCode))
        {
          vConfigAAPSWCKeys(enKeyMode, enKeyCode, bSetMute);
        }
      else
        {
          m_poSpiCmdInterface->vSendKeyEvent(m_u32DeviceHandle, enKeyMode,
              enKeyCode, scrUsrCntxt);
        }
    }
}

/***************************************************************************
** FUNCTION   :tVoid devprj_tclKeyConfig::vConfigAAPSWCKeys(tenKeyMode enKeyMode,
                                            tenKeyCode enKeyCode,
                                            tBool& bSetMute)
***************************************************************************/
tVoid devprj_tclKeyConfig::vConfigAAPSWCKeys(tenKeyMode enKeyMode,
                                            tenKeyCode enKeyCode,
                                            tBool& bSetMute)
{
   ETG_TRACE_USR1(("vConfigAAPSWCKeys entered"));
   tenSpeechAppState enSpeechAppState = e8SPI_SPEECH_UNKNOWN;
   tenPhoneAppState enPhoneAppState = e8SPI_PHONE_UNKNOWN;
   tenNavAppState enNavAppState = e8SPI_NAV_UNKNOWN;

   if (NULL != m_poSpiCmdInterface)
      {
         //! Get the Phone Speech App State
         m_poSpiCmdInterface->vGetAppStateInfo(enSpeechAppState,enPhoneAppState,enNavAppState);

         switch (enKeyCode)
         {
         case e32DEV_PTT:
            {
               vHandleAAPPTTKey(enSpeechAppState,enKeyMode, enKeyCode);
            }
            break;
         case e32DEV_PHONE_END:
            {
               vHandleAAPENDKey(enSpeechAppState,enKeyMode, enKeyCode, bSetMute);
            }
            break;
         default:
            {
               ETG_TRACE_USR4(("Invalid Key received %d", enKeyCode));
            }
            break;
          }
      }
}

/***************************************************************************
 ** FUNCTION   :tVoid devprj_tclKeyConfig::vHandleAAPPTTKey(
 tenKeyMode enKeyMode,tenKeyCode enKeyCode)
 ***************************************************************************/
t_Void devprj_tclKeyConfig::vHandleAAPPTTKey(
         tenSpeechAppState enSpeechAppState, tenKeyMode enKeyMode,
         tenKeyCode enKeyCode)
{
   ETG_TRACE_USR1(("vHandleAAPPTTKey() entered."));
   Timer* poTimer = Timer::getInstance();

   if (NULL != poTimer)
   {
      if (e8KEY_PRESS == enKeyMode)
      {
         vHandleBTCallOnKey(e8_BT_CALLACCEPT);

         if ((e8SPI_SPEECH_SPEAKING != enSpeechAppState) && (e8_BT_NOACTIVITY == m_enBtActivityStatus))
         {
            ETG_TRACE_USR2(("Android Auto PTT key Pressed"));
            // Start the timer as soon as the PTT Key Press event is received
            //@Note: Timer is used to to track long press of PTT key event
            poTimer->StartTimer(rLongPressTimerID, TIMER_PTT_LONGPRESS_MSEC,
                     0, this, bCbLongPressTimeOut, NULL);
         }
      }
      else if ((e8KEY_RELEASE == enKeyMode) && (e8_BT_NOACTIVITY == m_enBtActivityStatus))
      {
         ETG_TRACE_USR2(("Android Auto PTT key Released"));

         /* If the key release event is received,then stop the timer.
          * Once the timer expires callback function will send events to
          * the device.
          */
         poTimer->CancelTimer(rLongPressTimerID);
         rLongPressTimerID = 0;
         if (true == m_bIsTimerExpired)
         {
            m_bIsTimerExpired = false;
         }

         //!Short Press of PTT key should End AAP speech session.
         else if ((e8SPI_SPEECH_SPEAKING == enSpeechAppState) && (NULL != m_poSpiCmdInterface))
         {
            //! To end Android Speech Rec Session SPI needs to send Both press and release at
            //! one go.
            m_poSpiCmdInterface->vSendKeyEvent(m_u32DeviceHandle,
                     e8KEY_PRESS, e32DEV_SEARCH, scrUsrCntxt);

            m_poSpiCmdInterface->vSendKeyEvent(m_u32DeviceHandle,
                     e8KEY_RELEASE, e32DEV_SEARCH, scrUsrCntxt);
         }

         else if ((NULL != m_poSpeechHMIClient) && (e8SPI_SPEECH_SPEAKING != enSpeechAppState)
                  && ((e8SR_INACTIVE == m_enSRStatus) || (e8SR_ACTIVE == m_enSRStatus)))
         {
            //Indicate the Device Projection Service that Native Speech Rec needs to be triggered
            /*Initially when no Native SR is active & Android auto SR is not active,the SR status will be Inactive.
             At this point pressing the PTT key should start the Native SR.
             Once the Native SR starts,the SR status becomes Active.
             Then pressing the PTT key should cancel the voice prompt*/
            ETG_TRACE_USR2(("Toggle SR button"));
            m_poSpeechHMIClient->bSendSRButtonEvent(enKeyCode);
         }
      }
   }
}

/***************************************************************************
** FUNCTION   :tVoid devprj_tclKeyConfig::vHandleAAPENDKey(
                         tenKeyMode enKeyMode,tenKeyCode enKeyCode)
***************************************************************************/

t_Void devprj_tclKeyConfig::vHandleAAPENDKey(tenSpeechAppState enSpeechAppState,tenKeyMode enKeyMode,
                                             tenKeyCode enKeyCode,tBool& bSetMute)
{
   ETG_TRACE_USR1(("vHandleAAPENDKey() entered."));

   if ( e8KEY_PRESS == enKeyMode )
   {
      vHandleBTCallOnKey(e8_BT_CALLEND);
      if ( e8_BT_NOACTIVITY == m_enBtActivityStatus )
      {

         if (e8SPI_SPEECH_SPEAKING == enSpeechAppState)
         {
            //! To end Android Speech Rec Session SPI needs to send Both press and release at
            //! one go.

            m_poSpiCmdInterface->vSendKeyEvent(m_u32DeviceHandle, e8KEY_PRESS,
                  e32DEV_SEARCH, scrUsrCntxt);

            m_poSpiCmdInterface->vSendKeyEvent(m_u32DeviceHandle,
                  e8KEY_RELEASE, e32DEV_SEARCH, scrUsrCntxt);
         }
         else if ( e8SR_ACTIVE == m_enSRStatus )
         {
            /**
             * Since no calls were handled, forward the request to native speech rec if
             *  it is active already.
             */
            ETG_TRACE_USR2(("SR is active,trigger SR end"));
            m_poSpeechHMIClient->bSendSRButtonEvent(enKeyCode);
         }
         else
         {
            /**
             * If the END key is pressed when there is no phone call or native speech session,
             * trigger the Global Mute of the AV Manager
             */
            ETG_TRACE_USR2(("SR is Inactive & no calls handled, set global mute"));
            bSetMute = true;
         }
      }
   }
}
/***************************************************************************
   ** FUNCTION: tBool devprj_tclKeyConfig::vHandleBTCallOnEndKey()
***************************************************************************/
t_Void devprj_tclKeyConfig::vHandleBTCallOnKey(tenBTCallAction enBTCallAction)
{
   ETG_TRACE_USR1(("devprj_tclKeyConfig::vHandleBTCallOnKey entered "));

   t_U16 u16CallSize = m_vTelCallStatusList.size();

   ETG_TRACE_USR4(("Number of calls :%d", u16CallSize));

   //! Set the Activity status to No activity
   m_enBtActivityStatus = e8_BT_NOACTIVITY;

   for (std::vector<trTelCallStatusInfo>::reverse_iterator itrCallList =
         m_vTelCallStatusList .rbegin(); itrCallList
         != m_vTelCallStatusList.rend(); ++itrCallList)
      {
         --u16CallSize;

         tenCallStatus enCallStatus = (itrCallList->enCallStatus);

         //! Now check the action to take for each call type.
         switch (enCallStatus)
            {
         case e8IDLE:
            {
               //! Call is idle no action.
               ETG_TRACE_USR4(("Idle Call :%d", u16CallSize));

            }
            break;
         case e8DIALING:
         case e8BUSY:
         case e8IN_VOICEMAIL:
            {
               //! cancel outgoing call in case of END key
               if (m_poTelephoneClient && (e8_BT_CALLHANDLED != m_enBtActivityStatus) &&
                     (e8_BT_CALLEND == enBTCallAction))
                  {
                     ETG_TRACE_USR4(("Trigger Call cancel for :%d", u16CallSize));
                     m_poTelephoneClient->vTriggerCancelCall(u16CallSize);
                     m_enBtActivityStatus = e8_BT_CALLHANDLED;
                  }
               else
               {
                  m_enBtActivityStatus = e8_BT_CALLONGOING;
               }

            }
            break;
         case e8RINGTONE:
            {
               /**
                * Based on whether it is PTT key or PTT end Key
                * handle the phone call behavior.
                */
               if (m_poTelephoneClient && (e8_BT_CALLHANDLED != m_enBtActivityStatus))
               {
                  ETG_TRACE_USR4(("Trigger Call Reject for :%d", u16CallSize));
                  (e8_BT_CALLACCEPT == enBTCallAction) ? (m_poTelephoneClient->vTriggerAcceptCall(u16CallSize))
                     : (m_poTelephoneClient->vTriggerRejectCall(u16CallSize));
                  m_enBtActivityStatus = e8_BT_CALLHANDLED;
               }
               else
               {
                  m_enBtActivityStatus = e8_BT_CALLONGOING;
               }
            }
            break;
         case e8ACTIVE:
         case e8CONFERENCE:
            {

               /**
                *  Do not hangup on all the active
                *  calls at one go.
                */
               if (m_poTelephoneClient && (e8_BT_CALLHANDLED != m_enBtActivityStatus)
                     && (e8_BT_CALLEND == enBTCallAction))
               {
                  ETG_TRACE_USR4(("Trigger Call Hung up for :%d", u16CallSize));
                  m_poTelephoneClient->vTriggerHangUpCall(u16CallSize);
                  m_enBtActivityStatus = e8_BT_CALLHANDLED;
               }
               else
               {
                  m_enBtActivityStatus = e8_BT_CALLONGOING;
               }
            }
            break;
         case e8ON_HOLD:
            {
               /**
                *  Hangup the active ongoing call
                *  if the call was put on hold and the were calls active donot
                *  hangup on one go.Check for previous calls.
                */
               if (m_poTelephoneClient && (e8_BT_CALLHANDLED != m_enBtActivityStatus) &&
                     (e8_BT_CALLEND == enBTCallAction))
               {
                  ETG_TRACE_USR4(("Trigger Call Hung up since call on Hold for :%d", u16CallSize));
                  m_poTelephoneClient->vTriggerHangUpCall(u16CallSize);
                  m_enBtActivityStatus = e8_BT_CALLHANDLED;
               }
               else
               {
                  m_enBtActivityStatus = e8_BT_CALLONGOING;
               }
            }
            break;
         default:
            {
               //! unknown call status
               ETG_TRACE_USR4(("Invalid Call State :%d", u16CallSize));
            }
            }
      }
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
