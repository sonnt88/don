/*!
*******************************************************************************
* \file              devprj_tclMainApp.cpp
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Device Projection main application class
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
16.10.2013 |                              | Initial Version
06.11.2013 |  Ramya Murthy                | Modified for Device Projection class
03.01.2014 |  Hari Priya E R(RBEI/ECP2)   | Included changes for handling 
device selection and launch application
21.01.2014 |  Shiva Kumar G (RBEI/ECP2)   | Trace cmd to set the system date
12.02.2014 |  Hari Priya E R(RBEI/ECP2)   | Included changes for EOL handling
06.04.2014 |  Ramya Murthy                | Initialisation sequence implementation
29.04.2014 |  Shiva Kumar G (RBEI/ECP2)   | Updated with the changes to fetch 
                                            Vehicle Marketing Region EOL Value
25.05.2014 |  Hari Priya E R              | Removed reading EOL value for screen varaint
31.07.2014 |  Ramya Murthy                | SPI feature configuration via LoadSettings()

Note: Initial version taken from SPI Main application class 
\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/

#include <unistd.h>

//@todo - Issue - bpstl ambiguous usage should be removed
#include "devprj_tclMainApp.h"

#include "EolHandler.h"
#include "devprj_tclService.h"
#include "spi_tclConfigReader.h"
#include "spi_tclDefsetHandler.h"
#include "spi_LoopbackTypes.h"

#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

#ifdef VARIANT_S_FTR_ENABLE_PROCSTART_DATAPOOL
   #define DP_S_IMPORT_INTERFACE_BASE
   #include "dp_if.h"
#endif

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
#include "trcGenProj/Header/devprj_tclMainApp.cpp.trc.h"
#endif
#endif

/******************************************************************************
| defines
|----------------------------------------------------------------------------*/
/******************************************************************************/
// Check for availability of SPI TRACE trace input channel defines.
#ifdef TR_TTFIS_SMARTPHONE_INT
// If exists use the existing defines.
#define SPI_TRACE_CHANNEL         TR_TTFIS_SMARTPHONE_INT
#else
// Trace channel is not defined yet.
#define SPI_TRACE_CHANNEL         ((TR_tenTraceChan)178)
#endif

//Initialize the static member variables
spi_tclTrace* devprj_tclMainApp::m_poCmdHndlr = OSAL_NULL;


//static const tU8 cou8DefaultScreenVariant = static_cast<t_U8>(e8COLORSYSTEM8INCH);   //Commented to fix Lint
/******************************************************************************
| GLOBAL VARIABLES
|----------------------------------------------------------------------------*/
/******************************************************************************/

/******************************************************************************
| METHODS
|----------------------------------------------------------------------------*/

/******************************************************************************
** FUNCTION:  devprj_tclMainApp::devprj_tclMainApp();
******************************************************************************/
devprj_tclMainApp::devprj_tclMainApp()
   : ahl_tclBaseOneThreadApp(CCA_C_U16_APP_SMARTPHONEINTEGRATION),
     m_poDevPrjService(OSAL_NULL),
     m_poDefSetHandler(OSAL_NULL)
{
   ETG_TRACE_USR1(("Constructor devprj_tclMainApp() entered \n"));
}  //! end of devprj_tclMainApp()

/******************************************************************************
** FUNCTION:  devprj_tclMainApp::~devprj_tclMainApp();
******************************************************************************/
devprj_tclMainApp::~devprj_tclMainApp()
{
   ETG_TRACE_USR1(("Destructor ~devprj_tclMainApp() entered \n"));
   m_poDevPrjService = OSAL_NULL;
   m_poDefSetHandler = OSAL_NULL;
}  //! end of ~devprj_tclMainApp()

/******************************************************************************
** FUNCTION:  tBool devprj_tclMainApp::bOnInit();
******************************************************************************/
tBool devprj_tclMainApp::bOnInit()
{
   ETG_TRACE_USR1(("devprj_tclMainApp::bOnInit() entered \n"));

   //Handle SIGPIPE Signal. Since SPI component is working with Sockets(to exchange data with ML or Carplay Devices),
   //it would get SIGPIPE signal, whenever the broken pipe issue occurs.

   //Broken Pipe - if other end of the socket(Phone's side in our case) is not able to read the data or socket connection is disconnected
   //our component would get SIGPIPE signal. If this signal is not handled, our component will crash.

   //This might have already handled by Platform. But to be independent of it, we are handling in our component also.
   struct sigaction rSigAct;
   memset (&rSigAct, '\0', sizeof(rSigAct));

   // Use the sa_sigaction field because the handles has two additional parameters 
   rSigAct.sa_sigaction = &devprj_tclMainApp::vSignalHandler;
   // The SA_SIGINFO flag tells sigaction() to use the sa_sigaction field, not sa_handler. 
   rSigAct.sa_flags = SA_SIGINFO;
   if (sigaction(SIGPIPE, &rSigAct, OSAL_NULL) < 0) 
   {
      ETG_TRACE_ERR(("devprj_tclMainApp::bOnInit:Error in registering for Signal Handler:SIGPIPE"));
   }//if (sigaction(SIGPIPE, &rSigAct, OSAL_NULL) < 0) 


   //! Activate the TTFIS command channel
   vPlugTraceIn(TR_TTFIS_SMARTPHONE_INT,(OSAL_tpfCallback)cb_vDefaultHandlerTraceCmds);

   // Add DevPrj Service
   m_poDevPrjService = OSAL_NEW devprj_tclService(this);
   NORMAL_M_ASSERT(OSAL_NULL != m_poDevPrjService);
   
   m_poCmdHndlr = OSAL_NEW spi_tclTrace(this);
   NORMAL_M_ASSERT(OSAL_NULL != m_poCmdHndlr);

   m_poDefSetHandler = new spi_tclDefsetHandler(this);
   NORMAL_M_ASSERT(OSAL_NULL != m_poDefSetHandler);

   tBool bSuccess = ((OSAL_NULL != m_poDevPrjService) &&
         (OSAL_NULL != m_poCmdHndlr) &&
         (OSAL_NULL != m_poDefSetHandler) &&
         (m_poDevPrjService->bInitialize()));

   ETG_TRACE_USR1(("bOnInit() left with result: %u. \n", ETG_ENUM(BOOL,bSuccess)));

   //! Assert added to check return state of bOnInit
   NORMAL_M_ASSERT(true == bSuccess);
   return true;
}//! end of bOnInit()

/******************************************************************************
** FUNCTION:  tVoid devprj_tclMainApp::vOnApplicationClose();
******************************************************************************/
tVoid devprj_tclMainApp::vOnApplicationClose()
{
   ETG_TRACE_USR1(("vOnApplicationClose() entered \n"));

   //! Deactivate the TTFIS command channel
   vUnplugTrace(TR_TTFIS_SMARTPHONE_INT);

   RELEASE_MEM_OSAL(m_poDefSetHandler);
   if (OSAL_NULL != m_poDevPrjService)
   {
      m_poDevPrjService->bUnInitialize();
   }
   RELEASE_MEM_OSAL(m_poDevPrjService);
   RELEASE_MEM_OSAL(m_poCmdHndlr);

   ahl_tclBaseOneThreadApp::vOnApplicationClose();

   ETG_TRACE_USR1(("vOnApplicationClose() left \n"));

}//! end of vOnApplicationClose()

/******************************************************************************
** FUNCTION:  tVoid devprj_tclMainApp::vOnLoadSettings();
******************************************************************************/
tVoid devprj_tclMainApp::vOnLoadSettings()
{
   ETG_TRACE_USR1(("vOnLoadSettings() entered \n"));

   //Read the EOL values
   vReadEOLCalibration();

   //! Read SPI Feature support info
   spi_tclConfigReader* poConfigReader = spi_tclConfigReader::getInstance();
   if ((OSAL_NULL != m_poDevPrjService) && (OSAL_NULL != poConfigReader))
   {
      trSpiFeatureSupport rSpiFeatureSupp;
      poConfigReader->vGetSpiFeatureSupport(rSpiFeatureSupp);
      m_poDevPrjService->vLoadSettings(rSpiFeatureSupp);
   }

   ETG_TRACE_USR1(("vOnLoadSettings() left \n"));

}   //! end of vOnLoadSettings()

/******************************************************************************
** FUNCTION:  tVoid devprj_tclMainApp::vOnSaveSettings();
******************************************************************************/
tVoid devprj_tclMainApp::vOnSaveSettings()
{
   ETG_TRACE_USR1(("vOnSaveSettings() entered \n"));

   if (OSAL_NULL != m_poDevPrjService)
   {
      m_poDevPrjService->vSaveSettings();
   }

   ETG_TRACE_USR1(("vOnSaveSettings() left \n"));

}//! end of vOnSaveSettings()

/******************************************************************************
** FUNCTION:  tVoid devprj_tclMainApp::vOnTimer(tU16 u16TimerId);
******************************************************************************/
tVoid devprj_tclMainApp::vOnTimer(tU16 u16TimerId)
{
   ETG_TRACE_USR1(("vOnTimer(): entered for TimerID 0x%x. \n", u16TimerId));

   if (OSAL_NULL != m_poDevPrjService)
   {
      m_poDevPrjService->vOnTimer(u16TimerId);
   }

   ETG_TRACE_USR1(("vOnTimer() left \n"));

}   //! end of vOnTimer()

/**************************************************************************
** FUNCTION   : tVoid devprj_tclMainApp::vOnEvent(OSAL_tEventMask nEvent)
**************************************************************************/
tVoid devprj_tclMainApp::vOnEvent(OSAL_tEventMask nEvent)
{
   ETG_TRACE_USR1(("vOnEvent(): entered for Event 0x%x. \n", (tU32) nEvent));

   //! Forward requested event to service
   if (OSAL_NULL != m_poDevPrjService)
   {
      switch (nEvent)
      {
         case SPI_C_U32_EVENT_ID_DEFSET_READEOL:
         {
            spi_tclConfigReader* poConfigReader = spi_tclConfigReader::getInstance();
            if (OSAL_NULL != poConfigReader)
            {
               m_poDevPrjService->vSetMLNotificationOnOff(poConfigReader->bGetMLNotificationSetting());
            }
         }//case SPI_C_U32_EVENT_ID_DEFSET_READEOL
            break;
         case SPI_C_U32_EVENT_ID_DEFSET_CLEARPRIVATEDATA:
         {
            m_poDevPrjService->vRestoreSettings();
         }//case SPI_C_U32_EVENT_ID_DEFSET_CLEARPRIVATEDATA
            break;
         case SPI_C_U32_EVENT_ID_DEFSET_TEF:
         case SPI_C_U32_EVENT_ID_DEFSET_CUSTOMER:
            // nothing to be done
         break;
         default:
         {
            ETG_TRACE_ERR(("vOnEvent(): Invalid event: 0x%x! \n", (tU32) nEvent));
         }//default
            break;
      } //switch (nEvent)
   } //if (OSAL_NULL != m_poDevPrjService)
}

/******************************************************************************
** FUNCTION:  tVoid devprj_tclMainApp::cb_vDefaultHandlerTraceCmds(const tU...
******************************************************************************/
tVoid devprj_tclMainApp::cb_vDefaultHandlerTraceCmds(const tU8* pcu8Data)
{
   ETG_TRACE_USR4(("cb_vDefaultHandlerTraceCmds entered"));
   if(OSAL_NULL != m_poCmdHndlr)
   {
      m_poCmdHndlr->vProcessTraceCmd(pcu8Data);
   }
}

/***************************************************************************
** FUNCTION   : devprj_tclMainApp::vReadEOLCalibration()
***************************************************************************/
tVoid devprj_tclMainApp::vReadEOLCalibration()
{
   ETG_TRACE_USR1(("vReadEOLCalibration() entered."));

   EolHandler oEolHandler;    // EOL handler
   
   //Read the vehicle sale region EOL Value
   tU8 u8EOLMarketingRegion = 0xFF;
   if( TRUE == oEolHandler.bRead( EOLLIB_TABLE_ID_COUNTRY, 
      EOLLIB_OFFSET_MARKETING_REGION, 
      &u8EOLMarketingRegion,
      sizeof(tU8)))
   {
      ETG_TRACE_USR4((" Marketing Region - %d ",u8EOLMarketingRegion));
   }//if( TRUE == oEolHandler.bRead( EOLLIB_TABLE_ID_COUNTRY, 

   if (OSAL_NULL!= m_poDevPrjService)
   {      
      m_poDevPrjService->vSetRegion(u8EOLMarketingRegion);
   }//if(OSAL_NULL!= m_poDevPrjService)
}

/***************************************************************************
** FUNCTION   : tVoid devprj_tclMainApp::vOnLoopback(tU16 u16ServiceID,...)
***************************************************************************/
tVoid devprj_tclMainApp::vOnLoopback(tU16 u16ServiceID, amt_tclBaseMessage* poMessage)
{
   ETG_TRACE_USR1(("vOnLoopback() entered."));
   SPI_INTENTIONALLY_UNUSED(u16ServiceID);
   SPI_INTENTIONALLY_UNUSED(poMessage);
}

/***************************************************************************
** FUNCTION   : t_Void devprj_tclMainApp::vSignalHandler()
***************************************************************************/
t_Void devprj_tclMainApp::vSignalHandler(t_S32 s32SigNum, 
                                         siginfo_t *pSiginfo, 
                                         t_Void *pContext)
{
   SPI_INTENTIONALLY_UNUSED(pContext);

   ETG_TRACE_USR1(("devprj_tclMainApp::vSignalHandler:: Received signal %d ",
      ETG_ENUM(SIGNALS,s32SigNum)));

   if(OSAL_NULL != pSiginfo)
   {
      ETG_TRACE_USR2(("devprj_tclMainApp::vSignalHandler:: Signal is received from PID-%d,UID-%d",
         (t_S32)(pSiginfo->si_pid),(t_S32)(pSiginfo->si_uid)));
   }//if(NULL != pSiginfo)

}

#ifdef VARIANT_S_FTR_ENABLE_BUILD_AS_PROCESS

#define EVENT_SHUTDOWN_NAME "SHUTDOWN_FC_SPI"
#define SCD_S_IMPORT_INTERFACE_GENERIC
#include "scd_if.h"
extern "C" OSAL_DECL tS32 vStartApp(tS32 cPar, tString aPar[])
{
   SPI_INTENTIONALLY_UNUSED(cPar);
   SPI_INTENTIONALLY_UNUSED(aPar);

   tS32 bRetVal = OSAL_OK;
   OSAL_tEventHandle  hEvShutdown = 0;
   OSAL_tEventMask    hEvRequest  = 0x00000001;


   et_vTraceOpen();
   et_vTraceBuffer(TR_TTFIS_SMARTPHONE_INT, TR_LEVEL_DATA, 9, "START FC_SPI ");
   scd_init();

   if (OSAL_s32EventCreate(EVENT_SHUTDOWN_NAME, &hEvShutdown) == OSAL_ERROR)
   {
      ETG_TRACE_ERR(("devprj_tclMainApp: Creation of SPM shutdown event failed!!!"));
      NORMAL_M_ASSERT_ALWAYS();
   }

   // Activate Exception Handling
   NORMAL_M_ASSERT(FALSE != exh_bInitExceptionHandling());

   OSAL_tProcessID pId = OSAL_ProcessWhoAmI();
   printf("\n---------------------------------------------------\n");
   printf("FC_SPI [PID: %d (0x%08X)]\n", (int) pId, pId);
   printf("---------------------------------------------------\n");

   #ifdef VARIANT_S_FTR_ENABLE_PROCSTART_DATAPOOL
       DP_vCreateDatapool();
   #endif

   devprj_tclMainApp *pSpiApp = OSAL_NEW devprj_tclMainApp;

   if (pSpiApp != OSAL_NULL)
   {
      if (!pSpiApp->bInitInstance(0, CCA_C_U16_APP_SMARTPHONEINTEGRATION))
      {
         printf("\n\npspiApp->bInitInstance() failed!!!\n");
      } 
      else 
      {
         printf("\n\npspiApp->bInitInstance() success!\n");
      }
   }

   // Wait for Shutdown-Signal    
   OSAL_s32EventWait(hEvShutdown, hEvRequest, OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER, &hEvRequest);
   if (pSpiApp != OSAL_NULL)
   {
      pSpiApp->vDeinitInstance();
      OSAL_DELETE pSpiApp;
   }
   printf("\n\nspi_tclMainApp: Exiting...\n");
   OSAL_s32EventClose(hEvShutdown);
   OSAL_s32EventDelete(EVENT_SHUTDOWN_NAME);
   et_vTraceClose();
   return bRetVal;
}
#endif

///////////////////////////////////////////////////////////////////////////////
// <EOF>

