/***********************************************************************/
/*!
* \file  spi_tclConfigReader.cpp
* \brief Class to get the Variant specific Info for Gen3 Projects
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to get the Variant specific Info for Gen3 Projects
AUTHOR:         Hari Priya E R
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
19.05.2013  | Hari Priya E R        | Initial Version
31.07.2013  | Ramya Murthy          | Implementation for generic config usage and
                                      SPI feature configuration
05.08.2013  | Ramya Murthy          | Added DriveSide info
06.08.2013  | Ramya Murthy          | Added MLNotification setting
28.11.2014  | Ramya Murthy          | Updated SPI Feature support info
16.04.2015  | Ram                   | Implemented reading KDS values for PSA project
08.05.2015  | Shiva Kumar G         | Extended for ML Client Profile configs
30.06.2015  | Tejaswini HB          | SetSpiFeatureEnabled only when feature is supported
03.07.2015  | Shiva Kumar Gurija    | Dynamic display configuration

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <string>
#include <sstream>
#include "Trace.h"
#include "KdsHandler.h"
#include "spi_tclConfigReader.h"
#include "Datapool.h"
#include "AAPTypes.h"
#include "DiPOTypes.h"
#include "RandomIdGenerator.h"


#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
#include "trcGenProj/Header/spi_tclConfigReader.cpp.trc.h"
#endif
#endif

#define FOUR_EIGHTY 480
//Display name "Continental Low Speed" dp_device_name="sharp_psa_rcc_a2_7inch" default values are taken.
//Display width & height in MM derived based on screen dimensions and layer dimensions
static const t_U16 scou16Width = 800;
static const t_U16 scou16Height = 480;
static const t_U16 scou16WidthInMM = 175;
static const t_U16 scou16HeightInMM = 105;

static const t_U16 scou16MLVideoLayerID = 3000;
static const t_U16 scou16MLVideoSurfaceID = 40;
static const t_U16 scou16MLTouchLayerID = 3000;
static const t_U16 scou16MLTouchSurfaceID = 40;
static const t_U16 scou16MLLayerWidth = 800;
static const t_U16 scou16MLLayerHeight = 480;
static const t_U16 scou16MLLayerWidthInMm = 175;
static const t_U16 scou16MLLayerHeightInMm = 105;

static const t_U16 scou16CPVideoLayerID = 3000;
static const t_U16 scou16CPVideoSurfaceID = 40;
static const t_U16 scou16CPTouchLayerID = 4000;
static const t_U16 scou16CPTouchSurfaceID = 80;
static const t_U16 scou16CPLayerWidth = 800;
static const t_U16 scou16CPLayerHeight = 480;
static const t_U16 scou16CPLayerWidthInMm = 175;
static const t_U16 scou16CPLayerHeightInMm = 105;

static const t_U16 scou16AAPVideoLayerID = 3000;
static const t_U16 scou16AAPVideoSurfaceID = 40;
static const t_U16 scou16AAPTouchLayerID = 4000;
static const t_U16 scou16AAPTouchSurfaceID = 80;
static const t_U16 scou16AAPLayerWidth = 800;
static const t_U16 scou16AAPLayerHeight = 480;
static const t_U16 scou16AAPLayerWidthInMm = 175;
static const t_U16 scou16AAPLayerHeightInMm = 105;
static const t_String sczEmptyStr="";

/**************************** SPI feature configurations *******************/
static const t_U32 scou32SPIFeatureKDSEntryPoint = 0xA118;
static const t_U32 scou32SPIFeatureKDSLength = 0x0009;
/* Byte @KDS Key which contains CarPlay supported info */
static const tU8 cou8PSACARPLAY_SUPPORTED_BYTE = 6;
/* Byte @KDS Key which contains Mirrorlink supported info */
static const tU8 cou8PSAMIRRORLINK_SUPPORTED_BYTE = 4;

//Vehicle Brand is available in 2nd Byte 5th-7th Bits.
//Mask for this is 00000111 = 7 - perform & operation with 2nd Byte with this to get brand value
static const t_U8 scou8VehicleBrandBytePos = 2;
static const t_U8 scou8VehicleBrandFromBitPos = 5;
static const t_U8 scou8VehicleBrandMask = 7;
static const t_String sczVehicleBrandPeugeot = "Peugeot";
static const t_String sczVehicleBrandeCitroen = "Citroen";
//HeadUnit Manufacturer Name
static const t_String sczHeadUnitManfacturerName = "Bosch CM";
//HeadUnit Model Name
static const t_String sczHeadUnitModelName = "PSA";
//Vehicle Manufacturer Name
static t_String sczVehicleManfacturerName = "PSA";
//Vehicle Model Name
static t_String sczVehicleModelName = "PSA";
// Vehicle Model Year
static const t_String sczVehicleModelYear = "MY16";

#define PSA_CARPLAY_SUPPORTED_MASK 5 // 5th bit is having Carplay info i.e b00100000
#define PSA_MIRRORLINK_SUPPORTED_MARK 4 // 4th bit is having Mirror link info i.e b00010000


#define PSA_IS_CARPLAY_SUPPORTED(BUFFER)  (0x01 == ((((BUFFER[cou8PSACARPLAY_SUPPORTED_BYTE])>>PSA_CARPLAY_SUPPORTED_MASK)&0x01)))
#define PSA_IS_MIRRORLINK_SUPPORTED(BUFFER) (0x01 == ((((BUFFER[cou8PSAMIRRORLINK_SUPPORTED_BYTE])>>PSA_MIRRORLINK_SUPPORTED_MARK)&0x01)))
#define PSA_VEHICLE_BRAND(BUFFER)  ( ((BUFFER[scou8VehicleBrandBytePos])>>scou8VehicleBrandFromBitPos)&(scou8VehicleBrandMask))

static const t_U32 scou32FctAndroidKDSEntryPoint = 0xA141 ;
static const t_U8 scou8FctAndroidKDSLength = 1 ;
static const t_U8 scou8AndroidAutoSupportInfoByte = 0;
static const t_U8 scou8AndroiAutoSupportBitPos = 7;// 7th bit is having Android Auto info i.e.b10000000

/**************************** SPI feature configurations *******************/

/*************************** Head Unit configurations **********************/
static const t_U32 scou32HUConfigKDSEntryPoint = 0xA167;
static const t_U32 scou32HUConfigKDSLength = 0x006C;

static const t_U8 scou8DevIDMaxLen = 32;
static const t_U8 scou8DevIDStrPos =  9 ;            // From 9th position to 40th Position --> 32 Characters = 256 bits
static const t_U8 scou8ManufacturerNameMaxLen = 32;
static const t_U8 scou8ManufacturerNameStrPos = 73;  // From 73rd position to 104th Position --> 32 Characters = 256 bits
static const t_U8 scou8FriendlyNameMaxLen = 32;
static const t_U8 scou8FriendlyNameStrPos = 41;     // From 41st position to 72nd Position --> 32 Characters = 256 bits

//Default Client ID
static const t_String sczDefaultClientID = "PSA_RCC_A2_MY16";
//Default Client Friendly Name
static const t_String sczClientFriendlyName = "MirrorLink Touchscreen" ;
//Client Manufacturer Name
static const t_String sczClientManfacturerName = "PSA";

/*************************** Head Unit configurations **********************/

/*************************** Region ****************************************/
static const t_U32 scou32RegionConfigKDSEntryPoint = 0xA127;
static const t_U32 scou32RegionConfigKDSLength = 0x0004;

//Region info is available in 0th Byte => 5th-7th Bits.
//Mask for this is 00000111 = 7 - perform & operation with 2nd Byte with this to get brand value
static const t_U8 scou8RegionInfoBytePos = 0;
static const t_U8 scou8RegionInfoFromBitPos = 5;
static const t_U8 scou8RegionInfoMask = 7;
#define PSA_REGION_INFO(BUFFER)  ( ((BUFFER[scou8RegionInfoBytePos])>>scou8RegionInfoFromBitPos)&(scou8RegionInfoMask))

/*!
* \typedef struct rEOLRegionMap
* \brief Structure to map the Calibration values with the respective Region Enumeration code 
*/
typedef struct rRegionMap
{
   //! Calibration Value
   t_U8 u8CalValue;
   //! Equivalent Region enumeration for the EOL Value
   tenRegion enRegion;
}trRegionMap;

/*!
* \brief Mapped the elements as mentioned as per the documents shared with PSA(Mention doc name?).
* All the t_U8 values(0x00 - 0xFF) which are not mentioned here, are by default equivalent to 0xFF - e8WORLD.
* 0xFF is used to check the end of the array. If any new elements needs to be added,
* insert before that
   BTEL_COUNTRY_000 & mapping as per the Excel sheet shared with PSA 
      0 = Europe                    ===> EU : Indicates European Union member states
      1 = Japan                     ===> JPN : Indicates Japan
      2 = South America             ===> AMERICA : Indicates Americas without countries listed separately
      3 = Asia and China            ===> APAC : Indicates APAC states without countries listed separately
      4 = Arabia Middle East        ===> AFRICA :  Indicates African countries without countries listed separately
      5 = United States of America  ===> USA : Indicates USA
      6 = Korea                     ===> KOR : Indicates Korea
      7 = Canada                    ===> CAN : Indicates Canada
*/
static const trRegionMap saRegionMap[]={{0x0,e8_EU},{0x1,e8_JPN},{0x2,e8_AMERICA},{0x3,e8_APAC},
{0x4,e8_AFRICA},{0x5,e8_USA},{0x6,e8_KOR},{0x7,e8_CAN},{0xFF,e8_WORLD}};

/*************************** Region ****************************************/


#define OEM_ICON_NAME ""
#define OEM_ICON_PATH ""
#define ICON_INFO_LENGTH 100


/***************************************************************************
** FUNCTION:  spi_tclConfigReader::spi_tclConfigReader()
***************************************************************************/
spi_tclConfigReader::spi_tclConfigReader():m_szClientID(sczDefaultClientID.c_str()),
m_szFriendlyName(sczClientFriendlyName.c_str()),
m_szManufacturerName(sczClientManfacturerName.c_str()),
m_enDriveModeInfo(e8PARK_MODE),
m_enNightModeInfo(e8_DAY_MODE),
m_u8AAPParkModeRestrictionInfo(0),
m_u8AAPDriveModeRestrictionInfo(0),
m_u8DipoParkModeRestrictionInfo(0),
m_u8DipoDriveModeRestrictionInfo(0)
{
   ETG_TRACE_USR1(("spi_tclConfigReader() entered "));

   //This is done here to avoid reading from data pool multiple times
   //This changes are read in constructor, because either carplayd or SPI process can invoke the 
   //constructor of the class. whichever process comes up first invokes this.
   vReadScreenConfigs();
}

/***************************************************************************
** FUNCTION:  spi_tclConfigReader::~spi_tclConfigReader()
***************************************************************************/
spi_tclConfigReader::~spi_tclConfigReader()
{
   ETG_TRACE_USR1(("~spi_tclConfigReader() entered \n"));
}

/***************************************************************************
** FUNCTION   :t_Void spi_tclConfigReader::vGetVideoConfigData()
***************************************************************************/
t_Void spi_tclConfigReader::vGetVideoConfigData(tenDeviceCategory enDevCat,
                                                trVideoConfigData& rfrVideoConfig)
{
   ETG_TRACE_USR1(("vGetVideoConfigData - %d",ETG_ENUM(DEVICE_CATEGORY,enDevCat)));

   m_oLock.s16Lock();
   rfrVideoConfig.u32ResolutionSupported = FOUR_EIGHTY;
   rfrVideoConfig.u32Screen_Width = m_rDisplayAttributes.u16ScreenWidth;
   rfrVideoConfig.u32Screen_Height = m_rDisplayAttributes.u16ScreenHeight;
   rfrVideoConfig.u32Screen_Width_Millimeter = m_rDisplayAttributes.u16ScreenWidthMm;
   rfrVideoConfig.u32Screen_Height_Millimeter = m_rDisplayAttributes.u16ScreenHeightMm;

   // Read the technology specific configurations
   for(t_U8 u8Index=0; u8Index<m_rDisplayAttributes.vecDisplayLayerAttr.size();u8Index++)
   {
      if(enDevCat == m_rDisplayAttributes.vecDisplayLayerAttr[u8Index].enDevCat)
      {
         const trDisplayLayerAttributes& corfrDispLayerAttr =  m_rDisplayAttributes.vecDisplayLayerAttr[u8Index];
         rfrVideoConfig.u32LayerId  = corfrDispLayerAttr.u16VideoLayerID;
         rfrVideoConfig.u32SurfaceId = corfrDispLayerAttr.u16VideoSurfaceID;
         rfrVideoConfig.u32TouchLayerId = corfrDispLayerAttr.u16TouchLayerID;
         rfrVideoConfig.u32TouchSurfaceId = corfrDispLayerAttr.u16TouchSurfaceID;

         rfrVideoConfig.u32ProjScreen_Width = corfrDispLayerAttr.u16LayerWidth;
         rfrVideoConfig.u32ProjScreen_Height = corfrDispLayerAttr.u16LayerHeight;

         rfrVideoConfig.u32ProjScreen_Width_Mm = corfrDispLayerAttr.u16DisplayWidthMm;
         rfrVideoConfig.u32ProjScreen_Height_Mm = corfrDispLayerAttr.u16DisplayHeightMm;

         //remove these two statements, once HMI implements Screen width & height in MM
         if( (0 == rfrVideoConfig.u32ProjScreen_Width_Mm)&&
            (0 != m_rDisplayAttributes.u16ScreenWidth)&&(0 != m_rDisplayAttributes.u16ScreenWidthMm) )
         {
            rfrVideoConfig.u32ProjScreen_Width_Mm = (t_U32)
               ((float)(corfrDispLayerAttr.u16LayerWidth)/(m_rDisplayAttributes.u16ScreenWidth))*(m_rDisplayAttributes.u16ScreenWidthMm);
         }//if( (0 == rfrVideoConfig.u32ProjSc

         if( (0 == rfrVideoConfig.u32ProjScreen_Height_Mm) &&
            (0 != m_rDisplayAttributes.u16ScreenHeight)&&(0 != m_rDisplayAttributes.u16ScreenHeightMm) )
         {
            rfrVideoConfig.u32ProjScreen_Height_Mm = (t_U32)
               ((float)(corfrDispLayerAttr.u16LayerHeight)/(m_rDisplayAttributes.u16ScreenHeight))*(m_rDisplayAttributes.u16ScreenHeightMm);
         }//if( (0 == rfrVideoConfig.u32ProjScreen_Heig

         break;
      }//if(enDevCat == m_rDisplayAttributes.v
   }// for(t_U8 u8Index=0; u8Index<m_rDisplayAttributes.vecDi
   m_oLock.vUnlock();


   ETG_TRACE_USR4(("Screen Width = %d",rfrVideoConfig.u32Screen_Width));
   ETG_TRACE_USR4(("Screen Height = %d",rfrVideoConfig.u32Screen_Height));
   ETG_TRACE_USR4(("Screen Width(mm) = %d",rfrVideoConfig.u32Screen_Width_Millimeter));
   ETG_TRACE_USR4(("Screen Height(mm) = %d",rfrVideoConfig.u32Screen_Height_Millimeter));
   ETG_TRACE_USR4(("Layer Id = %d",rfrVideoConfig.u32LayerId));
   ETG_TRACE_USR4(("Surface Id = %d",rfrVideoConfig.u32SurfaceId));
   ETG_TRACE_USR4(("Touch Layer Id = %d",rfrVideoConfig.u32TouchLayerId));
   ETG_TRACE_USR4(("Touch Surface Id  = %d",rfrVideoConfig.u32TouchSurfaceId));
   ETG_TRACE_USR4(("Proj Screen Width = %d",rfrVideoConfig.u32ProjScreen_Width));
   ETG_TRACE_USR4(("Proj Screen Height = %d",rfrVideoConfig.u32ProjScreen_Height));
   ETG_TRACE_USR4(("Proj Screen Width(mm) = %d",rfrVideoConfig.u32ProjScreen_Width_Mm));
   ETG_TRACE_USR4(("Proj Screen Height(mm) = %d",rfrVideoConfig.u32ProjScreen_Height_Mm));

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclConfigReader::vGetSpiFeatureSupport(...)
***************************************************************************/
t_Void spi_tclConfigReader::vGetSpiFeatureSupport(trSpiFeatureSupport& rfrSpiFeatureSupport)
{
   //! Set supported SPI features
   KdsHandler oKdsHandler;
   t_U8 u8DataBuf[scou32SPIFeatureKDSLength];
   memset(u8DataBuf, 0, sizeof(u8DataBuf));

   ETG_TRACE_USR1(("spi_tclConfigReader::vGetSpiFeatureSupport"));

   t_Bool bIsDipoSupp = true;
   t_Bool bIsMirrorLinkSupp = true;

   // check if KDS read is successful
   if(oKdsHandler.bReadData(scou32SPIFeatureKDSEntryPoint,scou32SPIFeatureKDSLength, u8DataBuf))
   {
      bIsDipoSupp = PSA_IS_CARPLAY_SUPPORTED(u8DataBuf);
      bIsMirrorLinkSupp = PSA_IS_MIRRORLINK_SUPPORTED(u8DataBuf);
      ETG_TRACE_USR4(("vGetSpiFeatureSupport: Read KDS ML value = %d \n",
               ETG_ENUM(BOOL, bIsMirrorLinkSupp)));
      ETG_TRACE_USR4(("vGetSpiFeatureSupport: Read KDS DIPO value = %d \n",
               ETG_ENUM(BOOL, bIsDipoSupp)));
   }

   t_Bool bIsAndroidAutoSupp = bIsAndroidAutoSupported();

      //! Add DiPO support
   rfrSpiFeatureSupport.vSetDipoSupport(bIsDipoSupp);

      //! Add MirrorLink support
#ifdef VARIANT_S_FTR_ENABLE_SPI_MIRRORLINK
   rfrSpiFeatureSupport.vSetMirrorLinkSupport(bIsMirrorLinkSupp);
   ETG_TRACE_USR4(("vGetSpiFeatureSupport: ML Feature supported? %d \n",
	   ETG_ENUM(BOOL, bIsMirrorLinkSupp)));
#else
   rfrSpiFeatureSupport.vSetMirrorLinkSupport(false);
   ETG_TRACE_USR1(("spi_tclConfigReader::vGetSpiFeatureSupport: Mirrorlink supported = FALSE \n"));
#endif

#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO

   rfrSpiFeatureSupport.vSetAndroidAutoSupport(bIsAndroidAutoSupp); 

   ETG_TRACE_USR1(("spi_tclConfigReader::vGetSpiFeatureSupport: Android Auto supported = %d",
      ETG_ENUM(BOOL,bIsAndroidAutoSupp)));

#else

   //by default set the support to Android Auto false, if it is not enable in the build.
   rfrSpiFeatureSupport.vSetAndroidAutoSupport(false);
   ETG_TRACE_USR1(("spi_tclConfigReader::vGetSpiFeatureSupport: Android Auto supported = FALSE "));

#endif

   // In PSA project, ACL_CEDC is not reading the Diag values from KDS. The responsibility is given to SPI_Server
   // HMI(external supplier)needs the data about ML/DIPO - ON/OFF, which ACL_CEDC needs to provide.
   // The FI to get ML/DIPO from spi server is through getDeviceusagePreferance. 
   // However getDeviceUsagePreference functionality is through DP.
   // Hence ML & DIPO DP values are populated based on the Diagnosis interface from KDS.
   vSetDPValuesBasedOnDiagValue(bIsDipoSupp,bIsMirrorLinkSupp,bIsAndroidAutoSupp);

   ETG_TRACE_USR4(("vGetSpiFeatureSupport: ML Feature supported? %d ",
            ETG_ENUM(BOOL, bIsMirrorLinkSupp)));
   ETG_TRACE_USR4(("vGetSpiFeatureSupport: DIPO Feature supported? %d ",
            ETG_ENUM(BOOL, bIsDipoSupp)));
   ETG_TRACE_USR4(("vGetSpiFeatureSupport: ANDROID AUTO Feature supported? %d ",
            ETG_ENUM(BOOL, bIsAndroidAutoSupp)));
}
/***************************************************************************
** FUNCTION:  t_Void spi_tclConfigReader::vSetDPValuesBasedOnDiagValue(...)
***************************************************************************/
t_Void spi_tclConfigReader::vSetDPValuesBasedOnDiagValue(t_Bool bIsDipoSupp,
                                                         t_Bool bIsMirrorLinkSupp,
                                                         t_Bool bAndroidAutoSupport)
{
    Datapool oDatapool;
    ETG_TRACE_USR1(("vSetDPValuesBasedOnDiagValue() entered."));

    tenEnabledInfo enDPDipoConfig = oDatapool.bReadDipoEnableSetting();
    tenEnabledInfo enDPMLConfig = oDatapool.bReadMLEnableSetting();
    tenEnabledInfo enDPAAPConfig = oDatapool.bReadAAPEnableSetting();

    ETG_TRACE_USR4(("vSetDPValuesBasedOnDiagValue:Current DiPo Datapool value = %d",ETG_ENUM(ENABLED_INFO, enDPDipoConfig)));
    ETG_TRACE_USR4(("vSetDPValuesBasedOnDiagValue:Current ML Datapool value = %d",ETG_ENUM(ENABLED_INFO, enDPMLConfig)));
    ETG_TRACE_USR4(("vSetDPValuesBasedOnDiagValue:Current Android Auto Datapool value = %d",ETG_ENUM(ENABLED_INFO, enDPAAPConfig)));

    if((e8USAGE_ENABLED == enDPDipoConfig)!=bIsDipoSupp)
    {  //Store the setting in data pool only if it is not same as Diag values.
       if(false == oDatapool.bWriteDipoEnableSetting(enDPDipoConfig))
       {
          ETG_TRACE_ERR(("Error in writing DiPo Enable Setting to datapool"));
       }//if(false == oDatapool.bWrit
    }

    if((e8USAGE_ENABLED == enDPMLConfig)!=bIsMirrorLinkSupp)
    {  //Store the setting in data pool only if it is not same as Diag values.
       if(false == oDatapool.bWriteMLEnableSetting(enDPMLConfig))
       {
          ETG_TRACE_ERR(("Error in writing ML Enable Setting to datapool"));
       }//if(false == oDatapool.bWrit
    }

    if((e8USAGE_ENABLED == enDPAAPConfig)!= bAndroidAutoSupport)
    {  //Store the setting in data pool only if it is not same as Diag values.
       if(false == oDatapool.bWriteAAPEnableSetting(enDPAAPConfig))
       {
          ETG_TRACE_ERR(("Error in writing AAP Enable Setting to datapool"));
       }//if(false == oDatapool.bWrit
    }
}

/***************************************************************************
** FUNCTION:  tenDriveSideInfo spi_tclConfigReader::enGetDriveSideInfo(...)
***************************************************************************/
tenDriveSideInfo spi_tclConfigReader::enGetDriveSideInfo()
{
   //! Initialize return value to default.
   tenDriveSideInfo enDriveSideInfo = e8LEFT_HAND_DRIVE;

   Datapool oDatapool;
   oDatapool.bReadDriveSideInfo(enDriveSideInfo);

   ETG_TRACE_USR1(("spi_tclConfigReader::enGetDriveSideInfo() left with enDriveSideInfo = %d \n",
         ETG_ENUM(DRIVE_SIDE_TYPE, enDriveSideInfo)));

   return enDriveSideInfo;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclConfigReader::vSetFeatureRestrictions(...)
 ***************************************************************************/
t_Void spi_tclConfigReader::vSetFeatureRestrictions(tenDeviceCategory enDevCategory,
      const t_U8 cou8ParkModeRestrictionInfo, const t_U8 cou8DriveModeRestrictionInfo)
{
   ETG_TRACE_USR1(("spi_tclConfigReader::vSetFeatureRestrictions() Entered with Device Category = %d",
         ETG_ENUM( DEVICE_CATEGORY, enDevCategory)));

  if (enDevCategory== e8DEV_TYPE_ANDROIDAUTO)
  {
      m_u8AAPParkModeRestrictionInfo = (cou8ParkModeRestrictionInfo & (0x1F));
      m_u8AAPDriveModeRestrictionInfo = (cou8DriveModeRestrictionInfo & (0x1F));

      ETG_TRACE_USR1(("[PARAM]::vSetFeatureRestrictions() After Masking : AAP Park Mode Restriction = %d, Drive Mode Restriction = %d ",
            m_u8AAPParkModeRestrictionInfo, m_u8AAPDriveModeRestrictionInfo));
  }
  else if (enDevCategory== e8DEV_TYPE_DIPO)
  {
     //! Not required to keep this information since CarPlay cannot support
     // two different values for feature lockout.
     m_u8DipoParkModeRestrictionInfo=cou8ParkModeRestrictionInfo;

     //! Write the data to data pool for future use
     Datapool oDatapool;
     t_Bool bStatus = oDatapool.bWriteDiPODriveRestrictionInfo(cou8DriveModeRestrictionInfo);
     tenResponseCode enResponseCode = (false == bStatus) ? e8FAILURE:e8SUCCESS;
     ETG_TRACE_USR1(("Set Drive restriction is completed with status : %d ", enResponseCode));
   }
}

/***************************************************************************
 ** FUNCTION:  t_U8 spi_tclConfigReader::u8GetAAPParkRestrictionInfo()
 ***************************************************************************/
t_U8 spi_tclConfigReader::u8GetAAPParkRestrictionInfo()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::u8GetAAPParkRestrictionInfo : "
         "AAP Park Mode Restriction = %d ", m_u8AAPParkModeRestrictionInfo));
   return m_u8AAPParkModeRestrictionInfo;
}
/***************************************************************************
 ** FUNCTION:  t_U8 spi_tclConfigReader::u8GetAAPDriveRestrictionInfo(...)
 ***************************************************************************/
t_U8 spi_tclConfigReader::u8GetAAPDriveRestrictionInfo()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::u8GetAAPDriveRestrictionInfo : "
         "AAP Drive Mode Restriction = %d ", m_u8AAPDriveModeRestrictionInfo));
   return m_u8AAPDriveModeRestrictionInfo;
}

/***************************************************************************
 ** FUNCTION:  t_U8 spi_tclConfigReader::u8GeDiPOParkRestrictionInfo(...)
 ***************************************************************************/
t_U8 spi_tclConfigReader::u8GeDiPOParkRestrictionInfo()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::u8GeDiPOParkRestrictionInfo "
         "DiPO Park Mode Restriction = %d ", m_u8DipoParkModeRestrictionInfo));
  return m_u8DipoParkModeRestrictionInfo;
}

/***************************************************************************
 ** FUNCTION:  t_U8 spi_tclConfigReader::u8GeDiPODriveRestrictionInfo(...)
 ***************************************************************************/
t_U8 spi_tclConfigReader::u8GeDiPODriveRestrictionInfo()
{
	ETG_TRACE_USR1(("spi_tclConfigReader::u8GeDiPODriveRestrictionInfo "));
	Datapool oDatapool;
	t_U8 u8DriveRestInfo;
	oDatapool.bReadDiPODriveRestrictionInfo(u8DriveRestInfo);
	return u8DriveRestInfo;
}
/***************************************************************************
** FUNCTION:  t_Bool spi_tclConfigReader::bGetMLNotificationSetting()
***************************************************************************/
t_Bool spi_tclConfigReader::bGetMLNotificationSetting()
{
   t_Bool bMLNotifOnOff = false; //default setting
   
   //TODO - Read ML Notification setting from calibration?   

   ETG_TRACE_USR2(("spi_tclConfigReader::bGetMLNotificationSetting() left with MLNotification = %d \n",
         ETG_ENUM(BOOL, bMLNotifOnOff)));
   return bMLNotifOnOff;
}

/***************************************************************************
** FUNCTION   :t_Void spi_tclConfigReader::vGetOemIconData(
              trVehicleBrandInfo& rfrVehicleBrandInfo)
***************************************************************************/
t_Void spi_tclConfigReader::vGetOemIconData(trVehicleBrandInfo& rfrVehicleBrandInfo)
{
   //Not Implemented
   memset(rfrVehicleBrandInfo.szOemIconPath, 0, ICON_INFO_LENGTH);
   memset(rfrVehicleBrandInfo.szOemName, 0, ICON_INFO_LENGTH);
   memset(rfrVehicleBrandInfo.szManufacturer, 0, ICON_INFO_LENGTH);
   memset(rfrVehicleBrandInfo.szModel, 0, ICON_INFO_LENGTH);
   strncpy(rfrVehicleBrandInfo.szOemIconPath, OEM_ICON_PATH , ICON_INFO_LENGTH);
   strncpy(rfrVehicleBrandInfo.szOemName, OEM_ICON_NAME , ICON_INFO_LENGTH);
   strncpy(rfrVehicleBrandInfo.szManufacturer, sczClientManfacturerName.c_str() , sczClientManfacturerName.length());
   strncpy(rfrVehicleBrandInfo.szModel, sczDefaultClientID.c_str() , sczDefaultClientID.length());

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclConfigReader::vReadSysConfigs()
***************************************************************************/
t_Void spi_tclConfigReader::vReadSysConfigs()
{
   KdsHandler oKdsHandler;
   t_U8 u8DataBuf[scou32HUConfigKDSLength];
   memset(u8DataBuf, 0, sizeof(u8DataBuf));
   t_String szManufacturerName;

   ETG_TRACE_USR1(("spi_tclConfigReader::vReadSysConfigs()"));

   // check if KDS read is successful
   if(oKdsHandler.bReadData(scou32HUConfigKDSEntryPoint,scou32HUConfigKDSLength, u8DataBuf))
   {
      //Device ID of the System -  [9,0] -> [40,7] (Bit len: 256 = 32 Characters)
      //9th Byte 0th Bit to 40th Byte 7th Bit position
      if('\0' != u8DataBuf[scou8DevIDStrPos])
      {
         m_szClientID = szExtractStr(u8DataBuf,scou8DevIDStrPos,scou8DevIDMaxLen).c_str();
         ETG_TRACE_USR4(("spi_tclConfigReader::vReadSysConfigs: Device ID-%s",
            m_szClientID.c_str()));
      }//if('\0' != u8DataBuf[scou8DevIDStrPos])
      else
      {
         ETG_TRACE_ERR(("spi_tclConfigReader::vReadSysConfigs-Error in reading Device ID"));
      }

      //Friendly Name of the System -  [41,0] -> [72,7] (Bit len: 256 = 32 Characters)
      //41st Byte 0th Bit to 72nd Byte 7th Bit position
      if('\0' != u8DataBuf[scou8FriendlyNameStrPos])
      {
         m_szFriendlyName = szExtractStr(u8DataBuf,scou8FriendlyNameStrPos,scou8FriendlyNameMaxLen).c_str();
         ETG_TRACE_USR4(("spi_tclConfigReader::vReadSysConfigs: Friendly Name-%s",
            m_szFriendlyName.c_str()));
      }//if('\0' != u8DataBuf[scou8FriendlyNameStrPos])
      else
      {
         ETG_TRACE_ERR(("spi_tclConfigReader::vReadSysConfigs-Error in reading Friendly Name"));
      }//if('\0' != u8DataBuf[scou8FriendlyNameStrPos])

      //Manufacturer Name of the System - [73,0] -> [104,7] (Bit len: 256 = 32 Characters)
      //73rd Byte 0th Bit to 104th Byte 7th Bit position
      if('\0' != u8DataBuf[scou8ManufacturerNameStrPos])
      {
         szManufacturerName = szExtractStr(u8DataBuf,scou8ManufacturerNameStrPos,
            scou8ManufacturerNameMaxLen).c_str();
         m_szManufacturerName = szManufacturerName.c_str();
         ETG_TRACE_USR4(("spi_tclConfigReader::vReadSysConfigs: Manufacturer Name-%s",
            szManufacturerName.c_str()));
      }//if('\0' != u8DataBuf[scou8ManufacturerNameStrPos])
      else
      {
         ETG_TRACE_ERR(("spi_tclConfigReader::vReadSysConfigs-Error in reading Manufacturer Name"));
      }//if(true == bReadVehicleBrand(szVehicleBrand))
   }//if(oKdsHandler.bReadData(scou32H
   else
   {
      ETG_TRACE_ERR(("spi_tclConfigReader::vReadSysConfigs:Error in reading KDS data"));
   }

   //If the manufacture name is empty, get the vehicle brand and map the manufacturer name as per the doc.
   if(sczEmptyStr == szManufacturerName)
   {
      //Read the data present at BTEL_DAT_DG_BRAND_CFG_000
      if(true == bReadVehicleBrand(szManufacturerName))
      {
         m_szManufacturerName = szManufacturerName.c_str();
         ETG_TRACE_USR4(("spi_tclConfigReader::vReadSysConfigs: Manufacturer Name-%s",
            m_szManufacturerName.c_str()));
      }//if(true == bReadVehicleBrand(sz
   }//if(sczEmptyStr == szManufacturerName)

}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclConfigReader::bReadVehicleBrand(t_String&...
***************************************************************************/
t_Bool spi_tclConfigReader::bReadVehicleBrand(t_String& rfszVehicleBrand)
{
   ETG_TRACE_USR1(("spi_tclConfigReader::bReadVehicleBrand"));
   t_Bool bRet=false;

   KdsHandler oKdsHandler;
   t_U8 u8DataBuf[scou32SPIFeatureKDSLength];
   memset(u8DataBuf, 0, sizeof(u8DataBuf));
   // check if KDS read is successful
   if(oKdsHandler.bReadData(scou32SPIFeatureKDSEntryPoint,scou32SPIFeatureKDSLength, u8DataBuf))
   {

      ETG_TRACE_USR4(("spi_tclConfigReader::bReadVehicleBrand:Byte->%d  Value->%d",
         scou8VehicleBrandBytePos,u8DataBuf[scou8VehicleBrandBytePos]));

      //2nd Byte 5th Bit  to 2nd Byte 7th Bit
      //For Ex: if the value is Citreon (1) , KDS will have the data like this => 00100000 
      //Consider first 3bits 001 => 1 is at 5th position (Little Endian)
      t_U8 u8VehicleBrand = PSA_VEHICLE_BRAND(u8DataBuf);
      ETG_TRACE_USR4(("spi_tclConfigReader::bReadVehicleBrand-%d",u8VehicleBrand));

      switch(u8VehicleBrand)
      {
      case 0:
         {
            rfszVehicleBrand = sczVehicleBrandPeugeot.c_str();
            bRet=true;
         }
         break;
      case 1:
         {
            rfszVehicleBrand = sczVehicleBrandeCitroen.c_str();
            bRet=true;
         }
         break;
      default:
         {
            ETG_TRACE_USR4(("spi_tclConfigReader::bReadVehicleBrand- Default value will be taken"));
         }
      }//switch(u8VehicleBrand)
   }//if(oKdsHandler.bReadData(scou32SPIFeatureKDSEntryPoi
   else
   {
      ETG_TRACE_ERR(("spi_tclConfigReader::bReadVehicleBrand:Error in reading KDS data"));
   }

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetClientID()
***************************************************************************/
t_String spi_tclConfigReader::szGetClientID()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::szGetClientID() left with ClientID = %s",
      m_szClientID.c_str()));

   return m_szClientID.c_str();
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetClientFriendlyName()
***************************************************************************/
t_String spi_tclConfigReader::szGetClientFriendlyName()
{

   ETG_TRACE_USR1(("spi_tclConfigReader::szGetClientFriendlyName() left with FriendlyName = %s",
      m_szFriendlyName.c_str()));

   return m_szFriendlyName.c_str();
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetClientManufacturerName()
***************************************************************************/
t_String spi_tclConfigReader::szGetClientManufacturerName()
{
   return m_szManufacturerName.c_str();
}
/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetModelYear()
***************************************************************************/
t_String spi_tclConfigReader::szGetModelYear()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::szGetModelYear() left with Model Year= %s",
         sczVehicleModelYear.c_str()));
   return sczVehicleModelYear;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetHeadUnitManufacturerName()
***************************************************************************/
t_String spi_tclConfigReader::szGetHeadUnitManufacturerName()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::szGetHeadUnitManufacturerName() left with Head unit manufacturer name = %s",
      sczHeadUnitManfacturerName.c_str()));
   return sczHeadUnitManfacturerName;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetHeadUnitModelName()
***************************************************************************/
t_String spi_tclConfigReader::szGetHeadUnitModelName()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::szGetHeadUnitModelName() left with Head unit model name = %s",
      sczHeadUnitModelName.c_str()));
   return sczHeadUnitModelName;
}

/***************************************************************************
** FUNCTION:  tenRegion spi_tclConfigReader::enGetRegion()
***************************************************************************/
tenRegion spi_tclConfigReader::enGetRegion()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::enGetRegion() "));
   tenRegion enRegion = e8_WORLD;
   KdsHandler oKdsHandler;
   t_U8 u8DataBuf[scou32RegionConfigKDSLength];
   memset(u8DataBuf, 0, sizeof(u8DataBuf));

   // check if KDS read is successful
   if(oKdsHandler.bReadData(scou32RegionConfigKDSEntryPoint,scou32RegionConfigKDSLength, u8DataBuf))
   {
      ETG_TRACE_USR4(("spi_tclConfigReader::enGetRegion:Byte->%d  Value->%d",
         scou8RegionInfoBytePos,u8DataBuf[scou8RegionInfoBytePos]));

      //Region Info is available in 0th Byte 5th-7th Bit =>[0,5] -> [0,7] (Bitlen: 3)
      t_U8 u8Val =  PSA_REGION_INFO(u8DataBuf);
      //read the actual region that needs to be set on the Phones
      for(t_U8 u8Index=0;saRegionMap[u8Index].u8CalValue != 0xFF;u8Index++)
      {
         if(saRegionMap[u8Index].u8CalValue == u8Val)
         {
            enRegion = saRegionMap[u8Index].enRegion;
            break;
         }// if(saRegionMap[u8Index].u8EOLValue 
      }//for(tU8 u8Index=0;saRegionMap[u8Index]

      ETG_TRACE_USR4(("spi_tclConfigReader::enGetRegion-%d ",enRegion));
   }

   return enRegion;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szExtractStr(t_String&...
***************************************************************************/
t_String spi_tclConfigReader::szExtractStr(t_U8 u8Data[],t_U8 u8FromPos,t_U8 u8Len)
{
   ETG_TRACE_USR1(("spi_tclConfigReader::szExtractStr"));
   t_String szOutput;
   std::stringstream szStr;
   t_Char* arr=new t_Char[u8Len];
   if(NULL != arr)
   {
      memcpy(arr,u8Data+u8FromPos,u8Len);
      for(t_U8 u8Index=0;u8Index<u8Len;u8Index++)
      {
         szStr<<arr[u8Index];
      }//for(t_U8 u8Index=0;u8Index<u8Len;u8Index++)
      szOutput=szStr.str();

      delete[] arr;
   }//if(NULL != arr)
   ETG_TRACE_USR1(("spi_tclConfigReader::szExtractStr-%s",szOutput.c_str()));

   return szOutput.c_str();

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclConfigReader::vReadScreenConfigs()
***************************************************************************/
t_Void spi_tclConfigReader::vReadScreenConfigs()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::vReadScreenConfigs"));

   Datapool oDatapool;
   trDisplayAttributes rDispAttr;

   t_Bool bResult = oDatapool.bReadScreenAttributes(rDispAttr);
   if((true == bResult)&&(0 != rDispAttr.u16ScreenWidth))
   {
      m_oLock.s16Lock();
      //Read is successful and there are valid values in Data pool
      m_rDisplayAttributes = rDispAttr ;
      m_oLock.vUnlock();
   }//if(true != oDatapool.bReadScreenAttributes(rDispAttr))
   else
   {
      m_oLock.s16Lock();

      //There are no values/Invalid values in Datapool, populate with the default values
      //(currently populated with default values).
      m_rDisplayAttributes.u16ScreenWidth = scou16Width;
      m_rDisplayAttributes.u16ScreenHeight = scou16Height;
      m_rDisplayAttributes.u16ScreenWidthMm = scou16WidthInMM;
      m_rDisplayAttributes.u16ScreenHeightMm = scou16HeightInMM;

      trDisplayLayerAttributes rMLDispLayerAttr;
      rMLDispLayerAttr.enDevCat = e8DEV_TYPE_MIRRORLINK;
      rMLDispLayerAttr.u16VideoLayerID = scou16MLVideoLayerID;
      rMLDispLayerAttr.u16VideoSurfaceID = scou16MLVideoSurfaceID;
      rMLDispLayerAttr.u16TouchLayerID = scou16MLTouchLayerID;
      rMLDispLayerAttr.u16TouchSurfaceID = scou16MLTouchSurfaceID;
      rMLDispLayerAttr.u16LayerWidth = scou16MLLayerWidth;
      rMLDispLayerAttr.u16LayerHeight = scou16MLLayerHeight;
      rMLDispLayerAttr.u16DisplayWidthMm = scou16MLLayerWidthInMm;
      rMLDispLayerAttr.u16DisplayHeightMm = scou16MLLayerHeightInMm;
      m_rDisplayAttributes.vecDisplayLayerAttr.push_back(rMLDispLayerAttr);

      trDisplayLayerAttributes rDiPoDispLayerAttr;
      rDiPoDispLayerAttr.enDevCat = e8DEV_TYPE_DIPO;
      rDiPoDispLayerAttr.u16VideoLayerID = scou16CPVideoLayerID;
      rDiPoDispLayerAttr.u16VideoSurfaceID = scou16CPVideoSurfaceID;
      rDiPoDispLayerAttr.u16TouchLayerID = scou16CPTouchLayerID;
      rDiPoDispLayerAttr.u16TouchSurfaceID = scou16CPTouchSurfaceID;
      rDiPoDispLayerAttr.u16LayerWidth = scou16CPLayerWidth;
      rDiPoDispLayerAttr.u16LayerHeight = scou16CPLayerHeight;
      rDiPoDispLayerAttr.u16DisplayWidthMm = scou16CPLayerWidthInMm;
      rDiPoDispLayerAttr.u16DisplayHeightMm = scou16CPLayerHeightInMm;
      m_rDisplayAttributes.vecDisplayLayerAttr.push_back(rDiPoDispLayerAttr);

      trDisplayLayerAttributes rAAPDispLayerAttr;
      rAAPDispLayerAttr.enDevCat = e8DEV_TYPE_ANDROIDAUTO;
      rAAPDispLayerAttr.u16VideoLayerID = scou16AAPVideoLayerID;
      rAAPDispLayerAttr.u16VideoSurfaceID = scou16AAPVideoSurfaceID;
      rAAPDispLayerAttr.u16TouchLayerID = scou16AAPTouchLayerID;
      rAAPDispLayerAttr.u16TouchSurfaceID = scou16AAPTouchSurfaceID;
      rAAPDispLayerAttr.u16LayerWidth = scou16AAPLayerWidth;
      rAAPDispLayerAttr.u16LayerHeight = scou16AAPLayerHeight;
      rAAPDispLayerAttr.u16DisplayWidthMm = scou16AAPLayerWidthInMm;
      rAAPDispLayerAttr.u16DisplayHeightMm = scou16AAPLayerHeightInMm;
      m_rDisplayAttributes.vecDisplayLayerAttr.push_back(rAAPDispLayerAttr);

      m_oLock.vUnlock();

   }//else
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclConfigReader::vSetScreenConfigs()
***************************************************************************/
t_Void spi_tclConfigReader::vSetScreenConfigs(const trDisplayAttributes& corfrDispLayerAttr)
{
   ETG_TRACE_USR1(("spi_tclConfigReader::vSetScreenConfigs"));


   ETG_TRACE_USR4(("Screen Width = %d",corfrDispLayerAttr.u16ScreenWidth));
   ETG_TRACE_USR4(("Screen Height = %d",corfrDispLayerAttr.u16ScreenHeight));
   ETG_TRACE_USR4(("Screen Width(mm) = %d",corfrDispLayerAttr.u16ScreenWidthMm));
   ETG_TRACE_USR4(("Screen Height(mm) = %d",corfrDispLayerAttr.u16ScreenHeightMm));
   for(t_U8 u8Index=0;u8Index<corfrDispLayerAttr.vecDisplayLayerAttr.size();u8Index++)
   {
      ETG_TRACE_USR4(("------------- %d Screen Configuration ----------",
         ETG_ENUM(DEVICE_CATEGORY,corfrDispLayerAttr.vecDisplayLayerAttr[u8Index].enDevCat)));

      ETG_TRACE_USR4(("Layer Id = %d",corfrDispLayerAttr.vecDisplayLayerAttr[u8Index].u16VideoLayerID));
      ETG_TRACE_USR4(("Surface Id = %d",corfrDispLayerAttr.vecDisplayLayerAttr[u8Index].u16VideoSurfaceID));
      ETG_TRACE_USR4(("Touch Layer Id = %d",corfrDispLayerAttr.vecDisplayLayerAttr[u8Index].u16TouchLayerID));
      ETG_TRACE_USR4(("Touch Surface Id  = %d",corfrDispLayerAttr.vecDisplayLayerAttr[u8Index].u16TouchSurfaceID));
      ETG_TRACE_USR4(("Proj Screen Width = %d",corfrDispLayerAttr.vecDisplayLayerAttr[u8Index].u16LayerWidth));
      ETG_TRACE_USR4(("Proj Screen Height = %d",corfrDispLayerAttr.vecDisplayLayerAttr[u8Index].u16LayerHeight));
      ETG_TRACE_USR4(("Proj Screen Width(mm) = %d",corfrDispLayerAttr.vecDisplayLayerAttr[u8Index].u16DisplayWidthMm));
      ETG_TRACE_USR4(("Proj Screen Height(mm) = %d",corfrDispLayerAttr.vecDisplayLayerAttr[u8Index].u16DisplayHeightMm));
   } //for(t_U8 u8Index=0;u8Index<corfrDispLayerAttr.

   m_oLock.s16Lock();
   //Set the screen configurations in data pool, if there is any change in the current values
   if(m_rDisplayAttributes != corfrDispLayerAttr)
   {
      Datapool oDatapool;
      if(true != oDatapool.bWriteScreenAttributes(corfrDispLayerAttr))
      {
         ETG_TRACE_ERR(("spi_tclConfigReader::vSetScreenConfigs: Error in setting values in datapool"));
      }//if(true != oDatapool.

      //Update the member variables of this class, to use in current power cycle.
      m_rDisplayAttributes = corfrDispLayerAttr;
   }//if(corfrDispLayerAttr != m_rDisplayAttributes)
   m_oLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclConfigReader::bGetRotaryCtrlSupport()
***************************************************************************/
t_Bool spi_tclConfigReader::bGetRotaryCtrlSupport()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::vSetScreenConfigs"));

   t_Bool bIsRotCtrlSupported = false;
   // By default Rotary Controller is disabled
   return bIsRotCtrlSupported;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetSoftwareVersion()
***************************************************************************/
t_String spi_tclConfigReader::szGetSoftwareVersion()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::szGetSoftwareVersion "));
   //! TODO : Check interface to get software version
   t_String szSoftwareVersion = "";
   return szSoftwareVersion;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vSetDriveModeInfo()
***************************************************************************/
t_Void spi_tclConfigReader::vSetDriveModeInfo(const tenVehicleConfiguration enDriveModeInfo)
{
	//The data will access from different threads. Hence locked before use.
	m_oLock.s16Lock();
	m_enDriveModeInfo = enDriveModeInfo;
	m_oLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vGetDriveModeInfo()
***************************************************************************/
t_Void spi_tclConfigReader::vGetDriveModeInfo(tenVehicleConfiguration &rfoenDriveModeInfo)
{
	m_oLock.s16Lock();
	rfoenDriveModeInfo =  m_enDriveModeInfo;
	m_oLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vSetNightModeInfo()
***************************************************************************/
t_Void spi_tclConfigReader::vSetNightModeInfo(const tenVehicleConfiguration enNightModeInfo)
{
	m_oLock.s16Lock();
	m_enNightModeInfo = enNightModeInfo;
	m_oLock.vUnlock();

}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vGetNightModeInfo()
***************************************************************************/
t_Void spi_tclConfigReader::vGetNightModeInfo(tenVehicleConfiguration &rfoenNightModeInfo)
{
	m_oLock.s16Lock();
	rfoenNightModeInfo = m_enNightModeInfo;
	m_oLock.vUnlock();

}
/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vSetDriveSideInfo()
***************************************************************************/
t_Void spi_tclConfigReader::vSetDriveSideInfo(const tenVehicleConfiguration enVehicleConfig)
{
	Datapool oDatapool;
	tenDriveSideInfo enDriveSideInfo = (e8_RIGHT_HAND_DRIVE != enVehicleConfig)?e8LEFT_HAND_DRIVE : e8RIGHT_HAND_DRIVE;
	oDatapool.bWriteDriveSideInfo(enDriveSideInfo);
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vGetDisplayInputParam()
***************************************************************************/
t_Void spi_tclConfigReader::vGetDisplayInputParam(t_U8 &rfu8DisplayInput)
{
   rfu8DisplayInput = (true == bGetRotaryCtrlSupport())?(HIGH_FIDELITY_TOUCH_WITH_KNOB):(HIGH_FIDELITY_TOUCH);

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclConfigReader::vGetVehicleInfoDataAAP(trVehicleInfo& rfrVehicleInfo)
***************************************************************************/

t_Void spi_tclConfigReader::vGetVehicleInfoDataAAP(trVehicleInfo& rfrVehicleInfo)
{
   rfrVehicleInfo.szModel = sczVehicleModelName;
   rfrVehicleInfo.szManufacturer = sczVehicleManfacturerName;
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclConfigReader::vGetDefaultProjUsageSettings
 ***************************************************************************/
t_Void spi_tclConfigReader::vGetDefaultProjUsageSettings(tenDeviceCategory enSPIType, tenEnabledInfo &enEnabledInfo)
{
   SPI_INTENTIONALLY_UNUSED(enSPIType);
   enEnabledInfo = e8USAGE_ENABLED;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclConfigReaderBase::vClearVehicleId()
 ** Setting Vehicle Id to DataPool.
 ***************************************************************************/
t_Void spi_tclConfigReader::vClearVehicleId()
{
   ETG_TRACE_USR1(("vClearVehicleId() entered \n"));
   Datapool oDatapool;
   t_String szVehicleIdentifier;
   oDatapool.bWriteVehicleId(szVehicleIdentifier);
   ETG_TRACE_USR1(("vClearVehicleId() entered and value of vehicle Id after writing in datapool is :%s",szVehicleIdentifier.c_str()));
}

/***************************************************************************
 ** FUNCTION:  t_String spi_tclConfigReaderBase::szGetVehicleId()
 ** Function returns Vehicle Id after reading it from DataPool.
 ***************************************************************************/
t_String spi_tclConfigReader::szGetVehicleId()
{
   ETG_TRACE_USR1(("szGetVehicleId() entered \n"));
   Datapool oDatapool;
   t_String szVehicleIdentifier;
   //Read Vehicle Id if available.
   oDatapool.bReadVehicleId(szVehicleIdentifier);
   ETG_TRACE_USR1(("szGetVehicleId() entered and value of vehicle Id after reading from datapool is :%s",szVehicleIdentifier.c_str()));
   //If Vehicle Id is not available,then generate the vehicle Id using Random Generator Algorithm
   //and write it to DataPool,Read and return it.
   ETG_TRACE_USR1(("length of Vehicle Id string after reading from datapool is :%d",szVehicleIdentifier.length()));
   if (0 == szVehicleIdentifier.length())
   {
      RandomIdGenerator oRandomIdGenerator;
      szVehicleIdentifier=oRandomIdGenerator.szgenerateRandomId();
      oDatapool.bWriteVehicleId(szVehicleIdentifier);
      ETG_TRACE_USR1(("szGetVehicleId() entered and value of vehicle Id after writing in datapool and then reading is :%s",szVehicleIdentifier.c_str()));
   }

   return szVehicleIdentifier;
}
/***************************************************************************
** FUNCTION:  t_Bool spi_tclConfigReader::bIsAndroidAutoSupported(...
***************************************************************************/
t_Bool spi_tclConfigReader::bIsAndroidAutoSupported()
{
   t_Bool bIsAndroidAutoSupport = false;

   KdsHandler oKdsHandler;
   t_U8 u8DataBuf[scou8FctAndroidKDSLength];
   memset(u8DataBuf, 0, sizeof(u8DataBuf));
   // check if KDS read is successful
   if(oKdsHandler.bReadData(scou32FctAndroidKDSEntryPoint,scou8FctAndroidKDSLength, u8DataBuf))
   {
      ETG_TRACE_USR4(("spi_tclConfigReader::bIsAndroidAutoSupported:Byte->%d  Value->%d",
         scou8AndroidAutoSupportInfoByte,u8DataBuf[scou8AndroidAutoSupportInfoByte]));
      //Android Auto support bit is at 0th Byte 7th position [0,7]->[0,7]
      //Data is in Little Endian Format X0000000
      bIsAndroidAutoSupport = (t_Bool)(u8DataBuf[scou8AndroidAutoSupportInfoByte]>>scou8AndroiAutoSupportBitPos);
   }

   return bIsAndroidAutoSupport;
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
