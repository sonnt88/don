/***********************************************************************/
/*!
* \file  spi_tclConfigReader.h
* \brief Class to get the Variant specific Info for Gen3 Projects
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to get the Variant specific Info for Gen3 Projects
AUTHOR:         Shihabudheen P M 
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
23.04.2015  | Shihabudheen P M      | Initial Version. Based on Suzuki configuration.
06.05.2015  | Ramkumar M            | Intial value based on G3G with ML disabled.
08.05.2015  | Shiva Kumar G         | extended for ML Client Profile configs
30.06.2015  | Tejaswini HB          | SetSpiFeatureEnabled only when feature is supported
14.08.2015  | Shiva Kumar Gurija    | Dynamic display configuration
\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "Trace.h"
#include "KdsHandler.h"
#include "Datapool.h"
#include "spi_tclConfigReader.h"
#include "AAPTypes.h"
#include "DiPOTypes.h"
#include "RandomIdGenerator.h"

#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
#include "trcGenProj/Header/spi_tclConfigReader.cpp.trc.h"
#endif
#endif


static const t_U16 cou16VARIANTCONFIG_KEY = 0xA110;
static const t_U8 cou8VARIANTCONFIG_SIZE = 0x40;
static const t_U8 cou8SPI_SUPPORTED_BYTE = 32;
static const t_U8 couCARPLAY_SUPPORTED_BIT = 8; //the bit poistion 4 indicate the CarPlay enabled flag. So the corresponding digit is 8.
static const t_U8 couAAP_SUPPORTED_BIT = 16; // the bit position 5 indicates the AAP enabled flag. So the corresponding digit is 16.
static const t_U16 scou16Width_8Inch = 800;
static const t_U16 scou16Height_8Inch = 480;
static const t_U16 scou16Width_8InchMM = 156;
static const t_U16 scou16Height_8InchMM = 82;
static const t_U16 scou16LayerId = 3500;
static const t_U16 scou16SurfaceId = 59;
static const t_U16 scou16TouchLayerId = 3501;
static const t_U16 scou16TouchSurfaceId = 80;
static const t_U16 cou16DRIVESIDECONFIG_KEY = 0xA112;
static const t_U8 cou8DRIVESIDECONFIG_SIZE = 0x40;
static const t_U8 cou8DRIVESIDECONFIG_BYTE = 13;

//Default Client Friendly Name
static const t_String sczClientFriendlyName = "" ;
//Client Manufacturer Name
static const t_String sczHeadUnitManfacturerName = "Bosch CM";
//Vehicle Model Name
static const t_String sczHeadUnitModelName = "RNAIVI";
//Vehicle Manufacturer Name
static t_String sczVehicleManfacturerName = "Nissan Connect";
//Vehicle Model Name
static t_String sczVehicleModelName = "B02E";
// Vehicle Model Year
static const t_String sczVehicleModelYear = "MY16";


#define OEM_ICON_NAME "Nissan"
#define OEM_ICON_PATH "/var/opt/bosch/static/spi/CarPlay/icons/Nissan/Nissan.png"
#define ICON_INFO_LENGTH 100
#define FOUR_EIGHTY 480

/***************************************************************************
** FUNCTION:  spi_tclConfigReader::spi_tclConfigReader()
***************************************************************************/
spi_tclConfigReader::spi_tclConfigReader():
m_enDriveModeInfo(e8PARK_MODE),
m_enNightModeInfo(e8_DAY_MODE),
m_u8AAPParkModeRestrictionInfo(0),
m_u8AAPDriveModeRestrictionInfo(0),
m_u8DipoParkModeRestrictionInfo(0),
m_u8DipoDriveModeRestrictionInfo(0)
{
   ETG_TRACE_USR1(("spi_tclConfigReader() entered "));
   vReadScreenConfigs();
}

/***************************************************************************
** FUNCTION:  spi_tclConfigReader::~spi_tclConfigReader()
***************************************************************************/
spi_tclConfigReader::~spi_tclConfigReader()
{
   ETG_TRACE_USR1(("~spi_tclConfigReader() entered \n"));
}

/***************************************************************************
** FUNCTION   :t_Void spi_tclConfigReader::vGetVideoConfigData()
***************************************************************************/
t_Void spi_tclConfigReader::vGetVideoConfigData(tenDeviceCategory enDevCat,
                                                trVideoConfigData& rfrVideoConfig)
{

   ETG_TRACE_USR1(("vGetVideoConfigData - %d",ETG_ENUM(DEVICE_CATEGORY,enDevCat)));

   m_oLock.s16Lock();
   rfrVideoConfig.u32ResolutionSupported = FOUR_EIGHTY;
   rfrVideoConfig.u32Screen_Width = m_rDisplayAttributes.u16ScreenWidth;
   rfrVideoConfig.u32Screen_Height = m_rDisplayAttributes.u16ScreenHeight;
   rfrVideoConfig.u32Screen_Width_Millimeter = m_rDisplayAttributes.u16ScreenWidthMm;
   rfrVideoConfig.u32Screen_Height_Millimeter = m_rDisplayAttributes.u16ScreenHeightMm;

   // Read the technology specific configurations
   for(t_U8 u8Index=0; u8Index<m_rDisplayAttributes.vecDisplayLayerAttr.size();u8Index++)
   {
      if(enDevCat == m_rDisplayAttributes.vecDisplayLayerAttr[u8Index].enDevCat)
      {
         const trDisplayLayerAttributes& corfrDispLayerAttr =  m_rDisplayAttributes.vecDisplayLayerAttr[u8Index];
         rfrVideoConfig.u32LayerId  = corfrDispLayerAttr.u16VideoLayerID;
         rfrVideoConfig.u32SurfaceId = corfrDispLayerAttr.u16VideoSurfaceID;
         rfrVideoConfig.u32TouchLayerId = corfrDispLayerAttr.u16TouchLayerID;
         rfrVideoConfig.u32TouchSurfaceId = corfrDispLayerAttr.u16TouchSurfaceID;

         rfrVideoConfig.u32ProjScreen_Width = corfrDispLayerAttr.u16LayerWidth;
         rfrVideoConfig.u32ProjScreen_Height = corfrDispLayerAttr.u16LayerHeight;

         rfrVideoConfig.u32ProjScreen_Width_Mm = corfrDispLayerAttr.u16DisplayWidthMm;
         rfrVideoConfig.u32ProjScreen_Height_Mm = corfrDispLayerAttr.u16DisplayHeightMm;

         //remove these two statements, once HMI implements Screen width & height in MM
         if( (0 == rfrVideoConfig.u32ProjScreen_Width_Mm)&&
            (0 != m_rDisplayAttributes.u16ScreenWidth)&&(0 != m_rDisplayAttributes.u16ScreenWidthMm) )
         {
            rfrVideoConfig.u32ProjScreen_Width_Mm = (t_U32)
               ((float)(corfrDispLayerAttr.u16LayerWidth)/(m_rDisplayAttributes.u16ScreenWidth))*(m_rDisplayAttributes.u16ScreenWidthMm);
         }//if( (0 == rfrVideoConfig.u32ProjSc

         if( (0 == rfrVideoConfig.u32ProjScreen_Height_Mm) &&
            (0 != m_rDisplayAttributes.u16ScreenHeight)&&(0 != m_rDisplayAttributes.u16ScreenHeightMm) )
         {
            rfrVideoConfig.u32ProjScreen_Height_Mm = (t_U32)
               ((float)(corfrDispLayerAttr.u16LayerHeight)/(m_rDisplayAttributes.u16ScreenHeight))*(m_rDisplayAttributes.u16ScreenHeightMm);
         }//if( (0 == rfrVideoConfig.u32ProjScreen_Heig

         break;
      }//if(enDevCat == m_rDisplayAttributes.v
   }// for(t_U8 u8Index=0; u8Index<m_rDisplayAttributes.vecDi
   m_oLock.vUnlock();


   ETG_TRACE_USR4(("Screen Width = %d",rfrVideoConfig.u32Screen_Width));
   ETG_TRACE_USR4(("Screen Height = %d",rfrVideoConfig.u32Screen_Height));
   ETG_TRACE_USR4(("Screen Width(mm) = %d",rfrVideoConfig.u32Screen_Width_Millimeter));
   ETG_TRACE_USR4(("Screen Height(mm) = %d",rfrVideoConfig.u32Screen_Height_Millimeter));
   ETG_TRACE_USR4(("Layer Id = %d",rfrVideoConfig.u32LayerId));
   ETG_TRACE_USR4(("Surface Id = %d",rfrVideoConfig.u32SurfaceId));
   ETG_TRACE_USR4(("Touch Layer Id = %d",rfrVideoConfig.u32TouchLayerId));
   ETG_TRACE_USR4(("Touch Surface Id  = %d",rfrVideoConfig.u32TouchSurfaceId));
   ETG_TRACE_USR4(("Proj Screen Width = %d",rfrVideoConfig.u32ProjScreen_Width));
   ETG_TRACE_USR4(("Proj Screen Height = %d",rfrVideoConfig.u32ProjScreen_Height));
   ETG_TRACE_USR4(("Proj Screen Width(mm) = %d",rfrVideoConfig.u32ProjScreen_Width_Mm));
   ETG_TRACE_USR4(("Proj Screen Height(mm) = %d",rfrVideoConfig.u32ProjScreen_Height_Mm));

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclConfigReader::vGetSpiFeatureSupport(...)
***************************************************************************/
t_Void spi_tclConfigReader::vGetSpiFeatureSupport(trSpiFeatureSupport& rfrSpiFeatureSupport)
{

   KdsHandler oKdsHandler;
   t_U8 u8DataBuf[cou8VARIANTCONFIG_SIZE];
   t_Bool bIsDipoSupp = false, bIsAAPSupp = false, bIsMLSupp = false;
   t_Bool bReadStatus = oKdsHandler.bReadData(cou16VARIANTCONFIG_KEY, cou8VARIANTCONFIG_SIZE, u8DataBuf);

   if (true == bReadStatus)
   {
      ETG_TRACE_USR1(("spi_tclConfigReader::vGetSpiFeatureSupport: CarPlay KDS Data byte value = %d", u8DataBuf[cou8SPI_SUPPORTED_BYTE]));
      ETG_TRACE_USR1(("spi_tclConfigReader::vGetSpiFeatureSupport: AAP KDS Data byte value = %d", u8DataBuf[cou8SPI_SUPPORTED_BYTE]));

      bIsDipoSupp = (t_Bool) (u8DataBuf[cou8SPI_SUPPORTED_BYTE] & couCARPLAY_SUPPORTED_BIT);
      bIsAAPSupp  = (t_Bool) (u8DataBuf[cou8SPI_SUPPORTED_BYTE] & couAAP_SUPPORTED_BIT);
   }

   //! Set supported SPI features
   rfrSpiFeatureSupport.vSetMirrorLinkSupport(bIsMLSupp);
   rfrSpiFeatureSupport.vSetDipoSupport(bIsDipoSupp);
   rfrSpiFeatureSupport.vSetAndroidAutoSupport(bIsAAPSupp);

   ETG_TRACE_USR1(("spi_tclConfigReader::vGetSpiFeatureSupport: MIRRORLINK supported = %d \n",
         ETG_ENUM(BOOL, bIsMLSupp)));
   ETG_TRACE_USR1(("spi_tclConfigReader::vGetSpiFeatureSupport: DIPO supported = %d \n",
         ETG_ENUM(BOOL, bIsDipoSupp)));
   ETG_TRACE_USR1(("spi_tclConfigReader::vGetSpiFeatureSupport: ANDROID AUTO supported = %d \n",
         ETG_ENUM(BOOL, bIsAAPSupp)));
}

/***************************************************************************
** FUNCTION:  tenDriveSideInfo spi_tclConfigReader::enGetDriveSideInfo(...)
***************************************************************************/
tenDriveSideInfo spi_tclConfigReader::enGetDriveSideInfo()
{
   //! Initialise return value to default.
   tenDriveSideInfo enDriveSideInfo = e8LEFT_HAND_DRIVE;

   KdsHandler oKdsHandler;
   t_U8 u8DataBuf[cou8DRIVESIDECONFIG_SIZE];
   t_Bool bReadStatus = oKdsHandler.bReadData(cou16DRIVESIDECONFIG_KEY, cou8DRIVESIDECONFIG_SIZE, u8DataBuf);
   if(true == bReadStatus)
   {
         // The first bit of the byte holds the information about the drive side.
		 // Hence ANDed with 0x01
		t_Bool bDriveSideInfo =  (t_Bool) (u8DataBuf[cou8DRIVESIDECONFIG_BYTE-1] & 0x01);
		enDriveSideInfo = (true == bDriveSideInfo) ? e8RIGHT_HAND_DRIVE: e8LEFT_HAND_DRIVE;
		ETG_TRACE_USR2(("spi_tclConfigReader::enGetDriveSideInfo() read value  = %d \n",
         u8DataBuf[cou8DRIVESIDECONFIG_BYTE-1]));
   }

   ETG_TRACE_USR1(("spi_tclConfigReader::enGetDriveSideInfo() left with enDriveSideInfo = %d \n",
         ETG_ENUM(DRIVE_SIDE_TYPE, enDriveSideInfo)));

   return enDriveSideInfo;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclConfigReader::vSetFeatureRestrictions(...)
 ***************************************************************************/
t_Void spi_tclConfigReader::vSetFeatureRestrictions(tenDeviceCategory enDevCategory,
      const t_U8 cou8ParkModeRestrictionInfo, const t_U8 cou8DriveModeRestrictionInfo)
{
   ETG_TRACE_USR1(("spi_tclConfigReader::vSetFeatureRestrictions() Entered with Device Category = %d",
         ETG_ENUM( DEVICE_CATEGORY, enDevCategory)));

  if (enDevCategory== e8DEV_TYPE_ANDROIDAUTO)
  {
      m_u8AAPParkModeRestrictionInfo = (cou8ParkModeRestrictionInfo & (0x1F));
      m_u8AAPDriveModeRestrictionInfo = (cou8DriveModeRestrictionInfo & (0x1F));

      ETG_TRACE_USR1(("[PARAM]::vSetFeatureRestrictions() After Masking : AAP Park Mode Restriction = %d, Drive Mode Restriction = %d ",
            m_u8AAPParkModeRestrictionInfo, m_u8AAPDriveModeRestrictionInfo));
  }
  else if (enDevCategory== e8DEV_TYPE_DIPO)
  {
     //! Not required to keep this information since CarPlay cannot support
     // two different values for feature lockout.
     m_u8DipoParkModeRestrictionInfo=cou8ParkModeRestrictionInfo;

     //! Write the data to data pool for future use
     Datapool oDatapool;
     t_Bool bStatus = oDatapool.bWriteDiPODriveRestrictionInfo(cou8DriveModeRestrictionInfo);
     tenResponseCode enResponseCode = (false == bStatus) ? e8FAILURE:e8SUCCESS;
     ETG_TRACE_USR1(("Set Drive restriction is completed with status : %d ", enResponseCode));
   }
}

/***************************************************************************
 ** FUNCTION:  t_U8 spi_tclConfigReader::u8GetAAPParkRestrictionInfo()
 ***************************************************************************/
t_U8 spi_tclConfigReader::u8GetAAPParkRestrictionInfo()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::u8GetAAPParkRestrictionInfo : "
         "AAP Park Mode Restriction = %d ", m_u8AAPParkModeRestrictionInfo));
   return m_u8AAPParkModeRestrictionInfo;
}
/***************************************************************************
 ** FUNCTION:  t_U8 spi_tclConfigReader::u8GetAAPDriveRestrictionInfo(...)
 ***************************************************************************/
t_U8 spi_tclConfigReader::u8GetAAPDriveRestrictionInfo()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::u8GetAAPDriveRestrictionInfo : "
         "AAP Drive Mode Restriction = %d ", m_u8AAPDriveModeRestrictionInfo));
   return m_u8AAPDriveModeRestrictionInfo;
}

/***************************************************************************
 ** FUNCTION:  t_U8 spi_tclConfigReader::u8GeDiPOParkRestrictionInfo(...)
 ***************************************************************************/
t_U8 spi_tclConfigReader::u8GeDiPOParkRestrictionInfo()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::u8GeDiPOParkRestrictionInfo "
         "DiPO Park Mode Restriction = %d ", m_u8DipoParkModeRestrictionInfo));
  return m_u8DipoParkModeRestrictionInfo;
}

/***************************************************************************
 ** FUNCTION:  t_U8 spi_tclConfigReader::u8GeDiPODriveRestrictionInfo(...)
***************************************************************************/
t_U8 spi_tclConfigReader::u8GeDiPODriveRestrictionInfo()
{
   Datapool oDatapool;
   t_U8 u8DriveRestInfo;
   oDatapool.bReadDiPODriveRestrictionInfo(u8DriveRestInfo);
   ETG_TRACE_USR4(("[PARAM]::spi_tclConfigReader::u8GeDiPODriveRestrictionInfo Drive Restriction = %d", u8DriveRestInfo));
   return u8DriveRestInfo;
}
/***************************************************************************
** FUNCTION:  t_Bool spi_tclConfigReader::bGetMLNotificationSetting()
***************************************************************************/
t_Bool spi_tclConfigReader::bGetMLNotificationSetting()
{
   t_Bool bMLNotifOnOff = false; //default setting
   
   //TODO - Read ML Notification setting from calibration?   

   ETG_TRACE_USR2(("spi_tclConfigReader::bGetMLNotificationSetting() left with MLNotification = %d \n",
         ETG_ENUM(BOOL, bMLNotifOnOff)));
   return bMLNotifOnOff;
}

/***************************************************************************
** FUNCTION   :t_Void spi_tclConfigReader::vGetOemIconData(
              trVehicleBrandInfo& rfrVehicleBrandInfo)
***************************************************************************/
t_Void spi_tclConfigReader::vGetOemIconData(trVehicleBrandInfo& rfrVehicleBrandInfo)
{
   //Not Implemeted
   memset(rfrVehicleBrandInfo.szOemIconPath, 0, ICON_INFO_LENGTH);
   memset(rfrVehicleBrandInfo.szOemName, 0, ICON_INFO_LENGTH);
   memset(rfrVehicleBrandInfo.szManufacturer, 0, STR_LENGTH);
   memset(rfrVehicleBrandInfo.szModel, 0, STR_LENGTH);
   strncpy(rfrVehicleBrandInfo.szOemIconPath, OEM_ICON_PATH , ICON_INFO_LENGTH);
   strncpy(rfrVehicleBrandInfo.szOemName, OEM_ICON_NAME , ICON_INFO_LENGTH);
   strncpy(rfrVehicleBrandInfo.szManufacturer,sczVehicleManfacturerName.c_str() , sczVehicleManfacturerName.length());
   strncpy(rfrVehicleBrandInfo.szModel, sczVehicleModelName.c_str() , sczVehicleModelName.length());
   sczVehicleModelName = rfrVehicleBrandInfo.szModel;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetClientID()
***************************************************************************/
t_String spi_tclConfigReader::szGetClientID()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::szGetClientID() left with ClientID = %s",
      cszClientProfileId.c_str()));

   return cszClientProfileId.c_str();
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetClientFriendlyName()
***************************************************************************/
t_String spi_tclConfigReader::szGetClientFriendlyName()
{

   ETG_TRACE_USR1(("spi_tclConfigReader::szGetClientFriendlyName() left with FriendlyName = %s",
      sczClientFriendlyName.c_str()));

   return sczClientFriendlyName.c_str();
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetClientManufacturerName()
***************************************************************************/
t_String spi_tclConfigReader::szGetClientManufacturerName()
{
   return sczVehicleManfacturerName.c_str();
}
/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetModelYear()
***************************************************************************/
t_String spi_tclConfigReader::szGetModelYear()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::szGetModelYear() left with Model Year= %s",
         sczVehicleModelYear.c_str()));
   return sczVehicleModelYear;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetHeadUnitManufacturerName()
***************************************************************************/
t_String spi_tclConfigReader::szGetHeadUnitManufacturerName()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::szGetHeadUnitManufacturerName() left with Head unit manufacturer name = %s",
      sczHeadUnitManfacturerName.c_str()));
   return sczHeadUnitManfacturerName;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetHeadUnitModelName()
***************************************************************************/
t_String spi_tclConfigReader::szGetHeadUnitModelName()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::szGetHeadUnitModelName() left with Head unit model name = %s",
      sczHeadUnitModelName.c_str()));
   return sczHeadUnitModelName;
}

/***************************************************************************
** FUNCTION:  tenRegion spi_tclConfigReader::enGetRegion()
***************************************************************************/
tenRegion spi_tclConfigReader::enGetRegion()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::enGetRegion() "));
   //return default value
   return e8_WORLD;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclConfigReader::vReadScreenConfigs()
***************************************************************************/
t_Void spi_tclConfigReader::vReadScreenConfigs()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::vReadScreenConfigs"));

   ETG_TRACE_USR1(("spi_tclConfigReader::vReadScreenConfigs"));

   Datapool oDatapool;
   trDisplayAttributes rDispAttr;

   t_Bool bResult = oDatapool.bReadScreenAttributes(rDispAttr);
   if((true == bResult)&&(0 != rDispAttr.u16ScreenWidth))
   {
      m_oLock.s16Lock();
      //Read is successful and there are valid values in Data pool
      m_rDisplayAttributes = rDispAttr ;
      m_oLock.vUnlock();
   }//if(true != oDatapool.bReadScreenAttributes(rDispAttr))
   else
   {
      m_oLock.s16Lock();

      //There are no values/Invalid values in Datapool, populate with the default values
      //(currently populated with default values).
      m_rDisplayAttributes.u16ScreenWidth = scou16Width_8Inch;
      m_rDisplayAttributes.u16ScreenHeight = scou16Height_8Inch;
      m_rDisplayAttributes.u16ScreenWidthMm = scou16Width_8InchMM;
      m_rDisplayAttributes.u16ScreenHeightMm = scou16Height_8InchMM;

      trDisplayLayerAttributes rMLDispLayerAttr;
      rMLDispLayerAttr.enDevCat = e8DEV_TYPE_MIRRORLINK;
      rMLDispLayerAttr.u16TouchLayerID = scou16LayerId;
      rMLDispLayerAttr.u16TouchSurfaceID = scou16SurfaceId;
      m_rDisplayAttributes.vecDisplayLayerAttr.push_back(rMLDispLayerAttr);

      trDisplayLayerAttributes rDiPoDispLayerAttr;
      rDiPoDispLayerAttr.enDevCat = e8DEV_TYPE_DIPO;
      rDiPoDispLayerAttr.u16TouchLayerID = scou16TouchLayerId;
      rDiPoDispLayerAttr.u16TouchSurfaceID = scou16TouchSurfaceId;
      m_rDisplayAttributes.vecDisplayLayerAttr.push_back(rDiPoDispLayerAttr);

      trDisplayLayerAttributes rAAPDispLayerAttr;
      rAAPDispLayerAttr.enDevCat = e8DEV_TYPE_ANDROIDAUTO;
      rAAPDispLayerAttr.u16TouchLayerID = scou16TouchLayerId;
      rAAPDispLayerAttr.u16TouchSurfaceID = scou16TouchSurfaceId;
      m_rDisplayAttributes.vecDisplayLayerAttr.push_back(rAAPDispLayerAttr);

      for(t_U8 u8Index=0;u8Index<m_rDisplayAttributes.vecDisplayLayerAttr.size();u8Index++)
      {
         m_rDisplayAttributes.vecDisplayLayerAttr[u8Index].u16VideoLayerID = scou16LayerId;
         m_rDisplayAttributes.vecDisplayLayerAttr[u8Index].u16VideoSurfaceID = scou16SurfaceId;
         m_rDisplayAttributes.vecDisplayLayerAttr[u8Index].u16LayerWidth = scou16Width_8Inch;
         m_rDisplayAttributes.vecDisplayLayerAttr[u8Index].u16LayerHeight = scou16Height_8Inch;
         m_rDisplayAttributes.vecDisplayLayerAttr[u8Index].u16DisplayWidthMm = scou16Width_8InchMM;
         m_rDisplayAttributes.vecDisplayLayerAttr[u8Index].u16DisplayHeightMm = scou16Height_8InchMM;
      }//for(t_U8 u8Index=0;u8Index<m_rDisplayAttributes.v

      m_oLock.vUnlock();

   }//else

}
/***************************************************************************
** FUNCTION:  t_Void spi_tclConfigReader::vSetScreenConfigs()
***************************************************************************/
t_Void spi_tclConfigReader::vSetScreenConfigs(const trDisplayAttributes& corfrDispLayerAttr)
{
   ETG_TRACE_USR1(("spi_tclConfigReader::vSetScreenConfigs"));
   m_oLock.s16Lock();
   //Set the screen configurations in data pool, if there is any change in the current values
   if(m_rDisplayAttributes != corfrDispLayerAttr)
   {
      Datapool oDatapool;
      if(true != oDatapool.bWriteScreenAttributes(corfrDispLayerAttr))
      {
         ETG_TRACE_ERR(("spi_tclConfigReader::vSetScreenConfigs: Error in setting values in datapool"));
      }//if(true != oDatapool.

      //Update the member variables of this class, to use in current power cycle.
      m_rDisplayAttributes = corfrDispLayerAttr;
   }//if(corfrDispLayerAttr != m_rDisplayAttributes)
   m_oLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclConfigReader::bGetRotaryCtrlSupport()
***************************************************************************/
t_Bool spi_tclConfigReader::bGetRotaryCtrlSupport()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::vSetScreenConfigs"));

   t_Bool bIsRotCtrlSupported = true;
   // By default Rotary Controller is enabled
   return bIsRotCtrlSupported;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::szGetSoftwareVersion()
***************************************************************************/
t_String spi_tclConfigReader::szGetSoftwareVersion()
{
   ETG_TRACE_USR1(("spi_tclConfigReader::szGetSoftwareVersion "));
   //! TODO : Check interface to get software version
   t_String szSoftwareVersion = "";
   return szSoftwareVersion;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vSetDriveModeInfo()
***************************************************************************/
t_Void spi_tclConfigReader::vSetDriveModeInfo(const tenVehicleConfiguration enDriveModeInfo)
{
	//The data will access from different threads. Hence locked before use.
	m_oLock.s16Lock();
	m_enDriveModeInfo = enDriveModeInfo;
	m_oLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vGetDriveModeInfo()
***************************************************************************/
t_Void spi_tclConfigReader::vGetDriveModeInfo(tenVehicleConfiguration &rfoenDriveModeInfo)
{
	m_oLock.s16Lock();
	rfoenDriveModeInfo =  m_enDriveModeInfo;
	m_oLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vSetNightModeInfo()
***************************************************************************/
t_Void spi_tclConfigReader::vSetNightModeInfo(const tenVehicleConfiguration enNightModeInfo)
{
	m_oLock.s16Lock();
	m_enNightModeInfo = enNightModeInfo;
	m_oLock.vUnlock();

}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vGetNightModeInfo()
***************************************************************************/
t_Void spi_tclConfigReader::vGetNightModeInfo(tenVehicleConfiguration &rfoenNightModeInfo)
{
	m_oLock.s16Lock();
	rfoenNightModeInfo = m_enNightModeInfo;
	m_oLock.vUnlock();

}

/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vSetDriveSideInfo()
***************************************************************************/
t_Void spi_tclConfigReader::vSetDriveSideInfo(const tenVehicleConfiguration enVehicleConfig)
{
   //!Note: Nothing to do here. Value always read from KDS.
   SPI_INTENTIONALLY_UNUSED(enVehicleConfig);
}
/***************************************************************************
** FUNCTION:  t_String spi_tclConfigReader::vGetDisplayInputParam()
***************************************************************************/
t_Void spi_tclConfigReader::vGetDisplayInputParam(t_U8 &rfu8DisplayInput)
{
   rfu8DisplayInput = (true == bGetRotaryCtrlSupport())?(LOW_FIDELITY_TOUCH_WITH_KNOB):(LOW_FIDELITY_TOUCH);

}
/***************************************************************************
** FUNCTION:  t_Void spi_tclConfigReader::vGetVehicleInfoDataAAP(trVehicleInfo& rfrVehicleInfo)
***************************************************************************/

t_Void spi_tclConfigReader::vGetVehicleInfoDataAAP(trVehicleInfo& rfrVehicleInfo)
{
   rfrVehicleInfo.szModel = sczVehicleModelName;
   rfrVehicleInfo.szManufacturer = sczVehicleManfacturerName;
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclConfigReader::vGetDefaultProjUsageSettings
 ***************************************************************************/
t_Void spi_tclConfigReader::vGetDefaultProjUsageSettings(tenDeviceCategory enSPIType, tenEnabledInfo &enEnabledInfo)
{
   SPI_INTENTIONALLY_UNUSED(enSPIType);
   enEnabledInfo = e8USAGE_CONF_REQD;
}
/***************************************************************************
 ** FUNCTION:  t_Void spi_tclConfigReaderBase::vClearVehicleId()
 ** Setting Vehicle Id to DataPool.
 ***************************************************************************/
t_Void spi_tclConfigReader::vClearVehicleId()
{
   ETG_TRACE_USR1(("vClearVehicleId() entered \n"));
   Datapool oDatapool;
   t_String szVehicleIdentifier;
   oDatapool.bWriteVehicleId(szVehicleIdentifier);
   ETG_TRACE_USR1(("vClearVehicleId() entered and value of vehicle Id after writing in datapool is :%s",szVehicleIdentifier.c_str()));
}

/***************************************************************************
 ** FUNCTION:  t_String spi_tclConfigReaderBase::szGetVehicleId()
 ** Function returns Vehicle Id after reading it from DataPool.
 ***************************************************************************/
t_String spi_tclConfigReader::szGetVehicleId()
{
   ETG_TRACE_USR1(("szGetVehicleId() entered \n"));
   Datapool oDatapool;
   t_String szVehicleIdentifier;
   //Read Vehicle Id if available.
   oDatapool.bReadVehicleId(szVehicleIdentifier);
   ETG_TRACE_USR1(("szGetVehicleId() entered and value of vehicle Id after reading from datapool is :%s",szVehicleIdentifier.c_str()));
   //If Vehicle Id is not available,then generate the vehicle Id using Random Generator Algorithm
   //and write it to DataPool,Read and return it.
   ETG_TRACE_USR1(("length of Vehicle Id string after reading from datapool is :%d",szVehicleIdentifier.length()));
   if (0 == szVehicleIdentifier.length())
   {
      RandomIdGenerator oRandomIdGenerator;
      szVehicleIdentifier=oRandomIdGenerator.szgenerateRandomId();
      oDatapool.bWriteVehicleId(szVehicleIdentifier);
      ETG_TRACE_USR1(("szGetVehicleId() entered and value of vehicle Id after writing in datapool and then reading is :%s",szVehicleIdentifier.c_str()));
   }

   return szVehicleIdentifier;
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
