/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdInputSource.h
 * \brief             Input wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Input wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 24.04.2015 |  Sameer Chandra		       | Initial Version
 16.07.2015 |  Sameer Chandra              | Added method vReportKnobkey to support Knob
                                             Encoder.

 \endverbatim
 ******************************************************************************/
 #ifndef GEN3X86
// Below code should only compile in case of Gen3arm Make
#ifndef SPI_TCLAAPINPUTSOURCE_H_
#define SPI_TCLAAPINPUTSOURCE_H_

#include "SPITypes.h"
#include "AAPTypes.h"
#include "RespBase.h"
#include "spi_tclAAPSessionDataIntf.h"
#include "aauto/WaylandInputSource.h"
#include "aauto/AditInputSource.h"

using namespace adit::aauto;

class spi_tclAAPInputSource
{
public:

   /***************************************************************************
    *********************************PUBLIC*************************************
    ***************************************************************************/

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPInputSource::spi_tclAAPInputSource();
    ***************************************************************************/
   /*!
    * \fn     spi_tclAAPInputSource()
    * \brief  Default Constructor
    * \sa     spi_tclAAPInputSource()
    **************************************************************************/
   spi_tclAAPInputSource();

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPInputSource::~spi_tclAAPInputSource()
    ***************************************************************************/
   /*!
    * \fn      ~spi_tclAAPInputSource()
    * \brief   Virtual Destructor
    * \sa      spi_tclAAPInputSource()
    **************************************************************************/
   ~spi_tclAAPInputSource();

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPInputSource::bInitialize()
    **************************************************************************/
   /*!
    * \fn      bInitialize()
    * \brief   Initializes the InputSource Endpoint, registers keycodes and touch
    * 	      screen.
    * \sa      bInitialize()
    **************************************************************************/
   t_Bool bInitialize(const trAAPInputConfig& rAAPInputConfig,
         const set<t_S32>& sKeyCodes);

   /***************************************************************************
    ** FUNCTION:  virtual spi_tclAAPInputSource::bUnInitialize()
    ***************************************************************************/
   /*!
    * \fn      bUnInitialize()
    * \brief   Uninitializes the InputSource Endpoint.
    * \sa      bUnInitialize()
    **************************************************************************/
   t_Void bUnInitialize();

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclAAPInputSource::vReportTouch
    ***************************************************************************/
   /*!
    * \fn      vReportTouch(t_U32 u32DeviceHandle,trTouchData &rfrTouchData)
    * \brief   Would be triggered from wayland class.
   * \param   u64timestamp  : [IN] unique identifier to AAP device
   * \param   s32numPointers     : [IN] reference to touch data structure which contains
   *          touch details received /ref trTouchData
   * \param   u32pointerIds
   * \param   u32XCoord : [IN] X Coordinate value for touch point
   * \param   u32YCoord : [IN] Y Coordinate value for touch point
   * \param   u32Action : [IN] Action state weather it is press, release etc.
   * \param   u32ActionIndex : [IN] Index of the touch point for which the change is reported.
    * \retval  NONE
    **************************************************************************/
   t_Void vReportTouch(t_U64 timestamp, t_S32 numPointers, const t_U32* pointerIds,
         const t_U32* x, const t_U32* y, t_U32 action, t_U32 actionIndex)
   {
      SPI_INTENTIONALLY_UNUSED(timestamp);
      SPI_INTENTIONALLY_UNUSED(numPointers);
      SPI_INTENTIONALLY_UNUSED(pointerIds);
      SPI_INTENTIONALLY_UNUSED(x);
      SPI_INTENTIONALLY_UNUSED(y);
      SPI_INTENTIONALLY_UNUSED(action);
      SPI_INTENTIONALLY_UNUSED(actionIndex);
   }

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPInputSource::vReportkey
    ***************************************************************************/
   /*!
    * \fn      vReportkey(t_U32 u32DeviceHandle, tenKeyMode enKeyMode,
    *		   tenKeyCode enKeyCode)
    * \brief   Would be triggered from wayland class.
   * \param   u64TimeStamp : [IN] unique identifier to AAP device
   * \param   u32KeyCode       : [IN] indicates keypress or keyrelease
   * \param   bIsDown       : [IN] unique key code identifier
   * \param   u32MetaState  : [IN] Used for special keys
    * \retval  NONE
    **************************************************************************/
   t_Void vReportkey(t_U64 timestamp, t_U32 keycode, t_Bool isDown, t_U32 metaState);

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPInputHandler::vReportKnobkey
   ***************************************************************************/
  /*!
   * \fn      vReportKnobkey(t_U64 u64TimeStamp, t_U32 u32KeyCode,
   *                         t_S32 s32DeltaCnts))
   * \brief   Receives Knob Encoder Change and forwards it to AAP InputSource Endpoint
   *       wrapper.
   * \param   u64TimeStamp : [IN] unique identifier to AAP device
   * \param   u32KeyCode   : [IN] unique code for knob rotary controller
   * \param   s32DeltaCnts   : [IN] Encoder Delta Change.
   * \retval  NONE
   **************************************************************************/
   t_Void   vReportKnobkey(t_U64 u64TimeStamp, t_U32 u32KeyCode, t_S32 s32DeltaCnts);



protected:

   /***************************************************************************
    *********************************PROTECTED**********************************
    ***************************************************************************/

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPInputSource(const spi_tclAAPInputSource...
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPInputSource(const spi_tclAAPInputSource& corfoSrc)
    * \brief   Copy constructor - Do not allow the creation of copy constructor
    * \param   corfoSrc : [IN] reference to source data interface object
    * \retval
    * \sa      spi_tclAAPInputSource()
    ***************************************************************************/
   spi_tclAAPInputSource(const spi_tclAAPInputSource& corfoSrc);

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPInputSource& operator=( const spi_tclAAPInput...
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPInputSource& operator=(const spi_tclAAPInputSource& corfoSrc))
    * \brief   Assignment operator
    * \param   corfoSrc : [IN] reference to source data interface object
    * \retval
    * \sa      spi_tclAAPInputSource(const spi_tclAAPInputSource& otrSrc)
    ***************************************************************************/
   spi_tclAAPInputSource& operator=(const spi_tclAAPInputSource& corfoSrc);

private:
   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPInputSource::vSetInputConfig
    ***************************************************************************/
   /*!
    * \fn      vSetInputConfig()
    * \brief   Sets the configuration parameters for ADIT input source.
    * \param   NONE
    * \retval  NONE
    **************************************************************************/
   t_Void vSetInputConfig(const trAAPInputConfig& rAAPInputConfig);

   //! Wayland InputSource Endpoint
   AditInputSource* m_poWlInputSource;

};
#endif /* SPI_TCLAAPINPUTSOURCE_H_ */
#endif /* GEN3X86 */