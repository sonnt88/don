/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdInputSource.cpp
 * \brief             Android Auto G3G Enpoint wrapper for Input
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Input wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 24.04.2015 |  Sameer Chandra		   	    | Initial Version
 11.07.2015 |  Ramya Murthy                | Fix for same session ID being used for multiple Endpoints
 16.07.2015 |  Sameer Chandra              | Added method vReportKnobkey to support Knob
                                             Encoder.

 \endverbatim
 ******************************************************************************/
#ifndef GEN3X86
// Below code should only compile in case of Gen3arm Make
#include "aauto_wayland.h"
#include "StringHandler.h"
#include "spi_tclAAPInputSource.h"
#include <set>
#include <errno.h>
//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
      #include "trcGenProj/Header/spi_tclAAPInputSource.cpp.trc.h"
   #endif
#endif

static const t_String cszLayerId 		= "wl-touch-layer-id";
static const t_String cszSurfaceId 		= "wl-touch-surface-id";
static const t_String cszDisplayWidth 	= "display-width";
static const t_String cszDisplayHeight	= "display-height";
static const t_String cszTouchWidth		= "touch-width";
static const t_String cszTouchHeight	= "touch-height";
static const t_String cszTouchMax		= "touch-maxnum";
static const t_String cszInputTriggerInterval = "wl-touch-input-trigger-interval-usec";
static const t_String cszEnableVerbose 	= "enable-verbose-logging";
static const t_String cszTouchType = "wl-touch-type";
//static const t_String cszSingleTouch = "3";
static const t_String cszMultiTouch = "4";

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::spi_tclAAPCmdInput()
***************************************************************************/
spi_tclAAPInputSource::spi_tclAAPInputSource():m_poWlInputSource(NULL)
{

   ETG_TRACE_USR1((" spi_tclAAPInputSource::spi_tclAAPInputSource entered "));

}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::~spi_tclAAPCmdInput()
***************************************************************************/
spi_tclAAPInputSource::~spi_tclAAPInputSource()
{

   ETG_TRACE_USR1((" spi_tclAAPInputSource::~spi_tclAAPInputSource entered "));
   m_poWlInputSource = NULL;

}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::bInitialize()
***************************************************************************/
t_Bool spi_tclAAPInputSource::bInitialize(const trAAPInputConfig& rAAPInputConfig, const set<t_S32>& sKeyCodes)
{
   //! Call the library entry point to enable DLT logging for input
   adit::aauto::WaylandEntryPoint(NULL);

   t_Bool bRetVal = false;
   //! Get Galreciever using Session Data Interface
   spi_tclAAPSessionDataIntf oSessionDataIntf;

   shared_ptr < GalReceiver > shpoGalReceiver = oSessionDataIntf.poGetGalReceiver();

   if ( shpoGalReceiver != nullptr )
   {
      m_poWlInputSource = new (std::nothrow) WaylandInputSource(
         e32SESSIONID_AAPTOUCH, shpoGalReceiver->messageRouter());
   }

   if ( NULL != m_poWlInputSource )
   {
      vSetInputConfig(rAAPInputConfig);

      t_Bool bInitWLSource = m_poWlInputSource->init();

      if ( true == bInitWLSource )
      {
         //! Register Touch Screen with InputSource Endpoint.
         ETG_TRACE_USR4(("spi_tclAAPInputSource::Register TouchScreen- Width:%d, Height:%d ", rAAPInputConfig.u32DisplayHeight, rAAPInputConfig.u32DisplayWidth));

         m_poWlInputSource->registerTouchScreen(rAAPInputConfig.u32DisplayHeight,
        		 rAAPInputConfig.u32DisplayWidth, static_cast<TouchScreenType>(rAAPInputConfig.enAAPTouchScreenType));

         //!Register Key Codes that would be used.
         ETG_TRACE_USR2(("spi_tclAAPInputSource::Register key codes "));

         m_poWlInputSource->registerKeycodes(sKeyCodes);

         //! Register the Input Source Endpoint with the GalReceiver.
         ETG_TRACE_USR2(("spi_tclAAPInputSource::Register Input Source End-point with GAL Receiver"));

         if ( shpoGalReceiver != nullptr )
         {
            bRetVal = shpoGalReceiver->registerService(m_poWlInputSource);
            ETG_TRACE_USR4(("spi_tclAAPInputSource::Registration with GalReceiver : %d\n", ETG_ENUM(
                        BOOL, bRetVal)));
         }
      }
	  else
      {
         ETG_TRACE_ERR(("spi_tclAAPInputSource:: Wayland input initialization failed. Check Wayland environment and configuration \n"));
      }
   }
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  spi_tclAAPInputSource::bUnInitialize()
***************************************************************************/
t_Void spi_tclAAPInputSource::bUnInitialize()
{
   ETG_TRACE_USR1(("spi_tclAAPCmdInput::bUnInitialize() entered "));
   if (NULL != m_poWlInputSource)
   {
      m_poWlInputSource->shutdown();
   }
   //! Delete the end-point since it is created for every connection/disconnection.
   RELEASE_MEM (m_poWlInputSource);
   
   //! Call the library exit point to disable DLT logging for input
   adit::aauto::WaylandExitPoint();

}

/***************************************************************************
** FUNCTION:  spi_tclAAPInputSource::vSetInputConfig()
***************************************************************************/

t_Void spi_tclAAPInputSource::vSetInputConfig(const trAAPInputConfig& rAAPInputConfig )
{
   if ( NULL != m_poWlInputSource )
   {
      StringHandler oStrConverter;

      t_String szLayerID;
      oStrConverter.vConvertIntToStr(rAAPInputConfig.u16LayerID, szLayerID,
            DECIMAL_STRING);

      ETG_TRACE_USR4(("LayerID:%s", szLayerID.c_str()));
      m_poWlInputSource->setConfigItem(cszLayerId.c_str(), szLayerID.c_str());

      t_String szSurfaceID;
      oStrConverter.vConvertIntToStr(rAAPInputConfig.u16SurfaceID, szSurfaceID,
            DECIMAL_STRING);

      ETG_TRACE_USR4(("SurfaceId:%s", szSurfaceID.c_str()));
      m_poWlInputSource->setConfigItem(cszSurfaceId.c_str(),
            szSurfaceID.c_str());

      t_String szDisplayHeight;
      oStrConverter.vConvertIntToStr(rAAPInputConfig.u32DisplayHeight,
            szDisplayHeight, DECIMAL_STRING);

      ETG_TRACE_USR4(("DisplayHeight:%s", szDisplayHeight.c_str()));
      m_poWlInputSource->setConfigItem(cszDisplayHeight.c_str(),
            szDisplayHeight.c_str());

      t_String szDisplayWidth;
      oStrConverter.vConvertIntToStr(rAAPInputConfig.u32DisplayWidth,
            szDisplayWidth, DECIMAL_STRING);

      ETG_TRACE_USR4(("DisplayWidth:%s", szDisplayWidth.c_str()));
      m_poWlInputSource->setConfigItem(cszDisplayWidth.c_str(),
            szDisplayWidth.c_str());

      t_String szTouchHeight;
      oStrConverter.vConvertIntToStr(rAAPInputConfig.u16TouchHeight,
            szTouchHeight, DECIMAL_STRING);

      ETG_TRACE_USR4(("TouchHeight:%s", szTouchHeight.c_str()));
      m_poWlInputSource->setConfigItem(cszTouchHeight.c_str(),
            szTouchHeight.c_str());

      t_String szTouchWidth;
      oStrConverter.vConvertIntToStr(rAAPInputConfig.u16TouchWidth,
            szTouchWidth, DECIMAL_STRING);

      ETG_TRACE_USR4(("TouchWidth:%s", szTouchWidth.c_str()));
      m_poWlInputSource->setConfigItem(cszTouchWidth.c_str(),
            szTouchWidth.c_str());

      t_String szTouchMaximum;
      oStrConverter.vConvertIntToStr(rAAPInputConfig.u8TouchMaximum,
            szTouchMaximum, DECIMAL_STRING);

      ETG_TRACE_USR4(("TouchMaximum:%s", szTouchMaximum.c_str()));
      m_poWlInputSource->setConfigItem(cszTouchMax.c_str(),
            szTouchMaximum.c_str());

      t_String szTriggerInterval;
      oStrConverter.vConvertIntToStr(rAAPInputConfig.u8TriggerInterval,
            szTriggerInterval, DECIMAL_STRING);

      ETG_TRACE_USR4(("TriggerInterval:%s", szTriggerInterval.c_str()));
      m_poWlInputSource->setConfigItem(cszInputTriggerInterval.c_str(),
            szTriggerInterval.c_str());

      t_String szEnableVerbose;
      oStrConverter.vConvertIntToStr(rAAPInputConfig.u8EnableVerbose,
            szEnableVerbose, DECIMAL_STRING);

      ETG_TRACE_USR4(("EnableVerbose:%s", szEnableVerbose.c_str()));
      m_poWlInputSource->setConfigItem(cszEnableVerbose.c_str(),
            szEnableVerbose.c_str());

      //Set to Single touch for now
      ETG_TRACE_USR4(("Touch Type :Single Touch"));
      m_poWlInputSource->setConfigItem(cszTouchType.c_str(), cszMultiTouch.c_str());


   }
}
/***************************************************************************
** FUNCTION:  spi_tclAAPInputSource::vReportkey()
***************************************************************************/
t_Void spi_tclAAPInputSource:: vReportkey(t_U64 u64TimeStamp, t_U32 u32KeyCode, t_Bool bIsDown, t_U32 u32MetaState)
{
   if ( NULL != m_poWlInputSource )
   {
      t_S32 s32ReportKeyStatus = m_poWlInputSource->reportKey(u64TimeStamp,
            u32KeyCode, bIsDown, u32MetaState);

      ETG_TRACE_USR4(("Reporting Key event Status : %d \n", s32ReportKeyStatus));
   }
}

/***************************************************************************
** FUNCTION:  spi_tclAAPInputSource::vReportKnobkey()
***************************************************************************/
t_Void spi_tclAAPInputSource:: vReportKnobkey(t_U64 u64TimeStamp, t_U32 u32KeyCode, t_S32 s32DeltaCnts)
{
   if (NULL != m_poWlInputSource)
   {
      t_S32 s32ReportKnobKeyStatus = m_poWlInputSource->reportRelative(u64TimeStamp, u32KeyCode,s32DeltaCnts);

      ETG_TRACE_USR2(("Reporting Knob Key event Status : %d \n", s32ReportKnobKeyStatus ));
   }
}
#endif /* GEN3X86 */
