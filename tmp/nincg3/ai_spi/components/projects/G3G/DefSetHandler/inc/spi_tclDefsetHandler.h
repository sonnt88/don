/*!
*******************************************************************************
* \file              spi_tclDefsetHandler.h
* \brief             SPI Defset handler
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    SPI Defset handler
COPYRIGHT:      &copy; RBEI

HISTORY:
 Date       |  Author                           | Modifications
 21.02.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version (taken from FC_Animation)

\endverbatim
******************************************************************************/

#ifndef _SPI_TCLDEFSETHANDLER_H
#define _SPI_TCLDEFSETHANDLER_H

//To avoid conflict with placement new
//#include <string>

#define DIAGLIB_INCLUDE_SYSTEM_SET
#include "diaglib_if.h"
using namespace diaglib;

#define SYSSET_TYPE_HMI           (diaglib::EN_TYPE_DEFAULT_HMI)
#define SYSSET_TYPE_TEF           (diaglib::EN_TYPE_DEFAULT_TEF)
#define SYSSET_TYPE_CUSTOMER      (diaglib::EN_TYPE_DEFAULT_CUSTOMER)
#define SYSSET_TYPE_CODING        (diaglib::EN_TYPE_CODING)
#define SYSSET_TYPE_CALIBRATION   (diaglib::EN_TYPE_CALIBRATION)



/*! 
* \class spi_tclDefsetHandler
* \brief Provides interface to register callbacks for Dialib
*
*/

class ahl_tclBaseOneThreadApp;

class spi_tclDefsetHandler: public diaglib::tclSystemSetListenerIF
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDefsetHandler::spi_tclDefsetHandler()
   ***************************************************************************/
   /*!
   * \brief   Parameterized Constructor.
   * \param   [cpoApp]: (->I) Pointer to Main Application.
   * \retval  None
   **************************************************************************/
   spi_tclDefsetHandler(ahl_tclBaseOneThreadApp* const cpoApp);

   /***************************************************************************
   ** FUNCTION:  virtual spi_tclDefsetHandler::~spi_tclDefsetHandler()
   ***************************************************************************/
   /*!
   * \brief   Destructor
   * \param   None
   * \retval  None
   **************************************************************************/
   virtual ~spi_tclDefsetHandler();

   /***************************************************************************
   * SystemSetListener methods
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDefsetHandler::vOnSystemSet()
   ***************************************************************************/
   /*!
   * \brief    Callback function to start System set.
   * \param    [u32SystemSetID]   SystemSet ID of the incoming request
   *           [u32SystemSetType] The type of system set that is requested:
   *           [MsgContext]     Internal message context. Just return exactly
   *                              this context when you answer to this request.
   * \retval   [tU32] ResultCode, currently not checked. Please return 0
   **************************************************************************/
   virtual tU32 vOnSystemSet (
     tU32 u32SystemSetID,
     diaglib::tenSystemSetType u32SystemSetType,
     diaglib::tContext MsgContext
     );

   /***************************************************************************
   ** FUNCTION:  spi_tclDefsetHandler::vOnSystemSetFinished()
   ***************************************************************************/
   /*!
   * \brief    Callback function. Send bAcknowledgeSystemSetFinished
   * \param    [u32SystemSetID]   SystemSet ID of the incoming request
   *           [u32SystemSetType] The type of system set that is requested:
   *           [MsgContext]    Internal message context. Just return exactly
   *                              this context when you answer to this request.
   * \retval   [tU32] ResultCode, currently not checked. Please return 0
   **************************************************************************/
   virtual tU32 vOnSystemSetFinished (
     tU32 u32SystemSetID,
     diaglib::tenSystemSetType u32SystemSetType,
     diaglib::tContext MsgContext
     );

   /***************************************************************************
   ** FUNCTION:  spi_tclDefsetHandler::vOnSystemSetPrepare()
   ***************************************************************************/
   /*!
   * \brief    To know whether System Set can be executed or not.
   *      E.g any communication/functionality which needs to performed can be done here.
   * \param    [u32SystemSetID]  SystemSet ID of the incoming request
   *           [u32SystemSetType]The type of system set that is requested:
   *           [MsgContext]  Internal message context. Just return exactly
   *                              this context when you answer to this request.
   * \retval  [tU32] ResultCode, currently not checked. Please return 0
   **************************************************************************/
   virtual tU32 vOnSystemSetPrepare (
     tU32 u32SystemSetID,
     diaglib::tenSystemSetType u32SystemSetType,
     diaglib::tContext MsgContext
     );

   /***************************************************************************
   ** FUNCTION:  spi_tclDefsetHandler::vOnSystemSetCheck()
   ***************************************************************************/
   /*!
   * \brief    To know all the values are in intendent state or not.
   *
   * \param    [u32SystemSetID]   SystemSet ID of the incoming request
   *           [u32SystemSetType] The type of system set that is requested:
   *           [MsgContext]    Internal message context. Just return exactly
   *                              this context when you answer to this request.
   * \retval    [tU32] ResultCode, currently not checked. Please return 0
   **************************************************************************/
   virtual tU32 vOnSystemSetCheck (
     tU32 u32SystemSetID,
     diaglib::tenSystemSetType u32SystemSetType,
     diaglib::tContext MsgContext
     );

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDefsetHandler::spi_tclDefsetHandler()
   ***************************************************************************/
   /*!
   * \brief   Default Constructor, will not be implemented.
   *          NOTE: This is a technique to disable the Default Constructor for
   *          this class. So if an attempt for the constructor is made compiler
   *          complains.
   **************************************************************************/
   spi_tclDefsetHandler();

   /**************************************************************************
   ** FUNCTION:  spi_tclDefsetHandler::spi_tclDefsetHandler(const...
   **************************************************************************/
   /*!
   * \fn      spi_tclDefsetHandler(const spi_tclDefsetHandler& oClient)
   * \brief   Copy Consturctor, will not be implemented.
   *          Avoids Lint Prio 3 warning: Info 1732: new in constructor for
   *          class'spi_tclDefsetHandler' which has no Copy Consturctor.
   *          NOTE: This is a technique to disable the Copy Consturctor for this
   *          class. So if an attempt for the copying is made linker complains.
   * \param   [IN] poMessage : Property to be set.
   **************************************************************************/
   spi_tclDefsetHandler(const spi_tclDefsetHandler& oDefSetHandler);

   /**************************************************************************
   ** FUNCTION:  spi_tclDefsetHandler::spi_tclDefsetHandler& operator=(...
   **************************************************************************/
   /*!
   * \fn      spi_tclDefsetHandler& operator=(
   *          const spi_tclDefsetHandler& oClient)
   * \brief   Assingment Operater, will not be implemented.
   *          Avoids Lint Prio 3 warning: Info 1732: new in constructor for
   *          class 'spi_tclDefsetHandler' which has no assignment operator.
   *          NOTE: This is a technique to disable the assignment operator for this
   *          class. So if an attempt for the assignment is made compiler complains.
   **************************************************************************/
   spi_tclDefsetHandler& operator=(const spi_tclDefsetHandler& oDefSetHandler);

   //! Pointer to Main Application
   ahl_tclBaseOneThreadApp* const m_cpoMainApp;

   //! Diaglib Service Handler
   diaglib::tclServiceDiaglib* m_poDiaglibService;

};
#endif // _SPI_TCLDEFSETHANDLER_H

// <EOF>

