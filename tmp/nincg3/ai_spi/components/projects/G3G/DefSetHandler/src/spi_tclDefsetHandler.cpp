/*!
*******************************************************************************
* \file              spi_tclDefsetHandler.h
* \brief             SPI Defset handler
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    SPI Defset handler
COPYRIGHT:      &copy; RBEI

HISTORY:
 Date       |  Author                           | Modifications
 21.02.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version (taken from FC_Animation)

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

#include "SPITypes.h"
#include "spi_LoopbackTypes.h"
#include "spi_tclDefsetHandler.h"

//!Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
      #include "trcGenProj/Header/spi_tclDefsetHandler.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| defines
|----------------------------------------------------------------------------*/
#define RELEASE_MEM_OSAL(VAR)      \
   if (OSAL_NULL != VAR)           \
   {                               \
      OSAL_DELETE VAR;             \
      VAR = OSAL_NULL;             \
   }

/******************************************************************************
** FUNCTION:  spi_tclDefsetHandler::spi_tclDefsetHandler(ahl_tclBaseOneThreadApp*)
******************************************************************************/
spi_tclDefsetHandler::spi_tclDefsetHandler(ahl_tclBaseOneThreadApp* const cpoApp)
   : m_cpoMainApp(cpoApp), m_poDiaglibService(OSAL_NULL)
{
   ETG_TRACE_USR1((" spi_tclDefsetHandler() entered \n"));

   m_poDiaglibService = OSAL_NEW tclServiceDiaglib(m_cpoMainApp, 1);
   if ((OSAL_NULL != m_poDiaglibService) && (OSAL_NULL != m_poDiaglibService->poGetSysSet()))
   {
      ETG_TRACE_USR4(("spi_tclDefsetHandler() - Registering System ID with Diaglib"));
      m_poDiaglibService->poGetSysSet()->vRegisterListener(SYSSETID_SMARTPHONE_INTEGRATION, this);
      m_poDiaglibService->poGetSysSet()->vRegisterListener(SYSSETID_ALL_COMP, this);
      m_poDiaglibService->poGetSysSet()->vRegisterListener(SYSSETID_GROUP_HMI_CLEAR_ALL_PRIVATE_DATA, this);
   }
   else
   {
      ETG_TRACE_ERR(("spi_tclDefsetHandler() - Failed to create the Diag Lib Service."));
      SPI_NORMAL_ASSERT_ALWAYS();
   }
}//spi_tclDefsetHandler::spi_tclDefsetHandler(ahl_tclBaseOneThreadApp* const cpoApp)

/******************************************************************************
** FUNCTION:  spi_tclDefsetHandler::~spi_tclDefsetHandler()
******************************************************************************/
spi_tclDefsetHandler::~spi_tclDefsetHandler()
{
   ETG_TRACE_USR1((" ~spi_tclDefsetHandler() entered \n"));
   RELEASE_MEM_OSAL(m_poDiaglibService);
}//spi_tclDefsetHandler::~spi_tclDefsetHandler()

/******************************************************************************
** FUNCTION:  tU32 spi_tclDefsetHandler::vOnSystemSet(tU32 u32SystemSetID,..)
******************************************************************************/
tU32 spi_tclDefsetHandler::vOnSystemSet(
      tU32 u32SystemSetID,
      tenSystemSetType u32SystemSetType,
      tContext MsgContext)
{
   ETG_TRACE_USR1(("spi_tclDefsetHandler::vOnSystemSet - DefSet for SystemSetID: %u, SystemType: %u ",
         u32SystemSetID, ETG_ENUM(DEFSET_SYSSET_TYPE, u32SystemSetType)));

   //! @Note: Actions taken for SystemSetID & SystemSetType are as given below:
   //! 1. SystemSetType = AllComp/SPI, and SystemSetID = TEF/CUSTOMER : EOL is read
   //! 2. SystemSetType = ClearAllPrivateData, and SystemSetID = HMI : Data is cleared

   tenInternalError enError = EN_ERROR_NO_ERROR;

   if (NULL != m_cpoMainApp)
   {
      if (
         ((SYSSETID_ALL_COMP == u32SystemSetID) || (SYSSETID_SMARTPHONE_INTEGRATION == u32SystemSetID))
         &&
         ((SYSSET_TYPE_TEF == u32SystemSetType) || (SYSSET_TYPE_CODING == u32SystemSetType))
         )
      {
         m_cpoMainApp->bPostEvent((tU32)SPI_C_U32_EVENT_ID_DEFSET_READEOL);
         m_cpoMainApp->bPostEvent((tU32)SPI_C_U32_EVENT_ID_DEFSET_TEF);
      }
      else if ((SYSSETID_GROUP_HMI_CLEAR_ALL_PRIVATE_DATA == u32SystemSetID) && (SYSSET_TYPE_HMI == u32SystemSetType))
      {
         m_cpoMainApp->bPostEvent((tU32)SPI_C_U32_EVENT_ID_DEFSET_CLEARPRIVATEDATA);
      }
      else if (SYSSET_TYPE_CUSTOMER == u32SystemSetType)
      {
         m_cpoMainApp->bPostEvent((tU32)SPI_C_U32_EVENT_ID_DEFSET_CUSTOMER);
      }
      else if (SYSSET_TYPE_CALIBRATION == u32SystemSetType)
      {
            // Nothing to be done from SPI point of view.
            // Projects are expecting to return ok even if nothing is done.
            // hence just RETURN OK.
            enError = EN_ERROR_NO_ERROR; // this is added to make lint happy
      }
      else
      {
         enError = EN_ERROR_INCOMPATIBLE_PARAMETER_SIGNATURE;
      }
   }//if (NULL != m_cpoMainApp)
   else
   {
      enError = EN_ERROR_FATAL_INTERNAL;
   }

   tU32 u32Return = U32_DIAGLIB_RETURN_OK;
   tclParameterVector oTmpVec;
   tenSystemSetResult oSysSetResult = EN_SYSTEMSET_OK;

   if (EN_ERROR_NO_ERROR != enError)
   {
      oSysSetResult = EN_SYSTEMSET_NOT_OK;
      trParameter paramError;
      paramError.enType = static_cast<tenParameterType> (EN_PARAMETER_TYPE_U8ERROR);
      paramError.u8Value = enError;
      oTmpVec.push_back(paramError);
      u32Return = U32_DIAGLIB_RETURN_NOT_OK;
   }

   //! Send DefSet result
   if (
      (OSAL_NULL != m_poDiaglibService)
      &&
      (OSAL_NULL != m_poDiaglibService->poGetSysSet())
      &&
      (FALSE == m_poDiaglibService->poGetSysSet()->bSendSystemSetResult(
            oSysSetResult, oTmpVec, MsgContext))
      )
   {
      ETG_TRACE_ERR(("spi_tclDefsetHandler::vOnSystemSet - SendSystemSetResult (for Result: %d) failed! ", u32Return));
   }//if (OSAL_NULL != m_poDiaglibService)

   return u32Return;

}//spi_tclDefsetHandler::vOnSystemSet(...)

/******************************************************************************
** FUNCTION:  tU32 spi_tclDefsetHandler::vOnSystemSetFinished(...)
******************************************************************************/
tU32 spi_tclDefsetHandler::vOnSystemSetFinished(
      tU32 u32SystemSetID,
      tenSystemSetType u32SystemSetType,
      tContext MsgContext)
{  
   ETG_TRACE_USR1(("spi_tclDefsetHandler::vOnSystemSetFinished entered \n"));

   SPI_INTENTIONALLY_UNUSED( u32SystemSetID );
   SPI_INTENTIONALLY_UNUSED( u32SystemSetType );

   //Response does not contain any data and is just an acknowledgment.

   if ((OSAL_NULL != m_poDiaglibService) && (OSAL_NULL != m_poDiaglibService->poGetSysSet()))
   {
      tBool bRetVal = m_poDiaglibService->poGetSysSet()-> bAcknowledgeSystemSetFinished(MsgContext);

      ETG_TRACE_USR4(("spi_tclDefsetHandler::vOnSystemSetFinished - Result:%d", ETG_ENUM(BOOL, bRetVal)));
   }//if (OSAL_NULL != m_poDiaglibService)

   return U32_DIAGLIB_RETURN_OK;
} // spi_tclDefsetHandler::vOnSystemSetFinished (..)

/******************************************************************************
** FUNCTION:  tU32 spi_tclDefsetHandler::vOnSystemSetPrepare(...)
******************************************************************************/
tU32 spi_tclDefsetHandler::vOnSystemSetPrepare(
      tU32 u32SystemSetID,
      tenSystemSetType u32SystemSetType,
      tContext MsgContext)
{ 
   ETG_TRACE_USR1(("spi_tclDefsetHandler::vOnSystemSetPrepare entered \n"));

   SPI_INTENTIONALLY_UNUSED( u32SystemSetID );
   SPI_INTENTIONALLY_UNUSED( u32SystemSetType );

   if ((OSAL_NULL != m_poDiaglibService) && (OSAL_NULL != m_poDiaglibService->poGetSysSet()))
   {
      tBool bResponse = TRUE;
      tclParameterVector oTmpVec;

      tBool bRetVal = m_poDiaglibService->poGetSysSet()->bSendSystemSetResult(
               (bResponse ? EN_SYSTEMSET_OK : EN_SYSTEMSET_NOT_OK), oTmpVec, MsgContext);

      ETG_TRACE_USR4(("spi_tclDefsetHandler::vOnSystemSetPrepare - Result:%d", ETG_ENUM(BOOL, bRetVal)));
   }//if (OSAL_NULL != m_poDiaglibService)

   return U32_DIAGLIB_RETURN_OK;
}//spi_tclDefsetHandler::vOnSystemSetPrepare(..)

/******************************************************************************
** FUNCTION:  tU32 spi_tclDefsetHandler::vOnSystemSetCheck(...)
******************************************************************************/
tU32 spi_tclDefsetHandler::vOnSystemSetCheck(
      tU32 u32SystemSetID,
      tenSystemSetType u32SystemSetType,
      tContext MsgContext)
{  
   ETG_TRACE_USR1(("spi_tclDefsetHandler::vOnSystemSetCheck entered \n"));

   SPI_INTENTIONALLY_UNUSED( u32SystemSetID );
   SPI_INTENTIONALLY_UNUSED( u32SystemSetType );

   if ((OSAL_NULL != m_poDiaglibService) && (OSAL_NULL != m_poDiaglibService->poGetSysSet()))
   {
      tBool bResponse = TRUE;
      tclParameterVector oTmpVec;

      tBool bRetVal = m_poDiaglibService->poGetSysSet()->bSendSystemSetCheckResult(
                  (bResponse ? EN_SYSTEMSET_OK : EN_SYSTEMSET_NOT_OK), oTmpVec, MsgContext);

      ETG_TRACE_USR4(("spi_tclDefsetHandler::vOnSystemSetCheck - Result:%d", ETG_ENUM(BOOL, bRetVal)));
   }//if (OSAL_NULL != m_poDiaglibService)

   return U32_DIAGLIB_RETURN_OK;
}//tU32 spi_tclDefsetHandler::vOnSystemSetCheck(...)

// <EOF>
