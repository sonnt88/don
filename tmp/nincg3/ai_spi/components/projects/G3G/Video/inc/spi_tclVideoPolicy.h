/***********************************************************************/
/*!
* \file  spi_tclVideoPolicy.h
* \brief Project specific Layer Manager Interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Project specific Layer Manager Interface
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
18.10.2013  | Shiva Kumar Gurija    | Initial Version
08.01.2014 |  Hari Priya E R        | Changes to include main app instance
21.01.2014  | Shiva Kumar Gurija    | Added with new elemenst to update Video 
Rendering Status

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLVIDEOPOLICY_H_
#define _SPI_TCLVIDEOPOLICY_H_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
//! Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

#define GENERICMSGS_S_IMPORT_INTERFACE_GENERIC
#include "generic_msgs_if.h"

#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

//Include public FI interface of this service.
#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_HMI_LAYER_SYNCFI_TYPES
#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_HMI_LAYER_SYNCFI_FUNCTIONIDS
#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_HMI_LAYER_SYNCFI_ERRORCODES
#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_HMI_LAYER_SYNCFI_SERVICEINFO
#include "midw_fi_if.h"

#include "spi_tclVideoPolicyBase.h"


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
class ahl_tclBaseOneThreadApp;
class ahl_tclBaseOneThreadService;
class spi_tclVideoIntf;

/****************************************************************************/
/*!
* \class spi_tclVideoPolicy
* \brief 
****************************************************************************/
class spi_tclVideoPolicy:public ahl_tclBaseOneThreadService,public spi_tclVideoPolicyBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclVideoPolicy::spi_tclVideoPolicy()
   ***************************************************************************/
   /*!
   * \fn      spi_tclVideoPolicy(ahl_tclBaseOneThreadApp* poMainApp ,
   *          spi_tclVideoIntf* poVideoIntf)
   * \brief   parameterized Constructor
   * \param   poMainApp    : [IN] Main Application Pointer
   * \param   poVideoIntf  : [IN] Video core class pointer
   * \sa      ~spi_tclVideoPolicy()
   **************************************************************************/
   spi_tclVideoPolicy(ahl_tclBaseOneThreadApp* poMainApp ,spi_tclVideoIntf* poVideoIntf);


   /***************************************************************************
   ** FUNCTION:  spi_tclVideoPolicy::~spi_tclVideoPolicy()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclVideoPolicy()
   * \brief   Destructor
   * \sa      spi_tclVideoPolicy()
   **************************************************************************/
   ~spi_tclVideoPolicy();

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideoPolicy::vUpdateVideoRenderingStatus()
   ***************************************************************************/
   /*!
   * \fn     t_Void vUpdateVideoRenderingStatus(t_Bool bRenderingStarted)
   * \brief  Method to update the video rendering status
   * \param  bRenderingStarted : [IN] true - Video rendering is started
   *                                  false - Video rendering is stopped
   * \retval t_Void
   **************************************************************************/
   t_Void vUpdateVideoRenderingStatus(t_Bool bRenderingStarted);


   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   * Overriding ahl_tclBaseOneThreadService methods.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclVideoPolicy::vOnServiceAvailable()
   ***************************************************************************/
   /*!
   * \fn      virtual tVoid vOnServiceAvailable()
   * \brief   This function is called by the CCA framework when the service
   *          which is offered by this server has become available.
   * \retval  tVoid
   * \sa      vOnServiceUnavailable()
   **************************************************************************/
   virtual tVoid vOnServiceAvailable();

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclVideoPolicy::vOnServiceUnavailable()
   ***************************************************************************/
   /*!
   * \fn      virtual tVoid vOnServiceUnavailable()
   * \brief   This function is called by the CCA framework when the service
   *          which is offered by this server has become unavailable.
   * \retval  tVoid
   * \sa      vOnServiceAvailable()
   **************************************************************************/
   virtual tVoid vOnServiceUnavailable();

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclVideoPolicy::bStatusMessageFactory(tU16 ...
   ***************************************************************************/
   /*!
   * \fn      virtual tBool bStatusMessageFactory(
   *                                               tU16 u16FunctionId,
   *                                               amt_tclServiceData& roOutMsg,
   *                                               amt_tclServiceData* poInMsg)
   * \brief   This function is called by the CCA framework to request ANY
   *          property which is offered by this service. For each property
   *          accessed via parameter 'u16FunctionId' the user has to prepare
   *          the corresponding FI data object which is then copied to the
   *          referenced parameter 'roOutMsg'.
   * \param   u16FunctionId: [IN]  Function ID of the requested property.
   * \param   roOutMsg     : [OUT] Reference to the service data object to which the
   *                                content of the prepared FI data object should be
   *                                copied to.
   * \param   poInMsg      : [IN]  Selector message which is used to select dedicated
   *                               content to be copied to 'roOutMsg' instead of
   *                               updating the entire FI data object.
   * \retval  tBool : TRUE = Status message for property successfully generated.
   *                  FALSE = Failed to generate status message for property.
   **************************************************************************/
   virtual tBool bStatusMessageFactory(
      tU16 u16FunctionId,
      amt_tclServiceData& roOutMsg,
      amt_tclServiceData* poInMsg);

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclVideoPolicy::bProcessSet(amt_tclServiceD...
   ***************************************************************************/
   /*!
   * \fn      virtual tBool bProcessSet(
   *                                    amt_tclServiceData* poMsg,
   *                                    tBool& bPropertyChanged,
   *                                    tU16& u16Error)
   * \brief   This function is called by the CCA framework when it has
   *          received a message for a property with Opcode 'SET' or 'PURESET'
   *          and there is no dedicated handler function defined in the
   *          message map for this pair of FID and opcode. The user has to
   *          set the application specific property to the requested value
   *          and the CCA framework then cares about informing the requesting
   *          client as well as other registered clients.
   * \param   poMsg            : 
   * \param   bPropertyChanged : 
   * \param   u16Error         : 
   * \retval  tBool : TRUE = Send 'STATUS' message to the requesting client if 
   *                  opcode was 'SET'. Send 'STATUS' message to other registered
   *                  clients as well if [OUT] parameter 'bPropertyChanged'
   *                  is set to TRUE.
   *	                FALSE = Send an error message to the requesting client.
   **************************************************************************/
   virtual tBool bProcessSet(
      amt_tclServiceData* poMessage,
      tBool& bPropertyChanged,
      tU16& u16Error);

   /*!
   * \brief  Message map definition macro
   */
   DECLARE_MSG_MAP(spi_tclVideoPolicy)

   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclVideoPolicy::spi_tclVideoPolicy()
   ***************************************************************************/
   /*!
   * \fn      spi_tclVideoPolicy()
   * \brief   Default Constructor
   * \sa      ~spi_tclVideoPolicy()
   **************************************************************************/
   spi_tclVideoPolicy();


   /***************************************************************************
   ** FUNCTION:  spi_tclVideoPolicy& spi_tclVideoPolicy::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclVideoPolicy& operator= (const spi_tclVideoPolicy &orfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclVideoPolicy& operator= (const spi_tclVideoPolicy &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclVideoPolicy::spi_tclVideoPolicy(const spi_tclVideoPol..
   ***************************************************************************/
   /*!
   * \fn      spi_tclVideoPolicy(const spi_tclVideoPolicy &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclVideoPolicy(const spi_tclVideoPolicy &corfrSrc);

   /***************************************************************************
   * Handler function declarations used by message map.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclVideoPolicy::vOnMSCreateAppInst(amt_tclSer..
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSCreateAppInst(amt_tclServiceData* poMsg)
   * \brief   Handler for Create Application Instance
   * \param   poMsg : Message
   * \retval  tVoid
   **************************************************************************/
   tVoid vOnMSCreateAppInst(amt_tclServiceData* poMsg);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclVideoPolicy::vOnMSDeleteAppInst(amt_tclSer..
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSDeleteAppInst(amt_tclServiceData* poMsg)
   * \brief   Handler for Delete Application Instance
   * \param   poMsg : Message
   * \retval  tVoid
   **************************************************************************/
   tVoid vOnMSDeleteAppInst(amt_tclServiceData* poMsg);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclVideoPolicy::vOnMSAppSettings(amt_tclServi...
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSAppSettings(amt_tclServiceData* poMsg)
   * \brief   Handler for Application settings
   * \param   poMsg : Message
   * \retval  tVoid
   **************************************************************************/
   tVoid vOnMSAppSettings(amt_tclServiceData* poMsg);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclVideoPolicy::vOnMSApplySettings(amt_tclSer..
   ***************************************************************************/
   /*!
   * \fn      tVoid vOnMSApplySettings(amt_tclServiceData* poMsg)
   * \brief   Handler for ApplySettings
   * \param   poMsg : Message
   * \retval  tVoid
   **************************************************************************/
   tVoid vOnMSApplySettings(amt_tclServiceData* poMsg);

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclVideoPolicy::bUpdateClients(tCU16 cu16FunID)
   ***************************************************************************/
   /*!
   * \fn      tBool bUpdateClients(tCU16 cu16FunID)
   * \brief   This function is used to update all registered clients.
   *          with respective property status.
   * \param   cu16FunID  : [IN] Property Function ID
   * \retval  tBool  : True if the update is sent successfully
   *                   else False
   **************************************************************************/
   tBool bUpdateClients(tCU16 cu16FunID);

   //! VideoIntf pointer object,used to send requests to lower layer. 
   spi_tclVideoIntf* m_poVideoIntf;

   //! View Status of SPI Layer
   midw_fi_tcl_e32_HMI_ViewStatus m_enViewStatus;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //spi_tclVideoPolicy

#endif //_SPI_TCLVIDEOPOLICY_H_


///////////////////////////////////////////////////////////////////////////////
// <EOF>


