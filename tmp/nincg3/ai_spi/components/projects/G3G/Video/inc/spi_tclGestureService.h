/***********************************************************************/
/*!
* \file  spi_service_Gestures.h
* \brief Project specific gesture_fi Interface
*************************************************************************
 \verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Project specific gesture_fi Interface
AUTHOR:         Vinoop U
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
18.10.2013  | Vinoop U              | Initial Version
08.01.2014 |  Hari Priya E R        | Changes to Extract the raw 
                                     coordinates based on Gesture Event

 \endverbatim
*************************************************************************/
#ifndef _SPI_TCLGESTURESERVICE_H_
#define _SPI_TCLGESTURESERVICE_H_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
//! Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

#define GENERICMSGS_S_IMPORT_INTERFACE_GENERIC
#include "generic_msgs_if.h"

#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

//Include message framework interface (AMT, msgfw).
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE

#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_HMI_GESTUREFI_TYPES
#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_HMI_GESTUREFI_SERVICEINFO
#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_HMI_GESTUREFI_FUNCTIONIDS
#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_HMI_GESTUREFI_ERRORCODES
#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_HMI_GESTUREFI_STDVISITORS
#include "midw_fi_if.h"

#include "XFiObjHandler.h"
using namespace shl::msgHandler;


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
/*! 
 * \XFIObjHandler<midw_hmi_gesturefi_tclMsgGestureEventSet> spi_tMsgGestureEventSet
 * \brief  Gesture Event Set Property
 */

typedef XFiObjHandler<midw_hmi_gesturefi_tclMsgGestureEventSet>
         spi_tMsgGestureEventSet;

typedef XFiObjHandler<midw_hmi_gesturefi_tclMsgEnableCharRecSet>
         spi_tMsgEnableCharRecSet;


typedef midw_fi_tcl_e32_HMI_Gesture_Type Gesture_tType;
typedef midw_fi_tcl_e32_HMI_Gesture_Event Gesture_tEvent;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
class ahl_tclBaseOneThreadService;
class devprj_tclMainApp;



/****************************************************************************/
/*!
* \class spi_tclGestureService
* \brief 
****************************************************************************/



class spi_tclGestureService:public ahl_tclBaseOneThreadService
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclGestureService::spi_tclGestureService()
   ***************************************************************************/
   /*!
   * \fn      spi_tclGestureService(devprj_tclMainApp* poMainApp)
   * \brief   parameterized Constructor
   * \param   poMainApp: [IN] Main Application Pointer
   * \sa      ~spi_tclGestureService()
   **************************************************************************/
   spi_tclGestureService(devprj_tclMainApp* poMainApp);

   /***************************************************************************
   ** FUNCTION:  spi_tclGestureService::~spi_tclGestureService()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclGestureService()
   * \brief   Destructor
   * \sa      spi_tclGestureService()
   **************************************************************************/
   virtual ~spi_tclGestureService();


   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   * Overriding ahl_tclBaseOneThreadService methods.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclGestureService::vOnServiceAvailable()
   ***************************************************************************/
   /*!
   * \fn      virtual tVoid vOnServiceAvailable()
   * \brief   This function is called by the CCA framework when the service
   *          which is offered by this server has become available.
   * \retval  tVoid
   * \sa      vOnServiceUnavailable()
   **************************************************************************/
   virtual tVoid vOnServiceAvailable();

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclGestureService::vOnServiceUnavailable()
   ***************************************************************************/
   /*!
   * \fn      virtual tVoid vOnServiceUnavailable()
   * \brief   This function is called by the CCA framework when the service
   *          which is offered by this server has become unavailable.
   * \retval  tVoid
   * \sa      vOnServiceAvailable()
   **************************************************************************/
   virtual tVoid vOnServiceUnavailable();

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclGestureService::bStatusMessageFactory
   ***************************************************************************/
   /*!
   * \fn      virtual tBool bStatusMessageFactory(
   *                                               tU16 u16FunctionId,
   *                                               amt_tclServiceData& roOutMsg,
   *                                               amt_tclServiceData* poInMsg)
   * \brief   This function is called by the CCA framework to request ANY
   *          property which is offered by this service. For each property
   *          accessed via parameter 'u16FunctionId' the user has to prepare
   *          the corresponding FI data object which is then copied to the
   *          referenced parameter 'roOutMsg'.
   * \param   u16FunctionId: [IN]  Function ID of the requested property.
   * \param   roOutMsg     : [OUT] Reference to the service data object to which the
   *                                content of the prepared FI data object should be
   *                                copied to.
   * \param   poInMsg      : [IN]  Selector message which is used to select dedicated
   *                               content to be copied to 'roOutMsg' instead of
   *                               updating the entire FI data object.
   * \retval  tBool : TRUE = Status message for property successfully generated.
   *                  FALSE = Failed to generate status message for property.
   **************************************************************************/
   virtual tBool bStatusMessageFactory(
      tU16 u16FunctionId,
      amt_tclServiceData& roOutMsg,
      amt_tclServiceData* poInMsg);

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclGestureService::bProcessSet
   ***************************************************************************/
   /*!
   * \fn      virtual tBool bProcessSet(
   *                                    amt_tclServiceData* poMsg,
   *                                    tBool& bPropertyChanged,
   *                                    tU16& u16Error)
   * \brief   This function is called by the CCA framework when it has
   *          received a message for a property with Opcode 'SET' or 'PURESET'
   *          and there is no dedicated handler function defined in the
   *          message map for this pair of FID and opcode. The user has to
   *          set the application specific property to the requested value
   *          and the CCA framework then cares about informing the requesting
   *          client as well as other registered clients.
   * \param   poMsg            : 
   * \param   bPropertyChanged : 
   * \param   u16Error         : 
   * \retval  tBool : TRUE = Send 'STATUS' message to the requesting client if 
   *                  opcode was 'SET'. Send 'STATUS' message to other registered
   *                  clients as well if [OUT] parameter 'bPropertyChanged'
   *                  is set to TRUE.
   *	                FALSE = Send an error message to the requesting client.
   **************************************************************************/
   virtual tBool bProcessSet(
      amt_tclServiceData* poMessage,
      tBool& bPropertyChanged,
      tU16& u16Error);

   /*!
   * \brief  Message map definition macro
   */
   DECLARE_MSG_MAP(spi_tclGestureService)

   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/


private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclGestureService::spi_tclGestureService()
   ***************************************************************************/
   /*!
   * \fn      spi_tclGestureService()
   * \brief   Default Constructor
   * \sa      ~spi_tclGestureService()
   **************************************************************************/
   spi_tclGestureService();


   /***************************************************************************
   ** FUNCTION:  spi_tclGestureService& spi_tclGestureService::operator= .
   ***************************************************************************/
   /*!
   * \fn      spi_tclGestureService& operator= (const spi_tclGestureService &orfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclGestureService& operator= (const spi_tclGestureService &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclGestureService::spi_tclGestureService
   ***************************************************************************/
   /*!
   * \fn      spi_tclGestureService(const spi_tclGestureService &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclGestureService(const spi_tclGestureService &corfrSrc);

   /***************************************************************************
   * Handler function declarations used by message map.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclGestureService::bUpdateClients
   ***************************************************************************/
   /*!
   * \fn      tBool bUpdateClients(tCU16 cu16FunID)
   * \brief   This function is used to update all registered clients.
   *          with respective property status.
   * \param   cu16FunID  : [IN] Property Function ID
   * \retval  tBool  : True if the update is sent successfully
   *                   else False
   **************************************************************************/
   tBool bUpdateClients(tCU16 cu16FunID);

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclGestureService::vPopulateFirstTouchCoord
   ***************************************************************************/
   /*!
   * \fn      tVoid vPopulateFirstTouchCoord(spi_tMsgGestureEventSet oFiGestureEventSet)
   * \brief   This function is used to populate the coordinate info for single touch
   * \param   oFiGestureEventSet  : [IN]Gesture Event Set object
   * \param   enMode              : [IN]Mode of gesture
   * \retval  tVoid 
   **************************************************************************/
   tVoid vPopulateFirstTouchCoord(spi_tMsgGestureEventSet oFiGestureEventSet,tenTouchMode enMode = e8TOUCH_PRESS);

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclGestureService::vPopulateSecondTouchCoord
   ***************************************************************************/
   /*!
   * \fn      tVoid vPopulateSecondTouchCoord(spi_tMsgGestureEventSet oFiGestureEventSet)
   * \brief   This function is used to populate the coordinate info for single touch
   * \param   oFiGestureEventSet  : [IN]Gesture Event Set object
   * \param   enMode              : [IN]Mode of gesture
   * \retval  tVoid 
   **************************************************************************/
   tVoid vPopulateSecondTouchCoord(spi_tMsgGestureEventSet oFiGestureEventSet,tenTouchMode enMode = e8TOUCH_PRESS);


   /***************************************************************************
   ** FUNCTION:  tBool spi_tclGestureService::vSendSingleTouchInfo
   ***************************************************************************/
   /*!
   * \fn      tVoid vSendSingleTouchInfo(trTouchCoordinates& rFirstCoordinate,
   trUserContext rUsrCtxt)
   * \brief   This function is used to populate and send the single touch data
   * \param   rFirstCoordinate  : [IN]Single touch coordinate info
   * \param   rUsrCtxt  : [IN]User context Info
   * \retval  tVoid 
   **************************************************************************/
   tVoid vSendSingleTouchInfo(trTouchCoordinates& rFirstCoordinate,
      trUserContext rUsrCtxt);

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclGestureService::vSendDoubleTouchInfo
   ***************************************************************************/
   /*!
   * \fn      tVoid vSendDoubleTouchInfo(trTouchCoordinates& rFirstCoordinate,
   trTouchCoordinates& rSecondCoordinate,
   trUserContext rUsrCtxt)
   * \brief   This function is used to populate and send the double touch data
   * \param   rFirstCoordinate  : [IN]First touch coordinate info
   * \param   rSecondCoordinate  : [IN]SEcond touch coordinate info
   * \param   rUsrCtxt  : [IN]User context Info
   * \retval  tVoid 
   **************************************************************************/
   tVoid vSendDoubleTouchInfo(trTouchCoordinates& rFirstCoordinate,
      trTouchCoordinates& rSecondCoordinate,
      trUserContext rUsrCtxt);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclGestureService::vHandleStartEvents
   ***************************************************************************/
   /*!
   * \fn      vHandleStartEvents(spi_tMsgGestureEventSet oFiSetDataObj,
      trUserContext rUsrCtxt)
   * \brief   This function is used to handle the Start events
   * \param   oFiSetDataObj  :     [IN]FI Set Data Object
   * \param   rUsrCtxt  :          [IN]User context Info
   * \retval  tVoid 
   **************************************************************************/
   tVoid vHandleStartEvents(spi_tMsgGestureEventSet oFiSetDataObj,
                                         trUserContext rUsrCtxt);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclGestureService::vHandleRawDataUpdateEvents
   ***************************************************************************/
   /*!
   * \fn      vHandleRawDataUpdateEvents(spi_tMsgGestureEventSet oFiSetDataObj,
      trUserContext rUsrCtxt)
   * \brief   This function is used to handle the Raw data update events
   * \param   oFiSetDataObj  :     [IN]FI Set Data Object
   * \param   rUsrCtxt  :          [IN]User context Info
   * \retval  tVoid 
   **************************************************************************/
   tVoid vHandleRawDataUpdateEvents(spi_tMsgGestureEventSet oFiSetDataObj,
                                         trUserContext rUsrCtxt);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclGestureService::vHandleReleaseEvents
   ***************************************************************************/
   /*!
   * \fn      vHandleReleaseEvents(spi_tMsgGestureEventSet oFiSetDataObj,
      trUserContext rUsrCtxt)
   * \brief   This function is used to handle the touch release events
   * \param   rUsrCtxt  :          [IN]User context Info
   * \retval  tVoid 
   **************************************************************************/
   tVoid vHandleReleaseEvents(trUserContext rUsrCtxt);


   //!Touch Co-ordinate of the First Touch
   trTouchCoordinates m_rFirstCoordinate;

   //!Touch Co-ordinate of the First Touch
   trTouchCoordinates m_rSecondCoordinate;

   //!Touch Related Info of First Touch
   trTouchInfo m_rFirstTouchInfo ;

   //! Touch Related Info of Second Touch
   trTouchInfo m_rSecondTouchInfo ;

   //Touch Data containing all the touch elements
   trTouchData m_rTouchData;

   //! Touch Release Validity
   tenTouchReleaseValidity m_enReleaseValidity;


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};

#endif //_SPI_SERVICE_GESTURES_H_


///////////////////////////////////////////////////////////////////////////////
// <EOF>



