/***********************************************************************/
/*!
* \file  spi_tclGestureService.cpp
* \brief Project specific Gesture_fi  Interface
*************************************************************************
 \verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Project specific gesture_fi Interface
AUTHOR:         Vinoop U
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
26.10.2013  | Vinoop U              | Initial Version
08.01.2014 |  Hari Priya E R        | Changes to Extract the raw 
                                     coordinates based on Gesture Event
19.01.2015  | Sameer Chandra        | Adaptations w.r.t AAP requirements.
27.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors

 \endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/



#include "SPITypes.h"

#include "StringHandler.h"
#include "devprj_tclMainApp.h"
#include "FIMsgDispatch.h"
#include "spi_tclCmdInterface.h"
#include "spi_tclGestureService.h"



#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VIDEO
      #include "trcGenProj/Header/spi_tclGestureService.cpp.trc.h"
   #endif
#endif



static const tU8 scou8FirstTouchId = 0;
static const tU8 scou8SecondTouchId = 1;

#define SPI_TCLGESTURESERVICE_FI_MAJOR_VERSION  MIDW_HMI_GESTUREFI_C_U16_SERVICE_MAJORVERSION
#define SPI_TCLGESTURESERVICE_FI_MINOR_VERSION  MIDW_HMI_GESTUREFI_C_U16_SERVICE_MINORVERSION
#define SPI_TCLGESTURESERVICE_FI_PATCH_VERSION  0



//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported

BEGIN_MSG_MAP(spi_tclGestureService, ahl_tclBaseWork)


END_MSG_MAP()

/***************************************************************************
** FUNCTION:  spi_tclGestureService::spi_tclGestureService()
***************************************************************************/
spi_tclGestureService::spi_tclGestureService(devprj_tclMainApp* poMainApp)
:ahl_tclBaseOneThreadService( poMainApp, 
                              CCA_C_U16_SRV_HMI_GESTURE,
                              SPI_TCLGESTURESERVICE_FI_MAJOR_VERSION,
                              SPI_TCLGESTURESERVICE_FI_MINOR_VERSION,
                              SPI_TCLGESTURESERVICE_FI_PATCH_VERSION)
{
   ETG_TRACE_USR1(("spi_tclGestureService() entered \n"));

}

/***************************************************************************
** FUNCTION:  spi_tclGestureService::~spi_tclGestureService()
***************************************************************************/
spi_tclGestureService::~spi_tclGestureService()
{
   //Destructor
   ETG_TRACE_USR1(("~spi_tclGestureService() entered \n"));
}


/***************************************************************************
** FUNCTION:  tVoid spi_tclGestureService::vOnServiceAvailable()
***************************************************************************/
tVoid spi_tclGestureService::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("spi_tclGestureService::vOnServiceAvailable() entered."));
   //Add code
}

/***************************************************************************
** FUNCTION:  tVoid spi_tclGestureService::vOnServiceUnavailable()
***************************************************************************/
tVoid spi_tclGestureService::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("spi_tclGestureService::vOnServiceUnavailable() entered."));
   //Add code
}

/***************************************************************************
** FUNCTION:  tBool spi_tclGestureService::bStatusMessageFactory
***************************************************************************/
tBool spi_tclGestureService::bStatusMessageFactory(
   tU16 u16FunctionId,
   amt_tclServiceData& roOutMsg,
   amt_tclServiceData* poInMsg)
{
   SPI_INTENTIONALLY_UNUSED(roOutMsg);

   ETG_TRACE_USR1(("spi_tclGestureService::bStatusMessageFactory() FID = 0x%4x", 
      u16FunctionId));

   (void*)(poInMsg); //to avoid lint warning

   tBool bSuccess = TRUE;

   switch(u16FunctionId)
   {
   case MIDW_HMI_GESTUREFI_C_U16_GESTUREEVENT:
      {
         ETG_TRACE_USR3(("Nothing to be done in bStatusMessageFactory:MIDW_HMI_GESTUREFI_C_U16_GESTUREEVENT "));
      }
      break;
   case MIDW_HMI_GESTUREFI_C_U16_ENABLECHARREC:
      {
      }
      break;
   default:
      {
         ETG_TRACE_USR3(("In spi_tclGestureService::bStatusMessageFactory: Status Not Defined"));
      }
      break;
   }

   ETG_TRACE_USR3(("Leaving spi_tclGestureService::bStatusMessageFactory"));

   return bSuccess;
}


/***************************************************************************
** FUNCTION:  tBool spi_tclGestureService::bProcessSet
***************************************************************************/

tBool spi_tclGestureService::bProcessSet(
   amt_tclServiceData* poMessage,
   tBool& bPropertyChanged,
   tU16& u16Error)
{
   SPI_INTENTIONALLY_UNUSED(u16Error);

   tBool bSuccess = TRUE;

   tU16 u16FunctionId = poMessage->u16GetFunctionID();

   ETG_TRACE_USR4(("bProcessSet() entered. FID = 0x%4x.", u16FunctionId));


   fi_tclVisitorMessage oVisitorMsg(poMessage);
   switch(u16FunctionId)
   {
      // Handle creation of the FI data object for this function ID and the
      // handover to the referenced service data object 'roOutMsg' here ...

      // Set local variable 'bSuccess' to TRUE if the creation of the FI data
      // object and the handover to 'roOutMsg' was successful.
   case MIDW_HMI_GESTUREFI_C_U16_GESTUREEVENT:

      {
         //  midw_hmi_gesturefi_tclMsgGestureEventSet oFiDataObject;
         spi_tMsgGestureEventSet oFiSetDataObj
            (*poMessage,SPI_TCLGESTURESERVICE_FI_MAJOR_VERSION);

         trUserContext rUsrCtxt;
         CPY_TO_USRCNTXT(poMessage, rUsrCtxt);

         m_rFirstTouchInfo.tvecTouchCoordinatesList.clear();
         m_rSecondTouchInfo.tvecTouchCoordinatesList.clear();
         m_rTouchData.tvecTouchInfoList.clear();

         if ( TRUE == oFiSetDataObj.bIsValid() )
         {
            tU32 u32GestureType = oFiSetDataObj.enGestureType.enType;

            //Process the touch co-ordinates only if the Gesture type is RawData
            if (u32GestureType == Gesture_tType::FI_EN_EN_HMI_GESTURE_RAW_DATA) 
            {
               tU32 u32GestureEvent = oFiSetDataObj.enGestureEvent.enType;
               ETG_TRACE_USR4(("GestureEvent = %d",u32GestureEvent ));

               ETG_TRACE_USR4(("bPoint1Valid = %d",oFiSetDataObj.bPoint1Valid ));
               ETG_TRACE_USR4(("bPoint2Valid = %d",oFiSetDataObj.bPoint2Valid ));

               
               /*For the Gesture Event START,only one of the two touch points is valid at any point of time. */
               if(u32GestureEvent == Gesture_tEvent::FI_EN_EN_HMI_GESTURE_START)
               {
                 vHandleStartEvents(oFiSetDataObj,rUsrCtxt);
               }
               /*For the Gesture Event RAW DATA UPDATE,either one or both the touch points could be valid */
               else if(u32GestureEvent == Gesture_tEvent::FI_EN_EN_HMI_GESTURE_RAW_DATA_UPDATE)
               {
                  vHandleRawDataUpdateEvents(oFiSetDataObj,rUsrCtxt);
               }
               /* Upon receiving the Abort Event,handle the release events for each of the touch events*/
               else if(u32GestureEvent == Gesture_tEvent::FI_EN_EN_HMI_GESTURE_ABORT)
               {
                  vHandleReleaseEvents(rUsrCtxt);
               }
            }
         }
         oFiSetDataObj.vDestroy();
      }
      break;

   default:
      {
         ETG_TRACE_USR3(("bProcessSet: Invalid 'FID = %u'.", u16FunctionId));
         bSuccess = FALSE;
      }
      break;
   }

   if (FALSE == bSuccess)
   {
      //LINT warning removal Symbol u16ErrorCode not subsequently referenced
      //tU16 u16ErrorCode = CCA_C_U16_ERROR_UNKNOWN_FCT_ID;
      ETG_TRACE_ERR(("bProcessSet(). Setting of property with 'FID = %u' failed.",
         u16FunctionId));
   }
   else
   {
      bPropertyChanged = TRUE;

      if(FALSE == bUpdateClients(u16FunctionId))
      {
         ETG_TRACE_ERR(("Update to clients failed for Gesture Event Set"));
      }
   }
   return bSuccess;
}

/***************************************************************************
** FUNCTION:  tVoid spi_tclGestureService::vHandleStartEvents
***************************************************************************/
tVoid spi_tclGestureService::vHandleStartEvents(spi_tMsgGestureEventSet oFiSetDataObj,
                                                trUserContext rUsrCtxt)
{
   ETG_TRACE_USR1(("vHandleStartEvents entered \n"));

   //!In case of AndroidAuto SPI needs to handle multi-touch start events differently.
  spi_tclCmdInterface* poSpiCmdInterface = spi_tclCmdInterface::getInstance();
  tU32 u32DeviceHandle = poSpiCmdInterface->u32GetSelectedDeviceHandle();

  //! Get Device category from selectedDevicehandle.
  tenDeviceCategory enDeviceCategory = poSpiCmdInterface->enGetDeviceCategory(
      u32DeviceHandle);


   /* If both the touch points are valid,then send a double touch trigger with both the the Co-ordinates info.
   Set the release validity to indicate that when the next Release event is received,
   a double release event trigger should be sent for both the  co-ordinates */
   if((true == oFiSetDataObj.bPoint1Valid) && (true == oFiSetDataObj.bPoint2Valid))
   {
      vPopulateFirstTouchCoord(oFiSetDataObj);

      /* In case of AndroidAuto, multi-touch should be denoted with a different
         gesture action, since only one PRESS and RELEASE for whole session of touch points is accepted by
         InputSource end-point.
       */
     (e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory)? (vPopulateSecondTouchCoord(oFiSetDataObj,e8TOUCH_MULTI_PRESS)):
                                                   (vPopulateSecondTouchCoord(oFiSetDataObj));
      vSendDoubleTouchInfo(m_rFirstCoordinate,m_rSecondCoordinate,rUsrCtxt);
      m_enReleaseValidity = e8P1P2Release;
   }
   
   /* If the first touch point is valid,send a single touch event trigger with the First Co-ordinate values.
   Set the Release Validity enum to indicate that when the next Release event is received,
   a release event trigger should be sent for the First co-ordinate.*/
   else if(true == oFiSetDataObj.bPoint1Valid)
   {
      vPopulateFirstTouchCoord(oFiSetDataObj);
      vSendSingleTouchInfo(m_rFirstCoordinate,rUsrCtxt);
      m_enReleaseValidity = e8P1Release;
   }
   /* If the second touch point is valid,send a single touch event trigger with the Second Co-ordinate values.
   Set the Release Validity enum to indicate that when the next Release event is received,
   a release event trigger should be sent for the Second co-ordinate.*/
   else if(true == oFiSetDataObj.bPoint2Valid)
   {
      vPopulateSecondTouchCoord(oFiSetDataObj);
      vSendSingleTouchInfo(m_rSecondCoordinate,rUsrCtxt);
      m_enReleaseValidity = e8P2Release;
   }
}





/***************************************************************************
** FUNCTION:  tVoid spi_tclGestureService::vHandleRawDataUpdateEvents
***************************************************************************/
tVoid spi_tclGestureService::vHandleRawDataUpdateEvents(spi_tMsgGestureEventSet oFiSetDataObj,
                                                        trUserContext rUsrCtxt)
{
   ETG_TRACE_USR1(("vHandleRawDataUpdateEvents entered \n"));

   //!In case of AndroidAuto SPI needs to handle raw data update events differently.
   spi_tclCmdInterface* poSpiCmdInterface = spi_tclCmdInterface::getInstance();
   tU32 u32DeviceHandle = poSpiCmdInterface->u32GetSelectedDeviceHandle();

   //! Get Device category from selectedDevicehandle.
   tenDeviceCategory enDeviceCategory = poSpiCmdInterface->enGetDeviceCategory(u32DeviceHandle);

   /*
    * If both the touch points are valid,then send a double touch trigger with both the the Co-ordinates info.
    * Set the release validity to indicate that when the next Release event is received,
    * a double release event trigger should be sent for both the  co-ordinates
    */


   if((true == oFiSetDataObj.bPoint1Valid) && (true == oFiSetDataObj.bPoint2Valid))
   {

       if (e8P1Release == m_enReleaseValidity )
         {
           /*
            * Suppose non-primary finger press is detected after startEvents, GestureFI sends as
            * a raw data update. In such a case for AndroidAuto, the new touch event start should
            * be indicated explicitly with "ACTION_POINTER_DOWN" event and "ACTION_POINTER_UP" respectively.
            * The primary touch point should be indicated as "ACTION_MOVED".
            */
           (e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory)? (vPopulateFirstTouchCoord(oFiSetDataObj,e8TOUCH_MOVED)):
                                                          (vPopulateFirstTouchCoord(oFiSetDataObj));
           (e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory)? (vPopulateSecondTouchCoord(oFiSetDataObj,e8TOUCH_MULTI_PRESS)):
                                                     (vPopulateSecondTouchCoord(oFiSetDataObj));
         }
       else if( e8P2Release == m_enReleaseValidity )
         {
          /*
           * Suppose non-primary finger press is detected after startEvents, GestureFI sends as
           * a raw data update. In such a case for AndroidAuto, the new touch event start should
           * be indicated explicitly with "ACTION_POINTER_DOWN" event and "ACTION_POINTER_UP" respectively.
           * The primary touch point should be indicated as "ACTION_MOVED".
           */
           (e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory)? (vPopulateFirstTouchCoord(oFiSetDataObj,e8TOUCH_MULTI_PRESS)):
                                                          (vPopulateFirstTouchCoord(oFiSetDataObj));
           (e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory)? (vPopulateSecondTouchCoord(oFiSetDataObj,e8TOUCH_MOVED)):
                                                              (vPopulateSecondTouchCoord(oFiSetDataObj));
         }
       else
         {
           /*
            * Suppose it is not the first press event for any of the touch points it is required for AndroidAuto
            * to indicate the same with only "ACTION_MOVED" instead of "PRESS/RELEASE" as per convention
            */
           (e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory)? (vPopulateFirstTouchCoord(oFiSetDataObj,e8TOUCH_MOVED)):
                                                          (vPopulateFirstTouchCoord(oFiSetDataObj));
           (e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory)? (vPopulateSecondTouchCoord(oFiSetDataObj,e8TOUCH_MOVED)):
                                                                (vPopulateSecondTouchCoord(oFiSetDataObj));
         }


      vSendDoubleTouchInfo(m_rFirstCoordinate,m_rSecondCoordinate,rUsrCtxt);
      m_enReleaseValidity = e8P1P2Release;
   }
   /* Suppose the first touch point is valid and the second touch point is not valid.
   First check if previously there was a double touch and release and in this case,
   just send a double touch event with first touch mode as Press and the second touch mode as release
   If previously there was no double touch,then it could be that there is a swipe with the first finger.
   In this case,send a single touch event trigger with the first co-ordinate value,
   In both the above cases,Set the release validity to indicate that when the next Release event is received,
   a release event trigger should be sent for the first co-ordinate..*/
   else if((true == oFiSetDataObj.bPoint1Valid) && (false == oFiSetDataObj.bPoint2Valid))
   {
      if(e8P1P2Release != m_enReleaseValidity)
      {

         /* In case of AndroidAuto, it is required that the raw update between "PRESS" & "RELEASE"
          * are intimated as "ACTION_MOVED"
          */

          (e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory) ? (vPopulateFirstTouchCoord(oFiSetDataObj,e8TOUCH_MOVED)):
                                                         (vPopulateFirstTouchCoord(oFiSetDataObj));
           vSendSingleTouchInfo(m_rFirstCoordinate,rUsrCtxt);
           m_enReleaseValidity = e8P1Release;

      }
      else
      {
          /*
           * In case of AndroidAuto, in case the non-primary touch point is release/not valid,
           * the phone should be intimated with a "ACTION_POINTER_UP" event.
           */

          (e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory) ? (m_rFirstCoordinate.enTouchMode = e8TOUCH_MOVED):
                                                         (m_rFirstCoordinate.enTouchMode = e8TOUCH_PRESS);
          (e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory) ? (m_rSecondCoordinate.enTouchMode = e8TOUCH_MULTI_RELEASE):
                                                         (m_rSecondCoordinate.enTouchMode = e8TOUCH_RELEASE);
         ETG_TRACE_USR4(("Send second finger release :XCoordinate= %d, YCoordinate= %d",m_rSecondCoordinate.s32XCoordinate,
            m_rSecondCoordinate.s32YCoordinate ));
         vSendDoubleTouchInfo(m_rFirstCoordinate,m_rSecondCoordinate,rUsrCtxt);
         m_enReleaseValidity = e8P1Release;
      }
   }
   /* Suppose the second touch point is valid and the first touch point is not valid.
   First check if previously there was a double touch and release and in this case,
   just send a double touch event with first touch mode as release and the second touch mode as press
   If previously there was no double touch,then it could be that there is a swipe with the second finger.
   In this case,send a single touch event trigger with the second co-ordinate value,
   In both the above cases,Set the release validity to indicate that when the next Release event is received,
   a release event trigger should be sent for the second co-ordinate..*/
   else if((false == oFiSetDataObj.bPoint1Valid) && (true == oFiSetDataObj.bPoint2Valid))
   {
       if (e8P1P2Release != m_enReleaseValidity)
        {
           /*
            * In case of AndroidAuto, it is required that the raw update between "PRESS" & "RELEASE"
            * are intimated as "ACTION_MOVED"
            */

          (e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory) ? (vPopulateSecondTouchCoord(oFiSetDataObj, e8TOUCH_MOVED)):
                                                         (vPopulateSecondTouchCoord(oFiSetDataObj));
          vSendSingleTouchInfo(m_rSecondCoordinate, rUsrCtxt);
          m_enReleaseValidity = e8P2Release;

        }
       else
       {
           /*
            * In case of AndroidAuto, in case the non-primary touch point is release/not valid,
            * the phone should be intimated with a "ACTION_POINTER_UP" event.
            */

           (e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory) ? (m_rFirstCoordinate.enTouchMode = e8TOUCH_MULTI_RELEASE):
                                                          (m_rFirstCoordinate.enTouchMode = e8TOUCH_RELEASE);
           (e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory) ? (m_rSecondCoordinate.enTouchMode = e8TOUCH_MOVED):
                                                          (m_rSecondCoordinate.enTouchMode = e8TOUCH_PRESS);

          ETG_TRACE_USR4(("Send first finger release :XCoordinate= %d, YCoordinate= %d",m_rFirstCoordinate.s32XCoordinate,
              m_rFirstCoordinate.s32YCoordinate ));
          vSendDoubleTouchInfo(m_rFirstCoordinate,m_rSecondCoordinate,rUsrCtxt);
          m_enReleaseValidity = e8P2Release;
       }
   }
   else if ((false == oFiSetDataObj.bPoint1Valid) && (false == oFiSetDataObj.bPoint2Valid))
     {
       ETG_TRACE_USR1(("Release equivalent raw data!\n"));
   }
}


/***************************************************************************
** FUNCTION:  tBool spi_tclGestureService::vHandleReleaseEvents
***************************************************************************/
tVoid spi_tclGestureService::vHandleReleaseEvents(trUserContext rUsrCtxt)
{
   ETG_TRACE_USR1(("vHandleReleaseEvents entered \n"));

   //!In case of AndroidAuto SPI needs to handle multiple release events differently.
   spi_tclCmdInterface* poSpiCmdInterface = spi_tclCmdInterface::getInstance();
   tU32 u32DeviceHandle = poSpiCmdInterface->u32GetSelectedDeviceHandle();

   //! Get Device category from selectedDevicehandle.
   tenDeviceCategory enDeviceCategory = poSpiCmdInterface->enGetDeviceCategory(u32DeviceHandle);
   switch(m_enReleaseValidity)
   {
   case e8P1Release:
      {
         m_rFirstCoordinate.enTouchMode = e8TOUCH_RELEASE;
         vSendSingleTouchInfo(m_rFirstCoordinate,rUsrCtxt);
      }
      break;
   case e8P2Release:
      {
         m_rSecondCoordinate.enTouchMode = e8TOUCH_RELEASE;
         vSendSingleTouchInfo(m_rSecondCoordinate,rUsrCtxt);
      }
      break;
   case e8P1P2Release:
      {
        /*
         * In case of AndroidAuto, in case the non-primary touch point is release/not valid,
         * the phone should be intimated with a "ACTION_POINTER_UP" event.
         */

        (e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory) ? (m_rFirstCoordinate.enTouchMode = e8TOUCH_MULTI_RELEASE):
                                                       (m_rFirstCoordinate.enTouchMode = e8TOUCH_RELEASE);
         m_rSecondCoordinate.enTouchMode = e8TOUCH_RELEASE;

         vSendDoubleTouchInfo(m_rFirstCoordinate,m_rSecondCoordinate,rUsrCtxt);
      }
      break;
   }
}
/***************************************************************************
** FUNCTION:  tBool spi_tclVideoPolicy::bUpdateClients
***************************************************************************/
tBool spi_tclGestureService::bUpdateClients(tCU16 cu16FunID)
{
   tBool bRet = FALSE;
   ail_tenCommunicationError enCommError = AIL_EN_N_NO_ERROR;

   //Update all clients registered for the property.
   enCommError = eUpdateClients(cu16FunID);

   bRet = (AIL_EN_N_NO_ERROR == enCommError);

   if(FALSE == bRet)
   {
      ETG_TRACE_ERR(( "bUpdateClients failed: %d", enCommError));
   }//if(FALSE == bRet)

   return bRet;
}

 /***************************************************************************
 ** FUNCTION:  tBool spi_tclGestureService::vSendSingleTouchInfo
 ***************************************************************************/
tVoid spi_tclGestureService::vSendSingleTouchInfo(
   trTouchCoordinates& rFirstCoordinate,
   trUserContext rUsrCtxt)
{
   ETG_TRACE_USR1(("vSendSingleTouchInfo() entered."));

   spi_tclCmdInterface* poSpiCmdInterface = spi_tclCmdInterface::getInstance();

   m_rFirstTouchInfo.tvecTouchCoordinatesList.push_back(rFirstCoordinate);
   m_rTouchData.tvecTouchInfoList.push_back(m_rFirstTouchInfo);
   m_rTouchData.u32TouchDescriptors = SINGLE_TOUCH;

   if(OSAL_NULL != poSpiCmdInterface)
   {
      tU32 u32DeviceHandle = poSpiCmdInterface->u32GetSelectedDeviceHandle();
      poSpiCmdInterface->vSendTouchEvent(u32DeviceHandle,m_rTouchData,rUsrCtxt);
   }
}


 /***************************************************************************
 ** FUNCTION:  tBool spi_tclGestureService::vSendDoubleTouchInfo()
 ***************************************************************************/
tVoid spi_tclGestureService::vSendDoubleTouchInfo(
   trTouchCoordinates& rFirstCoordinate,
   trTouchCoordinates& rSecondCoordinate,
   trUserContext rUsrCtxt)
{
   ETG_TRACE_USR1(("vSendDoubleTouchInfo() entered."));

   spi_tclCmdInterface* poSpiCmdInterface = spi_tclCmdInterface::getInstance();


   m_rFirstTouchInfo.tvecTouchCoordinatesList.push_back(rFirstCoordinate);
   m_rSecondTouchInfo.tvecTouchCoordinatesList.push_back(rSecondCoordinate);

   m_rTouchData.tvecTouchInfoList.push_back(m_rFirstTouchInfo);
   m_rTouchData.tvecTouchInfoList.push_back(m_rSecondTouchInfo);
   m_rTouchData.u32TouchDescriptors = DOUBLE_TOUCH;


   if(OSAL_NULL != poSpiCmdInterface)
   {
      tU32 u32DeviceHandle = poSpiCmdInterface->u32GetSelectedDeviceHandle();
      poSpiCmdInterface->vSendTouchEvent(u32DeviceHandle,m_rTouchData,rUsrCtxt);
   }
}


/***************************************************************************
** FUNCTION:  tBool spi_tclGestureService::vPopulateFirstTouchCoord
***************************************************************************/
tVoid spi_tclGestureService::vPopulateFirstTouchCoord(spi_tMsgGestureEventSet oFiGestureEventSet,tenTouchMode enMode)
{
   ETG_TRACE_USR1(("vPopulateFirstTouchCoord() entered."));

   //!Populate the first touch co-ordinate info
   m_rFirstCoordinate.enTouchMode = enMode;
   m_rFirstCoordinate.s32XCoordinate = oFiGestureEventSet.rGesturePoint1.s32XCoordinate;
   m_rFirstCoordinate.s32YCoordinate= oFiGestureEventSet.rGesturePoint1.s32YCoordinate;
   m_rFirstCoordinate.u8Identifier = scou8FirstTouchId;

   ETG_TRACE_USR4(("First XCoordinate= %d", m_rFirstCoordinate.s32XCoordinate));
   ETG_TRACE_USR4(("First YCoordinate= %d", m_rFirstCoordinate.s32YCoordinate));
}


/***************************************************************************
** FUNCTION:  tBool spi_tclGestureService::vPopulateSecondTouchCoord
***************************************************************************/
tVoid spi_tclGestureService::vPopulateSecondTouchCoord(spi_tMsgGestureEventSet oFiGestureEventSet,tenTouchMode enMode)
{
   ETG_TRACE_USR1(("vPopulateSecondTouchCoord() entered."));

   //!Populate the second touch co-ordinate info
   m_rSecondCoordinate.enTouchMode = enMode;
   m_rSecondCoordinate.s32XCoordinate = oFiGestureEventSet.rGesturePoint2.s32XCoordinate;
   m_rSecondCoordinate.s32YCoordinate= oFiGestureEventSet.rGesturePoint2.s32YCoordinate;
   m_rSecondCoordinate.u8Identifier = scou8SecondTouchId;

   ETG_TRACE_USR4(("Second XCoordinate= %d", m_rSecondCoordinate.s32XCoordinate));
   ETG_TRACE_USR4(("Second YCoordinate= %d",m_rSecondCoordinate.s32YCoordinate));
}
//lint –restore





