/***********************************************************************/
/*!
* \file  spi_tclVideoPolicy.cpp
* \brief Project specific Layer Manager Interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Project specific Layer Manager Interface
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
18.10.2013  | Shiva Kumar Gurija    | Initial Version
08.01.2014 |  Hari Priya E R        | Changes to include main app instance
21.01.2014  | Shiva Kumar Gurija    | Added with new elemenst to update Video 
Rendering Status

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/


#include "SPITypes.h"
#include "StringHandler.h"
#include "XFiObjHandler.h"
#include "FIMsgDispatch.h"
#include "spi_tclConfigReader.h"
#include "spi_tclVideoIntf.h"
#include "spi_tclVideoPolicy.h"

using namespace shl::msgHandler;


#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VIDEO
#include "trcGenProj/Header/spi_tclVideoPolicy.cpp.trc.h"
#endif
#endif


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
/*! 
* \XFIObjHandler<midw_hmi_layer_syncfi_tclMsgCreateApplicationInstanceMethodStart> spi_tMsgCreateAppInstMS
* \brief  Method start, with auto extract & auto destroy feature.
*/
typedef XFiObjHandler<midw_hmi_layer_syncfi_tclMsgCreateApplicationInstanceMethodStart>
spi_tMsgCreateAppInstMS;

/*! 
* \XFIObjHandler<midw_hmi_layer_syncfi_tclMsgDeleteApplicationInstanceMethodStart> spi_tMsgDeleteAppInstMS
* \brief  Method start, with auto extract & auto destroy feature.
*/
typedef XFiObjHandler<midw_hmi_layer_syncfi_tclMsgDeleteApplicationInstanceMethodStart>
spi_tMsgDeleteAppInstMS;

/*! 
* \XFIObjHandler<midw_hmi_layer_syncfi_tclMsgApplicationSettingsMethodStart> spi_tMsgAppSettingsMS
* \brief  Method start, with auto extract & auto destroy feature.
*/
typedef XFiObjHandler<midw_hmi_layer_syncfi_tclMsgApplicationSettingsMethodStart>
spi_tMsgAppSettingsMS;

/*! 
* \XFIObjHandler<midw_hmi_layer_syncfi_tclMsgApplySettingsMethodStart> spi_tMsgApplySettingsMS
* \brief  Method start, with auto extract & auto destroy feature.
*/
typedef XFiObjHandler<midw_hmi_layer_syncfi_tclMsgApplySettingsMethodStart>
spi_tMsgApplySettingsMS;

/*! 
* \XFIObjHandler<midw_hmi_layer_syncfi_tclMsgViewStatusSet> spi_tMsgViewStatusSet
* \brief  Property Set, with auto extract & auto destroy feature.
*/
typedef XFiObjHandler<midw_hmi_layer_syncfi_tclMsgViewStatusSet>
spi_tMsgViewStatusSet;


typedef midw_fi_tcl_e32_HMI_ViewStatus::tenType spi_tenViewStatus;

typedef enum
{
   //EN_VIEW_STATUS_INVALID = midw_fi_tcl_e32_HMI_ViewStatus::FI_EN_EN_HMI_LAYER_SYNC_VIEW_STATUS_INVALID, //Commented to suppress Lint Fix
   EN_VIEW_STATUS_ACTIVE = midw_fi_tcl_e32_HMI_ViewStatus::FI_EN_EN_HMI_LAYER_SYNC_VIEW_STATUS_ACTIVE,
   EN_VIEW_STATUS_STOPPED = midw_fi_tcl_e32_HMI_ViewStatus::FI_EN_EN_HMI_LAYER_SYNC_VIEW_STATUS_STOPPED
   //EN_VIEW_STATUS_ABOUT_TO_CHANGE = midw_fi_tcl_e32_HMI_ViewStatus::FI_EN_EN_HMI_LAYER_SYNC_VIEW_STATUS_ABOUT_TO_CHANGE  //Commented to suppress Lint Fix
}spi_tenViewStatusType;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
#define SPI_TCLVIDEOPOLICY_MAJOR_VERSION  MIDW_HMI_LAYER_SYNCFI_C_U16_SERVICE_MAJORVERSION
#define SPI_TCLVIDEOPOLICY_MINOR_VERSION  MIDW_HMI_LAYER_SYNCFI_C_U16_SERVICE_MINORVERSION
#define SPI_TCLVIDEOPOLICY_PATCH_VERSION  0



/*!
* \brief CCA MESSAGE MAP 
*/

BEGIN_MSG_MAP(spi_tclVideoPolicy, ahl_tclBaseWork)

   ON_MESSAGE_SVCDATA(MIDW_HMI_LAYER_SYNCFI_C_U16_CREATEAPPLICATIONINSTANCE,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSCreateAppInst)

   ON_MESSAGE_SVCDATA(MIDW_HMI_LAYER_SYNCFI_C_U16_DELETEAPPLICATIONINSTANCE,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSDeleteAppInst)

   ON_MESSAGE_SVCDATA(MIDW_HMI_LAYER_SYNCFI_C_U16_APPLICATIONSETTINGS,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSAppSettings)

   ON_MESSAGE_SVCDATA(MIDW_HMI_LAYER_SYNCFI_C_U16_APPLYSETTINGS,
   AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSApplySettings)

END_MSG_MAP()


/***************************************************************************
** FUNCTION:  spi_tclVideoPolicy::spi_tclVideoPolicy()
***************************************************************************/
spi_tclVideoPolicy::spi_tclVideoPolicy( ahl_tclBaseOneThreadApp* poMainApp,
                                       spi_tclVideoIntf* poVideoIntf)
:ahl_tclBaseOneThreadService( poMainApp, 
                             MIDW_HMI_LAYER_SYNCFI_C_U16_SERVICE_ID,
                             SPI_TCLVIDEOPOLICY_MAJOR_VERSION,
                             SPI_TCLVIDEOPOLICY_MINOR_VERSION,
                             SPI_TCLVIDEOPOLICY_PATCH_VERSION),
                             m_poVideoIntf(poVideoIntf)
{
   //constructor
   ETG_TRACE_USR1(("spi_tclVideoPolicy() entered \n"));
}

/***************************************************************************
** FUNCTION:  spi_tclVideoPolicy::~spi_tclVideoPolicy()
***************************************************************************/
spi_tclVideoPolicy::~spi_tclVideoPolicy()
{
   //Destructor
   ETG_TRACE_USR1(("~spi_tclVideoPolicy() entered \n"));
   m_poVideoIntf = OSAL_NULL;
}


/***************************************************************************
** FUNCTION:  tVoid spi_tclVideoPolicy::vOnServiceAvailable()
***************************************************************************/
tVoid spi_tclVideoPolicy::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("spi_tclLayerSyncService::vOnServiceAvailable() entered."));
   //Add code
}

/***************************************************************************
** FUNCTION:  tVoid spi_tclVideoPolicy::vOnServiceUnavailable()
***************************************************************************/
tVoid spi_tclVideoPolicy::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("spi_tclLayerSyncService::vOnServiceUnavailable() entered."));
   //Add code
}

/***************************************************************************
** FUNCTION:  tBool spi_tclVideoPolicy::bStatusMessageFactory(tU16 ...
***************************************************************************/
tBool spi_tclVideoPolicy::bStatusMessageFactory(tU16 u16FunctionId,
                                                amt_tclServiceData& roOutMsg,
                                                amt_tclServiceData* poInMsg)
{
   ETG_TRACE_USR1(("spi_tclLayerSyncService::bStatusMessageFactory() FID = 0x%4x", u16FunctionId));

   (tVoid)(poInMsg); //to avoid lint warning

   tBool bSuccess = FALSE;

   switch (u16FunctionId) 
   {
   case MIDW_HMI_LAYER_SYNCFI_C_U16_VIEWSTATUS: 
      {

         midw_hmi_layer_syncfi_tclMsgViewStatusStatus oFiStatusDataObj;

         //Set the View Handle to Zero, since SPI doesn't support multiple views
         oFiStatusDataObj.u32ViewHandle = 0 ;

         oFiStatusDataObj.enViewStatus = m_enViewStatus;

         //Hand over the message 
         fi_tclVisitorMessage oVisitorMsg(oFiStatusDataObj,SPI_TCLVIDEOPOLICY_MAJOR_VERSION);
         bSuccess = oVisitorMsg.bHandOver(&roOutMsg);

      }
      break;

      //Currently it is not required for SPI, since we have only one view
   case MIDW_HMI_LAYER_SYNCFI_C_U16_VIEWTYPE: 
      {

         midw_hmi_layer_syncfi_tclMsgViewTypeStatus oFiStatusDataObj;

         //Set the View Handle to Zero, since SPI doesn't support multiple views
         oFiStatusDataObj.u32ViewHandle = 0 ;

         //Set the View Type to TEXT(default value), since SPI doesn't need Video type(defined for IApps)
         oFiStatusDataObj.enViewType.enType = midw_fi_tcl_e32_HMI_ViewType::FI_EN_EN_HMI_LAYER_SYNC_VIEW_TYPE_TEXT;

         //Hand over the message 
         fi_tclVisitorMessage oVisitorMsg(oFiStatusDataObj,SPI_TCLVIDEOPOLICY_MAJOR_VERSION);
         bSuccess = oVisitorMsg.bHandOver(&roOutMsg);
      }
      break;

   default: 
      {
         ETG_TRACE_ERR(("spi_tclLayerSyncService::bStatusMessageFactory: Status Not Defined"));
      }
      break;

   }//switch (u16FunctionId) 

   if (FALSE == bSuccess)
   {
      ETG_TRACE_ERR(("bStatusMessageFactory: Creation of message with 'FID = %u' failed.", u16FunctionId));
      NORMAL_M_ASSERT_ALWAYS();
   }// if (false == bSuccess)

   return bSuccess;
}


/***************************************************************************
** FUNCTION:  tBool spi_tclVideoPolicy::bProcessSet(amt_tclServiceD...
***************************************************************************/

tBool spi_tclVideoPolicy::bProcessSet(amt_tclServiceData* poMsg, 
                                      tBool& bPropertyChanged, 
                                      tU16& u16Error)
{
   ETG_TRACE_USR1(("spi_tclLayerSyncService::bProcessSet() entered."));

   (tVoid)u16Error; //To avoid lint Warning

   tBool bSuccess = TRUE; 

   tU16 u16FunctionId = poMsg->u16GetFunctionID();
   ETG_TRACE_USR2(("spi_tclLayerSyncService::bProcessSet: FID = 0x%4x", u16FunctionId));

   switch(u16FunctionId)
   {
   case MIDW_HMI_LAYER_SYNCFI_C_U16_VIEWSTATUS:
      {
         spi_tMsgViewStatusSet oFiSetDataObj(*poMsg,SPI_TCLVIDEOPOLICY_MAJOR_VERSION);
         if( ( TRUE == oFiSetDataObj.bIsValid() )&&( 0 == oFiSetDataObj.u32ViewHandle ) )
         {
            spi_tenViewStatusType enViewStatusType = static_cast<spi_tenViewStatusType>(oFiSetDataObj.enViewStatus.enType);

            switch(enViewStatusType)
            {
            case EN_VIEW_STATUS_ACTIVE:
               {
                  //send start rendering req to ADIT and 
                  //send status update when you get update from ADIT
                  if(OSAL_NULL != m_poVideoIntf)
                  {
                     m_poVideoIntf->vStartVideoRendering(true);
                  }//if(NULL != m_poVideoIntf)
               }
               break;
            case EN_VIEW_STATUS_STOPPED:
               {
                  //send stop rendering req to ADIT and 
                  //send status update when you get update from ADIT
                  if(OSAL_NULL != m_poVideoIntf)
                  {
                     m_poVideoIntf->vStartVideoRendering(false);
                  }//if(NULL != m_poVideoIntf)
               }
               break;
            default:
               {
                  ETG_TRACE_ERR(("spi_tclLayerSyncService::bProcessSet(). Invalid View Status"));
               }//default
               break;
            }//switch(oFiSetDataObj.enViewStatus.enType)
         }//if(( TRUE == oFiSetDataObj.bIsValid() )
      }//case MIDW_HMI_LAYER_SYNCFI_C_U16_VIEWSTATUS
      break;
   default:
      {
         ETG_TRACE_ERR(("spi_tclLayerSyncService::bProcessSet(). Invalid 'FID = %u'.", u16FunctionId));
         bSuccess = FALSE;
      }//default
      break;
   }//switch(u16FunctionId)

   if (FALSE == bSuccess)
   {
      ETG_TRACE_ERR(("spi_tclLayerSyncService::bProcessSet(). Setting of property with 'FID = 0x%4x' failed.", u16FunctionId));
      NORMAL_M_ASSERT_ALWAYS();
   }//if (FALSE == bSuccess)

   bPropertyChanged = bSuccess;


   return bSuccess;
}

/***************************************************************************
** FUNCTION:  tVoid spi_tclVideoPolicy::vOnMSCreateAppInst(amt_tclS...
***************************************************************************/
tVoid spi_tclVideoPolicy::vOnMSCreateAppInst(amt_tclServiceData* poMsg)
{
   ETG_TRACE_USR1(("vOnMSCreateAppInst entered"));

   spi_tMsgCreateAppInstMS oFiMSDataObj(*poMsg,SPI_TCLVIDEOPOLICY_MAJOR_VERSION);
   if( TRUE == oFiMSDataObj.bIsValid() )
   {
      trMsgContext rMsgCntxt;
      CPY_TO_USRCNTXT(poMsg, rMsgCntxt.rUserContext);

      midw_hmi_layer_syncfi_tclMsgCreateApplicationInstanceMethodResult oFiMRDataObj;

      //Set the View Handle to Zero(Default Value), since SPI doesn't support multiple views
      oFiMRDataObj.u32ViewHandle = 0;

      //Fill the Layer Id details - get the details from tclVideo
      midw_fi_tcl_ApplicationLayerDetails oLayerdetails;

      tU32 u32LayerId = 0;

      spi_tclConfigReader* poConfigReader = spi_tclConfigReader::getInstance();
      if(NULL!= poConfigReader)
      {
         //For GM, RNAIVI and Suzuki, same layer ID is used for all Mirror link, Carplay an Android Auto tech.
         //This file is common for all these projects.
         trVideoConfigData rVideoConfigData;
         poConfigReader->vGetVideoConfigData(e8DEV_TYPE_MIRRORLINK,rVideoConfigData);
         u32LayerId = rVideoConfigData.u32LayerId;
      }//if(OSAL_NULL != poConfigReader)

      StringHandler oStrUtil;
      std::string szLayerName;
      //Convert Layer Name to Hex string
      oStrUtil.vConvertIntToStr(u32LayerId, szLayerName);

      oLayerdetails.szLayerName.bSet(szLayerName.c_str(), midw_fi_tclString::FI_EN_UTF8);
      oFiMRDataObj.rListOfLayerDetails.push_back(oLayerdetails);

      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oFiMRDataObj,rMsgCntxt,SPI_TCLVIDEOPOLICY_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("vOnMSCreateAppInst:Error in posting Message"));
      }//if( FALSE == oPostMsg.bSendMessage

   }//if(( TRUE == oFiMSDataObj.bIsValid()..
   else
   {
      ETG_TRACE_ERR(("vOnMSCreateAppInst: Fi Msg is Invalid or PoMsg is NULL"));
   }

}


/***************************************************************************
** FUNCTION:  tVoid spi_tclVideoPolicy::vOnMSDeleteAppInst(amt_tclSe...
***************************************************************************/
tVoid spi_tclVideoPolicy::vOnMSDeleteAppInst(amt_tclServiceData* poMsg)
{
   ETG_TRACE_USR1(("vOnMSDeleteAppInst entered"));

   spi_tMsgDeleteAppInstMS oFiMSDataObj(*poMsg,SPI_TCLVIDEOPOLICY_MAJOR_VERSION);
   if(
      ( TRUE == oFiMSDataObj.bIsValid() )
      &&
      //SPI supports only one view and the view handle is set to zero(Default Value).
      ( 0 == oFiMSDataObj.u32ViewHandle )
      )
   {
      trMsgContext rMsgCntxt;
      CPY_TO_USRCNTXT(poMsg, rMsgCntxt.rUserContext);

      //Nothing needs to be done in SPI on this method start,just send the response
      midw_hmi_layer_syncfi_tclMsgDeleteApplicationInstanceMethodResult oFiMRDataObj;

      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oFiMRDataObj,rMsgCntxt,SPI_TCLVIDEOPOLICY_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("vOnMSDeleteAppInst:Error in posting Message"));
      }//if( FALSE == oPostMsg.bSendMessage

   }//if(( TRUE == oFiMSDataObj.bIsValid()..
   else
   {
      ETG_TRACE_ERR(("vOnMSDeleteAppInst: Invalid View Handle or Fi Msg is Invalid or PoMsg is NULL"));
   }

}

/***************************************************************************
** FUNCTION:  tVoid spi_tclVideoPolicy::vOnMSAppSettings(amt_tclServi...
***************************************************************************/
tVoid spi_tclVideoPolicy::vOnMSAppSettings(amt_tclServiceData* poMsg)
{
   ETG_TRACE_USR1(("vOnMSAppSettings entered"));

   spi_tMsgAppSettingsMS oFiMSDataObj(*poMsg,SPI_TCLVIDEOPOLICY_MAJOR_VERSION);
   if(
      ( TRUE == oFiMSDataObj.bIsValid() )
      &&
      //SPI supports only one view and the view handle is set to zero(Default Value).
      ( 0 == oFiMSDataObj.u32ViewHandle )
      )
   {
      //Nothing needs to be done on On Application Settings Request, Just post the 
      //response to HMI. This method start request followed by View Status Set Req.
      //We no need to apply settings again and again, this was defined for MAP usage.
      trMsgContext rMsgCntxt;
      CPY_TO_USRCNTXT(poMsg, rMsgCntxt.rUserContext);

      midw_hmi_layer_syncfi_tclMsgApplicationSettingsMethodResult oFiMRDataObj;

      //Set the View Handle to zero, Since SPI is not supporting multiple views.
      oFiMRDataObj.u32ViewHandle = 0;

      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oFiMRDataObj,rMsgCntxt,SPI_TCLVIDEOPOLICY_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("vOnMSAppSettings:Error in posting Message"));
      }//if( FALSE == oPostMsg.bSendMessage

   }//if(( TRUE == oFiMSDataObj.bIsValid()..
   else
   {
      ETG_TRACE_ERR(("vOnMSAppSettings: Invalid View Handle or Fi Msg is Invalid or PoMsg is NULL"));
   }
}


/***************************************************************************
** FUNCTION:  tVoid spi_tclVideoPolicy::vOnMSApplySettings(amt_tclSer..
***************************************************************************/
tVoid spi_tclVideoPolicy::vOnMSApplySettings(amt_tclServiceData* poMsg)
{
   ETG_TRACE_USR1(("vOnMSApplySettings entered"));

   spi_tMsgApplySettingsMS oFiMSDataObj(*poMsg,SPI_TCLVIDEOPOLICY_MAJOR_VERSION);
   if(
      ( TRUE == oFiMSDataObj.bIsValid() )
      &&
      //SPI supports only one view and the view handle is set to zero(Default Value).
      ( 0 == oFiMSDataObj.u32ViewHandle )
      )
   {
      trMsgContext rMsgCntxt;
      CPY_TO_USRCNTXT(poMsg, rMsgCntxt.rUserContext);

      //Nothing needs to be done on On Apply Settings Request, Just post the 
      //response to HMI.
      midw_hmi_layer_syncfi_tclMsgApplySettingsMethodResult oFiMRDataObj;

      //Set the View Handle to zero, Since SPI is not supporting multiple views.
      oFiMRDataObj.u32ViewHandle = 0;

      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oFiMRDataObj,rMsgCntxt,SPI_TCLVIDEOPOLICY_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("vOnMSApplySettings:Error in posting Message"));
      }//if( FALSE == oPostMsg.bSendMessage

   }//if(( TRUE == oFiMSDataObj.bIsValid()..
   else
   {
      ETG_TRACE_ERR(("vOnMSApplySettings: Invalid View Handle or Fi Msg is Invalid or PoMsg is NULL"));
   }

}

/***************************************************************************
** FUNCTION:  tBool spi_tclVideoPolicy::bUpdateClients(tCU16 cu16FunID)
***************************************************************************/
tBool spi_tclVideoPolicy::bUpdateClients(tCU16 cu16FunID)
{
   tBool bRet = TRUE;
   ail_tenCommunicationError enCommError = eUpdateClients(cu16FunID);

   if(AIL_EN_N_NO_ERROR != enCommError)
   {
      ETG_TRACE_ERR(( "bUpdateClients failed: %d", enCommError));
      bRet = FALSE;
   }//if(AIL_EN_N_NO_ERROR != enCommError)

   return bRet;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclVideoPolicy::vUpdateVideoRenderingStatus()
***************************************************************************/
t_Void spi_tclVideoPolicy::vUpdateVideoRenderingStatus(t_Bool bRenderingStarted)
{
   ETG_TRACE_USR1(("vUpdateVideoRenderingStatus - %d",bRenderingStarted));

   //Set the View status type
   m_enViewStatus.enType = (TRUE==bRenderingStarted)? (static_cast<spi_tenViewStatus>(EN_VIEW_STATUS_ACTIVE)):
      (static_cast<spi_tenViewStatus>(EN_VIEW_STATUS_STOPPED));


   if(FALSE == bUpdateClients(MIDW_HMI_LAYER_SYNCFI_C_U16_VIEWSTATUS))
   {
      ETG_TRACE_ERR(("vUpdateVideoRenderingStatus:bUpdateClients failed"));
   }//if(FALSE == bUpdateClients(MIDW_HMI_LAYER_SYNCFI_C_U16_VIEWSTATUS))
}


///////////////////////////////////////////////////////////////////////////////
// <EOF>



