/*!
 *******************************************************************************
 * \file              spi_tclMPlayClientHandler.cpp
 * \brief             DiPO Client handler class
 *******************************************************************************
 \verbatim
 PROJECT:        G3G
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    DiPO Client handler class for Media player service
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 08.1.2014  |  Shihabudheen P M            | Initial Version
 02.04.2014 | Hari Priya E R               | Implemented Timer for triggering
                                             transfer of GPS Data
 15.04.2014 | Ramya Muthy                  | Implemented Location data transfer
                                             and removed timer implementation.
 13.06.2014 | Ramya Murthy                 | Implementation for MPlay FI extn 
                                             for location info.
 31.07.2014 | Ramya Murthy                 | Changes for dynamic registration of properties
 05.11.2014 | Ramya Murthy                 | Implementation for Application metadata.
 05.11.2014 | Ramya Murthy                 | Implementation of revised CarPlay media concept 
 23.04.2015 | Ramya Murthy                 | Changes to provide PASCD and PAGCD sentences
 06.05.2015 | Tejaswini HB                 | Lint Fix
 26.05.2015 | Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors
 14.12.2015 | SHITANSHU SHEKHAR(RBEI/ECP2) | Implemented interfaces as per new Media player FI:
                                           | vOnDiPONowPlayingStatus(), vOnDiPOPlayBackStatus()
                                           | vOnDiPOPlayBackShuffleModeStatus(),
                                           | vOnDiPOPlayBackRepeatModeStatus(),
                                           | enGetPhoneCallState(), enGetPhoneCallDirection(),
                                           | enGetCallStateUpdateService(),
                                           | enGetCallStateUpdateDisconnectReason(),
                                           | vOnDiPOCallStateStatus(), enGetRegistrationStatus()
                                           | vOnDiPOCommunicationsStatus()
 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"
//Include common fi interface
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

#define GENERICMSGS_S_IMPORT_INTERFACE_GENERIC
#include "generic_msgs_if.h"

#include <cstdlib>
#include <algorithm>
#include <utility>
#include "FIMsgDispatch.h"
#include "XFiObjHandler.h"
#include "spi_tclDiPOConnectionIntf.h"
using namespace std;
using namespace shl::msgHandler;

#define DIPO_MPLY_MAJORVERSION MPLAY_APPCONTROLFI_C_U16_SERVICE_MAJORVERSION
#include "spi_tclFactory.h"
#include "spi_tclAudio.h"
#include "spi_tclRespInterface.h"
#include "spi_tclDiPoConnection.h"
#include "spi_tclDataServiceTypes.h"
#include "spi_LoopbackTypes.h"
#include "spi_tclMPlayClientHandler.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DIPO
#include "trcGenProj/Header/spi_tclMPlayClientHandler.cpp.trc.h"
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
#define MPLAYFI_CHAR_SET_UTF8   (mplay_fi_tclString::FI_EN_UTF8)

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/*! 
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgDiPODeviceConnectionsStatus> spi_tDevConnectionStatus
 * \brief  Property status, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<mplay_appcontrolfi_tclMsgDiPODeviceConnectionsStatus>
         spi_tDevConnectionStatus;

/*! 
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOPlaytimeStatus> spi_tMsgPlayTimeStatus
 * \brief  Property status, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOPlaytimeStatus>
         spi_tMsgPlayTimeStatus;

/*!
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgDiPONowPlayingStatus> spi_tMsgNowPlayingStatus
 * \brief  Property status, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<mplay_appcontrolfi_tclMsgDiPONowPlayingStatus>
         spi_tMsgNowPlayingStatus;

/*!
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOPlaybackStatusStatus> spi_tMsgPlayBackStatus
 * \brief  Property status, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOPlaybackStatusStatus>
         spi_tMsgPlayBackStatus;

/*!
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOPlaybackShuffleModeStatus> spi_tMsgPlayBackShuffleModeStatus
 * \brief  Property status, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOPlaybackShuffleModeStatus>
         spi_tMsgPlayBackShuffleModeStatus;

/*!
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOPlaybackRepeatModeStatus> spi_tMsgPlayBackRepeatModeStatus
 * \brief  Property status, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOPlaybackRepeatModeStatus>
         spi_tMsgPlayBackRepeatModeStatus;

/*!
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOCallStateStatus> spi_tMsgCallStateStatus
 * \brief  Property status, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOCallStateStatus>
         spi_tMsgCallStateStatus;

/*!
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOCommunicationsStatus> spi_tMsgCommunicationsStatus
 * \brief  Property status, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOCommunicationsStatus>
         spi_tMsgCommunicationsStatus;

/*!
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgCloseMethodResult> spi_tMsgDipoActiveResult
 * \brief  Method result, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOActiveDeviceMethodResult>
         spi_tMsgDipoActiveResult;

/*! 
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgDiPORoleSwitchRequestMethodResult> spi_tMsgDipoRoleSwitchResult
 * \brief  Method result, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<
         mplay_appcontrolfi_tclMsgDiPORoleSwitchRequestMethodResult>
         spi_tMsgDipoRoleSwitchResult;

/*! 
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOGetMediaObjectAlbumArtInfoMethodResult> spi_tMsgDipoGetAlbArtInfoResult
 * \brief  Method result, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<
         mplay_appcontrolfi_tclMsgDiPOGetMediaObjectAlbumArtInfoMethodResult>
         spi_tMsgDipoGetAlbArtInfoResult;

/*! 
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOGetMediaObjectAlbumArtMethodResult> spi_tMsgDiPOGetAlbumArt
 * \brief  Method result, with auto extract & auto destroy feature.
 */
//typedef XFiObjHandler<
//         mplay_appcontrolfi_tclMsgDiPOGetMediaObjectAlbumArtMethodResult>
//         spi_tMsgDiPOGetAlbumArt;          //commented to remove Lint Warning

/*! 
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOTransferGPSDataMethodResult> spi_tMsgDipoTransGPSDataReult
 * \brief  Method result, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOTransferGPSDataMethodResult>
         spi_tMsgDipoTransGPSDataReult;

/*! 
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOTransferDRDataMethodResult> spi_tMsgDipoTransDRDataResult
 * \brief  Method result, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOTransferDRDataMethodResult>
         spi_tMsgDipoTransDRDataResult;

/*!
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOLocationInfoStatus> spi_tMsgDipoLocInfoStatus
 * \brief  Method result, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOLocationInfoStatus>
         spi_tMsgDipoLocInfoStatus;

/*!
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgDiPORequestAudioDeviceMethodResult> spi_tMsgDipoReqAudioDevResult
 * \brief  Method result, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<mplay_appcontrolfi_tclMsgDiPORequestAudioDeviceMethodResult>
         spi_tMsgDipoReqAudioDevResult;

/*!
 * \XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOReleaseAudioDeviceMethodResult> spi_tMsgDipoRelAudioDevResult
 * \brief  Method result, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<mplay_appcontrolfi_tclMsgDiPOReleaseAudioDeviceMethodResult>
         spi_tMsgDipoRelAudioDevResult;

//! Connection get
typedef mplay_appcontrolfi_tclMsgDiPODeviceConnectionsGet MplayConnectionsGet; 

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/
#ifdef VARIANT_S_FTR_ENABLE_GM
   #define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_SERVICEINFO
   #include "most_fi_if.h"
   static const t_U32 cou32SPI_SERVICE_ID = CCA_C_U16_SRV_FB_DEVICEPROJECTION;
#else
   static const t_U32 cou32SPI_SERVICE_ID = CCA_C_U16_SRV_SMARTPHONEINTEGRATION;
#endif

/******************************************************************************
 ** CCA MESSAGE MAP
******************************************************************************/
// Message map for the media player fi
BEGIN_MSG_MAP(spi_tclMPlayClientHandler, ahl_tclBaseWork)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPODEVICECONNECTIONS,
         AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnDiPODeviceConnectionsStatus)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPOPLAYTIME,
         AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnDiPOPlaytimeStatus)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPONOWPLAYING,
         AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnDiPONowPlayingStatus)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPOPLAYBACKSTATUS,
         AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnDiPOPlayBackStatus)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPOPLAYBACKSHUFFLEMODE,
         AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnDiPOPlayBackShuffleModeStatus)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPOPLAYBACKREPEATMODE,
         AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnDiPOPlayBackRepeatModeStatus)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPOCALLSTATE,
         AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnDiPOCallStateStatus)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPOCOMMUNICATIONS,
         AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnDiPOCommunicationsStatus)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPOACTIVEDEVICE,
         AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnDiPOActiveDevice)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPOROLESWITCHREQUEST,
         AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnDiPORoleSwitchRequest)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPOGETMEDIAOBJECTALBUMARTINFO,
         AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnDiPOGetMediaObjectAlbumArtInfo)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPOGETMEDIAOBJECTALBUMART,
         AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnDiPOGetMediaObjectAlbumArt)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPOTRANSFERGPSDATA,
         AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnDiPOTransferGPSData)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPOTRANSFERDRDATA,
         AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnDiPOTransferDRData)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPOLOCATIONINFO,
         AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusDiPOLocationInfo)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPOREQUESTAUDIODEVICE,
         AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnDiPOReqAudioDevice)

ON_MESSAGE_SVCDATA( MPLAY_APPCONTROLFI_C_U16_DIPORELEASEAUDIODEVICE,
         AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnDiPORelAudioDevice)

END_MSG_MAP()

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::spi_tclMPlayClientHandler()
 ***************************************************************************/
spi_tclMPlayClientHandler::spi_tclMPlayClientHandler(ahl_tclBaseOneThreadApp* poMainApp,
      spi_tclDiPoConnection* poConnIntf)
      : ahl_tclBaseOneThreadClientHandler(
         poMainApp, /* Application Pointer */
         CCA_C_U16_SRV_IPOD_APP_CONTROL, /* ID of used Service */
         MPLAY_APPCONTROLFI_C_U16_SERVICE_MAJORVERSION, /* MajorVersion of used Service */
         MPLAY_APPCONTROLFI_C_U16_SERVICE_MINORVERSION, /* MinorVersion of used Service */
         CCA_C_U16_APP_MEDIAPLAYER),
         m_poMainApp(poMainApp),
         m_poConnIntf(poConnIntf),
         m_bRoleSwitchTriggerStatus(false),
         m_u32DeviceTag(0),
         m_bIsRegistrationReq(false),
		 m_enDevConnReq(e8DEVCONNREQ_SELECT)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
** FUNCTION:  spi_tclMPlayClientHandler::spi_tclMPlayClientHandler(..)
***************************************************************************/
spi_tclMPlayClientHandler::spi_tclMPlayClientHandler(ahl_tclBaseOneThreadApp* poMainApp)
   : ahl_tclBaseOneThreadClientHandler(
      poMainApp,                                      /* Application Pointer */
      CCA_C_U16_SRV_IPOD_APP_CONTROL,                 /* ID of used Service */
      MPLAY_APPCONTROLFI_C_U16_SERVICE_MAJORVERSION,  /* MajorVersion of used Service */
      MPLAY_APPCONTROLFI_C_U16_SERVICE_MINORVERSION,  /* MinorVersion of used Service */
      CCA_C_U16_APP_MEDIAPLAYER),
     m_poMainApp(poMainApp),
     m_poConnIntf(NULL),
     m_bRoleSwitchTriggerStatus(false),
     m_u32DeviceTag(0),
     m_bIsRegistrationReq(false),
	 m_enDevConnReq(e8DEVCONNREQ_SELECT)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   //@Note: This constructor is not used. Only created since invoking getInstance()
   //method without parameters causes compilation error since ahl_tclBaseOneThreadClientHandler()
   //default ctor is not implemented.
   //To avoid this, getInstance(ahl_tclBaseOneThreadApp*) is called.
 
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::~spi_tclMPlayClientHandler()
 ***************************************************************************/
spi_tclMPlayClientHandler::~spi_tclMPlayClientHandler()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   m_poConnIntf               = NULL;
   m_poMainApp                = NULL;
   m_u32DeviceTag             = 0;
   m_bIsRegistrationReq       = NULL;
   m_bRoleSwitchTriggerStatus = NULL;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vOnServiceAvailable()
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnServiceAvailable()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (true == m_bIsRegistrationReq)
   {
      vRegisterForProperties();
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vOnServiceUnavailable()
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnServiceUnavailable()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (true == m_bIsRegistrationReq)
   {
      vUnregisterForProperties();
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vRegisterForProperties()
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vRegisterForProperties()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   m_bIsRegistrationReq = true;

   if (bIfServiceAvailable())
   {
      vAddAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPODEVICECONNECTIONS);
      vAddAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPOLOCATIONINFO);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vRegisterForMetadataProperties()
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vRegisterForMetadataProperties()
{
   ETG_TRACE_USR1(("spi_tclMPlayClientHandler::vRegisterForMetadataProperties entered \n"));
   vAddAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPOCALLSTATE);

#ifndef VARIANT_S_FTR_ENABLE_GM // For GM, HMI is registering directly to mediaplyaer
   vAddAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPOPLAYTIME);
   vAddAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPONOWPLAYING);
   vAddAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPOPLAYBACKSTATUS);
   vAddAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPOPLAYBACKSHUFFLEMODE);
   vAddAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPOPLAYBACKREPEATMODE);
   vAddAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPOCOMMUNICATIONS);
#endif
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vUnregisterForProperties()
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vUnregisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclMPlayClientHandler::vUnregisterForProperties entered \n"));

   if (bIfServiceAvailable())
   {
      vRemoveAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPODEVICECONNECTIONS);
      vRemoveAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPOLOCATIONINFO);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vUnregisterForMetadataProperties()
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vUnregisterForMetadataProperties()
{
   ETG_TRACE_USR1(("spi_tclMPlayClientHandler::vUnregisterForMetadataProperties entered \n"));
   vRemoveAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPOCALLSTATE);

#ifndef VARIANT_S_FTR_ENABLE_GM
   vRemoveAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPOPLAYTIME);
   vRemoveAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPONOWPLAYING);
   vRemoveAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPOPLAYBACKSTATUS);
   vRemoveAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPOPLAYBACKSHUFFLEMODE);
   vRemoveAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPOPLAYBACKREPEATMODE);
   vRemoveAutoRegisterForProperty( MPLAY_APPCONTROLFI_C_U16_DIPOCOMMUNICATIONS);
#endif
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vOnDiPODeviceConnectionsStatus(..
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnDiPODeviceConnectionsStatus(
         amt_tclServiceData* poMessage/*mplay_appcontrolfi_tclMsgDiPODeviceConnectionsStatus &oDevConnectionStatus*/)
{

	/*lint -esym(40,nullptr)nullptr Undeclared identifier */
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   trMsgContext oMsgContext;
   t_String szVersion;

   spi_tDevConnectionStatus oDevConnectionStatus(*poMessage,
            DIPO_MPLY_MAJORVERSION);
   // CPY_TO_USRCNTXT(poMessage, oMsgContext.rUserContext);
   // clear all the elements in the vector
   m_rDiPODeviceList.clear();

   if (oDevConnectionStatus.bIsValid())
   {
      ETG_TRACE_USR2(("Number of Devices : %d", oDevConnectionStatus.oDeviceInfo.oItems.size()));

      for (size_t u8Index = 0; u8Index
               < oDevConnectionStatus.oDeviceInfo.oItems.size(); u8Index++)
      {
         ETG_TRACE_USR2(("Device Index = %d  DeviceID : %d", u8Index, oDevConnectionStatus.oDeviceInfo.oItems[u8Index].u8DeviceTag));
         ETG_TRACE_USR2(("DeviceName : %s", oDevConnectionStatus.oDeviceInfo.oItems[u8Index].sDeviceName.szGet(mplay_fi_tclString::FI_EN_UTF8)));
         ETG_TRACE_USR2(("DeviceType : %d", oDevConnectionStatus.oDeviceInfo.oItems[u8Index].e8DeviceType.enType));
         ETG_TRACE_USR2(("DiPO Capable : %d", oDevConnectionStatus.oDeviceInfo.oItems[u8Index].bDiPOCapable));
         ETG_TRACE_USR2(("Connected Flag : %d", oDevConnectionStatus.oDeviceInfo.oItems[u8Index].bDeviceConnected));
         ETG_TRACE_USR2(("Connection type : %d", oDevConnectionStatus.oDeviceInfo.oItems[u8Index].e8ConnectionType.enType));
         ETG_TRACE_USR2(("Device state : %d", oDevConnectionStatus.oDeviceInfo.oItems[u8Index].e8DeviceState.enType));
		 ETG_TRACE_USR2(("USB Port type : %d", oDevConnectionStatus.oDeviceInfo.oItems[u8Index].e8DiPOCaps.enType));
         //! Treat the device as connected if the device is USB connected and
         //! if the connected flag is set or carplay mode is active
         t_Bool bIsDeviceConnected =
                  (mplay_fi_tcl_e8_DiPOConnectionType::FI_EN_E8DCT_USB == oDevConnectionStatus.oDeviceInfo.oItems[u8Index].e8ConnectionType.enType) &&
                  ((true == oDevConnectionStatus.oDeviceInfo.oItems[u8Index].bDeviceConnected)
                  ||(true == oDevConnectionStatus.oDeviceInfo.oItems[u8Index].bDiPOCapable));

         //! indicates device connection status when role switch is in progress but authentication is pending.
         tenDeviceConnectionStatus enDeviceState = (mplay_fi_tcl_e8_DiPODeviceStatus::FI_EN_E8DS_NONE ==
                  oDevConnectionStatus.oDeviceInfo.oItems[u8Index].e8DeviceState.enType)?
                           e8DEV_NOT_CONNECTED:e8DEV_CONNECTED;

         // Create a temporary device object
         trDeviceInfo rDeviceInfo;
         if ((mplay_fi_tcl_e8_DiPODeviceType::FI_EN_E8DTY_IPHONE
                  == oDevConnectionStatus.oDeviceInfo.oItems[u8Index].e8DeviceType.enType)
                  && (true == bIsDeviceConnected))
         {
            // Device Connection
            rDeviceInfo.u32DeviceHandle = oDevConnectionStatus.oDeviceInfo.oItems[u8Index].u8DeviceTag;

            GET_STRINGDATA_FROM_FI_STRINGOBJ(oDevConnectionStatus.oDeviceInfo.oItems[u8Index].sDeviceName,
                  MPLAYFI_CHAR_SET_UTF8, rDeviceInfo.szDeviceName);

            rDeviceInfo.enDeviceCategory = e8DEV_TYPE_DIPO;

            rDeviceInfo.rProjectionCapability.enDeviceType = e8_APPLE_DEVICE;
            rDeviceInfo.rProjectionCapability.enCarplaySupport = e8SPI_SUPPORTED;
            rDeviceInfo.rProjectionCapability.enAndroidAutoSupport = e8SPI_NOTSUPPORTED;
            rDeviceInfo.rProjectionCapability.enMirrorlinkSupport = e8SPI_NOTSUPPORTED;

            rDeviceInfo.enDeviceConnectionType = e8USB_CONNECTED;

            // check for DiPO active status of a device
            rDeviceInfo.enSessionStatus
                     = oDevConnectionStatus.oDeviceInfo.oItems[u8Index] .bDiPOCapable
                                                                                     ? e8_SESSION_ACTIVE
                                                                                     : e8_SESSION_INACTIVE;
            //! Extract information for device is device connected to OTG port
            rDeviceInfo.rProjectionCapability.enUSBPortType
            		= (oDevConnectionStatus.oDeviceInfo.oItems[u8Index].e8DiPOCaps.enType != mplay_fi_tcl_e8_DiPOCaps::FI_EN_E8DIPO_CAP_NONE) ?
            						e8_PORT_TYPE_OTG :e8_PORT_TYPE_NON_OTG;

            ETG_TRACE_USR2(("USB Port Type : %d \n", ETG_ENUM(USB_PORT_TYPE,rDeviceInfo.rProjectionCapability.enUSBPortType)));
			
			rDeviceInfo.rProjectionCapability.enCarplaySupport 
			        = (oDevConnectionStatus.oDeviceInfo.oItems[u8Index].e8DiPOCaps.enType == 
					        (mplay_fi_tcl_e8_DiPOCaps::FI_EN_E8DIPO_CAP_CARPLAY_FEASIBLE || mplay_fi_tcl_e8_DiPOCaps::FI_EN_E8DIPO_CAP_CARPLAY || 
							    mplay_fi_tcl_e8_DiPOCaps::FI_EN_E8DIPO_CAP_CARPLAY_NATIVE_TRANSPORT)) ?
					                    e8SPI_SUPPORTED : e8SPI_SUPPORT_UNKNOWN;

            GET_STRINGDATA_FROM_FI_STRINGOBJ(oDevConnectionStatus.oDeviceInfo.oItems[u8Index].sDiPOVersion,
                 MPLAYFI_CHAR_SET_UTF8, szVersion);
            //   vPopulateDipoVersion(szVersion, rDeviceInfo.rVersionInfo.u32MajorVersion,
            //      rDeviceInfo.rVersionInfo.u32MinorVersion, rDeviceInfo.rVersionInfo.u32PatchVersion);

            rDeviceInfo.rVersionInfo.u32MajorVersion = 0;
            rDeviceInfo.rVersionInfo.u32MinorVersion = 0;
            rDeviceInfo.rVersionInfo.u32PatchVersion = 0;
            // Check for device connected status
            rDeviceInfo.enDeviceConnectionStatus
                     = (true == bIsDeviceConnected) ? e8DEV_CONNECTED : e8DEV_NOT_CONNECTED;
            if (e8DEV_CONNECTED == rDeviceInfo.enDeviceConnectionStatus)
            {
               // Push back the connected device to the list
               m_rDiPODeviceList.push_back(rDeviceInfo);
            }// if(e8DEV_CONNECTED == rDeviceInfo.enDeviceConnectionStatus)

            if (nullptr != m_poConnIntf)
            {
               m_poConnIntf->vOnDiPODeviceConnect(rDeviceInfo);
            }// if(NULL != m_poConnIntf)


         }
		 // m_bRoleSwitchTriggerStatus indicate whether role switch operations are in progress or not. 
         // It is checking to update the device disconnection, to avoid the unnecessary update
		 // In between the role switch is in progress.
         //! enDeviceState checks if the device is disconnected during role switch
         else if ((mplay_fi_tcl_e8_DiPODeviceType::FI_EN_E8DTY_IPHONE
                  == oDevConnectionStatus.oDeviceInfo.oItems[u8Index].e8DeviceType.enType)
                  && ((false == m_bRoleSwitchTriggerStatus) && (e8DEV_CONNECTED != enDeviceState)))
         {
            // Device disconnection
            if (nullptr != m_poConnIntf)
            {
               m_poConnIntf->vOnDiPODeviceDisConnect(
                        oDevConnectionStatus.oDeviceInfo.oItems[u8Index].u8DeviceTag);

            }// if(NULL != m_poConnIntf)
         }
      }// for loop end
   }
   else //if (oDevConnectionStatus.bIsValid())
   {
      ETG_TRACE_ERR(("Message extraction failed."));
   } //if (oDevConnectionStatus.bIsValid()) 

   oDevConnectionStatus.vDestroy(); 
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vOnDiPOPlaytimeStatus(amt_tclServiceData)
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnDiPOPlaytimeStatus(
         amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1((" spi_tclMPlayClientHandler::vOnDiPOPlaytimeStatus entered \n"));

   spi_tMsgPlayTimeStatus oMsgPlayTimeStatus(*poMessage, DIPO_MPLY_MAJORVERSION);

   if(oMsgPlayTimeStatus.bIsValid())
   {
	  //Store the media Play time  info and send it to service.
     m_rAppMediaPlaytime.u32ElapsedPlayTime = oMsgPlayTimeStatus.u32ElapsedPlaytime;
     m_rAppMediaPlaytime.u32TotalPlayTime = oMsgPlayTimeStatus.u32TotalPlaytime;

      ETG_TRACE_USR4(("[PARAM]: spi_tclMPlayClientHandler::vOnDiPOPlaytimeStatus "
   		   "ElapsedTime = %d, Total Play Time = %d ",
   		   m_rAppMediaPlaytime.u32ElapsedPlayTime, m_rAppMediaPlaytime.u32TotalPlayTime));

      if (NULL != m_poConnIntf)
      {
         m_poConnIntf->vPostApplicationMediaPlaytime(m_rAppMediaPlaytime, corEmptyUsrContext);
      }//if (NULL != m_poConnIntf)
   }
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclMPlayClientHandler::vOnDiPOPlaytimeStatus : Message extraction failed. \n"));
   }

   oMsgPlayTimeStatus.vDestroy();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPONowPlayingStatus()
***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnDiPONowPlayingStatus(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1((" spi_tclMPlayClientHandler::vOnDiPONowPlayingStatus entered \n"));

   spi_tMsgNowPlayingStatus oMsgNowPlayingStatus(*poMessage, DIPO_MPLY_MAJORVERSION);

   if(oMsgNowPlayingStatus.bIsValid())
   {
      //Store the media meta data info and send it to service.
      m_rAppMediaMetaData.bMediaMetadataValid = true;

      GET_STRINGDATA_FROM_FI_STRINGOBJ(oMsgNowPlayingStatus.sAppName,
            MPLAYFI_CHAR_SET_UTF8, m_rAppMediaMetaData.szAppName);
      GET_STRINGDATA_FROM_FI_STRINGOBJ(oMsgNowPlayingStatus.sTitle,
            MPLAYFI_CHAR_SET_UTF8, m_rAppMediaMetaData.szTitle);
      GET_STRINGDATA_FROM_FI_STRINGOBJ(oMsgNowPlayingStatus.sArtist,
            MPLAYFI_CHAR_SET_UTF8, m_rAppMediaMetaData.szArtist);
      GET_STRINGDATA_FROM_FI_STRINGOBJ(oMsgNowPlayingStatus.sAlbum,
            MPLAYFI_CHAR_SET_UTF8, m_rAppMediaMetaData.szAlbum);
      GET_STRINGDATA_FROM_FI_STRINGOBJ(oMsgNowPlayingStatus.sGenre,
            MPLAYFI_CHAR_SET_UTF8, m_rAppMediaMetaData.szGenre);
      GET_STRINGDATA_FROM_FI_STRINGOBJ(oMsgNowPlayingStatus.sComposer,
            MPLAYFI_CHAR_SET_UTF8, m_rAppMediaMetaData.szComposerName);
      GET_STRINGDATA_FROM_FI_STRINGOBJ(oMsgNowPlayingStatus.sAlbumArt,
            MPLAYFI_CHAR_SET_UTF8, m_rAppMediaMetaData.szImageUrl);

      // Track number from media player
      m_rAppMediaMetaData.u32TrackNumber = static_cast<t_U32>(oMsgNowPlayingStatus.u16AlbumTrackNumber);
      m_rAppMediaMetaData.u32AlbumTrackCount = static_cast<t_U32>(oMsgNowPlayingStatus.u16AlbumTrackCount);

      // Disc Number from media player
      m_rAppMediaMetaData.u32AlbumDiscNumber = static_cast<t_U32>(oMsgNowPlayingStatus.u16AlbumDiscNumber);
      m_rAppMediaMetaData.u32AlbumDiscCount = static_cast<t_U32>(oMsgNowPlayingStatus.u16AlbumDiscCount);

      // Chapter count from media player
      m_rAppMediaMetaData.u32ChapterCount = static_cast<t_U32>(oMsgNowPlayingStatus.u16ChapterCount);

      // Now playing info according iAP2 Spec
      m_rAppMediaMetaData.bITunesRadioAd = oMsgNowPlayingStatus.biTunesRadioAd;
      GET_STRINGDATA_FROM_FI_STRINGOBJ(oMsgNowPlayingStatus.siTunesRadioStationName,
            MPLAYFI_CHAR_SET_UTF8, m_rAppMediaMetaData.szITunesRadioStationName);

      ETG_TRACE_USR4(("[PARAM]:spi_tclMPlayClientHandler::vOnDiPONowPlayingStatus entered: bMediaMetadataValid = %u ",
            ETG_ENUM(BOOL, m_rAppMediaMetaData.bMediaMetadataValid)));
      ETG_TRACE_USR4(("[PARAM]: vOnDiPONowPlayingStatus: Artist = %s ",
    		  m_rAppMediaMetaData.szArtist.c_str()));

      ETG_TRACE_USR4(("[PARAM]: vOnDiPONowPlayingStatus: Title = %s ",
    		  m_rAppMediaMetaData.szTitle.c_str()));

      ETG_TRACE_USR4(("[PARAM]: vOnDiPONowPlayingStatus: Album = %s ",
    		  m_rAppMediaMetaData.szAlbum.c_str()));

      ETG_TRACE_USR4(("[PARAM]: vOnDiPONowPlayingStatus: Genre = %s ",
    		  m_rAppMediaMetaData.szGenre.c_str()));

      ETG_TRACE_USR4(("[PARAM]: vOnDiPONowPlayingStatus: AppName = %s ",
    		  m_rAppMediaMetaData.szAppName.c_str()));

      ETG_TRACE_USR4(("[PARAM]: vOnDiPONowPlayingStatus: ImageUrl = %s ",
    		  m_rAppMediaMetaData.szImageUrl.c_str()));

      ETG_TRACE_USR4(("[PARAM]: vOnDiPONowPlayingStatus: Composer = %s ",
    		  m_rAppMediaMetaData.szComposerName.c_str()));

      ETG_TRACE_USR4(("[PARAM]: vOnDiPONowPlayingStatus: Track Number = %d, Album Track Count = %d ",
            m_rAppMediaMetaData.u32TrackNumber, m_rAppMediaMetaData.u32AlbumTrackCount));

      ETG_TRACE_USR4(("[PARAM]: vOnDiPONowPlayingStatus: Album Disc Number = %d, Album Disc Count = %d ",
            m_rAppMediaMetaData.u32AlbumDiscNumber, m_rAppMediaMetaData.u32AlbumDiscCount));

      ETG_TRACE_USR4(("[PARAM]: vOnDiPONowPlayingStatus: Chapter Count = %d ",
            m_rAppMediaMetaData.u32ChapterCount));

      ETG_TRACE_USR4(("[PARAM]: vOnDiPONowPlayingStatus: ITune Radio Ad status = %d ",
            ETG_ENUM(BOOL,m_rAppMediaMetaData.bITunesRadioAd)));

      ETG_TRACE_USR4(("[PARAM]: vOnDiPONowPlayingStatus: ITune Radio Name = %s ",
            m_rAppMediaMetaData.szITunesRadioStationName.c_str()));

      if(!((m_rAppMediaMetaData.szImageUrl).empty()))
      {
         if(false == bDiPOGetAlbumArtInfo(m_rAppMediaMetaData.szImageUrl))
         {
    	     ETG_TRACE_ERR(("[ERR]: vOnDiPONowPlayingStatus: Album Art Info Request Failed"));
         }
      }

      // Posting meta data
      if (NULL != m_poConnIntf)
      {
         m_poConnIntf->vPostApplicationMediaMetaData(m_rAppMediaMetaData, corEmptyUsrContext);
      }//if (NULL != m_poConnIntf)

   }
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclMPlayClientHandler::vOnDiPONowPlayingStatus : Message extraction failed. \n"));
   }

   oMsgNowPlayingStatus.vDestroy();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPOPlayBackStatus()
***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnDiPOPlayBackStatus(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1((" spi_tclMPlayClientHandler::vOnDiPOPlayBackStatus entered "));

   spi_tMsgPlayBackStatus oMsgPlayBackStatus(*poMessage, DIPO_MPLY_MAJORVERSION);

   if (oMsgPlayBackStatus.bIsValid())
   {
      //Retrieve the Play back state
      switch(oMsgPlayBackStatus.e8State.enType)
      {
      case mplay_fi_tcl_e8_DiPOPlaybackStatus::FI_EN_E8DIPO_PBS_STOPPED :
         m_rAppMediaMetaData.enMediaPlayBackState = e8PLAYBACK_STOPPED;
         break;
      case mplay_fi_tcl_e8_DiPOPlaybackStatus::FI_EN_E8DIPO_PBS_PLAYING :
         m_rAppMediaMetaData.enMediaPlayBackState = e8PLAYBACK_PLAYING;
         break;
      case mplay_fi_tcl_e8_DiPOPlaybackStatus::FI_EN_E8DIPO_PBS_PAUSED  :
         m_rAppMediaMetaData.enMediaPlayBackState = e8PLAYBACK_PAUSED;
         break;
      case mplay_fi_tcl_e8_DiPOPlaybackStatus::FI_EN_E8DIPO_PBS_SEEKFORWARD :
         m_rAppMediaMetaData.enMediaPlayBackState = e8PLAYBACK_SEEKFORWARD;
         break;
      case mplay_fi_tcl_e8_DiPOPlaybackStatus::FI_EN_E8DIPO_PBS_SEEKBACKWARD :
         m_rAppMediaMetaData.enMediaPlayBackState = e8PLAYBACK_SEEKBACKWARD;
         break;
      default :
         m_rAppMediaMetaData.enMediaPlayBackState = e8PLAYBACK_NOT_KNOWN;
         break;
      }

      ETG_TRACE_USR4(("[PARAM]: spi_tclMPlayClientHandler::vOnDiPOPlayBackStatus Play Back State = %d",
            ETG_ENUM(PLAYBACK_STATE,m_rAppMediaMetaData.enMediaPlayBackState)));

      // Posting meta data
      if (NULL != m_poConnIntf)
      {
         m_poConnIntf->vPostApplicationMediaMetaData(m_rAppMediaMetaData, corEmptyUsrContext);
      }//if (NULL != m_poConnIntf)
   }
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclMPlayClientHandler::vOnDiPOPlayBackStatus : Message extraction failed."));
   }
   oMsgPlayBackStatus.vDestroy();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPOPlayBackShuffleModeStatus()
***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnDiPOPlayBackShuffleModeStatus(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1((" spi_tclMPlayClientHandler::vOnDiPOPlayBackShuffleModeStatus entered "));

   spi_tMsgPlayBackShuffleModeStatus oMsgPlayBackShuffleStatus(*poMessage, DIPO_MPLY_MAJORVERSION);

   if (oMsgPlayBackShuffleStatus.bIsValid())
   {
      //Retrieve the Shuffle mode
      switch(oMsgPlayBackShuffleStatus.e8Mode.enType)
      {
      case mplay_fi_tcl_e8_DiPOPlaybackShuffleMode::FI_EN_E8DIPO_PBSM_OFF :
         m_rAppMediaMetaData.enMediaPlayBackShuffleState = e8SHUFFLE_OFF;
         break;
      case mplay_fi_tcl_e8_DiPOPlaybackShuffleMode::FI_EN_E8DIPO_PBSM_SONGS :
         m_rAppMediaMetaData.enMediaPlayBackShuffleState = e8SHUFFLE_SONGS;
         break;
      case mplay_fi_tcl_e8_DiPOPlaybackShuffleMode::FI_EN_E8DIPO_PBSM_ALBUMS :
         m_rAppMediaMetaData.enMediaPlayBackShuffleState = e8SHUFFLE_ALBUMS;
         break;
      default :
         m_rAppMediaMetaData.enMediaPlayBackShuffleState = e8SHUFFLE_NOT_KNOWN;
         break;
      }

      ETG_TRACE_USR4(("[PARAM]: spi_tclMPlayClientHandler::vOnDiPOPlayBackShuffleModeStatus Shuffle State = %d",
            ETG_ENUM(SHUFFLE_MODE, m_rAppMediaMetaData.enMediaPlayBackShuffleState)));

      // Posting meta data
      if (NULL != m_poConnIntf)
      {
         m_poConnIntf->vPostApplicationMediaMetaData(m_rAppMediaMetaData, corEmptyUsrContext);
      }//if (NULL != m_poConnIntf)
   }
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclMPlayClientHandler::vOnDiPOPlayBackShuffleModeStatus : Message extraction failed."));
   }
   oMsgPlayBackShuffleStatus.vDestroy();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPOPlayBackRepeatModeStatus()
***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnDiPOPlayBackRepeatModeStatus(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1((" spi_tclMPlayClientHandler::vOnDiPOPlayBackRepeatModeStatus entered "));

   spi_tMsgPlayBackRepeatModeStatus oMsgPlayBackRepeatStatus(*poMessage, DIPO_MPLY_MAJORVERSION);

   if (oMsgPlayBackRepeatStatus.bIsValid())
   {
      // Retrieve Repeat Mode
      switch(oMsgPlayBackRepeatStatus.e8Mode.enType)
      {
      case mplay_fi_tcl_e8_DiPOPlaybackRepeatMode::FI_EN_E8DIPO_PBRM_OFF :
         m_rAppMediaMetaData.enMediaPlayBackRepeatState = e8REPEAT_OFF;
         break;
      case mplay_fi_tcl_e8_DiPOPlaybackRepeatMode::FI_EN_E8DIPO_PBRM_ONE :
         m_rAppMediaMetaData.enMediaPlayBackRepeatState = e8REPEAT_ONE;
         break;
      case mplay_fi_tcl_e8_DiPOPlaybackRepeatMode::FI_EN_E8DIPO_PBRM_ALL :
         m_rAppMediaMetaData.enMediaPlayBackRepeatState = e8REPEAT_ALL;
         break;
      default :
         m_rAppMediaMetaData.enMediaPlayBackRepeatState = e8REPEAT_NOT_KNOWN;
         break;
      }

      ETG_TRACE_USR4(("[PARAM]: spi_tclMPlayClientHandler::vOnDiPOPlayBackRepeatModeStatus Repeat Mode = %d",
            ETG_ENUM(REPEAT_MODE, m_rAppMediaMetaData.enMediaPlayBackRepeatState)));

      // Posting meta data
      if (NULL != m_poConnIntf)
      {
         m_poConnIntf->vPostApplicationMediaMetaData(m_rAppMediaMetaData, corEmptyUsrContext);
      }//if (NULL != m_poConnIntf)
   }
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclMPlayClientHandler::vOnDiPOPlayBackRepeatModeStatus : Message extraction failed."));
   }
   oMsgPlayBackRepeatStatus.vDestroy();
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::enGetPhoneCallState()
 ***************************************************************************/
tenPhoneCallState spi_tclMPlayClientHandler::enGetPhoneCallState(
      mplay_fi_tcl_e8_DiPOCallStateUpdateStatus &rfrenPhoneCallState)
{
   ETG_TRACE_USR1(("spi_tclMPlayClientHandler::enGetPhoneCallState entered "));

   tenPhoneCallState enPhoneCallState = e8PHONE_CALL_STATE_NOT_KNOWN;
   switch(rfrenPhoneCallState.enType)
   {
   case mplay_fi_tcl_e8_DiPOCallStateUpdateStatus::FI_EN_E8DIPO_CSUS_DISCONNECTED :
      enPhoneCallState = e8CALL_DISCONNECTED;
      break;
   case mplay_fi_tcl_e8_DiPOCallStateUpdateStatus::FI_EN_E8DIPO_CSUS_SENDING :
      enPhoneCallState = e8CALL_SENDING;
      break;
   case mplay_fi_tcl_e8_DiPOCallStateUpdateStatus::FI_EN_E8DIPO_CSUS_RINGING :
      enPhoneCallState = e8CALL_RINGING;
      break;
   case mplay_fi_tcl_e8_DiPOCallStateUpdateStatus::FI_EN_E8DIPO_CSUS_CONNECTING :
      enPhoneCallState = e8CALL_CONNECTING;
      break;
   case mplay_fi_tcl_e8_DiPOCallStateUpdateStatus::FI_EN_E8DIPO_CSUS_ACTIVE :
      enPhoneCallState = e8CALL_ACTIVE;
      break;
   case mplay_fi_tcl_e8_DiPOCallStateUpdateStatus::FI_EN_E8DIPO_CSUS_HELD :
      enPhoneCallState = e8CALL_HELD;
      break;
   case mplay_fi_tcl_e8_DiPOCallStateUpdateStatus::FI_EN_E8DIPO_CSUS_DISCONNECTING :
      enPhoneCallState = e8CALL_DISCONNECTING;
      break;
   default :
      enPhoneCallState = e8PHONE_CALL_STATE_NOT_KNOWN;
   }
   return enPhoneCallState;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::enGetPhoneCallDirection()
 ***************************************************************************/
tenPhoneCallDirection spi_tclMPlayClientHandler::enGetPhoneCallDirection(
      mplay_fi_tcl_e8_DiPOCallStateUpdateDirection &rfenPhoneCallDirection)
{
   ETG_TRACE_USR1(("spi_tclMPlayClientHandler::enGetPhoneCallDirection entered "));

   tenPhoneCallDirection enPhoneCallDirection = e8PHONE_CALL_DIRECTION_NOT_KNOWN;
   switch(rfenPhoneCallDirection.enType)
   {
   case mplay_fi_tcl_e8_DiPOCallStateUpdateDirection::FI_EN_E8DIPO_CSUD_INCOMING :
      enPhoneCallDirection = e8CALL_INCOMING;
      break;
   case mplay_fi_tcl_e8_DiPOCallStateUpdateDirection::FI_EN_E8DIPO_CSUD_OUTGOING :
      enPhoneCallDirection = e8CALL_OUTGOING;
      break;
   default :
      enPhoneCallDirection = e8PHONE_CALL_DIRECTION_NOT_KNOWN;
   }

   return enPhoneCallDirection;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::enGetCallStateUpdateService()
 ***************************************************************************/
tenCallStateUpdateService spi_tclMPlayClientHandler::enGetCallStateUpdateService(
      mplay_fi_tcl_e8_DiPOCallStateUpdateService &rfenCallStateUpdateService)
{
   ETG_TRACE_USR1(("spi_tclMPlayClientHandler::enGetCallStateUpdateService entered "));

   tenCallStateUpdateService enCallStateUpdateService = e8CALLSTATE_SERVICE_UNKNOWN;
   switch(rfenCallStateUpdateService.enType)
   {
   case mplay_fi_tcl_e8_DiPOCallStateUpdateService::FI_EN_E8DIPO_CSEV_TELEPHONY :
      enCallStateUpdateService = e8CALLSTATE_SERVICE_TELEPHONY;
      break;
   case mplay_fi_tcl_e8_DiPOCallStateUpdateService::FI_EN_E8DIPO_CSEV_FACETIMEAUDIO :
      enCallStateUpdateService = e8CALLSTATE_SERVICE_FACETIMEAUDIO;
      break;
   case mplay_fi_tcl_e8_DiPOCallStateUpdateService::FI_EN_E8DIPO_CSEV_FACETIMEVIDEO :
      enCallStateUpdateService = e8CALLSTATE_SERVICE_FACETIMEVIDEO;
      break;
   default :
      enCallStateUpdateService = e8CALLSTATE_SERVICE_UNKNOWN;
   }
   return enCallStateUpdateService;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::enGetCallStateUpdateDisconnectReason()
 ***************************************************************************/
tenCallStateUpdateDisconnectReason spi_tclMPlayClientHandler::enGetCallStateUpdateDisconnectReason(
      mplay_fi_tcl_e8_DiPOCallStateUpdateDisconnectReason &rfenCallStateUpdateDisconnectReason)
{
   ETG_TRACE_USR1(("spi_tclMPlayClientHandler::enGetCallStateUpdateDisconnectReason entered "));

   tenCallStateUpdateDisconnectReason enCallStateUpdateDisconnectReason = e8CALLSTATE_DISCONNECT_REASON_UNKNOWN;
   switch(rfenCallStateUpdateDisconnectReason.enType)
   {
   case mplay_fi_tcl_e8_DiPOCallStateUpdateDisconnectReason::FI_EN_E8DIPO_CSDR_ENDED :
      enCallStateUpdateDisconnectReason = e8CALLSTATE_DISCONNECT_REASON_ENDED;
      break;
   case mplay_fi_tcl_e8_DiPOCallStateUpdateDisconnectReason::FI_EN_E8DIPO_CSDR_DECLINED :
      enCallStateUpdateDisconnectReason = e8CALLSTATE_DISCONNECT_REASON_DECLINED;
      break;
   case mplay_fi_tcl_e8_DiPOCallStateUpdateDisconnectReason::FI_EN_E8DIPO_CSDR_FAILED :
      enCallStateUpdateDisconnectReason = e8CALLSTATE_DISCONNECT_REASON_FAILED;
      break;
   default :
      enCallStateUpdateDisconnectReason = e8CALLSTATE_DISCONNECT_REASON_UNKNOWN;
   }
   return enCallStateUpdateDisconnectReason;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vOnDiPOCallStateStatus(amt_tclServiceData)
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnDiPOCallStateStatus(
         amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclMPlayClientHandler::vOnDiPOCallStateStatus entered "));

   spi_tMsgCallStateStatus oMsgCallStateData(*poMessage, DIPO_MPLY_MAJORVERSION);

   if (oMsgCallStateData.bIsValid())
   {
      // clear the member variable vector list
      m_rAppPhoneData.tvecPhoneCallMetaDataList.clear();
      m_rAppPhoneData.bPhoneMetadataValid = true;

      for (tU16 u16Index = 0 ; u16Index < oMsgCallStateData.oCallStateUpdate.oItems.size() ; ++u16Index)
      {
         //Store the phone meta data info and send it to service.
         trPhoneCallMetaData rPhoneCallMetaDataList;

         GET_STRINGDATA_FROM_FI_STRINGOBJ(oMsgCallStateData.oCallStateUpdate.oItems[u16Index].sRemoteID,
               MPLAYFI_CHAR_SET_UTF8, rPhoneCallMetaDataList.szPhoneNumber);
         GET_STRINGDATA_FROM_FI_STRINGOBJ(oMsgCallStateData.oCallStateUpdate.oItems[u16Index].sDisplayName,
               MPLAYFI_CHAR_SET_UTF8, rPhoneCallMetaDataList.szDisplayName);

         rPhoneCallMetaDataList.enPhoneCallState = enGetPhoneCallState(oMsgCallStateData.oCallStateUpdate.oItems[u16Index].eStatus);
         rPhoneCallMetaDataList.enPhoneCallDirection = enGetPhoneCallDirection(oMsgCallStateData.oCallStateUpdate.oItems[u16Index].eDirection);

         GET_STRINGDATA_FROM_FI_STRINGOBJ(oMsgCallStateData.oCallStateUpdate.oItems[u16Index].sCallUUID,
               MPLAYFI_CHAR_SET_UTF8, rPhoneCallMetaDataList.szCallUUID);
         GET_STRINGDATA_FROM_FI_STRINGOBJ(oMsgCallStateData.oCallStateUpdate.oItems[u16Index].sLabel,
               MPLAYFI_CHAR_SET_UTF8, rPhoneCallMetaDataList.szCallerLabel);

         rPhoneCallMetaDataList.enCallStateUpdateService = enGetCallStateUpdateService(oMsgCallStateData.oCallStateUpdate.oItems[u16Index].eService);
         rPhoneCallMetaDataList.bConferencedCall = oMsgCallStateData.oCallStateUpdate.oItems[u16Index].bIsConferenced;
         rPhoneCallMetaDataList.u8ConferenceGroup = oMsgCallStateData.oCallStateUpdate.oItems[u16Index].u8ConferenceGroup;
         rPhoneCallMetaDataList.enCallStateUpdateDisconnectReason = enGetCallStateUpdateDisconnectReason(oMsgCallStateData.oCallStateUpdate.oItems[u16Index].eDisconnectReason);

         ETG_TRACE_USR4(("[PARAM]:vOnDiPOCallStateStatus : Phone number = %s ", rPhoneCallMetaDataList.szPhoneNumber.c_str()));
         ETG_TRACE_USR4(("[PARAM]:vOnDiPOCallStateStatus : Display name = %s ", rPhoneCallMetaDataList.szDisplayName.c_str()));

         ETG_TRACE_USR4(("[PARAM]:vOnDiPOCallStateStatus : Phone call state = %d, Phone call direction = %d ",
               ETG_ENUM(PHONE_CALL_STATE, rPhoneCallMetaDataList.enPhoneCallState),
               ETG_ENUM(PHONE_CALL_DIRECTION, rPhoneCallMetaDataList.enPhoneCallDirection)));

         ETG_TRACE_USR4(("[PARAM]:vOnDiPOCallStateStatus : Call UUID = %s ", rPhoneCallMetaDataList.szCallUUID.c_str()));
         ETG_TRACE_USR4(("[PARAM]:vOnDiPOCallStateStatus : Caller Label = %s ", rPhoneCallMetaDataList.szCallerLabel.c_str()));

         ETG_TRACE_USR4(("[PARAM]:vOnDiPOCallStateStatus : Call service of current device = %d, Is this call part of conference = %d ",
               ETG_ENUM(CALLSTATE_SERVICE, rPhoneCallMetaDataList.enCallStateUpdateService),
               ETG_ENUM(BOOL, rPhoneCallMetaDataList.bConferencedCall)));

         ETG_TRACE_USR4(("[PARAM]:vOnDiPOCallStateStatus : Conference group number = %d ",
               rPhoneCallMetaDataList.u8ConferenceGroup));

         ETG_TRACE_USR4(("[PARAM]:vOnDiPOCallStateStatus : Call disconnect reason = %d ",
               ETG_ENUM(CALLDISCONNECT_REASON, rPhoneCallMetaDataList.enCallStateUpdateDisconnectReason)));

         //push back data to the vector
         m_rAppPhoneData.tvecPhoneCallMetaDataList.push_back(rPhoneCallMetaDataList);
      }

      // post the data to service
      if (NULL != m_poConnIntf)
      {
         m_poConnIntf->vPostApplicationPhoneData(m_rAppPhoneData, corEmptyUsrContext);
      }//if (NULL != m_poConnIntf)
   }
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclMPlayClientHandler::vOnDiPOCallStateStatus : Message extraction failed. \n"));
   }
   oMsgCallStateData.vDestroy();
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::enGetRegistrationStatus()
 ***************************************************************************/
tenRegistrationStatus spi_tclMPlayClientHandler::enGetRegistrationStatus(
      mplay_fi_tcl_e8_DiPOCommunicationsUpdateRegistrationStatus &rfenRegistrationStatus)
{
   ETG_TRACE_USR1(("spi_tclMPlayClientHandler::enGetRegistrationStatus entered "));

   tenRegistrationStatus enRegistrationStatus = e8PHONE_REGISTRATION_NOT_KNOWN;
   switch(rfenRegistrationStatus.enType)
   {
   case mplay_fi_tcl_e8_DiPOCommunicationsUpdateRegistrationStatus::FI_EN_E8DIPO_CURS_UNKNONW :
      enRegistrationStatus = e8PHONE_REGISTRATION_NOT_KNOWN;
      break;
   case mplay_fi_tcl_e8_DiPOCommunicationsUpdateRegistrationStatus::FI_EN_E8DIPO_CURS_NOTREGISTERED :
      enRegistrationStatus = e8NOT_REGISTERED;
      break;
   case mplay_fi_tcl_e8_DiPOCommunicationsUpdateRegistrationStatus::FI_EN_E8DIPO_CURS_SEARCHING :
      enRegistrationStatus = e8SEARCHING;
      break;
   case mplay_fi_tcl_e8_DiPOCommunicationsUpdateRegistrationStatus::FI_EN_E8DIPO_CURS_DENIED :
      enRegistrationStatus = e8DENIED;
      break;
   case mplay_fi_tcl_e8_DiPOCommunicationsUpdateRegistrationStatus::FI_EN_E8DIPO_CURS_REGISTEREDHOME :
      enRegistrationStatus = e8REGISTERED_HOME;
      break;
   case mplay_fi_tcl_e8_DiPOCommunicationsUpdateRegistrationStatus::FI_EN_E8DIPO_CURS_REGISTEREDROAMING :
      enRegistrationStatus = e8REGISTERED_ROAMING;
      break;
   case mplay_fi_tcl_e8_DiPOCommunicationsUpdateRegistrationStatus::FI_EN_E8DIPO_CURS_EMERGENCYCALLONLY :
      enRegistrationStatus = e8EMERGENCY_CALLS_ONLY;
      break;
   default :
      enRegistrationStatus = e8PHONE_REGISTRATION_NOT_KNOWN;
   }
   return enRegistrationStatus;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vOnDiPOCommunicationsStatus(amt_tclServiceData)
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnDiPOCommunicationsStatus(
         amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclMPlayClientHandler::vOnDiPOCommunicationsStatus entered "));

   spi_tMsgCommunicationsStatus oMsgCommunicationsData(*poMessage, DIPO_MPLY_MAJORVERSION);

   if (oMsgCommunicationsData.bIsValid())
   {
      //Store the phone meta data info and send it to service.

      m_rAppPhoneData.bPhoneMetadataValid = true;
      m_rAppPhoneData.enSignalStrength = static_cast<tenSignalStrength>(oMsgCommunicationsData.eSignalStrength.enType);
      m_rAppPhoneData.enRegistrationStatus = enGetRegistrationStatus(oMsgCommunicationsData.eRegistrationStatus);
      m_rAppPhoneData.enAirPlaneModeStatus = (true == oMsgCommunicationsData.bAirplaneModeStatus) ? (e8AIRPLANE_ON) : (e8AIRPLANE_OFF);

      GET_STRINGDATA_FROM_FI_STRINGOBJ(oMsgCommunicationsData.sCarrierName,
            MPLAYFI_CHAR_SET_UTF8, m_rAppPhoneData.szCarrierName);

      m_rAppPhoneData.enMute = (true == oMsgCommunicationsData.bMuteStatus) ? (e8PHONE_MUTE_ON):(e8PHONE_MUTE_OFF);
      m_rAppPhoneData.u8CurrentCallCount = oMsgCommunicationsData.u8CurrentCallCount;

      ETG_TRACE_USR4(("[PARAM]:vOnDiPOCommunicationsStatus : phone metadata validity = %d", ETG_ENUM(BOOL, m_rAppPhoneData.bPhoneMetadataValid)));

      ETG_TRACE_USR4(("[PARAM]:vOnDiPOCommunicationsStatus : Signal Strength = %d, Registration Status = %d ",
            ETG_ENUM(SIGNAL_STRENGTH, m_rAppPhoneData.enSignalStrength), ETG_ENUM(REGISTRATION_STATUS, m_rAppPhoneData.enRegistrationStatus)));

      ETG_TRACE_USR4(("[PARAM]:vOnDiPOCommunicationsStatus : Airplane Mode = %d",
            ETG_ENUM(AIRPLANE_MODE_STATUS, m_rAppPhoneData.enAirPlaneModeStatus)));

      ETG_TRACE_USR4(("[PARAM]:vOnDiPOCommunicationsStatus : Carrier Name on the phone = %s",
            m_rAppPhoneData.szCarrierName.c_str()));

      ETG_TRACE_USR4(("[PARAM]:vOnDiPOCommunicationsStatus : Mute Status = %d, Current Call Count = %d ",
            ETG_ENUM(PHONE_MUTE_STATUS, m_rAppPhoneData.enMute), m_rAppPhoneData.u8CurrentCallCount));

      //post the data to service
      if (NULL != m_poConnIntf)
      {
         m_poConnIntf->vPostApplicationPhoneData(m_rAppPhoneData, corEmptyUsrContext);
      }//if (NULL != m_poConnIntf)
   }
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclMPlayClientHandler::vOnDiPOCommunicationsStatus : Message extraction failed. \n"));
   }
   oMsgCommunicationsData.vDestroy();
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vOnDiPOActiveDevice(amt_tclServiceData)
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnDiPOActiveDevice(
         amt_tclServiceData* poMessage) const
{

   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   spi_tMsgDipoActiveResult oDiPOActiveResult(*poMessage, DIPO_MPLY_MAJORVERSION);

   if (oDiPOActiveResult.bIsValid())
   {
      if (mplay_fi_tcl_e8_DiPOResponse::FI_EN_E8DIPO_OK != oDiPOActiveResult.e8DiPOResponse.enType)
      {
          ETG_TRACE_ERR(("DiPO Active operation failed."));         
      }
   }
   else //if (!oDiPOActiveResult.bIsValid())
   {
      ETG_TRACE_ERR(("Message extraction failed."));
   }

   oDiPOActiveResult.vDestroy();
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vOnDiPORoleSwitchRequest(amt_tclServiceData)
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnDiPORoleSwitchRequest(
         amt_tclServiceData* poMessage/*mplay_appcontrolfi_tclMsgDiPORoleSwitchRequestMethodResult &oRoleSwitchResult*/)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   spi_tMsgDipoRoleSwitchResult oRoleSwitchResult(*poMessage,
            DIPO_MPLY_MAJORVERSION);
   trMsgContext oMsgContext;
   tenErrorCode enError = e8NO_ERRORS;
   tenResponseCode enResponseCode = e8FAILURE;
   CPY_TO_USRCNTXT(poMessage, oMsgContext.rUserContext);
   if (oRoleSwitchResult.bIsValid())
   {
      m_bRoleSwitchTriggerStatus = false;
      t_Bool bDeviceListUpdateRequired = true;
      if (mplay_fi_tcl_e8_DiPOResponse::FI_EN_E8DIPO_OK == 
         oRoleSwitchResult.e8DiPOResponse.enType)
      {
         bDeviceListUpdateRequired = false;
         enResponseCode = e8SUCCESS;
      }
      else if((mplay_fi_tcl_e8_DiPOResponse::FI_EN_E8DIPO_ERROR_UNKNOWN == 
         oRoleSwitchResult.e8DiPOResponse.enType) &&
         (e8DEVCONNREQ_DESELECT == m_enDevConnReq))
      {
         enResponseCode = e8SUCCESS;
      }
      else if((mplay_fi_tcl_e8_DiPOResponse::FI_EN_E8DIPO_ERROR_UNKNOWN == 
         oRoleSwitchResult.e8DiPOResponse.enType) && (e8DEVCONNREQ_SELECT == m_enDevConnReq))
      {
         enError = e8UNKNOWN_ERROR;
      }
      else if(mplay_fi_tcl_e8_DiPOResponse::FI_EN_E8DIPO_ERROR_INVALIDHANLDE == 
         oRoleSwitchResult.e8DiPOResponse.enType)
      {
         enError = e8INVALID_DEV_HANDLE;
      }
      else
      {
         enError = e8UNSUPPORTED_OPERATION;
      } // end of if

      // Send the device selection response to connection manager
      if(nullptr != m_poConnIntf)
      {
         ETG_TRACE_USR1((" USB role swich result %d\n", ETG_ENUM( DIPO_ROLE_SWITCH, oRoleSwitchResult.e8DiPOResponse.enType)));
         m_poConnIntf->vPostSelectDeviceResult(enResponseCode, enError);
      } // if(NULL != m_poConnIntf) 

      // Explicitly requesting for an device list update from mediaplayer.
      // This logic is added because SPI is skipping any device disconnection update during a role switch.
      // So if there is a device disconnection during role switch, 
      // We need a status update to update this to HMI.
      if(true == bDeviceListUpdateRequired)
      {
         trMsgContext rMsgContext;
         vPopulateMessageContext(rMsgContext);
         FIMsgDispatch msgDispatcher(m_poMainApp);
         MplayConnectionsGet oMplayConnGet;
         t_Bool bRetVal = msgDispatcher.bSendMessage(oMplayConnGet,
         rMsgContext,
         DIPO_MPLY_MAJORVERSION);
		 ETG_TRACE_USR1(("Message send status from vOnDiPORoleSwitchRequest %d", bRetVal));
		 
      }
   }
   else // if (!oRoleSwitchResult.bIsValid())
   {
      ETG_TRACE_ERR(("Message extraction failed."));
   }

   oRoleSwitchResult.vDestroy();
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vOnDiPOGetMediaObjectAlbumArtInfo(amt_tclServiceData)
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnDiPOGetMediaObjectAlbumArtInfo(
         amt_tclServiceData* poMessage)
{
   //SPI_INTENTIONALLY_UNUSED(poMessage);
   ETG_TRACE_USR1((" spi_tclMPlayClientHandler::vOnDiPOGetMediaObjectAlbumArtInfo entered \n"));
   //SPI_INTENTIONALLY_UNUSED(poMessage);           //To remove Lint Warning

   spi_tMsgDipoGetAlbArtInfoResult oMsgDipoGetAlbArtInfoResult(*poMessage, DIPO_MPLY_MAJORVERSION);

   if(oMsgDipoGetAlbArtInfoResult.bIsValid())
   {
	  m_rAppMediaMetaData.u32ImageSize = oMsgDipoGetAlbArtInfoResult.u32PhotoSize;
      GET_STRINGDATA_FROM_FI_STRINGOBJ(oMsgDipoGetAlbArtInfoResult.sMIMEImageSubtype,
            MPLAYFI_CHAR_SET_UTF8, m_rAppMediaMetaData.szImageMIMEType);

      ETG_TRACE_USR4(("[PARAM]: vOnDiPOGetMediaObjectAlbumArtInfo: ImageMIMEType = %s ",
    		  m_rAppMediaMetaData.szImageMIMEType.c_str()));

      ETG_TRACE_USR4(("[PARAM]: vOnDiPOGetMediaObjectAlbumArtInfo: ImageSize = %d ",
    		  m_rAppMediaMetaData.u32ImageSize));

      // Posting meta data
      if (NULL != m_poConnIntf)
      {
         m_poConnIntf->vPostApplicationMediaMetaData(m_rAppMediaMetaData, corEmptyUsrContext);
      }//if (NULL != m_poConnIntf)
   }
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclMPlayClientHandler::vOnDiPOGetMediaObjectAlbumArtInfo Message extraction failed."));
   }
   oMsgDipoGetAlbArtInfoResult.vDestroy();
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vOnDiPOGetMediaObjectAlbumArt(amt_tclServiceData)
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnDiPOGetMediaObjectAlbumArt(
         amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(poMessage);           //To remove Lint Warning
   //! Add code
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vOnDiPOTransferGPSData(amt_tclServiceData)
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnDiPOTransferGPSData(
         amt_tclServiceData* poMessage) const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   spi_tMsgDipoTransGPSDataReult oGPSDataResult(*poMessage,
            DIPO_MPLY_MAJORVERSION);
   if (oGPSDataResult.bIsValid())
   {
      if (mplay_fi_tcl_e8_DiPOResponse::FI_EN_E8DIPO_OK == oGPSDataResult.e8DiPOResponse.enType)
      {
         // TODO : send the response using response interface
      }
   }
   else
   {
       ETG_TRACE_ERR(("Message extraction failed."));
   }
   oGPSDataResult.vDestroy();
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vOnDiPOTransferDRData(amt_tclServiceData)
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnDiPOTransferDRData(
         amt_tclServiceData* poMessage) const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   spi_tMsgDipoTransDRDataResult oDRDataResult(*poMessage,
            DIPO_MPLY_MAJORVERSION);

   if (oDRDataResult.bIsValid())
   {
      if (mplay_fi_tcl_e8_DiPOResponse::FI_EN_E8DIPO_OK == oDRDataResult.e8DiPOResponse.enType)
      {
         // TODO : send the response using response interface
      }
   }
   else
   {
       ETG_TRACE_ERR(("Message extraction failed."));
   }
   oDRDataResult.vDestroy();
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vOnStatusDiPOLocationInfo(amt_tclServiceData)
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnStatusDiPOLocationInfo(
         amt_tclServiceData* poMessage) const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   spi_tMsgDipoLocInfoStatus oLocInfoStatus(*poMessage, DIPO_MPLY_MAJORVERSION);
   spi_tclFactory* poFactory = spi_tclFactory::getInstance();

   if ((true == oLocInfoStatus.bIsValid()) && (NULL != poFactory))
   {
         spi_tclDataService* poDataService = poFactory->poGetDataServiceInstance();
         if (NULL != poDataService)
         {
            std::vector<tenNmeaSentenceType> NmeaSentencesList;

            //! Populate required Nmea sentences into list for Start LocationInfo request
            if (oLocInfoStatus.bStartStopLocationInfo)
            {
               if (true == oLocInfoStatus.b8LocationInfoType.bGPGGA())
               {
                  ETG_TRACE_USR4(("vOnStatusDiPOLocationInfo: GPGGA sentence requested "));
                  NmeaSentencesList.push_back(e8NMEA_GPGGA);
               }
               if (true == oLocInfoStatus.b8LocationInfoType.bGPRMC())
               {
                  ETG_TRACE_USR4(("vOnStatusDiPOLocationInfo: GPRMC sentence requested "));
                  NmeaSentencesList.push_back(e8NMEA_GPRMC);
               }
               if (true == oLocInfoStatus.b8LocationInfoType.bPASCD())
               {
                  ETG_TRACE_USR4(("vOnStatusDiPOLocationInfo: PASCD sentence requested "));
                  NmeaSentencesList.push_back(e8NMEA_PASCD);
               }
               if (true == oLocInfoStatus.b8LocationInfoType.bPAGCD())
               {
                  ETG_TRACE_USR4(("vOnStatusDiPOLocationInfo: PAGCD sentence requested "));
                  NmeaSentencesList.push_back(e8NMEA_PAGCD);
               }
               /* Below formats are currently not supported
                  Code retained for future use.
               if (true == oLocInfoStatus.b8LocationInfoType.bGPGSV())
               {
                  NmeaSentencesList.push_back(e8NMEA_GPGSV);
               }
               if (true == oLocInfoStatus.b8LocationInfoType.bGPHDT())
               {
                  NmeaSentencesList.push_back(e8NMEA_GPHDT);
               }
               if (true == oLocInfoStatus.b8LocationInfoType.bPAACD())
               {
                  NmeaSentencesList.push_back(e8NMEA_PAACD);
               }*/
            } //if (oLocInfoStatus.bStartStopLocationInfo)

            poDataService->vStartStopLocationData(
                  oLocInfoStatus.bStartStopLocationInfo, NmeaSentencesList);
         } //if (NULL != poDataService)
   } //if ((true == oLocInfoStatus.bIsValid()) && (NULL != poFactory))
   else if (false == oLocInfoStatus.bIsValid())
   {
       ETG_TRACE_ERR(("Message extraction failed."));
   }
   oLocInfoStatus.vDestroy();
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vOnDiPOReqAudioDevice(amt_tclServiceData)
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnDiPOReqAudioDevice(
         amt_tclServiceData* poMessage) const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   spi_tMsgDipoReqAudioDevResult oReqAudDevResult(*poMessage, DIPO_MPLY_MAJORVERSION);
   if (true == oReqAudDevResult.bIsValid())
   {
      //Retrieve Audio device name & error code
      t_String szAudioDevice;
      GET_STRINGDATA_FROM_FI_STRINGOBJ(oReqAudDevResult.sDeviceName,
            mplay_fi_tclString::FI_EN_UTF8, szAudioDevice);
      tenAudioError enAudioErr = enGetAudioError(oReqAudDevResult.e8Error);

      ETG_TRACE_USR3(("vOnDiPOReqAudioDevice: Received AudioError = %d, AudioDevice = %s ",
            ETG_ENUM(AUDIO_ERROR, enAudioErr), szAudioDevice.c_str()));

      //! Send Audio Device name & error code in loopback msg.
      tLbMplayAudioDevice oAudioDevLbMsg
            (
               m_poMainApp->u16GetAppId(),       // Source AppID
               m_poMainApp->u16GetAppId(),       // Target AppID
               0,                                // RegisterID
               (tU16)enAudioErr,                 // CmdCounter
               cou32SPI_SERVICE_ID,              // ServiceID
               SPI_C_U16_IFID_MPLAY_REQAUDIODEV_RESULT, // Function ID
               AMT_C_U8_CCAMSG_OPCODE_STATUS,    // Opcode
               strlen(szAudioDevice.c_str()) + 1 // Buffer Size
            );

      //! Set the data & post message
      oAudioDevLbMsg.vSetData((const tChar*)(szAudioDevice.c_str()));

      if (true == oAudioDevLbMsg.bIsValid())
      {
         if (AIL_EN_N_NO_ERROR != m_poMainApp->enPostMessage(&oAudioDevLbMsg, TRUE))
         {
            ETG_TRACE_ERR(("vOnDiPOReqAudioDevice: Loopback message posting failed! "));
         }
      } //if (true == oAudioDevLbMsg.bIsValid())
      else
      {
        ETG_TRACE_ERR(("vOnDiPOReqAudioDevice: Loopback message creation failed! "));
      }
   }//if (true == oReqAudDevResult.bIsValid())
   else
   {
      ETG_TRACE_ERR(("vOnDiPOReqAudioDevice: Invalid message received! "));
   }
   oReqAudDevResult.vDestroy();
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vOnDiPORelAudioDevice(amt_tclServiceData)
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vOnDiPORelAudioDevice(
         amt_tclServiceData* poMessage) const
{
   ETG_TRACE_USR1(("spi_tclMPlayClientHandler::vOnDiPORelAudioDevice() entered "));

   spi_tMsgDipoRelAudioDevResult oRelAudDevResult(*poMessage, DIPO_MPLY_MAJORVERSION);
   if (true == oRelAudDevResult.bIsValid())
   {
      //Retrieve error code
      tenAudioError enAudioErr = enGetAudioError(oRelAudDevResult.e8Error);

      ETG_TRACE_USR3(("vOnDiPORelAudioDevice: Received AudioError = %d ",
            ETG_ENUM(AUDIO_ERROR, enAudioErr)));

   }//if (true == oRelAudDevResult.bIsValid())
   else
   {
      ETG_TRACE_ERR(("vOnDiPORelAudioDevice: Invalid message received! "));
   }
   oRelAudDevResult.vDestroy();
}

/* Functions to interact with SPI design */
/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::bDiPORoleSwitchRequest(tU8 deviceTag,. 
 ***************************************************************************/
t_Bool spi_tclMPlayClientHandler::bDiPORoleSwitchRequest(t_U32 u32DeviceTag, tenDeviceConnectionReq enDevConnReq)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bRetVal = false;
   m_enDevConnReq = enDevConnReq;
   mplay_appcontrolfi_tclMsgDiPORoleSwitchRequestMethodStart oMplayRoleSwitch;

   // Generate message context
   trMsgContext rMsgContext;
   vPopulateMessageContext(rMsgContext);
   FIMsgDispatch msgDispatcher(m_poMainApp);

   if(e8DEVCONNREQ_SELECT == enDevConnReq)
   {
	  ETG_TRACE_USR1(("From bDiPORoleSwitchRequest() connection called"));	
      // Message to do a role switch to the host mode
      m_bRoleSwitchTriggerStatus = true;
      oMplayRoleSwitch.u8DeviceTag = u32DeviceTag;
      //!For Dummy Implementation,will be removed later
      m_u32DeviceTag = u32DeviceTag;
      oMplayRoleSwitch.e8DiPORoleStatus.enType
         = mplay_fi_tcl_e8_DiPORoleStatus::FI_EN_E8DIPO_HOSTMODE;
      bRetVal = msgDispatcher.bSendMessage(oMplayRoleSwitch,
         rMsgContext,
         DIPO_MPLY_MAJORVERSION);
      
   }
   else // if(e8DEV_CONNECT == enDevConnReq)
   {
      ETG_TRACE_USR1(("From bDiPORoleSwitchRequest() disconnection called"));
      m_bRoleSwitchTriggerStatus = true;
      oMplayRoleSwitch.u8DeviceTag = u32DeviceTag;
      oMplayRoleSwitch.e8DiPORoleStatus.enType
         = mplay_fi_tcl_e8_DiPORoleStatus::FI_EN_E8DIPO_CLIENTMODE;
      bRetVal = msgDispatcher.bSendMessage(oMplayRoleSwitch,
         rMsgContext,
         DIPO_MPLY_MAJORVERSION);  

   } // if(e8DEV_CONNECT == enDevConnReq)
   return bRetVal;
}
/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::bDiPODeviceActivate(tU8 deviceTag,. 
 ***************************************************************************/
t_Bool spi_tclMPlayClientHandler::bDiPODeviceActivate(t_U8 u8DeviceTag,
         t_Bool bDeviceActive)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   mplay_appcontrolfi_tclMsgDiPOActiveDeviceMethodStart oMplayDeviceActive;
   oMplayDeviceActive.u8DeviceTag = u8DeviceTag;
   oMplayDeviceActive.bDiPOActive = bDeviceActive;

   // generate message context
   trMsgContext rMsgContext;
   vPopulateMessageContext(rMsgContext);
   FIMsgDispatch msgDispatcher(m_poMainApp);

   // Send method start to mplay fi
   return msgDispatcher.bSendMessage(oMplayDeviceActive,
            rMsgContext,
            DIPO_MPLY_MAJORVERSION);
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::bDiPOGetAlbumArtInfo(tU8 deviceTag,DiPOImageURL url) 
 ***************************************************************************/
t_Bool spi_tclMPlayClientHandler::bDiPOGetAlbumArtInfo(t_String sAlbumArt)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__)); 
   tBool bResult = false;

   if (OSAL_NULL != m_poMainApp)
   {
      mplay_appcontrolfi_tclMsgDiPOGetMediaObjectAlbumArtInfoMethodStart oMPlayAlbumArtInfo;
      oMPlayAlbumArtInfo.sAlbumArt.bSet(sAlbumArt.c_str(), MPLAYFI_CHAR_SET_UTF8);

      // Generate message context
      trMsgContext rMsgContext;
      vPopulateMessageContext(rMsgContext);
      FIMsgDispatch msgDispatcher(m_poMainApp);

      // Send method start to mplay fi
      bResult = msgDispatcher.bSendMessage(oMPlayAlbumArtInfo,
            rMsgContext,
            DIPO_MPLY_MAJORVERSION);
   } // if (OSAL_NULL != m_poMainApp)

   return bResult;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::bGetDiPOAlbumart(tU8 deviceTag,DiPOImageURL url)
 ***************************************************************************/
t_Bool spi_tclMPlayClientHandler::bGetDiPOAlbumArt(t_String sUrl)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   tBool bResult = false;

   if (OSAL_NULL != m_poMainApp)
   {
      mplay_appcontrolfi_tclMsgDiPOGetMediaObjectAlbumArtMethodStart oMPlayAlbumArt;
      oMPlayAlbumArt.sAlbumArt.bSet(sUrl.c_str(), MPLAYFI_CHAR_SET_UTF8);

      // populate message context
      trMsgContext rMsgContext;
      vPopulateMessageContext(rMsgContext);
      FIMsgDispatch msgDispatcher(m_poMainApp);

      // send method start to mplay fi
      bResult = msgDispatcher.bSendMessage(oMPlayAlbumArt,
               rMsgContext,
               DIPO_MPLY_MAJORVERSION);
   } // if (OSAL_NULL != m_poMainApp)

   return bResult;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMPlayClientHandler::bTransferGPSData(t_String...
 ***************************************************************************/
t_Bool spi_tclMPlayClientHandler::bTransferGPSData(t_U32 u32DeviceHandle,
      t_String szGPGGAData, t_String szGPRMCData,
      t_String szGPGSVData, t_String szGPHDTData)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   t_Bool bResult = false;
   if (OSAL_NULL != m_poMainApp)
   {
      mplay_appcontrolfi_tclMsgDiPOTransferGPSDataMethodStart oMPlayGPSData;
      oMPlayGPSData.u8DeviceTag = u32DeviceHandle;
      oMPlayGPSData.sGPGGAData.bSet(szGPGGAData.c_str(), MPLAYFI_CHAR_SET_UTF8);
      oMPlayGPSData.sGPRMCData.bSet(szGPRMCData.c_str(), MPLAYFI_CHAR_SET_UTF8);
      oMPlayGPSData.sGPGSVData.bSet(szGPGSVData.c_str(), MPLAYFI_CHAR_SET_UTF8);
      oMPlayGPSData.sGPHDTData.bSet(szGPHDTData.c_str(), MPLAYFI_CHAR_SET_UTF8);

      // populate message context
      trMsgContext rMsgContext;
      vPopulateMessageContext(rMsgContext);
      FIMsgDispatch msgDispatcher(m_poMainApp);

      // send the message to mplay fi
      bResult = msgDispatcher.bSendMessage(oMPlayGPSData,
               rMsgContext,
               DIPO_MPLY_MAJORVERSION);
   }
   return bResult;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMPlayClientHandler::bTransferDrData(t_String...
 ***************************************************************************/
t_Bool spi_tclMPlayClientHandler::bTransferDrData(t_U32 u32DeviceHandle,
      t_String szPASCDData, t_String szPAGCDData, t_String szPAACDData)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   t_Bool bResult = false;
   if (OSAL_NULL != m_poMainApp)
   {
      mplay_appcontrolfi_tclMsgDiPOTransferDRDataMethodStart oMplayDrData;
      oMplayDrData.u8DeviceTag = u32DeviceHandle;
      oMplayDrData.sPASCDData.bSet(szPASCDData.c_str(), MPLAYFI_CHAR_SET_UTF8);
      oMplayDrData.sPAGCDData.bSet(szPAGCDData.c_str(), MPLAYFI_CHAR_SET_UTF8);
      oMplayDrData.sPAACDData.bSet(szPAACDData.c_str(), MPLAYFI_CHAR_SET_UTF8);

      // populate message context
      trMsgContext rMsgContext;
      vPopulateMessageContext(rMsgContext);
      FIMsgDispatch msgDispatcher(m_poMainApp);

      // send the message to mplay fi
      bResult = msgDispatcher.bSendMessage(oMplayDrData, rMsgContext,
               DIPO_MPLY_MAJORVERSION);
   }
   return bResult;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMPlayClientHandler::bRequestAudioDevice()
 ***************************************************************************/
t_Bool spi_tclMPlayClientHandler::bRequestAudioDevice(t_U32 u32DeviceHandle)
{
  ETG_TRACE_USR1(("spi_tclMPlayClientHandler::bRequestAudioDevice: DeviceTag %d ", u32DeviceHandle));

   t_Bool bResult = false;

   if (OSAL_NULL != m_poMainApp)
   {
      mplay_appcontrolfi_tclMsgDiPORequestAudioDeviceMethodStart oMplayReqAudDeviceMS;
      oMplayReqAudDeviceMS.u8DeviceTag = u32DeviceHandle;
      
      //! populate message context
      trMsgContext rMsgContext;
      vPopulateMessageContext(rMsgContext);
      FIMsgDispatch msgDispatcher(m_poMainApp);

      //! send the message to mediaplayer
      bResult = msgDispatcher.bSendMessage(oMplayReqAudDeviceMS, rMsgContext, DIPO_MPLY_MAJORVERSION);

      if (false == bResult)
      {
         ETG_TRACE_ERR((" bRequestAudioDevice: Posting RequestAudioDevice MS message failed! "));
      }
   }//if (OSAL_NULL != m_poMainApp)
   return bResult;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMPlayClientHandler::bReleaseAudioDevice()
 ***************************************************************************/
t_Bool spi_tclMPlayClientHandler::bReleaseAudioDevice(t_U32 u32DeviceHandle)
{
  ETG_TRACE_USR1(("spi_tclMPlayClientHandler::bReleaseAudioDevice: DeviceTag %d ", u32DeviceHandle));

   t_Bool bResult = false;

   if (OSAL_NULL != m_poMainApp)
   {
      mplay_appcontrolfi_tclMsgDiPOReleaseAudioDeviceMethodStart oMplayRelAudDeviceMS;
      oMplayRelAudDeviceMS.u8DeviceTag = u32DeviceHandle;

      //! populate message context
      trMsgContext rMsgContext;
      vPopulateMessageContext(rMsgContext);
      FIMsgDispatch msgDispatcher(m_poMainApp);

      //! send the message to mediaplayer
      bResult = msgDispatcher.bSendMessage(oMplayRelAudDeviceMS, rMsgContext, DIPO_MPLY_MAJORVERSION);

      if (false == bResult)
      {
         ETG_TRACE_ERR((" bReleaseAudioDevice: Posting ReleaseAudioDevice MS message failed! "));
      }
   }//if (OSAL_NULL != m_poMainApp)
   return bResult;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vPopulateMessageContext(trMsgContext
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vPopulateMessageContext(
         trMsgContext &rMsgContext) const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   rMsgContext.rUserContext.u32DestAppID = u16GetServerAppID();
   rMsgContext.rUserContext.u32SrcAppID = CCA_C_U16_APP_SMARTPHONEINTEGRATION;
   rMsgContext.rUserContext.u32RegID = u16GetRegID();
   rMsgContext.rUserContext.u32CmdCtr = 0;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMPlayClientHandler::vPopulateDipoVersion(trMsgContext
 ***************************************************************************/
t_Void spi_tclMPlayClientHandler::vPopulateDipoVersion(t_String szSource,
         t_U32 &u32XValue, t_U32 &u32YValue, t_U32 &u32ZValue) const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   t_U8 u8FirstSepPos = szSource.find_first_of(".");
   t_U8 u8SecondSepPos = szSource.find_first_of(".", u8FirstSepPos+1);
   t_String szXValue = szSource.substr(0, u8FirstSepPos);
   t_String szYValue = szSource.substr(u8FirstSepPos+1, u8SecondSepPos-1);
   t_String szZValue = szSource.substr(u8SecondSepPos+1);
   u32XValue = atoi(szXValue.c_str());
   u32YValue = atoi(szYValue.c_str());
   u32ZValue = atoi(szZValue.c_str());
   ETG_TRACE_USR1(("Function vPopulateDipoVersion() XValue == %s",szXValue.c_str()));
   ETG_TRACE_USR1(("Function vPopulateDipoVersion() XValue == %d",u32XValue ));
}

/***************************************************************************
 ** FUNCTION:  tenAudioError spi_tclMPlayClientHandler::enGetAudioError(...)
 ***************************************************************************/
tenAudioError spi_tclMPlayClientHandler::enGetAudioError(
      const mplay_tFiAudError& rfcpAudioError) const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   tenAudioError enAudError;
   switch (rfcpAudioError.enType)
   {
      case mplay_tFiAudError::FI_EN_E8NO_ERROR:
         enAudError = e8_AUDIOERROR_NONE;
         break;
      case mplay_tFiAudError::FI_EN_E8ACTIVATION_ERROR:
         enAudError = e8_AUDIOERROR_AVACTIVATION;
         break;
      case mplay_tFiAudError::FI_EN_E8ALLOCATE_ERROR:
         enAudError = e8_AUDIOERROR_ALLOCATE;
         break;
      case mplay_tFiAudError::FI_EN_E8SOURCEACT_ON_ERROR:
         enAudError = e8_AUDIOERROR_STARTSOURCEACT;
         break;
      case mplay_tFiAudError::FI_EN_E8DEACTIVATION_ERROR:
         enAudError = e8_AUDIOERROR_AVDEACTIVATION;
         break;
      case mplay_tFiAudError::FI_EN_E8DEALLOCATE_ERROR:
         enAudError = e8_AUDIOERROR_DEALLOCATE;
         break;
      case mplay_tFiAudError::FI_EN_E8SOURCEACT_OFF_ERROR:
         enAudError = e8_AUDIOERROR_STOPSOURCEACT;
         break;
      case mplay_tFiAudError::FI_EN_E8UNKNOWN_ERROR:
      default:
         enAudError = e8_AUDIOERROR_UNKNOWN;
         break;
   }//switch (rfcpAudioError.enType)

   ETG_TRACE_USR3(("enGetAudioError() left with %d ",
         ETG_ENUM(AUDIO_ERROR, enAudError)));
   return enAudError;
}


// code for testing
t_Void spi_tclMPlayClientHandler::vTestMediaPlayer()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   /*mplay_appcontrolfi_tclMsgDiPODeviceConnectionsStatus oDipoDevConnStatus;
   mplay_fi_tcl_DiPODeviceInfoItem oInfoItem;


   oInfoItem.u8DeviceTag = 1;
   oInfoItem.bDeviceConnected = true;
   oInfoItem.bDiPOActive = false;
   oInfoItem.sDeviceName.bSet("iphone",
      mplay_fi_tclString::FI_EN_UTF8);
   oInfoItem.sDeviceSerialNumber.bSet("abcd", mplay_fi_tclString::FI_EN_UTF8);
   oInfoItem.bDeviceActiveSource = true;
   oInfoItem.sDiPOVersion.bSet("1.1.1", mplay_fi_tclString::FI_EN_UTF8);
   oInfoItem.bDiPOCapable = true;
   oInfoItem.e8DeviceType.enType = mplay_fi_tcl_e8_DiPODeviceType::FI_EN_E8DTY_IPHONE;

   oDipoDevConnStatus.oDeviceInfo.oItems.push_back(oInfoItem);

   // device two data
   oInfoItem.u8DeviceTag = 2;
   oInfoItem.bDeviceConnected = false;
   oInfoItem.bDiPOActive = false;
   oInfoItem.sDeviceName.bSet("iphone",
      mplay_fi_tclString::FI_EN_UTF8);
   oInfoItem.sDeviceSerialNumber.bSet("efgh", mplay_fi_tclString::FI_EN_UTF8);
   oInfoItem.bDeviceActiveSource = true;
   oInfoItem.sDiPOVersion.bSet("1.1.1", mplay_fi_tclString::FI_EN_UTF8);
   oInfoItem.bDiPOCapable = true;
   oInfoItem.e8DeviceType.enType = mplay_fi_tcl_e8_DiPODeviceType::FI_EN_E8DTY_IPHONE;

   oDipoDevConnStatus.oDeviceInfo.oItems.push_back(oInfoItem);
   // device 3 data

   oInfoItem.u8DeviceTag = 3;
   oInfoItem.bDeviceConnected = false;
   oInfoItem.bDiPOActive = false;
   oInfoItem.sDeviceName.bSet("iphone",
      mplay_fi_tclString::FI_EN_UTF8);
   oInfoItem.sDeviceSerialNumber.bSet("efgh", mplay_fi_tclString::FI_EN_UTF8);
   oInfoItem.bDeviceActiveSource = true;
   oInfoItem.sDiPOVersion.bSet("1.1.1", mplay_fi_tclString::FI_EN_UTF8);
   oInfoItem.bDiPOCapable = true;
   oInfoItem.e8DeviceType.enType = mplay_fi_tcl_e8_DiPODeviceType::FI_EN_E8DTY_IPHONE;
   oDipoDevConnStatus.oDeviceInfo.oItems.push_back(oInfoItem);

   vOnDiPODeviceConnectionsStatus(oDipoDevConnStatus);*/
}

t_Void spi_tclMPlayClientHandler::vDiPORoleSwitchResultTest()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
  /* mplay_appcontrolfi_tclMsgDiPORoleSwitchRequestMethodResult oDiPORoleSwitchMR;
   oDiPORoleSwitchMR.e8DiPOResponse.enType = mplay_fi_tcl_e8_DiPOResponse::FI_EN_E8DIPO_OK;
   vOnDiPORoleSwitchRequest(oDiPORoleSwitchMR);*/      
}

//lint –restore
