/*!
*******************************************************************************
* \file              spi_tclMPlayClientHandler.h
* \brief             DiPO Client handler class.
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO Client handler class for MPlay service
COPYRIGHT:      &copy; RBEI

HISTORY:
 Date       |  Author                      | Modifications
 08.1.2014  |  Shihabudheen P M            | Initial Version
 02.04.2014 | Hari Priya E R               | Implemented Timer for triggering
                                             transfer of GPS Data
 15.04.2014 | Ramya Muthy                  | Implemented Location data transfer
                                             and removed timer implementation.
 13.06.2014 | Ramya Murthy                 | Implementation for MPlay FI extn 
                                             for location info.
 31.07.2014 | Ramya Murthy                 | Changes for dynamic registration of properties
 05.11.2014 | Ramya Murthy                 | Implementation for Application metadata.
 05.11.2014 | Ramya Murthy                 | Implementation of revised CarPlay media concept 
 14.12.2015 | SHITANSHU SHEKHAR(RBEI/ECP2) | Implemented interfaces as per new Media player FI:
                                           | vOnDiPONowPlayingStatus(), vOnDiPOPlayBackStatus()
                                           | vOnDiPOPlayBackShuffleModeStatus(),
                                           | vOnDiPOPlayBackRepeatModeStatus(),
                                           | enGetPhoneCallState(), enGetPhoneCallDirection(),
                                           | enGetCallStateUpdateService(),
                                           | enGetCallStateUpdateDisconnectReason(),
                                           | vOnDiPOCallStateStatus(), enGetRegistrationStatus()
                                           | vOnDiPOCommunicationsStatus()

\endverbatim
******************************************************************************/
#ifndef SPI_TCLMPLAY_CLIENT_HANDLER_H_
#define SPI_TCLMPLAY_CLIENT_HANDLER_H_

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include <cstring>
#include <vector>

// Includes mplayAppControl Fi
#define MPLAY_FI_S_IMPORT_INTERFACE_FI_TYPES
#define MPLAY_FI_S_IMPORT_INTERFACE_MPLAY_APPCONTROLFI_TYPES
#define MPLAY_FI_S_IMPORT_INTERFACE_MPLAY_APPCONTROLFI_FUNCTIONIDS
#define MPLAY_FI_S_IMPORT_INTERFACE_MPLAY_APPCONTROLFI_ERRORCODES
#define MPLAY_FI_S_IMPORT_INTERFACE_MPLAY_APPCONTROLFI_SERVICEINFO
#include "mplay_fi_if.h"

#include "Timer.h"
#include "DiPOTypes.h"
#include "SPITypes.h"
#include "GenericSingleton.h"
#include "spi_tclClientHandlerBase.h"


/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
typedef mplay_fi_tcl_e8_AudioError  mplay_tFiAudError;


//! Forward declarations
class spi_tclMainApp;
class ahl_tclBaseOneThreadClientHandler;
class ahl_tclBaseOneThreadApp;
class spi_tclRespInterface;
//class mplay_appcontrolfi_tclMsgDiPODeviceConnectionsStatus;
//class mplay_appcontrolfi_tclMsgDiPORoleSwitchRequestMethodResult;
class spi_tclDiPOConnectionIntf;
class spi_tclDiPoConnection;



/****************************************************************************/
/*!
* \class DiPO_tclSPIClientHandler
* \brief DiPO clinet handler Implementation
*
* DiPO_tclSpiClientHandler is implement the client handler for DiPO based
* services provided by media player fi. It mainly dealt with connection
* establishment and management
****************************************************************************/
class spi_tclMPlayClientHandler : public ahl_tclBaseOneThreadClientHandler,
   public GenericSingleton<spi_tclMPlayClientHandler>, public spi_tclClientHandlerBase
{
public:

   /***************************************************************************
   *********************************PUBLIC************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  virtual spi_tclMPlayClientHandler::~spi_tclMPlayClientHandler()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclMPlayClientHandler()
   * \brief   Destructor
   * \sa
   **************************************************************************/
   virtual ~spi_tclMPlayClientHandler();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vRegisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers for all interested properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vUnregisterForProperties()
   **************************************************************************/
   virtual t_Void vRegisterForProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vRegisterForMetadataProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForMetadataProperties()
   * \brief   Registers for all interested properties to Metadata
   * \sa      vRegisterForMetadataProperties()
   **************************************************************************/
   virtual t_Void vRegisterForMetadataProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vUnregisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Un-registers all subscribed properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vRegisterForProperties()
   **************************************************************************/
   virtual t_Void vUnregisterForProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vUnregisterForMetadataProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForMetadataProperties()
   * \brief   Un-registers all subscribed properties to Metadata
   * \sa      vUnregisterForMetadataProperties()
   **************************************************************************/
   virtual t_Void vUnregisterForMetadataProperties();
   /* Call function declarations */

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMPlayClientHandler::bDiPORoleSwitchRequest(t_U8..
   ***************************************************************************/
   /*!
   * \fn      bDiPORoleSwitchRequest(t_U8 u8DeviceTag, )
   * \brief   Function to activate the dipo device
   * \param   u8DeviceTag: [IN] unique identifier of the device
   * \param   enDiPORoleStatus : [IN] Role to perform role switch
   * \sa
   **************************************************************************/
   t_Bool bDiPORoleSwitchRequest(t_U32 u8DeviceTag, tenDeviceConnectionReq enDevConnReq);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMPlayClientHandler::bDiPODeviceActivate(tU8 ...
   ***************************************************************************/
   /*!
   * \fn      bDiPODeviceActivate(tU8 deviceTag, t_Bool deviceActive)
   * \brief   Function to activate the dipo device
   * \param   u8DeviceTag: [IN] unique identifier of the device
   * \param   deviceActive : [IN] active status
   * \sa
   **************************************************************************/
   t_Bool bDiPODeviceActivate(t_U8 u8DeviceTag, t_Bool deviceActive);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMPlayClientHandler::bDiPOGetAlbumArtInfo(t_String sAlbumArt)
   ***************************************************************************/
   /*!
   * \fn      bDiPOGetAlbumArtInfo(t_String sAlbumArt)
   * \brief   Function to get album art info
   * \param   sAlbumArt : [IN] Album art
   * \retval  bool : true if succees, false otherwise
   **************************************************************************/
   t_Bool bDiPOGetAlbumArtInfo(t_String sAlbumArt);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMPlayClientHandler::bDiPOGetAlbumArt(t_String sUrl)
   ***************************************************************************/
   /*!
   * \fn     bDiPOGetAlbumArt(t_String sUrl)
   * \brief  Function to get album art
   * \param  sAlbumArt : [IN] Album art
   * \retval bool : true if succees, false otherwise
   **************************************************************************/
   t_Bool bGetDiPOAlbumArt(t_String sUrl);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMPlayClientHandler::bTransferGPSData(t_String...
   ***************************************************************************/
   /*!
   * \fn     bTransferGPSData(t_U32 u32DeviceHandle, t_String szGPGGAData,
   *            t_String szGPRMCData, t_String szGPGSVData, t_String szGPHDTData)
   * \brief  Function to transfer GPS date to device.
   * \param  u32DeviceHandle : [IN] Unique device handle of active device
   * \param  szGPGGAData : [IN] GPGGA data
   * \param  szGPRMCData : [IN] GPRMS data
   * \param  szGPGSVData : [IN] GPGSV data
   * \param  szGPHDTData : [IN] GPHDT data
   * \retval bool : true if succees, false otherwise
   **************************************************************************/
   t_Bool bTransferGPSData(t_U32 u32DeviceHandle,
         t_String szGPGGAData, t_String szGPRMCData,
         t_String szGPGSVData, t_String szGPHDTData);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMPlayClientHandler::bTransferDrData(t_String...
   ***************************************************************************/
   /*!
   * \fn     bTransferDrData(t_U32 u32DeviceHandle, t_String szPASCDData,
   *            t_String szPAGCDData, t_String szPAACDData)
   * \brief  Function to transfer DR date to device.
   * \param  u32DeviceHandle : [IN] Unique device handle of active device
   * \param  szPASCDData : [IN] PASCD data
   * \param  szPAGCDData : [IN] PAGCD data
   * \param  szPAACDData : [IN] PAGCD data
   * \retval bool : true if succees, false otherwise
   **************************************************************************/
   t_Bool bTransferDrData(t_U32 u32DeviceHandle, t_String szPASCDData,
         t_String szPAGCDData, t_String szPAACDData);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMPlayClientHandler::bRequestAudioDevice()
   ***************************************************************************/
   /*!
   * \fn     bRequestAudioDevice(t_U32 u32DeviceHandle)
   * \brief  Function to get audio device name
   * \retval bool : true if succees, false otherwise
   **************************************************************************/
   t_Bool bRequestAudioDevice(t_U32 u32DeviceHandle);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMPlayClientHandler::bReleaseAudioDevice()
   ***************************************************************************/
   /*!
   * \fn     bReleaseAudioDevice(t_U32 u32DeviceHandle)
   * \brief  Function to release audio device
   * \retval bool : true if succees, false otherwise
   **************************************************************************/
   t_Bool bReleaseAudioDevice(t_U32 u32DeviceHandle);

   // For test - Remove later
   t_Void vDiPORoleSwitchResultTest();

   // For test - Remove later
   t_Void vTestMediaPlayer();


   friend class GenericSingleton<spi_tclMPlayClientHandler>;

   /*************************************************************************
   ****************************END OF PUBLIC*********************************
   *************************************************************************/

protected:

   /*************************************************************************
   ****************************START OF PROTECTED****************************
   *************************************************************************/

   /***************************************************************************
   * Overriding ahl_tclBaseOneThreadClientHandler methods.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnServiceAvailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceAvailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes available, e.g. server has been started.
   * \sa
   **************************************************************************/
   virtual t_Void vOnServiceAvailable();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnServiceUnavailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceUnavailable()
   * \brief   This function is called by the framework if the service of our server
   *          becomes unavailable, e.g. server has been shut down.
   * \sa
   **************************************************************************/
   virtual t_Void vOnServiceUnavailable();

   /*************************************************************************
   ****************************END OF PROTECTED******************************
   *************************************************************************/

private:

   /*************************************************************************
   ****************************START OF PRIVATE******************************
   *************************************************************************/

   /***************************************************************************
   * Handler function declarations.
   **************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPODeviecConnectionsStatus()
   ***************************************************************************/
   /*!
   * \fn      vOnDiPODeviecConnectionsStatus(amt_tclServiceData* poMessage)
   * \brief   Call back function for DiPO Device Connection
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnDiPODeviceConnectionsStatus(amt_tclServiceData* poMessage /*mplay_appcontrolfi_tclMsgDiPODeviceConnectionsStatus &oDevConnectionStatus*/);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPOPlaytimeStatus()
   ***************************************************************************/
   /*!
   * \fn      vOnDiPOPlaytimeStatus(amt_tclServiceData* poMessage)
   * \brief   Call back function for Play time status
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnDiPOPlaytimeStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPONowPlayingStatus()
   ***************************************************************************/
   /*!
   * \fn      vOnDiPONowPlayingStatus(amt_tclServiceData* poMessage)
   * \brief   Call back function for Meta Data status
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnDiPONowPlayingStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPOPlayBackStatus()
   ***************************************************************************/
   /*!
   * \fn      vOnDiPOPlayBackStatus(amt_tclServiceData* poMessage)
   * \brief   Call back function for Meta Data status
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnDiPOPlayBackStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPOPlayBackShuffleModeStatus()
   ***************************************************************************/
   /*!
   * \fn      vOnDiPOPlayBackShuffleModeStatus(amt_tclServiceData* poMessage)
   * \brief   Call back function for Meta Data status
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnDiPOPlayBackShuffleModeStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPOPlayBackRepeatModeStatus()
   ***************************************************************************/
   /*!
   * \fn      vOnDiPOPlayBackRepeatModeStatus(amt_tclServiceData* poMessage)
   * \brief   Call back function for Meta Data status
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnDiPOPlayBackRepeatModeStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::enGetPhoneCallState()
   ***************************************************************************/
   /*!
   * \fn      enGetPhoneCallState(mplay_fi_tcl_e8_DiPOCallStateUpdateStatus &rfrenPhoneCallState)
   * \brief   Call back function for Phone Data status
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   tenPhoneCallState enGetPhoneCallState(
         mplay_fi_tcl_e8_DiPOCallStateUpdateStatus &rfrenPhoneCallState);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::enGetPhoneCallDirection()
   ***************************************************************************/
   /*!
   * \fn      enGetPhoneCallDirection(mplay_fi_tcl_e8_DiPOCallStateUpdateDirection &rfenPhoneCallDirection)
   * \brief   Call back function for Phone Data status
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   tenPhoneCallDirection enGetPhoneCallDirection(
         mplay_fi_tcl_e8_DiPOCallStateUpdateDirection &rfenPhoneCallDirection);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::enGetCallStateUpdateService()
   ***************************************************************************/
   /*!
   * \fn      enGetCallStateUpdateService(mplay_fi_tcl_e8_DiPOCallStateUpdateService &rfenCallStateUpdateService)
   * \brief   Call back function for Phone Data status
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   tenCallStateUpdateService enGetCallStateUpdateService(
         mplay_fi_tcl_e8_DiPOCallStateUpdateService &rfenCallStateUpdateService);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::enGetCallStateUpdateService()
   ***************************************************************************/
   /*!
   * \fn      enGetCallStateUpdateService(mplay_fi_tcl_e8_DiPOCallStateUpdateDisconnectReason
   *           &rfenCallStateUpdateDisconnectReason)
   * \brief   Call back function for Phone Data status
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   tenCallStateUpdateDisconnectReason enGetCallStateUpdateDisconnectReason(
         mplay_fi_tcl_e8_DiPOCallStateUpdateDisconnectReason &rfenCallStateUpdateDisconnectReason);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPOCallStateStatus()
   ***************************************************************************/
   /*!
   * \fn      vOnDiPOCallStateStatus(amt_tclServiceData* poMessage)
   * \brief   Call back function for Phone Data status
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnDiPOCallStateStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::enGetRegistrationStatus()
   ***************************************************************************/
   /*!
   * \fn      enGetRegistrationStatus(mplay_fi_tcl_e8_DiPOCommunicationsUpdateRegistrationStatus
   *           &rfenRegistrationStatus)
   * \brief   Call back function for Phone Data status
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   tenRegistrationStatus enGetRegistrationStatus(
         mplay_fi_tcl_e8_DiPOCommunicationsUpdateRegistrationStatus &rfenRegistrationStatus);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPOCommunicationsStatus()
   ***************************************************************************/
   /*!
   * \fn      vOnDiPOCommunicationsStatus(amt_tclServiceData* poMessage)
   * \brief   Call back function for Phone Data status
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnDiPOCommunicationsStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPOActiveDevice..
   ***************************************************************************/
   /*!
   * \fn      vOnDiPOActiveDevice(amt_tclServiceData* poMessage)
   * \brief
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnDiPOActiveDevice(amt_tclServiceData* poMessage) const;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPORoleSwitchRequest..
   ***************************************************************************/
   /*!
   * \fn      vOnDiPORoleSwitchRequest(amt_tclServiceData* poMessage)
   * \brief
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnDiPORoleSwitchRequest(amt_tclServiceData* poMessage /*mplay_appcontrolfi_tclMsgDiPORoleSwitchRequestMethodResult &oRoleSwitchResult*/);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPOGetMediaObjectAlbu..
   ***************************************************************************/
   /*!
   * \fn      vOnDiPOGetMediaObjectAlbumArtInfo(amt_tclServiceData* poMessage)
   * \brief
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnDiPOGetMediaObjectAlbumArtInfo(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPOGetMediaObjectAlbumArt..
   ***************************************************************************/
   /*!
   * \fn      vOnDiPOGetMediaObjectAlbumArt(amt_tclServiceData* poMessage)
   * \brief
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnDiPOGetMediaObjectAlbumArt(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPOTransferGPSData(..
   ***************************************************************************/
   /*!
   * \fn      vOnDiPOTransferGPSData(amt_tclServiceData* poMessage)
   * \brief
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnDiPOTransferGPSData(amt_tclServiceData* poMessage) const;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMPlayClientHandler::vOnDiPOTransferDRData..
   ***************************************************************************/
   /*!
   * \fn      vOnDiPOTransferDRData(amt_tclServiceData* poMessage)
   * \brief
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnDiPOTransferDRData(amt_tclServiceData* poMessage) const;

   /***************************************************************************
   ** FUNCTION:  spi_tclMPlayClientHandler::vOnStatusDiPOLocationInfo(amt_tclServiceData)
   ***************************************************************************/
   /*!
   * \fn      vOnStatusDiPOLocationInfo(amt_tclServiceData* poMessage)
   * \brief   Method to handle DipoLocationInfo Status message from MediaPlayer
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnStatusDiPOLocationInfo(amt_tclServiceData* poMessage) const;

   /***************************************************************************
   ** FUNCTION:  spi_tclMPlayClientHandler::vOnDiPOReqAudioDevice(amt_tclServiceData)
   ***************************************************************************/
   /*!
   * \fn      vOnDiPOReqAudioDevice(amt_tclServiceData* poMessage)
   * \brief   Method to handle DiPOAudioDevice MethodResult message from MediaPlayer
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnDiPOReqAudioDevice(amt_tclServiceData* poMessage) const;

   /***************************************************************************
   ** FUNCTION:  spi_tclMPlayClientHandler::vOnDiPORelAudioDevice(amt_tclServiceData)
   ***************************************************************************/
   /*!
   * \fn      vOnDiPORelAudioDevice(amt_tclServiceData* poMessage)
   * \brief   Method to handle DiPOReleaseAudioDevice MethodResult message from MediaPlayer
   * \param   poMessage : [IN] ponter to message
   * \sa
   **************************************************************************/
   t_Void vOnDiPORelAudioDevice(amt_tclServiceData* poMessage) const;

   /***************************************************************************
   * Other function declarations.
   **************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMPlayClientHandler::spi_tclMPlayClientHandler(..)
   ***************************************************************************/
   /*!
   * \fn     spi_tclMPlayClientHandler()
   * \brief  Default Constructor
   * \sa
   **************************************************************************/
   spi_tclMPlayClientHandler();

   /***************************************************************************
   ** FUNCTION:  spi_tclMPlayClientHandler::spi_tclMPlayClientHandler(..)
   ***************************************************************************/
   /*!
   * \fn     spi_tclMPlayClientHandler(ahl_tclBaseOneThreadApp* poMainApp)
   * \brief  Parameterized Constructor
   * \sa
   **************************************************************************/
   spi_tclMPlayClientHandler(ahl_tclBaseOneThreadApp* poMainApp);

   /***************************************************************************
   ** FUNCTION:  spi_tclMPlayClientHandler::spi_tclMPlayClientHandler(..)
   ***************************************************************************/
   /*!
   * \fn     spi_tclMPlayClientHandler(DiPO_tclMainApp* poMainApp,
   *            spi_tclDiPoConnection* poConnIntf)
   * \brief  Parameterized Constructor
   * \param  poMainApp : [IN] Pointer to main app.
   * \param  poConnIntf : [IN] Pointer to Dipo connection interface
   * \sa
   **************************************************************************/
   spi_tclMPlayClientHandler(ahl_tclBaseOneThreadApp* poMainApp, spi_tclDiPoConnection* poConnIntf);

   /***************************************************************************
   ** FUNCTION:  virtual spi_tclMPlayClientHandler::spi_tclMPlayClientHandler& ..
   ***************************************************************************/
   /*!
   * \fn      spi_tclMPlayClientHandler& operator=(const ..)
   * \brief   Assignment operator will not be implemented. So any attempt to
   *          use the same will cought by the compiler
   * \param   oClientHandler :[IN] reference to the client handler
   * \sa
   **************************************************************************/
   spi_tclMPlayClientHandler& operator=(
             const spi_tclMPlayClientHandler& rfoClientHandler);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMPlayClientHandler::vPopulateMessageContext(..
   ***************************************************************************/
   /*!
   * \fn     vPopulateMessageContext(trMsgContext &rMsgContext)
   * \brief  Function to populate the message context
   * \param  rMsgContext : [OUT] Message context
   **************************************************************************/
   t_Void vPopulateMessageContext(trMsgContext &rMsgContext) const;

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMPlayClientHandler::vPopulateDipoVersion(..
   ***************************************************************************/
   /*!
   * \fn     vPopulateDipoVersion(t_String szSource, t_U32 &u32XValue,....
   * \brief  Function to populate the DiPo version
   * \param  szSource : [IN] Source string from the device
   * \param  u32XValue : [OUT] Major version
   * \param  u32YValue : [OUT] minor version
   * \param  u32ZValue : [OUT] patch version
   **************************************************************************/
   t_Void vPopulateDipoVersion(t_String szSource, t_U32 &u32XValue,
      t_U32 &u32YValue, t_U32 &u32ZValue) const;

   /***************************************************************************
   ** FUNCTION:  tenAudioError spi_tclMPlayClientHandler::enGetAudioError(...)
   ***************************************************************************/
   /*!
   * \fn     enGetAudioError(const mplay_tFiAudError& rfcpAudioError)
   * \brief  Function to convert MPlay AudioError to SPI AudioError code
   * \param  rfcpAudioError : [IN] Mplay FI AudioError object
   **************************************************************************/
   tenAudioError enGetAudioError(const mplay_tFiAudError& rfcpAudioError) const;

    //! Main application pointer
   ahl_tclBaseOneThreadApp* m_poMainApp;

   //! Response interface endity
   // spi_tclRespInterface* m_poRespIntf;

   //! DiPo media metadata
   trAppMediaMetaData m_rAppMediaMetaData;

   //! DiPo  media playtime
   trAppMediaPlaytime m_rAppMediaPlaytime;

   // Phone Metadata
   trAppPhoneData m_rAppPhoneData;

   // DiPo Connection manager interface
   spi_tclDiPOConnectionIntf *m_poConnIntf;

   //! Hold the connected device list
   std::vector<trDeviceInfo> m_rDiPODeviceList;

   //!Device Tag value
   t_U32 m_u32DeviceTag;

   //! Role switch trigger status
   t_Bool m_bRoleSwitchTriggerStatus;

   //! Indicates if registration to Mediaplayer properties is required
   t_Bool m_bIsRegistrationReq;

   //! Device selection request status
   tenDeviceConnectionReq m_enDevConnReq;

   /*************************************************************************
   ****************************END OF PRIVATE*********************************
   *************************************************************************/

   /***************************************************************************
   * Message map definition macro
   ***************************************************************************/
   DECLARE_MSG_MAP(spi_tclMPlayClientHandler)
};

#endif // SPI_TCLMPLAY_CLIENT_HANDLER_H_
