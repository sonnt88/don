/*!
*******************************************************************************
* \file              spi_tclSensorDataClientHandler.cpp
* \brief             sensor Data Client handler class
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    sensor Data Client handler class 
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        |  Author                       | Modifications
04.6.2014   |  Vinoop U                     | Initial Version
16.6.2014   |  Ramya Murthy                 | Changes for using VDSensor data in Location info.
27.07.2015  |  SHITANSHU SHEKHAR            | Implemented vOnMSGyro3dGetHwInfoMethodResult(),
                                                vOnGyro3dData_UpdateStatus() function.
06.08.2015  |  SHITANSHU SHEKHAR            | Implemented vOnMSAcc3dGetHwInfoMethodResult(),
                                                vOnAllSensorAcc3dData_UpdateStatus() function.
03.12.2015  |  SHITANSHU SHEKHAR            | Implemented vOnAcc3dData_UpdateStatus()

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <ctime>
#include <cmath>

#include "spi_tclSensorDataClientHandler.h"
//@todo - Issue - bpstl ambiguous usage should be removed
#include "XFiObjHandler.h"
#include "FIMsgDispatch.h"
using namespace shl::msgHandler;

//Include common fi interface
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

#define GENERICMSGS_S_IMPORT_INTERFACE_GENERIC
#include "generic_msgs_if.h"

#define MATH_S_IMPORT_INTERFACE_GENERIC
#include "math_if.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DATASERVICE
#include "trcGenProj/Header/spi_tclSensorDataClientHandler.cpp.trc.h"
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

typedef XFiObjHandler<sensor_locationfi_tclMsgGnssDataStatus>
   spi_tXFiGnssDataStatus;
typedef XFiObjHandler<sensor_locationfi_tclMsgGyro3dData_UpdateStatus>
   spi_tXFiGyro3dData_UpdateStatus;
typedef XFiObjHandler<sensor_locationfi_tclMsgAcc3dData_UpdateStatus>
   spi_tXFiAcc3dData_UpdateStatus;

typedef XFiObjHandler<sensor_locationfi_tclMsgGyro3dGetHwInfoMethodResult>
   spi_tXFiGyro3dGetHwInfoMethodResult;
typedef XFiObjHandler<sensor_locationfi_tclMsgAcc3dGetHwInfoMethodResult>
   spi_tXFiAcc3dGetHwInfoMethodResult;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/******************************************************************************/
/*                                                                            */
/* CCA MESSAGE MAP                                                            */
/*                                                                            */
/******************************************************************************/

BEGIN_MSG_MAP(spi_tclSensorDataClientHandler, ahl_tclBaseWork)
   // Add your ON_MESSAGE_SVCDATA() macros here to define which corresponding 
   // method should be called on receiving a specific message.
   ON_MESSAGE_SVCDATA(SENSOR_LOCATIONFI_C_U16_GYRO3DGETHWINFO,
   AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnMSGyro3dGetHwInfoMethodResult)
   ON_MESSAGE_SVCDATA(SENSOR_LOCATIONFI_C_U16_ACC3DGETHWINFO,
   AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnMSAcc3dGetHwInfoMethodResult)

   ON_MESSAGE_SVCDATA(SENSOR_LOCATIONFI_C_U16_GNSSDATA,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnGnssDataStatus)
   ON_MESSAGE_SVCDATA(SENSOR_LOCATIONFI_C_U16_GYRO3DDATA_UPDATE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnGyro3dData_UpdateStatus)
   ON_MESSAGE_SVCDATA(SENSOR_LOCATIONFI_C_U16_ACC3DDATA_UPDATE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnAcc3dData_UpdateStatus)

END_MSG_MAP()


/******************************************************************************/
/*                                                                            */
/* METHODS                                                                    */
/*                                                                            */
/******************************************************************************/

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/*******************************************************************************
* FUNCTION: spi_tclSensorDataClientHandler::
*             spi_tclSensorDataClientHandler(ahl_tclBaseOneThreadApp* poMainAppl)
*******************************************************************************/
spi_tclSensorDataClientHandler::spi_tclSensorDataClientHandler(ahl_tclBaseOneThreadApp* poMainApp)
   : ahl_tclBaseOneThreadClientHandler(
      poMainApp,                                       /* Application Pointer          */
      CCA_C_U16_SRV_SENSOR_LOCATION,                   /* ID of used Service           */
      SENSOR_LOCATIONFI_C_U16_SERVICE_MAJORVERSION,    /* MajorVersion of used Service */
      SENSOR_LOCATIONFI_C_U16_SERVICE_MINORVERSION),   /* MinorVersion of used Service */
     m_poMainApp(poMainApp),
     m_bGyroEstimScaleFactor(false),
     m_bAccEstimScaleFactor(false)
{
   ETG_TRACE_USR1(("spi_tclSensorDataClientHandler::spi_tclSensorDataClientHandler entered "));
}

/*******************************************************************************
* FUNCTION: spi_tclSensorDataClientHandler::
*             ~spi_tclSensorDataClientHandler(t_Void)
*******************************************************************************/
spi_tclSensorDataClientHandler::
~spi_tclSensorDataClientHandler()
{
   ETG_TRACE_USR1(("spi_tclSensorDataClientHandler::~spi_tclSensorDataClientHandler entered "));
   m_poMainApp = NULL;
}

/*******************************************************************************
* FUNCTION: t_Void spi_tclSensorDataClientHandler::vOnServiceAvailable()
*******************************************************************************/
t_Void spi_tclSensorDataClientHandler::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("spi_tclSensorDataClientHandler::vOnServiceAvailable entered "));

   vInvokeMethodStart();
}

/*******************************************************************************
* FUNCTION: t_Void spi_tclSensorDataClientHandler::vOnServiceUnavailable(
*******************************************************************************/
t_Void spi_tclSensorDataClientHandler::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("spi_tclSensorDataClientHandler::vOnServiceUnavailable entered "));
}

/*******************************************************************************
* FUNCTION: t_Void spi_tclSensorDataClientHandler::vStartMethodStart()
*******************************************************************************/
t_Void spi_tclSensorDataClientHandler::vInvokeMethodStart()
{
   ETG_TRACE_USR1(("spi_tclSensorDataClientHandler::vInvokeMethodStart entered "));

   sensor_locationfi_tclMsgGyro3dGetHwInfoMethodStart oGyro3dGetHwInfoMethodStart;
   sensor_locationfi_tclMsgAcc3dGetHwInfoMethodStart oAcc3dGetHwInfoMethodStart;

   //! Create Msg context for gyro
   trMsgContext rMsgCtxt;
   rMsgCtxt.rUserContext.u32SrcAppID  = CCA_C_U16_APP_SMARTPHONEINTEGRATION;
   rMsgCtxt.rUserContext.u32DestAppID = u16GetServerAppID();
   rMsgCtxt.rUserContext.u32RegID     = u16GetRegID();
   rMsgCtxt.rUserContext.u32CmdCtr    = 0;

   //!Post MethodStart for Gyro
   FIMsgDispatch oMsgDispatcher(m_poMainApp);
   t_Bool bGyroSuccess =
      oMsgDispatcher.bSendMessage(oGyro3dGetHwInfoMethodStart, rMsgCtxt, SENSOR_LOCATIONFI_C_U16_SERVICE_MAJORVERSION);

   //! Create Msg context for acceleration
   trMsgContext rMsgCtxtAcc;
   rMsgCtxtAcc.rUserContext.u32SrcAppID  = CCA_C_U16_APP_SMARTPHONEINTEGRATION;
   rMsgCtxtAcc.rUserContext.u32DestAppID = u16GetServerAppID();
   rMsgCtxtAcc.rUserContext.u32RegID     = u16GetRegID();
   rMsgCtxtAcc.rUserContext.u32CmdCtr    = 0;

   //!Post MethodStart  for acceleration
   FIMsgDispatch oMsgDispatcherAcc(m_poMainApp);
   t_Bool bAccSuccess =
      oMsgDispatcherAcc.bSendMessage(oAcc3dGetHwInfoMethodStart, rMsgCtxtAcc, SENSOR_LOCATIONFI_C_U16_SERVICE_MAJORVERSION);

   ETG_TRACE_USR2(("[DESC]:spi_tclSensorDataClientHandler::vInvokeMethodStart() "
      "Gyro start method Status = %d, Acceleration start method Status = %d",
      ETG_ENUM(BOOL, bGyroSuccess), ETG_ENUM(BOOL, bAccSuccess)));
}

/***************************************************************************
* FUNCTION:  t_Void spi_tclSensorDataClientHandler::vRegisterForProperty()
***************************************************************************/
t_Void spi_tclSensorDataClientHandler::vRegisterForProperty(
   const trSensorDataCallbacks& rfcorSensorDataCb)
{
   //! Register subscribed properties
   vAddAutoRegisterForProperty(SENSOR_LOCATIONFI_C_U16_GNSSDATA);
   ETG_TRACE_USR2(("[DESC]:spi_tclSensorDataClientHandler:"
      "vRegisterForProperties: Registered FunctionID = 0x%x ",
      SENSOR_LOCATIONFI_C_U16_GNSSDATA));

   m_rSensorDataCallbacks = rfcorSensorDataCb;
}

/***************************************************************************
* FUNCTION:  t_Void spi_tclSensorDataClientHandler::vRegisterForGyroAccProperty()
***************************************************************************/
t_Void spi_tclSensorDataClientHandler::vRegisterForGyroAccProperty(
   const trSensorDataCallbacks& rfcorSensorDataCb)
{
   vAddAutoRegisterForProperty(SENSOR_LOCATIONFI_C_U16_GYRO3DDATA_UPDATE);
   vAddAutoRegisterForProperty(SENSOR_LOCATIONFI_C_U16_ACC3DDATA_UPDATE);
   ETG_TRACE_USR2(("[DESC]:spi_tclSensorDataClientHandler:"
      "vRegisterForGyroAccProperty: Registered FunctionID = 0x%x ",
      SENSOR_LOCATIONFI_C_U16_ACC3DDATA_UPDATE));
   m_rSensorDataCallbacks = rfcorSensorDataCb;
}

/***************************************************************************
* FUNCTION:  t_Void spi_tclSensorDataClientHandler::vUnregisterForProperty()
***************************************************************************/
t_Void spi_tclSensorDataClientHandler::vUnregisterForProperty()
{
   //! Unregister subscribed properties
   vRemoveAutoRegisterForProperty(SENSOR_LOCATIONFI_C_U16_GNSSDATA);
   ETG_TRACE_USR2(("[DESC]:spi_tclSensorDataClientHandler::"
      "vUnregisterForProperty: Registered FunctionID = 0x%x ",
      SENSOR_LOCATIONFI_C_U16_GNSSDATA));
}

/***************************************************************************
* FUNCTION:  t_Void spi_tclSensorDataClientHandler::vUnregisterForGyroAccProperty()
***************************************************************************/
t_Void spi_tclSensorDataClientHandler::vUnregisterForGyroAccProperty()
{
   vRemoveAutoRegisterForProperty(SENSOR_LOCATIONFI_C_U16_GYRO3DDATA_UPDATE);
   vRemoveAutoRegisterForProperty(SENSOR_LOCATIONFI_C_U16_ACC3DDATA_UPDATE);
   ETG_TRACE_USR2(("[DESC]:spi_tclSensorDataClientHandler::"
      "vUnregisterForGyroAccProperty: Registered FunctionID = 0x%x ",
      SENSOR_LOCATIONFI_C_U16_ACC3DDATA_UPDATE));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vOnMSAcc3dGetHwInfoMethodResult()
***************************************************************************/
t_Void spi_tclSensorDataClientHandler::vOnMSAcc3dGetHwInfoMethodResult(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclSensorDataClientHandler::vOnMSAcc3dGetHwInfoMethodResult entered "));
   spi_tXFiAcc3dGetHwInfoMethodResult oAcc3dGetHwInfo(*poMessage, SENSOR_LOCATIONFI_C_U16_SERVICE_MAJORVERSION);

   if (true == oAcc3dGetHwInfo.bIsValid())
   {
      m_bAccEstimScaleFactor = (  (abs(static_cast<t_S32>(oAcc3dGetHwInfo.Acc3dHwInfo.RAxes.EstimScaleFactor)) > EPS)
         && (abs(static_cast<t_S32>(oAcc3dGetHwInfo.Acc3dHwInfo.SAxes.EstimScaleFactor)) > EPS)
         && (abs(static_cast<t_S32>(oAcc3dGetHwInfo.Acc3dHwInfo.TAxes.EstimScaleFactor)) > EPS) );

      if(true == m_bAccEstimScaleFactor)
      {
         ETG_TRACE_USR4(("[PARAM]:Acc3d HW Info: MountAngleRX - %d, MountAngleSX - %d, MountAngleTX - %d",
                  oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngRX, oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngSX,
                  oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngTX));

         ETG_TRACE_USR4(("[PARAM]:Acc3d HW Info : MountAngleRY - %d, MountAngleSY - %d, MountAngleTY - %d",
                  oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngRY, oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngSY,
                  oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngTY));

         ETG_TRACE_USR4(("[PARAM]:Acc3d HW Info : MountAngleRZ - %d, MountAngleSZ - %d, MountAngleTZ - %d",
                  oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngRZ, oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngSZ,
                  oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngTZ));

         //! Calculate offset values based on the mounting angle
         m_rAccScaleAndOffset.dCosRX = cos((t_Double)oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngRX / 180.0 * 3.1415);
         m_rAccScaleAndOffset.dCosSX = cos((t_Double)oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngSX / 180.0 * 3.1415);
         m_rAccScaleAndOffset.dCosTX = cos((t_Double)oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngTX / 180.0 * 3.1415);

         m_rAccScaleAndOffset.dCosRY = cos((t_Double)oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngRY / 180.0 * 3.1415);
         m_rAccScaleAndOffset.dCosSY = cos((t_Double)oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngSY / 180.0 * 3.1415);
         m_rAccScaleAndOffset.dCosTY = cos((t_Double)oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngTY / 180.0 * 3.1415);

         m_rAccScaleAndOffset.dCosRZ = cos((t_Double)oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngRZ / 180.0 * 3.1415);
         m_rAccScaleAndOffset.dCosSZ = cos((t_Double)oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngSZ / 180.0 * 3.1415);
         m_rAccScaleAndOffset.dCosTZ = cos((t_Double)oAcc3dGetHwInfo.Acc3dHwInfo.MountAngles.AngTZ / 180.0 * 3.1415);

         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA,"[PARAM]:Acc3d HW Info : CosineRX - %lf, CosineSX - %lf, CosineTX - %lf",
                  m_rAccScaleAndOffset.dCosRX, m_rAccScaleAndOffset.dCosSX, m_rAccScaleAndOffset.dCosTX));

         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA,"[PARAM]:Acc3d HW Info : CosineRY - %lf, CosineSY - %lf, CosineTY - %lf",
                  m_rAccScaleAndOffset.dCosRY, m_rAccScaleAndOffset.dCosSY, m_rAccScaleAndOffset.dCosTY));

         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA,"[PARAM]:Acc3d HW Info : CosineRZ - %lf, CosineSZ - %lf, CosineTZ - %lf",
                  m_rAccScaleAndOffset.dCosRZ, m_rAccScaleAndOffset.dCosSZ, m_rAccScaleAndOffset.dCosTZ));

         //! Calculate the Offset values for XYZ coordinates based on the mounting angles and Estimated offset in RST coordinates
         m_rAccScaleAndOffset.fOffsetX = static_cast<t_Float>((m_rAccScaleAndOffset.dCosRX * oAcc3dGetHwInfo.Acc3dHwInfo.RAxes.EstimOffset)
                                           + (m_rAccScaleAndOffset.dCosSX * oAcc3dGetHwInfo.Acc3dHwInfo.SAxes.EstimOffset)
                                           + (m_rAccScaleAndOffset.dCosTX * oAcc3dGetHwInfo.Acc3dHwInfo.TAxes.EstimOffset));

         m_rAccScaleAndOffset.fOffsetY = static_cast<t_Float>((m_rAccScaleAndOffset.dCosRY * oAcc3dGetHwInfo.Acc3dHwInfo.RAxes.EstimOffset)
                                           + (m_rAccScaleAndOffset.dCosSY * oAcc3dGetHwInfo.Acc3dHwInfo.SAxes.EstimOffset)
                                           + (m_rAccScaleAndOffset.dCosTY * oAcc3dGetHwInfo.Acc3dHwInfo.TAxes.EstimOffset));

         m_rAccScaleAndOffset.fOffsetZ = static_cast<t_Float>((m_rAccScaleAndOffset.dCosRZ * oAcc3dGetHwInfo.Acc3dHwInfo.RAxes.EstimOffset)
                                           + (m_rAccScaleAndOffset.dCosSZ * oAcc3dGetHwInfo.Acc3dHwInfo.SAxes.EstimOffset)
                                           + (m_rAccScaleAndOffset.dCosTZ * oAcc3dGetHwInfo.Acc3dHwInfo.TAxes.EstimOffset));

         //! Calculate the scaling factor based on the mounting angles and the provided estimated scale factor for RST coordinates
         m_rAccScaleAndOffset.fScaleX = static_cast<t_Float>((m_rAccScaleAndOffset.dCosRX * oAcc3dGetHwInfo.Acc3dHwInfo.RAxes.EstimScaleFactor)
                                          + (m_rAccScaleAndOffset.dCosSX * oAcc3dGetHwInfo.Acc3dHwInfo.SAxes.EstimScaleFactor)
                                          + (m_rAccScaleAndOffset.dCosTX * oAcc3dGetHwInfo.Acc3dHwInfo.TAxes.EstimScaleFactor));

         m_rAccScaleAndOffset.fScaleY = static_cast<t_Float>((m_rAccScaleAndOffset.dCosRY * oAcc3dGetHwInfo.Acc3dHwInfo.RAxes.EstimScaleFactor)
                                          + (m_rAccScaleAndOffset.dCosSY * oAcc3dGetHwInfo.Acc3dHwInfo.SAxes.EstimScaleFactor)
                                          + (m_rAccScaleAndOffset.dCosTY * oAcc3dGetHwInfo.Acc3dHwInfo.TAxes.EstimScaleFactor));

         m_rAccScaleAndOffset.fScaleZ = static_cast<t_Float>((m_rAccScaleAndOffset.dCosRZ * oAcc3dGetHwInfo.Acc3dHwInfo.RAxes.EstimScaleFactor)
                                          + (m_rAccScaleAndOffset.dCosSZ * oAcc3dGetHwInfo.Acc3dHwInfo.SAxes.EstimScaleFactor)
                                          + (m_rAccScaleAndOffset.dCosTZ * oAcc3dGetHwInfo.Acc3dHwInfo.TAxes.EstimScaleFactor));

         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA,"[PARAM]:Acc3d HW Info : OffsetX - %f, OffsetY - %f, OffsetZ - %f",
                  m_rAccScaleAndOffset.fOffsetX, m_rAccScaleAndOffset.fOffsetY, m_rAccScaleAndOffset.fOffsetZ));

         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA,"[PARAM]:Acc3d HW Info : ScaleX - %f, ScaleY - %f, ScaleZ - %f",
                  m_rAccScaleAndOffset.fScaleX, m_rAccScaleAndOffset.fScaleY, m_rAccScaleAndOffset.fScaleZ));
      }//End of if(true == m_bAccEstimScaleFactor)
   }//End of if (true == oAcc3dGetHwInfo.bIsValid())
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclSensorDataClientHandler::vOnMSAcc3dGetHwInfoMethodResult: Message extraction failed! "));
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vOnAcc3dData_UpdateStatus()
***************************************************************************/
t_Void spi_tclSensorDataClientHandler::vOnAcc3dData_UpdateStatus(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclSensorDataClientHandler::vOnAcc3dData_UpdateStatus entered "
         "estimscalefactor status = %d ", ETG_ENUM(BOOL, m_bAccEstimScaleFactor)));

   spi_tXFiAcc3dData_UpdateStatus oAcc3dData_update(*poMessage, SENSOR_LOCATIONFI_C_U16_SERVICE_MAJORVERSION);

   if ((true == oAcc3dData_update.bIsValid()) && (true == m_bAccEstimScaleFactor))
   {
      std::vector<trAccSensorData> vecrAccSensorData;

      ETG_TRACE_USR4(("[PARAM]:spi_tclSensorDataClientHandler::vOnAcc3dData_UpdateStatus Acc list size = %d ",
            oAcc3dData_update.Acc3dData.size()));

      for (tU16 u16Index = 0 ; u16Index < oAcc3dData_update.Acc3dData.size() ; ++u16Index)
      {
         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA, "[PARAM]:vOnAcc3dData_UpdateStatus:"
               " RVal = %d, SVal = %d, TVal = %d", oAcc3dData_update.Acc3dData[u16Index].RVal,
               oAcc3dData_update.Acc3dData[u16Index].SVal,
               oAcc3dData_update.Acc3dData[u16Index].TVal));

         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA, "[PARAM]:vOnAcc3dData_UpdateStatus:"
               " RStatus = %d, SStatus = %d, TStatus = %d", oAcc3dData_update.Acc3dData[u16Index].RStatus.enType,
               oAcc3dData_update.Acc3dData[u16Index].SStatus.enType,
               oAcc3dData_update.Acc3dData[u16Index].TStatus.enType));

        //check for acc status of R,S,T coordinate
         t_Bool bAccNormal = ((sensor_fi_tcl_e16_AccStatus::FI_EN_ACCSTATE_CONNECTED_NORMAL
                  == (oAcc3dData_update.Acc3dData[u16Index].RStatus.enType))
                  && (sensor_fi_tcl_e16_AccStatus::FI_EN_ACCSTATE_CONNECTED_NORMAL
                           == (oAcc3dData_update.Acc3dData[u16Index].SStatus.enType))
                  && (sensor_fi_tcl_e16_AccStatus::FI_EN_ACCSTATE_CONNECTED_NORMAL
                           == (oAcc3dData_update.Acc3dData[u16Index].TStatus.enType)));

         if(true == bAccNormal)
         {
            t_Float fAccValueZ = 0.0;
            t_Float fAccValueY = 0.0;
            t_Float fAccValueX = 0.0;

            //Deriving x,y,z coordinate value from adc values of acc r-coordinate
            fAccValueX += (m_rAccScaleAndOffset.dCosRX * (oAcc3dData_update.Acc3dData[u16Index].RVal));
            fAccValueY += (m_rAccScaleAndOffset.dCosRY * (oAcc3dData_update.Acc3dData[u16Index].RVal));
            fAccValueZ += (m_rAccScaleAndOffset.dCosRZ * (oAcc3dData_update.Acc3dData[u16Index].RVal));

            //Deriving x,y,z coordinate value from adc values of acc s-coordinate
            fAccValueX += (m_rAccScaleAndOffset.dCosSX * (oAcc3dData_update.Acc3dData[u16Index].SVal));
            fAccValueY += (m_rAccScaleAndOffset.dCosSY * (oAcc3dData_update.Acc3dData[u16Index].SVal));
            fAccValueZ += (m_rAccScaleAndOffset.dCosSZ * (oAcc3dData_update.Acc3dData[u16Index].SVal));

            //Deriving x,y,z coordinate value from adc values of acc t-coordinate
            fAccValueX += (m_rAccScaleAndOffset.dCosTX * (oAcc3dData_update.Acc3dData[u16Index].TVal));
            fAccValueY += (m_rAccScaleAndOffset.dCosTY * (oAcc3dData_update.Acc3dData[u16Index].TVal));
            fAccValueZ += (m_rAccScaleAndOffset.dCosTZ * (oAcc3dData_update.Acc3dData[u16Index].TVal));

            //! Apply offset correction for calculated values
            t_Float fAccRelativeValueX = (fAccValueX - m_rAccScaleAndOffset.fOffsetX);
            t_Float fAccRelativeValueY = (fAccValueY - m_rAccScaleAndOffset.fOffsetY);
            t_Float fAccRelativeValueZ = (fAccValueZ - m_rAccScaleAndOffset.fOffsetZ);

            //! Convert the values to m/s2 by using the scale factor
            trAccSensorData rAccSensorData;
            rAccSensorData.fAccX = fAccRelativeValueX / m_rAccScaleAndOffset.fScaleX;
            rAccSensorData.fAccY = fAccRelativeValueY / m_rAccScaleAndOffset.fScaleY;
            rAccSensorData.fAccZ = fAccRelativeValueZ / m_rAccScaleAndOffset.fScaleZ;

            vecrAccSensorData.push_back(rAccSensorData);

            ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA, "[PARAM]:vOnAcc3dData_UpdateStatus: Accelerometer Values after offset "
                  " X Axis = %f, Y Axis = %f, Z Axis = %f", fAccValueX, fAccValueY, fAccValueZ));
            ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA, "[PARAM]:vOnAcc3dData_UpdateStatus: Accelerometer Values in m/s2 after scaling"
                  " X Axis = %f, Y Axis = %f, Z Axis = %f", rAccSensorData.fAccX, rAccSensorData.fAccY, rAccSensorData.fAccZ));
         }
      }

      //@TODO - usage of C++11 gives lint error - compress lint
      //! Forward Sensor data to subscriber
      if (NULL != m_rSensorDataCallbacks.fvOnAccSensorData)
      {
         m_rSensorDataCallbacks.fvOnAccSensorData(vecrAccSensorData);
      }
   }
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclSensorDataClientHandler::vOnAcc3dData_UpdateStatus: Message extraction failed! "));
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vOnMSGyro3dGetHwInfoMethodResult()
***************************************************************************/
t_Void spi_tclSensorDataClientHandler::vOnMSGyro3dGetHwInfoMethodResult(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclSensorDataClientHandler::vOnMSGyro3dGetHwInfoMethodResult entered "));
   spi_tXFiGyro3dGetHwInfoMethodResult oGyro3dGetHwInfo(*poMessage, SENSOR_LOCATIONFI_C_U16_SERVICE_MAJORVERSION);
   if (true == oGyro3dGetHwInfo.bIsValid())
   {
      m_bGyroEstimScaleFactor = ((abs(static_cast<t_S32>(oGyro3dGetHwInfo.Gyro3dHwInfo.RAxes.EstimScaleFactor)) > EPS)
         && (abs(static_cast<t_S32>(oGyro3dGetHwInfo.Gyro3dHwInfo.SAxes.EstimScaleFactor)) > EPS)
         && (abs(static_cast<t_S32>(oGyro3dGetHwInfo.Gyro3dHwInfo.TAxes.EstimScaleFactor)) > EPS) );

      if (true == m_bGyroEstimScaleFactor)
      {
         ETG_TRACE_USR4(("[PARAM]:Gyro3d HW Info: MountAngleRX - %d, MountAngleSX - %d, MountAngleTX - %d",
                  oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngRX, oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngSX,
                  oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngTX));

         ETG_TRACE_USR4(("[PARAM]:Gyro3d HW Info : MountAngleRY - %d, MountAngleSY - %d, MountAngleTY - %d",
                  oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngRY, oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngSY,
                  oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngTY));

         ETG_TRACE_USR4(("[PARAM]:Gyro3d HW Info : MountAngleRZ - %d, MountAngleSZ - %d, MountAngleTZ - %d",
                  oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngRZ, oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngSZ,
                  oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngTZ));

         //! Calculate offset values based on the mounting angle for gyro sensor
         m_rGyroScaleAndOffset.dCosRX = cos((t_Double)oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngRX / 180.0 * 3.1415);
         m_rGyroScaleAndOffset.dCosSX = cos((t_Double)oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngSX / 180.0 * 3.1415);
         m_rGyroScaleAndOffset.dCosTX = cos((t_Double)oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngTX / 180.0 * 3.1415);

         m_rGyroScaleAndOffset.dCosRY = cos((t_Double)oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngRY / 180.0 * 3.1415);
         m_rGyroScaleAndOffset.dCosSY = cos((t_Double)oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngSY / 180.0 * 3.1415);
         m_rGyroScaleAndOffset.dCosTY = cos((t_Double)oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngTY / 180.0 * 3.1415);

         m_rGyroScaleAndOffset.dCosRZ = cos((t_Double)oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngRZ / 180.0 * 3.1415);
         m_rGyroScaleAndOffset.dCosSZ = cos((t_Double)oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngSZ / 180.0 * 3.1415);
         m_rGyroScaleAndOffset.dCosTZ = cos((t_Double)oGyro3dGetHwInfo.Gyro3dHwInfo.MountAngles.AngTZ / 180.0 * 3.1415);

         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA,"[PARAM]:Gyro3d HW Info : CosineRX - %lf, CosineSX - %lf, CosineTX - %lf",
                  m_rGyroScaleAndOffset.dCosRX, m_rGyroScaleAndOffset.dCosSX, m_rGyroScaleAndOffset.dCosTX));

         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA,"[PARAM]:Gyro3d HW Info : CosineRY - %lf, CosineSY - %lf, CosineTY - %lf",
                  m_rGyroScaleAndOffset.dCosRY, m_rGyroScaleAndOffset.dCosSY, m_rGyroScaleAndOffset.dCosTY));

         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA,"[PARAM]:Gyro3d HW Info : CosineRZ - %lf, CosineSZ - %lf, CosineTZ - %lf",
                  m_rGyroScaleAndOffset.dCosRZ, m_rGyroScaleAndOffset.dCosSZ, m_rGyroScaleAndOffset.dCosTZ));

         //! Calculate the Offset values for XYZ coordinates based on the mounting angles and Estimated offset in RST coordinates
         m_rGyroScaleAndOffset.fOffsetX = static_cast<t_Float>((m_rGyroScaleAndOffset.dCosRX * oGyro3dGetHwInfo.Gyro3dHwInfo.RAxes.EstimOffset)
                                           + (m_rGyroScaleAndOffset.dCosSX * oGyro3dGetHwInfo.Gyro3dHwInfo.SAxes.EstimOffset)
                                           + (m_rGyroScaleAndOffset.dCosTX * oGyro3dGetHwInfo.Gyro3dHwInfo.TAxes.EstimOffset));

         m_rGyroScaleAndOffset.fOffsetY = static_cast<t_Float>((m_rGyroScaleAndOffset.dCosRY * oGyro3dGetHwInfo.Gyro3dHwInfo.RAxes.EstimOffset)
                                           + (m_rGyroScaleAndOffset.dCosSY * oGyro3dGetHwInfo.Gyro3dHwInfo.SAxes.EstimOffset)
                                           + (m_rGyroScaleAndOffset.dCosTY * oGyro3dGetHwInfo.Gyro3dHwInfo.TAxes.EstimOffset));

         m_rGyroScaleAndOffset.fOffsetZ = static_cast<t_Float>((m_rGyroScaleAndOffset.dCosRZ * oGyro3dGetHwInfo.Gyro3dHwInfo.RAxes.EstimOffset)
                                           + (m_rGyroScaleAndOffset.dCosSZ * oGyro3dGetHwInfo.Gyro3dHwInfo.SAxes.EstimOffset)
                                           + (m_rGyroScaleAndOffset.dCosTZ * oGyro3dGetHwInfo.Gyro3dHwInfo.TAxes.EstimOffset));

         //! Calculate the scaling factor based on the mounting angles and the provided estimated scale factor for RST coordinates
         m_rGyroScaleAndOffset.fScaleX = static_cast<t_Float>((m_rGyroScaleAndOffset.dCosRX * oGyro3dGetHwInfo.Gyro3dHwInfo.RAxes.EstimScaleFactor)
                                          + (m_rGyroScaleAndOffset.dCosSX * oGyro3dGetHwInfo.Gyro3dHwInfo.SAxes.EstimScaleFactor)
                                          + (m_rGyroScaleAndOffset.dCosTX * oGyro3dGetHwInfo.Gyro3dHwInfo.TAxes.EstimScaleFactor));

         m_rGyroScaleAndOffset.fScaleY = static_cast<t_Float>((m_rGyroScaleAndOffset.dCosRY * oGyro3dGetHwInfo.Gyro3dHwInfo.RAxes.EstimScaleFactor)
                                          + (m_rGyroScaleAndOffset.dCosSY * oGyro3dGetHwInfo.Gyro3dHwInfo.SAxes.EstimScaleFactor)
                                          + (m_rGyroScaleAndOffset.dCosTY * oGyro3dGetHwInfo.Gyro3dHwInfo.TAxes.EstimScaleFactor));

         m_rGyroScaleAndOffset.fScaleZ = static_cast<t_Float>((m_rGyroScaleAndOffset.dCosRZ * oGyro3dGetHwInfo.Gyro3dHwInfo.RAxes.EstimScaleFactor)
                                          + (m_rGyroScaleAndOffset.dCosSZ * oGyro3dGetHwInfo.Gyro3dHwInfo.SAxes.EstimScaleFactor)
                                          + (m_rGyroScaleAndOffset.dCosTZ * oGyro3dGetHwInfo.Gyro3dHwInfo.TAxes.EstimScaleFactor));

         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA,"[PARAM]:Gyro3d HW Info : OffsetX - %f, OffsetY - %f, OffsetZ - %f",
                  m_rGyroScaleAndOffset.fOffsetX, m_rGyroScaleAndOffset.fOffsetY, m_rGyroScaleAndOffset.fOffsetZ));

         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA,"[PARAM]:Gyro3d HW Info : ScaleX - %f, ScaleY - %f, ScaleZ - %f",
                  m_rGyroScaleAndOffset.fScaleX, m_rGyroScaleAndOffset.fScaleY, m_rGyroScaleAndOffset.fScaleZ));
      }//End of if(true == m_bGyroEstimScaleFactor)
   }//End of if (true == oGyro3dGetHwInfo.bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclSensorDataClientHandler::vOnMSGyro3dGetHwInfoMethodResult: Message extraction failed! "));
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vOnGyro3dData_UpdateStatus()
***************************************************************************/
t_Void spi_tclSensorDataClientHandler::vOnGyro3dData_UpdateStatus(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclSensorDataClientHandler::vOnGyro3dData_UpdateStatus entered "
         "Estimscalefactor statusfor Gyro sensor = %d ", m_bGyroEstimScaleFactor));

   spi_tXFiGyro3dData_UpdateStatus oGyro3dData_Update(*poMessage, SENSOR_LOCATIONFI_C_U16_SERVICE_MAJORVERSION);

   if ((true == oGyro3dData_Update.bIsValid()) && (true == m_bGyroEstimScaleFactor))
   {
      std::vector<trGyroSensorData> vecrGyroSensorData;

      ETG_TRACE_USR4(("[PARAM]:vOnGyro3dData_UpdateStatus: Gyro list size = %d ",
             oGyro3dData_Update.Gyro3dData.size()));

      for (tU16 u16Index = 0 ; u16Index < oGyro3dData_Update.Gyro3dData.size() ; ++u16Index)
      {
        ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA, "[PARAM]:vOnGyro3dData_UpdateStatus: Gyro values received in RST coordinates"
         " RVal = %d, SVal = %d, TVal = %d", oGyro3dData_Update.Gyro3dData[u16Index].RVal,
         oGyro3dData_Update.Gyro3dData[u16Index].SVal, oGyro3dData_Update.Gyro3dData[u16Index].TVal));

        ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA, "[PARAM]:vOnGyro3dData_UpdateStatus:Gyro status received for RST coordinates"
         " RStatus = %d, SStatus = %d, TStatus = %d", oGyro3dData_Update.Gyro3dData[u16Index].RStatus.enType,
         oGyro3dData_Update.Gyro3dData[u16Index].SStatus.enType, oGyro3dData_Update.Gyro3dData[u16Index].TStatus.enType));

        //check for gyrostatus of R,S,T coordinate
         t_Bool bGyroNormal = ((sensor_fi_tcl_e16_GyroStatus::FI_EN_GYROSTATE_CONNECTED_NORMAL
                  == (oGyro3dData_Update.Gyro3dData[u16Index].RStatus.enType))
                  && (sensor_fi_tcl_e16_GyroStatus::FI_EN_GYROSTATE_CONNECTED_NORMAL
                           == (oGyro3dData_Update.Gyro3dData[u16Index].SStatus.enType))
                  && (sensor_fi_tcl_e16_GyroStatus::FI_EN_GYROSTATE_CONNECTED_NORMAL
                           == (oGyro3dData_Update.Gyro3dData[u16Index].TStatus.enType)));

        if (true == bGyroNormal)
         {
            t_Float gyroValueZ = 0.0;
            t_Float gyroValueY = 0.0;
            t_Float gyroValueX = 0.0;

            //Deriving x,y,z coordinate value from adc values of gyro r-coordinate
            gyroValueX += (m_rGyroScaleAndOffset.dCosRX * (oGyro3dData_Update.Gyro3dData[u16Index].RVal));
            gyroValueY += (m_rGyroScaleAndOffset.dCosRY * (oGyro3dData_Update.Gyro3dData[u16Index].RVal));
            gyroValueZ += (m_rGyroScaleAndOffset.dCosRZ * (oGyro3dData_Update.Gyro3dData[u16Index].RVal));

            //Deriving x,y,z coordinate value from adc values of gyro s-coordinate
            gyroValueX += (m_rGyroScaleAndOffset.dCosSX * (oGyro3dData_Update.Gyro3dData[u16Index].SVal));
            gyroValueY += (m_rGyroScaleAndOffset.dCosSY * (oGyro3dData_Update.Gyro3dData[u16Index].SVal));
            gyroValueZ += (m_rGyroScaleAndOffset.dCosSZ * (oGyro3dData_Update.Gyro3dData[u16Index].SVal));

            //Deriving x,y,z coordinate value from adc values of gyro t-coordinate
            gyroValueX += (m_rGyroScaleAndOffset.dCosTX * (oGyro3dData_Update.Gyro3dData[u16Index].TVal));
            gyroValueY += (m_rGyroScaleAndOffset.dCosTY * (oGyro3dData_Update.Gyro3dData[u16Index].TVal));
            gyroValueZ += (m_rGyroScaleAndOffset.dCosTZ * (oGyro3dData_Update.Gyro3dData[u16Index].TVal));

            //! Apply offset correction for calculated values
            t_Float fGyroRelativeValueX = (gyroValueX - m_rGyroScaleAndOffset.fOffsetX);
            t_Float fGyroRelativeValueY = (gyroValueY - m_rGyroScaleAndOffset.fOffsetY);
            t_Float fGyroRelativeValueZ = (gyroValueZ - m_rGyroScaleAndOffset.fOffsetZ);


            //! Convert the values to rad/s by using the scale factor
            trGyroSensorData rGyroSensorData;
            rGyroSensorData.fGyroX = fGyroRelativeValueX / m_rGyroScaleAndOffset.fScaleX;
            rGyroSensorData.fGyroY = fGyroRelativeValueY / m_rGyroScaleAndOffset.fScaleY;
            rGyroSensorData.fGyroZ = fGyroRelativeValueZ / m_rGyroScaleAndOffset.fScaleZ;

            vecrGyroSensorData.push_back(rGyroSensorData);

           ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA, "[PARAM]:vOnGyro3dData_UpdateStatus:Gyro Values after Calc-"
            " gyroValueZ = %f, gyroValueY = %f, gyroValueX = %f", gyroValueZ,
            gyroValueY, gyroValueX));
           ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA, "[PARAM]:vOnGyro3dData_UpdateStatus:Gyro Values after correction-"
            " gyroValueZ = %f, gyroValueY = %f, gyroValueX = %f", rGyroSensorData.fGyroZ,
            rGyroSensorData.fGyroY, rGyroSensorData.fGyroX));
        }
      }

      //@TODO - usage of C++11 gives lint error - compress lint
      //! Forward Sensor data to subscriber
      if (NULL != m_rSensorDataCallbacks.fvOnGyroSensorData)
      {
         (m_rSensorDataCallbacks.fvOnGyroSensorData)(vecrGyroSensorData);
      }
   }
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclSensorDataClientHandler::vOnGyro3dData_UpdateStatus: Message extraction failed! "));
   }
}

/*******************************************************************************
** FUNCTION:   spi_tclSensorDataClientHandler::vOnGnssDataStatus(amt_tclServiceData...
*******************************************************************************/
t_Void spi_tclSensorDataClientHandler::vOnGnssDataStatus(amt_tclServiceData* poMessage)
{
   spi_tXFiGnssDataStatus oGnssDataStatus (*poMessage, SENSOR_LOCATIONFI_C_U16_SERVICE_MAJORVERSION);

   /*lint -esym(40,fvOnSensorData)fvOnSensorData Undeclared identifier */
   if (true == oGnssDataStatus.bIsValid())
   {
      //trSensorData rGnssData;

      m_rSensorData.bGeoidalSepAvailable = true;
      m_rSensorData.bHDOPAvailable = true;
      m_rSensorData.bNumSatUsedAvailable = true;

      m_rSensorData.u32TimeStamp = oGnssDataStatus.GnssData.Timestamp;
      m_rSensorData.PosixTime = s32GetPosixTime(oGnssDataStatus.GnssData.GnssPvtData.UtcTimeDate);
      m_rSensorData.enGnssQuality = static_cast<tenGnssQuality>(
         oGnssDataStatus.GnssData.GnssPvtData.GnssStatus.GnssQuality.enType);
      m_rSensorData.enGnssMode = static_cast<tenGnssMode>(
         oGnssDataStatus.GnssData.GnssPvtData.GnssStatus.GnssMode.enType);
      m_rSensorData.u16SatellitesUsed = oGnssDataStatus.GnssData.GnssPvtData.SatellitesUsed;
      m_rSensorData.dHDOP = oGnssDataStatus.GnssData.GnssPvtData.HDOP;
      m_rSensorData.dGeoidalSeparation = oGnssDataStatus.GnssData.GnssPvtData.GeoidalSeparation;
      m_rSensorData.fAccuracy = static_cast<t_Float> (sqrt((t_Double)oGnssDataStatus.GnssData.GnssPvtData.PositionCovarianceMatrix.Elem0
         +
         (t_Double)oGnssDataStatus.GnssData.GnssPvtData.PositionCovarianceMatrix.Elem5));

      t_Float fSpeed = 0;
      fSpeed = (oGnssDataStatus.GnssData.GnssPvtData.VelocityEast * oGnssDataStatus.GnssData.GnssPvtData.VelocityEast);
      fSpeed += (oGnssDataStatus.GnssData.GnssPvtData.VelocityNorth * oGnssDataStatus.GnssData.GnssPvtData.VelocityNorth);
      m_rSensorData.fSpeed = static_cast<t_Float> (sqrt((t_Double)fSpeed));

      // Calculate heading from VelocityNorth and VelocityEast
      t_Float fHeading = 0.0;
      if ((fabs((t_Double)oGnssDataStatus.GnssData.GnssPvtData.VelocityEast) >= 0.01) ||
         (fabs((t_Double)oGnssDataStatus.GnssData.GnssPvtData.VelocityNorth) >= 0.01))
      {
         fHeading = static_cast<t_Float>(
            atan2(static_cast<t_Double>(oGnssDataStatus.GnssData.GnssPvtData.VelocityEast),
            static_cast<t_Double>(oGnssDataStatus.GnssData.GnssPvtData.VelocityNorth))
            );
      } // else heading can't be determined
      m_rSensorData.fHeading = fHeading;   // Heading is received in radian

      m_rSensorData.dLatitude = static_cast<t_Double>(oGnssDataStatus.GnssData.GnssPvtData.Latitude);     // Latitude is received in radian
      m_rSensorData.dLongitude = static_cast<t_Double>(oGnssDataStatus.GnssData.GnssPvtData.Longitude);   // Longitude is received in radian
      m_rSensorData.fAltitude = oGnssDataStatus.GnssData.GnssPvtData.AltitudeWGS84;

      //! Forward Sensor data to subscriber
      if (NULL != m_rSensorDataCallbacks.fvOnSensorData)
      {
         m_rSensorDataCallbacks.fvOnSensorData(m_rSensorData);
      }
   } //if (true == oGnssDataStatus.bIsValid())
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclSensorDataClientHandler::vOnGnssDataStatus: Message extraction failed! "));
   }
}

/*******************************************************************************
** FUNCTION:   t_PosixTime spi_tclSensorDataClientHandler::s32GetPosixTime(const...
*******************************************************************************/
t_PosixTime spi_tclSensorDataClientHandler::s32GetPosixTime(
   const t_FiTimeDateInfo& rfcoTimeDateInfo)
{
   struct tm rTimeDate;
   rTimeDate.tm_hour = rfcoTimeDateInfo.td_hour;
   rTimeDate.tm_min = rfcoTimeDateInfo.td_minute;
   rTimeDate.tm_sec = rfcoTimeDateInfo.td_second;
   rTimeDate.tm_mday = rfcoTimeDateInfo.td_day;
   rTimeDate.tm_mon = (1 <= rfcoTimeDateInfo.td_month) ? (rfcoTimeDateInfo.td_month - 1) : rfcoTimeDateInfo.td_month;
   rTimeDate.tm_year = (1900 <= rfcoTimeDateInfo.td_year) ? (rfcoTimeDateInfo.td_year - 1900) : rfcoTimeDateInfo.td_year;
   //SPI_NORMAL_ASSERT((1 > rfcoTimeDateInfo.td_month) || (1900 > rfcoTimeDateInfo.td_year));

   ETG_TRACE_USR4(("[PARAM]:spi_tclSensorDataClientHandler::s32GetPosixTime: "
      "td_hour = %u, td_minute = %u, td_second = %u, "
      "td_day = %u, td_month = %u, td_year = %u ",
      rfcoTimeDateInfo.td_hour, rfcoTimeDateInfo.td_minute, rfcoTimeDateInfo.td_second,
      rfcoTimeDateInfo.td_day, rfcoTimeDateInfo.td_month, rfcoTimeDateInfo.td_year));

   return (t_S32)mktime(&rTimeDate);
}

//lint –restore

