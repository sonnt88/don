/*!
*******************************************************************************
* \file              spi_tclSensorDataClientHandler.h
* \brief             sensor Data Client handler class
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    sensor Data Client handler class 
COPYRIGHT:      &copy; RBEI

HISTORY:
Date      |  Author                      | Modifications
04.6.2014 |  Vinoop U                    | Initial Version
16.6.2014 |  Ramya Murthy                | Changes for using VDSensor data in Location info.

\endverbatim
******************************************************************************/

#ifndef _SPI_TCL_CLIENTHANDLER_SENSORDATA_H_
#define _SPI_TCL_CLIENTHANDLER_SENSORDATA_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

#define SENSOR_FI_S_IMPORT_INTERFACE_SENSOR_LOCATIONFI_TYPES
#define SENSOR_FI_S_IMPORT_INTERFACE_SENSOR_LOCATIONFI_ERRORCODES
#define SENSOR_FI_S_IMPORT_INTERFACE_SENSOR_LOCATIONFI_FUNCTIONIDS
#define SENSOR_FI_S_IMPORT_INTERFACE_SENSOR_LOCATIONFI_SERVICEINFO
#include "sensor_fi_if.h"

#include "SPITypes.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/


typedef sensor_fi_tcl_TimeDate  t_FiTimeDateInfo;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

struct trSensorScaleAndOffset
{
      t_Float fOffsetX;
      t_Float fOffsetY;
      t_Float fOffsetZ;

      t_Float fScaleX;
      t_Float fScaleY;
      t_Float fScaleZ;

      t_Double dCosRX;
      t_Double dCosSX;
      t_Double dCosTX;

      t_Double dCosRY;
      t_Double dCosSY;
      t_Double dCosTY;

      t_Double dCosRZ;
      t_Double dCosSZ;
      t_Double dCosTZ;

      trSensorScaleAndOffset():
         fOffsetX(0.0),fOffsetY(0.0),fOffsetZ(0.0),
         fScaleX(0.0),fScaleY(0.0),fScaleZ(0.0),
         dCosRX(0),dCosSX(0),dCosTX(0),
         dCosRY(0),dCosSY(0),dCosTY(0),
         dCosRZ(0),dCosSZ(0),dCosTZ(0){}
};

/**
* \brief   SENSOR_FI Client handler class
*/
class spi_tclSensorDataClientHandler 
   : public ahl_tclBaseOneThreadClientHandler
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclSensorDataClientHandler::spi_tclSensorDataClientHandler(..)
   ***************************************************************************/
   /*!
   * \fn     spi_tclSensorDataClientHandler(ahl_tclBaseOneThreadApp* poMainApp)
   * \brief  Parametrized Constructor
   * \param  poMainApp : [IN] Pointer to main app.
   * \sa
   **************************************************************************/
   spi_tclSensorDataClientHandler(ahl_tclBaseOneThreadApp* poMainApp);

   /***************************************************************************
   ** FUNCTION:  virtual spi_tclSensorDataClientHandler::~spi_tclSensorDataClientHandler()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclSensorDataClientHandler()
   * \brief   Destructor
   * \sa
   **************************************************************************/
   virtual ~spi_tclSensorDataClientHandler();

   /***************************************************************************
   * Application specific methods.
   ***************************************************************************/

   /**************************************************************************
   ** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vProcessTimer(tU16 u...
   **************************************************************************/
   /*!
   * \fn      vProcessTimer(tU16 u16TimerId)
   * \brief   This method is called from the vOnTimer() method of this
   *          CCA application on the expiration of a previously via method
   *          bStartTimer() started timer.
   * \param   u16TimerId : [IN]
   **************************************************************************/
   t_Void vProcessTimer(t_U16 u16TimerId);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vOnServiceAvailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceAvailable()
   * \brief   This function is called by the framework if the service of our server
   *         becomes available, e.g. server has been started.
   * \sa
   **************************************************************************/
   virtual t_Void vOnServiceAvailable();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vOnServiceUnavailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceUnavailable()
   * \brief   This function is called by the framework if the service of our server
   *         becomes unavailable, e.g. server has been shut down.
   * \sa
   **************************************************************************/
   virtual t_Void vOnServiceUnavailable();

   /***************************************************************************
   ** Handler function declarations used by message map.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vRegisterForProperty()
   ***************************************************************************/
   /*!
   * \fn      vRegisterForProperty(const trSensorDataCallbacks& rfcorSensorDataCb)
   * \brief   This function is called for Property registration 
   * \param   rfcorSensorDataCb: [IN] Structure containing callbacks to subscriber,
   *             to be called when subscribed data is received by client handler.
   * \sa
   **************************************************************************/
   virtual t_Void vRegisterForProperty(const trSensorDataCallbacks& rfcorSensorDataCb);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vRegisterForGyroAccProperty()
   ***************************************************************************/
   /*!
   * \fn      vRegisterForGyroAccProperty(const trSensorDataCallbacks& rfcorSensorDataCb)
   * \brief   This function is called for Property registration
   * \param   rfcorSensorDataCb: [IN] Structure containing callbacks to subscriber,
   *             to be called when subscribed data is received by client handler.
   * \sa
   **************************************************************************/
   virtual t_Void vRegisterForGyroAccProperty(const trSensorDataCallbacks& rfcorSensorDataCb);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vUnregisterForProperty()
   ***************************************************************************/
   /*!
   * \fn      vUnregisterForProperty()
   * \brief   This function is called for Property unregistration 
   * \sa
   **************************************************************************/
   virtual t_Void vUnregisterForProperty();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vUnregisterForGyroAccProperty()
   ***************************************************************************/
   /*!
   * \fn      vUnregisterForGyroAccProperty()
   * \brief   This function is called for Property unregistration
   * \sa
   **************************************************************************/
   virtual t_Void vUnregisterForGyroAccProperty();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vInvokeMethodStart()
   ***************************************************************************/
   /*!
   * \fn      vInvokeMethodStart()
   * \brief   This method is used to get Gyro3dGetHwInfo.
   * \param   None
   * \sa
   **************************************************************************/
   t_Void vInvokeMethodStart();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vOnMSAcc3dGetHwInfoMethodResult()
   ***************************************************************************/
   /*!
   * \fn      vOnMSAcc3dGetHwInfoMethodResult(amt_tclServiceData* poMessage)
   * \brief   This Property represents Acc3dGetHwInfo.
   * \param   poMessage: [IN] Pointer to message
   * \sa
   **************************************************************************/
   t_Void vOnMSAcc3dGetHwInfoMethodResult(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vOnAcc3dData_UpdateStatus()
   ***************************************************************************/
   /*!
   * \fn      vOnAcc3dData_UpdateStatus(amt_tclServiceData* poMessage)
   * \brief   This Property represents acceleration data message.
   * \param   poMessage: [IN] Pointer to message
   * \sa
   **************************************************************************/
   t_Void vOnAcc3dData_UpdateStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vOnAllSensorAcc3dData_UpdateStatus()
   ***************************************************************************/
   /*!
   * \fn      vOnAllSensorAcc3dData_UpdateStatus(amt_tclServiceData* poMessage)
   * \brief   This Property represents acceleration data message.
   * \param   poMessage: [IN] Pointer to message
   * \sa
   **************************************************************************/
   t_Void vOnAllSensorAcc3dData_UpdateStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vOnMSGyro3dGetHwInfoMethodResult()
   ***************************************************************************/
   /*!
   * \fn      vOnMSGyro3dGetHwInfoMethodResult(amt_tclServiceData* poMessage)
   * \brief   This Property represents Gyro3dGetHwInfo.
   * \param   poMessage: [IN] Pointer to message
   * \sa
   **************************************************************************/
   t_Void vOnMSGyro3dGetHwInfoMethodResult(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vOnGyro3dData_UpdateStatus()
   ***************************************************************************/
   /*!
   * \fn      vOnGyro3dData_UpdateStatus(amt_tclServiceData* poMessage)
   * \brief   This Property represents gyro data message.
   * \param   poMessage: [IN] Pointer to message
   * \sa
   **************************************************************************/
   t_Void vOnGyro3dData_UpdateStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSensorDataClientHandler::vOnGnssDataStatus()
   ***************************************************************************/
   /*!
   * \fn      vOnGnssDataStatus(amt_tclServiceData* poMessage)
   * \brief   This Property represents periodical GNSS-data-message.
   * \param   poMessage: [IN] Pointer to GNSS message
   * \sa
   **************************************************************************/
   t_Void vOnGnssDataStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   * Message map definition macro
   ***************************************************************************/
   DECLARE_MSG_MAP(spi_tclSensorDataClientHandler)

   /*************************************************************************
   ****************************END OF PUBLIC*********************************
   *************************************************************************/

private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /*******************************************************************************
   ** FUNCTION:   t_PosixTime spi_tclSensorDataClientHandler::s32GetPosixTime(const...
   *******************************************************************************/
   /*!
   * \fn      s32GetPosixTime(const t_FiTimeDateInfo& rfcoTimeDateInfo)
   * \brief   Converts time date structure from Sensor FI to Posix time.
   * \param   rfcoTimeDateInfo: [IN] Pointer to GNSS message
   * \sa
   **************************************************************************/
   t_PosixTime s32GetPosixTime(const t_FiTimeDateInfo& rfcoTimeDateInfo);

   /***************************************************************************
   ** FUNCTION:  spi_tclSensorDataClientHandler::spi_tclSensorDataClientHandler()
   ***************************************************************************/
   /*!
   * \fn     spi_tclSensorDataClientHandler()
   * \brief  Default Constructor, will not be implemented.
   **************************************************************************/
   spi_tclSensorDataClientHandler();

   /**************************************************************************
   * Assingment Operater, will not be implemented.
   * Avoids Lint Prio 3 warning: Info 1732: new in constructor for class
   * 'spi_tclSensorDataClientHandler' which has no assignment operator.
   * NOTE: This is a technique to disable the assignment operator for this
   * class. So if an attempt for the assignment is made compiler complains.
   **************************************************************************/
   spi_tclSensorDataClientHandler& operator=(const spi_tclSensorDataClientHandler &oClientHandler);

   /***************************************************************************
   *! Data members
   ***************************************************************************/

   /*
    * Main application pointer
    */
   ahl_tclBaseOneThreadApp*   m_poMainApp;
   /*
    * Structure containing callbacks to SENSOR_FI data subscriber.
    */
   trSensorDataCallbacks      m_rSensorDataCallbacks;

   trSensorScaleAndOffset m_rGyroScaleAndOffset;

   trSensorScaleAndOffset m_rAccScaleAndOffset;

   trSensorData m_rSensorData;

   t_Bool m_bGyroEstimScaleFactor;

   t_Bool m_bAccEstimScaleFactor;

   /*************************************************************************
   ****************************END OF PRIVATE********************************
   *************************************************************************/

};

#endif // spi_tclSensorDataClientHandler
