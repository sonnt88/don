/*
 * spi_tclAudioBusClient.cpp
 *
 *  Created on: Feb 5, 2015
 *      Author: tch5kor
 */

#include <spi_tclAudioDBusClient.h>
//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclAudioDBusClient.cpp.trc.h"
#endif
#endif

static GAsyncQueue *proxy_call_queue;
static t_S32 pipe_fd[2];
static GIOChannel *pipe_ch;

/******************************************************************
Function Name: proxy_call_queue_push

Description:
Push a proxy call argument structure into the queue of proxy calls.
This may be called from any thread.  The proxy call will be issued
from the glib main loop thread.

Arguments:
In: call_args - pointer to a DBusGProxyCallArgs struct
*******************************************************************/
void proxy_call_queue_push(DBusGProxyCallArgs * call_args)
{
	ETG_TRACE_USR1(("proxy_call_queue_push Entered"));
    g_async_queue_push(proxy_call_queue, call_args);
    write_to_proxy_call_pipe('A');
}

/***************************************************************************
 ** FUNCTION:  write_to_proxy_call_pipe
 ***************************************************************************/
static t_S32 write_to_proxy_call_pipe(t_Char val) {

	/*lint -esym(40,EAGAIN)EAGAIN Undeclared identifier */
	/*lint -esym(40,errno)errno Undeclared identifier */
	ETG_TRACE_USR1(("write_to_proxy_call_pipe Entered "));
	t_S32 S32WriteResult;
	do {
		S32WriteResult = write(pipe_fd[1], &val, 1);
	} while (S32WriteResult < 0 && errno == EAGAIN);
	/*
	 * The pipe should never fill up, but if it does we can ignore the error
	 * since we only care if the pipe has data ready to read or not.
	 */
	if (S32WriteResult < 0)
		return S32WriteResult;
	else
		return 0;
}

/******************************************************************
Function Name: initialize_proxy_call_queue

Description:
Create the queue for dbus proxy calls required by the dbus-glib
bindings.

Returns: Non-zero on failure, or zero on success.
   *******************************************************************/
t_S32 initialize_proxy_call_queue()
  {
     /* create the queue */
	ETG_TRACE_USR1((" initialize_proxy_call_queue entered"));
     proxy_call_queue = g_async_queue_new();
     if (proxy_call_queue == NULL)
     {
        ETG_TRACE_ERR(("Failed to create the dbus proxy call queue.   "));
        return -1;
     }

     /* open the pipe */
     if (pipe(&pipe_fd[0]) < 0)
     {
        ETG_TRACE_ERR(("Failed to create the dbus proxy call pipe.   "));
        finalize_proxy_call_queue();
        return -1;
     }
     if ((fcntl(pipe_fd[0], F_SETFL, O_NONBLOCK) < 0)
           || (fcntl(pipe_fd[1], F_SETFL, O_NONBLOCK) < 0))
     {
        ETG_TRACE_ERR(("Failed to set nonblock flag for the dbus proxy call pipe.   "));
        finalize_proxy_call_queue();
        return -1;
     }

     /* bind the read descriptor of the pipe to a GIOChannel */
     pipe_ch = g_io_channel_unix_new(pipe_fd[0]);
     if (pipe_ch == NULL)
     {
        ETG_TRACE_ERR(("Failed to create GIOChannel for dbus proxy call pipe.   "));
        finalize_proxy_call_queue();
        return -1;
     }
     if (g_io_channel_set_encoding(pipe_ch, NULL, NULL)
           != G_IO_STATUS_NORMAL)
     {
        ETG_TRACE_ERR(("Failed to set encoding for dbus proxy call pipe GIOChannel.   "));
        finalize_proxy_call_queue();
        return -1;
     }

     /*
      * Add a watch on the GIOChannel to wakeup the main loop when data is
      * available to read.
      */
     g_io_add_watch(pipe_ch,
           (GIOCondition) (G_IO_IN | G_IO_HUP | G_IO_ERR),
           process_proxy_call_queue, NULL);

     return 0;
  }

/******************************************************************
Function Name: finalize_proxy_call_queue

Description:
Destroy the queue for dbus proxy calls required by the dbus-glib
bindings.
 *******************************************************************/
t_Void finalize_proxy_call_queue()
{
	ETG_TRACE_USR1((" finalize_proxy_call_queue ENTER "));
   /* free the GIOChannel */
   if (pipe_ch)
   {
      g_io_channel_unref(pipe_ch);
      pipe_ch = NULL;
   }

   /* close the pipe */
   if (pipe_fd[0])
      close(pipe_fd[0]);
   if (pipe_fd[1])
      close(pipe_fd[1]);
   pipe_fd[0] = pipe_fd[1] = 0;

   /* free the queue */
   if (proxy_call_queue)
   {
      g_async_queue_unref(proxy_call_queue);
      proxy_call_queue = NULL;
   }
}

static gboolean process_proxy_call_queue(GIOChannel * source,
      GIOCondition condition,
      gpointer userdata)
{
	ETG_TRACE_USR1(("process_proxy_call_queue ENTER %d",pthread_self()));

   const gsize buf_size = 32;
   GIOStatus status;
   gchar buf[buf_size];
   gsize bytes_read = buf_size;
   DBusGProxyCallArgs *call_args;

   (void) userdata;

   if (condition & G_IO_HUP)
   {
      ETG_TRACE_ERR(("Dbus proxy call queue broken pipe."));
      return false;
   }
   else if (condition & G_IO_ERR)
   {
      ETG_TRACE_ERR(("Dbus proxy call queue pipe I/O error."));
      return false;
   }
   else if (!(condition & G_IO_IN))
   {
      ETG_TRACE_ERR(("Dbus proxy call queue: nothing to read from pipe."));
   }

   /* Drain the pipe fifo.  We don't care about the data. */
   while (bytes_read == buf_size)
   {
      status = g_io_channel_read_chars(source, buf, buf_size, &bytes_read, NULL);
   }

   while ((call_args =
         (DBusGProxyCallArgs *) g_async_queue_try_pop(proxy_call_queue)) != NULL)
   {

         if(call_args->proxy != NULL && call_args->method != NULL)
            dbus_g_proxy_begin_call_with_marshaled_args(call_args);


      dbus_g_proxy_call_args_free(call_args);
   }
   return true;
}

