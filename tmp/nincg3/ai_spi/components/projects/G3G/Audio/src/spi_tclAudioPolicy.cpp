/*!
*******************************************************************************
* \file             spi_tclAudioPolicy.cpp
* \brief            Gen3 Audio Policy class for PSA 
*******************************************************************************
\verbatim
PROJECT:        Gen3 Projects
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Gen3 Audio Policy class for PSA 
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                                  | Modifications
29.10.2013 |  Hari Priya E R(RBEI/ECP2)               | Initial Version
02.12.2016 |  Ramya Murthy(RBEI/ECP2)                 | Improvements to release audio resource early, 
                                                        taken from GM (Fix for SUZUKI-23851)

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include <ahl_if.h>

#include "spi_tclCmdInterface.h"
#include "spi_tclAudioPolicy.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclAudioPolicy.cpp.trc.h"
#endif

/******************************************************************************
 | defines
 |----------------------------------------------------------------------------*/
#define CHAR_SET_UTF8  (midw_fi_tclString::FI_EN_UTF8)
#define SRCACT_ON      (midw_fi_tcl_e8_SrcActivity::FI_EN_ON)
#define SRCACT_OFF     (midw_fi_tcl_e8_SrcActivity::FI_EN_OFF)
#define SRCACT_PAUSE   (midw_fi_tcl_e8_SrcActivity::FI_EN_PAUSE)

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/
static const t_U8 scou8Shift3Bytes = 24;
static const t_U8 scou8Shift2Bytes = 16;

//! Stores Projection device speech app state
static tenSpeechAppState senDeviceSpeechAppState = e8SPI_SPEECH_UNKNOWN;

/***************************************************************************
** FUNCTION:  spi_tclAudioPolicy::spi_tclAudioPolicy
(spi_tclCmdInterface* poSpiCmdIntf,ahl_tclBaseOneThreadApp* poMainApp);
***************************************************************************/
spi_tclAudioPolicy::spi_tclAudioPolicy(spi_tclCmdInterface* poSpiCmdIntf, ahl_tclBaseOneThreadApp* poMainApp):
      arl_tclISource((ahl_tclBaseOneThreadApp*) poMainApp),
      m_poSpiCmdIntf(poSpiCmdIntf),
      m_bSpeechChnRequested(false)
{
   ETG_TRACE_USR1(("spi_tclAudioPolicy::spi_tclAudioPolicy"));
   SPI_NORMAL_ASSERT((NULL == poMainApp) || (NULL == m_poSpiCmdIntf));
}

/***************************************************************************
** FUNCTION:  spi_tclAudioPolicy::~spi_tclAudioPolicy();
***************************************************************************/
spi_tclAudioPolicy::~spi_tclAudioPolicy()
{
   ETG_TRACE_USR1(("spi_tclAudioPolicy::~spi_tclAudioPolicy"));
   m_poSpiCmdIntf = NULL;
}

/***************************************************************************
 ** FUNCTION:  bool spi_tclAudioPolicy::bRequestAudioActivation(tU8)
 ***************************************************************************/
tBool spi_tclAudioPolicy::bRequestAudioActivation(tU8 u8SourceNum)
{
   ETG_TRACE_USR1(("spi_tclAudioPolicy::bRequestAudioActivation entered for SrcNum %d ", u8SourceNum));

   tBool bRetVal = false;
   arl_tenSource enSrcNum = static_cast<arl_tenSource> (u8SourceNum);

   //! Update Device Speech app state
   senDeviceSpeechAppState = (ARL_SRC_SPI_VR == enSrcNum) ? (e8SPI_SPEECH_SPEAKING) : senDeviceSpeechAppState;

   m_oSpeechChnReqLock.s16Lock();
   m_bSpeechChnRequested = (ARL_SRC_SPI_VR == enSrcNum) ? (true) : m_bSpeechChnRequested;
   m_oSpeechChnReqLock.vUnlock();

   if (ARL_SRC_SPI_MAIN == enSrcNum)
   {
     bRetVal = bSetSrcAvailability(u8SourceNum, true);
   }
   bRetVal = bSetAudioRouteRequest(enSrcNum, ARL_EN_ISRC_ACT_ON);

   return bRetVal;

}//spi_tclAudioPolicy::bRequestAudioActivation(tU8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  bool spi_tclAudioPolicy::bRequestAudioDeactivation(tU8)
 ***************************************************************************/
tBool spi_tclAudioPolicy::bRequestAudioDeactivation(tU8 u8SourceNum)
{
   ETG_TRACE_USR1(("spi_tclAudioPolicy::bRequestAudioDeactivation entered for SrcNum %d ", u8SourceNum));

   arl_tenSource enSrcNum = static_cast<arl_tenSource> (u8SourceNum);

   //! Update Device Speech app state
   senDeviceSpeechAppState = (ARL_SRC_SPI_VR == enSrcNum) ? (e8SPI_SPEECH_END) : (senDeviceSpeechAppState);

   tBool bRetVal = bSetAudioRouteRequest(enSrcNum, ARL_EN_ISRC_ACT_OFF);

   return bRetVal;

}//spi_tclAudioPolicy::bRequestAudioDeactivation(tU8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  bool spi_tclAudioPolicy::bPauseAudioActivity(tU8)
 ***************************************************************************/
tBool spi_tclAudioPolicy::bPauseAudioActivity(tU8 u8SourceNum)
{
   SPI_INTENTIONALLY_UNUSED(u8SourceNum);
   tBool bRetVal = true;
   ETG_TRACE_USR1((" bPauseAudioActivity entered %d\n", u8SourceNum));

   return bRetVal;

}//spi_tclAudioPolicy::bPauseAudioActivity(tU8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  void spi_tclAudioPolicy::vStartSourceActivityResult(tU8,tBool)
 ***************************************************************************/
void spi_tclAudioPolicy::vStartSourceActivityResult(tU8 u8SourceNum,
      tBool bError)
{
   SPI_INTENTIONALLY_UNUSED(bError);
   ETG_TRACE_USR1((" vStartSourceActivityResult entered %d %d\n", u8SourceNum, bError));
   arl_tSrcActivity oSrcActivity;

   arl_tenSource enSrcNum = static_cast<arl_tenSource> (u8SourceNum);
   oSrcActivity.enType = SRCACT_ON;

   //arl_tclISource::vSourceActivityResult(enSrcNum, rfcoSrcActivity);
   vSourceActivityResult(enSrcNum, oSrcActivity);

}//spi_tclAudioPolicy::vStartSourceActivityResult(tU8 tBool)

/***************************************************************************
 ** FUNCTION:  void spi_tclAudioPolicy::vStopSourceActivityResult(tU8,tBool)
 ***************************************************************************/
void spi_tclAudioPolicy::vStopSourceActivityResult(tU8 u8SourceNum,
      tBool bError)
{
   SPI_INTENTIONALLY_UNUSED(bError);
   ETG_TRACE_USR1((" vStopSourceActivityResult entered %d %d\n", u8SourceNum, bError));

   arl_tSrcActivity oSrcActivity;

   arl_tenSource enSrcNum = static_cast<arl_tenSource> (u8SourceNum);
   oSrcActivity.enType = SRCACT_OFF;

   //arl_tclISource::vSourceActivityResult(enSrcNum, rfcoSrcActivity);
   vSourceActivityResult(enSrcNum, oSrcActivity);

}//spi_tclAudioPolicy::vStopSourceActivityResult(tU8 tBool)

/***************************************************************************
 ** FUNCTION:  void spi_tclAudioPolicy::vPauseSourceActivityResult(tU8,tBool)
 ***************************************************************************/
void spi_tclAudioPolicy::vPauseSourceActivityResult(tU8 u8SourceNum,
      tBool bError)
{
   SPI_INTENTIONALLY_UNUSED(bError);
   ETG_TRACE_USR1((" vPauseSourceActivityResult entered %d %d\n", u8SourceNum, bError));

   arl_tSrcActivity oSrcActivity;

   arl_tenSource enSrcNum = static_cast<arl_tenSource> (u8SourceNum);
   oSrcActivity.enType = SRCACT_PAUSE;

   //arl_tclISource::vSourceActivityResult(enSrcNum, rfcoSrcActivity);
   vSourceActivityResult(enSrcNum, oSrcActivity);

}//spi_tclAudioPolicy::vPauseSourceActivityResult(tU8 tBool)

/***************************************************************************
 ** FUNCTION:  tVoid spi_tclAudioPolicy::bSetSourceAvailability(tU8,tBool)
 ***************************************************************************/
tBool spi_tclAudioPolicy::bSetSrcAvailability(tU8 u8SourceNum, tBool bAvail)
{
   tBool bRetVal = true;
   ETG_TRACE_USR1((" bSetSrcAvailability entered u8SourceNum = %d, bAvail = %d \n", u8SourceNum, bAvail));

   /*Trigger ARL function.*/
   arl_tenSrcAvailability enSrcAvailability = (true == bAvail)
         ? ARL_EN_SRC_PLAYABLE : ARL_EN_SRC_NOT_AVAILABLE;
   arl_tenAvailabilityReason enAvailabilityReason = (true == bAvail)
         ? ARL_EN_REASON_NEWMEDIA : ARL_EN_REASON_NOMEDIA;
		 
   /*Sub Source ID(Param 4) is 0 which is the default value.*/
   bSetSourceAvailability(enSrcAvailability, enAvailabilityReason,
         static_cast<arl_tenSource> (u8SourceNum));

   return bRetVal;

}//spi_tclAudioPolicy::bSetSourceAvailability(tU8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  tVoid spi_tclAudioPolicy::vSetServiceAvailable()
 ***************************************************************************/
t_Void spi_tclAudioPolicy::vSetServiceAvailable(tBool bAvail)
{
   ETG_TRACE_USR1((" vSetServiceAvailable entered  bAvail = %d \n", bAvail));
   vSetSrcAvailable(bAvail);
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclAudioPolicy::bSetAudioDucking()
***************************************************************************/
t_Bool spi_tclAudioPolicy::bSetAudioDucking(const t_U8 cou8SrcNum, const t_U16 cou16RampDuration,
      const t_U8 cou8VolumeindB,
      const tenDuckingType coenDuckingType)
{
   SPI_INTENTIONALLY_UNUSED(cou8SrcNum);

   t_U32 u32DuckingType = coenDuckingType;
   t_U32 u32VolumeLevel = cou8VolumeindB;
   //! Populate ducking value where the first two byte (from LSB) represents Ramp duration
   //! third byte represents Volume level in dB, fourth byte represents Ducking Type
   t_U32 u32DuckingVal = (u32DuckingType << scou8Shift3Bytes) | (u32VolumeLevel
            << scou8Shift2Bytes) | (cou16RampDuration);

   t_Bool bRetVal = bSetAudioProperty(ARL_SRC_SPI_MIX,
         ARL_SRC_IPOD_DUCKING, u32DuckingVal);
   ETG_TRACE_USR1((" spi_tclAudioPolicy::bSetAudioDucking u32DuckingVal = 0x%x, "
         "SetAudioProperty result = %d \n", u32DuckingVal, ETG_ENUM(BOOL, bRetVal)));

   return true;
}

/***************************************************************************
 ** FUNCTION:  tVoid spi_tclAudioPolicy::vOnRequestAudioRoute(tU8)
 ***************************************************************************/
tBool spi_tclAudioPolicy::bSetSourceMute(tU8 u8SourceNum)
{
   SPI_INTENTIONALLY_UNUSED(u8SourceNum);
   tBool bRetVal = true;
   ETG_TRACE_USR1((" bSetSourceMute entered \n"));

   return bRetVal;

}//spi_tclAudioPolicy::bSetSourceMute(tU8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  tVoid spi_tclAudioPolicy::bSetSourceDemute(tU8)
 ***************************************************************************/
tBool spi_tclAudioPolicy::bSetSourceDemute(tU8 u8SourceNum)
{
   SPI_INTENTIONALLY_UNUSED(u8SourceNum);
   tBool bRetVal = true;
   ETG_TRACE_USR1(("bSetSourceDemute entered \n"));

   return bRetVal;

}//spi_tclAudioPolicy::bSetSourceDemute(tU8 u8SourceNum)


/***************************************************************************
** FUNCTION:  tBool spi_tclAudioPolicy::bOnSrcActivity(
arl_tenSource enSrcNum, const arl_tSrcActivity& rfcoSrcActivity)
 ***************************************************************************/
tBool spi_tclAudioPolicy::bOnSrcActivity(arl_tenSource enSrcNum,
      const arl_tSrcActivity& rfcoSrcActivity)
{
   ETG_TRACE_USR1((" bOnSrcActivity entered %d %d\n", enSrcNum, rfcoSrcActivity.enType));

   tU8 u8SrcNum = static_cast<tU8> (enSrcNum);
   tBool bRetVal = (NULL != m_poSpiCmdIntf);
   arl_tenActivity enSrcActivity = ARL_EN_ISRC_ACT_OFF;

   m_oSpeechChnReqLock.s16Lock();
   m_bSpeechChnRequested = (ARL_SRC_SPI_VR == enSrcNum)? (false) : m_bSpeechChnRequested;
   t_Bool bIsSpeechChnRequested = m_bSpeechChnRequested;
   m_oSpeechChnReqLock.vUnlock();

   // Pointer NULL check kept for LINT Compliance
   if (NULL != m_poSpiCmdIntf)
   {
      switch (rfcoSrcActivity.enType)
      {
         case SRCACT_ON:
         {
            ETG_TRACE_USR4((" Source Activity ON received for Src Num: %d", u8SrcNum));
            m_poSpiCmdIntf->vOnStartSourceActivity(u8SrcNum);
            enSrcActivity = ARL_EN_ISRC_ACT_ON;
         }
            break;

         case SRCACT_PAUSE:
         {
            ETG_TRACE_USR4((" Source Activity PAUSE received for Src Num: %d", u8SrcNum));
            m_poSpiCmdIntf->vOnStopSourceActivity(u8SrcNum);
            enSrcActivity = ARL_EN_ISRC_ACT_PAUSE;
         }
            break;

         case SRCACT_OFF:
         {
            ETG_TRACE_USR4((" Source Activity OFF received for Src Num: %d", u8SrcNum));
            m_poSpiCmdIntf->vOnStopSourceActivity(u8SrcNum);
            enSrcActivity = ARL_EN_ISRC_ACT_OFF;
         }
            break;

         default:
         {
            ETG_TRACE_USR4((" Unknown Source Activity received for Src Num: %d", u8SrcNum));
            bRetVal = false;
         }
            break;

      }//switch(rfcoSrcActivity.enType)

      t_U32 u32SelDevHandle = m_poSpiCmdIntf->u32GetSelectedDeviceHandle();

      //! Set audio context to MAIN if HU has taken the main audio, else to INVALID.
      //! (will be set to correct state once AudioContext update is received)
      //! Note: SPI should take/borrow audio focus to MD asap, to stop it from streaming music.
      t_Bool bIsAudioPausedorStopped =
            ((ARL_EN_ISRC_ACT_OFF == enSrcActivity) || (ARL_EN_ISRC_ACT_PAUSE == enSrcActivity));
      if ((true == bIsAudioPausedorStopped) && (ARL_SRC_SPI_MAIN == enSrcNum) &&
            (e8SPI_SPEECH_SPEAKING != senDeviceSpeechAppState) && (false == bIsSpeechChnRequested))
      {
         tenAudioContext enAudioContext = (ARL_EN_ISRC_ACT_OFF == enSrcActivity) ?
               (e8SPI_AUDIO_MAIN) : (e8SPI_AUDIO_UNKNOWN);
         m_poSpiCmdIntf->vSetAccessoryAudioContext(u32SelDevHandle,
               enAudioContext, true, corEmptyUsrContext, e8DEV_TYPE_ANDROIDAUTO);
      }

      //! Take the audio resource if CarPlay media is active, so that audio device is released asap.
      t_Bool bIsCarPlayMainAudioStopped =
            (e8DEV_TYPE_DIPO == m_poSpiCmdIntf->enGetDeviceCategory(u32SelDevHandle)) &&
            (ARL_SRC_SPI_MAIN == enSrcNum) && (ARL_EN_ISRC_ACT_OFF == enSrcActivity);
      if (true == bIsCarPlayMainAudioStopped)
      {
         m_poSpiCmdIntf->vSetAccessoryAudioContext(u32SelDevHandle,
                  e8SPI_AUDIO_MAIN, true, corEmptyUsrContext, e8DEV_TYPE_DIPO);
      }
   }//if(NULL != m_poSpiCmdIntf)

   //! when Audio source switches from carplay media to FM, application receives deallocate
   //! and source activity OFF but the entry in ARL table is still ON. This has to be cleared
   //! to prevent restoration of carplay media source.
   //! Default FM source should be allocated when CarPlay deivce is unplugged. Therefore
   //! maintain the "Route Request" property status to latest always.(SUZUKI-23850)
   if (ARL_SRC_SPI_MAIN == enSrcNum)
   {
      ETG_TRACE_USR4((" Audio Route Request %d is sent for ARL_SRC_SPI_MAIN \n", enSrcActivity));
      bSetAudioRouteRequest(enSrcNum, enSrcActivity);
   }

   return bRetVal;

} //spi_tclAudioPolicy::bOnSrcActivityStart(arl_tenSource,const arl_tSrcActivity&)

/***************************************************************************
** FUNCTION:  tBool spi_tclAudioPolicy::bOnAllocate(arl_tenSource enSrcNum,
                        const arl_tAllocRouteResult& rfcoAllocRoute)
***************************************************************************/
tBool spi_tclAudioPolicy::bOnAllocate(arl_tenSource enSrcNum,
                             const arl_tAllocRouteResult& rfcoAllocRoute)
{
   ETG_TRACE_USR1((" bOnAllocate entered %d\n", enSrcNum));
   tBool bRetVal = true;
   trAudSrcInfo rSrcInfo;

   if(NULL != m_poSpiCmdIntf)
   {
      tU8 u8Src = static_cast<tU8> (enSrcNum);

      t_U8 u8InListSize = rfcoAllocRoute.listInputDev.strALSADev.size();
      t_U8 u8OutListSize = rfcoAllocRoute.listOutputDev.strALSADev.size();

      ETG_TRACE_USR4((" Audio Device List Size: Input [%d], Output [%d]", u8InListSize, u8OutListSize)); 

      /* In case of Main Audio and Mix Audio source , we get only one ALSA device names.*/
      if ((1 == u8OutListSize)&& (0 == u8InListSize))
      {
         GET_STRINGDATA_FROM_FI_STRINGOBJ((rfcoAllocRoute.listOutputDev.strALSADev.front()), CHAR_SET_UTF8, rSrcInfo.rMainAudDevNames.szOutputDev);
         ETG_TRACE_USR4(("bOnAllocate: ALSAOutDev : %s \n", rSrcInfo.rMainAudDevNames.szOutputDev.c_str()));
      }
      /* In case of Phone Audio and VR Audio source , we get only four ALSA device names(2-ECNR and 2- Main Audio).*/
      else if((3 == u8OutListSize) && (3 == u8InListSize))
      {
         GET_STRINGDATA_FROM_FI_STRINGOBJ((rfcoAllocRoute.listInputDev.strALSADev.at(0)), CHAR_SET_UTF8, rSrcInfo.rMainAudDevNames.szInputDev);
         ETG_TRACE_USR4(("bOnAllocate: ALSAInDev : %s ", rSrcInfo.rMainAudDevNames.szInputDev.c_str()));

         GET_STRINGDATA_FROM_FI_STRINGOBJ((rfcoAllocRoute.listInputDev.strALSADev.at(1)), CHAR_SET_UTF8, rSrcInfo.rEcnrAudDevNames.szInputDev);
         ETG_TRACE_USR4(("bOnAllocate: ECNR ALSAInDev : %s ", rSrcInfo.rEcnrAudDevNames.szInputDev.c_str()));

         GET_STRINGDATA_FROM_FI_STRINGOBJ((rfcoAllocRoute.listOutputDev.strALSADev.at(1)), CHAR_SET_UTF8, rSrcInfo.rEcnrAudDevNames.szOutputDev);
         ETG_TRACE_USR4(("bOnAllocate: ECNR ALSAOutDev : %s ", rSrcInfo.rEcnrAudDevNames.szOutputDev.c_str()));

         GET_STRINGDATA_FROM_FI_STRINGOBJ((rfcoAllocRoute.listOutputDev.strALSADev.at(2)), CHAR_SET_UTF8, rSrcInfo.rMainAudDevNames.szOutputDev);
         ETG_TRACE_USR4(("bOnAllocate: ALSAOutDev : %s ", rSrcInfo.rMainAudDevNames.szOutputDev.c_str()));
      }
      else
      {
         ETG_TRACE_ERR(("bOnAllocate: Invalid ALSA Device Names\n"));
         bRetVal = false;
      }
      bRetVal = (true == bRetVal)? m_poSpiCmdIntf->bOnRouteAllocateResult(u8Src, rSrcInfo) : bRetVal;
   }//if(NULL != m_poSpiCmdIntf)
   return bRetVal;
}

/************************************************************************************
 ** FUNCTION:  tBool spi_tclAudioPolicy::bOnDeAllocate(arl_tenSource enSrcNum)
 *************************************************************************************/
tBool spi_tclAudioPolicy::bOnDeAllocate(arl_tenSource enSrcNum)
{
   ETG_TRACE_USR1((" bOnDeAllocate entered %d\n", enSrcNum));

   tBool bRetVal = (NULL != m_poSpiCmdIntf);

   if (NULL != m_poSpiCmdIntf)
   {
      tU8 u8Src = static_cast<tU8> (enSrcNum);
      m_poSpiCmdIntf->vOnRouteDeAllocateResult(u8Src);

   }//if(NULL != m_poSpiCmdIntf)

   return bRetVal;
}


/***************************************************************************
** FUNCTION:  tVoid spi_tclAudioPolicy::vOnError()
 ***************************************************************************/
tVoid spi_tclAudioPolicy::vOnError(tU8 u8SrcNum, arl_tenISourceError cenError)
{
   tenAudioError enAudiError = e8_AUDIOERROR_UNKNOWN;

   if (NULL != m_poSpiCmdIntf)
   {
      switch (cenError)
      {
         case ARL_EN_ISRC_REQAVACT_ERR:
         {
            enAudiError = e8_AUDIOERROR_AVACTIVATION;
         }
            break;

         case ARL_EN_ISRC_REQAVDEACT_ERR:
         {
            enAudiError = e8_AUDIOERROR_AVDEACTIVATION;
         }
            break;

         case ARL_EN_ISRC_ALLOC_ERR:
         {
            enAudiError = e8_AUDIOERROR_ALLOCATE;
         }
            break;

         case ARL_EN_ISRC_DEALLOC_ERR:
         {
            enAudiError = e8_AUDIOERROR_DEALLOCATE;
         }
            break;

         case ARL_EN_ISRC_SRCACT_ON_ERR:
         {
            enAudiError = e8_AUDIOERROR_STARTSOURCEACT;
         }
            break;

         case ARL_EN_ISRC_SRCACT_OFF_ERR:
         {
            enAudiError = e8_AUDIOERROR_STOPSOURCEACT;
         }
            break;

         default:
         {
            enAudiError = e8_AUDIOERROR_GENERIC;
         }
            break;
      }//switch(cenError)

      ETG_TRACE_USR4(("spi_tclAudioPolicy::vOnError: Audio Error for Source Number %d Error = %d",
               u8SrcNum, enAudiError));

      //! Store audio source as inactive in case of activation errors
      t_Bool bIsSourceInactive = ((e8_AUDIOERROR_AVACTIVATION == enAudiError) ||
            (e8_AUDIOERROR_ALLOCATE == enAudiError) ||  (e8_AUDIOERROR_STARTSOURCEACT == enAudiError));
      if (true == bIsSourceInactive)
      {
         arl_tenSource enSrcNum = static_cast<arl_tenSource> (u8SrcNum);

         m_oSpeechChnReqLock.s16Lock();
         m_bSpeechChnRequested = (ARL_SRC_SPI_VR == enSrcNum)? (false) : m_bSpeechChnRequested;
         t_Bool bIsSpeechChnRequested = m_bSpeechChnRequested;
         m_oSpeechChnReqLock.vUnlock();

         m_poSpiCmdIntf->vOnStopSourceActivity(u8SrcNum);

         //! Set audio context as invalid (will be set to correct state once AudioContext update is received)
         //! Note: SPI should take/borrow audio focus to MD asap, to stop it from streaming music.
         if ((ARL_SRC_SPI_MAIN == enSrcNum) && (e8SPI_SPEECH_SPEAKING != senDeviceSpeechAppState) && (false == bIsSpeechChnRequested))
         {
            m_poSpiCmdIntf->vSetAccessoryAudioContext(m_poSpiCmdIntf->u32GetSelectedDeviceHandle(),
                  e8SPI_AUDIO_UNKNOWN, true, corEmptyUsrContext, e8DEV_TYPE_ANDROIDAUTO);
         }
      }//if (true == bIsSourceInactive)

      m_poSpiCmdIntf->vOnAudioError(u8SrcNum,enAudiError);
   }//if (NULL != m_poSpiCmdIntf)

}// spi_tclAudioPolicy::vOnError(tU8 u8SrcNum,arl_tenISourceError cenError)


/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAudioPolicy::vOnDeviceAppStateChange(tU8)
 ***************************************************************************/
t_Void spi_tclAudioPolicy::vOnDeviceAppStateChange(tenSpeechAppState enSpeechAppState)
{
   ETG_TRACE_USR1(("spi_tclAudioPolicy::vOnDeviceAppStateChange entered "));

   //! Store Device speech app state
   senDeviceSpeechAppState = enSpeechAppState;
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
