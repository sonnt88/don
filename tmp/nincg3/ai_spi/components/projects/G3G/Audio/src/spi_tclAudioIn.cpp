/*!
 *******************************************************************************
 * \file             spi_tclAudioIn.cpp
 * \brief            Audio Input Handling using ECNR
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Audio Input Handling using ECNR
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 24.03.2014 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclAudioIn.h"
#include "spi_tclAudioDBusClient.h"
#include "spi_EcnrIntrospection.h"

#include <pthread.h>
//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclAudioIn.cpp.trc.h"
#endif
#endif

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
//! Defines for ECNR DBus service
#define ECNR_OBJECT_PATH "/"
#define ECNR_SERVICE_NAME "org.bosch.ecnr.service"
#define ECNR_INTERFACE_NAME "org.bosch.ecnr.service"
#define ECNR_ALSA_INTERFACE_NAME "org.bosch.ecnr.alsa"

//! TODO : Remove hardcoded value and use include file from ECNR
//! SPI AppID as defined in ECNR config
static const t_UChar cocECNRAppID = 2;
static const t_UChar cocECNRStartMode = 1;
//! SPI Dataset Offset as defined in ECNR config
static const t_U32 cou32SPIDatasetOffset = 200;

#define SEND_AUDIOIN_RESULT(FUNCTIONCALL, ERROR, USERDATA)               \
t_Bool bResult = (NULL == ERROR);                                        \
if (NULL != ERROR)                                                       \
{                                                                        \
   g_error_free(ERROR);                                                  \
}                                                                        \
                                                                         \
spi_tclAudioIn *poAudioIn = static_cast<spi_tclAudioIn*>(USERDATA);      \
if (NULL != poAudioIn)                                                   \
{                                                                        \
   poAudioIn->FUNCTIONCALL(bResult);                                     \
}

/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclAudioIn::spi_tclAudioIn
 ***************************************************************************/
spi_tclAudioIn::spi_tclAudioIn() :
         m_poEcnrProxy(NULL), m_poEcnrAlsaProxy(NULL), m_poDBusConn(NULL)
{
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioIn::~spi_tclAudioIn
 ***************************************************************************/
spi_tclAudioIn::~spi_tclAudioIn()
{
   m_poEcnrProxy = NULL;
   m_poEcnrAlsaProxy = NULL;
   m_poDBusConn = NULL;
   finalize_proxy_call_queue();
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioIn::vSubscribeForAudioIn
 ***************************************************************************/
t_Void spi_tclAudioIn::vSubscribeForAudioIn()
{
   ETG_TRACE_USR1(("spi_tclAudioIn::vSubscribeForAudioIn entered \n"));

   //! Get connection to the system bus (ECNR service is launched on system bus)
   GError *poError = NULL;
   m_poDBusConn = dbus_g_bus_get(DBUS_BUS_SYSTEM, &poError);

   if(NULL == poError)
   {
	   //! Initialize the queue to start enquing dbus calls and process when
	   //! required.
	   initialize_proxy_call_queue();
      //! Creates a new proxy for a remote interface(org.bosch.ecnr.service).
      //! Method calls and signal connections over this proxy will
      //! go to org.bosch.ecnr.service
      //! org.bosch.ecnr.service is expected to support the given interface name
      m_poEcnrProxy = dbus_g_proxy_new_for_name(m_poDBusConn, ECNR_SERVICE_NAME,
               ECNR_OBJECT_PATH, ECNR_INTERFACE_NAME);
      if (NULL == m_poEcnrProxy)
      {
         ETG_TRACE_ERR(("spi_tclAudioIn::vSubscribeForAudioIn:"
         " Couldn't create the proxy object for ECNR interface \n"));
      }// if (m_poEcnrProxy == NULL)

      //! Creates a new proxy for a remote interface(org.bosch.ecnr.alsa).
      //! Method calls and signal connections over this proxy will
      //! go to org.bosch.ecnr.service
      //! org.bosch.ecnr.service is expected to support the given interface name
      m_poEcnrAlsaProxy =  dbus_g_proxy_new_for_name(m_poDBusConn, ECNR_SERVICE_NAME,
               ECNR_OBJECT_PATH, ECNR_ALSA_INTERFACE_NAME);
      if (NULL == m_poEcnrAlsaProxy)
      {
         ETG_TRACE_ERR(("spi_tclAudioIn::vSubscribeForAudioIn:"
         " Couldn't create the proxy object for ECNR ALSA interface \n"));
      }// if (m_poEcnrProxy == NULL)
   }
   else
   {
      ETG_TRACE_ERR(("spi_tclAudioIn::vSubscribeForAudioIn: "
      "Failed to connect to system bus! Error = %s\n ", poError->message));
      g_error_free(poError);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioIn::vInitializeAudioIn
 ***************************************************************************/
t_Void spi_tclAudioIn::vInitializeAudioIn(tenAudioInDataSet enAudioDataSet)
{
   ETG_TRACE_USR1(("spi_tclAudioIn::vInitializeAudioIn: enAudioDataSet = %d\n",
            ETG_ENUM(AUDIO_ECNR_DATASET, enAudioDataSet)));
   //! changed to synchronous interface of ecnr
   GError *poError = NULL;
   org_bosch_ecnr_service_ecnr_initialize(
         m_poEcnrProxy, cocECNRAppID,  cou32SPIDatasetOffset + enAudioDataSet,  &poError);

   if (NULL != poError)
   {
      ETG_TRACE_ERR(("spi_tclAudioIn::vInitializeAudioIn:  Error = %s\n ",
                  poError->message));
   }
   ETG_TRACE_USR4(("spi_tclAudioIn::vInitializeAudioIn left \n"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioIn::vUnitializeAudioIn
 ***************************************************************************/
t_Void spi_tclAudioIn::vUnitializeAudioIn()
{
   ETG_TRACE_USR1(("spi_tclAudioIn::vUnitializeAudioIn entered \n"));
   //! changed to synchronous interface of ecnr
   GError *poError = NULL;
   org_bosch_ecnr_service_ecnr_destroy(
         m_poEcnrProxy, cocECNRAppID, &poError);

   if (NULL != poError)
   {
      ETG_TRACE_ERR(("spi_tclAudioIn::vUnitializeAudioIn:  Error = %s\n ",
                  poError->message));
   }
   ETG_TRACE_USR4(("spi_tclAudioIn::vUnitializeAudioIn left \n"));

}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioIn::vStartAudioIn
 ***************************************************************************/
t_Void spi_tclAudioIn::vStartAudioIn()
{
   ETG_TRACE_USR1(("spi_tclAudioIn::vStartAudioIn entered \n"));
   //! As suggested by ECNR team, synchronous blocking call is used for start audio.
   //! @Note: Please don't pass the ecnr startMode as 0 for synchronous call as this would block for ~1 minute.
   //! instead use the asynchrnous interface if startMode has to be set to 0
   GError *poError = NULL;
   org_bosch_ecnr_service_ecnr_start_audio_ext(
         m_poEcnrProxy, cocECNRAppID, cocECNRStartMode,  &poError);

   if (NULL != poError)
   {
      ETG_TRACE_ERR(("spi_tclAudioIn::vStartAudioIn:  Error = %s\n ",
                  poError->message));
   }
   ETG_TRACE_USR4(("spi_tclAudioIn::vStartAudioIn left \n"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioIn::vStopAudioIn
 ***************************************************************************/
t_Void spi_tclAudioIn::vStopAudioIn()
{
   ETG_TRACE_USR1(("spi_tclAudioIn::vStopAudioIn entered \n"));
   //! changed to synchronous interface of ecnr
   GError *poError = NULL;
   org_bosch_ecnr_service_ecnr_stop_audio(m_poEcnrProxy, cocECNRAppID, &poError);

   if (NULL != poError)
   {
      ETG_TRACE_ERR(("spi_tclAudioIn::vStopAudioIn:  Error = %s\n ", poError->message));
   }
   ETG_TRACE_USR4(("spi_tclAudioIn::vStopAudioIn left\n"));

}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioIn::vSetAudioInConfig
 ***************************************************************************/
t_Void spi_tclAudioIn::vSetAudioInConfig(tenAudioInDataSet enAudioDataSet)
{
   ETG_TRACE_USR1((" vSetAudioInConfig enAudioDataSet = %d \n", ETG_ENUM(AUDIO_ECNR_DATASET, enAudioDataSet)));

   //! changed to synchronous interface of ecnr
   GError *poError = NULL;
   org_bosch_ecnr_service_ecnr_set_configuration(m_poEcnrProxy, cocECNRAppID,cou32SPIDatasetOffset + enAudioDataSet, &poError);

   if (NULL != poError)
   {
      ETG_TRACE_ERR(("spi_tclAudioIn::vSetAudioInConfig:  Error = %s\n ", poError->message));
   }

   ETG_TRACE_USR4(("spi_tclAudioIn::vSetAudioInConfig left \n"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioIn::vSetWBAudio
 ***************************************************************************/
t_Void spi_tclAudioIn::vSetWBAudio(t_Bool bSettoWB)
{
   ETG_TRACE_USR1((" %s Not Implemented  \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(bSettoWB);
   //! TODO
}


/***************************************************************************
 ** FUNCTION:  spi_tclAudioIn::vSetWBAudio
 ***************************************************************************/
t_Void spi_tclAudioIn::vSetAlsaDevice(t_U32 u32AlsaDeviceSelector, t_String szAlsaDeviceName)
{
   ETG_TRACE_USR1(("spi_tclAudioIn::vSetAlsaDevice entered \n"));

   if (false == szAlsaDeviceName.empty())
   {
      ETG_TRACE_USR4((" ECNR ALSA Device %s \n", szAlsaDeviceName.c_str()));
      ETG_TRACE_USR4((" ECNR ALSA Device Selector %d \n", u32AlsaDeviceSelector));
      //! Solution for  SUZUKI-25184 
      //! As suggested by ECNR team, synchronous blocking call is used for setting alsa device.
      //! If StartAudio is called as blocking and vSetAlsaDevice as asynchronous call the calls
      //! can reach ecnr in reverse order. Hence changing vSetAlsaDevice also to blocking call
      GError *poError = NULL;
      org_bosch_ecnr_alsa_ecnr_alsa_set_device(
            m_poEcnrAlsaProxy, cocECNRAppID, u32AlsaDeviceSelector, szAlsaDeviceName.c_str(), &poError);

      if (NULL != poError)
      {
         ETG_TRACE_ERR(("spi_tclAudioIn::vSetAlsaDevice:  Error = %s\n ",
                     poError->message));
      }
   }//End of if (false == szAlsaDeviceName.empty())

   ETG_TRACE_USR4(("spi_tclAudioIn::vSetAlsaDevice left \n"));
}
/***************************************************************************
 *********************************PRIVATE***********************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclAudioIn::vEcnrInitializeCb
 ***************************************************************************/
t_Void spi_tclAudioIn::vEcnrInitializeCb(DBusGProxy* poProxy, GError* poError,
         gpointer pvUserdata)
{
   ETG_TRACE_USR1(("spi_tclAudioIn::vEcnrInitializeCb Entered %d \n", pthread_self()));
   SPI_INTENTIONALLY_UNUSED(poProxy);

   if (NULL != poError)
   {
      ETG_TRACE_ERR(("spi_tclAudioIn::vEcnrInitializeCb:  Error = %s\n ",
                  poError->message));
   }

   //! Send result to registered callback
   SEND_AUDIOIN_RESULT(vResultInitializeAudioInRes, poError, pvUserdata);
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioIn::vEcnrUnitializeCb
 ***************************************************************************/
t_Void spi_tclAudioIn::vEcnrUnitializeCb(DBusGProxy* poProxy, GError* poError,
         gpointer pvUserdata)
{
   ETG_TRACE_USR1(("spi_tclAudioIn::vEcnrUnitializeCb Entered %d \n", pthread_self()));
   SPI_INTENTIONALLY_UNUSED(poProxy);

   if (NULL != poError)
   {
      ETG_TRACE_ERR(("spi_tclAudioIn::vEcnrUnitializeCb:  Error = %s\n ",
                  poError->message));
   }

   //! Send result to registered callback
   SEND_AUDIOIN_RESULT(vResultUnitializeAudioInRes, poError, pvUserdata);
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioIn::vEcnrStartAudioCb
 ***************************************************************************/
t_Void spi_tclAudioIn::vEcnrStartAudioCb(DBusGProxy* poProxy, GError* poError,
         gpointer pvUserdata)
{
   ETG_TRACE_USR1(("spi_tclAudioIn::vEcnrStartAudioCb Entered %d \n", pthread_self()));
   SPI_INTENTIONALLY_UNUSED(poProxy);

   if (NULL != poError)
   {
      ETG_TRACE_ERR(("spi_tclAudioIn::vEcnrStartAudioCb:  Error = %s\n ",
                  poError->message));
   }
   //! This callbck is not used currently as synchronous blocking call is used for start audio
   //! Send result to registered callback
   SEND_AUDIOIN_RESULT(vResultStartAudioInRes, poError, pvUserdata);
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioIn::vEcnrStopAudioCb
 ***************************************************************************/
t_Void spi_tclAudioIn::vEcnrStopAudioCb(DBusGProxy* poProxy, GError* poError,
         gpointer pvUserdata)
{
   ETG_TRACE_USR1(("spi_tclAudioIn::vEcnrStopAudioCb Entered %d \n", pthread_self()));
   SPI_INTENTIONALLY_UNUSED(poProxy);

   if (NULL != poError)
   {
      ETG_TRACE_ERR(("spi_tclAudioIn::vEcnrStopAudioCb:  Error = %s\n ",
                  poError->message));
   }

   //! Send result to registered callback
   SEND_AUDIOIN_RESULT(vResultStopAudioInRes, poError, pvUserdata);
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioIn::vEcnrSetAlsaDeviceCb
 ***************************************************************************/
t_Void spi_tclAudioIn::vEcnrSetAlsaDeviceCb(DBusGProxy* poProxy, GError* poError,
         gpointer pvUserdata)
{
   ETG_TRACE_USR1(("spi_tclAudioIn::vEcnrSetAlsaDeviceCb Entered %d \n", pthread_self()));
   SPI_INTENTIONALLY_UNUSED(poProxy);

   if (NULL != poError)
   {
      ETG_TRACE_ERR(("spi_tclAudioIn::vEcnrSetAlsaDeviceCb:  Error = %s\n ",
                  poError->message));
   }

   //! Send result to registered callback
   SEND_AUDIOIN_RESULT(vResultSetAlsaDeviceRes, poError, pvUserdata);
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioIn::vEcnrSetConfigCb
 ***************************************************************************/
t_Void spi_tclAudioIn::vEcnrSetConfigCb(DBusGProxy *poProxy, GError *poError, gpointer pvUserdata)
{
   ETG_TRACE_USR1(("spi_tclAudioIn::vEcnrSetConfigCb Entered %d \n", pthread_self()));
   SPI_INTENTIONALLY_UNUSED(poProxy);

   if (NULL != poError)
   {
      ETG_TRACE_ERR(("spi_tclAudioIn::vEcnrSetConfigCb:  Error = %s\n ",
                  poError->message));
   }
   //! Send result to registered callback
   SEND_AUDIOIN_RESULT(vResultSetAudioConfigRes, poError, pvUserdata);
}

