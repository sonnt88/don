/*!
 *******************************************************************************
 * \file             spi_tclAudioPolicy.h
 * \brief            Gen3 Audio Policy class for PSA 
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3 Projects
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Gen3 Audio Policy class for PSA 
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                                  | Modifications
 29.10.2013 |  Hari Priya E R(RBEI/ECP2)               | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef SPI_TCLAUDIOPOLICY_H
#define SPI_TCLAUDIOPOLICY_H

#define ARL_S_IMPORT_INTERFACE_GENERIC
#include "audio_routing_lib.h"

#include "Lock.h"

/**
 *  class definitions.
 */
class spi_tclCmdInterface;
class ahl_tclBaseOneThreadApp;

/**
 * Gen3 Policy class that realises the audio interface based on ARL.
 */

class spi_tclAudioPolicy: public arl_tclISource
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioPolicy::spi_tclAudioPolicy(spi_tclCmdInterface* poSpiCmdIntf,
       ahl_tclBaseOneThreadApp* poMainApp);
       ***************************************************************************/
      /*!
       * \fn      spi_tclAudioPolicy(spi_tclCmdInterface* poSpiCmdIntf,
       *             ahl_tclBaseOneThreadApp* poMainApp)
       * \brief   Parameterised Constructor
       **************************************************************************/
      spi_tclAudioPolicy(spi_tclCmdInterface* poSpiCmdIntf,
               ahl_tclBaseOneThreadApp* poMainApp);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioPolicy::~spi_tclAudioPolicy();
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclAudioPolicy()
       * \brief   Virtual Destructor
       **************************************************************************/
      virtual ~spi_tclAudioPolicy();

      /***************************************************************************
       ** FUNCTION:  tBool spi_tclAudioPolicy::bRequestAudioActivation(tU8)
       ***************************************************************************/
      /*!
       * \fn      bRequestAudioActivation(tU8 u8SourceNum)
       * \brief   Request to the Audio Manager by Component for Starting Audio Playback.
       *          Mandatory Interface to be implemented as per Project Audio Policy.
       * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
       *          Source Number will be defined for Audio Source by the Audio Component.
       * \retval  Bool value
       **************************************************************************/
      virtual tBool bRequestAudioActivation(tU8 u8SourceNum);

      /***************************************************************************
       ** FUNCTION:  tBool spi_tclAudioPolicy::bRequestAudioDeactivation(tU8)
       ***************************************************************************/
      /*!
       * \fn      bRequestAudioDeactivation(tU8 u8SourceNum)
       * \brief   Request to the Audio Manager by Component for Stopping Audio Playback.
       *          Mandatory Interface to be implemented as per Project Audio Policy.
       * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
       *          Source Number will be defined for Audio Source by the Audio Component.
       * \retval  Bool value
       **************************************************************************/
      virtual tBool bRequestAudioDeactivation(tU8 u8SourceNum);

      /***************************************************************************
       ** FUNCTION:  tBool spi_tclAudioPolicy::bPauseAudioActivity(tU8)
       ***************************************************************************/
      /*!
       * \fn      bPauseAudioActivity(tU8 u8SourceNum)
       * \brief   Request to the Audio Manager by Component for Pausing Audio Playback.
       *          Optional Interface to be implemented if supported and required.
       * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
       *          Source Number will be defined for Audio Source by the Audio Component.
       * \retval  Bool value
       **************************************************************************/
      virtual tBool bPauseAudioActivity(tU8 u8SourceNum);

      /***************************************************************************
       ** FUNCTION:  tVoid spi_tclAudioPolicy::vStartSourceActivityResult(tU8, tBool)
       ***************************************************************************/
      /*!
       * \fn      vStartSourceActivityResult(tU8 u8SourceNum, tBool bError)
       * \brief   Acknowledgement from the Source Component to Audio Manager indicating
       *          Successful Start of Audio Playback on the allocated route.
       *		     Mandatory Interface to be implemented.
       * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
       *          [bError]: TRUE for Error Condition, FALSE otherwise
       *          Source Number will be defined for Audio Source by the Audio Component.
       * \retval  NONE
       **************************************************************************/
      virtual tVoid vStartSourceActivityResult(tU8 u8SourceNum, tBool bError =
               FALSE);

      /***************************************************************************
       ** FUNCTION:  tVoid spi_tclAudioPolicy::vStopSourceActivityResult(tU8, tBool)
       ***************************************************************************/
      /*!
       * \fn      vStopSourceActivityResult(tU8 u8SourceNum, tBool bError)
       * \brief   Acknowledgement from the Source Component to Audio Manager indicating
       *          Successful Stop of Audio Playback on the allocated route.
       *		   Mandatory Interface to be implemented.
       * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
       *          [bError]: TRUE for Error Condition, FALSE otherwise
       * \retval  NONE
       **************************************************************************/
      virtual tVoid vStopSourceActivityResult(tU8 u8SourceNum, tBool bError =
               FALSE);

      /***************************************************************************
       ** FUNCTION:  tVoid spi_tclAudioPolicy::vPauseSourceActivityResult(tU8, tBool)
       ***************************************************************************/
      /*!
       * \fn      vPauseSourceActivityResult(tU8 u8SourceNum, tBool bError)
       * \brief   Acknowledgement from the Source Component to Audio Manager indicating
       *          Successful Pause of Audio Playback on the allocated route.
       *		     Optional Interface to be implemented if supported and required
       * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
       *          [bError]: TRUE for Error Condition, FALSE otherwise
       *          Source Number will be defined for Audio Source by the Audio Component.
       * \retval  Bool value
       **************************************************************************/
      virtual tVoid vPauseSourceActivityResult(tU8 u8SourceNum, tBool bError =
               FALSE);

      /***************************************************************************
       ** FUNCTION:  tBool spi_tclAudioPolicy::bSetSrcAvailability(tU8,tBool)
       ***************************************************************************/
      /*!
       * \fn      bSetSrcAvailability(tU8 u8SourceNum,tBool bAvail)
       * \brief   Register the Availability of State of Source with Audio Manager.
       *   	   Optional Interface to be implemented if supported and required
       * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
       *          [bAvail]: TRUE is Source Available, FALSE if Unavailable
       *          Source Number will be defined for Audio Source by the Audio Component.
       * \retval  Bool value
       **************************************************************************/
      virtual tBool
               bSetSrcAvailability(tU8 u8SourceNum, tBool bAvail = TRUE);

      /***************************************************************************
       ** FUNCTION:  tBool spi_tclAudioPolicy::vSetServiceAvailable(tU8,tBool)
       ***************************************************************************/
      /*!
       * \fn      vSetServiceAvailable(tU8 u8SourceNum,tBool bAvail)
       * \brief   Register the Availability of service with Audio Manager.
       *          [bAvail]: TRUE is Source Available, FALSE if Unavailable
       **************************************************************************/
      virtual t_Void vSetServiceAvailable(tBool bAvail);

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclAudioPolicy::bSetAudioDucking()
      ***************************************************************************/
      /*!
      * \fn     bSetAudioDucking
      * \brief  Interface to set audio ducking ON/OFF.
      * \param  cou8SrcNum: Sourc Number(Not used for G3G Audio)
      * \param  cou16RampDuration: Ramp duration in milliseconds
      * \param  cou8VolumeindB: Volume level in dB
      * \param  coenDuckingType: Ducking/ Unducking
      **************************************************************************/
      virtual t_Bool bSetAudioDucking(const t_U8 cou8SrcNum, const t_U16 cou16RampDuration,
               const t_U8 cou8VolumeindB, const tenDuckingType coenDuckingType);

      /***************************************************************************
       ** FUNCTION:  tBool spi_tclAudioPolicy::bSetSourceMute(tU8)
       ***************************************************************************/
      /*!
       * \fn      bSetSourceMuteOn(tU8 u8SourceNum)
       * \brief   Request to Audio Manager to Mute the Source Audio.
       *          Optional Interface to be implemented if supported and required.
       * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
       *          Source Number will be defined for Audio Source by the Audio Component.
       * \retval  Bool value
       **************************************************************************/
      virtual tBool bSetSourceMute(tU8 u8SourceNum);

      /***************************************************************************
       ** FUNCTION:  tBool spi_tclAudioPolicy::bSetSourceDemute(tU8)
       ***************************************************************************/
      /*!
       * \fn      tBool bSetSourceDemute(tU8 u8SourceNum)
       * \brief   Request to Audio Manager to Demute the Source Audio.
       *		   Optional Interface to be implemented if supported and required.
       * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
       *          Source Number will be defined for Audio Source by the Audio Component.
       * \retval  Bool value
       **************************************************************************/
      virtual tBool bSetSourceDemute(tU8 u8SourceNum);

      /***************************************************************************
      ** FUNCTION:  tBool spi_tclAudioPolicy::bOnSrcActivity(const arl..
      ***************************************************************************/
      /*!
       * \brief   CALLED BY AUDIO-ROUTING-LIB:
       *          Application specific function on Source Activity start.
       * \param   [enSrcNum]: (I) Source Number.
       * \param   [rfcoSrcActivity]: (I) Source Activity
       * \retval  tBool: TRUE, if source activity was successful, FALSE otherwise
       **************************************************************************/
      virtual tBool bOnSrcActivity(arl_tenSource enSrcNum,
               const arl_tSrcActivity& rfcoSrcActivity);

      /***************************************************************************
      ** FUNCTION:  virtual tBool spi_tclAudioPolicy::bOnAllocate(..
      ***************************************************************************/
      /*!
       * \brief   CALLED BY AUDIO-ROUTING-LIB:
       *          Application specific function after Allocate is processed.
       * \param   [enSrcNum]:  (I) Source Number.
       * \param   [rfcoAllocRoute]: (I) Reference to Allocate route result
       * \retval  tBool: TRUE, if Application performed operations successfully,
       *          FALSE otherwise
       **************************************************************************/
      virtual tBool bOnAllocate(arl_tenSource enSrcNum,
               const arl_tAllocRouteResult& rfcoAllocRoute);

      /***************************************************************************
      ** FUNCTION:  virtual tBool spi_tclAudioPolicy::bOnDeAllocate(
      ***************************************************************************/
      /*!
       * \brief   CALLED BY AUDIO-ROUTING-LIB:
       *          May be overridden by Player App to Release Resources.
       *          Application specific function after DeAllocate is processed.
       * \param   [enSrcNum]:  (I) Source Number.
       * \retval  tBool: TRUE, if Application performed operations successfully,
       *          FALSE otherwise
       **************************************************************************/
      virtual tBool bOnDeAllocate(arl_tenSource enSrcNum);

      /***************************************************************************
      ** FUNCTION:  virtual tVoid spi_tclAudioPolicy::vOnError()
      ***************************************************************************/
      /*!
       * \brief   Function callback to trigger actions on request AV Activation
       *          error
       * \note    This function needs to be overloaded by the derived class only in
       *          case the source is self triggered & needs to send error message
       *          to clients based on source number.
       * \param   [u8SrcNum]:  (I) Source Number.
       * \param   [cenError]:  (I) ISource Error type.
       * \retval  NONE
       **************************************************************************/
      virtual tVoid vOnError(tU8 u8SrcNum, arl_tenISourceError cenError);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAudioPolicy::vOnDeviceAppStateChange(tU8)
       ***************************************************************************/
      /*!
       * \fn      vOnDeviceAppStateChange()
       * \brief   Interface to receive Device app state changes
       * \param   [enSpeechAppState]: New Device Speech app state
       * \retval  Bool value
       **************************************************************************/
      virtual t_Void vOnDeviceAppStateChange(tenSpeechAppState enSpeechAppState);

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

      //!Instance of the Command Interface class
      spi_tclCmdInterface* m_poSpiCmdIntf;

      //! Indicates whether Speech channel activation is requested by SPI
      t_Bool m_bSpeechChnRequested;

      //! Lock object for m_bSpeechChnRequested flag
      Lock   m_oSpeechChnReqLock;

};
#endif   // #ifndef SPI_TCLAUDIOPOLICY_H
