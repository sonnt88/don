/*
 * spi_tclAudioDBusClient.h
 *
 *  Created on: Feb 5, 2015
 *      Author: tch5kor
 */

#ifndef SPI_TCLAUDIODBUSCLIENT_H_
#define SPI_TCLAUDIOBUSCLIENT_H_


#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <glib.h>
#include <gio/gio.h>
#include <dbus/dbus-glib.h>
#include <BaseTypes.h>

// protoypes for the proxy call routines needed for the dbus-glib bindings
static t_S32 write_to_proxy_call_pipe(t_Char val);
t_Void proxy_call_queue_push(DBusGProxyCallArgs * call_args);
t_Void finalize_proxy_call_queue();
gint initialize_proxy_call_queue();
static gboolean process_proxy_call_queue(GIOChannel * source,
      GIOCondition condition,
      gpointer userdata);



#endif /* SPI_TCLAUDIODBUSCLIENT_H_ */
