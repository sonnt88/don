/*!
 *******************************************************************************
 * \file             spi_tclAudioIn.h
 * \brief            Audio Input Handling using ECNR
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Audio Input Handling using ECNR
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 24.03.2014 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLAUDIOIN_H_
#define SPI_TCLAUDIOIN_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclAudioInBase.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclAudioInBase
 * \brief Audio Input Handling using ECNR
 */

class spi_tclAudioIn: public spi_tclAudioInBase
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioIn::spi_tclAudioIn
       ***************************************************************************/
      /*!
       * \fn     spi_tclAudioIn()
       * \brief  Default Constructor
       * \sa      ~spi_tclAudioIn()
       **************************************************************************/
      spi_tclAudioIn();

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioIn::~spi_tclAudioIn
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclAudioIn()
       * \brief  virtual Destructor
       * \sa     spi_tclAudioIn()
       **************************************************************************/
      virtual ~spi_tclAudioIn();

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioIn::vSubscribeForAudioIn
       ***************************************************************************/
      /*!
       * \fn     vSubscribeForAudioIn()
       * \brief  implements spi_tclAudioInBase::vSubscribeForAudioIn
       * \sa     spi_tclAudioInBase::vSubscribeForAudioIn
       **************************************************************************/
      t_Void vSubscribeForAudioIn();

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioIn::vInitializeAudioIn
       ***************************************************************************/
      /*!
       * \fn     vInitializeAudioIn(tenAudioInDataSet enAudioDataSet)
       * \brief  implements spi_tclAudioInBase::vInitializeAudioIn
       * \sa     spi_tclAudioInBase::vInitializeAudioIn
       **************************************************************************/
      t_Void vInitializeAudioIn(tenAudioInDataSet enAudioDataSet);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioIn::vUnitializeAudioIn
       ***************************************************************************/
      /*!
       * \fn     vUnitializeAudioIn()
       * \brief  implements spi_tclAudioInBase::vUnitializeAudioIn
       * \sa     spi_tclAudioInBase::vUnitializeAudioIn
       **************************************************************************/
      t_Void vUnitializeAudioIn();

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioIn::vStartAudioIn
       ***************************************************************************/
      /*!
       * \fn     vStartAudioIn()
       * \brief  implements spi_tclAudioInBase::vStartAudioIn
       * \sa     spi_tclAudioInBase::vStartAudioIn
       **************************************************************************/
      t_Void vStartAudioIn();

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioIn::vStopAudioIn
       ***************************************************************************/
      /*!
       * \fn     vStopAudioIn()
       * \brief  implements spi_tclAudioInBase::vStopAudioIn
       * \sa     spi_tclAudioInBase::vStopAudioIn
       **************************************************************************/
      t_Void vStopAudioIn();

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioIn::vSetAudioInConfig
       ***************************************************************************/
      /*!
       * \fn     vSetAudioInConfig(tenAudioInDataSet enAudioDataSet)
       * \brief  implements spi_tclAudioInBase::vSetAudioInConfig
       * \sa     spi_tclAudioInBase::vSetAudioInConfig
       **************************************************************************/
      t_Void vSetAudioInConfig(tenAudioInDataSet enAudioDataSet);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioIn::vSetWBAudio
       ***************************************************************************/
      /*!
       * \fn     vSetWBAudio()
       * \brief  implements spi_tclAudioInBase::vSetWBAudio
       * \sa     spi_tclAudioInBase::vSetWBAudio
       **************************************************************************/
      t_Void vSetWBAudio(t_Bool bSettoWB);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioIn::vSetAlsaDevice
       ***************************************************************************/
      /*!
       * \fn     vSetAlsaDevice()
       * \brief  implements spi_tclAudioInBase::vSetAlsaDevice
       * \sa     spi_tclAudioInBase::vSetAlsaDevice
       **************************************************************************/
      t_Void vSetAlsaDevice(t_U32 u32AlsaDeviceSelector, t_String szAlsaDeviceName);

   private:

      /***************************************************************************
       *********************************PRIVATE***********************************
       ***************************************************************************/

      /*************************************************************************
       **  FUNCTION : spi_tclAudioIn::spi_tclAudioIn(const spi_tclAudioIn &corfAudioIn)
       *************************************************************************/
      /*!
       * \fn     spi_tclAudioIn(const spi_tclAudioIn &corfAudioIn)
       * \brief  Copy constructor: Made private to prevent unintended usage
       *         of Default copy constructor
       * \param  corfAudioIn : [IN] reference to AudioIn class
       *************************************************************************/
      spi_tclAudioIn(const spi_tclAudioIn &corfAudioIn);

      /*************************************************************************
       **  FUNCTION : spi_tclAudioIn& operator=(const spi_tclAudioIn &corfAudioIn)
       *************************************************************************/
      /*!
       * \fn     spi_tclAudioIn& operator=(const spi_tclAudioIn &corfAudioIn) {}
       * \brief  Overloaded function: Made private to prevent unintended usage of
       *         default assignment operator
       * \param  corfAudioIn : [IN] reference to AudioIn class
       *************************************************************************/
      spi_tclAudioIn& operator=(const spi_tclAudioIn &corfAudioIn);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioIn::vEcnrInitializeCb
       ***************************************************************************/
      /*!
       * \fn     vEcnrInitializeCb
       * \brief  Called by the DBus ECNR service as a result for ecnrInitilize (async call)
       * \param  poProxy: Pointer to DBus Proxy for which the result is sent
       * \param  poError: populated in case of error with error data else NULL
       * \param  pvUserdata: user context
       * \sa     spi_tclAudioIn::vInitializeAudioIn
       **************************************************************************/
      static t_Void vEcnrInitializeCb(DBusGProxy *poProxy, GError *poError, gpointer pvUserdata);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioIn::vEcnrUnitializeCb
       ***************************************************************************/
      /*!
       * \fn     vEcnrUnitializeCb
       * \brief  Called by the DBus ECNR service as a result for ecnrdestroy (async call)
       * \param  poProxy: Pointer to DBus Proxy for which the result is sent
       * \param  poError: populated in case of error with error data else NULL
       * \param  pvUserdata: user context
       * \sa     spi_tclAudioIn::vUnitializeAudioIn
       **************************************************************************/
      static t_Void vEcnrUnitializeCb(DBusGProxy *poProxy, GError *poError, gpointer pvUserdata);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioIn::vEcnrStartAudioCb
       ***************************************************************************/
      /*!
       * \fn     vEcnrStartAudioCb
       * \brief  Called by the DBus ECNR service as a result for ecnrStartAudio (async call)
       * \param  poProxy: Pointer to DBus Proxy for which the result is sent
       * \param  poError: populated in case of error with error data else NULL
       * \param  pvUserdata: user context
       * \sa     spi_tclAudioIn::vStartAudioIn
       **************************************************************************/
      static t_Void vEcnrStartAudioCb(DBusGProxy *poProxy, GError *poError, gpointer pvUserdata);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioIn::vEcnrStopAudioCb
       ***************************************************************************/
      /*!
       * \fn     vEcnrStopAudioCb
       * \brief  Called by the DBus ECNR service as a result for ecnrStopAudio (async call)
       * \param  poProxy: Pointer to DBus Proxy for which the result is sent
       * \param  poError: populated in case of error with error data else NULL
       * \param  pvUserdata: user context
       * \sa     spi_tclAudioIn::vStopAudioIn
       **************************************************************************/
      static t_Void vEcnrStopAudioCb(DBusGProxy *poProxy, GError *poError, gpointer pvUserdata);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioIn::vEcnrSetAlsaDeviceCb
       ***************************************************************************/
      /*!
       * \fn     vEcnrSetAlsaDeviceCb
       * \brief  Called by the DBus ECNR service as a result for ecnrAlsaSetDevice (async call)
       * \param  poProxy: Pointer to DBus Proxy for which the result is sent
       * \param  poError: populated in case of error with error data else NULL
       * \param  pvUserdata: user context
       * \sa     na
       **************************************************************************/
      static t_Void vEcnrSetAlsaDeviceCb(DBusGProxy *poProxy, GError *poError, gpointer pvUserdata);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioIn::vEcnrSetConfigCb
       ***************************************************************************/
      /*!
       * \fn     vEcnrSetConfigCb
       * \brief  Called by the DBus ECNR service as a result for ecnrStopAudio (async call)
       * \param  poProxy: Pointer to DBus Proxy for which the result is sent
       * \param  poError: populated in case of error with error data else NULL
       * \param  pvUserdata: user context
       * \sa     spi_tclAudioIn::vStopAudioIn
       **************************************************************************/
      static t_Void vEcnrSetConfigCb(DBusGProxy *poProxy, GError *poError, gpointer pvUserdata);

      //! Instance of DBus proxy for ECNR Service
      DBusGProxy *m_poEcnrProxy;

      //! Instance of DBus proxy for ECNR Service for AlSA
      DBusGProxy *m_poEcnrAlsaProxy;

      //! Instance of the DBus Connection
      DBusGConnection *m_poDBusConn;

};

#endif /* SPI_TCLAUDIOIN_H_ */
