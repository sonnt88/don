/*!
 *******************************************************************************
 * \file              spi_tclPosDataClientHandler.cpp
 * \brief             Position Data Client handler class
 *******************************************************************************
 \verbatim
 PROJECT:        G3G
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Position Data Client handler class 
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date      |  Author                      | Modifications
 08.1.2014 |  Vinoop U                    | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCL_CLIENTHANDLER_POSITIONDATA_H_
#define _SPI_TCL_CLIENTHANDLER_POSITIONDATA_H_

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/
#include <cstring>

#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

#include "SPITypes.h"
#include "Lock.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/


/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/* Forward declarations */

/**
 * \brief   POS_FI Client handler class
 */
class spi_tclPosDataClientHandler 
   : public ahl_tclBaseOneThreadClientHandler
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclPosDataClientHandler::spi_tclPosDataClientHandler(..)
   ***************************************************************************/
   /*!
   * \fn     spi_tclPosDataClientHandler(ahl_tclBaseOneThreadApp* poMainApp)
   * \brief  Parametrized Constructor
   * \param  poMainApp : [IN] Pointer to main app.
   * \sa
   **************************************************************************/
   spi_tclPosDataClientHandler(ahl_tclBaseOneThreadApp* poMainApp);

   /***************************************************************************
   ** FUNCTION:  virtual spi_tclPosDataClientHandler::~spi_tclPosDataClientHandler()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclPosDataClientHandler()
   * \brief   Destructor
   * \sa
   **************************************************************************/
   virtual ~spi_tclPosDataClientHandler();

   /***************************************************************************
   * Application specific methods.
   ***************************************************************************/

   /**************************************************************************
   ** FUNCTION:  t_Void spi_tclPosDataClientHandler::vProcessTimer(tU16 u...
   **************************************************************************/
   /*!
   * \fn      vProcessTimer(tU16 u16TimerId)
   * \brief   This method is called from the vOnTimer() method of this
   *          CCA application on the expiration of a previously via method
   *          bStartTimer() started timer.
   * \param   u16TimerId : [IN]
   **************************************************************************/
   t_Void vProcessTimer(t_U16 u16TimerId);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclPosDataClientHandler::vOnServiceAvailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceAvailable()
   * \brief   This function is called by the framework if the service of our server
   *         becomes available, e.g. server has been started.
   * \sa
   **************************************************************************/
   virtual t_Void vOnServiceAvailable();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclPosDataClientHandler::vOnServiceUnavailable()
   ***************************************************************************/
   /*!
   * \fn      vOnServiceUnavailable()
   * \brief   This function is called by the framework if the service of our server
   *         becomes unavailable, e.g. server has been shut down.
   * \sa
   **************************************************************************/
   virtual t_Void vOnServiceUnavailable();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclPosDataClientHandler::vRegisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers for interested properties to the client handler.
   * \param   enLocDataType: [IN] Indicates type of data being subscribed
   * \param   rLocDataCallbacks: [IN] Structure containing callbacks to subscriber,
   *             to be called when subscribed data is received by client handler.
   **************************************************************************/
   virtual t_Void vRegisterForProperties(tenLocationDataType enLocDataType,
         trLocationDataCallbacks rLocDataCallbacks);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclPosDataClientHandler::vUnregisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Registers for interested properties to the client handler.
   * \param   enLocDataType: [IN] Indicates type of data being un-subscribed
   **************************************************************************/
   virtual t_Void vUnregisterForProperties(tenLocationDataType enLocDataType);

   /***************************************************************************
   * Handler function declarations used by message map.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclPosDataClientHandler::vOnPositionStatus()
   ***************************************************************************/
   /*!
   * \fn      vOnPositionStatus(amt_tclServiceData* poMessage)
   * \brief   This Property represents the calculated position of the device.
   * \param   poMessage: [IN] Pointer to PositionStatus message
   * \sa
   **************************************************************************/
   t_Void vOnPositionStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclPosDataClientHandler::vOnDeadreckoningPositionStatus()
   ***************************************************************************/
   /*!
   * \fn      vOnDeadreckoningPositionStatus(amt_tclServiceData* poMessage)
   * \brief   This Property represents the calculated deadreckoning 
   *          position of the device including quality values.
   * \param   poMessage: [IN] Pointer to DeadreckoningPositionStatus message
   * \sa
   **************************************************************************/
   t_Void vOnDeadreckoningPositionStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   * Message map definition macro
   ***************************************************************************/
   DECLARE_MSG_MAP(spi_tclPosDataClientHandler)

   /*************************************************************************
   ****************************END OF PUBLIC*********************************
   *************************************************************************/

private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclPosDataClientHandler::spi_tclPosDataClientHandler()
   ***************************************************************************/
   /*!
   * \fn     spi_tclPosDataClientHandler()
   * \brief  Default Constructor, will not be implemented.
   **************************************************************************/
   spi_tclPosDataClientHandler();

   /**************************************************************************
   * Assingment Operater, will not be implemented.
   * Avoids Lint Prio 3 warning: Info 1732: new in constructor for class
   * 'spi_tclPosDataClientHandler' which has no assignment operator.
   * NOTE: This is a technique to disable the assignment operator for this
   * class. So if an attempt for the assignment is made compiler complains.
   **************************************************************************/
   spi_tclPosDataClientHandler& operator=(const spi_tclPosDataClientHandler &oClientHandler);

   /***************************************************************************
   *! Data members
   ***************************************************************************/

   /*
    * Main application pointer
    */
   ahl_tclBaseOneThreadApp*   m_poMainApp;

   /*
    * Structure containing callbacks to POS_FI data subscriber.
    */
   trLocationDataCallbacks    m_rLocDataCallbacks;

   /*
    * Lock used to protect simultaneous access of std::function
    */

   Lock m_lock;


   /*************************************************************************
   ****************************END OF PRIVATE********************************
   *************************************************************************/

};

#endif // spi_tclPosDataClientHandler
