/*!
 *******************************************************************************
 * \file              spi_tclPosDataClientHandler.cpp
 * \brief             Position Data Client handler class
 *******************************************************************************
 \verbatim
 PROJECT:        G3G
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Position Data Client handler class 
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 08.01.2014 |  Vinoop U                    | Initial Version
 19.01.2015 |  Ramya Murthy                | CarPlay location data from DeadReckoningPosition property
 23.04.2015 |  Ramya Murthy                | Extraction of Sensor type & Transmission status info from DRPostion 

 \endverbatim
 ******************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <cstdlib>

#include "spi_tclPosDataClientHandler.h"
//@todo - Issue - bpstl ambiguous usage should be removed
#include "FIMsgDispatch.h"
#include "XFiObjHandler.h"
using namespace shl::msgHandler;

//Include common fi interface
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

#define GENERICMSGS_S_IMPORT_INTERFACE_GENERIC
#include "generic_msgs_if.h"

//Include FI interface of used service
#define FI_S_IMPORT_INTERFACE_POSFI_TYPES
#define FI_S_IMPORT_INTERFACE_POSFI_FUNCTIONIDS
#define FI_S_IMPORT_INTERFACE_POSFI_ERRORCODES
#define FI_S_IMPORT_INTERFACE_POSFI_SERVICEINFO
#include "fi_if.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DATASERVICE
#include "trcGenProj/Header/spi_tclPosDataClientHandler.cpp.trc.h"
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/*!
* \XFiObjHandler<posfi_tclMsgPositionStatus> spi_tDevConnectionStatus
* \brief  Popoerty status, with auto extract & auto destroy feature.
*/

typedef XFiObjHandler<posfi_tclMsgPositionStatus>
   spi_tPositionStatus;

/*!
* \XFiObjHandler<posfi_tclMsgDeadReckoningPositionStatus> spi_tDevConnectionStatus
* \brief  Popoerty status, with auto extract & auto destroy feature.
*/

typedef XFiObjHandler<posfi_tclMsgDeadReckoningPositionStatus>
   spi_tDRPositionStatus;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static const trLocationDataCallbacks rEmptyLocCallbacks;

//! Resolution of Lat/Long values as per POS_FI as per POS_FI properties
//! "Position" and "DeadReckoningPosition"
static const t_Double codLatLongResolution = 4294967296/360.0;

//! Resolution of Heading values as per POS_FI properties "Position"
//! and "DeadReckoningPosition"
#ifdef VARIANT_S_FTR_ENABLE_PSA
   static const t_Double codHeadingResolution = 4294967296/360.0;
   static const t_Double codDRHeadingResolution = 4294967296/360.0;
#else
   static const t_Double codHeadingResolution = 256/360.0;
   static const t_Double codDRHeadingResolution = 100/1.0;
#endif

/******************************************************************************/
/*                                                                            */
/* CCA MESSAGE MAP                                                            */
/*                                                                            */
/******************************************************************************/

BEGIN_MSG_MAP(spi_tclPosDataClientHandler, ahl_tclBaseWork)
   // Add your ON_MESSAGE_SVCDATA() macros here to define which corresponding 
   // method should be called on receiving a specific message.
   ON_MESSAGE_SVCDATA(POSFI_C_U16_POSITION,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnPositionStatus)
   ON_MESSAGE_SVCDATA(POSFI_C_U16_DEADRECKONINGPOSITION,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnDeadreckoningPositionStatus)
END_MSG_MAP()


/******************************************************************************/
/*                                                                            */
/* METHODS                                                                    */
/*                                                                            */
/******************************************************************************/

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/*******************************************************************************
* FUNCTION: spi_tclPosDataClientHandler::
*             spi_tclPosDataClientHandler(ahl_tclBaseOneThreadApp* poMainAppl)
*******************************************************************************/
spi_tclPosDataClientHandler::spi_tclPosDataClientHandler(ahl_tclBaseOneThreadApp* poMainApp)
   : ahl_tclBaseOneThreadClientHandler(
     poMainApp,                           /* Application Pointer          */
     CCA_C_U16_SRV_POSITION,              /* ID of used Service           */
     POSFI_C_U16_SERVICE_MAJORVERSION,    /* MajorVersion of used Service */
     POSFI_C_U16_SERVICE_MINORVERSION),   /* MinorVersion of used Service */
     m_poMainApp(poMainApp)
{
   ETG_TRACE_USR1(("spi_tclPosDataClientHandler::spi_tclPosDataClientHandler entered \n"));
}

/*******************************************************************************
* FUNCTION: spi_tclPosDataClientHandler::
*             ~spi_tclPosDataClientHandler(t_Void)
*******************************************************************************/
spi_tclPosDataClientHandler::
   ~spi_tclPosDataClientHandler()
{
   ETG_TRACE_USR1(("spi_tclPosDataClientHandler::~spi_tclPosDataClientHandler entered \n"));
   m_poMainApp = NULL;
}

/*******************************************************************************
* FUNCTION: t_Void spi_tclPosDataClientHandler::vOnServiceAvailable()
*******************************************************************************/
t_Void spi_tclPosDataClientHandler::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("spi_tclPosDataClientHandler::vOnServiceAvailable entered \n"));
}

/*******************************************************************************
* FUNCTION: t_Void spi_tclPosDataClientHandler::vOnServiceUnavailable(
*******************************************************************************/
t_Void spi_tclPosDataClientHandler::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("spi_tclPosDataClientHandler::vOnServiceUnavailable entered \n"));
}

/*******************************************************************************
** FUNCTION:   spi_tclPosDataClientHandler::vOnPositionStatus(amt_tclServiceData...
*******************************************************************************/
t_Void spi_tclPosDataClientHandler::vOnPositionStatus(amt_tclServiceData* poMessage)
{

	/*lint -esym(40,fvOnGpsData)fvOnGpsData Undeclared identifier */
   m_lock.s16Lock();
   spi_tPositionStatus oPositionStatus(*poMessage, POSFI_C_U16_SERVICE_MAJORVERSION);

   if (
      (true == oPositionStatus.bIsValid())
      &&
      (NULL != m_rLocDataCallbacks.fvOnGpsData)
      )
   {
      //! Create GPS data structure by extracting details from the FI data object
      trGPSData rGpsData;

      rGpsData.bLatitudeAvailable = true;
      rGpsData.bLongitudeAvailable = true;
      rGpsData.bHeadingAvailable = true;
      rGpsData.bSpeedAvailable = true;
      rGpsData.bAltitudeAvailable = true;

      rGpsData.s32Latitude = static_cast<t_S32> (oPositionStatus.PositionElementBlock.PositionWGS84.Latitude.s32Value);
      rGpsData.s32Longitude = static_cast<t_S32> (oPositionStatus.PositionElementBlock.PositionWGS84.Longitude.s32Value);
      rGpsData.dLatLongResolution = codLatLongResolution;
      rGpsData.dHeadingResolution = codHeadingResolution;
      rGpsData.dHeading = static_cast<t_S32> (oPositionStatus.PositionElementBlock.Heading.u8Value);
      //TODO - For PSA, Heading should be converted to values as per GM(TravelMap) POS_FI values.
      rGpsData.s16Speed = static_cast<t_S16> (oPositionStatus.PositionElementBlock.Speed.s16Value);
      rGpsData.s32Altitude = static_cast<t_S32> (oPositionStatus.Height.s32Value);
      rGpsData.PosixTime = static_cast<t_PosixTime> (oPositionStatus.SensorPosixTime.PosixTime.Time);
      rGpsData.u16ExactTime = static_cast<t_U16> (oPositionStatus.SensorPosixTime.ExactTime);
      rGpsData.enHeadingDir = e8ANTICLOCKWISE;

      ETG_TRACE_USR4(("spi_tclPosDataClientHandler::vOnPositionStatus: "
            "Latitude = %d, Longitude = %d, Heading = %f, Heading Direction = %d, Speed = %d, "
            "Altitude = %d, PosixTime = 0x%x, ExactTime(ms) = %d \n",
            rGpsData.s32Latitude, rGpsData.s32Longitude, rGpsData.dHeading,ETG_ENUM(HEADING_DIR,rGpsData.enHeadingDir),
            rGpsData.s16Speed, rGpsData.s32Altitude, rGpsData.PosixTime, rGpsData.u16ExactTime));

      //! Forward data to subscriber
      m_rLocDataCallbacks.fvOnGpsData(rGpsData);
   }
   else 
   {
      ETG_TRACE_ERR(("spi_tclPosDataClientHandler::vOnPositionStatus: Message extraction failed! \n"));
   }
   m_lock.vUnlock();
}

/*******************************************************************************
** FUNCTION:   spi_tclPosDataClientHandler::vOnDeadreckoningPositionStatus(...
*******************************************************************************/
t_Void spi_tclPosDataClientHandler::vOnDeadreckoningPositionStatus(amt_tclServiceData* poMessage)
{
   m_lock.s16Lock();
   spi_tDRPositionStatus oDeadReckoningPositionStatus(*poMessage, POSFI_C_U16_SERVICE_MAJORVERSION);

   if (
      (true == oDeadReckoningPositionStatus.bIsValid())
      &&
      (NULL != m_rLocDataCallbacks.fvOnGpsData)
      )
   {
      ETG_TRACE_USR4(("spi_tclPosDataClientHandler::vOnDeadreckoningPositionStatus: DR Status Type %d \n",
            oDeadReckoningPositionStatus.Status.enType));

      if (fi_tcl_e8_DeadReckoningPositionState::FI_EN_UNKNOWN != oDeadReckoningPositionStatus.Status.enType)
      {
         //! Create GPS data structure by extracting details from the FI data object
         trGPSData rGpsData;

         rGpsData.bLatitudeAvailable = true;
         rGpsData.bLongitudeAvailable = true;
         rGpsData.bHeadingAvailable = true;
         rGpsData.bSpeedAvailable = true;
         rGpsData.bAltitudeAvailable = true;

         rGpsData.s32Latitude = static_cast<t_S32> (oDeadReckoningPositionStatus.Position.Latitude.s32Value);
         rGpsData.s32Longitude = static_cast<t_S32> (oDeadReckoningPositionStatus.Position.Longitude.s32Value);
         rGpsData.dLatLongResolution = codLatLongResolution;
         rGpsData.dHeadingResolution = codDRHeadingResolution;
         rGpsData.dHeading = static_cast<t_S32> (oDeadReckoningPositionStatus.Heading.u16Value);
         //TODO - For PSA, Heading should be converted to values as per GM(TravelMap) POS_FI values.
         rGpsData.s16Speed = static_cast<t_S16> (oDeadReckoningPositionStatus.Speed.s16Value);
         rGpsData.s32Altitude = static_cast<t_S32> (oDeadReckoningPositionStatus.Height.s32Value);
         rGpsData.PosixTime = static_cast<t_PosixTime> (oDeadReckoningPositionStatus.SensorPosixTime.PosixTime.Time);
         rGpsData.u16ExactTime = static_cast<t_U16> (oDeadReckoningPositionStatus.SensorPosixTime.ExactTime);
         rGpsData.enHeadingDir = e8CLOCKWISE;
         if ((oDeadReckoningPositionStatus.CurrentlyUsedSensors.bABS_WheelCounter_RearLeft()) &&
               (oDeadReckoningPositionStatus.CurrentlyUsedSensors.bABS_WheelCounter_RearRight()))
         {
            rGpsData.enSensorType = e8SENSOR_TYPE_COMBINED_LEFT_RIGHT_WHEEL;
         }
         /*rGpsData.enTransmissionStatus = (0 <= oDeadReckoningPositionStatus.Speed.s16Value) ?
               (e8TRANSMISSION_DRIVING_FWD) : (e8TRANSMISSION_REVERSE);*/
         rGpsData.s16TurnRate = oDeadReckoningPositionStatus.Turnrate.s16Value;
         rGpsData.s16Acceleration = oDeadReckoningPositionStatus.Acceleration.s16Value;

         //! Forward data to subscriber
         m_rLocDataCallbacks.fvOnGpsData(rGpsData);
      }//if (fi_tcl_e8_DeadReckoningPositionState::FI_EN_UNKNOWN != oDeadReckoningPositionStatus.Status.enType)
   }
   else
   {
      ETG_TRACE_ERR(("spi_tclPosDataClientHandler::vOnDeadreckoningPositionStatus: Message extraction failed! \n"));
   }
   m_lock.vUnlock();
}
/**************************************************************************
** FUNCTION:  t_Void spi_tclPosDataClientHandler::vRegisterForProperties()
**************************************************************************/
t_Void spi_tclPosDataClientHandler::vRegisterForProperties(
      tenLocationDataType enLocDataType,
      trLocationDataCallbacks rLocDataCallbacks)
{
   ETG_TRACE_USR1(("spi_tclPosDataClientHandler::vRegisterForProperties:"
         " entered with LocationData type = %d \n",
         ETG_ENUM(LOCATION_DATA_TYPE, enLocDataType)));
   SPI_INTENTIONALLY_UNUSED(enLocDataType);

   //! Register for properties
   tU16 u16FuncID = POSFI_C_U16_DEADRECKONINGPOSITION;
   ETG_TRACE_USR4(("vRegisterForProperties: Registered FunctionID = 0x%x \n", u16FuncID));

   //! Store call-backs for sending data before registering to property
   m_rLocDataCallbacks = rLocDataCallbacks;

   vAddAutoRegisterForProperty(u16FuncID);
}

/**************************************************************************
** FUNCTION:  t_Void spi_tclPosDataClientHandler::vUnregisterForProperties()
**************************************************************************/
t_Void spi_tclPosDataClientHandler::vUnregisterForProperties(
      tenLocationDataType enLocDataType)
{
   ETG_TRACE_USR1(("spi_tclPosDataClientHandler::vUnregisterForProperties entered"));

	//! Unregister is triggered from connection manager context,
	//! which needs to be synchronized since there is a chance of
	//! simultaneous access.
   m_lock.s16Lock();

   ETG_TRACE_USR1(("spi_tclPosDataClientHandler::vUnregisterForProperties:"
         " entered with LocationData type = %d \n",
         ETG_ENUM(LOCATION_DATA_TYPE, enLocDataType)));
   SPI_INTENTIONALLY_UNUSED(enLocDataType);

   //! Unregister for properties
   tU16 u16FuncID = POSFI_C_U16_DEADRECKONINGPOSITION;
   ETG_TRACE_USR4(("vUnregisterForProperties: Unregistered FunctionID = 0x%x \n", u16FuncID));

   vRemoveAutoRegisterForProperty(u16FuncID);

   //! Clear callbacks
   m_rLocDataCallbacks = rEmptyLocCallbacks;
   m_lock.vUnlock();
}

//lint –restore

