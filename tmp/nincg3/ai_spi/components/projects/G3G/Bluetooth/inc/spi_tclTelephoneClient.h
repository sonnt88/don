/*!
*******************************************************************************
* \file              spi_tclTelephoneClient.h
* \brief             Telephone Client handler class
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Telephone Client handler class
COPYRIGHT:      &copy; RBEI

HISTORY:
 Date       |  Author                           | Modifications
 06.03.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version

\endverbatim
******************************************************************************/

#ifndef _SPI_TCLTELEPHONECLIENT_H_
#define _SPI_TCLTELEPHONECLIENT_H_

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
//!Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

//!Include common fi interface
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

//!Include Telephone FI types
#define MOST_FI_S_IMPORT_INTERFACE_MOST_TELFI_FUNCTIONIDS
#define MOST_FI_S_IMPORT_INTERFACE_MOST_TELFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_TELFI_ERRORCODES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_TELFI_SERVICEINFO
#define MOST_FI_S_IMPORT_INTERFACE_MOST_DEVPRJFI_SERVICEINFO
#include "most_fi_if.h"

#include "SPITypes.h"
#include "spi_tclTelephonePolicyBase.h"


/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
typedef most_telfi_tclMsgBaseMessage telfi_MsgBase;

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/* Forward Declarations. */

/*!
* \class spi_tclTelephoneClient
* \brief Telephone client handler class.
*/
class spi_tclTelephoneClient
   : public ahl_tclBaseOneThreadClientHandler, public spi_tclTelephonePolicyBase
{

public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclTelephoneClient::spi_tclTelephoneClient(ahl_tclBaseOneThreadApp...)
   **************************************************************************/
   /*!
   * \fn      spi_tclTelephoneClient(ahl_tclBaseOneThreadApp* poMainAppl,
   *             t_U16 u16ServiceID)
   * \brief   Overloaded Constructor
   * \param   [IN] poMainAppl : Pointer to main CCA application
   **************************************************************************/
   spi_tclTelephoneClient(ahl_tclBaseOneThreadApp* poMainAppl, t_U16 u16ServiceID);

   /***************************************************************************
   ** FUNCTION:  spi_tclTelephoneClient::~spi_tclTelephoneClient()
   **************************************************************************/
   /*!
   * \fn      ~spi_tclTelephoneClient()
   * \brief   Destructor
   **************************************************************************/
   virtual ~spi_tclTelephoneClient();

   /**************************************************************************
   * Overriding ahl_tclBaseOneThreadService methods.
   **************************************************************************/

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclTelephoneClient::vOnServiceAvailable();
   **************************************************************************/
   /*!
   * \fn      vOnServiceAvailable()
   * \brief   This method is called by the framework if the service of our
   *          server becomes available, e.g. server has been started.
   * \param   None
   **************************************************************************/
   virtual tVoid vOnServiceAvailable();

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclTelephoneClient::vOnServiceUnavailable();
   **************************************************************************/
   /*!
   * \fn      vOnServiceUnavailable()
   * \brief   This method is called by the framework if the service of our
   *          server becomes unavailable, e.g. server has been shut down.
   * \param   None
   **************************************************************************/
   virtual tVoid vOnServiceUnavailable();

   /**************************************************************************
   * Overriding spi_tclTelephonePolicyBase methods.
   **************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclTelephoneClient::vRegisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers for interested properties to Telephone FBlock.
   **************************************************************************/
   virtual t_Void vRegisterForProperties();

	/***************************************************************************
	** FUNCTION:  t_Void spi_tclTelephoneClient::vUnregisterForProperties()
	**************************************************************************/
	/*!
	* \fn      vUnregisterForProperties()
	* \brief   Registers for interested properties to Telephone FBlock.
	**************************************************************************/
   virtual t_Void vUnregisterForProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclTelephoneClient::vRegisterCallbacks()
   ***************************************************************************/
   /*!
   * \fn      vRegisterCallbacks(trTelephoneCallbacks rTelephoneCallbacks)
   * \brief   Interface to register for Telephone events callbacks
   *          Optional interface to be implemented.
   * \param   [IN] rTelephoneCallbacks: Callbacks structure
   * \retval  None
   **************************************************************************/
   virtual t_Void vRegisterCallbacks(trTelephoneCallbacks rTelephoneCallbacks);
   
   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclTelephoneClient::vTriggerAcceptCall(
              t_U16 u16CallInstance)
   ***************************************************************************/
   /*!
   * \fn      vTriggerAcceptCall(t_U16 u16CallInstance)
   * \brief   Method to triggger call accept.
   * \param   u16CallInstance: [IN]  Call Instance
   * \retval  NONE
   **************************************************************************/
   t_Void vTriggerAcceptCall(t_U16 u16CallInstance);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclTelephoneClient::vTriggerHangUpCall(
   t_U16 u16CallInstance)
   ***************************************************************************/
   /*!
   * \fn      vTriggerHangUpCall(t_U16 u16CallInstance)
   * \brief   Method to triggger call hangup.
   * \param   u16CallInstance: [IN]  Call Instance
   * \retval  NONE
   **************************************************************************/
   t_Void vTriggerHangUpCall(t_U16 u16CallInstance);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclTelephoneClient::vTriggerRejectCall(
   t_U16 u16CallInstance)
   ***************************************************************************/
   /*!
   * \fn      vTriggerRejectCall(t_U16 u16CallInstance)
   * \brief   Method to triggger call reject.
   * \param   u16CallInstance: [IN]  Call Instance
   * \retval  NONE
   **************************************************************************/
   t_Void  vTriggerRejectCall(t_U16 u16CallInstance);
   
   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclTelephoneClient::vTriggerCancelCall(
   t_U16 u16CallInstance)
   ***************************************************************************/
   /*!
   * \fn      vTriggerCancelCall(t_U16 u16CallInstance)
   * \brief   Method to triggger call cancel.
   * \param   u16CallInstance: [IN]  Call Instance
   * \retval  NONE
   **************************************************************************/
   t_Void  vTriggerCancelCall(t_U16 u16CallInstance);

   /***************************************************************************
   * Message map definition macro
   ***************************************************************************/
   DECLARE_MSG_MAP(spi_tclTelephoneClient)


private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   * ! Handler method declarations used by message map.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclTelephoneClient::vOnStatusCallStatusNotice(...)
   **************************************************************************/
   /*!
   * \fn      vOnStatusCallStatusNotice(amt_tclServiceData* poMessage)
   * \brief   Called by framework when DeviceListStatus property update message is
   *          sent by Telephone FBlock.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnStatusCallStatusNotice(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclTelephoneClient::vOnMRAcceptCall(amt_tclServiceData* 
                        poMessage)const
   **************************************************************************/
   /*!
   * \fn      vOnMRAcceptCall(amt_tclServiceData* poMessage)const
   * \brief   Called by framework when there is a result for the Call Accept Trigger
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   t_Void vOnMRAcceptCall(amt_tclServiceData* poMessage)const;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclTelephoneClient::vOnMRHangUpCall(amt_tclServiceData* 
                        poMessage)const
   **************************************************************************/
   /*!
   * \fn      vOnMRHangUpCall(amt_tclServiceData* poMessage)const
   * \brief   Called by framework when there is a result for the Call HangUp Trigger
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   t_Void vOnMRHangUpCall(amt_tclServiceData* poMessage)const;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclTelephoneClient::vOnMRRejectCall(amt_tclServiceData* 
                        poMessage)const
   **************************************************************************/
   /*!
   * \fn      vOnMRRejectCall(amt_tclServiceData* poMessage)const
   * \brief   Called by framework when there is a result for the Call Reject Trigger
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   t_Void vOnMRRejectCall(amt_tclServiceData* poMessage)const;

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclTelephoneClient::vOnMRCancelCall(amt_tclServiceData*
                         poMessage)const
    **************************************************************************/
    /*!
    * \fn      vOnMRCancelCall(amt_tclServiceData* poMessage)const
    * \brief   Called by framework when there is a result for the Call Cancel Trigger
    * \param   [IN] poMessage : Pointer to message
    **************************************************************************/
    t_Void vOnMRCancelCall(amt_tclServiceData* poMessage)const;

   /**************************************************************************
   * ! Other methods
   **************************************************************************/

   /**************************************************************************
   ** FUNCTION:  spi_tclTelephoneClient::spi_tclTelephoneClient()
   **************************************************************************/
   /*!
   * \fn      spi_tclTelephoneClient()
   * \brief   Default Constructor, will not be implemented.
   *          NOTE: This is a technique to disable the Default Constructor for
   *          this class. So if an attempt for the constructor is made compiler
   *          complains.
   **************************************************************************/
   spi_tclTelephoneClient();

   /**************************************************************************
   ** FUNCTION:  spi_tclTelephoneClient::spi_tclTelephoneClient(const...
   **************************************************************************/
   /*!
   * \fn      spi_tclTelephoneClient(const spi_tclTelephoneClient& oClient)
   * \brief   Copy Consturctor, will not be implemented.
   *          Avoids Lint Prio 3 warning: Info 1732: new in constructor for
   *          class'spi_tclTelephoneClient' which has no Copy Consturctor.
   *          NOTE: This is a technique to disable the Copy Consturctor for this
   *          class. So if an attempt for the copying is made linker complains.
   * \param   [IN] poMessage : Property to be set.
   **************************************************************************/
   spi_tclTelephoneClient(const spi_tclTelephoneClient& oClient);

   /**************************************************************************
   ** FUNCTION:  spi_tclTelephoneClient::spi_tclTelephoneClient& operator=(...
   **************************************************************************/
   /*!
   * \fn      spi_tclTelephoneClient& operator=(
   *          const spi_tclTelephoneClient& oClient)
   * \brief   Assingment Operater, will not be implemented.
   *          Avoids Lint Prio 3 warning: Info 1732: new in constructor for
   *          class 'spi_tclTelephoneClient' which has no assignment operator.
   *          NOTE: This is a technique to disable the assignment operator for this
   *          class. So if an attempt for the assignment is made compiler complains.
   **************************************************************************/
   spi_tclTelephoneClient& operator=(const spi_tclTelephoneClient& oClient);

   /**************************************************************************
   ** FUNCTION:  tBool spi_tclTelephoneClient::bPostMethodStart(const...
   **************************************************************************/
   /*!
   * \fn      bPostMethodStart(const btset_FiMsgBase& rfcooBTSetMS,
   *           t_U32 u32CmdCounter)
   * \brief   Posts a BT settings MethodStart message.
   * \param   [IN] rfcooBTSetMS : BT settings Method Start message type
   * \param   [IN] u32CmdCounter : Command counter value to be set in the message.
   * \retval  tBool: TRUE - if message posting is successful, else FALSE.
   **************************************************************************/
   t_Bool bPostMethodStart(
      const telfi_MsgBase& rfcooTelFIMS) const;

   /***************************************************************************
   * ! Data members
   ***************************************************************************/

   /***************************************************************************
   ** Main Application pointer
   ***************************************************************************/
   ahl_tclBaseOneThreadApp*   m_poMainAppl;

   /***************************************************************************
   ** Telephone callbacks structure
   ***************************************************************************/
   trTelephoneCallbacks    m_rTelCallbacks;

};

#endif // _SPI_TCLTELEPHONECLIENT_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
