/*!
*******************************************************************************
* \file              spi_tclBluetoothClient.h
* \brief             Bluetooth Settings Client handler class
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Bluetooth Settings Client handler class
COPYRIGHT:      &copy; RBEI

HISTORY:
 Date       |  Author                           | Modifications
 21.02.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 21.05.2014 |  Ramya Murthy (RBEI/ECP2)         | Revised logic for storing DeviceList
                                                  and validation of service responses.
 04.06.2014 |  Ramya Murthy (RBEI/ECP2)         | VehicleBTAddress implementation
 22.09.2014 |  Ramya Murthy (RBEI/ECP2)         | Implemented locks for DeviceList and
                                                  ChngDevRequestsList maps
 28.10.2014 |  Ramya Murthy (RBEI/ECP2)         | Implementation for setting BluetoothAudioSource
                                                  (Fix for SUZUKI-18263)
 19.05.2015 |  Ramya Murthy (RBEI/ECP2)         | Adaptation to DeviceListExtended property
 18.08.2015 |  Ramya Murthy (RBEI/ECP2)         | Adaptation to new BTSettings interfaces for AndroidAuto

\endverbatim
******************************************************************************/

#ifndef _SPI_TCLBLUETOOTHCLIENT_H_
#define _SPI_TCLBLUETOOTHCLIENT_H_

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include <map>
#include <queue>

//!Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

//!Include common fi interface
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

//!Include Bluetooth Settings FI type defines
#define MOST_FI_S_IMPORT_INTERFACE_MOST_BTSETFI_FUNCTIONIDS
#define MOST_FI_S_IMPORT_INTERFACE_MOST_BTSETFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_BTSETFI_ERRORCODES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_BTSETFI_SERVICEINFO
#include "most_fi_if.h"

#include "XFiObjHandler.h"
using namespace shl::msgHandler;

#include "SPITypes.h"
#include "spi_BluetoothTypedefs.h"
#include "spi_tclBluetoothPolicyBase.h"
#include "Lock.h"


/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
//! Type defines for iterators to DeviceList map.
typedef std::map<tU8, trBluetoothDeviceInfo>::iterator         tBTDevListMapItr;
typedef std::map<tU8, trBluetoothDeviceInfo>::const_iterator   tBTDevListMapConstItr;

typedef std::map<tU8, trBTChangeDeviceRequestInfo>::iterator         tBTChngDevReqMapItr;
typedef std::map<tU8, trBTChangeDeviceRequestInfo>::const_iterator   tBTChngDevReqMapConstItr;

//! BT Settings FI types
typedef most_btsetfi_tclMsgBaseMessage                   btset_FiMsgBase;
typedef most_fi_tcl_BTSetDateTimeStamp                   btset_tDateTimestamp;
typedef most_fi_tcl_e8_BTSetConnectionResult::tenType    btset_tenBTConnectionResult;

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/* Forward Declarations. */

/*!
* \class spi_tclBluetoothClient
* \brief Bluetooth Settings client handler class that realizes the BluetoothPolicy interface.
*/
class spi_tclBluetoothClient
   : public ahl_tclBaseOneThreadClientHandler, public spi_tclBluetoothPolicyBase
{

public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::spi_tclBluetoothClient(ahl_tclBaseOneThreadApp...)
   **************************************************************************/
   /*!
   * \fn      spi_tclBluetoothClient(ahl_tclBaseOneThreadApp* poMainAppl,
   *             t_U16 u16ServiceID)
   * \brief   Overloaded Constructor
   * \param   [IN] poMainAppl : Pointer to main CCA application
   * \param   [IN] u16ServiceID : Unique ID of Bluetooth Service
   **************************************************************************/
   spi_tclBluetoothClient(ahl_tclBaseOneThreadApp* poMainAppl,
         t_U16 u16ServiceID);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::~spi_tclBluetoothClient()
   **************************************************************************/
   /*!
   * \fn      ~spi_tclBluetoothClient()
   * \brief   Destructor
   **************************************************************************/
   virtual ~spi_tclBluetoothClient();

   /**************************************************************************
   * Overriding ahl_tclBaseOneThreadService methods.
   **************************************************************************/

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclBluetoothClient::vOnServiceAvailable();
   **************************************************************************/
   /*!
   * \fn      vOnServiceAvailable()
   * \brief   This method is called by the framework if the service of our
   *          server becomes available, e.g. server has been started.
   * \param   None
   **************************************************************************/
   virtual tVoid vOnServiceAvailable();

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclBluetoothClient::vOnServiceUnavailable();
   **************************************************************************/
   /*!
   * \fn      vOnServiceUnavailable()
   * \brief   This method is called by the framework if the service of our
   *          server becomes unavailable, e.g. server has been shut down.
   * \param   None
   **************************************************************************/
   virtual tVoid vOnServiceUnavailable();

   /**************************************************************************
   * Overriding spi_tclBluetoothPolicyBase methods.
   **************************************************************************/

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclBluetoothClient::vRegisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers for interested properties to Bluetooth Service.
   **************************************************************************/
   virtual tVoid vRegisterForProperties();

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclBluetoothClient::vUnregisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Registers for interested properties to Bluetooth Service.
   **************************************************************************/
   virtual tVoid vUnregisterForProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothClient::vRegisterCallbacks()
   ***************************************************************************/
   /*!
   * \fn      vRegisterCallbacks(trBluetoothCallbacks rBTRespCallbacks)
   * \brief   Interface to register for BT connection callbacks
   *          Optional interface to be implemented.
   * \param   [IN] rBTRespCallbacks: Callbacks structure
   * \retval  None
   **************************************************************************/
   virtual t_Void vRegisterCallbacks(trBluetoothCallbacks rBTRespCallbacks);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothPolicyBase::vRegisterPairingInfoCallbacks()
   ***************************************************************************/
   /*!
   * \fn      vRegisterPairingInfoCallbacks(trBluetoothPairingCallbacks rBTPairInfoCallbacks)
   * \brief   Interface to register for BT pairing info callbacks.
   *          Optional interface to be implemented.
   * \param   [IN] rBTPairInfoCallbacks: Callbacks structure
   * \retval  None
   **************************************************************************/
   virtual t_Void vRegisterPairingInfoCallbacks(trBluetoothPairingCallbacks rBTPairInfoCallbacks);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothClient::vRegisterDeviceNameCallback()
   ***************************************************************************/
   /*!
   * \fn      vRegisterDeviceNameCallback(trBTDeviceNameCbInfo rBTDeviceNameCbInfo)
   * \brief   Interface to register for device name callback of a specific device
   *          Optional interface to be implemented.
   * \param   [IN] rBTDeviceNameCbInfo: Callback info structure
   * \retval  None
   **************************************************************************/
   virtual t_Void vRegisterDeviceNameCallback(trBTDeviceNameCbInfo rBTDeviceNameCbInfo);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothClient::bSendPairingAction(t_String...)
   ***************************************************************************/
   /*!
   * \fn      bSendPairingAction(tenBTPairingAction enAction)
   * \brief   Request from Bluetooth Manager to Bluetooth service for sending
   *          Pairing response.
   *          Optional interface to be implemented.
   * \param   [IN] enAction: Action for pairing
   * \retval  Bool value: TRUE - if response is sent successfully, else FALSE.
   **************************************************************************/
   virtual t_Bool bSendPairingAction(tenBTPairingAction enAction);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothClient::bConnectBTDevice(const...)
   ***************************************************************************/
   /*!
   * \fn      bConnectBTDevice(const t_String& rfcoszDeviceBTAddress)
   * \brief   Request from the Bluetooth Manager to the Bluetooth service
   *          to connect a BT device.
   *          Optional interface to be implemented.
   * \param   [IN] rfcoszDeviceBTAddress: BT address of device to be connected.
   * \retval  t_Bool: true - if Connect request is sent successully to BT Service, else false.
   **************************************************************************/
   virtual t_Bool bConnectBTDevice(const t_String& rfcoszDeviceBTAddress);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothClient::bDisconnectBTDevice(const...)
   ***************************************************************************/
   /*!
   * \fn      bDisconnectBTDevice(const t_String& rfcoszDeviceBTAddress)
   * \brief   Request from the Bluetooth Manager to the Bluetooth service
   *          to disconnect a BT device.
   *          Optional interface to be implemented.
   * \param   [IN] rfcoszDeviceBTAddress: BT address of device to be disconnected.
   * \retval  t_Bool: true - if Disconnect request is sent successully to BT Service, else false.
   **************************************************************************/
   virtual t_Bool bDisconnectBTDevice(const t_String& rfcoszDeviceBTAddress);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothClient::bBlockBTDevice(...)
   ***************************************************************************/
   /*!
   * \fn      bBlockBTDevice(tenBTDeviceBlockType enBlockType,
   *             const t_String& rfcoszDeviceBTAddress)
   * \brief   Request from the Bluetooth Manager to the Bluetooth service
   *          to block connection/pairing of one or more BT devices.
   *          Optional interface to be implemented.
   * \param   [IN] enBlockType: Type of blocking to be performed
   * \param   [IN] rfcoszDeviceBTAddress: BT device address of device to be blocked
   *               (ignored if Blocking type is for all devices)
   * \retval  t_Bool: true - if Block request is sent successully to BT Service, else false.
   **************************************************************************/
   virtual t_Bool bBlockBTDevice(tenBTDeviceBlockType enBlockType,
         const t_String& rfcoszDeviceBTAddress);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothClient::bUnblockBTDevice(...)
   ***************************************************************************/
   /*!
   * \fn      bUnblockBTDevice(tenBTDeviceUnblockType enUnblockType,
   *             const t_String& rfcoszDeviceBTAddress)
   * \brief   Request from the Bluetooth Manager to the Bluetooth service
   *          to unblock connection/pairing of one or more BT devices.
   *          Optional interface to be implemented.
   * \param   [IN] enUnblockType: Type of unblocking to be performed
   * \param   [IN] rfcoszDeviceBTAddress: BT device address of device to be unblocked
   *               (ignored if Blocking type is for all devices)
   * \retval  t_Bool: true - if Unblock request is sent successully to BT Service, else false.
   **************************************************************************/
   virtual t_Bool bUnblockBTDevice(tenBTDeviceUnblockType enUnblockType,
         const t_String& rfcoszDeviceBTAddress);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothClient::bGetPairingStatus(const...)
   ***************************************************************************/
   /*!
   * \fn      bGetPairingStatus(const t_String& rfcoszDeviceBTAddress)
   * \brief   Request from the Bluetooth Manager to the Bluetooth service
   *          for the pairing status of a BT device.
   *          Optional interface to be implemented.
   * \param   [IN] rfcoszDeviceBTAddress: BT address of device.
   * \retval  Bool value: TRUE - if Pairing is required, else FALSE
   **************************************************************************/
   virtual t_Bool bGetPairingStatus(const t_String& rfcoszDeviceBTAddress);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothClient::bGetConnectionStatus(const...)
   ***************************************************************************/
   /*!
   * \fn      bGetConnectionStatus(const t_String& rfcoszDeviceBTAddress)
   * \brief   Request from the Bluetooth Manager to the Bluetooth service
   *          for the connection status of a BT device.
   *          Optional interface to be implemented.
   * \param   [IN] rfcoszDeviceBTAddress: BT address of device.
   * \retval  Bool value: TRUE - if Device is connected(active), else FALSE
   **************************************************************************/
   virtual t_Bool bGetConnectionStatus(const t_String& rfcoszDeviceBTAddress);

   /***************************************************************************
   ** FUNCTION:  t_String spi_tclBluetoothClient::szGetConnectedDeviceBTAddress()
   ***************************************************************************/
   /*!
   * \fn      szGetConnectedDeviceBTAddress()
   * \brief   Request from the Bluetooth Manager to the Bluetooth service
   *          for the BT address of the currently connected BT device.
   *          Optional interface to be implemented.
   * \retval  t_String : BT address of Connected(Active) device.
   *            If no device is connected, returns NULL string.
   **************************************************************************/
   virtual t_String szGetConnectedDeviceBTAddress();

   /***************************************************************************
   ** FUNCTION:  t_U32 spi_tclBluetoothClient::u32GetBTDeviceHandle(const...)
   ***************************************************************************/
   /*!
   * \fn      u32GetBTDeviceHandle(const t_String& rfcoszDeviceBTAddress)
   * \brief   Request from Bluetooth Manager to the Bluetooth Client
   *          for the BT DeviceHandle of a device.
   *          Optional interface to be implemented.
   * \param   [IN] rfcoszDeviceBTAddress: BT address of device.
   * \retval  t_U32 : BT device handle of device with BT address = rfcoszDeviceBTAddress.
   *          If the device is not found in BT DeviceList, zero is returned.
   **************************************************************************/
   virtual t_U32 u32GetBTDeviceHandle(const t_String& rfcoszDeviceBTAddress);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothClient::vGetVehicleBTAddress(const...)
   ***************************************************************************/
   /*!
   * \fn      vGetVehicleBTAddress()
   * \brief   Function to get the vehicle BT address
   * \param   szVehicleBtId : [IN]Vehicle BT address
   * \retval  NONOE
   **************************************************************************/
   virtual t_Void vGetVehicleBTAddress(t_String &szVehicleBtId);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothClient::vSetBTProfile(tenBTProfile enBTProfile)
   ***************************************************************************/
   /*!
   * \fn      vSetBTProfile(tenBTProfile enBTProfile)
   * \brief   Interface to enable/disable BT profiles.
   *          Optional interface to be implemented.
   * \param   enBTProfile : [IN] Identifies BT profile to be set
   * \retval  NONE
   **************************************************************************/
   virtual t_Void vSetBTProfile(tenBTProfile enBTProfile);

   /***************************************************************************
   ** FUNCTION:  t_String spi_tclBluetoothClient::szGetBTDeviceName()
   ***************************************************************************/
   /*!
   * \fn      szGetBTDeviceName()
   * \brief   Interface to fetch name of a BT device.
   * \param   [IN] rfcszDeviceBTAddress: BT address of device.
   * \retval  t_String : BT device name of requested device.
   *            If device name is not available, returns empty string.
   **************************************************************************/
   virtual t_String szGetBTDeviceName(const t_String& rfcszDeviceBTAddress);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothClient::bConfigureHUState(...)
   ***************************************************************************/
   /*!
   * \fn      bConfigureHUState()
   * \brief   Interface to configure HU to pairable and/or connectable state
   *          for a specific device.
   *          Optional interface to be implemented.
   * \param   [IN] enPairableMode: Indicates desired pairable state
   * \param   [IN] enConnectableMode: Indicates desired connectable state
   * \param   [IN] rfcszDeviceBTAddress: BT address of device.
   * \retval  t_Bool : True - if request to configure HU is sent successfully.
   *              False if:
   *              (1) Device address is invalid.
   *              (2) Pairing is already onoing
   **************************************************************************/
   virtual t_Bool bConfigureHUState(tenBTLocalModeType enPairableMode,
         tenBTLocalModeType enConnectableMode, const t_String& rfcszDeviceBTAddress);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothClient::bSwitchBTOnOff(...)
   ***************************************************************************/
   /*!
   * \fn      bSwitchBTOnOff()
   * \brief   Interface to Switch On/Off Bluetooth in HU.
   *          Optional interface to be implemented.
   * \param   [IN] bBluetoothOnOff:
   *              True - If BT is to be switched On
   *              False - If BT is to be switched Off
   * \retval  t_Bool : True - if request to configure HU is sent successfully,
   *              else false.
   **************************************************************************/
   virtual t_Bool bSwitchBTOnOff(t_Bool bBluetoothOnOff);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothClient::bGetBTOnStatus(...)
   ***************************************************************************/
   /*!
   * \fn      bGetBTOnStatus()
   * \brief   Interface to get Bluetooth On/Off status in HU.
   *          Optional interface to be implemented.
   * \param   None
   * \retval  t_Bool : True - if BT is On, else false.
   **************************************************************************/
   virtual t_Bool bGetBTOnStatus();

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   * ! Handler method declarations used by message map.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclBluetoothClient::vOnStatusDeviceListExtended(...)
   ***************************************************************************/
   /*!
   * \fn      vOnStatusDeviceListExtended(amt_tclServiceData* poMessage)
   * \brief   Called by framework when DeviceListStatus property update message is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   * \retval  None
   **************************************************************************/
   tVoid vOnStatusDeviceListExtended(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclBluetoothClient::vOnMRGetDeviceInfo(amt_...
   **************************************************************************/
   /*!
   * \fn      vOnMRChangeDevState(amt_tclServiceData* poMessage)
   * \brief   Called by framework when GetDeviceInfo method result is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   * \retval  None
   **************************************************************************/
   tVoid vOnMRGetDeviceInfo(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclBluetoothClient::vOnMRChangeDevState(amt_...
   **************************************************************************/
   /*!
   * \fn      vOnMRChangeDevState(amt_tclServiceData* poMessage)
   * \brief   Called by framework when ChangeDeviceState method result is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   * \retval  None
   **************************************************************************/
   tVoid vOnMRChangeDevState(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vOnErrorChangeDevState(amt_tclServiceData...
   **************************************************************************/
   /*!
   * \fn      vOnErrorChangeDevState(amt_tclServiceData* poMessage)
   * \brief   Called by framework when ChangeDeviceState error is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vOnErrorChangeDevState(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vOnErrorGetDeviceInfo(...)
   **************************************************************************/
   /*!
   * \fn      vOnErrorGetDeviceInfo(amt_tclServiceData* poMessage)
   * \brief   Called by framework when GetDeviceInfo error is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   * \retval  None
   **************************************************************************/
   tVoid vOnErrorGetDeviceInfo(amt_tclServiceData* poMessage);

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclBluetoothClient::vOnStatusPairingStatus(...)
   **************************************************************************/
   /*!
   * \fn      vOnStatusPairingStatus(amt_tclServiceData* poMessage)
   * \brief   Called by framework when PairingStatus property update is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   * \retval  None
   **************************************************************************/
   tVoid vOnStatusPairingStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vOnVehicleBTAddressStatus(amt_tclServiceData...
   **************************************************************************/
   /*!
   * \fn      vOnVehicleBTAddressStatus(amt_tclServiceData* poMessage)
   * \brief   Called by framework when VehicleBTAddress property update is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   * \retval  None
   **************************************************************************/
   tVoid vOnVehicleBTAddressStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vOnBTAudioSourceStatus(amt_tclServiceData...
   **************************************************************************/
   /*!
   * \fn      vOnBTAudioSourceStatus(amt_tclServiceData* poMessage)
   * \brief   Called by framework when BluetoothAudioSource property update is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   * \retval  None
   **************************************************************************/
   tVoid vOnBTAudioSourceStatus(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vOnMRPairingResponse(amt_tclServiceData...
   **************************************************************************/
   /*!
   * \fn      vOnMRPairingResponse(amt_tclServiceData* poMessage)
   * \brief   Called by framework when PairingResponse method result is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   * \retval  None
   **************************************************************************/
   tVoid vOnMRPairingResponse(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vOnErrorPairingResponse(amt_tclServiceData...
   **************************************************************************/
   /*!
   * \fn      vOnErrorPairingResponse(amt_tclServiceData* poMessage)
   * \brief   Called by framework when PairingResponse error is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   * \retval  None
   **************************************************************************/
   tVoid vOnErrorPairingResponse(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vOnMRSwitchBTLocalMode(amt_tclServiceData...
   **************************************************************************/
   /*!
   * \fn      vOnMRSwitchBTLocalMode(amt_tclServiceData* poMessage)
   * \brief   Called by framework when SwitchBTLocalMode method result is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   * \retval  None
   **************************************************************************/
   tVoid vOnMRSwitchBTLocalMode(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vOnErrorSwitchBTOnOff(amt_tclServiceData...
   **************************************************************************/
   /*!
   * \fn      vOnMRSwitchBTOnOff(amt_tclServiceData* poMessage)
   * \brief   Called by framework when SwitchBluetoothOnOff method result is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   * \retval  None
   **************************************************************************/
   tVoid vOnMRSwitchBTOnOff(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vOnErrorSwitchBTOnOff(amt_tclServiceData...
   **************************************************************************/
   /*!
   * \fn      vOnErrorSwitchBTOnOff(amt_tclServiceData* poMessage)
   * \brief   Called by framework when SwitchBluetoothOnOff error is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   * \retval  None
   **************************************************************************/
   tVoid vOnErrorSwitchBTOnOff(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vOnErrorSwitchBTLocalMode(amt_tclServiceData...
   **************************************************************************/
   /*!
   * \fn      vOnErrorSwitchBTLocalMode(amt_tclServiceData* poMessage)
   * \brief   Called by framework when SwitchBTLocalMode error is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   * \retval  None
   **************************************************************************/
   tVoid vOnErrorSwitchBTLocalMode(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vOnStatusPairableMode(amt_tclServiceData...
   **************************************************************************/
   /*!
   * \fn      vOnStatusPairableMode(amt_tclServiceData* poMessage)
   * \brief   Called by framework when PairableMode property update is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   * \retval  None
   **************************************************************************/
   tVoid vOnStatusPairableMode(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vOnStatusPairingRequest(amt_tclServiceData...
   **************************************************************************/
   /*!
   * \fn      vOnStatusPairingRequest(amt_tclServiceData* poMessage)
   * \brief   Called by framework when PairingRequest property update is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   * \retval  None
   **************************************************************************/
   tVoid vOnStatusPairingRequest(amt_tclServiceData* poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vOnStatusBluetoothOnOff(amt_tclServiceData...
   **************************************************************************/
   /*!
   * \fn      vOnStatusBluetoothOnOff(amt_tclServiceData* poMessage)
   * \brief   Called by framework when BluetoothOnOff property update is
   *          sent by Bluetooth Service.
   * \param   [IN] poMessage : Pointer to message
   * \retval  None
   **************************************************************************/
   tVoid vOnStatusBluetoothOnOff(amt_tclServiceData* poMessage);

   /**************************************************************************
   * BT settings message posting methods
   **************************************************************************/

   /**************************************************************************
   ** FUNCTION:  tBool spi_tclBluetoothClient::bPostMethodStart(const...
   **************************************************************************/
   /*!
   * \fn      bPostMethodStart(const btset_FiMsgBase& rfcooBTSetMS,
   *           t_U32 u32CmdCounter)
   * \brief   Posts a BT settings MethodStart message.
   * \param   [IN] rfcooBTSetMS : BT settings Method Start message type
   * \param   [IN] u32CmdCounter : Command counter value to be set in the message.
   * \retval  tBool: TRUE - if message posting is successful, else FALSE.
   **************************************************************************/
   tBool bPostMethodStart(const btset_FiMsgBase& rfcooBTSetMS,
         t_U32 u32CmdCounter) const;

   /**************************************************************************
   * ! Other methods
   **************************************************************************/

   /***************************************************************************
   ** FUNCTION:  tU8 spi_tclBluetoothClient::u8GetBTDeviceHandle(const t_String&...)
   ***************************************************************************/
   /*!
   * \fn      u8GetBTDeviceHandle(const t_String& rfcoszDeviceBTAddress)
   * \brief   Retrieves BT DeviceHandle of the device for input BT Address.
   *          If device does not exist in BT DeviceList, 0 (i.e. invalid handle)
   *          is returned.
   * \param   [IN] rfcoszDeviceBTAddress : BT Address of a device
   * \retval  tU8
   **************************************************************************/
   tU8 u8GetBTDeviceHandle(const t_String& rfcoszDeviceBTAddress);

    /**************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vAddToDeviceList(tU8 u8DeviceHandle,...)
   **************************************************************************/
   /*!
   * \fn      vAddToDeviceList(tU8 u8DeviceHandle, tBool bDeviceConnStatus)
   * \brief   Adds a new device to internal BTDeviceList with its connection status
   *          information, and requests device info from Bluetooth Service using
   *          GetDeviceInfo message.
   * \param   [IN] u8DeviceHandle : Unique handle of device to be added.
   * \param   [IN] bDeviceConnStatus : Connection status of BT device
   *               (TRUE - if connected, else FALSE).
   * \param   [IN] rfcszDeviceName : Name of BT device
   * \param   [IN] rfcszBTAddress : BT Address of device
   * \retval  None
   **************************************************************************/
   tVoid vAddToDeviceList(tU8 u8DeviceHandle, tBool bDeviceConnStatus,
            const t_String& rfcszDeviceName, const t_String& rfcszBTAddress);

   /**************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vRemoveFromDeviceList(tU8 u8DeviceHandle)
   **************************************************************************/
   /*!
   * \fn      vRemoveFromDeviceList(tU8 u8DeviceHandle, tBool bDeviceConnStatus)
   * \brief   Removes a device from BTDeviceList map.
   * \param   [IN] u8DeviceHandle : Unique handle of device to be deleted.
   * \retval  None
   **************************************************************************/
   tVoid vRemoveFromDeviceList(tU8 u8DeviceHandle);

   /**************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vUpdateDeviceConnStatus(tU8...)
   **************************************************************************/
   /*!
   * \fn      vUpdateDeviceConnStatus(tU8 u8DeviceHandle, tBool bDeviceConnStatus)
   * \brief   Updates the connection status of a device in BTDeviceList map.
   * \param   [IN] u8DeviceHandle : Unique handle of device.
   * \param   [IN] bDeviceConnectionStatus : Connection status of device.
   * \retval  None
   **************************************************************************/
   tVoid vUpdateDeviceConnStatus(tU8 u8DeviceHandle,
         tBool bDeviceConnectionStatus);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothClient::bRequestChangeDeviceState(t_String...)
   ***************************************************************************/
   /*!
   * \fn      vStoreDeviceChangeRequestInfo(tU8 u8DeviceHandle,
   *             const trBTChangeDeviceRequestInfo& rfcorRequestInfo)
   * \brief   Stores an instance of ChangeDeviceState request info in
   *          ChngDevRequestsList map, with key as the DeviceHandle.
   * \param   [IN] u8DeviceHandle : Unique handle of device
   * \param   [IN] enRequestType : ChangeDeviceState request type
   * \retval  None
   **************************************************************************/
   t_Bool bRequestChangeDeviceState(tU8 u8DeviceHandle,
         tenBTSetConnectionRequest enRequestType);

   /**************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vStoreDeviceChangeRequestInfo(tU8 ...
   **************************************************************************/
   /*!
   * \fn      vStoreDeviceChangeRequestInfo(tU8 u8DeviceHandle,
   *             const trBTChangeDeviceRequestInfo& rfcorRequestInfo)
   * \brief   Stores an instance of ChangeDeviceState request info in
   *          ChngDevRequestsList map, with key as the DeviceHandle.
   * \param   [IN] u8DeviceHandle : Unique handle of device
   * \param   [IN] rfcorRequestInfo : Structure containing info about triggered
   *               ChangeDeviceState operation.
   * \retval  None
   **************************************************************************/
   tVoid vStoreDeviceChangeRequestInfo(tU8 u8DeviceHandle,
         const trBTChangeDeviceRequestInfo& rfcorRequestInfo);

   /**************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vDeleteDeviceChangeRequestInfo(tU8 ...
   **************************************************************************/
   /*!
   * \fn      vDeleteDeviceChangeRequestInfo(tU8 u8DeviceHandle)
   * \brief   Removes the instance of ChangeDeviceState request stored in
   *          ChngDevRequestsList map for specified DeviceHandle.
   * \param   [IN] u8DeviceHandle : Unique handle of device
   * \retval  None
   **************************************************************************/
   tVoid vDeleteDeviceChangeRequestInfo(tU8 u8DeviceHandle);

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclBluetoothClient::bIsDeviceValid(tU8...)
   ***************************************************************************/
   /*!
   * \fn      bIsDeviceValid(tU8 u8DeviceHandle, tBTDevListMapItr& rfBTDevLstMapItr)
   * \brief   Checks if specified device exists in the internal BT DeviceList map
   * \param   [IN] u8DeviceHandle : BT DeviceHandle
   * \retval  tBool: TRUE - If Device is found in BT DeviceList, else FALSE
   **************************************************************************/
   tBool bIsDeviceValid(tU8 u8DeviceHandle);

   /***************************************************************************
   ** FUNCTION:  tBool spi_tclBluetoothClient::bFindChangeDevReqInfoInMap(tU8...)
   ***************************************************************************/
   /*!
   * \fn      bFindChangeDevReqInfoInMap(tU8 u8DeviceHandle,
   *             trBTChangeDeviceRequestInfo& rfRequestInfo)
   * \brief   Checks if specified device's ChangeDeviceState request info
   *          is stored in ChngDevRequestsList map, and if found, provides
   *          a copy of the request info.
   * \param   [IN] u8DeviceHandle : BT DeviceHandle
   * \param   [OUT] rfRequestInfo : ChangeDeviceState request info for specified device.
   * \retval  tBool: TRUE - If Device is found in BT DeviceList, else FALSE
   **************************************************************************/
   tBool bFindChangeDevReqInfoInMap(tU8 u8DeviceHandle,
         trBTChangeDeviceRequestInfo& rfRequestInfo);

   /**************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vNotifyBTConnectionChanged(...)
   **************************************************************************/
   /*!
   * \fn      vNotifyBTConnectionChanged(const t_String& rfcoszDevBTAddress,
   *             tenBTConnectionResult enBTConnResult)
   * \brief   Notifies BT Manager about a device connection change.
   *          (Used to inform when a change in connection status of a device is
   *          detected by the client-handler. i.e, when a BT device changes to
   *          connected or disconnected state)
   * \param   [IN] rfcoszDevBTAddress : BT Address of changed device
   * \param   [IN] enBTConnResult : Connection status of changed device.
   * \retval  None
   **************************************************************************/
   tVoid vNotifyBTConnectionChanged(const t_String& rfcoszDevBTAddress,
         tenBTConnectionResult enBTConnResult) const;

   /**************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vNotifyBTConnectionResult(...)
   **************************************************************************/
   /*!
   * \fn      vNotifyBTConnectionResult(const t_String& rfcoszDevBTAddress,
   *             tenBTConnectionResult enBTConnResult)
   * \brief   Notifies BT Manager about the connection result of a device.
   *          (Used to inform about the result of a connection change request
   *          triggered by the client-handler.)
   * \param   [IN] rfcoszDevBTAddress : BT Address of changed device
   * \param   [IN] enBTConnResult : Connection status of changed device.
   * \retval  None
   **************************************************************************/
   tVoid vNotifyBTConnectionResult(const t_String& rfcoszDevBTAddress,
         tenBTConnectionResult enBTConnResult) const;

   /**************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::vNotifyBTRequestError(t_String...)
   **************************************************************************/
   /*!
   * \fn      vNotifyBTRequestError(t_String szDeviceBTAddress = "",
   *             tenBTSetConnectionRequest enRequestType = e8BT_REQUEST_UNKNOWN)
   * \brief   Notifies BT Manager about connection error for a device.
   *          (Used to inform about the error result of a connection change request
   *          triggered by the client-handler.)
   * \param   [IN] szDeviceBTAddress : BT Address of changed device
   * \param   [IN] enRequestType : Type of operation triggered for which error
   *                occurred.
   * \retval  None
   **************************************************************************/
   tVoid vNotifyBTRequestError(t_String szDeviceBTAddress = "",
         tenBTSetConnectionRequest enRequestType = e8BT_REQUEST_UNKNOWN) const;

   /**************************************************************************
   ** FUNCTION:  t_String spi_tclBluetoothClient::szGetDateTimestamp(const...
   **************************************************************************/
   /*!
   * \fn      szGetDateTimestamp(const btset_tDateTimestamp& rfcooDateTimeStamp)
   * \brief   Converts a DateTimeStamp object to a single string.
   * \param   [IN] rfcooDateTimeStamp : BT Settings DateTimeStamp object.
   * \retval  t_String
   **************************************************************************/
   t_String szGetDateTimestamp(const btset_tDateTimestamp& rfcooDateTimeStamp) const;

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclBluetoothClient::szGetBTDeviceAddress(...)
   **************************************************************************/
   /*!
   * \fn      szGetBTDeviceAddress(tU8 u8DeviceHandle)
   * \brief   Retrieves BT address of a device from DeviceList
   * \param   [IN] u8DeviceHandle : BT Device Handle
   * \retval  t_String
   **************************************************************************/
   t_String szGetBTDeviceAddress(tU8 u8DeviceHandle);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::enGetBTConnResult(...)
   **************************************************************************/
   /*!
   * \fn      enGetBTConnResult(const tenBTSetConnectionRequest& rfcoBTConnRequest,
   *             const btset_tenBTConnectionResult& rfcoBTSetFiConnResult)
   * \brief   Analyses the result of BT connection request based on request type.
   * \param   [IN] rfcoBTConnRequest : Type of connection request made
   * \param   [IN] rfcoBTSetFiConnResult : Type of connection result received
   * \retval  tenBTConnectionResult
   **************************************************************************/
   tenBTConnectionResult enGetBTConnResult(
         const tenBTSetConnectionRequest& rfcoBTConnRequest,
         const btset_tenBTConnectionResult& rfcoBTSetFiConnResult);

   /***************************************************************************
   ** FUNCTION:  tVoid spi_tclBluetoothClient::vSetDevicesDisconnected()
   **************************************************************************/
   /*!
   * \fn      vSetDevicesDisconnected()
   * \brief   Sets all devices in list to disconnected
   * \param   None
   * \retval  None
   **************************************************************************/
   tVoid vSetDevicesDisconnected();

   /**************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::spi_tclBluetoothClient()
   **************************************************************************/
   /*!
   * \fn      spi_tclBluetoothClient()
   * \brief   Default Constructor, will not be implemented.
   *          NOTE: This is a technique to disable the Default Constructor for
   *          this class. So if an attempt for the constructor is made compiler
   *          complains.
   **************************************************************************/
   spi_tclBluetoothClient();

   /**************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::spi_tclBluetoothClient(const...
   **************************************************************************/
   /*!
   * \fn      spi_tclBluetoothClient(const spi_tclBluetoothClient& oClient)
   * \brief   Copy Consturctor, will not be implemented.
   *          Avoids Lint Prio 3 warning: Info 1732: new in constructor for
   *          class'spi_tclBluetoothClient' which has no Copy Consturctor.
   *          NOTE: This is a technique to disable the Copy Consturctor for this
   *          class. So if an attempt for the copying is made linker complains.
   * \param   [IN] poMessage : Property to be set.
   **************************************************************************/
   spi_tclBluetoothClient(const spi_tclBluetoothClient& oClient);

   /**************************************************************************
   ** FUNCTION:  spi_tclBluetoothClient::spi_tclBluetoothClient& operator=(...
   **************************************************************************/
   /*!
   * \fn      spi_tclBluetoothClient& operator=(
   *          const spi_tclBluetoothClient& oClient)
   * \brief   Assingment Operater, will not be implemented.
   *          Avoids Lint Prio 3 warning: Info 1732: new in constructor for
   *          class 'spi_tclBluetoothClient' which has no assignment operator.
   *          NOTE: This is a technique to disable the assignment operator for this
   *          class. So if an attempt for the assignment is made compiler complains.
   **************************************************************************/
   spi_tclBluetoothClient& operator=(const spi_tclBluetoothClient& oClient);

   /***************************************************************************
   * ! Data members
   ***************************************************************************/

   /***************************************************************************
   ** Main Application pointer
   ***************************************************************************/
   ahl_tclBaseOneThreadApp*                     m_poMainAppl;

   /***************************************************************************
   ** Bluetooth Device List Map
   ***************************************************************************/
   std::map<tU8, trBluetoothDeviceInfo>         m_mapBTDeviceList;

   /***************************************************************************
   ** ChangeDeviceState Requests Map
   ***************************************************************************/
   std::map<tU8, trBTChangeDeviceRequestInfo>   m_mapChngDevRequestsList;

   /***************************************************************************
   ** Bluetooth Response callbacks structure for BT Manager
   ***************************************************************************/
   trBluetoothCallbacks                         m_rBluetoothCallbacks;

   /***************************************************************************
   ** Bluetooth pairing info callbacks structure for BT Manager
   ***************************************************************************/
   trBluetoothPairingCallbacks                  m_rBTPairInfoCallbacks;

   /***************************************************************************
   ** Bluetooth Device name callback for BT Manager
   ***************************************************************************/
   trBTDeviceNameCbInfo                         m_rBTDeviceNameCbInfo;

  /***************************************************************************
   ** Lock object for DeviceList map
   ***************************************************************************/
   Lock  m_oDevLstLock;

  /***************************************************************************
   ** Lock object for ChangeDeviceState requests map
   ***************************************************************************/
   Lock  m_oChnDevLstLock;

   /***************************************************************************
   ** Bluetooth on/off status
   ***************************************************************************/
   t_Bool   m_bIsBluetoothOn;

   /***************************************************************************
    ** Lock object for Bluetooth on/off status
    ***************************************************************************/
    Lock  m_oBTOnOffLock;

   /***************************************************************************
   * Message map definition macro
   ***************************************************************************/
   DECLARE_MSG_MAP(spi_tclBluetoothClient)
};

#endif // _SPI_TCLBLUETOOTHCLIENT_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
