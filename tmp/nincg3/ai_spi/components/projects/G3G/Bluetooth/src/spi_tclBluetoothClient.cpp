/*!
*******************************************************************************
* \file              spi_tclBluetoothClient.cpp
* \brief             Bluetooth Settings Client handler class
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Bluetooth Settings Client handler class
COPYRIGHT:      &copy; RBEI

HISTORY:
 Date       |  Author                           | Modifications
 21.02.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 21.05.2014 |  Ramya Murthy (RBEI/ECP2)         | Revised logic for storing DeviceList
                                                  and validation of service responses.
 04.06.2014 |  Ramya Murthy (RBEI/ECP2)         | VehicleBTAddress implementation
 11.09.2014 |  Shihabudheen P M (RBEI/ECP2)     | Modified for writing/reading BT address 
                                                  to Datapool    
 22.09.2014 |  Ramya Murthy (RBEI/ECP2)         | Implemented locks for DeviceList and
                                                  ChngDevRequestsList maps
 28.10.2014 |  Ramya Murthy (RBEI/ECP2)         | Implementation for setting BluetoothAudioSource
                                                  (Fix for SUZUKI-18263)
 27.05.2015 |  Tejaswini H B(RBEI/ECP2)         | Added Lint comments to suppress C++11 Errors
 19.05.2015 |  Ramya Murthy (RBEI/ECP2)         | Adaptation to DeviceListExtended property
 08.07.2015 |  Ramya Murthy (RBEI/ECP2)         | Added check to ensure BluetoothAudioSource.Set(ValidDevHandle)
                                                  is used to enable A2DP profile
 18.08.2015 |  Ramya Murthy (RBEI/ECP2)         | Adaptation to new BTSettings interfaces for AndroidAuto
 
\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include <algorithm>

#include "XFiObjHandler.h"
#include "spi_tclBluetoothClient.h"
#include "spi_LoopbackTypes.h"
#include "Datapool.h"

//For message dispatcher
#include "FIMsgDispatch.h"
using namespace shl::msgHandler;

#define GENERICMSGS_S_IMPORT_INTERFACE_GENERIC
#include "generic_msgs_if.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_BLUETOOTH
      #include "trcGenProj/Header/spi_tclBluetoothClient.cpp.trc.h"
   #endif
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
//! BT Settings types
typedef most_fi_tcl_BTSetDeviceListExtendedResultItem          btset_DeviceListExtItem;

typedef most_fi_tcl_e8_BTSetChangeDeviceStateAction::tenType   btset_tenChngDevStateAction;

typedef most_fi_tcl_e8_BTSetConnectionResult                   btset_tFiBTConnectionResult;

typedef most_fi_tcl_e8_BTSetDeviceStatus                       btset_tFiDevStatusType;
typedef most_fi_tcl_e8_BTSetDeviceStatus::tenType              btset_tenDevStatusType;

typedef most_fi_tcl_e8_BTSetPairingResponseType::tenType       btset_tenPairingResponseType;
typedef most_fi_tcl_e8_BTSetDeviceDisconnectedReason::tenType  btset_tenDevDisconnReasonType;
typedef most_fi_tcl_BTLocalModeType::tenType                   btset_tenBTLocalMode;

typedef most_btsetfi_tclMsgChangeDeviceStateMethodStart        btset_tMSChangeDevState;
typedef most_btsetfi_tclMsgBluetoothAudioSourcePureSet         btset_tPSBTAudioSource;
typedef most_btsetfi_tclMsgPairingResponseMethodStart          btset_tMSPairResponse;
typedef most_btsetfi_tclMsgSwitchBTLocalModeMethodStart        btset_tMSSwitchBTLocMode;
typedef most_btsetfi_tclMsgSwitchBluetoothOnOffMethodStart     btset_tMSSwitchBTOnOff;

typedef XFiObjHandler<most_btsetfi_tclMsgDeviceListExtendedStatus>       btset_tXFiStDeviceListExt;
typedef XFiObjHandler<most_btsetfi_tclMsgPairingStatusStatus>            btset_tXFiStPairingStatus;
typedef XFiObjHandler<most_btsetfi_tclMsgChangeDeviceStateMethodResult>  btset_tXFiMRChangeDevState;
typedef XFiObjHandler<most_btsetfi_tclMsgChangeDeviceStateError>         btset_tXFiErrChangeDevState;
typedef XFiObjHandler<most_btsetfi_tclMsgVehicleBTAddressStatus>         btset_tXFiStVehicleBTAddr;
typedef XFiObjHandler<most_btsetfi_tclMsgBluetoothAudioSourceStatus>     btset_tXFiStBTAudioSource;
typedef XFiObjHandler<most_btsetfi_tclMsgPairingResponseMethodResult>    btset_tXFiMRPairingResponse;
typedef XFiObjHandler<most_btsetfi_tclMsgPairingResponseError>           btset_tXFiErrPairingResponse;
typedef XFiObjHandler<most_btsetfi_tclMsgSwitchBTLocalModeMethodResult>  btset_tXFiMRSwitchBTLocMode;
typedef XFiObjHandler<most_btsetfi_tclMsgSwitchBluetoothOnOffMethodResult>  btset_tXFiMRSwitchBTOnOff;
typedef XFiObjHandler<most_btsetfi_tclMsgSwitchBluetoothOnOffError>      btset_tXFiErrSwitchBTOnOff;
typedef XFiObjHandler<most_btsetfi_tclMsgSwitchBTLocalModeError>         btset_tXFiErrSwitchBTLocMode;
typedef XFiObjHandler<most_btsetfi_tclMsgPairableModeStatus>             btset_tXFiStPairableMode;
typedef XFiObjHandler<most_btsetfi_tclMsgPairingRequestStatus>           btset_tXFiStPairingRequest;
typedef XFiObjHandler<most_btsetfi_tclMsgBluetoothOnOffStatus>           btset_tXFiStBTOnOff;

//! BT Settings FBlock DeviceList vector typedef
typedef bpstl::vector<btset_DeviceListExtItem, bpstl::allocator<btset_DeviceListExtItem> >::iterator
      btset_tFiDeviceListExtItr;

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/
#define BTSETTINGS_FI_MAJOR_VERSION  MOST_BTSETFI_C_U16_SERVICE_MAJORVERSION
#define BTSETTINGS_FI_MINOR_VERSION  MOST_BTSETFI_C_U16_SERVICE_MINORVERSION
//#define BTSETTINGS_FI_PATCH_VERSION  0                                         //Commented to suppress Lint Fix

//! Bluetooth Settings FBlock types
#define BTDEVCONNRESULT_CONNECTED     (btset_tFiBTConnectionResult::FI_EN_E8DEVICE_CONNECTED)
#define BTDEVCONNRESULT_DISCONNECTED  (btset_tFiBTConnectionResult::FI_EN_E8DEVICE_DISCONNECTED)
#define BTDEVCONNRESULT_NOTCONNECTED  (btset_tFiBTConnectionResult::FI_EN_E8DEVICE_NOT_CONNECTED)
#define BTDEVCONNRESULT_NOTAPPLICABLE (btset_tFiBTConnectionResult::FI_EN_E8NOT_APPLICABLE)
#define BTDEVCONNRESULT_BLOCKED       (btset_tFiBTConnectionResult::FI_EN_E8DEVICE_BLOCKED)
#define BTDEVCONNRESULT_BLOCKALLEXCEPT (btset_tFiBTConnectionResult::FI_EN_E8BLOCK_ALL_EXCEPT)
#define BTDEVCONNRESULT_UNBOCKED      (btset_tFiBTConnectionResult::FI_EN_E8DEVICE_UNBLOCKED)
#define BTDEV_CONNECTEDOSD            (btset_tFiDevStatusType::FI_EN_E8DEVICE_CONNECTED_OSD)
#define BTDEV_DISCONNECTED            (btset_tFiDevStatusType::FI_EN_E8DEVICE_DISCONNECTED)
#define BTDEV_DELETED                 (btset_tFiDevStatusType::FI_EN_E8DEVICE_DELETED)
#define CHAR_SET_UTF8                 (most_fi_tcl_String::FI_EN_UTF8)

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static const t_U8 cou8InvalidDeviceHandle = 0;

/******************************************************************************
** CCA MESSAGE MAP
******************************************************************************/
BEGIN_MSG_MAP(spi_tclBluetoothClient, ahl_tclBaseWork)

   /* -------------------------------Methods.---------------------------------*/
   ON_MESSAGE_SVCDATA(MOST_BTSETFI_C_U16_DEVICELISTEXTENDED,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusDeviceListExtended)
   ON_MESSAGE_SVCDATA(MOST_BTSETFI_C_U16_CHANGEDEVICESTATE,
   AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnMRChangeDevState)
   ON_MESSAGE_SVCDATA(MOST_BTSETFI_C_U16_CHANGEDEVICESTATE,
   AMT_C_U8_CCAMSG_OPCODE_ERROR, vOnErrorChangeDevState)
   ON_MESSAGE_SVCDATA(MOST_BTSETFI_C_U16_PAIRINGSTATUS,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusPairingStatus)
   ON_MESSAGE_SVCDATA(MOST_BTSETFI_C_U16_VEHICLEBTADDRESS,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnVehicleBTAddressStatus)
   ON_MESSAGE_SVCDATA(MOST_BTSETFI_C_U16_BLUETOOTHAUDIOSOURCE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnBTAudioSourceStatus)
   ON_MESSAGE_SVCDATA(MOST_BTSETFI_C_U16_PAIRINGRESPONSE,
   AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnMRPairingResponse)
   ON_MESSAGE_SVCDATA(MOST_BTSETFI_C_U16_PAIRINGRESPONSE,
   AMT_C_U8_CCAMSG_OPCODE_ERROR, vOnErrorPairingResponse)
   ON_MESSAGE_SVCDATA(MOST_BTSETFI_C_U16_SWITCHBTLOCALMODE,
   AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnMRSwitchBTLocalMode)
   ON_MESSAGE_SVCDATA(MOST_BTSETFI_C_U16_SWITCHBLUETOOTHONOFF,
   AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnMRSwitchBTOnOff)
   ON_MESSAGE_SVCDATA(MOST_BTSETFI_C_U16_SWITCHBTLOCALMODE,
   AMT_C_U8_CCAMSG_OPCODE_ERROR, vOnErrorSwitchBTLocalMode)
   ON_MESSAGE_SVCDATA(MOST_BTSETFI_C_U16_PAIRABLEMODE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusPairableMode)
   ON_MESSAGE_SVCDATA(MOST_BTSETFI_C_U16_PAIRINGREQUEST,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusPairingRequest)
   ON_MESSAGE_SVCDATA(MOST_BTSETFI_C_U16_BLUETOOTHONOFF,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusBluetoothOnOff)

   /* -------------------------------Methods.---------------------------------*/

END_MSG_MAP();


/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::spi_tclBluetoothClient(...)
**************************************************************************/
spi_tclBluetoothClient::spi_tclBluetoothClient(ahl_tclBaseOneThreadApp* poMainAppl,
     tU16 u16ServiceID)
     : ahl_tclBaseOneThreadClientHandler(
         poMainAppl,                           /* Application Pointer */
         u16ServiceID,                         /* ID of used Service */
         BTSETTINGS_FI_MAJOR_VERSION,          /* MajorVersion of used Service */
         BTSETTINGS_FI_MINOR_VERSION),         /* MinorVersion of used Service */
       m_poMainAppl(poMainAppl),
       m_bIsBluetoothOn(true)
{
   ETG_TRACE_USR1(("spi_tclBluetoothClient() entered "));
   NORMAL_M_ASSERT(OSAL_NULL != m_poMainAppl);
}//! end of spi_tclBluetoothClient()

/***************************************************************************
** FUNCTION:  virtual spi_tclBluetoothClient::~spi_tclBluetoothClient()
**************************************************************************/
spi_tclBluetoothClient::~spi_tclBluetoothClient()
{
   ETG_TRACE_USR1(("~spi_tclBluetoothClient() entered "));

   //Clear the containers
   m_oChnDevLstLock.s16Lock();
   m_mapChngDevRequestsList.clear();
   m_oChnDevLstLock.vUnlock();

   m_oDevLstLock.s16Lock();
   m_mapBTDeviceList.clear();
   m_oDevLstLock.vUnlock();
   
   m_poMainAppl = OSAL_NULL;

}//! end of spi_tclBluetoothClient()

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnServiceAvailable()
**************************************************************************/
tVoid spi_tclBluetoothClient::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnServiceAvailable() entered "));

   vRegisterForProperties();
   //@Note: Need to register for properties when service is available, since
   //BT Device list has to be maintained internally throughout the ignition cycle.
}//! end of spi_tclBluetoothClient()

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnServiceUnavailable()
**************************************************************************/
tVoid spi_tclBluetoothClient::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnServiceUnavailable() "));
   vUnregisterForProperties();
}//! end of spi_tclBluetoothClient()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetoothClient::vRegisterForProperties()
**************************************************************************/
t_Void spi_tclBluetoothClient::vRegisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclBluetoothClient::vRegisterForProperties() "));

   //! Register for interested properties
   vAddAutoRegisterForProperty(MOST_BTSETFI_C_U16_DEVICELISTEXTENDED);
   vAddAutoRegisterForProperty(MOST_BTSETFI_C_U16_PAIRINGSTATUS);
   vAddAutoRegisterForProperty(MOST_BTSETFI_C_U16_VEHICLEBTADDRESS);
   vAddAutoRegisterForProperty(MOST_BTSETFI_C_U16_BLUETOOTHAUDIOSOURCE);
   vAddAutoRegisterForProperty(MOST_BTSETFI_C_U16_PAIRABLEMODE);
   vAddAutoRegisterForProperty(MOST_BTSETFI_C_U16_PAIRINGREQUEST);
   vAddAutoRegisterForProperty(MOST_BTSETFI_C_U16_BLUETOOTHONOFF);

}//! end of vRegisterForProperties()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetoothClient::vUnregisterForProperties()
**************************************************************************/
t_Void spi_tclBluetoothClient::vUnregisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclBluetoothClient::vUnregisterForProperties() "));

   //! Unregister subscribed properties
   vRemoveAutoRegisterForProperty(MOST_BTSETFI_C_U16_DEVICELISTEXTENDED);
   vRemoveAutoRegisterForProperty(MOST_BTSETFI_C_U16_PAIRINGSTATUS);
   vRemoveAutoRegisterForProperty(MOST_BTSETFI_C_U16_VEHICLEBTADDRESS);
   vRemoveAutoRegisterForProperty(MOST_BTSETFI_C_U16_BLUETOOTHAUDIOSOURCE);
   vRemoveAutoRegisterForProperty(MOST_BTSETFI_C_U16_PAIRABLEMODE);
   vRemoveAutoRegisterForProperty(MOST_BTSETFI_C_U16_PAIRINGREQUEST);
   vRemoveAutoRegisterForProperty(MOST_BTSETFI_C_U16_BLUETOOTHONOFF);

}//! end of vUnregisterForProperties()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetoothClient::vRegisterCallbacks()
***************************************************************************/
t_Void spi_tclBluetoothClient::vRegisterCallbacks(trBluetoothCallbacks rBTRespCallbacks)
{
   //! Store the callbacks
   m_rBluetoothCallbacks = rBTRespCallbacks;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetoothPolicyBase::vRegisterPairingInfoCallbacks()
***************************************************************************/
t_Void spi_tclBluetoothClient::vRegisterPairingInfoCallbacks(
      trBluetoothPairingCallbacks rBTPairInfoCallbacks)
{
   m_rBTPairInfoCallbacks = rBTPairInfoCallbacks;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetoothClient::vRegisterCallbacks()
***************************************************************************/
t_Void spi_tclBluetoothClient::vRegisterDeviceNameCallback(
      trBTDeviceNameCbInfo rBTDeviceNameCbInfo)
{
   //! Store callback info
   m_rBTDeviceNameCbInfo = rBTDeviceNameCbInfo;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclBluetoothClient::bSendPairingAction(...)
***************************************************************************/
t_Bool spi_tclBluetoothClient::bSendPairingAction(tenBTPairingAction enAction)
{
   btset_tMSPairResponse oMSPairResponse;
   oMSPairResponse.e8PairingResponseType.enType =
         static_cast<btset_tenPairingResponseType>(enAction);
   t_Bool bSuccess = bPostMethodStart(oMSPairResponse, 0);

   ETG_TRACE_USR4(("spi_tclBluetoothClient::bSendPairingAction() left with bSuccess = %u (for Action = %d) ",
         ETG_ENUM(BOOL, bSuccess), ETG_ENUM(BTPAIRING_ACTION, enAction)));
   return bSuccess;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclBluetoothClient::bConnectBTDevice(t_String...)
***************************************************************************/
t_Bool spi_tclBluetoothClient::bConnectBTDevice(const t_String& rfcoszDeviceBTAddress)
{
   //! Send Connect request
   t_Bool bConnReqSent = false;
   tU8 u8DeviceHandle = static_cast<tU8>(u32GetBTDeviceHandle(rfcoszDeviceBTAddress));

   if (IS_VALID_DEVHANDLE(u8DeviceHandle))
   {
      bConnReqSent = bRequestChangeDeviceState(u8DeviceHandle, e8BT_REQUEST_CONNECT);
   }//if (IS_VALID_DEVHANDLE(u8DeviceHandle))

   ETG_TRACE_USR4(("spi_tclBluetoothClient::bConnectBTDevice() left with bConnReqSent = %u (for BTAddress = %s) ",
         ETG_ENUM(BOOL, bConnReqSent), rfcoszDeviceBTAddress.c_str()));
   return bConnReqSent;
}//! end of bConnectBTDevice()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclBluetoothClient::bDisconnectBTDevice(const...)
***************************************************************************/
t_Bool spi_tclBluetoothClient::bDisconnectBTDevice(const t_String& rfcoszDeviceBTAddress)
{
   //! Send Disconnect request
   t_Bool bDisconnReqSent = false;
   tU8 u8DeviceHandle = static_cast<tU8>(u32GetBTDeviceHandle(rfcoszDeviceBTAddress));

   if (IS_VALID_DEVHANDLE(u8DeviceHandle))
   {
      bDisconnReqSent = bRequestChangeDeviceState(u8DeviceHandle, e8BT_REQUEST_DISCONNECT);
   } //if (IS_VALID_DEVHANDLE(u8DeviceHandle))

   ETG_TRACE_USR4(("spi_tclBluetoothClient::bDisconnectBTDevice() left with bDisconnReqSent = %u (for BTAddress = %s) ",
         ETG_ENUM(BOOL, bDisconnReqSent), rfcoszDeviceBTAddress.c_str()));
   return bDisconnReqSent;
}//! end of bDisconnectBTDevice()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclBluetoothClient::bBlockBTDevice(...)
***************************************************************************/
t_Bool spi_tclBluetoothClient::bBlockBTDevice(tenBTDeviceBlockType enBlockType,
      const t_String& rfcoszDeviceBTAddress)
{
   //! Send Block request
   //@Note: DeviceHandle = 0 for block all devices.
   t_Bool bBlockReqSent = false;
   switch (enBlockType)
   {
      case e8BLOCK_SINGLE_DEVICE:
      {
         tU8 u8DeviceHandle = static_cast<tU8>(u32GetBTDeviceHandle(rfcoszDeviceBTAddress));
         //@Note: If requested device is not paired, block should not be sent.
         bBlockReqSent = ((IS_VALID_DEVHANDLE(u8DeviceHandle)) &&
               (bRequestChangeDeviceState(u8DeviceHandle, e8BT_REQUEST_BLOCK)));
      }
         break;
      case e8BLOCK_ALL_DEVICES:
      {
         bBlockReqSent = bRequestChangeDeviceState(cou8InvalidDeviceHandle, e8BT_REQUEST_BLOCK);
      }
         break;
      case e8BLOCK_ALL_EXCEPT:
      {
         tU8 u8DeviceHandle = static_cast<tU8>(u32GetBTDeviceHandle(rfcoszDeviceBTAddress));
         //@Note: If requested device is not paired, block should not be sent.
         bBlockReqSent = ((IS_VALID_DEVHANDLE(u8DeviceHandle)) &&
               (bRequestChangeDeviceState(u8DeviceHandle, e8BT_REQUEST_BLOCK_ALL_EXCEPT)));
      }
         break;
      default:
         ETG_TRACE_ERR(("spi_tclBluetoothClient::bBlockBTDevice: Invalid Block type! "));
         break;
   }

   ETG_TRACE_USR4(("spi_tclBluetoothClient::bBlockBTDevice() left with "
         "bBlockReqSent = %u (for BlockType = %u, BTAddress = %s) ",
         ETG_ENUM(BOOL, bBlockReqSent), ETG_ENUM(BT_BLOCK_TYPE, enBlockType), rfcoszDeviceBTAddress.c_str()));
   return bBlockReqSent;
}//! end of bBlockBTDevice()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclBluetoothClient::bUnblockBTDevice(...)
***************************************************************************/
t_Bool spi_tclBluetoothClient::bUnblockBTDevice(tenBTDeviceUnblockType enUnblockType,
      const t_String& rfcoszDeviceBTAddress)
{
   //! Send Unblock request
   //@Note: DeviceHandle = 0 for unblock all devices.
   t_Bool bUnblockReqSent = false;
   switch (enUnblockType)
   {
      case e8UNBLOCK_SINGLE_DEVICE:
      {
         tU8 u8DeviceHandle = static_cast<tU8>(u32GetBTDeviceHandle(rfcoszDeviceBTAddress));
         //@Note: If requested device is not paired, unblock should not be sent.
         bUnblockReqSent = ((IS_VALID_DEVHANDLE(u8DeviceHandle)) &&
               (bRequestChangeDeviceState(u8DeviceHandle, e8BT_REQUEST_UNBLOCK)));
      }
         break;
      case e8UNBLOCK_ALL_DEVICES:
      {
         bUnblockReqSent = bRequestChangeDeviceState(cou8InvalidDeviceHandle, e8BT_REQUEST_UNBLOCK);
      }
         break;
      case e8UNBLOCK_ALL_DEVICES_AUTO_CONNECT:
      {
         bUnblockReqSent = bRequestChangeDeviceState(cou8InvalidDeviceHandle, e8BT_REQUEST_UNBLOCK_AUTO_CONNECT);
      }
         break;
      default:
         ETG_TRACE_ERR(("spi_tclBluetoothClient::bUnblockBTDevice: Invalid Unblock type! "));
         break;
   }

   ETG_TRACE_USR4(("spi_tclBluetoothClient::bUnblockBTDevice() left with "
         "bUnblockReqSent = %u (for UnblockType = %u, BTAddress = %s) ",
         ETG_ENUM(BOOL, bUnblockReqSent), ETG_ENUM(BT_UNBLOCK_TYPE, enUnblockType), rfcoszDeviceBTAddress.c_str()));
   return bUnblockReqSent;
}//! end of bUnblockBTDevice()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclBluetoothClient::bGetPairingStatus(const...)
***************************************************************************/
t_Bool spi_tclBluetoothClient::bGetPairingStatus(const t_String& rfcoszDeviceBTAddress)
{
   //! If device exists in BT DeviceList map, device is already paired.
   //! Else pairing is required.
   t_Bool bDevicePaired =
         (cou8InvalidDeviceHandle != u8GetBTDeviceHandle(rfcoszDeviceBTAddress));

   ETG_TRACE_USR2(("spi_tclBluetoothClient::bGetPairingStatus() left with: %u (for BTAddress = %s) ",
         ETG_ENUM(BOOL, bDevicePaired),
         rfcoszDeviceBTAddress.c_str()));

   return bDevicePaired;

}//! end of bGetPairingStatus()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclBluetoothClient::bGetConnectionStatus(t_String...)
***************************************************************************/
t_Bool spi_tclBluetoothClient::bGetConnectionStatus(const t_String& rfcoszDeviceBTAddress)
{
   t_Bool bIsDeviceConnected = false;
   tU8 u8BTDevHandle = u8GetBTDeviceHandle(rfcoszDeviceBTAddress);
   
   if (TRUE == bIsDeviceValid(u8BTDevHandle))
   {
      //@Note: m_oDevLstLock should be used only after bIsDeviceValid() call, as it uses same lock.
      m_oDevLstLock.s16Lock();

      //! Check for device's connection status in BT DeviceList Map.
      tBTDevListMapConstItr DevLstMapCoItr = m_mapBTDeviceList.find(u8BTDevHandle);
      if (DevLstMapCoItr != m_mapBTDeviceList.end())
      {
         bIsDeviceConnected = (DevLstMapCoItr->second).bDeviceConnected;
      }

      m_oDevLstLock.vUnlock();
   }//if (TRUE == bIsDeviceValid(u8BTDevHandle))

   ETG_TRACE_USR2(("spi_tclBluetoothClient::bGetConnectionStatus() left with "
         "DeviceConnected = %u (for BTAddress = %s) ",
         ETG_ENUM(BOOL, bIsDeviceConnected), rfcoszDeviceBTAddress.c_str()));
   return bIsDeviceConnected;

}//! end of bGetConnectionStatus()

/***************************************************************************
** FUNCTION:  t_String spi_tclBluetoothClient::szGetConnectedDeviceBTAddress()
***************************************************************************/
t_String spi_tclBluetoothClient::szGetConnectedDeviceBTAddress()
{
   t_String szConnBTAddr = "";

   m_oDevLstLock.s16Lock();

   //! Search for connected device in BT DeviceList Map.
   //! If a device is connected, return its BT Address.
   for (tBTDevListMapConstItr DevLstMapCoItr = m_mapBTDeviceList.begin();
        DevLstMapCoItr != m_mapBTDeviceList.end();
        ++DevLstMapCoItr)
   {
      if ((DevLstMapCoItr->second).bDeviceConnected)
      {
         szConnBTAddr = (DevLstMapCoItr->second).szBTAddress;
         break;
      }
   } //for (tBTDevListMapConstItr DevLstMapCoItr = ...)

   m_oDevLstLock.vUnlock();

   return szConnBTAddr;

}//! end of szGetConnectedDeviceBTAddress()

/***************************************************************************
** FUNCTION:  t_U32 spi_tclBluetoothClient::u32GetBTDeviceHandle(t_String...)
***************************************************************************/
t_U32 spi_tclBluetoothClient::u32GetBTDeviceHandle(
      const t_String& rfcoszDeviceBTAddress)
{
   //! Retrieve and send BT device handle
   return u8GetBTDeviceHandle(rfcoszDeviceBTAddress);
}//! end of u32GetBTDeviceHandle()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetoothClient::vGetVehicleBTAddress(t_String...)
***************************************************************************/
t_Void spi_tclBluetoothClient::vGetVehicleBTAddress(t_String &szVehicleBtId)
{
   Datapool oDataPool;
   oDataPool.bReadBluetoothMacAddress(szVehicleBtId);
   ETG_TRACE_USR2(("spi_tclBluetoothClient::vGetVehicleBTAddress = %s ", szVehicleBtId.c_str()));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetoothClient::vSetBTProfile(tenBTProfile enBTProfile)
***************************************************************************/
t_Void spi_tclBluetoothClient::vSetBTProfile(tenBTProfile enBTProfile)
{
   ETG_TRACE_USR1(("spi_tclBluetoothClient::vSetBTProfile entered: enBTProfile = %u ",
         ETG_ENUM(BT_PROFILE, enBTProfile)));

   //! To enable A2DP, send BluetoothAudioSource.Set(DevHandle = Valid handle)
   //! To disable A2DP, send BluetoothAudioSource.Set(DevHandle = 0)
   tU8 u8DeviceHandle = (e8A2DP_ENABLED == enBTProfile) ?
         (u8GetBTDeviceHandle(szGetConnectedDeviceBTAddress())) :
         (0);

   if (
      ((e8A2DP_ENABLED == enBTProfile) && (0 != u8DeviceHandle)) /*Enable profile only if device is connected*/
      ||
      (e8A2DP_DISABLED == enBTProfile)
      )
   {
      //! Set BluetoothAudioSource to required device.
      btset_tPSBTAudioSource oPSBTAudioSource;
      oPSBTAudioSource.u8DeviceHandle = u8DeviceHandle;
      ETG_TRACE_USR4(("spi_tclBluetoothClient::vSetBTProfile: Calculated device handle = %u ", u8DeviceHandle));
      if (FALSE == bPostMethodStart(oPSBTAudioSource, 0))
      {
         ETG_TRACE_ERR(("spi_tclBluetoothClient::vSetBTProfile: Posting BluetoothAudioSource PureSet failed! "));
      }
   }
}

/***************************************************************************
** FUNCTION:  t_String spi_tclBluetoothClient::szGetBTDeviceName()
***************************************************************************/
t_String spi_tclBluetoothClient::szGetBTDeviceName(const t_String& rfcszDeviceBTAddress)
{
   t_String szDeviceName = "";

   if (true == IS_VALID_BT_ADDRESS(rfcszDeviceBTAddress))
   {
      m_oDevLstLock.s16Lock();

      //! Search for required device in BT DeviceList Map.
      for (tBTDevListMapConstItr DevLstMapCoItr = m_mapBTDeviceList.begin();
           DevLstMapCoItr != m_mapBTDeviceList.end();
           ++DevLstMapCoItr)
      {
         if (rfcszDeviceBTAddress == (DevLstMapCoItr->second).szBTAddress)
         {
            szDeviceName = (DevLstMapCoItr->second).szDeviceName;
            break;
         }
      } //for (tBTDevListMapConstItr DevLstMapCoItr = ...)

      m_oDevLstLock.vUnlock();
   }

   return szDeviceName;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclBluetoothClient::bConfigureHUState(...)
***************************************************************************/
t_Bool spi_tclBluetoothClient::bConfigureHUState(
      tenBTLocalModeType enPairableMode,
      tenBTLocalModeType enConnectableMode,
      const t_String& rfcszDeviceBTAddress)
{
   t_String szBTAddress = rfcszDeviceBTAddress;
   std::transform(szBTAddress.begin(), szBTAddress.end(), szBTAddress.begin(), ::tolower);

   //TODO - check if BT Address is empty in current pairable mode?
   //! Create & send SwitchBTLocalMode request
   btset_tMSSwitchBTLocMode oMSSwitchBTLocMode;
   oMSSwitchBTLocMode.Pairable.enType = static_cast<btset_tenBTLocalMode>(enPairableMode);
   oMSSwitchBTLocMode.Connectable.enType = static_cast<btset_tenBTLocalMode>(enConnectableMode);
   oMSSwitchBTLocMode.sBTAddress.bSet(szBTAddress.c_str(), CHAR_SET_UTF8);

   t_Bool bSendSuccess = bPostMethodStart(oMSSwitchBTLocMode, 0);

   ETG_TRACE_USR4(("spi_tclBluetoothClient::bConfigureHUState() left with "
         "bSendSuccess = %u (for PairableMode = %d, ConnectableMode = %d, BTAddress = %s) ",
         ETG_ENUM(BOOL, bSendSuccess), ETG_ENUM(BTSET_LOCAL_MODE, enPairableMode),
         ETG_ENUM(BTSET_LOCAL_MODE, enConnectableMode), szBTAddress.c_str()));

   return bSendSuccess;
}//! end of bConfigureHUState()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclBluetoothClient::bSwitchBTOnOff(...)
***************************************************************************/
t_Bool spi_tclBluetoothClient::bSwitchBTOnOff(t_Bool bBluetoothOnOff)
{
   btset_tMSSwitchBTOnOff oMSSwitchBTOnOff;
   oMSSwitchBTOnOff.bBTOnOff = bBluetoothOnOff;

   t_Bool bSendSuccess = bPostMethodStart(oMSSwitchBTOnOff, 0);

   ETG_TRACE_USR4(("spi_tclBluetoothClient::bSwitchBTOnOff() left with "
         "bSendSuccess = %u (for BluetoothOnOff = %d) ",
         ETG_ENUM(BOOL, bSendSuccess), ETG_ENUM(BOOL, bBluetoothOnOff)));

   return bSendSuccess;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclBluetoothClient::bGetBTOnStatus(...)
***************************************************************************/
t_Bool spi_tclBluetoothClient::bGetBTOnStatus()
{
   m_oBTOnOffLock.s16Lock();
   t_Bool bIsBluetoothOn = m_bIsBluetoothOn;
   m_oBTOnOffLock.vUnlock();

   return bIsBluetoothOn;
}

/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnStatusDeviceListExtended(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclBluetoothClient::
	vOnStatusDeviceListExtended(amt_tclServiceData* poMessage)
{
   /*lint -esym(40,fvOnBTDeviceNameUpdate)fvOnBTDeviceNameUpdate Undeclared identifier */

   //! Extract the MethodResult msg details
	btset_tXFiStDeviceListExt oXFiDevListExtStatus(*poMessage, BTSETTINGS_FI_MAJOR_VERSION);

   if (true == oXFiDevListExtStatus.bIsValid())
   {
      tU8 u8ListChangeDevHandle = oXFiDevListExtStatus.oDeviceListChange.u8DeviceHandle;
      btset_tenDevStatusType enListChangeDevStatus =
            oXFiDevListExtStatus.oDeviceListChange.e8DeviceStatus.enType;
      btset_tenDevDisconnReasonType enListChangeDevDisconnReason =
            oXFiDevListExtStatus.oDeviceListChange.e8DeviceDisconnectedReason.enType;

      //! Print Device List details
      ETG_TRACE_USR4(("spi_tclBluetoothClient::vOnStatusDeviceListExtended: NumPairedDevices = %u,"
            "DeviceListChange DeviceHandle = 0x%x, DeviceStatus = %u, DisconnectionReason = %u ",
            oXFiDevListExtStatus.u8NumPairedDevices,
            u8ListChangeDevHandle,
    		   ETG_ENUM(BTSET_DEVICE_STATUS, enListChangeDevStatus),
    		   ETG_ENUM(BTSET_DEVICEDISCONN_REASON, enListChangeDevDisconnReason)));

      //! Handle device deletion first, since deleted device will not be part of device list.
      if (BTDEV_DELETED == enListChangeDevStatus)
      {
         vRemoveFromDeviceList(u8ListChangeDevHandle);
      }

      //! Validate each device in list
      for (btset_tFiDeviceListExtItr LstItr = oXFiDevListExtStatus.oDeviceListExtendedResult.oItems.begin();
           LstItr != oXFiDevListExtStatus.oDeviceListExtendedResult.oItems.end();
           ++LstItr)
      {
         //!---------------------------------------------------------------------------------------
         // @Note:
         // 1. BTSettings FBlock sends DeviceList property update when a device is connected,
         // disconnected or deleted with DeviceListChange as CONNECTED_OSD/DISCONNECTED/DELETED
         // respectively.
         // DeviceListChange "NO_CHANGE" will (probably) only be sent during property registration.
         // 2. BTSettings FBlock does not send DeviceList property update when a device is paired.
         // (That info can be checked with PairingStatus update)
         //!---------------------------------------------------------------------------------------

         tU8 u8LstItemDevHandle = (*LstItr).u8DeviceHandle;
         tBool bIsNewDevice = (FALSE == bIsDeviceValid(u8LstItemDevHandle));

         //! If device already exists in DeviceList Map, update its status in the map.
         if (
            (FALSE == bIsNewDevice)
            &&
            (u8LstItemDevHandle == u8ListChangeDevHandle)
            &&
            ((BTDEV_CONNECTEDOSD == enListChangeDevStatus) || (BTDEV_DISCONNECTED == enListChangeDevStatus))
            )
         {
            vUpdateDeviceConnStatus(u8LstItemDevHandle, (*LstItr).bDeviceConnectedStatus);

            // @Note: Since as per current BT requirements only one BT device can be connected at any time,
            // DeviceStatus "CONNECTED" and "CHANGED_TO_OSD" are invalid, and hence not checked.
            // "DELETED" status is not handled here.
         } //if ((FALSE == bIsNewDevice) && ...)

         //! If there is a new device in list, add it to DeviceList map.
         else if (TRUE == bIsNewDevice)
         {
            // @Note: Since as per current BT requirements only one BT device can be connected at a time,
            // the OutgoingSourceDeviceStatus should be same as bDeviceConnectedStatus for each device.
            tBool bLstItemDevConnStatus = (*LstItr).bOutgoingSourceDeviceStatus;
            SPI_NORMAL_ASSERT((*LstItr).bDeviceConnectedStatus != bLstItemDevConnStatus);

	         t_String szLstItemDeviceName = "";
            GET_STRINGDATA_FROM_FI_STRINGOBJ((*LstItr).sDeviceName, CHAR_SET_UTF8, szLstItemDeviceName);

            t_String szLstItemBTAddress = "";
            GET_STRINGDATA_FROM_FI_STRINGOBJ((*LstItr).sDeviceAddress, CHAR_SET_UTF8, szLstItemBTAddress);
            std::transform(szLstItemBTAddress.begin(), szLstItemBTAddress.end(), szLstItemBTAddress.begin(), ::toupper);

            //! Print details of new device in list.
            ETG_TRACE_USR4(("spi_tclBluetoothClient::vOnStatusDeviceListExtended: DeviceListResult info: "
                  "DeviceHandle = 0x%x, DateTimeStamp = %s ",
                  u8LstItemDevHandle, szGetDateTimestamp((*LstItr).oDateTimeStamp).c_str()));

            vAddToDeviceList(u8LstItemDevHandle, bLstItemDevConnStatus, szLstItemDeviceName, szLstItemBTAddress);
            // @Note: Need to add device here (and not only on PairingStatus update), due to below scenarios:
            // a) Startup scenario (Since during startup only DeviceList update would be received.
            // PairingStatus will not be sent for already paired devices)
            // b) In the event that DeviceList update (for device connection) is received
            // before PairingStatus update is received (for a newly paired device).

            //! Notify connected device
            if (true == bLstItemDevConnStatus)
            {
               vNotifyBTConnectionChanged(szLstItemBTAddress, e8BT_RESULT_CONNECTED);
            }

            //! Notify device name
            if ((szLstItemBTAddress == m_rBTDeviceNameCbInfo.szBTAddress) &&
                  (NULL != m_rBTDeviceNameCbInfo.fvOnBTDeviceNameUpdate))
            {
               m_rBTDeviceNameCbInfo.fvOnBTDeviceNameUpdate(szLstItemBTAddress, szLstItemDeviceName);
            }
         }//else if (TRUE == bIsNewDevice)
      } //for (btset_tFiDeviceListExtItr LstItr = ...)
   } //if (true == oXFiDevListExtStatus.bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnStatusDeviceListExtended: Invalid message received! "));
   }

}//! end of vOnStatusDeviceListExtended()

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnMRChangeDevState(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclBluetoothClient::
      vOnMRChangeDevState(amt_tclServiceData* poMessage)
{
   //! Extract the MethodResult msg details
   btset_tXFiMRChangeDevState oXFiDevState(*poMessage, BTSETTINGS_FI_MAJOR_VERSION);

   if (true == oXFiDevState.bIsValid())
   {
      //Extract device handle from CmdCounter of message
      trUserContext rUsrCtxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCtxt);
      tU8 u8DeviceHandle = static_cast<tU8>(rUsrCtxt.u32CmdCtr);

      //! Print ChangeDeviceState result details
      ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnMRChangeDevState() entered: "
            "DeviceHandle(extracted) = 0x%x, ConnectionResult = %u ",
            u8DeviceHandle,
            ETG_ENUM(BTSET_CONNECTION_RESULT, oXFiDevState.e8ConnectionResult.enType)));

      SPI_NORMAL_ASSERT(oXFiDevState.bReplaceDeviceRequired);
      // @Note: 'ReplaceDeviceRequired' flag is used only when two devices can be simultaneously 
      // connected. Since as per current BT requirements only one BT device can be connected at
      // any time, this flag should not be used.

      //! Retrieve ChangeDeviceState request info from map using Key as extracted DeviceHandle,
      //! and validate the result of ChangeDeviceState operation.
      trBTChangeDeviceRequestInfo rReqInfo;

      if (
         (TRUE == bFindChangeDevReqInfoInMap(u8DeviceHandle, rReqInfo))
         &&
         (e8BT_REQUEST_UNKNOWN != rReqInfo.enRequestType)
         )
      {
         ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnMRChangeDevState() entered: ConnectionRequest = %u ",
               ETG_ENUM(BT_CONNECTION_REQ_TYPE, rReqInfo.enRequestType)));

         //! Analyze the device connection result based on the request type stored in
         //! ChngDevRequestsList map & notify result to BT Manager
         tenBTConnectionResult enConnResult = enGetBTConnResult(rReqInfo.enRequestType,
               oXFiDevState.e8ConnectionResult.enType);
         
         //! If Block All is successful, set all devices as disconnected in internal BT device list.
         //@Note: This is done to prevent triggering duplicate device switch events in BTManager when
         //one or more DisableBT msg is received from CarPlay
         if ((e8BT_RESULT_BLOCKED == enConnResult) && (cou8InvalidDeviceHandle == u8DeviceHandle))
         {
            vSetDevicesDisconnected();
         }
         
         vNotifyBTConnectionResult(rReqInfo.szDevBTAddress, enConnResult);

      } //if ((TRUE == bFindChangeDevReqInfoInMap(u8DeviceHandle, rReqInfo)) && ...)
      else
      {
         //! Notify default error to BT Manager
         vNotifyBTRequestError();
         ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnMRChangeDevState: "
               "Invalid handle/ChangeDeviceState request not found/Invalid request! "));
      }

      //! Remove the ChangeDeviceState request info from map (since the response is received).
      vDeleteDeviceChangeRequestInfo(u8DeviceHandle);

   } //if (true == oXFiDevState.bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnMRChangeDevState: Invalid message received! "));
   }
}//! end of vOnMRChangeDevState()

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnErrorChangeDevState(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclBluetoothClient::
      vOnErrorChangeDevState(amt_tclServiceData* poMessage)
{
   //! Extract the MethodResult msg details
   btset_tXFiErrChangeDevState oXFiDevStateErr(*poMessage, BTSETTINGS_FI_MAJOR_VERSION);

   if (true == oXFiDevStateErr.bIsValid())
   {
      //Extract device handle from CmdCounter of message
      trUserContext rUsrCtxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCtxt);
      tU8 u8DeviceHandle = static_cast<tU8>(rUsrCtxt.u32CmdCtr);
      ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnErrorChangeDevState() entered: "
            "DeviceHandle(extracted) = 0x%x ", u8DeviceHandle));

      //! Retrieve ChangeDeviceState request info from map using Key as extracted DeviceHandle,
      //! and forward the error result to BT Manager.
      trBTChangeDeviceRequestInfo rReqInfo;

      if (
         (TRUE == bFindChangeDevReqInfoInMap(u8DeviceHandle, rReqInfo))
         &&
         (e8BT_REQUEST_UNKNOWN != rReqInfo.enRequestType)
         )
      {
         //! Notify error result to BT Manager
         vNotifyBTRequestError(rReqInfo.szDevBTAddress, rReqInfo.enRequestType);
      } //if ((TRUE == bFindChangeDevReqInfoInMap(u8DeviceHandle, rReqInfo)) && ...)
      else
      {
         //! Notify default error to BT Manager
         vNotifyBTRequestError();
         ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnErrorChangeDevState: "
               "Invalid handle/ChangeDeviceState request not found/Invalid request! "));
      }
      //! Remove the ChangeDeviceState request info from map (since the response is received).
      vDeleteDeviceChangeRequestInfo(u8DeviceHandle);

   } //if (true == oXFiDevStateErr.bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnErrorChangeDevState: Invalid message received! "));
   }

}//! end of vOnErrorChangeDevState()

/**************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnStatusPairingStatus(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclBluetoothClient::
      vOnStatusPairingStatus(amt_tclServiceData* poMessage)
{
   btset_tXFiStPairingStatus oXFiPairingStatus(*poMessage, BTSETTINGS_FI_MAJOR_VERSION);

   if (true == oXFiPairingStatus.bIsValid())
   {
      tenBTSetPairingStatus enPairingStatus = static_cast<tenBTSetPairingStatus>(
            oXFiPairingStatus.e8PairingStatusType.enType);

      //! Print Pairing Status details
      ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnStatusPairingStatus() entered: "
            "DeviceHandle = 0x%x, PairingStatusType = %u ",
            oXFiPairingStatus.u8DeviceHandle,
            ETG_ENUM(BTSET_PAIRING_STATUS, enPairingStatus)));

      //TODO - currently info not required
   } //if (true == oXFiPairingStatus.bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnStatusPairingStatus: Invalid message received! "));
   }

}//! end of vOnStatusPairingStatus()

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnVehicleBTAddressStatus(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclBluetoothClient::
   vOnVehicleBTAddressStatus(amt_tclServiceData* poMessage)
{
   //! Extract the status msg
   btset_tXFiStVehicleBTAddr oXFiVehBTAddrMsg(*poMessage, BTSETTINGS_FI_MAJOR_VERSION);

#ifdef VARIANT_S_FTR_ENABLE_GM
   tU16 u16ServiceID = CCA_C_U16_SRV_FB_DEVICEPROJECTION;
#else
   tU16 u16ServiceID = CCA_C_U16_SRV_SMARTPHONEINTEGRATION;
#endif

   //! Retrieve Vehicle BT Address
   if (true == oXFiVehBTAddrMsg.bIsValid())
   {
      t_String szVehicleBTAddr = "";
      GET_STRINGDATA_FROM_FI_STRINGOBJ(
            oXFiVehBTAddrMsg.sVehicleBTAddress, CHAR_SET_UTF8, szVehicleBTAddr);

      ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnVehicleBTAddressStatus() entered: ServiceID = %d Vehicle BTAddress = %s ",
            u16ServiceID, szVehicleBTAddr.c_str()));

      //! Store the address
      if ((false == szVehicleBTAddr.empty()) && (OSAL_NULL != m_poMainAppl))
      {
         //Writing the data to SPI data pool for future use
          Datapool oDatapool;
          oDatapool.bWriteBluetoothMacAddress(szVehicleBTAddr);

          tLbOnVehicleBTAdressUpdate
            oBtAddressUpdate(m_poMainAppl->u16GetAppId(), // Source AppID
                           m_poMainAppl->u16GetAppId(),
                           0,
                           0,
                           u16ServiceID,
                           SPI_C_U16_IFID_VEHICLE_BTADDRESS_UPDATE,
                           AMT_C_U8_CCAMSG_OPCODE_STATUS,
                           (strlen(szVehicleBTAddr.c_str()) + 1));

          //! Set the data & post message
          oBtAddressUpdate.vSetData((const tChar*)(szVehicleBTAddr.c_str()));

          if (true == oBtAddressUpdate.bIsValid())
          {
             if (AIL_EN_N_NO_ERROR != m_poMainAppl->enPostMessage(&oBtAddressUpdate, TRUE))
             {
                ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnVehicleBTAddressStatus() Loopback message posting failed! "));
             }
          } //if (true == oAudioDevLbMsg.bIsValid())
          else
          {
              ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnVehicleBTAddressStatus(): Loopback message creation failed! "));
          }
      }
   } //if (true == oXFiVehBTAddrMsg.bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnVehicleBTAddressStatus: Invalid message received! "));
   }

}//! end of vOnVehicleBTAddressStatus()

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnBTAudioSourceStatus(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclBluetoothClient::vOnBTAudioSourceStatus(amt_tclServiceData* poMessage)
{
   //! Extract the status msg
   btset_tXFiStBTAudioSource oXFiBTAudioSourceMsg(*poMessage, BTSETTINGS_FI_MAJOR_VERSION);

   //! Retrieve DeviceHandle of BT Audio source
   if (true == oXFiBTAudioSourceMsg.bIsValid())
   {
      ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnBTAudioSourceStatus: Received DeviceHandle = %u ",
            oXFiBTAudioSourceMsg.u8DeviceHandle));
   } //if (true == oXFiBTAudioSourceMsg.bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnBTAudioSourceStatus: Invalid message received! "));
   }

}//! end of vOnBTAudioSourceStatus()

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnMRPairingResponse(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclBluetoothClient::vOnMRPairingResponse(amt_tclServiceData* poMessage)
{
   //! Extract the status msg
   btset_tXFiMRPairingResponse oXFiPairResponse(*poMessage, BTSETTINGS_FI_MAJOR_VERSION);

   if (true == oXFiPairResponse.bIsValid())
   {
      ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnMRPairingResponse(): PairingStatusType = %d ",
            ETG_ENUM(BTSET_PAIRING_STATUS, oXFiPairResponse.e8PairingStatusType.enType)));

      //! Add code

   } //if (true == oXFiPairResponse.bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnMRPairingResponse: Invalid message received! "));
   }
}//! end of vOnMRPairingResponse()

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnErrorPairingResponse(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclBluetoothClient::vOnErrorPairingResponse(amt_tclServiceData* poMessage)
{
   //! Extract the status msg
   btset_tXFiErrPairingResponse oXFiPairResponseErr(*poMessage, BTSETTINGS_FI_MAJOR_VERSION);

   if (true == oXFiPairResponseErr.bIsValid())
   {
      ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnErrorPairingResponse() entered "));

      //! Add code

   } //if (true == oXFiPairResponseErr.bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnErrorPairingResponse: Invalid message received! "));
   }
}//! end of vOnErrorPairingResponse()

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnMRSwitchBTLocalMode(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclBluetoothClient::vOnMRSwitchBTLocalMode(amt_tclServiceData* poMessage)
{
   //! Extract the msg
   btset_tXFiMRSwitchBTLocMode oXFiSwitchBTLocMode(*poMessage, BTSETTINGS_FI_MAJOR_VERSION);
   if (true == oXFiSwitchBTLocMode.bIsValid())
   {
      ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnMRSwitchBTLocalMode() entered: Result = %d ",
            ETG_ENUM(BOOL, oXFiSwitchBTLocMode.bResult)));
      //TODO - currently info not needed

   } //if (true == oXFiPairResponse.bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnMRSwitchBTLocalMode: Invalid message received! "));
   }
}//! end of vOnMRSwitchBTLocalMode()

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnErrorSwitchBTLocalMode(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclBluetoothClient::vOnErrorSwitchBTLocalMode(amt_tclServiceData* poMessage)
{
   //! Extract the status msg
   btset_tXFiErrSwitchBTLocMode oXFiSwitchBTLocModeErr(*poMessage, BTSETTINGS_FI_MAJOR_VERSION);
   if (true == oXFiSwitchBTLocModeErr.bIsValid())
   {
      ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnErrorSwitchBTLocalMode() entered "));
      //TODO - currently info not needed

   } //if (true == oXFiSwitchBTLocModeErr.bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnErrorSwitchBTLocalMode: Invalid message received! "));
   }
}//! end of vOnErrorSwitchBTLocalMode()

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnMRSwitchBTOnOff(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclBluetoothClient::vOnMRSwitchBTOnOff(amt_tclServiceData* poMessage)
{
   //! Extract the msg
   btset_tXFiMRSwitchBTOnOff oXFiSwitchBTOnOff(*poMessage, BTSETTINGS_FI_MAJOR_VERSION);
   if (true == oXFiSwitchBTOnOff.bIsValid())
   {
      ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnMRSwitchBTOnOff() entered: Result = %d ",
            ETG_ENUM(BOOL, oXFiSwitchBTOnOff.bBTOnOffResult)));

      //@Note: Result is not used since MR only indicates request is posted to BT stack
	  //Wait for property update to know the result.
	  
   } //if (true == oXFiPairResponse.bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnMRSwitchBTOnOff: Invalid message received! "));
   }
}//! end of vOnMRSwitchBTOnOff()

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnErrorSwitchBTOnOff(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclBluetoothClient::vOnErrorSwitchBTOnOff(amt_tclServiceData* poMessage)
{
   //! Extract the msg
   btset_tXFiErrSwitchBTOnOff oXFiSwitchBTOnOffErr(*poMessage, BTSETTINGS_FI_MAJOR_VERSION);
   if (true == oXFiSwitchBTOnOffErr.bIsValid())
   {
      ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnErrorSwitchBTOnOff() entered "));

      //TODO - information currently not required

   } //if (true == oXFiPairResponse.bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnErrorSwitchBTOnOff: Invalid message received! "));
   }
}//! end of vOnErrorSwitchBTOnOff()

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnStatusPairableMode(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclBluetoothClient::vOnStatusPairableMode(amt_tclServiceData* poMessage)
{
   btset_tXFiStPairableMode otXFiPairableMode(*poMessage, BTSETTINGS_FI_MAJOR_VERSION);
   if (true == otXFiPairableMode.bIsValid())
   {
      //! Extract info from msg.
      trBTPairiableModeInfo rPairableModeInfo;
      rPairableModeInfo.bHUInPairableState = otXFiPairableMode.Pairable;
      rPairableModeInfo.bHUInConnectableState = otXFiPairableMode.Connectable;
      rPairableModeInfo.bHUInPairingOngoingState = otXFiPairableMode.PairingInProgress;
      GET_STRINGDATA_FROM_FI_STRINGOBJ(
            otXFiPairableMode.sBTAddress, CHAR_SET_UTF8, rPairableModeInfo.szBTAddress);
      std::transform(rPairableModeInfo.szBTAddress.begin(), rPairableModeInfo.szBTAddress.end(),
            rPairableModeInfo.szBTAddress.begin(), ::toupper);

      ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnStatusPairableMode() entered: "
            "Pairable = %d, Connectable = %d, PairingInProgress = %d, BTAddress = %s ",
            ETG_ENUM(BOOL, rPairableModeInfo.bHUInPairableState),
            ETG_ENUM(BOOL, rPairableModeInfo.bHUInConnectableState),
            ETG_ENUM(BOOL, rPairableModeInfo.bHUInPairingOngoingState),
            rPairableModeInfo.szBTAddress.c_str()));

      if (NULL != m_rBTPairInfoCallbacks.fvOnBTPairableMode)
      {
         m_rBTPairInfoCallbacks.fvOnBTPairableMode(rPairableModeInfo);
      }

   } //if (true == otXFiPairableMode.bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnStatusPairableMode: Invalid message received! "));
   }
}//! end of vOnStatusPairableMode()

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnStatusPairingRequest(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclBluetoothClient::vOnStatusPairingRequest(amt_tclServiceData* poMessage)
{
   btset_tXFiStPairingRequest oXFiPairingReq(*poMessage, BTSETTINGS_FI_MAJOR_VERSION);
   if (true == oXFiPairingReq.bIsValid())
   {
      //! Extract info from msg.
      trBTPairingRequestInfo rPairingReqInfo;
      t_String szRemoteDevName;

      GET_STRINGDATA_FROM_FI_STRINGOBJ(
            oXFiPairingReq.sRemoteDeviceAddress, CHAR_SET_UTF8, rPairingReqInfo.szRemoteDevBTAddr);
      std::transform(rPairingReqInfo.szRemoteDevBTAddr.begin(), rPairingReqInfo.szRemoteDevBTAddr.end(),
            rPairingReqInfo.szRemoteDevBTAddr.begin(), ::toupper);

      GET_STRINGDATA_FROM_FI_STRINGOBJ(
            oXFiPairingReq.sPinNumber, CHAR_SET_UTF8, rPairingReqInfo.szPairingPin);
      GET_STRINGDATA_FROM_FI_STRINGOBJ(
            oXFiPairingReq.sRemoteDeviceAddress, CHAR_SET_UTF8, szRemoteDevName);

      rPairingReqInfo.enPairingMethod = static_cast<tenBTSetPairingMethod>(
            oXFiPairingReq.e8PairingType.enType);
      tBool bPairingInitiatedFromSytem =
            (most_fi_tcl_e8_BTSetOrigin::FI_EN_E8SYSTEM == oXFiPairingReq.e8Origin.enType);

      ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnStatusPairingRequest(): PairingInitiatedFromSytem = %d, RemoteDevBTAddr = %s ",
            ETG_ENUM(BOOL, bPairingInitiatedFromSytem), rPairingReqInfo.szRemoteDevBTAddr.c_str()));
      ETG_TRACE_USR2((" vOnStatusPairingRequest(): PairingType = %d, PairingPin = %s ",
            ETG_ENUM(BTSET_PAIRING_METHOD, rPairingReqInfo.enPairingMethod), rPairingReqInfo.szPairingPin.c_str()));
      ETG_TRACE_USR2((" vOnStatusPairingRequest(): RemoteDevName = %s ", szRemoteDevName.c_str()));

      if ((false == rPairingReqInfo.szPairingPin.empty()) &&
            (IS_VALID_BT_ADDRESS(rPairingReqInfo.szRemoteDevBTAddr)) &&
            (NULL != m_rBTPairInfoCallbacks.fvOnBTPairingInfo))
      {
         m_rBTPairInfoCallbacks.fvOnBTPairingInfo(rPairingReqInfo);
      }

   } //if (true == oXFiPairingReq.bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnStatusPairingRequest: Invalid message received! "));
   }
}//! end of vOnStatusPairingRequest()

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vOnStatusBluetoothOnOff(amt_tclServiceData...
**************************************************************************/
tVoid spi_tclBluetoothClient::vOnStatusBluetoothOnOff(amt_tclServiceData* poMessage)
{
   btset_tXFiStBTOnOff oXFiBTOnOff(*poMessage, BTSETTINGS_FI_MAJOR_VERSION);
   if (true == oXFiBTOnOff.bIsValid())
   {
      ETG_TRACE_USR1(("spi_tclBluetoothClient::vOnStatusBluetoothOnOff(): Is Bluetooth On = %d ",
            ETG_ENUM(BOOL, oXFiBTOnOff.bBTOnOff)));

      //! Store BT On/Off state
      m_oBTOnOffLock.s16Lock();
      t_Bool bNotifyBTOnOffChange = (m_bIsBluetoothOn != oXFiBTOnOff.bBTOnOff);
      m_bIsBluetoothOn = oXFiBTOnOff.bBTOnOff;
      m_oBTOnOffLock.vUnlock();

      if ((bNotifyBTOnOffChange) && (NULL != m_rBluetoothCallbacks.fvOnBTOnOffChanged))
      {
         m_rBluetoothCallbacks.fvOnBTOnOffChanged(oXFiBTOnOff.bBTOnOff);
      }
   } //if (true == oXFiBTOnOff.bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::vOnStatusBluetoothOnOff: Invalid message received! "));
   }
}//! end of vOnStatusBluetoothOnOff()

/**************************************************************************
** FUNCTION:  tBool spi_tclBluetoothClient::bPostMethodStart(const...)
**************************************************************************/
tBool spi_tclBluetoothClient::bPostMethodStart(
      const btset_FiMsgBase& rfcooBTSetMS, t_U32 u32CmdCounter) const
{
   tBool bSuccess = false;

   if (OSAL_NULL != m_poMainAppl)
   {
      //! Create Msg context
      trMsgContext rMsgCtxt;
      rMsgCtxt.rUserContext.u32SrcAppID  = CCA_C_U16_APP_SMARTPHONEINTEGRATION;
      rMsgCtxt.rUserContext.u32DestAppID = u16GetServerAppID();
      rMsgCtxt.rUserContext.u32RegID     = u16GetRegID();
      rMsgCtxt.rUserContext.u32CmdCtr    = u32CmdCounter;

      //!Post BT settings MethodStart
      FIMsgDispatch oMsgDispatcher(m_poMainAppl);
      bSuccess = oMsgDispatcher.bSendMessage(rfcooBTSetMS, rMsgCtxt, BTSETTINGS_FI_MAJOR_VERSION);
   }

   ETG_TRACE_USR2(("spi_tclBluetoothClient::bPostMethodStart() left with: "
         "Message post success = %u (for FID = 0x%x) ",
         ETG_ENUM(BOOL, bSuccess),
         rfcooBTSetMS.u16GetFunctionID()));
   return bSuccess;

}//! end of bPostMethodResult()

/***************************************************************************
** FUNCTION:  tU8 spi_tclBluetoothClient::u8GetBTDeviceHandle(const t_String&...)
***************************************************************************/
tU8 spi_tclBluetoothClient::u8GetBTDeviceHandle(const t_String& rfcoszDeviceBTAddress)
{
   tU8 u8BTDevHandle = cou8InvalidDeviceHandle;

   //! Search for Device with matching BTAddress in DeviceList Map.
   if (true == IS_VALID_BT_ADDRESS(rfcoszDeviceBTAddress))
   {
      m_oDevLstLock.s16Lock();

      for (tBTDevListMapConstItr DevLstMapCoItr = m_mapBTDeviceList.begin();
          DevLstMapCoItr != m_mapBTDeviceList.end();
          ++DevLstMapCoItr)
      {
         if (rfcoszDeviceBTAddress == ((DevLstMapCoItr->second).szBTAddress))
         {
            u8BTDevHandle = DevLstMapCoItr->first;
            break;
         }
      } //for (tBTDevListMapConstItr DevLstMapCoItr = ...)

      m_oDevLstLock.vUnlock();

      ETG_TRACE_USR4(("spi_tclBluetoothClient::u8GetBTDeviceHandle() left with: "
            "BTDeviceHandle = 0x%x (for BTAddress = %s) ",
            u8BTDevHandle, rfcoszDeviceBTAddress.c_str()));
   } //if (coszBlankBTAddress != rfcoszDeviceBTAddress)
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::u8GetBTDeviceHandle: Invalid BT Address!"));
   }
   return u8BTDevHandle;

}//! end of u32GetBTDeviceHandle()

/**************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vAddToDeviceList(tU8 u8DeviceHandle,...)
**************************************************************************/
tVoid spi_tclBluetoothClient::vAddToDeviceList(tU8 u8DeviceHandle,
      tBool bDeviceConnStatus, const t_String& rfcszDeviceName, const t_String& rfcszBTAddress)
{
   ETG_TRACE_USR1(("spi_tclBluetoothClient::vAddToDeviceList() entered: "
         "BT DeviceHandle = 0x%x, ConnectionStatus = %u, DeviceName = %s ",
         u8DeviceHandle, ETG_ENUM(BOOL, bDeviceConnStatus), rfcszDeviceName.c_str()));
   ETG_TRACE_USR1(("spi_tclBluetoothClient::vAddToDeviceList() entered: "
         "BT Address = %s ", rfcszBTAddress.c_str()));

   tBTDevListMapConstItr DevLstMapCoItr; //not used

   //! If device does not already exist in DeviceList map, add it and
   //! send GetDeviceInfo request to obtain BT Address.
   if (FALSE == bIsDeviceValid(u8DeviceHandle))
   {
      trBluetoothDeviceInfo rDevInfo;
      rDevInfo.bDeviceConnected = bDeviceConnStatus;
      rDevInfo.szDeviceName = rfcszDeviceName.c_str();
      rDevInfo.szBTAddress = rfcszBTAddress.c_str();

      //@Note: m_oDevLstLock should be used only after bIsDeviceValid() call, as it uses same lock.
      m_oDevLstLock.s16Lock();
      m_mapBTDeviceList.insert(std::pair<tU8, trBluetoothDeviceInfo>(u8DeviceHandle, rDevInfo));
      //@Note: BT address will be updated on GetDeviceInfo response, and
      //Connection status will be updated on DeviceList updates.
      m_oDevLstLock.vUnlock();

   } //if (FALSE == bIsDeviceValid(u8DeviceHandle))
   else
   {
      ETG_TRACE_ERR((" vAddToDeviceList: Device already exists/Invalid handle! "));
   }
}//! end of vAddToDeviceList()

/**************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vRemoveFromDeviceList(tU8 u8DeviceHandle)
**************************************************************************/
tVoid spi_tclBluetoothClient::vRemoveFromDeviceList(tU8 u8DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclBluetoothClient::vRemoveFromDeviceList() entered: BT DeviceHandle = 0x%x ", u8DeviceHandle));

   //! Remove device from DeviceList map.
   if (TRUE == bIsDeviceValid(u8DeviceHandle))
   {
      //@Note: m_oDevLstLock should be used only after bIsDeviceValid() call, as it uses same lock.
      m_oDevLstLock.s16Lock();

      tBTDevListMapItr DevLstMapItr = m_mapBTDeviceList.find(u8DeviceHandle);
      if (m_mapBTDeviceList.end() != DevLstMapItr)
      {
         m_mapBTDeviceList.erase(DevLstMapItr);
      }

      m_oDevLstLock.vUnlock();
   }//if (TRUE == bIsDeviceValid(u8DeviceHandle))

}//! end of vRemoveFromDeviceList()

/**************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vUpdateDeviceConnStatus(tU8...)
**************************************************************************/
tVoid spi_tclBluetoothClient::vUpdateDeviceConnStatus(tU8 u8DeviceHandle,
      tBool bDeviceConnectionStatus)
{
   ETG_TRACE_USR1(("spi_tclBluetoothClient::vUpdateDeviceConnStatus() entered: "
         "BT DeviceHandle = 0x%x, DeviceConnectionStatus = %u ",
         u8DeviceHandle, ETG_ENUM(BOOL, bDeviceConnectionStatus)));

   if (TRUE == bIsDeviceValid(u8DeviceHandle))
   {
      //@Note: m_oDevLstLock should be used only after bIsDeviceValid() call, as it uses same lock.
      m_oDevLstLock.s16Lock();

      tBTDevListMapItr DevLstMapItr = m_mapBTDeviceList.find(u8DeviceHandle);
      if (m_mapBTDeviceList.end() != DevLstMapItr)
      {
         //! Update connection status of device
         (DevLstMapItr->second).bDeviceConnected =
               static_cast<t_Bool> (bDeviceConnectionStatus);
         tenBTConnectionResult enConnResult = (bDeviceConnectionStatus) ?
               (e8BT_RESULT_CONNECTED) : (e8BT_RESULT_DISCONNECTED);

         //! Notify connection/disconnection change to BT Manager
         vNotifyBTConnectionChanged((DevLstMapItr->second).szBTAddress, enConnResult);
      }//if (m_mapBTDeviceList.end() != DevLstMapItr)

      m_oDevLstLock.vUnlock();
   }//if (TRUE == bIsDeviceValid(u8DeviceHandle))

}//! end of vUpdateDeviceConnStatus()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclBluetoothClient::bRequestChangeDeviceState(t_String...)
***************************************************************************/
t_Bool spi_tclBluetoothClient::bRequestChangeDeviceState(tU8 u8DeviceHandle,
      tenBTSetConnectionRequest enRequestType)
{
   t_Bool bChangeDevStateReqSent = false;

   //! Create & post ChangeDeviceState MS msg (with CmdCounter = DeviceHandle)
   // @Note: The CmdCounter value is set to DeviceHandle and extracted later on, 
   // since DeviceHandle parameter is not available in ChangeDeviceState method result/error message.
   btset_tMSChangeDevState oMSChangeDevState;
   oMSChangeDevState.u8DeviceHandle = u8DeviceHandle;
   oMSChangeDevState.e8ChangeDeviceStateAction.enType =
         static_cast<btset_tenChngDevStateAction>(enRequestType);

   if (TRUE == bPostMethodStart(oMSChangeDevState, u8DeviceHandle))
   {
      bChangeDevStateReqSent = true;

      //! Store ChangeDeviceState request details in map.
      // @Note: Since different types of requests are triggered via ChangeDeviceState method, 
      // request type should be stored to validate result in ChangeDeviceState method result/ error.
      t_String szBTAddress = (IS_VALID_DEVHANDLE(u8DeviceHandle)) ?
            (szGetBTDeviceAddress(u8DeviceHandle)) : ("");
      trBTChangeDeviceRequestInfo rReqInfo(szBTAddress, enRequestType);
      vStoreDeviceChangeRequestInfo(u8DeviceHandle, rReqInfo);
   }
   /* Not required to send error since function's return value can be used to validate 
      msg posting success. Code retained for future use.
   else
   {
      vNotifyBTRequestError(rfcoszDeviceBTAddress, enRequestType);
      ETG_TRACE_ERR(("bRequestChangeDeviceState: Posting ChangeDeviceState MethodStart failed! "));
   }*/

   ETG_TRACE_USR3(("spi_tclBluetoothClient::bRequestChangeDeviceState() left with "
         "bChangeDevStateReqSent = %u (for DevHandle = %u) ",
         ETG_ENUM(BOOL, bChangeDevStateReqSent), u8DeviceHandle));
   return bChangeDevStateReqSent;
}//! end of bRequestChangeDeviceState()

/**************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vStoreDeviceChangeRequestInfo(tU8 ...
**************************************************************************/
tVoid spi_tclBluetoothClient::vStoreDeviceChangeRequestInfo(tU8 u8DeviceHandle,
      const trBTChangeDeviceRequestInfo& rfcorRequestInfo)
{
   ETG_TRACE_USR1(("spi_tclBluetoothClient::vStoreDeviceChangeRequestInfo() entered: "
         "BT DeviceHandle = 0x%x, ChangeDeviceState RequestType = %u ",
         u8DeviceHandle, ETG_ENUM(BT_CONNECTION_REQ_TYPE, rfcorRequestInfo.enRequestType)));

   //! Store the ChangeDeviceRequest info in map.
   //! (If already exists, overwrite the data)
   m_oChnDevLstLock.s16Lock();

   tBTChngDevReqMapItr ChngDevReqMapItr = m_mapChngDevRequestsList.find(u8DeviceHandle);
   if (ChngDevReqMapItr == m_mapChngDevRequestsList.end())
   {
      m_mapChngDevRequestsList.insert(
         std::pair<tU8, trBTChangeDeviceRequestInfo>(u8DeviceHandle, rfcorRequestInfo));
   }
   else
   {
      (ChngDevReqMapItr->second).szDevBTAddress = rfcorRequestInfo.szDevBTAddress;
      (ChngDevReqMapItr->second).enRequestType = rfcorRequestInfo.enRequestType;
   }

   m_oChnDevLstLock.vUnlock();
}//! end of vStoreDeviceChangeRequestInfo()

/**************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vDeleteDeviceChangeRequestInfo(tU8 ...
**************************************************************************/
tVoid spi_tclBluetoothClient::vDeleteDeviceChangeRequestInfo(tU8 u8DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclBluetoothClient::vDeleteDeviceChangeRequestInfo() entered: "
         "BT DeviceHandle = 0x%x ", u8DeviceHandle));

   m_oChnDevLstLock.s16Lock();

   tBTChngDevReqMapItr ChngDevReqMapItr = m_mapChngDevRequestsList.find(u8DeviceHandle);

   //! Remove the ChangeDeviceRequest info from map.
   if (m_mapChngDevRequestsList.end() != ChngDevReqMapItr)
   {
      m_mapChngDevRequestsList.erase(ChngDevReqMapItr);
   } 
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::vDeleteDeviceChangeRequestInfo: "
            "ChangeDeviceState request info does not exist "));
   }

   m_oChnDevLstLock.vUnlock();

}//! end of vDeleteDeviceChangeRequestInfo()

/***************************************************************************
** FUNCTION:  tBool spi_tclBluetoothClient::bIsDeviceValid(tU8...)
***************************************************************************/
tBool spi_tclBluetoothClient::bIsDeviceValid(tU8 u8DeviceHandle)
{
   //! Search in DeviceList map for requested device
   tBool bDeviceFound = FALSE;
   if (IS_VALID_DEVHANDLE(u8DeviceHandle))
   {
      m_oDevLstLock.s16Lock();
      bDeviceFound = (m_mapBTDeviceList.end() != m_mapBTDeviceList.find(u8DeviceHandle));
      m_oDevLstLock.vUnlock();

      if (FALSE == bDeviceFound)
      {
         ETG_TRACE_ERR(("spi_tclBluetoothClient::bIsDeviceValid: DeviceHandle 0x%x not found in DeviceList! ",
            u8DeviceHandle));
      }
   }//if (IS_VALID_DEVHANDLE(u8DeviceHandle))
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::bIsDeviceValid: Invalid DeviceHandle 0x%x! ", u8DeviceHandle));
   }
   return bDeviceFound;

}//! end of bIsDeviceValid()

/***************************************************************************
** FUNCTION:  tBool spi_tclBluetoothClient::bFindChangeDevReqInfoInMap(tU8...)
***************************************************************************/
tBool spi_tclBluetoothClient::bFindChangeDevReqInfoInMap(tU8 u8DeviceHandle,
      trBTChangeDeviceRequestInfo& rfRequestInfo)
{
   tBool bRequestFound = FALSE;
   m_oChnDevLstLock.s16Lock();

   tBTChngDevReqMapConstItr ChngDevReqMapCoItr =
         m_mapChngDevRequestsList.find(u8DeviceHandle);
   if (m_mapChngDevRequestsList.end() != ChngDevReqMapCoItr)
   {
      rfRequestInfo = ChngDevReqMapCoItr->second;
      bRequestFound = TRUE;
   }

   m_oChnDevLstLock.vUnlock();

   if (FALSE == bRequestFound)
   {
      ETG_TRACE_ERR(("spi_tclBluetoothClient::bFindChangeDevReqInfoInMap: "
            "Request for DeviceHandle = 0x%x not found! ", u8DeviceHandle));
   }
   return bRequestFound;

}//! end of bFindChangeDevReqInfoInMap()

/**************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vNotifyBTConnectionChanged(...)
**************************************************************************/
tVoid spi_tclBluetoothClient::vNotifyBTConnectionChanged(
      const t_String& rfcoszDevBTAddress,
      tenBTConnectionResult enBTConnResult) const
{
	/*lint -esym(40,fvOnBTConnectionChanged)fvOnBTConnectionChanged Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclBluetoothClient::vNotifyBTConnectionChanged() entered: "
         "BT ConnectionResultType = %u, Device BTAddress = %s ",
         ETG_ENUM(BT_CONNECTION_RESULT, enBTConnResult),
         rfcoszDevBTAddress.c_str()));

   if (NULL != m_rBluetoothCallbacks.fvOnBTConnectionChanged)
   {
      //! Notify BT Manager with error info
      m_rBluetoothCallbacks.fvOnBTConnectionChanged(rfcoszDevBTAddress, enBTConnResult);
   } //if (NULL != m_rBluetoothCallbacks.fvOnBTConnectionResult)

}//! end of vNotifyBTConnectionChanged()

/**************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vNotifyBTConnectionResult(...)
**************************************************************************/
tVoid spi_tclBluetoothClient::vNotifyBTConnectionResult(
      const t_String& rfcoszDevBTAddress,
      tenBTConnectionResult enBTConnResult) const
{
	/*lint -esym(40,fvOnBTConnectionResult)fvOnBTConnectionResult Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclBluetoothClient::vNotifyBTConnectionResult() entered: "
         "BT ConnectionResultType = %u, Device BTAddress = %s ",
         ETG_ENUM(BT_CONNECTION_RESULT, enBTConnResult),
         rfcoszDevBTAddress.c_str()));

   if (NULL != m_rBluetoothCallbacks.fvOnBTConnectionResult)
   {
      //! Notify BT Manager with error info
      m_rBluetoothCallbacks.fvOnBTConnectionResult(rfcoszDevBTAddress, enBTConnResult);
   } //if (NULL != m_rBluetoothCallbacks.fvOnBTConnectionResult)

}//! end of vNotifyBTConnectionResult()

/**************************************************************************
** FUNCTION:  spi_tclBluetoothClient::vNotifyBTRequestError(...)
**************************************************************************/
tVoid spi_tclBluetoothClient::vNotifyBTRequestError(t_String szDeviceBTAddress,
      tenBTSetConnectionRequest enRequestType) const
{
	/*lint -esym(40,fvOnBTConnectionResult)fvOnBTConnectionResult Undeclared identifier */
	ETG_TRACE_USR1(("spi_tclBluetoothClient::vNotifyBTRequestError entered: "
         "BT ConnectionRequestType = %u, Device BTAddress = %s ",
         ETG_ENUM(BT_CONNECTION_REQ_TYPE, enRequestType),
         szDeviceBTAddress.c_str()));

   if (NULL != m_rBluetoothCallbacks.fvOnBTConnectionResult)
   {
      tenBTConnectionResult enConnResult = e8BT_RESULT_UNKNOWN;

      switch (enRequestType)
      {
         case e8BT_REQUEST_CONNECT:
            enConnResult = e8BT_RESULT_CONN_FAILED;
            break;
         case e8BT_REQUEST_DISCONNECT:
            enConnResult = e8BT_RESULT_DISCONN_FAILED;
            break;
         case e8BT_REQUEST_BLOCK:
            enConnResult = e8BT_RESULT_BLOCK_FAILED;
            break;
         case e8BT_REQUEST_UNBLOCK:
            enConnResult = e8BT_RESULT_UNBLOCK_FAILED;
            break;
         default:
            ETG_TRACE_ERR(("spi_tclBluetoothClient::vNotifyBTRequestError: Incorrect enum encountered! "));
            break;
      } //switch (enRequestType)

      //! Notify BT Manager with error info
      m_rBluetoothCallbacks.fvOnBTConnectionResult(szDeviceBTAddress, enConnResult);
   } //if (NULL != m_rBluetoothCallbacks.fvOnBTConnectionResult)

}//! end of vNotifyBTRequestError()

/**************************************************************************
** FUNCTION:  tVoid spi_tclBluetoothClient::szGetDateTimestamp(...)
**************************************************************************/
t_String spi_tclBluetoothClient::
      szGetDateTimestamp(const btset_tDateTimestamp& rfcooDateTimeStamp) const
{
   //! Extract Date Stamp
   t_String szYear;
   GET_STRINGDATA_FROM_FI_STRINGOBJ(
         rfcooDateTimeStamp.oDateStamp.sCldrYear, CHAR_SET_UTF8, szYear);
   t_String szMonth;
   GET_STRINGDATA_FROM_FI_STRINGOBJ(
         rfcooDateTimeStamp.oDateStamp.sCldrMonth, CHAR_SET_UTF8, szMonth);
   t_String szDay;
   GET_STRINGDATA_FROM_FI_STRINGOBJ(
         rfcooDateTimeStamp.oDateStamp.sCldrDay, CHAR_SET_UTF8, szDay);

   //! Extract Time Stamp
   t_String szHour;
   GET_STRINGDATA_FROM_FI_STRINGOBJ(
         rfcooDateTimeStamp.oTimeStamp.sHours, CHAR_SET_UTF8, szHour);
   t_String szMinute;
   GET_STRINGDATA_FROM_FI_STRINGOBJ(
         rfcooDateTimeStamp.oTimeStamp.sMinutes, CHAR_SET_UTF8, szMinute);
   t_String szSeconds;
   GET_STRINGDATA_FROM_FI_STRINGOBJ(
         rfcooDateTimeStamp.oTimeStamp.sSeconds, CHAR_SET_UTF8, szSeconds);

   //! Create DateTimeStamp
   t_String szDateTimeStamp =
         szYear + "/" + szMonth + "/" + szDay + " " +
         szHour + ":" + szMinute + ":" + szSeconds;

   return szDateTimeStamp;

}//! end of szGetDateTimestamp()

/**************************************************************************
** FUNCTION:  tVoid spi_tclBluetoothClient::szGetDateTimestamp(...)
**************************************************************************/
t_String spi_tclBluetoothClient::szGetBTDeviceAddress(tU8 u8DeviceHandle)
{
   t_String szBTAddress;

   m_oDevLstLock.s16Lock();
   tBTDevListMapItr DevLstMapItr =  m_mapBTDeviceList.find(u8DeviceHandle);
   if (m_mapBTDeviceList.end() != DevLstMapItr)
   {
      szBTAddress = (DevLstMapItr->second).szBTAddress;
   }
   m_oDevLstLock.vUnlock();

   return szBTAddress;
}

/***************************************************************************
** FUNCTION:  spi_tclBluetoothClient::enGetBTConnResult(...)
**************************************************************************/
tenBTConnectionResult spi_tclBluetoothClient::enGetBTConnResult(
      const tenBTSetConnectionRequest& rfcoBTConnRequest,
      const btset_tenBTConnectionResult& rfcoBTSetFiConnResult)
{
   //! Analyze the device connection result based on the request type
   tenBTConnectionResult enConnResult = e8BT_RESULT_UNKNOWN;
   switch (rfcoBTSetFiConnResult)
   {
      case BTDEVCONNRESULT_CONNECTED:
      {
         //@Note: BT Settings FBlock sends this result when:
         //1. Conn. request is successful.
         //2. Unblock request is successful & has resulted in auto-reconnection.
         enConnResult = (e8BT_REQUEST_CONNECT == rfcoBTConnRequest) ?
               (e8BT_RESULT_CONNECTED) : (e8BT_RESULT_UNBLOCKED);
      }
         break;
      case BTDEVCONNRESULT_DISCONNECTED:
      {
         //@Note: BT Settings FBlock sends this result when:
         //1. Disconn. request is successful.
         //2. Block request is successful & has caused a device disconnection.
         enConnResult = (e8BT_REQUEST_DISCONNECT == rfcoBTConnRequest) ?
               (e8BT_RESULT_DISCONNECTED) : (e8BT_RESULT_BLOCKED);
      }
         break;
      case BTDEVCONNRESULT_NOTCONNECTED:
      {
         //@Note: BT Settings FBlock sends this result when:
         //1. Conn./Disconn. request is unsuccessful.
         //2. Unblock request is successful, but has NOT resulted in auto-reconnection.
         if (e8BT_REQUEST_CONNECT == rfcoBTConnRequest)
         {
            enConnResult = e8BT_RESULT_CONN_FAILED;
         }
         else if (e8BT_REQUEST_DISCONNECT == rfcoBTConnRequest)
         {
            enConnResult = e8BT_RESULT_DISCONN_FAILED;
         }
         else if (e8BT_REQUEST_UNBLOCK == rfcoBTConnRequest)
         {
            enConnResult = e8BT_RESULT_UNBLOCKED;
         }
      }
         break;
      case BTDEVCONNRESULT_BLOCKED:
      {
         //@Note: BT Settings FBlock sends this result when:
         //Block request is successful, but has NOT caused a device disconnection.
         enConnResult = e8BT_RESULT_BLOCKED;
      }
         break;
      case BTDEVCONNRESULT_NOTAPPLICABLE:
      {
         //@Note: BT Settings FBlock sends this result when:
         //1. Unblock is successful, but there are no paired BT devices
         //2. Block is successful, but there are no paired BT devices
         if (e8BT_REQUEST_UNBLOCK == rfcoBTConnRequest)
         {
            enConnResult = e8BT_RESULT_UNBLOCKED;
         }
         else if (e8BT_REQUEST_BLOCK == rfcoBTConnRequest)
         {
            enConnResult = e8BT_RESULT_BLOCKED;
         }
      }
         break;
      case BTDEVCONNRESULT_BLOCKALLEXCEPT:
      {
         enConnResult = e8BT_RESULT_BLOCKED;
      }
         break;
      case BTDEVCONNRESULT_UNBOCKED:
      {
         enConnResult = e8BT_RESULT_UNBLOCKED;
      }
         break;
      default:
         ETG_TRACE_ERR(("spi_tclBluetoothClient::enGetBTConnResult: Incorrect enum encountered! "));
         break;
   } //switch (oXFiDevState.e8ConnectionResult.enType)

   return enConnResult;
}

/***************************************************************************
** FUNCTION:  tVoid spi_tclBluetoothClient::vSetDevicesDisconnected()
**************************************************************************/
tVoid spi_tclBluetoothClient::vSetDevicesDisconnected()
{
   ETG_TRACE_USR1(("spi_tclBluetoothClient::vSetDevicesDisconnected() entered "));

   m_oDevLstLock.s16Lock();

   for (tBTDevListMapItr DevLstMapItr = m_mapBTDeviceList.begin();
         DevLstMapItr != m_mapBTDeviceList.end();
         ++DevLstMapItr)
   {
      (DevLstMapItr->second).bDeviceConnected = false;
   } //for (tBTDevListMapItr DevLstMapItr = ...)

   m_oDevLstLock.vUnlock();
}

//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>
