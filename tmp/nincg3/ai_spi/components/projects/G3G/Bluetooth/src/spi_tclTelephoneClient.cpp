/*!
*******************************************************************************
* \file              spi_tclTelephoneClient.cpp
* \brief             Telephone FBlock Client handler class
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Telephone FBlock Client handler class
COPYRIGHT:      &copy; RBEI

HISTORY:
 Date       |  Author                           | Modifications
 06.03.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 27.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors
\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "XFiObjHandler.h"
#include "spi_tclTelephoneClient.h"

//For message dispatcher
#include "FIMsgDispatch.h"
using namespace shl::msgHandler;

#define GENERICMSGS_S_IMPORT_INTERFACE_GENERIC
#include "generic_msgs_if.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_BLUETOOTH
      #include "trcGenProj/Header/spi_tclTelephoneClient.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/
#define TELEPHONE_FI_MAJOR_VERSION  MOST_TELFI_C_U16_SERVICE_MAJORVERSION
#define TELEPHONE_FI_MINOR_VERSION  MOST_TELFI_C_U16_SERVICE_MINORVERSION
//#define TELEPHONE_FI_PATCH_VERSION  0

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
typedef XFiObjHandler<most_telfi_tclMsgCallStatusNoticeStatus>  tel_tXFiStCallStatusNotice;
typedef most_telfi_tclMsgAcceptCallMethodStart telFi_tMSAcceptCall;
typedef most_telfi_tclMsgHangUpCallMethodStart telFi_tMSHangUpCall;
typedef most_telfi_tclMsgRejectCallMethodStart telFi_tMSRejectCall ;
typedef most_telfi_tclMsgCancelOutgoingCallMethodStart telFi_tMSCancelCall;
typedef XFiObjHandler<most_telfi_tclMsgAcceptCallMethodResult> tel_XFiMRAcceptCall;
typedef XFiObjHandler<most_telfi_tclMsgHangUpCallMethodResult> tel_XFiMRHangUpCall;
typedef XFiObjHandler<most_telfi_tclMsgRejectCallMethodResult> tel_XFiMRRejectCall;
typedef XFiObjHandler<most_telfi_tclMsgRejectCallMethodResult> tel_XFiMRCancelCall;


//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/
#define CALLSTATUS_IDLE   (most_fi_tcl_e8_TelCallStatus::FI_EN_E8IDLE)
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
** CCA MESSAGE MAP
******************************************************************************/
BEGIN_MSG_MAP(spi_tclTelephoneClient, ahl_tclBaseWork)

   /* -------------------------------Methods.---------------------------------*/
   ON_MESSAGE_SVCDATA(MOST_TELFI_C_U16_ACCEPTCALL,
   AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnMRAcceptCall)
      ON_MESSAGE_SVCDATA(MOST_TELFI_C_U16_HANGUPCALL,
   AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnMRHangUpCall)
      ON_MESSAGE_SVCDATA(MOST_TELFI_C_U16_REJECTCALL,
   AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnMRRejectCall)
   ON_MESSAGE_SVCDATA(MOST_TELFI_C_U16_CALLSTATUSNOTICE,
   AMT_C_U8_CCAMSG_OPCODE_STATUS, vOnStatusCallStatusNotice)
   ON_MESSAGE_SVCDATA(MOST_TELFI_C_U16_CANCELOUTGOINGCALL,
    AMT_C_U8_CCAMSG_OPCODE_METHODRESULT, vOnMRCancelCall)

   /* -------------------------------Methods.---------------------------------*/

END_MSG_MAP()


/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclTelephoneClient::spi_tclTelephoneClient(spi_tclBluetoothPolicyBase...
**************************************************************************/
spi_tclTelephoneClient::spi_tclTelephoneClient(ahl_tclBaseOneThreadApp* poMainAppl, t_U16 u16ServiceID)
     : ahl_tclBaseOneThreadClientHandler(
         poMainAppl,                           /* Application Pointer */
         u16ServiceID,                         /* ID of used Service */
         TELEPHONE_FI_MAJOR_VERSION,           /* MajorVersion of used Service */
         TELEPHONE_FI_MINOR_VERSION),          /* MinorVersion of used Service */
       m_poMainAppl(poMainAppl)
{
   ETG_TRACE_USR1(("spi_tclTelephoneClient() entered with Service ID - %d ",u16ServiceID));
   NORMAL_M_ASSERT(OSAL_NULL != m_poMainAppl);
}//! end of spi_tclTelephoneClient()

/***************************************************************************
** FUNCTION:  virtual spi_tclTelephoneClient::~spi_tclTelephoneClient...
**************************************************************************/
spi_tclTelephoneClient::~spi_tclTelephoneClient()
{
   ETG_TRACE_USR1(("~spi_tclTelephoneClient() entered "));

   m_poMainAppl = OSAL_NULL;
}//! end of spi_tclTelephoneClient()

/***************************************************************************
** FUNCTION:  spi_tclTelephoneClient::vOnServiceAvailable()
**************************************************************************/
tVoid spi_tclTelephoneClient::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("spi_tclTelephoneClient::vOnServiceAvailable() entered "));
   vRegisterForProperties();
}//! end of vOnServiceAvailable()

/***************************************************************************
** FUNCTION:  spi_tclTelephoneClient::vOnServiceUnavailable()
**************************************************************************/
tVoid spi_tclTelephoneClient::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("spi_tclTelephoneClient::vOnServiceUnavailable() entered "));
   vUnregisterForProperties();
}//! end of vOnServiceUnavailable()

/***************************************************************************
** FUNCTION:  t_Void spi_tclTelephoneClient::vRegisterForProperties()
**************************************************************************/
t_Void spi_tclTelephoneClient::vRegisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclTelephoneClient::vRegisterForProperties() entered "));

   //! Register for interested properties
   vAddAutoRegisterForProperty(MOST_TELFI_C_U16_CALLSTATUSNOTICE);
}//! end of vRegisterForProperties()

/***************************************************************************
** FUNCTION:  t_Void spi_tclTelephoneClient::vUnregisterForProperties()
**************************************************************************/
t_Void spi_tclTelephoneClient::vUnregisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclTelephoneClient::vUnregisterForProperties() entered "));
   //! Unregister subscribed properties
   vRemoveAutoRegisterForProperty(MOST_TELFI_C_U16_CALLSTATUSNOTICE);
}//! end of vUnregisterForProperties()

/***************************************************************************
** FUNCTION:  t_Void spi_tclTelephoneClient::vRegisterCallbacks()
***************************************************************************/
t_Void spi_tclTelephoneClient::vRegisterCallbacks(trTelephoneCallbacks rTelephoneCallbacks)
{
   //! Store callbacks
   m_rTelCallbacks = rTelephoneCallbacks;
}

/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclTelephoneClient::vOnStatusCallStatusNotice(...)
**************************************************************************/
tVoid spi_tclTelephoneClient::
      vOnStatusCallStatusNotice(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclTelephoneClient::vOnStatusCallStatusNotice entered "));

	/*lint -esym(40,fvOnTelephoneCallActivity)fvOnTelephoneCallActivity Undeclared identifier */
	/*lint -esym(40,fvOnTelephoneCallStatus)fvOnTelephoneCallStatus Undeclared identifier */
   //! Extract the MethodResult msg details
   tel_tXFiStCallStatusNotice oXFiCallStatus(*poMessage, TELEPHONE_FI_MAJOR_VERSION);

   if (true == oXFiCallStatus.bIsValid())
   {
      t_Bool bIsCallActive = false;
      std::vector<trTelCallStatusInfo> vecTelCallStatusList;

      t_U32 u32CallListSize = oXFiCallStatus.oCallStatusNoticeStream.oItems.size();
      ETG_TRACE_USR4(("[PARAM]:vOnStatusCallStatusNotice: Call List Size: %d", u32CallListSize));

      //! Validate if any call is active
      for (tU32 u32LstIndx = 0; u32LstIndx < u32CallListSize; u32LstIndx++)
      {
         t_Bool bIsCallInstanceActive =
                  (CALLSTATUS_IDLE != (oXFiCallStatus.oCallStatusNoticeStream[u32LstIndx].e8CallStatus.enType));
         bIsCallActive = (bIsCallActive) || (bIsCallInstanceActive);
         
#ifdef VARIANT_S_FTR_ENABLE_GM_TELKEYHANDLING

         trTelCallStatusInfo rCallStatusInfo;
         rCallStatusInfo.enCallStatus = static_cast<tenCallStatus>(
               oXFiCallStatus.oCallStatusNoticeStream[u32LstIndx].e8CallStatus.enType);

         rCallStatusInfo.u16CallInstance = oXFiCallStatus.oCallStatusNoticeStream[u32LstIndx].u16CallInstance;
         ETG_TRACE_USR4(("[PARAM]:vOnStatusCallStatusNotice: Call Status = %d, Call Instance = %d ",
               ETG_ENUM(TEL_CALL_STATUS, rCallStatusInfo.enCallStatus),
               rCallStatusInfo.u16CallInstance));

         //Add call info to list
         vecTelCallStatusList.push_back(rCallStatusInfo);

#endif
      }

      //! Send overall Call activity info
      if (NULL != m_rTelCallbacks.fvOnTelephoneCallActivity)
      {
         m_rTelCallbacks.fvOnTelephoneCallActivity(bIsCallActive);
      }

      //! Send info on each call instance
      if (NULL != m_rTelCallbacks.fvOnTelephoneCallStatus)
      {
         m_rTelCallbacks.fvOnTelephoneCallStatus(vecTelCallStatusList);
      }
   } //if (true == oXFiCallStatus.bIsValid())
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnStatusCallStatusNotice: Invalid message received! "));
   }

}//! end of vOnStatusCallStatusNotice()


/***************************************************************************
** FUNCTION: t_Void spi_tclTelephoneClient::vTriggerAcceptCall(
                t_U16 u16CallInstance)
**************************************************************************/
t_Void spi_tclTelephoneClient::vTriggerAcceptCall(t_U16 u16CallInstance)
{
   ETG_TRACE_USR1(("spi_tclTelephoneClient::vTriggerAcceptCall entered "));

   telFi_tMSAcceptCall oMSAcceptCall;
   oMSAcceptCall.u16CallInstance = u16CallInstance;

   if (FALSE == bPostMethodStart(oMSAcceptCall))
   {
      ETG_TRACE_ERR(("[ERR]:vTriggerAcceptCall: Posting AcceptCall MethodStart failed "));
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclTelephoneClient::vTriggerHangUpCall(
                t_U16 u16CallInstance)
**************************************************************************/
t_Void spi_tclTelephoneClient::vTriggerHangUpCall(t_U16 u16CallInstance)
{
   ETG_TRACE_USR1(("spi_tclTelephoneClient::vTriggerHangUpCall entered "));

   telFi_tMSHangUpCall oMSHangUpCall;
   oMSHangUpCall.u16CallInstance = u16CallInstance;

   if (FALSE == bPostMethodStart(oMSHangUpCall))
   {
      ETG_TRACE_ERR(("[ERR]:vTriggerHangUpCall: Posting HangUpCall MethodStart failed "));
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclTelephoneClient::vTriggerRejectCall(
                t_U16 u16CallInstance)
**************************************************************************/
t_Void spi_tclTelephoneClient::vTriggerRejectCall(t_U16 u16CallInstance)
{
   ETG_TRACE_USR1(("spi_tclTelephoneClient::vTriggerRejectCall entered "));

   telFi_tMSRejectCall oMSRejectCall;
   oMSRejectCall.u16CallInstance = u16CallInstance;

   if (FALSE == bPostMethodStart(oMSRejectCall))
   {
      ETG_TRACE_ERR(("[ERR]:vTriggerRejectCall: Posting RejectCall MethodStart failed! "));
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclTelephoneClient::vTriggerCancelCall(
                t_U16 u16CallInstance)
**************************************************************************/
t_Void spi_tclTelephoneClient::vTriggerCancelCall(t_U16 u16CallInstance)
{
   ETG_TRACE_USR1(("spi_tclTelephoneClient::vTriggerCancelCall entered "));

  telFi_tMSCancelCall oMSCancelCall;
  oMSCancelCall.u16CallInstance = u16CallInstance;

   if (FALSE == bPostMethodStart(oMSCancelCall))
   {
      ETG_TRACE_ERR(("[ERR]:vTriggerCancelCall: Posting CancelCall MethodStart failed! "));
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclTelephoneClient::vOnMRHangUpCall(
amt_tclServiceData* poMessage)const
**************************************************************************/
t_Void spi_tclTelephoneClient::vOnMRHangUpCall(amt_tclServiceData* poMessage)const
{
   ETG_TRACE_USR1(("spi_tclTelephoneClient::vOnMRHangUpCall entered "));
   tel_XFiMRHangUpCall oXFiMRHangUpCall(*poMessage,TELEPHONE_FI_MAJOR_VERSION);

   if (true == oXFiMRHangUpCall.bIsValid())
   {
      t_U16 u16CallInstance = oXFiMRHangUpCall.u16CallInstance;
      tenCallStatus enCallStatus = static_cast<tenCallStatus>(oXFiMRHangUpCall.e8CallStatus.enType);
      ETG_TRACE_USR2(("[DESC]:vOnMRHangUpCall: Received HangUpCall result for Call Instance = %u, Call Status = %u ",
            u16CallInstance, ETG_ENUM(TEL_CALL_STATUS, enCallStatus)));
   }//if ((true == oXFiMRHangUpCall.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnMRHangUpCall: Invalid message/Null pointer "));
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclTelephoneClient::vOnMRAcceptCall(
amt_tclServiceData* poMessage)const
**************************************************************************/
t_Void spi_tclTelephoneClient::vOnMRAcceptCall(amt_tclServiceData* poMessage)const
{
   ETG_TRACE_USR1(("spi_tclTelephoneClient::vOnMRAcceptCall entered "));

  tel_XFiMRAcceptCall oXFiMRAcceptCall(*poMessage,TELEPHONE_FI_MAJOR_VERSION);

   if (true == oXFiMRAcceptCall.bIsValid())
   {
      t_U16 u16CallInstance = oXFiMRAcceptCall.u16CallInstance;
      tenCallStatus enCallStatus = static_cast<tenCallStatus>(oXFiMRAcceptCall.e8CallStatus.enType);
      ETG_TRACE_USR2(("[DESC]:vOnMRAcceptCall: Received AcceptCall result for Call Instance = %u, Call Status = %u ",
            u16CallInstance, ETG_ENUM(TEL_CALL_STATUS, enCallStatus)));
   }//if ((true == oXFiMRAcceptCall.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnMRAcceptCall: Invalid message/Null pointer "));
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclTelephoneClient::vOnMRRejectCall(
amt_tclServiceData* poMessage)const
**************************************************************************/
t_Void spi_tclTelephoneClient::vOnMRRejectCall(amt_tclServiceData* poMessage)const
{
   ETG_TRACE_USR1(("spi_tclTelephoneClient::vOnMRRejectCall entered "));

   tel_XFiMRRejectCall oXFiMRRejectCall(*poMessage,TELEPHONE_FI_MAJOR_VERSION);

   if (true == oXFiMRRejectCall.bIsValid())
   {
      t_U16 u16CallInstance = oXFiMRRejectCall.u16CallInstance;
      tenCallStatus enCallStatus = static_cast<tenCallStatus>(oXFiMRRejectCall.e8CallStatus.enType);
      ETG_TRACE_USR2(("[DESC]:vOnMRRejectCall: Received RejectCall result for Call Instance = %u, Call Status = %u ",
            u16CallInstance, ETG_ENUM(TEL_CALL_STATUS, enCallStatus)));
   }//if ((true == oXFiMRRejectCall.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnMRRejectCall: Invalid message/Null pointer "));
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclTelephoneClient::vOnMRCancelCall(
amt_tclServiceData* poMessage)const
**************************************************************************/
t_Void spi_tclTelephoneClient::vOnMRCancelCall(amt_tclServiceData* poMessage)const
{
   ETG_TRACE_USR1(("spi_tclTelephoneClient::vOnMRCancelCall entered "));

   tel_XFiMRCancelCall oXFiMRCancelCall(*poMessage,TELEPHONE_FI_MAJOR_VERSION);

   if (true == oXFiMRCancelCall.bIsValid())
   {
      t_U16 u16CallInstance = oXFiMRCancelCall.u16CallInstance;
      tenCallStatus enCallStatus = static_cast<tenCallStatus>(oXFiMRCancelCall.e8CallStatus.enType);
      ETG_TRACE_USR2(("[DESC]:vOnMRCancelCall: Received CancelCall result for Call Instance = %u, Call Status = %u ",
            u16CallInstance, ETG_ENUM(TEL_CALL_STATUS, enCallStatus)));
   }//if ((true == oXFiMRRejectCall.bIsValid())&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnMRCancelCall: Invalid message/Null pointer "));
   }
}

/**************************************************************************
** FUNCTION:  t_Bool spi_tclTelephoneClient::bPostMethodStart(const...)
**************************************************************************/
t_Bool spi_tclTelephoneClient::bPostMethodStart(
      const telfi_MsgBase& rfcooTelFIMS) const
{
   t_Bool bSuccess = false;
   if (OSAL_NULL != m_poMainAppl)
   {
      //! Create Msg context
      trMsgContext rMsgCtxt;
      rMsgCtxt.rUserContext.u32SrcAppID  = CCA_C_U16_APP_SMARTPHONEINTEGRATION;
      rMsgCtxt.rUserContext.u32DestAppID = u16GetServerAppID();
      rMsgCtxt.rUserContext.u32RegID     = u16GetRegID();
      rMsgCtxt.rUserContext.u32CmdCtr    = 0;

      //!Post Telephone Client Method Start
      FIMsgDispatch oMsgDispatcher(m_poMainAppl);
      bSuccess = oMsgDispatcher.bSendMessage(rfcooTelFIMS, rMsgCtxt, TELEPHONE_FI_MAJOR_VERSION);

      ETG_TRACE_USR2(("spi_tclTelephoneClient::bPostMethodStart() left with: "
            "Message post success = %u (for FID = 0x%x) ",
            ETG_ENUM(BOOL, bSuccess),
            rfcooTelFIMS.u16GetFunctionID()));
   }
   return bSuccess;

}//! end of bPostMethodResult()

//lint –restore

///////////////////////////////////////////////////////////////////////////////
// <EOF>


