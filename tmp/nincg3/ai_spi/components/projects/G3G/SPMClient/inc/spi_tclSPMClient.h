/*!
*******************************************************************************
* \file              spi_tclSPMClient.h
* \brief             SPM Client handler class
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    SPM Client handler class
COPYRIGHT:      &copy; RBEI

HISTORY:
 Date       |  Author                           | Modifications
 15.11.2014 |  Hari Priya E R (RBEI/ECP2)       | Initial Version

\endverbatim
******************************************************************************/

#ifndef _SPI_TCLSPMCLIENT_H_
#define _SPI_TCLSPMCLIENT_H_

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
//!Include Application Help Library.
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

//!Include common fi interface
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

//!Include SPM FI types
#define CCA_S_IMPORT_INTERFACE_GENERIC
#define CFC_FI_S_IMPORT_INTERFACE_CFC_SPMFI_TYPES
#define CFC_FI_S_IMPORT_INTERFACE_CFC_SPMFI_FUNCTIONIDS
#define CFC_FI_S_IMPORT_INTERFACE_CFC_SPMFI_SERVICEINFO
#include "cfc_fi_if.h"

#include "SPITypes.h"


/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
typedef cfc_spmfi_tclMsgBaseMessage spm_FiMsgBase;

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/* Forward Declarations. */

/*!
* \class spi_tclSPMClient
* \brief SPM client handler class.
*/
class spi_tclSPMClient
   : public ahl_tclBaseOneThreadClientHandler
{

public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclSPMClient::spi_tclSPMClient(ahl_tclBaseOneThreadApp...)
   **************************************************************************/
   /*!
   * \fn      spi_tclSPMClient(ahl_tclBaseOneThreadApp* poMainAppl)
   * \brief   Overloaded Constructor
   * \param   [IN] poMainAppl : Pointer to main CCA application
   **************************************************************************/
   spi_tclSPMClient(ahl_tclBaseOneThreadApp* poMainAppl);

   /***************************************************************************
   ** FUNCTION:  spi_tclSPMClient::~spi_tclSPMClient()
   **************************************************************************/
   /*!
   * \fn      ~spi_tclSPMClient()
   * \brief   Destructor
   **************************************************************************/
   virtual ~spi_tclSPMClient();

   /**************************************************************************
   * Overriding ahl_tclBaseOneThreadService methods.
   **************************************************************************/

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclSPMClient::vOnServiceAvailable();
   **************************************************************************/
   /*!
   * \fn      vOnServiceAvailable()
   * \brief   This method is called by the framework if the service of our
   *          server becomes available, e.g. server has been started.
   * \param   None
   **************************************************************************/
   virtual tVoid vOnServiceAvailable();

   /**************************************************************************
   ** FUNCTION:  tVoid spi_tclSPMClient::vOnServiceUnavailable();
   **************************************************************************/
   /*!
   * \fn      vOnServiceUnavailable()
   * \brief   This method is called by the framework if the service of our
   *          server becomes unavailable, e.g. server has been shut down.
   * \param   None
   **************************************************************************/
   virtual tVoid vOnServiceUnavailable();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSPMClient::vRegisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers for interested properties to SPM.
   **************************************************************************/
   virtual t_Void vRegisterForProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSPMClient::vUnregisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Registers for interested properties to SPM
   **************************************************************************/
   virtual t_Void vUnregisterForProperties();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclSPMClient::vSendSpmPhoneSubState()
   **************************************************************************/
   /*!
   * \fn      vSendSpmPhoneSubState(tBool bSubStateData)
   * \brief   Send the SPM SubState Info 
   * \param   bSubStateData: [IN] Boolean value which indicates the Substate 
                 data to be set or released
   **************************************************************************/
   t_Void vSendSpmPhoneSubState(tBool bSubStateData);


private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   * ! Handler method declarations used by message map.
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclSPMClient::vHandle_SystemState_Status(...)
   **************************************************************************/
   /*!
   * \fn      vHandle_SystemState_Status(amt_tclServiceData* poMessage)
   * \brief   Called by framework when System State property update message is
   *          sent by SPM.
   * \param   [IN] poMessage : Pointer to message
   **************************************************************************/
   tVoid vHandle_SystemState_Status(amt_tclServiceData* poMessage);

   /**************************************************************************
   * ! Other methods
   **************************************************************************/
   /**************************************************************************
   ** FUNCTION:  t_Bool spi_tclSpeechHMIClient::bPostMethodStart(const...
   **************************************************************************/
   /*!
   * \fn      bPostMessage(const spm_FiMsgBase& rfcoSPMMsgBase)
   * \brief   Posts a message.
   * \param   [IN] rfcoSPMMsgBase : SPM Fi message type
   * \retval  tBool: TRUE - if message posting is successful, else FALSE.
   **************************************************************************/
   t_Bool bPostMessage(const spm_FiMsgBase& rfcoSPMMsgBase) const;

   /**************************************************************************
   ** FUNCTION:  spi_tclSPMClient::spi_tclSPMClient()
   **************************************************************************/
   /*!
   * \fn      spi_tclSPMClient()
   * \brief   Default Constructor, will not be implemented.
   *          NOTE: This is a technique to disable the Default Constructor for
   *          this class. So if an attempt for the constructor is made compiler
   *          complains.
   **************************************************************************/
   spi_tclSPMClient();

   /**************************************************************************
   ** FUNCTION:  spi_tclSPMClient::spi_tclSPMClient(const...
   **************************************************************************/
   /*!
   * \fn      spi_tclSPMClient(const spi_tclSPMClient& oClient)
   * \brief   Copy Consturctor, will not be implemented.
   *          Avoids Lint Prio 3 warning: Info 1732: new in constructor for
   *          class'spi_tclSPMClient' which has no Copy Consturctor.
   *          NOTE: This is a technique to disable the Copy Consturctor for this
   *          class. So if an attempt for the copying is made linker complains.
   * \param   [IN] poMessage : Property to be set.
   **************************************************************************/
   spi_tclSPMClient(const spi_tclSPMClient& oClient);

   /**************************************************************************
   ** FUNCTION:  spi_tclSPMClient::spi_tclSPMClient& operator=(...
   **************************************************************************/
   /*!
   * \fn      spi_tclSPMClient& operator=(
   *          const spi_tclSPMClient& oClient)
   * \brief   Assingment Operater, will not be implemented.
   *          Avoids Lint Prio 3 warning: Info 1732: new in constructor for
   *          class 'spi_tclSPMClient' which has no assignment operator.
   *          NOTE: This is a technique to disable the assignment operator for this
   *          class. So if an attempt for the assignment is made compiler complains.
   **************************************************************************/
   spi_tclSPMClient& operator=(const spi_tclSPMClient& oClient);

   /***************************************************************************
   * ! Data members
   ***************************************************************************/

   /***************************************************************************
   ** Main Application pointer
   ***************************************************************************/
   ahl_tclBaseOneThreadApp*   m_poMainAppl;

};

#endif // _SPI_TCLSPMCLIENT_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
