/*!
*******************************************************************************
* \file              spi_tclSPMClient.h
* \brief             SPM Client handler class
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    SPM Client handler class
COPYRIGHT:      &copy; RBEI

HISTORY:
 Date       |  Author                           | Modifications
 15.11.2014 |  Hari Priya E R (RBEI/ECP2)       | Initial Version

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "spi_tclSPMClient.h"

//For message dispatcher
#include "FIMsgDispatch.h"
using namespace shl::msgHandler;

#define GENERICMSGS_S_IMPORT_INTERFACE_GENERIC
#include "generic_msgs_if.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
      #include "trcGenProj/Header/spi_tclSPMClient.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/
#define SPM_FI_MAJOR_VERSION  CFC_SPMFI_C_U16_SERVICE_MAJORVERSION
#define SPM_FI_MINOR_VERSION  CFC_SPMFI_C_U16_SERVICE_MINORVERSION


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
typedef cfc_spmfi_tclMsgSubStatesSet spm_tFiSubStatesSet;

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/
#define SPM_SUBSTATE_PHONE   (cfc_fi_tcl_SPM_e32_SubStateType::FI_EN_SPM_U32_SUBSTATE_PHONE);

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclSPMClient::spi_tclSPMClient(spi_tclBluetoothPolicyBase...
**************************************************************************/
spi_tclSPMClient::spi_tclSPMClient(ahl_tclBaseOneThreadApp* poMainAppl)
     : ahl_tclBaseOneThreadClientHandler(
         poMainAppl,                           /* Application Pointer */
         CCA_C_U16_SRV_SPM,                         /* ID of used Service */
         SPM_FI_MAJOR_VERSION,           /* MajorVersion of used Service */
         SPM_FI_MINOR_VERSION),          /* MinorVersion of used Service */
       m_poMainAppl(poMainAppl)
{
   NORMAL_M_ASSERT(OSAL_NULL != m_poMainAppl);
}//! end of spi_tclSPMClient()

/***************************************************************************
** FUNCTION:  virtual spi_tclSPMClient::~spi_tclSPMClient...
**************************************************************************/
spi_tclSPMClient::~spi_tclSPMClient()
{
   ETG_TRACE_USR1(("~spi_tclSPMClient() entered "));

   m_poMainAppl = OSAL_NULL;
}//! end of spi_tclSPMClient()

/***************************************************************************
** FUNCTION:  spi_tclSPMClient::vOnServiceAvailable()
**************************************************************************/
tVoid spi_tclSPMClient::vOnServiceAvailable()
{
   ETG_TRACE_USR1(("spi_tclSPMClient::vOnServiceAvailable() entered "));
}//! end of vOnServiceAvailable()

/***************************************************************************
** FUNCTION:  spi_tclSPMClient::vOnServiceUnavailable()
**************************************************************************/
tVoid spi_tclSPMClient::vOnServiceUnavailable()
{
   ETG_TRACE_USR1(("spi_tclSPMClient::vOnServiceUnavailable() entered "));
}//! end of vOnServiceUnavailable()

/***************************************************************************
** FUNCTION:  t_Void spi_tclSPMClient::vRegisterForProperties()
**************************************************************************/
t_Void spi_tclSPMClient::vRegisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclSPMClient::vRegisterForProperties() entered "));
   //! Register for interested properties
}//! end of vRegisterForProperties()

/***************************************************************************
** FUNCTION:  t_Void spi_tclSPMClient::vUnregisterForProperties()
**************************************************************************/
t_Void spi_tclSPMClient::vUnregisterForProperties()
{
   ETG_TRACE_USR1(("spi_tclSPMClient::vUnregisterForProperties() entered "));
   //! Unregister subscribed properties
}//! end of vUnregisterForProperties()


/***************************************************************************
** FUNCTION: t_Void spi_tclSPMClient::vSendSpmPhoneSubState(...)
**************************************************************************/
t_Void  spi_tclSPMClient::vSendSpmPhoneSubState(tBool bSubStateData)
{
   ETG_TRACE_USR1(("spi_tclSPMClient::vSendSpmPhoneSubState with bSubStateData:%d\n",
      ETG_ENUM(BOOL,bSubStateData)));
   spm_tFiSubStatesSet oFiSubStatesSet;
   // set parameter
   oFiSubStatesSet.SubStateData = bSubStateData;
   oFiSubStatesSet.SubStateType.enType = SPM_SUBSTATE_PHONE;

    if (FALSE == bPostMessage(oFiSubStatesSet))
   {
      ETG_TRACE_ERR(("vSendSpmPhoneSubState: Posting SPMSubState Property Set failed! \n"));
   }
}

/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/*******************************************************************************
** FUNCTION:   spi_tclSPMClient::bPostMethodStart(most_navfi_tclMsgBaseMessage...
*******************************************************************************/
t_Bool spi_tclSPMClient::bPostMessage(const spm_FiMsgBase& rfcoSPMMsgBase)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   tBool bSuccess = FALSE;

   if (OSAL_NULL != m_poMainAppl)
   {
      //! Create Msg context
      trMsgContext rMsgCtxt;
      rMsgCtxt.rUserContext.u32SrcAppID  = CCA_C_U16_APP_SMARTPHONEINTEGRATION;
      rMsgCtxt.rUserContext.u32DestAppID = u16GetServerAppID();
      rMsgCtxt.rUserContext.u32RegID     = u16GetRegID();
      rMsgCtxt.rUserContext.u32CmdCtr    = 0;

      //!Post BT settings MethodStart
      FIMsgDispatch oMsgDispatcher(m_poMainAppl);
      bSuccess = oMsgDispatcher.bSendMessage(rfcoSPMMsgBase, rMsgCtxt, SPM_FI_MAJOR_VERSION);
   }

   ETG_TRACE_USR2(("bPostMethodStart() left with: Message (FID = 0x%x) Post Success = %u \n",
         rfcoSPMMsgBase.u16GetFunctionID(), bSuccess));
   return bSuccess;
}



///////////////////////////////////////////////////////////////////////////////
// <EOF>


