#!/usr/local/bin/perl

use strict;
use File::Copy;
use File::Find;

#local and global variables
my $SPI_Folder="D:/Jenkins/Test_S2/views/SPI_Integration_G3g.vws/ai_spi";
my $LS_Folder="$SPI_Folder"."/Integration/LogiscopeData/";
my $InputFileName="";
my $Count=0;
my $FILELIST_FILE = "$LS_Folder"."FileList.lst";
my $localdirpath;
my $Component = undef;
my $create='"C:/Program Files (x86)/IBM/Rational/Logiscope/6.6/bin/create.exe"';
my $batch='"C:/Program Files (x86)/IBM/Rational/Logiscope/6.6/bin/batch.exe"';
my @components="";@components=qw(spi);
#my $zip='"C:\\Program Files\\7-Zip\\7z.exe"';

#main function
generate_metrics();
calculate_maintainability();
calculate_cc();
#run_compress_copy();
exit 0;

#----------------------------------------------------------------------------------------------
sub generate_metrics
#----------------------------------------------------------------------------------------------
{
    foreach my $component(@components){
        chomp($component);
        print "$component\n";
		$localdirpath = "$SPI_Folder";
        print "$localdirpath \n";

        print "Taking the product $component for finding the Code complexity...\n\n ";
        unlink  $FILELIST_FILE;
        open(FILO, ">$FILELIST_FILE") || die ("Could not open the file $FILELIST_FILE\n");
        find(\&findcppfiles, "$localdirpath");
        close(FILO);

        my $ttp="$LS_Folder"."spi.ttp";
        print "$ttp";
        # run the commands to find code complexity
        system "$create -audit -lang cpp $ttp -list $FILELIST_FILE" || die ("Could not create the project for Logiscope! Please check the environment variable or license availbility\n");
        system "$batch $ttp -table -noframe" || die ("Could not compile the created project for Quality check! Could be a problem of license availbility\n");
        system "$batch $ttp -table -noframe -addin export -format csv" || die ("Could not compile the created project for Quality check! Could be a problem of license availbility\n");
        print "\n\nDone.\n\n";
       
    }
}

#----------------------------------------------------------------------------------------------
sub calculate_maintainability
#----------------------------------------------------------------------------------------------
{
    my $quality="";
    my $compname="";
    my @array="";
    my @filelist="";
    my $file1="";
    my @MaintainabilityData;
	#my @MaintainabilityData_Fair;
	#my @MaintainabilityData_Good;
	#my @MaintainabilityData_Excellent;
    my @FileContents;    
    my $componentColumn = 0;
    my $ValueColumn = 2;
    my $componentId =0;
	#my $componentColumn_Fair = 0;
    #my $ValueColumn_Fair = 2;
    #my $componentId_Fair =0;
	#my $componentColumn_Good = 0;
    #my $ValueColumn_Good = 2;
    #my $componentId_Good =0;
	#my $componentColumn_Excellent = 0;
    #my $ValueColumn_Excellent = 2;
    #my $componentId_Excellent =0;
    #my $MaintainabilityMetricsPoor  ="$LS_Folder/"."maintainability_poor.csv"; unlink $MaintainabilityMetricsPoor;
	#my $MaintainabilityMetricsFair  ="$LS_Folder/"."maintainability_fair.csv"; unlink $MaintainabilityMetricsFair;
	#my $MaintainabilityMetricsGood  ="$LS_Folder/"."maintainability_good.csv"; unlink $MaintainabilityMetricsGood;
	my $MaintainabilityMetrics  ="$LS_Folder/"."maintainability.csv"; unlink $MaintainabilityMetrics;
  
    foreach $compname (@components)
    {
        # Poor calculation starts #################################################
		@array="";
        $Count=0;
        $InputFileName="$LS_Folder"."/Logiscope/reports.dir/"."$compname"."quality.html";
        open(READ_LINES,"<$InputFileName") or die ("Can't open input file $InputFileName");
        @FileContents=<READ_LINES>;
    
        foreach (@FileContents){
            chomp($_);
            if (/Factor : MAINTAINABILITY : POOR/../<\/table>/){ 
                $quality = "MAINTAINABILITY : POOR";
                push @array, $_ unless /^(Factor : MAINTAINABILITY : POOR|<\/table>)$/;
            }           
        }      

        foreach $file1 (@array) {   
            if ($file1 =~/::/ig){ 
                $Count++;
                push @filelist, $file1;
            }
        }
        print "-------------> $compname -->  $Count \n";
        $MaintainabilityData[$componentId]->[$componentColumn] = $compname;
		$MaintainabilityData[$componentId]->[$ValueColumn] = $Count;    
        $componentId++;
		
		open(MAINTAINABILITYCOUNT,">$MaintainabilityMetrics") or die ("Can't open $MaintainabilityMetrics");
		my $index = 0;
		while  ($index < $componentId)
		{
			print MAINTAINABILITYCOUNT $MaintainabilityData[$index]->[$componentColumn].",";
			$index++;
		}
		print MAINTAINABILITYCOUNT"\n";
		$index=0;
		while  ($index < $componentId)
		{
			print MAINTAINABILITYCOUNT $MaintainabilityData[$index]->[$ValueColumn].",";
			$index++;
		}    
		close (MAINTAINABILITYCOUNT);
		
		# Poor calculation ends #################################################
		
		# Fair calculation starts #################################################
		@array="";
        $Count=0;
        $InputFileName="$LS_Folder"."/Logiscope/reports.dir/"."$compname"."quality.html";
        open(READ_LINES,"<$InputFileName") or die ("Can't open input file $InputFileName");
        @FileContents=<READ_LINES>;
    
        foreach (@FileContents){
            chomp($_);
            if (/Factor : MAINTAINABILITY : FAIR/../<\/table>/){ 
                $quality = "MAINTAINABILITY : FAIR";
                push @array, $_ unless /^(Factor : MAINTAINABILITY : FAIR|<\/table>)$/;
            }           
        }      

        foreach $file1 (@array) {   
            if ($file1 =~/::/ig){ 
                $Count++;
                push @filelist, $file1;
            }
        }
        print "-------------> $compname -->  $Count \n";
        $MaintainabilityData[$componentId]->[$componentColumn] = $compname;
		$MaintainabilityData[$componentId]->[$ValueColumn] = $Count;    
        $componentId++;
		
		open(MAINTAINABILITYCOUNT,">$MaintainabilityMetrics") or die ("Can't open $MaintainabilityMetrics");
		my $index = 0;
		while  ($index < $componentId)
		{
			print MAINTAINABILITYCOUNT $MaintainabilityData[$index]->[$componentColumn].",";
			$index++;
		}
		print MAINTAINABILITYCOUNT "\n";
		$index=0;
		while  ($index < $componentId)
		{
			print MAINTAINABILITYCOUNT $MaintainabilityData[$index]->[$ValueColumn].",";
			$index++;
		}    
		close (MAINTAINABILITYCOUNT);
		# Fair calculation ends #################################################

		
		
		# Good calculation start #################################################
		
		@array="";
        $Count=0;
        $InputFileName="$LS_Folder"."/Logiscope/reports.dir/"."$compname"."quality.html";
        open(READ_LINES,"<$InputFileName") or die ("Can't open input file $InputFileName");
        @FileContents=<READ_LINES>;
    
        foreach (@FileContents){
            chomp($_);
            if (/Factor : MAINTAINABILITY : GOOD/../<\/table>/){ 
                $quality = "MAINTAINABILITY : GOOD";
                push @array, $_ unless /^(Factor : MAINTAINABILITY : GOOD|<\/table>)$/;
            }           
        }      

        foreach $file1 (@array) {   
            if ($file1 =~/::/ig){ 
                $Count++;
                push @filelist, $file1;
            }
        }
        print "-------------> $compname -->  $Count \n";
        $MaintainabilityData[$componentId]->[$componentColumn] = $compname;
		$MaintainabilityData[$componentId]->[$ValueColumn] = $Count;    
        $componentId++;

		open(MAINTAINABILITYCOUNT,">$MaintainabilityMetrics") or die ("Can't open $MaintainabilityMetrics");
		my $index = 0;
		while  ($index < $componentId)
		{
			print MAINTAINABILITYCOUNT $MaintainabilityData[$index]->[$componentColumn].",";
			$index++;
		}
		print MAINTAINABILITYCOUNT "\n";
		$index=0;
		while  ($index < $componentId)
		{
			print MAINTAINABILITYCOUNT $MaintainabilityData[$index]->[$ValueColumn].",";
			$index++;
		}    
		close (MAINTAINABILITYCOUNT);
		
		# Good calculation ends #################################################
		
		# Excellent calculation Start #################################################
		@array="";
        $Count=0;
        $InputFileName="$LS_Folder"."/Logiscope/reports.dir/"."$compname"."quality.html";
        open(READ_LINES,"<$InputFileName") or die ("Can't open input file $InputFileName");
        @FileContents=<READ_LINES>;
    
        foreach (@FileContents){
            chomp($_);
            if (/Factor : MAINTAINABILITY : EXCELLENT/../<\/table>/){ 
                $quality = "MAINTAINABILITY : EXCELLENT";
                push @array, $_ unless /^(Factor : MAINTAINABILITY : EXCELLENT|<\/table>)$/;
            }           
        }      

        foreach $file1 (@array) {   
            if ($file1 =~/::/ig){ 
                $Count++;
                push @filelist, $file1;
            }
        }
        print "-------------> $compname -->  $Count \n";
        $MaintainabilityData[$componentId]->[$componentColumn] = $compname;
		$MaintainabilityData[$componentId]->[$ValueColumn] = $Count;    
        $componentId++;
		
		open(MAINTAINABILITYCOUNT,">$MaintainabilityMetrics") or die ("Can't open $MaintainabilityMetrics");
		my $index = 0;
		while  ($index < $componentId)
		{
			print MAINTAINABILITYCOUNT $MaintainabilityData[$index]->[$componentColumn].",";
			$index++;
		}
		print MAINTAINABILITYCOUNT "\n";
		$index=0;
		while  ($index < $componentId)
		{
			print MAINTAINABILITYCOUNT $MaintainabilityData[$index]->[$ValueColumn].",";
			$index++;
		}    
		close (MAINTAINABILITYCOUNT);
			
		# Excellent calculation ends #################################################
    }

    
}

#----------------------------------------------------------------------------------------------
sub calculate_cc
#----------------------------------------------------------------------------------------------
{
    my $quality="";
    my $compname="";
    my @array="";
    my @filelist="";
    my $file1="";
    my @CyclomaticData;      
    my $componentColumn=0;
    my $ValueColumn=1;
    my $componentId=0;
    my $CyclomaticComplexity  ="$LS_Folder/"."cc.csv"; unlink $CyclomaticComplexity;
    
    foreach $compname (@components)
    {
        @array="";
        $Count=0;
        $InputFileName="$LS_Folder/"."/Logiscope/reports.dir/"."$compname"."quality.html";
        open(READ_LINES,"<$InputFileName") or die ("Can't open input file $InputFileName");
        while(<READ_LINES>){
            chomp($_);
            if (/Metric : Cyclomatic number \(VG\)<\/A><\/H3><P>The objects/../<\/table>/){ 
                push @array, $_ unless /^(Metric : Cyclomatic number|<\/table>)$/;
            }           
        }      

        foreach $file1 (@array) {   
            if ($file1 =~/::/ig){ 
                $Count++;
                push @filelist, $file1;
            }
        }
        print "-------------> $compname -->  $Count \n";
        $CyclomaticData[$componentId]->[$componentColumn] = $compname;
		$CyclomaticData[$componentId]->[$ValueColumn] = $Count;    
        $componentId++;
    }
    
    open(CYCLOMATICCOUNT,">$CyclomaticComplexity") or die ("Can't open $CyclomaticComplexity");
    my $index = 0;
    while  ($index < $componentId)
    {
        print CYCLOMATICCOUNT $CyclomaticData[$index]->[$componentColumn].",";
        $index++;
    }
    print CYCLOMATICCOUNT "\n";
    $index=0;
    while  ($index < $componentId)
    {
        print CYCLOMATICCOUNT $CyclomaticData[$index]->[$ValueColumn].",";
        $index++;
    }    
    close (CYCLOMATICCOUNT);
}

#----------------------------------------------------------------------------------------------
#sub run_compress_copy
#----------------------------------------------------------------------------------------------
#{
    # compress the generated files
    #my $filename = &CurrTime;
    #my $cmd = "$zip a -tzip $filename $LS_Folder/*";
    #system($cmd);

    #copy it to a server
    #my $dest="\\\\10.47.34.130\\binaries\\RN_AIVI_HMI_nightlybuild\\logiscope_reports";
    #copy($filename,$dest) or die "Copy failed: $!"; 
#}

#----------------------------------------------------------------------------------------------
sub findcppfiles
#----------------------------------------------------------------------------------------------
{
	my @arrayoffiles;
	-f and /\.cpp$/ or return;
	my $filenamepath =$File::Find::name;
	my $filename = $_;
	print FILO "$filenamepath\n";
}

#----------------------------------------------------------------------------------------------
sub CurrTime             # Get current time and return it in a specified format
#----------------------------------------------------------------------------------------------
{
  my $FormatTime ="";
  my ($sec,$min,$hour,$day,$mon,$year,$weekday,$yearday,$isdst) = localtime (time);
  my $param1 = shift @_ || '';
  my $param2 = shift @_ || '';
  if ($param2 eq "format") {
    ($sec,$min,$hour,$day,$mon,$year,$weekday,$yearday,$isdst) = localtime ($param1);
  }

  my @Weekdays = ("Sun","Mon","Tue","Wed","Thu","Fri","Sat");

  $mon++; $yearday++; $year+=1900;

  my $yearshort = substr($year,2,2);
  $mon          = $mon < 10 ? $mon = "0".$mon : $mon;
  $day          = $day < 10 ? $day = "0".$day : $day;
  $hour         = $hour < 10 ? $hour = "0".$hour : $hour;
  $min          = $min < 10 ? $min = "0".$min : $min;
  $sec          = $sec < 10 ? $sec = "0".$sec : $sec;
  $weekday      = $Weekdays[$weekday];

  if ($param1 eq "filename" || $param2 eq "format") {
    $FormatTime = $yearshort.$mon.$day."_".$hour.$min;
  }
  else {
    	$FormatTime = "F:\\Aniket\\DOC\\logiscope_pdal\\logiscope.zip"
  }

  return $FormatTime;
}
