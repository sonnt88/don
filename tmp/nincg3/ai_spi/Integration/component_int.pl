#! /usr/bin/perl -w

#----------------------------------------------------------------------------------------------------------------------#
my $Filename         = 'component_int.pl';
my $Copyright        = '(c) 2014 Robert Bosch Engineering and Business Solutions India Pvt Limited';
my $VERSION          = '5.00';
my $Date             = '30.06.2014';
my $Author           = 'Dinesh Kumar Subbaiyan (RBEI/ECA2)';
my $lastModified     = '02.09.2014 - Dinesh Kumar Subbaiyan (RBEI/ECA2)';
#----------------------------------------------------------------------------------------------------------------------#

#------------------------------------------------------------------------------
# Perl modules used within the script
#------------------------------------------------------------------------------
use strict;
use warnings;
use Data::Dumper;
use File::Copy;
use Cwd qw();

#------------------------------------------------------------------------------
# global variables
#------------------------------------------------------------------------------
my $CurrentWorkDir		= Cwd::cwd();
#print "\n\n$CurrentWorkDir\n\n\n";
my $config_spec         ="";
my $CurrentLabel        ="";
my $OldLabel            ="";
my $path 				="";
my $ViewName            ="";
my $ViewPath            ="";
my $ViewType            ="";
my $mergeError          ="";
my $label				="";
my $label_split			="";
my $maxlabels			="";
my $ShortTime	 		="";
my $labelview_path      ="";
my $ipk_decision        ="";
my $labelview_path_check ="";
my $Step				= 1;
my $ERRORCODE			="";
my @labels				="";
my @exclude_labels_list ="";
my $flag 				="flase";

my $VIEWPATH =$ARGV[0];
my $CurrentWorkDir = "$VIEWPATH\\ai_spi";
#print $VIEWPATH;
my ( $block, %hash );
#print "murali,aniket";

#------------------------------------------------------------------------------
# Read values from configuration files
#------------------------------------------------------------------------------
#my $Jenkins_wrkspace    ="$ENV{WORKSPACE}";
my $Jenkins_wrkspace    ="$VIEWPATH\\ai_spi\\";
#my $build_config        ="$ENV{CLEARCASE_VIEWPATH}"."\\ai_internet\\doc\\Integration\\config.ini";
my $build_config        ="$VIEWPATH\\ai_spi\\Integration\\config.ini";
open(DATA,"<$build_config") or die ("Can't open input file");
while (<DATA>) {
    $block = $1 if /\[(.+)\]/;
    $hash{$block}{$1} = $2 if /(\S+?)\s*=\s*(\S+)/ and defined $block;
}
my $vob                =$hash{'BUILDPROPRTIES'}{'VOB'};
my $build_check_vob    =$hash{'BUILDCHECK'}{'BUILD_CHECK_VOB'};
my $buildcheck_one     =$hash{'BUILDCHECK'}{'BUILD_CHECK_LABEL'};
my $BranchName         =$hash{'BUILDPROPRTIES'}{'BRANCH'};
my $UserID             =$hash{'USERPROPERTIES'}{'USERID'};
my $patch_labels       =$hash{'PATCH_RULES'}{'PATCH_LABELS'};
my $patch_label_vob    =$hash{'PATCH_RULES'}{'VOB'};
my $exclude_labels     =$hash{'EXCLUDE_LABELS'}{'LABELS'}; 

#------------------------------------------------------------------------------
# Jenkins Variables and log files
#------------------------------------------------------------------------------
my $jenkins_log			="$Jenkins_wrkspace"."\\componenet_int.log";
my $file_output 		="$Jenkins_wrkspace"."\\Labels_waiting_for_integration.txt";
my $failed_labels       ="$Jenkins_wrkspace"."\\failed_labels.txt"; 
my $conflict_label      ="";
#my $viewpath            = "$ENV{CLEARCASE_VIEWPATH}"."$vob";
#my $viewpath            ="W:\\mnu4kor_di_midw_g3g_audio_int_6.vws";
my $viewpath            ="$VIEWPATH";
my $co_files			="$Jenkins_wrkspace"."\\checkout_files_list.txt";


#------------------------------------------------------------------------------
# calculate time and current working directory
#------------------------------------------------------------------------------
my $st = time();
my $lt = localtime($st);

#------------------------------------------------------------------------------
# Integration Server Details for view creation and build
#------------------------------------------------------------------------------
my $server_address      ="";  
my $linux_build_config  ="";  
my $linux_build_script  ="";  

#------------------------------------------------------------------------------
# remove old log files
#------------------------------------------------------------------------------
unlink $file_output;
unlink $jenkins_log;
unlink $co_files;

#------------------------------------------------------------------------------
# Main script
#------------------------------------------------------------------------------
eval_cc_env();
labels_wait();
#increase_version();
RESTART_MERGE:
read_data();
merge_labels();
merge_check();
#check_server();
#read_update_cs();
#update_view_cs();
#ipk_gen_check();
#linux_build_prep();

# collect all possible child processes and exit
1 while (wait() != -1);
exit(0);


#------------------------------------------------------------------------------
# Check the Clearcase Environment
#------------------------------------------------------------------------------
sub eval_cc_env
{
    &writeInfo("Evaluating CC environment for Sub Project Integration");
        
    #chdir($viewpath);
    #$CurrentWorkDir		= Cwd::cwd();
	#$CurrentWorkDir ="$VIEWPATH\\ai_spi";
	#$CurrentWorkDir		= "$VIEWPATH\\ai_spi\\Integration";
    
    if ( open (VIEW_INFO, "cleartool lsview -properties -full -cview|") )
    {
        while (<VIEW_INFO>) {
            if ($_ =~ /\s*(\S+)\s+(\S+\.vws).*/) {
                $ViewName = sprintf("%s",$1);
                $ViewPath = sprintf("%s",$2);
            }
            if ($_ =~ /Properties:\s*(\w+)\s.*/) {
                $ViewType = sprintf("%s",$1);
            }
        }
    close (VIEW_INFO);
    }
    &writeInfo("Script is running in the following ClearCase environment:\n  ViewName:   $ViewName\n  ViewPath:   $ViewPath\n  ViewType:   $ViewType\n  BranchName: $BranchName\n VOB: $vob \n");
}

#------------------------------------------------------------------------------
# List of label from waiting for int & check the excludes labels
#------------------------------------------------------------------------------
sub labels_wait
{
	&writeInfo("Total available labels in waiting for integration script are");
    
    my @labels_split_list="";
    
	my $wait_cmd= "\\\\kor-ccadm-CM-AI.apac.bosch.com\\di_cc\\cc\\di_admin\\tools\\perl\\bin\\WaitForInt.pl -VOB $vob -IntBranch $BranchName";
	my @labels_wait_int=`perl $wait_cmd`;chomp(@labels_wait_int);
   
    if (grep ! /^\s*$/, @labels_wait_int) {
        my $count=0;
        # Enter if only there are labels in exclude list
        if ($exclude_labels ne "") {
            foreach $label_split (@labels_wait_int) {
                @labels_split_list = (split (/\s+/,$label_split));
                foreach my $lable (@labels_split_list){      
                    open (MYFILE, ">>$file_output") or die "can't open $file_output: $!";
                    # get exclude labels list from config file and check against waiting for int list
                    @exclude_labels_list = split(',', $exclude_labels);
                    if ( my $found = grep { $_ eq $lable } @exclude_labels_list ) {
                        # If found do not write into waiting for int list
                        print "ALERT : $lable is in Exclude label list so, it will not be considered for integration\n";
                    }
                    else{
                        $count++;
                        $maxlabels++;
                        print  MYFILE "$lable \n";
                        print ("$count, $lable\n");
                    }
                }
            }
		}
        # If no labels in exclude list do normal process
        else {
		    foreach $label_split (@labels_wait_int){
            @labels_split_list = (split (/\s+/,$label_split));
            foreach my $lable (@labels_split_list){
                $count++;
                $maxlabels++;
                open (MYFILE, ">>$file_output") or die "can't open $file_output: $!";
                print  MYFILE "$lable \n";
                print ("$count, $lable\n");
                &writecontent($lable);
            }
        }
        close(MYFILE);
        }
	}
 
    else {
        print "ALERT : there are NO labels in Waiting for int. So, Integration will be stopeed \n";
        die;
    }
}

#------------------------------------------------------------------------------
# Increase Version Number
#------------------------------------------------------------------------------
sub increase_version
{
	&writeInfo("Increase Sub Project Version Number");
	
    my $Version=$CurrentWorkDir."/"."cc-label.txt";
    my $ProcedNext= "";
    my $OldLabel= "";

	if ( open (FH, "$Version") or die ("Can't open input file") ) {
			my @cc_label_content = <FH>;
			close FH;
			$CurrentLabel     = shift (@cc_label_content);
			$CurrentLabel     =~ /(\S+)\s+(\S+)(_)(\S+)\s+(\S+)/i;   
			$CurrentLabel     = sprintf("%s%s%02d",$2,$3,$4+1);  
			$OldLabel 		  = sprintf("%s%s%02d",$2,$3,$4);
	}
	else {
		$ERRORCODE = "Last version from $Version could not be evaluated! \n\n";
		$ERRORCODE .= "Check whether you have checkin/checkout access!=> EXIT!!!";
		goto ENDOFFILE;
	}
	
	local *update = sub
	{
		my $cmd = "cleartool co -nc $Version"; &SystemCommand($cmd);
		open (FH, ">$Version") || die "Could not write '$CurrentLabel' into $Version!";
		print FH "PRODUCT_VERSION_LABEL  $CurrentLabel $OldLabel\n";
		print "PRODUCT_VERSION_LABEL  $CurrentLabel $OldLabel\n";
		print FH "REGISTRY_VERSION_LABEL $CurrentLabel $OldLabel\n";
		print "REGISTRY_VERSION_LABEL $CurrentLabel $OldLabel\n";
		close FH;
	};

	STARTVERSION:	
    $ProcedNext="Y";  ## Hardcode it
	if (($ProcedNext eq 'y')||($ProcedNext eq 'Y')){
		update();
	}
	elsif (($ProcedNext eq 'n')||($ProcedNext eq 'N')){
		$CurrentLabel=$OldLabel;
	 }
	else { 
		print "Wrong selection.\n";
		goto STARTVERSION;
	}
}

#------------------------------------------------------------------------------
# Read the labels for the label list file 
#------------------------------------------------------------------------------	
sub read_data
{
	&writeInfo("Reading data from final label list !");
    
    @labels="";
    $maxlabels="";
    open(MYFILE,"<$file_output") or die ("Can't open input file");
	while(<MYFILE>){
		my $file_name=$_;
        $maxlabels++;
        chomp($file_name);
        push( @labels,$file_name);
	}
    chomp(@labels); @labels = grep defined, @labels; shift @labels; chop(@labels);  # clear the unwanted enties from the array
    print "@labels";    
    close(MYFILE);
}

#------------------------------------------------------------------------------
# Merge labels from waiting for int list
#------------------------------------------------------------------------------	
sub merge_labels
{   
	my $slno=0;
	open(MYFILE,"<$file_output") or die ("Can't open input file");
	#my $CurrWorkDir = $CurrentWorkDir."/".ai_spi;
	#my $CurrentWorkDir = "$VIEWPATH\\ai_spi";
	#print "$CurrentWorkDir\n";
	chdir($CurrentWorkDir);
	while(<MYFILE>){
		chomp($_);
		my $file_name=$_;
		print "\n";
		$slno++;
		&writeInfo("Merging from label $file_name");
		my $cmd = "cleartool findmerge $CurrentWorkDir -all  -c \"Integrate $file_name\" -fversion $file_name -merge -abort";
		print "$cmd\n";
               
		if (system ($cmd) ) {
			print (" FAILED: $?\n\n");
			$mergeError = 1;
		} 
		else {
			print " OK, completed\n\n";	
		}
   }
}


#------------------------------------------------------------------------------
# Check whether Merge is proper or not
#------------------------------------------------------------------------------	
sub merge_check

{

	&writeInfo("Checking whether all the labels are properly merged or not !");
    print "$CurrentWorkDir\n";
	my $i=0;
	$mergeError = 0;

	foreach my $label (@labels) {
		if ($label eq "") {
			$maxlabels--;
			next;
		}
		$i++;
		print "($i / $maxlabels)\tChecking if $label is fully merged ...\n";
		#&SystemCommand("start \"Checking if $label is fully merged\" perl \\\\kor-ccadm-CM-AI.apac.bosch.com\\di_cc\\cc\\di_admin\\tools\\perl\\bin\\findmergeLabel.pl -label $label");  
		#my $d = "perl \\\\kor-ccadm-CM-AI.apac.bosch.com\\di_cc\\cc\\di_admin\\tools\\perl\\bin\\findmergeLabel.pl -label $label";
		#print `$d`;
				my $d1 = "\\\\kor-ccadm-CM-AI.apac.bosch.com\\di_cc\\cc\\di_admin\\tools\\perl\\bin\\findmergeLabel.pl -label $label";
				my $d = `perl $d1`;
 print $d;
	}

    
	# wait for all checked labels
	my $file=""; my $line=""; my $checked = 0;
    my @mergedlabels ="";
    my @failedlabels="";
	$i=0;
	my %h_checkedlabels;
	my $lbcnt =  $maxlabels;
	print ("\n Checking $lbcnt labels.\nPlease wait until the process is finished ...\n");
	
	#print "1\n";
	while ($lbcnt != 0) {
	#print "1\n";
		foreach $label (@labels) {
        chomp($label);
			next if ($label eq "");
            chomp($label);
				#print "1\n";
			$file = "$label".".txt";
				#print "2\n";
			# see if label is already checked
			if (!defined($h_checkedlabels{$label})) { 
				# check if label is fully (OK) or incompletly (ERR) merged...

                chomp($CurrentWorkDir);
                my $test=$CurrentWorkDir."//OK_"."${file}";
				#print $test;
				if ( -e $test ) {
					$line = "OK, fully merged";
					print $line;
					push @mergedlabels, $label;
					unlink($test);         # delete log file for successfully merged label
					$checked = 1;
				} if ( -e $CurrentWorkDir."//ERR_"."${file}" ) {
					$line = "ERROR, merge incomplete";
					push @failedlabels, $label;
					$mergeError = 1;
					# unlink("./ERR_${file}");      # keep errors so do not delete the log file
					$checked = 1;
				}
			
				if ( $checked ) {
					$lbcnt--;   # decrease number of label counter
					$i++;       # increase number of checked labels
					print("\n\t($i / $maxlabels)\t $label: $line\n");
					$h_checkedlabels{$label} = $line;
					$checked = 0;
				}
			}
			
		}
		# next label
		if ( $lbcnt != 0) {
			sleep(5);   # wait 5 seconds before new checks will start (give findmerge a chance and time...)
		}
	}
	


    
    # check/backup exisisting directories - local sub-routine
    local *handle_conflict_labels = sub
    {
        my $d = shift;
        
        # Write the failed labels into a file for further reference
        open(WRITEFAIL,">$failed_labels") or die ("Can't open input file");
        print WRITEFAIL "$d \n";
        close(WRITEFAIL);
               
        # Remove the label from waiting for int list and do uncdo checkouk of the files
        open(WRITEWAIT,">$file_output") or die ("Can't open input file");
        foreach my $LINE ( @labels ) { 
            chomp($LINE);
            if  (($LINE=~m/$d/)) {
                my @unco_list=`cleartool find . -ver "lbtype_sub($d)" -print | perl -p -e "s/\@\@.*//"`;
                foreach my $unco_label (@unco_list) {
                    $unco_label =~ s/^.//g;
                    $unco_label="$CurrentWorkDir"."$unco_label";
                    $unco_label =~ s/\//\\/g;
                    `cleartool unco -keep $unco_label`
                }
            }
            else {
                print WRITEWAIT "$LINE \n";
            }
        }
        close(WRITEWAIT); 
    };
    
	if ( $mergeError ) {
		if ( $#mergedlabels >= 0 ) {
			&writecontent("The following labels are incompletly merged\n");
			print "\nThe following labels are incompletly merged:\n";
            
            # cleanup the array, exclude the first element of the array because its always an empty value
            chomp(@failedlabels); @failedlabels = grep defined, @failedlabels; shift @failedlabels;
                                 
            foreach my $faillabel( @failedlabels ) {
                if ( my $found = grep { $_ eq $faillabel } @exclude_labels_list ) {
                # If found do not call undo handle_conflict_labels subroutine
                    print "$faillabel is in Exclude label list so, it will not be considered as failed\n";
                }
                else {
                    print "\t $faillabel \n";
                    $conflict_label=$faillabel;
                    &handle_conflict_labels($conflict_label);
                }
            }
			&writecontent("The following labels are successfully merged\n");
			print "\nThe following labels are successfully merged:\n";
			foreach ( @mergedlabels ) {
				print "\t$_\n";
				&writecontent("\t $_ \n");
			}
			print "\n";
		} 
        else {
			&writeerror("ERROR: there are no successfully merged labels");
		}
    goto RESTART_MERGE;    
	}
}

#------------------------------------------------------------------------------
# Check build server is available or not for doing the build
#------------------------------------------------------------------------------
sub check_server
{
    &writeInfo("Server check for starting the integration");
    
    $server_address="\\\\10.47.38.24\\"."$UserID"."\\";  # BAN Backup Server      
    START:
    chdir($server_address);
    if (-e $server_address) {
        print "Server is available \n";
    }
    else {
        print "WARNING : $server_address is not available \n So, system will be waiting for 5 minutes and it will retry \n";
        print "WARNING : Please map the server samba share into an windows drive \n";

    }
}


#------------------------------------------------------------------------------
# Sub Routine for updating the version
#------------------------------------------------------------------------------
sub  read_update_cs
{  

	my $OldLabel ="";
	my $id="";
	
    &writeInfo("Correcting the Config spec with latest pre integration label");    
    
    $config_spec="C:/ccstg/"."$UserID"."_"."$buildcheck_one".".vws"."/"."config_spec";  
    my $Version=$CurrentWorkDir."/"."cc_label.txt";
    
	if ( open (FH, "$Version") or die ("Can't open input file") ) {
			my @cc_label_content = <FH>;
			close FH;
            $CurrentLabel     = shift (@cc_label_content);
			$CurrentLabel     =~ /(\S+)\s+(\S+)(_)(\S+)\s+(\S+)/i;
            $CurrentLabel     = sprintf("%s%s%02d",$2,$3,$4);
			$OldLabel 		  = sprintf($5,$6,$7);
	}

    # Read the CS    
    open(CSREAD,"<$config_spec") or die ("Can't open input file");
    my @LINES = <CSREAD>; 
    close( CSREAD); 
    
    # sleep for 5 seconds because same file is going to be opened again
    sleep(5);
    

    #$vob =~ s|^\\||;
    my $patch_label_vob_new=$patch_label_vob;
    $patch_label_vob_new=~ s|^\\||;
    
    # Update only the component label to the next verion in the created label view's CS
    open(CSWRITE,">$config_spec") or die ("Can't open input file");
    foreach my $LINE ( @LINES ) { 
		chomp($LINE);
        if  (($LINE=~m/$OldLabel/)){
            $LINE=~s/$OldLabel/$CurrentLabel/ig;
            print CSWRITE "$LINE  \n";
        }
        else {
            print CSWRITE "$LINE \n";
        }
	}
    close(CSWRITE); 
    
   # sleep for 3 seconds because same is going to be opeened again
    sleep(3);
    
    # Update the patch labels if it is mentioned in the config file or else skip this step itself
    if ($patch_labels ne "")
    {
        # Update only the patch labels in config spec
        open(CSWRITE,">$config_spec") or die ("Can't open input file");
        foreach my $LINE ( @LINES ) { 
            chomp($LINE);
            if (($LINE=~/$patch_label_vob_new/)&&($LINE!~/^load/)){  
                # Get the VOB id for applying patch label     
                my @cc_result=`cleartool lsvob -long $patch_label_vob`;
                    foreach my $cc_value( @cc_result ) { 
                    chomp($cc_value);
                    if  (($cc_value=~m/Vob family uuid:/)){
                        $cc_value =~s/\.//ig;
                        $cc_value =~s/\://ig;
                        my @vob_id = split (/\s+/,$cc_value);
                        $id=$vob_id[3];
                    }
                }
                # Patch label rule    
                my $patch_rule="element ["."$id"."="."$patch_label_vob"."]/..."."        ".  "$patch_labels"."            "."-nocheckout";
                print CSWRITE "$patch_rule  \n";
                print CSWRITE "$LINE  \n";               
            }
            else {
                print CSWRITE "$LINE \n";
            }
        }
        close(CSWRITE);
    }    
}

#------------------------------------------------------------------------------
# Sub Routine for updating the the config spec after chainging component label
#------------------------------------------------------------------------------
sub update_view_cs
{
    &writeInfo("Updating the Config spec of the label view with laest sub project integration label");
   
    print "labelview_path is $labelview_path \n";
    chdir($labelview_path_check);
    my $cmd="cleartool setcs -current";
    #SystemCommand($cmd);
}

#-----------------------------------------------------------------------------------------------------
# Check the checkedout files list and decide which components to be build 
#-----------------------------------------------------------------------------------------------------
sub ipk_gen_check
{
	&writeInfo("Checking whether .js file is checkedout or not for ipk generation");
    chdir($viewpath);
	`cleartool lsco -r -s -me $CurrentWorkDir > $co_files`;

    open(READFILE,"<$co_files") or die ("Can't open input file");
    while(<READFILE>){
        chomp($_);
        if ($_ =~ m/iApps.js/i){
            $ipk_decision="IPK";
            #print " $ipk_decision \n";
            last;
        }
        else {
            $ipk_decision="NIL";
            #print " $ipk_decision \n";
        }
    }
    close(READFILE);
}

#------------------------------------------------------------------------------
# Preparing the details required for linux build
#------------------------------------------------------------------------------
sub linux_build_prep
{
    &writeInfo("Preparation steps for Linux build... here all the required files will be copie to server");
    
    $linux_build_config="$server_address"."build_prop";
    $linux_build_script="$server_address"."linux_build";

    # Linux build script & dependencies are available in componenet vob
    my $linux_build_script_src  ="$viewpath"."\\doc\\Integration\\linux_build_scripts\\linux_build";
    my $linux_build_dep_one     ="$viewpath"."\\doc\\Integration\\linux_build_scripts\\makeinc_adapt.pl";
    my $linux_build_dep_two     ="$viewpath"."\\doc\\Integration\\linux_build_scripts\\setup.sh";
    
    # Destination path where the build dependencies file will be copied"
    my $linux_view_path         ="$server_address"."views\\"."$ENV{CLEARCASE_VIEWNAME}";
    my $ai_projects_path        ="$server_address"."views\\"."$ENV{CLEARCASE_VIEWNAME}"."\\ai_projects\\";

    # remove the config and build script if already present in server under your home directory
    unlink($linux_build_config);
    unlink($linux_build_script);
	  
    print "copy 1 is $linux_build_script_src,$server_address \n"; copy($linux_build_script_src,$server_address) or die "Copy failed: $!"; 
    print "copy 2 is $linux_build_dep_one,$ai_projects_path \n"; copy($linux_build_dep_one,$ai_projects_path) or die "Copy failed: $!";
    print "copy 3 is $linux_build_dep_two,$ai_projects_path \n"; copy($linux_build_dep_two,$ai_projects_path) or die "Copy failed: $!";
        
    
    # Just a shortcut to make the build_prop usable on linux
    $server_address="/home/"."$UserID"."/";
    
    # Below varibales are required for starting the linux build using the script "linux_build"
    $linux_view_path         ="$server_address"."views/"."$ENV{CLEARCASE_VIEWNAME}";
    $ai_projects_path        ="$server_address"."views/"."$ENV{CLEARCASE_VIEWNAME}"."/ai_projects/";
	my $linux_build_gen      ="/home/$UserID/samba/views/"."$ENV{CLEARCASE_VIEWNAME}"."_GEN";
    my $build_script_path    ="$linux_view_path"."/ai_projects/tools/prj_build/modules/build.pl";
    
    # write configuration file for starting the linux build and this will be referred by the script "linux_build"
    open(CSWRITE,">$linux_build_config") or die ("Can't open input file");
    print CSWRITE "VIEWPATH=$linux_view_path \n";
    print CSWRITE "VIEWGENPATH=$linux_build_gen \n";
    print CSWRITE "BUILDSCRIPT=$build_script_path \n";
    print CSWRITE "USERID=$UserID \n";
    print CSWRITE "IPK=$ipk_decision\n";
    close(CSWRITE);
}


#------------------------------------------------------------------------------
#  Get current time and return it in a specified format
#------------------------------------------------------------------------------
sub CurrTime
{
  my $FormatTime ="";
  my ($sec,$min,$hour,$day,$mon,$year,$weekday,$yearday,$isdst) = localtime (time);
  my $param1 = shift @_ || '';
  my $param2 = shift @_ || '';
  if ($param2 eq "format") {
    ($sec,$min,$hour,$day,$mon,$year,$weekday,$yearday,$isdst) = localtime ($param1);
  }

  my @Weekdays = ("Sun","Mon","Tue","Wed","Thu","Fri","Sat");

  $mon++; $yearday++; $year+=1900;

  my $yearshort = substr($year,2,2);
  $mon          = $mon < 10 ? $mon = "0".$mon : $mon;
  $day          = $day < 10 ? $day = "0".$day : $day;
  $hour         = $hour < 10 ? $hour = "0".$hour : $hour;
  $min          = $min < 10 ? $min = "0".$min : $min;
  $sec          = $sec < 10 ? $sec = "0".$sec : $sec;
  $weekday      = $Weekdays[$weekday];

  if ($param1 eq "filename" || $param2 eq "format") {
    $FormatTime = $yearshort.$mon.$day."_".$hour.$min;
  }
  else {
    $FormatTime = $weekday.", ".$day.".".$mon.".".$year.", ".$hour.":".$min.":".$sec;
	$ShortTime = $day."".$mon."".$year."".$hour."".$min."".$sec;
  }
  return $FormatTime;
}

#------------------------------------------------------------------------------
#  For running system command and check the result
#------------------------------------------------------------------------------
sub SystemCommand
{
    my $systemCommand = $_[0];

    my $returnCode = system( $systemCommand );
    if ( $returnCode != 0 ) 
    { 
        die "Failed executing [$systemCommand]\n"; 
    }
}

#------------------------------------------------------------------------------
#  Write info to screen and send it to a logfile
#------------------------------------------------------------------------------
sub writeInfo
{

local *writeToLog = sub
  {
  my $StepInfo = shift @_;
  if (open(FH, ">> $jenkins_log"))
  {
    print FH $StepInfo;
    close FH;
  }
};

  my $infoText = shift @_;
  my $time = &CurrTime;
  my $frame = "-----------------------------------------------------------------------------------------------------------------\n";
  my $OutputText = $frame."  INFO ".$Step." (".$time."):\n  ".$infoText."\n".$frame."\n";
  print "\n$OutputText";
  &writeToLog( $OutputText );
  $Step++;
}


#------------------------------------------------------------------------------
#  Write info to screen and send it to a logfile
#------------------------------------------------------------------------------
sub writeerror
{
  my $StepInfo = shift @_;
  if (open(FH, ">> $jenkins_log"))
  {
    print FH $StepInfo;
    close FH;
  }
}

#------------------------------------------------------------------------------
#  Write info to screen and send it to a logfile
#------------------------------------------------------------------------------
sub writecontent
{
  my $StepInfo = shift @_;
  if (open(FH, ">> $jenkins_log"))
  {
    print FH $StepInfo;
    close FH;
  }
}

my $et = time();
my $checkTime = $et -$st;
$lt = localtime($et);
&writeInfo("Finished and Total duration taken is: $checkTime seconds");

