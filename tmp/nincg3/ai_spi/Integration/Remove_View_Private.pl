#!/usr/bin/perl
######################################################################
# This Script is used to remove all the View Private files from the Entire View Snapshot or Dynamic
#  There is no need to pass any arguments Just run the scripts from ai_projects environment 
#  Author:Sunil.Dasappnavar(ECA3)
#  
######################################################################
# 
######################################################################
#die ("Please specify the plat  form G3G or gen2 \n Example :Get_Config_Spec.pl G3G \n or Get_Config_Spec.pl gen2 \n") if (@ARGV != 1) ;

use Cwd;
$param = $ARGV[0];
print "Parm = $param\n";
chomp $param;
$param =~ s/\\/\\/g;

#$param =~ /(G3G|gen2)/i ? print "valid parameter supplied :$1 \n"  : die "invalid parameter please pass the G3G or gen2 as parameter: \n";
 
# if ($param eq G3G ) {
 # $SWNAVIROOT = $ENV{_SWSAMBAROOT};
# } else {
# $SWNAVIROOT = $ENV{_SWNAVIROOT};
# }
$path = $ARGV[0];
$newpath = $path;
$newpath =~ s/\\ai_projects/\\/g;
$newpath =~ s/\\/\\/g;
 
my $DIR = "";
if (opendir( $DIR, $newpath))
  {
	print "\Able to open $newpath\n";
  }
else
    {	
	 die "cant open $newpath \n";
      }
$satarttime=`echo %time%`;
print  "Started to  Remove View private  files  $satarttime\n";


while ( my $entry = readdir $DIR ) {
	
      next unless -d $newpath . '/' . $entry;
      next if $entry eq '.' or $entry eq '..';
      my @output = "$entry";
	  #@output =(ai_connectivity);
	  chomp @output;
		

foreach my $line (@output) {
   
  			   print "clearing View private files from VOB $line\n";
  			    $ep =$newpath."".$line;
				print "$ep\n";
				
	 		   chdir("$ep");
			   #$a = "cleartool ls -r -view_only | sed -e 's:\\\\:/:g' | xargs -d '\\n' echo\n 2>&1";
			   $a = "cleartool ls -r -view_only | sed -e 's:\\\\:/:g' | xargs -d '\\n' rm -rf 2>&1";
			   $b= print  `$a`
    }
}
$endttime=`echo %time%`;
print  "Removed all the View private files from the View at $endttime\n";
closedir $DIR;