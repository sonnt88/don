#!/usr/bin/perl
die ("Please pass the vob name and label Name as  command argument \n") if (@ARGV != 4) ;
my $vob = $ARGV[0];
my $labelname = $ARGV[1];
my $Project = $ARGV[2];
use English;
use FindBin ;
use lib "\\\\".$ENV{"CCSERVER"}."\\di_cc\\cc\\di_admin\\tools\\perl\\bin\\lib\\";
use lib "$FindBin::Bin/lib" ;
use ClearCase::ClearPrompt qw (clearprompt clearprompt_dir $CT +die +ERROR);
use Cwd ;
use Sys::Hostname;
#use strict;
use BPConfig qw ($Slash $Ct $Contact $SlashMatch $null $BPCSDir $UserName 
                 $TmpDir $OSNAME $Share $BPCSRootDir $BPMetricsSVFile $BPTypesDir 
                 $BPFastmodeVobs $BPTypesList $BPLocalLabelTypesList $BPTypesWinDir);
use BPutils qw (label_exist abort is_albd_running info_message SelectVob);
use vars qw ($prompt $answer $cmd $status); # reused
use Getopt::Long;


#$Vob = qx /$CT describe -fmt \"%n::%On\" vob:$Vob 2>NUL/;
$Vob = qx /$CT describe -fmt \"%n::%On\" vob:\\$vob 2>NUL/;
if ($?) {
  abort("Specified VOB does not exist");
}
chomp $Vob;
($Vob, $VobOID) = split(/\:\:/, $Vob);
print "Vob selected: $Vob\n" if $debug;

$VobOID =~ s/[\.\:]//g;
print "VOB OID: $VobOID\n" if $debug;
#######################################################################
# Set the config spec / and update view
#######################################################################
# $view = qx/$CT lsview $viewTag/;
# chomp $view;
#$view =~ s/(.*) //;
# my $CS = $view . $Slash . "config_spec";
# my $CS = "D:\\config_spec";
`mkdir c:\\ccstg\\$Project`;
my $CS = "c:\\ccstg\\$Project\\config_spec";
# my $CCS = $view . $Slash . ".compiled_spec";
my $CCS = "D:\\compiled_spec";

#
# Search config of the base label for other subsystems
#
# my $basecs = qx /$Ct desc -fmt %[ConfigSpec]SNa lbtype:$Label\@$Vob/;
my $basecs = qx /$Ct desc -fmt %[ConfigSpec]SNa lbtype:$labelname\@\\$vob/;
my $basis = "";
my $baselabelcs = "$labelname"; #  Kumar Added this
my $loadsection = "";   # optional load section
#undef $/;           # read in whole file, not just one line or paragraph

# read only section (0...n lines)
if ($basecs =~ /#BEGIN\s+RO\s+SECTION\s+\-\s+KEEP\s+THIS\s+LINE\s*\n*(.*?)#END\s+RO\s+SECTION\s+\-\s+KEEP\s+THIS\s+LINE\s*\n*/sm) {
  $basis = $1;
  $basis =~ s/\$BPCSRootDir/$BPCSRootDir/g;
  print "BASIS: $basis\n" if $debug;
}
#adding the Base label(s) to the configspec (0...n lines)
if ($basecs =~ /#BEGIN\s+BASE\s+LABEL\s+\-\s+KEEP\s+THIS\s+LINE\s*\n*(.*?)#END\s+BASE\s+LABEL\s+\-\s+KEEP\s+THIS\s+LINE\s*\n*/sm) {
  $baselabelcs = $1;
  unless ( $baselabelcs =~ /\s+-nocheckout\s+$/s ) {
    $baselabelcs =~ s/\s+$/ -nocheckout\n/s;   # add no checkout rule
  } else {
    $baselabelcs =~ s/\s+$/\n/s;
  }
  print "BASE LABEL: $baselabelcs\n" if $debug;
}
# special load section for cross-vob compilation (0...n lines)
if ($basecs =~ /#BEGIN\s+LOAD\s+SECTION\s+\-\s+KEEP\s+THIS\s+LINE\s*\n*(.*?)#END\s+LOAD\s+SECTION\s+\-\s+KEEP\s+THIS\s+LINE\s*\n*/sm) {
  $loadsection = $1;
  print "LOAD SECTION: $loadsection\n" if $debug;
}

$baselabelcs ="#BEGIN BASE LABEL - KEEP THIS LINE\n" . $baselabelcs . "#END BASE LABEL - KEEP THIS LINE\n";
if ( $basis !~ /^\s*$/ ) {
  $basecs = "#BEGIN RO SECTION - KEEP THIS LINE\n" . $basis . "#END RO SECTION - KEEP THIS LINE\n";
} else {
  $basecs = "";
}

chmod 0770 , ($view, $CS, $CCS);
open (CS , ">$CS") or print "Couldn't open $CS: $!";

# if ($Vtype =~ "snapshot" ) {
  print CS "element * CHECKEDOUT\n";
  print CS "element [$VobOID=$Vob]/... $labelname  -nocheckout\n";
  print CS $baselabelcs;
  print CS $basecs;
  print CS "#BEGIN LOAD SECTION - KEEP THIS LINE\n" . $loadsection . "#END LOAD SECTION - KEEP THIS LINE\n";
  print CS "load $Vob\n";
close (CS);
sleep 2;
chmod 0770 , ($CS, $CCS);

################################################
$file = "c:\\ccstg\\$Project\\config_spec";
my $SPIlabel = $ARGV[3];
     open my $in, '<', $file or die "Can't read old file: $!";
    open my $out, '>', "$file.new" or die "Can't write new file: $!";
		# print $out "element * CHECKEDOUT\n";
	   # print $out "element [a33d8dbc57ea43f9abda592d737be77a=/ai_spi]/...    $SPIlabel                -nocheckout\n";
       while( <$in> ) { # print the lines before the change
    print $out $_;
    last if $. == 1; # line number before change
    }
    my $line = <$in>;
     print $out "element * CHECKEDOUT\n";
	  print $out "element [a33d8dbc57ea43f9abda592d737be77a=/ai_spi]/...                       		   $SPIlabel                -nocheckout\n";
	   #$line =~ s/\b(perl)\b/Perl/g;
	   
	   
print $out $line;
       while( <$in> ) { # print the rest of the lines
    print $out $_;
    }
	
	   close $out;


