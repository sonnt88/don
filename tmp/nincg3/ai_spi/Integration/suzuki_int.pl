use File::Copy;
use File::Basename;
use Cwd qw();
use Getopt::Long;
use Cwd 'abs_path';

my $build_type;
my $adr_label;
my $v850_label;
my $samba_drive;
my $pre_int;
my $pass;
my $ser;

if (@ARGV) {
   GetOptions (
     
        'build_type=s' => \$build_type,
		'samba=s' => \$samba_drive,
		'pre_int=s' => \$pre_int,
		'passwd=s' => \$pass,
		'server=s' => \$ser

    );
}

unlink "$samba_drive/samba/views/engineering_download_creation_success.txt";
unlink "$samba_drive/samba/views/usb_stick_creation_success.txt";

###################################### Variable Declaration #################################
my $ERRORCODE;
my $CurrentWorkDir	= Cwd::cwd();
chomp $CurrentWorkDir;
my $samba_share;
my $MIDWLABEL_OLD_OLD;
my $MIDWLABEL;
my $INT_MAIN_VERSION_OLD_OLD;
my $cancat ='_';
my $cancat1 = "$pre_int";  
my $cancat2 ='\\';
my $binary_dir;
#my $CnBinaryPath = '//bosch.com/dfsrb/DfsCN/LOC/Szh/Department/RBAC-CM/11_Project/47_Suzuki SLN CN/08_SW_release/exchange_RBEI';
my @server_path_pre = ("//bosch.com/dfsrb/DfsIN/loc/Kor/NE2/ECM/ECG//Common//Binary_Data//SUZUKI/Versions", "//10.169.90.238/Suzuki_Software/01_Binary/PreIntegration" );
my @server_path_main = ("//bosch.com//dfsrb//DfsIN//loc/Kor/NE2/ECM/ECG/Common/Binary_Data/SUZUKI/Versions","//10.169.90.238/Suzuki_Software/01_Binary/PreIntegration","//bosch.com/dfsrb/DfsDE/DIV/CM/DI/Projects/BinaryExchange/BinaryExchange/Suzuki/Versions","//bosch.com/dfsrb/DfsCN/LOC/Szh/Department/RBAC-CM/11_Project/47_Suzuki SLN CN/08_SW_release/exchange_RBEI");
 

chomp $build_type;
chomp $adr_label;
chomp $v850_label;
print "Build Type  : $build_type\n";
print "PRE_INT     : $pre_int\n";
print "samba drive : $samba_drive\n";
print "\n";




print "\n";
my $abs_path = "$0";
my @view_name = split(/\\/,"$abs_path");
my $cclabel = "R:\\ake8kor_g3g_ai_spi_int_1\\ai_spi\\cc-label.txt";
my $ccmain = "$view_name[0]/$view_name[1]/$view_name[2]/cc_main.txt";
my $ccmai1 = "$view_name[0]/$view_name[1]/$view_name[2]/cc_imail.emtf";
`cleartool co -nc "R:\\ake8kor_g3g_ai_spi_int_1\\ai_spi\\cc-label.txt"`;
#`cleartool co -nc "$view_name[0]/$view_name[1]/$view_name[2]/cc_main.txt"`;
cc_label_update ();
#cc_main_update ();

my $userid = $ENV{'USERNAME'};
chomp $userid;
$userid = lc($userid);
my $vn=$userid."_".$MIDWLABEL;
my $SDir = "$samba_drive\\views\\$vn.vws";


#`cleartool ci -nc "$view_name[0]/$view_name[1]/$view_name[2]/cc-label.txt"`;
`cleartool ci -nc "R:\\ake8kor_g3g_ai_spi_int_1\\ai_spi\\cc-label.txt"`;
#`cleartool ci -nc "$view_name[0]/$view_name[1]/$view_name[2]/cc_main.txt"`;
print "$view_name[0]/$view_name[1]/$view_name[2]\n";
#chdir "$view_name[0]/$view_name[1]/$view_name[2]";
system ("perl //kor-ccadm-CM-AI.apac.bosch.com/di_cc/cc/di_admin/tools/perl/bin/Label.pl --Directory R:\\ake8kor_g3g_ai_spi_int_1\\ai_spi\\ --label $MIDWLABEL  -c Daily main build");

exit;

my $binary_main = "$INT_MAIN_VERSION_OLD_OLD$cancat$MIDWLABEL_OLD_OLD";
$binary_dir = "$INT_MAIN_VERSION_OLD_OLD$cancat$MIDWLABEL_OLD_OLD$cancat1";



if (("$build_type" eq 'pre' ) || ("$build_type" eq 'PRE' )) {
                      print "\n";
                      print "BINARIES WILL BE COPIED TO FOLLOWING LOCATIONS:\n";
					  print "\t";
                      print join("\n \t", @server_path_pre), "\n";
					  print "\n";
					  print "Binary directory name : $binary_dir\n";
					  print "\n";
                      print "HIT ENTER IF LINKS AND DIRECTORY VERSIONS ARE PROPER OR ELSE TERMINATE AND RESTART THE SCRIPT!!!!!!!!!!!!!\n";
                      <>;
                      }

if (("$build_type" eq 'main' ) || ("$build_type" eq 'MAIN' )) {
                      print "\n";
                      print "BINARIES WILL BE COPIED TO FOLLOWING LOCATIONS:\n";
					  print "\t";
                      print join("\n \t", @server_path_main), "\n";
					  print "\n";
					  print "Binary directory name : $binary_main\n";
					  print "\n";
                      print "HIT ENTER IF LINKS AND DIRECTORY VERSIONS ARE PROPER OR ELSE TERMINATE AND RESTART THE SCRIPT!!!!!!!!!!!!!\n";
                      <>;
					  system ("//kor-ccadm-CM-AI.apac.bosch.com/di_cc/cc/di_admin/tools/perl/bin/LabelView_suzuki.pl -label $MIDWLABEL -vType snapshot -SDir $SDir");
					  
                      }


#mail_generatin ();
#ssh ();



sleeep_usb:
if (-e "$samba_drive\\samba\\views\\usb_stick_creation_success.txt") {
secure_copy_usb ();
}

else {
sleep (15);
goto sleeep_usb;
      }

sleep_eng:
if (-e "$samba_drive\\samba\\views\\engineering_download_creation_success.txt") {
secure_copy_eng ();
}

else {
sleep (15);
goto sleep_eng;
      }
	  
	  
sub cc_label_update {
print "\n";
print "*****************************************************************************************************\n";
print "Increase Label version\n";
print "*****************************************************************************************************\n";
chomp $cclabel;
#print "$cclabel\n";
if ( open (FH, "<$cclabel") ) {
        binmode FH;
		my @cc_label_content = <FH>;
		close FH;
        
        #copy ($cclabel,$cclabel_bk);    # take backup of cc-label.txt
        
		$MIDWLABEL     = shift (@cc_label_content);
		$MIDWLABEL     =~ /(\S+)\s+(\S+)(F|S)(\S+)\s+(\S+)/i;
		$MIDWLABEL     = sprintf("%s%s%02d",$2,$3,$4+1);
		$MIDWLABEL     = $ARGV[0];
		
		$MIDWLABEL_OLD = sprintf("%s%s%02d",$2,$3,$4);
	}
else {
	$ERRORCODE = "Last ai_project Label label could not be evaluated!";
	goto ENDOFFILE;
	}
print "\n";
print "Current Label version : $MIDWLABEL_OLD\n";
print "New Label version     : $MIDWLABEL\n";
$MIDWLABEL_OLD_OLD = $MIDWLABEL;
print "\n";
#print "Press 'Y' to change the Label version to the one mentioned above or press 'N' to change to a new value \n";
#print "\n";
#$proced_next = <>;
$proced_next = 'y';
print "\n";
chomp($proced_next);
if (($proced_next eq 'y')||(($proced_next eq 'Y'))){
       	open (FH, ">$cclabel") || die "Could not write '$MIDWLABEL' into $cclabel!";
		print FH "PRODUCT_VERSION_LABEL  $MIDWLABEL $MIDWLABEL_OLD\n";
		print FH "REGISTRY_VERSION_LABEL $MIDWLABEL $MIDWLABEL_OLD\n";
		close FH;
        #chomp  $MIDWLABEL; 
	}
elsif (($proced_next eq 'n')||(($proced_next eq 'N'))){
    START:
    print "Enter the New Label name and hit Enter to continue";
    print "\n";
    $proced_next = <>;
  chomp $proced_next;
    $MIDWLABEL=$proced_next;
	$MIDWLABEL_OLD_OLD = $MIDWLABEL;
    print "\n";
    print "Current MIDW version : $MIDWLABEL_OLD\n";
    print "Entered version      : $MIDWLABEL \n";
    print "\n";
    print "Press 'Y' to continue or 'N' to change value \n";
    #print "\n";
    
    $proced_next = <>;
chomp($proced_next);
if (($proced_next eq 'y')||(($proced_next eq 'Y'))){
	    open (FH, ">$cclabel") || die "Could not write '$MIDWLABEL' into $cclabel!";
		print FH "PRODUCT_VERSION_LABEL  $MIDWLABEL $MIDWLABEL_OLD\n";
		print FH "REGISTRY_VERSION_LABEL $MIDWLABEL $MIDWLABEL_OLD\n";
		close FH;
        #chomp  $MIDWLABEL;
      
	}
    
 elsif (($proced_next eq 'n')||(($proced_next eq 'N'))){
goto START;  
}
}
}

sub cc_main_update {

print "*****************************************************************************************************\n";
print "Main version update\n";
print "*****************************************************************************************************\n";

if ( open (FH, "<$ccmain") ) {
  binmode FH;
  my @cc_main_content = <FH>;
  close FH;
  
  copy ($ccmain,$ccmain_bk);       #take backup of cc_main.txt
  
  $INT_MAIN_VERSION_OLD = shift (@cc_main_content);
  $OLD_VERSION = $INT_MAIN_VERSION_OLD;
  $INT_MAIN_VERSION_OLD =~ s/\s//gi;
  $INT_MAIN_VERSION_OLD =~ /(\d+)_(\w+).*/;
  $INT_VERS_NR          = $1+1;
  $INT_VERS_DATE        = &CurrTime("versdate");
  $INT_MAIN_VERSION     = sprintf("%04d_%s",$INT_VERS_NR,$INT_VERS_DATE);
  #print "$OLD_VERSION";
  #print "$INT_MAIN_VERSION";
  
print "\n";
print "Current Main version : $OLD_VERSION\n";
print "New Main version     : $INT_MAIN_VERSION\n";
$INT_MAIN_VERSION_OLD_OLD = $INT_MAIN_VERSION;
print "\n";
#print "Press 'Y' to change the MAIN version to the one mentioned above or press 'N' to change to a new value \n";
#print "\n";
#$proced_next = <>;
$proced_next = 'y';
print "\n";
chomp($proced_next);
if (($proced_next eq 'y')||(($proced_next eq 'Y'))){
        open (FH, ">$ccmain") || die "Could not write '$INT_MAIN_VERSION' into $ccmain!";
		print FH "$INT_MAIN_VERSION\n";
		close FH;
        #chomp $INT_MAIN_VERSION;
      
	}
elsif (($proced_next eq 'n')||(($proced_next eq 'N'))){
    START:
    print "Enter the New MAIN version and hit Enter to continue";
    print "\n";
    $proced_next = <>;
  chomp $proced_next;
    $INT_MAIN_VERSION=$proced_next;
	$INT_MAIN_VERSION_OLD_OLD = $INT_MAIN_VERSION;
    print "\n";
    print "Current Main version : $OLD_VERSION\n";
    print "New Main version     : $INT_MAIN_VERSION\n";
    print "\n";
    print "Press 'Y' to continue or 'N' to change value ";
    #print "\n";
    
    $proced_next = <>;
chomp($proced_next);
if (($proced_next eq 'y')||(($proced_next eq 'Y'))){
        open (FH, ">$ccmain") || die "Could not write '$INT_MAIN_VERSION' into $ccmain!";
		print FH "$INT_MAIN_VERSION\n";
		close FH;
        #chomp $INT_MAIN_VERSION;
       
	}
    
 elsif (($proced_next eq 'n')||(($proced_next eq 'N'))){
goto START;  
}
}
}
  
  sub CurrTime             # Get current time and return it in a specified format
{
  my $FormatTime ="";
  my ($sec,$min,$hour,$day,$mon,$year,$weekday,$yearday,$isdst) = localtime (time);
  my $param1 = shift @_;
  my $param2 = shift @_;
  if ($param2 eq "format") {
    ($sec,$min,$hour,$day,$mon,$year,$weekday,$yearday,$isdst) = localtime ($param1);
  }

  my @Weekdays = ("Sun","Mon","Tue","Wed","Thu","Fri","Sat");

  $mon++; $yearday++; $year+=1900;

  my $yearshort = substr($year,2,2);
  $mon          = $mon < 10 ? $mon = "0".$mon : $mon;
  $day          = $day < 10 ? $day = "0".$day : $day;
  $hour         = $hour < 10 ? $hour = "0".$hour : $hour;
  $min          = $min < 10 ? $min = "0".$min : $min;
  $sec          = $sec < 10 ? $sec = "0".$sec : $sec;
  $weekday      = $Weekdays[$weekday];

  if ($param1 eq "filename" || $param2 eq "format") {
    $FormatTime = $yearshort.$mon.$day."_".$hour.$min;
  }
  elsif ($param1 eq "versdate") {
    $FormatTime = $yearshort.$mon.$day;
  }
  else {
    $FormatTime = $weekday.", ".$day.".".$mon.".".$year.", ".$hour.":".$min.":".$sec;
  }

  return $FormatTime;
}

}

####################################  mail creation ###################################

sub mail_generatin {
chomp $binary_dir;

#################################### To, CC & subject ###############################
unlink "$ccmai1";
#print "$abspath\n";
chomp $ccmai1;
open (MYFILE, ">>$ccmai1") or die;
print MYFILE "TO: Sunil Dasappanavar (RBEI/ECA2); FIXED-TERM Sureshkumar Sakthivel (RBEI/ECA2);\n";
print MYFILE "CC: RBEI/ECG-SUZUKI Integrators; \n";
print MYFILE "CC: XXXXXXXXXXXXXXXXXXXXXXXXXX \n"; 
print MYFILE "SU: [SUZUKI SLN] [AI_PROJECTS] [NEW_SW] for $binary_dir\n";


######################## MAIL CONTENT  ###################################                           
print  MYFILE "BO: <PB><AS>Hello All,<PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AS> Please find the Binaries for <SB><AB>$binary_dir<SN> <AS>at below path for <AR><SB>testing.<SN><PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AS><SB>Integrated Labels:<SN><PE>\n";
print  MYFILE  "BO: <PB><LB><LM>https:\\\\hi-dms.de.bosch.com\\docushare\\dsweb\\Get\\Document-586997\\SUZUKI_SLN_IntegrationPlanning.xls<LE><PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AS><SB> KOR:<SN><PE>\n";
print  MYFILE  "BO: <PB><AS>Binaries:<LB><LM>\\\\bosch.com\\DfsRB\\DfsIN\\LOC\\Kor\\NE2\\ECM\\ECG\\Common\\Binary_Data\\SUZUKI\\Versions\\$binary_dir<LE><PE>\n";
print  MYFILE  "BO: <PB><AS>Map Data:<LB><LM>\\\\bosch.com\\DfsRB\\DfsIN\\LOC\\Kor\\NE2\\ECM\\ECG\\Common\\Binary_Data\\SUZUKI\\Versions\\MAP_DATA<LE><PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AS><SB> HI:<SN><PE>\n";
print  MYFILE  "BO: <PB><AS>Binaries:<LB><LM>\\\\bosch.com\\dfsrb\\DfsDE\\DIV\\CM\\DI\\Projects\\BinaryExchange\\BinaryExchange\\Suzuki\\Versions\\$binary_dir<LE><PE>\n";
print  MYFILE  "BO: <PB><AS>Map Data:<LB><LM>\\\\bosch.com\\dfsrb\\DfsDE\\DIV\\CM\\DI\\Projects\\BinaryExchange\\BinaryExchange\\Suzuki\\Versions\\MAP_DATA<LE><PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AS><SB> COB:<SN><PE>\n";
print  MYFILE  "BO: <PB><AS>Binaries:<LB><LM>\\\\10.169.90.238\\Suzuki_Software\\01_Binary\\$binary_dir<LE><PE>\n";
print  MYFILE  "BO: <PB><AS>Map Data:<LB><LM>\\\\10.169.90.238\\Suzuki_Software\\01_Binary\\MAP_DATA<LE><PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AS> <SB>CN:<SN><PE>\n";
print  MYFILE  "BO: <PB><AS>Binaries:<LB><LM>\\\\bosch.com\\dfsrb\\DfsCN\\LOC\\Szh\\Department\\RBAC-CM\\11_Project\\47_Suzuki SLN CN\\08_SW_release\\exchange_RBEI\\$binary_dir<LE><PE>\n";
print  MYFILE  "BO: <PB><AS>Map Data:<LB><LM>\\\\bosch.com\\dfsrb\\DfsCN\\LOC\\Szh\\Department\\RBAC-CM\\11_Project\\47_Suzuki SLN CN\\08_SW_release\\exchange_RBEI\\MAP_DATA<LE><PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AB><SB>BVT : In progress<SN><PE>\n";
print  MYFILE "BO: <PB><AB><SB>LSIM: In prorgress<SN><PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><SB><AR>Attention!!!!!!<SN><PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AR>Please install below packages<PE>\n";
print  MYFILE "BO: <PB><AR>sudo apt-get update<PE>\n";
print  MYFILE "BO: <PB><AR>sudo apt-get install realpath<PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AR>Please Rename the MasterScript.txt accordingly <PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AR>MasterScript_AB_Sample.txt     => A/B Sample GEX <PE>\n";
print  MYFILE "BO: <PB><AR>MasterScript_AB_Sample_CN.txt  => A/B Sample CN <PE>\n";
print  MYFILE "BO: <PB><AR>MasterScript_C_Sample.txt      => C Sample GEX <PE>\n";
print  MYFILE "BO: <PB><AR>MasterScript_C_Sample_CN.txt   => C Sample CN <PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AR>Please use MasterScript_C_Sample_DL.txt for Flashing C Sample Dual Lite Board <PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AR><SB>CN Flashing:<SN><PE>\n";
print  MYFILE "BO: <PB><AR><TB> <IB> Copy the Binary folder to local PC<PE>\n";
print  MYFILE "BO: <PB><AR><TB> <IB> Copy the Map data (sd2-CHN_JAC_CII_13Q2_v1_oS.tar.bz2)  to patches folder ( under the Binary folder )<PE>\n";
print  MYFILE "BO: <PB><AR><TB> <IB> Rename the MasterScript.txt (based on Target sample)<PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AR><TB>MasterScript_AB_Sample_CN.txt =>  Masterscript.txt<PE>\n";
print  MYFILE "BO: <PB><AR><TB>MasterScript_C_Sample_CN.txt  =>  Masterscript.txt<PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AR><TB> <IB> Start the Engineering Download<PE>\n";
print  MYFILE "BO: <PB><AR><TB> <IB> Please use the below KDS script for china variant<PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AR><SB>USB Stick:<SN><PE>\n";
print  MYFILE "BO: <PB><AR> a) Use [binary_path]\\download\\stick\\chn => China<PE>\n";
print  MYFILE "BO: <PB><AR> b) Use [binary_path]\\download\\stick\\gex => GEX<PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AS>HOW to Download  Via USB:<PE>\n";
print  MYFILE "BO: <PB><AG><SB>This procedure is only required when you want to move from a SW below the AI_PRJ_SUZUKI_LINUX_14.0F109 to a AI_PRJ_SUZUKI_LINUX_14.0F109 or above<PE><SN>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AS>Under the following link you can also find the document in Docushare:<LB><LM>\\\\hi-dms.de.bosch.com\\docushare\\dsweb\\Services\\Document-651337 <LE><PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AR><SB>Important:<SN><PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AS><SB>Recommended usage of latest NAVI SD Cards  :<PE><SN>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AS><SB>1.	Use the encrypted D-Sample SD Cards<PE><SN>\n";
print  MYFILE "BO: <PB><AS><SB>2.	For Engineering purposes : Alternatively, NAVI-DATA can be copied from the following paths :<PE><SN>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AS>Mexico D1   : <LB><LM> \\\\hi-navidata.hi.de.bosch.com\\cd-data\\DATA-STORE\\Suzuki\\SLN\\GEX\\sd2-Suzuki-SLN_D_MEX_2014\\v1_oS\\data_compressed <LE><PE>\n";
print  MYFILE "BO: <PB><AS>India  D2   : <LB><LM>\\\\hi-navidata.hi.de.bosch.com\\cd-data\\DATA-STORE\\Suzuki\\SLN\\GEX\\sd2-Suzuki-SLN_D2_IND_2013Q4\\v3\\data_compressed <LE><PE>\n";
print  MYFILE "BO: <PB><AS>Thailand    : <LB><LM>\\\\hi-navidata.hi.de.bosch.com\\cd-data\\DATA-STORE\\Suzuki\\LN\\GEX\\sd2-Suzuki-SLN_D_THAI_2014\\v2_oS\\data_compressed <LE><PE>\n";
print  MYFILE "BO: <PB><AS>Australia/NZ: <LB><LM>\\\\hi-navidata.hi.de.bosch.com\\cd-data\\DATA-STORE\\Suzuki\\SLN\\GEX\\sd2-Suzuki-SLN_D_AUS-NZL_2014\\v1_oS\\data_compressed <LE><PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AR><SB>EUR<SN><PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AS>Eur: <LB><LM>\\\\hi-navidata.hi.de.bosch.com\\cd-data\\DATA-STORE\\Suzuki\\SLN\\EUR\\sd2-Suzuki-SLN_C_EUR_2013\\v9\\data_compressed <LE><PE>\n";
print  MYFILE "BO: <PB><AS>Eur: <LB><LM>\\\\hi-navidata.hi.de.bosch.com\\cd-data\\DATA-STORE\\Suzuki\\SLN\\EUR\\sd0-Suzuki-SLN_B_TUR_2013\\v1_os\\data_compressed <LE><PE>\n";
print  MYFILE "BO: <PB><AS>Eur: <LB><LM>\\\\hi-navidata.hi.de.bosch.com\\cd-data\\DATA-STORE\\Suzuki\\SLN\\EUR\\sd2-Suzuki-SLN_C_RUS_2013\\v7\\data_compressed <LE><PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AR><SB>Usage Steps:<SN><PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AS>1) Please copy the �navidata.tar.bz2� from MAP_DATA paths to the patches folder. <PE>\n";
print  MYFILE "BO: <PB><AS>	KOR Server Map Data:<LB><LM>\\\\bosch.com\\DfsRB\\DfsIN\\LOC\\Kor\\NE2\\ECM\\ECG\\Common\\Binary_Data\\SUZUKI\\Versions\\MAP_DATA<LE><PE>\n";
print  MYFILE "BO: <PB><AS>	COB Server Map Data:<LB><LM>\\\\10.169.90.238\\Suzuki_Software\\01_Binary\\MAP_DATA <LE><PE>\n";
print  MYFILE "BO: <PB><AS>	HI Server  Map Data:<LB><LM>\\\\bosch.com\\dfsrb\\DfsDE\\DIV\\CM\\DI\\Projects\\BinaryExchange\\BinaryExchange\\Suzuki\\Versions\\MAP_DATA <LE><PE>\n";
print  MYFILE "BO: <PB><AS>	CN Server Map Data:<LB><LM>\\\\bosch.com\\dfsrb\\DfsCN\\LOC\\Szh\\Department\\RBAC-CM\\11_Project\\47_Suzuki SLN CN\\08_SW_release\\exchange_RBEI\\Map_Data <LE><PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AS>2) Add the below line in your MasterScript.txt ( above the run scripts/adapt_rfs.sh) , flash to use the Engineering SD Card install patches/navidata.tar.bz2   /dev/mmcblk1p6/ <PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AR>� Please read following Software Download document carefully for �Software Update� and device �KDS configuration� update.<PE>\n";
print  MYFILE "BO: <PB><AS><LB><LM>https:\\\\hi-dms.de.bosch.com\\docushare\\dsweb\\Get\\Document-611824\\SWDownload_Suzuki_Customer_v5.doc<LE><PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AR><SB>Use the below Build Command to run X86:<PE><SN>\n";
print  MYFILE "BO: <PB><AR>build.pl �lsim<PE>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <IL>\n";
print  MYFILE "BO: <PB><AS>Best Regards<PE>\n";
print  MYFILE "BO: <PB><AS>Friendly Integrator<PE>\n";

system ( "perl R:/$view_name[1]/ai_projects/tools/prj_build/bin_copy_mail.pl --template $view_name[1] --samba $samba_drive" ) ;

close (MYFILE);
 
}

##################auto login#####################
sub ssh {
unlink 'D:/remote.cmd';
open (MYFILE1, '>>D:/remote.cmd') or die "unable to open remote.cmd!!!";     
print  MYFILE1 "cd /home/$userid/Views/$vn.vws/ai_projects\n";
print  MYFILE1 "source setup.sh\n";
print  MYFILE1 "int_prj.sh\n";
close (MYFILE1);
#my $ser = 'uus1kor@kor1018437';
chomp $pass;
system ( qq `"start c:\\programme\\Putty\\putty.exe -ssh -2 -l $userid -pw $pass -m d:remote.cmd $ser"`);
}

############################### secure copy usb stick #########################################
sub secure_copy_usb {

if (("$build_type" eq 'pre' ) || ("$build_type" eq 'PRE' )) {
                     foreach (@server_path_pre) {
                           			
								 system (qq `start robocopy "$samba_drive\\samba\\views\\$vn.vws_GEN\\ai_projects\\generated\\ai_sw_update\\stick" "$_\\$binary_dir\\download\\stick" /E /V`);
                           			 
                                  }
                      }

if (("$build_type" eq 'main' ) || ("$build_type" eq 'MAIN' )) {

                       foreach (@server_path_main) {
					  
                     system (qq `start robocopy "$samba_drive\\samba\\views\\$vn.vws_GEN\\ai_projects\\generated\\ai_sw_update\\stick" "$_\\$binary_main\\download\\stick" /E /V`);
                     }
                      }

}

########################################### copy engineering download ##################################
sub secure_copy_eng {

if (("$build_type" eq 'pre' ) || ("$build_type" eq 'PRE' )) {
                     foreach (@server_path_pre) {

								 system (qq `start robocopy "$samba_drive\\samba\\views\\$vn.vws_GEN\\ai_projects\\generated\\SUZUKI_RELEASE\\$binary_main" "$_\\$binary_dir" /E /V`);

                                  }
                      }

if (("$build_type" eq 'main' ) || ("$build_type" eq 'MAIN' )) {

                       foreach (@server_path_main) {
					  
                     system (qq `start robocopy "$samba_drive\\samba\\views\\$vn.vws_GEN\\ai_projects\\generated\\SUZUKI_RELEASE\\$binary_main" "$_\\$binary_main" /E /V`);
                     }
                      }

}

ENDOFFILE:
exit( 0 );
