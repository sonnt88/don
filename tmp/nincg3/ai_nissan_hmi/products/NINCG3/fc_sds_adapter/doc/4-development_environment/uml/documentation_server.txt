@startuml

hide footbox

database docServer as "Documentation Server\nsi0vmc0298.de.bosch.com"
database buildServer as "Build-Server\nhi-z0af4.hi.de.bosch.com"

[-> docServer : run cron job, e.g. hourly
activate docServer
docServer -> docServer : update_html_docs.sh
activate docServer
docServer -> buildServer : get_ai_nissan_hmi.sh
docServer -> buildServer : build_sds_adapter_docs.sh 
docServer <- buildServer : copy documentation archive 
docServer -> buildServer : cccc_sds_adapter.sh 
docServer <- buildServer : copy results archive 

@enduml
