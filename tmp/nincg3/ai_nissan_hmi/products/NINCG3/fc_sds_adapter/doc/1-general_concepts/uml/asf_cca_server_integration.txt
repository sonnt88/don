@startuml

hide circle
hide attributes

package ASF {
   class BaseComponent #lightgrey
   class CcaServerHookStub #lightgrey {
      {abstract} onCcaMessage(Message&) : void
   }
}

package ail {
   class ail_tclAppInterfaceRestricted #E08064 {
      {abstract} bDispatchCCAMessages(amt_tclBaseMessage*) : tBool
   }
   class ail_tclOneThreadAppInterface #E08064

   ail_tclOneThreadAppInterface -up-|> ail_tclAppInterfaceRestricted
}

package ahl {
   class ahl_tclBaseOneThreadApp #ffC060
   class ahl_tclBaseOneThreadObject #ffC060 {
      {abstract} vMyDispatchMessage(amt_tclServiceData*) : tVoid
   }
   class ahl_tclBaseOneThreadService #ffC060

   ahl_tclBaseOneThreadService -up-|> ahl_tclBaseOneThreadObject
}

package AhlServerFramework {
   class AhlApp #FFFF90 {
      vAddService(AhlService*) : tVoid
      onCcaMessage(Message&) : void
   }
   class AhlService #FFFF90 {
      vAddFunction(Function*) : tVoid
      vMyDispatchMessage(amt_tclServiceData*) : tVoid
   }
   class Function #FFFF90 {
      {abstract} vHandleMessage(amt_tclServiceData*) : tVoid
      u16GetFunctionId() : tU16
   }
   class ServerMethod #FFFF90 {
      vHandleMessage(amt_tclServiceData*) : tVoid
      {abstract} vMethodStart(amt_tclServiceData*) : tVoid
   }
   class ServerProperty #FFFF90 {
      vHandleMessage(amt_tclServiceData*) : tVoid
      {abstract} vGet(amt_tclServiceData*) : tVoid
      {abstract} vSet(amt_tclServiceData*) : tVoid
      {abstract} vUpreg(amt_tclServiceData*) : tVoid
      {abstract} vRelUpreg(amt_tclServiceData*) : tVoid
   }

   AhlApp -right-> "*" AhlService   
   AhlService -right-> "*" Function: dispatch to
   ServerMethod -up-|> Function
   ServerProperty -up-|> Function
}

package SdsAdapter {
   class SdsAdapter #lightgreen
   class Sds2HmiServer #lightgreen
   class clSDS_Method_CommonStopSession #lightgreen {
      vMethodStart(amt_tclServiceData*) : tVoid
   }
   class clSDS_Property_PhoneStatus #lightgreen {
      vGet(amt_tclServiceData*) : tVoid
      vSet(amt_tclServiceData*) : tVoid
      vUpreg(amt_tclServiceData*) : tVoid
      vRelUpreg(amt_tclServiceData*) : tVoid
   }
   
   SdsAdapter -right-> Sds2HmiServer
   Sds2HmiServer -up-> AhlApp
   Sds2HmiServer -up-> AhlService
   Sds2HmiServer .right.> clSDS_Method_CommonStopSession : creates
   Sds2HmiServer .right.> clSDS_Property_PhoneStatus : creates
}

SdsAdapter -up|> BaseComponent
AhlService -up-|> ahl_tclBaseOneThreadService
AhlApp -up-|> CcaServerHookStub
AhlApp -up-|> ahl_tclBaseOneThreadApp
ahl_tclBaseOneThreadApp -up-|> ail_tclOneThreadAppInterface
clSDS_Method_CommonStopSession -up-|> ServerMethod
clSDS_Property_PhoneStatus -up-|> ServerProperty
 
@enduml