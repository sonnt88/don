= General Concepts =

[partintro]
--
This part describes the high-level concepts such as component design. It also
covers general design concepts such as the integration of CCA server into an ASF
component or the strategy of dependency management and package design.
--

== Component Overview ==

The _SdsAdapter_ connects the SDS core components to the rest of the system. It implements the
server side of the _sds2hmi_sds_fi_ message interface. The client side of this interface is
represented by two SDS core components. Firstly, the _SDS_VC_ component which implements the
_Voice Dialog Statemachine_. Secondly, the _SDS_TS_ component which implements the 
_Transcription Service_.

One major responsibility of the _SdsAdapter_ is to retrieve required status information 
from various middleware components and provide it to the SDS core components. This can comprise
phone, tuner and media player states. On the other hand can receive functional requests
which it needs to delegate to the appropriate middleware component. This could be a request
to play a specific audio track or the initiation of a phone call. There is a wealth of different
services defined in the _sds2hmi_sds_fi_. Therefore, _SdsAdapter_ has to connect to a large
number of functional middleware services. All such service communication is going through
asynchronous message interfaces.

The second major responsibility of _SdsAdapter_ is concerned with the display of the _SDS
Help Popups_. Every voice dialog state is associated with a specific help popup that either
shows the available commands, a selection list, a query or confirmation. The trigger to show
a particular popup is coming from _SDS_VC_. However, _SDS_VC_ does not know anything about
the concrete content of a popup, about the current text language or any formatting. It is
up to _SdsAdapter_ to determine the popup content and string formatting.

The exact graphical representation is the responsibility of the _SDS HMI Application_. This
is a separate component that implements the GUI screens related to the speech ialog system.
Thus, the _SdsAdapter_ provides the popup contents to the _SDS HMI Application_ through another
message interface.

The below component diagram provides an overview of the connected services and clients.
It is generated from the _SdsAdapter.cmc_ component manifest. Click on the thumbnail
to enlarge the picture.

image:1-general_concepts/uml/sds_adapter_interfaces.png["Component Overview",
width=800, link="1-general_concepts/uml/sds_adapter_interfaces.png"]


== ASF - Automotive Service Framework ==
The 'SdsAdapter' is based on 'ASF', the 'Automotive Service Framework'.
'ASF' supports simple integration of different 'Service Technologies' such as 'CCA' or 'DBUS'
in a single component. 'ASF' provides a single-threaded environment to the 
component developer. This protects the component developer from concurrency or 
data protection issues that would involve locks or semaphores.

The http://hi-asf.de.bosch.com:8082/public/asf/releases/latest/ASF%20User%20Guide.pdf['ASF User Guide'] 
has further details on 'ASF' and its motivation. Along with more information it is available from the
https://inside-docupedia.bosch.com/confluence/display/cmaiasf/Automotive+Service+Framework+%28ASF%29['ASF Home Page'].


== CCA Server and ASF ==

One of the core responsibilities of 'SdsAdapter' is to implement the server side of the 'sds2hmi_sds_fi'.
However, 'ASF' does not provide support for the generation of 'CCA' server stubs.
Fortunately, we found a way to integrate the legacy 'AHL' and 'AIL' frameworks into an ASF application.
This even allowed to port much of the 'sds2hmi' server code from the 'LCN2kai' project.

The below picture shows how the ASF framework and the legacy CCA framework classes are integrated
in the _SdsAdapter_ component. Click the image to enlarge.
 
image:1-general_concepts/uml/asf_cca_server_integration.png["CCA Server and ASF", 
width=800, link="1-general_concepts/uml/asf_cca_server_integration.png"]

.Server Message Flow

- Any CCA message to the _SdsAdapter_ is first received by the ASF framework
- CCA messages meant for a server are passed to the _AhlApp_ class through the
  _CcaServerHookStub::onCcaMessage()_ callback interface
- The _AhlApp_ delegates the message to _AhlApp::bDispatchCCAMessages()_, which
  is inherited from its _AIL_ base class _ail_tclAppInterfaceRestricted_
- The AIL/AHL framework dispatches the message to the addressed AHL service and
  the message finally reaches the _AhlService::vMyDispatchMessage()_ method.
- The _AhlService_, which holds a collection of function implementations, now
  dispatches the message to the appropriate function implementation.
- The _Function_ class also provides methods for sending the response


== Tracing ==

=== Minimizing Trace Code ===

Trace code often obfuscates the actual implementation. If the feature implementation
is interleaved with trace code it becomes more difficult to read and understand.
Thus, we *strive to keep the amount of trace code to a minimum* in our implementation.

=== Tracing Component Interfaces ===

Our CCA and ASF communication frameworks provide generic trace mechanisms. Using these
mechanisms *we can trace all messages on the component boundaries without adding a single
line of trace code*.

.ASF Logs
The ASF framework comes with its own logging concept. Each class can define its own logger.
For each logger you can define a separate log level in the log configuration of an 
application. However, our trace concept is still based on TTFis. To simplify the handling
of ASF logs all ASF logs of an application are made available through one TTFis trace class.

For _SdsAdapter_ you can use trace class _TR_CLASS_SDSADP_ASF_ with trace level _4_ to
obtain the ASF _Info_ traces. All incoming and outgoing interface messages are traced
with their payload:

----
[Info |sdsAdapterThread|2fcfbb40|    76|sdsAdapter      |   168|/MOST_Tel_FI/MOST_Tel_FIProxy |0x0a5a0640] <- onBTDeviceVoiceRecognitionExtendedStatus, act=173672000, cb=0xa59c604, proxy=0xa58ecf8
[Info |sdsAdapterThread|2fcfbb40|    76|sdsAdapter      |   168|/MOST_Tel_FI/MOST_Tel_FIProxy |0x3051e460] payload: {
	"bBTDeviceVoiceRecActive": false,
	"bBTDeviceVoiceRecSupported": false,
	"e8SiriAvailabilityState": "e8NOT_AVAILABLE"
}
----

Please see the
http://hi-asf.de.bosch.com:8082/public/asf/releases/latest/ASF%20User%20Guide.pdf[ASF User Guide]
for more details on the ASF logging concept.

.CCA Service Recorder

The _CCA Service Recorder_ provides generic tracing capabilities for legacy CCA components.
This is useful for the _sds2hmi_sds_fi_ server interface. To use the service recorder it
must be generally enabled with trace class _TR_CLASS_SRVREC_ on level 8. In addition, any
service that you want to trace must also be enabled with one of the below trace levels:

- Level 4 +
  Only message header is traced, no payload
- *Level 6* +
  payload is traced in a single TTFis message, but truncated to ~60 bytes +
  this can be evaluated with trc rules generated from the FI xml +
  message logs can be watched in live trace 
- Level 7 +
  payload is traced in multiple TTFis messages, but truncated to ? bytes +
  payload can only be analyzed offline
- Level 8 +
  full payload is traced in multiple TTFis messages
  payload can only be analyzed offline


=== Tracing Implementation Details ===

Besides the interface messages, one might want to add traces for some program logic.
This can of course be done using the well-known ETG traces.
Please use trace class TR_CLASS_SDSADP_DETAILS for this purpose.
The corresponding output is prefixed with _sdsdtl:_.

=== TRC Rules ===

With the below rules we can trace all boundaries of the _SdsAdapter_ component as
well as the detailed traces:

----
.TR_SET_CONFIG TR_CLASS_SDSADP_ASF 4

.TR_SET_CONFIG TR_CLASS_SRVREC 8
.TR_SET_CONFIG TR_CLASS_SRVREC_SAAL 6
.TR_SET_CONFIG TR_CLASS_SRVREC_STREAMROUTER 6
.TR_SET_CONFIG TR_CLASS_SRVREC_AUD_SRC_ROUTE 6

.TR_SET_CONFIG TR_CLASS_SDSADP_DETAILS 8
----

Filter the trace output for the following strings:

 - _sds2hmi_sds_fi_ for all messages between _SdsAdapter_ and the SDS Middleware
 - _sdsasf:_ for all ASF logs
 - _sdsdtl:_ for implementation details
 - _midw_aud_src_route_fi_ for audio routing
