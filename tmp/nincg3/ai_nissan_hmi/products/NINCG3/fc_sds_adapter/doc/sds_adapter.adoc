:compat-mode:
:doctype: book
:toc2:

= SDS Adapter Development Handbook =

[preface]
= Preface =
This handbook covers all aspects of the development of the _SDS Adapter_ component.
The book ranges from the high-level concepts to detailed design, from
specifications to supporting tools. The development handbook is meant to be the
central source of information for all technical concerns. In case a particular topic is not (yet) covered in this book you may also consult the
https://inside-docupedia.bosch.com/confluence/display/gen3generic/Speech+Domain[Speech Domain Wiki Page].

The handbook book is made up of multiple parts covering different aspects of
the component development. It can be viewed as HTML, PDF or even plain text.

This book is written in simple text files using http://asciidoc.org/[_AsciiDoc_].
The text files can be easily put under revision control along with the
source code of the _SDS Adapter_ component. Embedded UML diagrams are also written as
text with http://plantuml.com/[_PlantUML_]. The _AsciiDoc_ text is then converted into a convenient
output format such as PDF or HTML.

Every developer on the team is encouraged to contribute to the documentation
in his or her specialized field. This document provides the skeleton which 
needs to be filled with flesh by the entire team.
 

include::1-general_concepts/part_general_concepts.adoc[]

include::2-feature_design/part_feature_design.adoc[]

include::3-specification_framework/part_specification_framework.adoc[]

include::4-development_environment/part_development_environment.adoc[]

include::5-testing/part_testing.adoc[]

include::6-troubleshooting/part_troubleshooting.adoc[]


////
[bibliography]
= Bibliography =
--
The books and articles listed here are in some way relevant to the concepts used in the _SdsAdapter_ component.
--

[bibliography]

- [[[large_scale]]] John Lakos. _Large-Scale C++ Software Design_.
  Addison-Wesley. 1996. ISBN 0-201-63362-0.
- [[[refactoring]]] Martin Fowler.
  _Refactoring - Improving the Design of Existing Code_. Addison-Wesley. 2000.
  ISBN 0-201-48567-2.
////


[index]
= Index =
////////////////////////////////////////////////////////////////
The index is normally left completely empty, it's contents are
generated automatically by the DocBook toolchain.
////////////////////////////////////////////////////////////////

