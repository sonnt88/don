= SdsAdapter =

== Documentation ==

- https://si0vmc0298.de.bosch.com/sds_adapter/handbook/sds_adapter.html[Single HTML Page]
- https://si0vmc0298.de.bosch.com/sds_adapter/handbook/sds_adapter.chunked/index.html[Page-wise HTML]
- https://si0vmc0298.de.bosch.com/sds_adapter/handbook/sds_adapter.pdf[PDF]
- https://si0vmc0298.de.bosch.com/sds_adapter/handbook/sds_adapter.adoc[Plain asciidoc for use with Firefox AsciiDoctor Preview Plugin]

== Code Analysis ==

- https://si0vmc0298.de.bosch.com/sds_adapter/code_analysis/latest_build_result[Latest Build Result]
- https://si0vmc0298.de.bosch.com/sds_adapter/code_analysis/cppdep_sds_adapter[Dependency Analysis with _cppdep_]
- https://si0vmc0298.de.bosch.com/sds_adapter/code_analysis/cccc_sds_adapter[Code Metrics derived with _cccc_]

== Tools ==

- https://si0vmc0298.de.bosch.com/sds_adapter/tools[Tools]

