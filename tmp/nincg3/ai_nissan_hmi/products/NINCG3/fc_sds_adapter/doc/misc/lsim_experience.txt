LSIM Experience

2015-09-21
- trying to trace TR_CLASS_DEV_ACOUSTICIN_LINUX and TR_CLASS_DEV_ACOUSTICOUT_LINUX
  SDS is no longer going to LISTENING state after enabling those trace classes
  
2015-09-21
- overnight build not possible due to build errors in apphmi_system and apphmi_navigation

2015-09-28
- LSIM image 15.0V38 doesn't work after prepare_lsim
- maybe my nissan_ncg3_int version is not compatible with 38 - it's still based on 37

2015-10-15
- continuous LSIM debugging since 2 weeks to get HMI usable again
  got broken with 15.0V38, no we have 15.0V41
- various issues found and solved:
  no SDS audio output
  procbaseearly synchronisation with fake device
  GUI framework crash in DirectTexture
  apphmi_tuner crash due to multiple UpRegs
  sds_adapter crash due to ASF-CCA port renamed in one place only
