#!/bin/bash
dir=$(dirname $0)
pushd $dir &> /dev/null
if [ "$1" == "--uml" ]; then
    umlFiles=$(find $dir | xargs grep -l -E "^@startuml" | sort)
    echo "converting PlantUML files ..."
    for file in $umlFiles; do
        echo $file
    done
    java -jar $dir/../tools/plantuml/plantuml.jar $umlFiles
else
    echo "skipping conversion of PlantUML files - use option --uml to convert PlantUML files"
fi
a2x --asciidoc-opts=--conf-file=lang-en.conf --asciidoc-opts=--theme=flask --format=chunked  sds_adapter.adoc
a2x --asciidoc-opts=--conf-file=lang-en.conf --asciidoc-opts=--theme=flask --format=pdf      sds_adapter.adoc
asciidoc --theme=flask sds_adapter.adoc
asciidoc --theme=flask index.adoc
pushd &> /dev/null

