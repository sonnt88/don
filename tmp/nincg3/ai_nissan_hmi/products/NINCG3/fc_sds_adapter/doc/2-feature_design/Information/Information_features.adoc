== Information features ==

This chapter covers the concept and design flow for SDS Information features.
A speech session can be started on the top level and based on the user requested information commands are supported via speech.
Information features are supported only in NOR configurations.The user uttered a command "Information", then the system activate the Information context. 
This context contains information services from XM,Navigation,TCU and VehicleInformation.
This command shall not be available in Mexico configuration.In Canada configuration this context will be replaced by the command "Traffic".

=== Traffic ===

In AIVI System provides functionality to the User, get to know the Traffic events depending on the user request via speech.
Then system will be transition to respective application.

.Used Methods
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.InfoShowMenu
 SDS2HMI_FI.CommonStopSession
 
.Sequence
image:2-feature_design/Information/uml/Traffic.png[]

=== Fuel Prices ===
User can get to know the  cheap or near gas stations fuel prices,depending on the user request via speech.
Fuel Prices are provided by SXM. It's functionality will be available in USA and Canada regions. Fuel Pricing will be based on the fuel type like Petrol or Diesel.
It is given through the Vehicle Configuration.The user can uttered a fuel prices in a "Sort by Price" or "Sort by Distance".

=== Sort By Price ===

User uttered a fuel prices as a "Sort by Price". Then the system will be displayed list of gas stations in near by location as a sorted by price.
Sort By Price was bifurcated with route guidance status.

.Step by Step utterance
User utters sort by price then POI gas stations are listed from near by locations with details like price,last price updated,distance and direction.
User selected POI Fuel Item will be go to 'SR_INF_ByPrice_Route' or SR_INF_ByPrice_NoRoute' Screen depending on the RG Status.
RG Status activate then Screen will be Shown as a "SR_INF_ByPrice_Route".RG Status Inactivate then Screen will be Shown as a "SR_INF_ByPrice_NoRoute".


=== Sort By Distance ===

User uttered a fuel prices as a "Sort by Distance". Then the system will be displayed list of gas stations in near by location as a sorted by distance tot he current car location.
Sort By Distance was bifurcated with route guidance status.

.Step by Step utterance
User utters sort by distance then POI gas stations are listed from near by locations with details like price,last price updated,distance and direction.
User selected POI Fuel Item will be go to 'SR_INF_ByDistance_Route' or SR_INF_ByDistance_NoRoute' Screen depending on the RG Status.
RG Status activate then Screen will be Shown as a "SR_INF_ByDistance_Route".RG Status Inactivate then Screen will be Shown as a "SR_INF_ByDistance_NoRoute".


SXm Fuel(Sxm MIDW) has two services, One for US and another for Canada.
To Call respective services based on current current country type.
The current country type information provided from Navi as a Position information(latitude,longitude and Current country).


.Used Methods for USA Service
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.CommonGetListInfo
 SDS2HMI_FI.CommonSelectListElement
 SDS2HMI_FI.NaviStartGuidance
 SDS2HMI_FI.InfoShowMenu
 SXMFUEL.GetFuelInfoListStart
 SXMFUEL.GetFuelStationInfoStart
 NavigationService.onPositionInformationUpdate
 NavigationService.StartGuidanceToPosWGS84Request
  
.Sequence
image:2-feature_design/Information/uml/FuelPricesUSA.png[]


.Used Methods for Canada Service
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.CommonGetListInfo
 SDS2HMI_FI.CommonSelectListElement
 SDS2HMI_FI.NaviStartGuidance
 SDS2HMI_FI.InfoShowMenu
 SXMFUEL.GetCanadianFuelInfoListStart
 SXMFUEL.GetCanadianFuelStationInfoStart
 NavigationService.onPositionInformationUpdate
 NavigationService.StartGuidanceToPosWGS84Request

.Sequence
image:2-feature_design/Information/uml/FuelPricesCanada.png[]


=== Sports ===

User select the sport type he/she is interested in.The list of sport types is defined in the SDS application.
Upon the user requested respective sport type,then system will be transition to respective application.

.Used Methods
 SDS2HMI_FI.CommonStatus
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.InfoShowMenu
 SDS2HMI_FI.CommonStopSession

.Sequence
image:2-feature_design/Information/uml/Sports.png[]


=== Stocks ===

In AIVI System provides functionality to the User, get to know the Stocks events depending on the user request via speech.
Then system will be transition to respective application.

.Used Methods
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.InfoShowMenu
 SDS2HMI_FI.CommonStopSession

.Sequence
image:2-feature_design/Information/uml/Stocks.png[]

=== Movies ===
User select the Movie Listing items, he/she is interested in.Upon the user requested respective movie list,then system will be transition to respective application.

.Used Methods
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.InfoShowMenu
 SDS2HMI_FI.CommonStopSession

.Sequence
image:2-feature_design/Information/uml/MovieListings.png[]


=== Current Weather ===

In AIVI System provides functionality to the User, get to know the Current weather report(wind,Humidity,weather station),depending on the user request via speech.
Then system will be transition to respective application.


.Used Methods
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.InfoShowMenu
 SDS2HMI_FI.CommonStopSession

.Sequence
image:2-feature_design/Information/uml/CurrentWeather.png[]



=== Weather Map ===
In AIVI System provides functionality to the User,get to Know the weather report map of a location ,depending on the user request via speech.


.Used Methods
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.InfoShowMenu
 SDS2HMI_FI.CommonStopSession

.Sequence
image:2-feature_design/Information/uml/WeatherMap.png[]

=== 5Days Fore cast ===
In AIVI System provides functionality to the User, get to know the 5 daysForecast of a weather report(wind,Humidity,weather station),depending on the user request via speech.Then system will be transition to respective application.

.Used Methods
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.InfoShowMenu
 SDS2HMI_FI.CommonStopSession

.Sequence
image:2-feature_design/Information/uml/CurrentWeather.png[]

=== 6hours Forecast ===
In AIVI System provides functionality to the User, get to know the 6hours of a weather report(wind,Humidity,weather station),depending on the user request via speech.
Then system will be transition to respective application.

.Used Methods
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.InfoShowMenu
 SDS2HMI_FI.CommonStopSession

.Sequence
image:2-feature_design/Information/uml/6hoursForeCast.png[]



=== Connect Voice Menu ===

In AIVI System provides functionality to the User,to connect Security of a system Control unit,depending on the user request via speech.
Then system will be transition to respective application.

.Used Methods
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.InfoShowMenu
 SDS2HMI_FI.CommonStopSession

.Sequence
image:2-feature_design/Information/uml/ConnectVoiceMenu.png[]

SportsSports
=== Energy Flow ===

In AIVI System provides functionality to the User,get to know of energy flow items(car performances,speed)of a car,depending on the user request via speech.
Then system will be transition to respective application.And also user has the option to select via haptically or speech inline.  


.Used Methods
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.InfoShowMenu
 SDS2HMI_FI.CommonStopSession

.Sequence
image:2-feature_design/Information/uml/EnergyFlow.png[]

