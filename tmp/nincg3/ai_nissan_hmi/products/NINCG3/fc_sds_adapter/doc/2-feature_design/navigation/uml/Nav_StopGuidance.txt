
@startuml

title: <size:20> Stop guidance  </size>

actor Speaker
participant "SDS_MW" as A
participant "SDSAdapter" as B
participant "org.bosch.cm.NavigationService" as C

Speaker -> A: Stop Guidance
activate A

A -> B: NaviStopGuidance.MS()

||20||

B -> C: sendCancelRouteGuidanceRequest()

||20||

B --> A: NaviStopGuidance.MR()

...

C -> B: onCancelRouteGuidanceResponse()


@enduml