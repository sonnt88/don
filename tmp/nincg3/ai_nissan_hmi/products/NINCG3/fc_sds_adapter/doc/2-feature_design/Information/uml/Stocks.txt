@startuml
hide footbox
actor user

participant SDSMW
box "SDS Adapter" #white
participant Sds2HmiServer
participant clSDS_Method_CommonShowDialog
participant clSDS_Method_InfoShowMenu
participant clSDS_Method_CommonStopSession
participant clSDS_MenuManager
participant clSDS_Display
participant GuiService

end box
box "AppHmi_Sds" #white
participant SdsAdapterHandler
participant SdsHall

end box
participant AppHmi_Master
participant AppHmi_Sxm

Sds2HmiServer ->Sds2HmiServer:createSds2HmiService
activate Sds2HmiServer
Sds2HmiServer->GuiService:GuiService()
Sds2HmiServer->clSDS_MenuManager:clSDS_MenuManager()
Sds2HmiServer->clSDS_Method_CommonShowDialog: clSDS_Method_CommonGetListInfo()
Sds2HmiServer ->clSDS_Method_InfoShowMenu :clSDS_Method_InfoShowMenu()
Sds2HmiServer -> clSDS_Method_CommonStopSession:clSDS_Method_CommonStopSession()
deactivate Sds2HmiServer

user --> SDSMW :user says "Stocks"

SDSMW -->clSDS_Method_CommonShowDialog:vMethodStart(ScreenID =SR_GLO_CONFRIM ,TID =Stocks)
activate clSDS_Method_CommonShowDialog
clSDS_Method_CommonShowDialog ->clSDS_MenuManager:vShowSDSView()
clSDS_MenuManager -> clSDS_Display:vShowSDSView(ScreenData)
clSDS_Display ->GuiService:sendPopupRequestSignal()
clSDS_Method_CommonShowDialog -->SDSMW :vSendMethodResult()
deactivate clSDS_Method_CommonShowDialog
GuiService -->SdsAdapterHandler:onPopupRequestSignal()
activate SdsAdapterHandler
SdsAdapterHandler ->SdsAdapterHandler:processHeaderData(signal)
SdsAdapterHandler ->SdsAdapterHandler:  processListData(signal)
SdsAdapterHandler ->SdsAdapterHandler:processLayout(Layout =C)
activate SdsAdapterHandler
note over SdsAdapterHandler
Stocks template  with  Confirm screen displayed.
end note
SdsAdapterHandler ->SdsHall: showScreenLayout(CurrentLayout)
deactivate SdsAdapterHandler
deactivate SdsAdapterHandler

SDSMW -->clSDS_Method_InfoShowMenu:vMethodStart(menuType =FI_EN_INFO_STOCKS_FAVOURITES)
activate clSDS_Method_InfoShowMenu
clSDS_Method_InfoShowMenu ->clSDS_Method_InfoShowMenu:bShowInfoScreen()
activate clSDS_Method_InfoShowMenu
clSDS_Method_InfoShowMenu ->GuiService:sendEventSignal(Event__SPEECH_DIALOG_SHOW_INFO_STOCK)
deactivate clSDS_Method_InfoShowMenu

alt bShowInfoScreen Fail(noMenuType)
clSDS_Method_InfoShowMenu --> SDSMW:vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
else bShowInfoScreen (MenuType successful )
clSDS_Method_InfoShowMenu --> SDSMW :vSendMethodResult()
end
deactivate clSDS_Method_InfoShowMenu

GuiService -->SdsAdapterHandler:onEventSignal()
activate SdsAdapterHandler
SdsAdapterHandler->SdsAdapterHandler:handleSXMEvents()
SdsAdapterHandler->SdsHall:contextSwitch(APPID_APPHMI_SXM, SXM_CONTEXT_STOCKS_MAIN)
deactivate SdsAdapterHandler
SdsHall -->AppHmi_Master:requestContextSwitch()
AppHmi_Master-->AppHmi_Sxm:ContextSwitch()

SDSMW -->clSDS_Method_CommonStopSession:vMethodStart()
activate clSDS_Method_CommonStopSession
clSDS_Method_CommonStopSession ->clSDS_MenuManager:vCloseGui()
clSDS_MenuManager ->clSDS_Display:vCloseGui()
clSDS_Display ->GuiService:sendPopupRequestCloseSignal()
clSDS_Method_CommonStopSession -->SDSMW:vSendMethodResult()
deactivate clSDS_Method_CommonStopSession
GuiService ->SdsAdapterHandler:onEventSignal()
SdsAdapterHandler ->SdsAdapterHandler:handleGeneralEvents(EventType =Event__SPEECH_DIALOG_SDS_STOP_SESSION)
alt _backTransitflag =true
SdsAdapterHandler ->SdsHall:contextSwitchBack()
SdsHall -->AppHmi_Master:requestBackContextSwitch()
else  _backTransitflag = false
SdsAdapterHandler ->SdsHall:contextSwitchComplete()
SdsHall -->AppHmi_Master:requestCompleteContextSwitch()
end 

@enduml

