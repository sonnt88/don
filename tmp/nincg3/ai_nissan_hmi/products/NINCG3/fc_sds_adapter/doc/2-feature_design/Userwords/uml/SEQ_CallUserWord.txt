@startuml
hide footbox

actor user
participant SDSMW
participant Sds2HmiServer
participant clSDS_Method_PhoneDialNumber
participant clSDS_Method_CommonShowDialog
participant clSDS_MenuManager
participant clSDS_Display
participant GuiService

box "SDS_HALL" #LightBlue
participant SdsAdapterHandler
participant SdsHall
participant AppSds_ContextRequestor
end box

participant MOST_PhonBk_FIProxy
participant MOST_Tel_FIProxy



Sds2HmiServer ->Sds2HmiServer:createproxies()
activate Sds2HmiServer
Sds2HmiServer-->MOST_PhonBk_FIProxy::createProxy("phoneBookPort", *this)
Sds2HmiServer-->MOST_Tel_FIProxy::createProxy("phoneTelPort", *this)
deactivate Sds2HmiServer

Sds2HmiServer ->Sds2HmiServer:createSds2HmiService
activate Sds2HmiServer
Sds2HmiServer->clSDS_Method_PhoneDialNumber :_dialNumber = new clSDS_Method_PhoneDialNumber(service, m_poUserwords, _telephoneProxy, _phoneBookProxy, _quickDialList);
deactivate Sds2HmiServer


note over user : User utters "Call <UserWord>"

SDSMW-->clSDS_Method_CommonShowDialog :vMethodStart()
clSDS_Method_CommonShowDialog->clSDS_MenuManager :vShowSDSView(oScreenData)
clSDS_Method_CommonShowDialog->clSDS_Method_CommonShowDialog:vSendResult(oScreenData)
clSDS_Method_CommonShowDialog-->SDSMW:vSendMethodResult(oResult);
clSDS_MenuManager->clSDS_Display:vShowSDSView(oScreenData)
clSDS_Display->clSDS_Display:vShowView(_popupRequestSignal, oScreenData.enReadScreenId())
clSDS_Display-->SdsAdapterHandler : _guiService.sendPopupRequestSignal(Layout(),Header(), SpeechInputState(),TextFields())
activate SdsAdapterHandler
SdsAdapterHandler->SdsAdapterHandler:onPopupRequestSignal(signal)
SdsAdapterHandler->SdsAdapterHandler:getLayout(signal)
SdsAdapterHandler->SdsAdapterHandler:processHeaderData(signal)
SdsAdapterHandler->SdsAdapterHandler:processListData(signal)
SdsAdapterHandler->SdsAdapterHandler:processLayout(signal)
deactivate SdsAdapterHandler
SdsAdapterHandler->SdsHall :showScreenLayout(currentLayout)
note over SdsHall : SdsHAll posts courier message to display Confirmation  Screen
SDSMW --> clSDS_Method_PhoneGetNumberInfo : vMethodStart(oMessage)
clSDS_Method_PhoneGetNumberInfo--> clSDS_Method_PhoneGetNumberInfo :vGetCallerNameAndNumberForUserword(UswID)
clSDS_Method_PhoneGetNumberInfo--> MOST_PhonBk_FIProxy :sendGetContactDetailsExtendedStart(UswID)
MOST_PhonBk_FIProxy-->clSDS_Method_PhoneGetNumberInfo : onGetContactDetailsExtendedResult(result)
clSDS_Method_PhoneGetNumberInfo-->SDSMW :vSendMethodResult(oResult)
 
SDSMW -->clSDS_Method_PhoneDialNumber : vMethodStart
clSDS_Method_PhoneDialNumber --> clSDS_Method_PhoneDialNumber : vInvokeDialNumber(oMessage)
clSDS_Method_PhoneDialNumber --> clSDS_Method_PhoneDialNumber : vDialNumberByUWID(oMessage.nIDValue)
clSDS_Method_PhoneDialNumber --> MOST_PhonBk_FIProxy : sendGetContactDetailsExtendedStart(UserwordId)
MOST_PhonBk_FIProxy --> clSDS_Method_PhoneDialNumber : onGetContactDetailsExtendedResult
clSDS_Method_PhoneDialNumber--> MOST_Tel_FIProxy :sendDialStart(phoneNumber)
clSDS_Method_PhoneDialNumber--> SDSMW : vSendMethodResult
MOST_Tel_FIProxy--> clSDS_Method_PhoneDialNumber : onDialResult

 
 SDSMW-->clSDS_Method_CommonStopSession:  vMethodStart()
 clSDS_Method_CommonStopSession-->clSDS_Method_CommonStopSession : vReleaseResources()
 clSDS_Method_CommonStopSession-->clSDS_Method_CommonStopSession : vSendStopSessionToStateMachine()
 clSDS_Method_CommonStopSession-->SDSMW :vSendMethodResult()
 clSDS_Method_CommonStopSession-->clSDS_MenuManager : closeGui()
 clSDS_MenuManager-->clSDS_Display :vCloseView()
 clSDS_Display-->SdsAdapterHandler : sendPopUpRequestCloseSignal()
 SdsAdapterHandler-->SdsAdapterHandler : onPopUpRequestCloseSignal(signal)
 SdsAdapterHandler-->SdsHAll : RequestPopUpClose()
 note over SdsHAll : SDS HALL posts courier message to close SDS Screen
 

 
@enduml