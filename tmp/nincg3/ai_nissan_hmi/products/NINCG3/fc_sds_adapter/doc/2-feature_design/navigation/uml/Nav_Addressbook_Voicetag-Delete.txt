
@startuml

title: <size:20>Voicetag -> Delete </size>

actor User
participant "org.bosch.cm.NavigationService" as A
participant "SDSAdapter" as B
participant "SDS_MW" as C

User -> A: Press "DELETE"

activate A

note right

Navi to display a confirmation pop up 
before sending Delete request

end note

||20||

A -> A: User selects Yes

||20||

A -> B: SDSVoiceTagIdStatus(destinationMemoryEntryId ,\nsdsVoiceTagId, DELETE_VOICETAG)

||20||


B -> B: Check Request type = DELETE_VOICETAG

||20||

B -> C: CommonActionRequest.Status\n(DELETE_USERWORD, ID)

||20||

C -> C: Delete USW

...

C -> B: CommonSetAvailableUserWords.MS\n(list of T_Userword)

||20||

B -> A: sendsdsUpdateVoiceTagRequest(SDSVoiceTagId voiceTagID)


note right
Structure **SDSVoiceTagIds**
           destinationMemoryEntryId  = destinationMemoryEntryId as sent by Navi
           sdsVoiceTagId       = Deleted VoicetagID
           SDSVoiceTagOptions  = VOICETAG_DELETED 
end note

||20||

B -> C: CommonSetAvailableUserWords.MR()

||20||

@enduml