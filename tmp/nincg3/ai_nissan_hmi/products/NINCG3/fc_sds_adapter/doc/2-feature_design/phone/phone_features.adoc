== Phone features ==

This chapter covers the concept and design flow for SDS phone features

=== Call Name ===

Phone features

image:2-feature_design/phone/images/SWAD_Call_Name.png[]

image:2-feature_design/phone/images/CLS_Phonebook.png[]


=== Call Relationship Type ===

==== Overview ====

Contacts in the so-called _vehicle phonebook_ aka. _quickdial list_ can be associated
with a relationship type such as _brother_, _sister_, _friend_, _wife_ and the like.
These types can be spoken along with the name to be called to improve the recognition
result, e.g. you could speak _call my friend Peter_. You may even omit the name and
only speak the relationship type, e.g. _call my wife_.

The vehicle phonebook is a separate phonebook that is stored in parallel to the
phonebook that is downloaded from the connected mobile. The _SdsAdapter_ must
announce both phonebooks to the SDS Transcription Service. The phonebooks are
announced with so-called device ids. The normal phonebook is associated with the
device id of the connected mobile. In contrast, the vehicle phonebook is
associated with a fake phone device. The device id of this fake device is
calculated from the device id of the connected device by adding 1000. Thus,
if the bluetooth service reports that a mobile with device id 2 is connected,
_SdsAdapter_ must report devices 2 and 1002 in its _PhoneStatus_ property.

image::2-feature_design/phone/images/phone_VPB_Relationship.png[]


==== Further Reading ====

- More details on dealing with the two phonebook types can be found in chapter 2.13.3 of
  https://hi-dms.de.bosch.com/docushare/dsweb/Services/Document-752890[this concept document]
  of the connectivity component team.
- The analysis of the relationship feature from the SDS middleware team can be found in  
  https://hi-dms.de.bosch.com/docushare/dsweb/Services/Document-758705[their concept document].
- In case you need clarification regarding connectivity components, the https://inside-docupedia.bosch.com/confluence/display/gen3generic/CpM+Connectivity[wiki page of the _Connectivity Component_]
  contains a wealth of links to further reading.
  
==== Phonebook database ====

https://hi-dms.de.bosch.com/docushare/dsweb/Services/Document-752890[Conectivity - Phonebook - New APIs needed for A-IVI] has important details on Vehicle Phonebooks in chapter _2.13.3 Car phonebook lists – Speech recognition aspects_.


.Relationship Types

The relationship types as encountered in phonebook.db are the following:

----
402 -> "my Parents "
403 -> "my Brother "
404 -> "my Sister "
405 -> "my Child "
406 -> "my Son "
407 -> "my Daughter "
408 -> "my Friend "
409 -> "my Partner "
410 -> "my Wife "
411 -> "my Husband "
----


==== Relationship Sequences ====

tbd.

=== Read Text ===

The A-IVI system provides the functionality for the  user can say e.g. "Read Text"

.Availability of the feature
This feature is enabled only when text messaging is enabled. 

.Details
The user can say e.g. "Read Text" from "SR_GLO_MAIN" screen or from "SR_PHO_MAIN" screen. On this screen transitions to SMS List selection context.
In the SMS list selection step the system shall display a list of available text messages with additional information like sender and receive time. 
A maximum of 20 SMS are made available from SDS.
From here, the user selects a message from the list the system shall transition to the SMS readout context - a maximum of 160 characters are displayed.
In the SMS readout context the currently selected SMS shall be read by TTS.
Additionally the user has the possibility to use the voice commands Next, Previous, Play, Reply and Call. 
When Next or Previous are selected the system shall remain in the current context but select the respectively changed text message. 
In case Call is selected the system shall initiate a phone call to the senders number and the voice recognition session shall end. 
In case Play is selected the readout of the selected text shall be repeated. 
In case Reply is selected then the system shall transition to the Select Text sublevel. 
NOTE : Reply shall be greyed out in case the paired phone does not support SMS sending. 

.Used Methods
 SDS2HMI_FI.CommonGetListInfo
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.CommonSelectListElement
 SDS2HMI_FI.TextMsgGetInfo
 SDS2HMI_FI.TextMsgGetContent
 SDS2HMI_FI.TextMsgCallbackSender
 SDS2HMI_FI.TextMsgSelectMessage
 MOST_MSG_FI.MessagingDeviceConnection
 MOST_MSG_FI.CreateMessageList
 MOST_MSG_FI.RequestSliceMessageList
 MOST_MSG_FI.GetMessage
 CLOCK_FI.LocalTimeDate_MinuteUpdate
 CLOCK_FI.TimeFormat
 CLOCK_FI.DateFormat

image::2-feature_design/phone/images/SEQ_ReadText.png[]
image::2-feature_design/phone/images/SEQ_ReadText_Call.png[]
image::2-feature_design/phone/images/SEQ_ReadText_NextText.png[]
image::2-feature_design/phone/images/SEQ_ReadText_Play.png[]
image::2-feature_design/phone/images/SEQ_ReadText_Reply.png[]

=== Custom messages ===
The A-IVI system provides the functionality for the  user can say "custom messages" in send text context

.Availability of the feature
This feature is enabled only when text messaging is enabled. 
If no individually designed SMS is available the user will be informed about this by an error prompt "There are no custom messages available" and the system shall return to the Text Selection Sublevel. 

.Details
Select SMS Text by Custom Messages
The user can utter a command e.g. "Custom Messages" and the system then shall transition to the next sublevel of text selection to let the user select one of 10 individually designed SMS via line selection.
By uttering a command e.g. "Line" followed by a number between 1 and 6 the user can select the respective item of the currently shown list.
After selecting a SMS the system shall transition to the Select Confirmation Sublevel.

.Steps
Custom messages can be saved from PhoneHMI screen.
GoTo SETUP->Phone->Text Message->Auto Reply (ON)->Edit Custom Text->Add New
<save custom messages>

ptt press->say "Read Text"-> "To" -> "Custom messages"

.Used Methods
 SDS2HMI_FI.CommonGetListInfo
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.CommonSelectListElement
 SDS2HMI_FI.TextMsgSend
 MOST_MSG_FI.PredefinedMessageList

image::2-feature_design/phone/images/SEQ_customMsg.png[]
 
=== Recent calls ===
The A-IVI system provides the functionality for the user can utter a command e.g. "Recent Calls" and the system then shall transition to the next sublevel to select one of the subsequent recent calls lists.

.Availability of the feature
This feature is enabled only when Phone is paired and call history permissions are enabled.

.Details
User can utter "recent calls" from "SR_GLO_MAIN" screen or from "SR_PHO_MAIN" screen.
The subsequent recent call list consist of incoming calls, outgoing calls and missed calls.
User can also utter "missed calls", "incoming calls", "outgoing calls" from "SR_GLO_MAIN" screen, from "SR_PHO_MAIN" screen or from "SR_PHO_RecentCalls" screen.
In the respective call list screen, user can utter a command e.g. "Line" followed by a number between 1 and 6 and the system then shall start a call to the respective number.
The speech recognition session shall end with this.

Additionally user can also say "next page", "previous page", "first page" and "last page" from call list screen.

.Steps
 case 1 : PttPress -> say "recent calls" -> say "Missed calls"
 case 2 : PttPress -> say "recent calls" -> say "Incoming calls"
 case 3 : PttPress -> say "recent calls" -> say "Outgoing calls"
 case 4 : PttPress -> say "Missed calls"
 case 5 : PttPress -> say "Incoming calls"
 case 6 : PttPress -> say "Outgoing calls"
 case 7 : PttPress -> say "Phone" -> say "recent calls" -> say "Missed calls"
 case 8 : PttPress -> say "Phone" -> say "recent calls" -> say "Incoming calls"
 case 9 : PttPress -> say "Phone" -> say "recent calls" -> say "Outgoing calls"
 
Additionally, call list are available in send text context when text messaging is enabled.

NOTE : When Phonebook and Call History permissions are disabled and user says "missed calls",
prompt saying "there are no missed calls" is played.

.Used Methods
 SDS2HMI_FI.CommonGetListInfo
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.CommonSelectListElement
 SDS2HMI_FI.PhoneDialNumber
 MOST_PHONBK_FI.DownloadStaus
 MOST_PHONBK_FI.DevicePhoneBookFeatureSupport
 MOST_PHONBK_FI.CreateCallHistoryList
 MOST_PHONBK_FI.RequestSliceCallHistoryList
 MOST_TEL_FI.Dial
 
image::2-feature_design/phone/images/SEQ_IncomingCallsList.png[]
image::2-feature_design/phone/images/SEQ_MissedCallList.png[]
image::2-feature_design/phone/images/SEQ_OutgoingCallList.png[]

=== Redial ===
The A-IVI system provides the functionality for the user can call the last dialled number if available by entering the command "Redial".

.Availability of the feature
This feature is enabled only when Phone is paired and call history permissions are enabled.

.Details
User can utter "redial" from "SR_GLO_MAIN" screen or from "SR_PHO_MAIN" screen where the last outgoing call number is dialled.
After the user has uttered the command the system will confirm this command and thereby show the number to be dialled at the screen. 
Afterwards the system will start dialling this number. 
The speech recognition session shall end with this.

.Steps
 case 1 : PttPress -> say "redial"
 case 2 : PttPress -> say "Phone" -> say "redial"
 

NOTE : When Phonebook and Call History permissions are disabled and user says "redial",
prompt saying "there are no number to redial" is played.

.Used Methods
 SDS2HMI_FI.CommonGetListInfo
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.PhoneRedialLastNumber
 MOST_PHONBK_FI.DownloadStaus
 MOST_PHONBK_FI.DevicePhoneBookFeatureSupport
 MOST_PHONBK_FI.CreateCallHistoryList
 MOST_PHONBK_FI.RequestSliceCallHistoryList
 MOST_TEL_FI.Dial
 
=== Select Phone ===
The A-IVI system provides the functionality for the user can call the last dialled number if available by entering the command "Redial".

.Availability of the feature
This feature is enabled only when Phone is paired and call history permissions are enabled.

.Details
User can utter "redial" from "SR_GLO_MAIN" screen or from "SR_PHO_MAIN" screen where the last outgoing call number is dialled.
After the user has uttered the command the system will confirm this command and thereby show the number to be dialled at the screen. 
Afterwards the system will start dialling this number. 
The speech recognition session shall end with this.

.Steps
 case 1 : PttPress -> say "redial"
 case 2 : PttPress -> say "Phone" -> say "redial"
 

NOTE : When Phonebook and Call History permissions are disabled and user says "redial",
prompt saying "there are no number to redial" is played.

.Used Methods
 SDS2HMI_FI.CommonGetListInfo
 SDS2HMI_FI.CommonShowDialog
 SDS2HMI_FI.PhoneRedialLastNumber
 MOST_PHONBK_FI.DownloadStaus
 MOST_PHONBK_FI.DevicePhoneBookFeatureSupport
 MOST_PHONBK_FI.CreateCallHistoryList
 MOST_PHONBK_FI.RequestSliceCallHistoryList
 MOST_TEL_FI.Dial