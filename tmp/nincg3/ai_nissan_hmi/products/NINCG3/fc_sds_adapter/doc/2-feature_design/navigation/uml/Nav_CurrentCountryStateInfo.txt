
@startuml

title: <size:20> CVP updates  </size>

||30||

participant "SDS_MW" as A
participant "SDSAdapter" as B
participant "org.bosch.cm.NavigationService" as C


C -> B: positionInformation(PositionInformation)

||20||


B -> B: Check change in values

||20||

B -> A: NaviCurrentCountryState.Status(Country, State, \nCountryName, StateName, CityName)


@enduml