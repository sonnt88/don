@startuml

title: <size:20> One shot destination entry  </size>

actor Speaker
participant "SDS_MW" as A
participant "SDSAdapter" as B
participant "org.bosch.cm.NavigationService" as C

Speaker -> A: Utters address
activate A

A -> B: NaviSetDestinationItem.MS(1,OSDE_ADDRESSES, DEFAULT, FALSE, 0xFF)
activate B

B -> C: sendSdsCheckAddressRequest(SDSAddress requestedAddress)
activate C

...

alt Result response from Navi

C --> B: onSdsCheckAddressResponse(SDSAddress verifiedAddress)

else Selectioncriterion type = reserved (No ambiguity)

B --> B : housenumberAvailable == TRUE && housenumberValid == TRUE

B --> A: NaviSetDestinationItem.MR(1, 0, Reserved, nAnswerOptions = b0101)

note right
**AnswerOptions**
           InvalidAddress = FALSE
           HouseNumberInRange = TRUE
           Ambiguous        = FALSE
           GuidancePossible = TRUE

end note

||60||
B -> C: sendsetLocationWithSdsInputRequest()

...

C --> B: onsetLocationWithSdsInputResponse()

...

Speaker -> A: Calculate route


A -> B: NaviStartGuidance.MS()


B -> C: sendstartGuidanceRequest()

||10||

B --> A: NaviStartGuidance.MR()

...

C --> B: onstartGuidanceResponse()


||10||
else Selectioncriterion type = junction (or) reserved

B --> B : housenumberAvailable == FALSE && housenumbervalid == FALSE \n, CROSS_ROAD_AVAILABLE = TRUE 

B --> A: NaviSetDestinationItem.MR(1, 0, Junction, nAnswerOptions = b0001)

note right
**AnswerOptions**
           InvalidAddress = FALSE
           HouseNumberInRange = FALSE
           Ambiguous        = FALSE
           GuidancePossible = TRUE

end note

else Selectioncriterion type = houseno

B --> B : housenumberAvailable == TRUE && housenumbervalid == FALSE

B --> A: NaviSetDestinationItem.MR(1, 0, houseno, nAnswerOptions = b0001)

note right
**AnswerOptions**
           InvalidAddress = FALSE
           HouseNumberInRange = FALSE
           Ambiguous        = FALSE
           GuidancePossible = TRUE

end note

else Selectioncriterion type = reserved (Ambiguity to be resolved further)

B --> B : housenumberAvailable = TRUE && housenumbervalid == TRUE , Ambigous = TRUE

B --> A: NaviSetDestinationItem.MR(1, 0, Reserved, nAnswerOptions = b0110)

note right
**AnswerOptions**
           InvalidAddress = FALSE
           HouseNumberInRange = TRUE
           Ambiguous        = TRUE
           GuidancePossible = FALSE

end note

end

||10||

alt Error response

C --> B: onSdsCheckAddressError(SDSAddress verifiedAddress)

B --> A: NaviSetDestinationItem.Error(DestinationValueNotResolved)

||20||

end

@enduml