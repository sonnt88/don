
@startuml

title: <size:20> Split map  </size>

actor Speaker
participant "SDS_MW" as A
participant "SDSAdapter" as B
participant "org.bosch.cm.NavigationService" as C

Speaker -> A: Show split map
activate A

||10||

A -> B: NaviSetMapMode.MS\n(nTBTSymbols = SYMBOLS_MIXED)
activate B

B -> C: sendShowMapScreenWithMapViewModeRequest\n(MAP_VIEW_MODE_SPLIT_MAP)
activate C

B --> A: NaviSetMapMode.MR()

...

C --> B: onShowMapScreenWithMapViewModeResponse()

||20||

@enduml