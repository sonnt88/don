== Audio Management ==

=== Overview ===

The _Audio Management_ covers the acquisition of audio input and output channels
as well as the configuration of the audio streaming paths for SDS.

=== Source Switching ===

The audio source switching is handled by the _Audio Manager_ and the _Stream Router_.
The _SdsAdapter_ integrates the ASF-specific variant of the _Audio Routing Library_.
This library provides the interface class _arl_tclISource_ASF_ which is inherited by 
our _SdsAudioSource_ class. _SdsAudioSource_ forms the core of the audio management
in _SdsAdapter_.

Upon starting an SDS session, _SdsAudioSource_ is used to allocate the audio
channel for SDS. The audio management will indicate to this class, when the
audio channel was set up.

=== ECNR ===

The _Echo Cancellation and Noise Reduction_ -- _ECNR_ for short -- is a component
that removes unwanted noise from the microphone signal. 

Of course, the microphone also picks up the speaker output, which would distort
the user's speech input. The ECNR takes care that the speaker output is also
removed from the microphone signal.

Upon starting an SDS session the ECNR must be configured for SDS mode.

=== Microphone Level ===

The level of the microphone input is supposed to be displayed within the
animated talking head icon on the SDS popups.
Therefore, we need to measure the microphone level. The measurement is performed
by the Audio Processor component, which also needs to be configured appropriately
upon starting an SDS session.

When the microphone level measurement is active, the _SdsAdapter_ receives regular
updates with measurement values. 


=== Audio Setup Sequences ===

==== Starting an SDS Session ====

image:2-feature_design/audio_management/uml/microphone_level.png[]

image:2-feature_design/audio_management/uml/CLS_Audio_Management.png[]


