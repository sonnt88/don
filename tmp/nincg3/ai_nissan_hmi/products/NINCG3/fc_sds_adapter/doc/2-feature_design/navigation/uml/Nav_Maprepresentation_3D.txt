
@startuml

title: <size:20> Maprepresentation -> 3D  </size>

actor Speaker
participant "SDS_MW" as A
participant "SDSAdapter" as B
participant "org.bosch.cm.NavigationService" as C


C -> B : representation(MapRepresentation)

...

Speaker -> A:  "Show 3D Map"
activate A

A -> B: NaviSetMapMode.MS(nMapView = 3D)

||20||



alt Check current map view representation

||20||

else 

B -> B : IF(Current_mapview == MAP_REPRESENTATION_NOT_SET)


||20||


B -> C: sendsetMapRepresentationRequest(MAP_REPRESENTATION_3D_CAR_HEADING)

||20||

...

C --> B: onsetMapRepresentationResponse()

||20||

else 

B -> B : IF(Current_mapview == MAP_REPRESENTATION_3D_CAR_HEADING)


||20||


B -> B: No action
||20||


else

B -> B : IF(Current_mapview == MAP_REPRESENTATION_2D_CAR_HEADING)


||20||


B -> C: sendsetMapRepresentationRequest(MAP_REPRESENTATION_3D_CAR_HEADING)

||20||

...

C --> B: onsetMapRepresentationResponse()

||20||

else

B -> B : IF(Current_mapview == MAP_REPRESENTATION_2D_NORTH_UP)


||20||


B -> C: sendsetMapRepresentationRequest(MAP_REPRESENTATION_3D_CAR_HEADING)

||20||

...

C --> B: onsetMapRepresentationResponse()

||20||

end

||10||

B --> A: NaviSetMapMode.MR()


@enduml