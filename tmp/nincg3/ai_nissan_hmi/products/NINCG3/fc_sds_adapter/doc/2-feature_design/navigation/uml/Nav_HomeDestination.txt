
@startuml

title: <size:20> Navigate to Home address  </size>

actor Speaker
participant "SDS_MW" as A
participant "SDSAdapter" as B
participant "org.bosch.cm.NavigationService" as C


C -> B : homeLocationStatus(Boolean)

Speaker -> A: Navigate to Home
activate A

A -> B: NaviSelectDestListEntry.MS(BYVAL, HOME_DEST,0)
activate B

||30||

B -> B: CheckHomeAddressAvailable()

||20||

alt HomeAddressAvailable = TRUE

B -> C : SendstartGuidanceToHomeLocationRequest()


B --> A: NaviSelectDestListEntry.MR()

||40||

...

C -> B : OnstartGuidanceToHomeLocationResponse()

||20||

else HomeAddressAvailable = FALSE

||20||

B --> A: NaviSelectDestListEntry.Error(NoHomeAddressAvailable)

||40||

end

||30||

@enduml