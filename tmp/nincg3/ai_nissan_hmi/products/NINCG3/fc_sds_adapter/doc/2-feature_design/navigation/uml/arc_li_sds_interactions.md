ARC_LI_SDS_Interactions {#arc_li_sds_interactions}
=======================

## Overview
Within this document, the interfaces and interactions between the main components SDS and NavigationCore with 
(Navigation-)HMI in the middle shall be described.

## Architecture 

In the following, the architecture of the involved components is sketched.
In addtion to the Standard HMI with user input via touch screen or rotary encoder(s) and visual feedback, 
the Speech Dialog System (SDS) is a second main client.

General architecture is as follows:

@startuml

interface "INavMiddleware" <<C++>>
interface "NaviService" <<franca/DBUS>>

package "Navi_RNAIVI_NDS_[VERSION]_armv7a.ipk" {
	interface "NavFI" <<FI>>
	interface "RenFI" <<FI>>

	package "procnav.out" #WhiteSmoke {
		[NavCore] <<CCA>> #DarkKhaki
		NavCore -up- [NavFI]
	}

	package "procmap.out" #WhiteSmoke {
		[MapViewer] <<CCA>> #DarkKhaki
		MapViewer -up- [RenFI]
	}

	package "libRNAIVINavMiddleware.so" #WhiteSmoke {
		interface "IBusinessCtrl" <<CMS>>

		package "RNAIVIPresCtrl" {
			[RNAIVIPresCtrl] <<lib>> #Khaki
			RNAIVIPresCtrl -up- [INavMiddleware]
			[RNAIVIPresCtrl] -down-> IBusinessCtrl
		}

		package "BusinessCtrl" {
			[BusinessCtrl] <<ASF>> #DarkKhaki
			BusinessCtrl -up- [IBusinessCtrl]
			[BusinessCtrl] -down-> NavFI
			[BusinessCtrl] -down-> RenFI
		}

	}
}

package "apphmi_navigation.out" #WhiteSmoke {
	interface "NaviHMIContract" <<XML>>

	[NaviGUI] <<CGIStudio>>
	[NaviStateMachine] <<VisualState>>
	[NaviHall] <<ASF>> #Khaki

	[NaviGUI] <-down-> NaviHMIContract
	[NaviStateMachine] <-left-> NaviHMIContract
	[NaviHall] <-up-> NaviHMIContract
	[NaviHall] -down-> INavMiddleware
	[NaviHall] -down- NaviService
}

interface "SdsGuiService" <<franca/DBUS>>

package "apphmi_sds.out" #WhiteSmoke {
	interface "SDSHMIContract" <<XML>>

	[SDSGUI] <<CGIStudio>>
	[SDSStateMachine] <<VisualState>>
	[SDSHall] <<ASF>> #Khaki

	[SDSGUI] <-down-> SDSHMIContract
	[SDSStateMachine] <-left-> SDSHMIContract
	[SDSHall] <-up-> SDSHMIContract
	[SDSHall] -down-> SdsGuiService
}

[SDSAdapter] <<ASF>>
[SDSAdapter] -up-> NaviService
[SDSAdapter] -up- SdsGuiService
[SDSAdapter] -down- sds2hmi_fi

interface "sds2hmi_fi" <<cca>>

package "SDS" {
	[SDSMW] <<CCA>>
	[SDSCore] <<???>>
	
	[SDSMW] -up-> sds2hmi_fi
	[SDSMW] -down-> [SDSCore]
}

note left of "SDSMW"
	internal structure of package SDS is only
	sketched here and likely much more complex.
end note

note left of "SDSMW" #lightblue
	Goronzy-Thomae Silke (CM-AI/ECD2)
	Sundeep Rasakatla (RBEI/ECF4)	
end note
	
note left of "SDSAdapter" 
	Other interfaces used by SDSAdapter to connect 
	SDS to the rest of the system are omitted here
	for simplification.
end note

note left of "SDSAdapter" #lightblue
	(Janssen Dirk (CM-AI/EPB1))
	(Chadha Gaurav (RBEI/ECV3))
	Palaniveeran Kannan (CM-AI/EPD2)
	Ravikumar Nagarajan (RBEI/ECP3Z)
	(Raju B D (RBEI/ECV3))
end note

note left of "NaviHall"
  NaviHall is the "glue" layer between HMIs, Halls
  and the core navigation. Incoming user inputs are
  being dispatched to the core navigation, results
  from the core navigation are dispatched back and
  incoming request from other Halls (like from
  SDSHall for speech input) are forwarded to the
  core navigation.
end note

note right of "NaviHall" #lightblue
	Jeevan Babu Otra (RBEI/ECV2)
	Rekha Shrikant Gangasiri (RBEI/ECV2)
end note

note right of "INavMiddleware"
  INavMiddleware is the interface
  between HMI and Navigation application.
  The complete navigation package is being
  built in navi_development. Only the HMI
  is being built in the project context.
end note

note right of RNAIVIPresCtrl #lightblue
	EXTERNAL Wall Holger (Fa. Aventon, CM/ESN2)
	Hornburg Bjoern (CM-AI/ECN3)
end note

note right of NavCore #lightblue
	Brandes Henry (CM/ESN2) (FC LocInput)
end note

note top of SDSGUI #lightblue
	Knoche Denny (CM-AI/EPB1) (FO SDS)
end note

note top of NaviGUI #lightblue
	Bruns Jochen (CM/ESN1) (DM LocInput)
end note

@enduml

## Use Cases

Primary source for definition of use cases is a document with interface description from SDS point of view [1]. 
Starting from this document, an adaption to the new system architecture introduced with RNAIVI system shall be 
developed.

###	Uses cases from SDS point of view

Use cases derived from the sds2hmi_fi methods and properties:

| Id | Use Case                           | Description                                                         |
|----------|--------------------------------|---------------------------------------------------------------------|
| UC_SDS_1 | NavGetCurrentDestination       | Get some info about the current destination (set in HMI context). Not used in NiKai, thus not required in the first step. |
| UC_SDS_2 | NaviCurrentNeighboringLocation | Get countries or states in the surrounding (around current location). Not used in NiKai so far. Usage within RNAIVI tbc. LIFI function in TM: GetNearbyStatesOrCountries |
| UC_SDS_3 | NaviGetNearbyStates            | Similar to 3.2 NaviCurrentNeighboringLocation, but requests states within a certain radius nearby a given state. Same functionality for Navi, but different from SDS point of view.
| UC_SDS_4 | NaviStatus                     | Get Status from Guidance and RouteCalculation. Not Navigation related: Signal SDS availability (according to current language and location) to HMI. |
| UC_SDS_5 | NaviCurrentCountryState        | Get current country and state (if available). Country is required as country code and name, state is required in short form (long form ignored). City name was provided also, but not used in NiKai. PosFI function in TM: PositionInfo()? |
| UC_SDS_6 | NaviStartGuidance | Start guidance to (previously) selected Destination. Destination itself is available in context of HMI. |
| UC_SDS_7 | NaviStopGuidance | Stop current guidance. |
| UC_SDS_8 | NaviGetWaypointListInfo | Get info, whether the waypoint list is full or not. |
| UC_SDS_9 | NaviShowNavMenu | Switch HMI from SDS to Navigation context/screen. |
| UC_SDS_10 | NaviSetDestinationAsWaypoint | Set current destination (in the context of HMI) as new waypoint or replace existing waypoint.  |
| UC_SDS_11 | NaviSelectDestListEntry | Select the stored "Home" destination, such that guidance can be started or that it can be inserted as way point. |
| UC_SDS_12 | NaviGetHouseNumberRange | Get the available house number range (min,max) along with a list of house number patterns for current selected street (in HMI context). LIFI function in TM: GetElementNameList(LocationDescription(up to street, + SelectionCriterion: HouseNumberPattern); |
| UC_SDS_13 | NaviSetDestinationContact | Not used in NiKai. Select an address from Address book contacts as destination. The contact is selected by id. One of its addresses is selected by a LocationType (e.g. Home). |
| UC_SDS_14 | NaviSetDestinationItem | Many sub use cases! Main use is to transfer and verify a destination (POI or Address) from SDS context to Navigation. |
| UC_SDS_15 | CommonGetListInfo | Get info about number (only rough classification: [0,1, multiple], but not the real number) of list elements for list types PreviousDestinations and AddressBook.  |
| UC_SDS_16 | CommonSelectListElement | Select a list entry as destionation, such that a guidance can be started. |
| UC_SDS_17 | NaviSetRouteCriteria? |  |

\todo UC_SDS_11 : Is this "Select Home" the only purpose? Paramter list contains an "UNKNOWN".

\todo UC_SDS_15 : How to transfer list content and real number? A partner method CommonShowDialog is mentioned (but not explained in [1])

\todo Any other use cases with relation to Navigation (middleware)?

###	Use cases from SDSAdapter point of view

The first usecase to implement is the SDS-One-Shot-Entry which is shown in the following diagram focused 
on the interface between SDSAdapter and the navigation middleware.

@startuml{SDSNAVOneShotentry.png}

title SDSOneShotAdressEntry (straigth forward case)

SDSAdapter-->"Navigation Service\n+ Middleware": checkSDSAddress(\n{Country:"USA",\nState:"HAWAII",\nPlace:"WAIPAHU",\nRoad:"KAHUANANI  ST",\nHousenumber:"94-296"}\n)
|||
"Navigation Service\n+ Middleware"-->FINavigation: MatchAsFarAsPossible\n("LocationDescription 1")
FINavigation-[#blue]->"Navigation Service\n+ Middleware": MatchAsFarAsPossibleResult\n("LocationDescription 2")
|||
hnote over "Navigation Service\n+ Middleware" : SelectionCriteria necessary?
|||
"Navigation Service\n+ Middleware"-->FINavigation: getSelectionCriteria\n("LocationDescription 2")
FINavigation-[#blue]->"Navigation Service\n+ Middleware": getSelectionCriteriaResult\n("CriteriaList")
|||
hnote over "Navigation Service\n+ Middleware" : gathering Data
|||
"Navigation Service\n+ Middleware"-[#blue]->SDSAdapter: checkSDSAddressResult\n(\n{Country:"USA",ambigous=false,\nState:"HAWAII",ambigous=false,\nPlace:"WAIPAHU",ambigous=false,\nRoad:"KAHUANANI  ST",ambigous=false,\nnavigable=true, removedInvalidEntry=true, ambigous=true,\nmoreEntrysPossible = true, housenumberValid = false,\nhousenumberAvailable = true, \naddableAddressElements[Housenumber,CrossRoad]")
|||
SDSAdapter-->"Navigation Service\n+ Middleware": setSDSDestination(isWaypoint=false)
"Navigation Service\n+ Middleware"-[#blue]->SDSAdapter:setSDSDestinationResult()
|||
SDSAdapter-->"Navigation Service\n+ Middleware": startSDSGuidance()
"Navigation Service\n+ Middleware"-[#blue]->SDSAdapter:startSDSGuidanceResult()

@enduml
    

\todo How are these usecases / interfaces towards Navi translated by SDS_Adapter? 

###	Use cases from NavigationCore/LocationInput point of view

| ID  | Use Case                          | Description                                                         |
|----------|---------------------------|---------------------------------------------------------------------|
| UC_LI_1 | GetStatesOrCountriesNearby | SDS needs to know the states around CVP and/or destination and/or along route to build its recognition context. |
| UC_LI_2 | GetHouseNumberRangeAndPatterns | For a street, given as LocationDescription, the minimum and maximum house number values and the list of house number pattern have to be returned. |
| UC_LI_3 | TransferAndVerifyDestination | An address or POI destination entered via SDS has to be verified against the database. This includes house number verification and ambiguity signalization. |

## References

[1] https://hi-dms.de.bosch.com/docushare/dsweb/Get/Document-733926/SWCD_SRM14-025_3rd%20Party%20Navi.doc 
