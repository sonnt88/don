@startuml
hide footbox

actor user
participant SDSMW
participant Sds2HmiServer
participant clSDS_Property_MediaStatus
participant clSDS_Method_MediaPlay
participant clSDS_Method_CommonShowDialog
participant clSDS_MenuManager
participant clSDS_Display
participant GuiService

box "SDS_HALL" #LightBlue
participant SdsAdapterHandler
participant SdsHall
participant AppSds_ContextRequestor
end box
participant Mplay_MediaPlayerProxy
participant AudioSourcechangeProxy



Sds2HmiServer ->Sds2HmiServer:createproxies()
activate Sds2HmiServer
Sds2HmiServer-->Mplay_MediaPlayerProxy:createProxy("sds2mediaPlayerFiPort", *this)
Sds2HmiServer-->AudioSourcechangeProxy :createProxy("audioSourceChangePort",this)
deactivate Sds2HmiServer

Sds2HmiServer ->Sds2HmiServer:createSds2HmiService
activate Sds2HmiServer
Sds2HmiServer->clSDS_Property_MediaStatus :_poMediaStatus = new clSDS_Property_MediaStatus(service, _sds2MediaProxy)
Sds2HmiServer->clSDS_Method_MediaPlay:  _poMediaPlayUSB =  new clSDS_Method_MediaPlay(service, _audioSrcChgProxy, _sds2MediaProxy, _poMediaGetAmbiguityList, (*_poGuiService), _mediaGetDeviceInfo)
deactivate Sds2HmiServer

activate clSDS_Method_MediaPlay 
clSDS_Method_MediaPlay -->clSDS_Method_MediaPlay : onServiceAvailable()
clSDS_Method_MediaPlay -->Mplay_MediaPlayerProxy : MediaPlayerDeviceConnections(UpReg)
note over clSDS_Method_MediaPlay : MediaPlayer updates information regarding all currently connected devices ,\n Sds Adapter Stores the list of available devices (_oDeviceList)\n _oDeviceList has has information of devicetag,device type and connected status
Mplay_MediaPlayerProxy-->clSDS_Method_MediaPlay : onMediaPlayerDeviceConnectionsStatus(status)
clSDS_Method_MediaPlay-->clSDS_Method_MediaPlay : onServiceAvailable()
clSDS_Method_MediaPlay-->AudioSourcechangeProxy : GetSourceList(Register)
clSDS_Method_MediaPlay-->AudioSourcechangeProxy :sendSourceListChanged(Register)
AudioSourcechangeProxy-->clSDS_Method_MediaPlay : onGetSourceListResponse(response)
AudioSourcechangeProxy--> clSDS_Method_MediaPlay :onSourceListChangedSignal(signal)
clSDS_Method_MediaPlay-->AudioSourcechangeProxy : GetSourceList(Register)
note over clSDS_Method_MediaPlay : AudioSourcechange updates Sds Adapter with Available source information ,\n SDS Adapter stores the list of available sources(_sourcelist),\n _sourcelist has information of SourceId and SubSourceID
AudioSourcechangeProxy-->clSDS_Method_MediaPlay : onGetSourceListResponse(response)
deactivate clSDS_Method_MediaPlay

activate clSDS_Property_MediaStatus 
clSDS_Property_MediaStatus -->clSDS_Property_MediaStatus : onServiceAvailable()
clSDS_Property_MediaStatus -->Mplay_MediaPlayerProxy : MediaPlayerDeviceConnections(UpReg)
note over clSDS_Property_MediaStatus : MediaPlayer updates information regarding all currently connected devices
Mplay_MediaPlayerProxy-->clSDS_Property_MediaStatus : onMediaPlayerDeviceConnectionsStatus(status)
clSDS_Property_MediaStatus-->clSDS_Property_MediaStatus :vSendStatus()


clSDS_Property_MediaStatus-->clSDS_Property_MediaStatus : vUpdateDeviceList(oMessage.DeviceList)
note over clSDS_Property_MediaStatus : To update the device list with status of connected IPOD and USB
clSDS_Property_MediaStatus-->clSDS_Property_MediaStatus : vUpdateDeviceListWithMedia(oDeviceList)
note over clSDS_Property_MediaStatus : To update the device list with status of connected Bluetooth Audio
clSDS_Property_MediaStatus-->clSDS_Property_MediaStatus : vUpdateDeviceListWithBTAudio(oDeviceList)
note over clSDS_Property_MediaStatus : To update the device list with status of connected AUX devices
clSDS_Property_MediaStatus-->clSDS_Property_MediaStatus : vUpdateDeviceListWithAux(oDeviceList)
note over clSDS_Property_MediaStatus : To update the device list with status of CD device inserted
clSDS_Property_MediaStatus-->clSDS_Property_MediaStatus : vUpdateDeviceListWithCD(oDeviceList)
note over clSDS_Property_MediaStatus : To update the device list with status of connected CarPlay device
clSDS_Property_MediaStatus-->clSDS_Property_MediaStatus : updateDeviceListWithCarPlayAudio(oDeviceList)
note over clSDS_Property_MediaStatus : To update the device list with status of connected Android Auto device
clSDS_Property_MediaStatus-->clSDS_Property_MediaStatus : updateDeviceListWithAndroidAutoAudio(oDeviceList)
deactivate clSDS_Property_MediaStatus

note over user : User utters "Play USB"
SDSMW-->clSDS_Method_CommonShowDialog :vMethodStart()
clSDS_Method_CommonShowDialog->clSDS_MenuManager :vShowSDSView(oScreenData)
clSDS_Method_CommonShowDialog->clSDS_Method_CommonShowDialog:vSendResult(oScreenData)
clSDS_Method_CommonShowDialog-->SDSMW:vSendMethodResult(oResult);
clSDS_MenuManager->clSDS_Display:vShowSDSView(oScreenData)
clSDS_Display->clSDS_Display:vShowView(_popupRequestSignal, oScreenData.enReadScreenId())
clSDS_Display-->SdsAdapterHandler : _guiService.sendPopupRequestSignal(Layout(),Header(), SpeechInputState(),TextFields())
activate SdsAdapterHandler
SdsAdapterHandler->SdsAdapterHandler:onPopupRequestSignal(signal)
SdsAdapterHandler->SdsAdapterHandler:getLayout(signal)
SdsAdapterHandler->SdsAdapterHandler:processHeaderData(signal)
SdsAdapterHandler->SdsAdapterHandler:processListData(signal)
SdsAdapterHandler->SdsAdapterHandler:processLayout(signal)
deactivate SdsAdapterHandler
SdsAdapterHandler->SdsHall :showScreenLayout(currentLayout)
note over SdsHall : SdsHAll posts courier message to display Confirmation  Screen

SDSMW-->clSDS_Method_MediaPlay::vMethodStart(oMessage)
 clSDS_Method_MediaPlay-->clSDS_Method_MediaPlay : vProcessMessage(oMessage)
 clSDS_Method_MediaPlay-->clSDS_Method_MediaPlay : vPlaySource(DriveID)
 clSDS_Method_MediaPlay-->clSDS_Method_MediaPlay : enGetHmiSourceType(SourceId)
 clSDS_Method_MediaPlay-->clSDS_Method_MediaPlay : activateSource(u32Index)
 clSDS_Method_MediaPlay-->clSDS_Method_MediaPlay : requestSourceActivation()
 clSDS_Method_MediaPlay--> AudioSourcechangeProxy : sendActivateSourceRequest(sourceInfo)
 clSDS_Method_MediaPlay-->SDSMW : vSendMethodResult()
 AudioSourcechangeProxy-->clSDS_Method_MediaPlay :onActivateSourceResponse(response)
 
 SDSMW-->clSDS_Method_CommonStopSession:  vMethodStart()
 clSDS_Method_CommonStopSession-->clSDS_Method_CommonStopSession : vReleaseResources()
 clSDS_Method_CommonStopSession-->clSDS_Method_CommonStopSession : vSendStopSessionToStateMachine()
 clSDS_Method_CommonStopSession-->SDSMW :vSendMethodResult()
 clSDS_Method_CommonStopSession-->clSDS_MenuManager : closeGui()
 clSDS_MenuManager-->clSDS_Display :vCloseView()
 clSDS_Display-->SdsAdapterHandler : sendPopUpRequestCloseSignal()
 SdsAdapterHandler-->SdsAdapterHandler : onPopUpRequestCloseSignal(signal)
 SdsAdapterHandler-->SdsHAll : RequestPopUpClose()
 note over SdsHAll : SDS HALL posts courier message to close SDS Screen
 

 
@enduml