
@startuml

title: <size:20> Show POI icons  </size>

actor Speaker
participant "SDS_MW" as A
participant "SDSAdapter" as B
participant "org.bosch.cm.NavigationService" as C


alt User requesting for show all POI icons

Speaker -> A:  "Show All Icons"
activate A

A -> B: NaviSetMapMode.MS(IconSetting)

||20||

note right
**T_Nav_IconSetting**
           DisplayAction = ICONDSPLY_SHOWALL
           CategoryPredefined = AllPOIs

end note


||20||


B -> C: sendsetPoiIconCategoriesInMapRequest\n(MapPOIIconCategories)



B --> A: NaviSetMapMode.MR()

...

C --> B: onsetPoiIconCategoriesInMapResponse()

||20||

else user requesting for show icon specific POI category

Speaker -> A:  "Show Restaurant Icons"



A -> B: NaviSetMapMode.MS(IconSetting)

||20||

note right
**T_Nav_IconSetting**
           DisplayAction = ICONDSPLY_SHOW
           CategoryPredefined = Restaurant

end note


||20||

B -> C: sendgetPoiIconCategoriesInMapRequest()

...

C --> B: ongetPoiIconCategoriesInMapResponse(MapPOIIconCategories)

||40||


B -> C: sendsetPoiIconCategoriesInMapRequest\n(MapPOIIconCategories + MAP_POI_ICON_CATEGORY_RESTAURANT)



B --> A: NaviSetMapMode.MR()

...

C --> B: onsetPoiIconCategoriesInMapResponse()

||20||

end

@enduml