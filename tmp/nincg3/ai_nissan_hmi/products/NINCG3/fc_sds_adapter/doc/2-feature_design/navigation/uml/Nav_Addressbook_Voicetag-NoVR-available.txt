
@startuml
     title: <size:20>VR Not supported</size>
                  
actor User
participant "org.bosch.cm.NavigationService" as A
participant "SDSAdapter" as B
participant "SDS_MW" as C
participant "SDS_Gui Service" as D

User -> A: Press "STORE/PLAY"

activate A

A -> B: SDSVoiceTagIdStatus(destinationMemoryEntryId ,\nsdsVoiceTagId, \nCREATE_NEW_VOICETAG / PLAY_VOICETAG)

||20||

B -> B: Check VR status

||20||

alt current SDS Status = IDLE_TTS_ONLY

||20||


B -> D: Show no VR supported SDS pop up

||20||

alt based on Navi request

||20||

else on CREATE_NEW_VOICETAG request

B -> A: sendsdsUpdateVoiceTagRequest(SDSVoiceTagId voiceTagID)

||20||

note right

Structure **SDSVoiceTagId**
           destinationMemoryEntryId  = destinationMemoryEntryId as sent by Navi
           sdsVoiceTagId       = 0
           SDSVoiceTagOptions  = NO_VOICETAG_CREATED
end note

||20||

else on PLAY_VOICETAG request

B -> B : No action

||20||

end

||20||

else current SDS Status = LOADING

||20||


B -> D: Show VR Loading SDS pop up

||20||

alt based on Navi request

||20||

else on CREATE_NEW_VOICETAG request

B -> A: sendsdsUpdateVoiceTagRequest(SDSVoiceTagId voiceTagID)

||20||

note right

Structure **SDSVoiceTagId**
           destinationMemoryEntryId  = destinationMemoryEntryId as sent by Navi
           sdsVoiceTagId             = 0
           SDSVoiceTagOptions        = NO_VOICETAG_CREATED
end note

||20||

else on PLAY_VOICETAG request

B -> B : No action

||20||

end 

end

||20||

@enduml