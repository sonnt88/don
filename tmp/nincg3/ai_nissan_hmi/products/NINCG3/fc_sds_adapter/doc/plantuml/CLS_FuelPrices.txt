@startuml

package application {
class clSDS_ListInfoObserver
class clSDS_List
class clSDS_ListScreen
class clSDS_FuelPriceList
class clSDS_NaviListItems  
class GuiService
}

package functions{
class clSDS_Method_CommonShowDialog
class clSDS_Method_CommonSelectListElement
class clSDS_Method_CommonGetListInfo
class clSDS_Method_NaviStartGuidance
class clSDS_Method_InfoShowMenu
}

package MIDW_EXT_SXM_FUEL_FI{
class GetFuelInfoListCallbackIF
class SxmListModeCallbackIF
class FuelInfoListsStatusCallbackIF
class FuelBrandNameListCallbackIF
class FuelTypeListCallbackIF
class SxmDataServiceStatusCallbackIF
class GetFuelStationInfoCallbackIF
} 

package MIDW_EXT_SXM_CANADIAN_FUEL_FI{
class CanadianFuelInfoListsStatusCallbackIF
class CanadianFuelBrandNameListCallbackIF
class CanadianFuelTypeListCallbackIF
class SxmListModeCallbackIF
class GetCanadianFuelInfoListCallbackIF
class GetCanadianFuelStationInfoCallbackIF
class SxmDataServiceStatusCallbackIF
}

package NavigationService{
class PositionInformationCallbackIF
class StartGuidanceToPosWGS84CallbackIF
}
package SdsGui{
class SdsGuiServiceProxy
}
package AppHmiSds{
class SdsHall
class SdsAdapterHandler
}
package KDS{
class DP_s32GetConfigItem
}


clSDS_Method_CommonGetListInfo -->clSDS_List
clSDS_Method_CommonGetListInfo -->clSDS_NaviListItems
clSDS_Method_CommonShowDialog -->clSDS_ListScreen
clSDS_Method_CommonShowDialog -->GuiService
clSDS_Method_CommonSelectListElement -->clSDS_ListScreen
clSDS_Method_NaviStartGuidance -->GuiService
clSDS_Method_NaviStartGuidance ->clSDS_NaviListItems  
clSDS_Method_InfoShowMenu -->GuiService

clSDS_List --|>clSDS_ListInfoObserver

clSDS_FuelPriceList --|>clSDS_NaviListItems
clSDS_FuelPriceList --|>GetFuelInfoListCallbackIF
clSDS_FuelPriceList --|>SxmListModeCallbackIF
clSDS_FuelPriceList --|>FuelInfoListsStatusCallbackIF
clSDS_FuelPriceList --|>FuelBrandNameListCallbackIF
clSDS_FuelPriceList --|>FuelTypeListCallbackIF
clSDS_FuelPriceList --|>SxmDataServiceStatusCallbackIF
clSDS_FuelPriceList --|>GetFuelStationInfoCallbackIF


clSDS_FuelPriceList --|>CanadianFuelInfoListsStatusCallbackIF
clSDS_FuelPriceList --|>CanadianFuelBrandNameListCallbackIF
clSDS_FuelPriceList --|>CanadianFuelTypeListCallbackIF
clSDS_FuelPriceList --|>SxmListModeCallbackIF
clSDS_FuelPriceList --|>GetCanadianFuelInfoListCallbackIF
clSDS_FuelPriceList --|>GetCanadianFuelStationInfoCallbackIF
clSDS_FuelPriceList --|>SxmDataServiceStatusCallbackIF

clSDS_FuelPriceList --|>PositionInformationCallbackIF
clSDS_FuelPriceList --|>StartGuidanceToPosWGS84CallbackIF

clSDS_FuelPriceList ...>DP_s32GetConfigItem

clSDS_FuelPriceList --|>clSDS_List

clSDS_List -->clSDS_ListInfoObserver
clSDS_ListScreen -->clSDS_List


GuiService --|>SdsGuiServiceProxy
SdsAdapterHandler -->SdsGuiServiceProxy
SdsHall -->SdsAdapterHandler

@enduml