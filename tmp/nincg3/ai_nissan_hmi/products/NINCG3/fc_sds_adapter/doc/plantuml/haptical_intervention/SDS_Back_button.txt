@startuml

hide footbox
title Start SDS in SR_NAV_Main in haptical Mode - Press Back button
actor user
participant hmi
participant adapter
participant vc
participant rec
participant pp

note over user, pp
   Precondition
   - system is in SR_Nav_Main; SDS is in haptical mode
   - display shows Navigation main menu
end note

user -> hmi : Push Back Soft Button
hmi -> adapter: backButtonPressed()
adapter -> vc : CommonActionRequest(BACK)
vc -> pp : cancelAllPrompts()
note right: skip any queued prompts
vc -> vc : cancelPauseMode()
vc -> vc : transitionTo(SR_GLO_Main)
vc -> rec : start()
vc -> adapter : showDialog(SR_GLO_Main)
adapter -> hmi : popupRequest(...)

@enduml