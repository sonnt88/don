package sds_gui_fi

interface PopUpService
{
    attribute String screenID readonly
    
    attribute SpeechInputState sdsStatus readonly
    
    <** @description : Indicates the strength of the audio input signal.
                   The value is interpreted as percentage.
                   The value range is 0 .. 100. **>
    attribute UInt32 microphoneLevel readonly
    
    broadcast PopupRequestClose {
        out {
        }  
    }
    
    broadcast PopupRequest {
       out {
           String layout
           String header
           SpeechInputState speechInputState
           TextField [] textFields
       }
    }
    
    enumeration SpeechInputState {
        UNKNOWN = "0"
        IDLE
        DIALOGOPEN
        LOADING
        ERROR
        LISTENING 
        ACTIVE
        PAUSE
    }
    
    struct TextField {
        String tagName
        String string
        TextAttribute attrib
    }
    
    enumeration TextAttribute {
        NORMAL
        COMMAND
        SELECTED
        GREYED_OUT
        AGE_TODAY
        AGE_ONE_DAY
        AGE_TWO_DAYS
        AGE_THREE_PLUS_DAYS
    }
}

interface SettingsService
{
    attribute Boolean bestMatchAudio

    attribute Boolean bestMatchPhoneBook
    
    attribute Boolean beepOnlyMode
    
    attribute Boolean shortPrompt
    
    attribute ActiveSpeakerGender voicePreference
    
    method SpeechRateUpdate {
        in  {
            UInt32 speechrate
        }
    }
    
    enumeration ActiveSpeakerGender {
        FEMALE
        MALE
    }
    
}

interface SdsPhoneService
{
    method VoiceTagAction {
        in  {
            UserWordAction  userAction
            UInt32  uniqueContactID
            UInt32  phoneProfileID
        }
    }
    
    method QuickDialListUpdate {
        in {
            QuickDialList[] quickDialList
        }
    }
    
    broadcast SpokenDigits {
        out {
            String digits
        }
    }  
        
    broadcast SmsContent {
        out {
            String header
            String message
        }
    }

    broadcast UserWordUpdate {
        out {
            UserWordList[] userWordLists
        }
    }
    
    struct QuickDialList {
        UInt32 uniqueContactID
        String relationshipType
    }
    
    struct UserWordList {
        UInt32 userWordProfileID
        UInt32[] phoneUWID
    }

    enumeration UserWordAction {
        RECORD
        DELETE
        REPLACE
        PLAY
    }
}

interface SdsGuiService
{

    <** @description : OBSOLETE -> use sdsStatus attribute from sds_gui_fi::PopUpService**>
    attribute SpeechInputState sdsStatus readonly

    attribute SpeechVersionParameter[] SpeechVersion readonly
  
    method pttPress {
        in {
            KeyState pttPressType
        }
    }

    method startSessionContext {
        in {
            ContextType startupContextType
        }
    }    
    
    method ManualOperation {
        in {
            OperationType operationType
            UInt32 value
        }
    }

       
    method TestModeUpdate {
        in {
            String tModesentence
        }
    }
   	
	method BackPress {

	}
	
    method AbortSession {

	}
	
	method StopSession {

	}
	
	method PauseSession {

	}
	
	method ResumeSession {

	}
	
    method settingsCommand {
    
    }
    
    method StartEarlyHandling {
    
    }

    method StopEarlyHandling {
    
    }
    
    method helpCommand {
    
    }
    
    <** @description : OBSOLETE -> use SpokenDigits broadcast from sds_gui_fi::SdsPhoneService**>
    broadcast SpokenDigits {
        out {
            String digits
        }
    } 
	  
    broadcast Event {
        out {
            Event guiEvent
        }
    }
    
   
    struct SpeechVersionParameter {
    	CoreSpeechParameter corespeechparameter
		String value	
    }

    <** @description : OBSOLETE -> use attribute SpeechInputState sdsStatus from sds_gui_fi::PopUpService**>
    enumeration SpeechInputState {
        UNKNOWN = "0"
        IDLE
        DIALOGOPEN
        LOADING
        ERROR
        LISTENING 
        ACTIVE
        PAUSE
    }

    enumeration KeyState {
        KEY_UNDEFINED
        KEY_HK_MFL_PTT_SHORT
        KEY_HK_MFL_PTT_LONG
    }

    enumeration Event {
        SPEECH_DIALOG_CLOSE
        SPEECH_DIALOG_LOADING_OPEN
        SPEECH_DIALOG_NAV_WAYPOINTLIST_FULL
        SPEECH_DIALOG_NEW_SMS_POPUP_CLOSE
        SPEECH_DIALOG_NO_DATA_CARRIER_OPEN
        SPEECH_DIALOG_SELECT_PAIRED_DEVICE_LIST
        SPEECH_DIALOG_SERVICE_UNAVAILABLE_CLOSE
        SPEECH_DIALOG_SHOW_INFO_TRAFFIC
        SPEECH_DIALOG_SHOW_INFO_CURRENT_WEATHER_REPORT
        SPEECH_DIALOG_SHOW_INFO_WEATHER_REPORT_MAP
        SPEECH_DIALOG_SHOW_INFO_WEATHER_5_DAY_FORECAST
        SPEECH_DIALOG_SHOW_INFO_WEATHER_6_HOUR_FORECAST
        SPEECH_DIALOG_SHOW_INFO_MOVIE_FAV_THEATERS
        SPEECH_DIALOG_SHOW_INFO_MOVIE_LIST_THEATERS
        SPEECH_DIALOG_SHOW_INFO_MOVIE_LIST_MOVIE
        SPEECH_DIALOG_SHOW_INFO_STOCK
        SPEECH_DIALOG_SHOW_INFO_SXM_SPORTS_ID
        SPEECH_DIALOG_SHOW_INFO_SXM_SPORTS_FAVOURITES
        SPEECH_DIALOG_SHOW_MENU_MEDIA
        SPEECH_DIALOG_SHOW_MENU_SMS
        SPEECH_DIALOG_SHOW_ON_MAP
        SPEECH_DIALOG_SHOW_PHONE_APP_SIRI
        SPEECH_DIALOG_SHOW_PHONE_APP_VOICE_ASSIST
        SPEECH_DIALOG_SDS_START_SESSION
        SPEECH_DIALOG_PHONE_GOTO_PAIRING
        SPEECH_DIALOG_SDS_STOP_SESSION
        SPEECH_DIALOG_SDS_PLAY_AM
        SPEECH_DIALOG_SDS_PLAY_FM
        SPEECH_DIALOG_SDS_PLAY_XM
        SPEECH_DIALOG_SHOW_TCU_VOICE_MENU
        SPEECH_DIALOG_SHOW_SPORTS_FAVOURITES
        SPEECH_DIALOG_SDS_PHONE_HANDSFREE_STATE
        SPEECH_DIALOG_SHOW_SPORTS_FOOTBALL
        SPEECH_DIALOG_SHOW_SPORTS_BASEBALL
        SPEECH_DIALOG_SHOW_SPORTS_BASKETBALL
        SPEECH_DIALOG_SHOW_SPORTS_GOLF
        SPEECH_DIALOG_SHOW_SPORTS_SOCCER
        SPEECH_DIALOG_SHOW_SPORTS_ICEHOCKEY
        SPEECH_DIALOG_SHOW_SPORTS_MOTORSPORTS
        SPEECH_DIALOG_SHOW_ENERGY_FLOW
        SPEECH_DIALOG_SHOW_POPUP_LOADING_OPEN
        SPEECH_DIALOG_SHOW_POPUP_NODATACARRIER_OPEN
        SPEECH_DIALOG_SHOW_POPUP_LANGUAGE_NOT_SUPPORTED
        SPEECH_DIALOG_SHOW_POPUP_NBEST_PHONEBOOK_OFF
        SPEECH_DIALOG_SHOW_POPUP_NBEST_AUDIO_OFF
        SPEECH_DIALOG_SHOW_VR_SETTINGS
        SPEECH_DIALOG_START_NAVIHOMEDESTINATION
        SPEECH_DIALOG_START_NAVIWORKDESTINATION
        SPEECH_DIALOG_SHOW_NAVI_CONTEXT_CANCEL_ROUTE
        SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_PASSIVE
        SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE
        SPEECH_DIALOG_PLAY_CARPLAY_AUDIO
        SPEECH_DIALOG_PLAY_ANDROID_AUTO_AUDIO
        SPEECH_DIALOG_PLAY_BEEP_AUDIO
        SPEECH_DIALOG_SDS_SESSION_ABORT
    }
   
    enumeration OperationType {
        MANUAL_INTERVENTION_HAPTICAL_SELECTION
        MANUAL_INTERVENTION_ENCODER_SELECTION          
        MANUAL_INTERVENTION_ENCODER_START_ROTATION
        MANUAL_INTERVENTION_ENCODER_FOCUS_MOVED          
        MANUAL_INTERVENTION_NEXT_PAGE          
        MANUAL_INTERVENTION_PREV_PAGE          
    }
       
    enumeration ContextType {
        SDS_CONTEXT_CALL
        SDS_CONTEXT_DIALNUM
        SDS_CONTEXT_CALLHIST
        SDS_CONTEXT_QUICKDIAL
        SDS_CONTEXT_READTEXT
        SDS_CONTEXT_SENDTEXT
        SDS_CONTEXT_STREETADDR
        SDS_CONTEXT_POI
        SDS_CONTEXT_POICAT
        SDS_CONTEXT_INTERSECTION
        SDS_CONTEXT_CITYCENTER
        SDS_CONTEXT_PLAYALBUM
        SDS_CONTEXT_PLAYARTIST
        SDS_CONTEXT_PLAYSONG
        SDS_CONTEXT_PLAYLIST
    }
   
    enumeration SdsContext {
        SPI_CONTEXT_CARPLAY_AUDIO    
        SPI_CONTEXT_ANDROIDAUTO_AUDIO
        SPI_CONTEXT_BACKGROUND
        SPI_CONTEXT_PHONE
    }
        enumeration CoreSpeechParameter {
    	COREPARAM_NONE
    	COREPARAM_TTS_ENGINE
    	COREPARAM_TTS_VOICE_NAME
    	COREPARAM_TTS_VOICE_GENDER
    	COREPARAM_TTS_VOICE_VERSION
    	COREPARAM_REC_VOCON_ENG_VERSION
    	COREPARAM_REC_ACO_MODEL_VERSION
    }
}
    
