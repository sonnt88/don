/*********************************************************************//**
 * \file       SXMDatabaseHandler.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "sqlite3.h"
#include "SXMDatabaseHandler.h"
#include "sds_sxm_query_defines.h"
#include "SdsSxmDB_Trace.h"

#if (OSAL_CONF==OSAL_LINUX)
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#endif

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSSXMDB
#include "trcGenProj/Header/SXMDatabaseHandler.cpp.trc.h"
#endif


SXMDatabaseHandler::SXMDatabaseHandler()
{
   ETG_TRACE_USR4(("SXMDatabaseHandler::SXMDatabaseHandler()"));

   m_bIsXMDBExists = false;
   m_blDeleteXMDatabase = false;
   m_pXMDBhandle = OSAL_NULL;
   m_pXMStmtHandle = OSAL_NULL;

   vSetUpXMDatabase();
}


SXMDatabaseHandler::~SXMDatabaseHandler()
{
   ETG_TRACE_USR4(("SXMDatabaseHandler::~SXMDatabaseHandler()"));

   closeXMDatabase();
}


void SXMDatabaseHandler::closeXMDatabase()
{
   ETG_TRACE_USR4(("SXMDatabaseHandler::closeXMDatabase()"));

   tU32 u32Ret  =   SQLITE_ERROR;
   if (OSAL_NULL != m_pXMDBhandle)
   {
      u32Ret = sqlite3_close(m_pXMDBhandle);
      if (SQLITE_OK != u32Ret)
      {
         ETG_TRACE_USR4(("SXMDatabaseHandler::~SXMDatabaseHandler() sqlite3_close failed(%d) for XM", u32Ret));
      }
      m_pXMDBhandle = OSAL_NULL;
      m_pXMStmtHandle = OSAL_NULL;
   }

   if (m_blDeleteXMDatabase)
   {
      ETG_TRACE_USR4(("SXMDatabaseHandler::~SXMDatabaseHandler() XM Database Needs to be deleted"));
      tChar l_strStringPath[SPEECH_XMTUNER_DATABASE_PATH_LOC_SiZE] = SPEECH_XMTUNER_DATABASE_PATH_LOC;
      if (OSAL_NULL != OSAL_szStringNConcat(l_strStringPath, FILE_SYSTEM_SEPERATOR, FILE_SYSTEM_SEPERATOR_SIZE))
      {
         if (OSAL_NULL != OSAL_szStringNConcat(l_strStringPath, SPEECH_XMTUNER_DATABASE_NAME, SPEECH_XMTUNER_DATABASE_NAME_SIZE))
         {
            try
            {
               bDeleteFile(l_strStringPath);
            }
            catch (...)
            {
               ETG_TRACE_USR4(("SXMDatabaseHandler::~SXMDatabaseHandler() Exception in blDeleteFile"));
            }
         }
      }
      m_blDeleteXMDatabase = false;
   }
   m_bIsXMDBExists  = false;
}


void SXMDatabaseHandler::openXMDatabase()
{
   ETG_TRACE_USR4(("SXMDatabaseHandler::openXMDatabase()"));

   if (m_bIsXMDBExists)
   {
      tU32 u32Ret = SQLITE_ERROR;
      u32Ret = sqlite3_open(m_dbFilePath, &m_pXMDBhandle);
      if ((u32Ret != SQLITE_OK) || (OSAL_NULL == m_pXMDBhandle))
      {
         if (bIsDeletionRequired(u32Ret))
         {
            m_blDeleteXMDatabase = true;
         }
         sqlite3_close(m_pXMDBhandle);
         m_pXMDBhandle = OSAL_NULL;
         u32Ret = SQLITE_ERROR;
      }
      ETG_TRACE_USR4(("SXMDatabaseHandler::openXMDatabase(): u32Ret = %d.", u32Ret));
   }
   else
   {
      vSetUpXMDatabase();
   }
}


tBool SXMDatabaseHandler::blIsFolderExists(tCString szFilePath)
{
   tBool blFilePathExists = false;
#if (OSAL_CONF==OSAL_LINUX)
   if (OSAL_NULL != szFilePath)
   {
      struct stat  st;
      if (stat(szFilePath, &st) == 0)
      {
         if (S_ISDIR(st.st_mode))
         {
            blFilePathExists = true;
         }
      }
   }
#endif
   return blFilePathExists;
}


tBool SXMDatabaseHandler::blIsFileExists(tCString szFilePath)
{
   tBool blFilePathExists = false;
#if (OSAL_CONF==OSAL_LINUX)
   if (OSAL_NULL != szFilePath)
   {
      struct stat  st;
      if (stat(szFilePath, &st) == 0)
      {
         if (S_ISREG(st.st_mode))
         {
            blFilePathExists = true;
         }
      }
   }
#endif
   return blFilePathExists;
}


tBool SXMDatabaseHandler::bCreateFolder(tCString szFilePath)
{
   tBool blRet = false;
#if (OSAL_CONF==OSAL_LINUX)
   if (OSAL_NULL != szFilePath)
   {
      if (!blIsFolderExists(szFilePath))
      {
         if (mkdir(szFilePath, 0777) == 0)
         {
            blRet = true;
         }
      }
      else
      {
         blRet = true;
      }
   }
#endif
   return blRet;
}


tBool SXMDatabaseHandler::blMakePath(tCString strPath)
{
   tBool          blStatus = false;
   if (OSAL_NULL != strPath)
   {
      tChar*           pp;
      tChar*           sp;
      tChar*         copypath = strdup(strPath);

      blStatus = true;;
      pp = copypath;
      while ((blStatus == true) && ((sp = (tChar*)OSAL_ps8StringSearchChar(pp, '/')) != 0))
      {
         if (sp != pp)
         {
            *sp = '\0';
            blStatus = bCreateFolder(copypath);
            *sp = '/';
         }
         pp = sp + 1;
      }
      if (blStatus)
      {
         blStatus = bCreateFolder(strPath);
      }
      free(copypath);
   }
   return blStatus;
}


tBool SXMDatabaseHandler::bDeleteFile(tCString strPath)
{
   tBool l_blStatus = false;
   if (OSAL_NULL != strPath)
   {
      if (blIsFileExists(strPath))
      {
#if (OSAL_CONF==OSAL_LINUX)
         if (unlink(strPath) == 0)
         {
            l_blStatus = true;
         }
#endif
      }
      else
      {
         l_blStatus = true;
      }
   }
   return l_blStatus;
}


tBool SXMDatabaseHandler::blGetXMTunerDataBasePath(/*OUT*/ tString szFilePath)
{
   tBool blSuccess = false;
   if (OSAL_NULL != szFilePath)
   {
      tU32 l_u32XMTunerDataBasePath = OSAL_u32StringLength(SPEECH_XMTUNER_DATABASE_OSAL_PATH);
      if (OSAL_NULL == OSAL_szStringNCopy(szFilePath, SPEECH_XMTUNER_DATABASE_OSAL_PATH, l_u32XMTunerDataBasePath))
      {
         ETG_TRACE_USR4(("SXMDatabaseHandler::blGetXMTunerDataBasePath():StringCopy failed"));
      }
      blSuccess = true;
   }
   return blSuccess;
}


tVoid SXMDatabaseHandler::vDeleteAllFromXMStationList()
{
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != m_pXMDBhandle)
   {
      u32Ret = sqlite3_exec(m_pXMDBhandle, QUERY_DELETE_ALL_DATA_XM_STATION_LIST, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         if (bIsDeletionRequired(u32Ret))
         {
            m_blDeleteXMDatabase = true;
         }
      }
   }
}


tVoid SXMDatabaseHandler::vDeleteAllFromXMTimeStampTbl()
{
   tU32 u32Ret                     =   SQLITE_ERROR;
   if (OSAL_NULL != m_pXMDBhandle)
   {
      u32Ret = sqlite3_exec(m_pXMDBhandle, QUERY_DELETE_ALL_DATA_XM_TIME_STAMPTBL, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         if (bIsDeletionRequired(u32Ret))
         {
            m_blDeleteXMDatabase = true;
         }
      }
   }
}


tU32 SXMDatabaseHandler::u32GetNoOfRecordsInXMDatabase()
{
   tU32 u32Ret                     =   SQLITE_ERROR;
   tU32 u32RecCount = 0;
   if (OSAL_NULL != m_pXMStmtHandle)
   {
      tString szQuery    =   OSAL_NEW tChar[MAX_QUERY_LEN];
      if (OSAL_NULL != szQuery)
      {
         OSAL_s32PrintFormat(szQuery, QUERY_ROW_COUNT_XM_TABLE);
         u32Ret = u32PrepareXMSelectQuery(szQuery);
         if (u32Ret == SQLITE_OK && OSAL_NULL != m_pXMStmtHandle)
         {
            u32Ret = sqlite3_step(m_pXMStmtHandle);
            if (SQLITE_ROW == u32Ret)
            {
               u32RecCount = sqlite3_column_int(m_pXMStmtHandle, FIELD_0);
            }
            u32Ret = sqlite3_finalize(m_pXMStmtHandle);
            if (u32Ret != SQLITE_OK)
            {
               ETG_TRACE_USR4(("SXMDatabaseHandler::u32GetNoOfRecordsInXMDatabase sqlite3_finalize failed"));
            }
         }
         OSAL_DELETE[] szQuery;
      }
   }
   return u32RecCount;
}


tBool SXMDatabaseHandler::blBeginTransactionForXM(tVoid)
{
   tBool l_blRet =   false;
   if (u32BeginTransaction(m_pXMDBhandle) != SQLITE_ERROR)
   {
      l_blRet = true;
   }
   return l_blRet;
}


tBool SXMDatabaseHandler::blEndTransactionForXM(tVoid)
{
   tBool l_blRet =   false;
   if (u32EndTransaction(m_pXMDBhandle) != SQLITE_ERROR)
   {
      l_blRet = true;
   }
   return l_blRet;
}


tBool SXMDatabaseHandler::bUpdateRecordForXMStationList(SXMChannelListItem* pXMStationListItem)
{
   tBool blRet = false;
   if (OSAL_NULL != pXMStationListItem)
   {
      tU32 u32Ret                     =   SQLITE_ERROR;
      if (OSAL_NULL != m_pXMDBhandle)
      {
         tString szQuery = OSAL_NEW tChar[MAX_QUERY_LEN];
         if (OSAL_NULL != szQuery)
         {
            tChar l_strName[STR_MAX] = {0};
            vProcessStringsForEscapeSequence((tCString)pXMStationListItem->m_sChannelName, l_strName);
            OSAL_s32PrintFormat(szQuery, FORMAT_QUERY_UPDATE_XM_STATION_LIST,
                                pXMStationListItem->m_u16ChannelSID,
                                pXMStationListItem->m_u16ChannelObjectID,
                                pXMStationListItem->m_u16ChannelNumber,
                                l_strName);

            u32Ret = sqlite3_exec(m_pXMDBhandle, szQuery, OSAL_NULL, OSAL_NULL, OSAL_NULL);
            if (bIsDeletionRequired(u32Ret))
            {
               m_blDeleteXMDatabase = true;
            }
            OSAL_DELETE []szQuery;
            if (u32Ret == SQLITE_OK)
            {
               blRet = true;
            }
         }
      }
   }
   return blRet;
}


tBool SXMDatabaseHandler::bUpdateRecordForXMPhoneticList(SXMPhoneticListItem* pXMPhoneticListItem)
{
   tBool blRet = false;
   if (OSAL_NULL != pXMPhoneticListItem)
   {
      tU32 u32Ret                     =   SQLITE_ERROR;
      if (OSAL_NULL != m_pXMDBhandle)
      {
         tString szQuery = OSAL_NEW tChar[MAX_QUERY_LEN];
         if (OSAL_NULL != szQuery)
         {
            tChar l_strName[STR_MAX] = {0};
            vProcessStringsForEscapeSequence((tCString)pXMPhoneticListItem->m_sPhoneticData, l_strName);

            ::std::string format_query_update_xm_phonetic_list = getFormatQueryUpdateXMPhoneticList(pXMPhoneticListItem->m_u16LanguageID);

            OSAL_s32PrintFormat(szQuery, format_query_update_xm_phonetic_list.c_str(),
                                pXMPhoneticListItem->m_u16LanguageID,
                                pXMPhoneticListItem->m_u16ChannelSID,
                                l_strName);

            u32Ret = sqlite3_exec(m_pXMDBhandle, szQuery, OSAL_NULL, OSAL_NULL, OSAL_NULL);
            if (bIsDeletionRequired(u32Ret))
            {
               m_blDeleteXMDatabase = true;
            }
            OSAL_DELETE []szQuery;
            if (u32Ret == SQLITE_OK)
            {
               blRet = true;
            }
         }
      }
   }
   return blRet;
}


std::string SXMDatabaseHandler::getFormatQueryUpdateXMPhoneticList(tU16 language_code)
{
   std::string format_query_update_xm_phonetic_list = "";

   switch (language_code)
   {
      case sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_ENG:
         format_query_update_xm_phonetic_list.append(FORMAT_QUERY_UPDATE_XM_PHONETIC_LIST_EN);
         break;

      case sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_SPA:
         format_query_update_xm_phonetic_list.append(FORMAT_QUERY_UPDATE_XM_PHONETIC_LIST_SP);
         break;

      case sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_FRA:
         format_query_update_xm_phonetic_list.append(FORMAT_QUERY_UPDATE_XM_PHONETIC_LIST_FR);
         break;

      default:
         break;
   }

   return format_query_update_xm_phonetic_list;
}


std::string SXMDatabaseHandler::getPhonemesFromTable(tU16 language_code)
{
   std::string format_query_get_phonemes_from_table = "";

   switch (language_code)
   {
      case sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_ENG:
         format_query_get_phonemes_from_table.append(FORMAT_QUERY_GET_PHONEMES_FROM_TABLE_EN);
         break;

      case sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_SPA:
         format_query_get_phonemes_from_table.append(FORMAT_QUERY_GET_PHONEMES_FROM_TABLE_SP);
         break;

      case sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_FRA:
         format_query_get_phonemes_from_table.append(FORMAT_QUERY_GET_PHONEMES_FROM_TABLE_FR);
         break;

      default:
         break;
   }

   return format_query_get_phonemes_from_table;
}


tU32 SXMDatabaseHandler::u32FindXMStationObjectId(tU16 u16ChannelNumber)
{
   ETG_TRACE_USR4(("SXMDatabaseHandler ::u32FindXMStationObjectId() u16ChannelNumber=%d.", u16ChannelNumber));
   tU32 u32ObjectId = SAAL_OBJECTID_NOTFOUND;
   tString szQuery = OSAL_NEW tChar[MAX_QUERY_LEN];
   if (OSAL_NULL != szQuery)
   {
      tU32 u32Ret                     =   SQLITE_ERROR;
      OSAL_s32PrintFormat(szQuery, FORMAT_QUERY_GET_OBJECT_ID_FROM_CHANNEL_NUM_XM_STATION_LIST, u16ChannelNumber);
      u32Ret = u32PrepareXMSelectQuery(szQuery);
      if (u32Ret == SQLITE_OK)
      {
         u32Ret = u32ExecXMSelectQuery(u32ObjectId);
         if (u32Ret != SQLITE_OK)
         {
            u32ObjectId = SAAL_OBJECTID_NOTFOUND;
         }
      }
      OSAL_DELETE []szQuery;
   }
   return u32ObjectId;
}


tU32 SXMDatabaseHandler::u32FindXMStationObjectId(tCString sChannelName)
{
   ETG_TRACE_USR4(("SXMDatabaseHandler::u32FindXMStationObjectId() sChannelName=%s.", sChannelName));
   tU32 u32ObjectId = SAAL_OBJECTID_NOTFOUND;
   if (OSAL_NULL != sChannelName)
   {
      tString szQuery = OSAL_NEW tChar[MAX_QUERY_LEN];
      if (OSAL_NULL != szQuery)
      {
         tU32 u32Ret                     =   SQLITE_ERROR;
         tChar l_strName[STR_MAX] = {0};
         vProcessStringsForEscapeSequence(sChannelName, l_strName);
         OSAL_s32PrintFormat(szQuery, FORMAT_QUERY_GET_OBJECT_ID_FROM_CHANNEL_NAME_XM_STATION_LIST, l_strName);
         u32Ret = u32PrepareXMSelectQuery(szQuery);
         if (u32Ret == SQLITE_OK)
         {
            u32Ret = u32ExecXMSelectQuery(u32ObjectId);
            if (u32Ret != SQLITE_OK)
            {
               u32ObjectId = SAAL_OBJECTID_NOTFOUND;
            }
         }
         OSAL_DELETE []szQuery;
      }
   }
   return u32ObjectId;
}


enXMChannelUpdateType SXMDatabaseHandler::enGetXMStationUpdateType(SXMChannelListItem* pXMStationListItem)
{
   enXMChannelUpdateType enUpdate = XM_LIST_NO_UPDATE_REQUIRED;
   if (OSAL_NULL != pXMStationListItem)
   {
      if (OSAL_NULL != m_pXMStmtHandle)
      {
         tString szQuery    =   OSAL_NEW tChar[MAX_QUERY_LEN];
         if (OSAL_NULL != szQuery)
         {
            tU32 u32Ret                     =   SQLITE_ERROR;
            OSAL_s32PrintFormat(szQuery, FORMAT_QUERY_GET_CHANNEL_NAME_FROM_CHANNEL_SID_XM_STATION_LIST, pXMStationListItem->m_u16ChannelSID);
            u32Ret = u32PrepareXMSelectQuery(szQuery);
            OSAL_DELETE []szQuery;
            if (u32Ret == SQLITE_OK)
            {
               u32Ret = sqlite3_step(m_pXMStmtHandle);
               if (bIsDeletionRequired(u32Ret))
               {
                  m_blDeleteXMDatabase = true;
               }
               if (SQLITE_ROW == u32Ret)
               {
                  tString strName = (tString)sqlite3_column_text(m_pXMStmtHandle, FIELD_0);
                  tU16 l_u16ChannelNumber = (tU16)sqlite3_column_int(m_pXMStmtHandle, FIELD_1);
                  if (l_u16ChannelNumber != pXMStationListItem->m_u16ChannelNumber)
                  {
                     enUpdate = XM_LIST_UPDATE_REQUIRED;
                  }
                  else
                  {
                     if (OSAL_s32StringNCompare(strName, pXMStationListItem->m_sChannelName, XM_CHANNEL_NAME_LEN) != 0)
                     {
                        enUpdate = XM_LIST_UPDATE_REQUIRED;
                     }
                  }
               }
               else
               {
                  enUpdate = XM_LIST_INSERT_REQUIRED;
               }
               sqlite3_finalize(m_pXMStmtHandle);
            }
         }
      }
   }
   return enUpdate;
}


enXMChannelUpdateType SXMDatabaseHandler::enGetXMPhoneticsUpdateType(SXMPhoneticListItem* pXMPhoneticListItem)
{
   enXMChannelUpdateType enUpdate = XM_LIST_NO_UPDATE_REQUIRED;
   if (OSAL_NULL != pXMPhoneticListItem)
   {
      if (OSAL_NULL != m_pXMStmtHandle)
      {
         tString szQuery    =   OSAL_NEW tChar[MAX_QUERY_LEN];
         if (OSAL_NULL != szQuery)
         {
            tU32 u32Ret                     =   SQLITE_ERROR;

            ::std::string format_query_get_phonemes_from_table = getPhonemesFromTable(pXMPhoneticListItem->m_u16LanguageID);
            OSAL_s32PrintFormat(szQuery, format_query_get_phonemes_from_table.c_str(), pXMPhoneticListItem->m_u16ChannelSID);
            u32Ret = u32PrepareXMSelectQuery(szQuery);
            OSAL_DELETE []szQuery;
            if (u32Ret == SQLITE_OK)
            {
               u32Ret = sqlite3_step(m_pXMStmtHandle);
               if (bIsDeletionRequired(u32Ret))
               {
                  m_blDeleteXMDatabase = true;
               }
               if (SQLITE_ROW == u32Ret)
               {
                  tString l_sPhoneticData = (tString)sqlite3_column_text(m_pXMStmtHandle, FIELD_0);
                  if (OSAL_s32StringNCompare(l_sPhoneticData, pXMPhoneticListItem->m_sPhoneticData, XM_CHANNEL_NAME_PHONETICS_LEN) != 0)
                  {
                     enUpdate = XM_LIST_UPDATE_REQUIRED;
                  }
               }
               else
               {
                  enUpdate = XM_LIST_INSERT_REQUIRED;
               }
               sqlite3_finalize(m_pXMStmtHandle);
            }
         }
      }
   }
   return enUpdate;
}


tVoid SXMDatabaseHandler::vUpdateTimeStamp()
{
   tU32 u32TimeStamp = u32GetCurrentTimeStamp();
   if (u32TimeStamp == INVALID_TIME_STAMP)
   {
      vDeleteAllFromXMTimeStampTbl();
      bInsertRecordForXMTimeStampTbl(XM_TIME_STAMP_ID, START_TIME_STAMP);
   }
   else
   {
      u32TimeStamp = u32TimeStamp + 1;
      if (u32TimeStamp == INVALID_TIME_STAMP)
      {
         u32TimeStamp = START_TIME_STAMP;
      }
      bUpdateRecordForXMTimeStampTbl(XM_TIME_STAMP_ID, u32TimeStamp);
   }
}


tBool SXMDatabaseHandler::bIsDeletionRequired(tU32 u32ErrorCode)
{
   tBool l_blRet = false;
   switch (u32ErrorCode)
   {
      case SQLITE_CANTOPEN:
      case SQLITE_SCHEMA:
      case SQLITE_MISMATCH:
      case SQLITE_AUTH:
      case SQLITE_FORMAT:
      case SQLITE_RANGE:
      case SQLITE_NOTADB:
      case SQLITE_CORRUPT:
      case SQLITE_FULL:
         l_blRet = true;
         ETG_TRACE_USR4(("SXMDatabaseHandler::bIsDeletionRequired(): Set to True u32ErrorCode = %d.", u32ErrorCode));
         break;
      default:
         break;
   }
   return l_blRet;
}


tU32 SXMDatabaseHandler::u32GetCurrentTimeStamp()
{
   tU32 u32Ret                     =   SQLITE_ERROR;
   tU32 u32TimeStamp = INVALID_TIME_STAMP;
   tString szQuery    =   OSAL_NEW tChar[MAX_QUERY_LEN];
   if (OSAL_NULL != szQuery)
   {
      OSAL_s32PrintFormat(szQuery, FORMAT_QUERY_GET_TIME_STAMP_FROM_ID_XM_TIME_STAMP_TBL, XM_TIME_STAMP_ID);
      u32Ret = u32PrepareXMSelectQuery(szQuery);
      if (u32Ret == SQLITE_OK)
      {
         u32Ret = u32ExecXMSelectQuery(u32TimeStamp);
         if (u32Ret != SQLITE_OK)
         {
            u32TimeStamp = INVALID_TIME_STAMP;
         }
      }
      OSAL_DELETE []szQuery;
   }
   return u32TimeStamp;
}


tBool SXMDatabaseHandler::bInsertRecordForXMTimeStampTbl(tU32 u32Id, tU32 u32TimeStamp)
{
   tBool blRet = false;
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != m_pXMDBhandle)
   {
      tString szQuery    =   OSAL_NEW tChar[MAX_QUERY_LEN];
      if (OSAL_NULL != szQuery)
      {
         OSAL_s32PrintFormat(szQuery, FORMAT_QUERY_INSERT_XM_TIME_STAMPTBL, u32Id, u32TimeStamp);
         u32Ret = sqlite3_exec(m_pXMDBhandle, szQuery, OSAL_NULL, OSAL_NULL, OSAL_NULL);
         if (bIsDeletionRequired(u32Ret))
         {
            m_blDeleteXMDatabase = true;
         }
         OSAL_DELETE []szQuery;
         if (u32Ret == SQLITE_OK)
         {
            blRet = true;
         }
      }
   }
   return blRet;
}


tBool SXMDatabaseHandler::bUpdateRecordForXMTimeStampTbl(tU32 u32Id, tU32 u32TimeStamp)
{
   tBool blRet = false;
   tU32 u32Ret                     =   SQLITE_ERROR;
   if (OSAL_NULL != m_pXMDBhandle)
   {
      tString szQuery    =   OSAL_NEW tChar[MAX_QUERY_LEN];
      if (OSAL_NULL != szQuery)
      {
         OSAL_s32PrintFormat(szQuery, FORMAT_QUERY_UPDATE_XM_TIME_STAMP_TBL, u32TimeStamp, u32Id);
         u32Ret = sqlite3_exec(m_pXMDBhandle, szQuery, OSAL_NULL, OSAL_NULL, OSAL_NULL);
         if (bIsDeletionRequired(u32Ret))
         {
            m_blDeleteXMDatabase = true;
         }
         OSAL_DELETE []szQuery;
         if (u32Ret == SQLITE_OK)
         {
            blRet = true;
         }
      }
   }
   return blRet;
}


tU32 SXMDatabaseHandler::u32CreateXMDB(tCString szDbFilePath)
{
   m_dbFilePath = szDbFilePath;

   ETG_TRACE_USR4(("SXMDatabaseHandler::u32CreateXMDB(): szDbFilePath = %s.", szDbFilePath));
   tU32 u32Ret = SQLITE_ERROR;
   u32Ret = sqlite3_open(szDbFilePath, &m_pXMDBhandle);
   if ((u32Ret != SQLITE_OK) || (OSAL_NULL == m_pXMDBhandle))
   {
      if (bIsDeletionRequired(u32Ret))
      {
         m_blDeleteXMDatabase = true;
      }
      sqlite3_close(m_pXMDBhandle);
      m_pXMDBhandle = OSAL_NULL;
      u32Ret = SQLITE_ERROR;
   }
   ETG_TRACE_USR4(("SXMDatabaseHandler::u32CreateXMDB(): u32Ret = %d.", u32Ret));
   return u32Ret;
}


tU32 SXMDatabaseHandler::u32CreateXMTables(tVoid)
{
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != m_pXMDBhandle)
   {
      // create XMStationList table
      u32Ret = sqlite3_exec(m_pXMDBhandle, QUERY_CREATE_XM_STATION_LIST_TABLE, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         if (bIsDeletionRequired(u32Ret))
         {
            m_blDeleteXMDatabase = true;
         }
         u32Ret = SQLITE_ERROR;
      }
      // create XMStationPhonetics_EN table
      u32Ret = sqlite3_exec(m_pXMDBhandle, QUERY_CREATE_XM_PHONETIC_LIST_TABLE_EN, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         if (bIsDeletionRequired(u32Ret))
         {
            m_blDeleteXMDatabase = true;
         }
         u32Ret = SQLITE_ERROR;
      }
      // create XMStationPhonetics_SP table
      u32Ret = sqlite3_exec(m_pXMDBhandle, QUERY_CREATE_XM_PHONETIC_LIST_TABLE_SP, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         if (bIsDeletionRequired(u32Ret))
         {
            m_blDeleteXMDatabase = true;
         }
         u32Ret = SQLITE_ERROR;
      }

      // create XMStationPhonetics_FR table
      u32Ret = sqlite3_exec(m_pXMDBhandle, QUERY_CREATE_XM_PHONETIC_LIST_TABLE_FR, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         if (bIsDeletionRequired(u32Ret))
         {
            m_blDeleteXMDatabase = true;
         }
         u32Ret = SQLITE_ERROR;
      }
      // create XMTimestamp table
      u32Ret = sqlite3_exec(m_pXMDBhandle, QUERY_CREATE_XM_TIME_STAMP_TABLE, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         if (bIsDeletionRequired(u32Ret))
         {
            m_blDeleteXMDatabase = true;
         }
         u32Ret = SQLITE_ERROR;
      }
   }
   ETG_TRACE_USR4(("SXMDatabaseHandler::u32CreateXMTables(): success = %d", u32Ret));
   return u32Ret;
}


tU32 SXMDatabaseHandler::u32CreateXMViews(tVoid)
{
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != m_pXMDBhandle)
   {
      u32Ret = sqlite3_exec(m_pXMDBhandle, QUERY_VIEW_XM_CHANNEL, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         if (bIsDeletionRequired(u32Ret))
         {
            m_blDeleteXMDatabase = true;
         }
         u32Ret = SQLITE_ERROR;
      }
      u32Ret = sqlite3_exec(m_pXMDBhandle, QUERY_VIEW_XM_CHECK_SUM, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         if (bIsDeletionRequired(u32Ret))
         {
            m_blDeleteXMDatabase = true;
         }
         u32Ret = SQLITE_ERROR;
      }
      u32Ret = sqlite3_exec(m_pXMDBhandle, QUERY_VIEW_XM_CHANNEL_PHONEMES, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         if (bIsDeletionRequired(u32Ret))
         {
            m_blDeleteXMDatabase = true;
         }
         u32Ret = SQLITE_ERROR;
      }
   }
   ETG_TRACE_USR4(("SXMDatabaseHandler::u32CreateXMViews(): success = %d", u32Ret));
   return u32Ret;
}


tVoid SXMDatabaseHandler::vProcessStringsForEscapeSequence(tCString strSource, tString strDest)
{
   if (OSAL_NULL != strSource && OSAL_NULL != strDest)
   {
      tU16 l_u16StrLength = OSAL_u32StringLength(strSource);
      if (l_u16StrLength >= STR_MAX)
      {
         l_u16StrLength = STR_MAX - 1;
      }
      if (OSAL_NULL != OSAL_ps8StringSearchChar(strSource, SQLITE_ESCAPE_SEQUENCE))
      {
         tU8 l_u8CntDest = 0;
         for (tU8 l_u8CntSrc = 0; l_u8CntSrc < l_u16StrLength; l_u8CntSrc++)
         {
            if (strSource[l_u8CntSrc] != SQLITE_ESCAPE_SEQUENCE)
            {
               strDest[l_u8CntDest++] = strSource[l_u8CntSrc];
            }
            else
            {
               strDest[l_u8CntDest++] = SQLITE_ESCAPE_SEQUENCE;
               strDest[l_u8CntDest++] = SQLITE_ESCAPE_SEQUENCE;
            }
         }
         strDest[l_u8CntDest] = 0;
      }
      else
      {
         if (OSAL_NULL == OSAL_szStringNCopy(strDest, strSource, l_u16StrLength))
         {
            ETG_TRACE_USR4(("SXMDatabaseHandler::vProcessStringsForEscapeSequence() Copy failed"));
         }
      }
   }
}


tU32 SXMDatabaseHandler::u32SetSynchronousModeOff(sqlite3* pDbHandle)
{
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != pDbHandle)
   {
      u32Ret = sqlite3_exec(pDbHandle, QUERY_PRAGAMA_SYNCHRONOUS_OFF, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         u32Ret = SQLITE_ERROR;
      }
   }
   return u32Ret;
}


tU32 SXMDatabaseHandler::u32SetJournalModeMemory(sqlite3* pDbHandle)
{
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != pDbHandle)
   {
      u32Ret = sqlite3_exec(pDbHandle, QUERY_PRAGAMA_JOURNAL_MODE_MEM, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         u32Ret = SQLITE_ERROR;
      }
   }
   return u32Ret;
}


tU32 SXMDatabaseHandler::u32BeginTransaction(sqlite3* pDbHandle)
{
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != pDbHandle)
   {
      u32Ret = sqlite3_exec(pDbHandle, QUERY_BEGIN_TRANSACTION, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         u32Ret = SQLITE_ERROR;
      }
   }
   return u32Ret;
}


tU32 SXMDatabaseHandler::u32EndTransaction(sqlite3* pDbHandle)
{
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != pDbHandle)
   {
      u32Ret = sqlite3_exec(pDbHandle, QUERY_END_TRANSACTION, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         u32Ret = SQLITE_ERROR;
      }
   }
   return u32Ret;
}


tU32 SXMDatabaseHandler::u32PrepareXMSelectQuery(tCString szQuery)
{
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != m_pXMDBhandle)
   {
      tCString    szSQLQueryTail;
      u32Ret = sqlite3_prepare_v2(
                  m_pXMDBhandle,					/* Database handle.                         */
                  szQuery,						/* SQL statement, UTF-8 encoded.            */
                  OSAL_u32StringLength(szQuery),	/* Maximum length of szQuery in bytes */
                  &m_pXMStmtHandle,				/* OUT: Statement handle.                   */
                  &szSQLQueryTail					/* OUT: Pointer to unused portion of zSql.  */
               );
      if ((u32Ret != SQLITE_OK) || (OSAL_NULL  == m_pXMStmtHandle))
      {
         if (bIsDeletionRequired(u32Ret))
         {
            m_blDeleteXMDatabase = true;
         }
      }
   }
   return u32Ret;
}


tU32 SXMDatabaseHandler::u32ExecXMSelectQuery(tU32&  rfu32ObjectId)
{
   tU32 u32Ret =  SQLITE_ERROR;
   if (OSAL_NULL != m_pXMStmtHandle)
   {
      u32Ret = sqlite3_step(m_pXMStmtHandle);
      if (bIsDeletionRequired(u32Ret))
      {
         m_blDeleteXMDatabase = true;
      }
      if (SQLITE_ROW == u32Ret)
      {
         rfu32ObjectId = (tU32)sqlite3_column_int(m_pXMStmtHandle,
                         FIELD_0);
      }
      u32Ret = sqlite3_finalize(m_pXMStmtHandle);
   }
   return u32Ret;
}


void SXMDatabaseHandler::vSetUpXMDatabase()
{
   ETG_TRACE_USR4(("SXMDatabaseHandler::vSetUpXMDatabase()"));
   tBool blFilePathExists = false;

   blFilePathExists = blIsFolderExists(SPEECH_XMTUNER_DATABASE_PATH_LOC);

   if (!blFilePathExists)
   {
      blFilePathExists = blMakePath(SPEECH_XMTUNER_DATABASE_PATH_LOC);
   }

   if (blFilePathExists)
   {
      tChar strStringPath[SPEECH_XMTUNER_DATABASE_PATH_LOC_SiZE] = SPEECH_XMTUNER_DATABASE_PATH_LOC;

      if (OSAL_NULL == OSAL_szStringNConcat(strStringPath, FILE_SYSTEM_SEPERATOR, FILE_SYSTEM_SEPERATOR_SIZE) ||
            OSAL_NULL == OSAL_szStringNConcat(strStringPath, SPEECH_XMTUNER_DATABASE_NAME, SPEECH_XMTUNER_DATABASE_NAME_SIZE))
      {
         ETG_TRACE_USR4(("SXMDatabaseHandler::vSetUpXMDatabase(): error when creating the database file path strStringPath = %s", strStringPath));
      }

      if (!blIsFileExists(strStringPath))
      {
         ETG_TRACE_USR4(("SXMDatabaseHandler::vSetUpXMDatabase():data base doesn't exist."));
         tU32 u32Ret = u32CreateXMDB(strStringPath);
         if ((SQLITE_ERROR != u32Ret) && (OSAL_NULL != m_pXMDBhandle))
         {
            u32Ret = u32SetSynchronousModeOff(m_pXMDBhandle);
            u32Ret = u32SetJournalModeMemory(m_pXMDBhandle);
            u32Ret = u32CreateXMTables();
            u32Ret = u32CreateXMViews();
            if (SQLITE_ERROR != u32Ret)
            {
               m_bIsXMDBExists = true;
               ETG_TRACE_USR4(("SXMDatabaseHandler::vSetUpXMDatabase():data base created successfully."));
            }
         }
      }
      else
      {
         ETG_TRACE_USR4(("SXMDatabaseHandler::vSetUpXMDatabase():data base exists."));
         tU32 u32Ret = u32CreateXMDB(strStringPath);
         if (SQLITE_ERROR != u32Ret)
         {
            u32CreateXMTables();
            u32CreateXMViews();
            m_bIsXMDBExists = true;
         }
      }
      if (m_bIsXMDBExists)
      {
         tU32 u32TimeStamp = u32GetCurrentTimeStamp();
         if (u32TimeStamp == INVALID_TIME_STAMP)
         {
            vDeleteAllFromXMTimeStampTbl();
            bInsertRecordForXMTimeStampTbl(XM_TIME_STAMP_ID, START_TIME_STAMP);
         }
      }
   }
   else
   {
      ETG_TRACE_USR4(("SXMDatabaseHandler::vSetUpXMDatabase():path for data base doesn't exist."));
   }
}
