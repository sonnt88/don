/*********************************************************************//**
 * \file       Sds2SxmDbService.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef Sds2SxmDbService_h
#define Sds2SxmDbService_h


#include "sds_sxm_fi/SdsSxmServiceStub.h"


class SXMDatabaseHandler;


class Sds2SxmDbService : public sds_sxm_fi::SdsSxmService::SdsSxmServiceStub
{
   public:
      Sds2SxmDbService();
      virtual ~Sds2SxmDbService();

      virtual void onStoreSXMChannelNamesRequest(const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::StoreSXMChannelNamesRequest >& request);
      virtual void onStoreSXMPhoneticDataRequest(const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::StoreSXMPhoneticDataRequest >& request);

   private:
      SXMDatabaseHandler* m_pSXMDatabaseHandler;
};


#endif
