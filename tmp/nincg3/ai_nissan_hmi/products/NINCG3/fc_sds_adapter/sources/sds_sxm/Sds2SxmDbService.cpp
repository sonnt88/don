/*********************************************************************//**
 * \file       Sds2SxmDbService.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "Sds2SxmDbService.h"
#include "SXMDatabaseHandler.h"


Sds2SxmDbService::Sds2SxmDbService() : SdsSxmServiceStub("SxmDbServicePort")
{
   m_pSXMDatabaseHandler = new SXMDatabaseHandler();
}


Sds2SxmDbService::~Sds2SxmDbService()
{
   if (m_pSXMDatabaseHandler != NULL)
   {
      delete m_pSXMDatabaseHandler;
      m_pSXMDatabaseHandler = NULL;
   }
}


void Sds2SxmDbService::onStoreSXMChannelNamesRequest(const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::StoreSXMChannelNamesRequest >& request)
{
   //sqlite update into database

   if (m_pSXMDatabaseHandler)
   {
      m_pSXMDatabaseHandler->openXMDatabase();

      if (m_pSXMDatabaseHandler->blBeginTransactionForXM())
      {
         bool updateTimestamp = false;

         std::vector< sds_sxm_fi::SdsSxmService::SXMChannelItem >::const_iterator it = request->getChannelsList().begin();
         while (it != request->getChannelsList().end())
         {
            SXMChannelListItem listItem(*it);

            enXMChannelUpdateType updateType = m_pSXMDatabaseHandler->enGetXMStationUpdateType(&listItem);

            if (updateType != XM_LIST_NO_UPDATE_REQUIRED)
            {
               updateTimestamp = true;

               m_pSXMDatabaseHandler->bUpdateRecordForXMStationList(&listItem);
            }

            ++it;
         }
         m_pSXMDatabaseHandler->blEndTransactionForXM();

         if (updateTimestamp)
         {
            m_pSXMDatabaseHandler->vUpdateTimeStamp();
         }

         sendStoreSXMChannelNamesResponse();
      }
      else
      {
         sendStoreSXMChannelNamesError(DBUS_ERROR_FAILED, "SQLITE begin transaction failed");
      }

      m_pSXMDatabaseHandler->closeXMDatabase();
   }
   else
   {
      sendStoreSXMChannelNamesError(DBUS_ERROR_FAILED, "m_pSXMDatabaseHandler is null");
   }
}


void Sds2SxmDbService::onStoreSXMPhoneticDataRequest(const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::StoreSXMPhoneticDataRequest >& request)
{
   if (m_pSXMDatabaseHandler)
   {
      m_pSXMDatabaseHandler->openXMDatabase();

      if (m_pSXMDatabaseHandler->blBeginTransactionForXM())
      {
         //save data into database
         bool updateTimestamp = false;

         std::vector< sds_sxm_fi::SdsSxmService::SXMPhoneticData >::const_iterator it = request->getPhonemesList().begin();
         while (it != request->getPhonemesList().end())
         {
            SXMPhoneticListItem phoneticItem(*it);

            enXMChannelUpdateType updateType = m_pSXMDatabaseHandler->enGetXMPhoneticsUpdateType(&phoneticItem);

            if (updateType != XM_LIST_NO_UPDATE_REQUIRED)
            {
               updateTimestamp = true;

               m_pSXMDatabaseHandler->bUpdateRecordForXMPhoneticList(&phoneticItem);
            }

            ++it;
         }

         m_pSXMDatabaseHandler->blEndTransactionForXM();

         if (updateTimestamp)
         {
            m_pSXMDatabaseHandler->vUpdateTimeStamp();
         }

         sendStoreSXMPhoneticDataResponse();
      }
      else
      {
         sendStoreSXMPhoneticDataError(DBUS_ERROR_FAILED, "SQLITE begin transaction failed");
      }

      m_pSXMDatabaseHandler->closeXMDatabase();
   }
   else
   {
      sendStoreSXMPhoneticDataError(DBUS_ERROR_FAILED, "m_pSXMDatabaseHandler is null");
   }
}
