/*********************************************************************//**
 * \file       SdsSxmDB.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "SdsSxmDB.h"
#include "Sds2SxmDbService.h"


namespace sds_sxm {


SdsSxmDB::SdsSxmDB(): BaseComponent()
{
   _sds2SxmDbService = new Sds2SxmDbService();
}


SdsSxmDB::~SdsSxmDB()
{
   delete _sds2SxmDbService;
   _sds2SxmDbService = NULL;
}


};
