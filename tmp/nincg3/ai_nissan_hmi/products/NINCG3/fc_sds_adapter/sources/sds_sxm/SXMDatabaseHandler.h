/*********************************************************************//**
 * \file       SXMDatabaseHandler.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef SXMDatabaseHandler_h
#define SXMDatabaseHandler_h


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "sds_sxm_fi/SdsSxmService.h"
#include "adapter/external/sds2hmi_fi.h"


#define XM_CHANNEL_NAME_LEN    (16+1)			//1 Extra char for NULL
#define XM_CHANNEL_NAME_LEN_EXT (32+1)			//1 Extra char for NULL
#define XM_CHANNEL_NAME_PHONETICS_LEN (255+1)	//1 Extra char for NULL

#define SAAL_OBJECTID_NOTFOUND       0xFFFF // If object ID is not found, 0xFFFF will be returned


struct sqlite3;
struct sqlite3_stmt;


enum enXMChannelUpdateType
{
   XM_LIST_NO_UPDATE_REQUIRED = 0UL,
   XM_LIST_UPDATE_REQUIRED = 1UL,
   XM_LIST_INSERT_REQUIRED = 2UL
};


class SXMChannelListItem
{
   public:
      tU16 m_u16ChannelSID;
      tU16 m_u16ChannelObjectID;
      tU16 m_u16ChannelNumber;
      tU8  m_sChannelName[XM_CHANNEL_NAME_LEN];
      SXMChannelListItem()
      {
         m_u16ChannelObjectID = SAAL_OBJECTID_NOTFOUND;
         m_u16ChannelSID = 0x00;
         m_u16ChannelNumber = 0x00;
         OSAL_pvMemorySet(m_sChannelName, 0, XM_CHANNEL_NAME_LEN);
      }
      SXMChannelListItem(const SXMChannelListItem& oItemXMList)
      {
         m_u16ChannelObjectID = oItemXMList.m_u16ChannelObjectID;
         m_u16ChannelNumber = oItemXMList.m_u16ChannelNumber;
         m_u16ChannelSID = oItemXMList.m_u16ChannelSID;
         if (OSAL_NULL != OSAL_szStringNCopy(m_sChannelName, oItemXMList.m_sChannelName, XM_CHANNEL_NAME_LEN))
         {
            m_sChannelName[XM_CHANNEL_NAME_LEN - 1 ] = '\0';
         }
      }
      SXMChannelListItem(const sds_sxm_fi::SdsSxmService::SXMChannelItem& oChannelListStreamItem)
      {
         m_u16ChannelObjectID = oChannelListStreamItem.getChannelID();
         m_u16ChannelNumber = oChannelListStreamItem.getChannelNumber();
         m_u16ChannelSID = oChannelListStreamItem.getServiceID();
//		if(OSAL_NULL != oChannelListStreamItem.sChannelName.szValue)
         if (OSAL_NULL != oChannelListStreamItem.getChannelName().c_str())
         {
            if (OSAL_NULL != OSAL_szStringNCopy(m_sChannelName, oChannelListStreamItem.getChannelName().c_str(), XM_CHANNEL_NAME_LEN))
            {
               m_sChannelName[XM_CHANNEL_NAME_LEN  - 1] = '\0';
            }
         }
         else
         {
            OSAL_pvMemorySet(m_sChannelName, 0, XM_CHANNEL_NAME_LEN);
         }
      }
};


class SXMPhoneticListItem
{
   public:
      tU16 m_u16LanguageID;
      tU16 m_u16ChannelSID;
      tU8  m_sPhoneticData[XM_CHANNEL_NAME_PHONETICS_LEN];
      SXMPhoneticListItem()
      {
         m_u16LanguageID = 0x00;
         m_u16ChannelSID = 0x00;
         OSAL_pvMemorySet(m_sPhoneticData, 0, XM_CHANNEL_NAME_PHONETICS_LEN);
      }
      SXMPhoneticListItem(const SXMPhoneticListItem& oItemXMList)
      {
         m_u16LanguageID = oItemXMList.m_u16LanguageID;
         m_u16ChannelSID = oItemXMList.m_u16ChannelSID;
         if (OSAL_NULL != OSAL_szStringNCopy(m_sPhoneticData, oItemXMList.m_sPhoneticData, XM_CHANNEL_NAME_PHONETICS_LEN))
         {
            m_sPhoneticData[XM_CHANNEL_NAME_PHONETICS_LEN - 1 ] = '\0';
         }
      }
      SXMPhoneticListItem(const sds_sxm_fi::SdsSxmService::SXMPhoneticData& oChannelListStreamItem)
      {
         m_u16LanguageID = oChannelListStreamItem.getLanguageID();
         m_u16ChannelSID = oChannelListStreamItem.getServiceID();
//		if(OSAL_NULL != oChannelListStreamItem.sChannelName.szValue)
         if (OSAL_NULL != oChannelListStreamItem.getPhoneticData().c_str())
         {
            if (OSAL_NULL != OSAL_szStringNCopy(m_sPhoneticData, oChannelListStreamItem.getPhoneticData().c_str(), XM_CHANNEL_NAME_PHONETICS_LEN))
            {
               m_sPhoneticData[XM_CHANNEL_NAME_PHONETICS_LEN  - 1] = '\0';
            }
         }
         else
         {
            OSAL_pvMemorySet(m_sPhoneticData, 0, XM_CHANNEL_NAME_PHONETICS_LEN);
         }
      }
};


class SXMDatabaseHandler
{
   public:
      SXMDatabaseHandler();
      virtual ~SXMDatabaseHandler();

      tBool blGetXMTunerDataBasePath(tString szFilePath);

      tVoid vDeleteAllFromXMStationList();
      tVoid vDeleteAllFromXMTimeStampTbl();

      tU32 u32GetNoOfRecordsInXMDatabase();

      tBool blBeginTransactionForXM(tVoid);
      tBool blEndTransactionForXM(tVoid);

      tBool bUpdateRecordForXMStationList(SXMChannelListItem* pXMStationListItem);
      tBool bUpdateRecordForXMPhoneticList(SXMPhoneticListItem* pXMPhoneticListItem);

      tU32 u32FindXMStationObjectId(tU16 u16ChannelNumber);
      tU32 u32FindXMStationObjectId(tCString sChannelName);
      enXMChannelUpdateType enGetXMStationUpdateType(SXMChannelListItem* pXMStationListItem);
      enXMChannelUpdateType enGetXMPhoneticsUpdateType(SXMPhoneticListItem* pXMPhoneticListItem);
      tVoid vUpdateTimeStamp();

      void closeXMDatabase();
      void openXMDatabase();

   private:
      tBool blIsFolderExists(tCString szFilePath);
      tBool blIsFileExists(tCString szFilePath);
      tBool bCreateFolder(tCString szFilePath);
      tBool blMakePath(tCString strPath);
      tBool bDeleteFile(tCString strPath);

      std::string getFormatQueryUpdateXMPhoneticList(tU16 language_code);
      std::string getPhonemesFromTable(tU16 language_code);

      tBool bIsDeletionRequired(tU32 u32ErrorCode);
      tU32 u32GetCurrentTimeStamp();
      tBool bInsertRecordForXMTimeStampTbl(tU32 u32Id, tU32 u32TimeStamp);
      tBool bUpdateRecordForXMTimeStampTbl(tU32 u32Id, tU32 u32TimeStamp);

      tU32 u32CreateXMDB(tCString szDbFilePath);

      tU32 u32CreateXMTables(tVoid);

      tU32 u32CreateXMViews(tVoid);

      tVoid vProcessStringsForEscapeSequence(tCString strSource, tString strDest);

      tU32 u32SetSynchronousModeOff(sqlite3* pDbHandle);
      tU32 u32SetJournalModeMemory(sqlite3* pDbHandle);
      tU32 u32BeginTransaction(sqlite3* pDbHandle);
      tU32 u32EndTransaction(sqlite3* pDbHandle);

      tU32 u32PrepareXMSelectQuery(tCString szQuery);

      tU32 u32ExecXMSelectQuery(tU32&  rfu32ObjectId);

      void vSetUpXMDatabase();

      tBool m_bIsXMDBExists;

      tBool m_blDeleteXMDatabase;

      sqlite3*        m_pXMDBhandle;                              // Database handle.
      sqlite3_stmt*   m_pXMStmtHandle;                            // Query statement handle.

      tCString m_dbFilePath;
};


#endif
