/*********************************************************************//**
 * \file       sds_sxm_query_defines.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef sds_sxm_query_defines_h
#define sds_sxm_query_defines_h


/*****************************************************************************/
/* SXM specific DEFINES                                                                   */
/*****************************************************************************/

#define FILE_SYSTEM_SEPERATOR						"/"
#define FILE_SYSTEM_SEPERATOR_SIZE					1

#define SPEECH_XMTUNER_DATABASE_NAME              "speech_XM_tuner_data.db"
#define SPEECH_XMTUNER_DATABASE_NAME_SIZE			23

#define SPEECH_XMTUNER_DATABASE_OSAL_PATH           "/dev/ffs2/dynamic/speech/radio/speech_XM_tuner_data.db"

#define SPEECH_XMTUNER_DATABASE_PATH_LOC            "/var/opt/bosch/dynamic/speech/radio"
#define SPEECH_XMTUNER_DATABASE_PATH_LOC_SiZE		59

#define XM_STATION_LIST_TABLEX             "XMStationList"
#define XM_PHONETICS_LIST_TABLEX           "XMStationPhonetics"
#define XM_TIME_STAMP_TABLEX               "XMTimestamp"

#define XM_TIME_STAMP_ID        0x01
#define INVALID_TIME_STAMP      0xFFFFFFFF
#define START_TIME_STAMP        0x01


#define QUERY_CREATE_XM_STATION_LIST_TABLE      "CREATE TABLE XMStationList( channel_object_id INTEGER, sid INTEGER PRIMARY KEY, channel_number INTEGER, channel_name CHAR(100));"
#define QUERY_CREATE_XM_TIME_STAMP_TABLE        "CREATE TABLE XMTimestamp( unique_id INTEGER PRIMARY KEY, time_stamp INTEGER);"

#define QUERY_CREATE_XM_PHONETIC_LIST_TABLE_EN     "CREATE TABLE XMStationPhonetics_EN( language_id INTEGER, sid INTEGER PRIMARY KEY, channel_phonemes CHAR(500));"
#define QUERY_CREATE_XM_PHONETIC_LIST_TABLE_SP     "CREATE TABLE XMStationPhonetics_SP( language_id INTEGER, sid INTEGER PRIMARY KEY, channel_phonemes CHAR(500));"
#define QUERY_CREATE_XM_PHONETIC_LIST_TABLE_FR     "CREATE TABLE XMStationPhonetics_FR( language_id INTEGER, sid INTEGER PRIMARY KEY, channel_phonemes CHAR(500));"


//#define FORMAT_QUERY_INSERT_XM_STATION_LIST	"INSERT INTO XMStationList VALUES  ('%lu', '%lu', '%u', '%s');"
#define FORMAT_QUERY_INSERT_XM_TIME_STAMPTBL	"INSERT INTO XMTimestamp VALUES    ('%lu', '%lu');"
#define FORMAT_QUERY_UPDATE_XM_TIME_STAMP_TBL	"UPDATE XMTimestamp SET time_stamp ='%lu'WHERE unique_id='%lu';"

//#define FORMAT_QUERY_UPDATE_XM_STATION_LIST	"UPDATE XMStationList SET channel_object_id ='%lu', channel_number='%u', channel_name='%s' WHERE sid='%lu';"
#define FORMAT_QUERY_UPDATE_XM_STATION_LIST		"INSERT OR REPLACE INTO  XMStationList (sid, channel_object_id, channel_number, channel_name) VALUES ('%lu', '%lu', '%lu', '%s');"

#define FORMAT_QUERY_UPDATE_XM_PHONETIC_LIST_EN		"INSERT OR REPLACE INTO  XMStationPhonetics_EN (language_id, sid, channel_phonemes) VALUES ('%lu', '%lu', '%s');"
#define FORMAT_QUERY_UPDATE_XM_PHONETIC_LIST_SP		"INSERT OR REPLACE INTO  XMStationPhonetics_SP (language_id, sid, channel_phonemes) VALUES ('%lu', '%lu', '%s');"
#define FORMAT_QUERY_UPDATE_XM_PHONETIC_LIST_FR		"INSERT OR REPLACE INTO  XMStationPhonetics_FR (language_id, sid, channel_phonemes) VALUES ('%lu', '%lu', '%s');"


#define FORMAT_QUERY_GET_OBJECT_ID_FROM_CHANNEL_NUM_XM_STATION_LIST             "SELECT channel_object_id FROM XMStationList WHERE channel_number='%u';"
#define FORMAT_QUERY_GET_OBJECT_ID_FROM_CHANNEL_NAME_XM_STATION_LIST            "SELECT channel_object_id FROM XMStationList WHERE channel_name='%s';"
#define FORMAT_QUERY_GET_CHANNEL_NAME_FROM_CHANNEL_SID_XM_STATION_LIST          "SELECT channel_name,channel_number FROM XMStationList WHERE sid='%lu';"

#define FORMAT_QUERY_GET_PHONEMES_FROM_TABLE_EN          "SELECT channel_phonemes FROM XMStationPhonetics_EN WHERE sid='%lu';"
#define FORMAT_QUERY_GET_PHONEMES_FROM_TABLE_FR          "SELECT channel_phonemes FROM XMStationPhonetics_SP WHERE sid='%lu';"
#define FORMAT_QUERY_GET_PHONEMES_FROM_TABLE_SP          "SELECT channel_phonemes FROM XMStationPhonetics_FR WHERE sid='%lu';"


#define FORMAT_QUERY_GET_TIME_STAMP_FROM_ID_XM_TIME_STAMP_TBL                   "SELECT time_stamp FROM XMTimestamp WHERE unique_id='%lu';"


#define QUERY_DELETE_ALL_DATA_XM_STATION_LIST            "delete from XMStationList;"
#define QUERY_DELETE_ALL_DATA_XM_TIME_STAMPTBL           "delete from XMTimestamp;"


#define QUERY_VIEW_XM_CHANNEL                            "CREATE VIEW XMChannel AS  SELECT channel_name AS XMChannelname,channel_object_id AS XMChannelID FROM XMStationList WHERE channel_name != '';"
#define QUERY_VIEW_XM_CHECK_SUM                          "CREATE VIEW XM_Checksum AS SELECT time_stamp AS Checksum FROM XMTimestamp;"
#define QUERY_VIEW_XM_CHANNEL_PHONEMES                   "CREATE VIEW XM_StationPhonetics AS \
														  SELECT language_id, XMStationList.channel_object_id AS channel_id, channel_phonemes, channel_name \
														  FROM XMStationPhonetics_EN INNER JOIN XMStationList \
														  ON XMStationPhonetics_EN.sid = XMStationList.sid	\
														  UNION \
														  SELECT language_id, XMStationList.channel_object_id AS channel_id, channel_phonemes, channel_name \
														  FROM XMStationPhonetics_SP INNER JOIN XMStationList \
														  ON XMStationPhonetics_SP.sid = XMStationList.sid \
														  UNION \
														  SELECT language_id, XMStationList.channel_object_id AS channel_id, channel_phonemes, channel_name \
														  FROM XMStationPhonetics_FR INNER JOIN XMStationList \
														  ON XMStationPhonetics_FR.sid = XMStationList.sid"


#define QUERY_ROW_COUNT_XM_TABLE                         "SELECT COUNT(*) FROM XMStationList;"

#define QUERY_PRAGAMA_JOURNAL_MODE_MEM				     "PRAGMA journal_mode = MEMORY;"

#define QUERY_PRAGAMA_SYNCHRONOUS_OFF					 "PRAGMA synchronous = OFF;"

#define QUERY_BEGIN_TRANSACTION							 "BEGIN TRANSACTION;"

#define QUERY_END_TRANSACTION							 "END TRANSACTION;"

#define SQLITE_ESCAPE_SEQUENCE   39		//Ascii value for ' is 39

#define MAX_QUERY_LEN       1024 /* = 9 KB. Note: Normally 8 KB is required but for SAAL 1KB is sufficient
for blob data. */

#define STR_MAX 500 // TODO: check this value

#define FIELD_0                             0
#define FIELD_1                             1

#endif /* sds_sxm_query_defines_h */
