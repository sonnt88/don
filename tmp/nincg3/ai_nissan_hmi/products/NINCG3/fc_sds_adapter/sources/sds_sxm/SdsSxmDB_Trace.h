#ifndef SDSSXMDB_TRACE_H
#define SDSSXMDB_TRACE_H


#define ETG_S_IMPORT_INTERFACE_GENERIC
#include "etg_if.h"


// trace classes based on TR_COMP_SAAL (256 * 70) = 0x4600 - see mc_trace.h

#define TR_CLASS_SDSSXMDB			0x4620


#endif /* SDSSXMDB_TRACE_H */
