/*********************************************************************//**
 * \file       clSDS_Method_TunerGetDataBases.cpp
 *
 * clSDS_Method_TunerGetDataBases method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_TunerGetDataBases.h"
#include "external/sds2hmi_fi.h"
#include "sds_sxm/sds_sxm_query_defines.h"
#include "sds_fm/sds_fm_query_defines.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_TunerGetDataBases::~clSDS_Method_TunerGetDataBases()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_TunerGetDataBases::clSDS_Method_TunerGetDataBases(ahl_tclBaseOneThreadService* pService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_TUNERGETDATABASES, pService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_TunerGetDataBases::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   sds2hmi_sdsfi_tclMsgTunerGetDatabasesMethodResult oResult;

   sds2hmi_fi_tcl_DeviceDatabase oDeviceDataBaseXM;
   oDeviceDataBaseXM.DeviceID = sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_XM;
   sds2hmi_fi_tclString oStringXM;
   oStringXM.bSet(SPEECH_XMTUNER_DATABASE_OSAL_PATH, sds2hmi_fi_tclString::FI_EN_UTF8);
   oDeviceDataBaseXM.Database.push_back(oStringXM);
   oResult.Devices.push_back(oDeviceDataBaseXM);

   // RDS
   sds2hmi_fi_tcl_DeviceDatabase oDeviceDataBaseFM;
   oDeviceDataBaseFM.DeviceID = 8;
   sds2hmi_fi_tclString oStringFM;
   oStringFM.bSet(SPEECH_FMRDSTUNER_DATABASE_PATH_LOC, sds2hmi_fi_tclString::FI_EN_UTF8);
   oDeviceDataBaseFM.Database.push_back(oStringFM);
   oResult.Devices.push_back(oDeviceDataBaseFM);

   sds2hmi_fi_tcl_DeviceDatabase oDeviceDataBaseHDFM;
   oDeviceDataBaseHDFM.DeviceID = sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_HD_FM;
   sds2hmi_fi_tclString oStringHDFM;
   oStringHDFM.bSet(SPEECH_FMHDTUNER_DATABASE_PATH_LOC, sds2hmi_fi_tclString::FI_EN_UTF8);
   oDeviceDataBaseHDFM.Database.push_back(oStringHDFM);
   oResult.Devices.push_back(oDeviceDataBaseHDFM);

   sds2hmi_fi_tcl_DeviceDatabase oDeviceDataBaseHDAM;
   oDeviceDataBaseHDAM.DeviceID = sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_HD_AM;
   sds2hmi_fi_tclString oStringHDAM;
   oStringHDAM.bSet("/dev/ffs/sds/TUNERDB.DB", sds2hmi_fi_tclString::FI_EN_UTF8);
   oDeviceDataBaseHDAM.Database.push_back(oStringHDAM);
   oResult.Devices.push_back(oDeviceDataBaseHDAM);

   vSendMethodResult(oResult);
}
