/*
 * clSDS_Property_CommonCoreSpeechParameters.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 */

#ifndef clSDS_Property_CommonCoreSpeechParameters_h
#define clSDS_Property_CommonCoreSpeechParameters_h


#include "Sds2HmiServer/framework/clServerProperty.h"
#include "application/GuiService.h"

class clSDS_Property_CommonCoreSpeechParameters
   : public clServerProperty
{
   public:
      virtual ~clSDS_Property_CommonCoreSpeechParameters();
      clSDS_Property_CommonCoreSpeechParameters(ahl_tclBaseOneThreadService* pService,
            GuiService& guiService);

   protected:
      virtual tVoid vGet(amt_tclServiceData* pInMsg);
      virtual tVoid vSet(amt_tclServiceData* pInMsg);
      virtual tVoid vUpreg(amt_tclServiceData* pInMsg);

   private:
      GuiService& _guiService;
};


#endif
