/*********************************************************************//**
 * \file       clSDS_Method_MediaGetDeviceInfo.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_MediaGetDeviceInfo_h
#define clSDS_Method_MediaGetDeviceInfo_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "mplay_MediaPlayer_FIProxy.h"

class clSDS_Method_MediaGetDeviceInfo
   : public clServerMethod
   , public mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsCallbackIF
   , public mplay_MediaPlayer_FI::CreateMediaPlayerCDListCallbackIF
   , public asf::core::ServiceAvailableIF
{
   public:
      virtual ~clSDS_Method_MediaGetDeviceInfo();
      clSDS_Method_MediaGetDeviceInfo(ahl_tclBaseOneThreadService* pService,
                                      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy > pMediaPlayerProxy);

      virtual void onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                               const asf::core::ServiceStateChange& stateChange) ;

      virtual void onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                                 const asf::core::ServiceStateChange& stateChange) ;

      virtual void onMediaPlayerDeviceConnectionsError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
            const ::boost::shared_ptr< mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsError >& error);

      virtual void onMediaPlayerDeviceConnectionsStatus(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
            const ::boost::shared_ptr< mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsStatus >& status);

      virtual void onCreateMediaPlayerCDListError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
            const ::boost::shared_ptr< mplay_MediaPlayer_FI::CreateMediaPlayerCDListError >& error);

      virtual void onCreateMediaPlayerCDListResult(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
            const ::boost::shared_ptr< mplay_MediaPlayer_FI::CreateMediaPlayerCDListResult >& result);

      unsigned int getListHandle() const;

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      void getMediaPlayerCDListInfo();
      uint8 getDeviceTag() const;
      void sendResult();

      boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy > _mediaPlayerProxy;
      unsigned int _listHandle;
      unsigned int _listSize;
};


#endif
