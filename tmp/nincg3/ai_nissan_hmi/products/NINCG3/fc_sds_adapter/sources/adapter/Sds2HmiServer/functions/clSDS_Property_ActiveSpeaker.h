/*********************************************************************//**
 * \file       clSDS_Property_ActiveSpeaker.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Property_ActiveSpeaker_h
#define clSDS_Property_ActiveSpeaker_h


#include "Sds2HmiServer/framework/clServerProperty.h"


class clSDS_LanguageMediator;


class clSDS_Property_ActiveSpeaker : public clServerProperty
{
   public:
      virtual ~clSDS_Property_ActiveSpeaker();
      clSDS_Property_ActiveSpeaker(ahl_tclBaseOneThreadService* pService, clSDS_LanguageMediator* pLanguageMediator);

   protected:
      virtual tVoid vGet(amt_tclServiceData* pInMsg);
      virtual tVoid vSet(amt_tclServiceData* pInMsg);
      virtual tVoid vUpreg(amt_tclServiceData* pInMsg);

   private:
      clSDS_LanguageMediator* _pLanguageMediator;
};


#endif
