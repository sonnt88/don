/*********************************************************************//**
 * \file       GuiService.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef GuiService_h
#define GuiService_h

#include "sds_gui_fi/SdsGuiServiceStub.h"


class clSDS_SessionControl;
class clSDS_MenuControl;
class EarlyStartupPlayer;


class GuiService :  public sds_gui_fi::SdsGuiService::SdsGuiServiceStub
{
   public:
      GuiService(clSDS_SessionControl* _pSessionControl, EarlyStartupPlayer* pEarlyStartupPlayer);

      virtual ~GuiService();

      virtual void onPttPressRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::PttPressRequest >& request);
      void vPttShortPressed();

      virtual void onManualOperationRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::ManualOperationRequest >& request);

      virtual void onStartSessionContextRequest(const ::boost::shared_ptr<  sds_gui_fi::SdsGuiService::StartSessionContextRequest >& request);

      virtual void onTestModeUpdateRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::TestModeUpdateRequest >& request);

      virtual void onSettingsCommandRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SettingsCommandRequest >& request);

      virtual void onAbortSessionRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::AbortSessionRequest >& request);

      virtual void onStopSessionRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StopSessionRequest >& request);

      virtual void onPauseSessionRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::PauseSessionRequest >& request);

      virtual void onResumeSessionRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::ResumeSessionRequest >& request);

      virtual void onBackPressRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::BackPressRequest >& request);

      virtual void onStartEarlyHandlingRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StartEarlyHandlingRequest >& request);

      virtual void onStopEarlyHandlingRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StopEarlyHandlingRequest >& request);

      virtual void onHelpCommandRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::HelpCommandRequest >& request);

      void setMenuControl(clSDS_MenuControl* pMenuControl);

      /**
       * enum ToggleType - Holds the definition values of Toggle Types.
       */
      enum ToggleType
      {
         ToggleType_OFF           = 0,
         ToggleType_ON            = 1,
         ToggleType_UNDEFINED     = 2
      };

   private:
      clSDS_SessionControl* _pSessionControl;
      clSDS_MenuControl* _pMenuControl;
      EarlyStartupPlayer* _pEarlyStartupPlayer;

      bool bIsPttPressAllowed() const;
};


#endif
