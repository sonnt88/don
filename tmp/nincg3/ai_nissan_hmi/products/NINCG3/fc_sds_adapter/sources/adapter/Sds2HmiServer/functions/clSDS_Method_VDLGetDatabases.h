/*********************************************************************//**
 * \file       clSDS_Method_VDLGetDatabases.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_VDLGetDatabases_h
#define clSDS_Method_VDLGetDatabases_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"


class clSDS_Method_VDLGetDatabases : public clServerMethod
{
   public:
      virtual ~clSDS_Method_VDLGetDatabases();
      clSDS_Method_VDLGetDatabases(ahl_tclBaseOneThreadService* pService);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
};


#endif
