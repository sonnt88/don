/*********************************************************************//**
 * \file       clSDS_Method_NaviStartGuidance.cpp
 *
 * clSDS_Method_NaviStartGuidance method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_NaviStartGuidance.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviSelectDestListEntry.h"
#include "Sds2HmiServer/functions/clSDS_Method_CommonGetListInfo.h"
#include "application/clSDS_NaviListItems.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_NaviStartGuidance::~clSDS_Method_NaviStartGuidance()
{
   _pNaviListItems = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_NaviStartGuidance::clSDS_Method_NaviStartGuidance(ahl_tclBaseOneThreadService* pService ,
      GuiService& guiService,
      clSDS_NaviListItems* pNaviListItems,
      ::boost::shared_ptr<NavigationServiceProxy> pNaviProxy)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_NAVISTARTGUIDANCE, pService),
     _pNaviListItems(pNaviListItems),
     _guiService(guiService),
     _navigationProxy(pNaviProxy)

{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviStartGuidance::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   if (_pNaviListItems != NULL)
   {
      clSDS_NaviListItems::enNaviListType nListType = _pNaviListItems->vGetNaviListType();
      switch (nListType)
      {
         case clSDS_NaviListItems::EN_NAVI_LIST_OSDE_ADDRESSES:
         {
            _navigationProxy->sendStartGuidanceRequest(*this);
            _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
         }
         break;
         case clSDS_NaviListItems::EN_NAVI_LIST_HOME_DESTINATION:
         {
            _navigationProxy->sendStartGuidanceToHomeLocationRequest(*this);
            _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
         }
         break;
         case clSDS_NaviListItems::EN_NAVI_LIST_WORK_DESTINATION:
         {
            _navigationProxy->sendStartGuidanceToWorkLocationRequest(*this);
            _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
         }
         break;
         case clSDS_NaviListItems::EN_NAVI_LIST_PREV_DESTINATIONS:
         case clSDS_NaviListItems::EN_NAVI_LIST__ADDRESSBOOK:
         {
            _pNaviListItems->vStartGuidance();
            _navigationProxy->sendStartGuidanceRequest(*this);
            _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
         }
         break;
         case clSDS_NaviListItems::EN_NAVI_LIST_FUEL_PRICES:
         {
            _pNaviListItems->vStartGuidance();
            _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
         }
         break;
         default:
            break;
      }
      _pNaviListItems->vResetNaviListType();
   }

   vSendMethodResult();
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviStartGuidance::onStartGuidanceError(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< StartGuidanceError >& /*error*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviStartGuidance::onStartGuidanceResponse(const ::boost::shared_ptr< NavigationServiceProxy >&/* proxy*/,
      const ::boost::shared_ptr< StartGuidanceResponse >& /*response*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviStartGuidance::onStartGuidanceToHomeLocationError(const ::boost::shared_ptr< NavigationServiceProxy >& /* proxy */,
      const ::boost::shared_ptr< StartGuidanceToHomeLocationError >& /* error */)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviStartGuidance::onStartGuidanceToHomeLocationResponse(const ::boost::shared_ptr< NavigationServiceProxy >& /* proxy */,
      const ::boost::shared_ptr< StartGuidanceToHomeLocationResponse >& /* response */)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviStartGuidance::onStartGuidanceToWorkLocationError(const ::boost::shared_ptr< NavigationServiceProxy >& /* proxy */,
      const ::boost::shared_ptr< StartGuidanceToWorkLocationError >& /* error */)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviStartGuidance::onStartGuidanceToWorkLocationResponse(const ::boost::shared_ptr< NavigationServiceProxy >& /* proxy */,
      const ::boost::shared_ptr< StartGuidanceToWorkLocationResponse >& /* response */)
{
}
