/*********************************************************************//**
 * \file       clSDS_Method_TextMsgSetNumber.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_TextMsgSetNumber_h
#define clSDS_Method_TextMsgSetNumber_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "MOST_PhonBk_FIProxy.h"


class clSDS_ListScreen;
class clSDS_PhonebookList;
class clSDS_Userwords;
class clSDS_Userwords;
class clSDS_QuickDialList;


class clSDS_Method_TextMsgSetNumber
   : public clServerMethod
   , public MOST_PhonBk_FI::GetContactDetailsExtendedCallbackIF
{
   public:
      virtual ~clSDS_Method_TextMsgSetNumber();
      clSDS_Method_TextMsgSetNumber(
         ahl_tclBaseOneThreadService* pService,
         clSDS_ListScreen* pListScreen,
         clSDS_PhonebookList* pPhonebookList,
         clSDS_Userwords* pUserwords,
         ::boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy> phonebookProxy,
         clSDS_QuickDialList* pQuickDialList);

      virtual void onGetContactDetailsExtendedError(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedError >& error);
      virtual void onGetContactDetailsExtendedResult(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedResult >& result);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      tVoid vSetSMSTargetNumberByContact(tU32 u32Index);
      tVoid vSetSMSTargetNumberByValue(tCString strPhoneNumber) const;
      tVoid vSaveCurrentNumberForCallBack(tCString strPhoneNumber) const;
      tVoid vSetSMSTargetNumberByUWID(tU32 u32UwId);
      void setMessageString(std::string strNumber) const;

      boost::shared_ptr<MOST_PhonBk_FI::MOST_PhonBk_FIProxy> _phonebookProxy;
      clSDS_PhonebookList* _pPhonebookList;
      clSDS_ListScreen* _pListScreen;
      clSDS_Userwords* _pUserwords;
      clSDS_QuickDialList* _pQuickDialList;
};


#endif
