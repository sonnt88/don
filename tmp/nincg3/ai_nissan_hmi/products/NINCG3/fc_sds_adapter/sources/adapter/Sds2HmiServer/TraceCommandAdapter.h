/*********************************************************************//**
 * \file       TraceCommandAdapter.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef TraceCommandAdapter_h
#define TraceCommandAdapter_h


class GuiService;
class PopUpService;
class EarlyStartupPlayer;


class TraceCommandAdapter
{
   public:
      TraceCommandAdapter(); // declare without implementation - for solving LINT warning
      TraceCommandAdapter(GuiService* guiService, PopUpService* popUpService, EarlyStartupPlayer* earlyStartupPlayer);
      virtual ~TraceCommandAdapter();

      static void pttShortPress();
      static void startAudio();
      static void stopAudio();
      static void setMicrophoneLevel(unsigned int level);
      static void commandNotProcessed(const unsigned char* data);

   private:
      static GuiService* _pGuiService;
      static PopUpService* _pPopUpService;
      static EarlyStartupPlayer* _pEarlyStartupPlayer;
};


#endif
