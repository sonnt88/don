/*********************************************************************//**
 * \file       clSDS_Method_CommonSetActiveApplication.cpp
 *
 * clSDS_Method_CommonSetActiveApplication method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_CommonSetActiveApplication.h"

/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_CommonSetActiveApplication::~clSDS_Method_CommonSetActiveApplication()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_CommonSetActiveApplication::clSDS_Method_CommonSetActiveApplication(ahl_tclBaseOneThreadService* pService, GuiService& guiService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_COMMONSETACTIVEAPPLICATION, pService), _guiService(guiService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_CommonSetActiveApplication::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_VR_SETTINGS);
   vSendMethodResult();
}
