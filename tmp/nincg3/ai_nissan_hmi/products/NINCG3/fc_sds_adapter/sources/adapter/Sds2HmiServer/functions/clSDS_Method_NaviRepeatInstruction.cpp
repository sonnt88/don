/*********************************************************************//**
 * \file       clSDS_Method_NaviRepeatInstruction.cpp
 *
 * clSDS_Method_NaviRepeatInstruction method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_NaviRepeatInstruction.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_NaviRepeatInstruction::~clSDS_Method_NaviRepeatInstruction()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_NaviRepeatInstruction::clSDS_Method_NaviRepeatInstruction(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr<NavigationServiceProxy> pNaviProxy, GuiService& guiService)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_NAVIREPEATINSTRUCTION, pService)
   , _navigationProxy(pNaviProxy)
   , _guiService(guiService)
{
}


/**************************************************************************//**

******************************************************************************/
void clSDS_Method_NaviRepeatInstruction::onRetriggerAcousticOutputError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< RetriggerAcousticOutputError >& /*error*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviRepeatInstruction::onRetriggerAcousticOutputResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< RetriggerAcousticOutputResponse >& /*response*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviRepeatInstruction::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   _navigationProxy->sendRetriggerAcousticOutputRequest(*this);
   _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_PASSIVE);
   vSendMethodResult();
}
