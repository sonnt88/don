/*********************************************************************//**
 * \file       clSDS_KDSConfiguration.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef clSDS_KDSConfiguration_h
#define clSDS_KDSConfiguration_h


#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"


class clSDS_KDSConfiguration
{
   public:
      enum Country
      {
         USA,
         CAN,
         MEX,
         UK ,
         TKY,
         RUS,
         OTHER_EUR,
         PRC,
         TWN,
         HKG_MACAU,
         GCC,
         EGP,
         ASR_NZR,
         BRA,
         AGT,
         OTHER_LAC,
         SAF,
         THI,
         SGP,
         MLY,
         BRN,
         INN,
         VNM,
         PHL,
         IND,
         JPN,
         KOR,
         OTHER_GOM
      };

      enum Variant
      {
         RENAULT,
         NISSAN ,
         INFINITI,
         EV_NISSAN
      };

      enum AllianceVoiceRecognitionModeType
      {
         OFF,
         BEGINNER,
         EXPERT
      };

      enum VehicleDistanceUnitType
      {
         EN_DIST_US_MILES_AND_KM,
         EN_DIST_KM_ONLY,
         EN_DIST_UK_MILES_AND_KM,
         EN_DIST_RESERVED
      };

      clSDS_KDSConfiguration();
      virtual ~clSDS_KDSConfiguration();

      static unsigned char getKDSConfigGeneric(const std::string& strConfigElement, const std::string& strConfigItem, unsigned char configValue = 0x00);

      static Country getMarketRegionType();
      static Variant getOpeningAnimationType();

      static unsigned char getDABMounted();
      static unsigned char getXMTunerMounted();
      static unsigned char getHEVInformationFunction();
      static unsigned char getBluetoothPhoneEnabled();

      static AllianceVoiceRecognitionModeType getAllianceVRModeType();
      static unsigned char getVRBeep();
      static unsigned char getVRShortPrompts();
      static unsigned char getVehicleEngineType();
      static VehicleDistanceUnitType getVehicleDistanceUnitsType();
};


#endif
