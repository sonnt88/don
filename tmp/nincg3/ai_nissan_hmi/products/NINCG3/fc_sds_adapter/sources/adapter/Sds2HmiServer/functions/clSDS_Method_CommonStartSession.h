/*********************************************************************//**
 * \file       clSDS_Method_CommonStartSession.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_CommonStartSession_h
#define clSDS_Method_CommonStartSession_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "application/GuiService.h"
#include "application/SdsAudioSourceObserver.h"


class SdsAudioSource;


class clSDS_Method_CommonStartSession
   : public clServerMethod
   , public SdsAudioSourceObserver
{
   public:
      virtual ~clSDS_Method_CommonStartSession();
      clSDS_Method_CommonStartSession(
         ahl_tclBaseOneThreadService* pService,
         SdsAudioSource& audioSource,
         GuiService& guiService);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      virtual void onAudioSourceStateChanged(arl_tenActivity state);

      SdsAudioSource& _audioSource;
      GuiService& _guiService;
      bool _waitingOnAudioActivity;
};


#endif
