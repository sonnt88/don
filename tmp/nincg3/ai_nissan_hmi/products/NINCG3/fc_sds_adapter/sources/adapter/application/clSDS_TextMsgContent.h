/*********************************************************************//**
 * \file       clSDS_TextMsgContent.h
 *
 * See .cpp file for description.
 * \author   heg6kor
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_TextMsgContent_h
#define clSDS_TextMsgContent_h

#include<string>

class clSDS_TextMsgContent
{
   public:
      clSDS_TextMsgContent();
      ~clSDS_TextMsgContent();
      static void setTextMsgContent(const std::string msgContent);
      static void setPhoneNumber(const std::string phoneNumber);
      static std::string getTextMsgContent();
      static std::string getPhoneNumber();

   private :
      static std::string _textContent;
      static std::string _phoneNumber;
};


#endif
