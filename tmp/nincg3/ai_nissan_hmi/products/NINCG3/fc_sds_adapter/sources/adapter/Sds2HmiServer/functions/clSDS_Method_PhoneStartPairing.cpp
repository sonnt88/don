/*********************************************************************//**
 * \file       clSDS_Method_PhoneStartPairing.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_PhoneStartPairing.h"
#include "external/sds2hmi_fi.h"


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_PhoneStartPairing::~clSDS_Method_PhoneStartPairing()
{
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_PhoneStartPairing::clSDS_Method_PhoneStartPairing(
   ahl_tclBaseOneThreadService* pService, GuiService& guiService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_PHONESTARTPAIRING, pService), _guiService(guiService)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_PhoneStartPairing::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_PHONE_GOTO_PAIRING);
   vSendMethodResult();
}
