/*********************************************************************//**
 * \file       clSDS_Method_NaviSetMapMode.cpp
 *
 * NaviSetMapMode method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_NaviSetMapMode.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_NaviSetMapMode.cpp.trc.h"
#endif


using namespace org::bosch::cm::navigation::NavigationService;


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_NaviSetMapMode::~clSDS_Method_NaviSetMapMode()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_NaviSetMapMode::clSDS_Method_NaviSetMapMode(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr< NavigationServiceProxy > naviProxy,
   GuiService& guiService)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_NAVISETMAPMODE, pService)
   , _naviProxy(naviProxy)
   , _guiService(guiService)
   , _mapRepresentation(MapRepresentation__MAP_REPRESENTATION_NOT_SET)
   , _poiIconReq(UNKNOWNTYPE)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSetMapMode::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgNaviSetMapModeMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   vGetMapOrientationfromSDS(oMessage);
   vSendMethodResult();
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _naviProxy)
   {
      _naviProxy->sendMapRepresentationRegister(*this);
      _naviProxy->sendMapRepresentationGet(*this);
   }
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _naviProxy)
   {
      _naviProxy->sendMapRepresentationDeregisterAll();
   }
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::onMapRepresentationUpdate(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< MapRepresentationUpdate >& update)
{
   _mapRepresentation = update->getMapRepresentation();
   ETG_TRACE_USR4(("clSDS_Method_NaviSetMapMode::onMapRepresentationUpdate mapRepresentation %d", _mapRepresentation));
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::onMapRepresentationError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< MapRepresentationError >& /*error*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::onSetMapRepresentationResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< SetMapRepresentationResponse >& /*response*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::onSetMapRepresentationError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< SetMapRepresentationError >& /*error*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::vGetMapOrientationfromSDS(const sds2hmi_sdsfi_tclMsgNaviSetMapModeMethodStart& oMessage)
{
   MapmodeType enMapmodeType = UNKNOWN;

   switch (oMessage.nMapView.enType)
   {
      case sds2hmi_fi_tcl_e8_NAV_MapView::FI_EN_VIEW_2D:
      {
         enMapmodeType = VIEW_2D;
         break;
      }
      case sds2hmi_fi_tcl_e8_NAV_MapView::FI_EN_VIEW_3D:
      {
         enMapmodeType = VIEW_3D;
         break;
      }
      default:
      {
         break;
      }
   }

   switch (oMessage.nMapOrientation.enType)
   {
      case sds2hmi_fi_tcl_e8_NAV_MapOrientation::FI_EN_NORTH_UP:
      {
         enMapmodeType = NORTH_UP;
         break;
      }
      case sds2hmi_fi_tcl_e8_NAV_MapOrientation::FI_EN_HEADING:
      {
         enMapmodeType = HEADING;
         break;
      }
      default:
      {
         break;
      }
   }

   vSendMapviewRequest(oMessage.nTBTSymbols);

   ETG_TRACE_USR4(("clSDS_Method_NaviSetMapMode::vGetMapOrientationfromSDS enMapmodeType %d", enMapmodeType));

   vGetTargetMapOrientation(enMapmodeType);

   vHandlePOIIconRequest(oMessage.IconSetting);
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::vGetTargetMapOrientation(MapmodeType enMapmodeType)
{
   org::bosch::cm::navigation::NavigationService::MapRepresentation enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_NOT_SET ;

   switch (_mapRepresentation)
   {
      case MapRepresentation__MAP_REPRESENTATION_NOT_SET:
      {
         if (enMapmodeType == VIEW_3D)
         {
            enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_3D_CAR_HEADING;
         }
         else if (enMapmodeType == VIEW_2D)
         {
            enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_2D_CAR_HEADING;
         }
         else if (enMapmodeType == HEADING)
         {
            enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_3D_CAR_HEADING;
         }
         else if (enMapmodeType == NORTH_UP)
         {
            enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_2D_NORTH_UP;
         }
         break;
      }
      case MapRepresentation__MAP_REPRESENTATION_3D_CAR_HEADING:
      {
         if (enMapmodeType == VIEW_3D)
         {
            enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_3D_CAR_HEADING;
         }
         else if (enMapmodeType == VIEW_2D)
         {
            enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_2D_CAR_HEADING;
         }
         else if (enMapmodeType == HEADING)
         {
            enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_3D_CAR_HEADING;
         }
         else if (enMapmodeType == NORTH_UP)
         {
            enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_2D_NORTH_UP;
         }
         break;
      }
      case MapRepresentation__MAP_REPRESENTATION_2D_CAR_HEADING:
      {
         if (enMapmodeType == VIEW_3D)
         {
            enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_3D_CAR_HEADING;
         }
         else if (enMapmodeType == VIEW_2D)
         {
            enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_2D_CAR_HEADING;
         }
         else if (enMapmodeType == HEADING)
         {
            enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_2D_CAR_HEADING;
         }
         else if (enMapmodeType == NORTH_UP)
         {
            enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_2D_NORTH_UP;
         }
         break;
      }
      case MapRepresentation__MAP_REPRESENTATION_2D_NORTH_UP:
      {
         if (enMapmodeType == VIEW_3D)
         {
            enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_3D_CAR_HEADING;
         }
         else if (enMapmodeType == VIEW_2D)
         {
            enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_2D_NORTH_UP;
         }
         else if (enMapmodeType == HEADING)
         {
            enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_2D_CAR_HEADING;
         }
         else if (enMapmodeType == NORTH_UP)
         {
            enTargetMapRepresentation = MapRepresentation__MAP_REPRESENTATION_2D_NORTH_UP;
         }
         break;
      }
      default:
      {
         break;
      }
   }
   ETG_TRACE_USR4(("clSDS_Method_NaviSetMapMode::vGetTargetMapOrientation enTargetMapRepresentation %d", enTargetMapRepresentation));

   if (MapRepresentation__MAP_REPRESENTATION_NOT_SET	!=	enTargetMapRepresentation)
   {
      vSendMapOrientationtoNavi(enTargetMapRepresentation);
   }
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::vSendMapviewRequest(const sds2hmi_fi_tcl_e8_NAV_TBT_Symbols& oNavTBTSymbols)
{
   switch (oNavTBTSymbols.enType)
   {
      case sds2hmi_fi_tcl_e8_NAV_TBT_Symbols::FI_EN_SYMBOLS_MIXED:
      {
         MapViewMode enMapviewmodeType = MapViewMode__MAP_VIEW_MODE_SPLIT_MAP;
         _naviProxy->sendShowMapScreenWithMapViewModeRequest(*this, enMapviewmodeType);

         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
      }
      break;
      case sds2hmi_fi_tcl_e8_NAV_TBT_Symbols::FI_EN_SYMBOLS_OFF:
      {
         MapViewMode enMapviewmodeType = MapViewMode__MAP_VIEW_MODE_FULL_MAP;
         _naviProxy->sendShowMapScreenWithMapViewModeRequest(*this, enMapviewmodeType);
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
      }
      break;
      default:
         break;
   }
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::vHandlePOIIconRequest(const sds2hmi_fi_tcl_Nav_IconSetting& oNavIconSettings)
{
   switch (oNavIconSettings.DisplayAction.enType)
   {
      case sds2hmi_fi_tcl_e8_NAV_IconDisplayAction::FI_EN_ICONDSPLY_SHOWALL:
         vSendShowallPOIIconRequest();
         break;
      case sds2hmi_fi_tcl_e8_NAV_IconDisplayAction::FI_EN_ICONDSPLY_SHOW:
         vSendShowPOIIconCategoryRequest(oNavIconSettings.CategoryPredefined);
         break;
      case sds2hmi_fi_tcl_e8_NAV_IconDisplayAction::FI_EN_ICONDSPLY_REMOVEALL:
         vSendRemoveallPOIIconRequest();
         break;
      case sds2hmi_fi_tcl_e8_NAV_IconDisplayAction::FI_EN_ICONDSPLY_REMOVE:
         vSendRemovePOIIconCategoryRequest(oNavIconSettings.CategoryPredefined);
         break;
      default:
         break;
   }
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::vSendShowallPOIIconRequest()
{
   ::std::vector< MapPOIIconCategory > poiCategories;

   MapPOIIconCategory	mapPOIicons;

   mapPOIicons	=	MapPOIIconCategory__MAP_POI_ICON_CATEGORY_RESTAURANT;
   poiCategories.push_back(mapPOIicons);

   mapPOIicons	=	MapPOIIconCategory__MAP_POI_ICON_CATEGORY_GAS_STATION;
   poiCategories.push_back(mapPOIicons);

   mapPOIicons	=	MapPOIIconCategory__MAP_POI_ICON_CATEGORY_HOTEL;
   poiCategories.push_back(mapPOIicons);

   mapPOIicons	=	MapPOIIconCategory__MAP_POI_ICON_CATEGORY_ATM;
   poiCategories.push_back(mapPOIicons);

   mapPOIicons	=	MapPOIIconCategory__MAP_POI_ICON_CATEGORY_REST_AREA;
   poiCategories.push_back(mapPOIicons);

   _naviProxy->sendSetPoiIconCategoriesInMapRequest(*this, poiCategories);
   _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::vSendShowPOIIconCategoryRequest(const sds2hmi_fi_tcl_e16_SelectionCriterionType& oSelectionCriteriontyperequested)
{
   _selectionCriteriontype.enType	=	oSelectionCriteriontyperequested.enType;
   _poiIconReq			=	ShowIcon;
   _naviProxy->sendGetPoiIconCategoriesInMapRequest(*this);
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::vSendRemoveallPOIIconRequest()
{
   ::std::vector< MapPOIIconCategory > poiCategories;

   poiCategories.clear();

   _naviProxy->sendSetPoiIconCategoriesInMapRequest(*this, poiCategories);
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::vSendRemovePOIIconCategoryRequest(const sds2hmi_fi_tcl_e16_SelectionCriterionType& oSelectionCriteriontyperequested)
{
   _selectionCriteriontype.enType =	oSelectionCriteriontyperequested.enType;
   _poiIconReq	=	RemoveIcon;
   _naviProxy->sendGetPoiIconCategoriesInMapRequest(*this);
}


void clSDS_Method_NaviSetMapMode::vSendMapOrientationtoNavi(MapRepresentation enTargetMapRepresentation)
{
   _naviProxy->sendSetMapRepresentationRequest(*this, enTargetMapRepresentation);
   _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::onGetPoiIconCategoriesInMapResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< GetPoiIconCategoriesInMapResponse >& response)
{
   ::std::vector< MapPOIIconCategory > mapPOIIconCategories	=	response->getMapPOIIconCategories();
   ::std::vector< MapPOIIconCategory >::iterator it;
   for (it = mapPOIIconCategories.begin(); it != mapPOIIconCategories.end(); ++it)
   {
      ETG_TRACE_USR4(("clSDS_Method_NaviSetMapMode::onGetPoiIconCategoriesInMapResponse mapPOIIconCategories: %d", *it));
   }

   MapPOIIconCategory	mapPOIicons;

   vGetPOIIConCategory(_selectionCriteriontype, mapPOIicons);
   ETG_TRACE_USR4(("clSDS_Method_NaviSetMapMode::onGetPoiIconCategoriesInMapResponse _selectionCriteriontype: %d", _selectionCriteriontype.enType));
   ETG_TRACE_USR4(("clSDS_Method_NaviSetMapMode::onGetPoiIconCategoriesInMapResponse mapPOIicons: %d", mapPOIicons));

   switch (_poiIconReq)
   {
      case ShowIcon:
      {
         mapPOIIconCategories.push_back(mapPOIicons);
         _naviProxy->sendSetPoiIconCategoriesInMapRequest(*this, mapPOIIconCategories);
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
         _poiIconReq = UNKNOWNTYPE;
      }
      break;
      case RemoveIcon:
      {
         ::std::vector< MapPOIIconCategory >::iterator itr;

         for (itr = mapPOIIconCategories.begin(); itr != mapPOIIconCategories.end();)
         {
            if (*itr == mapPOIicons)
            {
               itr = mapPOIIconCategories.erase(itr);
               ETG_TRACE_USR4(("clSDS_Method_NaviSetMapMode::onGetPoiIconCategoriesInMapResponse mapPOIIconCategories: ERASED"));
            }
            else
            {
               ++itr;
            }
         }

         _naviProxy->sendSetPoiIconCategoriesInMapRequest(*this, mapPOIIconCategories);
         ETG_TRACE_USR4(("clSDS_Method_NaviSetMapMode::onGetPoiIconCategoriesInMapResponse sendSetPoiIconCategoriesInMapRequest MS Sent"));
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
         _poiIconReq			=	UNKNOWNTYPE;
      }
      break;
      default:
         break;
   }
}


/***********************************************************************//**
* TODO jnd2hi: function should return the value of MapPOIIconCategory rather than modify referenced parameter
***************************************************************************/
void clSDS_Method_NaviSetMapMode::vGetPOIIConCategory(const sds2hmi_fi_tcl_e16_SelectionCriterionType&	oSelectioncriteriontype, MapPOIIconCategory& enmappoiicons) const
{
   switch (oSelectioncriteriontype.enType)
   {
      case sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_RESTAURANT:
         enmappoiicons =	MapPOIIconCategory__MAP_POI_ICON_CATEGORY_RESTAURANT;
         break;
      case sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_GASSTATION:
         enmappoiicons =	MapPOIIconCategory__MAP_POI_ICON_CATEGORY_GAS_STATION;
         break;
      case sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_HOTEL_MOTEL:
         enmappoiicons =	MapPOIIconCategory__MAP_POI_ICON_CATEGORY_HOTEL;
         break;
      case sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_AUTOMATICTELLERMACHINE:
         enmappoiicons =	MapPOIIconCategory__MAP_POI_ICON_CATEGORY_ATM;
         break;
      case sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_PUBLICRESTROOM:
         enmappoiicons =	MapPOIIconCategory__MAP_POI_ICON_CATEGORY_REST_AREA;
         break;
      default:
         break;
   }
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::onShowMapScreenWithMapViewModeError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< ShowMapScreenWithMapViewModeError >& /*error*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void  clSDS_Method_NaviSetMapMode::onShowMapScreenWithMapViewModeResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >&/* proxy*/,
   const ::boost::shared_ptr< ShowMapScreenWithMapViewModeResponse >& /*response*/)
{
}


/***********************************************************************//**
*
***************************************************************************/

void clSDS_Method_NaviSetMapMode::onSetPoiIconCategoriesInMapResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< SetPoiIconCategoriesInMapResponse >& /*response*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::onSetPoiIconCategoriesInMapError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< SetPoiIconCategoriesInMapError >& /*error*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetMapMode::onGetPoiIconCategoriesInMapError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< GetPoiIconCategoriesInMapError >& /*error*/)
{
}
