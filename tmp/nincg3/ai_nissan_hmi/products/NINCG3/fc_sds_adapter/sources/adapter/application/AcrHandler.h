/*********************************************************************//**
 * \file       AcrHandler.h
 *
 * Handles audio channel requests from the prompt player.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef AcrHandler_h
#define AcrHandler_h


#include "CMB_ACR_FIProxy.h"


class AcrHandler:
   public asf::core::ServiceAvailableIF,
   public CMB_ACR_FI::ChannelRequestCallbackIF,
   public CMB_ACR_FI::VoiceAdviceCallbackIF
{
   public:
      virtual ~AcrHandler();
      AcrHandler(::boost::shared_ptr< CMB_ACR_FI::CMB_ACR_FIProxy > acrProxy);

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onChannelRequestStatus(
         const ::boost::shared_ptr< CMB_ACR_FI::CMB_ACR_FIProxy >& proxy,
         const ::boost::shared_ptr< CMB_ACR_FI::ChannelRequestStatus >& status);
      virtual void onChannelRequestError(
         const ::boost::shared_ptr< CMB_ACR_FI::CMB_ACR_FIProxy >& proxy,
         const ::boost::shared_ptr< CMB_ACR_FI::ChannelRequestError >& error);

      virtual void onVoiceAdviceError(
         const ::boost::shared_ptr< CMB_ACR_FI::CMB_ACR_FIProxy >& proxy,
         const ::boost::shared_ptr< CMB_ACR_FI::VoiceAdviceError >& error);
      virtual void onVoiceAdviceResult(
         const ::boost::shared_ptr< CMB_ACR_FI::CMB_ACR_FIProxy >& proxy,
         const ::boost::shared_ptr< CMB_ACR_FI::VoiceAdviceResult >& result);

   private:
      ::boost::shared_ptr< CMB_ACR_FI::CMB_ACR_FIProxy >_acrProxy;
};


#endif
