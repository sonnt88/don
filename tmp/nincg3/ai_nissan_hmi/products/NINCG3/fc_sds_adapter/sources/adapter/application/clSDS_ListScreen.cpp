/*********************************************************************//**
 * \file       clSDS_ListScreen.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_ListScreen.h"


/**************************************************************************//**
*Destructor
******************************************************************************/
clSDS_ListScreen::~clSDS_ListScreen()
{
   _pCurrentList = NULL;
   _oListMap.clear();
}


/**************************************************************************//**
*Constructor
******************************************************************************/
clSDS_ListScreen::clSDS_ListScreen()
{
   _pCurrentList = NULL;
   _oListMap.clear();
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_ListScreen::bIsListScreen(Sds_ViewId enScreenId)
{
   if (!_oListMap.empty())
   {
      if (_oListMap.find(enScreenId) != _oListMap.end())
      {
         return TRUE;
      }
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_ListScreen::vSetCurrentList(Sds_ViewId enScreenId)
{
   _pCurrentList = pGetList(enScreenId);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_ListScreen::vAddToList(Sds_ViewId enScreenId, clSDS_List* pHMIList)
{
   if (pHMIList == NULL)
   {
      NORMAL_M_ASSERT_ALWAYS();
      return;	//lint !e527 Unreachable code - jnd2hi: behind assertion
   }
   _oListMap[enScreenId] = pHMIList;
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::vector<clSDS_ListItems> clSDS_ListScreen::vGetListScreenContents()
{
   bpstl::vector<clSDS_ListItems> oCurrentListItems;
   if (_pCurrentList != NULL)
   {
      stRange oRange = _pCurrentList->stGetRangeOfPage();
      oCurrentListItems = _pCurrentList->oGetItems(oRange.u32StartIndex, oRange.u32EndIndex);
   }
   return oCurrentListItems;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_ListScreen::vSetListScreenContents(bpstl::vector<clSDS_ListItems> listItems)
{
   if (NULL != _pCurrentList)
   {
      _pCurrentList->setItems(listItems);
   }
}


/**************************************************************************//**
*
******************************************************************************/
clSDS_List* clSDS_ListScreen::pGetList(Sds_ViewId enScreenId)
{
   return _oListMap.find(enScreenId)->second;
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_ListScreen::u32GetPageNumber() const
{
   if (_pCurrentList != NULL)
   {
      return _pCurrentList->u32GetPageNumber();
   }
   return 0;
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_ListScreen::bSelectElement(tU32 u32SelectedIndex)
{
   if (_pCurrentList != NULL)
   {
      return _pCurrentList->bSelectElement(u32SelectedIndex);
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::string clSDS_ListScreen::oGetSelectedItem(tU32 u32Index)
{
   bpstl::string oSelectedItem;
   if (_pCurrentList != NULL)
   {
      oSelectedItem = _pCurrentList->oGetSelectedItem(u32Index);
   }
   return oSelectedItem;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_ListScreen::vResetPageNumber()
{
   if (_pCurrentList != NULL)
   {
      _pCurrentList->vResetPageNumber();
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_ListScreen::vIncrementPageNumber()
{
   if (_pCurrentList != NULL)
   {
      _pCurrentList->vIncrementPageNumber();
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_ListScreen::vDecrementPageNumber()
{
   if (_pCurrentList != NULL)
   {
      _pCurrentList->vDecrementPageNumber();
   }
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_ListScreen::bHasPreviousPage() const
{
   if (_pCurrentList != NULL)
   {
      return _pCurrentList->bHasPreviousPage();
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_ListScreen::bHasNextPage() const
{
   if (_pCurrentList != NULL)
   {
      return _pCurrentList->bHasNextPage();
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
tU8 clSDS_ListScreen::u8GetNumOfElementsOnPage() const
{
   if (_pCurrentList != NULL)
   {
      return _pCurrentList->u8GetNumOfElementsOnPage();
   }
   return 0;
}


std::vector<sds2hmi_fi_tcl_HMIElementDescription> clSDS_ListScreen::getHmiElementDescription(int index)
{
   std::vector<sds2hmi_fi_tcl_HMIElementDescription> hmiElementDescription;
   if (_pCurrentList != NULL)
   {
      return _pCurrentList->getHmiElementDescription(index);
   }
   return hmiElementDescription;
}
