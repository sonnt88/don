/*********************************************************************//**
 * \file       clSDS_Method_MediaGetDeviceInfo.cpp
 *
 * clSDS_Method_MediaGetDeviceInfo method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_MediaGetDeviceInfo.h"
#include "external/sds2hmi_fi.h"


/**************************************************************************//**
*Destructor
******************************************************************************/
clSDS_Method_MediaGetDeviceInfo::~clSDS_Method_MediaGetDeviceInfo()
{
}


/**************************************************************************//**
*Constructor
******************************************************************************/
clSDS_Method_MediaGetDeviceInfo::clSDS_Method_MediaGetDeviceInfo(ahl_tclBaseOneThreadService* pService,
      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy > pMediaPlayerProxy)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_MEDIAGETDEVICEINFO, pService)
   , _mediaPlayerProxy(pMediaPlayerProxy)
   , _listHandle(0)
   , _listSize(0)
{
}


/**************************************************************************//**
 *
******************************************************************************/
tVoid clSDS_Method_MediaGetDeviceInfo::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   getMediaPlayerCDListInfo();
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_MediaGetDeviceInfo::sendResult()
{
   sds2hmi_sdsfi_tclMsgMediaGetDeviceInfoMethodResult oResult;
   oResult.NumberOfTracks = static_cast<unsigned long>(_listSize);
   vSendMethodResult(oResult);
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_MediaGetDeviceInfo::getMediaPlayerCDListInfo()
{
   uint8 deviceTag = getDeviceTag();
   if (deviceTag)
   {
      _mediaPlayerProxy->sendCreateMediaPlayerCDListStart(*this, "/", deviceTag, ::MPlay_fi_types::T_e8_MPlayFileTypeSelection__e8FTS_ALL);
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_UNKNOWNERROR);
   }
}


/***********************************************************************//**
*
***************************************************************************/
uint8 clSDS_Method_MediaGetDeviceInfo::getDeviceTag() const
{
   if (_mediaPlayerProxy)
   {
      const mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsStatus& connectionStatus = _mediaPlayerProxy->getMediaPlayerDeviceConnections();
      if (connectionStatus.hasODeviceInfo())
      {
         const ::MPlay_fi_types::T_MPlayDeviceInfo& deviceInfo = connectionStatus.getODeviceInfo();
         for (::std::vector< MPlay_fi_types::T_MPlayDeviceInfoItem >::const_iterator itr = deviceInfo.begin(); itr != deviceInfo.end(); ++itr)
         {
            if (itr->getE8DeviceType() == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA)
            {
               return itr->getU8DeviceTag();
            }
         }
      }
   }
   return 0;
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_MediaGetDeviceInfo::onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& /*proxy*/,
      const asf::core::ServiceStateChange& /*stateChange*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_MediaGetDeviceInfo::onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& /*proxy*/,
      const asf::core::ServiceStateChange& /*stateChange*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_MediaGetDeviceInfo::onMediaPlayerDeviceConnectionsError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsError >& /*error*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_MediaGetDeviceInfo::onMediaPlayerDeviceConnectionsStatus(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsStatus >& /*status*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_MediaGetDeviceInfo::onCreateMediaPlayerCDListError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< mplay_MediaPlayer_FI::CreateMediaPlayerCDListError >& /*error*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_MediaGetDeviceInfo::onCreateMediaPlayerCDListResult(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< mplay_MediaPlayer_FI::CreateMediaPlayerCDListResult >& result)
{
   if (result->hasU32ListHandle())
   {
      _listHandle = result->getU32ListHandle();
   }
   if (result->hasU32ListSize())
   {
      _listSize = result->getU32ListSize();
   }
   sendResult();
}


/***********************************************************************//**
*
***************************************************************************/
unsigned int clSDS_Method_MediaGetDeviceInfo::getListHandle() const
{
   return _listHandle;
}
