/*********************************************************************//**
 * \file       clSDS_RecentCallsList.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_RecentCallsList_h
#define clSDS_RecentCallsList_h


#include "application/clSDS_List.h"
#include "MOST_PhonBk_FIProxy.h"
#include "application/clSDS_PhoneStatusObserver.h"
#include <string>

class clSDS_RecentCallsList
   : public clSDS_List
   , public clSDS_PhoneStatusObserver
   , public asf::core::ServiceAvailableIF
   , public MOST_PhonBk_FI::DownloadStateCallbackIF
   , public MOST_PhonBk_FI::DevicePhoneBookFeatureSupportCallbackIF
   , public MOST_PhonBk_FI::CreateCallHistoryListCallbackIF
   , public MOST_PhonBk_FI::RequestSliceCallHistoryListCallbackIF
{
   public:
      virtual ~clSDS_RecentCallsList();
      clSDS_RecentCallsList(::boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy> pPhoneBookProxy);
      tU32 u32GetSize();
      std::vector<clSDS_ListItems> oGetItems(tU32 u32StartIndex, tU32 u32EndIndex);
      tBool bSelectElement(tU32 u32SelectedIndex);
      std::string oGetSelectedItem(tU32 u32Index);
      void getOutgoingCallHistoryList();
      void getIncomingCallHistoryList();
      void getMissedCallHistoryList();
      std::string getLastDialedNumber() const;

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onDownloadStateStatus(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::DownloadStateStatus >& status);
      virtual void onDownloadStateError(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::DownloadStateError >& error);

      virtual void onDevicePhoneBookFeatureSupportError(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::DevicePhoneBookFeatureSupportError >& error);
      virtual void onDevicePhoneBookFeatureSupportStatus(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::DevicePhoneBookFeatureSupportStatus >& status);

      virtual void onCreateCallHistoryListError(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::CreateCallHistoryListError >& error);
      virtual void onCreateCallHistoryListResult(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::CreateCallHistoryListResult >& result);

      virtual void onRequestSliceCallHistoryListError(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::RequestSliceCallHistoryListError >& error);
      virtual void onRequestSliceCallHistoryListResult(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::RequestSliceCallHistoryListResult >& result);

      virtual void phoneStatusChanged(uint8 deviceHandle, most_BTSet_fi_types::T_e8_BTSetDeviceStatus status);
      virtual std::vector<sds2hmi_fi_tcl_HMIElementDescription> getHmiElementDescription(int index);

   private:
      std::string oGetName(tU32 u32Index);
      std::string oGetPhonenumber(tU32 u32Index);
      tU32 u32GetNumberType(tU32 u32Index) const;
      std::string oGetItem(tU32 u32Index);
      void setRecentCallList(const most_PhonBk_fi_types::T_PhonBkCallHistoryListSliceResult& callHistoryListSliceResult);
      void updateHeadlineTagforRedial();

      tVoid vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType);

      struct RecentCall
      {
         std::string Name;
         std::string PhoneNumber;
      };

      std::vector<RecentCall> _recentCallList;
      boost::shared_ptr<MOST_PhonBk_FI::MOST_PhonBk_FIProxy> _phoneBookProxy;
      unsigned int _downloadStatus;
      uint16 _recentCallHistorySize;
};


#endif
