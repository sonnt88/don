/*********************************************************************//**
 * \file       clSDS_StringVarList.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_StringVarList_h
#define clSDS_StringVarList_h


#define SYSTEM_S_IMPORT_INTERFACE_MAP
#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"

class clSDS_StringVarList
{
   public:
      virtual ~clSDS_StringVarList();
      clSDS_StringVarList();

      static tVoid vSetVariable(const bpstl::string& oVariable, const bpstl::string& oValue);
      static bpstl::string oResolveVariables(bpstl::string oString);
      static bpstl::map<bpstl::string, bpstl::string> getVariableList();
      static void printStaticVariables();

   private:
      static bpstl::map<bpstl::string, bpstl::string> _mapVariableList;
};


#endif
