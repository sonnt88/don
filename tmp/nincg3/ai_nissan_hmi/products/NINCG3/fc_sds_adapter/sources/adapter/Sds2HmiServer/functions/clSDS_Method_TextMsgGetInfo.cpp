/*********************************************************************//**
 * \file       clSDS_Method_TextMsgGetInfo.cpp
 *
 * clSDS_Method_TextMsgGetInfo method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_TextMsgGetInfo.h"
#include "application/clSDS_ReadSmsList.h"
#include "application/clSDS_TextMsgContent.h"
#include "application/StringUtils.h"

#include "SdsAdapter_Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_TextMsgGetInfo.cpp.trc.h"
#endif

#define UTFUTIL_S_IMPORT_INTERFACE_GENERIC
#include "utf_if.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_TextMsgGetInfo::~clSDS_Method_TextMsgGetInfo()
{
   _pReadSmsList = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_TextMsgGetInfo::clSDS_Method_TextMsgGetInfo(ahl_tclBaseOneThreadService* pService, clSDS_ReadSmsList* pSmsList)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_TEXTMSGGETINFO, pService)
   , _pReadSmsList(pSmsList)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_TextMsgGetInfo::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   sds2hmi_sdsfi_tclMsgTextMsgGetInfoMethodResult oMessage;
   vGetCurrentSmsInformation(oMessage);
   vTraceMethodResult(oMessage);
   vSendMethodResult(oMessage);

   vSaveCurrentSmsNumberForCallBackAndSendText();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_TextMsgGetInfo::vGetCurrentSmsInformation(
   sds2hmi_sdsfi_tclMsgTextMsgGetInfoMethodResult& oMessage) const
{
   oMessage.nMoreThanOne = bMoreThanOneSmsAvailable();
   oMessage.nFirstSMS = bCurrentSmsIsFirstSms();
   oMessage.nLastSMS = bCurrentSmsIsLastSms();

   std::string senderPhoneNumber = _pReadSmsList->getSelectedPhoneNumber();
   oMessage.sPhoneNumberSender.bSet(senderPhoneNumber.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);

   std::string senderName = _pReadSmsList->getSelectedContactName();
   oMessage.sNameSender.bSet(senderName.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);

   std::string oCurrentSmsDetails;
   vGetCurrentSmsDetails(oCurrentSmsDetails);
   oMessage.sAdditionalInformation.bSet(oCurrentSmsDetails.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_Method_TextMsgGetInfo::bMoreThanOneSmsAvailable() const
{
   return (_pReadSmsList->u32GetSize() > 1);
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_Method_TextMsgGetInfo::bCurrentSmsIsFirstSms() const
{
   return (_pReadSmsList->getSelectedSmsIndex() == 1);
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_Method_TextMsgGetInfo::bCurrentSmsIsLastSms() const
{
   return (_pReadSmsList->u32GetSize() == _pReadSmsList->getSelectedSmsIndex());
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_TextMsgGetInfo::vGetCurrentSmsDetails(std::string& oString) const
{
   std::string oCurrentSmsRecievedTime;
   oCurrentSmsRecievedTime = _pReadSmsList->getTimeDate();
   std::string oCurrentSmsIndex;
   vGetCurrentSmsIndex(oCurrentSmsIndex);

   if (!oCurrentSmsRecievedTime.empty())
   {
      oString.append(" ");
      oString.append(oCurrentSmsRecievedTime);
   }
   oString.append(" ");
   oString.append(oCurrentSmsIndex);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_TextMsgGetInfo::vGetCurrentSmsIndex(bpstl::string& oString) const
{
   oString.assign("\xEF\xA0\xBB");
   oString += StringUtils::toString(_pReadSmsList->getSelectedSmsIndex());
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_TextMsgGetInfo::vSaveCurrentSmsNumberForCallBackAndSendText() const
{
   clSDS_TextMsgContent::setPhoneNumber(_pReadSmsList->getSelectedPhoneNumber());
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_Method_TextMsgGetInfo::vTraceMethodResult(const sds2hmi_sdsfi_tclMsgTextMsgGetInfoMethodResult& oMessage) const
{
   bpstl::string oTraceString;
   oTraceString.assign("sNameSender: '");
   tString pString = oMessage.sNameSender.szGet(sds2hmi_fi_tclString::FI_EN_UTF8);
   oTraceString.append(pString);
   OSAL_DELETE[] pString;

   oTraceString.append("' sAdditionalInformation: '");
   pString = oMessage.sAdditionalInformation.szGet(sds2hmi_fi_tclString::FI_EN_UTF8);
   oTraceString.append(pString);
   OSAL_DELETE[] pString;

   oTraceString.append("' sPhoneNumberSender: '");
   pString = oMessage.sPhoneNumberSender.szGet(sds2hmi_fi_tclString::FI_EN_UTF8);
   oTraceString.append(pString);
   OSAL_DELETE[] pString;
   oTraceString.append("'");

// TODO jnd2hi rework trace
//   ET_TRACE_BIN(
//      TR_CLASS_SDSADP_DETAILS,
//      TR_LEVEL_HMI_INFO,
//      ET_EN_T16 _ TRACE_CCA_VALUE _
//      ET_EN_T16 _ _pService->u16GetServiceId() _
//      ET_EN_T16 _ u16GetFunctionID() _
//      ET_EN_T8 _ oMessage.nMoreThanOne _
//      ET_EN_T8 _ oMessage.nFirstSMS _
//      ET_EN_T8 _ oMessage.nLastSMS _
//      ET_EN_STRING _ oTraceString.c_str() _
//      ET_EN_DONE);
}
