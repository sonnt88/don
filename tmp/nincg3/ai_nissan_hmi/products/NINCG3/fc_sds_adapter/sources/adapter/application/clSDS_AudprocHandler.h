/*********************************************************************//**
 * \file       clSDS_AudprocHandler.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_AudprocHandler_h
#define clSDS_AudprocHandler_h


#include "org/bosch/audproc/serviceProxy.h"


class PopUpService;


class clSDS_AudprocHandler
   : public org::bosch::audproc::service::AudprocInitializeCallbackIF
   , public org::bosch::audproc::service::AudprocStartAudioCallbackIF
   , public org::bosch::audproc::service::AudprocStopAudioCallbackIF
   , public org::bosch::audproc::service::AudprocDestroyCallbackIF
   , public org::bosch::audproc::service::AudprocSetParamCallbackIF
   , public org::bosch::audproc::service::AudprocMicrolevelStatusCallbackIF
{
   public:
      clSDS_AudprocHandler(::boost::shared_ptr< org::bosch::audproc::service::ServiceProxy > audprocProxy , PopUpService* popUpService);
      virtual ~clSDS_AudprocHandler();

      void initialize();
      void destroy();
      void startAudio();
      void stopAudio();
      void setParam();

   private:
      virtual void onAudprocInitializeError(const ::boost::shared_ptr< org::bosch::audproc::service::ServiceProxy >& proxy, const ::boost::shared_ptr< org::bosch::audproc::service::AudprocInitializeError >& error);
      virtual void onAudprocInitializeResponse(const ::boost::shared_ptr< org::bosch::audproc::service::ServiceProxy >& proxy, const ::boost::shared_ptr< org::bosch::audproc::service::AudprocInitializeResponse >& response);

      virtual void onAudprocDestroyError(const ::boost::shared_ptr< org::bosch::audproc::service::ServiceProxy >& proxy, const ::boost::shared_ptr< org::bosch::audproc::service::AudprocDestroyError >& error);
      virtual void onAudprocDestroyResponse(const ::boost::shared_ptr< org::bosch::audproc::service::ServiceProxy >& proxy, const ::boost::shared_ptr< org::bosch::audproc::service::AudprocDestroyResponse >& response);

      virtual void onAudprocStartAudioError(const ::boost::shared_ptr< org::bosch::audproc::service::ServiceProxy >& proxy, const ::boost::shared_ptr< org::bosch::audproc::service::AudprocStartAudioError >& error);
      virtual void onAudprocStartAudioResponse(const ::boost::shared_ptr< org::bosch::audproc::service::ServiceProxy >& proxy, const ::boost::shared_ptr< org::bosch::audproc::service::AudprocStartAudioResponse >& response);

      virtual void onAudprocStopAudioError(const ::boost::shared_ptr< org::bosch::audproc::service::ServiceProxy >& proxy, const ::boost::shared_ptr< org::bosch::audproc::service::AudprocStopAudioError >& error);
      virtual void onAudprocStopAudioResponse(const ::boost::shared_ptr< org::bosch::audproc::service::ServiceProxy >& proxy, const ::boost::shared_ptr< org::bosch::audproc::service::AudprocStopAudioResponse >& response);

      virtual void onAudprocSetParamError(const ::boost::shared_ptr< org::bosch::audproc::service::ServiceProxy >& proxy, const ::boost::shared_ptr< org::bosch::audproc::service::AudprocSetParamError >& error);
      virtual void onAudprocSetParamResponse(const ::boost::shared_ptr< org::bosch::audproc::service::ServiceProxy >& proxy, const ::boost::shared_ptr< org::bosch::audproc::service::AudprocSetParamResponse >& response);

      virtual void onAudprocMicrolevelStatusError(const ::boost::shared_ptr< org::bosch::audproc::service::ServiceProxy >& proxy, const ::boost::shared_ptr< org::bosch::audproc::service::AudprocMicrolevelStatusError >& error);
      virtual void onAudprocMicrolevelStatusSignal(const ::boost::shared_ptr< org::bosch::audproc::service::ServiceProxy >& proxy, const ::boost::shared_ptr< org::bosch::audproc::service::AudprocMicrolevelStatusSignal >& signal);

      boost::shared_ptr< org::bosch::audproc::service::ServiceProxy > _audprocProxy;
      PopUpService* _popUpService;
};


#endif
