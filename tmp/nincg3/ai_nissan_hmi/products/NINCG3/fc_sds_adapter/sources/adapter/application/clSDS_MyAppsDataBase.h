/*********************************************************************//**
 * \file       clSDS_MyAppsDataBase.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_MyAppsDataBase_h
#define clSDS_MyAppsDataBase_h


#define SYSTEM_S_IMPORT_INTERFACE_STRING
#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#include "stl_pif.h"

#ifdef SQL_SUPPORTED
#define SQL_S_IMPORT_INTERFACE_CLIENT
#include "sql_if.h"

#define SQLDB_S_IMPORT_INTERFACE_BASE
#define SQLDB_S_IMPORT_INTERFACE_NOTRACE
#include "sqldb_if.h"
#endif

#include "external/sds2hmi_fi.h"


class clSDS_MyAppsDataBaseObserver;


struct stMyappNames
{
   bpstl::string oAppName;
   bpstl::string oLanguageId;
};


#ifdef SQL_SUPPORTED
class clSDS_MyAppsDataBase : public sql::Client
#else
class clSDS_MyAppsDataBase
#endif
{
   public:
      virtual ~clSDS_MyAppsDataBase();
      clSDS_MyAppsDataBase();
      virtual tS32 answer(tS32 s32NumOfEntry, tChar** vals, tChar** cols, tU32 arg);
      tVoid vRegisterObserver(clSDS_MyAppsDataBaseObserver* pObserver);
      bpstl::vector<stMyappNames> oGetMyAppsNameList() const;
      tVoid vRequestMyAppsNames();
      tBool bOpenMyAppsDataBase() const;
      tVoid vReadMyAppsDataBase() const;
      tVoid vClearMyAppsList();

   private:
      OSAL_tThreadID _hThread;
      tVoid vRequestAppsNamefromDataBase() const;
      tVoid vCreateThreadToReadMyAppsDataBase();
      bpstl::vector<clSDS_MyAppsDataBaseObserver*> _observers;
      bpstl::vector<stMyappNames> _oMyAppsNameList;
      tVoid vNotifyObservers();
};


#endif
