/*********************************************************************//**
 * \file       clSDS_Property_NaviStatus.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Property_NaviStatus_h
#define clSDS_Property_NaviStatus_h


#include "Sds2HmiServer/framework/clServerProperty.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"
#include "org/bosch/cm/navigation/NavigationServiceConst.h"
#include "external/sds2hmi_fi.h"


class clSDS_Property_NaviStatus
   : public clServerProperty
   , public asf::core::ServiceAvailableIF
   , public org::bosch::cm::navigation::NavigationService::NavStatusCallbackIF
{
   public:
      virtual ~clSDS_Property_NaviStatus();
      clSDS_Property_NaviStatus(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr<org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > naviProxy);

      tVoid vSendNavStatus();

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      void onNavStatusError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavStatusError >& error);
      void onNavStatusUpdate(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavStatusUpdate >& update);

   protected:
      virtual tVoid vGet(amt_tclServiceData* pInMsg);
      virtual tVoid vSet(amt_tclServiceData* pInMsg);
      virtual tVoid vUpreg(amt_tclServiceData* pInMsg);

   private:
      sds2hmi_fi_tcl_e8_NAV_Status::tenType sdsNaviStatus;
      sds2hmi_fi_tcl_e8_NAV_VDEAvailInfo::tenType enVDEAvailInfo;
      boost::shared_ptr<org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > _naviProxy;
};


#endif
