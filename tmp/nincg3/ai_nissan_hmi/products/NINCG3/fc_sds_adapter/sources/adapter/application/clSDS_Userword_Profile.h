/*********************************************************************//**
 * \file       clSDS_Userword_Profile.h
 *
 * Userwords related to one phone profile
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Userword_Profile_h
#define clSDS_Userword_Profile_h


#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"

#include "legacy/HSI_PHONE_SDS_Interface.h"
#include "application/clSDS_Userword_Entry.h"


class clSDS_Userword_Entry;
class sds2hmi_fi_tcl_Userword;


class clSDS_Userword_Profile
{
   public:
      clSDS_Userword_Profile(tU32 u32ProfileNum);
      ~clSDS_Userword_Profile();

      tVoid vSynchroniseWithAvailableUserwords(sds2hmi_fi_tcl_Userword oSDS_user);
      tBool bEntryExists(const bpstl::string& strName, const bpstl::string& strNumber);
      tU32 u32GetUWIDByNameAndNumber(const bpstl::string& strName, const bpstl::string& strNumber);
      tU32 u32CreateNewUWIDForNameAndNumber(bpstl::string strName, bpstl::string strNumber);
      tBool bGetNameAndPhoneNumberByUWID(tU32 u32UserwordID, bpstl::string& strName, bpstl::string& strNumber);
      tVoid vCreateUserwordEntryForPhone(tU32 u32Index, tsCMPhone_UserwordEntry& oUWEntry);
      tU32 u32GetEntryCount() const;
      tU32 u32GetProfileNumber() const;

   private:
      clSDS_Userword_Entry* pGetEntryByUWID(tU32 u32UWID);
      const clSDS_Userword_Entry* pGetEntryByNameAndNumber(const bpstl::string& strName, const bpstl::string& strNumber);
      tBool isEntryNewlyRecorded(tU32 tU32UWID) const;
      tU32 u32GetFreeUWID();
      tVoid vDeleteNotUsedEntries();

      tU32 _u32ProfileNum;
      bpstl::vector<clSDS_Userword_Entry> _oUserwordEntryList;
      clSDS_Userword_Entry _oEntry;
};


#endif
