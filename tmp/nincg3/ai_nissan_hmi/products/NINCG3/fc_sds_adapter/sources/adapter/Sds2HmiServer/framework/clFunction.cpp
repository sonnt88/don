/*********************************************************************//**
 * \file       clFunction.cpp
 *
 * Base class for CCA functions.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/framework/clFunction.h"
#include "SdsAdapter_Trace.h"

#define GENERICMSGS_S_IMPORT_INTERFACE_GENERIC
#include "generic_msgs_if.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_CCA_FW
#include "trcGenProj/Header/clFunction.cpp.trc.h"
#endif


/**************************************************************************//**
* Destructor
******************************************************************************/
clFunction::~clFunction()
{
   _pService = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clFunction::clFunction(tU16 u16FunctionID, ahl_tclBaseOneThreadService* pService)
   :  _pService(pService),
      _u16FunctionID(u16FunctionID)
{
}


/**************************************************************************//**
*
******************************************************************************/
tU16 clFunction::u16GetFunctionID(tVoid) const
{
   return _u16FunctionID;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clFunction::vSendMessage(tU16 u16DestAppID,
                               tU16 u16CmdCtr,
                               tU16 u16RegisterId,
                               const fi_tclTypeBase& oOutData,
                               tU16 u16Fid,
                               tU8 u8OpCode)
{
   fi_tclVisitorMessage oOutVisitorMsg(oOutData, getMajorVersion());

   /* Set the CCA message information */
   oOutVisitorMsg.vInitServiceData(
      _pService->u16GetAppID(),     /* Source app-ID */
      u16DestAppID,                      /* Dest. app-ID */
      AMT_C_U8_CCAMSG_STREAMTYPE_NODATA, /* stream type*/
      0,                                 /* stream counter*/
      u16RegisterId,                     /* Registry ID */
      u16CmdCtr,                         /* Command counter */
      _pService->u16GetServiceID(),
      u16Fid,                            /* Function-ID */
      u8OpCode,                          /* OpCode */
      0,                                 /* ACT */
      0,                                 /* Source sub-ID */
      0);                                /* Dest. sub-ID */

   ETG_TRACE_USR1(("tx appid=%04x function=%04x regid=%d opcode=%d cmdctr=%d ", u16DestAppID, u16Fid, u16RegisterId, u8OpCode, u16CmdCtr));

   _pService->ePostMessage(&oOutVisitorMsg);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clFunction::vSendEmptyMessage(tU16 u16DestAppID,
                                    tU16 u16CmdCtr,
                                    tU16 u16RegisterID,
                                    tU16 u16Fid,
                                    tU8 u8Opcode)
{
   gm_tclEmptyMessage oOutMessage(
      _pService->u16GetAppID(),
      u16DestAppID,
      u16RegisterID,
      u16CmdCtr,
      _pService->u16GetServiceID(),
      u16Fid,
      u8Opcode);

   ETG_TRACE_USR1(("tx appid=%04x function=%04x regid=%d opcode=%d cmdctr=%d ", u16DestAppID, u16Fid, u16RegisterID, u8Opcode, u16CmdCtr));
   _pService->ePostMessage(&oOutMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clFunction::vGetDataFromAmt(amt_tclServiceData* pFIMsg, fi_tclTypeBase& oFIData)
{
   fi_tclVisitorMessage oInVisitorMsg(pFIMsg);
   if (oInVisitorMsg.s32GetData(oFIData, getMajorVersion()) == OSAL_ERROR)
   {
      ETG_TRACE_ERR(("Error in clFunction::vGetDataFromAmt() for service 0x%x, function 0x%x", _pService->u16GetServiceID(), pFIMsg->u16GetFunctionID()));
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clFunction::vSendErrorMessage(tU16 u16DestAppID,
                                    tU16 u16CmdCtr,
                                    tU16 u16RegisterID,
                                    tU16 u16ErrorCode)
{
   gm_tclU16Message  oSendErrorMessage(
      _pService->u16GetAppID(),       // source app id
      u16DestAppID,
      u16RegisterID,
      u16CmdCtr,
      _pService->u16GetServiceID(),
      u16GetFunctionID(),
      AMT_C_U8_CCAMSG_OPCODE_ERROR);
   oSendErrorMessage.vSetWord(u16ErrorCode);
   _pService->ePostMessage(&oSendErrorMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tU16 clFunction::getMajorVersion()
{
   tU16 major = 0;
   tU16 minor = 0;
   tU16 patch = 0;
   _pService->bGetServiceVersion(_pService->u16GetServiceID(), major, minor, patch);
   return major;
}
