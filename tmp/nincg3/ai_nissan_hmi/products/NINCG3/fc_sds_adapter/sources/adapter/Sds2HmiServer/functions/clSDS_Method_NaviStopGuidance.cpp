/*********************************************************************//**
 * \file       clSDS_Method_NaviStopGuidance.cpp
 *
 * clSDS_Method_NaviStopGuidance method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_NaviStopGuidance.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_NaviStopGuidance::~clSDS_Method_NaviStopGuidance()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_NaviStopGuidance::clSDS_Method_NaviStopGuidance(ahl_tclBaseOneThreadService* pService,
      ::boost::shared_ptr< NavigationServiceProxy > pSds2NaviProxy
      , GuiService& guiService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_NAVISTOPGUIDANCE, pService)
   , _sds2NaviProxy(pSds2NaviProxy)
   , _guiService(guiService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviStopGuidance::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   _sds2NaviProxy->sendCancelRouteGuidanceRequest(*this);
   _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_PASSIVE);
   vSendMethodResult();
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviStopGuidance::onCancelRouteGuidanceResponse(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< CancelRouteGuidanceResponse >& /*response*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviStopGuidance::onCancelRouteGuidanceError(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< CancelRouteGuidanceError >& /*error*/)
{
}
