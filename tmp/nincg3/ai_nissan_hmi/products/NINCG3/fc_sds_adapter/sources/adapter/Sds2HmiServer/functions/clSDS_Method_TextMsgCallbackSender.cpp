/*********************************************************************//**
 * \file       clSDS_Method_TextMsgCallbackSender.cpp
 *
 * clSDS_Method_TextMsgCallbackSender method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_TextMsgCallbackSender.h"
#include "external/sds2hmi_fi.h"
#include "application/clSDS_Iconizer.h"
#include "application/clSDS_TextMsgContent.h"


using namespace MOST_Tel_FI;
using namespace most_Tel_fi_types;


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_TextMsgCallbackSender::~clSDS_Method_TextMsgCallbackSender()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_TextMsgCallbackSender::clSDS_Method_TextMsgCallbackSender(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > pTelProxy)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_TEXTMSGCALLBACKSENDER, pService)
   , _telephoneProxy(pTelProxy)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_TextMsgCallbackSender::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   std::string phoneNumber = clSDS_TextMsgContent::getPhoneNumber();
   phoneNumber = clSDS_Iconizer::oRemoveIconPrefix(phoneNumber);
   if (_telephoneProxy->isAvailable())
   {
      _telephoneProxy->sendDialStart(*this, phoneNumber, T_e8_TelEchoCancellationNoiseReductionSetting__e8ECNR_NOCHANGE);
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}


void clSDS_Method_TextMsgCallbackSender::onDialError(
   const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< DialError >& /*error*/)
{
   vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
}


void clSDS_Method_TextMsgCallbackSender::onDialResult(
   const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< DialResult >& /*result*/)
{
   vSendMethodResult();
}
