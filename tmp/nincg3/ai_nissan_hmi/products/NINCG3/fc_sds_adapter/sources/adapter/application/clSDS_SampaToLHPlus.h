/*********************************************************************//**
 * \file       clSDS_SampaToLHPlus.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_SampaToLHPlus_h
#define clSDS_SampaToLHPlus_h


#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include <stl_pif.h>


#define RULES_EN 23
#define RULES_FR 14
#define RULES_SP 8


class clSDS_SampaToLHPlus
{
   public:
      clSDS_SampaToLHPlus();
      virtual ~clSDS_SampaToLHPlus();
      static std::string translate(const int language_id, const ::std::string& sampa_word_string);

   private:
      static std::string translate_EN(const ::std::string& sampa_word_string);
      static std::string translate_FR(const ::std::string& sampa_word_string);
      static std::string translate_SP(const ::std::string& sampa_word_string);
      static std::string getTranslation_EN(const char* c);
      static std::string getTranslation_FR(const char* c);
      static std::string getTranslation_SP(const char* c);
};


#endif
