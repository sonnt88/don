/*********************************************************************//**
 * \file       clSDS_RecentCallsList.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_RecentCallsList.h"
#include "application/clSDS_Iconizer.h"
#include "application/clSDS_PhoneNumberFormatter.h"
#include "application/clSDS_StringVarList.h"
#include "view_db/Sds_TextDB.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_RecentCallsList.cpp.trc.h"
#endif


using namespace MOST_PhonBk_FI;
using namespace most_PhonBk_fi_types;
using namespace most_BTSet_fi_types;


/**************************************************************************//**
 *Destructor
 ******************************************************************************/
clSDS_RecentCallsList::~clSDS_RecentCallsList()
{
}


/**************************************************************************//**
 *Constructor
 ******************************************************************************/
clSDS_RecentCallsList::clSDS_RecentCallsList(::boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy> pPhoneBookProxy)
   : _recentCallList()
   , _phoneBookProxy(pPhoneBookProxy)
   , _downloadStatus(0)
   , _recentCallHistorySize(0)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tU32 clSDS_RecentCallsList::u32GetSize()
{
   return _recentCallList.size();
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::vector<clSDS_ListItems> clSDS_RecentCallsList::oGetItems(tU32 u32StartIndex, tU32 u32EndIndex)
{
   std::vector<clSDS_ListItems> oListItems;
   for (tU32 u32Index = u32StartIndex; u32Index < u32EndIndex; u32Index++)
   {
      clSDS_ListItems oListItem;
      oListItem.oDescription.szString = oGetItem(u32Index);
      oListItems.push_back(oListItem);
   }
   return oListItems;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_RecentCallsList::oGetItem(tU32 u32Index)
{
   if (oGetName(u32Index) != "")
   {
      return clSDS_Iconizer::oAddIconInfix(oGetName(u32Index), (tU8)u32GetNumberType(u32Index),
                                           clSDS_PhoneNumberFormatter::oFormatNumber(oGetPhonenumber(u32Index)));
   }
   else if (oGetPhonenumber(u32Index) != "")
   {
      return oGetPhonenumber(u32Index);
   }
   else
   {
      return Sds_TextDB_vGetText(SDS_TEXT_PRIVATE_NUMBER);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_RecentCallsList::oGetName(tU32 u32Index)
{
   if (u32Index < _recentCallList.size())
   {
      return _recentCallList[u32Index].Name;
   }
   return "";
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_RecentCallsList::oGetPhonenumber(tU32 u32Index)
{
   if (u32Index < _recentCallList.size())
   {
      return _recentCallList[u32Index].PhoneNumber;
   }
   return "";
}


/***********************************************************************//**
 *
 ***************************************************************************/
tU32 clSDS_RecentCallsList::u32GetNumberType(tU32 /*u32Index*/) const
{
   return 0;
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_RecentCallsList::bSelectElement(tU32 u32SelectedIndex)
{
   if (u32SelectedIndex > 0)
   {
      clSDS_StringVarList::vSetVariable("$(ContactName)", oGetName(u32SelectedIndex - 1));
      clSDS_StringVarList::vSetVariable("$(PhNumber)", oGetPhonenumber(u32SelectedIndex - 1));
      clSDS_StringVarList::vSetVariable("$(Contact)", oGetItem(u32SelectedIndex - 1));
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_RecentCallsList::oGetSelectedItem(tU32 u32Index)
{
   if (u32Index > 0)
   {
      return oGetPhonenumber(u32Index - 1);
   }
   return "";
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_RecentCallsList::setRecentCallList(const most_PhonBk_fi_types::T_PhonBkCallHistoryListSliceResult& callHistoryListSliceResult)
{
   most_PhonBk_fi_types::T_PhonBkCallHistoryListSliceResultItem listItem;
   RecentCall callEntry;
   _recentCallList.clear();
   for (tU32 listCount = 0; listCount != callHistoryListSliceResult.size(); ++listCount)
   {
      listItem = callHistoryListSliceResult[listCount];
      if (listItem.getSFirstName() != "" && listItem.getSLastName() != "")
      {
         callEntry.Name = listItem.getSFirstName();
         callEntry.Name.append(" ");
         callEntry.Name.append(listItem.getSLastName());
      }
      else if (listItem.getSFirstName() != "")
      {
         callEntry.Name = listItem.getSFirstName();
      }
      else
      {
         callEntry.Name = listItem.getSLastName();
      }

      callEntry.PhoneNumber = listItem.getSPhoneNumber();
      _recentCallList.push_back(callEntry);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_RecentCallsList::getLastDialedNumber() const
{
   if (_recentCallList.size())
   {
      return _recentCallList[0].PhoneNumber;
   }
   return "";
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_RecentCallsList::getOutgoingCallHistoryList()
{
   if ((_downloadStatus) && (_phoneBookProxy->hasDevicePhoneBookFeatureSupport()))
   {
      // create call history list with of type Outgoing calls sorted by Date and Time
      _phoneBookProxy->sendCreateCallHistoryListStart(*this
            , _phoneBookProxy->getDevicePhoneBookFeatureSupport().getU8DeviceHandle()
            , T_e8_PhonBkCallHistoryType__e8OCH, T_e8_PhonBkCallHistorySortType__e8CH_SORT_DATETIME);
   }
   else
   {
      notifyListObserver();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_RecentCallsList::getIncomingCallHistoryList()
{
   if ((_downloadStatus) && (_phoneBookProxy->hasDevicePhoneBookFeatureSupport()))
   {
      // create call history list with of type Incoming calls sorted by Date and Time
      _phoneBookProxy->sendCreateCallHistoryListStart(*this
            , _phoneBookProxy->getDevicePhoneBookFeatureSupport().getU8DeviceHandle()
            , T_e8_PhonBkCallHistoryType__e8ICH, T_e8_PhonBkCallHistorySortType__e8CH_SORT_DATETIME);
   }
   else
   {
      notifyListObserver();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_RecentCallsList::getMissedCallHistoryList()
{
   if ((_downloadStatus) && (_phoneBookProxy->hasDevicePhoneBookFeatureSupport()))
   {
      // create call history list with of type Missed calls sorted by Date and Time
      _phoneBookProxy->sendCreateCallHistoryListStart(*this
            , _phoneBookProxy->getDevicePhoneBookFeatureSupport().getU8DeviceHandle()
            , T_e8_PhonBkCallHistoryType__e8MCH, T_e8_PhonBkCallHistorySortType__e8CH_SORT_DATETIME);
   }
   else
   {
      notifyListObserver();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_RecentCallsList::updateHeadlineTagforRedial()
{
   bSelectElement(1);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_RecentCallsList::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _phoneBookProxy)
   {
      _phoneBookProxy->sendDownloadStateUpReg(*this);
      _phoneBookProxy->sendDevicePhoneBookFeatureSupportUpReg(*this);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_RecentCallsList::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _phoneBookProxy)
   {
      _phoneBookProxy->sendDownloadStateRelUpRegAll();
      _phoneBookProxy->sendDevicePhoneBookFeatureSupportRelUpRegAll();
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_RecentCallsList::onDownloadStateStatus(
   const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< DownloadStateStatus >& status)
{
   if (status->getODownloadStateStream().size())
   {
      const T_PhonBkDownloadStateStreamItem& oDownloadStatus  = status->getODownloadStateStream()[0];
      if (T_e8_PhonBkRecentCallListDownloadState__e8RCDS_COMPLETE == oDownloadStatus.getE8RecentCallListDownloadState())
      {
         _downloadStatus = 1;
      }
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_RecentCallsList::onDownloadStateError(
   const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< DownloadStateError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_RecentCallsList::onDevicePhoneBookFeatureSupportError(
   const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< DevicePhoneBookFeatureSupportError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_RecentCallsList::onDevicePhoneBookFeatureSupportStatus(
   const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< DevicePhoneBookFeatureSupportStatus >& /*status*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_RecentCallsList::onCreateCallHistoryListResult(
   const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< CreateCallHistoryListResult >& result)
{
   uint16 listHandle = result->getU16ListHandle();
   _recentCallHistorySize = result->getU16ListLength();
   if (_recentCallHistorySize)
   {
      _phoneBookProxy->sendRequestSliceCallHistoryListStart(*this, listHandle, 0, _recentCallHistorySize);
   }
   else
   {
      _recentCallList.clear();
      notifyListObserver();
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_RecentCallsList::onCreateCallHistoryListError(
   const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< CreateCallHistoryListError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_RecentCallsList::onRequestSliceCallHistoryListResult(
   const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< RequestSliceCallHistoryListResult >& result)
{
   if (result->getOCallHistoryListSliceResult().size())
   {
      const T_PhonBkCallHistoryListSliceResult& callHistoryListSliceResult = result->getOCallHistoryListSliceResult();
      setRecentCallList(callHistoryListSliceResult);
      updateHeadlineTagforRedial();
      notifyListObserver();
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_RecentCallsList::onRequestSliceCallHistoryListError(
   const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< RequestSliceCallHistoryListError >& /*error*/)
{
}


tVoid clSDS_RecentCallsList::vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType enListType)
{
   switch (enListType)
   {
      case sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_PHONE_MISSED_CALLS:
         getMissedCallHistoryList();
         break;

      case sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_PHONE_ANSWERED_CALLS:
         getIncomingCallHistoryList();
         break;

      case sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_PHONE_DIALLED_CALLS:
         getOutgoingCallHistoryList();
         break;

      default:
         notifyListObserver();
         break;
   }
}


void clSDS_RecentCallsList::phoneStatusChanged(uint8 /*deviceHandle*/, most_BTSet_fi_types::T_e8_BTSetDeviceStatus status)
{
   if (status  == T_e8_BTSetDeviceStatus__e8DEVICE_DISCONNECTED)
   {
      _recentCallList.clear();
      _downloadStatus = false;
   }
}


std::vector<sds2hmi_fi_tcl_HMIElementDescription> clSDS_RecentCallsList::getHmiElementDescription(int index)
{
   std::vector<sds2hmi_fi_tcl_HMIElementDescription> hmiElementDescriptionList;
   sds2hmi_fi_tcl_HMIElementDescription hmiElementDescription;
   if (oGetName(index - 1) != "")
   {
      hmiElementDescription.DescriptorTag = "CONTACTNAME";
      hmiElementDescription.DescriptorValue = oGetName(index - 1).c_str();
      hmiElementDescriptionList.push_back(hmiElementDescription);
   }
   else if (oGetPhonenumber(index - 1) != "")
   {
      hmiElementDescription.DescriptorTag = "PHONENUMBER";
      hmiElementDescription.DescriptorValue = oGetPhonenumber(index - 1).c_str();
      hmiElementDescriptionList.push_back(hmiElementDescription);
   }
   else
   {
      ETG_TRACE_USR1(("clSDS_RecentCallsList::getHmiElementDescription : no ContactInfo"));
   }
   return hmiElementDescriptionList;
}
