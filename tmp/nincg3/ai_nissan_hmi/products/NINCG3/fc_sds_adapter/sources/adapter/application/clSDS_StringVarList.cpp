/*********************************************************************//**
 * \file       clSDS_StringVarList.cpp
 *
 * clSDS_StringVarList class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_StringVarList.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_StringVarList.cpp.trc.h"
#endif

bpstl::map<bpstl::string, bpstl::string> clSDS_StringVarList::_mapVariableList;


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_StringVarList::~clSDS_StringVarList()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_StringVarList::clSDS_StringVarList()
{
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_StringVarList::vSetVariable(const bpstl::string& oVariable, const bpstl::string& oValue)
{
   _mapVariableList[oVariable] = oValue;
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::string clSDS_StringVarList::oResolveVariables(bpstl::string oString)
{
   bpstl::map<bpstl::string, bpstl::string>::const_iterator iter;
   for (iter = _mapVariableList.begin(); iter != _mapVariableList.end(); ++iter)
   {
      if (oString.find(iter->first) != std::string::npos)
      {
         oString.replace(oString.find(iter->first), iter->first.size(), iter->second);
      }
   }
   return oString;
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::map<bpstl::string, bpstl::string> clSDS_StringVarList::getVariableList()
{
   return _mapVariableList;
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_StringVarList::printStaticVariables()
{
   bpstl::map<bpstl::string, bpstl::string>::const_iterator it;
   for (it = _mapVariableList.begin(); it != _mapVariableList.end(); ++it)
   {
      ETG_TRACE_USR1(("clSDS_StringVarList::printStaticVariables() first = %s", (*it).first.c_str()));
      ETG_TRACE_USR1(("clSDS_StringVarList::printStaticVariables() second = %s", (*it).second.c_str()));
   }
}
