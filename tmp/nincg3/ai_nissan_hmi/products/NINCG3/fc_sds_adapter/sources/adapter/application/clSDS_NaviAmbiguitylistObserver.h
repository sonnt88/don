/*********************************************************************//**
 * \file       clSDS_NaviAmbiguitylistObserver.h
 *
 * NaviAmbiguitylistObserver
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef clSDS_NaviAmbiguitylistObserver_h_
#define clSDS_NaviAmbiguitylistObserver_h_

#include "external/sds2hmi_fi.h"

class clSDS_NaviAmbiguitylistObserver
{
   public:
      clSDS_NaviAmbiguitylistObserver();
      virtual ~clSDS_NaviAmbiguitylistObserver();
      virtual void vAmbiguityListResolved() = 0;
};


#endif
