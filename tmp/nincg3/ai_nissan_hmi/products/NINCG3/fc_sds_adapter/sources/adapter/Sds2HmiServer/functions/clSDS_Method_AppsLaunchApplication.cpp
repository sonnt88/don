/*********************************************************************//**
 * \file       clSDS_Method_AppsLaunchApplication.cpp
 *
 * clSDS_Method_AppsLaunchApplication method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_AppsLaunchApplication.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_AppsLaunchApplication.cpp.trc.h"
#endif


using namespace most_Tel_fi_types_Extended;


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_AppsLaunchApplication::~clSDS_Method_AppsLaunchApplication()
{
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_AppsLaunchApplication::clSDS_Method_AppsLaunchApplication(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > pSds2TelProxy)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_APPSLAUNCHAPPLICATION, pService)
   , _telephoneProxy(pSds2TelProxy)
   , _voiceRecActive(false)
   , _voiceRecSupported(false)
   , _siriStatus(T_e8_TelSiriAvailabilityState__e8NOT_AVAILABLE)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_AppsLaunchApplication::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgAppsLaunchApplicationMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);

   // TODO jnd2hi: remove all members and use proxy members directly
   if (_voiceRecSupported)
   {
      _telephoneProxy->sendBTDeviceVoiceRecognitionExtendedPureSet(true);
   }
   else
   {
      if (_siriStatus == T_e8_TelSiriAvailabilityState__e8AVAILABLE_ENABLED)
      {
         _telephoneProxy->sendBTDeviceVoiceRecognitionExtendedPureSet(true);
      }
      // TODO jnd2hi: is it really correct to send true/false one after the other
      _telephoneProxy->sendBTDeviceVoiceRecognitionExtendedPureSet(false);
   }

   // TODO jnd2hi: what is the meaning of this request if AppId is not used? Is there an implicit meaning of the method?
   ETG_TRACE_USR1(("AppId = %d", oMessage.AppId));

   vSendMethodResult();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_AppsLaunchApplication::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _telephoneProxy)
   {
      _telephoneProxy->sendBTDeviceVoiceRecognitionExtendedUpReg(*this);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_AppsLaunchApplication::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _telephoneProxy)
   {
      _telephoneProxy->sendBTDeviceVoiceRecognitionRelUpRegAll();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_AppsLaunchApplication::onBTDeviceVoiceRecognitionExtendedStatus(
   const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_Tel_FI::BTDeviceVoiceRecognitionExtendedStatus >& status)
{
   _voiceRecActive = status->getBBTDeviceVoiceRecActive();
   _voiceRecSupported = status->getBBTDeviceVoiceRecSupported();
   _siriStatus = status->getE8SiriAvailabilityState();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_AppsLaunchApplication::onBTDeviceVoiceRecognitionExtendedError(
   const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_Tel_FI::BTDeviceVoiceRecognitionExtendedError >& /*error*/)
{
}
