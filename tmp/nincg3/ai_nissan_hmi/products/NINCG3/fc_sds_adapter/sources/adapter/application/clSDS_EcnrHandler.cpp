/*********************************************************************//**
 * \file       clSDS_EcnrHandler.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_EcnrHandler.h"


clSDS_EcnrHandler::clSDS_EcnrHandler(::boost::shared_ptr< ServiceProxy > ecnrProxy)
   : _ecnrProxy(ecnrProxy)
{
}


clSDS_EcnrHandler::~clSDS_EcnrHandler()
{
}


void clSDS_EcnrHandler::initialize()
{
   if (_ecnrProxy->isAvailable())
   {
      _ecnrProxy->sendEcnrInitializeRequest(*this, ECNR_APP_ID, ECNR_CONFIG_ID);
   }
}


void clSDS_EcnrHandler::destroy()
{
   if (_ecnrProxy->isAvailable())
   {
      _ecnrProxy->sendEcnrDestroyRequest(*this, ECNR_APP_ID);
   }
}


void clSDS_EcnrHandler::startAudio()
{
   if (_ecnrProxy->isAvailable())
   {
      _ecnrProxy->sendEcnrStartAudioExtRequest(*this, ECNR_APP_ID, 1);
   }
}


void clSDS_EcnrHandler::stopAudio()
{
   if (_ecnrProxy->isAvailable())
   {
      _ecnrProxy->sendEcnrStopAudioRequest(*this, ECNR_APP_ID);
   }
}


void clSDS_EcnrHandler::onEcnrInitializeError(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< EcnrInitializeError >& /*error*/)
{
}


void clSDS_EcnrHandler::onEcnrInitializeResponse(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< EcnrInitializeResponse >& /*response*/)
{
}


void clSDS_EcnrHandler::onEcnrDestroyError(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< EcnrDestroyError >& /*error*/)
{
}


void clSDS_EcnrHandler::onEcnrDestroyResponse(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< EcnrDestroyResponse >& /*response*/)
{
}


void clSDS_EcnrHandler::onEcnrStartAudioExtError(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< EcnrStartAudioExtError >& /*error*/)
{
}


void clSDS_EcnrHandler::onEcnrStartAudioExtResponse(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< EcnrStartAudioExtResponse >& /*response*/)
{
}


void clSDS_EcnrHandler::onEcnrStopAudioError(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< EcnrStopAudioError >& /*error*/)
{
}


void clSDS_EcnrHandler::onEcnrStopAudioResponse(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< EcnrStopAudioResponse >& /*response*/)
{
}
