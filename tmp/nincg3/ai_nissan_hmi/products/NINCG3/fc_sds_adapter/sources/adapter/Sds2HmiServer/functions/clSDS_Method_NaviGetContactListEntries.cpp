/*********************************************************************//**
 * \file       clSDS_Method_NaviGetContactListEntries.cpp
 *
 * clSDS_Method_NaviGetContactListEntries method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_NaviGetContactListEntries.h"
#include "external/sds2hmi_fi.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_NaviGetContactListEntries::~clSDS_Method_NaviGetContactListEntries()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_NaviGetContactListEntries::clSDS_Method_NaviGetContactListEntries(ahl_tclBaseOneThreadService* pService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_NAVIGETCONTACTLISTENTRIES, pService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviGetContactListEntries::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgNaviGetContactListEntriesMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);

   sds2hmi_sdsfi_tclMsgNaviGetContactListEntriesMethodResult oResult;
   vSendMethodResult(oResult);
}
