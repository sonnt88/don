/*********************************************************************//**
 * \file       clServerMethod.cpp
 *
 * Base class for SDS server method implementations.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/framework/clServerMethod.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_CCA_FW
#include "trcGenProj/Header/clServerMethod.cpp.trc.h"
#endif


/**************************************************************************//**
* Destructor
******************************************************************************/
clServerMethod::~clServerMethod()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clServerMethod::clServerMethod(tU16 u16FunctionID, ahl_tclBaseOneThreadService* pService)
   : clFunction(u16FunctionID, pService)
   , _u16DestAppID(0)
   , _u16RegisterID(0)
   , _u16CmdCtr(0)
{
}


/**************************************************************************//**
* Performs opcode check for incoming message. Delegates message handling to
* derived class.
******************************************************************************/
tVoid clServerMethod::vHandleMessage(amt_tclServiceData* pInMsg)
{
   ETG_TRACE_USR1(("rx appid=%04x function=%04x regid=%d opcode=%d cmdctr=%d ", pInMsg->u16GetSourceAppID(), pInMsg->u16GetFunctionID(), pInMsg->u16GetRegisterID(), pInMsg->u8GetOpCode(), pInMsg->u16GetCmdCounter()));
   switch (pInMsg->u8GetOpCode())
   {
      case AMT_C_U8_CCAMSG_OPCODE_METHODSTART:
         vSaveResponseParameters(pInMsg);
         vMethodStart(pInMsg);
         break;

      default:
         break;
   }
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clServerMethod::vSaveResponseParameters(const amt_tclServiceData* pInMessage)
{
   _u16DestAppID = pInMessage->u16GetSourceAppID();
   _u16RegisterID = pInMessage->u16GetRegisterID();
   _u16CmdCtr = pInMessage->u16GetCmdCounter();
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clServerMethod::vSendMethodResult()
{
   vSendEmptyMessage(
      _u16DestAppID,
      _u16CmdCtr,
      _u16RegisterID,
      u16GetFunctionID(),
      AMT_C_U8_CCAMSG_OPCODE_METHODRESULT);
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clServerMethod::vSendMethodResult(const fi_tclTypeBase& oOutData)
{
   vSendMessage(
      _u16DestAppID,
      _u16CmdCtr,
      _u16RegisterID,
      oOutData,
      u16GetFunctionID(),
      AMT_C_U8_CCAMSG_OPCODE_METHODRESULT);
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clServerMethod::vSendErrorMessage(tU16 u16ErrorCode)
{
   clFunction::vSendErrorMessage(
      _u16DestAppID,
      _u16CmdCtr,
      _u16RegisterID,
      u16ErrorCode);
}
