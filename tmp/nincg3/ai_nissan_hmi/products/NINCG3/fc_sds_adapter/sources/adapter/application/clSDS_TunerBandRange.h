/*********************************************************************//**
 * \file       clSDS_TunerBandRange.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef clSDS_TunerBandRange_h
#define clSDS_TunerBandRange_h

#include "tuner_main_fiProxy.h"

class clSDS_TunerBandRange
   : public asf::core::ServiceAvailableIF
   , public tuner_main_fi::FID_TUN_G_AVAILABLE_BAND_RANGECallbackIF
{
   public:
      clSDS_TunerBandRange(::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy > tuner_fi_proxy);
      virtual ~clSDS_TunerBandRange();

      virtual void onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                               const asf::core::ServiceStateChange& stateChange);
      virtual void onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                                 const asf::core::ServiceStateChange& stateChange);

      virtual void onFID_TUN_G_AVAILABLE_BAND_RANGEError(const ::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy >& proxy,
            const ::boost::shared_ptr< tuner_main_fi::FID_TUN_G_AVAILABLE_BAND_RANGEError >& error);
      virtual void onFID_TUN_G_AVAILABLE_BAND_RANGEStatus(const ::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy >& proxy,
            const ::boost::shared_ptr< tuner_main_fi::FID_TUN_G_AVAILABLE_BAND_RANGEStatus >& status);

   private:
      std::string formatFMFrequency(uint32 fmFreq) const;

      ::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy > _tunerFiProxy;
};


#endif /* clSDS_TunerBandRange_h */
