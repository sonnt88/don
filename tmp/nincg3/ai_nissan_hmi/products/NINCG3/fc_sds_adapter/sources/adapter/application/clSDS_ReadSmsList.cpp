/*********************************************************************//**
 * \file       clSDS_ReadSmsList.cpp
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "clSDS_ReadSmsList.h"
#include "application/clSDS_StringVarList.h"
#include "clSDS_ReadTextListObserver.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_ReadSmsList.cpp.trc.h"
#endif


#define MAX_SIZE_OF_SMS_LIST 20


using namespace MOST_Msg_FI;
using namespace most_Msg_fi_types;
using namespace clock_main_fi;
using namespace most_BTSet_fi_types;


/***********************************************************************//**
 *
 ***************************************************************************/
clSDS_ReadSmsList::clSDS_ReadSmsList(
   ::boost::shared_ptr< MOST_Msg_FIProxy > pSds2MsgProxy,
   ::boost::shared_ptr< ::clock_main_fi::Clock_main_fiProxy > pClockproxy,
   clSDS_FormatTimeDate* pFormatTimeDate)

   : _messageProxy(pSds2MsgProxy)
   , _clockproxy(pClockproxy)
   , _pFormatTimeDate(pFormatTimeDate)
   , _smsListInfo()
   , _index(0)
   , _listHandle(0)
   , _deviceHandle(0)
   , _currentDate()
   , _pReadTextListObserver(NULL)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
clSDS_ReadSmsList::~clSDS_ReadSmsList()
{
   _pFormatTimeDate = NULL;
   _pReadTextListObserver = NULL;
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::setTextListObserver(clSDS_ReadTextListObserver* obs)
{
   if (obs != NULL)
   {
      _pReadTextListObserver = obs;
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _messageProxy)
   {
      _messageProxy->sendMessagingDeviceConnectionUpReg(*this);
      _messageProxy->sendMessageListChangeUpReg(*this);
   }
   if (proxy == _clockproxy)
   {
      _clockproxy->sendLocalTimeDate_MinuteUpdateUpReg(*this);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _messageProxy)
   {
      _messageProxy->sendMessagingDeviceConnectionRelUpRegAll();
      _messageProxy->sendMessageListChangeRelUpRegAll();
   }
   if (proxy == _clockproxy)
   {
      _clockproxy->sendLocalTimeDate_MinuteUpdateRelUpRegAll();
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::onMessagingDeviceConnectionError(
   const ::boost::shared_ptr< MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MessagingDeviceConnectionError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::onMessagingDeviceConnectionStatus(
   const ::boost::shared_ptr< MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MessagingDeviceConnectionStatus >& status)
{
   if (_deviceHandle && _deviceHandle == status->getU8DeviceHandle())
   {
      ETG_TRACE_USR1(("clSDS_ReadSmsList::onMessagingDeviceConnectionStatus: deviceHandle - %d", _deviceHandle));
      if (status->getBSMSSupport())
      {
         ;
         _messageProxy->sendCreateMessageListStart(*this, _deviceHandle, ::T_e8_MsgFolderType__e8MSG_FOLDER_INBOX, 0,
               ::T_e8_MsgMessageListType__e8MSG_LIST_SMS_MMS, ::T_e8_MsgMessageListSortType__e8MSG_LIST_SORT_TIMESTAMP,
               ::T_e8_MsgMessageListFilterType__e8MSG_LIST_FILTER_ALL);
      }
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::onCreateMessageListError(
   const ::boost::shared_ptr< MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ::CreateMessageListError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::onCreateMessageListResult(
   const ::boost::shared_ptr< MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ::CreateMessageListResult >& result)
{
   _listHandle = result->getU16ListHandle();
   requestSliceMessageList();
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::requestSliceMessageList()
{
   _messageProxy->sendRequestSliceMessageListStart(*this, _listHandle, 0, MAX_SIZE_OF_SMS_LIST);
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::onRequestSliceMessageListError(
   const ::boost::shared_ptr< MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ::RequestSliceMessageListError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::onRequestSliceMessageListResult(
   const ::boost::shared_ptr< MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ::RequestSliceMessageListResult >& result)
{
   _smsListInfo.clear();
   const ::T_MsgMessageListSliceResult& resultList = result->getOMessageListSliceResult();
   struct smsInfo smsItem;
   for (std::vector< ::T_MsgMessageListSliceResultItem >::const_iterator itr = resultList.begin(); itr != resultList.end(); ++itr)
   {
      std::string formattedString("");
      smsItem.messageHandle.setU8DeviceHandle(itr->getOMessageHandle().getU8DeviceHandle());
      smsItem.messageHandle.setU32MsgHandleUpper(itr->getOMessageHandle().getU32MsgHandleUpper());
      smsItem.messageHandle.setU32MsgHandleLower(itr->getOMessageHandle().getU32MsgHandleLower());

      _pFormatTimeDate->formatTimeDate(itr->getOMessageDateTime().getOMessageTime().getSHours(),
                                       itr->getOMessageDateTime().getOMessageTime().getSMinutes(),
                                       itr->getOMessageDateTime().getOMessageDate().getSCldrDay(),
                                       itr->getOMessageDateTime().getOMessageDate().getSCldrMonth(),
                                       itr->getOMessageDateTime().getOMessageDate().getSCldrYear(), formattedString);

      smsItem.formattedTimeDate = formattedString;

      if (itr->getSFirstName().size() || itr->getSLastName().size())
      {
         smsItem.contactName = itr->getSFirstName() + itr->getSLastName();
      }
      smsItem.phoneNumber = itr->getSPhoneNumber();
      _smsListInfo.push_back(smsItem);
   }
   // update when change in list size
   if (_pReadTextListObserver != NULL)
   {
      _pReadTextListObserver->textListChanged(_smsListInfo.size());
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::onReleaseMessageListError(
   const ::boost::shared_ptr< MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ReleaseMessageListError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::onReleaseMessageListResult(
   const ::boost::shared_ptr< MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ReleaseMessageListResult >& /*result*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::onMessageListChangeError(
   const ::boost::shared_ptr< MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MessageListChangeError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::onMessageListChangeStatus(
   const ::boost::shared_ptr< MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MessageListChangeStatus >& status)
{
   // update list when new messages are received
   if ((status->getE8ListChangeType() == (T_e8_MsgListChangeType__e8LCH_ITEMS_ADDED || T_e8_MsgListChangeType__e8LCH_ITEMS_REMOVED))
         && (_listHandle == status->getU16ListHandle()))
   {
      requestSliceMessageList();
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::onLocalTimeDate_MinuteUpdateError(
   const ::boost::shared_ptr< Clock_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< LocalTimeDate_MinuteUpdateError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::onLocalTimeDate_MinuteUpdateStatus(
   const ::boost::shared_ptr< Clock_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< LocalTimeDate_MinuteUpdateStatus >& status)
{
   if (_currentDate.day)
   {
      // update sms list when system DDMMYYYY is changed
      if (_currentDate.day != status->getU8Day() || _currentDate.month != status->getU8Month() || _currentDate.year != status->getS16Year())
      {
         _currentDate.day = status->getU8Day();
         _currentDate.month = status->getU8Month();
         _currentDate.year = status->getS16Year();
         requestSliceMessageList();
      }
   }
   else
   {
      _currentDate.day = status->getU8Day();
      _currentDate.month = status->getU8Month();
      _currentDate.year = status->getS16Year();
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tU32 clSDS_ReadSmsList::u32GetSize()
{
   ETG_TRACE_USR1(("clSDS_ReadSmsList::u32GetSize: %d", _smsListInfo.size()));
   return _smsListInfo.size();
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::vector<clSDS_ListItems> clSDS_ReadSmsList::oGetItems(tU32 u32StartIndex, tU32 u32EndIndex)
{
   std::vector<clSDS_ListItems> oListItems;
   for (tU32 u32Index = u32StartIndex; u32Index < u32EndIndex; u32Index++)
   {
      clSDS_ListItems oListItem;
      oListItem.oDescription.szString = oGetItem(u32Index);
      oListItems.push_back(oListItem);
   }
   return oListItems;
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_ReadSmsList::oGetItem(tU32 u32Index)
{
   std::string smsListElement;
   if (u32Index < _smsListInfo.size())
   {
      if (!_smsListInfo[u32Index].contactName.empty())
      {
         smsListElement = _smsListInfo[u32Index].contactName + "   " + _smsListInfo[u32Index].formattedTimeDate;
      }
      else
      {
         smsListElement = _smsListInfo[u32Index].phoneNumber + "   " +  _smsListInfo[u32Index].formattedTimeDate;;
      }
   }
   return smsListElement;
}


/***********************************************************************//**
 *
 ***************************************************************************/
tBool clSDS_ReadSmsList::bSelectElement(tU32 u32SelectedIndex)
{
   if (u32SelectedIndex <= _smsListInfo.size())
   {
      _index = u32SelectedIndex - 1;
      clSDS_StringVarList::vSetVariable("$(ContactName)", _smsListInfo[u32SelectedIndex - 1].contactName);
      clSDS_StringVarList::vSetVariable("$(DateTime)", _smsListInfo[u32SelectedIndex - 1].formattedTimeDate);
      return true;
   }
   else
   {
      return false;
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_ReadSmsList::vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType)
{
   if (listType == sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_PHONE_RECEIVED_SMS)
   {
      // reset index to zero
      _index = 0;
      notifyListObserver();
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
unsigned int clSDS_ReadSmsList::getSelectedSmsIndex() const
{
   if (_index < _smsListInfo.size())
   {
      return (_index + 1);
   }
   return 0;
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_ReadSmsList::getSelectedPhoneNumber() const
{
   if (_index < _smsListInfo.size())
   {
      return _smsListInfo[_index].phoneNumber;
   }
   return "";
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_ReadSmsList::getSelectedContactName() const
{
   if (_index < _smsListInfo.size())
   {
      return _smsListInfo[_index].contactName;
   }
   return "";
}


/***********************************************************************//**
 *
 ***************************************************************************/
T_MsgMessageHandle clSDS_ReadSmsList::getMessageHandle() const
{
   T_MsgMessageHandle messageHandle;
   if (_index < _smsListInfo.size())
   {
      return _smsListInfo[_index].messageHandle;
   }
   return messageHandle;
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::selectNextMessage()
{
   if (_index < (_smsListInfo.size() - 1))
   {
      ++_index;
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::selectPreviousMessage()
{
   if (_index > 0)
   {
      --_index;
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_ReadSmsList::getTimeDate() const
{
   if (_index < _smsListInfo.size())
   {
      return _smsListInfo[_index].formattedTimeDate;
   }
   return "";
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_ReadSmsList::phoneStatusChanged(uint8 deviceHandle, most_BTSet_fi_types::T_e8_BTSetDeviceStatus status)
{
   if (status == T_e8_BTSetDeviceStatus__e8DEVICE_CONNECTED_OSD)
   {
      ETG_TRACE_USR1(("clSDS_ReadSmsList::T_e8_BTSetDeviceStatus__e8DEVICE_CONNECTED_OSD DeviceID %d", deviceHandle));
      _deviceHandle = deviceHandle;
   }
   if (status == T_e8_BTSetDeviceStatus__e8DEVICE_CHANGED_TO_OSD)
   {
      ETG_TRACE_USR1(("clSDS_ReadSmsList::T_e8_BTSetDeviceStatus__e8DEVICE_CHANGED_TO_OSD DeviceID %d", deviceHandle));
      _deviceHandle = deviceHandle;
   }
   if (status == T_e8_BTSetDeviceStatus__e8DEVICE_NO_CHANGE)
   {
      ETG_TRACE_USR1(("clSDS_ReadSmsList::T_e8_BTSetDeviceStatus__e8DEVICE_NO_CHANGE DeviceID %d", deviceHandle));
      _deviceHandle = deviceHandle;
   }
   if (status  == T_e8_BTSetDeviceStatus__e8DEVICE_DISCONNECTED)
   {
      ETG_TRACE_USR1(("clSDS_ReadSmsList::onDeviceListStatus : T_e8_BTSetDeviceStatus__e8DEVICE_DISCONNECTED"));
      if (_messageProxy->isAvailable())
      {
         _messageProxy->sendReleaseMessageListStart(*this, _listHandle);
         _smsListInfo.clear();
      }
   }
}
