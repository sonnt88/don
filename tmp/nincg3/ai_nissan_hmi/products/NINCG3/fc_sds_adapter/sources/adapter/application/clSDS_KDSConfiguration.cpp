/*********************************************************************//**
 * \file       clSDS_KDSConfiguration.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "application/clSDS_KDSConfiguration.h"
#include "application/StringUtils.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_KDSConfiguration.cpp.trc.h"
#endif

#define DP_S_IMPORT_INTERFACE_FI
#include "dp_generic_if.h"


clSDS_KDSConfiguration::clSDS_KDSConfiguration()
{
}


clSDS_KDSConfiguration::~clSDS_KDSConfiguration()
{
}


unsigned char clSDS_KDSConfiguration::getKDSConfigGeneric(const std::string& strConfigElement, const std::string& strConfigItem, unsigned char configValue)
{
   std::string trace = "";
   trace.append("  strConfigElement = ");
   trace.append(strConfigElement);
   trace.append("  strConfigItem = ");
   trace.append(strConfigItem);

   if (DP_S32_NO_ERR == DP_s32GetConfigItem(strConfigElement.c_str(), strConfigItem.c_str(), &configValue, 1))
   {
      trace.append("  configValue = ");
      trace.append(StringUtils::toString(configValue));
   }
   else
   {
      trace.append("KDS ReadError");
   }

   ETG_TRACE_ERR(("clSDS_KDSConfiguration::getKDSConfigGeneric() %s", trace.c_str()));

   return configValue;
}


clSDS_KDSConfiguration::Country clSDS_KDSConfiguration::getMarketRegionType()
{
   return (Country)getKDSConfigGeneric("VehicleInformation", "DestinationRegion1", USA);
}


clSDS_KDSConfiguration::Variant clSDS_KDSConfiguration::getOpeningAnimationType()
{
   return (Variant)getKDSConfigGeneric("SystemConfiguration1", "OpeningAnimation", NISSAN);
}


unsigned char clSDS_KDSConfiguration::getDABMounted()
{
   return getKDSConfigGeneric("CMVariantCoding", "DAB");
}


unsigned char clSDS_KDSConfiguration::getXMTunerMounted()
{
   return getKDSConfigGeneric("CMVariantCoding", "XMTuner");
}


unsigned char clSDS_KDSConfiguration::getHEVInformationFunction()
{
   return getKDSConfigGeneric("SystemConfiguration1", "HEVInformationFunction");
}


unsigned char clSDS_KDSConfiguration::getBluetoothPhoneEnabled()
{
   return 1; // TODO jnd2hi: retrieve configuration from appropriate KDS entry
}


clSDS_KDSConfiguration::AllianceVoiceRecognitionModeType clSDS_KDSConfiguration::getAllianceVRModeType()
{
   return (AllianceVoiceRecognitionModeType)getKDSConfigGeneric("SystemConfiguration1", "VoiceRecognition", EXPERT);
}


unsigned char clSDS_KDSConfiguration::getVRBeep()
{
   return getKDSConfigGeneric("SystemConfiguration1", "VRBeepOnly");
}


unsigned char clSDS_KDSConfiguration::getVRShortPrompts()
{
   return getKDSConfigGeneric("SystemConfiguration1", "VRShortPrompts");
}


unsigned char clSDS_KDSConfiguration:: getVehicleEngineType()
{
   return getKDSConfigGeneric("VehicleInformation", "FuelType");
}


clSDS_KDSConfiguration::VehicleDistanceUnitType clSDS_KDSConfiguration::getVehicleDistanceUnitsType()
{
   return (VehicleDistanceUnitType)getKDSConfigGeneric("SystemConfiguration1", "SupportedDistanceUnits", EN_DIST_RESERVED);
}
