/*********************************************************************//**
 * \file       clSDS_MultipleDestinationsList.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_MultipleDestinationsList_h
#define clSDS_MultipleDestinationsList_h


#define SYSTEM_S_IMPORT_INTERFACE_STRING
#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#include "stl_pif.h"
#include "application/clSDS_List.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"

using namespace org::bosch::cm::navigation::NavigationService;

class clSDS_NaviAmbiguitylistObserver;

class clSDS_MultipleDestinationsList
   : public clSDS_List
   , public SdsGetRefinementListCallbackIF
{
   public:
      virtual ~clSDS_MultipleDestinationsList();
      clSDS_MultipleDestinationsList(::boost::shared_ptr< NavigationServiceProxy> pNaviProxy);

      virtual void onSdsGetRefinementListError(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< SdsGetRefinementListError >& error);
      virtual void onSdsGetRefinementListResponse(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< SdsGetRefinementListResponse >& response);

      tVoid vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType);
      tU32 u32GetSize();
      bpstl::vector<clSDS_ListItems> oGetItems(tU32 u32StartIndex, tU32 u32EndIndex);
      clSDS_ListItems oGetListItem(tU32 u32Index);
      tVoid vRequestAmbigListToNavi();
      tVoid vHandleAmbiguityList(::std::vector< RefinementListElement >& oAmbiguityList);
      std::string oGetItem(tU32 u32Index);
      tBool bSelectElement(tU32 u32SelectedIndex);
      void vRegisterAmbiguityListObserver(clSDS_NaviAmbiguitylistObserver* oAmbiguitylistObserver);
      void vNotifyAmbiguitylistObserver();

      std::vector<std::string> _addressList;
      boost::shared_ptr<NavigationServiceProxy> _navigationProxy;
      std::vector<clSDS_NaviAmbiguitylistObserver*> _oAmbiguityObserverlist;
};


#endif
