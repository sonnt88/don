/*********************************************************************//**
 * \file       clSDS_Method_PhoneSendDTMFDigits.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_PhoneSendDTMFDigits.h"
#include "external/sds2hmi_fi.h"


using namespace MOST_Tel_FI;
using namespace most_Tel_fi_types;


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_PhoneSendDTMFDigits::~clSDS_Method_PhoneSendDTMFDigits()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_PhoneSendDTMFDigits::clSDS_Method_PhoneSendDTMFDigits(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > pSds2TelProxy)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_PHONESENDDTMFDIGITS, pService)
   , _telephoneProxy(pSds2TelProxy)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_PhoneSendDTMFDigits::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgPhoneSendDTMFDigitsMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   if (_telephoneProxy->isAvailable())
   {
      _telephoneProxy->sendSendDTMFStart(*this, 0xFF, oMessage.nDTMFDigits.szValue);
   }
   vSendMethodResult();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneSendDTMFDigits::onSendDTMFError(
   const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ::MOST_Tel_FI::SendDTMFError >& /*error*/)
{
   vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneSendDTMFDigits::onSendDTMFResult(
   const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ::MOST_Tel_FI::SendDTMFResult >& /*result*/)
{
}
