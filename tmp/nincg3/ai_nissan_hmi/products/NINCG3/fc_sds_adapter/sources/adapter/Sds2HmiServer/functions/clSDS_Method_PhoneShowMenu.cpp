/*********************************************************************//**
 * \file       clSDS_Method_PhoneShowMenu.cpp
 *
 * clSDS_Method_PhoneShowMenu method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_PhoneShowMenu.h"
#include "application/GuiService.h"
#include "external/sds2hmi_fi.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_PhoneShowMenu.cpp.trc.h"
#endif


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_PhoneShowMenu::~clSDS_Method_PhoneShowMenu()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_PhoneShowMenu::clSDS_Method_PhoneShowMenu(
   ahl_tclBaseOneThreadService* pService,
   GuiService& guiService)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_PHONESHOWMENU, pService)
   , _guiService(guiService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_PhoneShowMenu::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgPhoneShowMenuMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);

   switch (oMessage.nMenuType.enType)
   {
      case sds2hmi_fi_tcl_e8_PHN_MenuType::FI_EN_SELECT_PHONE:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SELECT_PAIRED_DEVICE_LIST);
         break;

      default:
         ETG_TRACE_FATAL(("unsupported phone menu type '%d' requested by SDS", oMessage.nMenuType.enType));
         break;
   }

   vSendMethodResult();
}
