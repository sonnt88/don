/*********************************************************************//**
 * \file       TraceCommandAdapter.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/TraceCommandAdapter.h"
#include "application/EarlyStartupPlayer.h"
#include "application/GuiService.h"
#include "application/PopUpService.h"
#include "SdsAdapter_Trace.h"

#define ETG_ENABLED
#include "trace_interface.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_I_TTFIS_CMD_PREFIX      "SdsAdapter_"
#define ETG_I_TRACE_CHANNEL         TR_TTFIS_SAAL
#define ETG_DEFAULT_TRACE_CLASS     TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/TraceCommandAdapter.cpp.trc.h"
#endif


GuiService* TraceCommandAdapter::_pGuiService = NULL;
PopUpService* TraceCommandAdapter::_pPopUpService = NULL;
EarlyStartupPlayer* TraceCommandAdapter::_pEarlyStartupPlayer = NULL;


TraceCommandAdapter::TraceCommandAdapter(GuiService* guiService, PopUpService* popUpService, EarlyStartupPlayer* earlyStartupPlayer)
{
   vInitPlatformEtg();
   ETG_I_REGISTER_FILE();
   ETG_I_REGISTER_CHN(commandNotProcessed);
   _pGuiService = guiService;
   _pPopUpService = popUpService;
   _pEarlyStartupPlayer = earlyStartupPlayer;
}


TraceCommandAdapter::~TraceCommandAdapter()
{
}


ETG_I_CMD_DEFINE((TraceCommandAdapter::pttShortPress, "pttShortPress"))

void TraceCommandAdapter::pttShortPress()
{
   if (_pGuiService)
   {
      _pGuiService->vPttShortPressed();
   }
}


ETG_I_CMD_DEFINE((TraceCommandAdapter::startAudio, "startAudio"))

void TraceCommandAdapter::startAudio()
{
   if (_pEarlyStartupPlayer)
   {
      _pEarlyStartupPlayer->requestStartPlayback();
   }
}


ETG_I_CMD_DEFINE((TraceCommandAdapter::stopAudio, "stopAudio"))

void TraceCommandAdapter::stopAudio()
{
   if (_pEarlyStartupPlayer)
   {
      _pEarlyStartupPlayer->requestStopPlayback();
   }
}


ETG_I_CMD_DEFINE((TraceCommandAdapter::setMicrophoneLevel, "setMicrophoneLevel %d", unsigned int))

void TraceCommandAdapter::setMicrophoneLevel(unsigned int level)
{
   if (_pPopUpService)
   {
      _pPopUpService->setMicrophoneLevel((uint32)level);
   }
}


/**
 *	sink for not processed ttfis input messages
 */
void TraceCommandAdapter::commandNotProcessed(const unsigned char* data)
{
   if (data)
   {
      ETG_TRACE_FATAL(("trace command was not processed: 0x%*x", ETG_LIST_LEN(data[0]), ETG_LIST_PTR_T8(data)));
   }
}
