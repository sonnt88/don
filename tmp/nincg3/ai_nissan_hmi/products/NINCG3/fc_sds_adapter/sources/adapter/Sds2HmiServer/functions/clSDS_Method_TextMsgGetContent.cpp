/*********************************************************************//**
 * \file       clSDS_Method_TextMsgGetContent.cpp
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "clSDS_Method_TextMsgGetContent.h"
#include "application/clSDS_ReadSmsList.h"
#include "application/SdsPhoneService.h"

#include "SdsAdapter_Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_TextMsgGetContent.cpp.trc.h"
#endif

#define SMS_TAG "\x1b\\tn=sms\\"

using namespace MOST_Msg_FI;
using namespace most_Msg_fi_types;


clSDS_Method_TextMsgGetContent::~clSDS_Method_TextMsgGetContent()
{
   _pReadSmsList = NULL;
   _pSdsPhoneService = NULL;
}


clSDS_Method_TextMsgGetContent::clSDS_Method_TextMsgGetContent(ahl_tclBaseOneThreadService* pService,
      ::boost::shared_ptr< ::MOST_Msg_FIProxy > pSds2MsgProxy, clSDS_ReadSmsList* pSmsList, SdsPhoneService* sdsPhoneService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_TEXTMSGGETCONTENT, pService)
   , _messageProxy(pSds2MsgProxy)
   , _pReadSmsList(pSmsList)
   , _pSdsPhoneService(sdsPhoneService)
{
}


void clSDS_Method_TextMsgGetContent::vMethodStart(amt_tclServiceData* /*pInMsg*/)
{
   T_MsgMessageHandle messageHandle = _pReadSmsList->getMessageHandle();
   if (_messageProxy->isAvailable())
   {
      _messageProxy->sendGetMessageStart(*this, messageHandle);
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}


void  clSDS_Method_TextMsgGetContent::sendResult(std::string smsContent)
{
   sds2hmi_sdsfi_tclMsgTextMsgGetContentMethodResult oMessage;
   std::string sms_Content = SMS_TAG + smsContent;
   oMessage.TxtMessage.bSet(sms_Content.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   vSendMethodResult(oMessage);
}


void clSDS_Method_TextMsgGetContent::onGetMessageError(
   const ::boost::shared_ptr< MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< GetMessageError >& /*error*/)
{
   vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
}


void clSDS_Method_TextMsgGetContent::onGetMessageResult(
   const ::boost::shared_ptr< MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< GetMessageResult >& result)
{
   std::string smsContent = result->getOMessageDetailsResult().getSShortMessage();
   ETG_TRACE_USR1(("clSDS_Method_TextMsgGetContent::onGetMessageResult: SmsContent is %s ", smsContent.c_str()));
   std::string header("");
   prepareHeader(header);
   if (_pSdsPhoneService)
   {
      _pSdsPhoneService->sendSmsContentSignal(header, smsContent);
   }
   sendResult(smsContent);
}


void clSDS_Method_TextMsgGetContent::prepareHeader(std::string& ostring) const
{
   if (_pReadSmsList != NULL)
   {
      if (!(_pReadSmsList->getSelectedContactName().empty()))
      {
         ostring = _pReadSmsList->getSelectedContactName() + _pReadSmsList->getTimeDate();
      }
      else
      {
         ostring = _pReadSmsList->getSelectedPhoneNumber() + _pReadSmsList->getTimeDate();
      }
   }
}
