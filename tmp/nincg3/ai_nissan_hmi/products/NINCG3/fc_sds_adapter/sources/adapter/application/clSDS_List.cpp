/*********************************************************************//**
 * \file       clSDS_List.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_List.h"


/**************************************************************************//**
*Destructor
******************************************************************************/
clSDS_List::~clSDS_List()
{
   _pListInfoObserver = NULL;
}


/**************************************************************************//**
*Constructor
******************************************************************************/
clSDS_List::clSDS_List(tU8 u8NumberOfItemPerPage) : _pListInfoObserver(NULL)
{
   _u8NumberOfItemPerPage = u8NumberOfItemPerPage;
   _u32PageNumber = 0;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_List::vResetPageNumber()
{
   _u32PageNumber = 0;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_List::vIncrementPageNumber()
{
   if (u32PageEndIndex() < u32GetSize())
   {
      _u32PageNumber++;
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_List::vDecrementPageNumber()
{
   if (_u32PageNumber > 0)
   {
      _u32PageNumber--;
   }
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_List::u32GetPageNumber() const
{
   return _u32PageNumber;
}


/**************************************************************************//**
*
******************************************************************************/
stRange clSDS_List::stGetRangeOfPage()
{
   stRange oRange;
   if (u32GetSize() < u32PageEndIndex())
   {
      oRange.u32StartIndex = u32PageStartIndex();
      oRange.u32EndIndex = u32GetSize();
   }
   else
   {
      oRange.u32StartIndex = u32PageStartIndex();
      oRange.u32EndIndex = u32PageEndIndex();
   }
   return oRange;
}


/**************************************************************************//**
*
******************************************************************************/
tU8 clSDS_List::u8GetNumOfElementsOnPage()
{
   stRange oRange = stGetRangeOfPage();
   return (tU8)(oRange.u32EndIndex - oRange.u32StartIndex);
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_List::bHasNextPage()
{
   return (u32GetSize() > u32PageEndIndex());
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_List::bHasPreviousPage() const
{
   return (_u32PageNumber > 0);
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_List::u32PageStartIndex() const
{
   return _u32PageNumber * _u8NumberOfItemPerPage;
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_List::u32PageEndIndex() const
{
   return u32PageStartIndex() + _u8NumberOfItemPerPage;
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::string clSDS_List::oGetSelectedItem(tU32 /*u32Index*/)
{
   return "";
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_List::setItems(bpstl::vector<clSDS_ListItems> /*items*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_List::setListObserver(clSDS_ListInfoObserver* pListInfoObserver)
{
   _pListInfoObserver = pListInfoObserver;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_List::notifyListObserver()
{
   if (_pListInfoObserver != NULL)
   {
      _pListInfoObserver->vListUpdated(this);
   }
}


/**************************************************************************//**
*
******************************************************************************/
std::string clSDS_List::oGetHMIListDescriptionItems(tU32 /*u32Index*/)
{
   return " ";
}


/**************************************************************************//**
*
******************************************************************************/
std::vector<sds2hmi_fi_tcl_HMIElementDescription> clSDS_List::getHmiElementDescription(int /*index*/)
{
   std::vector<sds2hmi_fi_tcl_HMIElementDescription> hmiElementDescription;
   return hmiElementDescription;
}
