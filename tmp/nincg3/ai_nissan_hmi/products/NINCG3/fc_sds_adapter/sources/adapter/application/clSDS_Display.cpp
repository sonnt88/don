/*********************************************************************//**
 * \file       clSDS_Display.cpp
 *
 * clSDS_Display class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_Display.h"
#include "application/clSDS_ListItems.h"
#include "application/clSDS_StringVarList.h"
#include "application/clSDS_SDSStatus.h"
#include "application/clSDS_ScreenData.h"
#include "application/clSDS_View.h"
#include "view_db/Sds_ViewDB.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Display::~clSDS_Display()
{
   _pSdsStatus = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Display::clSDS_Display(clSDS_SDSStatus* pSdsStatus, GuiService& guiService, PopUpService& popupService)
   : _guiService(guiService)
   , _popupService(popupService)
   , _pSdsStatus(pSdsStatus)
   , _viewId(SDS_VIEW_ID_LIMIT)
   , _headerTemplateId(SDS_HEADER_TEMPLATE_ID_LIMIT)
   , _isCommandListAvailable(true)
   , _cursorIndex(0)
   , _state(clSDS_Display::INACTIVE)
{
   _pSdsStatus->vRegisterObserver(this);
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_Display::vShowView(const sds_gui_fi::PopUpService::PopupRequestSignal& popupRequestSignal, Sds_ViewId viewId)
{
   _viewId = viewId;
   _popupService.sendPopupRequestSignal(popupRequestSignal.getLayout(),
                                        popupRequestSignal.getHeader(),
                                        popupRequestSignal.getSpeechInputState(),
                                        popupRequestSignal.getTextFields());

   if (SDS_VIEW_ID_LIMIT == viewId)
   {
      _popupService.setScreenID("");
   }
   else
   {
      _popupService.setScreenID(Sds_ViewDB_pcGetName(viewId));
   }
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_Display::vCloseView(tBool /*bError*/)
{
   if (_state == clSDS_Display::ACTIVE)
   {
      _state = clSDS_Display::INACTIVE;
      _popupService.sendPopupRequestCloseSignal();
   }
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_Display::vSDSStatusChanged()
{
   SetSpeechInputState();
   createAndShowView();
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Display::cusorMoved(uint32 cursorIndex)
{
   _cursorIndex = cursorIndex;
   createAndShowView();
}


/***********************************************************************//**
*
***************************************************************************/
Sds_ViewId clSDS_Display::enGetScreenId() const
{
   return _viewId;
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_Display::vShowSDSView(clSDS_ScreenData& oScreenData)
{
   _state = clSDS_Display::ACTIVE;
   _screenVariableData = oScreenData.oGetScreenVariableData();
   _items = oScreenData.oGetListItemsofView();
   _viewId = oScreenData.enReadScreenId();
   _headerTemplateId = oScreenData.enReadTemplateId();
   _headerValueMap = oScreenData.oGetHeaderVariableData();
   _cursorIndex = 0;
   createAndShowView();
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Display::createAndShowView()
{
   if (_state == clSDS_Display::ACTIVE)
   {
      clSDS_View oView;
      sds_gui_fi::PopUpService::PopupRequestSignal popupRequestSignal;
      oView.vCreateView(popupRequestSignal, _viewId, _items, _screenVariableData, _cursorIndex);
      oView.vCreateViewHeader(popupRequestSignal, _headerTemplateId, _headerValueMap, _viewId);
      if (_pSdsStatus && _pSdsStatus->bIsListening())
      {
         popupRequestSignal.setSpeechInputState(sds_gui_fi::PopUpService::SpeechInputState__LISTENING);
         _headerTemplateId = SDS_TEMPLATE_NONE;
      }
      getCommandListAvailability(oView);
      vShowView(popupRequestSignal, _viewId);
   }
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Display::SetSpeechInputState()
{
   if (_pSdsStatus)
   {
      sds_gui_fi::PopUpService::SpeechInputState speechInputState;

      switch (_pSdsStatus->getStatus())
      {
         case clSDS_SDSStatus::EN_IDLE:
            speechInputState = sds_gui_fi::PopUpService::SpeechInputState__IDLE;
            _guiService.setSdsStatus(sds_gui_fi::SdsGuiService::SpeechInputState__IDLE);
            break;
         case clSDS_SDSStatus::EN_DIALOGOPEN:
            speechInputState = sds_gui_fi::PopUpService::SpeechInputState__DIALOGOPEN;
            _guiService.setSdsStatus(sds_gui_fi::SdsGuiService::SpeechInputState__DIALOGOPEN);
            break;
         case clSDS_SDSStatus::EN_LOADING:
            speechInputState = sds_gui_fi::PopUpService::SpeechInputState__LOADING;
            _guiService.setSdsStatus(sds_gui_fi::SdsGuiService::SpeechInputState__LOADING);
            break;
         case clSDS_SDSStatus::EN_ERROR:
            speechInputState = sds_gui_fi::PopUpService::SpeechInputState__ERROR;
            _guiService.setSdsStatus(sds_gui_fi::SdsGuiService::SpeechInputState__ERROR);
            break;
         case clSDS_SDSStatus::EN_LISTENING:
            speechInputState = sds_gui_fi::PopUpService::SpeechInputState__LISTENING;
            _guiService.setSdsStatus(sds_gui_fi::SdsGuiService::SpeechInputState__LISTENING);
            break;
         case clSDS_SDSStatus::EN_ACTIVE:
            speechInputState = sds_gui_fi::PopUpService::SpeechInputState__ACTIVE;
            _guiService.setSdsStatus(sds_gui_fi::SdsGuiService::SpeechInputState__ACTIVE);
            break;
         case clSDS_SDSStatus::EN_PAUSE:
            speechInputState = sds_gui_fi::PopUpService::SpeechInputState__PAUSE;
            _guiService.setSdsStatus(sds_gui_fi::SdsGuiService::SpeechInputState__PAUSE);
            break;
         default:
            speechInputState = sds_gui_fi::PopUpService::SpeechInputState__UNKNOWN;
            _guiService.setSdsStatus(sds_gui_fi::SdsGuiService::SpeechInputState__UNKNOWN);
            break;
      }

      _popupService.setSdsStatus(speechInputState);
   }
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::map<bpstl::string, bpstl::string> clSDS_Display::getScreenVariableData() const
{
   return _screenVariableData;
}


/***********************************************************************//**
*
***************************************************************************/
bool clSDS_Display::hasCommandList() const
{
   return _isCommandListAvailable;
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Display::getCommandListAvailability(const clSDS_View& sdsView)
{
   if (sdsView.getCommandListCount())
   {
      _isCommandListAvailable = true;
   }
   else
   {
      _isCommandListAvailable = false;
   }
}
