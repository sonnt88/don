/*********************************************************************//**
 * \file       clSDS_AmbigContactList.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_AmbigContactList.h"
#include "application/clSDS_StringVarList.h"

clSDS_AmbigContactList::clSDS_AmbigContactList()
{
}


clSDS_AmbigContactList::~clSDS_AmbigContactList()
{
}


void clSDS_AmbigContactList::setAmbigContactList(bpstl::vector<bpstl::string> contactNames)
{
   _ambigContactNames = contactNames;
}


tU32 clSDS_AmbigContactList::u32GetSize()
{
   return _ambigContactNames.size();
}


bpstl::vector<clSDS_ListItems> clSDS_AmbigContactList::oGetItems(tU32 u32StartIndex, tU32 u32EndIndex)
{
   bpstl::vector<clSDS_ListItems> oListItems;
   if (u32StartIndex < u32GetSize())
   {
      for (tU32 u32Index = u32StartIndex; u32Index < bpstl::min(u32EndIndex, u32GetSize()); u32Index++)
      {
         clSDS_ListItems oListItem;
         oListItem.oDescription.szString = oGetItem(u32Index);
         oListItems.push_back(oListItem);
      }
   }
   return oListItems;
}


tBool clSDS_AmbigContactList::bSelectElement(tU32 u32SelectedIndex)
{
   clSDS_StringVarList::vSetVariable("$(ContactName)", oGetSelectedItem(u32SelectedIndex));

   return true;
}


tVoid clSDS_AmbigContactList::vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType)
{
   NORMAL_M_ASSERT(listType == sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_CONTACT_AMBIGUITY);

   notifyListObserver();
}


bpstl::string clSDS_AmbigContactList::oGetSelectedItem(tU32 u32Index)
{
   return oGetItem(u32Index);
}


bpstl::string clSDS_AmbigContactList::oGetItem(tU32 u32Index) const
{
   bpstl::string str;

   if (_ambigContactNames.size() > u32Index)
   {
      str = _ambigContactNames[u32Index];
   }

   return str;
}
