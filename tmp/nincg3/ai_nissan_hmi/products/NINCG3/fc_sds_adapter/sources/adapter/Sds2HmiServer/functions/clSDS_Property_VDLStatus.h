/*********************************************************************//**
 * \file       clSDS_Property_VDLStatus.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Property_VDLStatus_h
#define clSDS_Property_VDLStatus_h


#include "Sds2HmiServer/framework/clServerProperty.h"
#include "external/sds2hmi_fi.h"


class clSDS_Property_VDLStatus : public clServerProperty
{
   public:
      virtual ~clSDS_Property_VDLStatus();
      clSDS_Property_VDLStatus(ahl_tclBaseOneThreadService* pService);
      tVoid vVdlDeviceConnectionLost();
      tVoid vSendStatus();

   protected:
      virtual tVoid vGet(amt_tclServiceData* pInMsg);
      virtual tVoid vSet(amt_tclServiceData* pInMsg);
      virtual tVoid vUpreg(amt_tclServiceData* pInMsg);
      tVoid vUpdateDeviceList(bpstl::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const;
      tVoid vUpdateSmartPhoneIntegrationDeviceList(bpstl::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const;
      sds2hmi_fi_tcl_e8_DeviceStatus::tenType eGetDeviceStatus() const;
      tVoid vTraceVDLStatus(const sds2hmi_sdsfi_tclMsgVDLStatusStatus& oMessage) const;
};


#endif
