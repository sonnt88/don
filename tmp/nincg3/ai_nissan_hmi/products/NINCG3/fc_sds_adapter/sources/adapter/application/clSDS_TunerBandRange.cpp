/*********************************************************************//**
 * \file       clSDS_TunerBandRange.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "application/clSDS_TunerBandRange.h"
#include "application/clSDS_StringVarList.h"
#include "application/StringUtils.h"

using namespace tuner_main_fi;
using namespace tuner_main_fi_types;


clSDS_TunerBandRange::clSDS_TunerBandRange(::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy > tuner_fi_proxy)
   : _tunerFiProxy(tuner_fi_proxy)
{
}


clSDS_TunerBandRange::~clSDS_TunerBandRange()
{
}


void clSDS_TunerBandRange::onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _tunerFiProxy)
   {
      _tunerFiProxy->sendFID_TUN_G_AVAILABLE_BAND_RANGEUpReg(*this); //currently not working from tuner side
      _tunerFiProxy->sendFID_TUN_G_AVAILABLE_BAND_RANGEGet(*this);
   }
}


void clSDS_TunerBandRange::onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _tunerFiProxy)
   {
      _tunerFiProxy->sendFID_TUN_G_AVAILABLE_BAND_RANGERelUpRegAll();
   }
}


void clSDS_TunerBandRange::onFID_TUN_G_AVAILABLE_BAND_RANGEError(const ::boost::shared_ptr< Tuner_main_fiProxy >& /*proxy*/,
      const ::boost::shared_ptr< FID_TUN_G_AVAILABLE_BAND_RANGEError >& /*error*/)
{
}


void clSDS_TunerBandRange::onFID_TUN_G_AVAILABLE_BAND_RANGEStatus(const ::boost::shared_ptr< Tuner_main_fiProxy >& /*proxy*/,
      const ::boost::shared_ptr< FID_TUN_G_AVAILABLE_BAND_RANGEStatus >& status)
{
   T_AvailableBandInfoList bandInfoList = status->getTunerAvailableBandInfoList();
   ::std::vector< T_AvailableBandInfoElement >::const_iterator it;

   for (it = bandInfoList.begin(); it != bandInfoList.end(); ++it)
   {
      T_e8_Tun_TunerBand band = it->getE8Band();
      uint32 low = it->getU32LowFrequency();
      uint32 high = it->getU32HighFrequency();

      switch (band)
      {
         case T_e8_Tun_TunerBand__TUN_BAND_FM:
            clSDS_StringVarList::vSetVariable("$(FMMinFreq)", formatFMFrequency(low));
            clSDS_StringVarList::vSetVariable("$(FMMaxFreq)", formatFMFrequency(high));
            break;

         case T_e8_Tun_TunerBand__TUN_BAND_MW:
            clSDS_StringVarList::vSetVariable("$(AMMinFreq)", StringUtils::toString(low));
            clSDS_StringVarList::vSetVariable("$(AMMaxFreq)", StringUtils::toString(high));
            break;

         default:
            break;
      }
   }
}


std::string clSDS_TunerBandRange::formatFMFrequency(uint32 fmFreq) const
{
   // e.g. from 87900 to 87.9
   std::string quot = StringUtils::toString(fmFreq / 1000);
   std::string rem = StringUtils::toString((fmFreq % 1000) / 100);
   return quot + "." + rem;
}
