/*********************************************************************//**
 * \file       clSDS_Method_NaviSetDestinationItem.cpp
 *
 * clSDS_Method_NaviSetDestinationItem method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_NaviSetDestinationItem.h"
#include "SdsAdapter_Trace.h"
#include "application/clSDS_ListScreen.h"
#include "application/clSDS_NaviListItems.h"

#define FI_S_IMPORT_INTERFACE_FI_TYPES
#include "fi_if.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_NaviSetDestinationItem.cpp.trc.h"
#endif


using namespace org::bosch::cm::navigation::NavigationService;


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_NaviSetDestinationItem::~clSDS_Method_NaviSetDestinationItem()
{
   _pMultipleDestiantionList = NULL;
   _pPOIList = NULL;
   _pNaviListItems = NULL;
   _pCommonShowDialog = NULL;
   _oRequestedsdsAddress.clear();
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_NaviSetDestinationItem::clSDS_Method_NaviSetDestinationItem(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr<NavigationServiceProxy> naviProxy,
   clSDS_MultipleDestinationsList* pMultipleDestinationList,
   clSDS_POIList* pPOIList,
   clSDS_NaviListItems* pNaviListItems,
   clSDS_Method_CommonShowDialog* pCommonShowDialog)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_NAVISETDESTINATIONITEM, pService)
   , _navigationProxy(naviProxy)
   , _pMultipleDestiantionList(pMultipleDestinationList)
   , _pPOIList(pPOIList)
   , _pNaviListItems(pNaviListItems)
   , _pCommonShowDialog(pCommonShowDialog)
{
   _oRequestedsdsAddress.clear();
   _enNavSelectionCriterionType = sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_RESERVED;
   _destinationInformationSize = 0;
   _pMultipleDestiantionList->vRegisterAmbiguityListObserver(this);
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vMethodStart(amt_tclServiceData* pInMessage)
{
   vGetDataFromAmt(pInMessage, _oMethodStart);
   vSetNavSelectionCriterionType();
   ETG_TRACE_USR1(("nAmbiguityAbsVal from SDS = %d", _oMethodStart.nAmbiguityAbsVal));
   if (_oMethodStart.nAmbiguityAbsVal == 0xFF)
   {
      vAddressRequestToNavi();
   }
   else
   {
      vSendListentrySelectionToNavi(_oMethodStart.nAmbiguityAbsVal);
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSetDestinationItem::onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
      const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _navigationProxy)
   {
      _navigationProxy->sendSdsAddressWithOptionsDeregisterAll();
      _navigationProxy->sendDestinationInformationDeregisterAll();
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSetDestinationItem::onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
      const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _navigationProxy)
   {
      _navigationProxy->sendSdsAddressWithOptionsRegister(*this);
      _navigationProxy->sendDestinationInformationRegister(*this);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetDestinationItem::onSdsCheckAddressError(const ::boost::shared_ptr<NavigationServiceProxy>& /*proxy*/,
      const ::boost::shared_ptr< SdsCheckAddressError >& /*Error*/)
{
   vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_DESTINATIONVALUENOTRESOLVED);
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetDestinationItem::onSdsCheckAddressResponse(const ::boost::shared_ptr<NavigationServiceProxy >& proxy,
      const ::boost::shared_ptr< SdsCheckAddressResponse >& /*osdsCheckAddressResponse*/)
{
   ETG_TRACE_USR1(("onSdsCheckAddressResponse()- Function entered"));
   vGetAddressOptionResultFromNavi(proxy);
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetDestinationItem::onSdsAddressWithOptionsUpdate(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< SdsAddressWithOptionsUpdate >& /*oSdsAddressWithOptionsUpdate*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetDestinationItem::onSdsAddressWithOptionsError(const ::boost::shared_ptr<NavigationServiceProxy>& /*proxy*/,
      const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::SdsAddressWithOptionsError >& /*error*/)
{
   vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_DESTINATIONVALUENOTRESOLVED);
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetDestinationItem::onSetLocationWithSdsInputError(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< SetLocationWithSdsInputError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetDestinationItem::onSetLocationWithSdsInputResponse(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< SetLocationWithSdsInputResponse >& /*response*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetDestinationItem::onSelectSdsRefinementError(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< SelectSdsRefinementError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetDestinationItem::onSelectSdsRefinementResponse(const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
      const ::boost::shared_ptr< SelectSdsRefinementResponse >& /*response*/)
{
   ETG_TRACE_USR1(("onSdsSelectRefinementResponse()- Function entered"));

   if (_enNavSelectionCriterionType == sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_POI)
   {
      vRequestPOIListToNavi();
   }
   else if (_enNavSelectionCriterionType == sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_ADDRESSES)
   {
      vGetAddressOptionResultFromNavi(proxy);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vGetAddressOptionResultFromNavi(const ::boost::shared_ptr< NavigationServiceProxy >& proxy)
{
   ::std::vector< SDSAddressElement> oVerifiedsdsAddress;
   ::std::vector< SDSAddressElementType > oElementTypes;
   SDSAddressOptions oSDSAddressOptions;
   oVerifiedsdsAddress.clear();
   oSDSAddressOptions = proxy->getSdsAddressWithOptions().getAddressOptions();
   oVerifiedsdsAddress = proxy->getSdsAddressWithOptions().getAddress();
   oElementTypes = proxy->getSdsAddressWithOptions().getAddressElementType();
   vHandleVerifiedSdsAddress(oVerifiedsdsAddress, oSDSAddressOptions, oElementTypes);
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetDestinationItem::onDestinationInformationError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< DestinationInformationError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetDestinationItem::onDestinationInformationUpdate(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< DestinationInformationUpdate >& update)
{
   ETG_TRACE_USR1(("onDestinationInformationUpdate()- Function entered"));
   _destinationInformationSize = update->getDestinationInformation().size();
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetDestinationItem::onRequestFreeTextSearchResultsError(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< RequestFreeTextSearchResultsError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetDestinationItem::onRequestFreeTextSearchResultsResponse(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< RequestFreeTextSearchResultsResponse >& response)
{
   ETG_TRACE_USR1(("onRequestFreeTextSearchResultsResponse()- Function entered"));

   ::std::vector< AddressListElement > oPOIAddressList;

   oPOIAddressList = response->getSearchResults();

   _pPOIList->vHandlePOIList(oPOIAddressList);

   tU16 u16NumberofElements = (tU16) oPOIAddressList.size();

   ETG_TRACE_USR1(("onRequestFreeTextSearchResultsResponse()- u16NumberofElements = %d", u16NumberofElements));

   if (u16NumberofElements == 0)
   {
      vHandlePOIResult(u16NumberofElements, FALSE);
   }
   else
   {
      vHandlePOIResult(u16NumberofElements, TRUE);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetDestinationItem::onSetLocationWithFreeTextSearchInputError(const ::boost::shared_ptr< NavigationServiceProxy >&/*proxy*/,
      const ::boost::shared_ptr< SetLocationWithFreeTextSearchInputError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetDestinationItem::onSetLocationWithFreeTextSearchInputResponse(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< SetLocationWithFreeTextSearchInputResponse >& /*response*/)
{
   ETG_TRACE_USR1(("SetLocationWithFreeTextSearchInputRequestResponse is received"));
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetNavSelectionCriterionType()
{
   if (_oMethodStart.nNumberOfElements > 0)
   {
      for (tU32 u32Index = 0; u32Index < _oMethodStart.tLocationDescription.Descriptors.size(); u32Index++)
      {
         _enNavSelectionCriterionType = _oMethodStart.tLocationDescription.Descriptors[u32Index].SelectionCriterion.enType;
      }
      _pNaviListItems->vSetNaviSelectionCriterionListType(_enNavSelectionCriterionType);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
sds2hmi_fi_tcl_e16_SelectionCriterionType::tenType clSDS_Method_NaviSetDestinationItem::GetNavSelectionCriterionType() const
{
   return _enNavSelectionCriterionType;
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vAddressRequestToNavi()
{
   if (bIsHouseNumberRequest())
   {
      vHandleHouseNrRequest(oGetHouseNrFromDescriptor());
   }
   else if (bIsOsdeRequest())
   {
      oGetOsdeStringFromDescriptor();
      vSendsdsCheckAddressRequest();
   }
   else if (bIsPOISearchRequest())
   {
      vHandlePOIRequest();
   }
   else
   {
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tBool clSDS_Method_NaviSetDestinationItem::bIsHouseNumberRequest()
{
   for (tU32 u32Index = 0; u32Index < _oMethodStart.tLocationDescription.Descriptors.size(); u32Index++)
   {
      if (_oMethodStart.tLocationDescription.Descriptors[u32Index].SelectionCriterion.enType == sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_HOUSENUMBER)
      {
         return TRUE;
      }
   }
   return FALSE;
}


/***********************************************************************//**
 *
 ***************************************************************************/
tBool clSDS_Method_NaviSetDestinationItem::bIsPOISearchRequest() const
{
   for (tU32 u32Index = 0; u32Index < _oMethodStart.tLocationDescription.Descriptors.size(); u32Index++)
   {
      if (_oMethodStart.tLocationDescription.Descriptors[u32Index].SelectionCriterion.enType == sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_POI)
      {
         return TRUE;
      }
   }
   return FALSE;
}


/***********************************************************************//**
 *
 ***************************************************************************/
tBool clSDS_Method_NaviSetDestinationItem::bIsOsdeRequest() const
{
   for (tU32 u32Index = 0; u32Index < _oMethodStart.tLocationDescription.Descriptors.size(); u32Index++)
   {
      if (_oMethodStart.tLocationDescription.Descriptors[u32Index].SelectionCriterion.enType == sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_ADDRESSES)
      {
         return TRUE;
      }
   }
   return FALSE;
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::oGetOsdeStringFromDescriptor()
{
   ETG_TRACE_USR1(("oGetOsdeStringFromDescriptor()- Function entered"));

   std::string oHouseNumberString("");
   std::string oFirstStreetNameString("");
   std::string oSecondStreetNameString("");
   std::string oCityNameString("");
   std::string oStateNameString("");
   std::string oCountryNameString("");
   //Country region is not handled by SDS_MW so the hardcoded country name is been sent to Navi hall.
   std::string oCountryNameStringDummy("United States");

   for (tU32 u32Index = 0; u32Index < _oMethodStart.tLocationDescription.Descriptors.size(); u32Index++)
   {
      if (_oMethodStart.tLocationDescription.Descriptors[u32Index].SelectionCriterion.enType == sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_ADDRESSES)
      {
         tString pString  = ((const fi_tcl_StringPosition&) * _oMethodStart.tLocationDescription.Descriptors[u32Index].poDataData).String.szGet(fi_tclString::FI_EN_UTF8);
         oHouseNumberString = oGetHouseNumberFromDescriptor(pString);
         oFirstStreetNameString = oGetFirstStreetNameFromDescriptor(pString);
         oSecondStreetNameString = oGetSecondStreetNameFromDescriptor(pString);
         oCityNameString = oGetCityNameFromDescriptor(pString);
         oStateNameString = oGetStateNameFromDescriptor(pString);
         oCountryNameString = oGetCountryNameFromDescriptor(pString);
      }
   }

   vSetCountryInformation(oCountryNameStringDummy);
   vSetStateInformation(oStateNameString);
   vSetCityInformation(oCityNameString);
   vSetFirstStreetInformation(oFirstStreetNameString);
   vSetSecondStreetInformation(oSecondStreetNameString);
   vSetHouseNumberInformation(oHouseNumberString);
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::oGetPOIStringFromDescriptor()
{
   ETG_TRACE_USR1(("oGetPOIStringFromDescriptor()- Function entered"));

   _oPOIName.BrandName.clear();
   _oPOIName.CategoryName.clear();
   std::string oStateNameString("");
   std::string oCityNameString("");
   //Country region is not hadled by SDS_MW so the hardcoded country name is been sent to Navi hall.
   std::string oCountryNameString("United States");

   for (tU32 u32Index = 0; u32Index < _oMethodStart.tLocationDescription.Descriptors.size(); u32Index++)
   {
      if (_oMethodStart.tLocationDescription.Descriptors[u32Index].SelectionCriterion.enType == sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_POI)
      {
         tString pString = ((const fi_tcl_StringPosition&) * _oMethodStart.tLocationDescription.Descriptors[u32Index].poDataData).String.szGet(fi_tclString::FI_EN_UTF8);
         oCityNameString = oGetCityNameFromDescriptor(pString);
         oStateNameString = oGetStateNameFromDescriptor(pString);
         _oPOIName.BrandName = oGetBrandNameFromDescriptor(pString);
         _oPOIName.CategoryName = oGetCategoryNameFromDescriptor(pString);
         _oPOIName.BuildingName = oGetBuildingNameFromDescriptor(pString);
      }
   }
   vSetCountryInformation(oCountryNameString);
   vSetStateInformation(oStateNameString);
   vSetCityInformation(oCityNameString);
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oGetHouseNrFromDescriptor() const
{
   std::string oHouseNumberString("");
   for (tU32 u32Index = 0; u32Index < _oMethodStart.tLocationDescription.Descriptors.size(); u32Index++)
   {
      tString pString  = ((const fi_tcl_StringPosition&) * _oMethodStart.tLocationDescription.Descriptors[u32Index].poDataData).String.szGet(fi_tclString::FI_EN_UTF8);
      oHouseNumberString = oGetHouseNumberFromDescriptor(pString);
   }
   return oHouseNumberString;
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vHandleHouseNrRequest(const std::string& oString)
{
   if (!oString.empty())
   {
      ETG_TRACE_USR1(("vHandleHouseNrRequest()- House number is available"));
      vSetRemainingAddressInformation();
      vSetHouseNumberInformation(oString);
      vSendsdsCheckAddressRequest();
   }
   else
   {
      ETG_TRACE_USR1(("vAddressRequestToNavi()- House number is not available"));
      vSetDestinationResult(ADDRESS_NOHOUSENO_RESULT);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vHandlePOIRequest()
{
   oGetPOIStringFromDescriptor();
   if (_oMethodStart.tLocationType.enType == sds2hmi_fi_tcl_e8_NAV_LocationType::FI_EN_DEFAULT)
   {
      vSendsdsCheckAddressRequest();
   }
   else
   {
      vRequestPOIListToNavi();
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oGetHouseNumberFromDescriptor(const std::string& oString) const
{
   const char* prefix = "houseno:";
   size_t pos = oString.find(prefix);
   if (pos != std::string::npos)
   {
      std::string oHouseNumberString = oString.substr(pos + strlen(prefix));
      oHouseNumberString = oRemoveDescriptorInformation(oHouseNumberString, "street:");
      oHouseNumberString = oRemoveDescriptorInformation(oHouseNumberString, "state:");
      oHouseNumberString = oRemoveDescriptorInformation(oHouseNumberString, "city:");
      oHouseNumberString = oRemoveDescriptorInformation(oHouseNumberString, "country:");
      oHouseNumberString = oRemoveEmptySpaces(oHouseNumberString);
      return oHouseNumberString;
   }
   return "";
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oGetFirstStreetNameFromDescriptor(const std::string& oString) const
{
   const char* prefix = "street:";
   size_t pos = oString.find(prefix);
   if (pos != std::string::npos)
   {
      std::string oStreetNameString = oString.substr(pos + strlen(prefix));
      oStreetNameString = oRemoveDescriptorInformation(oStreetNameString, "houseno:");
      oStreetNameString = oRemoveDescriptorInformation(oStreetNameString, "street2:");
      oStreetNameString = oRemoveDescriptorInformation(oStreetNameString, "state:");
      oStreetNameString = oRemoveDescriptorInformation(oStreetNameString, "city:");
      oStreetNameString = oRemoveDescriptorInformation(oStreetNameString, "country:");
      oStreetNameString = oRemoveEmptySpaces(oStreetNameString);
      return oStreetNameString;
   }
   return "";
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oGetSecondStreetNameFromDescriptor(const std::string& oString) const
{
   const char* prefix = "street2:";
   size_t pos = oString.find(prefix);
   if (pos != std::string::npos)
   {
      std::string oStreetNameString = oString.substr(pos + strlen(prefix));
      oStreetNameString = oRemoveDescriptorInformation(oStreetNameString, "houseno:");
      oStreetNameString = oRemoveDescriptorInformation(oStreetNameString, "street:");
      oStreetNameString = oRemoveDescriptorInformation(oStreetNameString, "state:");
      oStreetNameString = oRemoveDescriptorInformation(oStreetNameString, "city:");
      oStreetNameString = oRemoveDescriptorInformation(oStreetNameString, "country:");
      oStreetNameString = oRemoveEmptySpaces(oStreetNameString);
      return oStreetNameString;
   }
   return "";
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oGetCityNameFromDescriptor(const std::string& oString) const
{
   const char* prefix = "city:";
   size_t pos = oString.find(prefix);
   if (pos != std::string::npos)
   {
      std::string oCityNameString = oString.substr(pos + strlen(prefix));
      if (_enNavSelectionCriterionType == sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_POI)
      {
         oCityNameString = oRemoveDescriptorInformation(oCityNameString, "poi:");
         oCityNameString = oRemoveDescriptorInformation(oCityNameString, "cat:");
         oCityNameString = oRemoveDescriptorInformation(oCityNameString, "attr:");
      }
      else
      {
         oCityNameString = oRemoveDescriptorInformation(oCityNameString, "houseno:");
         oCityNameString = oRemoveDescriptorInformation(oCityNameString, "street:");
         oCityNameString = oRemoveDescriptorInformation(oCityNameString, "street2:");
      }
      oCityNameString = oRemoveDescriptorInformation(oCityNameString, "state:");
      oCityNameString = oRemoveDescriptorInformation(oCityNameString, "country:");
      oCityNameString = oRemoveEmptySpaces(oCityNameString);
      return oCityNameString;
   }
   return "";
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oGetStateNameFromDescriptor(const std::string& oString) const
{
   const char* prefix = "state:";
   size_t pos = oString.find(prefix);
   if (pos != std::string::npos)
   {
      std::string oStateNameString = oString.substr(pos + strlen(prefix));
      if (_enNavSelectionCriterionType == sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_POI)
      {
         oStateNameString = oRemoveDescriptorInformation(oStateNameString, "poi:");
         oStateNameString = oRemoveDescriptorInformation(oStateNameString, "cat:");
         oStateNameString = oRemoveDescriptorInformation(oStateNameString, "attr:");
      }
      else
      {
         oStateNameString = oRemoveDescriptorInformation(oStateNameString, "houseno:");
         oStateNameString = oRemoveDescriptorInformation(oStateNameString, "street:");
         oStateNameString = oRemoveDescriptorInformation(oStateNameString, "street2:");
      }

      oStateNameString = oRemoveDescriptorInformation(oStateNameString, "city:");
      oStateNameString = oRemoveDescriptorInformation(oStateNameString, "country:");
      oStateNameString = oRemoveEmptySpaces(oStateNameString);
      return oStateNameString;
   }
   return "";
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oGetCountryNameFromDescriptor(const std::string& oString) const
{
   const char* prefix = "country:";
   size_t pos = oString.find(prefix);
   if (pos != std::string::npos)
   {
      std::string oCountryNameString = oString.substr(pos + strlen(prefix));
      if (_enNavSelectionCriterionType == sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_POI)
      {
         oCountryNameString = oRemoveDescriptorInformation(oCountryNameString, "poi:");
         oCountryNameString = oRemoveDescriptorInformation(oCountryNameString, "cat:");
         oCountryNameString = oRemoveDescriptorInformation(oCountryNameString, "attr:");
      }
      else
      {
         oCountryNameString = oRemoveDescriptorInformation(oCountryNameString, "houseno:");
         oCountryNameString = oRemoveDescriptorInformation(oCountryNameString, "street:");
         oCountryNameString = oRemoveDescriptorInformation(oCountryNameString, "street2:");
      }

      oCountryNameString = oRemoveDescriptorInformation(oCountryNameString, "city:");
      oCountryNameString = oRemoveDescriptorInformation(oCountryNameString, "state:");
      oCountryNameString = oRemoveEmptySpaces(oCountryNameString);
      return oCountryNameString;
   }
   return "";
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oGetCategoryNameFromDescriptor(const std::string& oString) const
{
   const char* prefix = "cat:";
   size_t pos = oString.find(prefix);
   if (pos != std::string::npos)
   {
      std::string oCategoryNameString = oString.substr(pos + strlen(prefix));
      oCategoryNameString = oRemoveDescriptorInformation(oCategoryNameString, "poi:");
      oCategoryNameString = oRemoveDescriptorInformation(oCategoryNameString, "attr:");
      oCategoryNameString = oRemoveDescriptorInformation(oCategoryNameString, "state:");
      oCategoryNameString = oRemoveDescriptorInformation(oCategoryNameString, "city:");
      oCategoryNameString = oRemoveDescriptorInformation(oCategoryNameString, "country:");
      oCategoryNameString = oRemoveEmptySpaces(oCategoryNameString);
      return oCategoryNameString;
   }
   return "";
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oGetBrandNameFromDescriptor(const std::string& oString) const
{
   const char* prefix = "attr:";
   size_t pos = oString.find(prefix);
   if (pos != std::string::npos)
   {
      std::string oBrandNameString = oString.substr(pos + strlen(prefix));
      oBrandNameString = oRemoveDescriptorInformation(oBrandNameString, "poi:");
      oBrandNameString = oRemoveDescriptorInformation(oBrandNameString, "cat:");
      oBrandNameString = oRemoveDescriptorInformation(oBrandNameString, "state:");
      oBrandNameString = oRemoveDescriptorInformation(oBrandNameString, "city:");
      oBrandNameString = oRemoveDescriptorInformation(oBrandNameString, "country:");
      return oBrandNameString;
   }
   return "";
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oGetBuildingNameFromDescriptor(const std::string& oString) const
{
   const char* prefix = "poi:";
   size_t pos = oString.find(prefix);
   if (pos != std::string::npos)
   {
      std::string oBuildingNameString = oString.substr(pos + strlen(prefix));
      oBuildingNameString = oRemoveDescriptorInformation(oBuildingNameString, "cat:");
      oBuildingNameString = oRemoveDescriptorInformation(oBuildingNameString, "attr:");
      oBuildingNameString = oRemoveDescriptorInformation(oBuildingNameString, "state:");
      oBuildingNameString = oRemoveDescriptorInformation(oBuildingNameString, "city:");
      oBuildingNameString = oRemoveDescriptorInformation(oBuildingNameString, "country:");
      oBuildingNameString = oRemoveEmptySpaces(oBuildingNameString);
      return oBuildingNameString;
   }
   return "";
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oRemoveDescriptorInformation(std::string oString, const std::string& oDescriptorType) const
{
   if (oString.find(oDescriptorType) != std::string::npos)
   {
      return oString.erase(oString.find(oDescriptorType));
   }
   return oString;
}


/***********************************************************************//**
 * TODO jnd2hi: replace with StringUtils::trim()
 ***************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oRemoveEmptySpaces(std::string oString) const
{
   ETG_TRACE_USR1(("oRemoveEmptySpaces()- String before = %s", oString.c_str()));
   if (!oString.empty())
   {
      if (oString[oString.size() - 1] == ' ')
      {
         std::string oTrimmedString;
         oTrimmedString = oString.substr(0, oString.size() - 1);
         ETG_TRACE_USR1(("oRemoveEmptySpaces()- String after = %s", oTrimmedString.c_str()));
         return oTrimmedString;
      }
   }
   return oString;
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetRemainingAddressInformation()
{
   vSetCountryInformation(_oResultDescriptor.CountryName);
   vSetStateInformation(_oResultDescriptor.StateName);
   vSetCityInformation(_oResultDescriptor.CityName);
   vSetFirstStreetInformation(_oResultDescriptor.StreetName);
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetCountryInformation(const std::string& oCountryName)
{
   if (!oCountryName.empty())
   {
      ETG_TRACE_USR1(("vSetCountryInformation()- CountryName= %s", oCountryName.c_str()));
      SDSAddressElement sdsAddressElement;
      SDSAddressElementType sdsAddressElementType;
      sdsAddressElementType = SDSAddressElementType__SDS_ADDRESS_ELEMENT_TYPE_COUNTRY;
      sdsAddressElement.setAddressElementType(sdsAddressElementType);
      SDSElementOptions sdsElementOptions;
      sdsElementOptions.setIsAmbigious(false);
      sdsAddressElement.setElementOptions(sdsElementOptions);
      sdsAddressElement.setData(oCountryName);
      _oRequestedsdsAddress.push_back(sdsAddressElement);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetStateInformation(const std::string& oStateName)
{
   if (!oStateName.empty())
   {
      ETG_TRACE_USR1(("vSetStateInformation()- StateName= %s", oStateName.c_str()));
      SDSAddressElement sdsAddressElement;
      SDSAddressElementType sdsAddressElementType;
      sdsAddressElementType = SDSAddressElementType__SDS_ADDRESS_ELEMENT_TYPE_STATE;
      sdsAddressElement.setAddressElementType(sdsAddressElementType);
      SDSElementOptions sdsElementOptions;
      sdsElementOptions.setIsAmbigious(false);
      sdsAddressElement.setElementOptions(sdsElementOptions);
      sdsAddressElement.setData(oStateName);
      _oRequestedsdsAddress.push_back(sdsAddressElement);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetCityInformation(const std::string& oCityName)
{
   if (!oCityName.empty())
   {
      ETG_TRACE_USR1(("vSetCityInformation()- CityName = %s", oCityName.c_str()));
      SDSAddressElement sdsAddressElement;
      SDSAddressElementType sdsAddressElementType;
      sdsAddressElementType = SDSAddressElementType__SDS_ADDRESS_ELEMENT_TYPE_PLACE;
      sdsAddressElement.setAddressElementType(sdsAddressElementType);
      SDSElementOptions sdsElementOptions;
      sdsElementOptions.setIsAmbigious(false);
      sdsAddressElement.setElementOptions(sdsElementOptions);
      sdsAddressElement.setData(oCityName);
      _oRequestedsdsAddress.push_back(sdsAddressElement);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetFirstStreetInformation(const std::string& oStreetName)
{
   if (!oStreetName.empty())
   {
      ETG_TRACE_USR1(("vSetFirstStreetInformation()- StreetName= %s", oStreetName.c_str()));
      SDSAddressElement sdsAddressElement;
      SDSAddressElementType sdsAddressElementType;
      sdsAddressElementType = SDSAddressElementType__SDS_ADDRESS_ELEMENT_TYPE_ROAD;
      sdsAddressElement.setAddressElementType(sdsAddressElementType);
      SDSElementOptions sdsElementOptions;
      sdsElementOptions.setIsAmbigious(false);
      sdsAddressElement.setElementOptions(sdsElementOptions);
      sdsAddressElement.setData(oStreetName);
      _oRequestedsdsAddress.push_back(sdsAddressElement);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetSecondStreetInformation(const std::string& oStreetName)
{
   if (!oStreetName.empty())
   {
      ETG_TRACE_USR1(("vSetSecondStreetInformation()- StreetName= %s", oStreetName.c_str()));
      SDSAddressElement sdsAddressElement;
      SDSAddressElementType sdsAddressElementType;
      sdsAddressElementType = SDSAddressElementType__SDS_ADDRESS_ELEMENT_TYPE_CROSSROAD;
      sdsAddressElement.setAddressElementType(sdsAddressElementType);
      SDSElementOptions sdsElementOptions;
      sdsElementOptions.setIsAmbigious(false);
      sdsAddressElement.setElementOptions(sdsElementOptions);
      sdsAddressElement.setData(oStreetName);
      _oRequestedsdsAddress.push_back(sdsAddressElement);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetHouseNumberInformation(const std::string& oHouseNumber)
{
   if (!oHouseNumber.empty())
   {
      ETG_TRACE_USR1(("vSetHouseNumberInformation()- oHouseNumber= %s", oHouseNumber.c_str()));
      SDSAddressElement sdsAddressElement;
      SDSAddressElementType sdsAddressElementType;
      sdsAddressElementType = SDSAddressElementType__SDS_ADDRESS_ELEMENT_TYPE_HOUSENUMBER;
      sdsAddressElement.setAddressElementType(sdsAddressElementType);
      SDSElementOptions sdsElementOptions;
      sdsElementOptions.setIsAmbigious(false);
      sdsAddressElement.setElementOptions(sdsElementOptions);
      sdsAddressElement.setData(oHouseNumber);
      _oRequestedsdsAddress.push_back(sdsAddressElement);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSendsdsCheckAddressRequest()
{
   ETG_TRACE_USR1(("Send address information to Navi Hall"));

   _navigationProxy->sendSdsCheckAddressRequest(*this, _oRequestedsdsAddress);

   _oRequestedsdsAddress.clear();
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vRequestPOIListToNavi()
{
   ETG_TRACE_USR1(("RequestPOIList to Navi Hall"));

   FreeTextSearchScope TextSearchType = FreeTextSearchScope__FREETEXT_SEARCH_SCOPE_POI_AT_CURRENT_POSITION;

   if (_oMethodStart.tLocationType.enType == sds2hmi_fi_tcl_e8_NAV_LocationType::FI_EN_DEFAULT)
   {
      TextSearchType = FreeTextSearchScope__FREETEXT_SEARCH_SCOPE_POI_AT_LOCATION;
   }
   else if (_oMethodStart.tLocationType.enType == sds2hmi_fi_tcl_e8_NAV_LocationType::FI_EN_NEARBY)
   {
      TextSearchType = FreeTextSearchScope__FREETEXT_SEARCH_SCOPE_POI_AT_CURRENT_POSITION;
   }
   else if (_oMethodStart.tLocationType.enType == sds2hmi_fi_tcl_e8_NAV_LocationType::FI_EN_ALONG_ROUTE)
   {
      TextSearchType = FreeTextSearchScope__FREETEXT_SEARCH_SCOPE_POI_ALONG_ROUTE;
   }
   else if (_oMethodStart.tLocationType.enType == sds2hmi_fi_tcl_e8_NAV_LocationType::FI_EN_NEAR_DESTINATION)
   {
      TextSearchType = FreeTextSearchScope__FREETEXT_SEARCH_SCOPE_POI_AT_DESTINATION_POSITION;
   }

   std::string oSearchString = oGetPOISearchString();
   tU32 u32StartIndex = 0;
   uint8 destinationIndex = (uint8)(_destinationInformationSize - 1);
   tU32 u32TotalElements = 20;

   _navigationProxy->sendRequestFreeTextSearchResultsRequest(*this, oSearchString, TextSearchType, destinationIndex, u32StartIndex, u32TotalElements);
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oGetPOISearchString() const
{
   if (!_oPOIName.BrandName.empty())
   {
      return _oPOIName.BrandName;
   }
   else if (!_oPOIName.CategoryName.empty())
   {
      return _oPOIName.CategoryName;
   }
   else if (!_oPOIName.BuildingName.empty())
   {
      return _oPOIName.BuildingName;
   }
   return "";
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vHandleVerifiedSdsAddress(::std::vector< SDSAddressElement >& oVerifiedAddress, const SDSAddressOptions& sdsAddressOptions, ::std::vector< SDSAddressElementType >& ElementTypes)
{
   ETG_TRACE_USR1(("vHandleVerifiedsdsAddress()- Function entered"));

   vResetResultValues();

   ::std::vector< SDSAddressElement, ::std::allocator<SDSAddressElement> >:: iterator it;

   for (it = oVerifiedAddress.begin(); it != oVerifiedAddress.end(); ++it)
   {
      if (it->getAddressElementType() == SDSAddressElementType__SDS_ADDRESS_ELEMENT_TYPE_HOUSENUMBER)
      {
         _oResultDescriptor.HouseNr = it->getData();
      }
      else if (it->getAddressElementType() == SDSAddressElementType__SDS_ADDRESS_ELEMENT_TYPE_ROAD)
      {
         _oResultDescriptor.StreetName = it->getData();
      }
      else if (it->getAddressElementType() == SDSAddressElementType__SDS_ADDRESS_ELEMENT_TYPE_CROSSROAD)
      {
         _oResultDescriptor.CrossStreetName = it->getData();
      }
      else if (it->getAddressElementType() == SDSAddressElementType__SDS_ADDRESS_ELEMENT_TYPE_PLACE)
      {
         _oResultDescriptor.CityName = it->getData();
      }
      else if (it->getAddressElementType() == SDSAddressElementType__SDS_ADDRESS_ELEMENT_TYPE_STATE)
      {
         _oResultDescriptor.StateName = it->getData();
      }
      else if (it->getAddressElementType() == SDSAddressElementType__SDS_ADDRESS_ELEMENT_TYPE_COUNTRY)
      {
         _oResultDescriptor.CountryName = it->getData();
      }
   }

   vHandleElementTypeResult(ElementTypes);
   vValidateSdsAddressOptions(sdsAddressOptions);
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vResetResultValues()
{
   _oResultDescriptor.HouseNr.clear();
   _oResultDescriptor.StreetName.clear();
   _oResultDescriptor.CrossStreetName.clear();
   _oResultDescriptor.CityName.clear();
   _oResultDescriptor.StateName.clear();
   _oResultDescriptor.bAmbigous = FALSE;
   _oResultDescriptor.bNavigable = FALSE;
   _oResultDescriptor.bHouseNrAvailable = FALSE;
   _oResultDescriptor.bHouseNrValid = FALSE;
   _oResultDescriptor.bCrossRoadAvailable = FALSE;
}


/***********************************************************************//**l
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vValidateSdsAddressOptions(const SDSAddressOptions& sdsAddressOptions)
{
   ETG_TRACE_USR1(("vValidatesdsAddressOptions()- Function entered"));

   if (sdsAddressOptions.getNavigable() == TRUE)
   {
      _oResultDescriptor.bNavigable = TRUE;
   }
   if (sdsAddressOptions.getAmbigious() == TRUE)
   {
      _oResultDescriptor.bAmbigous = TRUE;
      _pMultipleDestiantionList->vRequestAmbigListToNavi();
   }
   if (sdsAddressOptions.getHouseNumberAvailable() == TRUE)
   {
      _oResultDescriptor.bHouseNrAvailable = TRUE;
   }
   if (sdsAddressOptions.getHouseNumberValid() == TRUE)
   {
      _oResultDescriptor.bHouseNrValid = TRUE;
   }
   if (_oResultDescriptor.bAmbigous == FALSE)
   {
      vSetDestinationResultforNonAmbiguity();
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vHandleElementTypeResult(::std::vector< SDSAddressElementType >& elementTypes)
{
   ETG_TRACE_USR1(("vHandleElementTypeResult()-Funtion Entered"));

   ::std::vector< SDSAddressElementType, ::std::allocator<SDSAddressElementType> >:: iterator it;

   for (it = elementTypes.begin(); it != elementTypes.end(); ++it)
   {
      if (*(it) == SDSAddressElementType__SDS_ADDRESS_ELEMENT_TYPE_CROSSROAD)
      {
         _oResultDescriptor.bCrossRoadAvailable = TRUE;
      }
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetDestinationResultforAmbiguity()
{
   //Intersecting street without ambiguity
   if ((!_oResultDescriptor.StreetName.empty()) &&
         (!_oResultDescriptor.CrossStreetName.empty()) &&
         (_oResultDescriptor.bCrossRoadAvailable == FALSE))
   {
      ETG_TRACE_USR1(("vSetDestinationResultforAmbiguity()- AddressMultipleResult for Intersecting Street"));
      vSetDestinationResult(ADDRESS_MULTIPLE_RESULT);
   }
   else if ((_oResultDescriptor.bCrossRoadAvailable == TRUE) &&
            (_oResultDescriptor.bHouseNrValid == FALSE))
   {
      ETG_TRACE_USR1(("vSetDestinationResultforAmbiguity()- JunctionMultilpleResult"));
      vSetDestinationResult(JUNCTION_MULTIPLE_RESULT);
   }
   //City center with Ambiguity
   else if (_oResultDescriptor.StreetName.empty() &&
            _oResultDescriptor.HouseNr.empty() &&
            (_enNavSelectionCriterionType == sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_ADDRESSES))
   {
      ETG_TRACE_USR1(("vSetDestinationResultforAmbiguity()- AddressMultipleResult for City center"));
      vSetDestinationResult(ADDRESS_MULTIPLE_RESULT);
   }

   else if (_oResultDescriptor.StreetName.empty() &&
            _oResultDescriptor.HouseNr.empty() &&
            (_enNavSelectionCriterionType == sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_POI))
   {
      ETG_TRACE_USR1(("vSetDestinationResultforAmbiguity()- POIMultilpleResult"));
      vSetDestinationResult(POI_MULTIPLE_RESULT);
   }
   else if ((_oResultDescriptor.bHouseNrValid == FALSE) &&
            (_oResultDescriptor.bHouseNrAvailable == TRUE))
   {
      ETG_TRACE_USR1(("vSetDestinationResultforAmbiguity()- AddressMultilpleResultForHouseNumberOutrange"));
      vSetDestinationResult(ADDRESS_MULTIPLE_RESULT_FOR_HOUSENR_OUTRANGE);
   }
   else if ((_oResultDescriptor.bHouseNrAvailable == TRUE) &&
            (_oResultDescriptor.bHouseNrValid == TRUE))
   {
      ETG_TRACE_USR1(("vSetDestinationResultforAmbiguity()- AddressMultilpleResult"));
      vSetDestinationResult(ADDRESS_MULTIPLE_RESULT);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetDestinationResultforNonAmbiguity()
{
   //Intersecting street without ambiguity
   if ((_oResultDescriptor.bCrossRoadAvailable == FALSE) &&
         (_oResultDescriptor.bNavigable == TRUE) &&
         (! _oResultDescriptor.CrossStreetName.empty()) &&
         (! _oResultDescriptor.StreetName.empty()))
   {
      ETG_TRACE_USR1(("vSetDestinationResultforNonAmbiguity()- Final JunctionResult"));
      vSetDestinationResult(ADDRESS_RESULT);
   }
   else if ((_oResultDescriptor.bCrossRoadAvailable == TRUE) &&
            (_oResultDescriptor.bHouseNrValid == FALSE))
   {
      ETG_TRACE_USR1(("vSetDestinationResultforNonAmbiguity()- JunctionResult"));
      vSetDestinationResult(JUNCTION_RESULT);
   }
   //City center without Ambiguity
   else if (_oResultDescriptor.StreetName.empty() &&
            _oResultDescriptor.HouseNr.empty() &&
            (_oResultDescriptor.bNavigable == TRUE) &&
            (_enNavSelectionCriterionType == sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_ADDRESSES))
   {
      ETG_TRACE_USR1(("vSetDestinationResultforNonAmbiguity()- AddressResult for City center"));
      vSetDestinationResult(ADDRESS_RESULT);
   }

   else if (_oResultDescriptor.StreetName.empty() &&
            _oResultDescriptor.HouseNr.empty() &&
            (_oResultDescriptor.bNavigable == TRUE) &&
            (_enNavSelectionCriterionType == sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_POI))
   {
      ETG_TRACE_USR1(("vSetDestinationResultforNonAmbiguity()- POI"));
      vRequestPOIListToNavi();
   }

   else if ((_oResultDescriptor.bHouseNrAvailable == TRUE) &&
            (_oResultDescriptor.bHouseNrValid == FALSE))
   {
      ETG_TRACE_USR1(("vSetDestinationResultforNonAmbiguity()- AddressHouseNrOutRangeResult"));
      vSetDestinationResult(ADDRESS_HOUSENO_OUTRANGE_RESULT);
   }

   else if ((_oResultDescriptor.bHouseNrValid == TRUE) &&
            (_oResultDescriptor.bNavigable == TRUE))
   {
      ETG_TRACE_USR1(("vSetDestinationResultforNonAmbiguity()- Address Result"));
      vSetDestinationResult(ADDRESS_RESULT);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSendListentrySelectionToNavi(tU8 ambiguousValue)
{
   ETG_TRACE_USR1(("vSendListentrySelectionToNavi()- Function entered"));
   if ((_pCommonShowDialog->GetsdsViewID() == SR_NAV_Ambig_List_Address) ||
         (_pCommonShowDialog->GetsdsViewID() == SR_NAV_Ambig_List_CityCenter) ||
         (_pCommonShowDialog->GetsdsViewID() == SR_NAV_Ambig_List_City))
   {
      _navigationProxy->sendSelectSdsRefinementRequest(*this, tU32(ambiguousValue));
      ETG_TRACE_USR1(("SelectSdsRefinementRequest is sent to Navi hall"));
      vSetDestinationResult(ADDRESS_RESULT);
   }
   else if (_pCommonShowDialog->GetsdsViewID() == SR_NAV_POI_List)
   {
      _navigationProxy->sendSetLocationWithFreeTextSearchInputRequest(*this, tU32(ambiguousValue));
      ETG_TRACE_USR1(("SetLocationWithFreeTextSearchInputRequest is sent to Navi hall"));
      tU16 u16NumberofElements = 0;
      vHandlePOIResult(u16NumberofElements, TRUE);
   }
}


/***********************************************************************//**
 * TODO jnd2hi: remove parameter bGuidancepossible which has become obsolete with sds2hmi FI v14.0.0
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vHandlePOIResult(tU16 numberOfElements, tBool bGuidancepossible)
{
   ETG_TRACE_USR1(("onRequestFreeTextSearchResultsResponse()- Function entered"));
   sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult oResult;
   oResult.nNumberOfElements = numberOfElements;
   vSetPOIResult(oResult, bGuidancepossible);
   vSendMethodResult(oResult);
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetDestinationResult(enResultType oResultType)
{
   ETG_TRACE_USR4(("vSetDestinationResult : Function Entered"));
   ETG_TRACE_USR4(("vSetDestinationResult : ResultType = %d", oResultType));

   if (oResultType == ADDRESS_RESULT || oResultType == ADDRESS_NOHOUSENO_RESULT)
   {
      _navigationProxy->sendSetLocationWithSdsInputRequest(*this);
      ETG_TRACE_USR1(("SetLocationWithSdsInputRequest is sent to Navi hall"));
   }

   sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult oResult;
   vPrepareSetDestinationResult(oResult, oResultType);
   vSendMethodResult(oResult);
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vPrepareSetDestinationResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult, enResultType oResultType) const
{
   ETG_TRACE_USR1(("vPrepareSetDestinationResult"));

   switch (oResultType)
   {
      case ADDRESS_NOHOUSENO_RESULT:
         vSetAddressNoHouseNrResult(oResult);
         break;

      case ADDRESS_RESULT:
         vSetAddressResult(oResult);
         break;

      case ADDRESS_HOUSENO_OUTRANGE_RESULT:
         vSetAddressHouseNrRangeResult(oResult);
         break;

      case ADDRESS_MULTIPLE_RESULT_FOR_HOUSENR_OUTRANGE:
         vSetAddressHouseNrRangeResult(oResult);
         break;

      case ADDRESS_MULTIPLE_RESULT:
         vSetMultipleAddressResult(oResult);
         break;

      case JUNCTION_RESULT:
         vSetJunctionResult(oResult);
         break;

      case JUNCTION_MULTIPLE_RESULT:
         vSetMultipleJunctionResult(oResult);
         break;

      case POI_MULTIPLE_RESULT:
         vSetPOIMultipleResult(oResult);
         break;

      default:
         ETG_TRACE_USR1(("vPrepareSetDestinationResult - default case"));
         break;
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetAddressNoHouseNrResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const
{
   sds2hmi_fi_tcl_e16_SelectionCriterionType enSelectionType;
   enSelectionType.enType = sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_RESERVED;
   oResult.nAnswerOptions.vSetAmbiguous(_oResultDescriptor.bAmbigous);
   oResult.nAnswerOptions.vSetGuidancePossible(_oResultDescriptor.bNavigable);
   oResult.nAnswerOptions.vSetHouseNumberInRange(FALSE);
   oResult.nAnswerOptions.vSetInvalidAddress(FALSE);
   oResult.tSelectionCriterionType.push_back(enSelectionType);
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetAddressResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const
{
   ETG_TRACE_USR1(("vSetAddressResult()- Function entered"));
   sds2hmi_fi_tcl_e16_SelectionCriterionType enSelectionType;
   enSelectionType.enType = sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_RESERVED;
   oResult.nAnswerOptions.vSetAmbiguous(FALSE);
   oResult.nAnswerOptions.vSetGuidancePossible(TRUE);
   oResult.nAnswerOptions.vSetHouseNumberInRange(TRUE);
   oResult.nAnswerOptions.vSetInvalidAddress(FALSE);
   oResult.tSelectionCriterionType.push_back(enSelectionType);
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetMultipleAddressResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const
{
   ETG_TRACE_USR1(("vSetMultipleAddressResult()- Function entered"));
   sds2hmi_fi_tcl_e16_SelectionCriterionType enSelectionType;
   enSelectionType.enType = sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_ADDRESSES;
   oResult.nAnswerOptions.vSetAmbiguous(TRUE);
   oResult.nAnswerOptions.vSetGuidancePossible(FALSE);
   oResult.nAnswerOptions.vSetHouseNumberInRange(TRUE);
   oResult.nAnswerOptions.vSetInvalidAddress(FALSE);
   oResult.tSelectionCriterionType.push_back(enSelectionType);
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetJunctionResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const
{
   sds2hmi_fi_tcl_e16_SelectionCriterionType enSelectionType;
   enSelectionType.enType = sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_JUNCTION;
   oResult.nAnswerOptions.vSetAmbiguous(FALSE);
   oResult.nAnswerOptions.vSetGuidancePossible(TRUE);
   oResult.nAnswerOptions.vSetHouseNumberInRange(FALSE);
   oResult.nAnswerOptions.vSetInvalidAddress(FALSE);
   oResult.tSelectionCriterionType.push_back(enSelectionType);
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetMultipleJunctionResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const
{
   sds2hmi_fi_tcl_e16_SelectionCriterionType enSelectionType;
   enSelectionType.enType = sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_JUNCTION;
   oResult.nAnswerOptions.vSetAmbiguous(TRUE);
   oResult.nAnswerOptions.vSetGuidancePossible(FALSE);
   oResult.nAnswerOptions.vSetHouseNumberInRange(FALSE);
   oResult.nAnswerOptions.vSetInvalidAddress(FALSE);
   oResult.tSelectionCriterionType.push_back(enSelectionType);
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetPOIMultipleResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const
{
   sds2hmi_fi_tcl_e16_SelectionCriterionType enSelectionType;
   enSelectionType.enType = sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_POI;
   oResult.nAnswerOptions.vSetAmbiguous(TRUE);
   oResult.nAnswerOptions.vSetGuidancePossible(FALSE);
   oResult.nAnswerOptions.vSetHouseNumberInRange(FALSE);
   oResult.nAnswerOptions.vSetInvalidAddress(FALSE);
   oResult.tSelectionCriterionType.push_back(enSelectionType);
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetAddressHouseNrRangeResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const
{
   sds2hmi_fi_tcl_e16_SelectionCriterionType enSelectionType;
   enSelectionType.enType = sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_HOUSENUMBER;
   oResult.nAnswerOptions.vSetAmbiguous(_oResultDescriptor.bAmbigous);
   oResult.nAnswerOptions.vSetGuidancePossible(_oResultDescriptor.bNavigable);
   oResult.nAnswerOptions.vSetHouseNumberInRange(FALSE);
   oResult.nAnswerOptions.vSetInvalidAddress(FALSE);
   oResult.tSelectionCriterionType.push_back(enSelectionType);
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_NaviSetDestinationItem::vSetPOIResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult, tBool bGuidancepossible) const
{
   sds2hmi_fi_tcl_e16_SelectionCriterionType enSelectionType;
   enSelectionType.enType = sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_RESERVED;
   oResult.nAnswerOptions.vSetAmbiguous(FALSE);
   oResult.nAnswerOptions.vSetHouseNumberInRange(FALSE);
   oResult.nAnswerOptions.vSetInvalidAddress(FALSE);
   oResult.nAnswerOptions.vSetGuidancePossible(bGuidancepossible);
   oResult.tSelectionCriterionType.push_back(enSelectionType);
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oGetOsdeLocationDescriptor() const
{
   std::string oOsdeString(oGetStateDescriptor());
   oOsdeString.append(oGetTownDescriptor());
   oOsdeString.append(oGetStreetDescriptor());
   return oOsdeString;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oGetStateDescriptor() const
{
   return "state:" + _oResultDescriptor.StateName;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oGetTownDescriptor() const
{
   return "city:" + _oResultDescriptor.CityName;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_Method_NaviSetDestinationItem::oGetStreetDescriptor() const
{
   return "street:" + _oResultDescriptor.StreetName;
}


/***********************************************************************//**
 *
 ***************************************************************************/
bpstl::string clSDS_Method_NaviSetDestinationItem::oGetHouseNumberString()
{
   bpstl::string oHouseNumberString("");
   for (tU32 u32Index = 0; u32Index < _oMethodStart.tLocationDescription.Descriptors.size(); u32Index++)
   {
      if ((_oMethodStart.tLocationDescription.Descriptors[u32Index].SelectionCriterion.enType == sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_HOUSENUMBER) ||
            (_oMethodStart.tLocationDescription.Descriptors[u32Index].SelectionCriterion.enType == sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_ADDRESSES))
      {
         tString pString  = ((const fi_tcl_StringPosition&) * _oMethodStart.tLocationDescription.Descriptors[u32Index].poDataData).String.szGet(fi_tclString::FI_EN_UTF8);
         oHouseNumberString = oGetHouseNumberFromDescriptor(pString);

         OSAL_DELETE[] pString;
      }
   }
   return oHouseNumberString;
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetDestinationItem::vAmbiguityListResolved()
{
   ETG_TRACE_USR1(("vAmbiguityListResolved"));
   vSetDestinationResultforAmbiguity();
}
