/*********************************************************************//**
 * \file       clSDS_ConfigurationFlags.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "application/clSDS_ConfigurationFlags.h"
#include "application/clSDS_KDSConfiguration.h"
#include "application/clSDS_StringVarList.h"
#include "application/StringUtils.h"


std::map<std::string, std::string> clSDS_ConfigurationFlags::dynamicVariables;


clSDS_ConfigurationFlags::clSDS_ConfigurationFlags()
{
}


clSDS_ConfigurationFlags::~clSDS_ConfigurationFlags()
{
}


bool clSDS_ConfigurationFlags::getNavKey()
{
//	get from KDS
//	using configElement == AIVIVariantCoding
//  check \nincg3_GEN\ai_projects\generated\components\datapool\access\dpIfKdsKeyValueAccess.cpp
   return true; //TODO raa8hi
}


bool clSDS_ConfigurationFlags::getNavAdrsKey()
{
   switch (clSDS_KDSConfiguration::getMarketRegionType())
   {
      case clSDS_KDSConfiguration::USA:
      case clSDS_KDSConfiguration::CAN:
      case clSDS_KDSConfiguration::MEX:
      case clSDS_KDSConfiguration::UK:
      case clSDS_KDSConfiguration::TKY:
      case clSDS_KDSConfiguration::RUS:
      case clSDS_KDSConfiguration::OTHER_EUR:
      case clSDS_KDSConfiguration::TWN:
      case clSDS_KDSConfiguration::GCC:
      case clSDS_KDSConfiguration::EGP:
      case clSDS_KDSConfiguration::ASR_NZR:
      case clSDS_KDSConfiguration::BRA:
      case clSDS_KDSConfiguration::AGT:
      case clSDS_KDSConfiguration::IND:
      case clSDS_KDSConfiguration::JPN:
      case clSDS_KDSConfiguration::KOR:
         return true;

      default:
         return false;
   }
}


bool clSDS_ConfigurationFlags::getNavStateAvailable()
{
   switch (clSDS_KDSConfiguration::getMarketRegionType())
   {
      case clSDS_KDSConfiguration::USA:
      case clSDS_KDSConfiguration::CAN:
      case clSDS_KDSConfiguration::MEX:
         return true;

      default:
         return false;
   }
}


bool clSDS_ConfigurationFlags::getNavCountryAvailable()
{
   switch (clSDS_KDSConfiguration::getMarketRegionType())
   {
      case clSDS_KDSConfiguration::UK:
      case clSDS_KDSConfiguration::TKY:
      case clSDS_KDSConfiguration::RUS:
      case clSDS_KDSConfiguration::OTHER_EUR:
      case clSDS_KDSConfiguration::GCC:
      case clSDS_KDSConfiguration::ASR_NZR:
         return true;

      default:
         return false;
   }
}


bool clSDS_ConfigurationFlags::getPOIKey()
{
   //same value as B_NAV_ADRS
   return getNavAdrsKey();
}


bool clSDS_ConfigurationFlags::getPOICatKey()
{
   //same value as B_NAV
   return getNavKey();
}


bool clSDS_ConfigurationFlags::getAudHDKey()
{
   switch (clSDS_KDSConfiguration::getMarketRegionType())
   {
      case clSDS_KDSConfiguration::USA:
      case clSDS_KDSConfiguration::CAN:
      case clSDS_KDSConfiguration::MEX:
      case clSDS_KDSConfiguration::UK:
      case clSDS_KDSConfiguration::TKY:
      case clSDS_KDSConfiguration::RUS:
      case clSDS_KDSConfiguration::OTHER_EUR:
         return true;

      default: //TODO raa8hi - check values with Denny
         return false;
   }
}


bool clSDS_ConfigurationFlags::getAudRDSKey()
{
   switch (clSDS_KDSConfiguration::getMarketRegionType())
   {
      case clSDS_KDSConfiguration::USA:
      case clSDS_KDSConfiguration::CAN:
      case clSDS_KDSConfiguration::MEX:
      case clSDS_KDSConfiguration::UK:
      case clSDS_KDSConfiguration::TKY:
      case clSDS_KDSConfiguration::RUS:
      case clSDS_KDSConfiguration::OTHER_EUR:
         return true;

      default: //TODO raa8hi - check values with Denny
         return false;
   }
}


bool clSDS_ConfigurationFlags::getAudDABKey()
{
   return (clSDS_KDSConfiguration::getDABMounted() == 1);
}


bool clSDS_ConfigurationFlags::getAudSXMKey()
{
   return (clSDS_KDSConfiguration::getXMTunerMounted() == 1);
}


unsigned int clSDS_ConfigurationFlags::getAudFreqTypeKey()
{
   switch (clSDS_KDSConfiguration::getMarketRegionType())
   {
      case clSDS_KDSConfiguration::USA:
      case clSDS_KDSConfiguration::CAN:
      case clSDS_KDSConfiguration::MEX:
         return 0;

      case clSDS_KDSConfiguration::UK:
      case clSDS_KDSConfiguration::TKY:
      case clSDS_KDSConfiguration::RUS:
      case clSDS_KDSConfiguration::OTHER_EUR:
         return 1;

      default:
         return 2; //TODO raa8hi - other values (2-4) have be checked with tuner colleagues
   }

// ------cases from previous project-------------------
//      case KOR:
//      case SAF:
//      case THI://What about THI and JPN value for frequency
//      case HKG_MACAU:
//      case JPN:
//      case TWN:
//      case IND:
//      case GCC:
//      case PRC:
//          return 2;
//
//      case BRA:
//      case OTHER_LAC:
//          return 3;
//
//      case ASR_NZR:
//          return 4;
//
//      default:
//         return 0;
//----------------------------------------------------------
}


bool clSDS_ConfigurationFlags::getInfKey()
{
   return true;
}


bool clSDS_ConfigurationFlags::getInfTrafficKey()
{
   switch (clSDS_KDSConfiguration::getMarketRegionType())
   {
      case clSDS_KDSConfiguration::USA:
      case clSDS_KDSConfiguration::CAN:
      case clSDS_KDSConfiguration::UK:
      case clSDS_KDSConfiguration::TKY:
      case clSDS_KDSConfiguration::RUS:
      case clSDS_KDSConfiguration::OTHER_EUR:
      case clSDS_KDSConfiguration::PRC:
      case clSDS_KDSConfiguration::ASR_NZR:
      case clSDS_KDSConfiguration::JPN:
         return true;

      default:
         return false;
   }
}


bool clSDS_ConfigurationFlags::getInfHEVKey()
{
   return (clSDS_KDSConfiguration::getHEVInformationFunction() == 1);
}


bool clSDS_ConfigurationFlags::getBluetoothPhoneEnabledKey()
{
   return (clSDS_KDSConfiguration::getBluetoothPhoneEnabled() == 1);
}


void clSDS_ConfigurationFlags::setDynamicVariable(const std::string& varName, const std::string& varValue)
{
   dynamicVariables[varName] = varValue;
}


std::map<std::string, std::string>  clSDS_ConfigurationFlags::getConfigurationFlags()
{
   std::map<std::string, std::string> variables;

   variables["B_NAV"] = StringUtils::toString(getNavKey());
   variables["B_NAV_ADRS"] = StringUtils::toString(getNavAdrsKey());
   variables["B_NAV_POI"] = StringUtils::toString(getPOIKey());
   variables["B_NAV_POICAT"] = StringUtils::toString(getPOICatKey());
   variables["B_NAV_CHNG_STATE"] = StringUtils::toString(getNavStateAvailable());
   variables["B_NAV_CHNG_CNTRY"] = StringUtils::toString(getNavCountryAvailable());

   variables["B_AUD_DAB"] = StringUtils::toString(getAudDABKey());
   variables["B_AUD_SXM"] = StringUtils::toString(getAudSXMKey());
   variables["B_AUD_RDS"] = StringUtils::toString(getAudRDSKey());
   variables["B_AUD_HD"] = StringUtils::toString(getAudHDKey());
   variables["E_AUD_FREQCY_TYPE"] = StringUtils::toString(getAudFreqTypeKey());

   variables["B_INF"] = StringUtils::toString(getInfKey());
   variables["B_INF_TRA"] = StringUtils::toString(getInfTrafficKey());
   variables["B_INF_HEV"] = StringUtils::toString(getInfHEVKey());

   variables["B_PHO"] = StringUtils::toString(getBluetoothPhoneEnabledKey());
   variables["B_PHO_BT_AVAILABLE"] = StringUtils::toString(getBluetoothPhoneEnabledKey()); //TODO raa8hi to be removed because B_PHO_BT_AVAILABLE --> B_PHO

   variables.insert(dynamicVariables.begin(), dynamicVariables.end());

   return variables;
}
