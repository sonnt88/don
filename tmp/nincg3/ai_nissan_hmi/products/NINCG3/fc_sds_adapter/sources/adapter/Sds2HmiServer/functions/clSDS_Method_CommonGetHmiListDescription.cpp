/*********************************************************************//**
 * \file       clSDS_Method_CommonGetHmiListDescription.cpp
 *
 * clSDS_Method_CommonGetHmiListDescription method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_CommonGetHmiListDescription.h"
#include "application/clSDS_List.h"

#include "SdsAdapter_Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_CommonGetHmiListDescription.cpp.trc.h"
#endif


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_CommonGetHmiListDescription::clSDS_Method_CommonGetHmiListDescription(
   ahl_tclBaseOneThreadService* pService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_COMMONGETHMILISTDESCRIPTION, pService)
   , _pCurrentList(NULL)
   , _isDescriptorTagAddressEntry(false)
{
}


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_CommonGetHmiListDescription::~clSDS_Method_CommonGetHmiListDescription()
{
   _pCurrentList = NULL;
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_CommonGetHmiListDescription::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgCommonGetHMIListDescriptionMethodStart oMessage;
   sds2hmi_sdsfi_tclMsgCommonGetHMIListDescriptionMethodResult oResult;
   vGetDataFromAmt(pInMessage, oMessage);
   if (bCheckHmiListDescriptionType(oMessage, oResult))
   {
      vSendMethodResult(oResult);
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
bool clSDS_Method_CommonGetHmiListDescription::bCheckHmiListDescriptionType(
   const sds2hmi_sdsfi_tclMsgCommonGetHMIListDescriptionMethodStart& oMessage,
   sds2hmi_sdsfi_tclMsgCommonGetHMIListDescriptionMethodResult& oResultListDescription)
{
   ETG_TRACE_USR1(("MenuType:%d", oMessage.ListType.enType));

   std::map<sds2hmi_fi_tcl_e8_HMI_ListType::tenType, clSDS_List*>::iterator it;
   it = _oListMap.find(oMessage.ListType.enType);
   _pCurrentList = it->second;

   switch (oMessage.ListType.enType)
   {
      case sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_ADDRESSBOOK:
         vSetAddressBookItems(oResultListDescription);
         return true;
      default:
         return false;
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_CommonGetHmiListDescription::vAddList(
   sds2hmi_fi_tcl_e8_HMI_ListType::tenType type,
   clSDS_List* pList)
{
   if (pList != NULL)
   {
      _oListMap[type] = pList;
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_CommonGetHmiListDescription::vSetAddressBookItems(
   sds2hmi_sdsfi_tclMsgCommonGetHMIListDescriptionMethodResult& oResult)
{
   std::string oPOIBrandName("");
   std::string oPOICategoryName("");
   // ToDO:house no,street,city & state in address book.Need to check with Navi for a proper order in address book.
   if (_pCurrentList)
   {
      unsigned long oAddressBookCount =  _pCurrentList->u32GetSize();

      for (unsigned int u32Index = 0; u32Index < oAddressBookCount; u32Index++)
      {
         _isDescriptorTagAddressEntry = false;
         sds2hmi_fi_tcl_HMIElementDescrptionList oHMIElementDescrptionList;
         std::string oAddressString  = _pCurrentList->oGetHMIListDescriptionItems(u32Index);
         oPOICategoryName = oGetCategoryNameFromDescriptor(oAddressString);
         oPOIBrandName = oGetBrandNameFromDescriptor(oAddressString);
         vSetAddressHomeEntry(oHMIElementDescrptionList, oAddressString);
         vSetPOIBrandName(oHMIElementDescrptionList, oPOIBrandName);
         vSetPOICategoryName(oHMIElementDescrptionList, oPOICategoryName);
         vSetAddressEntry(oHMIElementDescrptionList, oAddressString);
         oResult.DescriptionList.push_back(oHMIElementDescrptionList);
      }
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_Method_CommonGetHmiListDescription::oGetCategoryNameFromDescriptor(
   const std::string& oString) const
{
   std::string oCategoryNameString = " ";
   if (!oString.empty())
   {
      size_t  const oCategoryStartPos = oString.find('(');
      size_t  const oCategoryEndPos = oString.find(')', oCategoryStartPos);
      if ((oCategoryStartPos != std::string::npos) && (oCategoryEndPos != std::string::npos))
      {
         oCategoryNameString = oString.substr(oCategoryStartPos, oCategoryEndPos);
         oCategoryNameString = oCategoryNameString.erase(0, oCategoryNameString.find('(') + 1);
         oCategoryNameString = oCategoryNameString.erase(oCategoryNameString.size() - 1);
      }
   }
   return oCategoryNameString;
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_Method_CommonGetHmiListDescription::oGetBrandNameFromDescriptor(
   const std::string& oString) const
{
   std::string oBandNameString = " ";
   if (!oString.empty())
   {
      size_t const oBrandStartPos = oString.find('(');
      size_t const oBrandEndPos = oString.find(')', oBrandStartPos);
      if ((oBrandStartPos != std::string::npos) && (oBrandEndPos != std::string::npos))
      {
         std::string  oCategoryNameStringstr = oString.substr(oBrandStartPos, oBrandEndPos);
         oBandNameString  = oRemoveDescriptorInformation(oString, oCategoryNameStringstr);
      }
   }
   return oBandNameString;
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_CommonGetHmiListDescription::vSetAddressHomeEntry(
   sds2hmi_fi_tcl_HMIElementDescrptionList& oHMIElementDescrptionList,
   const std::string& oAddressEntry)
{
   //TODO: Need to check  Home entry with Navi.
   if (oAddressEntry.compare("My Home") == 0)
   {
      _isDescriptorTagAddressEntry  = true;
      sds2hmi_fi_tcl_HMIElementDescription oHMIElementDescription;
      oHMIElementDescription.DescriptorTag = "PLAINTEXT";
      oHMIElementDescription.DescriptorValue = "My Home";
      oHMIElementDescrptionList.DescriptionList.push_back(oHMIElementDescription);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_CommonGetHmiListDescription::vSetPOIBrandName(
   sds2hmi_fi_tcl_HMIElementDescrptionList& oHMIElementDescrptionList,
   const std::string& oPOIBrandName)
{
   if (!oPOIBrandName.empty())
   {
      _isDescriptorTagAddressEntry = true;
      sds2hmi_fi_tcl_HMIElementDescription oHMIElementDescription;
      oHMIElementDescription.DescriptorTag = "POIBRANDNAME";
      oHMIElementDescription.DescriptorValue = oPOIBrandName.c_str();
      oHMIElementDescrptionList.DescriptionList.push_back(oHMIElementDescription);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_CommonGetHmiListDescription::vSetPOICategoryName(
   sds2hmi_fi_tcl_HMIElementDescrptionList& oHMIElementDescrptionList,
   const std::string& oPOICategoryName)
{
   if (!oPOICategoryName.empty())
   {
      _isDescriptorTagAddressEntry = true;
      sds2hmi_fi_tcl_HMIElementDescription oHMIElementDescription;
      oHMIElementDescription.DescriptorTag = "POICATEGORYNAME";
      oHMIElementDescription.DescriptorValue = oPOICategoryName.c_str();
      oHMIElementDescrptionList.DescriptionList.push_back(oHMIElementDescription);
   }
}


/***********************************************************************************************//**
 *SDS  didn’t find suitable Descriptor tag for the address entry.DescriptorTag is PlainText
 ***************************************************************************************************/
void clSDS_Method_CommonGetHmiListDescription::vSetAddressEntry(
   sds2hmi_fi_tcl_HMIElementDescrptionList& oHMIElementDescrptionList,
   const std::string& oAddressString)const
{
   if ((!oAddressString.empty()) && (!_isDescriptorTagAddressEntry))
   {
      sds2hmi_fi_tcl_HMIElementDescription oHMIElementDescription;
      oHMIElementDescription.DescriptorTag = "PLAINTEXT";
      oHMIElementDescription.DescriptorValue = oAddressString.c_str();
      ETG_TRACE_USR1(("oAddressString:%d", oAddressString.c_str()));
      oHMIElementDescrptionList.DescriptionList.push_back(oHMIElementDescription);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
std::string clSDS_Method_CommonGetHmiListDescription::oRemoveDescriptorInformation(
   std::string oString, const std::string& oDescriptorType) const
{
   if (oString.find(oDescriptorType) != std::string::npos)
   {
      return oString.erase(oString.find(oDescriptorType));
   }
   return oString;
}
