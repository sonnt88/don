/*********************************************************************//**
 * \file       AudioSourceHandler.cpp
 *
 * AcrHandler class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "AudioSourceHandler.h"
#include "clSDS_SDSStatus.h"
#include "SdsAudioSource.h"


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
AudioSourceHandler::AudioSourceHandler(clSDS_SDSStatus* pSDSStatus, SdsAudioSource& audioSource)
   : _pSDSStatus(pSDSStatus),
     _audioSource(audioSource)
{
   _pSDSStatus->vRegisterObserver(this);
}


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
AudioSourceHandler::~AudioSourceHandler()
{
   _pSDSStatus = NULL;
}


/**************************************************************************//**

 ******************************************************************************/
void AudioSourceHandler::vSDSStatusChanged()
{
   if (_pSDSStatus->getStatus() == clSDS_SDSStatus::EN_PAUSE)
   {
      _audioSource.bSetAudioRouteRequest(ARL_SRC_VRU, ARL_EN_ISRC_ACT_OFF);
   }
}
