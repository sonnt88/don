/*********************************************************************//**
 * \file       clSDS_Property_ConnectedDeviceStatus.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Property_ConnectedDeviceStatus.h"
#include "external/sds2hmi_fi.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Property_ConnectedDeviceStatus::~clSDS_Property_ConnectedDeviceStatus()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Property_ConnectedDeviceStatus::clSDS_Property_ConnectedDeviceStatus(ahl_tclBaseOneThreadService* pService)
   : clServerProperty(SDS2HMI_SDSFI_C_U16_CONNECTEDDEVICESTATUS, pService)
   , _mediaState(0)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_ConnectedDeviceStatus::vSet(amt_tclServiceData* pInMsg)
{
   sds2hmi_sdsfi_tclMsgConnectedDeviceStatusSet message;
   vGetDataFromAmt(pInMsg, message);
   this->_connectedDeviceStatus = message.ConnectedDeviceStatus;
   this->_mediaState = message.MediaState;
   this->_e8Slot = message.e8Slot;
   vSendStatus();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_ConnectedDeviceStatus::vGet(amt_tclServiceData*  /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_ConnectedDeviceStatus::vUpreg(amt_tclServiceData*  /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_ConnectedDeviceStatus::vSendStatus()
{
   sds2hmi_sdsfi_tclMsgConnectedDeviceStatusStatus message;
   message.ConnectedDeviceStatus = this->_connectedDeviceStatus;
   message.MediaState = this->_mediaState;
   message.e8Slot = this->_e8Slot;
   vStatus(message);
}
