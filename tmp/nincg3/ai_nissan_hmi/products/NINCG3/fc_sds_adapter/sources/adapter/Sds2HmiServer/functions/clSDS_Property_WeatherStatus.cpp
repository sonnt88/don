/*********************************************************************//**
 * \file       clSDS_Property_WeatherStatus.cpp
 *
 * Common Action Request property implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Property_WeatherStatus.h"
#include "external/sds2hmi_fi.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Property_WeatherStatus::~clSDS_Property_WeatherStatus()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Property_WeatherStatus::clSDS_Property_WeatherStatus(ahl_tclBaseOneThreadService* pService)
   : clServerProperty(SDS2HMI_SDSFI_C_U16_TUNERSTATUS, pService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_WeatherStatus::vSet(amt_tclServiceData* /*pInMsg*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_WeatherStatus::vGet(amt_tclServiceData* /*pInMsg*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_WeatherStatus::vUpreg(amt_tclServiceData* /*pInMsg*/)
{
   sds2hmi_sdsfi_tclMsgWeatherStatusStatus oMessage;
   oMessage.Status.enType = sds2hmi_fi_tcl_e8_WEA_Status::FI_EN_IDLE;
   oMessage.MenuType.enType = sds2hmi_fi_tcl_e8_WEA_MenuType::FI_EN_MAIN;
   vStatus(oMessage);
}
