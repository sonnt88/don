/*********************************************************************//**
 * \file       clSDS_PhoneStatusObserver.h
 *
 * PhonebookStatusObserver (AIF)
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_PhoneStatusObserver_h
#define clSDS_PhoneStatusObserver_h

#include "MOST_BTSet_FIProxy.h"
#include "asf/core/Types.h"

class clSDS_PhoneStatusObserver
{
   public:
      virtual ~clSDS_PhoneStatusObserver();
      clSDS_PhoneStatusObserver();
      virtual void phoneStatusChanged(uint8 deviceHandle, most_BTSet_fi_types::T_e8_BTSetDeviceStatus status) = 0;
};


#endif
