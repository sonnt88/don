/*********************************************************************//**
 * \file       clServerMethod.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clMethodServer_h
#define clMethodServer_h


#include "Sds2HmiServer/framework/clFunction.h"
#include <vector>

#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#include "fi_msgfw_if.h"


class clServerMethod: public clFunction
{
   public:
      virtual ~clServerMethod();
      clServerMethod(tU16 u16FunctionID, ahl_tclBaseOneThreadService* pService);
      virtual tVoid vHandleMessage(amt_tclServiceData* pInMsg);
      tVoid vSendMethodResult(const fi_tclTypeBase& oOutData);
      tVoid vSendErrorMessage(tU16 u16ErrorCode);

   protected:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg) = 0;
      tVoid vSendMethodResult();

   private:
      tVoid vSaveResponseParameters(const amt_tclServiceData* pInMessage);

      tU16 _u16DestAppID;
      tU16 _u16RegisterID;
      tU16 _u16CmdCtr;
};


#endif
