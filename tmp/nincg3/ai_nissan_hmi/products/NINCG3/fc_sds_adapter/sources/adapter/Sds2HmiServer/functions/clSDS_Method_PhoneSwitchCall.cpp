/*********************************************************************//**
 * \file       clSDS_Method_PhoneSwitchCall.cpp
 *
 * clSDS_Method_PhoneSwitchCall method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_PhoneSwitchCall.h"
#include "external/sds2hmi_fi.h"


using namespace MOST_Tel_FI;
using namespace most_Tel_fi_types;


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_PhoneSwitchCall::~clSDS_Method_PhoneSwitchCall()
{
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_PhoneSwitchCall::clSDS_Method_PhoneSwitchCall(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > pSds2TelProxy)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_PHONESWITCHCALL, pService)
   , _telephoneProxy(pSds2TelProxy)
{
   _bActiveCallPresent = false;
   _bHoldCallPresent = false;
   _u16ActiveCallInstance = 0xFF;
   _u16HoldCallInstance = 0xFF;
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_PhoneSwitchCall::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   sendSwapCall();
   vSendMethodResult();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneSwitchCall::onSwapCallError(
   const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ::MOST_Tel_FI::SwapCallError >& /*error*/)
{
   vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneSwitchCall::onSwapCallResult(
   const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ::MOST_Tel_FI::SwapCallResult >& /*result*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneSwitchCall::onAvailable(
   const ::boost::shared_ptr< asf::core::Proxy >& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _telephoneProxy)
   {
      _telephoneProxy->sendCallStatusNoticeUpReg(*this);
   }
}


void clSDS_Method_PhoneSwitchCall::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _telephoneProxy)
   {
      _telephoneProxy->sendCallStatusNoticeRelUpRegAll();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneSwitchCall::onCallStatusNoticeError(
   const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeError >& /*error*/)
{
   vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneSwitchCall::onCallStatusNoticeStatus(
   const ::boost::shared_ptr< MOST_Tel_FIProxy >& proxy,
   const ::boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeStatus >& status)
{
   if (proxy == _telephoneProxy)
   {
      evaluateHoldCallPresent(status);
      evaluateActiveCallPresent(status);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneSwitchCall::evaluateHoldCallPresent(
   const boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeStatus >& status)
{
   bool bHoldCallPresent = false;
   for (size_t i = 0 ; i < status->getOCallStatusNoticeStream().size() ; i++)
   {
      if (status->getOCallStatusNoticeStream()[i].getE8CallStatus() == T_e8_TelCallStatus__e8ON_HOLD)
      {
         bHoldCallPresent = true;
         _bHoldCallPresent = bHoldCallPresent;
         _u16HoldCallInstance = status->getOCallStatusNoticeStream()[i].getU16CallInstance();
      }
   }
   if (bHoldCallPresent == false)
   {
      _bHoldCallPresent = false;
      _u16HoldCallInstance = 0xFF;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneSwitchCall::evaluateActiveCallPresent(
   const boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeStatus >& status)
{
   bool bActiveCallPresent = false;
   for (size_t i = 0 ; i < status->getOCallStatusNoticeStream().size() ; i++)
   {
      if (status->getOCallStatusNoticeStream()[i].getE8CallStatus() == T_e8_TelCallStatus__e8ACTIVE)
      {
         bActiveCallPresent = true;
         _bActiveCallPresent = bActiveCallPresent;
         _u16ActiveCallInstance = (tU16)i;
      }
   }
   if (bActiveCallPresent == false)
   {
      _bActiveCallPresent = false;
      _u16ActiveCallInstance = 0xFF;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneSwitchCall::sendSwapCall()
{
   T_TelStreamOfCallInstances callInstances;
   callInstances.clear();
   callInstances.push_back(_u16ActiveCallInstance);
   callInstances.push_back(_u16HoldCallInstance);
   if (callInstances.size())
   {
      _telephoneProxy->sendSwapCallStart(*this, callInstances);
   }
}
