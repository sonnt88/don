/*********************************************************************//**
 * \file       clSDS_Userword_Entry.cpp
 *
 * clSDS_Userword_Entry class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_Userword_Entry.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Userword_Entry::~clSDS_Userword_Entry()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Userword_Entry::clSDS_Userword_Entry()
   : _bAvail(FALSE),
     _u32UWID(0)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userword_Entry::vSetName(const bpstl::string& strName)
{
   _strName = strName;
}


/**************************************************************************//**
*
******************************************************************************/
const bpstl::string& clSDS_Userword_Entry::oGetName() const
{
   return _strName;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userword_Entry::vSetNumber(const bpstl::string& strPhoneNumber)
{
   _strPhoneNumber = strPhoneNumber;
}


/**************************************************************************//**
*
******************************************************************************/
const bpstl::string& clSDS_Userword_Entry::oGetNumber() const
{
   return _strPhoneNumber;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userword_Entry::vSetUWID(tU32 u32UWID)
{
   _u32UWID = u32UWID;
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_Userword_Entry::u32GetUWID() const
{
   return _u32UWID;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userword_Entry::vSetAvail(tBool bAvail)
{
   _bAvail = bAvail;
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_Userword_Entry::bGeAvail() const
{
   return _bAvail;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userword_Entry::vMarkAsNotUsed(tVoid)
{
   vSetAvail(FALSE);
   vSetName("");
   vSetNumber("");
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userword_Entry::vSetupFromBuffer(tPCU8 pcu8Buf, tU32 u32Length)
{
   if (u32Length < 10) // minimum size for item data
   {
      NORMAL_M_ASSERT_ALWAYS();  // unexpected data size
      return;	//lint !e527 Unreachable code - jnd2hi: behind assertion
   }

   tPCU8 pu8Field = pcu8Buf;

   OSAL_pvMemoryCopy(&_bAvail, pu8Field, sizeof(_bAvail));
   pu8Field += sizeof(_bAvail);
   OSAL_pvMemoryCopy(&_u32UWID, pu8Field, sizeof(_u32UWID));
   pu8Field += sizeof(_u32UWID);

   tU8* pu8TempField = const_cast<tU8*>(pu8Field);
   _strPhoneNumber = (tChar*) pu8TempField;

   pu8Field += (_strPhoneNumber.size() + sizeof(tU8));

   pu8TempField = const_cast<tU8*>(pu8Field);
   _strName = (tChar*) pu8TempField;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userword_Entry::vGetBuffer(tPCU8 pcu8Buf, tU32 u32MaxLength) const
{
   if (u32MaxLength < u32GetBufferSize())
   {
      NORMAL_M_ASSERT_ALWAYS();  // allocated buffer is too small
      return;	//lint !e527 Unreachable code - jnd2hi: behind assertion
   }

   tU8* pu8Field = const_cast<tU8*>(pcu8Buf);

   OSAL_pvMemoryCopy(pu8Field, &_bAvail, sizeof(_bAvail));
   pu8Field += sizeof(_bAvail);
   OSAL_pvMemoryCopy(pu8Field, &_u32UWID, sizeof(_u32UWID));
   pu8Field += sizeof(_u32UWID);
   OSAL_pvMemoryCopy(pu8Field, _strPhoneNumber.c_str(), _strPhoneNumber.size());
   pu8Field += _strPhoneNumber.size();
   *pu8Field = 0x00;
   pu8Field++;
   OSAL_pvMemoryCopy(pu8Field, _strName.c_str(), _strName.size());
   pu8Field += _strName.size();
   *pu8Field = 0x00;
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_Userword_Entry::u32GetBufferSize() const
{
   tU32 u32ReturnSize = sizeof(_bAvail);
   u32ReturnSize += sizeof(_u32UWID);
   u32ReturnSize += _strPhoneNumber.size() + sizeof(tU8);
   u32ReturnSize += _strName.size() + sizeof(tU8);

   return u32ReturnSize;
}
