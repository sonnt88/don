/*********************************************************************//**
 * \file       clSDS_Method_NaviSetZoomSetting.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_NaviSetZoomSetting_h
#define clSDS_Method_NaviSetZoomSetting_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"
#include "application/GuiService.h"


class clSDS_Method_NaviSetZoomSetting
   : public clServerMethod
   , public org::bosch::cm::navigation::NavigationService::SetZoomInStepCallbackIF
   , public org::bosch::cm::navigation::NavigationService::SetZoomOutStepCallbackIF
   , public	org::bosch::cm::navigation::NavigationService::ShowMapScreenCarsorLockModeCallbackIF
{
   public:
      virtual ~clSDS_Method_NaviSetZoomSetting();
      clSDS_Method_NaviSetZoomSetting(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > pSds2NaviProxy,
         GuiService& guiService);

      virtual void onSetZoomInStepError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::SetZoomInStepError >& error);
      virtual void onSetZoomInStepResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::SetZoomInStepResponse >& response);

      virtual void onSetZoomOutStepError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::SetZoomOutStepError >& error);
      virtual void onSetZoomOutStepResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::SetZoomOutStepResponse >& response);

      virtual void onShowMapScreenCarsorLockModeError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::ShowMapScreenCarsorLockModeError >& error);

      virtual void onShowMapScreenCarsorLockModeResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::ShowMapScreenCarsorLockModeResponse >& response);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      void vSendZoomRequest(const sds2hmi_sdsfi_tclMsgNaviSetZoomSettingMethodStart& oMessage);
      boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > _sds2NaviProxy;
      GuiService& _guiService;
};


#endif
