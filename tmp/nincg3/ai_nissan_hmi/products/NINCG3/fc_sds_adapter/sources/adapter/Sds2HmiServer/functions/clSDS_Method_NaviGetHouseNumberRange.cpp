/*********************************************************************//**
 * \file       clSDS_Method_NaviGetHouseNumberRange.cpp
 *
 * clSDS_Method_NaviGetHouseNumberRange method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_NaviGetHouseNumberRange.h"
#include "application/clSDS_StringVarList.h"
#include "external/sds2hmi_fi.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_NaviGetHouseNumberRange::~clSDS_Method_NaviGetHouseNumberRange()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_NaviGetHouseNumberRange::clSDS_Method_NaviGetHouseNumberRange(ahl_tclBaseOneThreadService* pService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_NAVIGETHOUSENUMBERRANGE, pService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviGetHouseNumberRange::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   vGetHouseNumberRangeFromNavi();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviGetHouseNumberRange::vGetHouseNumberRangeFromNavi()
{
   //Work around for setting the house number range to SDS
   sds2hmi_sdsfi_tclMsgNaviGetHouseNumberRangeMethodResult oResult;
   bpstl::string oMinHouseNumberRange("1");
   bpstl::string oMaxHouseNumberRange("1000");
   oResult.sFirst.bSet(oMinHouseNumberRange.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   oResult.sLast.bSet(oMaxHouseNumberRange.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   oResult.bResult = TRUE;
   vSendMethodResult(oResult);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviGetHouseNumberRange::vSendNoHouseNumber()
{
   sds2hmi_sdsfi_tclMsgNaviGetHouseNumberRangeMethodResult oResult;
   oResult.bResult = FALSE;
   vSendMethodResult(oResult);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviGetHouseNumberRange::vSendHouseNumberRange()
{
   sds2hmi_sdsfi_tclMsgNaviGetHouseNumberRangeMethodResult oResult;
   bpstl::string oString;
   // dataPool.vGetString(DPNAVI__SDS_HNO_START_RANGE, oString);
   oResult.sFirst.bSet(oString.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   clSDS_StringVarList::vSetVariable("$(HouseNumberRange_Lower)", oString);
   // dataPool.vGetString(DPNAVI__SDS_HNO_END_RANGE, oString);
   oResult.sLast.bSet(oString.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   clSDS_StringVarList::vSetVariable("$(HouseNumberRange_Upper)", oString);
   vAddHouseNumberPatterns(oResult);
   oResult.bResult = TRUE;
   vSendMethodResult(oResult);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviGetHouseNumberRange::vAddHouseNumberPatterns(sds2hmi_sdsfi_tclMsgNaviGetHouseNumberRangeMethodResult& /*oResult*/) const
{
//   clDataPoolList oList(DPNAVI__HNO_PATTERN_LIST);
//   for (tU32 u32Index = 0; u32Index < (tU32)dataPool.u16GetValue(DPNAVI__HNO_PATTERN_LIST_COUNT); u32Index++)
//   {
//      sds2hmi_fi_tclString oTempString;
//      oTempString.bSet((oList.oGetString(u32Index, 0)).c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
//
//      oResult.PatternList.push_back(oTempString);
//   }
}
