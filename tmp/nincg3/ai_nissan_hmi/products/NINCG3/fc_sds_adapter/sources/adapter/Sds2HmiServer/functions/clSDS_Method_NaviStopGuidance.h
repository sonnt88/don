/*********************************************************************//**
 * \file       clSDS_Method_NaviStopGuidance.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_NaviStopGuidance_h
#define clSDS_Method_NaviStopGuidance_h


#include "org/bosch/cm/navigation/NavigationServiceProxy.h"
#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "application/GuiService.h"


using namespace org::bosch::cm::navigation::NavigationService;


class clSDS_Method_NaviStopGuidance
   : public clServerMethod
   , public CancelRouteGuidanceCallbackIF
{
   public:
      virtual ~clSDS_Method_NaviStopGuidance();
      clSDS_Method_NaviStopGuidance(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< NavigationServiceProxy > pSds2NaviProxy,
         GuiService& guiService);

      virtual void onCancelRouteGuidanceError(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< CancelRouteGuidanceError >& error);
      virtual void onCancelRouteGuidanceResponse(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< CancelRouteGuidanceResponse >& response);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      boost::shared_ptr< NavigationServiceProxy > _sds2NaviProxy;
      GuiService& _guiService;
};


#endif
