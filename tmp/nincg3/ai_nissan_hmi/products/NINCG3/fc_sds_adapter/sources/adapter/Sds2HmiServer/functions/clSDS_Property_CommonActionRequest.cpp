/*********************************************************************//**
 * \file       clSDS_Property_CommonActionRequest.cpp
 *
 * Common Action Request property implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Property_CommonActionRequest.h"
#include "application/clSDS_LanguageMediator.h"
#include "external/sds2hmi_fi.h"


#include "SdsAdapter_Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Property_CommonActionRequest.cpp.trc.h"
#endif


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Property_CommonActionRequest::~clSDS_Property_CommonActionRequest()
{
   _pLanguageMediator = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Property_CommonActionRequest::clSDS_Property_CommonActionRequest(ahl_tclBaseOneThreadService* pService, clSDS_LanguageMediator* languageMediator)
   : clServerProperty(SDS2HMI_SDSFI_C_U16_COMMONACTIONREQUEST, pService)
   , _pLanguageMediator(languageMediator)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonActionRequest::vSet(amt_tclServiceData* /*pInMsg*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonActionRequest::vGet(amt_tclServiceData* /*pInMsg*/)
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_NONE;
   vStatus(oMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonActionRequest::vUpreg(amt_tclServiceData* /*pInMsg*/)
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_NONE;
   vStatus(oMessage);
   _pLanguageMediator->setActionRequestAvailable(true);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonActionRequest::vSendPttEvent(tU32 u32Context)
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_PTT_SHORT_PRESS;
   oMessage.Value = u32Context;
   vStatus(oMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonActionRequest::vSendPTTLongPressEvent()
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_CANCEL;
   vStatus(oMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonActionRequest::vSendBackEvent()
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_BACK;
   vStatus(oMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonActionRequest::vAbortDialog()
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_ABORT_DIALOG;
   vStatus(oMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonActionRequest::sendCancelDialog()
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_CANCEL;
   vStatus(oMessage);
}


///**************************************************************************//**
//*
//******************************************************************************/
//tVoid clSDS_Property_CommonActionRequest::vOnLanguageChangeRequest(tU16 u16SpeakerId)
//{
//   switch (u16SpeakerId)
//   {
//      case REQUEST_ALL_SPEAKERS:
//         vRequestAllSpeakers();
//         break;
//
//      default:
//         vSetSpeaker(u16SpeakerId);
//         break;
//   }
//}

tVoid clSDS_Property_CommonActionRequest::vSetSpeaker(tU16 u16SpeakerId)
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_SET_SPEAKER;
   oMessage.Value = tU32(u16SpeakerId);
   vStatus(oMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonActionRequest::vRequestAllSpeakers()
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_GET_ALL_SPEAKERS;
   vStatus(oMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonActionRequest::vClearPrivateData()
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_CLEAR_PRIVATE_DATA;
   vStatus(oMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonActionRequest::vDeleteUserWord(tU32 tU32UWID)
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_DELETE_USERWORD;
   oMessage.Value = tU32UWID;
   vStatus(oMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonActionRequest::vDeleteAllUserWords(tU32 tU32UWProfile)
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_DELETE_USERWORDS;
   oMessage.Value = tU32UWProfile;
   vStatus(oMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonActionRequest::vSetUWProfile(tU32 u32NewSDSUser)
{
   ETG_TRACE_USR4(("clSDS_Property_CommonActionRequest::vSetUWProfile"));
   ETG_TRACE_USR4(("clSDS_Property_CommonActionRequest::vSetUWProfile u32NewSDSUser =%d", u32NewSDSUser));
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_SET_PROFILE;
   oMessage.Value = u32NewSDSUser;
   vStatus(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_CommonActionRequest::sendEnterManualMode()
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_ENTER_MANUALMODE;
   vStatus(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/

void clSDS_Property_CommonActionRequest::sendListSelectRequest(unsigned int value)
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_LIST_SELECT;
   oMessage.Value = value;
   vStatus(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/

void clSDS_Property_CommonActionRequest::sendSelectRequest(unsigned int value)
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_SELECTION;
   oMessage.Value = value;
   vStatus(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonActionRequest::sendFocusMoved(unsigned int value)
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_FOCUS_MOVED;
   oMessage.Value = value;
   vStatus(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonActionRequest::sendNextPageRequest()
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_LIST_DOWN;
   vStatus(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/

void clSDS_Property_CommonActionRequest::sendPreviousPageRequest()
{
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_LIST_UP;
   vStatus(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonActionRequest::recordUserWord(unsigned long userWordListID)
{
   ETG_TRACE_USR4(("clSDS_Property_CommonActionRequest::vRecordUserWord"));
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_RECORD_USERWORD;
   oMessage.Value = userWordListID;
   vStatus(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonActionRequest::replaceUserWord(unsigned long userWordListID)
{
   ETG_TRACE_USR4(("clSDS_Property_CommonActionRequest::replaceUserWord"));
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_REPLACE_USERWORD;
   oMessage.Value = userWordListID;
   vStatus(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonActionRequest::playUserWord(unsigned long userWordListID)
{
   ETG_TRACE_USR4(("clSDS_Property_CommonActionRequest::playUserWord"));
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_PLAY_USERWORD;
   oMessage.Value = userWordListID;
   vStatus(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonActionRequest::sendEnterPauseMode()
{
   ETG_TRACE_USR4(("clSDS_Property_CommonActionRequest::sendEnterPauseMode"));
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_ENTER_PAUSE_MODE;
   vStatus(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonActionRequest::sendExitPauseMode()
{
   ETG_TRACE_USR4(("clSDS_Property_CommonActionRequest::sendExitPauseMode"));
   sds2hmi_sdsfi_tclMsgCommonActionRequestStatus oMessage;
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_Action::FI_EN_EXIT_PAUSE_MODE;
   vStatus(oMessage);
}
