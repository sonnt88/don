/*********************************************************************//**
 * \file       clSDS_ReadTextListObserver.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_ReadTextListObserver_h
#define clSDS_ReadTextListObserver_h

class clSDS_ReadTextListObserver
{
   public:
      clSDS_ReadTextListObserver();
      virtual ~clSDS_ReadTextListObserver();
      virtual void textListChanged(unsigned int size) = 0;
};


#endif /* clSDS_ReadTextListObserver_h */
