/*********************************************************************//**
 * \file       clSDS_XMLStringCreation.h
 *
 * This class holds the xml Buffer created out of provided strings.
 * It acts as an interface with the XML library. It returns a created
 * XML string
 * /remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef CLSDS_XMLSTRINGCREATION_H
#define CLSDS_XMLSTRINGCREATION_H


#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"
#include <map>
#include <string>

typedef std::map<std::string, tU32> ConfigItemMap;
typedef std::map<std::string, ConfigItemMap > GroupMap;


struct SDSConfigData
{
   bool phoneBookNBestMatch;
   bool audioNBestMatch;
   bool beepOnlyMode;
   tU8 tcuType;
};


class clSDS_XMLStringCreation
{
   public:
      virtual ~clSDS_XMLStringCreation();
      clSDS_XMLStringCreation();

      static std::string getStaticXmlString();
      static std::string getDynamicXmlString(const SDSConfigData& cfgData);

   private:
      static void addRootContextCommandFlags(GroupMap& staticGroups);
};


#endif
