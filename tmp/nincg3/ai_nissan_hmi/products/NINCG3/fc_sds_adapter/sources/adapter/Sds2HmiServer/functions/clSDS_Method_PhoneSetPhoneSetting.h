/*********************************************************************//**
 * \file       clSDS_Method_PhoneSetPhoneSetting.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_PhoneSetPhoneSetting_h
#define clSDS_Method_PhoneSetPhoneSetting_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "MOST_BTSet_FIProxy.h"
#include "MOST_Tel_FIProxy.h"
#include "application/GuiService.h"


class clSDS_Property_PhoneStatus;


class clSDS_Method_PhoneSetPhoneSetting
   : public clServerMethod
   , public asf::core::ServiceAvailableIF
   , public MOST_Tel_FI::TransferCallToVehicleCallbackIF
   , public MOST_BTSet_FI::SwitchBluetoothOnOffCallbackIF
   , public MOST_BTSet_FI::BluetoothOnOffCallbackIF
{
   public:
      virtual ~clSDS_Method_PhoneSetPhoneSetting();
      clSDS_Method_PhoneSetPhoneSetting(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy > bluetooshSettingsProxy,
         ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > telephoneProxy,
         clSDS_Property_PhoneStatus* pPhoneStatus,
         GuiService& guiService);

      virtual void onTransferCallToVehicleError(
         const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Tel_FI::TransferCallToVehicleError >& error);

      virtual void onTransferCallToVehicleResult(
         const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Tel_FI::TransferCallToVehicleResult >& result);

      virtual void onSwitchBluetoothOnOffError(
         const ::boost::shared_ptr< MOST_BTSet_FI::MOST_BTSet_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_BTSet_FI::SwitchBluetoothOnOffError >& error);

      virtual void onSwitchBluetoothOnOffResult(
         const ::boost::shared_ptr< MOST_BTSet_FI::MOST_BTSet_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_BTSet_FI::SwitchBluetoothOnOffResult >& result);

      virtual void onBluetoothOnOffError(
         const ::boost::shared_ptr< MOST_BTSet_FI::MOST_BTSet_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_BTSet_FI::BluetoothOnOffError >& error);

      virtual void onBluetoothOnOffStatus(
         const ::boost::shared_ptr< MOST_BTSet_FI::MOST_BTSet_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_BTSet_FI::BluetoothOnOffStatus >& status);

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      void setBluetooth();
      void setHandsfreeMode();
      GuiService& _guiService;
      boost::shared_ptr< MOST_BTSet_FI::MOST_BTSet_FIProxy > _bluetoothSettingsProxy;
      boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy > _telephoneProxy;
      clSDS_Property_PhoneStatus* _pPhoneStatus;
      bool _btOnOffState;
};


#endif
