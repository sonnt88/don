/*********************************************************************//**
 * \file       clSDS_Method_CommonShowDialog.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_CommonShowDialog_h
#define clSDS_Method_CommonShowDialog_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "view_db/Sds_ViewDB.h"


class clSDS_MenuManager;
class clSDS_ListScreen;
class clSDS_Property_PhoneStatus;
class clSDS_ScreenData;
class GuiService;
class SdsPhoneService;


class clSDS_Method_CommonShowDialog : public clServerMethod
{
   public:
      virtual ~clSDS_Method_CommonShowDialog();
      clSDS_Method_CommonShowDialog(
         ahl_tclBaseOneThreadService* pService,
         clSDS_MenuManager* pMenuManager,
         clSDS_ListScreen* pListScreen,
         clSDS_Property_PhoneStatus* pProperty_PhoneStatus,
         GuiService* pGuiService,
         SdsPhoneService* pSdsPhoneService);

      Sds_ViewId GetsdsViewID() const;

   private:
      enum
      {
         SINGLE_PAGE = 1,
         PREVIOUS_PAGE = 2,
         NEXT_PAGE = 4,
         NEXT_PREVIOUS_PAGE = 6
      };

      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      tVoid vUpdatePageNumber(sds2hmi_fi_tcl_e8_PopUp_UpdateType nPopUp_UpDateType);
      tVoid vSendResult(const clSDS_ScreenData& oScreenData);
      sds2hmi_fi_tcl_e8_MainDomains::tenType getPreviousDomain(Sds_ViewId viewId) const;
      sds2hmi_fi_tcl_e8_MainDomains::tenType getNextDomain(Sds_ViewId viewId) const;
      tU8 u8GetNextPreviousOptions() const;
      tVoid vTraceXmlData(const bpstl::string& oXmlData) const;
      tVoid vUpdateSpokenDigitString(const clSDS_ScreenData& oScreenData, Sds_ViewId enViewId);
      tVoid vSetsdsViewID(Sds_ViewId enViewId);
      bool isPhoneAvailable() const;

      clSDS_MenuManager* _pMenuManager;
      GuiService* _pGuiService;
      SdsPhoneService* _pSdsPhoneService;
      clSDS_ListScreen* _pListScreen;
      clSDS_Property_PhoneStatus* _pProperty_PhoneStatus;
      Sds_ViewId _enViewID;
};


#endif
