/*********************************************************************//**
 * \file       clSDS_FuelPriceList.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_FuelPriceList.h"
#include "application/clSDS_StringVarList.h"
#include "view_db/Sds_TextDB.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_FuelPriceList.cpp.trc.h"
#endif


#define NUM_OF_DIGITS_IN_DIRECTION_VALUE 5
#define MAX_NUMBER_OF_FUEL_PRICE_LIST_ENTRIES 25
#define FUEL_TYPE_PETROL 0x00
#define FUEL_TYPE_DIESEL  0x01
#define SXM_LIST_ID  1
#define POI_DIST_MAX_CHAR_LEN  10

#define DIST_CONVERT_KM_100000              100000
#define DIST_CONVERT_KM_1000                1000
#define DIST_CONVERT_M_100                  100
#define DIST_CONVERT_M_10                   10
#define DIST_CONVERT_MI_1609                1609 //1 miles in meters
#define DIST_CONVERT_METERS_TO_YDS          1.0936
#define DIST_CONVERT_METERS_TO_FT           3.2808399
#define DIST_THREE_FOURTH_MILE_IN_YARDS	    1320
#define DIST_THREE_FOURTH_MILE_IN_MTRS      1206.75
#define DIST_HALF_MILE_IN_MTRS              804.672
#define DIST_ONE_FOURTH_MILE_IN_MTRS     	402.336
#define DIST_1000_FT                        1000
#define DIST_900_FT                         900
#define DIST_800_FT                         800
#define DIST_700_FT                         700
#define DIST_600_FT                         600
#define DIST_500_FT                         500
#define DIST_400_FT                         400
#define DIST_300_FT                         300
#define DIST_250_FT                         250
#define DIST_200_FT                         200
#define DIST_150_FT                         150
#define DIST_100_FT                         100
#define DIST_50_FT                          50

#define MIN_LATITUDE						 0.0
#define MAX_LATITUDE 						90.0
#define MIN_LONGITUDE 						-178.0
#define MAX_LONGITUDE 						-30.0
#define DOUBLETOINT							1000000.0


using namespace midw_ext_sxm_fuel_fi_types;
using namespace midw_ext_sxm_fi_types;
using namespace MIDW_EXT_SXM_FUEL_FI;
using namespace MIDW_EXT_SXM_CANADIAN_FUEL_FI;
using namespace midw_ext_sxm_canadian_fuel_fi_types;
using namespace org::bosch::cm::navigation::NavigationService;


/**************************************************************************//**
 *Destructor
 ******************************************************************************/
clSDS_FuelPriceList::~clSDS_FuelPriceList()
{
}


/**************************************************************************//**
 *Constructor
 ******************************************************************************/
clSDS_FuelPriceList::clSDS_FuelPriceList(
   ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FIProxy > sxmFuelProxy,
   ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy > sxmCanadianFuelProxy,
   ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > naviProxy)

   : _sxmFuelProxy(sxmFuelProxy)
   , _sxmCanadianFuelProxy(sxmCanadianFuelProxy)
   , _naviProxy(naviProxy)
   , _fuelListCount(0)
   , _distanceUnits(EM_DIST_KM)
   , _currentCountryCode("USA")
   , _fuelListNumberPOI(0)
{
   _vehicleEngineType = clSDS_KDSConfiguration::getVehicleEngineType();
   _vehicleDistanceUnits = clSDS_KDSConfiguration::getVehicleDistanceUnitsType();
}


/**************************************************************************//**
 *
 ******************************************************************************/
unsigned long clSDS_FuelPriceList::u32GetSize()
{
   return _fuelListCount;
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FuelPriceList::oSetFuelListCount(tU32 u32FuelStationCount)
{
   _fuelListCount = u32FuelStationCount;
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FuelPriceList::oSetUSAFuelInfoList(const midw_ext_sxm_fuel_fi_types::T_FuelInfoList& fuelInfoList)
{
   ::std::vector< T_FuelInfo >::const_iterator lFuelList = fuelInfoList.begin();
   for (unsigned int u32Index = 0; u32Index < u32GetSize() ; u32Index++)
   {
      double dDistance = lFuelList->getFuelDistKm();
      dDistance = dDistance * 1000;
      std::string dDistanceValue = vConvertDistaneIntoString((unsigned long)dDistance);
      std::string dDistanceUnit = oGetDistanceUnit(_distanceUnits);

      int lon = lFuelList->getFuelLoc().getLon();
      int lat = lFuelList->getFuelLoc().getLat();

      stFuelListItems oFuelListItems;
      oFuelListItems.szDistanceUnit = dDistanceUnit;
      oFuelListItems.szDistanceValue = dDistanceValue;
      oFuelListItems.szBrandName = lFuelList->getBrand();
      oFuelListItems.szStationName = lFuelList->getStationName();
      oFuelListItems.szFuelPriceValue = lFuelList->getFuelPrice();
      oFuelListItems.u8fuelAge = lFuelList->getFuelAge();
      oFuelListItems.u8DirectionGUI = oGetDirection(lat, lon);;
      oFuelListItems.numPricesFuelType = lFuelList->getNumPrices();
      oFuelListItems.longitude = vConvertlatlongTodouble(lon);
      oFuelListItems.latitude = vConvertlatlongTodouble(lat);
      oFuelListItems.fuelLocID = lFuelList->getFuel_LOCID();
      _fuelListItems.push_back(oFuelListItems);
      ++lFuelList;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void  clSDS_FuelPriceList::oClearFuelListItems()
{
   _fuelListItems.clear();
}


/***********************************************************************//**
 * Service Unavailable of SXM ,Naiv Proxy's and Deregister  Properties
 ***************************************************************************/
void clSDS_FuelPriceList::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_sxmFuelProxy == proxy)
   {
      _sxmFuelProxy->sendFuelInfoListsStatusRelUpRegAll();
      _sxmFuelProxy->sendFuelBrandNameListRelUpRegAll();
      _sxmFuelProxy->sendFuelTypeListRelUpRegAll();
      _sxmFuelProxy->sendSxmListModeRelUpRegAll();
      _sxmFuelProxy->sendSxmDataServiceStatusRelUpRegAll();
   }
   if (_sxmCanadianFuelProxy == proxy)
   {
      _sxmCanadianFuelProxy->sendCanadianFuelInfoListsStatusRelUpRegAll();
      _sxmCanadianFuelProxy->sendCanadianFuelBrandNameListRelUpRegAll();
      _sxmCanadianFuelProxy->sendCanadianFuelTypeListRelUpRegAll();
      _sxmCanadianFuelProxy->sendSxmDataServiceStatusRelUpRegAll();
      _sxmCanadianFuelProxy->sendSxmListModeRelUpRegAll();
   }
   if (_naviProxy == proxy)
   {
      _naviProxy->sendPositionInformationDeregisterAll();
   }
}


/***********************************************************************//**
 *Service available of SXM,Navi Proxy's and register Properties
 ***************************************************************************/
void clSDS_FuelPriceList::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_sxmFuelProxy == proxy)
   {
      _sxmFuelProxy->sendSxmDataServiceStatusUpReg(*this);
      _sxmFuelProxy->sendFuelInfoListsStatusUpReg(*this);
      _sxmFuelProxy->sendFuelBrandNameListUpReg(*this);
      _sxmFuelProxy->sendFuelTypeListUpReg(*this);
      _sxmFuelProxy->sendSxmListModeUpReg(*this);
   }
   if (_sxmCanadianFuelProxy == proxy)
   {
      _sxmCanadianFuelProxy->sendSxmDataServiceStatusUpReg(*this);
      _sxmCanadianFuelProxy->sendCanadianFuelInfoListsStatusUpReg(*this);
      _sxmCanadianFuelProxy->sendCanadianFuelBrandNameListUpReg(*this);
      _sxmCanadianFuelProxy->sendCanadianFuelTypeListUpReg(*this);
      _sxmCanadianFuelProxy->sendSxmListModeUpReg(*this);
   }
   if (_naviProxy == proxy)
   {
      _naviProxy->sendPositionInformationRegister(*this);
      _naviProxy->sendPositionInformationGet(*this);
   }
}


/***********************************************************************//**
 * Method Start to SXM Fuel service for USA region
 ***************************************************************************/
void clSDS_FuelPriceList::onStartUSAFuelPriceList(midw_ext_sxm_fuel_fi_types::T_e8_SortType sortType)

{
   if (T_e8_SxmDataServiceStatus__SUBSCRIBED == _sxmFuelProxy->getSxmDataServiceStatus().getDataServiceState().getStatus())
   {
      T_SxmListMode lSxmListMode;
      lSxmListMode.setListID(SXM_LIST_ID);
      lSxmListMode.setMode(T_e8_SxmListMode__RELEASE);
      _sxmFuelProxy->sendSxmListModeSet(*this, lSxmListMode);
      T_FuelTypeInfo lFuelTypeInfo;
      switch (_vehicleEngineType)
      {
         case FUEL_TYPE_PETROL:
         {
            lFuelTypeInfo.setFuelName("Regular");
            lFuelTypeInfo.setFuelType(T_e8_FuelTypeEnum__FUEL_TYPE_REGULAR);
         }
         break;
         case FUEL_TYPE_DIESEL:
         {
            lFuelTypeInfo.setFuelName("Diesel");
            lFuelTypeInfo.setFuelType(T_e8_FuelTypeEnum__FUEL_TYPE_DIESEL);
         }
         break;
         default :
         {
            lFuelTypeInfo.setFuelName("Unknown");
            lFuelTypeInfo.setFuelType(T_e8_FuelTypeEnum__FUEL_TYPE_UNKNOWN);
         }
         break;
      }
      _sxmFuelProxy->sendGetFuelInfoListStart(*this,
                                              T_e8_SxmPredefinedListIds__NEAR_BY,
                                              T_e8_SxmListOperation__GET,
                                              "All Brands",
                                              lFuelTypeInfo,
                                              sortType);
   }

   else
   {
      oSetFuelListCount(0);
      oClearFuelListItems();
      notifyListObserver();
   }
}


/***********************************************************************//**
 * *Method Error giving the Fuel Error details for USA Region
 ***************************************************************************/
void clSDS_FuelPriceList::onGetFuelInfoListError(
   const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< GetFuelInfoListError >& /*error*/)
{
   oSetFuelListCount(0);
   oClearFuelListItems();
   notifyListObserver();
}


/***********************************************************************//**
 **Method result giving the Fuel  list for USA Region
 ***************************************************************************/
void clSDS_FuelPriceList::onGetFuelInfoListResult(const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< GetFuelInfoListResult >& result)
{
   oClearFuelListItems();
   if ((result->getFuelInfoList().size() > 0) && (T_e8_SxmPredefinedListIds__NEAR_BY == result->getListID()))
   {
      unsigned long u32SDSFuelListCount = result->getFuelInfoList().size();
      unsigned long u32MaxCount =::std::min(u32SDSFuelListCount, (tU32)MAX_NUMBER_OF_FUEL_PRICE_LIST_ENTRIES);
      oSetFuelListCount(u32MaxCount);
      oSetUSAFuelInfoList(result->getFuelInfoList());
   }
   else
   {
      oSetFuelListCount(0);
   }
   notifyListObserver();
}


/***********************************************************************//**
 * Callback SXM List mode Error from  SXM Middleware
 ***************************************************************************/
void clSDS_FuelPriceList::onSxmListModeError(
   const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr<SxmListModeError >&  /*error*/)
{
}


/***********************************************************************//**
 * Callback SXM List mode Status from  SXM Middleware
 ***************************************************************************/
void clSDS_FuelPriceList::onSxmListModeStatus(
   const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::SxmListModeStatus >& /*status*/)
{
}


/***********************************************************************//**
 * Callback Fuel InfoList  Error from  SXM Middleware
 ***************************************************************************/

void clSDS_FuelPriceList:: onFuelInfoListsStatusError(
   const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FIProxy >&/*proxy*/,
   const ::boost::shared_ptr< FuelInfoListsStatusError >&  /*error*/)
{
}


/***********************************************************************//**
 * Callback Fuel InfoList Status  from  SXM Middleware
 ***************************************************************************/
void clSDS_FuelPriceList::onFuelInfoListsStatusStatus(
   const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< FuelInfoListsStatusStatus >& /*status*/)
{
}


/***********************************************************************//**
 * Method FuelBrand NameList Error from  SXM Middleware
 ***************************************************************************/
void clSDS_FuelPriceList::onFuelBrandNameListError(
   const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FIProxy >&  /*proxy*/,
   const ::boost::shared_ptr< FuelBrandNameListError >& /*error*/)
{
}


/***********************************************************************//**
 * Callback FuelBrand NameList Status from  SXM Middleware
 ***************************************************************************/
void clSDS_FuelPriceList::onFuelBrandNameListStatus(
   const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FIProxy >&/*proxy*/,
   const ::boost::shared_ptr< FuelBrandNameListStatus >& /*status*/)
{
}


/***********************************************************************//**
 * Callback FuelTypeListError from  SXM Middleware
 ***************************************************************************/
void clSDS_FuelPriceList::onFuelTypeListError(
   const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< FuelTypeListError >& /*error*/)
{
}


/***********************************************************************//**
 * Callback FuelTypeList Status from  SXM Middleware
 ***************************************************************************/
void clSDS_FuelPriceList::onFuelTypeListStatus(
   const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FIProxy >&  /*proxy*/,
   const ::boost::shared_ptr< FuelTypeListStatus >& /*status*/)
{
}


/***********************************************************************//**
 * Callback SxmDataServiceStatus Error from SXM Middleware.
 ***************************************************************************/

void clSDS_FuelPriceList::onSxmDataServiceStatusError(
   const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr<SxmDataServiceStatusError >& /*error*/)
{
}


/***********************************************************************//**
 * Callback SxmDataServiceStatus  from SXM Middleware.
 ***************************************************************************/
void clSDS_FuelPriceList:: onSxmDataServiceStatusStatus(
   const ::boost::shared_ptr<MIDW_EXT_SXM_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::SxmDataServiceStatusStatus >& /*status*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_FuelPriceList::bSelectElement(tU32 u32SelectedIndex)
{
   if (u32SelectedIndex > 0)
   {
      _fuelListNumberPOI = u32SelectedIndex - 1;
      onStartStationInfoByRegion(_fuelListNumberPOI);
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FuelPriceList::vUpdateHeadLineTagForFuelPrice() const
{
   clSDS_StringVarList::vSetVariable("$(POIName)", _fuelStationInfo.szStationNameBrandName);
   clSDS_StringVarList::vSetVariable("$(City)", _fuelStationInfo.szCityName);
   clSDS_StringVarList::vSetVariable("$(State)", _fuelStationInfo.szStateName);
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::vector<clSDS_ListItems> clSDS_FuelPriceList::oGetItems(tU32 u32StartIndex, tU32 u32EndIndex)
{
   std::vector<clSDS_ListItems> oListItems;

   for (unsigned long u32Index = u32StartIndex; u32Index < ::std::min(u32EndIndex, u32GetSize()); u32Index++)
   {
      oListItems.push_back(oGetListItem(u32Index));
   }
   return oListItems;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_FuelPriceList::oGetFuelPriceValue(tU32 u32Index) const
{
   return _fuelListItems[u32Index].szFuelPriceValue;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_FuelPriceList::oGetFuelAge(tU8 u8FuelAge) const
{
   std::string oFuelAgeStr;

   switch (u8FuelAge)
   {
      case TODAY:
         oFuelAgeStr = Sds_TextDB_vGetText(SDS_TEXT_TODAY);
         break;

      case YESTERDAY:
         oFuelAgeStr = Sds_TextDB_vGetText(SDS_TEXT_ONE_DAY);
         break;

      case TWO_DAY:
         oFuelAgeStr = Sds_TextDB_vGetText(SDS_TEXT_TWO_DAYS);
         break;

      default:
         oFuelAgeStr = Sds_TextDB_vGetText(SDS_TEXT_THREE_DAYS_PLUS);
         break;
   }
   return oFuelAgeStr;
}


/**************************************************************************//**
 *
 ******************************************************************************/
clSDS_ListItems::tenColorofText clSDS_FuelPriceList::enGetFuelAgeColor(tU8 u8FuelAge) const
{
   switch (u8FuelAge)
   {
      case TODAY:
         return clSDS_ListItems::PRICE_AGE_CURRENT;

      case YESTERDAY:
         return clSDS_ListItems::PRICE_AGE_1_DAY;

      case TWO_DAY:
         return clSDS_ListItems::PRICE_AGE_2_DAYS;

      default:
         return clSDS_ListItems::PRICE_AGE_3_DAYS_PLUS;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
clSDS_ListItems clSDS_FuelPriceList::oGetListItem(tU32 u32Index) const
{
   clSDS_ListItems oListItem;
   oListItem.oPrice.szString = oGetFuelPriceValue(u32Index);
   oListItem.oLastPriceUpdate.szString = oGetLastPriceUpdate(u32Index);
   oListItem.oLastPriceUpdate.enTextColor = enGetLastPriceTextColor(u32Index);
   oListItem.oDescription.szString = oGetBrandOrFuelStationName(u32Index);
   oListItem.oDistance.szString = oGetDistance(u32Index);

   tChar szTempString[NUM_OF_DIGITS_IN_DIRECTION_VALUE] = "";
   OSALUTIL_s32SaveNPrintFormat(szTempString, sizeof(szTempString), "%d", oGetDirectionGUI(u32Index));
   oListItem.oDirectionSymbol.szString = szTempString;
   return oListItem;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_FuelPriceList::oGetBrandOrFuelStationName(tU32 u32Index) const
{
   const std::string oBrandStr = _fuelListItems[u32Index].szBrandName;
   const std::string oStationNameStr = _fuelListItems[u32Index].szStationName;

   if (oBrandStr.empty())
   {
      return oStationNameStr;
   }
   return oBrandStr;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_FuelPriceList::oGetDistance(tU32 u32Index) const
{
   std::string oDistance = _fuelListItems[u32Index].szDistanceValue;
   std::string oUnit = _fuelListItems[u32Index].szDistanceUnit;
   std::string oDistanceWithUnit = oDistance + " " + oUnit;
   return oDistanceWithUnit;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_FuelPriceList::oGetLastPriceUpdate(tU32 u32Index) const
{
   if (_fuelListItems[u32Index].numPricesFuelType != 0)
   {
      return oGetFuelAge(_fuelListItems[u32Index].u8fuelAge);
   }
   return "-";
}


/**************************************************************************//**
 *
 ******************************************************************************/
clSDS_ListItems::tenColorofText clSDS_FuelPriceList::enGetLastPriceTextColor(tU32 u32Index) const
{
   if (_fuelListItems[u32Index].numPricesFuelType != 0)
   {
      return enGetFuelAgeColor(_fuelListItems[u32Index].u8fuelAge);
   }
   return clSDS_ListItems::NORMAL;
}


/**************************************************************************//**
 *
 ******************************************************************************/
unsigned char clSDS_FuelPriceList::oGetDirectionGUI(tU32 u32Index)const
{
   return _fuelListItems[u32Index].u8DirectionGUI;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_FuelPriceList::vConvertDistaneIntoString(unsigned long u32DistToPOIMts)
{
   unsigned long u32DistInMis = (u32DistToPOIMts / DIST_CONVERT_MI_1609) ;
   unsigned long u32DistInYards = (unsigned long)(u32DistToPOIMts * DIST_CONVERT_METERS_TO_YDS);
   unsigned long u32DistInFT = (unsigned long)(u32DistToPOIMts * DIST_CONVERT_METERS_TO_FT);
   char autf8DistToPOI[POI_DIST_MAX_CHAR_LEN] = {'\0'};
   std::string dDistanceValue = "";
   if (_vehicleDistanceUnits == clSDS_KDSConfiguration::EN_DIST_UK_MILES_AND_KM)
   {
      if (u32DistInYards <  DIST_THREE_FOURTH_MILE_IN_YARDS)
      {
         sprintf(autf8DistToPOI, "%lu", u32DistInYards);
         _distanceUnits = EM_DIST_YARDS;
      }
      else
      {
         sprintf(autf8DistToPOI, "%lu.%1lu", u32DistInMis,
                 ((u32DistToPOIMts * DIST_CONVERT_M_10) / DIST_CONVERT_MI_1609) % DIST_CONVERT_M_10);
         _distanceUnits = EM_DIST_MILES;
      }
   }
   else if (_vehicleDistanceUnits == clSDS_KDSConfiguration::EN_DIST_US_MILES_AND_KM)
   {
      //Below 1mile, 3/4 mi, 1/2 mi, 1/4 mi, next steps in "ft"
      if ((u32DistToPOIMts < DIST_CONVERT_MI_1609) && (u32DistToPOIMts >= DIST_THREE_FOURTH_MILE_IN_MTRS))
      {
         char autf8DistToPOI1[POI_DIST_MAX_CHAR_LEN] = "3/4";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI1);
         _distanceUnits = EM_DIST_MILES;
      }
      else if ((u32DistToPOIMts < DIST_THREE_FOURTH_MILE_IN_MTRS) && (u32DistToPOIMts >= DIST_HALF_MILE_IN_MTRS))
      {
         char autf8DistToPOI2[POI_DIST_MAX_CHAR_LEN] = "1/2";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI2);
         _distanceUnits = EM_DIST_MILES;
      }
      else if ((u32DistToPOIMts < DIST_HALF_MILE_IN_MTRS) && (u32DistToPOIMts >= DIST_ONE_FOURTH_MILE_IN_MTRS))
      {
         char autf8DistToPOI3[POI_DIST_MAX_CHAR_LEN] = "1/4";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI3);
         _distanceUnits = EM_DIST_MILES;
      }
      else if ((u32DistToPOIMts <  DIST_ONE_FOURTH_MILE_IN_MTRS) && (u32DistInFT >= DIST_1000_FT))
      {
         char autf8DistToPOI3[POI_DIST_MAX_CHAR_LEN] = "1000";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI3);
         _distanceUnits = EM_DIST_FEET;
      }
      else if ((u32DistInFT <  DIST_1000_FT) && (u32DistInFT >= DIST_900_FT))
      {
         char autf8DistToPOI3[POI_DIST_MAX_CHAR_LEN] = "900";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI3);
         _distanceUnits = EM_DIST_FEET;
      }
      else if ((u32DistInFT <  DIST_900_FT) && (u32DistInFT >= DIST_800_FT))
      {
         char autf8DistToPOI3[POI_DIST_MAX_CHAR_LEN] = "800";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI3);
         _distanceUnits = EM_DIST_FEET;
      }
      else if ((u32DistInFT <  DIST_800_FT) && (u32DistInFT >= DIST_700_FT))
      {
         char autf8DistToPOI3[POI_DIST_MAX_CHAR_LEN] = "700";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI3);
         _distanceUnits = EM_DIST_FEET;
      }
      else if ((u32DistInFT <  DIST_700_FT) && (u32DistInFT >= DIST_600_FT))
      {
         char autf8DistToPOI3[POI_DIST_MAX_CHAR_LEN] = "600";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI3);
         _distanceUnits = EM_DIST_FEET;
      }
      else if ((u32DistInFT <  DIST_600_FT) && (u32DistInFT >= DIST_500_FT))
      {
         char autf8DistToPOI3[POI_DIST_MAX_CHAR_LEN] = "500";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI3);
         _distanceUnits = EM_DIST_FEET;
      }
      else if ((u32DistInFT <  DIST_500_FT) && (u32DistInFT >= DIST_400_FT))
      {
         char autf8DistToPOI3[POI_DIST_MAX_CHAR_LEN] = "400";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI3);
         _distanceUnits = EM_DIST_FEET;
      }
      else if ((u32DistInFT <  DIST_400_FT) && (u32DistInFT >= DIST_300_FT))
      {
         char autf8DistToPOI3[POI_DIST_MAX_CHAR_LEN] = "300";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI3);
         _distanceUnits = EM_DIST_FEET;
      }
      else if ((u32DistInFT <  DIST_300_FT) && (u32DistInFT >= DIST_250_FT))
      {
         char autf8DistToPOI3[POI_DIST_MAX_CHAR_LEN] = "250";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI3);
         _distanceUnits = EM_DIST_FEET;
      }
      else if ((u32DistInFT <  DIST_250_FT) && (u32DistInFT >= DIST_200_FT))
      {
         char autf8DistToPOI3[POI_DIST_MAX_CHAR_LEN] = "200";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI3);
         _distanceUnits = EM_DIST_FEET;
      }
      else if ((u32DistInFT <  DIST_200_FT) && (u32DistInFT >= DIST_150_FT))
      {
         char autf8DistToPOI3[POI_DIST_MAX_CHAR_LEN] = "150";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI3);
         _distanceUnits = EM_DIST_FEET;
      }
      else if ((u32DistInFT <  DIST_150_FT) && (u32DistInFT >= DIST_100_FT))
      {
         char autf8DistToPOI3[POI_DIST_MAX_CHAR_LEN] = "100";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI3);
         _distanceUnits = EM_DIST_FEET;
      }
      else if ((u32DistInFT <  DIST_100_FT) && (u32DistInFT >= DIST_50_FT))
      {
         char autf8DistToPOI3[POI_DIST_MAX_CHAR_LEN] = "50";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI3);
         _distanceUnits = EM_DIST_FEET;
      }
      else if (u32DistInFT < DIST_50_FT)
      {
         char autf8DistToPOI3[POI_DIST_MAX_CHAR_LEN] = "0";
         sprintf(autf8DistToPOI, "%s", autf8DistToPOI3);
         _distanceUnits = EM_DIST_FEET;
      }
      else
      {
         sprintf(autf8DistToPOI, "%lu.%1lu", u32DistInMis,
                 ((u32DistToPOIMts * DIST_CONVERT_M_10) /  DIST_CONVERT_MI_1609) % DIST_CONVERT_M_10);
         _distanceUnits = EM_DIST_MILES;
      }
   }
   else
   {
      //Distance in KM.
      if (u32DistInMis >= DIST_CONVERT_M_100)
      {
         sprintf(autf8DistToPOI, "%lu", u32DistInMis);
         _distanceUnits = EM_DIST_MILES;
      }
      else if (u32DistToPOIMts >= DIST_CONVERT_KM_100000)
      {
         sprintf(autf8DistToPOI, "%lu", u32DistToPOIMts / DIST_CONVERT_KM_1000);
         _distanceUnits = EM_DIST_KM;
      }
      else
      {
         sprintf(autf8DistToPOI, "%lu.%1lu", u32DistToPOIMts / DIST_CONVERT_KM_1000,
                 (u32DistToPOIMts % DIST_CONVERT_KM_1000) / DIST_CONVERT_M_100);
         _distanceUnits = EM_DIST_KM;
      }
   }
   dDistanceValue = autf8DistToPOI;
   return dDistanceValue;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_FuelPriceList::oGetDistanceUnit(enDistanceUnits u8DistanceUnit) const
{
   std::string mDistanceUnit = "";
   switch (u8DistanceUnit)
   {
      case EM_DIST_YARDS:
         mDistanceUnit = "yds";
         break;
      case EM_DIST_FEET:
         mDistanceUnit = "ft";
         break;
      case EM_DIST_MILES:
         mDistanceUnit = "mi";
         break;
      default:
         mDistanceUnit = "km";
         break;
   }
   return mDistanceUnit;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_FuelPriceList::vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType)
{
   switch (listType)
   {
      case sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_FUEL_PRICES:
         onFuelSortByPriceRegion();
         break;
      case sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_FUEL_DISTANCES:
         onFuelSortByDistanceRegion();
         break;

      default:
         oSetFuelListCount(0);
         oClearFuelListItems();
         notifyListObserver();
         break;
   }
}


/***********************************************************************//**
 * Callback FuelInfoList Error from SXM Middleware for Canada region
 ***************************************************************************/
void clSDS_FuelPriceList::onCanadianFuelInfoListsStatusError(
   const ::boost::shared_ptr<MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >&/*proxy*/,
   const ::boost::shared_ptr<CanadianFuelInfoListsStatusError >& /*error*/)
{
}


/***********************************************************************//**
 * Callback FuelInfoList status from SXM Middleware for Canada region
 ***************************************************************************/
void clSDS_FuelPriceList::onCanadianFuelInfoListsStatusStatus(
   const ::boost::shared_ptr<MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >&/* proxy*/,
   const ::boost::shared_ptr<CanadianFuelInfoListsStatusStatus >& /*status*/)
{
}


/***********************************************************************//**
 * Callback FuelBrandNameList Error from SXM Middleware for Canada region
 ***************************************************************************/
void clSDS_FuelPriceList::onCanadianFuelBrandNameListError(
   const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr<CanadianFuelBrandNameListError >& /*error*/)
{
}


/***********************************************************************//**
 * Callback FuelBrandList from SXM Middleware for Canada region
 ***************************************************************************/
void clSDS_FuelPriceList::onCanadianFuelBrandNameListStatus(
   const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< CanadianFuelBrandNameListStatus >& /*status*/)
{
}


/***********************************************************************//**
 * Callback FuelTypeList Error SXM Middleware for Canada region
 ***************************************************************************/
void clSDS_FuelPriceList::onCanadianFuelTypeListError(
   const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< CanadianFuelTypeListError >& /*error*/)
{
}


/***********************************************************************//**
 * Callback FuelTypeList status from SXM Middleware for Canada region
 ***************************************************************************/
void clSDS_FuelPriceList::onCanadianFuelTypeListStatus(
   const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< CanadianFuelTypeListStatus >& /*status*/)
{
}


/***********************************************************************//**
 * Callback Sxm List mode error  from SXM Middleware for Canada region
 ***************************************************************************/
void clSDS_FuelPriceList::onSxmListModeError(
   const ::boost::shared_ptr<MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >&/* proxy*/,
   const ::boost::shared_ptr< SxmListModeError >& /*error*/)
{
}


/***********************************************************************//**
 * Callback Sxm List mode status  from SXM Middleware for Canada region
 ***************************************************************************/
void clSDS_FuelPriceList:: onSxmListModeStatus(
   const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::SxmListModeStatus >& /*status*/)
{
}


/***********************************************************************//**
 * Callback SxmDataServiceStatus Error from SXM Middleware for Canada region
 ***************************************************************************/
void clSDS_FuelPriceList:: onSxmDataServiceStatusError(
   const ::boost::shared_ptr<MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr<SxmDataServiceStatusError >& /*error*/)
{
}


/***********************************************************************//**
 * Callback SxmDataServiceStatus  from SXM Middleware for Canada region
 ***************************************************************************/
void clSDS_FuelPriceList:: onSxmDataServiceStatusStatus(
   const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::SxmDataServiceStatusStatus >& /*status*/)
{
}


/***********************************************************************//**
  Method Start to SXM Fuel service for Canada region
 ***************************************************************************/
void clSDS_FuelPriceList::onStartCanadianFuelPriceList(T_e8_CanFuelSortType sortType)
{
   if (T_e8_SxmDataServiceStatus__SUBSCRIBED == _sxmCanadianFuelProxy->getSxmDataServiceStatus().getDataServiceState().getStatus())
   {
      T_SxmListMode lSxmListMode;
      lSxmListMode.setListID(SXM_LIST_ID);
      lSxmListMode.setMode(T_e8_SxmListMode__RELEASE);
      _sxmCanadianFuelProxy->sendSxmListModeSet(*this, lSxmListMode);
      T_CanFuelTypeInfo canadianFuelType;
      switch (_vehicleEngineType)
      {
         case FUEL_TYPE_PETROL:
         {
            canadianFuelType.setCanFuelName("Regular");
            canadianFuelType.setCanFuelType(T_e8_CanFuelTypeEnum__CAN_FUEL_TYPE_REGULAR);
         }
         break;
         case FUEL_TYPE_DIESEL:
         {
            canadianFuelType.setCanFuelName("Diesel");
            canadianFuelType.setCanFuelType(T_e8_CanFuelTypeEnum__CAN_FUEL_TYPE_DIESEL);
         }
         break;
         default :
         {
            canadianFuelType.setCanFuelName("Unknown");
            canadianFuelType.setCanFuelType(T_e8_CanFuelTypeEnum__CAN_FUEL_TYPE_UNKNOWN);
         }
         break;
      }
      _sxmCanadianFuelProxy->sendGetCanadianFuelInfoListStart(*this,
            T_e8_SxmPredefinedListIds__NEAR_BY,
            T_e8_SxmListOperation__GET,
            "All Brands",
            canadianFuelType,
            sortType);
   }
   else
   {
      oSetFuelListCount(0);
      oClearFuelListItems();
      notifyListObserver();
   }
}


/***********************************************************************//**
 *   * Method error  SXM Fuel service for Canada region
 ***************************************************************************/
void clSDS_FuelPriceList::onGetCanadianFuelInfoListError(
   const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< GetCanadianFuelInfoListError >&/* error*/)
{
   oSetFuelListCount(0);
   oClearFuelListItems();
   notifyListObserver();
}


/***********************************************************************//**
 * Method result for Sxm service Canada region
 ***************************************************************************/

void clSDS_FuelPriceList::onGetCanadianFuelInfoListResult(
   const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr<GetCanadianFuelInfoListResult >& result)
{
   oClearFuelListItems();
   if ((result->getCanadianFuelInfoList().size() > 0) && (T_e8_SxmPredefinedListIds__NEAR_BY == result->getListID()))
   {
      unsigned long u32SDSFuelListCount = result->getCanadianFuelInfoList().size();
      unsigned long u32MaxCount =::std::min(u32SDSFuelListCount, (tU32)MAX_NUMBER_OF_FUEL_PRICE_LIST_ENTRIES);
      oSetFuelListCount(u32MaxCount);
      oSetCanadianFuelInfoList(result->getCanadianFuelInfoList());
   }
   else
   {
      oSetFuelListCount(0);
   }
   notifyListObserver();
}


/***********************************************************************//**
 * Fuel info list details for Canada region
 ***************************************************************************/
void  clSDS_FuelPriceList::oSetCanadianFuelInfoList(const T_CanFuelInfoList& canadianFuelInfoList)
{
   std::vector< T_CanFuelInfo >::const_iterator lCandianFuelList = canadianFuelInfoList.begin();
   for (unsigned int u32Index = 0; u32Index < u32GetSize() ; u32Index++)
   {
      double dDistance = lCandianFuelList->getCanFuelDistKm();
      dDistance = dDistance * 1000;
      std::string dDistanceValue = vConvertDistaneIntoString((unsigned long)dDistance);
      std::string dDistanceUnit = oGetDistanceUnit(_distanceUnits);

      int latitude = lCandianFuelList->getCanFuelLoc().getLat();
      int longitude = lCandianFuelList->getCanFuelLoc().getLon();

      stFuelListItems oFuelListItems;
      oFuelListItems.szDistanceUnit = dDistanceUnit;
      oFuelListItems.szDistanceValue = dDistanceValue;
      oFuelListItems.szBrandName = lCandianFuelList->getBrand();
      oFuelListItems.szStationName = lCandianFuelList->getStationName();
      oFuelListItems.szFuelPriceValue = lCandianFuelList->getCanFuelPrice();
      oFuelListItems.u8fuelAge = lCandianFuelList->getCanFuelAge();
      oFuelListItems.u8DirectionGUI = oGetDirection(latitude, longitude);
      oFuelListItems.numPricesFuelType = lCandianFuelList->getNumPrices();
      oFuelListItems.longitude = vConvertlatlongTodouble(longitude);
      oFuelListItems.latitude = vConvertlatlongTodouble(latitude);
      oFuelListItems.fuelLocID = lCandianFuelList->getCanFuel_LOCID();
      _fuelListItems.push_back(oFuelListItems);
      ++lCandianFuelList;
   }
}


/***********************************************************************//**
 *  sort by price based on Current Country
 ***************************************************************************/
void clSDS_FuelPriceList::onFuelSortByPriceRegion()
{
   if (_currentCountryCode.compare("USA") == 0)
   {
      onStartUSAFuelPriceList(T_e8_SortType__FUEL_SORT_BY_PRICE);
   }
   else if (_currentCountryCode.compare("CAN") == 0)
   {
      onStartCanadianFuelPriceList(T_e8_CanFuelSortType__CAN_FUEL_SORT_BY_PRICE);
   }
   else
   {
      oSetFuelListCount(0);
      oClearFuelListItems();
      notifyListObserver();
   }
}


/***********************************************************************//**
 *  sort by distance based on current country
 ***************************************************************************/
void clSDS_FuelPriceList::onFuelSortByDistanceRegion()
{
   if (_currentCountryCode.compare("USA") == 0)
   {
      onStartUSAFuelPriceList(T_e8_SortType__FUEL_SORT_BY_DISTANCE);
   }
   else if (_currentCountryCode.compare("CAN") == 0)
   {
      onStartCanadianFuelPriceList(T_e8_CanFuelSortType__CAN_FUEL_SORT_BY_DISTANCE);
   }
   else
   {
      oSetFuelListCount(0);
      oClearFuelListItems();
      notifyListObserver();
   }
}


/***********************************************************************//**
 *  Position Information error callback form  Navi
 ***************************************************************************/
void clSDS_FuelPriceList::onPositionInformationError(
   const ::boost::shared_ptr< NavigationServiceProxy >&  /*proxy*/,
   const ::boost::shared_ptr< PositionInformationError >& /*error*/)
{
}


/***********************************************************************//**
 *  Position Information like Current Country code ,lat & log form Navi.
 ***************************************************************************/
void clSDS_FuelPriceList::onPositionInformationUpdate(const ::boost::shared_ptr< NavigationServiceProxy >&  /*proxy*/,
      const ::boost::shared_ptr< PositionInformationUpdate >& update)
{
   _currentCountryCode = update->getPositionInformation().getCountryCode();
   ETG_TRACE_USR4((" Update country code %s:", update->getPositionInformation().getCountryCode().c_str()));
}


/***********************************************************************//**
 *   start route guidance to  Navi.
 ***************************************************************************/
void clSDS_FuelPriceList::vStartGuidance()
{
   PositionWGS84 location;
   location.setLongitude(_fuelListItems[_fuelListNumberPOI].longitude);
   location.setLatitude(_fuelListItems[_fuelListNumberPOI].latitude);
   _naviProxy->sendStartGuidanceToPosWGS84Request(*this, location);
}


/***********************************************************************//**
 *   route guidance error callback form Navi.
 ***************************************************************************/
void clSDS_FuelPriceList::onStartGuidanceToPosWGS84Error(
   const ::boost::shared_ptr<NavigationServiceProxy>& /*proxy*/,
   const ::boost::shared_ptr<StartGuidanceToPosWGS84Error >& /*error*/)
{
}


/***********************************************************************//**
 *  route guidance result callback form Navi.
 ***************************************************************************/
void clSDS_FuelPriceList::onStartGuidanceToPosWGS84Response(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< StartGuidanceToPosWGS84Response >& /*response*/)
{
}


/***********************************************************************//**
 *  Add as way point to current route guidance to  Navi.
 ***************************************************************************/
void clSDS_FuelPriceList::vAddasWayPoint()
{
   PositionWGS84 location;
   location.setLongitude(_fuelListItems[_fuelListNumberPOI].longitude);
   location.setLatitude(_fuelListItems[_fuelListNumberPOI].latitude);
   _naviProxy->sendSetLocationWithCoordinatesRequest(*this, location);
}


/***********************************************************************//**
 *  Add as way point to current route guidance to  Navi.
 ***************************************************************************/
void clSDS_FuelPriceList::onSetLocationWithCoordinatesError(
   const ::boost::shared_ptr<NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< SetLocationWithCoordinatesError >& /*error*/)
{
}


/***********************************************************************//**
 *  Add as way point to current route guidance to  Navi.
 ***************************************************************************/
void clSDS_FuelPriceList::onSetLocationWithCoordinatesResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< SetLocationWithCoordinatesResponse >&/*response*/)
{
}


/***********************************************************************//**
 *  function to get Direction Heading.
 ***************************************************************************/
enHeadingType clSDS_FuelPriceList::oGetDirection(int dLatitude,  int dLongitude)const
{
   double targetlat = vConvertlatlongTodouble(dLatitude);
   double targetlon = vConvertlatlongTodouble(dLongitude);
   double currLat = _naviProxy->getPositionInformation().getPositionWGS84().getLatitude();
   double currLon = _naviProxy->getPositionInformation().getPositionWGS84().getLongitude();

   if ((false == bCheckLocationBoundary(targetlat, targetlon))
         || (false == bCheckLocationBoundary(currLat, currLon)))
   {
      return EN_HEADING_INDEX_UNKNOWN;
   }

   //Get car direction from two set of coordinates.
   double cardirection = vCalculateAirDistanceWithDirection(targetlat, targetlon, currLat, currLon);

   /** adjusts direction to current car heading */
   double dHeading = carRelativeDirection(
                        static_cast<double>(_naviProxy->getPositionInformation().getVehicleHeading()), cardirection);
   enHeadingType eType = static_cast<enHeadingType>(formatDirectionInfo(dHeading));
   return eType;
}


/***********************************************************************//**
 *  Function to check the NAR boundary's
 ***************************************************************************/
bool clSDS_FuelPriceList::bCheckLocationBoundary(double lat, double lon)const
{
   bool bIsWithinLimit = true;
   if (lat < MIN_LATITUDE || lat > MAX_LATITUDE || lon < MIN_LONGITUDE || lon > MAX_LONGITUDE)
   {
      bIsWithinLimit = false;
   }
   return bIsWithinLimit;
}


/***********************************************************************//**
 *  function to get latitude or longitude in double format.
 ***************************************************************************/
double clSDS_FuelPriceList::vConvertlatlongTodouble(signed int latlong)const
{
   double doLatLong = static_cast <double>(latlong);
   return (doLatLong / DOUBLETOINT);
}


/*******************************************************************************//**
 *  function to calculate direction using two set of coordinates(latitude,longitude).
 ***********************************************************************************/
double clSDS_FuelPriceList::vCalculateAirDistanceWithDirection(double dDestinationLatitude,
      double dDestinationLongitude,
      double dCurrentLatitude,
      double dCurrentLongitude)const
{
   double deltaLatitude  = dDestinationLatitude - dCurrentLatitude;
   double deltaLongitude = dDestinationLongitude - dCurrentLongitude;
   deltaLongitude *= ::cos(vDegrees2radians((dDestinationLatitude + dCurrentLatitude) / 2.0));

   // range of direction in degrees: -180 to + 180
   double carDirection = vRadians2degrees(::atan2(deltaLongitude, deltaLatitude));

   // convert direction into North-relative angle:
   carDirection = ::fmod((carDirection + 360.0), 360.0);

   return carDirection;
}


/***********************************************************************//**
 *  function to convert radians2degrees.
 ***************************************************************************/
double clSDS_FuelPriceList::vRadians2degrees(double angle_degrees)const
{
   return (180 / M_PI) * angle_degrees;
}


/***********************************************************************//**
 *  function to convert degree2radians.
 ***************************************************************************/
double clSDS_FuelPriceList::vDegrees2radians(double angle_degrees)const
{
   return (M_PI / 180) * angle_degrees;
}


/***********************************************************************//**
adjusts direction to current car heading
 ***************************************************************************/
double clSDS_FuelPriceList::carRelativeDirection(double currentDirection, double carDirection)const
{
   return makeCircular(carDirection - currentDirection);
}


/***********************************************************************//**
 *  8 Directions calculations like  enHeadingType.
 *  8 directions: 360 / 8 = 45° for each direction sector
 *  N = -22.5 to 22.5, NE = 22.5 to 67.5, .....
 *  East (90°), West(270°) 	North  (0°)  S (180°)
 ***************************************************************************/
int clSDS_FuelPriceList::formatDirectionInfo(double dActualAngle)const
{
   int heading = -1;
   unsigned int uHeadingArr[8] = {0, 1, 2, 3, 4, 5, 6, 7};
   if (dActualAngle >= 0.0 && dActualAngle <= 360.0)
   {
      dActualAngle = round(dActualAngle);
      unsigned int val = static_cast <unsigned int>(round(dActualAngle / 45));
      heading = uHeadingArr[(val % 8)];
   }
   return heading;
}


/***********************************************************************//**
  function to limits direction to 0-360.
***************************************************************************/
double clSDS_FuelPriceList::makeCircular(double degree)const
{
   double fmod_degree = ::fmod(degree, 360.0);
   return ::fabs(fmod_degree >= 0.0 ? fmod_degree : fmod_degree + 360.0);
}


/***********************************************************************//**

***************************************************************************/
unsigned long clSDS_FuelPriceList::oGetSelectedIndex()const
{
   return _fuelListNumberPOI;
}


/***********************************************************************//**

***************************************************************************/
void clSDS_FuelPriceList::onStartStationInfoByRegion(unsigned long selectedIndex)
{
   unsigned int fuelLOCID  = _fuelListItems[selectedIndex].fuelLocID;
   if (_currentCountryCode.compare("USA") == 0)
   {
      _sxmFuelProxy->sendGetFuelStationInfoStart(*this, fuelLOCID, T_e8_SxmPredefinedListIds__NEAR_BY);
   }
   else if (_currentCountryCode.compare("CAN") == 0)
   {
      _sxmCanadianFuelProxy->sendGetCanadianFuelStationInfoStart(*this, fuelLOCID, T_e8_SxmPredefinedListIds__NEAR_BY);
   }
   else
   {
      ETG_TRACE_ERR(("Unknown Country code :%s", _currentCountryCode.c_str()));
   }
}


/***********************************************************************//**

***************************************************************************/
void clSDS_FuelPriceList::onGetFuelStationInfoError(const ::boost::shared_ptr<MIDW_EXT_SXM_FUEL_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< GetFuelStationInfoError >& /*error*/)
{
}


/***********************************************************************//**

***************************************************************************/
void clSDS_FuelPriceList::onGetFuelStationInfoResult(
   const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr<  GetFuelStationInfoResult >& result)
{
   if (T_e8_SxmPredefinedListIds__NEAR_BY == result->getListID())
   {
      if (result->getFuelStationInfo().getBrand().empty())
      {
         _fuelStationInfo.szStationNameBrandName = result->getFuelStationInfo().getStationName();
      }
      else
      {
         _fuelStationInfo.szStationNameBrandName = result->getFuelStationInfo().getBrand();
      }
      _fuelStationInfo.szCityName = result->getFuelStationInfo().getCity();
      _fuelStationInfo.szStateName = result->getFuelStationInfo().getState();
      vUpdateHeadLineTagForFuelPrice();
   }
}


/***********************************************************************//**

***************************************************************************/
void clSDS_FuelPriceList::onGetCanadianFuelStationInfoError(
   const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< GetCanadianFuelStationInfoError >& /*error*/)
{
}


/***********************************************************************//**

***************************************************************************/
void clSDS_FuelPriceList::onGetCanadianFuelStationInfoResult(
   const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::GetCanadianFuelStationInfoResult >& result)
{
   if (T_e8_SxmPredefinedListIds__NEAR_BY == result->getListID())
   {
      if (result->getCanadianFuelStationInfo().getBrand().empty())
      {
         _fuelStationInfo.szStationNameBrandName = result->getCanadianFuelStationInfo().getStationName();
      }
      else
      {
         _fuelStationInfo.szStationNameBrandName = result->getCanadianFuelStationInfo().getBrand();
      }
      _fuelStationInfo.szCityName = result->getCanadianFuelStationInfo().getState();
      _fuelStationInfo.szStateName = result->getCanadianFuelStationInfo().getCity();
      vUpdateHeadLineTagForFuelPrice();
   }
}
