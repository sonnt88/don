/*********************************************************************//**
 * \file       clSDS_Display.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Display_h
#define clSDS_Display_h

#include "view_db/Sds_ViewDB.h"
#include "application/clSDS_SDSStatusObserver.h"
#include "application/GuiService.h"
#include "application/PopUpService.h"


class clSDS_SDSStatus;
class clSDS_ScreenData;
class clSDS_ListItems;
class clSDS_View;


class clSDS_Display : public clSDS_SDSStatusObserver
{
   public:
      virtual ~clSDS_Display();
      clSDS_Display(clSDS_SDSStatus* pSdsStatus, GuiService& guiService, PopUpService& popupService);
      void cusorMoved(uint32 cursorIndex);
      tVoid vCloseView(tBool bError = FALSE);
      Sds_ViewId enGetScreenId() const;
      tVoid vShowSDSView(clSDS_ScreenData& oScreenData);
      bpstl::map<bpstl::string, bpstl::string> getScreenVariableData() const;
      bool hasCommandList() const;

   private:
      enum State
      {
         INACTIVE,
         ACTIVE
      };

      tBool bIsIndexWithInPage(tU32 u32NumOfListItems);
      void SetSpeechInputState();
      void getCommandListAvailability(const clSDS_View& sdsView);
      tVoid vSDSStatusChanged();
      void createAndShowView();
      tVoid vShowView(const sds_gui_fi::PopUpService::PopupRequestSignal& popupRequestSignal, Sds_ViewId viewId);

      GuiService& _guiService;
      PopUpService& _popupService;
      clSDS_SDSStatus* _pSdsStatus;
      Sds_ViewId _viewId;
      bpstl::map<bpstl::string, bpstl::string> _screenVariableData;
      bpstl::vector<clSDS_ListItems> _items;
      bpstl::map<bpstl::string, bpstl::string> _headerValueMap;
      Sds_HeaderTemplateId _headerTemplateId;
      bool _isCommandListAvailable;
      uint32 _cursorIndex;
      State _state;
};


#endif
