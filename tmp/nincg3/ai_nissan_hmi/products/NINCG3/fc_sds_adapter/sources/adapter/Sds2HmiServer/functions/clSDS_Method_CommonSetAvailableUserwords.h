/*********************************************************************//**
 * \file       clSDS_Method_CommonSetAvailableUserwords.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_CommonSetAvailableUserwords_h
#define clSDS_Method_CommonSetAvailableUserwords_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "application/SdsPhoneService.h"
#include "MOST_PhonBk_FIProxy.h"


class clSDS_Userwords;
class clSDS_QuickDialList;


class clSDS_Method_CommonSetAvailableUserwords
   : public clServerMethod
   , public asf::core::ServiceAvailableIF
{
   public:
      virtual ~clSDS_Method_CommonSetAvailableUserwords();
      clSDS_Method_CommonSetAvailableUserwords(
         ahl_tclBaseOneThreadService* pService,
         clSDS_Userwords* pUserwords,
         SdsPhoneService& sdsPhoneService,
         clSDS_QuickDialList* pQuickDialList,
         ::boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy> phonebookProxy);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

   private:
      struct UserwordInfo
      {
         uint32 profileId;
         std::vector<uint32> userWordID;
      };
      typedef bpstl::vector<sds2hmi_fi_tcl_Userword, bpstl::allocator<sds2hmi_fi_tcl_Userword> > AvailableUserwords;

      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      tVoid vTraceMethodStart(const AvailableUserwords& oUserwords) const;

      boost::shared_ptr<MOST_PhonBk_FI::MOST_PhonBk_FIProxy> _phonebookProxy;
      clSDS_Userwords* _pUserwords;
      SdsPhoneService& _sdsPhoneService;
      clSDS_QuickDialList* _pQuickDialList;
      std::vector<UserwordInfo> _userwordList;
};


#endif
