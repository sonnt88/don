/*********************************************************************//**
 * \file       clSDS_MyAppsDataBaseObserver.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_MyAppsDataBaseObserver_h
#define clSDS_MyAppsDataBaseObserver_h


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"


class clSDS_MyAppsDataBaseObserver
{
   public:
      virtual ~clSDS_MyAppsDataBaseObserver();
      clSDS_MyAppsDataBaseObserver();
      virtual tVoid vMyAppsListAvailable() = 0;
};


#endif
