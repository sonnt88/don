/*********************************************************************//**
 * \file       clSDS_NBestList.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef CLSDS_NBESTLIST_H_
#define CLSDS_NBESTLIST_H_

#include "application/clSDS_List.h"

class clSDS_NBestList: public clSDS_List
{
   public:
      clSDS_NBestList();
      virtual ~clSDS_NBestList();

      virtual void vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType);
      virtual tU32 u32GetSize();
      virtual std::vector<clSDS_ListItems> oGetItems(tU32 u32StartIndex, tU32 u32EndIndex);
      virtual tBool bSelectElement(tU32 u32SelectedIndex);

      void setItems(std::vector<clSDS_ListItems> items);

   private:
      std::vector<clSDS_ListItems> _listItems;
};


#endif /* CLSDS_NBESTLIST_H_ */
