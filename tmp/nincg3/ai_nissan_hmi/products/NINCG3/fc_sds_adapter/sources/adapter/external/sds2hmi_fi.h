/*********************************************************************//**
 * \file       sds2hmi_fi.h
 *
 * Helper file to avoid repetition of the defines before the actual
 * FI include.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef sds2hmi_fi_h
#define sds2hmi_fi_h

#define SDS2HMI_FI_S_IMPORT_INTERFACE_SDS2HMI_SDSFI_DBGVISITORS
#define SDS2HMI_FI_S_IMPORT_INTERFACE_SDS2HMI_SDSFI_FUNCTIONIDS
#define SDS2HMI_FI_S_IMPORT_INTERFACE_SDS2HMI_SDSFI_SERVICEINFO
#define SDS2HMI_FI_S_IMPORT_INTERFACE_SDS2HMI_SDSFI_ERRORCODES
#include "sds2hmi_fi_gen_if.h"

#endif
