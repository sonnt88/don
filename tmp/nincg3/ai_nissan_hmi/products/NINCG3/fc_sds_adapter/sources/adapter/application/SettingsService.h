/*********************************************************************//**
 * \file       SettingsService.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef SettingsService_h
#define SettingsService_h


#include "sds_gui_fi/SettingsServiceStub.h"


class clSDS_PromptModeObserver;
class clSDS_SdsConfigObserver;
class SettingsObserver;

class SettingsService  : public sds_gui_fi::SettingsService::SettingsServiceStub
{
   public:
      SettingsService();
      virtual ~SettingsService();

      virtual void onBeepOnlyModeSet(const ::boost::shared_ptr< sds_gui_fi::SettingsService::BeepOnlyModeSet >& payload);
      virtual void onShortPromptSet(const ::boost::shared_ptr< sds_gui_fi::SettingsService::ShortPromptSet >& payload);
      virtual void onBestMatchAudioSet(const ::boost::shared_ptr< sds_gui_fi::SettingsService::BestMatchAudioSet >& payload);
      virtual void onBestMatchPhoneBookSet(const ::boost::shared_ptr< sds_gui_fi::SettingsService::BestMatchPhoneBookSet >& payload);
      virtual void onVoicePreferenceSet(const ::boost::shared_ptr< sds_gui_fi::SettingsService::VoicePreferenceSet >& payload);
      virtual void onSpeechRateUpdateRequest(const ::boost::shared_ptr< sds_gui_fi::SettingsService::SpeechRateUpdateRequest >& request);

      void addPromptModeObserver(clSDS_PromptModeObserver* obs);
      void addObserver(clSDS_SdsConfigObserver* obs);
      void addObserver(SettingsObserver* pObserver);

   private:
      void notifyObservers();

      std::vector<SettingsObserver*> _observers;
      clSDS_SdsConfigObserver* _pSdsConfigObserver;
      clSDS_PromptModeObserver* _promptModeObserver;
};


#endif
