/*********************************************************************//**
 * \file       StringUtils.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "application/StringUtils.h"
#include <stdio.h>


StringUtils::StringUtils()
{
}


StringUtils::~StringUtils()
{
}


std::string StringUtils::toString(unsigned int number)
{
   char acBuffer[40];
   sprintf(acBuffer, "%u", number);
   return std::string(acBuffer);
}


std::string& StringUtils::trim(std::string& str)
{
   // trim leading white spaces
   std::size_t pos = str.find_first_not_of(" \t");
   if (std::string::npos != pos)
   {
      str.erase(0, pos);
   }

   // trim trailing white spaces
   pos = str.find_last_not_of(" \t");
   if (std::string::npos != pos)
   {
      str.erase(pos + 1);
   }
   return str;
}
