/*********************************************************************//**
 * \file       clSDS_SessionControl.cpp
 *
 * SDS Session Control class implementation implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_SessionControl.h"
#include "application/clSDS_SDSStatus.h"
#include "application/clSDS_SdsControl.h"
#include "Sds2HmiServer/functions/clSDS_Property_PhoneStatus.h"

using namespace sds_gui_fi::SdsGuiService;

/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_SessionControl::~clSDS_SessionControl()
{
   _pSDSStatus = NULL;
   _pSdsControl = NULL;
   _pPhoneStatus = NULL;
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_SessionControl::clSDS_SessionControl(
   clSDS_SDSStatus* pSDSStatus,
   clSDS_Property_PhoneStatus* pPhoneStatus,
   clSDS_SdsControl* pSdsControl)

   : _pSDSStatus(pSDSStatus)
   , _pPhoneStatus(pPhoneStatus)
   , _pSdsControl(pSdsControl)
   , _startupContext(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_GLOBAL)
{
   _pSDSStatus->vRegisterObserver(this);
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_SessionControl::vPttLongPressed() const
{
   if (_pSdsControl)
   {
      _pSdsControl->vSendPTTLongPressEvent();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_SessionControl::vBackButtonPressed() const
{
   if (_pSdsControl)
   {
      _pSdsControl->vSendBackEvent();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_SessionControl::vStoreSessionContext(sds2hmi_fi_tcl_e8_SDS_EntryPoint::tenType startupContext)
{
   _startupContext = startupContext;
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_SessionControl::vStopSession() const
{
   if (_pSdsControl)
   {
      _pSdsControl->sendCancelDialog();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_SessionControl::vAbortSession() const
{
   if (_pSdsControl)
   {
      _pSdsControl->vAbortDialog();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_SessionControl::vSwcPhoneEndPressed() const
{
   if (_pSdsControl)
   {
      _pSdsControl->vSendBackEvent();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_SessionControl::bIsPttPressAllowed() const
{
   // TODO jnd2hi: determine conditions for allowing PTT
   return TRUE;
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_SessionControl::vSDSStatusChanged()
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
sds2hmi_fi_tcl_e8_SDS_EntryPoint::tenType clSDS_SessionControl::oGetStartupContext() const
{
   if (bMultipleCallDtmfSession() && (_startupContext == sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_GLOBAL))
   {
      return sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_PHONE_OMC;
   }
   if (bSingleCallDtmfSession() && (_startupContext == sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_GLOBAL))
   {
      return sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_PHONE_OAC;
   }
   return _startupContext;
}


/**************************************************************************//**
 *
 ******************************************************************************/
/*tBool clSDS_SessionControl::bSingleCallDtmfSession() const
{
   if ( HSI_PHONE_STS_IN_CALL == dataPool.u16GetValue(DPTELEPHONE__SDS_GETPHONESTATUS_STATUS) &&
         (dataPool.bGetValue(DPTELEPHONE__HANDS_FREE_STATE))
      )
   {
      return TRUE;
   }
   return FALSE;
}*/

/**************************************************************************//**
 *
 ******************************************************************************/
/*tBool clSDS_SessionControl::bMultipleCallDtmfSession() const
{
   if ( HSI_PHONE_STS_SWITCH_CALL == dataPool.u16GetValue(DPTELEPHONE__SDS_GETPHONESTATUS_STATUS) &&
         (dataPool.bGetValue(DPTELEPHONE__HANDS_FREE_STATE))
      )
   {
      return TRUE;
   }
   return FALSE;
}*/


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_SessionControl::vSendPttEvent()
{
   _startupContext = oGetStartupContext();
   if (_pSdsControl)
   {
      _pSdsControl->vSendPttEvent(tU32(_startupContext));
   }
   _startupContext = sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_GLOBAL;
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_SessionControl::bDownloadFailed() const
{
   return _pSDSStatus->bIsError();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_SessionControl::bIsInitialising() const
{
   return _pSDSStatus->bIsLoading();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_SessionControl::bIsSessionActive() const
{
   return _pSDSStatus->bIsActive();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_SessionControl::bIsListening() const
{
   return _pSDSStatus->bIsListening();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_SessionControl::bSingleCallDtmfSession() const
{
   if (_pPhoneStatus)
   {
      if (_pPhoneStatus->getPhoneStatus() == sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_INCALL)
      {
         return TRUE;
      }
   }
   return FALSE;
}


tBool clSDS_SessionControl::bMultipleCallDtmfSession() const
{
   if (_pPhoneStatus)
   {
      if (_pPhoneStatus->getPhoneStatus() == sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_INCALL_HOLD)
      {
         return TRUE;
      }
   }
   return FALSE;
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_SessionControl::vRequestAllSpeakers()
{
   if (_pSdsControl)
   {
      _pSdsControl->vRequestAllSpeakers();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_SessionControl::vSetSpeaker(tU16 u16SpeakerId)
{
   if (_pSdsControl)
   {
      _pSdsControl->vSetSpeaker(u16SpeakerId);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_SessionControl::sendStartSessionContext(ContextType contextId)
{
   _startupContext =  mapGui2SdsStartupContext(contextId);
   if (_pSdsControl)
   {
      _pSdsControl->vSendPttEvent(tU32(_startupContext));
   }
   _startupContext = sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_GLOBAL;
}


/**************************************************************************//**
 *
 ******************************************************************************/
sds2hmi_fi_tcl_e8_SDS_EntryPoint::tenType clSDS_SessionControl::mapGui2SdsStartupContext(ContextType startupContext) const
{
   switch (startupContext)
   {
      case ContextType__SDS_CONTEXT_CALL:
         return(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_CALL_CONTACT);

      case ContextType__SDS_CONTEXT_DIALNUM:
         return(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_DIAL_NUMBER);

      case ContextType__SDS_CONTEXT_CALLHIST:
         return(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_CALL_HISTORY);

      case ContextType__SDS_CONTEXT_QUICKDIAL:
         return(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_QUICK_DIAL);

      case ContextType__SDS_CONTEXT_READTEXT:
         return(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_READ_TEXT);

      case ContextType__SDS_CONTEXT_SENDTEXT:
         return(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_SEND_TEXT);

      case ContextType__SDS_CONTEXT_STREETADDR:
         return(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_NAVI_ENTER_ADDRESS);

      case ContextType__SDS_CONTEXT_POI:
         return(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_NAVI_ENTER_POI);

      case ContextType__SDS_CONTEXT_POICAT:
         return(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_NAVI_ENTER_POICAT);

      case ContextType__SDS_CONTEXT_INTERSECTION:
         return(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_NAVI_ENTER_INTERSECTION);

      case ContextType__SDS_CONTEXT_CITYCENTER:
         return(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_NAVI_CITYCENTER);

      case ContextType__SDS_CONTEXT_PLAYALBUM:
         return(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_SELECT_ALBUM);

      case ContextType__SDS_CONTEXT_PLAYARTIST:
         return(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_SELECT_ARTIST);

      case ContextType__SDS_CONTEXT_PLAYSONG:
         return(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_SELECT_SONG);

      case ContextType__SDS_CONTEXT_PLAYLIST:
         return(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_SELECT_PLAYLIST);

      default:
         return(sds2hmi_fi_tcl_e8_SDS_EntryPoint::FI_EN_SDS_ENTRY_GLOBAL);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_SessionControl::sendEnterPauseMode()
{
   if (_pSdsControl)
   {
      _pSdsControl->sendEnterPauseMode();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_SessionControl::sendExitPauseMode()
{
   if (_pSdsControl)
   {
      _pSdsControl->sendExitPauseMode();
   }
}
