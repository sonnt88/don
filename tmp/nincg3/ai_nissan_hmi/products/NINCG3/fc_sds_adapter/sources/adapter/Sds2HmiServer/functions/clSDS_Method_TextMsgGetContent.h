/*********************************************************************//**
 * \file       clSDS_Method_TextMsgGetContent.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_TextMsgGetContent_h
#define clSDS_Method_TextMsgGetContent_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "MOST_Msg_FIProxy.h"


class SdsPhoneService;
class clSDS_ReadSmsList;


class clSDS_Method_TextMsgGetContent
   : public clServerMethod
   , public MOST_Msg_FI::GetMessageCallbackIF
{
   public:
      virtual ~clSDS_Method_TextMsgGetContent();
      clSDS_Method_TextMsgGetContent(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< ::MOST_Msg_FI::MOST_Msg_FIProxy > pSds2MsgProxy,
         clSDS_ReadSmsList* pSmsList,
         SdsPhoneService* sdsPhoneService);

      virtual void onGetMessageError(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::GetMessageError >& error);
      virtual void onGetMessageResult(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::GetMessageResult >& result);

   protected:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);

   private:
      void sendResult(std::string smsContent);
      void prepareHeader(std::string& ostring) const;
      boost::shared_ptr< ::MOST_Msg_FI::MOST_Msg_FIProxy > _messageProxy;
      clSDS_ReadSmsList* _pReadSmsList;
      SdsPhoneService* _pSdsPhoneService;
};


#endif
