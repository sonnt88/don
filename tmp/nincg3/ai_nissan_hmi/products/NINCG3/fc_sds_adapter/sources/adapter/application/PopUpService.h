/*********************************************************************//**
 * \file       PopUpService.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/


#ifndef PopUpService_h
#define PopUpService_h

#include "sds_gui_fi/PopUpServiceStub.h"

class PopUpService : public sds_gui_fi::PopUpService::PopUpServiceStub
{
   public:
      PopUpService();
      virtual ~PopUpService();
};


#endif /* PopUpService_h */
