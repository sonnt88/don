/*********************************************************************//**
 * \file       clSDS_Method_PhoneGetContactListEntries.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_PhoneGetContactListEntries_h
#define clSDS_Method_PhoneGetContactListEntries_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "application/clSDS_PhonebookListClient.h"
#include "external/sds2hmi_fi.h"
#include "MOST_PhonBk_FIProxy.h"

#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"


class clSDS_PhonebookList;
class clSDS_Userwords;
class clSDS_QuickDialList;


class clSDS_Method_PhoneGetContactListEntries : public clServerMethod, clSDS_PhonebookListClient
   , public MOST_PhonBk_FI::GetContactDetailsExtendedCallbackIF
{
   public:
      virtual ~clSDS_Method_PhoneGetContactListEntries();
      clSDS_Method_PhoneGetContactListEntries(
         ahl_tclBaseOneThreadService* pService,
         clSDS_PhonebookList* pPhonebookList,
         clSDS_Userwords* pUserwords,
         ::boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy> phonebookProxy,
         clSDS_QuickDialList* pQuickDialList);

      virtual void onGetContactDetailsExtendedError(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedError >& error);
      virtual void onGetContactDetailsExtendedResult(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedResult >& result);

   private:
      virtual tVoid vPhonebookListAvailable();
      tVoid vPhonebookListSendMethodResult();
      bpstl::string oGetUniquePhoneNumber() const;
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      tVoid vRequestPhonebookDetails();
      tVoid vRequestUserwordDetails();
      tVoid vTraceMethodResult(const sds2hmi_sdsfi_tclMsgPhoneGetContactListEntriesMethodResult& oMethodResult) const;
      sds2hmi_fi_tcl_PhoneEntry oGetPhoneEntryData() const;

      boost::shared_ptr<MOST_PhonBk_FI::MOST_PhonBk_FIProxy> _phonebookProxy;
      clSDS_PhonebookList* _pPhonebookList;
      clSDS_Userwords* _pUserwords;
      clSDS_QuickDialList* _pQuickDialList;
      tU16 _u16StartIndex;
      sds2hmi_fi_tcl_e8_GEN_SelectionType::tenType _e8SelectionType;
};


#endif
