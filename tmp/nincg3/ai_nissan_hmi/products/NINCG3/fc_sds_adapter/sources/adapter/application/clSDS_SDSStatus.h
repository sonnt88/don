/*********************************************************************//**
 * \file       clSDS_SDSStatus.h
 *
 * clSDS_SDSStatus class functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_SDSStatus_h
#define clSDS_SDSStatus_h


#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"
#include "external/sds2hmi_fi.h"
#include "application/PopUpService.h"

class clSDS_SDSStatusObserver;

class clSDS_SDSStatus
{
   public:
      enum enSDSStatus
      {
         EN_IDLE,
         EN_DIALOGOPEN,
         EN_LOADING,
         EN_ERROR,
         EN_LISTENING,
         EN_ACTIVE,
         EN_UNKNOWN,
         EN_PAUSE
      };
      virtual ~clSDS_SDSStatus();
      clSDS_SDSStatus();
      tVoid vRegisterObserver(clSDS_SDSStatusObserver* pObserver);
      tVoid vSDSStatusChanged(enSDSStatus oStatus);
      tBool bIsIdle() const;
      tBool bIsError() const;
      tBool bIsLoading() const;
      tBool bIsDialogOpen() const;
      tBool bIsListening() const;
      tBool bIsActive() const;
      enSDSStatus getStatus() const;

   private:
      clSDS_SDSStatus::enSDSStatus _oSDSStatus;
      tVoid vNotifyObservers();
      bpstl::vector<clSDS_SDSStatusObserver*> _observers;
};


#endif
