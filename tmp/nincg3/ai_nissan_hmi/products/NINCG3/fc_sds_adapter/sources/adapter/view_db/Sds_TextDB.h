/*********************************************************************//**
 * \file       Sds_TextDB.h
 *
 * Exported interface of the text data base.
 * See Sds_TextDB.dat for valid language and text ids.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef Sds_TextDB_h
#define Sds_TextDB_h


//lint -efile(451,Sds_TextDB.dat) repeatedly included but does not have a standard include guard - jnd2hi


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"


/**
 *  Generation of fixtext language enum.
 *  See Sds_TextDB.dat for valid language ids.
 */
enum Sds_TextLanguageId
{
#define SDS_TEXT_LANGUAGE(id)                 id,
#include "Sds_TextDB.dat"
#undef SDS_TEXT_LANGUAGE
   SDS_TEXT_LANGUAGE_ID_LIMIT
};


/**
 *  Generation of fixtext id enum.
 *  See Sds_TextDB.dat for valid fixtext ids.
 */
enum Sds_TextId
{
#define SDS_TEXT(id, isMultiLang, offset)     id,
#include "Sds_TextDB.dat"
#undef SDS_TEXT
   SDS_TEXT_ID_LIMIT
};


void Sds_TextDB_vSetLanguage(Sds_TextLanguageId enLanguage);
const char* Sds_TextDB_pGetLanguageName(Sds_TextLanguageId enLanguage);
const tChar* Sds_TextDB_vGetText(Sds_TextId enTextId);
unsigned char Sds_TextDB_bIsMatchingTextId(Sds_TextId enView, const bpstl::string& oString);


#endif
