/*********************************************************************//**
 * \file       clSDS_Method_PhoneSetContact.cpp
 *
 * clSDS_Method_PhoneSetContact method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_PhoneSetContact.h"
#include "application/clSDS_Iconizer.h"
#include "application/clSDS_PhonebookList.h"
#include "application/clSDS_StringVarList.h"


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_PhoneSetContact::~clSDS_Method_PhoneSetContact()
{
   _pPhonebookList = NULL;
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_PhoneSetContact::clSDS_Method_PhoneSetContact(ahl_tclBaseOneThreadService* pService, clSDS_PhonebookList* pclPhonebookList)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_PHONESETCONTACT, pService), _pPhonebookList(pclPhonebookList)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_PhoneSetContact::vPhonebookListAvailable()
{
   if (_pPhonebookList != NULL)
   {
      if (_pPhonebookList->bIsEntryAvailable() == true)
      {
         vSendMethodResult();
      }
      else
      {
         vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_LISTEMPTY);
      }
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_PhoneSetContact::vRequestContactEntriesFromPhone(sds2hmi_fi_tcl_e8_PHN_SelectionType::tenType enSelectionType, tU32 u32ValueId)
{
   if (_pPhonebookList != NULL)
   {
      switch (enSelectionType)
      {
         case sds2hmi_fi_tcl_e8_PHN_SelectionType::FI_EN_BY_CONTACT_ID:
            _pPhonebookList->vRequestPhonebookDetailsFromPhone(this, u32ValueId, clSDS_PhonebookList::EN_SELECTION_NEXT, true);
            break;

         case sds2hmi_fi_tcl_e8_PHN_SelectionType::FI_EN_BY_PHONEBOOK_INDEX:
            _pPhonebookList->vRequestPhonebookDetailsFromPhone(this, u32ValueId, clSDS_PhonebookList::EN_SELECTION_NEXT, false);
            break;

         default:
            vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
            break;
      }
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_PhoneSetContact::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgPhoneSetContactMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   vRequestContactEntriesFromPhone(oMessage.SelectionType.enType, oMessage.ValueID);
}
