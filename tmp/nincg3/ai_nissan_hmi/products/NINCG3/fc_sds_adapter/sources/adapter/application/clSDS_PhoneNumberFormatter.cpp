/*********************************************************************//**
 * \file       clSDS_PhoneNumberFormatter.cpp
 *
 * clSDS_PhoneNumberFormatter class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_PhoneNumberFormatter.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_PhoneNumberFormatter::~clSDS_PhoneNumberFormatter()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_PhoneNumberFormatter::clSDS_PhoneNumberFormatter()
{
}


/***********************************************************************//**
* 2345678 => 234-5678
* 23456789 => 234-567-89
***************************************************************************/
tVoid clSDS_PhoneNumberFormatter::vFormatNumberStartingWithGreaterThanOne(bpstl::string& oNumber)
{
   if ((oNumber.size() > 6) && (oNumber.size() != 7))
   {
      oNumber.insert(6, "-");
   }
   oNumber.insert(3, "-");
}


/***********************************************************************//**
* 123456789 => 1-234-567-8
***************************************************************************/
tVoid clSDS_PhoneNumberFormatter::vFormatNumberStartingWithOne(bpstl::string& oNumber)
{
   tU32 u32Size = (tU32)oNumber.size();

   if (u32Size > 7)
   {
      oNumber.insert(7, "-");
   }

   if (u32Size > 4)
   {
      oNumber.insert(4, "-");
   }

   oNumber.insert(1, "-");
}


/***********************************************************************//**
*
***************************************************************************/
tBool bValidateFirstCharacter(const tChar character, tU32 u32Size)
{
   if (((character < '1') || (character > '9'))  ||
         ((character > '1') && (u32Size >= NUMBER_MAX_LIMIT - 1))
      )
   {
      return FALSE;
   }
   return TRUE;
}


/***********************************************************************//**
*
***************************************************************************/
tBool bValidateRemainingCharacters(const bpstl::string& oNumber)
{
   for (tU32 u32Index = 1; u32Index < (tU32)oNumber.size(); u32Index++)
   {
      if ((oNumber[u32Index] < '0') || (oNumber[u32Index]  > '9'))
      {
         return FALSE;
      }
   }
   return TRUE;
}


/***********************************************************************//**
*
***************************************************************************/
tBool bIsFormattingRequired(const bpstl::string& oNumber)
{
   if ((oNumber.size() > NUMBER_MIN_LIMIT) && (oNumber.size() < NUMBER_MAX_LIMIT))
   {
      tBool bReturnValue = bValidateFirstCharacter(oNumber[0], (tU32)oNumber.size());
      if (bReturnValue)
      {
         bReturnValue = bValidateRemainingCharacters(oNumber);
      }
      return bReturnValue;
   }
   return FALSE;
}


/**********************************************************************************//**
* expected formatting
* 123456789 => 1-234-567-8 (Formatting required for number length between 4 and 11)
* 23456789 => 234-567-89 (Formatting required for number length between 4 and 11)
* 012345678 => 0123456789 (No formatting for Number starting with 0, *, #, +)
***********************************************************************************/
bpstl::string clSDS_PhoneNumberFormatter::oFormatNumber(const bpstl::string& oNumber)
{
   if (bIsFormattingRequired(oNumber))
   {
      bpstl::string oFormatDigits(oNumber);
      if (oNumber[0] == '1')
      {
         vFormatNumberStartingWithOne(oFormatDigits);
      }
      else
      {
         vFormatNumberStartingWithGreaterThanOne(oFormatDigits);
      }
      return oFormatDigits;
   }
   return oNumber;
}
