/*********************************************************************//**
 * \file       clSDS_G2P_FactorySettings.cpp
 *
 * clSDS_G2P_FactorySettings class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_G2P_FactorySettings.h"
#include "application/clSDS_SdsControl.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_G2P_FactorySettings::~clSDS_G2P_FactorySettings()
{
   _pSdsControl = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_G2P_FactorySettings::clSDS_G2P_FactorySettings(clSDS_SdsControl* pSdsControl)
   : _pSdsControl(pSdsControl)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_G2P_FactorySettings::vSetG2PFactorySettings() const
{
   if (_pSdsControl)
   {
      _pSdsControl->vClearPrivateData();
   }
}
