/*********************************************************************//**
 * \file       clSDS_Method_NaviSetNavSetting.cpp
 *
 * clSDS_Method_NaviSetNavSetting method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_NaviSetNavSetting.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_NaviSetNavSetting.cpp.trc.h"
#endif


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_NaviSetNavSetting::~clSDS_Method_NaviSetNavSetting()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_NaviSetNavSetting::clSDS_Method_NaviSetNavSetting(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr< NavigationServiceProxy > pSds2NaviProxy,
   GuiService& guiService)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_NAVISETNAVSETTING, pService)
   , _sds2NaviProxy(pSds2NaviProxy)
   , _guiService(guiService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviSetNavSetting::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgNaviSetNavSettingMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   vSendVoiceGuidanceSettingRequest(oMessage);
   vSendMethodResult();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_NaviSetNavSetting::vSendVoiceGuidanceSettingRequest(const sds2hmi_sdsfi_tclMsgNaviSetNavSettingMethodStart& oMessage)
{
   switch (oMessage.nNavSetting.enType)
   {
      case sds2hmi_fi_tcl_e8_NAV_Setting::FI_EN_VOICE_GUID_ON:
         _sds2NaviProxy->sendSetVoiceGuidanceRequest(*this, true);
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_PASSIVE);
         break;

      case sds2hmi_fi_tcl_e8_NAV_Setting::FI_EN_VOICE_GUID_OFF:
         _sds2NaviProxy->sendSetVoiceGuidanceRequest(*this, false);
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_PASSIVE);
         break;

      default:
         break;
   }
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetNavSetting::onSetVoiceGuidanceResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< SetVoiceGuidanceResponse >& /*response*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetNavSetting::onSetVoiceGuidanceError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< SetVoiceGuidanceError >& /*error*/)
{
}
