/*********************************************************************//**
 * \file       clSDS_XMLDoc.h
 *
 * This class holds the xml doc created out of provided XML stream.
 * It acts as an interface with the XML library. It returns the contents
 * of the XML stream in a HMI defined tree structure (clSDS_TagContents)
 * /remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_XMLDoc_h
#define clSDS_XMLDoc_h


#include "libxml/parser.h"

#define SYSTEM_S_IMPORT_INTERFACE_STRING
#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#include "stl_pif.h"


class clSDS_TagContents;


class clSDS_XMLDoc
{
   public:
      virtual ~clSDS_XMLDoc();
      clSDS_XMLDoc(bpstl::string const& oXMLStream);
      bpstl::vector<clSDS_TagContents> oGetElementsOfTag(bpstl::string const& oTagName);

   private:
      tVoid vReadChildContents(const xmlNode* oTopNode, bpstl::vector<clSDS_TagContents>& oTagContents);
      clSDS_TagContents oReadTagContents(const xmlNode* oNode);

      xmlDocPtr _doc;
};


#endif
