/*********************************************************************//**
 * \file       clSDS_Property_CommonStatus.cpp
 *
 * Common Action Request property implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Property_CommonStatus.h"
#include "application/clSDS_IsVehicleMoving.h"
#include "external/sds2hmi_fi.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Property_CommonStatus.cpp.trc.h"
#endif


#define MAX_VEHICLE_SPEED_IN_MILES 5
#define KMPH_TO_MILES_CONVERSION_VALUE 161


using namespace VEHICLE_MAIN_FI;
using namespace tcu_main_fi;
using namespace sxm_audio_main_fi;


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Property_CommonStatus::~clSDS_Property_CommonStatus()
{
   _pIsVehicleMoving = NULL;
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Property_CommonStatus::clSDS_Property_CommonStatus(
   ahl_tclBaseOneThreadService* pService,
   clSDS_IsVehicleMoving* pIsVehicleMoving,
   ::boost::shared_ptr< VEHICLE_MAIN_FIProxy > pVehicleFiProxy,
   ::boost::shared_ptr< Tcu_main_fiProxy > pTcuFiProxy,
   ::boost::shared_ptr< Sxm_audio_main_fiProxy > posxmAudioProxy)
   : clServerProperty(SDS2HMI_SDSFI_C_U16_COMMONSTATUS, pService),
     _pIsVehicleMoving(pIsVehicleMoving),
     _vehicleProxy(pVehicleFiProxy),
     _tcuProxy(pTcuFiProxy),
     _sxmAudioProxy(posxmAudioProxy)
{
   _pIsVehicleMoving->vRegisterObserver(this);
   _tcuServiceStatus = false;
   _vehicleSpeed = 0;
   _sxmSportsStatus = false;
   _marketRegionType = clSDS_KDSConfiguration::getMarketRegionType();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_CommonStatus::vSet(amt_tclServiceData* /*pInMsg*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_CommonStatus::vGet(amt_tclServiceData* /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_CommonStatus::vUpreg(amt_tclServiceData* /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_CommonStatus::vSendStatus()
{
   sds2hmi_sdsfi_tclMsgCommonStatusStatus oMessage;
   oMessage.IsVehicleMoving = ((_vehicleSpeed > MAX_VEHICLE_SPEED_IN_MILES) ? TRUE : FALSE);
   oMessage.FeatureConfiguration.bSet(oGetFeatureConfiguration().c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   oMessage.TCUAvialable = _tcuServiceStatus;
   oMessage.SportsServiceAvailable = _sxmSportsStatus;

   vStatus(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_CommonStatus::vTCUServiceStatusChanged()
{
   ETG_TRACE_USR1(("TCU Service Status Changed %d", _tcuServiceStatus));
   vSendStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_CommonStatus::vSportsServiceStatusChanged()
{
   ETG_TRACE_USR1(("Sports Service Status Changed %d", _sxmSportsStatus));
   vSendStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_CommonStatus::vVehicleSpeedChanged()
{
   ETG_TRACE_USR1(("Vehicle Speed Changed. Speed is %f ", _vehicleSpeed));
   vSendStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
bpstl::string clSDS_Property_CommonStatus::oGetFeatureConfiguration() const
{
   switch (_marketRegionType)
   {
      case clSDS_KDSConfiguration::USA:
         return "USA";

      case clSDS_KDSConfiguration::OTHER_EUR:
         return "EUR";

      case clSDS_KDSConfiguration::GCC:
         return"GCC";

      case clSDS_KDSConfiguration::CAN:
         return"CAN";

      case clSDS_KDSConfiguration::MEX:
         return "MEX";

      default:
         return "";
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonStatus::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _vehicleProxy)
   {
      _vehicleProxy->sendSpeedUpReg(*this);
   }
   if (proxy == _tcuProxy)
   {
      _tcuProxy->sendProvisionServiceUpReg(*this);
   }
   if (proxy == _sxmAudioProxy)
   {
      _sxmAudioProxy->sendSxmDataServiceStatusUpReg(*this);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonStatus::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _vehicleProxy)
   {
      _vehicleProxy->sendSpeedRelUpRegAll();
   }
   if (proxy == _tcuProxy)
   {
      _tcuProxy->sendProvisionServiceRelUpRegAll();
   }
   if (proxy == _sxmAudioProxy)
   {
      _sxmAudioProxy->sendSxmDataServiceStatusRelUpRegAll();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonStatus::onSpeedError(
   const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< SpeedError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonStatus::onSpeedStatus(
   const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< SpeedStatus >& status)
{
   uint16 _vehicleSpeedinKmph = status->getSpeedValue();
   if (_vehicleSpeedinKmph < 65535)
   {
      _vehicleSpeed = _vehicleSpeedinKmph / KMPH_TO_MILES_CONVERSION_VALUE;
      vVehicleSpeedChanged();
   }
   else
   {
      ETG_TRACE_USR1(("Vehicle speed received from Vehicle is invalid"));
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonStatus::onProvisionServiceError(
   const ::boost::shared_ptr< Tcu_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< ProvisionServiceError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonStatus::onProvisionServiceStatus(
   const ::boost::shared_ptr< Tcu_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< ProvisionServiceStatus >& status)
{
   if (status->hasProvisionService())
   {
      _tcuServiceStatus = status->getProvisionService().getBOperatorService();
   }
   vTCUServiceStatusChanged();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonStatus::onSxmDataServiceStatusError(
   const ::boost::shared_ptr< Sxm_audio_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< SxmDataServiceStatusError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonStatus::onSxmDataServiceStatusStatus(
   const ::boost::shared_ptr< Sxm_audio_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< SxmDataServiceStatusStatus >& status)
{
   std::vector< ::sxm_main_fi_types::T_SxmListElemDataSrvState >  ::const_iterator lSxmList = status->getList().begin();
   unsigned int sxmListCount = status->getList().size();
   for (unsigned int u32Index = 0; u32Index < sxmListCount ; u32Index++)
   {
      if ((lSxmList->getStatus().getStatus() == sxm_main_fi_types::T_e8_SxmDataServiceStatus__SUBSCRIBED) &&
            (lSxmList->getType() == sxm_main_fi_types::T_e8_SxmDataServiceType__SPORTS))
      {
         _sxmSportsStatus = true;
         break;
      }
      else
      {
         _sxmSportsStatus = false;
      }
      ++lSxmList;
   }
   vSportsServiceStatusChanged();
}
