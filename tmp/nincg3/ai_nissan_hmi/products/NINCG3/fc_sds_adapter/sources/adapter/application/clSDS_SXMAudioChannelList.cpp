/*********************************************************************//**
 * \file       clSDS_SXMAudioChannelList.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "application/clSDS_SXMAudioChannelList.h"
#include "application/clSDS_TunerStateObserver.h"
#include "application/clSDS_SampaToLHPlus.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_SXMAudioChannelList.cpp.trc.h"
#endif

#define ENTIRE_RANGE 0
#define MIN_RANGE_INDEX 0

using namespace sxm_audio_main_fi;
using namespace MIDW_EXT_SXM_PHONETICS_FI;

clSDS_SXMAudioChannelList::clSDS_SXMAudioChannelList(::boost::shared_ptr< Sxm_audio_main_fiProxy > sxmAudioProxy, ::boost::shared_ptr< MIDW_EXT_SXM_PHONETICS_FIProxy > sxmPhoneticsProxy, ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::SdsSxmServiceProxy > sds2sxmProxy)
   : _sxmAudioProxy(sxmAudioProxy),
     _sxmPhoneticsProxy(sxmPhoneticsProxy),
     _sds2sxmProxy(sds2sxmProxy),
     _pTunerStateObserver(NULL),
     _requestsCount(0)
{
}


clSDS_SXMAudioChannelList::~clSDS_SXMAudioChannelList()
{
   _pTunerStateObserver = NULL;
}


void clSDS_SXMAudioChannelList::onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _sxmAudioProxy)
   {
      ETG_TRACE_USR4(("clSDS_SXMAudioChannelList::onUnavailable - AUDIO"));
      _channelList.clear();
      _sxmAudioProxy->sendChannelListStatusRelUpRegAll();
   }
   else if (proxy == _sxmPhoneticsProxy)
   {
      ETG_TRACE_USR4(("clSDS_SXMAudioChannelList::onUnavailable - PHONETICS"));
      _sxmPhoneticsProxy->sendPhoneticsUpdateRelUpRegAll();
   }
   else if (proxy == _sds2sxmProxy)
   {
      ETG_TRACE_USR4(("clSDS_SXMAudioChannelList::onUnavailable - SQL"));
   }
}


void clSDS_SXMAudioChannelList::onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _sxmAudioProxy)
   {
      ETG_TRACE_USR4(("clSDS_SXMAudioChannelList::onAvailable - AUDIO"));
      _sxmAudioProxy->sendChannelListStatusUpReg(*this);
   }
   else if (proxy == _sxmPhoneticsProxy)
   {
      ETG_TRACE_USR4(("clSDS_SXMAudioChannelList::onAvailable - PHONETICS"));
      _sxmPhoneticsProxy->sendPhoneticsUpdateUpReg(*this);
   }
   else if (proxy == _sds2sxmProxy)
   {
      ETG_TRACE_USR4(("clSDS_SXMAudioChannelList::onAvailable - SQL"));
   }
}


void clSDS_SXMAudioChannelList::onChannelListStatusError(const ::boost::shared_ptr< Sxm_audio_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ChannelListStatusError >& /*error*/)
{
}


void clSDS_SXMAudioChannelList::onChannelListStatusStatus(const ::boost::shared_ptr< Sxm_audio_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ChannelListStatusStatus >& status)
{
   ETG_TRACE_USR4(("clSDS_SXMAudioChannelList::onChannelListStatusStatus - status = %d", status->getAvailability()));

   if (status->getAvailability())
   {
      _channelList.clear();

      incrRequests();

      _sxmAudioProxy->sendGetChannelListStart(*this, MIN_RANGE_INDEX, ENTIRE_RANGE);
   }
}


void clSDS_SXMAudioChannelList::onGetChannelListError(const ::boost::shared_ptr< Sxm_audio_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< GetChannelListError >& /*error*/)
{
   decrRequests();
}


void clSDS_SXMAudioChannelList::onGetChannelListResult(const ::boost::shared_ptr< Sxm_audio_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< GetChannelListResult >& result)
{
   ETG_TRACE_USR4(("clSDS_SXMAudioChannelList::onGetChannelListResult - size = %d", result->getChannelList().size()));

   // process channel information from sxm_audio_fi
   setSXMAudioChannelsList(result->getChannelList());

   if (_sds2sxmProxy->isAvailable())
   {
      _sds2sxmProxy->sendStoreSXMChannelNamesRequest(*this, _channelList);
   }
}


void clSDS_SXMAudioChannelList::setSXMAudioChannelsList(const ::std::vector< ::sxm_audio_main_fi_types::T_ChannelListEntry >& channelsList)
{
   ETG_TRACE_USR4(("clSDS_SXMAudioChannelList::setSXMAudioChannelsList"));

   ::std::vector< ::sxm_audio_main_fi_types::T_ChannelListEntry >::const_iterator it = channelsList.begin();
   while (it != channelsList.end())
   {
      sds_sxm_fi::SdsSxmService::SXMChannelItem listItem;
      listItem.setServiceID(it->getServiceID());
      listItem.setChannelID(it->getChannelID());
      listItem.setChannelNumber(it->getChannelID()); // for compatibility with SDS MW
      listItem.setChannelName("");
      if (it->getChannelNameAvail())
      {
         listItem.setChannelName(it->getChannelName());
      }
      _channelList.push_back(listItem);
      ++it;
   }
}


//PhoneticsUpdateCallbackIF
void clSDS_SXMAudioChannelList::onPhoneticsUpdateError(const ::boost::shared_ptr< MIDW_EXT_SXM_PHONETICS_FIProxy >& /*proxy*/, const ::boost::shared_ptr< PhoneticsUpdateError >& /*error*/)
{
}


void clSDS_SXMAudioChannelList::onPhoneticsUpdateStatus(const ::boost::shared_ptr< MIDW_EXT_SXM_PHONETICS_FIProxy >& /*proxy*/, const ::boost::shared_ptr< PhoneticsUpdateStatus >& status)
{
   const tU8 languageType = status->getLanguageType();
   const tU8 phoneticsType = status->getPhoneticsType();

   ETG_TRACE_USR4(("clSDS_SXMAudioChannelList::onPhoneticsUpdateStatus - phoneticsType = %d, - languageType = %d", phoneticsType, languageType));

   // request phonetic data from sxm_phonetics_fi
   if (phoneticsType & midw_ext_sxm_phonetics_fi_types::T_e8PhoneticsType__PHONETIC_TYPE_CHANNELS)
   {
      if (languageType & midw_ext_sxm_phonetics_fi_types::T_e8LanguageType__PHONETIC_LANG_TYPE_ENGLISH)
      {
         _sxmPhoneticsProxy->sendGetPhoneticsDataStart(*this,
               midw_ext_sxm_phonetics_fi_types::T_e8PhoneticsType__PHONETIC_TYPE_CHANNELS,
               midw_ext_sxm_phonetics_fi_types::T_e8LanguageType__PHONETIC_LANG_TYPE_ENGLISH);

         incrRequests();
      }
      if (languageType & midw_ext_sxm_phonetics_fi_types::T_e8LanguageType__PHONETIC_LANG_TYPE_FRENCH)
      {
         _sxmPhoneticsProxy->sendGetPhoneticsDataStart(*this,
               midw_ext_sxm_phonetics_fi_types::T_e8PhoneticsType__PHONETIC_TYPE_CHANNELS,
               midw_ext_sxm_phonetics_fi_types::T_e8LanguageType__PHONETIC_LANG_TYPE_FRENCH);

         incrRequests();
      }
      if (languageType & midw_ext_sxm_phonetics_fi_types::T_e8LanguageType__PHONETIC_LANG_TYPE_SPANISH)
      {
         _sxmPhoneticsProxy->sendGetPhoneticsDataStart(*this,
               midw_ext_sxm_phonetics_fi_types::T_e8PhoneticsType__PHONETIC_TYPE_CHANNELS,
               midw_ext_sxm_phonetics_fi_types::T_e8LanguageType__PHONETIC_LANG_TYPE_SPANISH);

         incrRequests();
      }
   }
}


//GetPhoneticsDataCallbackIF
void clSDS_SXMAudioChannelList::onGetPhoneticsDataError(const ::boost::shared_ptr< MIDW_EXT_SXM_PHONETICS_FIProxy >& /*proxy*/, const ::boost::shared_ptr< GetPhoneticsDataError >& /*error*/)
{
   decrRequests();
}


void clSDS_SXMAudioChannelList::onGetPhoneticsDataResult(const ::boost::shared_ptr< MIDW_EXT_SXM_PHONETICS_FIProxy >& /*proxy*/, const ::boost::shared_ptr< GetPhoneticsDataResult >& result)
{
   // process channel information from sxm_phonetics_fi
   const tU8 phoneticsType = result->getPhoneticsType();
   const tU8 languageType = result->getLanguageType();
   const sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::tenType languageCode = getLanguageCode(languageType);

   ETG_TRACE_USR4(("clSDS_SXMAudioChannelList::onGetPhoneticsDataResult - phoneticsType = %d, languageType = %d", phoneticsType, languageType));

   if ((phoneticsType == midw_ext_sxm_phonetics_fi_types::T_e8PhoneticsType__PHONETIC_TYPE_CHANNELS)
         && (languageCode != sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_INVALID))
   {
      ::std::vector< ::midw_ext_sxm_phonetics_fi_types::T_ChannelPhoneticsInfo > channelsList = result->getChannelList();
      const tU32 listSize = channelsList.size();

      ETG_TRACE_USR4(("clSDS_SXMAudioChannelList::onGetPhoneticsDataResult - listSize = %d", listSize));

      if (listSize > 0)
      {
         setSXMPhoneticsDataList(result->getChannelList(), languageCode);
      }
   }
}


void clSDS_SXMAudioChannelList::setSXMPhoneticsDataList(const ::std::vector< ::midw_ext_sxm_phonetics_fi_types::T_ChannelPhoneticsInfo >& phoneticsList, sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::tenType languageCode)
{
   ETG_TRACE_USR4(("clSDS_SXMAudioChannelList::setSXMPhoneticsDataList - languageCode = %d", languageCode));

   if (_sds2sxmProxy->isAvailable())
   {
      ::std::vector< sds_sxm_fi::SdsSxmService::SXMPhoneticData > _phoneticData;

      ::std::vector< ::midw_ext_sxm_phonetics_fi_types::T_ChannelPhoneticsInfo >::const_iterator it = phoneticsList.begin();
      while (it != phoneticsList.end())
      {
         sds_sxm_fi::SdsSxmService::SXMPhoneticData listItem;

         listItem.setLanguageID(languageCode);
         listItem.setServiceID(it->getID());

         ::std::string data = "";

         ::std::vector< ::std::string > sampaDataList = it->getPhoneticsData();
         for (::std::vector< ::std::string >::iterator sampaIt = sampaDataList.begin(); sampaIt != sampaDataList.end(); ++sampaIt)
         {
            ::std::string translatedStr = clSDS_SampaToLHPlus::translate(languageCode, *sampaIt);

            // phonetics data must be no longer than 222 characters
            if ((data.size() + DELIMITER_SIZE + translatedStr.size()) > (MAX_CHAR_NUMBER - 1))
            {
               break;
            }

            if (std::strcmp(data.c_str(), "") && std::strcmp(translatedStr.c_str(), ""))
            {
               data.append(DELIMITER);
            }

            data.append(translatedStr);
         }

         listItem.setPhoneticData(data);

         _phoneticData.push_back(listItem);

         ++it;
      }

      _sds2sxmProxy->sendStoreSXMPhoneticDataRequest(*this, _phoneticData);
   }
}


void clSDS_SXMAudioChannelList::setSXMTunerStateObserver(clSDS_TunerStateObserver* obs)
{
   if (obs != NULL)
   {
      _pTunerStateObserver = obs;
   }
}


void clSDS_SXMAudioChannelList::onStoreSXMChannelNamesError(const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::SdsSxmServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::StoreSXMChannelNamesError >& /*error*/)
{
   decrRequests();
}


void clSDS_SXMAudioChannelList::onStoreSXMChannelNamesResponse(const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::SdsSxmServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::StoreSXMChannelNamesResponse >& /*response*/)
{
   decrRequests();
}


void clSDS_SXMAudioChannelList::onStoreSXMPhoneticDataError(const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::SdsSxmServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::StoreSXMPhoneticDataError >& /*error*/)
{
   decrRequests();
}


void clSDS_SXMAudioChannelList::onStoreSXMPhoneticDataResponse(const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::SdsSxmServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::StoreSXMPhoneticDataResponse >& /*response*/)
{
   decrRequests();
}


sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::tenType clSDS_SXMAudioChannelList::getLanguageCode(uint8 language_type) const
{
   switch (language_type)
   {
      case midw_ext_sxm_phonetics_fi_types::T_e8LanguageType__PHONETIC_LANG_TYPE_ENGLISH:
         return sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_ENG;

      case midw_ext_sxm_phonetics_fi_types::T_e8LanguageType__PHONETIC_LANG_TYPE_FRENCH:
         return sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_FRA;

      case midw_ext_sxm_phonetics_fi_types::T_e8LanguageType__PHONETIC_LANG_TYPE_SPANISH:
         return sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_SPA;

      default:
         return sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_INVALID;
   }
}


void clSDS_SXMAudioChannelList::incrRequests()
{
   ++_requestsCount;

   if (_requestsCount == 1 && _pTunerStateObserver)
   {
      ETG_TRACE_USR4(("clSDS_SXMAudioChannelList::incrRequests - FI_EN_DEVICE_UPDATING"));

      _pTunerStateObserver->vTunerStatusChanged(sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_XM, sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_UPDATING);
   }
}


void clSDS_SXMAudioChannelList::decrRequests()
{
   --_requestsCount;

   if (_requestsCount == 0 && _pTunerStateObserver)
   {
      ETG_TRACE_USR4(("clSDS_SXMAudioChannelList::decrRequests - FI_EN_DEVICE_READY"));

      _pTunerStateObserver->vTunerStatusChanged(sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_XM, sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_READY);
   }
}
