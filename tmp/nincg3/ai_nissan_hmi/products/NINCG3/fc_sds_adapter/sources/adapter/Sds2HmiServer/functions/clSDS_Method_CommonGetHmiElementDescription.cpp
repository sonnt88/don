/*********************************************************************//**
 * \file       clSDS_Method_CommonGetHmiElementDescription.cpp
 *
 * clSDS_Method_CommonGetHmiElementDescription method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_CommonGetHmiElementDescription.h"
#include "external/sds2hmi_fi.h"
#include "application/clSDS_MenuManager.h"
#include "application/clSDS_ListScreen.h"

/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_CommonGetHmiElementDescription::clSDS_Method_CommonGetHmiElementDescription(
   ahl_tclBaseOneThreadService* pService,
   clSDS_MenuManager* pSDS_MenuManager,
   clSDS_ListScreen* poListScreen)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_COMMONGETHMIELEMENTDESCRIPTION, pService)
   , _pSDS_MenuManager(pSDS_MenuManager)
   , m_poListScreen(poListScreen)
{
}


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_CommonGetHmiElementDescription::~clSDS_Method_CommonGetHmiElementDescription()
{
   _pSDS_MenuManager = NULL;
   m_poListScreen = NULL;
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_CommonGetHmiElementDescription::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgCommonGetHMIElementDescriptionMethodStart oMessage;
   sds2hmi_sdsfi_tclMsgCommonGetHMIElementDescriptionMethodResult oResult;
   vGetDataFromAmt(pInMessage, oMessage);
   getHmiDescription(oMessage, oResult);
   vSendMethodResult(oResult);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_CommonGetHmiElementDescription::getHmiDescription(sds2hmi_sdsfi_tclMsgCommonGetHMIElementDescriptionMethodStart& oMessage,
      sds2hmi_sdsfi_tclMsgCommonGetHMIElementDescriptionMethodResult& oResult)
{
   if (oMessage.DescriptionMode.enType == sds2hmi_fi_tcl_e8_HMIElementDescriptorMode::FI_EN_DESCR_PROMPT)
   {
      switch (oMessage.ElementReference.enType)
      {
         case sds2hmi_fi_tcl_e8_HMIElementReference::FI_EN_ELEMENT_ONFOCUS :
         {
            if (_pSDS_MenuManager != NULL)
            {
               sds2hmi_fi_tcl_HMIElementDescription oHMIElementDescription;
               oHMIElementDescription.DescriptorTag = "PROMPTID";
               oHMIElementDescription.DescriptorValue  = _pSDS_MenuManager->getPromptId().c_str();
               oResult.Description.push_back(oHMIElementDescription);
            }
         }
         break;
         case sds2hmi_fi_tcl_e8_HMIElementReference::FI_EN_ELEMENT_BY_LINENUMBER :
         {
            if (m_poListScreen != NULL)
            {
               oResult.Description = m_poListScreen->getHmiElementDescription(oMessage.HMIId);
            }
         }
         break;
         default :
            break;
      }
   }
}
