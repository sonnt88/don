/*********************************************************************//**
 * \file       clSDS_Method_TextMsgSend.cpp
 *
 * clSDS_Method_TextMsgSend method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_TextMsgSend.h"
#include "external/sds2hmi_fi.h"
#include "application/clSDS_TextMsgContent.h"


#define NEW_MESSAGE_HANDLE 1


using namespace MOST_Msg_FI;
using namespace most_Msg_fi_types;
using namespace fi_basetypes_most;
using namespace MOST_PhonBk_FI;
using namespace most_PhonBk_fi_types;


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_TextMsgSend::~clSDS_Method_TextMsgSend()
{
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_TextMsgSend::clSDS_Method_TextMsgSend(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr< ::MOST_Msg_FI::MOST_Msg_FIProxy > pSds2MsgProxy,
   ::boost::shared_ptr< ::MOST_PhonBk_FIProxy> pPhoneBookProxy)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_TEXTMSGSEND, pService)
   , _sds2MsgProxy(pSds2MsgProxy)
   , _phoneBookProxy(pPhoneBookProxy)
   , _messageHandle()
   , _createMessageHandle(0)
   , _msgAddressField()
   , _msgSubject()
   , _messageBody()
   , _msgAttachmentHandleStream()
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_TextMsgSend::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   _createMessageHandle = NEW_MESSAGE_HANDLE;
   _messageHandle.setU8DeviceHandle(_phoneBookProxy->getDevicePhoneBookFeatureSupport().getU8DeviceHandle());
   _sds2MsgProxy->sendCreateMessageStart(*this, _createMessageHandle, _messageHandle, most_Msg_fi_types::T_e8_MsgMessageType__e8MSG_TYPE_SMS,
                                         most_Msg_fi_types::T_e8_MsgCreateMessageType__e8NEW_MESSAGE);
}


/**************************************************************************//**
 *
 ******************************************************************************/
/*tVoid clSDS_Method_TextMsgSend::vReplySendSMSResult(tU8 u8SendSmsResult)
{
   if (u8SendSmsResult == 0)
   {
      vSendMethodResult();
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}*/
void clSDS_Method_TextMsgSend::onCreateMessageError(
   const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_Msg_FI::CreateMessageError >& /*error*/)
{
   clearNewMessageDetails();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TextMsgSend::onCreateMessageResult(
   const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_Msg_FI::CreateMessageResult >& result)
{
   _createMessageHandle = result->getU8CreateMessageHandle();
   std::string phoneNumber = clSDS_TextMsgContent::getPhoneNumber();
   T_MsgAddressFieldItem msgAddressFieldItem;
   msgAddressFieldItem.setSPhoneNumber(phoneNumber);
   msgAddressFieldItem.setE8AddressFieldType(T_e8_MsgAddressFieldType__e8ADDRESS_FIELD_TO);
   _msgAddressField.push_back(msgAddressFieldItem);
   _sds2MsgProxy->sendProvideMessageHeaderStart(*this, _createMessageHandle, _msgSubject, _msgAddressField);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TextMsgSend::onProvideMessageHeaderError(
   const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_Msg_FI::ProvideMessageHeaderError >& /*error*/)
{
   clearNewMessageDetails();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TextMsgSend::onProvideMessageHeaderResult(
   const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_Msg_FI::ProvideMessageHeaderResult >& result)
{
   _createMessageHandle = result->getU8CreateMessageHandle();
   _messageBody.push_back(clSDS_TextMsgContent::getTextMsgContent());
   _sds2MsgProxy->sendProvideMessageBodyStart(*this, _createMessageHandle, 0, _msgAttachmentHandleStream,
         1, _messageBody);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TextMsgSend::onProvideMessageBodyError(
   const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_Msg_FI::ProvideMessageBodyError >& /*error*/)
{
   clearNewMessageDetails();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TextMsgSend::onProvideMessageBodyResult(
   const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_Msg_FI::ProvideMessageBodyResult >& result)
{
   _createMessageHandle = result->getU8CreateMessageHandle();
   _sds2MsgProxy->sendSendMessageStart(*this, _createMessageHandle, most_Msg_fi_types::T_e8_MsgFolderType__e8MSG_FOLDER_SENT);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TextMsgSend::onSendMessageError(
   const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_Msg_FI::SendMessageError >& /*error*/)
{
   clearNewMessageDetails();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TextMsgSend::onSendMessageResult(
   const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_Msg_FI::SendMessageResult >& result)
{
   clearNewMessageDetails();
   if (result->getE8SentMessageStatus() == T_e8_MsgSentMessageStatus__e8MSG_SENT_STATUS_SUCCESS)
   {
      vSendMethodResult();
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TextMsgSend::onAvailable(
   const ::boost::shared_ptr< asf::core::Proxy >& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _phoneBookProxy)
   {
      _phoneBookProxy->sendDevicePhoneBookFeatureSupportUpReg(*this);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TextMsgSend::onUnavailable(
   const ::boost::shared_ptr< asf::core::Proxy >& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _phoneBookProxy)
   {
      _phoneBookProxy->sendDevicePhoneBookFeatureSupportRelUpRegAll();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TextMsgSend::onDevicePhoneBookFeatureSupportError(
   const ::boost::shared_ptr< ::MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ::DevicePhoneBookFeatureSupportError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TextMsgSend::onDevicePhoneBookFeatureSupportStatus(
   const ::boost::shared_ptr< ::MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ::DevicePhoneBookFeatureSupportStatus >& /*status*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TextMsgSend::clearNewMessageDetails()
{
   _messageHandle.clear();
   _createMessageHandle = 0;
   _msgAddressField.clear();
   _messageBody.clear();
}
