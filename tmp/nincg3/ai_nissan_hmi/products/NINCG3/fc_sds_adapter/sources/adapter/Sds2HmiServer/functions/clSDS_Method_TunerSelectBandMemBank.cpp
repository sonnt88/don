/*********************************************************************//**
 * \file       clSDS_Method_TunerSelectBandMemBank.cpp
 *
 * clSDS_Method_TunerSelectBandMemBank method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_TunerSelectBandMemBank.h"
#include "AppHmi_MasterBase/AudioInterface/AudioDefines.h"


using namespace NS_AUDIOSRCCHG;
using namespace sds_gui_fi::SdsGuiService;


#define INITIALISE_SUBSRC_ID     (-1)


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_TunerSelectBandMemBank::clSDS_Method_TunerSelectBandMemBank(
   ahl_tclBaseOneThreadService* pService,
   boost::shared_ptr< AudioSourceChangeProxy > proxy,
   GuiService& guiService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_TUNERSELECTBAND_MEMBANK, pService)
   , _audioSrcChgProxy(proxy)
   , _guiService(guiService)
{
}


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_TunerSelectBandMemBank::~clSDS_Method_TunerSelectBandMemBank()
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_TunerSelectBandMemBank::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgTunerSelectBand_MemBankMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   vActivateTunerSource(oMessage.Band);
   vSendMethodResult();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TunerSelectBandMemBank::vActivateTunerSource(const sds2hmi_fi_tcl_e8_TUN_Band& band)
{
   int mAudioSourecID = 0;
   switch (band.enType)
   {
      case sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_FM:
         mAudioSourecID = SRC_TUNER_FM;
         _guiService.sendEventSignal(Event__SPEECH_DIALOG_SDS_PLAY_FM);
         break;
      case sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_AM_MW:
         mAudioSourecID = SRC_TUNER_AM;
         _guiService.sendEventSignal(Event__SPEECH_DIALOG_SDS_PLAY_AM);
         break;
      case sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_XM:
         mAudioSourecID = SRC_TUNER_XM;
         _guiService.sendEventSignal(Event__SPEECH_DIALOG_SDS_PLAY_XM);
         break;
      default:
         return ;
   }

   _srcData.setSrcId(mAudioSourecID);
   _srcData.setSubSrcId(INITIALISE_SUBSRC_ID);
   _audioSrcChgProxy->sendActivateSourceRequest(*this, _srcData);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TunerSelectBandMemBank::onActivateSourceError(
   const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/,
   const ::boost::shared_ptr< ActivateSourceError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TunerSelectBandMemBank::onActivateSourceResponse(
   const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/,
   const ::boost::shared_ptr< ActivateSourceResponse >& /*response*/)
{
}
