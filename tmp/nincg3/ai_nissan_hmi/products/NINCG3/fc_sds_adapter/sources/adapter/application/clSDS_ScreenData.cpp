/*********************************************************************//**
 * \file       clSDS_ScreenData.cpp
 *
 * clSDS_ScreenData class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_ScreenData.h"
#include "application/clSDS_ListItems.h"
#include "application/clSDS_TagContents.h"
#include "application/clSDS_XMLDoc.h"
#include "application/StringUtils.h"
#include "SdsAdapter_Trace.h"
#include <cstdlib>

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_ScreenData.cpp.trc.h"
#endif


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_ScreenData::~clSDS_ScreenData()
{
   try
   {
      delete _pXMLDoc;
   }
   catch (...)
   {
      ETG_TRACE_FATAL(("Exception in destructor"));
   }
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_ScreenData::clSDS_ScreenData(bpstl::string const& oXMLString)
{
   _pXMLDoc = new clSDS_XMLDoc(oXMLString);
   _phoneNumberDigits = "";
   _templateId = "";
}


/***********************************************************************//**
*
***************************************************************************/
Sds_ViewId clSDS_ScreenData::enReadScreenId() const
{
   if (NULL != _pXMLDoc)
   {
      bpstl::vector<clSDS_TagContents> oTagContents = _pXMLDoc->oGetElementsOfTag("SCREENID");
      if (oTagContents.size())
      {
         bpstl::string oScreenId(oTagContents.back().oTagValue.c_str());
         return Sds_ViewDB_enFindViewId(oScreenId);
      }
   }
   return SDS_VIEW_ID_LIMIT;
}


/***********************************************************************//**
*
***************************************************************************/
Sds_HeaderTemplateId clSDS_ScreenData::enReadTemplateId(tVoid)
{
   if (NULL != _pXMLDoc)
   {
      bpstl::vector<clSDS_TagContents> oHeaderTagContents = _pXMLDoc->oGetElementsOfTag("HEADER");
      if (oHeaderTagContents.size())
      {
         bpstl::vector<clSDS_TagContents> oTagContents = oHeaderTagContents.back().voChildrenTags;
         for (tU32 u32Index = 0; u32Index < oTagContents.size(); u32Index++)
         {
            if ((oTagContents[u32Index].oTagName == "TEMPLATE"))
            {
               return enReadTemplateId(oTagContents[u32Index].voChildrenTags);
            }
         }
      }
   }
   return SDS_HEADER_TEMPLATE_ID_LIMIT;
}


/***********************************************************************//**
*
***************************************************************************/
Sds_HeaderTemplateId clSDS_ScreenData::enReadTemplateId(bpstl::vector<clSDS_TagContents>  &oTagContents)
{
   for (tU32 u32Index = 0; u32Index < oTagContents.size(); u32Index++)
   {
      if ((oTagContents[u32Index].oTagName == "TID"))
      {
         bpstl::string oTemplateId(oTagContents[u32Index].oTagValue.c_str());
         ETG_TRACE_USR1(("Template ID = %s", oTemplateId.c_str()));
         if (strcmp(oTemplateId.c_str(), "PhoneNumberDigits") == 0)
         {
            _templateId = oTagContents[u32Index].oTagValue.c_str();
         }
         return Sds_ViewDB_enFindTemplateId(oTemplateId);
      }
   }
   ETG_TRACE_USR1(("No Template ID"));
   return SDS_HEADER_TEMPLATE_ID_LIMIT;
}


/***********************************************************************//**
*
***************************************************************************/
tU32 clSDS_ScreenData::u32GetCount(bpstl::string const& oTag) const
{
   if (NULL != _pXMLDoc)
   {
      return _pXMLDoc->oGetElementsOfTag(oTag).size();
   }
   return 0;
}


/***********************************************************************//**
*
***************************************************************************/
tU32 clSDS_ScreenData::u32GetNumberOfElements() const
{
   return u32GetCount("ELEMENT");
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_ScreenData::vStoreListContents(const bpstl::vector<clSDS_ListItems> &oListContents)
{
   if (!oListContents.empty())
   {
      _oListContents = oListContents;
   }
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::vector<clSDS_ListItems> clSDS_ScreenData::oGetListItemsofView(tVoid)
{
   if (!_oListContents.empty())
   {
      return _oListContents;
   }

   bpstl::vector<clSDS_ListItems>  oListContents;
   if (NULL != _pXMLDoc)
   {
      bpstl::vector<clSDS_TagContents> oTagContents = _pXMLDoc->oGetElementsOfTag("ELEMENT");
      for (tU32 u32Index = 0; u32Index < oTagContents.size(); u32Index++)
      {
         clSDS_ListItems oListContent;

         std::string oTagValue = oTagContents[u32Index].oTagValue;

         bpstl::vector<uint> localIds = getLocalIds(oTagContents[u32Index].voChildrenTags);

         oListContent.oDescription.szString = formatTagValue(oTagValue, localIds);

         oListContents.push_back(oListContent);
      }

      printListItems(oListContents);
   }
   return oListContents;
}


tVoid clSDS_ScreenData::printListItems(const bpstl::vector<clSDS_ListItems>& listContents) const
{
   bpstl::string oTempTrace;

   for (tU32 u32Index = 0; u32Index < listContents.size(); u32Index++)
   {
      oTempTrace.append(StringUtils::toString(u32Index + 1) + ": " + listContents[u32Index].oDescription.szString);
   }

   if (!oTempTrace.empty())
   {
      oTempTrace.append("$");
      bpstl::string oPart1(oTempTrace.substr(0, 226));
      ETG_TRACE_USR1(("NBestList = %s", oPart1.c_str()));
      if (oTempTrace.length() > 226)
      {
         bpstl::string oPart2(oTempTrace.substr(226, oTempTrace.length() - 226));
         ETG_TRACE_USR1(("%s", oPart2.c_str()));
      }
   }
}


bpstl::vector<uint> clSDS_ScreenData::getLocalIds(bpstl::vector<clSDS_TagContents>& oChildrenTags) const
{
   bpstl::vector<uint> localIds;

   for (uint i = 0; i < oChildrenTags.size(); i++)
   {
      if (oChildrenTags[i].oTagName == "LOCALID")
      {
         uint localId = (uint)atoi(oChildrenTags[i].oTagValue.c_str());
         localIds.push_back(localId);
      }
   }
   return localIds;
}


Sds_TextId clSDS_ScreenData::getTextIDForLocalId(const bpstl::string& localIdString) const
{
   uint localId = (uint)std::atoi(localIdString.c_str());
   return getTextIDForLocalId(localId);
}


Sds_TextId clSDS_ScreenData::getTextIDForLocalId(uint localId) const
{
   const uint LOCATIONS_OFFSET = 100;
   const Sds_TextId locations[] = { SDS_TEXT_HOMENUMBER, SDS_TEXT_WORKNUMBER, SDS_TEXT_MOBILENUMBER, SDS_TEXT_OTHER};
   const uint LOCATIONS_COUNT = sizeof(locations) / sizeof(locations[0]);

   const uint RELATIONSHIPS_OFFSET = 400;
   const Sds_TextId relationships[] =
   {
      SDS_TEXT_MOM, SDS_TEXT_DAD, SDS_TEXT_PARENTS, SDS_TEXT_BROTHER, SDS_TEXT_SISTER,
      SDS_TEXT_CHILD, SDS_TEXT_SON, SDS_TEXT_DAUGHTER,
      SDS_TEXT_FRIEND, SDS_TEXT_PARTNER, SDS_TEXT_WIFE, SDS_TEXT_HUSBAND,
      SDS_TEXT_MYHOME, SDS_TEXT_MYOFFICE, SDS_TEXT_ASSISTANT, SDS_TEXT_MANAGER, SDS_TEXT_OTHER
   };
   const uint RELATIONSHIPS_COUNT = sizeof(relationships) / sizeof(relationships[0]);

   if ((localId >= RELATIONSHIPS_OFFSET) && (localId < RELATIONSHIPS_OFFSET + RELATIONSHIPS_COUNT))
   {
      return relationships[localId - RELATIONSHIPS_OFFSET];
   }

   if ((localId >= LOCATIONS_OFFSET) && (localId < LOCATIONS_OFFSET + LOCATIONS_COUNT))
   {
      return locations[localId - LOCATIONS_OFFSET];
   }

   NORMAL_M_ASSERT_ALWAYS();
   return SDS_TEXT_ID_LIMIT;	//lint !e527 Unreachable code - jnd2hi: behind assertion
}


bpstl::string clSDS_ScreenData::formatTagValue(bpstl::string tagValue, bpstl::vector<uint> localIDs) const
{
   bpstl::string oTagValue = tagValue;

   StringUtils::trim(oTagValue);//string may need trimming

   for (uint i = 0; i < localIDs.size(); i++)
   {
      Sds_TextId textID = getTextIDForLocalId(localIDs[i]);
      if (SDS_TEXT_ID_LIMIT != textID)
      {
         oTagValue.append(" (");
         oTagValue.append(Sds_TextDB_vGetText(textID));
         oTagValue.append(")");
      }
   }

   return oTagValue;
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::map<bpstl::string, bpstl::string> clSDS_ScreenData::oGetScreenVariableData(tVoid)
{
   bpstl::vector<bpstl::string> oTagList;
   oTagList.push_back("PARAMETERS");
   oTagList.push_back("PARAMETER");
   return oGetMap(oTagList, "NAME", "VALUE");
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::map<bpstl::string, bpstl::string> clSDS_ScreenData::oGetHeaderVariableData(tVoid)
{
   bpstl::vector<bpstl::string> oTagList;
   oTagList.push_back("HEADER");
   oTagList.push_back("TEMPLATE");
   oTagList.push_back("VARIABLE");
   return oGetMap(oTagList, "TAG", "VALUE");
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::vector<bpstl::string> clSDS_ScreenData::oGetList(bpstl::vector<bpstl::string> &oTagList)
{
   if (oTagList.size() && NULL != _pXMLDoc)
   {
      bpstl::vector<clSDS_TagContents> oTagContents = _pXMLDoc->oGetElementsOfTag(oTagList[0]);
      return oGetListofElements(oTagList, oTagContents);
   }
   bpstl::vector<bpstl::string> oDummyList;
   return oDummyList;
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::map<bpstl::string, bpstl::string> clSDS_ScreenData::oGetMap(bpstl::vector<bpstl::string> &oTagList, bpstl::string const& oKeyId, bpstl::string const& oValueId)
{
   if (oTagList.size()  && NULL != _pXMLDoc)
   {
      bpstl::vector<clSDS_TagContents> oTagContents = _pXMLDoc->oGetElementsOfTag(oTagList[0]);
      return oGetMap(oTagList, oTagContents, oKeyId, oValueId);
   }
   bpstl::map<bpstl::string, bpstl::string> oDummyMap;
   return oDummyMap;
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::map<bpstl::string, bpstl::string> clSDS_ScreenData::oGetMap(bpstl::vector<bpstl::string> &oTagList, bpstl::vector<clSDS_TagContents> &oTagContents, bpstl::string const& oKeyId, bpstl::string const& oValueId)
{
   VariableData oData;
   oData.oMap[""] = ""; // Empty set which is added by default for all calls
   oData.oKeyId = oKeyId;
   oData.oValueId = oValueId;
   vAddDataToMap(0, oTagList, oTagContents, oData);
   return oData.oMap;
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::vector<bpstl::string> clSDS_ScreenData::oGetListofElements(bpstl::vector<bpstl::string> &oTagList, bpstl::vector<clSDS_TagContents> &oTagContents)
{
   bpstl::vector<bpstl::string> oDataList;
   oDataList.push_back(""); // Empty string symbolizes no condition which is added by default for all calls
   vAddDataToList(0, oTagList, oTagContents, oDataList);
   return oDataList;
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_ScreenData::vAddDataToMap(tU32 u32TagIndex, bpstl::vector<bpstl::string> &oTagList, bpstl::vector<clSDS_TagContents> &oTagContents, VariableData& oData)
{
   for (tU32 u32Index = 0; u32Index < oTagContents.size(); u32Index++)
   {
      if (bIsLastTag(u32TagIndex, oTagList))
      {
         vAddTagDataToMap(u32TagIndex, oTagList, oTagContents[u32Index], oData);
      }
      else
      {
         vGotoNextTag(u32TagIndex, oTagList, oTagContents[u32Index], oData);
      }
   }
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_ScreenData::vAddDataToList(tU32 u32TagIndex, bpstl::vector<bpstl::string> &oTagList, bpstl::vector<clSDS_TagContents> &oTagContents, bpstl::vector<bpstl::string> &oDataList)
{
   for (tU32 u32Index = 0; u32Index < oTagContents.size(); u32Index++)
   {
      if (bIsLastTag(u32TagIndex, oTagList))
      {
         vAddTagDataToList(u32TagIndex, oTagList, oTagContents[u32Index], oDataList);
      }
      else
      {
         vGotoNextTag(u32TagIndex, oTagList, oTagContents[u32Index], oDataList);
      }
   }
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_ScreenData::vAddTagDataToList(
   tU32 u32TagIndex,
   bpstl::vector<bpstl::string> &oTagList,
   const clSDS_TagContents& oTagContent,
   bpstl::vector<bpstl::string> &oDataList) const
{
   if ((oTagContent.oTagName == (oTagList[u32TagIndex])))
   {
      oDataList.push_back(oTagContent.oTagValue);
   }
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_ScreenData::vAddTagDataToMap(
   tU32 u32TagIndex,
   bpstl::vector<bpstl::string> &oTagList,
   const clSDS_TagContents& oTagContent,
   VariableData& oData)
{
   OSAL_C_PARAMETER_INTENTIONALLY_UNUSED(u32TagIndex);
   if (oTagContent.oTagName == oTagList.back())
   {
      bpstl::vector<clSDS_TagContents> oTagContents = oTagContent.voChildrenTags;
      const bpstl::string key = oGetKey(oTagContents, oData);
      const bpstl::string value = oGetValue(oTagContents, oData);
      vAddToMap(key, value, oData);
      vGetPhoneNumberDigits(oTagContents, oData);
   }
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_ScreenData::vAddToMap(bpstl::string const& oKey, bpstl::string const& oValue, VariableData& oData) const
{
   oData.oMap[oKey] = oValue;
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::string clSDS_ScreenData::oGetKey(bpstl::vector<clSDS_TagContents> &oTagContents, const VariableData& oData) const
{
   for (tU32 u32Index = 0; u32Index < oTagContents.size(); u32Index++)
   {
      if ((oTagContents[u32Index].oTagName == (oData.oKeyId)))
      {
         ETG_TRACE_USR1(("Param = %s", oTagContents[u32Index].oTagValue.c_str()));
         return oTagContents[u32Index].oTagValue;
      }
   }
   return "";
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::string clSDS_ScreenData::oGetValue(bpstl::vector<clSDS_TagContents> &oTagContents, const VariableData& oData) const
{
   for (tU32 u32Index = 0; u32Index < oTagContents.size(); u32Index++)
   {
      if ((oTagContents[u32Index].oTagName == (oData.oValueId)))
      {
         bpstl::string value;

         // check for special case <VALUE><LOCALID>xxx</LOCALID></VALUE>
         if (hasLocalIDValue(oTagContents[u32Index].voChildrenTags))
         {
            value = getLocalIDValue(oTagContents[u32Index].voChildrenTags[0].oTagValue);
         }
         else //regular case: <VALUE>xxx</VALUE>
         {
            value = oTagContents[u32Index].oTagValue;
         }

         ETG_TRACE_USR1(("Value = %s", value.c_str()));

         return value;
      }
   }
   return "";
}


tBool clSDS_ScreenData::hasLocalIDValue(bpstl::vector<clSDS_TagContents> voChildrenTags) const
{
   if (voChildrenTags.size() == 1 &&
         voChildrenTags[0].oTagName == "LOCALID")
   {
      return true;
   }
   return false;
}


bpstl::string clSDS_ScreenData::getLocalIDValue(bpstl::string rawLocalID) const
{
   Sds_TextId textId = getTextIDForLocalId(rawLocalID);
   return Sds_TextDB_vGetText(textId);
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_ScreenData::vGotoNextTag(tU32 u32TagIndex, bpstl::vector<bpstl::string> &oTagList, clSDS_TagContents& oTagContent, VariableData& oData)
{
   if (u32TagIndex < (oTagList.size() - 1))
   {
      if ((oTagContent.oTagName == (oTagList[u32TagIndex])))
      {
         vAddDataToMap((u32TagIndex + 1), oTagList, oTagContent.voChildrenTags, oData);
      }
   }
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_ScreenData::vGotoNextTag(tU32 u32TagIndex, bpstl::vector<bpstl::string> &oTagList, clSDS_TagContents& oTagContent, bpstl::vector<bpstl::string> &oDataList)
{
   if (u32TagIndex < (oTagList.size() - 1))
   {
      if ((oTagContent.oTagName == (oTagList[u32TagIndex])))
      {
         vAddDataToList((u32TagIndex + 1), oTagList, oTagContent.voChildrenTags, oDataList);
      }
   }
}


/***********************************************************************//**
*
***************************************************************************/
tBool clSDS_ScreenData::bIsLastTag(tU32 u32TagIndex, const bpstl::vector<bpstl::string> &oTagList) const
{
   if (u32TagIndex == (oTagList.size() - 1))
   {
      return TRUE;
   }
   return FALSE;
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_ScreenData::vGetPhoneNumberDigits(bpstl::vector<clSDS_TagContents> &oTagContents, const VariableData& oData)
{
   for (tU32 u32Index = 0; u32Index < oTagContents.size(); u32Index++)
   {
      if ((oTagContents[u32Index].oTagName == (oData.oValueId)))
      {
         if (strcmp(_templateId.c_str(), "PhoneNumberDigits") == 0)
         {
            _phoneNumberDigits = oTagContents[u32Index].oTagValue;
            ETG_TRACE_USR1(("PhNumDigits_TagValue = %s", _phoneNumberDigits.c_str()));
         }
      }
   }
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::string clSDS_ScreenData::getPhNumDigit() const
{
   return _phoneNumberDigits;
}
