/*********************************************************************//**
 * \file       clSDS_QuickDialList.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_QuickDialList.h"
#include "application/clSDS_Iconizer.h"
#include "application/clSDS_StringVarList.h"

#include "SdsAdapter_Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_QuickDialList.cpp.trc.h"
#endif


using namespace MOST_PhonBk_FI;
using namespace most_PhonBk_fi_types;
using namespace most_PhonBk__fi_types_Extended;
using namespace sds_gui_fi::SdsPhoneService;


/**************************************************************************//**
 *Destructor
 ******************************************************************************/
clSDS_QuickDialList::~clSDS_QuickDialList()
{
}


/**************************************************************************//**
 *Constructor
 ******************************************************************************/
clSDS_QuickDialList::clSDS_QuickDialList(::boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy> phonebookProxy)
   : _phonebookProxy(phonebookProxy)
   , _contactHandleIndex(0)

{
   _quickDialCount = 0;
   _quickDialListSize = 0;
}


/**************************************************************************//**
 *
 ******************************************************************************/
tU32 clSDS_QuickDialList::u32GetSize()
{
   return _quickDialListForDisplay.size();
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::vector<clSDS_ListItems> clSDS_QuickDialList::oGetItems(tU32 u32StartIndex, tU32 u32EndIndex)
{
   std::vector<clSDS_ListItems> oListItems;
   for (tU32 u32Index = u32StartIndex; u32Index < u32EndIndex; u32Index++)
   {
      clSDS_ListItems oListItem;
      oListItem.oDescription.szString = oGetItem(u32Index);
      oListItems.push_back(oListItem);
   }
   return oListItems;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_QuickDialList::oGetItem(tU32 u32Index)
{
   if (!oGetName(u32Index).empty())
   {
      return clSDS_Iconizer::oAddIconInfix(oGetName(u32Index), oGetNumberType(u32Index),
                                           oGetRelationshipType(u32Index));
   }
   return "";
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_QuickDialList::bSelectElement(tU32 u32SelectedIndex)
{
   if (u32SelectedIndex > 0)
   {
      clSDS_StringVarList::vSetVariable("$(ContactName)", oGetName(u32SelectedIndex - 1));
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
 *
 ******************************************************************************/
bpstl::string clSDS_QuickDialList::oGetSelectedItem(tU32 u32Index)
{
   if (u32Index > 0)
   {
      return oGetPhoneNumber(u32Index - 1);
   }
   return "";
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_QuickDialList::handleQuickDialList(const ::boost::shared_ptr< sds_gui_fi::SdsPhoneService::QuickDialListUpdateRequest >& /*request*/) const
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_QuickDialList::oGetName(tU32 u32Index)
{
   std::string contactName = "";
   if (_quickDialListForDisplay.size())
   {
      if (!_quickDialListForDisplay[u32Index].firstName.empty())
      {
         if (!_quickDialListForDisplay[u32Index].lastName.empty())
         {
            contactName = _quickDialListForDisplay[u32Index].firstName;
            contactName.append(" ");
            contactName.append(_quickDialListForDisplay[u32Index].lastName);
         }
         else
         {
            contactName = _quickDialListForDisplay[u32Index].firstName;
         }
      }
      else
      {
         if (!_quickDialListForDisplay[u32Index].lastName.empty())
         {
            contactName = _quickDialListForDisplay[u32Index].lastName;
         }
         else
         {
            contactName = "";
         }
      }
   }
   return contactName;
}


/**************************************************************************//**
 *
 ******************************************************************************/
uint8 clSDS_QuickDialList::oGetNumberType(tU32 u32Index)
{
   if (_quickDialListForDisplay.size())
   {
      if (_quickDialListForDisplay[u32Index].phoneNumberType == sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_MOBILE1 ||
            _quickDialListForDisplay[u32Index].phoneNumberType == sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_MOBILE2)
      {
         return clSDS_Iconizer::PHONENUMBER_TYPE_MOBILE;
      }
      else if (_quickDialListForDisplay[u32Index].phoneNumberType == sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_OFFICE1 ||
               _quickDialListForDisplay[u32Index].phoneNumberType == sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_OFFICE2)
      {
         return clSDS_Iconizer::PHONENUMBER_TYPE_OFFICE;
      }
      else if (_quickDialListForDisplay[u32Index].phoneNumberType == sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_HOME1 ||
               _quickDialListForDisplay[u32Index].phoneNumberType == sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_HOME2)
      {
         return clSDS_Iconizer::PHONENUMBER_TYPE_HOME;
      }
      else
      {
         return clSDS_Iconizer::PHONENUMBER_TYPE_UNKNOWN;
      }
   }
   return clSDS_Iconizer::PHONENUMBER_TYPE_UNKNOWN;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_QuickDialList::oGetRelationshipType(tU32 u32Index)
{
   if (_quickDialListForDisplay.size())
   {
      if (!_quickDialListForDisplay[u32Index].relationshipType.empty())
      {
         return _quickDialListForDisplay[u32Index].relationshipType;
      }
   }
   return "";
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_QuickDialList::oGetPhoneNumber(tU32 u32Index)
{
   if (_quickDialListForDisplay.size())
   {
      if (!_quickDialListForDisplay[u32Index].phoneNumber.empty())
      {
         return _quickDialListForDisplay[u32Index].phoneNumber;
      }
   }
   return "";
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_QuickDialList::onRequestPhoneBookListSliceExtendedResult(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< MOST_PhonBk_FI::RequestPhoneBookListSliceExtendedResult >& result)
{
   if (result->getOPhoneBookListSliceResultExtended().size())
   {
      for (unsigned int i = 0; i < result->getOPhoneBookListSliceResultExtended().size(); i++)
      {
         uint32 _ContactHandle = result->getOPhoneBookListSliceResultExtended().at(i).getU32ContactHandle();
         if (_ContactHandle)
         {
            _quickDialCount++;
         }
         _phonebookProxy->sendGetContactDetailsExtendedStart(*this, _ContactHandle);
      }
   }
   else
   {
      _quickDialListForDisplay.clear();
      notifyListObserver();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_QuickDialList::onRequestPhoneBookListSliceExtendedError(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< MOST_PhonBk_FI::RequestPhoneBookListSliceExtendedError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_QuickDialList::onCreateContactListResult(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< MOST_PhonBk_FI::CreateContactListResult >& result)
{
   uint16 PBQuickDialHandle = result->getU16ListHandle();
   _quickDialListSize = result->getU16ListLength();
   if (_quickDialListSize)
   {
      _phonebookProxy->sendRequestPhoneBookListSliceExtendedStart(*this, PBQuickDialHandle, 0, _quickDialListSize);
   }
   else
   {
      _quickDialListForDisplay.clear();
      notifyListObserver();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_QuickDialList::onCreateContactListError(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< MOST_PhonBk_FI::CreateContactListError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_QuickDialList:: getQuickDialList()
{
   _quickDialListForDisplay.clear();
   if (_phonebookProxy->hasDevicePhoneBookFeatureSupport())
   {
      _phonebookProxy->sendCreateContactListStart(*this,
            _phonebookProxy->getDevicePhoneBookFeatureSupport().getU8DeviceHandle(),
            T_e8_PhonBkContactListType__VEHICLE, T_e8_PhonBkContactSortType__e8PB_LIST_SORT_POSITION);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_QuickDialList::vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType enListType)
{
   switch (enListType)
   {
      case sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_PHONE_USERWORDS:
         getQuickDialList();
         break;

      default:
         notifyListObserver();
         break;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_QuickDialList::listUserWords(const sds2hmi_sdsfi_tclMsgCommonSetAvailableUserwordsMethodStart& oMessage)
{
   _oMessage = oMessage;
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_QuickDialList::onGetContactDetailsExtendedError(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_QuickDialList::onGetContactDetailsExtendedResult(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedResult >& result)
{
   quickDialListForDisplay quickDialListForDisplayItem;
   quickDialListForDisplayItem.firstName = result->getOContactDetailsExtended().getSFirstName();
   quickDialListForDisplayItem.lastName = result->getOContactDetailsExtended().getSLastName();
   quickDialListForDisplayItem.phoneNumber = getPhoneNumber(result->getOContactDetailsExtended());
   quickDialListForDisplayItem.phoneNumberType = getPhoneNumberType(result->getOContactDetailsExtended());
   quickDialListForDisplayItem.relationshipType = result->getOContactDetailsExtended().getSCategory();
   _quickDialListForDisplay.push_back(quickDialListForDisplayItem);

   if (_quickDialCount == _quickDialListForDisplay.size())
   {
      notifyListObserver();
      _quickDialCount = 0;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_QuickDialList::getPhoneNumber(::most_PhonBk__fi_types_Extended::T_PhonBkContactDetailsExtended contactDetailsExtd) const
{
   std::string phoneNumber = "";
   if (!contactDetailsExtd.getOPhoneNumber1().getSNumber().empty())
   {
      phoneNumber = contactDetailsExtd.getOPhoneNumber1().getSNumber();
   }
   else if (!contactDetailsExtd.getOPhoneNumber2().getSNumber().empty())
   {
      phoneNumber = contactDetailsExtd.getOPhoneNumber2().getSNumber();
   }
   else if (!contactDetailsExtd.getOPhoneNumber3().getSNumber().empty())
   {
      phoneNumber = contactDetailsExtd.getOPhoneNumber3().getSNumber();
   }
   else if (!contactDetailsExtd.getOPhoneNumber4().getSNumber().empty())
   {
      phoneNumber = contactDetailsExtd.getOPhoneNumber4().getSNumber();
   }
   else if (!contactDetailsExtd.getOPhoneNumber5().getSNumber().empty())
   {
      phoneNumber = contactDetailsExtd.getOPhoneNumber5().getSNumber();
   }
   else if (!contactDetailsExtd.getOPhoneNumber6().getSNumber().empty())
   {
      phoneNumber = contactDetailsExtd.getOPhoneNumber6().getSNumber();
   }
   else if (!contactDetailsExtd.getOPhoneNumber7().getSNumber().empty())
   {
      phoneNumber = contactDetailsExtd.getOPhoneNumber7().getSNumber();
   }
   else if (!contactDetailsExtd.getOPhoneNumber8().getSNumber().empty())
   {
      phoneNumber = contactDetailsExtd.getOPhoneNumber8().getSNumber();
   }
   else
   {
      phoneNumber = "";
   }
   return phoneNumber;
}


/**************************************************************************//**
 *
 ******************************************************************************/
sds2hmi_fi_tcl_e8_PHN_NumberType::tenType clSDS_QuickDialList::getPhoneNumberType(::most_PhonBk__fi_types_Extended::T_PhonBkContactDetailsExtended contactDetailsExtd) const
{
   sds2hmi_fi_tcl_e8_PHN_NumberType::tenType phoneNumberType =  sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_UNKNOWN;

   if (contactDetailsExtd.getOPhoneNumber1().getE8Type() == T_e8_PhonBkNumberType__CELL)
   {
      phoneNumberType = sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_MOBILE1;
   }
   else if (contactDetailsExtd.getOPhoneNumber2().getE8Type() == T_e8_PhonBkNumberType__CELL)
   {
      phoneNumberType = sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_MOBILE2;
   }
   else if (contactDetailsExtd.getOPhoneNumber1().getE8Type() == T_e8_PhonBkNumberType__HOME)
   {
      phoneNumberType = sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_HOME1;
   }
   else if (contactDetailsExtd.getOPhoneNumber2().getE8Type() == T_e8_PhonBkNumberType__HOME)
   {
      phoneNumberType = sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_HOME2;
   }
   else if (contactDetailsExtd.getOPhoneNumber1().getE8Type() == T_e8_PhonBkNumberType__WORK)
   {
      phoneNumberType = sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_OFFICE1;
   }
   else if (contactDetailsExtd.getOPhoneNumber2().getE8Type() == T_e8_PhonBkNumberType__WORK)
   {
      phoneNumberType = sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_OFFICE2;
   }
   else if (contactDetailsExtd.getOPhoneNumber1().getE8Type() == T_e8_PhonBkNumberType__OTHER ||
            contactDetailsExtd.getOPhoneNumber2().getE8Type() == T_e8_PhonBkNumberType__OTHER)
   {
      phoneNumberType = sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_OTHER;
   }
   else if (contactDetailsExtd.getOPhoneNumber1().getE8Type() == T_e8_PhonBkNumberType__PREFERRED ||
            contactDetailsExtd.getOPhoneNumber2().getE8Type() == T_e8_PhonBkNumberType__PREFERRED)
   {
      phoneNumberType = sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_UNKNOWN;
   }
   else
   {
      phoneNumberType = sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_UNKNOWN;
   }
   return phoneNumberType;
}
