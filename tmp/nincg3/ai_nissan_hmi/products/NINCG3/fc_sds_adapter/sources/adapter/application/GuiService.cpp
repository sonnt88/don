/*********************************************************************//**
 * \file       GuiService.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "application/GuiService.h"
#include "application/clSDS_SessionControl.h"
#include "application/clSDS_XMLStringCreation.h"
#include "application/clSDS_MenuControl.h"
#include "application/clSDS_Userwords.h"
#include "application/clSDS_LanguageMediator.h"
#include "application/clSDS_KDSConfiguration.h"
#include "application/EarlyStartupPlayer.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/GuiService.cpp.trc.h"
#endif


using namespace sds_gui_fi::SdsGuiService;


GuiService::GuiService(clSDS_SessionControl* pSessionControl, EarlyStartupPlayer* pEarlyStartupPlayer)
   : SdsGuiServiceStub("GuiServicePort")
   , _pSessionControl(pSessionControl)
   , _pMenuControl(NULL)
   , _pEarlyStartupPlayer(pEarlyStartupPlayer)
{
}


void GuiService::onPttPressRequest(const ::boost::shared_ptr< PttPressRequest >& request)
{
   if (_pSessionControl)
   {
      if (request->getPttPressType() == KeyState__KEY_HK_MFL_PTT_SHORT)
      {
         vPttShortPressed();
      }
   }
}


void GuiService::vPttShortPressed()
{
   if (_pSessionControl)
   {
      if (bIsPttPressAllowed())
      {
         if (_pSessionControl->bDownloadFailed())
         {
            sendEventSignal(Event__SPEECH_DIALOG_SHOW_POPUP_NODATACARRIER_OPEN);
         }
         else if (_pSessionControl->bIsInitialising())
         {
            sendEventSignal(Event__SPEECH_DIALOG_SHOW_POPUP_LOADING_OPEN);
         }
         else
         {
            _pSessionControl->vSendPttEvent();
         }
      }
      else
      {
         ETG_TRACE_USR4((" SDS Suppress. Play Audio Beep"));
         sendEventSignal(Event__SPEECH_DIALOG_PLAY_BEEP_AUDIO);
      }
   }
}


void GuiService::onManualOperationRequest(const ::boost::shared_ptr< ManualOperationRequest >& request)
{
   if (_pMenuControl)
   {
      switch (request->getOperationType())
      {
         case OperationType__MANUAL_INTERVENTION_HAPTICAL_SELECTION:
            _pMenuControl->onElementSelected(request->getValue());
            break;

         case OperationType__MANUAL_INTERVENTION_ENCODER_FOCUS_MOVED:
            _pMenuControl->onCursorMoved(request->getValue());
            break;

         case OperationType__MANUAL_INTERVENTION_NEXT_PAGE:
            _pMenuControl->onRequestNextPage();
            break;

         case OperationType__MANUAL_INTERVENTION_PREV_PAGE:
            _pMenuControl->onRequestPreviousPage();
            break;

         default:
            break;
      }
   }
}


GuiService::~GuiService()
{
   _pSessionControl = NULL;
   _pMenuControl = NULL;
   _pEarlyStartupPlayer = NULL;
}


void GuiService::onStartSessionContextRequest(const ::boost::shared_ptr<  sds_gui_fi::SdsGuiService::StartSessionContextRequest >& request)
{
   if (_pSessionControl)
   {
      _pSessionControl->sendStartSessionContext(request->getStartupContextType());
   }
}


void GuiService::onTestModeUpdateRequest(const ::boost::shared_ptr< TestModeUpdateRequest >& /*request*/)
{
}


void GuiService::setMenuControl(clSDS_MenuControl* pMenuControl)
{
   _pMenuControl = pMenuControl;
}


bool GuiService::bIsPttPressAllowed()const
{
   if ((clSDS_KDSConfiguration::getAllianceVRModeType()) == clSDS_KDSConfiguration::OFF)
   {
      ETG_TRACE_USR1(("VR configuration set as '%d' ", clSDS_KDSConfiguration::getAllianceVRModeType()));
      return false;
   }
   else
   {
      return true;
   }
}


void GuiService::onSettingsCommandRequest(const ::boost::shared_ptr< SettingsCommandRequest >& /*request*/)
{
   if (_pMenuControl)
   {
      _pMenuControl->onSettingsRequest();
   }
}


void GuiService::onHelpCommandRequest(const ::boost::shared_ptr< HelpCommandRequest >& /*request*/)
{
   if (_pMenuControl)
   {
      _pMenuControl->onHelpRequest();
   }
}


void GuiService::onAbortSessionRequest(const ::boost::shared_ptr< AbortSessionRequest >& /*request*/)
{
   if (_pSessionControl)
   {
      sendEventSignal(Event__SPEECH_DIALOG_SDS_SESSION_ABORT);
      _pSessionControl->vAbortSession();
      ETG_TRACE_USR4(("Aborting SDS session"));
   }
}


void GuiService::onStopSessionRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StopSessionRequest >& /* request */)
{
   if (_pSessionControl)
   {
      _pSessionControl->vStopSession();
      ETG_TRACE_USR4(("Stoping SDS session"));
   }
}


void GuiService::onBackPressRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::BackPressRequest >& /*request*/)
{
   if (_pSessionControl)
   {
      _pSessionControl->vBackButtonPressed();
      ETG_TRACE_USR4(("BackButtonPressed"));
   }
}


void GuiService::onPauseSessionRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::PauseSessionRequest >& /* request */)
{
   if (_pSessionControl)
   {
      _pSessionControl->sendEnterPauseMode();
   }
}


void GuiService::onResumeSessionRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::ResumeSessionRequest >& /* request */)
{
   if (_pSessionControl)
   {
      _pSessionControl->sendExitPauseMode();
   }
}


void GuiService::onStartEarlyHandlingRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StartEarlyHandlingRequest >& /*request*/)
{
   if (_pEarlyStartupPlayer)
   {
      _pEarlyStartupPlayer->requestStartPlayback();
   }
}


void GuiService::onStopEarlyHandlingRequest(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StopEarlyHandlingRequest >& /*request*/)
{
   if (_pEarlyStartupPlayer)
   {
      _pEarlyStartupPlayer->requestStopPlayback();
   }
}
