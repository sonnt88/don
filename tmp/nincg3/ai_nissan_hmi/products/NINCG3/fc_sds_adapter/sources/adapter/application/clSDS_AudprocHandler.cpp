/*********************************************************************//**
 * \file       clSDS_AudprocHandler.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_AudprocHandler.h"
#include "application/PopUpService.h"

#include "SdsAdapter_Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_AudprocHandler.cpp.trc.h"
#endif


using namespace org::bosch::audproc::service;


#define AUDPROC_SOURCE_ID 		11 /*see audproc-introspection.xml*/
#define AUDPROC_CONFIG_FILE 	"" /*not used*/


clSDS_AudprocHandler::clSDS_AudprocHandler(::boost::shared_ptr< ServiceProxy > audprocProxy, PopUpService* popUpService)
   : _audprocProxy(audprocProxy), _popUpService(popUpService)
{
}


clSDS_AudprocHandler::~clSDS_AudprocHandler()
{
   _popUpService = NULL;
}


void clSDS_AudprocHandler::initialize()
{
   if (_audprocProxy->isAvailable())
   {
      _audprocProxy->sendAudprocInitializeRequest(*this, AUDPROC_SOURCE_ID, AUDPROC_CONFIG_FILE);
   }
}


void clSDS_AudprocHandler::destroy()
{
   if (_audprocProxy->isAvailable())
   {
      _audprocProxy->sendAudprocDestroyRequest(*this, AUDPROC_SOURCE_ID);
   }
}


void clSDS_AudprocHandler::startAudio()
{
   setParam();

   if (_audprocProxy->isAvailable())
   {
      _audprocProxy->sendAudprocStartAudioRequest(*this, AUDPROC_SOURCE_ID);
   }
}


void clSDS_AudprocHandler::stopAudio()
{
   if (_audprocProxy->isAvailable())
   {
      _audprocProxy->sendAudprocStopAudioRequest(*this, AUDPROC_SOURCE_ID);
   }
}


void clSDS_AudprocHandler::setParam()
{
   const uint16 parameterId = 257;	// #define     aplMicLvlWatchRefInt    0x0101  (ai_audio\components\AudioProcessingLib\include\apl.h)
   const uint8 paramType = 3;		// _audproc_type_defs::AUDPROC_TYPE_U16  (ai_audio\components\AudioProcess\include\audproc-common-defs.h)
   const uint32 uParameter = 200;	// time in ms , in the range from 200ms to 65535 ms
   const int32 sParameter = 0;		// not used in this case

   if (_audprocProxy->isAvailable())
   {
      _audprocProxy->sendAudprocSetParamRequest(*this, AUDPROC_SOURCE_ID, parameterId, paramType, uParameter, sParameter);
   }
}


void clSDS_AudprocHandler::onAudprocInitializeError(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< AudprocInitializeError >& /*error*/)
{
}


void clSDS_AudprocHandler::onAudprocInitializeResponse(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< AudprocInitializeResponse >& /*response*/)
{
}


void clSDS_AudprocHandler::onAudprocDestroyError(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< AudprocDestroyError >& /*error*/)
{
}


void clSDS_AudprocHandler::onAudprocDestroyResponse(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< AudprocDestroyResponse >& /*response*/)
{
}


void clSDS_AudprocHandler::onAudprocStartAudioError(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< AudprocStartAudioError >& /*error*/)
{
}


void clSDS_AudprocHandler::onAudprocStartAudioResponse(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< AudprocStartAudioResponse >& /*response*/)
{
   if (_audprocProxy->isAvailable())
   {
      _audprocProxy->sendAudprocMicrolevelStatusRegister(*this);
   }
}


void clSDS_AudprocHandler::onAudprocStopAudioError(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< AudprocStopAudioError >& /*error*/)
{
}


void clSDS_AudprocHandler::onAudprocStopAudioResponse(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< AudprocStopAudioResponse >& /*response*/)
{
   if (_audprocProxy->isAvailable())
   {
      _audprocProxy->sendAudprocMicrolevelStatusDeregisterAll();
   }
}


void clSDS_AudprocHandler::onAudprocSetParamError(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< AudprocSetParamError >& /*error*/)
{
}


void clSDS_AudprocHandler::onAudprocSetParamResponse(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< AudprocSetParamResponse >& /*response*/)
{
}


void clSDS_AudprocHandler::onAudprocMicrolevelStatusError(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< AudprocMicrolevelStatusError >& /*error*/)
{
}


void clSDS_AudprocHandler::onAudprocMicrolevelStatusSignal(const ::boost::shared_ptr< ServiceProxy >& /*proxy*/, const ::boost::shared_ptr< AudprocMicrolevelStatusSignal >& signal)
{
   // calculate percentage value by interpolating between MIN == 0% and MAX == 100%
   const uint32 MIN = 100;
   const uint32 MAX = 10000;
   uint32 limited = signal->getStatusMicroLevel();
   limited = limited < MIN ? MIN : limited;
   limited = limited > MAX ? MAX : limited;
   uint32 percent = ((limited - MIN) * 100) / (MAX + 1 - MIN);
   _popUpService->setMicrophoneLevel(percent);
}
