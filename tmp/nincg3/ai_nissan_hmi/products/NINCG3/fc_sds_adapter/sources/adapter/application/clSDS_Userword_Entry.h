/*********************************************************************//**
 * \file       clSDS_Userword_Entry.h
 *
 * Datapool access related to one userword
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Userword_Entry_h
#define clSDS_Userword_Entry_h


#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"


#define MAX_TOTAL_USERWORDS   20


class clSDS_Userword_Entry
{
   public:
      ~clSDS_Userword_Entry();
      clSDS_Userword_Entry();

      tVoid vSetUWID(tU32 u32UWID);
      tU32 u32GetUWID() const;
      tVoid vSetAvail(tBool bAvail);
      tBool bGeAvail() const;
      tVoid vSetName(const bpstl::string& strName);
      const bpstl::string& oGetName() const;
      tVoid vSetNumber(const bpstl::string& strPhoneNumber);
      const bpstl::string& oGetNumber() const;

      tVoid vMarkAsNotUsed(tVoid);
      tVoid vSetupFromBuffer(tPCU8 pcu8Buf, tU32 u32Length);
      tU32 u32GetBufferSize() const;
      tVoid vGetBuffer(tPCU8 pcu8Buf, tU32 u32MaxLength) const;

   private:

      tBool _bAvail;
      tU32 _u32UWID;
      bpstl::string _strPhoneNumber;
      bpstl::string _strName;
};


#endif
