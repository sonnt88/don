#ifndef FUEL_COMMON_SOURECS_H
#define FUEL_COMMON_SOURECS_H

enum enDistanceUnits
{
   EM_DIST_YARDS,
   EM_DIST_FEET,
   EM_DIST_MILES,
   EM_DIST_KM
};


enum enHeadingType
{
   EN_HEADING_INDEX_UNKNOWN         = -1,
   EN_HEADING_INDEX_NORTH           = 0,
   EN_HEADING_INDEX_NORTH_EAST      = 1,
   EN_HEADING_INDEX_EAST            = 2,
   EN_HEADING_INDEX_SOUTH_EAST      = 3,
   EN_HEADING_INDEX_SOUTH           = 4,
   EN_HEADING_INDEX_SOUTH_WEST      = 5,
   EN_HEADING_INDEX_WEST            = 6,
   EN_HEADING_INDEX_NORTH_WEST      = 7,
};


enum FuelPriceAge
{
   TODAY = 0,
   YESTERDAY = 1,
   TWO_DAY = 2,
   THREE_DAY = 3,
   VERY_OLD = 4
};


struct stFuelListItems
{
   stFuelListItems() { }
   std::string  szDistanceUnit;
   std::string szDistanceValue;
   std::string szBrandName;
   std::string szStationName;
   std::string szFuelPriceValue;
   unsigned char u8fuelAge;
   enHeadingType u8DirectionGUI;
   unsigned int numPricesFuelType;
   double longitude;
   double latitude;
   unsigned int fuelLocID;
};


struct stFuelStation
{
   stFuelStation() { }
   std::string szStationNameBrandName;
   std::string szCityName;
   std::string szStateName;
};


#endif
