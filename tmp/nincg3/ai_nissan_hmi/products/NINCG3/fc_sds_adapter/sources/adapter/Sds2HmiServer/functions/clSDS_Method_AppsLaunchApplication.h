/*********************************************************************//**
 * \file       clSDS_Method_AppsLaunchApplication.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_AppsLaunchApplication_h
#define clSDS_Method_AppsLaunchApplication_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "MOST_Tel_FIProxy.h"


class clSDS_Method_AppsLaunchApplication
   : public clServerMethod
   , public asf::core::ServiceAvailableIF
   , public MOST_Tel_FI::BTDeviceVoiceRecognitionExtendedCallbackIF
{
   public:
      virtual ~clSDS_Method_AppsLaunchApplication();
      clSDS_Method_AppsLaunchApplication(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > pSds2TelProxy);

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onBTDeviceVoiceRecognitionExtendedStatus(
         const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Tel_FI::BTDeviceVoiceRecognitionExtendedStatus >& status);
      virtual void onBTDeviceVoiceRecognitionExtendedError(
         const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Tel_FI::BTDeviceVoiceRecognitionExtendedError >& error);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy > _telephoneProxy;
      bool _voiceRecActive;
      bool _voiceRecSupported;
      most_Tel_fi_types_Extended::T_e8_TelSiriAvailabilityState _siriStatus;
};


#endif
