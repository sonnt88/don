/*********************************************************************//**
 * \file       clSDS_Method_NaviSelectDestListEntry.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_NaviSelectDestListEntry_h
#define clSDS_Method_NaviSelectDestListEntry_h


#include "external/sds2hmi_fi.h"
#include "Sds2HmiServer/framework/clServerMethod.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"


class clSDS_NaviListItems;


class clSDS_Method_NaviSelectDestListEntry
   : public clServerMethod
   , public asf::core::ServiceAvailableIF
   , public org::bosch::cm::navigation::NavigationService::HomeLocationCallbackIF
   , public org::bosch::cm::navigation::NavigationService::WorkLocationCallbackIF
{
   public:
      virtual ~clSDS_Method_NaviSelectDestListEntry();
      clSDS_Method_NaviSelectDestListEntry(
         ahl_tclBaseOneThreadService* pService,
         clSDS_NaviListItems* pNaviListItems,
         ::boost::shared_ptr<org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > naviProxy);

      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onHomeLocationError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::HomeLocationError >& error);
      virtual void onHomeLocationUpdate(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::HomeLocationUpdate >& update);

      virtual void onWorkLocationError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::WorkLocationError >& error);
      virtual void onWorkLocationUpdate(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::WorkLocationUpdate >& update);

      sds2hmi_fi_tcl_e8_NAV_ListType::tenType getNavDestinationType();
      void ResetNavDestinationType();

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      void setHomeDestinationStatus(bool bStatus);
      void setWorkDestinationStatus(bool bStatus);

      boost::shared_ptr<org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > _naviProxy;
      clSDS_NaviListItems* _pNaviListItems;
      sds2hmi_fi_tcl_e8_NAV_ListType::tenType _enNavDestType;
      bool _bHomedestinationAvailable;
      bool _bWorkDestinationAvailable;
};


#endif
