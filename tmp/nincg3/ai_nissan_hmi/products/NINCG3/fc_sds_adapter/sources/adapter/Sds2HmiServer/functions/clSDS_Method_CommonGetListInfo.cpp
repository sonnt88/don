/*********************************************************************//**
 * \file       clSDS_Method_CommonGetListInfo.cpp
 *
 * clSDS_Method_CommonGetListInfo method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "Sds2HmiServer/functions/clSDS_Method_CommonGetListInfo.h"
#include "application/clSDS_List.h"
#include "application/clSDS_NaviListItems.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_CommonGetListInfo.cpp.trc.h"
#endif


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_CommonGetListInfo::~clSDS_Method_CommonGetListInfo()
{
   _pCurrentList = NULL;
   _pNaviListItems = NULL;
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_CommonGetListInfo::clSDS_Method_CommonGetListInfo(ahl_tclBaseOneThreadService* pService,
      clSDS_NaviListItems* pNaviListItems)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_COMMONGETLISTINFO, pService)
   , _pCurrentList(NULL)
   , _pNaviListItems(pNaviListItems)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_CommonGetListInfo::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgCommonGetListInfoMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   vProcessMessageByListType(oMessage.ListType.enType);
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_CommonGetListInfo::vProcessMessageByListType(sds2hmi_fi_tcl_e8_HMI_ListType::tenType enListType)
{
   bpstl::map<sds2hmi_fi_tcl_e8_HMI_ListType::tenType, clSDS_List*>::iterator it;
   it = _oListMap.find(enListType);
   _pCurrentList = it->second;

   if (it != _oListMap.end() && (_pCurrentList != NULL))
   {
      vSetNaviHMIListType(enListType);
      _pCurrentList->vGetListInfo(enListType);
   }
   else
   {
      vSendListAvailability(0);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
sds2hmi_fi_tcl_e8_HMI_ListSize::tenType clSDS_Method_CommonGetListInfo::enGetListAvailability(tU32 u32ListCount) const
{
   if (u32ListCount > 1)
   {
      return sds2hmi_fi_tcl_e8_HMI_ListSize::FI_EN_MULTIPLE;
   }
   else if (u32ListCount == 1)
   {
      return sds2hmi_fi_tcl_e8_HMI_ListSize::FI_EN_ONE;
   }
   else
   {
      return sds2hmi_fi_tcl_e8_HMI_ListSize::FI_EN_NONE;
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_CommonGetListInfo::vSendListAvailability(tU32 u32ListCount)
{
   sds2hmi_sdsfi_tclMsgCommonGetListInfoMethodResult oResult;
   oResult.ListSize.enType = enGetListAvailability(u32ListCount);
   vSendMethodResult(oResult);
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_CommonGetListInfo::vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::tenType type, clSDS_List* pList)
{
   if (pList != NULL)
   {
      pList->setListObserver(this);
      _oListMap[type] = pList;
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_CommonGetListInfo::vListUpdated(clSDS_List* pList)
{
   if (pList != NULL && pList == _pCurrentList)
   {
      vSendListAvailability(pList->u32GetSize());
      _pCurrentList = NULL;
   }
}


/***********************************************************************//**
 *Set Navi HMI List type
 ***************************************************************************/
void clSDS_Method_CommonGetListInfo::vSetNaviHMIListType(sds2hmi_fi_tcl_e8_HMI_ListType::tenType enListType)
{
   if (_pNaviListItems)
   {
      _pNaviListItems->vSetNaviHMIListType(enListType);
   }
}
