/*********************************************************************//**
 * \file       clSDS_ContactNumberList.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_ContactNumberList_h
#define clSDS_ContactNumberList_h


#define SYSTEM_S_IMPORT_INTERFACE_STRING
#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#include "stl_pif.h"
#include "application/clSDS_List.h"

class clSDS_PhonebookList;

class clSDS_ContactNumberList : public clSDS_List
{
   public:
      virtual ~clSDS_ContactNumberList();
      clSDS_ContactNumberList(clSDS_PhonebookList* pPhonebookList);
      tU32 u32GetSize();
      bpstl::vector<clSDS_ListItems> oGetItems(tU32 u32StartIndex, tU32 u32EndIndex);
      tBool bSelectElement(tU32 u32SelectedIndex);
      bpstl::string oGetSelectedItem(tU32 u32Index);

   private:
      bpstl::string oGetConfirmationItem(tU32 u32Index) const;
      bpstl::string oGetItem(tU32 u32Index) const;

      tVoid vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType);

      clSDS_PhonebookList* _pPhonebookList;
};


#endif
