/*********************************************************************//**
 * \file       clSDS_Method_CommonStartSession.cpp
 *
 * clSDS_Method_CommonStartSession method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_CommonStartSession.h"
#include "application/SdsAudioSource.h"
#include "external/sds2hmi_fi.h"


using namespace sds_gui_fi::SdsGuiService;


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_CommonStartSession::~clSDS_Method_CommonStartSession()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_CommonStartSession::clSDS_Method_CommonStartSession(
   ahl_tclBaseOneThreadService* pService,
   SdsAudioSource& audioSource,
   GuiService& guiService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_COMMONSTARTSESSION, pService)
   , _audioSource(audioSource)
   , _guiService(guiService)
   , _waitingOnAudioActivity(false)
{
   _audioSource.addObserver(this);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_CommonStartSession::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   _waitingOnAudioActivity = true;
   _audioSource.sendAudioRouteRequest(ARL_SRC_VRU, ARL_EN_ISRC_ACT_ON);
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_CommonStartSession::onAudioSourceStateChanged(arl_tenActivity state)
{
   if (_waitingOnAudioActivity)
   {
      if (state == ARL_EN_ISRC_ACT_ON)
      {
         _guiService.sendEventSignal(Event__SPEECH_DIALOG_SDS_START_SESSION);
         vSendMethodResult();
      }
      else
      {
         // TODO raa8hi: send error?
      }
      _waitingOnAudioActivity = false;
   }
}
