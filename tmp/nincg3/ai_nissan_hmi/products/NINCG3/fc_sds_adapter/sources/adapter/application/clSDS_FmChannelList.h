/*********************************************************************//**
 * \file       clSDS_FmChannelList.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_FmChannelList_h
#define clSDS_FmChannelList_h


#include "sds_fm_fi/SdsFmServiceProxy.h"
#include "tuner_main_fiProxy.h"
#include "tuner_main_fi_types.h"
#include "asf/core/Timer.h"


class clSDS_TunerStateObserver;


class clSDS_FmChannelList
   : public asf::core::ServiceAvailableIF
   , public sds_fm_fi::SdsFmService::StoreRDSChannelNamesCallbackIF
   , public sds_fm_fi::SdsFmService::StoreHDChannelNamesCallbackIF
   , public tuner_main_fi::FID_TUN_G_GET_CONFIG_LISTCallbackIF
   , public tuner_main_fi::FID_TUN_S_GET_CONFIG_LISTCallbackIF
   , public tuner_main_fi::FID_TUN_S_STATIONLIST_EXITCallbackIF
   , public asf::core::TimerCallbackIF
{
   public :
      clSDS_FmChannelList();     // default constructor without implementation
      clSDS_FmChannelList(
         ::boost::shared_ptr< sds_fm_fi::SdsFmService::SdsFmServiceProxy > _pSdsFmProxy,
         ::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy > tun_fi_proxy);

      virtual ~clSDS_FmChannelList();

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onStoreRDSChannelNamesError(
         const ::boost::shared_ptr< sds_fm_fi::SdsFmService::SdsFmServiceProxy >& proxy,
         const ::boost::shared_ptr< sds_fm_fi::SdsFmService::StoreRDSChannelNamesError >& error);
      virtual void onStoreRDSChannelNamesResponse(
         const ::boost::shared_ptr< sds_fm_fi::SdsFmService::SdsFmServiceProxy >& proxy,
         const ::boost::shared_ptr< sds_fm_fi::SdsFmService::StoreRDSChannelNamesResponse >& response);

      virtual void onStoreHDChannelNamesError(
         const ::boost::shared_ptr< sds_fm_fi::SdsFmService::SdsFmServiceProxy >& proxy,
         const ::boost::shared_ptr< sds_fm_fi::SdsFmService::StoreHDChannelNamesError >& error);
      virtual void onStoreHDChannelNamesResponse(
         const ::boost::shared_ptr< sds_fm_fi::SdsFmService::SdsFmServiceProxy >& proxy,
         const ::boost::shared_ptr< sds_fm_fi::SdsFmService::StoreHDChannelNamesResponse >& response);

      virtual void onFID_TUN_G_GET_CONFIG_LISTError(
         const ::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy >& proxy,
         const ::boost::shared_ptr< tuner_main_fi::FID_TUN_G_GET_CONFIG_LISTError >& error);
      virtual void onFID_TUN_G_GET_CONFIG_LISTStatus(
         const ::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy >& proxy,
         const ::boost::shared_ptr< tuner_main_fi::FID_TUN_G_GET_CONFIG_LISTStatus >& status);

      virtual void onFID_TUN_S_GET_CONFIG_LISTError(
         const ::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy >& proxy,
         const ::boost::shared_ptr< tuner_main_fi::FID_TUN_S_GET_CONFIG_LISTError >& error);
      virtual void onFID_TUN_S_GET_CONFIG_LISTResult(
         const ::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy >& proxy,
         const ::boost::shared_ptr< tuner_main_fi::FID_TUN_S_GET_CONFIG_LISTResult >& result);

      virtual void onFID_TUN_S_STATIONLIST_EXITError(
         const ::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy >& proxy,
         const ::boost::shared_ptr< tuner_main_fi::FID_TUN_S_STATIONLIST_EXITError >& error);
      virtual void onFID_TUN_S_STATIONLIST_EXITResult(
         const ::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy >& proxy,
         const ::boost::shared_ptr< tuner_main_fi::FID_TUN_S_STATIONLIST_EXITResult >& result);

      void setFmChannelListObserver(clSDS_TunerStateObserver* pTunerStateObserver);
      uint32 getFrequencyForObjectID(uint32 objectID);
      uint32 getAudioProgramForObjectID(uint32 objectID);
      virtual void onExpired(asf::core::Timer& timer, boost::shared_ptr<asf::core::TimerPayload> data);

   private:
      bool isFMListUpdated();
      void updateFmObjectIDToChannelNameMap();
      void setTimer();
      void setObjectIdsforHDStationList();
      bool isFMHDListUpdated();

      boost::shared_ptr< sds_fm_fi::SdsFmService::SdsFmServiceProxy > _sdsFmProxy;
      boost::shared_ptr< ::tuner_main_fi::Tuner_main_fiProxy > _tunerProxy;
      clSDS_TunerStateObserver* _pTunerStateObserver;
      std::vector<sds_fm_fi::SdsFmService::FMChannelItem> _fmChannelList;
      std::map<uint32 , std::string> _fmObjectIDToChannelNameMap;
      asf::core::Timer _timer;
      bool _requestChannelList;
      bool _releaseChannelList;
};


#endif
