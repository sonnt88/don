/*********************************************************************//**
 * \file       clSDS_Method_InfoShowMenu.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_InfoShowMenu_h
#define clSDS_Method_InfoShowMenu_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "application/GuiService.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"


using namespace org::bosch::cm::navigation::NavigationService;


class clSDS_Method_InfoShowMenu
   : public clServerMethod
   , public ShowWhereAmIScreenCallbackIF
   , public ShowAdjustCurrentLocationScreenCallbackIF
{
   public:
      virtual ~clSDS_Method_InfoShowMenu();
      clSDS_Method_InfoShowMenu(
         ahl_tclBaseOneThreadService* pService,
         GuiService& guiService,
         ::boost::shared_ptr< NavigationServiceProxy > pSds2NaviProxy);

      virtual void onShowWhereAmIScreenError(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< ShowWhereAmIScreenError >& error);
      virtual void onShowWhereAmIScreenResponse(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< ShowWhereAmIScreenResponse >& response);

      virtual void onShowAdjustCurrentLocationScreenError(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< ShowAdjustCurrentLocationScreenError >& error);
      virtual void onShowAdjustCurrentLocationScreenResponse(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< ShowAdjustCurrentLocationScreenResponse >& response);

   private:
      GuiService& _guiService;
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      tBool bShowInfoScreen(const sds2hmi_sdsfi_tclMsgInfoShowMenuMethodStart& oMessage);
      boost::shared_ptr< NavigationServiceProxy > _sds2NaviProxy;
};


#endif
