/*********************************************************************//**
 * \file       clSDS_SDSStatus.cpp
 *
 * clSDS_SDSStatus class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_SDSStatus.h"
#include "application/clSDS_SDSStatusObserver.h"

/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_SDSStatus::~clSDS_SDSStatus()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_SDSStatus::clSDS_SDSStatus()
   : _oSDSStatus(clSDS_SDSStatus::EN_UNKNOWN)

{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_SDSStatus::vRegisterObserver(clSDS_SDSStatusObserver* pObserver)
{
   if (pObserver != NULL)
   {
      _observers.push_back(pObserver);
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_SDSStatus::vNotifyObservers()
{
   bpstl::vector<clSDS_SDSStatusObserver*>::iterator iter = _observers.begin();
   while (iter != _observers.end())
   {
      if (*iter != NULL)
      {
         (*iter)->vSDSStatusChanged();
      }
      ++iter;
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_SDSStatus::vSDSStatusChanged(enSDSStatus oStatus)
{
   _oSDSStatus = oStatus;
   vNotifyObservers();
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_SDSStatus::bIsIdle() const
{
   if (clSDS_SDSStatus::EN_IDLE == _oSDSStatus)
   {
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_SDSStatus::bIsError() const
{
   if (clSDS_SDSStatus::EN_ERROR == _oSDSStatus)
   {
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_SDSStatus::bIsLoading() const
{
   if (clSDS_SDSStatus::EN_LOADING == _oSDSStatus)
   {
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_SDSStatus::bIsDialogOpen() const
{
   if (clSDS_SDSStatus::EN_DIALOGOPEN == _oSDSStatus)
   {
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_SDSStatus::bIsListening() const
{
   if (clSDS_SDSStatus::EN_LISTENING == _oSDSStatus)
   {
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_SDSStatus::bIsActive() const
{
   if (clSDS_SDSStatus::EN_ACTIVE == _oSDSStatus)
   {
      return TRUE;
   }
   return FALSE;
}


clSDS_SDSStatus::enSDSStatus clSDS_SDSStatus::getStatus() const
{
   return _oSDSStatus;
}
