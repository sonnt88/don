/*********************************************************************//**
 * \file       clSDS_TextMsgContent.cpp
 *
 * \author   heg6kor
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "clSDS_TextMsgContent.h"

std::string clSDS_TextMsgContent::_textContent;
std::string clSDS_TextMsgContent::_phoneNumber;
/**************************************************************************//**
 *Destructor
 ******************************************************************************/
clSDS_TextMsgContent::~clSDS_TextMsgContent()
{
}


/**************************************************************************//**
 *Constructor
 ******************************************************************************/
clSDS_TextMsgContent::clSDS_TextMsgContent()
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_TextMsgContent::setTextMsgContent(const std::string msgContent)
{
   _textContent = msgContent;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_TextMsgContent::getTextMsgContent()
{
   return _textContent;
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_TextMsgContent::setPhoneNumber(const std::string phoneNumber)
{
   _phoneNumber = phoneNumber;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_TextMsgContent::getPhoneNumber()
{
   return _phoneNumber;
}
