/*********************************************************************//**
 * \file       clSDS_Method_PhoneSendDTMFDigits.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_PhoneSendDTMFDigits_h
#define clSDS_Method_PhoneSendDTMFDigits_h


#include "MOST_Tel_FIProxy.h"
#include "Sds2HmiServer/framework/clServerMethod.h"


class clSDS_Method_PhoneSendDTMFDigits
   : public clServerMethod
   , public MOST_Tel_FI::SendDTMFCallbackIF
{
   public:
      virtual ~clSDS_Method_PhoneSendDTMFDigits();
      clSDS_Method_PhoneSendDTMFDigits(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > pSds2TelProxy);

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onSendDTMFError(
         const ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< ::MOST_Tel_FI::SendDTMFError >& error);
      virtual void onSendDTMFResult(
         const ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< ::MOST_Tel_FI::SendDTMFResult >& result);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy > _telephoneProxy;
};


#endif
