/*********************************************************************//**
 * \file       clSDS_Method_MediaGetAmbiguityList.cpp
 *
 * clSDS_Method_MediaGetAmbiguityList method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_MediaGetAmbiguityList.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_MediaGetAmbiguityList.cpp.trc.h"
#endif


using namespace mplay_MediaPlayer_FI;
using namespace MPlay_fi_types;


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_MediaGetAmbiguityList::~clSDS_Method_MediaGetAmbiguityList()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_MediaGetAmbiguityList::clSDS_Method_MediaGetAmbiguityList(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy > mediaPlayerProxy)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_MEDIAGETAMBIGUITYLIST, pService)
   , _mediaPlayerProxy(mediaPlayerProxy)
   , _e8DeviceType(T_e8_MPlayDeviceType__e8DTY_UNKNOWN)
   , _tagID(0)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_MediaGetAmbiguityList::onGetMediaObjectError(
   const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< mplay_MediaPlayer_FI::GetMediaObjectError >& /*error*/)
{
   _mediaIds.erase(_mediaIds.begin());
   if (_mediaIds.empty())
   {
      vSendResult();
   }
   else
   {
      vRequestMediaMetadata();
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_MediaGetAmbiguityList::onGetMediaObjectResult(
   const ::boost::shared_ptr<mplay_MediaPlayer_FI:: Mplay_MediaPlayer_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< mplay_MediaPlayer_FI::GetMediaObjectResult >& result)
{
   _mediaIds.erase(_mediaIds.begin());
   const T_MPlayMediaObject& oMediaObject = result->getOMediaObject();
   T_e8_MPlayCategoryType e8CategoryType = oMediaObject.getE8CategoryType();
   _artist = oMediaObject.getSMetaDataField2();
   _album  = oMediaObject.getSMetaDataField3();
   _song = oMediaObject.getSMetaDataField4();
   _e8DeviceType = oMediaObject.getE8DeviceType();
   _tagID = oMediaObject.getU32Tag();
   vProcessMessageByCategory(e8CategoryType, oMediaObject.getBIsPlaying());
}


/**************************************************************************//**
*
******************************************************************************/
unsigned int clSDS_Method_MediaGetAmbiguityList::vGetTagID() const
{
   return _tagID;
}


/**************************************************************************//**
*
******************************************************************************/
T_e8_MPlayDeviceType clSDS_Method_MediaGetAmbiguityList::vGetE8DeviceType() const
{
   return _e8DeviceType;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_MediaGetAmbiguityList::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgMediaGetAmbiguityListMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   _stringIdPairs.clear();
   _mediaIds = oMessage.ValueIDs;
   if (!oMessage.ValueIDs.empty())
   {
      vRequestMediaMetadata();
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_MediaGetAmbiguityList::vRequestMediaMetadata()
{
   if (_mediaPlayerProxy->isAvailable())
   {
      _mediaPlayerProxy->sendGetMediaObjectStart(
         *this,
         _mediaIds.front().ID,
         ::MPlay_fi_types::T_e8_MPlayCategoryType__e8CTY_NONE);
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_MediaGetAmbiguityList::vSendResult()
{
   // TODO jnd2hi: seems that code is only working for a single media id - what if list contains multiple ids?? LCN2kai code used to handle that
   sds2hmi_sdsfi_tclMsgMediaGetAmbiguityListMethodResult oResult;
   oResult.AmbiguousEntries = _stringIdPairs;

   if (oResult.AmbiguousEntries.size() > 0)
   {
      vSendMethodResult(oResult);
      vTraceMediaAmbiguityResult();
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_MEDIAOBJNOTAVAILABLE);
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_MediaGetAmbiguityList::vProcessMessageByCategory(T_e8_MPlayCategoryType category, tBool bIsMediaObjectValid)
{
   if (bIsMediaObjectValid)
   {
      switch (category)
      {
         case T_e8_MPlayCategoryType__e8CTY_SONG:
            vProcessSongData();
            break;

         case T_e8_MPlayCategoryType__e8CTY_ALBUM:
            vProcessAlbumData();
            break;

         case T_e8_MPlayCategoryType__e8CTY_ARTIST:
            vProcessArtistData();
            break;

         default:
            vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
            break;
      }
   }
   else
   {
      vSendResult();
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_MediaGetAmbiguityList::vProcessSongData()
{
   _song = _song + "," + _artist + "," + _album;
   vStoreAmbigInfo(_song);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_MediaGetAmbiguityList::vProcessAlbumData()
{
   _album = _album + "," + _artist;
   vStoreAmbigInfo(_album);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_MediaGetAmbiguityList::vProcessArtistData()
{
   vStoreAmbigInfo(_artist);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_MediaGetAmbiguityList::vStoreAmbigInfo(const bpstl::string& oMediaInfo)
{
   sds2hmi_fi_tcl_MediaIDString oStringIdPair;
   oStringIdPair.ID = vGetTagID();
   oStringIdPair.MediaType.enType = sds2hmi_fi_tcl_e8_MediaType::FI_EN_MEDIAOBJECTID;
   oStringIdPair.String.bSet(oMediaInfo.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   _stringIdPairs.push_back(oStringIdPair);
   if (_mediaIds.empty())
   {
      vSendResult();
   }
   else
   {
      vRequestMediaMetadata();
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_MediaGetAmbiguityList::vTraceMediaAmbiguityResult()
{
   for (tU32 u32index = 0; u32index < _stringIdPairs.size(); u32index++)
   {
      ETG_TRACE_USR1(("Media Id 0x%x MediaString %s", _stringIdPairs[u32index].ID, _stringIdPairs[u32index].String.szValue));
   }
}
