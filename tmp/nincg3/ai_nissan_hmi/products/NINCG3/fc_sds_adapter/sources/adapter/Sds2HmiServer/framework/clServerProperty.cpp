/*********************************************************************//**
 * \file       clServerProperty.cpp
 *
 * Base class for CCA property implementations.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/framework/clServerProperty.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_CCA_FW
#include "trcGenProj/Header/clServerProperty.cpp.trc.h"
#endif


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clServerProperty::~clServerProperty()
{
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clServerProperty::clServerProperty(tU16 u16FunctionID, ahl_tclBaseOneThreadService* pService)
   : clFunction(u16FunctionID, pService)
   , _u16DestAppID(AMT_C_U32_BASEMSG_SOURCEAPPID)
   , _u16RegisterID(0)
   , _u16RequestCmdCtr(0)
   , _u32RequestType(AMT_C_U8_CCAMSG_OPCODE_UPREG)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clServerProperty::vHandleMessage(amt_tclServiceData* pInMsg)
{
   if (pInMsg != NULL)
   {
      ETG_TRACE_USR1(("rx appid=%04x function=%04x regid=%d opcode=%d cmdctr=%d ", pInMsg->u16GetSourceAppID(), pInMsg->u16GetFunctionID(), pInMsg->u16GetRegisterID(), pInMsg->u8GetOpCode(), pInMsg->u16GetCmdCounter()));
      switch (pInMsg->u8GetOpCode())
      {
         case AMT_C_U8_CCAMSG_OPCODE_SET:
            vSaveResponseParameters(pInMsg);
            vSet(pInMsg);
            break;

         case AMT_C_U8_CCAMSG_OPCODE_GET:
            vSaveResponseParameters(pInMsg);
            vGet(pInMsg);
            break;

         case AMT_C_U8_CCAMSG_OPCODE_UPREG:
            vAddToNotificationTable(pInMsg);
            vUpreg(pInMsg);
            break;

         case AMT_C_U8_CCAMSG_OPCODE_RELUPREG:
            vRemoveFromNotificationTable(pInMsg);
            break;

         case AMT_C_U8_CCAMSG_OPCODE_ERROR:
            // TODO: jnd2hi activate code again
            //            _pService->vSendErrorMessage(
            //               pInMsg->u16GetFunctionID(),
            //               _pService->_u16InvalidOpCodeError);
            break;

         default:
            break;
      }
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clServerProperty::vAddToNotificationTable(const amt_tclServiceData* pInMessage)
{
   if (pInMessage != NULL)
   {
      (tVoid)_notificationTable.bAddNotification(
         pInMessage->u16GetFunctionID(),
         pInMessage->u16GetSourceAppID(),
         pInMessage->u16GetRegisterID(),
         TRUE,
         pInMessage->u16GetCmdCounter(),
         pInMessage->u16GetSourceSubID());
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clServerProperty::vRemoveFromNotificationTable(const amt_tclServiceData* pInMessage)
{
   if (pInMessage != NULL)
   {
      (tVoid)_notificationTable.bRemoveNotification(
         pInMessage->u16GetFunctionID(),
         pInMessage->u16GetSourceAppID(),
         pInMessage->u16GetRegisterID(),
         TRUE,
         pInMessage->u16GetCmdCounter(),
         pInMessage->u16GetSourceSubID());
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clServerProperty::vSaveResponseParameters(const amt_tclServiceData* pInMessage)
{
   if (pInMessage != NULL)
   {
      _u16DestAppID = pInMessage->u16GetSourceAppID();
      _u16RegisterID = pInMessage->u16GetRegisterID();
      _u32RequestType = pInMessage->u8GetOpCode();
      _u16RequestCmdCtr = pInMessage->u16GetCmdCounter();
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clServerProperty::vStatus(const fi_tclTypeBase& oOutData)
{
   if (_u32RequestType == AMT_C_U8_CCAMSG_OPCODE_GET)
   {
      vSendMessage(
         _u16DestAppID,
         _u16RequestCmdCtr,
         _u16RegisterID,
         oOutData,
         u16GetFunctionID(),
         AMT_C_U8_CCAMSG_OPCODE_STATUS);
      _u32RequestType = AMT_C_U8_CCAMSG_OPCODE_UPREG;
   }
   else if (_u32RequestType == AMT_C_U8_CCAMSG_OPCODE_SET)
   {
      vSendMessage(
         _u16DestAppID,
         _u16RequestCmdCtr,
         _u16RegisterID,
         oOutData,
         u16GetFunctionID(),
         AMT_C_U8_CCAMSG_OPCODE_STATUS);
      _u32RequestType = AMT_C_U8_CCAMSG_OPCODE_UPREG;
      vStatusToRegisteredClients(oOutData);
   }
   else
   {
      vStatusToRegisteredClients(oOutData);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clServerProperty::vStatusToRegisteredClients(const fi_tclTypeBase& oOutData)
{
   ahl_tNotification* pNotify = _notificationTable.poGetNotificationList(u16GetFunctionID());
   for (; pNotify != OSAL_NULL; pNotify = pNotify->pNext)
   {
      vSendMessage(
         pNotify->u16AppID,
         pNotify->u16CmdCounter,
         pNotify->u16RegisterID,
         oOutData,
         u16GetFunctionID(),
         AMT_C_U8_CCAMSG_OPCODE_STATUS);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clServerProperty::vSet(amt_tclServiceData* /*pInMsg*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clServerProperty::vUpreg(amt_tclServiceData* /*pInMsg*/)
{
}
