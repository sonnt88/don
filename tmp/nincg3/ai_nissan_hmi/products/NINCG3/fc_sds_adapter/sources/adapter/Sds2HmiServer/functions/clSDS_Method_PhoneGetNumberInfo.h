/*********************************************************************//**
 * \file       clSDS_Method_PhoneGetNumberInfo.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_PhoneGetNumberInfo_h
#define clSDS_Method_PhoneGetNumberInfo_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "MOST_PhonBk_FIProxy.h"

class clSDS_Userwords;


class clSDS_Method_PhoneGetNumberInfo
   : public clServerMethod
   , public MOST_PhonBk_FI::GetContactDetailsExtendedCallbackIF
{
   public:
      virtual ~clSDS_Method_PhoneGetNumberInfo();
      clSDS_Method_PhoneGetNumberInfo(ahl_tclBaseOneThreadService* pService, clSDS_Userwords* pUserword,
                                      boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy > pPhoneBkProxy);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      tVoid vGetCallerNameAndNumberForUserword(uint32 userwordID);

      virtual void onGetContactDetailsExtendedError(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedError >& error);
      virtual void onGetContactDetailsExtendedResult(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedResult >& result);

      clSDS_Userwords* _pUserwordsNameAndNumberInfo;
      boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy > _phoneBookProxy;
};


#endif
