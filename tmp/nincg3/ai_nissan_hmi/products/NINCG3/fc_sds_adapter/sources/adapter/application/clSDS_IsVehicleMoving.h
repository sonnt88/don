/*********************************************************************//**
 * \file       clSDS_IsVehicleMoving.h
 *
 * List Phonebook functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_IsVehicleMoving_h
#define clSDS_IsVehicleMoving_h


#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"


class clSDS_IsVehicleMovingObserver;


class clSDS_IsVehicleMoving
{
   public:
      virtual ~clSDS_IsVehicleMoving();
      clSDS_IsVehicleMoving();
      tVoid vRegisterObserver(clSDS_IsVehicleMovingObserver* pObserver);
      tVoid vOnVehicleSpeedChange();

   private:
      tVoid vNotifyObservers();
      bpstl::vector<clSDS_IsVehicleMovingObserver*> _observers;
};


#endif
