/*********************************************************************//**
 * \file       clSDS_Property_TextMsgStatus.cpp
 *
 * Common Action Request property implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Property_TextMsgStatus.h"
#include "external/sds2hmi_fi.h"

using namespace MOST_Msg_FI;
using namespace most_Msg_fi_types;


/**
 * See also https://hi-cmts.apps.intranet.bosch.com:8443/browse/CMG3G-7698
 */
#define MSG_SMS_SUPPORT          ((uint16)0x0001u)
#define MSG_SMS_SEND_SUPPORT     ((uint16)0x0001u)


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Property_TextMsgStatus::~clSDS_Property_TextMsgStatus()
{
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Property_TextMsgStatus::clSDS_Property_TextMsgStatus(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr< ::MOST_Msg_FI::MOST_Msg_FIProxy > msgProxy)

   : clServerProperty(SDS2HMI_SDSFI_C_U16_TEXTMSGSTATUS, pService)
   , _msgProxy(msgProxy)
   , _messagesAvailable(FALSE)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_TextMsgStatus::vSet(amt_tclServiceData* /* pInMsg */)
{
   vSendTextMsgStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_TextMsgStatus::vGet(amt_tclServiceData* /* pInMsg */)
{
   vSendTextMsgStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_TextMsgStatus::vUpreg(amt_tclServiceData* /* pInMsg */)
{
   vSendTextMsgStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_TextMsgStatus::vSendTextMsgStatus()
{
   sds2hmi_sdsfi_tclMsgTextMsgStatusStatus oMessage;
   oMessage.Status = _smsStatus;
   oMessage.IsMsgAvailable = _messagesAvailable;
   vStatus(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_TextMsgStatus::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _msgProxy)
   {
      _msgProxy->sendMapDeviceCapabilitiesUpReg(*this);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_TextMsgStatus::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _msgProxy)
   {
      _msgProxy->sendMapDeviceCapabilitiesRelUpRegAll();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_TextMsgStatus::onMapDeviceCapabilitiesError(
   const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_Msg_FI::MapDeviceCapabilitiesError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_TextMsgStatus::onMapDeviceCapabilitiesStatus(
   const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_Msg_FI::MapDeviceCapabilitiesStatus >& status)
{
   _smsStatus.enType = getSMSProfileStatus(status->getU16SupportedMessageTypes(), status->getU16SupportedMapFeatures());
   vSendTextMsgStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
static bool flagIsSet(uint32 value, uint32 flag)
{
   return ((value & flag) != 0);
}


/**************************************************************************//**
 *
 ******************************************************************************/
sds2hmi_fi_tcl_e8_SMS_Status::tenType clSDS_Property_TextMsgStatus::getSMSProfileStatus(uint16 messageTypes, uint16 mapFeatures) const
{
   if (flagIsSet(messageTypes, MSG_SMS_SUPPORT))
   {
      if (flagIsSet(mapFeatures, MSG_SMS_SEND_SUPPORT))
      {
         return sds2hmi_fi_tcl_e8_SMS_Status::FI_EN_READ_SEND;
      }
      return sds2hmi_fi_tcl_e8_SMS_Status::FI_EN_READ_ONLY;
   }
   else
   {
      return sds2hmi_fi_tcl_e8_SMS_Status::FI_EN_DISABLED;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_TextMsgStatus::textListChanged(unsigned int size)
{
   _messagesAvailable = (size > 0);
   vSendTextMsgStatus();
}
