/*********************************************************************//**
 * \file       Sds2HmiServer.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/TraceCommandAdapter.h"
#include "Sds2HmiServer/Sds2HmiServer.h"
#include "Sds2HmiServer/framework/AhlApp.h"
#include "Sds2HmiServer/framework/AhlService.h"
// sds2hmi_fi methods
#include "Sds2HmiServer/functions/clSDS_Method_AppsLaunchApplication.h"
#include "Sds2HmiServer/functions/clSDS_Method_CommonGetHmiElementDescription.h"
#include "Sds2HmiServer/functions/clSDS_Method_CommonGetHmiListDescription.h"
#include "Sds2HmiServer/functions/clSDS_Method_CommonGetListInfo.h"
#include "Sds2HmiServer/functions/clSDS_Method_CommonInteractionLogger.h"
#include "Sds2HmiServer/functions/clSDS_Method_CommonSelectListElement.h"
#include "Sds2HmiServer/functions/clSDS_Method_CommonSetActiveApplication.h"
#include "Sds2HmiServer/functions/clSDS_Method_CommonSetAvailableSpeakers.h"
#include "Sds2HmiServer/functions/clSDS_Method_CommonSetAvailableUserwords.h"
#include "Sds2HmiServer/functions/clSDS_Method_CommonShowDialog.h"
#include "Sds2HmiServer/functions/clSDS_Method_CommonStartSession.h"
#include "Sds2HmiServer/functions/clSDS_Method_CommonStopSession.h"
#include "Sds2HmiServer/functions/clSDS_Method_ContactsGetAmbiguityList.h"
#include "Sds2HmiServer/functions/clSDS_Method_InfoShowMenu.h"
#include "Sds2HmiServer/functions/clSDS_Method_MediaGetAmbiguityList.h"
#include "Sds2HmiServer/functions/clSDS_Method_MediaGetDataBase.h"
#include "Sds2HmiServer/functions/clSDS_Method_MediaGetDeviceInfo.h"
#include "Sds2HmiServer/functions/clSDS_Method_MediaPlay.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviGetAmbiguityList.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviGetContactListEntries.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviGetCurrentDestination.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviGetHouseNumberRange.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviGetWaypointListInfo.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviRepeatInstruction.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviSelectDestListEntry.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviSetDestinationAsWaypoint.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviSetDestinationItem.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviSetMapMode.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviSetZoomSetting.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviShowNavMenu.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviStartGuidance.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviStopGuidance.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviSetRouteCriteria.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviSetNavSetting.h"
#include "Sds2HmiServer/functions/clSDS_Method_PhoneDialContact.h"
#include "Sds2HmiServer/functions/clSDS_Method_PhoneDialNumber.h"
#include "Sds2HmiServer/functions/clSDS_Method_PhoneGetContactListEntries.h"
#include "Sds2HmiServer/functions/clSDS_Method_PhoneGetDataBases.h"
#include "Sds2HmiServer/functions/clSDS_Method_PhoneGetNumberInfo.h"
#include "Sds2HmiServer/functions/clSDS_Method_PhoneRedialLastNumber.h"
#include "Sds2HmiServer/functions/clSDS_Method_PhoneSelectDevice.h"
#include "Sds2HmiServer/functions/clSDS_Method_PhoneSendDTMFDigits.h"
#include "Sds2HmiServer/functions/clSDS_Method_PhoneSetContact.h"
#include "Sds2HmiServer/functions/clSDS_Method_PhoneSetPhoneSetting.h"
#include "Sds2HmiServer/functions/clSDS_Method_PhoneShowMenu.h"
#include "Sds2HmiServer/functions/clSDS_Method_PhoneStartPairing.h"
#include "Sds2HmiServer/functions/clSDS_Method_PhoneSwitchCall.h"
#include "Sds2HmiServer/functions/clSDS_Method_TextMsgCallbackSender.h"
#include "Sds2HmiServer/functions/clSDS_Method_TextMsgGetContent.h"
#include "Sds2HmiServer/functions/clSDS_Method_TextMsgGetInfo.h"
#include "Sds2HmiServer/functions/clSDS_Method_TextMsgSend.h"
#include "Sds2HmiServer/functions/clSDS_Method_TextMsgSetContent.h"
#include "Sds2HmiServer/functions/clSDS_Method_TextMsgSetNumber.h"
#include "Sds2HmiServer/functions/clSDS_Method_TextMsgSelectMessage.h"
#include "Sds2HmiServer/functions/clSDS_Method_TunerGetDataBases.h"
#include "Sds2HmiServer/functions/clSDS_Method_TunerSelectBandMemBank.h"
#include "Sds2HmiServer/functions/clSDS_Method_TunerSelectStation.h"
#include "Sds2HmiServer/functions/clSDS_Method_VDLGetDatabases.h"


// sds2hmi_fi properties
#include "Sds2HmiServer/functions/clSDS_Property_ActiveSpeaker.h"
#include "Sds2HmiServer/functions/clSDS_Property_CommonActionRequest.h"
#include "Sds2HmiServer/functions/clSDS_Property_CommonSDSConfiguration.h"
#include "Sds2HmiServer/functions/clSDS_Property_CommonCoreSpeechParameters.h"
#include "Sds2HmiServer/functions/clSDS_Property_CommonSettingsRequest.h"
#include "Sds2HmiServer/functions/clSDS_Property_CommonStatus.h"
#include "Sds2HmiServer/functions/clSDS_Property_ConnectedDeviceStatus.h"
#include "Sds2HmiServer/functions/clSDS_Property_MediaStatus.h"
#include "Sds2HmiServer/functions/clSDS_Property_NaviCurrentCountryState.h"
#include "Sds2HmiServer/functions/clSDS_Property_NaviStatus.h"
#include "Sds2HmiServer/functions/clSDS_Property_PhoneStatus.h"
#include "Sds2HmiServer/functions/clSDS_Property_SdsStatus.h"
#include "Sds2HmiServer/functions/clSDS_Property_SpecialAppStatus.h"
#include "Sds2HmiServer/functions/clSDS_Property_TextMsgStatus.h"
#include "Sds2HmiServer/functions/clSDS_Property_TunerStatus.h"
#include "Sds2HmiServer/functions/clSDS_Property_VDLStatus.h"
#include "Sds2HmiServer/functions/clSDS_Property_WeatherStatus.h"
// application
#include "application/AcrHandler.h"
#include "application/AudioSourceHandler.h"
#include "application/EarlyStartupPlayer.h"
#include "application/clSDS_AddressBookList.h"
#include "application/clSDS_AmbigContactList.h"
#include "application/clSDS_AmbigNumberList.h"
#include "application/clSDS_CustomSMSList.h"
#include "application/clSDS_ContactNumberList.h"
#include "application/clSDS_Display.h"
#include "application/clSDS_FmChannelList.h"
#include "application/clSDS_FormatTimeDate.h"
#include "application/clSDS_FuelPriceList.h"
#include "application/clSDS_G2P_FactorySettings.h"
#include "application/clSDS_IsVehicleMoving.h"
#include "application/clSDS_LanguageMediator.h"
#include "application/clSDS_ListScreen.h"
#include "application/clSDS_MenuManager.h"
#include "application/clSDS_MultipleDestinationsList.h"
#include "application/clSDS_MyAppsDataBase.h"
#include "application/clSDS_MyAppsList.h"
#include "application/clSDS_NBestList.h"
#include "application/clSDS_NaviListItems.h"
#include "application/clSDS_PhonebookList.h"
#include "application/clSDS_POIList.h"
#include "application/clSDS_PreviousDestinationsList.h"
#include "application/clSDS_QuickDialList.h"
#include "application/clSDS_ReadSmsList.h"
#include "application/clSDS_RecentCallsList.h"
#include "application/clSDS_SDSStatus.h"
#include "application/clSDS_SessionControl.h"
#include "application/clSDS_SdsControl.h"
#include "application/clSDS_SXMAudioChannelList.h"
#include "application/clSDS_TunerBandRange.h"
#include "application/clSDS_Userwords.h"
#include "application/GuiService.h"
#include "application/PopUpService.h"
#include "application/SettingsService.h"
#include "application/SdsAudioSource.h"
#include "application/SdsPhoneService.h"
#include "external/sds2hmi_fi.h"
#include "view_db/Sds_ViewDB.h"

#define SCD_S_IMPORT_INTERFACE_GENERIC
#include "scd_if.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/Sds2HmiServer.cpp.trc.h"
#endif


#define SAFE_CREATE(pointerVar, constructorCall)       pointerVar = new constructorCall; if (!pointerVar) return service

#define DELETE_AND_PURGE(member)         if (member) delete member; member = NULL


using namespace asf::core;
using namespace bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange;
using namespace MOST_BTSet_FI;
using namespace MOST_Msg_FI;
using namespace MOST_PhonBk_FI;
using namespace MOST_Tel_FI;
using namespace sxm_audio_main_fi;
using namespace MIDW_EXT_SXM_FUEL_FI;
using namespace MIDW_EXT_SXM_CANADIAN_FUEL_FI;
using namespace MIDW_EXT_SXM_PHONETICS_FI;
using namespace tcu_main_fi;
using namespace tuner_main_fi;
using namespace tunermaster_main_fi;
using namespace mplay_MediaPlayer_FI;
using namespace org::bosch::cm::navigation::NavigationService;
using namespace VEHICLE_MAIN_FI;
using namespace clock_main_fi;
using namespace CMB_ACR_FI;
using namespace smartphoneint_main_fi;

Sds2HmiServer::Sds2HmiServer()
   : _pAudioSourceHandler(NULL)
   , _pAddressBookList(NULL)
   , _pAmbigContactList(NULL)
   , _pAmbigNumberList(NULL)
   , _pContactNumberList(NULL)
   , _pCustomSMSList(NULL)
   , _pDisplay(NULL)
   , _pEarlyStartupPlayer(NULL)
   , _pFmChannelList(NULL)
   , _pFormatTimeDate(NULL)
   , _pFuelPriceList(NULL)
   , _pG2P_FactorySetttings(NULL)
   , _pIsVehicleMoving(NULL)
   , _pLanguageMediator(NULL)
   , _pListScreen(NULL)
   , _pMenuManager(NULL)
   , _pMultipleDestinationList(NULL)
   , _pMyAppsDataBase(NULL)
   , _pNaviListItems(NULL)
   , _pNBestList(NULL)
   , _pPhonebookList(NULL)
   , _pPOIList(NULL)
   , _pPreviousDestinationsList(NULL)
   , _pQuickDialList(NULL)
   , _pReadSmsList(NULL)
   , _pRecentCallsList(NULL)
   , _pSDSStatus(NULL)
   , _pSessionControl(NULL)
   , _pSxmAudioChannelList(NULL)
   , _pTunerBandRange(NULL)
   , _pUserwords(NULL)
   , _pAcrHandler(NULL)

   , _pGuiService(NULL)
   , _pPopUpService(NULL)
   , _pSdsAudioSource(NULL)
   , _pSdsPhoneService(NULL)
   , _pSettingsService(NULL)
   , _pTraceCommandAdapter(NULL)

   , _pMethodAppsLaunchApplication(NULL)
   , _pMethodCommonGetHmiListDescription(NULL)
   , _pMethodCommonGetListInfo(NULL)
   , _pMethodCommonSetActiveApplication(NULL)
   , _pMethodCommonSetAvailableUserwords(NULL)
   , _pMethodCommonShowDialog(NULL)
   , _pMethodContactsGetAmbiguityList(NULL)
   , _pMethodMediaGetAmbiguityList(NULL)
   , _pMethodMediaGetDeviceInfo(NULL)
   , _pMethodMediaPlay(NULL)
   , _pMethodNaviGetWaypointListInfo(NULL)
   , _pMethodNaviSelectDestListEntry(NULL)
   , _pMethodNaviSetDestinationAsWaypoint(NULL)
   , _pMethodNaviSetDestinationItem(NULL)
   , _pMethodNaviSetMapMode(NULL)
   , _pMethodPhoneDialContact(NULL)
   , _pMethodPhoneDialNumber(NULL)
   , _pMethodPhoneGetContactListEntries(NULL)
   , _pMethodPhoneRedialLastNumber(NULL)
   , _pMethodPhoneSetPhoneSetting(NULL)
   , _pMethodPhoneStartPairing(NULL)
   , _pMethodPhoneSwitchCall(NULL)
   , _pMethodTextMsgCallbackSender(NULL)
   , _pMethodTextMsgGetContent(NULL)
   , _pMethodTextMsgGetInfo(NULL)
   , _pMethodTextMsgSelectMessage(NULL)
   , _pMethodTextMsgSend(NULL)
   , _pMethodTextMsgSetContent(NULL)
   , _pMethodTextMsgSetNumber(NULL)
   , _pMethodTunerSelectBandMemBank(NULL)
   , _pMethodTunerSelectStation(NULL)

   , _pPropertyCommonActionRequest(NULL)
   , _pPropertyCommonCoreSpeechParameters(NULL)
   , _pPropertyCommonSDSConfiguration(NULL)
   , _pPropertyCommonSettingsRequest(NULL)
   , _pPropertyCommonStatus(NULL)
   , _pPropertyConnectedDeviceStatus(NULL)
   , _pPropertyMediaStatus(NULL)
   , _pPropertyNaviCurrentCountryState(NULL)
   , _pPropertyNaviStatus(NULL)
   , _pPropertyPhoneStatus(NULL)
   , _pPropertySpecialAppStatus(NULL)
   , _pPropertyTextMsgStatus(NULL)
   , _pPropertyTunerStatus(NULL)
{
   ETG_TRACE_USR1(("Sds2HmiServer::Constructor"));
   buildRegistry();
   amt_bInit();
   scd_init();
   createproxies();
   _pAhlApp = new AhlApp(CCA_C_U16_APP_SAALS);
   _pSds2HmiService = createSds2HmiService(_pAhlApp);
   registerServiceAvailableDelegates();

   if (_pAhlApp->bInitInstance(0, CCA_C_U16_APP_SAALS, OSAL_ThreadWhoAmI()) == FALSE)
   {
      ETG_TRACE_ERR(("!!! vStartCCAApplications(): Heat bInitInstance FAILED"));
   }
}


Sds2HmiServer::~Sds2HmiServer()
{
   try
   {
      DELETE_AND_PURGE(_pAudioSourceHandler);
      DELETE_AND_PURGE(_pAddressBookList);
      DELETE_AND_PURGE(_pAhlApp);
      DELETE_AND_PURGE(_pAmbigContactList);
      DELETE_AND_PURGE(_pAmbigNumberList);
      DELETE_AND_PURGE(_pContactNumberList);
      DELETE_AND_PURGE(_pCustomSMSList);
      DELETE_AND_PURGE(_pDisplay);
      DELETE_AND_PURGE(_pEarlyStartupPlayer);
      DELETE_AND_PURGE(_pFmChannelList);
      DELETE_AND_PURGE(_pFormatTimeDate);
      DELETE_AND_PURGE(_pFuelPriceList);
      DELETE_AND_PURGE(_pG2P_FactorySetttings);
      DELETE_AND_PURGE(_pGuiService);
      DELETE_AND_PURGE(_pIsVehicleMoving);
      DELETE_AND_PURGE(_pLanguageMediator);
      DELETE_AND_PURGE(_pListScreen);
      DELETE_AND_PURGE(_pMenuManager);
      DELETE_AND_PURGE(_pMultipleDestinationList);
      DELETE_AND_PURGE(_pMyAppsDataBase);
      DELETE_AND_PURGE(_pNaviListItems);
      DELETE_AND_PURGE(_pNBestList);
      DELETE_AND_PURGE(_pPhonebookList);
      DELETE_AND_PURGE(_pPOIList);
      DELETE_AND_PURGE(_pPopUpService);
      DELETE_AND_PURGE(_pPreviousDestinationsList);
      DELETE_AND_PURGE(_pQuickDialList);
      DELETE_AND_PURGE(_pReadSmsList);
      DELETE_AND_PURGE(_pRecentCallsList);
      DELETE_AND_PURGE(_pSds2HmiService);
      DELETE_AND_PURGE(_pSdsAudioSource);
      DELETE_AND_PURGE(_pSdsPhoneService);
      DELETE_AND_PURGE(_pSDSStatus);
      DELETE_AND_PURGE(_pSessionControl);
      DELETE_AND_PURGE(_pSettingsService);
      DELETE_AND_PURGE(_pSxmAudioChannelList);
      DELETE_AND_PURGE(_pTraceCommandAdapter);
      DELETE_AND_PURGE(_pTunerBandRange);
      DELETE_AND_PURGE(_pUserwords);
      DELETE_AND_PURGE(_pAcrHandler);

      DELETE_AND_PURGE(_pMethodAppsLaunchApplication);
      DELETE_AND_PURGE(_pMethodCommonGetHmiListDescription);
      DELETE_AND_PURGE(_pMethodCommonGetListInfo);
      DELETE_AND_PURGE(_pMethodCommonSetActiveApplication);
      DELETE_AND_PURGE(_pMethodCommonSetAvailableUserwords);
      DELETE_AND_PURGE(_pMethodCommonShowDialog);
      DELETE_AND_PURGE(_pMethodContactsGetAmbiguityList);
      DELETE_AND_PURGE(_pMethodMediaGetAmbiguityList);
      DELETE_AND_PURGE(_pMethodMediaGetDeviceInfo);
      DELETE_AND_PURGE(_pMethodMediaPlay);
      DELETE_AND_PURGE(_pMethodNaviGetWaypointListInfo);
      DELETE_AND_PURGE(_pMethodNaviSelectDestListEntry);
      DELETE_AND_PURGE(_pMethodNaviSetDestinationAsWaypoint);
      DELETE_AND_PURGE(_pMethodNaviSetDestinationItem);
      DELETE_AND_PURGE(_pMethodNaviSetMapMode);
      DELETE_AND_PURGE(_pMethodPhoneDialContact);
      DELETE_AND_PURGE(_pMethodPhoneDialNumber);
      DELETE_AND_PURGE(_pMethodPhoneGetContactListEntries);
      DELETE_AND_PURGE(_pMethodPhoneRedialLastNumber);
      DELETE_AND_PURGE(_pMethodPhoneSetPhoneSetting);
      DELETE_AND_PURGE(_pMethodPhoneStartPairing);
      DELETE_AND_PURGE(_pMethodPhoneSwitchCall);
      DELETE_AND_PURGE(_pMethodTextMsgCallbackSender);
      DELETE_AND_PURGE(_pMethodTextMsgGetContent);
      DELETE_AND_PURGE(_pMethodTextMsgGetInfo);
      DELETE_AND_PURGE(_pMethodTextMsgSelectMessage);
      DELETE_AND_PURGE(_pMethodTextMsgSend);
      DELETE_AND_PURGE(_pMethodTextMsgSetContent);
      DELETE_AND_PURGE(_pMethodTextMsgSetNumber);
      DELETE_AND_PURGE(_pMethodTunerSelectBandMemBank);
      DELETE_AND_PURGE(_pMethodTunerSelectStation);

      DELETE_AND_PURGE(_pPropertyCommonActionRequest);
      DELETE_AND_PURGE(_pPropertyCommonCoreSpeechParameters);
      DELETE_AND_PURGE(_pPropertyCommonSDSConfiguration);
      DELETE_AND_PURGE(_pPropertyCommonSettingsRequest);
      DELETE_AND_PURGE(_pPropertyCommonStatus);
      DELETE_AND_PURGE(_pPropertyConnectedDeviceStatus);
      DELETE_AND_PURGE(_pPropertyMediaStatus);
      DELETE_AND_PURGE(_pPropertyNaviCurrentCountryState);
      DELETE_AND_PURGE(_pPropertyNaviStatus);
      DELETE_AND_PURGE(_pPropertyPhoneStatus);
      DELETE_AND_PURGE(_pPropertySpecialAppStatus);
      DELETE_AND_PURGE(_pPropertyTextMsgStatus);
      DELETE_AND_PURGE(_pPropertyTunerStatus);
   }
   catch (...)
   {
      ETG_TRACE_FATAL(("exception occurred in ~Sds2HmiServer()"));
   }
}


void Sds2HmiServer::buildRegistry() const
{
   OSAL_tIODescriptor regHandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_REGISTRY, OSAL_EN_READWRITE);
   if (regHandle != OSAL_ERROR)
   {
      const char regFileName[] = "/dev/root/opt/bosch/base/registry/sds_adapter.reg";
      if ((OSAL_s32IOControl(regHandle, OSAL_C_S32_IOCTRL_BUILD_REG, (tS32) regFileName)) == OSAL_OK)
      {
         ETG_TRACE_USR1(("REGISTRY loaded '%s'", regFileName));
      }
      else
      {
         ETG_TRACE_ERR(("FAILED to load REGISTRY '%s'", regFileName));
      }
      OSAL_s32IOClose(regHandle);
   }
}


// TODO jnd2hi extract creation of application objects from server
AhlService* Sds2HmiServer::createSds2HmiService(AhlApp* ahlApp)
{
   AhlService* service = new AhlService(ahlApp, CCA_C_U16_SRV_SAAL, SDS2HMI_SDSFI_C_U16_SERVICE_MAJORVERSION, SDS2HMI_SDSFI_C_U16_SERVICE_MINORVERSION);

   if (service != NULL)
   {
      SAFE_CREATE(_pPopUpService, PopUpService());
      _pSDSStatus = new clSDS_SDSStatus();
      _pMyAppsDataBase = new clSDS_MyAppsDataBase();
      SAFE_CREATE(_pSettingsService, SettingsService());
      SAFE_CREATE(_pLanguageMediator, clSDS_LanguageMediator(_pSDSStatus, *_pSettingsService, _vehicleProxy));
      _pPropertyCommonActionRequest = new clSDS_Property_CommonActionRequest(service, _pLanguageMediator);
      service->vAddFunction(_pPropertyCommonActionRequest);
      _pLanguageMediator->vSetSDSControl(_pPropertyCommonActionRequest);
      _pQuickDialList = new clSDS_QuickDialList(_phonebookProxy);
      _pUserwords = new clSDS_Userwords(_pSDSStatus, _pPropertyCommonActionRequest, _pQuickDialList);
      SAFE_CREATE(_pPropertyPhoneStatus, clSDS_Property_PhoneStatus(service, _bluetoothSettingsProxy, _telephoneProxy, _phonebookProxy,  _pUserwords));
      _pSessionControl = new clSDS_SessionControl(_pSDSStatus, _pPropertyPhoneStatus, _pPropertyCommonActionRequest);
      _pG2P_FactorySetttings = new clSDS_G2P_FactorySettings(_pPropertyCommonActionRequest);
      _pPropertyConnectedDeviceStatus = new clSDS_Property_ConnectedDeviceStatus(service);
      service->vAddFunction(_pPropertyConnectedDeviceStatus);
      service->vAddFunction(new clSDS_Property_ActiveSpeaker(service, _pLanguageMediator));
      _pIsVehicleMoving = new clSDS_IsVehicleMoving();
      _pPropertyCommonStatus = new clSDS_Property_CommonStatus(service, _pIsVehicleMoving, _vehicleProxy , _tcuProxy, _sxmAudioProxy);
      service->vAddFunction(_pPropertyCommonStatus);

      _pPropertyNaviCurrentCountryState = new clSDS_Property_NaviCurrentCountryState(service, _naviProxy);
      service->vAddFunction(_pPropertyNaviCurrentCountryState);

      service->vAddFunction(new clSDS_Property_SdsStatus(service, _pSDSStatus));

      _pPropertyMediaStatus = new clSDS_Property_MediaStatus(service, _mediaPlayerProxy, _smatphoneintProxy);
      service->vAddFunction(_pPropertyMediaStatus);
      _pPropertyNaviStatus = new clSDS_Property_NaviStatus(service , _naviProxy);
      service->vAddFunction(_pPropertyNaviStatus);

      service->vAddFunction(_pPropertyPhoneStatus);

      SAFE_CREATE(_pNaviListItems, clSDS_NaviListItems());

      SAFE_CREATE(_pSdsAudioSource, SdsAudioSource(ahlApp, _ecnrProxy, _audprocProxy, _pPopUpService));
      SAFE_CREATE(_pEarlyStartupPlayer, EarlyStartupPlayer(*_pSdsAudioSource));

      SAFE_CREATE(_pGuiService, GuiService(_pSessionControl, _pEarlyStartupPlayer));

      SAFE_CREATE(_pSdsPhoneService, SdsPhoneService(_pUserwords));

      _pTraceCommandAdapter = new TraceCommandAdapter(_pGuiService, _pPopUpService, _pEarlyStartupPlayer);

      _pPropertyCommonSDSConfiguration  = new clSDS_Property_CommonSDSConfiguration(service, _tcuProxy, _pSettingsService);
      service->vAddFunction(_pPropertyCommonSDSConfiguration);

      _pPropertyCommonSettingsRequest = new clSDS_Property_CommonSettingsRequest(service, _pSettingsService);
      service->vAddFunction(_pPropertyCommonSettingsRequest);

      _pPropertySpecialAppStatus = new clSDS_Property_SpecialAppStatus(service, _telephoneProxy);
      service->vAddFunction(_pPropertySpecialAppStatus);
      _pPropertyTunerStatus = new clSDS_Property_TunerStatus(service);
      service->vAddFunction(_pPropertyTunerStatus);
      service->vAddFunction(new clSDS_Property_VDLStatus(service));
      service->vAddFunction(new clSDS_Property_WeatherStatus(service));
      _pPropertyTextMsgStatus = new clSDS_Property_TextMsgStatus(service, _msgProxy);
      service->vAddFunction(_pPropertyTextMsgStatus);

      /* for SDS2HMI Methods */
      _pMethodAppsLaunchApplication = new clSDS_Method_AppsLaunchApplication(service, _telephoneProxy);
      service->vAddFunction(_pMethodAppsLaunchApplication);
      service->vAddFunction(new clSDS_Method_CommonInteractionLogger(service));
      service->vAddFunction(new clSDS_Method_CommonSetAvailableSpeakers(service, _pLanguageMediator));
      SAFE_CREATE(_pListScreen, clSDS_ListScreen());

      _pDisplay = new clSDS_Display(_pSDSStatus, *_pGuiService, *_pPopUpService);
      _pMenuManager = new clSDS_MenuManager(_pDisplay, _pPropertyCommonActionRequest);

      _pGuiService->setMenuControl(_pMenuManager);
      _pMethodCommonShowDialog = new clSDS_Method_CommonShowDialog(service, _pMenuManager, _pListScreen, _pPropertyPhoneStatus, _pGuiService, _pSdsPhoneService);
      service->vAddFunction(_pMethodCommonShowDialog);
      service->vAddFunction(new clSDS_Method_CommonStartSession(service, *_pSdsAudioSource, *_pGuiService));
      service->vAddFunction(new clSDS_Method_CommonStopSession(service, *_pSdsAudioSource, _pMenuManager, *_pGuiService));
      service->vAddFunction(new clSDS_Method_CommonSelectListElement(service, _pListScreen));

      _pPropertyCommonCoreSpeechParameters = new clSDS_Property_CommonCoreSpeechParameters(service, *_pGuiService);
      service->vAddFunction(_pPropertyCommonCoreSpeechParameters);

      _pMethodCommonSetAvailableUserwords = new clSDS_Method_CommonSetAvailableUserwords(service, _pUserwords, *_pSdsPhoneService, _pQuickDialList, _phonebookProxy);

      service->vAddFunction(_pMethodCommonSetAvailableUserwords);
      service->vAddFunction(new clSDS_Method_PhoneGetNumberInfo(service, _pUserwords, _phonebookProxy));
      _pMethodMediaGetDeviceInfo = new clSDS_Method_MediaGetDeviceInfo(service, _mediaPlayerProxy);
      service->vAddFunction(_pMethodMediaGetDeviceInfo);

      service->vAddFunction(new clSDS_Method_InfoShowMenu(service, *_pGuiService, _naviProxy));
      _pMethodMediaGetAmbiguityList = new clSDS_Method_MediaGetAmbiguityList(service, _mediaPlayerProxy);
      service->vAddFunction(_pMethodMediaGetAmbiguityList);
      _pMethodMediaPlay = new clSDS_Method_MediaPlay(service, _audioSourceChangeProxy, _mediaPlayerProxy, _pMethodMediaGetAmbiguityList, *_pGuiService, _pMethodMediaGetDeviceInfo);
      service->vAddFunction(_pMethodMediaPlay);

      service->vAddFunction(new clSDS_Method_MediaGetDataBase(service));
      service->vAddFunction(new clSDS_Method_NaviGetAmbiguityList(service));
      service->vAddFunction(new clSDS_Method_NaviGetContactListEntries(service));
      service->vAddFunction(new clSDS_Method_NaviGetCurrentDestination(service));

      SAFE_CREATE(_pMethodCommonGetListInfo, clSDS_Method_CommonGetListInfo(service, _pNaviListItems));
      service->vAddFunction(_pMethodCommonGetListInfo);

      _pMethodNaviSelectDestListEntry = new clSDS_Method_NaviSelectDestListEntry(service, _pNaviListItems, _naviProxy);
      service->vAddFunction(_pMethodNaviSelectDestListEntry);

      _pMethodNaviGetWaypointListInfo = new clSDS_Method_NaviGetWaypointListInfo(service, _naviProxy);
      service->vAddFunction(_pMethodNaviGetWaypointListInfo);

      _pMethodNaviSetDestinationAsWaypoint = new clSDS_Method_NaviSetDestinationAsWaypoint(service, _pMethodNaviGetWaypointListInfo, _naviProxy, *_pGuiService, _pNaviListItems);
      service->vAddFunction(_pMethodNaviSetDestinationAsWaypoint);

      _pMultipleDestinationList = new clSDS_MultipleDestinationsList(_naviProxy);
      _pPOIList = new clSDS_POIList();

      _pListScreen->vAddToList(SR_NAV_Ambig_List_Address, _pMultipleDestinationList);
      _pListScreen->vAddToList(SR_NAV_Ambig_List_CityCenter, _pMultipleDestinationList);
      _pListScreen->vAddToList(SR_NAV_Ambig_List_City, _pMultipleDestinationList);
      _pListScreen->vAddToList(SR_NAV_POI_List, _pPOIList);

      _pMethodNaviSetDestinationItem = new clSDS_Method_NaviSetDestinationItem(service, _naviProxy, _pMultipleDestinationList,
            _pPOIList, _pNaviListItems, _pMethodCommonShowDialog);
      service->vAddFunction(_pMethodNaviSetDestinationItem);
      _pMethodNaviSetMapMode = new clSDS_Method_NaviSetMapMode(service, _naviProxy, *_pGuiService);
      service->vAddFunction(_pMethodNaviSetMapMode);
      service->vAddFunction(new clSDS_Method_NaviStartGuidance(service , *_pGuiService, _pNaviListItems, _naviProxy));
      service->vAddFunction(new clSDS_Method_NaviStopGuidance(service, _naviProxy, *_pGuiService));
      service->vAddFunction(new clSDS_Method_NaviSetZoomSetting(service, _naviProxy, *_pGuiService));
      service->vAddFunction(new clSDS_Method_NaviSetRouteCriteria(service, _naviProxy, *_pGuiService));
      service->vAddFunction(new clSDS_Method_NaviSetNavSetting(service, _naviProxy, *_pGuiService));
      service->vAddFunction(new clSDS_Method_NaviShowNavMenu(service, *_pGuiService, _naviProxy));
      service->vAddFunction(new clSDS_Method_NaviRepeatInstruction(service, _naviProxy, *_pGuiService));
      service->vAddFunction(new clSDS_Method_NaviGetHouseNumberRange(service));
      service->vAddFunction(new clSDS_Method_PhoneGetDataBases(service));
      service->vAddFunction(new clSDS_Method_PhoneSelectDevice(service));
      service->vAddFunction(new clSDS_Method_PhoneShowMenu(service, *_pGuiService));
      _pMethodPhoneSetPhoneSetting = new clSDS_Method_PhoneSetPhoneSetting(service, _bluetoothSettingsProxy, _telephoneProxy , _pPropertyPhoneStatus, *_pGuiService);
      service->vAddFunction(_pMethodPhoneSetPhoneSetting);
      _pMethodPhoneStartPairing = new clSDS_Method_PhoneStartPairing(service, *_pGuiService);
      service->vAddFunction(_pMethodPhoneStartPairing);

      service->vAddFunction(new clSDS_Method_TunerGetDataBases(service));

      _pMethodTunerSelectBandMemBank = new clSDS_Method_TunerSelectBandMemBank(service, _audioSourceChangeProxy, *_pGuiService);
      service->vAddFunction(_pMethodTunerSelectBandMemBank);

      _pMethodPhoneSwitchCall = new clSDS_Method_PhoneSwitchCall(service, _telephoneProxy);
      service->vAddFunction(_pMethodPhoneSwitchCall);
      service->vAddFunction(new clSDS_Method_PhoneSendDTMFDigits(service, _telephoneProxy));
      _pMethodTextMsgSetContent = new clSDS_Method_TextMsgSetContent(service);
      service->vAddFunction(_pMethodTextMsgSetContent);
      service->vAddFunction(new clSDS_Method_VDLGetDatabases(service));
      _pPhonebookList = new clSDS_PhonebookList(_phonebookProxy);
      _pRecentCallsList = new clSDS_RecentCallsList(_phonebookProxy);
      _pCustomSMSList = new clSDS_CustomSMSList(_msgProxy);
      _pFormatTimeDate = new clSDS_FormatTimeDate(_clockProxy);
      SAFE_CREATE(_pReadSmsList, clSDS_ReadSmsList(_msgProxy, _clockProxy, _pFormatTimeDate));
      _pReadSmsList->setTextListObserver(_pPropertyTextMsgStatus);
      _pPropertyPhoneStatus->setPhoneStatusObserver(_pReadSmsList);
      _pPropertyPhoneStatus->setPhoneStatusObserver(_pRecentCallsList);
      _pMethodTextMsgGetInfo = new clSDS_Method_TextMsgGetInfo(service, _pReadSmsList);
      service->vAddFunction(_pMethodTextMsgGetInfo);
      _pMethodTextMsgSelectMessage = new clSDS_Method_TextMsgSelectMessage(service, _pReadSmsList);
      service->vAddFunction(_pMethodTextMsgSelectMessage);
      _pMethodTextMsgGetContent = new clSDS_Method_TextMsgGetContent(service, _msgProxy, _pReadSmsList, _pSdsPhoneService);
      service->vAddFunction(_pMethodTextMsgGetContent);
      _pMethodTextMsgCallbackSender = new clSDS_Method_TextMsgCallbackSender(service, _telephoneProxy);
      service->vAddFunction(_pMethodTextMsgCallbackSender);

      _pNBestList = new clSDS_NBestList();

      //_pListScreen->vAddToList(SR_APP_MAIN, _pMyAppsList);

      _pFuelPriceList = new clSDS_FuelPriceList(_sxmFuelProxy, _sxmCanadianFuelProxy, _naviProxy);
      _pListScreen->vAddToList(SR_INF_Sort_List_Distance, _pFuelPriceList);
      _pListScreen->vAddToList(SR_INF_Sort_List_Price, _pFuelPriceList);
      _pMethodCommonGetListInfo->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_FUEL_PRICES, _pFuelPriceList);
      _pMethodCommonGetListInfo->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_FUEL_DISTANCES, _pFuelPriceList);

      _pMethodTextMsgSetNumber = new clSDS_Method_TextMsgSetNumber(service, _pListScreen, _pPhonebookList, _pUserwords, _phonebookProxy, _pQuickDialList);
      service->vAddFunction(_pMethodTextMsgSetNumber);
      _pListScreen->vAddToList(SR_PHO_Quickdial, _pQuickDialList);
      _pListScreen->vAddToList(SR_PHO_Quickdial_Send, _pQuickDialList);
      _pListScreen->vAddToList(SR_PHO_OutCall_List, _pRecentCallsList);
      _pListScreen->vAddToList(SR_PHO_MissCall_List, _pRecentCallsList);
      _pListScreen->vAddToList(SR_PHO_InCall_List, _pRecentCallsList);
      _pListScreen->vAddToList(SR_PHO_SendCustom, _pCustomSMSList);
      _pListScreen->vAddToList(SR_PHO_ReadText, _pReadSmsList);
      _pListScreen->vAddToList(SR_GLO_Nbest_List, _pNBestList);
      _pMethodCommonGetListInfo->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_PHONE_MISSED_CALLS, _pRecentCallsList);
      _pMethodCommonGetListInfo->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_PHONE_ANSWERED_CALLS, _pRecentCallsList);
      _pMethodCommonGetListInfo->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_PHONE_DIALLED_CALLS, _pRecentCallsList);
      _pMethodCommonGetListInfo->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_CUSTOM_SMS, _pCustomSMSList);
      _pMethodCommonGetListInfo->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_PHONE_RECEIVED_SMS, _pReadSmsList);
      _pMethodCommonGetListInfo->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_PHONE_USERWORDS, _pQuickDialList);

      _pPreviousDestinationsList =  new clSDS_PreviousDestinationsList(_naviProxy);
      _pMethodCommonGetListInfo->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_NAV_PREV_DESTINATIONS, _pPreviousDestinationsList);
      _pListScreen->vAddToList(SR_NAV_PrevDest_List, _pPreviousDestinationsList);

      _pAddressBookList = new clSDS_AddressBookList(_naviProxy);
      _pMethodCommonGetListInfo->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_ADDRESSBOOK, _pAddressBookList);
      _pListScreen->vAddToList(SR_NAV_AddrBook_List, _pAddressBookList);

//      _pListScreen->vAddToList(SR_NAV_DISAMBIG_LIST, new clSDS_MultipleDestinationsList());
//      _pListScreen->vAddToList(SR_PHO_AMBIG_LIST, new clSDS_ContactNumberList(_pPhonebookList));

      _pContactNumberList = new clSDS_ContactNumberList(_pPhonebookList);
//      _clSDS_Method_CommonGetListInfo->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_CONTACT_AMBIGUITY, _pContactNumberList);
//      _pListScreen->vAddToList(SR_PHO_Ambig_List_Contact, _pContactNumberList);

      _pAmbigContactList = new clSDS_AmbigContactList();
      _pListScreen->vAddToList(SR_PHO_Ambig_List_Contact, _pAmbigContactList);
      _pListScreen->vAddToList(SR_PHO_Ambig_List_Contact_Send, _pAmbigContactList);

      _pAmbigNumberList = new clSDS_AmbigNumberList();
      _pListScreen->vAddToList(SR_PHO_Ambig_List_Number, _pAmbigNumberList);
      _pListScreen->vAddToList(SR_PHO_Ambig_List_Number_Send, _pAmbigNumberList);

      _pNaviListItems->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_NAV_PREV_DESTINATIONS, _pPreviousDestinationsList);
      _pNaviListItems->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_ADDRESSBOOK, _pAddressBookList);
      _pNaviListItems->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_FUEL_PRICES, _pFuelPriceList);
      _pNaviListItems->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_FUEL_DISTANCES, _pFuelPriceList);

      _pMethodTextMsgSend = new clSDS_Method_TextMsgSend(service, _msgProxy, _phonebookProxy);
      service->vAddFunction(_pMethodTextMsgSend);

      _pMethodPhoneRedialLastNumber = new clSDS_Method_PhoneRedialLastNumber(service, _pRecentCallsList, _telephoneProxy);
      service->vAddFunction(_pMethodPhoneRedialLastNumber);

      _pMethodPhoneGetContactListEntries = new clSDS_Method_PhoneGetContactListEntries(service, _pPhonebookList, _pUserwords, _phonebookProxy, _pQuickDialList);
      service->vAddFunction(_pMethodPhoneGetContactListEntries);
      service->vAddFunction(new clSDS_Method_PhoneSetContact(service, _pPhonebookList));

      _pMethodPhoneDialNumber = new clSDS_Method_PhoneDialNumber(service, _pListScreen, _pPhonebookList, _pUserwords, _telephoneProxy, _phonebookProxy, _pQuickDialList);
      service->vAddFunction(_pMethodPhoneDialNumber);

      _pMethodPhoneDialContact = new clSDS_Method_PhoneDialContact(service, _telephoneProxy, _phonebookProxy);
      service->vAddFunction(_pMethodPhoneDialContact);

      SAFE_CREATE(_pSxmAudioChannelList, clSDS_SXMAudioChannelList(_sxmAudioProxy, _sxmPhoneticsProxy, _sdsSxmProxy));
      _pSxmAudioChannelList->setSXMTunerStateObserver(_pPropertyTunerStatus);

      service->vAddFunction(new clSDS_Method_CommonGetHmiElementDescription(service, _pMenuManager, _pListScreen));
      SAFE_CREATE(_pMethodCommonGetHmiListDescription , clSDS_Method_CommonGetHmiListDescription(service));
      service->vAddFunction(_pMethodCommonGetHmiListDescription);
      _pMethodCommonGetHmiListDescription->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_ADDRESSBOOK, _pAddressBookList);
      _pMethodCommonGetHmiListDescription->vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_NAV_PREV_DESTINATIONS, _pPreviousDestinationsList);

      _pMethodContactsGetAmbiguityList = new clSDS_Method_ContactsGetAmbiguityList(service, _phonebookProxy, _pAmbigContactList, _pAmbigNumberList);
      service->vAddFunction(_pMethodContactsGetAmbiguityList);

      SAFE_CREATE(_pFmChannelList, clSDS_FmChannelList(_sdsFmProxy, _tunerProxy));
      _pFmChannelList->setFmChannelListObserver(_pPropertyTunerStatus);

      _pMethodTunerSelectStation = new clSDS_Method_TunerSelectStation(service, _audioSourceChangeProxy, _tunerProxy,
            *_pGuiService, _sxmAudioProxy, _tunerMasterProxy, _pFmChannelList);
      service->vAddFunction(_pMethodTunerSelectStation);

      _pMethodCommonSetActiveApplication = new clSDS_Method_CommonSetActiveApplication(service, *_pGuiService);
      service->vAddFunction(_pMethodCommonSetActiveApplication);

      SAFE_CREATE(_pTunerBandRange, clSDS_TunerBandRange(_tunerProxy));

      SAFE_CREATE(_pAcrHandler, AcrHandler(_acrProxy));
      SAFE_CREATE(_pAudioSourceHandler, AudioSourceHandler(_pSDSStatus, *_pSdsAudioSource));
   }

   return service;
}


void Sds2HmiServer::onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const ServiceStateChange& stateChange)
{
   ETG_TRACE_USR1(("Sds2HmiServer::onAvailable for port %s", proxy->getPortName().c_str()));

   for (std::vector<asf::core::ServiceAvailableIF*>::const_iterator iter = _serviceAvailableDelegates.begin(); iter != _serviceAvailableDelegates.end(); ++iter)
   {
      if ((*iter) != NULL)
      {
         (*iter)->onAvailable(proxy, stateChange);
      }
   }
}


void Sds2HmiServer::onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const ServiceStateChange& stateChange)
{
   for (std::vector<asf::core::ServiceAvailableIF*>::const_iterator iter = _serviceAvailableDelegates.begin(); iter != _serviceAvailableDelegates.end(); ++iter)
   {
      if ((*iter) != NULL)
      {
         (*iter)->onUnavailable(proxy, stateChange);
      }
   }
}


void Sds2HmiServer::createproxies()
{
   _audioSourceChangeProxy = AudioSourceChangeProxy::createProxy("audioSourceChangePort", *this);
   _audprocProxy = org::bosch::audproc::service::ServiceProxy::createProxy("audprocServicePort", *this);
   _bluetoothSettingsProxy = MOST_BTSet_FIProxy::createProxy("sds2phoneBTSetPort", *this);
   _clockProxy = Clock_main_fiProxy::createProxy("clockFiPort", *this);
   _ecnrProxy = org::bosch::ecnr::service::ServiceProxy::createProxy("ecnrServicePort", *this);
   _mediaPlayerProxy = Mplay_MediaPlayer_FIProxy::createProxy("sds2mediaPlayerFiPort", *this);
   _msgProxy = MOST_Msg_FIProxy::createProxy("sds2phoneMsgPort", *this);
   _naviProxy = NavigationServiceProxy::createProxy("naviServicePort", *this);
   _phonebookProxy = MOST_PhonBk_FIProxy::createProxy("phoneBookPort", *this);
   _sdsFmProxy = sds_fm_fi::SdsFmService::SdsFmServiceProxy::createProxy("sdsFmServicePort", *this);
   _sdsSxmProxy = sds_sxm_fi::SdsSxmService::SdsSxmServiceProxy::createProxy("sdsSxmServicePort", *this);
   _sxmAudioProxy = Sxm_audio_main_fiProxy::createProxy("sdsAudioFiPort", *this);
   _sxmCanadianFuelProxy = MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy::createProxy("sds2SxmCanadianFuelFiPort", *this);
   _sxmFuelProxy = MIDW_EXT_SXM_FUEL_FIProxy::createProxy("sds2sxmFuelFiPort", *this);
   _sxmPhoneticsProxy = MIDW_EXT_SXM_PHONETICS_FIProxy::createProxy("sdsPhoneticsFiPort", *this);
   _tcuProxy = Tcu_main_fiProxy::createProxy("tcuFiPort", *this);
   _telephoneProxy = MOST_Tel_FIProxy::createProxy("phoneTelPort", *this);
   _tunerMasterProxy = Tunermaster_main_fiProxy::createProxy("sdstunermasterFiPort", *this);
   _tunerProxy = Tuner_main_fiProxy::createProxy("tunerFiPort", *this);
   _vehicleProxy = VEHICLE_MAIN_FIProxy::createProxy("sds2vehicledataPort", *this);
   _acrProxy = CMB_ACR_FIProxy::createProxy("acrPort", *this);
   _smatphoneintProxy = Smartphoneint_main_fiProxy::createProxy("spiFiPort", *this);
}


void Sds2HmiServer::registerServiceAvailableDelegates()
{
   addServiceAvailableDelegate(_pCustomSMSList);
   addServiceAvailableDelegate(_pFmChannelList);
   addServiceAvailableDelegate(_pFormatTimeDate);
   addServiceAvailableDelegate(_pFuelPriceList);
   addServiceAvailableDelegate(_pLanguageMediator);
   addServiceAvailableDelegate(_pPhonebookList);
   addServiceAvailableDelegate(_pReadSmsList);
   addServiceAvailableDelegate(_pRecentCallsList);
   addServiceAvailableDelegate(_pSxmAudioChannelList);
   addServiceAvailableDelegate(_pTunerBandRange);

   addServiceAvailableDelegate(_pMethodAppsLaunchApplication);
   addServiceAvailableDelegate(_pMethodMediaGetDeviceInfo);
   addServiceAvailableDelegate(_pMethodMediaPlay);
   addServiceAvailableDelegate(_pMethodNaviGetWaypointListInfo);
   addServiceAvailableDelegate(_pMethodNaviSelectDestListEntry);
   addServiceAvailableDelegate(_pMethodNaviSetDestinationItem);
   addServiceAvailableDelegate(_pMethodNaviSetMapMode);
   addServiceAvailableDelegate(_pMethodPhoneDialNumber);
   addServiceAvailableDelegate(_pMethodPhoneRedialLastNumber);
   addServiceAvailableDelegate(_pMethodPhoneSetPhoneSetting);
   addServiceAvailableDelegate(_pMethodPhoneSwitchCall);
   addServiceAvailableDelegate(_pMethodTextMsgSend);

   addServiceAvailableDelegate(_pPropertyCommonSDSConfiguration);
   addServiceAvailableDelegate(_pPropertyCommonStatus);
   addServiceAvailableDelegate(_pPropertyMediaStatus);
   addServiceAvailableDelegate(_pPropertyNaviCurrentCountryState);
   addServiceAvailableDelegate(_pPropertyNaviStatus);
   addServiceAvailableDelegate(_pPropertyPhoneStatus);
   addServiceAvailableDelegate(_pPropertySpecialAppStatus);
   addServiceAvailableDelegate(_pPropertyTextMsgStatus);

   addServiceAvailableDelegate(_pAcrHandler);
}


void Sds2HmiServer::addServiceAvailableDelegate(asf::core::ServiceAvailableIF* obs) //lint -e429
{
   _serviceAvailableDelegates.push_back(obs);
}
