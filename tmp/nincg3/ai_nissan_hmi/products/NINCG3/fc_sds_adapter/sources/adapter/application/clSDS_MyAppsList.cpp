/*********************************************************************//**
 * \file       clSDS_MyAppsList.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_MyAppsList.h"
#include "application/clSDS_StringVarList.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_MyAppsList.cpp.trc.h"
#endif


#define NUMBER_OF_LIST_ITEMS_PER_PAGE 10


static tBool bSmartPhoneDataBaseReady()
{
   // TODO jnd2hi determine origin of midw_ext_fi_tcl_e8_DeviceStatus
   //return (dataPool.u8GetValue(DPSMARTPHONE__DATA_BASE_STATUS) == (tU8)midw_ext_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_READY);
   return FALSE;
}


/**************************************************************************//**
*Destructor
******************************************************************************/
clSDS_MyAppsList::~clSDS_MyAppsList()
{
   _pMyAppsDataBase = NULL;
}


/**************************************************************************//**
*Constructor
******************************************************************************/
clSDS_MyAppsList::clSDS_MyAppsList(clSDS_MyAppsDataBase* pMyAppsDataBase): clSDS_List(NUMBER_OF_LIST_ITEMS_PER_PAGE)
{
   _pMyAppsDataBase = pMyAppsDataBase;
   _oCurrentMenuLanguage.ISO639_3_SDSLanguageCode.enType = sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_INVALID;
   _bIsMyAppsListAvailable = FALSE;
   if (_pMyAppsDataBase != NULL)
   {
      _pMyAppsDataBase->vRegisterObserver(this);
   }
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_MyAppsList::u32GetSize()
{
   return 0; //dataPool.u8GetValue(DPSDS__MYAPPS_COUNT);
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::vector<clSDS_ListItems> clSDS_MyAppsList::oGetItems(tU32 u32StartIndex, tU32 u32EndIndex)
{
   bpstl::vector<clSDS_ListItems> oListItems;

   for (tU32 u32Index = u32StartIndex; u32Index < bpstl::min(u32EndIndex, u32GetSize()); u32Index++)
   {
      oListItems.push_back(oGetListItem(u32Index));
   }
   return oListItems;
}


/**************************************************************************//**
*
******************************************************************************/
clSDS_ListItems clSDS_MyAppsList::oGetListItem(tU32 /*u32Index*/) const
{
   clSDS_ListItems oListItem;
   // clDataPoolList myAppsList(DPSDS__MYAPPS);

   oListItem.oCommand.szString = ""; // myAppsList.oGetString(u32Index, 0);
   oListItem.oCommand.enTextColor = clSDS_ListItems::COMMAND;
   oListItem.bIsListScreenWithoutIndex = TRUE;
   ETG_TRACE_USR1(("MyApps Name = %s", oListItem.oCommand.szString.c_str()));
   return oListItem;
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_MyAppsList::bSelectElement(tU32 /*u32SelectedIndex*/)
{
   // clDataPoolList myAppsList(DPSDS__MYAPPS);
   bpstl::string oSelectedAppsName = ""; // myAppsList.oGetString(u32SelectedIndex, 0);

   clSDS_StringVarList::vSetVariable("$(AppName)", oSelectedAppsName);
   return TRUE;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_MyAppsList::vMenuLanguageChanged(const sds2hmi_fi_tcl_SDSLanguageID& oMenuLanguage)
{
   if (_oCurrentMenuLanguage.ISO639_3_SDSLanguageCode.enType != oMenuLanguage.ISO639_3_SDSLanguageCode.enType)
   {
      _oCurrentMenuLanguage = oMenuLanguage;
      if (bSmartPhoneDataBaseReady())
      {
         if (_pMyAppsDataBase != NULL && (_bIsMyAppsListAvailable == TRUE))
         {
            vClearMyAppsList();
            bpstl::vector<stMyappNames> oMyAppsNameList = _pMyAppsDataBase->oGetMyAppsNameList();
            vFillMyAppsListDatapool(oMyAppsNameList);
         }
      }
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_MyAppsList::vMyAppsDataBaseStatusChange()
{
   if (bSmartPhoneDataBaseReady())
   {
      vLoadMyAppsList();
   }
   else
   {
      _bIsMyAppsListAvailable = FALSE;
      vClearMyAppsList();
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_MyAppsList::vLoadMyAppsList()
{
   //here we are only requesting to read the data base and result will be notified later
   if (_pMyAppsDataBase != NULL)
   {
      _bIsMyAppsListAvailable = FALSE;
      _pMyAppsDataBase->vRequestMyAppsNames();
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_MyAppsList::vClearMyAppsList() const
{
//   tU8 u8MyAppListCount = dataPool.u8GetValue(DPSDS__MYAPPS_COUNT);
//   clDataPoolList myAppsList(DPSDS__MYAPPS);
//
//   for (tU8 u8Index = 0; u8Index < u8MyAppListCount; u8Index++)
//   {
//      myAppsList.vSetString(u8Index, 0, "");
//   }
//   dataPool.vSetU8Value(DPSDS__MYAPPS_COUNT, 0);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_MyAppsList::vMyAppsListAvailable()
{
   if (_pMyAppsDataBase != NULL)
   {
      _bIsMyAppsListAvailable = TRUE;
      bpstl::vector<stMyappNames> oMyAppsNameList = _pMyAppsDataBase->oGetMyAppsNameList();
      vFillMyAppsListDatapool(oMyAppsNameList);
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_MyAppsList::vUpdateCurrentLanguage()
{
   tU16 u16IsoLanguage = 0; // dataPool.u16GetValue(DPNAVI__ISO_LANGUAGE);
// TODO jnd2hi adapt to ISO639-3 language/country codes
//   if (u16IsoLanguage == (tU16)(fi_tcl_e16_ISOLanguageCode::FI_EN_ISO_639_1_UE))
//   {
//      u16IsoLanguage = (tU16) (sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode:::FI_EN_ISO_639_1_EN);
//   }
   _oCurrentMenuLanguage.ISO639_3_SDSLanguageCode.enType = (sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::tenType)u16IsoLanguage;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_MyAppsList::vFillMyAppsListDatapool(bpstl::vector<stMyappNames>& oMyAppsNameList)
{
   // clDataPoolList myAppsList(DPSDS__MYAPPS);
   // dataPool.vSetU8Value(DPSDS__MYAPPS_COUNT, 0);
   bpstl::vector<stMyappNames>::iterator iter = oMyAppsNameList.begin();

   if (_oCurrentMenuLanguage.ISO639_3_SDSLanguageCode.enType == sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_INVALID)
   {
      vUpdateCurrentLanguage();
   }
   tChar szTempString[50] = "";
   OSALUTIL_s32SaveNPrintFormat(szTempString, sizeof(szTempString), "%d", _oCurrentMenuLanguage.ISO639_3_SDSLanguageCode.enType);

   ETG_TRACE_USR1(("MyApps List Element Update for Language ID %s", szTempString));
   tU8 u8Index = 0;
   for (; iter != oMyAppsNameList.end(); ++iter)
   {
      if (OSAL_s32StringCompare(iter->oLanguageId.c_str(), szTempString) == 0)
      {
         // myAppsList.vSetString(u8Index, 0, iter->oAppName);
         ETG_TRACE_USR1(("MyApps List Element: Index = %d AppsName = %s", u8Index, iter->oAppName.c_str()));
         u8Index++;
      }
   }
   // dataPool.vSetU8Value(DPSDS__MYAPPS_COUNT, u8Index);
}
