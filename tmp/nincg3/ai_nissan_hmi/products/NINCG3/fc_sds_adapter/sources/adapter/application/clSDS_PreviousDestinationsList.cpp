/*********************************************************************//**
 * \file       clSDS_PreviousDestinationsList.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_PreviousDestinationsList.h"
#include "application/clSDS_StringVarList.h"
using namespace org::bosch::cm::navigation::NavigationService;


/**************************************************************************//**
*Destructor
******************************************************************************/
clSDS_PreviousDestinationsList::~clSDS_PreviousDestinationsList()
{
   oClearPreviousDestinationListItems();
}


/**************************************************************************//**
*Constructor
******************************************************************************/
clSDS_PreviousDestinationsList::clSDS_PreviousDestinationsList(::boost::shared_ptr< NavigationServiceProxy > ponaviProxy)
   : _naviProxy(ponaviProxy)
{
   _previousDestinationCount = 0;
   _previousDestinationPOIIndexValue = 0;
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_PreviousDestinationsList::bSelectElement(tU32 u32SelectedIndex)
{
   if (u32SelectedIndex > 0)
   {
      clSDS_StringVarList::vSetVariable("$(Address)", oGetItem(u32SelectedIndex - 1));
      oSetPreviousDestinationIndex(u32SelectedIndex - 1);
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_PreviousDestinationsList::vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType)
{
   if (listType == sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_NAV_PREV_DESTINATIONS)
   {
      _naviProxy->sendGetLastDestinationsRequest(*this);
   }
   else
   {
      oSetPreviousDestinationListCount(0);
      notifyListObserver();
   }
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_PreviousDestinationsList::u32GetSize()
{
   return _previousDestinationCount;
}


/**************************************************************************//**
*
******************************************************************************/
std::vector<clSDS_ListItems> clSDS_PreviousDestinationsList:: oGetItems(tU32 u32StartIndex, tU32 u32EndIndex)
{
   std::vector<clSDS_ListItems> oListItems;
   for (tU32 u32Index = u32StartIndex; u32Index < std::min(u32EndIndex, u32GetSize()); u32Index++)
   {
      clSDS_ListItems oListItem;
      oListItem.oDescription.szString = oGetItem(u32Index);
      oListItems.push_back(oListItem);
   }
   return oListItems;
}


/**************************************************************************//**
*
******************************************************************************/
void  clSDS_PreviousDestinationsList::onGetLastDestinationsError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< GetLastDestinationsError >& /*error*/)
{
   oClearPreviousDestinationListItems();
   oSetPreviousDestinationListCount(0);
   notifyListObserver();
}


/**************************************************************************//**
*
******************************************************************************/
void  clSDS_PreviousDestinationsList::onGetLastDestinationsResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< GetLastDestinationsResponse >& response)
{
   oClearPreviousDestinationListItems();
   if (!response->getLastDestinations().empty())
   {
      _previousDestinationName = response->getLastDestinations();
      oSetPreviousDestinationListCount(response->getLastDestinations().size());
   }
   else
   {
      oSetPreviousDestinationListCount(0);
   }
   notifyListObserver();
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_PreviousDestinationsList::oSetPreviousDestinationListCount(unsigned long previousDestinationList)
{
   _previousDestinationCount = previousDestinationList;
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_PreviousDestinationsList::oClearPreviousDestinationListItems()
{
   _previousDestinationName.clear();
}


/**************************************************************************//**
*
******************************************************************************/
std::string clSDS_PreviousDestinationsList::oGetItem(tU32 u32Index)
{
   if (_previousDestinationName.size())
   {
      return _previousDestinationName[u32Index].getData();
   }
   else
   {
      return "";
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_PreviousDestinationsList::oSetPreviousDestinationIndex(unsigned long previousDestinationPOI)
{
   _previousDestinationPOIIndexValue = previousDestinationPOI;
}


unsigned long clSDS_PreviousDestinationsList::vGetPreviousDestinationIndex() const
{
   return _previousDestinationPOIIndexValue;
}


/**************************************************************************//**
*
******************************************************************************/

void clSDS_PreviousDestinationsList::vStartGuidance()
{
   unsigned lIndex = static_cast<unsigned int>(vGetPreviousDestinationIndex());
   _naviProxy->sendStartGuidanceToLastDestinationRequest(*this, lIndex);
}


/**************************************************************************//**
 * Add as way point to current route guidance to  Navi.
******************************************************************************/
void clSDS_PreviousDestinationsList::vAddasWayPoint()
{
   unsigned lIndex = static_cast<unsigned int>(vGetPreviousDestinationIndex());
   _naviProxy->sendStartGuidanceToLastDestinationRequest(*this, lIndex);
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_PreviousDestinationsList::onStartGuidanceToLastDestinationError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< StartGuidanceToLastDestinationError >& /*error*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_PreviousDestinationsList::onStartGuidanceToLastDestinationResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< StartGuidanceToLastDestinationResponse >& /*response*/)
{
}
