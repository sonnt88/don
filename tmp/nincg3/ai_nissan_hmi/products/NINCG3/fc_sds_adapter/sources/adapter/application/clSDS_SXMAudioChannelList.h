/*********************************************************************//**
 * \file       clSDS_SXMAudioChannelList.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef clSDS_SXMAudioChannelList_h
#define clSDS_SXMAudioChannelList_h


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "external/sds2hmi_fi.h"
#include "sxm_audio_main_fiProxy.h"
#include "MIDW_EXT_SXM_PHONETICS_FIProxy.h"
#include "sds_sxm_fi/SdsSxmServiceProxy.h"


#define DELIMITER "|"
#define DELIMITER_SIZE 1
#define MAX_CHAR_NUMBER 224


class clSDS_TunerStateObserver;


class clSDS_SXMAudioChannelList
   : public asf::core::ServiceAvailableIF
   , public sxm_audio_main_fi::ChannelListStatusCallbackIF
   , public sxm_audio_main_fi::GetChannelListCallbackIF
   , public MIDW_EXT_SXM_PHONETICS_FI::PhoneticsUpdateCallbackIF
   , public MIDW_EXT_SXM_PHONETICS_FI::GetPhoneticsDataCallbackIF
   , public sds_sxm_fi::SdsSxmService::StoreSXMChannelNamesCallbackIF
   , public sds_sxm_fi::SdsSxmService::StoreSXMPhoneticDataCallbackIF
{
   public:
      clSDS_SXMAudioChannelList(::boost::shared_ptr< sxm_audio_main_fi::Sxm_audio_main_fiProxy > sxmAudioProxy, ::boost::shared_ptr< MIDW_EXT_SXM_PHONETICS_FI::MIDW_EXT_SXM_PHONETICS_FIProxy > sxmPhoneticsProxy, ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::SdsSxmServiceProxy > sds2sxmProxy);
      virtual ~clSDS_SXMAudioChannelList();

      void onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy, const asf::core::ServiceStateChange& stateChange);
      void onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy, const asf::core::ServiceStateChange& stateChange);

      void setSXMTunerStateObserver(clSDS_TunerStateObserver* obs);

   private:

      //ChannelListStatusCallbackIF
      virtual void onChannelListStatusError(const ::boost::shared_ptr< sxm_audio_main_fi::Sxm_audio_main_fiProxy >& proxy, const ::boost::shared_ptr< sxm_audio_main_fi::ChannelListStatusError >& error);
      virtual void onChannelListStatusStatus(const ::boost::shared_ptr< sxm_audio_main_fi::Sxm_audio_main_fiProxy >& proxy, const ::boost::shared_ptr< sxm_audio_main_fi::ChannelListStatusStatus >& status);

      //GetChannelListCallbackIF
      virtual void onGetChannelListError(const ::boost::shared_ptr< sxm_audio_main_fi::Sxm_audio_main_fiProxy >& proxy, const ::boost::shared_ptr< sxm_audio_main_fi::GetChannelListError >& error);
      virtual void onGetChannelListResult(const ::boost::shared_ptr< sxm_audio_main_fi::Sxm_audio_main_fiProxy >& proxy, const ::boost::shared_ptr< sxm_audio_main_fi::GetChannelListResult >& result);

      //PhoneticsUpdateCallbackIF
      virtual void onPhoneticsUpdateError(const ::boost::shared_ptr< MIDW_EXT_SXM_PHONETICS_FI::MIDW_EXT_SXM_PHONETICS_FIProxy >& proxy, const ::boost::shared_ptr< MIDW_EXT_SXM_PHONETICS_FI::PhoneticsUpdateError >& error);
      virtual void onPhoneticsUpdateStatus(const ::boost::shared_ptr< MIDW_EXT_SXM_PHONETICS_FI::MIDW_EXT_SXM_PHONETICS_FIProxy >& proxy, const ::boost::shared_ptr< MIDW_EXT_SXM_PHONETICS_FI::PhoneticsUpdateStatus >& status);

      //GetPhoneticsDataCallbackIF
      virtual void onGetPhoneticsDataError(const ::boost::shared_ptr< MIDW_EXT_SXM_PHONETICS_FI::MIDW_EXT_SXM_PHONETICS_FIProxy >& proxy, const ::boost::shared_ptr< MIDW_EXT_SXM_PHONETICS_FI::GetPhoneticsDataError >& error);
      virtual void onGetPhoneticsDataResult(const ::boost::shared_ptr< MIDW_EXT_SXM_PHONETICS_FI::MIDW_EXT_SXM_PHONETICS_FIProxy >& proxy, const ::boost::shared_ptr< MIDW_EXT_SXM_PHONETICS_FI::GetPhoneticsDataResult >& result);

      //StoreSXMChannelNamesCallbackIF
      virtual void onStoreSXMChannelNamesError(const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::SdsSxmServiceProxy >& proxy, const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::StoreSXMChannelNamesError >& error);
      virtual void onStoreSXMChannelNamesResponse(const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::SdsSxmServiceProxy >& proxy, const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::StoreSXMChannelNamesResponse >& response);

      //StoreSXMPhoneticDataCallbackIF
      virtual void onStoreSXMPhoneticDataError(const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::SdsSxmServiceProxy >& proxy, const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::StoreSXMPhoneticDataError >& error);
      virtual void onStoreSXMPhoneticDataResponse(const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::SdsSxmServiceProxy >& proxy, const ::boost::shared_ptr< sds_sxm_fi::SdsSxmService::StoreSXMPhoneticDataResponse >& response);

      void setSXMAudioChannelsList(const ::std::vector< ::sxm_audio_main_fi_types::T_ChannelListEntry >& channelsList);
      void setSXMPhoneticsDataList(const ::std::vector< ::midw_ext_sxm_phonetics_fi_types::T_ChannelPhoneticsInfo >& phoneticsList, sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::tenType languageCode);

      void incrRequests();
      void decrRequests();

      sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::tenType getLanguageCode(uint8 language_type) const;

      boost::shared_ptr< sxm_audio_main_fi::Sxm_audio_main_fiProxy > _sxmAudioProxy;
      boost::shared_ptr< MIDW_EXT_SXM_PHONETICS_FI::MIDW_EXT_SXM_PHONETICS_FIProxy > _sxmPhoneticsProxy;
      boost::shared_ptr< sds_sxm_fi::SdsSxmService::SdsSxmServiceProxy > _sds2sxmProxy;

      std::vector< sds_sxm_fi::SdsSxmService::SXMChannelItem > _channelList;

      clSDS_TunerStateObserver* _pTunerStateObserver;

      int _requestsCount;
};


#endif
