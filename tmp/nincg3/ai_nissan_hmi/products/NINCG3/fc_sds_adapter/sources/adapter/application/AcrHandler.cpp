/*********************************************************************//**
 * \file       AcrHandler.cpp
 *
 * Handles audio channel requests from the prompt player.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include <application/AcrHandler.h>


using namespace CMB_ACR_FI;
using namespace acr_fi_types;


#define STRING_DEVICE_ACOUSTICOUT_IF_SPEECH           "/dev/acousticout/speech"


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
AcrHandler::~AcrHandler()
{
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
AcrHandler::AcrHandler(::boost::shared_ptr< CMB_ACR_FI::CMB_ACR_FIProxy> acrProxy)
   : _acrProxy(acrProxy)
{
}


void AcrHandler::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _acrProxy)
   {
      _acrProxy->sendChannelRequestUpReg(*this);
   }
}


void AcrHandler::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _acrProxy)
   {
      _acrProxy->sendChannelRequestRelUpRegAll();
   }
}


void AcrHandler::onChannelRequestStatus(
   const ::boost::shared_ptr< CMB_ACR_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ChannelRequestStatus >& status)
{
   const acr_fi_types::T_ChannelRequestStatusList& requestList = status->getChannelRequestStatusList();

   for (size_t i = 0; i < requestList.size(); ++i)
   {
      const acr_fi_types::T_ChannelRequestStatus& request = requestList[i];
      if ((request.getCategory() == T_e8_VoiceCategory__DialogPrompt) &&
            (request.getStatus() == T_e8_CatStatus__NeedChannel))
      {
         acr_fi_types::T_AudioChannelAdvice channelAdvice;
         channelAdvice.setIndex(request.getIndex());
         channelAdvice.setDevice(STRING_DEVICE_ACOUSTICOUT_IF_SPEECH);
         channelAdvice.setChannelAdvice(T_e8_AudioChannelAdvice__Start);
         _acrProxy->sendVoiceAdviceStart(*this, channelAdvice);
      }
   }
}


void AcrHandler::onChannelRequestError(
   const ::boost::shared_ptr< CMB_ACR_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ChannelRequestError >& /*error*/)
{
}


void AcrHandler::onVoiceAdviceError(
   const ::boost::shared_ptr< CMB_ACR_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< VoiceAdviceError >& /*error*/)
{
}


void AcrHandler::onVoiceAdviceResult(
   const ::boost::shared_ptr< CMB_ACR_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< VoiceAdviceResult >& /*result*/)
{
}
