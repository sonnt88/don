/*********************************************************************//**
 * \file       clSDS_MenuManager.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_MenuManager_h
#define clSDS_MenuManager_h


#include "view_db/Sds_ViewDB.h"
#include "application/clSDS_MenuControl.h"


class clSDS_Display;
class clSDS_SDSStatus;
class clSDS_ScreenData;
class clSDS_SdsControl;
class GuiService;
class PopUpService;


class clSDS_MenuManager : public clSDS_MenuControl
{
   public:
      virtual ~clSDS_MenuManager();
      clSDS_MenuManager(
         clSDS_Display* pDisplay,
         clSDS_SdsControl* pSdsControl);
      tVoid vShowSDSView(clSDS_ScreenData& oScreenData);
      tVoid vCloseGUI(tBool bError = FALSE) const;
      void onElementSelected(unsigned int selectedIndex);
      void onCursorMoved(unsigned int cursorIndex);
      void onRequestNextPage();
      void onRequestPreviousPage();
      void onSettingsRequest();
      void onHelpRequest();
      std::string getPromptId() const;
      unsigned int convertToSdsListIndex(unsigned int absoluteIndex) const;

   private:
      clSDS_Display* _pDisplay;
      clSDS_SdsControl* _pSdsControl;
      std::string _grammarId;
      std::string _promptId;
};


#endif
