/*********************************************************************//**
 * \file       clSDS_Method_NaviSelectDestListEntry.cpp
 *
 * clSDS_Method_NaviSelectDestListEntry method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_NaviSelectDestListEntry.h"
#include "external/sds2hmi_fi.h"
#include "application/clSDS_NaviListItems.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_NaviSelectDestListEntry.cpp.trc.h"
#endif


using namespace org::bosch::cm::navigation::NavigationService;


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_NaviSelectDestListEntry::~clSDS_Method_NaviSelectDestListEntry()
{
   _pNaviListItems = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_NaviSelectDestListEntry::clSDS_Method_NaviSelectDestListEntry(
   ahl_tclBaseOneThreadService* pService,
   clSDS_NaviListItems* pNaviListItems,
   ::boost::shared_ptr<org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > naviProxy)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_NAVISELECTDESTLISTENTRY, pService)
   , _naviProxy(naviProxy)
   , _pNaviListItems(pNaviListItems)
   , _enNavDestType(sds2hmi_fi_tcl_e8_NAV_ListType::FI_EN_UNKNOWN)
   , _bHomedestinationAvailable(false)
   , _bWorkDestinationAvailable(false)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviSelectDestListEntry::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgNaviSelectDestListEntryMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   _pNaviListItems->vSetNaviDestListType(oMessage.nListEntryType.enType);

   switch (oMessage.nListEntryType.enType)
   {
      case sds2hmi_fi_tcl_e8_NAV_ListType::FI_EN_HOME_DEST:
      {
         if (_bHomedestinationAvailable)
         {
            _enNavDestType = sds2hmi_fi_tcl_e8_NAV_ListType::FI_EN_HOME_DEST;
            vSendMethodResult();
         }
         else
         {
            vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_NOHOMEADDRESSAVAILABLE);
         }

         break;
      }
      case sds2hmi_fi_tcl_e8_NAV_ListType::FI_EN_WORK_DEST:
      {
         if (_bWorkDestinationAvailable)
         {
            _enNavDestType = sds2hmi_fi_tcl_e8_NAV_ListType::FI_EN_WORK_DEST;
            vSendMethodResult();
         }
         else
         {
            //error code to be replaced with NoWorkAvailable
            vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_NOWORKADDRESSAVAILABLE);
         }

         break;
      }
      default:
      {
         //TODO send a generic methodError message
         break;
      }
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSelectDestListEntry::setHomeDestinationStatus(bool bStatus)
{
   _bHomedestinationAvailable = bStatus;
   ETG_TRACE_USR4((" clSDS_Method_NaviSelectDestListEntry::setHomeDestinationStatus bHomedestinationAvailable %d", _bHomedestinationAvailable));
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSelectDestListEntry::setWorkDestinationStatus(bool bStatus)
{
   _bWorkDestinationAvailable = bStatus;
   ETG_TRACE_USR4((" clSDS_Method_NaviSelectDestListEntry::setWorkDestinationStatus bWorkDestinationAvailable %d", _bWorkDestinationAvailable));
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSelectDestListEntry::onHomeLocationError(const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& /*proxy*/, const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::HomeLocationError >& /*error*/)
{
   ETG_TRACE_USR4(("clSDS_Method_NaviSelectDestListEntry::onHomeLocationError"));
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSelectDestListEntry::onHomeLocationUpdate(const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& /*proxy*/, const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::HomeLocationUpdate >& update)
{
   setHomeDestinationStatus(update->getHomeLocation().getValid());
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSelectDestListEntry::onWorkLocationError(const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& /*proxy*/, const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::WorkLocationError >& /*error*/)
{
   ETG_TRACE_USR4(("clSDS_Method_NaviSelectDestListEntry::onWorkLocationError"));
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSelectDestListEntry::onWorkLocationUpdate(const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& /*proxy*/, const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::WorkLocationUpdate >& update)
{
   setWorkDestinationStatus(update->getWorkLocation().getValid());
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSelectDestListEntry::onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
      const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _naviProxy)
   {
      _naviProxy->sendHomeLocationRegister(*this);
      _naviProxy->sendHomeLocationGet(*this);
      _naviProxy->sendWorkLocationRegister(*this);
      _naviProxy->sendWorkLocationGet(*this);
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSelectDestListEntry::onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
      const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _naviProxy)
   {
      _naviProxy->sendHomeLocationDeregisterAll();
      _naviProxy->sendWorkLocationDeregisterAll();
   }
}


/**************************************************************************//**
*
******************************************************************************/
sds2hmi_fi_tcl_e8_NAV_ListType::tenType clSDS_Method_NaviSelectDestListEntry::getNavDestinationType()
{
   ETG_TRACE_USR4((" clSDS_Method_NaviSelectDestListEntry::getNavDestinationType enNavDestType %d", _enNavDestType));
   return _enNavDestType;
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSelectDestListEntry::ResetNavDestinationType()
{
   _enNavDestType = sds2hmi_fi_tcl_e8_NAV_ListType::FI_EN_UNKNOWN;
}
