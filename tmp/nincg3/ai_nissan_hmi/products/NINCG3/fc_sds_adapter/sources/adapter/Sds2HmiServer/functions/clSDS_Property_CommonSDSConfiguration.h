/*********************************************************************//**
 * \file       clSDS_Property_CommonSDSConfiguration.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Property_CommonSDSConfiguration_h
#define clSDS_Property_CommonSDSConfiguration_h


#include "Sds2HmiServer/framework/clServerProperty.h"
#include "application/clSDS_SdsConfigObserver.h"
#include "application/clSDS_XMLStringCreation.h"
#include "tcu_main_fiProxy.h"


class SettingsService;


class clSDS_Property_CommonSDSConfiguration
   : public clServerProperty
   , public asf::core::ServiceAvailableIF
   , public tcu_main_fi::ProvisionServiceCallbackIF
   , public clSDS_SdsConfigObserver
{
   public:
      virtual ~clSDS_Property_CommonSDSConfiguration();

      clSDS_Property_CommonSDSConfiguration(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< tcu_main_fi::Tcu_main_fiProxy > pTcuFiProxy,
         SettingsService* settingsService);

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      //callbacks for TCU service  Available  from tcu_main_fi
      virtual void onProvisionServiceError(
         const ::boost::shared_ptr< tcu_main_fi::Tcu_main_fiProxy >& proxy,
         const ::boost::shared_ptr< tcu_main_fi::ProvisionServiceError >& error);
      virtual void onProvisionServiceStatus(
         const ::boost::shared_ptr< tcu_main_fi::Tcu_main_fiProxy >& proxy,
         const ::boost::shared_ptr< tcu_main_fi::ProvisionServiceStatus >& status);

      virtual void updateNBestMatchAudio(bool audioNBest);
      virtual void updateNBestMatchPhoneBook(bool phoneNBest);
      virtual void updateBeepOnlyMode(bool beepOnlyModeStatus);

   protected:
      virtual tVoid vGet(amt_tclServiceData* pInMsg);
      virtual tVoid vSet(amt_tclServiceData* pInMsg);
      virtual tVoid vUpreg(amt_tclServiceData* pInMsg);

   private:
      tVoid vSendStatus();

      SDSConfigData _sdsConfigData;
      boost::shared_ptr< tcu_main_fi::Tcu_main_fiProxy > _tcuFiProxy;
};


#endif
