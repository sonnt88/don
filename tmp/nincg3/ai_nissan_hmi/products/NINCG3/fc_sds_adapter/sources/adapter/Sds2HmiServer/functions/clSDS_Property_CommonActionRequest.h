/*********************************************************************//**
 * \file       clSDS_Property_CommonActionRequest.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Property_CommonActionRequest_h
#define clSDS_Property_CommonActionRequest_h


#include "Sds2HmiServer/framework/clServerProperty.h"
#include "application/clSDS_SdsControl.h"

class clSDS_LanguageMediator;


class clSDS_Property_CommonActionRequest : public clServerProperty, public clSDS_SdsControl
{
   public:
      virtual ~clSDS_Property_CommonActionRequest();
      clSDS_Property_CommonActionRequest(ahl_tclBaseOneThreadService* pService, clSDS_LanguageMediator* languageMediator);
      tVoid vSendPttEvent(tU32 u32Context);
      tVoid vSendPTTLongPressEvent();
      tVoid vSendBackEvent();
      tVoid vAbortDialog();
      tVoid vDeleteUserWord(tU32 tU32UWID);
      tVoid vDeleteAllUserWords(tU32 tU32UWProfile);
      tVoid vSetUWProfile(tU32 u32NewSDSUser);
      tVoid vSetSpeaker(tU16 u16SpeakerId);
      tVoid vRequestAllSpeakers();
      tVoid vClearPrivateData();
      tVoid sendEnterManualMode();
      void sendFocusMoved(unsigned int value);
      void sendNextPageRequest();
      void sendPreviousPageRequest();
      void sendListSelectRequest(unsigned int value);
      void sendSelectRequest(unsigned int value);
      void sendCancelDialog();
      void recordUserWord(unsigned long userWordListID);
      void replaceUserWord(unsigned long userWordListID);
      void playUserWord(unsigned long userWordListID);
      void sendEnterPauseMode();
      void sendExitPauseMode();

   protected:
      virtual tVoid vGet(amt_tclServiceData* pInMsg);
      virtual tVoid vSet(amt_tclServiceData* pInMsg);
      virtual tVoid vUpreg(amt_tclServiceData* pInMsg);

   private:
      clSDS_LanguageMediator* _pLanguageMediator;
};


#endif
