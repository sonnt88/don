/*********************************************************************//**
 * \file       clSDS_Method_TunerSelectStation.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_TunerSelectStation_h
#define clSDS_Method_TunerSelectStation_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "tuner_main_fiProxy.h"
#include "bosch/cm/ai/hmi/masteraudioservice/AudioSourceChangeProxy.h"
#include "sxm_audio_main_fiProxy.h"
#include "tunermaster_main_fiProxy.h"


#define NS_AUDIOSRCCHG        bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange

class GuiService;
class clSDS_FmChannelList;

class clSDS_Method_TunerSelectStation
   : public clServerMethod
   , public NS_AUDIOSRCCHG::ActivateSourceCallbackIF
   , public sxm_audio_main_fi::SelectChannelCallbackIF
   , public tunermaster_main_fi::FID_TUNMSTR_S_DIRECT_FREQUENCY_INPUTCallbackIF
   , public tunermaster_main_fi::FID_TUNMSTR_S_PRESET_HANDLINGCallbackIF
   , public sxm_audio_main_fi::RecallPresetCallbackIF
   , public tuner_main_fi::FID_TUN_S_SET_PICallbackIF
   , public tuner_main_fi::FID_TUN_S_SEL_HD_AUDIOPRGMDIRECTCallbackIF
{
   public:
      virtual ~clSDS_Method_TunerSelectStation();
      clSDS_Method_TunerSelectStation(
         ahl_tclBaseOneThreadService* pService,
         boost::shared_ptr< NS_AUDIOSRCCHG::AudioSourceChangeProxy > audioSourceChangeProxy,
         ::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy > tunerProxy,
         GuiService& guiService,
         ::boost::shared_ptr< sxm_audio_main_fi::Sxm_audio_main_fiProxy > sxmAudioProxy,
         ::boost::shared_ptr< tunermaster_main_fi::Tunermaster_main_fiProxy > tunerMasterProxy,
         clSDS_FmChannelList* pFmChannelList);

      virtual void onActivateSourceError(
         const ::boost::shared_ptr< NS_AUDIOSRCCHG::AudioSourceChangeProxy >& proxy,
         const ::boost::shared_ptr< NS_AUDIOSRCCHG::ActivateSourceError >& error);
      virtual void onActivateSourceResponse(
         const ::boost::shared_ptr< NS_AUDIOSRCCHG::AudioSourceChangeProxy >& proxy,
         const ::boost::shared_ptr< NS_AUDIOSRCCHG::ActivateSourceResponse >& response);

      virtual void onSelectChannelError(
         const ::boost::shared_ptr< sxm_audio_main_fi::Sxm_audio_main_fiProxy >& proxy,
         const ::boost::shared_ptr< sxm_audio_main_fi::SelectChannelError >& error);
      virtual void onSelectChannelResult(
         const ::boost::shared_ptr< sxm_audio_main_fi::Sxm_audio_main_fiProxy >& proxy,
         const ::boost::shared_ptr< sxm_audio_main_fi::SelectChannelResult >& result);

      virtual void onFID_TUNMSTR_S_PRESET_HANDLINGError(
         const ::boost::shared_ptr< tunermaster_main_fi::Tunermaster_main_fiProxy >& proxy,
         const ::boost::shared_ptr< tunermaster_main_fi::FID_TUNMSTR_S_PRESET_HANDLINGError >& error);
      virtual void onFID_TUNMSTR_S_PRESET_HANDLINGResult(
         const ::boost::shared_ptr< tunermaster_main_fi::Tunermaster_main_fiProxy >& proxy,
         const ::boost::shared_ptr< tunermaster_main_fi::FID_TUNMSTR_S_PRESET_HANDLINGResult >& result);

      virtual void onRecallPresetError(
         const ::boost::shared_ptr< sxm_audio_main_fi::Sxm_audio_main_fiProxy >& proxy,
         const ::boost::shared_ptr< sxm_audio_main_fi::RecallPresetError >& error);
      virtual void onRecallPresetResult(
         const ::boost::shared_ptr< sxm_audio_main_fi::Sxm_audio_main_fiProxy >& proxy,
         const ::boost::shared_ptr< sxm_audio_main_fi::RecallPresetResult >& result);

      virtual void onFID_TUN_S_SET_PIError(
         const ::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy >& proxy,
         const ::boost::shared_ptr< tuner_main_fi::FID_TUN_S_SET_PIError >& error);
      virtual void onFID_TUN_S_SET_PIResult(
         const ::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy >& proxy,
         const ::boost::shared_ptr< tuner_main_fi::FID_TUN_S_SET_PIResult >& result);

      virtual void onFID_TUN_S_SEL_HD_AUDIOPRGMDIRECTError(
         const ::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy >& proxy,
         const ::boost::shared_ptr< tuner_main_fi::FID_TUN_S_SEL_HD_AUDIOPRGMDIRECTError >& error);
      virtual void onFID_TUN_S_SEL_HD_AUDIOPRGMDIRECTResult(
         const ::boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy >& proxy,
         const ::boost::shared_ptr< tuner_main_fi::FID_TUN_S_SEL_HD_AUDIOPRGMDIRECTResult >& result);

      virtual void onFID_TUNMSTR_S_DIRECT_FREQUENCY_INPUTResult(const ::boost::shared_ptr< tunermaster_main_fi::Tunermaster_main_fiProxy >& proxy,
            const ::boost::shared_ptr< tunermaster_main_fi::FID_TUNMSTR_S_DIRECT_FREQUENCY_INPUTResult >& result);
      virtual void onFID_TUNMSTR_S_DIRECT_FREQUENCY_INPUTError(const ::boost::shared_ptr< tunermaster_main_fi::Tunermaster_main_fiProxy >& proxy,
            const ::boost::shared_ptr< tunermaster_main_fi::FID_TUNMSTR_S_DIRECT_FREQUENCY_INPUTError >& Error);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      void activateTunerSource(unsigned int sourceid);
      tBool bTunerStation(const sds2hmi_sdsfi_tclMsgTunerSelectStationMethodStart& oMessage);
      void vProcessMessage(const sds2hmi_sdsfi_tclMsgTunerSelectStationMethodStart& oMessage);
      tBool bTuneToPresetChannel(const sds2hmi_sdsfi_tclMsgTunerSelectStationMethodStart& oMessage);
      bool bTuneToChannelName(const sds2hmi_sdsfi_tclMsgTunerSelectStationMethodStart& oMessage);

      GuiService& _guiService;
      boost::shared_ptr< NS_AUDIOSRCCHG::AudioSourceChangeProxy > _audioSourceChangeProxy;
      boost::shared_ptr< sxm_audio_main_fi::Sxm_audio_main_fiProxy > _sxmAudioProxy;
      boost::shared_ptr< tunermaster_main_fi::Tunermaster_main_fiProxy > _tunerMasterProxy;
      boost::shared_ptr< ::tuner_main_fi::Tuner_main_fiProxy > _tunerProxy;
      clSDS_FmChannelList* _pFmChannelList;
      NS_AUDIOSRCCHG::sourceData _srcData;
};


#endif
