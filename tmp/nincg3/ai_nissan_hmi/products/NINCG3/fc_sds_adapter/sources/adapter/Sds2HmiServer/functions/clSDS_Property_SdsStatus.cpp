/*********************************************************************//**
 * \file       clSDS_Property_SdsStatus.cpp
 *
 * Common Action Request property implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Property_SdsStatus.h"
#include "application/clSDS_SDSStatus.h"
#include "application/clSDS_SDSStatusObserver.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Property_SdsStatus::~clSDS_Property_SdsStatus()
{
   _pSDSStatus = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Property_SdsStatus::clSDS_Property_SdsStatus(ahl_tclBaseOneThreadService* pService, clSDS_SDSStatus* pSDSStatus)
   : clServerProperty(SDS2HMI_SDSFI_C_U16_SDS_STATUS, pService)
   , _SdsStatus(sds2hmi_fi_tcl_e8_SDS_Status::FI_EN_UNKNOWN)
{
   _pSDSStatus = pSDSStatus;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_SdsStatus::vSet(amt_tclServiceData* pInMsg)
{
   sds2hmi_sdsfi_tclMsgSDS_StatusSet oMessage;
   vGetDataFromAmt(pInMsg, oMessage);
   _SdsStatus = oMessage.State.enType;
   sds2hmi_sdsfi_tclMsgSDS_StatusStatus oStatusMessage;
   oStatusMessage.State.enType = _SdsStatus;
   vStatus(oStatusMessage);
   vSendSDSStatus();
}


tVoid clSDS_Property_SdsStatus::vGet(amt_tclServiceData* /*pInMsg*/)
{
   sds2hmi_sdsfi_tclMsgSDS_StatusStatus oStatusMessage;
   oStatusMessage.State.enType = _SdsStatus;
   vStatus(oStatusMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_SdsStatus::vUpreg(amt_tclServiceData* /*pInMsg*/)
{
   sds2hmi_sdsfi_tclMsgSDS_StatusStatus oMessage;
   oMessage.State.enType = _SdsStatus;
   vStatus(oMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_SdsStatus::vSendSDSStatus()
{
   clSDS_SDSStatus::enSDSStatus oStatus(clSDS_SDSStatus::EN_UNKNOWN);
   switch (_SdsStatus)
   {
      case sds2hmi_fi_tcl_e8_SDS_Status::FI_EN_IDLE:
      case sds2hmi_fi_tcl_e8_SDS_Status::FI_EN_IDLE_TTS_ONLY:
         oStatus = clSDS_SDSStatus::EN_IDLE;
         break;

      case sds2hmi_fi_tcl_e8_SDS_Status::FI_EN_ERROR:
      case sds2hmi_fi_tcl_e8_SDS_Status::FI_EN_DATA_ERROR:
      case sds2hmi_fi_tcl_e8_SDS_Status::FI_EN_UNAVAILABLE:
         oStatus = clSDS_SDSStatus::EN_ERROR;
         break;

      case sds2hmi_fi_tcl_e8_SDS_Status::FI_EN_LISTENING:
         oStatus = clSDS_SDSStatus::EN_LISTENING;
         break;

      case sds2hmi_fi_tcl_e8_SDS_Status::FI_EN_LOADING:
         oStatus = clSDS_SDSStatus::EN_LOADING;
         break;

      case sds2hmi_fi_tcl_e8_SDS_Status::FI_EN_PROCESSING:
         oStatus = clSDS_SDSStatus::EN_DIALOGOPEN;
         break;

      case sds2hmi_fi_tcl_e8_SDS_Status::FI_EN_ACTIVE:
         oStatus = clSDS_SDSStatus::EN_ACTIVE;
         break;

      case sds2hmi_fi_tcl_e8_SDS_Status::FI_EN_PAUSE:
         oStatus = clSDS_SDSStatus::EN_PAUSE;
         break;

      case sds2hmi_fi_tcl_e8_SDS_Status::FI_EN_UNKNOWN:
         oStatus = clSDS_SDSStatus::EN_UNKNOWN;
         break;

      default:
         break;
   }
   _pSDSStatus->vSDSStatusChanged(oStatus);
}
