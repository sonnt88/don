/*********************************************************************//**
 * \file       clSDS_Method_ContactsGetAmbiguityList.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "Sds2HmiServer/functions/clSDS_Method_ContactsGetAmbiguityList.h"
#include "application/clSDS_AmbigContactList.h"
#include "application/clSDS_AmbigNumberList.h"
#include "application/clSDS_PhoneNumberFormatter.h"
#include "application/clSDS_StringVarList.h"
#include "application/StringUtils.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_ContactsGetAmbiguityList.cpp.trc.h"
#endif

using namespace MOST_PhonBk_FI;
using namespace most_PhonBk_fi_types;


clSDS_Method_ContactsGetAmbiguityList::clSDS_Method_ContactsGetAmbiguityList(ahl_tclBaseOneThreadService* pService
      , ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy> phoneBookProxy
      , clSDS_AmbigContactList* ambigContactList
      , clSDS_AmbigNumberList* ambigNumberList)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_CONTACTSGETAMBIGUITYLIST, pService)
   , _phoneBookProxy(phoneBookProxy)
   , _pAmbigContactList(ambigContactList)
   , _pAmbigNumberList(ambigNumberList)
{
   _locationType.enType = sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_UNKNOWN;
   _requireAllLocationIfNoExactMatch = false;
}


clSDS_Method_ContactsGetAmbiguityList::~clSDS_Method_ContactsGetAmbiguityList()
{
   _pAmbigContactList = NULL;
   _pAmbigNumberList = NULL;
}


void clSDS_Method_ContactsGetAmbiguityList::vMethodStart(amt_tclServiceData* pInMsg)
{
   sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodStart oMessage;
   vGetDataFromAmt(pInMsg, oMessage);

   _contactIDList.clear();
   _contactDetailList.clear();

   if (sds2hmi_fi_tcl_e8_CON_EntryType::FI_EN_PHONE_NUMBER == oMessage.EntryType.enType)
   {
      _contactIDList = oMessage.ContactIDs;
      _locationType = oMessage.LocationType;
      _requireAllLocationIfNoExactMatch = (oMessage.RequireAllLocationIfNoExactMatch == TRUE);

      ETG_TRACE_USR4(("clSDS_Method_ContactsGetAmbiguityList::vMethodStart - _contactIDList.size() = %d", _contactIDList.size()));

      if (_contactIDList.size() > 0)
      {
         // Name disambiguation 	(_contactIDList.size() > 1) -> AMBIG_CONTACT
         // Number disambiguation 	(_contactIDList.size() == 1)-> AMBIG_ALL_ENTRIES / SINGLE_ENTRY / NO_ENTRY

         resolveAmbiguity();
      }
      else // contacts.size() == 0
      {
         vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
      }
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_ENTRYTYPENOTSUPPORTED);
   }
}


void clSDS_Method_ContactsGetAmbiguityList::resolveAmbiguity()
{
   if (_phoneBookProxy->isAvailable())
   {
      for (tU32 i = 0; i < _contactIDList.size(); i++) // only one iteration for number disambiguation
      {
         ETG_TRACE_USR4(("clSDS_Method_ContactsGetAmbiguityList::resolveAmbiguity - ContactID = %d", _contactIDList[i]));

         _phoneBookProxy->sendGetContactDetailsStart(*this, _contactIDList[i], T_e8_PhonBkContactDetailFilter__e8CDF_NONE);
      }
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}


void clSDS_Method_ContactsGetAmbiguityList::onGetContactDetailsError(const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/, const ::boost::shared_ptr< GetContactDetailsError >& /*error*/)
{
   sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult methodResult;
   methodResult.ResultType.enType = sds2hmi_fi_tcl_e8_CON_AmbiguityListType::FI_EN_CONTACT_UNAVAILABLE;
   vSendMethodResult(methodResult);
}


void clSDS_Method_ContactsGetAmbiguityList::onGetContactDetailsResult(const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/, const ::boost::shared_ptr< GetContactDetailsResult >& result)
{
   T_PhonBkContactDetails oContactDetails = result->getOContactDetails();
   _contactDetailList.push_back(oContactDetails);

   if (_contactIDList.size() == 1) // case of NUMBER disambiguation -> send method result with all phone numbers
   {
      updateHeaderTagWithContactName(oContactDetails);

      sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult methodResult;
      setResultEntriesAndType(_contactIDList[0], oContactDetails, methodResult);
      ETG_TRACE_USR4(("clSDS_Method_ContactsGetAmbiguityList::onGetContactDetailsResult - results count = %d", methodResult.ResultEntries.size()));

      vSendMethodResult(methodResult);
   }
   else // case of NAME disambiguation
   {
      if (_contactIDList.size() == _contactDetailList.size()) // we have received details for all contact ids
      {
         sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult methodResult;
         methodResult.ResultType.enType = sds2hmi_fi_tcl_e8_CON_AmbiguityListType::FI_EN_AMBIG_CONTACT;

         for (tU32 i = 0; i < _contactIDList.size(); i++)
         {
            sds2hmi_fi_tcl_CON_AmbiguityResultEntry resultEntry;
            resultEntry.LocationType = _locationType;
            resultEntry.ContactID = _contactIDList[i];
            methodResult.ResultEntries.push_back(resultEntry);
         }

         setAmbigContacts(_contactDetailList);

         vSendMethodResult(methodResult);
      }
      else
      {
         // still case of name disambiguation, but we have not received details for all contact ids
         // so, we have to wait for the details from the other remaining contacts
         // and only after that send the response with method result
      }
   }
}


void clSDS_Method_ContactsGetAmbiguityList::setResultEntriesAndType(tU32 contactID, T_PhonBkContactDetails contactDetails, sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& methodResult)
{
   ETG_TRACE_USR4(("clSDS_Method_ContactsGetAmbiguityList::setResultEntriesAndType - locationType = %d", _locationType.enType));

   switch (_locationType.enType)
   {
      case sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_HOME1:
         setHomeEntries(contactID, contactDetails, methodResult);
         break;

      case sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_OFFICE1:
         setOfficeEntries(contactID, contactDetails, methodResult);
         break;

      case sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_MOBILE1:
         setMobileEntries(contactID, contactDetails, methodResult);
         break;

      case sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_OTHER:
         setOtherEntries(contactID, contactDetails, methodResult);
         break;

      case sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_UNKNOWN:
         setAllLocationsEntries(contactID, contactDetails, methodResult);
         break;

      default:
         break;
   }

   setResultType(methodResult);

   if (methodResult.ResultEntries.size() == 0  && _requireAllLocationIfNoExactMatch)
   {
      setAllLocationsEntries(contactID, contactDetails, methodResult);

      if (methodResult.ResultEntries.size() > 0)
      {
         methodResult.ResultType.enType = sds2hmi_fi_tcl_e8_CON_AmbiguityListType::FI_EN_NO_MATCHING_LOCATION;

         setAmbigNumbers(methodResult.ResultEntries);
      }
   }
}


void clSDS_Method_ContactsGetAmbiguityList::setResultType(sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& methodResult)
{
   if (methodResult.ResultEntries.size() == 1)
   {
      methodResult.ResultType.enType = sds2hmi_fi_tcl_e8_CON_AmbiguityListType::FI_EN_SINGLE_ENTRY;
   }
   else if (methodResult.ResultEntries.size() > 1)
   {
      methodResult.ResultType.enType = sds2hmi_fi_tcl_e8_CON_AmbiguityListType::FI_EN_AMBIG_ALL_ENTRIES;

      setAmbigNumbers(methodResult.ResultEntries);
   }
   else //methodResult.ResultEntries.size() == 0
   {
      methodResult.ResultType.enType = sds2hmi_fi_tcl_e8_CON_AmbiguityListType::FI_EN_NO_ENTRY;
   }
}


void clSDS_Method_ContactsGetAmbiguityList::setAmbigContacts(std::vector<most_PhonBk_fi_types::T_PhonBkContactDetails > contactDetailList)
{
   std::vector<std::string> contactsNames;

   for (tU32 i = 0; i < contactDetailList.size(); i++)
   {
      contactsNames.push_back(getContactName(contactDetailList[i]));
   }

   _pAmbigContactList->setAmbigContactList(contactsNames);
}


void clSDS_Method_ContactsGetAmbiguityList::setAmbigNumbers(std::vector<sds2hmi_fi_tcl_CON_AmbiguityResultEntry> resultEntries)
{
   std::vector<std::string> contactsNumbers;

   for (tU32 i = 0; i < resultEntries.size(); i++)
   {
      contactsNumbers.push_back(clSDS_PhoneNumberFormatter::oFormatNumber(resultEntries[i].DisplayString.szValue));
   }

   _pAmbigNumberList->setAmbigNumberList(contactsNumbers);
}


void clSDS_Method_ContactsGetAmbiguityList::updateHeaderTagWithContactName(most_PhonBk_fi_types::T_PhonBkContactDetails contactDetails) const
{
   std::string fullName = getContactName(contactDetails);

   clSDS_StringVarList::vSetVariable("$(Name)", fullName);

   ETG_TRACE_USR4(("clSDS_Method_ContactsGetAmbiguityList::setContactName - fullName = %s", fullName.c_str()));
}


std::string clSDS_Method_ContactsGetAmbiguityList::getContactName(T_PhonBkContactDetails contactDetails) const
{
   std::string fullName;
   fullName.append(contactDetails.getSFirstName());
   fullName.append(" ");
   fullName.append(contactDetails.getSLastName());

   StringUtils::trim(fullName);

   return fullName;
}


static void addNumber(const std::string& number, tU32 contactID, sds2hmi_fi_tcl_e8_PHN_NumberType::tenType type, sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& result)
{
   if (!number.empty())
   {
      sds2hmi_fi_tcl_CON_AmbiguityResultEntry resultEntry;
      resultEntry.LocationType.enType = type;
      resultEntry.DisplayString = number.c_str();
      resultEntry.ContactID = contactID;
      result.ResultEntries.push_back(resultEntry);
   }
}


void clSDS_Method_ContactsGetAmbiguityList::setHomeEntries(tU32 contactID, T_PhonBkContactDetails contactDetails, sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& result) const
{
   addNumber(contactDetails.getSHomeNumber1(), contactID, sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_HOME1, result);
   addNumber(contactDetails.getSHomeNumber2(), contactID, sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_HOME2, result);
}


void clSDS_Method_ContactsGetAmbiguityList::setMobileEntries(tU32 contactID, T_PhonBkContactDetails contactDetails, sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& result) const
{
   addNumber(contactDetails.getSCellNumber1(), contactID, sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_MOBILE1, result);
   addNumber(contactDetails.getSCellNumber2(), contactID, sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_MOBILE2, result);
}


void clSDS_Method_ContactsGetAmbiguityList::setOfficeEntries(tU32 contactID, T_PhonBkContactDetails contactDetails, sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& result) const
{
   addNumber(contactDetails.getSWorkNumber1(), contactID, sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_OFFICE1, result);
   addNumber(contactDetails.getSWorkNumber2(), contactID, sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_OFFICE2, result);
}


void clSDS_Method_ContactsGetAmbiguityList::setOtherEntries(tU32 contactID, T_PhonBkContactDetails contactDetails, sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& result) const
{
   addNumber(contactDetails.getSOtherNumber(), contactID, sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_OTHER, result);
}


void clSDS_Method_ContactsGetAmbiguityList::setPreferredEntries(tU32 contactID, T_PhonBkContactDetails contactDetails, sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& result) const
{
   addNumber(contactDetails.getSPreferredNumber(), contactID, sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_UNKNOWN, result);
}


void clSDS_Method_ContactsGetAmbiguityList::setAllLocationsEntries(tU32 contactID, T_PhonBkContactDetails contactDetails, sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& result) const
{
   setHomeEntries(contactID, contactDetails, result);
   setOfficeEntries(contactID, contactDetails, result);
   setMobileEntries(contactID, contactDetails, result);
   setOtherEntries(contactID, contactDetails, result);
   setPreferredEntries(contactID, contactDetails, result);
}
