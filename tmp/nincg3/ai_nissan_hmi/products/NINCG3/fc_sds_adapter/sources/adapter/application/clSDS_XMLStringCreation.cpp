/*********************************************************************//**
 * \file       clSDS_XMLStringCreation.cpp
 *
 * clSDS_XMLStringCreation.cpp class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_XMLStringCreation.h"
#include "application/clSDS_ConfigurationFlags.h"
#include "libxml/xmlwriter.h"


/**************************************************************************//**

******************************************************************************/
static void removeLineBreaks(std::string& str)
{
   size_t pos = str.find("\n");
   while (pos != std::string::npos)
   {
      str.erase(pos, 1);
      pos = str.find("\n");
   }
}


/**************************************************************************//**

******************************************************************************/
static std::string formatXml(const GroupMap& groups)
{
   xmlBufferPtr bufptr = xmlBufferCreate();
   xmlTextWriterPtr wrtptr = xmlNewTextWriterMemory(bufptr, 0);
   xmlTextWriterStartDocument(wrtptr, NULL, "UTF-8", NULL);

   xmlTextWriterStartElement(wrtptr, (const xmlChar*) "SDS_CONFIG_PARAM");
   xmlTextWriterWriteAttribute(wrtptr, (const xmlChar*) "project", (const xmlChar*) "AIVI");

   GroupMap::const_iterator groupIter = groups.begin();
   while (groupIter != groups.end())
   {
      xmlTextWriterStartElement(wrtptr, (const xmlChar*)"CONFIG_GROUP");
      xmlTextWriterWriteAttribute(wrtptr, (const xmlChar*) "name", (const xmlChar*) groupIter->first.c_str());
      xmlTextWriterStartElement(wrtptr, (const xmlChar*) "PARAMETERS");
      ConfigItemMap itemMap = groupIter->second;
      ConfigItemMap::const_iterator itemIter = itemMap.begin();
      while (itemIter != itemMap.end())
      {
         xmlTextWriterStartElement(wrtptr, (const xmlChar*) "PARAMETER");
         xmlTextWriterWriteElement(wrtptr, (const xmlChar*) "NAME", (const xmlChar*) itemIter->first.c_str());
         xmlTextWriterWriteFormatElement(wrtptr, (const xmlChar*) "VALUE", "%lu", itemIter->second);
         xmlTextWriterEndElement(wrtptr);
         ++itemIter;
      }
      xmlTextWriterEndElement(wrtptr);
      xmlTextWriterEndElement(wrtptr);
      ++groupIter;
   }
   xmlTextWriterEndElement(wrtptr);
   xmlTextWriterEndDocument(wrtptr);
   xmlTextWriterFlush(wrtptr);
   std::string str = (const char*) bufptr->content;
   xmlFreeTextWriter(wrtptr);
   xmlBufferFree(bufptr);
   removeLineBreaks(str);
   return str;
}


/**************************************************************************//**
*Destructor
******************************************************************************/
clSDS_XMLStringCreation::~clSDS_XMLStringCreation()
{
}


/**************************************************************************//**
*Constructor
******************************************************************************/
clSDS_XMLStringCreation::clSDS_XMLStringCreation()
{
}


/**************************************************************************//**

******************************************************************************/
std::string clSDS_XMLStringCreation::getStaticXmlString()
{
   GroupMap staticGroups;

   staticGroups["NAV"]["B_NAV"] = clSDS_ConfigurationFlags::getNavKey();
   staticGroups["NAV"]["B_NAV_ADRS"] = clSDS_ConfigurationFlags::getNavAdrsKey();
   staticGroups["NAV"]["B_NAV_POI"] = clSDS_ConfigurationFlags::getPOIKey();
   staticGroups["NAV"]["B_NAV_POICAT"] = clSDS_ConfigurationFlags::getPOICatKey();
   staticGroups["NAV"]["B_NAV_CHNG_STATE"] = clSDS_ConfigurationFlags::getNavStateAvailable();
   staticGroups["NAV"]["B_NAV_CHNG_CNTRY"] = clSDS_ConfigurationFlags::getNavCountryAvailable();

   staticGroups["INF"]["B_INF"] = clSDS_ConfigurationFlags::getInfKey();
   staticGroups["INF"]["B_INF_HEV"] = clSDS_ConfigurationFlags::getInfHEVKey();
   staticGroups["INF"]["B_INF_TRA"] = clSDS_ConfigurationFlags::getInfTrafficKey();

   staticGroups["AUD"]["B_AUD_DAB"] = clSDS_ConfigurationFlags::getAudDABKey();
   staticGroups["AUD"]["B_AUD_SXM"] = clSDS_ConfigurationFlags::getAudSXMKey();
   staticGroups["AUD"]["B_AUD_RDS"] = clSDS_ConfigurationFlags::getAudRDSKey();
   staticGroups["AUD"]["B_AUD_HD"] = clSDS_ConfigurationFlags::getAudHDKey();
   staticGroups["AUD"]["E_AUD_FREQCY_TYPE"] = clSDS_ConfigurationFlags::getAudFreqTypeKey(); //TODO raa8hi - to be removed - flag no longer exist in the dialog spec

   staticGroups["PHO"]["B_PHO"] = clSDS_ConfigurationFlags::getBluetoothPhoneEnabledKey();
   staticGroups["PHO"]["B_PHO_BT_AVAILABLE"] = clSDS_ConfigurationFlags::getBluetoothPhoneEnabledKey(); //TODO raa8hi to be removed because B_PHO_BT_AVAILABLE --> B_PHO

   addRootContextCommandFlags(staticGroups);

   return formatXml(staticGroups);
}


/**************************************************************************//**

******************************************************************************/
std::string clSDS_XMLStringCreation::getDynamicXmlString(const SDSConfigData& cfgData)
{
   //TODO raa8hi: check with Denny and remove the flags below - they no longer exist in the dialog spec

   GroupMap dynamicGroups;

   dynamicGroups["INF"]["E_INF_CNT_TO_TCU_IN_INFO"] = cfgData.tcuType;
   dynamicGroups["AUD"]["B_AUD_Nbest"] = cfgData.audioNBestMatch;
   dynamicGroups["PHO"]["B_PHO_Nbest"] = cfgData.phoneBookNBestMatch;
   dynamicGroups["GRL"]["B_GRL_BEEPONLY_MODE"] = cfgData.beepOnlyMode;

   return formatXml(dynamicGroups);
}


/**************************************************************************//**
Root Context Phone expert mode tags
******************************************************************************/
void clSDS_XMLStringCreation::addRootContextCommandFlags(GroupMap& staticGroups)
{
   staticGroups["PHO"]["B_PHO_RC_VoiceMail"] = 1;
   staticGroups["PHO"]["B_PHO_RC_VoiceAssistant"] = 1;
   staticGroups["PHO"]["B_PHO_RC_Siri"] = 1;
   staticGroups["PHO"]["B_PHO_RC_SendText"] = 1;
   staticGroups["PHO"]["B_PHO_RC_SelectPhone"] = 1;
   staticGroups["PHO"]["B_PHO_RC_Redial"] = 1;
   staticGroups["PHO"]["B_PHO_RC_RecentCalls"] = 1;
   staticGroups["PHO"]["B_PHO_RC_ReadText"] = 1;
   staticGroups["PHO"]["B_PHO_RC_Quickdial"] = 1;
   staticGroups["PHO"]["B_PHO_RC_Phonebook"] = 1;
   staticGroups["PHO"]["B_PHO_RC_OutgoingCalls"] = 1;
   staticGroups["PHO"]["B_PHO_RC_MissedCalls"] = 1;
   staticGroups["PHO"]["B_PHO_RC_IncomingCalls"] =  1;
   staticGroups["PHO"]["B_PHO_RC_Call"] = 1;

   staticGroups["NAV"]["B_NAV_RC_Zoom"] = 1;
   staticGroups["NAV"]["B_NAV_RC_Work"] = 1;
   staticGroups["NAV"]["B_NAV_RC_VoiceGuidance"] = 1;
   staticGroups["NAV"]["B_NAV_RC_TurnMap"] = 1;
   staticGroups["NAV"]["B_NAV_RC_StreetAddress"] = 1;
   staticGroups["NAV"]["B_NAV_RC_RepeatInstruction"] = 1;
   staticGroups["NAV"]["B_NAV_RC_ReRoute"] = 1;
   staticGroups["NAV"]["B_NAV_RC_PreviousDestinations"] = 1;
   staticGroups["NAV"]["B_NAV_RC_POICategory"] = 1;
   staticGroups["NAV"]["B_NAV_RC_POI"] = 1;
   staticGroups["NAV"]["B_NAV_RC_Nearby"] = 1;
   staticGroups["NAV"]["B_NAV_RC_Map"] = 1;
   staticGroups["NAV"]["B_NAV_RC_Icons"] = 1;
   staticGroups["NAV"]["B_NAV_RC_Home"] = 1;
   staticGroups["NAV"]["B_NAV_RC_CityCenter"] = 1;
   staticGroups["NAV"]["B_NAV_RC_CancelRoute"] = 1;
   staticGroups["NAV"]["B_NAV_RC_AddressBook"] = 1;
   staticGroups["NAV"]["B_NAV_RC_ChangeCountry"] = 1;

   staticGroups["INF"]["B_INF_RC_WhereAmI"] = 1;
   staticGroups["INF"]["B_INF_RC_WeatherMap"] = 1;
   staticGroups["INF"]["B_INF_RC_VoiceMenu"] = 1;
   staticGroups["INF"]["B_INF_RC_Traffic"]  = 1;
   staticGroups["INF"]["B_INF_RC_Stocks"] = 1;
   staticGroups["INF"]["B_INF_RC_Sports"] = 1;
   staticGroups["INF"]["B_INF_RC_Parking"] = 1;
   staticGroups["INF"]["B_INF_RC_MovieTheaters"] = 1;
   staticGroups["INF"]["B_INF_RC_FuelEconomy"] = 1;
   staticGroups["INF"]["B_INF_RC_FuelPrices"] = 1;
   staticGroups["INF"]["B_INF_RC_EnergyFlow"] = 1;
   staticGroups["INF"]["B_INF_RC_CurrentWeather"] = 1;
   staticGroups["INF"]["B_INF_RC_ClimateControl"] = 1;
   staticGroups["INF"]["B_INF_RC_6HourForecast"]  = 1;
   staticGroups["INF"]["B_INF_RC_5DayForecast"] = 1;
   staticGroups["INF"]["B_INF_RC_InformationFeeds"] = 1;
   staticGroups["INF"]["B_INF_RC_Maintenance"] = 1;
   staticGroups["INF"]["B_INF_RC_Temperature"] = 1;
   staticGroups["INF"]["B_INF_RC_TirePressure"] = 1;

   staticGroups["AUD"]["B_AUD_RC_TuneSXM"] = 1;
   staticGroups["AUD"]["B_AUD_RC_TuneFM"] = 1;
   staticGroups["AUD"]["B_AUD_RC_TuneAM"] = 1;
   staticGroups["AUD"]["B_AUD_RC_TuneDAB"] = 1;
   staticGroups["AUD"]["B_AUD_RC_TuneDRM"] = 1;
   staticGroups["AUD"]["B_AUD_RC_Preset"] = 1;
   staticGroups["AUD"]["B_AUD_RC_PlaySource"] = 1;
   staticGroups["AUD"]["B_AUD_RC_MusicSearch"] = 1;
   staticGroups["AUD"]["B_AUD_RC_CDTrack"] = 1;
   staticGroups["AUD"]["B_AUD_RC_TV"] = 1;
}
