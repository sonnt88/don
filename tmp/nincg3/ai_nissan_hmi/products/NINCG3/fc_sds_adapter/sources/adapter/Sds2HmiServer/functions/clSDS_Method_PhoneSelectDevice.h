/*********************************************************************//**
 * \file       clSDS_Method_PhoneSelectDevice.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_PhoneSelectDevice_h
#define clSDS_Method_PhoneSelectDevice_h


#include "Sds2HmiServer/framework/clServerMethod.h"


class clSDS_Method_PhoneSelectDevice : public clServerMethod
{
   public:
      virtual ~clSDS_Method_PhoneSelectDevice();
      clSDS_Method_PhoneSelectDevice(ahl_tclBaseOneThreadService* pService);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
};


#endif
