/*********************************************************************//**
 * \file       clSDS_Method_TextMsgSetContent.cpp
 *
 * clSDS_Method_TextMsgSetContent method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_TextMsgSetContent.h"
#include "external/sds2hmi_fi.h"
#include "application/clSDS_TextMsgContent.h"
#include "view_db/Sds_TextDB.h"


static const Sds_TextId predefinedSms[] =
{
   SDS_TEXT_DRIVING_CAN_T_TEXT,
   SDS_TEXT_CALL_ME,
   SDS_TEXT_ON_MY_WAY,
   SDS_TEXT_RUNNING_LATE,
   SDS_TEXT_OKAY,
   SDS_TEXT_YES,
   SDS_TEXT_NO,
   SDS_TEXT_WHERE_ARE_YOU,
   SDS_TEXT_WHEN,
};


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_TextMsgSetContent::~clSDS_Method_TextMsgSetContent()
{
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_TextMsgSetContent::clSDS_Method_TextMsgSetContent(ahl_tclBaseOneThreadService* pService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_TEXTMSGSETCONTENT, pService)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_TextMsgSetContent::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgTextMsgSetContentMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);

   std::string sms = getSmsString(oMessage.nContentID);
   clSDS_TextMsgContent::setTextMsgContent(sms);

   vSendMethodResult();
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_Method_TextMsgSetContent::getSmsString(uint32 listIndex) const
{
   const uint32 SMS_LIST_SIZE = sizeof(predefinedSms) / sizeof(predefinedSms[0]);
   if ((listIndex > 0) && (listIndex <= SMS_LIST_SIZE))
   {
      return Sds_TextDB_vGetText(predefinedSms[listIndex - 1]);
   }
   return "";
}
