/*********************************************************************//**
 * \file       clSDS_Property_CommonSettingsRequest.cpp
 *
 * Common Action Request property implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Property_CommonSettingsRequest.h"
#include "external/sds2hmi_fi.h"
#include "application/SettingsService.h"

/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Property_CommonSettingsRequest::~clSDS_Property_CommonSettingsRequest()
{
   delete _pofiU32Value;
   _pofiU32Value = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Property_CommonSettingsRequest::clSDS_Property_CommonSettingsRequest(ahl_tclBaseOneThreadService* pService, SettingsService* settingsService):
   clServerProperty(SDS2HMI_SDSFI_C_U16_COMMONSETTINGSREQUEST, pService), _promptMode(false)


{
   _pofiU32Value  = new sds2hmi_fi_tclU32();
   settingsService->addPromptModeObserver(this);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonSettingsRequest::vSet(amt_tclServiceData* /*pInMsg*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonSettingsRequest::vGet(amt_tclServiceData*  /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonSettingsRequest::vUpreg(amt_tclServiceData*  /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonSettingsRequest::vSendStatus()
{
   sds2hmi_sdsfi_tclMsgCommonSettingsRequestStatus oMessage;
   oMessage.Settings.clear();
   oMessage.Settings.resize(0);
   oMessage.Action.enType = sds2hmi_fi_tcl_e8_ParameterAction::FI_EN_SET;
   sds2hmi_fi_tcl_SDS_SettingsRequest oSettingStatus;
   sds2hmi_fi_tcl_e8_SDSSettings oSettings;
   oSettings.enType = sds2hmi_fi_tcl_e8_SDSSettings::FI_EN_INFOSET_PROMPTMODE;
   oSettingsUnion.ParameterType.enType = sds2hmi_fi_tcl_e8_SDS_ParameterType::FI_EN_TU32;
   if (_pofiU32Value != NULL)
   {
      if (_promptMode)
      {
         _pofiU32Value->u32Value = sds2hmi_fi_tcl_e8_PromptSetting::FI_EN_SHORT;
      }
      else
      {
         _pofiU32Value->u32Value = sds2hmi_fi_tcl_e8_PromptSetting::FI_EN_LONG;
      }
      oSettingsUnion.poParameterData = _pofiU32Value;
   }
   oSettingStatus.Setting = oSettings;
   oSettingStatus.Value = oSettingsUnion;
   oMessage.Settings.push_back(oSettingStatus);
   vStatus(oMessage);
}


void clSDS_Property_CommonSettingsRequest::promptModeChanged(bool promptMode)
{
   _promptMode = promptMode;
   vSendStatus();
}
