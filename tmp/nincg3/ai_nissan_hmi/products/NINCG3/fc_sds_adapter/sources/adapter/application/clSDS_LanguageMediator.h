/*********************************************************************//**
 * \file       clSDS_LanguageMediator.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_LanguageMediator_h
#define clSDS_LanguageMediator_h


#include "application/clSDS_SDSStatusObserver.h"
#include "application/SettingsObserver.h"
#include "application/SettingsService.h"
#include "external/sds2hmi_fi.h"
#include "VEHICLE_MAIN_FIProxy.h"


class clSDS_SDSStatus;
class clSDS_MyAppsList;
class Sds2VehicleData;
class clSDS_SessionControl;
class clSDS_SdsControl;
class SettingsService;


class clSDS_LanguageMediator
   : public clSDS_SDSStatusObserver
   , public SettingsObserver
   , public asf::core::ServiceAvailableIF
   , public VEHICLE_MAIN_FI::LanguageCallbackIF
   , public VEHICLE_MAIN_FI::SetLanguageCallbackIF
{
   public:
      virtual ~clSDS_LanguageMediator();
      clSDS_LanguageMediator(
         clSDS_SDSStatus* pSDSStatus,
         SettingsService& settingsService,
         boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >vehicleProxy);

      tVoid setAvailableSpeakers(const bpstl::vector<sds2hmi_fi_tcl_LanguageAndSpeaker>& availableSpeakers);
      tVoid setActiveSpeaker(tU16 activeSpeaker);
      tVoid setActionRequestAvailable(tBool isAvailable);

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onLanguageError(
         const ::boost::shared_ptr< VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy,
         const ::boost::shared_ptr< VEHICLE_MAIN_FI::LanguageError >& error);
      virtual void onLanguageStatus(
         const ::boost::shared_ptr< VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy,
         const ::boost::shared_ptr< VEHICLE_MAIN_FI::LanguageStatus >& status);

      virtual void onSetLanguageError(
         const ::boost::shared_ptr< VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy,
         const ::boost::shared_ptr< VEHICLE_MAIN_FI::SetLanguageError >& error);
      virtual void onSetLanguageResult(
         const ::boost::shared_ptr< VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy,
         const ::boost::shared_ptr< VEHICLE_MAIN_FI::SetLanguageResult >& result);

      void vSetSDSControl(clSDS_SdsControl* pSdsControl);

   private:
      tVoid vSDSStatusChanged();
      void onSettingsChanged();
      tVoid vSynchroniseSdsLanguage();
      tBool bSdsIsReadyToSetLanguage() const;
      tBool bTooManyRetries() const;
      tVoid sendSpeakerRequest(tU16 speakerId);
      tU16 getSpeakerIdForLanguage(const sds2hmi_fi_tcl_SDSLanguageID& language, sds2hmi_fi_tcl_e8_Gender gender);
      tVoid updateSdsLanguageInVehicleData();
      sds2hmi_fi_tcl_SDSLanguageID getLanguageOfActiveSpeaker() const;
      sds2hmi_fi_tcl_e8_Gender getSdsGender(sds_gui_fi::SettingsService::ActiveSpeakerGender guiGender) const;

      tBool _actionRequestAvailable;
      tBool _bWaitingForResult;
      tU32 _u32RetryCounter;
      vehicle_main_fi_types::T_e8_Language_Code _systemLanguage;
      tU16 _activeSpeaker;
      clSDS_SDSStatus* _pSDSStatus;
      SettingsService& _settingsService;
      bpstl::vector<sds2hmi_fi_tcl_LanguageAndSpeaker> _availableSpeakers;
      sds2hmi_fi_tcl_e8_Gender _preferredGender;
      boost::shared_ptr< VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy > _vehicleProxy;
      clSDS_SdsControl* _pSdsControl;
};


#endif
