/*********************************************************************//**
 * \file       clSDS_Method_NaviSetDestinationAsWaypoint.cpp
 *
 * clSDS_Method_NaviSetDestinationAsWaypoint method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_NaviSetDestinationAsWaypoint.h"
#include "application/clSDS_NaviListItems.h"

using namespace org::bosch::cm::navigation::NavigationService;
/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_NaviSetDestinationAsWaypoint::~clSDS_Method_NaviSetDestinationAsWaypoint()
{
   _pMethod_NaviGetWaypointListInfo = NULL;
   _pNaviListItems = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_NaviSetDestinationAsWaypoint::clSDS_Method_NaviSetDestinationAsWaypoint(
   ahl_tclBaseOneThreadService* pService,
   clSDS_Method_NaviGetWaypointListInfo* pWayPointListInfo,
   ::boost::shared_ptr< NavigationServiceProxy> pNaviProxy,
   GuiService& guiService,
   clSDS_NaviListItems* poNaviListItems)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_NAVISETDESTINATIONASWAYPOINT, pService)
   , _pMethod_NaviGetWaypointListInfo(pWayPointListInfo)
   , _navigationProxy(pNaviProxy)
   , _guiService(guiService)
   , _pNaviListItems(poNaviListItems)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSetDestinationAsWaypoint::onInsertWaypointError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< InsertWaypointError >& /*error*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSetDestinationAsWaypoint::onInsertWaypointResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< InsertWaypointResponse >& /*response*/)
{
   _pMethod_NaviGetWaypointListInfo->onSetAddAsWayPoint(TRUE);
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSetDestinationAsWaypoint::onReplaceWaypointError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< ReplaceWaypointError >& /*error*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSetDestinationAsWaypoint::onReplaceWaypointResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< ReplaceWaypointResponse >& /*response*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSetDestinationAsWaypoint::onShowDeleteWaypointListScreenError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< ShowDeleteWaypointListScreenError >& /*error*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviSetDestinationAsWaypoint::onShowDeleteWaypointListScreenResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< ShowDeleteWaypointListScreenResponse >& /*response*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviSetDestinationAsWaypoint::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   sds2hmi_sdsfi_tclMsgNaviSetDestinationAsWaypointMethodResult oMessage;
   oMessage.nWaypointMustBeDeleted = FALSE;
   tU32 u32Indexvalue = 1;
   if (_pMethod_NaviGetWaypointListInfo->waypointListIsFull())
   {
      //Send request to Navi
      _navigationProxy->sendShowDeleteWaypointListScreenRequest(*this);
      oMessage.nWaypointMustBeDeleted = TRUE;
   }
   else
   {
      if (_pNaviListItems != NULL)
      {
         clSDS_NaviListItems::enNaviListType nListType = _pNaviListItems->vGetNaviListType();

         switch (nListType)
         {
            case clSDS_NaviListItems::EN_NAVI_LIST_PREV_DESTINATIONS:
            case clSDS_NaviListItems::EN_NAVI_LIST__ADDRESSBOOK:
            case clSDS_NaviListItems::EN_NAVI_LIST_FUEL_PRICES:
            {
               _pNaviListItems->vAddasWayPoint();
            }
            break;
            default:
               break;
         }
      }

      _navigationProxy->sendInsertWaypointRequest(*this, u32Indexvalue);
      _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
   }

   vSendMethodResult(oMessage);
}
