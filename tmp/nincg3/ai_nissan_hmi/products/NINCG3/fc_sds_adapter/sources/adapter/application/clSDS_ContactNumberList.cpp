/*********************************************************************//**
 * \file       clSDS_ContactNumberList.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_ContactNumberList.h"
#include "application/clSDS_Iconizer.h"
#include "application/clSDS_PhoneNumberFormatter.h"
#include "application/clSDS_PhonebookList.h"
#include "application/clSDS_StringVarList.h"
#include "external/sds2hmi_fi.h"


/**************************************************************************//**
*Destructor
******************************************************************************/
clSDS_ContactNumberList::~clSDS_ContactNumberList()
{
   _pPhonebookList = NULL;
}


/**************************************************************************//**
*Constructor
******************************************************************************/
clSDS_ContactNumberList::clSDS_ContactNumberList(clSDS_PhonebookList* pPhonebookList) : _pPhonebookList(pPhonebookList)
{
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_ContactNumberList::bSelectElement(tU32 u32SelectedIndex)
{
   clSDS_StringVarList::vSetVariable("$(Contact)", oGetConfirmationItem(u32SelectedIndex));
   return TRUE;
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::vector<clSDS_ListItems> clSDS_ContactNumberList::oGetItems(tU32 u32StartIndex, tU32 u32EndIndex)
{
   bpstl::vector<clSDS_ListItems> oListItems;
   if (u32StartIndex < u32GetSize())
   {
      for (tU32 u32Index = u32StartIndex; u32Index < bpstl::min(u32EndIndex, u32GetSize()); u32Index++)
      {
         clSDS_ListItems oListItem;
         oListItem.oDescription.szString = oGetItem(u32Index);
         oListItems.push_back(oListItem);
      }
   }
   return oListItems;
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::string clSDS_ContactNumberList::oGetItem(tU32 u32Index) const
{
   if (_pPhonebookList != NULL)
   {
      bpstl::string oNumber(_pPhonebookList->oGetPhoneNumbers().oPBDetails_TelNumber[u32Index]);
      if (!oNumber.empty())
      {
         tU8 u8IconType = _pPhonebookList->oGetPhoneNumbers().u8PBDetails_IconType[u32Index];
         return clSDS_Iconizer::oAddIconPrefix(u8IconType, clSDS_PhoneNumberFormatter::oFormatNumber(oNumber));
      }
   }
   return " ";
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_ContactNumberList::u32GetSize()
{
   if (_pPhonebookList != NULL)
   {
      return _pPhonebookList->u32GetContactDetailCount();
   }
   else
   {
      return 0;
   }
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::string clSDS_ContactNumberList::oGetConfirmationItem(tU32 u32Index) const
{
   if ((_pPhonebookList != NULL) && (u32Index > 0))
   {
      bpstl::string oNumber(_pPhonebookList->oGetPhoneNumbers().oPBDetails_TelNumber[u32Index - 1]);
      bpstl::string oName(_pPhonebookList->oGetPhoneNumbers().oPBDetails_Name);
      tU8 u8IconType = _pPhonebookList->oGetPhoneNumbers().u8PBDetails_IconType[u32Index - 1];

      return clSDS_Iconizer::oAddIconInfix(oName, u8IconType, clSDS_PhoneNumberFormatter::oFormatNumber(oNumber));
   }
   else
   {
      return ("");
   }
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::string clSDS_ContactNumberList::oGetSelectedItem(tU32 u32Index)
{
   if ((_pPhonebookList != NULL) && (u32Index > 0))
   {
      return _pPhonebookList->oGetPhoneNumbers().oPBDetails_TelNumber[u32Index - 1];
   }
   return "";
}


tVoid clSDS_ContactNumberList::vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType)
{
   NORMAL_M_ASSERT(listType == sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_CONTACT_AMBIGUITY);

   if (_pPhonebookList != NULL)
   {
      _pPhonebookList->vUpdateHeadLineTagForSingleContact();
   }
   notifyListObserver();
}
