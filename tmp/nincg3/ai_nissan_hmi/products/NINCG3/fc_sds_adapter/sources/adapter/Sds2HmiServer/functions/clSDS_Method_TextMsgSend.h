/*********************************************************************//**
 * \file       clSDS_Method_TextMsgSend.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_TextMsgSend_h
#define clSDS_Method_TextMsgSend_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "MOST_Msg_FIProxy.h"
#include "MOST_PhonBk_FIProxy.h"


class clSDS_Method_TextMsgSend : public clServerMethod
   , public MOST_Msg_FI::CreateMessageCallbackIF
   , public MOST_Msg_FI::ProvideMessageHeaderCallbackIF
   , public MOST_Msg_FI::ProvideMessageBodyCallbackIF
   , public MOST_Msg_FI::SendMessageCallbackIF
   , public asf::core::ServiceAvailableIF
   , public MOST_PhonBk_FI::DevicePhoneBookFeatureSupportCallbackIF
{
   public:
      virtual ~clSDS_Method_TextMsgSend();
      clSDS_Method_TextMsgSend(ahl_tclBaseOneThreadService* pService,
                               ::boost::shared_ptr< ::MOST_Msg_FI::MOST_Msg_FIProxy > pSds2MsgProxy, ::boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy> pPhoneBookProxy);
      tVoid vReplySendSMSResult(tU8 u8SendSmsResult);

      virtual void onAvailable(
         const ::boost::shared_ptr< asf::core::Proxy >& proxy,
         const asf::core::ServiceStateChange& stateChange) ;
      virtual void onUnavailable(
         const ::boost::shared_ptr< asf::core::Proxy >& proxy,
         const asf::core::ServiceStateChange& stateChange) ;

      virtual void onCreateMessageError(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::CreateMessageError >& error);
      virtual void onCreateMessageResult(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::CreateMessageResult >& result);

      virtual void onProvideMessageHeaderError(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::ProvideMessageHeaderError >& error);
      virtual void onProvideMessageHeaderResult(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::ProvideMessageHeaderResult >& result);

      virtual void onProvideMessageBodyError(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::ProvideMessageBodyError >& error);
      virtual void onProvideMessageBodyResult(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::ProvideMessageBodyResult >& result);

      virtual void onSendMessageError(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::SendMessageError >& error);
      virtual void onSendMessageResult(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::SendMessageResult >& result);

      virtual void onDevicePhoneBookFeatureSupportError(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::DevicePhoneBookFeatureSupportError >& error);
      virtual void onDevicePhoneBookFeatureSupportStatus(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::DevicePhoneBookFeatureSupportStatus >& status);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      void clearNewMessageDetails();
      boost::shared_ptr< ::MOST_Msg_FI::MOST_Msg_FIProxy > _sds2MsgProxy;
      boost::shared_ptr<MOST_PhonBk_FI::MOST_PhonBk_FIProxy> _phoneBookProxy;

      most_Msg_fi_types::T_MsgMessageHandle _messageHandle;
      uint8 _createMessageHandle;
      std::vector< most_Msg_fi_types::T_MsgAddressFieldItem > _msgAddressField;
      std::string _msgSubject;
      most_Msg_fi_types::T_MsgMessageBodyText _messageBody;
      std::vector< uint16 > _msgAttachmentHandleStream;
};


#endif
