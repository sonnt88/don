/*********************************************************************//**
 * \file       Sds_ViewDB.cpp
 *
 * Exported interface of the view data base.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "view_db/Sds_ViewDB.h"
#include "view_db/Sds_TextDB.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/Sds_ViewDB.cpp.trc.h"
#endif


struct SubCommand
{
   Sds_ConditionId enVisibe;
   Sds_TextId enTextId;
};


struct Command
{
   Sds_ConditionId enVisibe;
   Sds_ConditionId enSelectable;
   const char* grammarId;
   const char* promptId;
   Sds_TextId enTextId;
   const SubCommand* subCommandList;
};


struct View
{
   const char* idName;
   char layout;
   Sds_TextId headline;
   Sds_TextId helpline;
   Sds_TextId info;
   const Command* commandList;
};


#define SUB_COMMAND_LIST_BEGIN(command)                     static const SubCommand subCommandList_##command[] = {
#define SUB_COMMAND(condition, textId)                         { condition, textId },
#define SUB_COMMAND_LIST_END                                   { SDS_CONDITION_ID_LIMIT, SDS_TEXT_ID_LIMIT } };
#include "Sds_ViewDB.dat"
#undef SUB_COMMAND_LIST_BEGIN
#undef SUB_COMMAND
#undef SUB_COMMAND_LIST_END


#define subCommandList_NONE                                                     NULL
#define COMMAND_LIST_BEGIN(id)                                                  static const Command commandList_##id[] = {
#define COMMAND(visible, selectable, grammarId, promptId, textId, command)      { visible, selectable, #grammarId, #promptId, textId, subCommandList_##command },
#define COMMAND_LIST_END                                                        { SDS_CONDITION_ID_LIMIT, SDS_CONDITION_ID_LIMIT, "0", "0", SDS_TEXT_ID_LIMIT, subCommandList_NONE } };
#include "Sds_ViewDB.dat"
#undef subCommandList_NONE
#undef COMMAND_LIST_BEGIN
#undef COMMAND
#undef COMMAND_LIST_END


static const View viewList[] =
{
#define SDS_VIEW(id, layout, headline, helpline, info)             		{ #id, layout, headline, helpline, info, commandList_##id },
#include "Sds_ViewDB.dat"
#undef SDS_VIEW
};


struct Condition
{
   const char* operation;
   const void* left;
   const void* right;
};


static const Condition g_aConditionTable[SDS_CONDITION_ID_LIMIT] =
{
//lint -esym(750, AND) "local macro not referenced - jnd2hi"
#define AND(left, right)                                "AND", (const void*) left, (const void*) left
#define EQUALS(left, right)                             "EQUALS", (const void*) #left, (const void*) #right
#define SDS_COND(id, expression)                        { expression },
#include "Sds_ViewDB.dat"
#undef SDS_COND
};


static const char* const aTemplateNames[] =
{
#define SDS_HEADER_TEMPLATES(id, textId, templateName)                  #templateName,
#include "Sds_ViewDB.dat"
#undef SDS_HEADER_TEMPLATES
};


/**
 * maps a template id to a text id
 */
static const tU16 g_aTemplateTextIds[SDS_HEADER_TEMPLATE_ID_LIMIT] =
{
#define SDS_HEADER_TEMPLATES(id, textId, templateName)      textId,
#include "Sds_ViewDB.dat"
#undef SDS_HEADER_TEMPLATES
};


/**************************************************************************//**
*
******************************************************************************/
const tChar* Sds_ViewDB_pcGetName(Sds_ViewId enViewId)
{
   if (enViewId < SDS_VIEW_ID_LIMIT)
   {
      return viewList[enViewId].idName;
   }
   return "<invalid view id>";
}


/**************************************************************************//**
*
******************************************************************************/
tChar Sds_ViewDB_cGetLayout(Sds_ViewId enViewId)
{
   if (enViewId < SDS_VIEW_ID_LIMIT)
   {
      return viewList[enViewId].layout;
   }
   return '?';
}


/**************************************************************************//**
*
******************************************************************************/
Sds_TextId Sds_ViewDB_enGetHeadlineTextId(Sds_ViewId enView)
{
   if (enView < SDS_VIEW_ID_LIMIT)
   {
      return viewList[enView].headline;
   }
   return SDS_TEXT_ID_LIMIT;
}


/**************************************************************************//**
*
******************************************************************************/
static Condition Sds_ViewDB_stGetCondition(Sds_ConditionId enConditionId)
{
   return g_aConditionTable[enConditionId];
}


/**************************************************************************//**
*
******************************************************************************/
static tU16 Sds_ViewDB_u16GetTemplateTextId(Sds_HeaderTemplateId enTemplateId)
{
   return g_aTemplateTextIds[enTemplateId];
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::string Sds_ViewDB_oGetTemplateString(Sds_HeaderTemplateId enTemplateId)
{
   tU16 u16TextID = Sds_ViewDB_u16GetTemplateTextId((enTemplateId));
   return Sds_TextDB_vGetText((Sds_TextId) u16TextID);
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::string Sds_ViewDB_oGetHeadline(Sds_ViewId enViewId)
{
   return Sds_TextDB_vGetText(Sds_ViewDB_enGetHeadlineTextId(enViewId));
}


/***********************************************************************//**
*
***************************************************************************/
Sds_ViewId Sds_ViewDB_enFindViewId(const bpstl::string& oScreenId)
{
   const tU32 numberOfViews = sizeof viewList / sizeof viewList[0];
   for (tU32 u32Index = 0; u32Index < numberOfViews; u32Index++)
   {
      if (oScreenId == viewList[u32Index].idName)
      {
         return (Sds_ViewId) u32Index;
      }
   }

   if (oScreenId.find("Confirm") != std::string::npos)
   {
      ETG_TRACE_USR1(("warning: mapped screenId '%s' to 'SR_GLO_Confirm'", oScreenId.c_str()));
      return SR_GLO_Confirm;
   }
   if (oScreenId.find("Query") != std::string::npos)
   {
      ETG_TRACE_USR1(("warning: mapped screenId '%s' to 'SR_GLO_Query'", oScreenId.c_str()));
      return SR_GLO_Query;
   }
   if ((oScreenId.find("NbestList") != std::string::npos) ||
         (oScreenId.find("NBestList") != std::string::npos))
   {
      ETG_TRACE_USR1(("warning: mapped screenId '%s' to 'SR_GLO_NbestList'", oScreenId.c_str()));
      return SR_GLO_Nbest_List;
   }
   if (oScreenId == "SR_SMS_SendText")
   {
      ETG_TRACE_USR1(("warning: mapped screenId '%s' to 'SR_PHO_SelectMessage'", oScreenId.c_str()));
      return SR_PHO_SelectMessage;
   }
   if (oScreenId == "SR_PHO_DialNumber")
   {
      ETG_TRACE_USR1(("warning: mapped screenId '%s' to 'SR_PHO_DialNumberSendText'", oScreenId.c_str()));
      return SR_PHO_DialNumberSendText;
   }
   if (oScreenId == "SR_PHO_DialNumber2")
   {
      ETG_TRACE_USR1(("warning: mapped screenId '%s' to 'SR_PHO_SelectMessage'", oScreenId.c_str()));
      return SR_PHO_DialNumberSendText2;
   }

   ETG_TRACE_FATAL(("error: unknown screen id '%s'", oScreenId.c_str()));
   return SDS_VIEW_ID_LIMIT;
}


/***********************************************************************//**
*
***************************************************************************/
Sds_HeaderTemplateId Sds_ViewDB_enFindTemplateId(const bpstl::string& oTemplateId)
{
   const tU32 numberOfTemplates = sizeof aTemplateNames / sizeof aTemplateNames[0];
   for (tU32 u32Index = 0; u32Index < numberOfTemplates; u32Index++)
   {
      if (oTemplateId == aTemplateNames[u32Index])
      {
         return (Sds_HeaderTemplateId) u32Index;
      }
   }
   return SDS_HEADER_TEMPLATE_ID_LIMIT;
}


/***********************************************************************//**
*
***************************************************************************/
static std::string convertTrueFalse(const std::string& str)
{
   if (str == "TRUE")
   {
      return "1";
   }
   if (str == "FALSE")
   {
      return "0";
   }
   return str;
}


/***********************************************************************//**
*
***************************************************************************/
static bool conditionValuesMatch(const std::string& str1, const std::string& str2)
{
   return (convertTrueFalse(str1) == convertTrueFalse(str2));
}


/***********************************************************************//**
*
***************************************************************************/
tBool Sds_ViewDB_bIsConditionTrue(Sds_ConditionId conditionId, bpstl::map<bpstl::string, bpstl::string> const& variables)
{
   if (conditionId == SDS_COND__DEFAULT)
   {
      return TRUE;
   }

   const Condition& condition = Sds_ViewDB_stGetCondition(conditionId);
   bpstl::string operation = condition.operation;

   if (operation == "EQUALS")
   {
      bpstl::string left = (const char*) condition.left;
      bpstl::string right = (const char*) condition.right;

      bpstl::map<bpstl::string, bpstl::string>::const_iterator iter;
      for (iter = variables.begin(); iter != variables.end(); ++iter)
      {
         if ((iter->first == left) && conditionValuesMatch(iter->second, right))
         {
            return TRUE;
         }
      }
   }
   if (operation == "AND")
   {
      Sds_ConditionId left = (Sds_ConditionId)(unsigned int) condition.left;
      Sds_ConditionId right = (Sds_ConditionId)(unsigned int) condition.right;
      return Sds_ViewDB_bIsConditionTrue(left, variables) && Sds_ViewDB_bIsConditionTrue(right, variables);
   }

   return FALSE;
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::vector<SelectableText> Sds_ViewDB_oGetCommandList(bpstl::map<bpstl::string, bpstl::string> const& oScreenVariableData, Sds_ViewId enViewId)
{
   bpstl::vector<SelectableText> commandStrings;
   const Command* command = viewList[enViewId].commandList;
   if (command)
   {
      while (command->enTextId != SDS_TEXT_ID_LIMIT)
      {
         if (Sds_ViewDB_bIsConditionTrue(command->enVisibe, oScreenVariableData))
         {
            SelectableText text;
            text.text = Sds_TextDB_vGetText(command->enTextId);
            text.isSelectable = (Sds_ViewDB_bIsConditionTrue(command->enSelectable, oScreenVariableData) == TRUE);
            commandStrings.push_back(text);
         }
         command++;
      }
   }
   return commandStrings;
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::vector<bpstl::string> Sds_ViewDB_oGetSubCommandList(bpstl::map<bpstl::string, bpstl::string> const& variables,  Sds_ViewId viewId, tU32 cursorIndex)
{
   bpstl::vector<bpstl::string> subCommandStrings;
   const SubCommand* subCommand = viewList[viewId].commandList[cursorIndex].subCommandList;

   if (subCommand)
   {
      while (subCommand->enTextId != SDS_TEXT_ID_LIMIT)
      {
         if (Sds_ViewDB_bIsConditionTrue(subCommand->enVisibe, variables))
         {
            subCommandStrings.push_back(Sds_TextDB_vGetText(subCommand->enTextId));
         }
         subCommand++;
      }
   }
   return subCommandStrings;
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::string Sds_ViewDB_oGetHelpline(Sds_ViewId enView)
{
   if (enView < SDS_VIEW_ID_LIMIT)
   {
      return Sds_TextDB_vGetText(viewList[enView].helpline);
   }
   return "";
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::string Sds_ViewDB_oGetInfo(Sds_ViewId enView)
{
   if (enView < SDS_VIEW_ID_LIMIT)
   {
      return Sds_TextDB_vGetText(viewList[enView].info);
   }
   return "";
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string Sds_ViewDB_getGrammarId(Sds_ViewId enViewId, unsigned int itemIndex, bpstl::map<bpstl::string, bpstl::string> screenVariableData)
{
   if (enViewId < SDS_VIEW_ID_LIMIT)
   {
      const Command* command = viewList[enViewId].commandList;
      if (command)
      {
         while (command->enTextId != SDS_TEXT_ID_LIMIT)
         {
            if (Sds_ViewDB_bIsConditionTrue(command->enVisibe, screenVariableData))
            {
               std::string strGrammarId = viewList[enViewId].commandList[itemIndex].grammarId;
               return strGrammarId;
            }
            command++;
         }
      }
   }
   return "";
}


/**************************************************************************//**
*
*
******************************************************************************/
std::string Sds_ViewDB_getPromptId(Sds_ViewId enViewId, unsigned int itemIndex, bpstl::map<bpstl::string, bpstl::string> screenVariableData)
{
   if (enViewId < SDS_VIEW_ID_LIMIT)
   {
      const Command* command = viewList[enViewId].commandList;
      if (command)
      {
         while (command->enTextId != SDS_TEXT_ID_LIMIT)
         {
            if (Sds_ViewDB_bIsConditionTrue(command->enVisibe, screenVariableData))
            {
               std::string strPromptId = viewList[enViewId].commandList[itemIndex].promptId;
               return strPromptId;
            }
            command++;
         }
      }
   }
   return "";
}
