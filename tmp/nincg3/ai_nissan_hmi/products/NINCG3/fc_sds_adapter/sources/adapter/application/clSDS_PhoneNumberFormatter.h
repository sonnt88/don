/*********************************************************************//**
 * \file       clSDS_PhoneNumberFormatter.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_PhoneNumberFormatter_h
#define clSDS_PhoneNumberFormatter_h


#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"


const tU32 NUMBER_MIN_LIMIT = 3;
const tU32 NUMBER_MAX_LIMIT = 12;


class clSDS_PhoneNumberFormatter
{
   public:
      virtual ~clSDS_PhoneNumberFormatter();
      clSDS_PhoneNumberFormatter();
      static bpstl::string oFormatNumber(const bpstl::string& oNumber);
   private:
      static tVoid vFormatNumberStartingWithOne(bpstl::string& oNumber);
      static tVoid vFormatNumberStartingWithGreaterThanOne(bpstl::string& oNumber);
};


#endif
