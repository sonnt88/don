/*********************************************************************//**
 * \file       clSDS_Method_NaviGetWaypointListInfo.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_NaviGetWaypointListInfo_h
#define clSDS_Method_NaviGetWaypointListInfo_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"
#include "external/sds2hmi_fi.h"


class clSDS_Method_NaviGetWaypointListInfo : public clServerMethod
   , public asf::core::ServiceAvailableIF
   , public org::bosch::cm::navigation::NavigationService::WaypointListCallbackIF
   , public org::bosch::cm::navigation::NavigationService::GetWaypointListCallbackIF
   , public org::bosch::cm::navigation::NavigationService::ApplyWaypointListChangeCallbackIF
{
   public:
      virtual ~clSDS_Method_NaviGetWaypointListInfo();
      clSDS_Method_NaviGetWaypointListInfo(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy> pNaviProxy);

      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onWaypointListError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::WaypointListError >& error);
      virtual void onWaypointListUpdate(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::WaypointListUpdate >& update);

      virtual void onGetWaypointListError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::GetWaypointListError >& error);
      virtual void onGetWaypointListResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::GetWaypointListResponse >& response);

      virtual void onApplyWaypointListChangeError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::ApplyWaypointListChangeError >& error);
      virtual void onApplyWaypointListChangeResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::ApplyWaypointListChangeResponse >& response);

      tBool waypointListIsFull() const;
      tVoid vSendGetWaypointListRequest();
      void onSetAddAsWayPoint(tBool bAddAsWayPoint);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      tVoid vSendResult();

      boost::shared_ptr<org::bosch::cm::navigation::NavigationService::NavigationServiceProxy> _naviProxy;
      size_t _waypointListSize;
      tBool _bWayListRequested;
      tBool _bAddAsWayPoint;
};


#endif
