/*********************************************************************//**
 * \file       clSDS_Method_NaviGetHouseNumberRange.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_NaviGetHouseNumberRange_h
#define clSDS_Method_NaviGetHouseNumberRange_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"


class clSDS_Method_NaviGetHouseNumberRange : public clServerMethod
{
   public:
      virtual ~clSDS_Method_NaviGetHouseNumberRange();
      clSDS_Method_NaviGetHouseNumberRange(ahl_tclBaseOneThreadService* pService);
      tVoid vSendHouseNumberRange();

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      tVoid vSendNoHouseNumber();
      tVoid vGetHouseNumberRangeFromNavi();
      tVoid vAddHouseNumberPatterns(sds2hmi_sdsfi_tclMsgNaviGetHouseNumberRangeMethodResult& oResult) const;
};


#endif
