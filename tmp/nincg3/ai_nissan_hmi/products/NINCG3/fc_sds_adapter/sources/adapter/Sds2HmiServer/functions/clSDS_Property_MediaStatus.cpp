/*********************************************************************//**
 * \file       clSDS_Property_MediaStatus.cpp
 *
 * Common Action Request property implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Property_MediaStatus.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Property_MediaStatus.cpp.trc.h"
#endif


using namespace MPlay_fi_types;
using namespace mplay_MediaPlayer_FI;
using namespace smartphoneint_main_fi;
using namespace smartphoneint_main_fi_types;


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Property_MediaStatus::~clSDS_Property_MediaStatus()
{
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Property_MediaStatus::clSDS_Property_MediaStatus(ahl_tclBaseOneThreadService* pService,
      ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy > pSds2MediaProxy,
      ::boost::shared_ptr< Smartphoneint_main_fiProxy > smartphoneProxy)
   : clServerProperty(SDS2HMI_SDSFI_C_U16_MEDIASTATUS, pService)
   , _sds2MediaProxy(pSds2MediaProxy)
   , _smartphoneProxy(smartphoneProxy)
{
   _carPlayAvailable = false;
   _androidAutoAvailable = false;
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_MediaStatus::vSet(amt_tclServiceData* /*pInMsg*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_MediaStatus::vGet(amt_tclServiceData* /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_MediaStatus::vUpreg(amt_tclServiceData* /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_MediaStatus::vSendStatus()
{
   sds2hmi_sdsfi_tclMsgMediaStatusStatus oMessage;
   vUpdateDeviceList(oMessage.DeviceList);
   if (!oMessage.DeviceList.empty())
   {
      vStatus(oMessage);
      vTraceMediaStatus(oMessage);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
sds2hmi_fi_tcl_e8_MPL_SourceType::tenType clSDS_Property_MediaStatus::eGetMediaPlayerSource(tU32 u32Index) const
{
   switch (_oDeviceList[u32Index].uiDeviceType)
   {
      case T_e8_MPlayDeviceType__e8DTY_IPOD:
      case T_e8_MPlayDeviceType__e8DTY_IPHONE:
         return sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_IPOD;

      case T_e8_MPlayDeviceType__e8DTY_USB:
         return sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_USB;

      default:
         return sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_UNKNOWN;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
sds2hmi_fi_tcl_e8_DeviceStatus::tenType clSDS_Property_MediaStatus::eGetDeviceStatus(tU32 u32Index) const
{
   switch (_oDeviceList[u32Index].indexedState)
   {
      case T_e8_MPlayDeviceIndexedState__e8IDS_COMPLETE:
         return sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_READY;

      case T_e8_MPlayDeviceIndexedState__e8IDS_PARTIAL:
         return sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_UPDATING;

      case T_e8_MPlayDeviceIndexedState__e8IDS_NOT_STARTED:
         return sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_VERIFYING;

      case T_e8_MPlayDeviceIndexedState__e8IDS_NOT_SUPPORTED:
      default:
         return sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_DISABLED;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_Property_MediaStatus::bUsbSourceAvailabe(tU32 u32Index) const
{
   if ((_oDeviceList[u32Index].uiDeviceType == T_e8_MPlayDeviceType__e8DTY_USB) && _oDeviceList[u32Index].bIsConnected == TRUE)
   {
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_Property_MediaStatus::bIpodSourceAvailabe(tU32 u32Index) const
{
   if (((_oDeviceList[u32Index].uiDeviceType == T_e8_MPlayDeviceType__e8DTY_IPOD) ||
         (_oDeviceList[u32Index].uiDeviceType == T_e8_MPlayDeviceType__e8DTY_IPHONE)) && _oDeviceList[u32Index].bIsConnected == TRUE)
   {
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_Property_MediaStatus::bMediaSourceAvailabe(tU32 u32Index) const
{
   switch (eGetMediaPlayerSource(u32Index))
   {
      case sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_IPOD:
         return bIpodSourceAvailabe(u32Index);

      case sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_USB:
         return bUsbSourceAvailabe(u32Index);

      default:
         return FALSE;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_Property_MediaStatus::bBTSourceAvailabe(tU32 u32Index) const
{
   if ((_oDeviceList[u32Index].uiDeviceType == T_e8_MPlayDeviceType__e8DTY_BLUETOOTH) && _oDeviceList[u32Index].bIsConnected == TRUE)
   {
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
 *_sds2PlayUSBProxy
 ******************************************************************************/
tBool clSDS_Property_MediaStatus::bCdAvailable(tU32 u32Index) const
{
   if ((_oDeviceList[u32Index].uiDeviceType == T_e8_MPlayDeviceType__e8DTY_CDDA) && _oDeviceList[u32Index].bIsConnected == TRUE)
   {
      return TRUE;
   }
   else
   {
      return FALSE;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_Property_MediaStatus::bAuxSourceAvailable() const
{
   return TRUE;
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_MediaStatus::vUpdateMediaDeviceStatus(sds2hmi_fi_tcl_DeviceStatus& oDeviceStatus, tU32 u32Index) const
{
   if (_oDeviceList[u32Index].bIsConnected == true)
   {
      oDeviceStatus.DeviceID = _oDeviceList[u32Index].uiDeviceTag;
      bpstl::string oDeviceInfo;
      oDeviceInfo = _oDeviceList[u32Index].sDeviceName;
      oDeviceStatus.DeviceName.bSet(oDeviceInfo.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
      oDeviceStatus.Status.enType = eGetDeviceStatus(u32Index);
      oDeviceStatus.Type.enType = eGetMediaPlayerSource(u32Index);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_MediaStatus::vUpdateDeviceListWithMedia(bpstl::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const
{
   if (_sds2MediaProxy->hasMediaPlayerDeviceConnections())
   {
      MediaPlayerDeviceConnectionsStatus mediastatus = _sds2MediaProxy->getMediaPlayerDeviceConnections();
      T_MPlayDeviceInfo oDeviceInfo = mediastatus.getODeviceInfo();

      for (tU32 u32Index = 0; u32Index < oDeviceInfo.size(); u32Index++)
      {
         if (bMediaSourceAvailabe(u32Index))
         {
            sds2hmi_fi_tcl_DeviceStatus oDeviceStatus;
            vUpdateMediaDeviceStatus(oDeviceStatus, u32Index);
            oDeviceList.push_back(oDeviceStatus);
         }
      }
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_MediaStatus::vUpdateDeviceListWithBTAudio(bpstl::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const
{
   if (_sds2MediaProxy->hasMediaPlayerDeviceConnections())
   {
      MediaPlayerDeviceConnectionsStatus mediastatus = _sds2MediaProxy->getMediaPlayerDeviceConnections();
      T_MPlayDeviceInfo oDeviceInfo = mediastatus.getODeviceInfo();

      for (tU32 u32Index = 0; u32Index < oDeviceInfo.size(); u32Index++)
      {
         if (bBTSourceAvailabe(u32Index))
         {
            sds2hmi_fi_tcl_DeviceStatus oDeviceStatus;
            oDeviceStatus.Type.enType = sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_BTAUDIO;
            oDeviceStatus.Status.enType = sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_READY;
            oDeviceList.push_back(oDeviceStatus);
         }
      }
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_MediaStatus::vUpdateDeviceListWithAux(bpstl::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const
{
   if (_sds2MediaProxy->hasMediaPlayerDeviceConnections())
   {
      MediaPlayerDeviceConnectionsStatus mediastatus = _sds2MediaProxy->getMediaPlayerDeviceConnections();
      T_MPlayDeviceInfo oDeviceInfo = mediastatus.getODeviceInfo();

      for (tU32 u32Index = 0; u32Index < oDeviceInfo.size(); u32Index++)
      {
         if (bAuxSourceAvailable())
         {
            sds2hmi_fi_tcl_DeviceStatus oDeviceStatus;
            oDeviceStatus.Type.enType = sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_LINE_IN;
            oDeviceStatus.Status.enType = sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_READY;
            oDeviceList.push_back(oDeviceStatus);
         }
      }
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_MediaStatus::vUpdateDeviceListWithCD(bpstl::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const
{
   if (_sds2MediaProxy->hasMediaPlayerDeviceConnections())
   {
      MediaPlayerDeviceConnectionsStatus mediastatus = _sds2MediaProxy->getMediaPlayerDeviceConnections();
      T_MPlayDeviceInfo oDeviceInfo = mediastatus.getODeviceInfo();

      for (tU32 u32Index = 0; u32Index < oDeviceInfo.size(); u32Index++)
      {
         if (bCdAvailable(u32Index))
         {
            sds2hmi_fi_tcl_DeviceStatus oDeviceStatus;
            oDeviceStatus.Type.enType = sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_CDA;
            oDeviceStatus.Status.enType = sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_READY;
            oDeviceList.push_back(oDeviceStatus);
         }
      }
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_MediaStatus::vUpdateDeviceList(bpstl::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const
{
   vUpdateDeviceListWithMedia(oDeviceList);
   vUpdateDeviceListWithBTAudio(oDeviceList);
   vUpdateDeviceListWithAux(oDeviceList);
   vUpdateDeviceListWithCD(oDeviceList);
   updateDeviceListWithCarPlayAudio(oDeviceList);
   updateDeviceListWithAndroidAutoAudio(oDeviceList);
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_MediaStatus::vTraceMediaStatus(const sds2hmi_sdsfi_tclMsgMediaStatusStatus& oMessage) const
{
   for (tU32 u32Index = 0; u32Index < oMessage.DeviceList.size(); u32Index++)
   {
      sds2hmi_fi_tcl_DeviceStatus oDeviceStatus(oMessage.DeviceList[u32Index]);
      ETG_TRACE_USR4(("DeviceID %d, DeviceStaus %d, DeviceType %d",
                      oDeviceStatus.DeviceID,
                      oDeviceStatus.Status.enType,
                      oDeviceStatus.Type.enType));
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_MediaStatus::onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
      const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_sds2MediaProxy == proxy)
   {
      _sds2MediaProxy->sendMediaPlayerDeviceConnectionsUpReg(*this);
   }

   if (_smartphoneProxy == proxy)
   {
      _smartphoneProxy->sendDeviceStatusInfoUpReg(*this);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_MediaStatus::onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
      const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_sds2MediaProxy != proxy)
   {
      _sds2MediaProxy->sendMediaPlayerDeviceConnectionsRelUpRegAll();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_MediaStatus:: onMediaPlayerDeviceConnectionsError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const boost::shared_ptr< MediaPlayerDeviceConnectionsError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_MediaStatus::onMediaPlayerDeviceConnectionsStatus(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const boost::shared_ptr< MediaPlayerDeviceConnectionsStatus >& status)
{
   T_MPlayDeviceInfo oDeviceInfo = status->getODeviceInfo();
   T_MPlayDeviceInfo::iterator itr = oDeviceInfo.begin();

   for (int Index = 0 ; itr != oDeviceInfo.end(); ++itr, ++Index)
   {
      _oDeviceList[Index].bIsConnected = itr->getBDeviceConnected();
      _oDeviceList[Index].uiDeviceTag = itr->getU8DeviceTag();
      _oDeviceList[Index].uiDeviceType = itr->getE8DeviceType();
      _oDeviceList[Index].sDeviceName = itr->getSDeviceName();
      _oDeviceList[Index].indexedState = itr->getE8DeviceIndexedState();
   }
   vSendStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_MediaStatus::onDeviceStatusInfoError(
   const ::boost::shared_ptr< Smartphoneint_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< DeviceStatusInfoError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_MediaStatus::onDeviceStatusInfoStatus(
   const ::boost::shared_ptr< Smartphoneint_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< DeviceStatusInfoStatus >& /*status*/)
{
   _smartphoneProxy->sendGetDeviceInfoListStart(*this);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_MediaStatus::onGetDeviceInfoListError(
   const ::boost::shared_ptr< Smartphoneint_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< GetDeviceInfoListError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_MediaStatus::onGetDeviceInfoListResult(
   const ::boost::shared_ptr< Smartphoneint_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< GetDeviceInfoListResult >& result)
{
   ::std::vector< T_DeviceDetails > deviceList = result->getDeviceInfoList();

   const bool carPlay = isCarPlayAvailable(deviceList);
   const bool androidAuto = isAndroidAutoAvailable(deviceList);

   if (_carPlayAvailable != carPlay
         || _androidAutoAvailable != androidAuto)
   {
      updateCarPlayAndroidAutoStatus(carPlay, androidAuto);
   }
}


bool clSDS_Property_MediaStatus::isCarPlayAvailable(::std::vector< T_DeviceDetails > deviceList) const
{
   for (unsigned int i = 0; i < deviceList.size(); i++)
   {
      if (deviceList[i].getEnDeviceCategory() == T_e8_DeviceCategory__DEV_TYPE_DIPO
            && deviceList[i].getEnDeviceConnectionStatus() == T_e8_DeviceConnectionStatus__DEV_CONNECTED)
      {
         return true;
      }
   }
   return false;
}


bool clSDS_Property_MediaStatus::isAndroidAutoAvailable(::std::vector< T_DeviceDetails > deviceList) const
{
   for (unsigned int i = 0; i < deviceList.size(); i++)
   {
      if (deviceList[i].getEnDeviceCategory() == T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO
            && deviceList[i].getEnDeviceConnectionStatus() == T_e8_DeviceConnectionStatus__DEV_CONNECTED)
      {
         return true;
      }
   }
   return false;
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_MediaStatus::updateCarPlayAndroidAutoStatus(bool carPlay, bool androidAuto)
{
   _carPlayAvailable = carPlay;
   _androidAutoAvailable = androidAuto;
   vSendStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_MediaStatus::updateDeviceListWithCarPlayAudio(std::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const
{
   if (_carPlayAvailable)
   {
      sds2hmi_fi_tcl_DeviceStatus oDeviceStatus;
      oDeviceStatus.Type.enType = sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_CARPLAY_AUD;
      oDeviceStatus.Status.enType = sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_READY;
      oDeviceList.push_back(oDeviceStatus);
   }
   else
   {
      sds2hmi_fi_tcl_DeviceStatus oDeviceStatus;
      oDeviceStatus.Type.enType = sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_CARPLAY_AUD;
      oDeviceStatus.Status.enType = sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_DISABLED;
      oDeviceList.push_back(oDeviceStatus);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_MediaStatus::updateDeviceListWithAndroidAutoAudio(std::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const
{
   if (_androidAutoAvailable)
   {
      sds2hmi_fi_tcl_DeviceStatus oDeviceStatus;
      oDeviceStatus.Type.enType = sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_ANDROID_AUD;
      oDeviceStatus.Status.enType = sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_READY;
      oDeviceList.push_back(oDeviceStatus);
   }
   else
   {
      sds2hmi_fi_tcl_DeviceStatus oDeviceStatus;
      oDeviceStatus.Type.enType = sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_ANDROID_AUD;
      oDeviceStatus.Status.enType = sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_DISABLED;
      oDeviceList.push_back(oDeviceStatus);
   }
}
