/*********************************************************************//**
 * \file       clSDS_Method_CommonSetAvailableUserwords.cpp
 *
 * clSDS_Method_CommonSetAvailableUserwords method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_CommonSetAvailableUserwords.h"
#include "external/sds2hmi_fi.h"
#include "application/clSDS_Userwords.h"
#include "application/clSDS_QuickDialList.h"
#include "application/StringUtils.h"
#include "SdsAdapter_Trace.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_CommonSetAvailableUserwords.cpp.trc.h"
#endif


#define UTFUTIL_S_IMPORT_INTERFACE_GENERIC
#include "utf_if.h"


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_CommonSetAvailableUserwords::~clSDS_Method_CommonSetAvailableUserwords()
{
   _pUserwords = NULL;
   _pQuickDialList = NULL;
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_CommonSetAvailableUserwords::clSDS_Method_CommonSetAvailableUserwords(ahl_tclBaseOneThreadService* pService,
      clSDS_Userwords* pUserwords,
      SdsPhoneService& sdsPhoneService,
      clSDS_QuickDialList* pQuickDialList,
      ::boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy> phonebookProxy)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_COMMONSETAVAILABLEUSERWORDS, pService)
   , _phonebookProxy(phonebookProxy)
   , _pUserwords(pUserwords)
   , _sdsPhoneService(sdsPhoneService)
   , _pQuickDialList(pQuickDialList)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_CommonSetAvailableUserwords::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgCommonSetAvailableUserwordsMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);

   _pQuickDialList->listUserWords(oMessage);
   // to update Phone HMI with avaialableUserwords
   std::vector< sds_gui_fi::SdsPhoneService::UserWordList > userWordList;
   sds_gui_fi::SdsPhoneService::UserWordList uwList;
   UserwordInfo userWordInfoItem;

   std::vector<sds2hmi_fi_tcl_Userword, std::allocator<sds2hmi_fi_tcl_Userword> > availableUserwords;
   availableUserwords = oMessage.AvailableUserwords;
   if (availableUserwords.size())
   {
      for (tU32 u32Index = 0; u32Index < availableUserwords.size(); u32Index++)
      {
         userWordInfoItem.profileId = availableUserwords[u32Index].UWProfile;
         uwList.setUserWordProfileID(availableUserwords[u32Index].UWProfile);
         ETG_TRACE_USR1(("Active profile number = %d", _phonebookProxy->getDevicePhoneBookFeatureSupport().getU8DeviceHandle()));
         if (availableUserwords[u32Index].UWProfile == _phonebookProxy->getDevicePhoneBookFeatureSupport().getU8DeviceHandle())
         {
            if (availableUserwords[u32Index].PhoneUWs.size())
            {
               std::vector<tU32, std::allocator<tU32> > phoneUWs;
               phoneUWs = availableUserwords[u32Index].PhoneUWs;
               for (tU32 Index = 0; Index < phoneUWs.size(); Index++)
               {
                  userWordInfoItem.userWordID.push_back(phoneUWs[Index]);
               }
               uwList.setPhoneUWID(userWordInfoItem.userWordID);
               userWordList.push_back(uwList);
            }
         }
      }
      _sdsPhoneService.sendUserWordUpdateSignal(userWordList);
   }
   vTraceMethodStart(oMessage.AvailableUserwords);
   vSendMethodResult();
}


/***********************************************************************//**
 *
 ***************************************************************************/
//static bpstl::string makeDebugString(const sds2hmi_fi_tcl_Userword oSDS_user)
//{
//   bpstl::string oTraceString;
//   for (tU32 u32SdsListIndex = 0; u32SdsListIndex < oSDS_user.PhoneUWs.size(); u32SdsListIndex++)
//   {
//      oTraceString.append(" ");
//      oTraceString.append(StringUtils::toString(oSDS_user.PhoneUWs[u32SdsListIndex]));
//   }
//   return oTraceString;
//}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_Method_CommonSetAvailableUserwords::vTraceMethodStart(const AvailableUserwords& /*oUserwords*/) const
{
   // TODO jnd2hi rework trace
   //   for (tU32 u32Index = 0; u32Index < oUserwords.size(); u32Index++)
   //   {
   //      ET_TRACE_BIN(
   //         TR_CLASS_SDSADP_DETAILS,
   //         TR_LEVEL_HMI_INFO,
   //         ET_EN_T16 _ TRACE_CCA_VALUE _
   //         ET_EN_T16 _ _pService->u16GetServiceId() _
   //         ET_EN_T16 _ u16GetFunctionID() _
   //         ET_EN_T32 _ oUserwords[u32Index].UWProfile _
   //         ET_EN_STRING _ makeDebugString(oUserwords[u32Index]).c_str() _
   //         ET_EN_DONE);
   //   }
}


void clSDS_Method_CommonSetAvailableUserwords::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& /*proxy*/,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_CommonSetAvailableUserwords::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& /*proxy*/,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
}
