/*********************************************************************//**
 * \file       AhlApp.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef AhlApp_h
#define AhlApp_h


#include "asf/cca/CcaServerHookStub.h"

#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"


class AhlApp
   : public ahl_tclBaseOneThreadApp
   , public asf::cca::CcaServerHook::CcaServerHookStub
{
   public:
      AhlApp(); // not implemented!
      AhlApp(tU16 appId);
      virtual ~AhlApp();
      void onCcaMessage(const ::boost::shared_ptr< asf::core::Blob >& request);

   private:
      DECLARE_CLASS_LOGGER();	//lint !e1516 Member declaration hides inherited member - jnd2hi
};


#endif
