/*********************************************************************//**
 * \file       clSDS_MyAppsDataBase.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_MyAppsDataBase.h"
#include "application/clSDS_MyAppsDataBaseObserver.h"
#include "SdsAdapter_Trace.h"

#ifdef SQL_SUPPORTED
using namespace sql;
#endif


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_MyAppsDataBase.cpp.trc.h"
#endif


/**************************************************************************//**
*
******************************************************************************/
static tVoid vMyAppsListThreadProc(tPVoid pvPtr)
{
   clSDS_MyAppsDataBase* pMyAppsDataBase = (clSDS_MyAppsDataBase*) pvPtr;
   if (pMyAppsDataBase != NULL)
   {
      if (pMyAppsDataBase->bOpenMyAppsDataBase() == TRUE)
      {
         ETG_TRACE_USR1(("MyApps Data base open to get Apps Nammes"));
         pMyAppsDataBase->vClearMyAppsList();
         pMyAppsDataBase->vReadMyAppsDataBase();
      }
      else
      {
         ETG_TRACE_USR1(("Data base opening Failed"));
      }
   }
   OSAL_vThreadExit();
}


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_MyAppsDataBase::~clSDS_MyAppsDataBase()
{
   _hThread = 0;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
#ifdef SQL_SUPPORTED
clSDS_MyAppsDataBase::clSDS_MyAppsDataBase(): sql::Client(sql::SYNCHRONOUS)
#else
clSDS_MyAppsDataBase::clSDS_MyAppsDataBase()
#endif
{
   _hThread = 0;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_MyAppsDataBase::vRegisterObserver(clSDS_MyAppsDataBaseObserver* pObserver)
{
   if (pObserver != NULL)
   {
      _observers.push_back(pObserver);
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_MyAppsDataBase::vNotifyObservers()
{
   bpstl::vector<clSDS_MyAppsDataBaseObserver*>::iterator iter = _observers.begin();
   while (iter != _observers.end())
   {
      if (*iter != NULL)
      {
         (*iter)->vMyAppsListAvailable();
      }
      ++iter;
   }
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_MyAppsDataBase::bOpenMyAppsDataBase() const
{
#ifdef SQL_SUPPORTED
   bpstl::string oMyAppsDataBasaePath;
   dataPool.vGetString(DPSMARTPHONE__DATA_BASE_PATH, oMyAppsDataBasaePath);
   if (open(oMyAppsDataBasaePath.c_str()) != OSAL_ERROR)
   {
      return TRUE;
   }
   return FALSE;
#else
   return TRUE;
#endif
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_MyAppsDataBase::vClearMyAppsList()
{
   _oMyAppsNameList.clear();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_MyAppsDataBase::vReadMyAppsDataBase() const
{
#ifdef SQL_SUPPORTED
   vRequestAppsNamefromDataBase();
   close();
#endif
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_MyAppsDataBase::vRequestAppsNamefromDataBase() const
{
#ifdef SQL_SUPPORTED
   ETG_TRACE_USR1(("Request sent to sql data base to get myapps name list"));
   bpstl::string sQuery = "SELECT Orthography, LangID FROM AppNames ORDER BY PageNumber";
   request(sQuery.c_str(), 0, OSAL_SQLDB_PRIO_MIN, OSAL_C_TIMEOUT_FOREVER);
   ETG_TRACE_USR1(("MyApps data base reading finished"));
   vNotifyObservers();
#endif
}


/**************************************************************************//**
*
******************************************************************************/
tS32 clSDS_MyAppsDataBase::answer(tS32 s32NumOfEntry, tChar** vals, tChar** cols, tU32 /*arg*/)
{
#ifdef SQL_SUPPORTED
   if (s32NumOfEntry == OSAL_SQLDB_ANSWER_ERROR)
   {
      return OSAL_ERROR;
   }

   if (vals && cols)
   {
      stMyappNames oMyAppsListElement;
      for (tU32 u32Index = 0; u32Index < (tU32)s32NumOfEntry; u32Index++)
      {
         if (vals[u32Index] && cols[u32Index])
         {
            if (OSAL_s32StringCompare(cols[u32Index], "Orthography") == 0)
            {
               oMyAppsListElement.oAppName.assign(vals[u32Index]);
            }
            else if (OSAL_s32StringCompare(cols[u32Index], "LangID") == 0)
            {
               oMyAppsListElement.oLanguageId.assign(vals[u32Index]);
            }
         }
      }
      bpstl::string oTraceString("");
      oTraceString = " AppsName = " + oMyAppsListElement.oAppName + " Languageid = " + oMyAppsListElement.oLanguageId;
      ETG_TRACE_USR1(("Database Element: = %s", oTraceString.c_str()));
      _oMyAppsNameList.push_back(oMyAppsListElement);
   }
#endif

   (void)s32NumOfEntry;
   (void)vals;
   (void)cols;

   return OSAL_OK;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_MyAppsDataBase::vCreateThreadToReadMyAppsDataBase()
{
   const tU32 u32ThreadPrio = OSAL_C_U32_THREAD_PRIORITY_NORMAL;
   const tU32 u32StackSize = 10000;

   OSAL_trThreadAttribute thread;
   static tChar szThreadName[] = "HMI_MYAPPS_LIST";
   thread.szName        = szThreadName;
   thread.s32StackSize  = u32StackSize;
   thread.u32Priority   = u32ThreadPrio;
   thread.pfEntry       = vMyAppsListThreadProc;
   thread.pvArg         = this;

   _hThread = OSAL_ThreadSpawn(&thread);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_MyAppsDataBase::vRequestMyAppsNames()
{
   vCreateThreadToReadMyAppsDataBase();
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::vector<stMyappNames> clSDS_MyAppsDataBase::oGetMyAppsNameList() const
{
   return _oMyAppsNameList;
}
