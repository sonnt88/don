/*********************************************************************//**
 * \file       clSDS_TagContents.cpp
 *
 * clSDS_TagContents class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_TagContents.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_TagContents::~clSDS_TagContents()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_TagContents::clSDS_TagContents()
{
}
