/*********************************************************************//**
 * \file       clSDS_XMLDoc.cpp
 *
 * clSDS_XMLDoc class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_XMLDoc.h"
#include "application/clSDS_TagContents.h"


#define READ_ONE_ELEMENT 1


clSDS_XMLDoc::clSDS_XMLDoc(bpstl::string const& oXMLStream)
   : _doc(xmlParseMemory(oXMLStream.c_str(), (int)oXMLStream.length()))
{
}


clSDS_XMLDoc::~clSDS_XMLDoc()
{
   if (_doc)
   {
      xmlFreeDoc(_doc);
      _doc = NULL;
   }
}


bpstl::vector<clSDS_TagContents> clSDS_XMLDoc::oGetElementsOfTag(bpstl::string const& oTagName)
{
   bpstl::vector<clSDS_TagContents> oTagContents;
   if (_doc)
   {
      xmlNodePtr oNode = xmlDocGetRootElement(_doc);
      oNode = oNode->children;
      while (oNode)
      {
         if (!xmlStrcmp(oNode->name, (const xmlChar*)(oTagName.c_str())))
         {
            clSDS_TagContents oTagContent = oReadTagContents(oNode);
            vReadChildContents(oNode, oTagContent.voChildrenTags);
            oTagContents.push_back(oTagContent);
         }
         oNode = oNode->next;
      }
   }
   return oTagContents;
}


tVoid clSDS_XMLDoc::vReadChildContents(const xmlNode* node, bpstl::vector<clSDS_TagContents>& oTagContents)
{
   xmlNodePtr child = node->children;
   while (child)
   {
      if (child->type != XML_CDATA_SECTION_NODE)
      {
         clSDS_TagContents oTagContent = oReadTagContents(child);
         vReadChildContents(child, oTagContent.voChildrenTags);
         oTagContents.push_back(oTagContent);
      }
      child = child->next;
   }
}


clSDS_TagContents clSDS_XMLDoc::oReadTagContents(const xmlNode* oNode)
{
   clSDS_TagContents oTagContent;
   oTagContent.oTagName = (const char*)oNode->name;
   xmlChar* oElement = xmlNodeListGetString(_doc, oNode->xmlChildrenNode, READ_ONE_ELEMENT);
   if (oElement)
   {
      oTagContent.oTagValue = (const char*)oElement;
      xmlFree(oElement);
   }
   return oTagContent;
}
