/*********************************************************************//**
 * \file       AhlApp.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "AhlApp.h"
#include "Sds2HmiServer/framework/StreamMessage.h"


DEFINE_CLASS_LOGGER_AND_LEVEL("sdshmi", AhlApp, Info);


AhlApp::AhlApp(tU16 appId)
   : ahl_tclBaseOneThreadApp(appId)
{
}


AhlApp::~AhlApp()
{
}


void AhlApp::onCcaMessage(const ::boost::shared_ptr< asf::core::Blob >& request)
{
   StreamMessage message((tU8*)request->getBytes());
   LOG_INFO("onCcaMessage(): type=%d", message.u8GetType());
   bDispatchCCAMessages(&message);
}
