/*********************************************************************//**
 * \file       clSDS_NBestList.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "application/clSDS_NBestList.h"

/**************************************************************************
* Constructor
***************************************************************************/
clSDS_NBestList::clSDS_NBestList()
{
}


/**************************************************************************
* Destructor
***************************************************************************/
clSDS_NBestList::~clSDS_NBestList()
{
}


/**************************************************************************
*
***************************************************************************/
void clSDS_NBestList::vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType /*listType*/)
{
}


/**************************************************************************
*
***************************************************************************/
tU32 clSDS_NBestList::u32GetSize()
{
   return _listItems.size();
}


/**************************************************************************
*
***************************************************************************/
std::vector<clSDS_ListItems> clSDS_NBestList::oGetItems(tU32 u32StartIndex, tU32 u32EndIndex)
{
   ::std::vector<clSDS_ListItems> oListItems;

   for (unsigned long u32Index = u32StartIndex; u32Index < ::std::min(u32EndIndex, u32GetSize()); u32Index++)
   {
      oListItems.push_back(_listItems[u32Index]);
   }
   return oListItems;
}


/**************************************************************************
*
***************************************************************************/
tBool clSDS_NBestList::bSelectElement(tU32 /*u32SelectedIndex*/)
{
   return true;
}


/**************************************************************************
*
***************************************************************************/
void clSDS_NBestList::setItems(std::vector<clSDS_ListItems> items)
{
   _listItems = items;
}
