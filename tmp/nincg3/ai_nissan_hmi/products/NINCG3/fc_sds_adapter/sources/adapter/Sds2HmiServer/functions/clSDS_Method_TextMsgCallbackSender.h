/*********************************************************************//**
 * \file       clSDS_Method_TextMsgCallbackSender.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_TextMsgCallbackSender_h
#define clSDS_Method_TextMsgCallbackSender_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "MOST_Tel_FIProxy.h"


class clSDS_Method_TextMsgCallbackSender
   : public clServerMethod
   , public MOST_Tel_FI::DialCallbackIF
{
   public:
      virtual ~clSDS_Method_TextMsgCallbackSender();
      clSDS_Method_TextMsgCallbackSender(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > pTelProxy);

      virtual void onDialError(
         const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Tel_FI::DialError >& error);
      virtual void onDialResult(
         const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Tel_FI::DialResult >& result);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > _telephoneProxy;
};


#endif
