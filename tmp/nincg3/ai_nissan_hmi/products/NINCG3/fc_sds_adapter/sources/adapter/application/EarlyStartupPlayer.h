/*********************************************************************//**
 * \file       EarlyStartupPlayer.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef EarlyStartupPlayer_h
#define EarlyStartupPlayer_h

#include <gst/gst.h>
#include "application/SdsAudioSourceObserver.h"
#include "asf/core/Timer.h"

#define NUMBER_OF_BYTES_FOR_FILE_LOCATION    	256
#define FILE_LOCATION_PATH						"/var/opt/bosch/dynamic/speech/radio/fakePrompt.mp3"


class SdsAudioSource;

class EarlyStartupPlayer
   : public SdsAudioSourceObserver
   , public asf::core::TimerCallbackIF
{
   public:
      EarlyStartupPlayer(SdsAudioSource& sdsAudioSource);
      virtual ~EarlyStartupPlayer();

      void onAudioSourceStateChanged(arl_tenActivity state);

      void onExpired(asf::core::Timer& timer, boost::shared_ptr<asf::core::TimerPayload> data);

      void requestStartPlayback();
      void requestStopPlayback();

   private:
      void startPlayback();
      void stopPlayback();

      void startTimer();

      SdsAudioSource& _sdsAudioSource;
      bool _audioChannelRequested;
      asf::core::Timer _timer;

      typedef struct
      {
         GstElement* file_source;
         GstElement* pipeline;
         GstElement* audio_decoder;
         GstElement* audioconvert;
         GstElement* alsasink;
         GstElement* bin_playback;
         GstBus* bus;
         GstMessage* message;
         gchar filelocation[NUMBER_OF_BYTES_FOR_FILE_LOCATION];
      } gstData;

      gstData gstreamerData;

      bool create_pipeline(gstData* data);
      static void on_pad_added(GstElement* src_element, GstPad* src_pad, gpointer data);
      bool init_audio_playback_pipeline(gstData* data);
      void start_playback_pipe(gstData* data);
      bool add_bin_playback_to_pipe(gstData* data);
      void remove_bin_playback_from_pipe(gstData* data);
      void delete_pipeline(gstData* data);
      bool check_bus_cb(gstData* data);
};


#endif /* EarlyStartupPlayer_h */
