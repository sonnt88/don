/*********************************************************************//**
 * \file       clSDS_TunerStateObserver.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef CLSDS_TUNERSTATEOBSERVER_H_
#define CLSDS_TUNERSTATEOBSERVER_H_

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "external/sds2hmi_fi.h"


class clSDS_TunerStateObserver
{
   public:
      clSDS_TunerStateObserver();
      virtual ~clSDS_TunerStateObserver();
      virtual tVoid vTunerStatusChanged(sds2hmi_fi_tcl_e8_TUN_Band::tenType devID, sds2hmi_fi_tcl_e8_DeviceStatus::tenType devStatus) = 0;
};


#endif /* CLSDS_TUNERSTATEOBSERVER_H_ */
