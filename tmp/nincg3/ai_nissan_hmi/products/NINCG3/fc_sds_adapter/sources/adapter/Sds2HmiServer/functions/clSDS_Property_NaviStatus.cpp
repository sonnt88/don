/*********************************************************************//**
 * \file       clSDS_Property_NaviStatus.cpp
 *
 * Common Action Request property implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "Sds2HmiServer/functions/clSDS_Property_NaviStatus.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Property_NaviStatus.cpp.trc.h"
#endif


using namespace org::bosch::cm::navigation::NavigationService;


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Property_NaviStatus::~clSDS_Property_NaviStatus()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Property_NaviStatus::clSDS_Property_NaviStatus(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr<org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > naviProxy)
   : clServerProperty(SDS2HMI_SDSFI_C_U16_NAVISTATUS, pService)
   , _naviProxy(naviProxy)
{
   sdsNaviStatus = sds2hmi_fi_tcl_e8_NAV_Status::FI_EN_IDLE;
   enVDEAvailInfo = sds2hmi_fi_tcl_e8_NAV_VDEAvailInfo::FI_EN_UNKNOWN;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_NaviStatus::vSet(amt_tclServiceData* pInMsg)
{
   sds2hmi_sdsfi_tclMsgNaviStatusSet oMessage;
   vGetDataFromAmt(pInMsg, oMessage);
   enVDEAvailInfo = oMessage.VDEAvailInfo.enType;
   vSendNavStatus();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_NaviStatus::vGet(amt_tclServiceData* /*pInMsg*/)
{
   vSendNavStatus();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_NaviStatus::vUpreg(amt_tclServiceData* /*pInMsg*/)
{
   vSendNavStatus();
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_Property_NaviStatus::vSendNavStatus()
{
   sds2hmi_sdsfi_tclMsgNaviStatusStatus oMessage;
   oMessage.MenuType.enType = sds2hmi_fi_tcl_e8_NAV_MenuType::FI_EN_MAP;
   oMessage.NavUnit.enType = sds2hmi_fi_tcl_e8_NAV_NavUnit::FI_EN_METRIC;
   oMessage.Status.enType = sdsNaviStatus;
   oMessage.VDEAvailInfo.enType = enVDEAvailInfo;
   vStatus(oMessage);
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Property_NaviStatus::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _naviProxy)
   {
      _naviProxy->sendNavStatusRegister(*this);
      _naviProxy->sendNavStatusGet(*this);
   }
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Property_NaviStatus::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _naviProxy)
   {
      _naviProxy->sendNavStatusDeregisterAll();
   }
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Property_NaviStatus::onNavStatusUpdate(
   const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavStatusUpdate >& update)
{
   switch (update->getNavStatus())
   {
      case NavStatus__NAVSTATUS_UNKNOWN:
      case NavStatus__NAVSTATUS_IDLE:
         sdsNaviStatus = sds2hmi_fi_tcl_e8_NAV_Status::FI_EN_IDLE;
         break;
      case NavStatus__NAVSTATUS_BUSY:
         sdsNaviStatus = sds2hmi_fi_tcl_e8_NAV_Status::FI_EN_BUSY;
         break;
      case NavStatus__NAVSTATUS_GUIDANCE_ACTIVE:
         sdsNaviStatus = sds2hmi_fi_tcl_e8_NAV_Status::FI_EN_GUIDANCE_ACTIVE;
         break;
      case  NavStatus__NAVSTATUS_CALCULATING_ROUTE:
         sdsNaviStatus = sds2hmi_fi_tcl_e8_NAV_Status::FI_EN_CALCULATING_ROUTE;
         break;
      case NavStatus__NAVSTATUS_GUIDANCE_ACTIVE_OFFBOARD:
         sdsNaviStatus = sds2hmi_fi_tcl_e8_NAV_Status::FI_EN_GUIDANCE_ACTIVE_OFFBOARD;
         break;
      default:
         sdsNaviStatus = sds2hmi_fi_tcl_e8_NAV_Status::FI_EN_IDLE;
         break;
   }
   vSendNavStatus();
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Property_NaviStatus::onNavStatusError(
   const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavStatusError >& /*error*/)
{
}
