/*********************************************************************//**
 * \file       clSDS_Property_PhoneStatus.cpp
 *
 * Common Action Request property implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Property_PhoneStatus.h"
#include "application/clSDS_PhoneStatusObserver.h"
#include "application/clSDS_Userwords.h"
#include "application/StringUtils.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Property_PhoneStatus.cpp.trc.h"
#endif


using namespace MOST_BTSet_FI;
using namespace most_BTSet_fi_types;
using namespace MOST_Tel_FI;
using namespace most_Tel_fi_types;
using namespace MOST_PhonBk_FI;
using namespace most_PhonBk_fi_types;
using namespace most_PhonBk__fi_types_Extended;


#define VPB_OFFSET 1000


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Property_PhoneStatus::~clSDS_Property_PhoneStatus()
{
   for (size_t obsIndex = 0; obsIndex != _phoneStatusObserverList.size(); ++obsIndex)
   {
      _phoneStatusObserverList[obsIndex] = NULL;
   }
   _pUserwords = NULL;
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Property_PhoneStatus::clSDS_Property_PhoneStatus(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy > pSds2BtSetProxy,
   ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > pSds2TelProxy,
   ::boost::shared_ptr <MOST_PhonBk_FIProxy> pPhoneBookProxy,
   clSDS_Userwords* userWords)
   : clServerProperty(SDS2HMI_SDSFI_C_U16_PHONESTATUS, pService)
   , _sds2BtSetProxy(pSds2BtSetProxy)
   , _telephoneProxy(pSds2TelProxy)
   , _phoneBookProxy(pPhoneBookProxy)
   , _inCallDeviceHandle(1)
   , _btStatus(false)
   , _callPresent(false)
   , _handsetMode(false)
   , _userConnectedState(false)
   , _firstCallStatus(T_e8_TelCallStatus__e8IDLE)
   , _secondCallStatus(T_e8_TelCallStatus__e8IDLE)
   , _pUserwords(userWords)
{
   _connectedDevice.id = 0;
   _connectedDevice.name = "";
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_PhoneStatus::vSet(amt_tclServiceData* /*pInMsg*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_PhoneStatus::vGet(amt_tclServiceData* /*pInMsg*/)
{
   vSendPhoneStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_PhoneStatus::vUpreg(amt_tclServiceData* /*pInMsg*/)
{
   vSendPhoneStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_PhoneStatus::eGetSdsTypePhonebookStatus(const most_PhonBk_fi_types::T_PhonBkDownloadStateStreamItem oDownloadStatus)
{
   // TODO jnd2hi: is it ok to derive connection state from phonebook? what if phonebook is not supported/allowed by connected phone
   switch (oDownloadStatus.getE8PhoneBookDownloadState())
   {
      case T_e8_PhonBkPhoneBookDownloadState__e8PBDS_COMPLETE:
         _connectedPhonedeviceStatus.enType = sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_READY;
         break;

      case T_e8_PhonBkPhoneBookDownloadState__e8PBDS_NOT_STARTED:
      case T_e8_PhonBkPhoneBookDownloadState__e8PBDS_CONTACT_INFO:
      case T_e8_PhonBkPhoneBookDownloadState__e8PBDS_CONTACT_PHOTO:
         _connectedPhonedeviceStatus.enType = sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_UPDATING;
         break;

      default:
         _connectedPhonedeviceStatus.enType = sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_DISABLED;
         break;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
sds2hmi_fi_tcl_DeviceStatus clSDS_Property_PhoneStatus::getDeviceStatus(const DeviceDescriptor& device) const
{
   sds2hmi_fi_tcl_DeviceStatus deviceStatus;
   deviceStatus.DeviceID = (tU32)device.id;
   deviceStatus.DeviceName.bSet(device.name.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);

   if (device.name == _connectedDevice.name)
   {
      // connected device (actual phone)
      deviceStatus.Status = _connectedPhonedeviceStatus;
   }
   else if (device.id == VPB_OFFSET + _connectedDevice.id)
   {
      // device for vehicle phonebook
      deviceStatus.Status = _connectedPhonedeviceStatus;
   }
   else
   {
      // all other paired but unconnected devices, including coresponding VPBs
      deviceStatus.Status.enType = sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_DISABLED;
   }
   return deviceStatus;
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_PhoneStatus::vSendPhoneStatus()
{
   sds2hmi_sdsfi_tclMsgPhoneStatusStatus oMessage;
   for (tU32 u32ListIndex = 0; u32ListIndex < _deviceList.size(); u32ListIndex++)
   {
      oMessage.DeviceList.push_back(getDeviceStatus(_deviceList[u32ListIndex]));
   }
   oMessage.MenuType.enType = sds2hmi_fi_tcl_e8_PHN_MenuType::FI_EN_OTHER;
   oMessage.Status = _phoneStatus;
   oMessage.BTenabled = _btStatus;
   oMessage.TransorderType = _preferredSortingOrder;
   oMessage.CarPlayAvailable = FALSE;
   vStatus(oMessage);
   vTracePhoneStatus(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_PhoneStatus::vTracePhoneStatus(const sds2hmi_sdsfi_tclMsgPhoneStatusStatus& /*oMessage*/) const
{
   // TODO jnd2hi rework trace
   //   ET_TRACE_BIN(
   //      TR_CLASS_SDSADP_DETAILS,
   //      TR_LEVEL_HMI_INFO,
   //      ET_EN_T16 _ TRACE_CCA_VALUE _
   //      ET_EN_T16 _ _pService->u16GetServiceId() _
   //      ET_EN_T16 _ u16GetFunctionID() _
   //      ET_EN_T16 _ 0 _
   //      ET_EN_T16 _ oMessage.BTenabled _
   //      ET_EN_T16 _ oMessage.TransorderType.enType _
   //      ET_EN_DONE);
   //
   //   for (tU32 u32Index = 0; u32Index < dataPool.u8GetValue(DPTELEPHONE__PAIRED_DEVICES_COUNT); u32Index++)
   //   {
   //      ET_TRACE_BIN(
   //         TR_CLASS_SDSADP_DETAILS,
   //         TR_LEVEL_HMI_INFO,
   //         ET_EN_T16 _ TRACE_CCA_VALUE _
   //         ET_EN_T16 _ _pService->u16GetServiceId() _
   //         ET_EN_T16 _ u16GetFunctionID() _
   //         ET_EN_T16 _ 1 _
   //         ET_EN_T16 _ oMessage.DeviceList[u32Index].DeviceID _
   //         ET_EN_T16 _ oMessage.DeviceList[u32Index].Status.enType _
   //         ET_EN_STRING _ oMessage.DeviceList[u32Index].DeviceName.szValue _
   //         ET_EN_DONE);
   //   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_PhoneStatus::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _sds2BtSetProxy)
   {
      _sds2BtSetProxy->sendBluetoothOnOffUpReg(*this);
      _sds2BtSetProxy->sendDeviceListUpReg(*this);
   }
   if (proxy == _telephoneProxy)
   {
      _telephoneProxy->sendCallStatusNoticeUpReg(*this);
   }
   if (proxy == _phoneBookProxy)
   {
      _phoneBookProxy->sendDownloadStateUpReg(*this);
      _phoneBookProxy->sendPreferredPhoneBookSortOrderUpReg(*this);
      _phoneBookProxy->sendListChangeUpReg(*this);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_PhoneStatus::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _sds2BtSetProxy)
   {
      _sds2BtSetProxy->sendBluetoothOnOffRelUpRegAll();
      _sds2BtSetProxy->sendDeviceListRelUpRegAll();
   }
   if (proxy == _telephoneProxy)
   {
      _telephoneProxy->sendCallStatusNoticeRelUpRegAll();
   }
   if (proxy == _phoneBookProxy)
   {
      _phoneBookProxy->sendDownloadStateRelUpRegAll();
      _phoneBookProxy->sendPreferredPhoneBookSortOrderRelUpRegAll();
      _phoneBookProxy->sendListChangeRelUpRegAll();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_PhoneStatus::onBluetoothOnOffError(
   const ::boost::shared_ptr< MOST_BTSet_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< BluetoothOnOffError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_PhoneStatus::onBluetoothOnOffStatus(
   const ::boost::shared_ptr< MOST_BTSet_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< BluetoothOnOffStatus >& status)
{
   _btStatus = status->getBBTOnOff();
   vSendPhoneStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_PhoneStatus::onCallStatusNoticeError(
   const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< CallStatusNoticeError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_PhoneStatus::onCallStatusNoticeStatus(
   const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< CallStatusNoticeStatus >& status)
{
   _handsetMode = false;
   _callPresent = false;
   //TODO raa8hi:  initialize _inCallDeviceHandle?
   for (uint8 i = 0 ; i < status->getOCallStatusNoticeStream().size() ; i++)
   {
      if (status->getOCallStatusNoticeStream()[i].getE8CallStatus() != T_e8_TelCallStatus__e8IDLE)
      {
         _callPresent = true;
         if (!(status->getOCallStatusNoticeStream()[i].getBUsingVehicleAudio()))
         {
            _handsetMode = true;
         }
      }

      if (status->getOCallStatusNoticeStream()[i].getE8CallStatus() == T_e8_TelCallStatus__e8ACTIVE)
      {
         _inCallDeviceHandle = status->getOCallStatusNoticeStream()[i].getU8DeviceHandle();
      }
   }
   // TODO jnd2hi: is it safe to access the list elements below without checking list size??
   _firstCallStatus = status->getOCallStatusNoticeStream()[0].getE8CallStatus();
   _secondCallStatus = status->getOCallStatusNoticeStream()[1].getE8CallStatus();
   handlePhoneStatus();
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Property_PhoneStatus::onDeviceListError(
   const ::boost::shared_ptr< MOST_BTSet_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< DeviceListError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_PhoneStatus::onDeviceListStatus(
   const ::boost::shared_ptr< MOST_BTSet_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< DeviceListStatus >& status)
{
   uint8 deviceHandle = status->getODeviceListChange().getU8DeviceHandle();
   T_e8_BTSetDeviceStatus deviceStatus = status->getODeviceListChange().getE8DeviceStatus();

   // notify observers on onDeviceListStatus
   for (size_t obsIndex = 0; obsIndex != _phoneStatusObserverList.size(); ++obsIndex)
   {
      if (_phoneStatusObserverList[obsIndex] != NULL)
      {
         _phoneStatusObserverList[obsIndex]->phoneStatusChanged(deviceHandle, deviceStatus);
      }
   }

   for (size_t itr = 0; itr != _deviceList.size(); itr++)
   {
      if (_deviceList[itr].id == deviceHandle && deviceStatus == T_e8_BTSetDeviceStatus__e8DEVICE_DELETED)
      {
         if (_pUserwords != NULL)
         {
            _pUserwords->vDeleteAllUserwords(deviceHandle);
         }
      }
   }

   _deviceList.clear();
   _userConnectedState = false;
   const T_BTSetDeviceListResult& odeviceListResult = status->getODeviceListResult();
   for (size_t i = 0; i != odeviceListResult.size(); ++i)
   {
      const T_BTSetDeviceListResultItem& resultDevice = odeviceListResult[i];
      DeviceDescriptor device;
      device.id = resultDevice.getU8DeviceHandle();
      device.name = resultDevice.getSDeviceName();
      if (resultDevice.getBOutgoingSourceDeviceStatus())
      {
         _userConnectedState = true;
         _connectedDevice = device;

         updateContactLists();
      }
      _deviceList.push_back(device);

      DeviceDescriptor vehiclePhonebookDevice;
      vehiclePhonebookDevice.id = device.id + VPB_OFFSET;
      vehiclePhonebookDevice.name =  "PhoneBook_VPB" + StringUtils::toString(device.id);
      _deviceList.push_back(vehiclePhonebookDevice);
   }

   if (_pUserwords != NULL && _userConnectedState)
   {
      _pUserwords->vSetActiveProfile(_connectedDevice.id);
   }

   handlePhoneStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_PhoneStatus::handlePhoneStatus()
{
   if (_handsetMode)
   {
      _phoneStatus.enType = sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_HANDSET_MODE;
   }
   else if (_userConnectedState)
   {
      if (_callPresent == false)
      {
         _phoneStatus.enType = sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_IDLE;
      }
      else if (_firstCallStatus == T_e8_TelCallStatus__e8BUSY || _secondCallStatus == T_e8_TelCallStatus__e8BUSY)
      {
         _phoneStatus.enType = sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_BUSY;
      }
      else if ((_firstCallStatus  == T_e8_TelCallStatus__e8ACTIVE && _secondCallStatus == T_e8_TelCallStatus__e8IDLE) ||
               (_firstCallStatus  == T_e8_TelCallStatus__e8IDLE && _secondCallStatus == T_e8_TelCallStatus__e8ACTIVE))
      {
         _phoneStatus.enType = sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_INCALL;
      }
      else if (_firstCallStatus == T_e8_TelCallStatus__e8DIALING || _secondCallStatus == T_e8_TelCallStatus__e8DIALING)
      {
         _phoneStatus.enType = sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_DIALLING;
      }
      else if (_firstCallStatus == T_e8_TelCallStatus__e8RINGTONE || _secondCallStatus == T_e8_TelCallStatus__e8RINGTONE)
      {
         _phoneStatus.enType = sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_RINGING;
      }
      else if ((_firstCallStatus == T_e8_TelCallStatus__e8ON_HOLD && _secondCallStatus == T_e8_TelCallStatus__e8ACTIVE) ||
               (_firstCallStatus == T_e8_TelCallStatus__e8ACTIVE && _secondCallStatus == T_e8_TelCallStatus__e8ON_HOLD))
      {
         _phoneStatus.enType = sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_INCALL_HOLD;
      }
      else
      {
         _phoneStatus.enType = sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_UNKNOWN;
      }
   }
   else if (_deviceList.size())
   {
      _phoneStatus.enType = sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_PHONE_NOT_READY;
   }
   else
   {
      _phoneStatus.enType =  sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_NOT_AVAILABLE;
   }

   vSendPhoneStatus();
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Property_PhoneStatus::onDownloadStateError(
   const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< DownloadStateError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_PhoneStatus::onDownloadStateStatus(
   const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< DownloadStateStatus >& status)
{
   if (status->getODownloadStateStream().size())
   {
      const T_PhonBkDownloadStateStreamItem oDownloadStatus = status->getODownloadStateStream()[0];
      eGetSdsTypePhonebookStatus(oDownloadStatus);
   }
   vSendPhoneStatus();
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Property_PhoneStatus::onPreferredPhoneBookSortOrderError(
   const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< PreferredPhoneBookSortOrderError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Property_PhoneStatus::onPreferredPhoneBookSortOrderStatus(
   const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< PreferredPhoneBookSortOrderStatus >& status)
{
   if (status->getE8PreferredPhoneBookSortOrder() ==  T_e8_PhonBkPreferredPhoneBookSortOrder__e8PREFERRED_SORT_ORDER_FIRSTNAME)
   {
      _preferredSortingOrder.enType = sds2hmi_fi_tcl_e8_PHN_Transorder::FI_EN_FIRSTNAME_LASTNAME;
   }
   else if (status->getE8PreferredPhoneBookSortOrder() ==  T_e8_PhonBkPreferredPhoneBookSortOrder__e8PREFERRED_SORT_ORDER_LASTNAME)
   {
      _preferredSortingOrder.enType = sds2hmi_fi_tcl_e8_PHN_Transorder::FI_EN_LASTNAME_FIRSTNAME;
   }
   else
   {
      _preferredSortingOrder.enType = sds2hmi_fi_tcl_e8_PHN_Transorder::FI_EN_UNKNOWN;
   }
   vSendPhoneStatus();
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Property_PhoneStatus::onCreateContactListError(
   const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< CreateContactListError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Property_PhoneStatus::onCreateContactListResult(
   const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< CreateContactListResult >& result)
{
   const uint16 listHandle = result->getU16ListHandle();

   _listHandlesMap[listHandle] = (uint16)_connectedDevice.id;
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Property_PhoneStatus::onListChangeError(
   const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ListChangeError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Property_PhoneStatus::onListChangeStatus(
   const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< ListChangeStatus >& status)
{
   const uint16 listHandle = status->getU16ListHandle();
   const T_e8_PhonBkListChangeType listChangeType = status->getE8ListChangeType();

   if (_listHandlesMap.find(listHandle) != _listHandlesMap.end()
         && T_e8_PhonBkListChangeType__e8LCH_CONTENT_CHANGED != listChangeType)
   {
      ETG_TRACE_USR4(("clSDS_Property_PhoneStatus: onListChangeStatus - listHandle = %d", listHandle));
      _listHandlesMap[listHandle] = status->getU16ListLength();

      _connectedPhonedeviceStatus.enType = sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_UPDATING;
      vSendPhoneStatus();
      _connectedPhonedeviceStatus.enType = sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_READY;
      vSendPhoneStatus();
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
uint8 clSDS_Property_PhoneStatus::getDeviceHandle() const
{
   return _inCallDeviceHandle;
}


/***********************************************************************//**
 *
 ***************************************************************************/
sds2hmi_fi_tcl_e8_PHN_Status::tenType clSDS_Property_PhoneStatus::getPhoneStatus() const
{
   return _phoneStatus.enType;
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Property_PhoneStatus::setPhoneStatusObserver(clSDS_PhoneStatusObserver* obs)
{
   if (obs != NULL)
   {
      ETG_TRACE_USR1(("clSDS_Property_PhoneStatus::setPhoneStatusObserver:NULL"));
      _phoneStatusObserverList.push_back(obs);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Property_PhoneStatus::updateContactLists()
{
   std::map<uint16, uint16>::const_iterator itr;
   bool isNewDevice = true;
   for (itr = _listHandlesMap.begin(); itr != _listHandlesMap.end(); ++itr)
   {
      if (itr->second == _connectedDevice.id)
      {
         isNewDevice = false;
      }
   }

   if (isNewDevice)
   {
      _phoneBookProxy->sendCreateContactListStart(*this, (uint8)_connectedDevice.id, T_e8_PhonBkContactListType__PHONEBOOK, T_e8_PhonBkContactSortType__e8PB_LIST_SORT_POSITION);
      _phoneBookProxy->sendCreateContactListStart(*this, (uint8)_connectedDevice.id, T_e8_PhonBkContactListType__VEHICLE, T_e8_PhonBkContactSortType__e8PB_LIST_SORT_POSITION);
   }
}
