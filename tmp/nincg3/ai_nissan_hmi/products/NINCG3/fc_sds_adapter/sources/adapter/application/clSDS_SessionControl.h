/*********************************************************************//**
 * \file       clSDS_SessionControl.h
 *
 * SDS session management
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_SessionControl_h
#define clSDS_SessionControl_h


#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"

#include "application/clSDS_SDSStatusObserver.h"
#include "sds_gui_fi/SdsGuiServiceConst.h"


class clSDS_Property_PhoneStatus;
class clSDS_SDSStatus;
class clSDS_SdsControl;


class clSDS_SessionControl : public clSDS_SDSStatusObserver
{
   public:
      virtual ~clSDS_SessionControl();
      clSDS_SessionControl(clSDS_SDSStatus* pSDSStatus, clSDS_Property_PhoneStatus* pPhoneStatus, clSDS_SdsControl* pSdsControl);
      tVoid vPttLongPressed()const;
      tVoid vBackButtonPressed()const;
      tVoid vSwcPhoneEndPressed()const;
      tVoid vAbortSession()const;
      tVoid vStoreSessionContext(sds2hmi_fi_tcl_e8_SDS_EntryPoint::tenType startupContext);
      tVoid vStopSession()const;
      tVoid vSDSStatusChanged();
      tBool bIsPttPressAllowed() const;
      tBool bDownloadFailed() const;
      tBool bIsInitialising() const;
      tBool bIsSessionActive() const;
      tBool bIsListening() const;
      tVoid vSendPttEvent();
      void vSetSpeaker(tU16 u16SpeakerId);
      void vRequestAllSpeakers();
      void sendStartSessionContext(sds_gui_fi::SdsGuiService::ContextType contextId);
      void sendEnterPauseMode();
      void sendExitPauseMode();

   private:
      sds2hmi_fi_tcl_e8_SDS_EntryPoint::tenType oGetStartupContext() const;
      tBool bSingleCallDtmfSession() const;
      tBool bMultipleCallDtmfSession() const;
      sds2hmi_fi_tcl_e8_SDS_EntryPoint::tenType mapGui2SdsStartupContext(sds_gui_fi::SdsGuiService::ContextType startupContext) const;

      clSDS_SDSStatus* _pSDSStatus;
      clSDS_Property_PhoneStatus* _pPhoneStatus;
      clSDS_SdsControl* _pSdsControl;
      sds2hmi_fi_tcl_e8_SDS_EntryPoint::tenType _startupContext;
};


#endif
