/*********************************************************************//**
 * \file       clSDS_MenuControl.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef clSDS_MenuControl_h
#define clSDS_MenuControl_h

class clSDS_MenuControl
{
   public:

      virtual ~clSDS_MenuControl()
      {
      }
      clSDS_MenuControl()
      {
      }

      virtual void onElementSelected(unsigned int selectedIndex) = 0;
      virtual void onCursorMoved(unsigned int cursorIndex) = 0;
      virtual void onRequestNextPage() = 0;
      virtual void onRequestPreviousPage() = 0;
      virtual void onSettingsRequest() = 0;
      virtual void onHelpRequest() = 0;
};


#endif
