/*********************************************************************//**
 * \file       SdsPhoneService.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "application/SdsPhoneService.h"
#include "application/clSDS_Userwords.h"


using namespace sds_gui_fi::SdsPhoneService;


SdsPhoneService::SdsPhoneService(clSDS_Userwords* pUserWords)
   : SdsPhoneServiceStub("SdsPhoneServicePort")
   , _pUserWords(pUserWords)
{
}


SdsPhoneService::~SdsPhoneService()
{
   _pUserWords = NULL;
}


void SdsPhoneService::onQuickDialListUpdateRequest(const ::boost::shared_ptr< QuickDialListUpdateRequest >& request)
{
   if (_pUserWords != NULL)
   {
      _pUserWords->handleQuickDialList(request);
   }
}


void SdsPhoneService::onVoiceTagActionRequest(const ::boost::shared_ptr< VoiceTagActionRequest >& request)
{
   if (_pUserWords != NULL)
   {
      _pUserWords->handleUserWordActions(request);
   }
}
