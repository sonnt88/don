/*********************************************************************//**
 * \file       clSDS_Method_MediaPlay.cpp
 *
 * clSDS_Method_MediaPlay method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_MediaPlay.h"
#include "AppHmi_MasterBase/AudioInterface/AudioDefines.h"
#include "Sds2HmiServer/functions/clSDS_Method_MediaGetAmbiguityList.h"
#include "Sds2HmiServer/functions/clSDS_Method_MediaGetDeviceInfo.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_MediaPlay.cpp.trc.h"
#endif


using namespace bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange;
using namespace mplay_MediaPlayer_FI;
using namespace MPlay_fi_types;


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_MediaPlay::~clSDS_Method_MediaPlay()
{
   _pMediaGetAmbiguityList = NULL;
   _pMediaGetDeviceInfo = NULL;
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_MediaPlay::clSDS_Method_MediaPlay(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::AudioSourceChangeProxy > audioSourceChangeProxy,
   ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy > mediaPlayerProxy,
   clSDS_Method_MediaGetAmbiguityList* pMediaGetAmbiguityList,
   GuiService& guiService,
   clSDS_Method_MediaGetDeviceInfo* pMediaGetDeviceInfo)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_MEDIAPLAY, pService)
   , _mediaPlayerProxy(mediaPlayerProxy)
   , _audioSourceChangeProxy(audioSourceChangeProxy)
   , _guiService(guiService)
   , _pMediaGetAmbiguityList(pMediaGetAmbiguityList)
   , _pMediaGetDeviceInfo(pMediaGetDeviceInfo)
   , _sourceInfoArray()
   , _oDeviceList()
   , _count(0)
   , _sourceListSize(0)
   , _u32MediaId(0)
   , _tagID(0)
   , _e8DeviceType(T_e8_MPlayDeviceType__e8DTY_UNKNOWN)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_MediaPlay::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgMediaPlayMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   vProcessMessage(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_MediaPlay::vProcessMessage(const sds2hmi_sdsfi_tclMsgMediaPlayMethodStart& oMessage)
{
   switch (oMessage.SelectionType.enType)
   {
      case sds2hmi_fi_tcl_e8_GEN_SelectionType::FI_EN_BYVAL:
         vProcessMediaRequests(oMessage);
         break;

      case sds2hmi_fi_tcl_e8_GEN_SelectionType::FI_EN_BYTRACK:
         vCdPlayByTrack(oMessage.ValueIDs[0]);
         break;

      case sds2hmi_fi_tcl_e8_GEN_SelectionType::FI_EN_BYSOURCE:
         vPlaySource(oMessage.DriveID);
         break;

      default:
         vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
         break;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_MediaPlay::vProcessMediaRequests(const sds2hmi_sdsfi_tclMsgMediaPlayMethodStart& oMessage)
{
   switch (oMessage.Action.enType)
   {
      case sds2hmi_fi_tcl_e8_PlayAction::FI_EN_SET:
         if (!oMessage.ValueIDs.empty())
         {
            _u32MediaId = oMessage.ValueIDs[0];
            _mediaPlayerProxy->sendGetMediaObjectStart(*this, _u32MediaId , ::MPlay_fi_types::T_e8_MPlayCategoryType__e8CTY_NONE);
         }
         else
         {
            vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
         }
         break;

      case sds2hmi_fi_tcl_e8_PlayAction::FI_EN_PLAY:
         _mediaPlayerProxy->sendPlayMediaPlayerObjectStart(*this, _u32MediaId, true);
         break;

      default:
         vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
         break;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_MediaPlay::vCdPlayByTrack(tU32 u32TrackId)
{
   // first do a source change to CD
   enGetHmiSourceType(sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_CDA);

   // play the track number
   if (_pMediaGetDeviceInfo != NULL)
   {
      unsigned int listHandle = _pMediaGetDeviceInfo->getListHandle();
      _mediaPlayerProxy->sendPlayItemFromListStart(*this, listHandle, u32TrackId, 0);
      vSendMethodResult();
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_MediaPlay::vPlaySource(tU32 u32SdsSourceId)
{
   sds2hmi_fi_tcl_e8_MPL_SourceType::tenType sourcetype =  enGetHmiSourceType(u32SdsSourceId);

   if (sourcetype != sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_UNKNOWN)
   {
      vSendMethodResult();
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
sds2hmi_fi_tcl_e8_MPL_SourceType::tenType clSDS_Method_MediaPlay::enGetHmiSourceType(tU32 u32SdsSourceId)
{
   sds2hmi_fi_tcl_e8_MPL_SourceType::tenType enSourceId = static_cast<sds2hmi_fi_tcl_e8_MPL_SourceType::tenType>(u32SdsSourceId);
   mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsStatus mediastatus = _mediaPlayerProxy->getMediaPlayerDeviceConnections();
   ::MPlay_fi_types::T_MPlayDeviceInfo oDeviceInfo = mediastatus.getODeviceInfo();

   switch (enSourceId)
   {
      case sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_IPOD:
         for (tU32 u32Index = 0; u32Index < oDeviceInfo.size(); u32Index++)
         {
            if (((_oDeviceList[u32Index].uiDeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPOD) ||
                  (_oDeviceList[u32Index].uiDeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPHONE)) && (_oDeviceList[u32Index].bIsConnected == TRUE))
            {
               activateSource(u32Index);
               break;
            }
         }
         return sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_IPOD;

      case sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_USB:
         for (tU32 u32Index = 0; u32Index < oDeviceInfo.size(); u32Index++)
         {
            if ((_oDeviceList[u32Index].uiDeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_USB) && (_oDeviceList[u32Index].bIsConnected == TRUE))
            {
               activateSource(u32Index);
               break;
            }
         }
         return sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_USB;

      case sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_CDA:
         for (tU32 u32Index = 0; u32Index < oDeviceInfo.size(); u32Index++)
         {
            if (_oDeviceList[u32Index].uiDeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA)
            {
               activateSource(u32Index);
               break;
            }
         }
         return sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_CDA;

      case sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_LINE_IN:
         //AUX source is always available, hence availability is not checked
         requestSourceActivation(SRC_MEDIA_AUX, -1);
         return sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_LINE_IN;

      case sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_BTAUDIO:
         for (tU32 u32Index = 0; u32Index < oDeviceInfo.size(); u32Index++)
         {
            if ((_oDeviceList[u32Index].uiDeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH)
                  && (_oDeviceList[u32Index].bIsConnected == TRUE))
            {
               requestSourceActivation(SRC_PHONE_BTAUDIO, -1);
               break;
            }
         }
         return sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_BTAUDIO;

      case sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_SDCARD:
         for (tU32 u32Index = 0; u32Index < oDeviceInfo.size(); u32Index++)
         {
            if (_oDeviceList[u32Index].uiDeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_SD)
            {
               activateSource(u32Index);
               break;
            }
         }
         return sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_SDCARD;

      case sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_CARPLAY_AUD:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_PLAY_CARPLAY_AUDIO);
         return sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_CARPLAY_AUD;

      case sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_ANDROID_AUD:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_PLAY_ANDROID_AUTO_AUDIO);
         return sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_ANDROID_AUD;

      default:
         return sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_UNKNOWN;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_MediaPlay::vMediaPlayStatus(tBool bResult)
{
   ETG_TRACE_USR1(("clSDS_Method_MediaPlay::vMediaPlayStatus"));
   clSDS_Method_MediaPlay::AudioSource enMediaAudioSource = enGetMediaAudioSource();
   if (bResult && (enMediaAudioSource != clSDS_Method_MediaPlay::NONE))
   {
      vSendMethodResult();
   }
   else
   {
      // TBD Error for Media Id not available should be defined by SDS
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_UNKNOWNERROR);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_MediaPlay::vMediaObjectValidationResponse(tBool bIsMediaObjectValid)
{
   if (bIsMediaObjectValid == TRUE)
   {
      vSendMethodResult();
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_MEDIAOBJNOTAVAILABLE);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
clSDS_Method_MediaPlay::AudioSource clSDS_Method_MediaPlay::enGetMediaAudioSource()
{
   mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsStatus mediastatus = _mediaPlayerProxy->getMediaPlayerDeviceConnections();
   ::MPlay_fi_types::T_MPlayDeviceInfo oDeviceInfo = mediastatus.getODeviceInfo();

   switch (_e8DeviceType)
   {
      case T_e8_MPlayDeviceType__e8DTY_USB:
         for (tU32 u32Index = 0; u32Index < oDeviceInfo.size(); u32Index++)
         {
            if ((_oDeviceList[u32Index].uiDeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_USB))
            {
               activateSource(u32Index);
               break;
            }
         }
         return clSDS_Method_MediaPlay::MEDIA_DB_PLAYER;

      case T_e8_MPlayDeviceType__e8DTY_IPOD:
      case T_e8_MPlayDeviceType__e8DTY_IPHONE:
         for (tU32 u32Index = 0; u32Index < oDeviceInfo.size(); u32Index++)
         {
            if ((_oDeviceList[u32Index].uiDeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPOD) ||
                  (_oDeviceList[u32Index].uiDeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPHONE))
            {
               activateSource(u32Index);
               break;
            }
         }
         return clSDS_Method_MediaPlay::MEDIA_IPOD;

      default:
         return clSDS_Method_MediaPlay::NONE;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _audioSourceChangeProxy)
   {
      _audioSourceChangeProxy->sendGetSourceListRequest(*this, 2);
      _audioSourceChangeProxy->sendSourceListChangedRegister(*this);
   }

   if (proxy == _mediaPlayerProxy)
   {
      _mediaPlayerProxy->sendMediaPlayerDeviceConnectionsUpReg(*this);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _audioSourceChangeProxy)
   {
      _audioSourceChangeProxy->sendDeregisterAll();
   }
   if (proxy == _mediaPlayerProxy)
   {
      _mediaPlayerProxy->sendMediaPlayerDeviceConnectionsRelUpRegAll();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::onGetSourceListError(
   const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/,
   const ::boost::shared_ptr< GetSourceListError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::onGetSourceListResponse(
   const ::boost::shared_ptr< AudioSourceChangeProxy >& proxy,
   const ::boost::shared_ptr< GetSourceListResponse >& response)
{
   if (_audioSourceChangeProxy == proxy)
   {
      const ::std::vector< sourceDetails >& sourceList = response->getSources();
      ::std::vector< sourceDetails >::const_iterator it = sourceList.begin();
      _sourceListSize = sourceList.size();

      //When a New list update arrives, reset the availability of all sources - Set to FALSE
      _sourcelist.clear();
      for (_count = 0; it != sourceList.end(); it++, ++_count)
      {
         // Update the list info using UpdateSourceList
         _sourceInfoArray[_count].sourceID = it->getSrcId();
         _sourceInfoArray[_count].subSourceID = it->getSubSrcId();
         _sourcelist.push_back(_sourceInfoArray[_count]);
      }
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::onActivateSourceError(
   const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/,
   const ::boost::shared_ptr< ActivateSourceError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::onActivateSourceResponse(
   const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/,
   const ::boost::shared_ptr< ActivateSourceResponse >& /*response*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::onSourceListChangedError(
   const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/,
   const ::boost::shared_ptr< SourceListChangedError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::onSourceListChangedSignal(
   const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/,
   const ::boost::shared_ptr< SourceListChangedSignal >& /*signal*/)
{
   if (_audioSourceChangeProxy)
   {
      _audioSourceChangeProxy->sendGetSourceListRequest(*this, GROUP_MEDIA);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay:: onMediaPlayerDeviceConnectionsError(
   const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
   const boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::onMediaPlayerDeviceConnectionsStatus(
   const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
   const boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsStatus >& status)
{
   for (size_t i = 0 ; i < status->getODeviceInfo().size(); i++)
   {
      const MPlay_fi_types::T_MPlayDeviceInfoItem& item = status->getODeviceInfo()[i];
      _oDeviceList[i].bIsConnected = item.getBDeviceConnected();
      _oDeviceList[i].uiDeviceTag = item.getU8DeviceTag();
      _oDeviceList[i].uiDeviceType = item.getE8DeviceType();
      ETG_TRACE_USR1(("bIsConnected  %d uiDeviceTag %d  uiDeviceType %d",
                      _oDeviceList[i].bIsConnected, _oDeviceList[i].uiDeviceTag, _oDeviceList[i].uiDeviceType));
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::onPlayMediaPlayerObjectError(
   const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< mplay_MediaPlayer_FI::PlayMediaPlayerObjectError >& /*error*/)
{
   vMediaPlayStatus(FALSE);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::onPlayMediaPlayerObjectResult(
   const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< mplay_MediaPlayer_FI::PlayMediaPlayerObjectResult >& /*result*/)
{
   vMediaPlayStatus(TRUE);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::requestSourceActivation(int32 SrcID, int32 SubSrcID)
{
   sourceData sourceInfo(SrcID, SubSrcID, 0);
   _audioSourceChangeProxy->sendActivateSourceRequest(*this, sourceInfo);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::activateSource(tU32 u32Index)
{
   for (tU32 Index = 0; Index < _sourceListSize; Index++)
   {
      if ((int)_oDeviceList[u32Index].uiDeviceTag == _sourceInfoArray[Index].subSourceID)
      {
         requestSourceActivation(_sourceInfoArray[Index].sourceID, _sourceInfoArray[Index].subSourceID);
         break;
      }
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::onPlayItemFromListError(
   const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< mplay_MediaPlayer_FI::PlayItemFromListError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::onPlayItemFromListResult(
   const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< mplay_MediaPlayer_FI::PlayItemFromListResult >& /*result*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::onGetMediaObjectError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< mplay_MediaPlayer_FI::GetMediaObjectError >& /*error*/)
{
   vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_MediaPlay::onGetMediaObjectResult(const ::boost::shared_ptr<mplay_MediaPlayer_FI:: Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< mplay_MediaPlayer_FI::GetMediaObjectResult >& result)
{
   const T_MPlayMediaObject& oMediaObject = result->getOMediaObject();
   _e8DeviceType = oMediaObject.getE8DeviceType();
   _tagID = oMediaObject.getU32Tag();
   vSendMethodResult();
}
