
/**************************************************************************//**
 * \file     clSDS_LanguageMediator.cpp
 *
 *           Takes care that the SDS language is consistent with the current
 *           menu language.
 *
 * \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
 ******************************************************************************/
#include "application/clSDS_LanguageMediator.h"
#include "application/clSDS_MyAppsList.h"
#include "application/clSDS_SDSStatus.h"
#include "application/clSDS_SdsControl.h"
#include "application/clSDS_SessionControl.h"
#include "view_db/Sds_TextDB.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_LanguageMediator.cpp.trc.h"
#endif


#define ARRAY_SIZE(array)	(sizeof (array) / sizeof (array)[0])


struct LanguageMapping
{
   sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::tenType sdsLanguageCode;
   sds2hmi_fi_tcl_e16_ISOCountryCode::tenType sdsCountryCode;
   Sds_TextLanguageId viewDBLanguage;
   vehicle_main_fi_types::T_e8_Language_Code vehicleLanguage;
};


/**
 * The language map is used to translate the various language identifiers from SDS, Vehicle-FI
 * and ViewDB into each other.
 */
static const LanguageMapping g_languageMap[] =
{
   { sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_ENG, sds2hmi_fi_tcl_e16_ISOCountryCode::FI_EN_ISO_ALPHA_3_USA, SDS_TEXT_LANGUAGE_ENG_USA, vehicle_main_fi_types::T_e8_Language_Code__English_US      },
   { sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_FRA, sds2hmi_fi_tcl_e16_ISOCountryCode::FI_EN_ISO_ALPHA_3_CAN, SDS_TEXT_LANGUAGE_FRA_CAN, vehicle_main_fi_types::T_e8_Language_Code__French_Canadian },
   { sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_SPA, sds2hmi_fi_tcl_e16_ISOCountryCode::FI_EN_ISO_ALPHA_3_MEX, SDS_TEXT_LANGUAGE_SPA_MEX, vehicle_main_fi_types::T_e8_Language_Code__Spanish_Mexican },
   { sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_DEU, sds2hmi_fi_tcl_e16_ISOCountryCode::FI_EN_ISO_ALPHA_3_DEU, SDS_TEXT_LANGUAGE_DEU_DEU, vehicle_main_fi_types::T_e8_Language_Code__German          },
   { sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_ENG, sds2hmi_fi_tcl_e16_ISOCountryCode::FI_EN_ISO_ALPHA_3_GBR, SDS_TEXT_LANGUAGE_ENG_GBR, vehicle_main_fi_types::T_e8_Language_Code__English_UK      },
   { sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_FRA, sds2hmi_fi_tcl_e16_ISOCountryCode::FI_EN_ISO_ALPHA_3_FRA, SDS_TEXT_LANGUAGE_FRA_FRA, vehicle_main_fi_types::T_e8_Language_Code__French          },
   { sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_SPA, sds2hmi_fi_tcl_e16_ISOCountryCode::FI_EN_ISO_ALPHA_3_ESP, SDS_TEXT_LANGUAGE_SPA_ESP, vehicle_main_fi_types::T_e8_Language_Code__Spanish         },
   { sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_NLD, sds2hmi_fi_tcl_e16_ISOCountryCode::FI_EN_ISO_ALPHA_3_NLD, SDS_TEXT_LANGUAGE_NLD_NLD, vehicle_main_fi_types::T_e8_Language_Code__Dutch           },
   { sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_ITA, sds2hmi_fi_tcl_e16_ISOCountryCode::FI_EN_ISO_ALPHA_3_ITA, SDS_TEXT_LANGUAGE_ITA_ITA, vehicle_main_fi_types::T_e8_Language_Code__Italian         },
   { sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_POR, sds2hmi_fi_tcl_e16_ISOCountryCode::FI_EN_ISO_ALPHA_3_PRT, SDS_TEXT_LANGUAGE_POR_PRT, vehicle_main_fi_types::T_e8_Language_Code__Portuguese      },
   { sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_RUS, sds2hmi_fi_tcl_e16_ISOCountryCode::FI_EN_ISO_ALPHA_3_RUS, SDS_TEXT_LANGUAGE_RUS_RUS, vehicle_main_fi_types::T_e8_Language_Code__Russian         },
};


/**************************************************************************//**
*
******************************************************************************/
static sds2hmi_fi_tcl_SDSLanguageID convertVehicleToSdsLanguage(::vehicle_main_fi_types::T_e8_Language_Code vehicleLanguage)
{
   sds2hmi_fi_tcl_SDSLanguageID sdsLanguage;
   for (size_t i = 0; i < ARRAY_SIZE(g_languageMap); i++)
   {
      if (g_languageMap[i].vehicleLanguage == vehicleLanguage)
      {
         sdsLanguage.ISO639_3_SDSLanguageCode.enType = g_languageMap[i].sdsLanguageCode;
         sdsLanguage.ISO3166_CountryCode.enType = g_languageMap[i].sdsCountryCode;
         break;
      }
   }
   return sdsLanguage;
}


/**************************************************************************//**
*
******************************************************************************/
static vehicle_main_fi_types::T_e8_Language_Code convertSdsToVehicleLanguage(const sds2hmi_fi_tcl_SDSLanguageID& sdsLanguage)
{
   for (size_t i = 0; i < ARRAY_SIZE(g_languageMap); i++)
   {
      if ((g_languageMap[i].sdsLanguageCode == sdsLanguage.ISO639_3_SDSLanguageCode.enType) &&
            (g_languageMap[i].sdsCountryCode == sdsLanguage.ISO3166_CountryCode.enType))
      {
         return g_languageMap[i].vehicleLanguage;
      }
   }
   return ::vehicle_main_fi_types::T_e8_Language_Code__UnSupported;
}


/**************************************************************************//**
*
******************************************************************************/
Sds_TextLanguageId convertSdsToViewDBLanguage(const sds2hmi_fi_tcl_SDSLanguageID& sdsLanguage)
{
   for (size_t i = 0; i < ARRAY_SIZE(g_languageMap); i++)
   {
      if ((g_languageMap[i].sdsLanguageCode == sdsLanguage.ISO639_3_SDSLanguageCode.enType) &&
            (g_languageMap[i].sdsCountryCode == sdsLanguage.ISO3166_CountryCode.enType))
      {
         return g_languageMap[i].viewDBLanguage;
      }
   }
   return SDS_TEXT_LANGUAGE_ENG_USA;
}


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_LanguageMediator::~clSDS_LanguageMediator()
{
   _pSDSStatus = NULL;
   _pSdsControl = NULL;
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_LanguageMediator::clSDS_LanguageMediator(
   clSDS_SDSStatus* pSDSStatus,
   SettingsService& settingsService,
   ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >vehicleProxy)

   : _actionRequestAvailable(false)
   , _bWaitingForResult(FALSE)
   , _u32RetryCounter(0)
   , _activeSpeaker(0)
   , _pSDSStatus(pSDSStatus)
   , _settingsService(settingsService)
   , _vehicleProxy(vehicleProxy)
   , _pSdsControl(NULL)
{
   _pSDSStatus->vRegisterObserver(this);
   _settingsService.addObserver(this);
}


/**************************************************************************//**
* Request SDS to set a new language.
******************************************************************************/
tVoid clSDS_LanguageMediator::sendSpeakerRequest(tU16 speakerId)
{
   _bWaitingForResult = TRUE;
   if (_pSdsControl)
   {
      _pSdsControl->vSetSpeaker(speakerId);
   }
   _u32RetryCounter++;
}


/**************************************************************************//**
* Returns TRUE if SDS is ready for setting another language. Otherwise FALSE.
******************************************************************************/
tBool clSDS_LanguageMediator::bSdsIsReadyToSetLanguage() const
{
   return (_pSDSStatus->bIsIdle() || _pSDSStatus->bIsError());
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_LanguageMediator::vSynchroniseSdsLanguage()
{
   if (_bWaitingForResult == TRUE)
   {
      ETG_TRACE_COMP(("LanguageMediator: waiting for result"));
      return;
   }

   sds2hmi_fi_tcl_SDSLanguageID systemLanguage = convertVehicleToSdsLanguage(_systemLanguage);
   tU16 requiredSpeaker = getSpeakerIdForLanguage(systemLanguage, _preferredGender);

   if (requiredSpeaker == 0)
   {
      ETG_TRACE_COMP(("LanguageMediator: speaker not available"));
      return;
   }

   if (requiredSpeaker == _activeSpeaker)
   {
      Sds_TextDB_vSetLanguage(convertSdsToViewDBLanguage(systemLanguage));
      updateSdsLanguageInVehicleData();
      ETG_TRACE_COMP(("LanguageMediator: languages are synchronized"));
      return;
   }

   if (bTooManyRetries())
   {
      ETG_TRACE_COMP(("LanguageMediator: too many retries"));
      return;
   }

   if (!bSdsIsReadyToSetLanguage())
   {
      ETG_TRACE_COMP(("LanguageMediator: SDS not ready"));
      return;
   }

   if (!_actionRequestAvailable)
   {
      ETG_TRACE_COMP(("LanguageMediator: property CommonActionRequest not yet registered"));
      return;
   }

   sendSpeakerRequest(requiredSpeaker);
   ETG_TRACE_COMP(("LanguageMediator: SDS language change triggered"));
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_LanguageMediator::updateSdsLanguageInVehicleData()
{
   if (_vehicleProxy->isAvailable() && _vehicleProxy->hasLanguage())
   {
      const ::std::vector< ::vehicle_main_fi_types::T_Language_SourceTable >& sourceTable = _vehicleProxy->getLanguage().getLangTable();
      ::std::vector< ::vehicle_main_fi_types::T_Language_SourceTable >::const_iterator iter = sourceTable.begin();
      ::vehicle_main_fi_types::T_e8_Language_Code sdsLanguage = convertSdsToVehicleLanguage(getLanguageOfActiveSpeaker());
      while (iter != sourceTable.end())
      {
         if ((iter->getEnLangSrcId() == ::vehicle_main_fi_types::T_e8_Language_SourceId__SDS) &&
               (iter->getEnLanguage() != sdsLanguage))
         {
            _vehicleProxy->sendSetLanguageStart(*this, ::vehicle_main_fi_types::T_e8_Language_SourceId__SDS, sdsLanguage);
            break;
         }
         ++iter;
      }
   }
}


/**************************************************************************//**
* Returns TRUE if retries are greater than 3.Otherwise FALSE.
******************************************************************************/
tBool clSDS_LanguageMediator::bTooManyRetries() const
{
   return (_u32RetryCounter >= 3);
}


/**************************************************************************//**
* Synchronising the SDS Language happens when SDS state changes
******************************************************************************/
tVoid clSDS_LanguageMediator::vSDSStatusChanged()
{
   vSynchroniseSdsLanguage();
}


/**************************************************************************//**
* Sets the active speaker as indicated by property SDS_ActiveSpeaker.
* An id of value 0 indicates an invalid speaker id.
******************************************************************************/
tVoid clSDS_LanguageMediator::setActiveSpeaker(tU16 activeSpeaker)
{
   _bWaitingForResult = FALSE;
   _activeSpeaker = activeSpeaker;
   if (activeSpeaker != 0)
   {
      _u32RetryCounter = 0;
   }
   vSynchroniseSdsLanguage();
}


/**************************************************************************//**
* Stores the available Languages supported by SDS
******************************************************************************/
tVoid clSDS_LanguageMediator::setAvailableSpeakers(const bpstl::vector<sds2hmi_fi_tcl_LanguageAndSpeaker>& availableSpeakers)
{
   _availableSpeakers = availableSpeakers;
   _u32RetryCounter = 0;
   vSynchroniseSdsLanguage();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_LanguageMediator::setActionRequestAvailable(tBool isAvailable)
{
   _actionRequestAvailable = isAvailable;
   vSynchroniseSdsLanguage();
}


/**************************************************************************//**
* Gets Language ID for selected menu Language
******************************************************************************/
sds2hmi_fi_tcl_SDSLanguageID clSDS_LanguageMediator::getLanguageOfActiveSpeaker() const
{
   for (tU32 u32Counter = 0; u32Counter < _availableSpeakers.size(); u32Counter++)
   {
      if (_availableSpeakers[u32Counter].SpeakerId == _activeSpeaker)
      {
         return _availableSpeakers[u32Counter].LanguageID;
      }
   }
   sds2hmi_fi_tcl_SDSLanguageID oLanguage;
   return oLanguage;
}


/**************************************************************************//**
*
******************************************************************************/
tU16 clSDS_LanguageMediator::getSpeakerIdForLanguage(
   const sds2hmi_fi_tcl_SDSLanguageID& language,
   sds2hmi_fi_tcl_e8_Gender gender)
{
   // first try to find speaker with preferred gender
   for (size_t i = 0; i < _availableSpeakers.size(); i++)
   {
      if ((_availableSpeakers[i].LanguageID == language) &&
            (_availableSpeakers[i].Gender == gender))
      {
         return _availableSpeakers[i].SpeakerId;
      }
   }

   // find speaker disregarding preferred speaker
   for (size_t i = 0; i < _availableSpeakers.size(); i++)
   {
      if (_availableSpeakers[i].LanguageID == language)
      {
         return _availableSpeakers[i].SpeakerId;
      }
   }

   // TODO jnd2hi: determine fall-back language here and try to find speaker

   return 0;
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_LanguageMediator::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_vehicleProxy == proxy)
   {
      _vehicleProxy ->sendLanguageUpReg(*this);
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_LanguageMediator::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_vehicleProxy == proxy)
   {
      _vehicleProxy->sendLanguageRelUpRegAll();
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_LanguageMediator::onLanguageError(
   const ::boost::shared_ptr< VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< VEHICLE_MAIN_FI::LanguageError >& /*error*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_LanguageMediator::onLanguageStatus(
   const ::boost::shared_ptr< VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< VEHICLE_MAIN_FI::LanguageStatus >& status)
{
   _systemLanguage = status->getLanguage();
   _u32RetryCounter = 0;
   vSynchroniseSdsLanguage();
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_LanguageMediator::onSetLanguageError(
   const ::boost::shared_ptr< VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< VEHICLE_MAIN_FI::SetLanguageError >& /*error*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_LanguageMediator::onSetLanguageResult(
   const ::boost::shared_ptr< VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< VEHICLE_MAIN_FI::SetLanguageResult >& /*result*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_LanguageMediator::vSetSDSControl(clSDS_SdsControl* pSdsControl)
{
   _pSdsControl = pSdsControl;
}


/**************************************************************************//**
 *
 ******************************************************************************/
sds2hmi_fi_tcl_e8_Gender clSDS_LanguageMediator::getSdsGender(sds_gui_fi::SettingsService::ActiveSpeakerGender guiGender) const
{
   sds2hmi_fi_tcl_e8_Gender gender;

   switch (guiGender)
   {
      case sds_gui_fi::SettingsService::ActiveSpeakerGender__FEMALE:
         gender.enType = sds2hmi_fi_tcl_e8_Gender::FI_EN_FEMALE;
         break;

      case sds_gui_fi::SettingsService::ActiveSpeakerGender__MALE:
      default:
         gender.enType = sds2hmi_fi_tcl_e8_Gender::FI_EN_MALE;
         break;
   }

   return gender;
}


void clSDS_LanguageMediator::onSettingsChanged()
{
   _preferredGender = getSdsGender(_settingsService.getVoicePreference());
   vSynchroniseSdsLanguage();
}
