/*********************************************************************//**
 * \file       clSDS_Method_PhoneDialContact.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_PhoneDialContact_h
#define clSDS_Method_PhoneDialContact_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "MOST_Tel_FIProxy.h"
#include "MOST_PhonBk_FIProxy.h"


class clSDS_Method_PhoneDialContact
   : public clServerMethod
   , public MOST_Tel_FI::DialCallbackIF
   , public MOST_PhonBk_FI::GetContactDetailsCallbackIF
{
   public:
      clSDS_Method_PhoneDialContact(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > pTelProxy,
         ::boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy > pPhoneBkProxy);
      virtual ~clSDS_Method_PhoneDialContact();

   private:
      virtual void vMethodStart(amt_tclServiceData* pInMsg);

      virtual void onDialError(
         const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Tel_FI::DialError >& error);
      virtual void onDialResult(
         const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Tel_FI::DialResult >& result);

      virtual void onGetContactDetailsError(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsError >& error);
      virtual void onGetContactDetailsResult(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsResult >& result);

      std::string getPhoneNumber(most_PhonBk_fi_types::T_PhonBkContactDetails contactDetails) const;

      boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > _telephoneProxy;
      boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy > _phoneBookProxy;

      sds2hmi_fi_tcl_e8_PHN_NumberType _numberType;
};


#endif
