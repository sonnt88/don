/*********************************************************************//**
 * \file       clSDS_FormatTimeDate.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_FormatTimeDate_h
#define clSDS_FormatTimeDate_h


#include "clock_main_fiProxy.h"


class clSDS_FormatTimeDate
   : public asf::core::ServiceAvailableIF
   , public clock_main_fi::TimeFormatCallbackIF
   , public clock_main_fi::DateFormatCallbackIF
   , public clock_main_fi::LocalTimeDate_MinuteUpdateCallbackIF
{
   public:
      clSDS_FormatTimeDate();    // default constructor without implementation
      clSDS_FormatTimeDate(::boost::shared_ptr< ::clock_main_fi::Clock_main_fiProxy > pClockproxy);
      ~clSDS_FormatTimeDate();

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onLocalTimeDate_MinuteUpdateError(
         const ::boost::shared_ptr< clock_main_fi::Clock_main_fiProxy >& proxy,
         const ::boost::shared_ptr< clock_main_fi::LocalTimeDate_MinuteUpdateError >& error);
      virtual void onLocalTimeDate_MinuteUpdateStatus(
         const ::boost::shared_ptr< clock_main_fi::Clock_main_fiProxy >& proxy,
         const ::boost::shared_ptr< clock_main_fi::LocalTimeDate_MinuteUpdateStatus >& status);

      virtual void onTimeFormatError(
         const ::boost::shared_ptr< clock_main_fi::Clock_main_fiProxy >& proxy,
         const ::boost::shared_ptr< clock_main_fi::TimeFormatError >& error);
      virtual void onTimeFormatStatus(
         const ::boost::shared_ptr< clock_main_fi::Clock_main_fiProxy >& proxy,
         const ::boost::shared_ptr< clock_main_fi::TimeFormatStatus >& status);

      virtual void onDateFormatError(
         const ::boost::shared_ptr< clock_main_fi::Clock_main_fiProxy >& proxy,
         const ::boost::shared_ptr< clock_main_fi::DateFormatError >& error);
      virtual void onDateFormatStatus(
         const ::boost::shared_ptr< clock_main_fi::Clock_main_fiProxy >& proxy,
         const ::boost::shared_ptr< clock_main_fi::DateFormatStatus >& status);

      void formatTimeDate(std::string hour, std::string minute, std::string day, std::string month, std::string year, std::string& ostring);

   private:

      struct Date
      {
         Date() : day(1), month(1), year(2000) {}
         int day;
         int month;
         int year;
      };

      int getDifferenceOfDays() const;
      void getSmsReceivedTime(std::string& oString);
      void getSmsReceivedDate(std::string& oString);
      void getReceivedDay(std::string& oString) const;
      void getTimeIn12HourFormat(std::string& oString);
      void getTimeIn24HourFormat(std::string& oString);

      boost::shared_ptr< ::clock_main_fi::Clock_main_fiProxy > _clockproxy;

      Date _currentDate;
      Date _receivedDate;

      clock_main_fi_types::VDCLK_TEN_TimeFormat _timeFormat;
      clock_main_fi_types::VDCLK_TEN_DateFormat _dateFormat;

      int _receivedSmsHour;
      int _receivedSmsMinute;
};


#endif
