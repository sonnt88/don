/*********************************************************************//**
 * \file       clSDS_Property_CommonStatus.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Property_CommonStatus_h
#define clSDS_Property_CommonStatus_h


#include "Sds2HmiServer/framework/clServerProperty.h"
#include "application/clSDS_IsVehicleMovingObserver.h"
#include "application/clSDS_KDSConfiguration.h"
#include "VEHICLE_MAIN_FIProxy.h"
#include "tcu_main_fiProxy.h"
#include "sxm_audio_main_fiProxy.h"


class clSDS_IsVehicleMoving;


class clSDS_Property_CommonStatus
   : public clServerProperty
   , public asf::core::ServiceAvailableIF
   , public clSDS_IsVehicleMovingObserver
   , public VEHICLE_MAIN_FI::SpeedCallbackIF
   , public tcu_main_fi::ProvisionServiceCallbackIF
   , public sxm_audio_main_fi::SxmDataServiceStatusCallbackIF
{
   public:
      virtual ~clSDS_Property_CommonStatus();
      clSDS_Property_CommonStatus(
         ahl_tclBaseOneThreadService* pService,
         clSDS_IsVehicleMoving* pIsVehicleMoving,
         ::boost::shared_ptr< VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy > pVehicleFiProxy,
         ::boost::shared_ptr< tcu_main_fi::Tcu_main_fiProxy > pTcuFiProxy,
         ::boost::shared_ptr< sxm_audio_main_fi::Sxm_audio_main_fiProxy > posxmAudiProxy);

      tVoid vVehicleSpeedChanged();
      tVoid vTCUServiceStatusChanged();
      tVoid vSportsServiceStatusChanged();

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onSpeedError(
         const ::boost::shared_ptr<  VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy,
         const ::boost::shared_ptr<  VEHICLE_MAIN_FI::SpeedError >& error);
      virtual void onSpeedStatus(
         const ::boost::shared_ptr<  VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy,
         const ::boost::shared_ptr<  VEHICLE_MAIN_FI::SpeedStatus >& status);

      virtual void onProvisionServiceError(
         const ::boost::shared_ptr< tcu_main_fi::Tcu_main_fiProxy >& proxy,
         const ::boost::shared_ptr< tcu_main_fi::ProvisionServiceError >& error);
      virtual void onProvisionServiceStatus(
         const ::boost::shared_ptr< tcu_main_fi::Tcu_main_fiProxy >& proxy,
         const ::boost::shared_ptr< tcu_main_fi::ProvisionServiceStatus >& status);

      virtual void onSxmDataServiceStatusError(
         const ::boost::shared_ptr<sxm_audio_main_fi::Sxm_audio_main_fiProxy >& proxy,
         const ::boost::shared_ptr< sxm_audio_main_fi::SxmDataServiceStatusError >& error);
      virtual void onSxmDataServiceStatusStatus(
         const ::boost::shared_ptr<sxm_audio_main_fi::Sxm_audio_main_fiProxy >& proxy,
         const ::boost::shared_ptr< sxm_audio_main_fi::SxmDataServiceStatusStatus >& status);

   protected:
      virtual tVoid vGet(amt_tclServiceData* pInMsg);
      virtual tVoid vSet(amt_tclServiceData* pInMsg);
      virtual tVoid vUpreg(amt_tclServiceData* pInMsg);

   private:
      clSDS_IsVehicleMoving* _pIsVehicleMoving;
      tVoid vSendStatus();
      bpstl::string oGetFeatureConfiguration() const;
      uint32 _vehicleSpeed;
      bool _tcuServiceStatus;
      boost::shared_ptr< VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy > _vehicleProxy;
      boost::shared_ptr< tcu_main_fi::Tcu_main_fiProxy > _tcuProxy;
      boost::shared_ptr< sxm_audio_main_fi::Sxm_audio_main_fiProxy > _sxmAudioProxy;
      bool _sxmSportsStatus;
      clSDS_KDSConfiguration::Country _marketRegionType;
};


#endif
