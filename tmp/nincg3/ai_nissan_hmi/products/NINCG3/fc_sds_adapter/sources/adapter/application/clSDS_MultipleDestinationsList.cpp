/*********************************************************************//**
 * \file       clSDS_MultipleDestinationsList.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_MultipleDestinationsList.h"
#include "application/clSDS_NaviAmbiguitylistObserver.h"
#include "application/clSDS_StringVarList.h"
#include "external/sds2hmi_fi.h"

#include "SdsAdapter_Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_MultipleDestinationsList.cpp.trc.h"
#endif

//#define DIST_VALUE_COLUMN   2
//#define DIRECTION_VALUE_COLUMN   4
//#define ADDRESS_COLUMN    1
//#define NUM_OF_DIGITS_IN_DIRECTION_VALUE 5


/**************************************************************************//**
*Destructor
******************************************************************************/
clSDS_MultipleDestinationsList::~clSDS_MultipleDestinationsList()
{
   _oAmbiguityObserverlist.clear();
}


/**************************************************************************//**
*Constructor
******************************************************************************/
clSDS_MultipleDestinationsList::clSDS_MultipleDestinationsList(::boost::shared_ptr<NavigationServiceProxy> pNaviProxy)
   : _navigationProxy(pNaviProxy)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_MultipleDestinationsList::onSdsGetRefinementListError(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< SdsGetRefinementListError >& /*error*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_MultipleDestinationsList::onSdsGetRefinementListResponse(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< SdsGetRefinementListResponse >& oSdsRefinementListresponse)
{
   ETG_TRACE_USR1(("onSdsGetRefinementListResponse"));
   ::std::vector< RefinementListElement > osdsRefinementList;

   osdsRefinementList = oSdsRefinementListresponse->getRefinements();

   vHandleAmbiguityList(osdsRefinementList);
   vNotifyAmbiguitylistObserver();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_MultipleDestinationsList::vRequestAmbigListToNavi()
{
   _addressList.clear();
   _navigationProxy->sendSdsGetRefinementListRequest(*this);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_MultipleDestinationsList::vHandleAmbiguityList(::std::vector< RefinementListElement >& oAmbiguityList)
{
   ETG_TRACE_USR1(("vHandleAmbiguityList"));
   ::std::vector< RefinementListElement, ::std::allocator<RefinementListElement> >:: iterator it;

   for (it = oAmbiguityList.begin(); it != oAmbiguityList.end(); ++it)
   {
      if (!(it->getData().empty()))
      {
         std::string oAddress = it->getData();
         _addressList.push_back(oAddress);
      }
   }
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_MultipleDestinationsList::u32GetSize()
{
   return _addressList.size();
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::vector<clSDS_ListItems> clSDS_MultipleDestinationsList::oGetItems(tU32 u32StartIndex, tU32 u32EndIndex)
{
   bpstl::vector<clSDS_ListItems> oListItems;

   for (tU32 u32Index = u32StartIndex; u32Index < bpstl::min(u32EndIndex, u32GetSize()); u32Index++)
   {
      oListItems.push_back(oGetListItem(u32Index));
   }
   return oListItems;
}


/**************************************************************************//**
*
******************************************************************************/
clSDS_ListItems clSDS_MultipleDestinationsList::oGetListItem(tU32 u32Index)
{
   clSDS_ListItems oListItem;
   oListItem.oDescription.szString = oGetItem(u32Index);
   return oListItem;
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_MultipleDestinationsList::oGetItem(tU32 u32Index)
{
   if (u32Index < u32GetSize())
   {
      return _addressList[u32Index];
   }
   return "";
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_MultipleDestinationsList::bSelectElement(tU32 u32SelectedIndex)
{
   if (u32SelectedIndex > 0)
   {
      clSDS_StringVarList::vSetVariable("$(Address)", oGetItem(u32SelectedIndex - 1));
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_MultipleDestinationsList::vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType /*listType*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_MultipleDestinationsList::vRegisterAmbiguityListObserver(clSDS_NaviAmbiguitylistObserver* oAmbiguitylistObserver)
{
   _oAmbiguityObserverlist.push_back(oAmbiguitylistObserver);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_MultipleDestinationsList::vNotifyAmbiguitylistObserver()
{
   ETG_TRACE_USR1(("vNotifyAmbiguitylistObserver"));
   bpstl::vector<clSDS_NaviAmbiguitylistObserver*>::iterator iter = _oAmbiguityObserverlist.begin();
   while (iter != _oAmbiguityObserverlist.end())
   {
      if (*iter != NULL)
      {
         (*iter)->vAmbiguityListResolved();
      }
      ++iter;
   }
}
