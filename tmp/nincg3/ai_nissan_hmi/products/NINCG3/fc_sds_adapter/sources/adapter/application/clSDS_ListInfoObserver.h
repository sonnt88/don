/*********************************************************************//**
 * \file       clSDS_ListInfoObserver.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_ListInfoObserver_h
#define clSDS_ListInfoObserver_h


class clSDS_List;


class clSDS_ListInfoObserver
{
   public:
      clSDS_ListInfoObserver();
      virtual ~clSDS_ListInfoObserver();

      /**
       * This callback of a list observer belongs to asynchronous list requests.
       * A concrete list implementation may have to request information about
       * its list content asynchronously from a middleware component. Upon
       * receiving the answer, the list observer can be notified with this callback.
       */
      virtual void vListUpdated(clSDS_List* pList) = 0;
};


#endif
