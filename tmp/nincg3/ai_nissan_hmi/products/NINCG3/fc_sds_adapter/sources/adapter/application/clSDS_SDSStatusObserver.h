/*********************************************************************//**
 * \file       clSDS_SDSStatusObserver.h
 *
 * SDSStatusObserver (AIF)
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_SDSStatusObserver_h
#define clSDS_SDSStatusObserver_h


#include "external/sds2hmi_fi.h"

class clSDS_SDSStatusObserver
{
   public:
      virtual ~clSDS_SDSStatusObserver();
      clSDS_SDSStatusObserver();
      virtual tVoid vSDSStatusChanged() = 0;
};


#endif
