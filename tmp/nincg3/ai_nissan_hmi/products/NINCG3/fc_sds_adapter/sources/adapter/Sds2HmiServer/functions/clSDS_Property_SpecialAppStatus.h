/*********************************************************************//**
 * \file       clSDS_Property_SpecialAppStatus.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Property_SpecialAppStatus_h
#define clSDS_Property_SpecialAppStatus_h


#include "Sds2HmiServer/framework/clServerProperty.h"
#include "MOST_Tel_FIProxy.h"


enum enSiriStatus
{
   SIRI_NOTAVAILABLE,
   SIRI_AVAILABLE_ENABLED,
   SIRI_AVAILABLE_DISABLED
};


class clSDS_Property_SpecialAppStatus
   : public clServerProperty
   , public asf::core::ServiceAvailableIF
   , public MOST_Tel_FI::BTDeviceVoiceRecognitionExtendedCallbackIF
{
   public:
      virtual ~clSDS_Property_SpecialAppStatus();
      clSDS_Property_SpecialAppStatus(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > pSds2TelProxy);

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onBTDeviceVoiceRecognitionExtendedStatus(
         const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Tel_FI::BTDeviceVoiceRecognitionExtendedStatus >& status);
      virtual void onBTDeviceVoiceRecognitionExtendedError(
         const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Tel_FI::BTDeviceVoiceRecognitionExtendedError >& error);

   protected:
      virtual tVoid vGet(amt_tclServiceData* pInMsg);
      virtual tVoid vSet(amt_tclServiceData* pInMsg);
      virtual tVoid vUpreg(amt_tclServiceData* pInMsg);

   private:
      tVoid vSendStatus();
      boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy > _telephoneProxy;
      uint8 _siriStatus;
      bool  _voiceRecActive;
      bool _voiceRecSupported;
};


#endif
