/*********************************************************************//**
 * \file       clSDS_Userwords.h
 *
 * Userword handling related functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Userwords_h
#define clSDS_Userwords_h


#include "application/clSDS_SDSStatusObserver.h"
#include "legacy/HSI_PHONE_SDS_Interface.h"
#include "external/sds2hmi_fi.h"
#include "sds_gui_fi/SdsPhoneServiceProxy.h"

#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"


class clSDS_SDSStatus;
class clSDS_Userword_Profile;
class clSDS_SdsControl;
class clSDS_QuickDialList;


class clSDS_Userwords  : public clSDS_SDSStatusObserver
{
   public:
      ~clSDS_Userwords();
      clSDS_Userwords(clSDS_SDSStatus* pSDSStatus, clSDS_SdsControl* pSdsControl, clSDS_QuickDialList* pQuickDialList);

      tBool bGetNameAndPhoneNumberByUWID(tU32 u32UserwordID, bpstl::string& strName, bpstl::string& strNumber);
      tU32 u32GetUWIDByNameAndNumber(const bpstl::string strName, const bpstl::string strNumber);
      tU32 u32CreateNewUWIDForNameAndNumber(bpstl::string strName, bpstl::string strNumber);
      uint32 getContactID() const;
      typedef bpstl::vector<sds2hmi_fi_tcl_Userword, bpstl::allocator<sds2hmi_fi_tcl_Userword> > tAvailableUserWords;
      tVoid vNewAvailableUserwords(const tAvailableUserWords& oUserwords);
      virtual tVoid vSDSStatusChanged();
      tVoid vSetActiveProfile(tU32 u32ActiveProfileNum);
      tVoid vDeleteAllUserwords(tU32 u32UWProfile);
      void handleQuickDialList(const ::boost::shared_ptr< sds_gui_fi::SdsPhoneService::QuickDialListUpdateRequest >& request) const;
      void handleUserWordActions(const ::boost::shared_ptr< sds_gui_fi::SdsPhoneService::VoiceTagActionRequest >& request);

   private:
      tBool bIsVoiceControlIdle() const;
      tBool bIsSpeechDialogRunning() const;
      tBool bIsPhonebookAvailable() const;

      tVoid vSynchroniseWithPhoneBook() const;
      tVoid vSynchroniseWithSDS(const tAvailableUserWords& oUserwords);
      tVoid vSynchroniseProfiles();

      // Phone CMM Message handlers
      tVoid vNewPhoneBookState(const tsCMPhone_ReqPBProfileDetails* ptrReqPBProfileDetails);
      tVoid vProcessUserwordListFromPhone(toCMPhone_UserwordList* pUserwordList);
      tVoid vRespondUserwordAvailabilityReqFromPhone(tsCMPhone_UserwordEntry* psUserwordEntry);
      tVoid vNewAvailableProfiles(tU32 u32Profiles);

      tVoid vDeleteNotUsedProfiles();

      toCMPhone_UserwordList* pCreateUserwordListForPhone();

      tVoid vDeleteUserword(tU32 u32UWID);
      tBool bSetSDSUser();
      void recordUserWord(unsigned long u32UWID);
      void replaceUserWord(unsigned long u32UWID);
      void playUserWord(unsigned long u32UWID);

      // Profile management
      clSDS_Userword_Profile* pCreateProfile(tU32 u32ProfileNum) const;
      clSDS_Userword_Profile* pGetProfile(tU32 u32ProfileNum);

      clSDS_SDSStatus* _pSDSStatus;
      clSDS_SdsControl* _pSdsControl;
      clSDS_QuickDialList* _pQuickDialList;
      bpstl::vector<clSDS_Userword_Profile*> _profileList;
      tU32 _u32ActiveProfileNum;
      tU32 _u32PBStatus;
      tBool _bUpdatedBySDS;
      tBool _bUpdatedByPhone;
      tU32 _u32MaxProfileCount;
      tU32 _u32CurrentSDSUser;
      tU32 _u32AvailableProfiles;
      uint32 _userWordListID;
};


#endif
