/*********************************************************************//**
 * \file       StreamMessage.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef StreamMessage_h
#define StreamMessage_h


#define AMT_S_IMPORT_INTERFACE_GENERIC
#include "amt_if.h"


class StreamMessage : public amt_tclBaseMessage
{
   public:
      StreamMessage(const tU8* pStreamData);
      virtual ~StreamMessage();

   private:
      StreamMessage();
};


#endif
