/*********************************************************************//**
 * \file       clSDS_ScreenData.h
 *
 * This class works on the clSDS_XMLDoc output and retrieves useful information
 * from the created tree. It knows information about the various xml definitions
 * and also about the tags which are used.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_ScreenData_h
#define clSDS_ScreenData_h


#include "view_db/Sds_TextDB.h"
#include "view_db/Sds_ViewDB.h"


#define SYSTEM_S_IMPORT_INTERFACE_STRING
#define SYSTEM_S_IMPORT_INTERFACE_MAP
#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#include "stl_pif.h"


class clSDS_XMLDoc;
class clSDS_ListItems;
class clSDS_TagContents;


class clSDS_ScreenData
{
   public:
      virtual ~clSDS_ScreenData();
      clSDS_ScreenData(bpstl::string const& oXMLString);
      clSDS_ScreenData(const clSDS_ScreenData& rhs); 				// copy constructor without implementation
      clSDS_ScreenData& operator =(const clSDS_ScreenData& rhs); 	// assignment constructor without implementation

      bpstl::vector<bpstl::string> oGetList(bpstl::vector<bpstl::string> &oTagList);
      Sds_ViewId enReadScreenId() const;
      Sds_HeaderTemplateId enReadTemplateId(tVoid);
      bpstl::map<bpstl::string, bpstl::string> oGetHeaderVariableData(tVoid);
      bpstl::map<bpstl::string, bpstl::string> oGetScreenVariableData(tVoid);
      tU32 u32GetNumberOfElements() const;
      tVoid vStoreListContents(const bpstl::vector<clSDS_ListItems> &oListContents);
      bpstl::vector<clSDS_ListItems> oGetListItemsofView(tVoid);
      bpstl::string getPhNumDigit() const;

   private:
      struct VariableData
      {
         bpstl::map<bpstl::string, bpstl::string> oMap;
         bpstl::string oKeyId;
         bpstl::string oValueId;
      };

      Sds_HeaderTemplateId enReadTemplateId(bpstl::vector<clSDS_TagContents> &oTagContents);
      tU32 u32GetCount(bpstl::string const& oTag) const;

      /* The below functions help in reading the contents from XML in a value key combination for a defined pattern
      Assume the below XML structure
      <Parent1><Parent2><Parent3><key>KEY1</key><value>VALUE1</value></Parent3><Parent3><key>KEY2</key><value>VALUE2</value></Parent3></Parent2></Parent1>
      Now the oGetMap function can be used to retrieve the value key pairs based on the XML format.
      ie: Once the XML definition <Parent1><Parent2><Parent3>, the key  and value definition <key>, <value> are provided, then it will return a map
      which reads , map[KEY1] = VALUE1 and map[KEY2] = VALUE2 Other below functions are its helper functions */
      bpstl::map<bpstl::string, bpstl::string> oGetMap(bpstl::vector<bpstl::string> &oTagList, bpstl::string const& oKeyId, bpstl::string const& oValueId);
      bpstl::map<bpstl::string, bpstl::string> oGetMap(bpstl::vector<bpstl::string> &oTagList, bpstl::vector<clSDS_TagContents> &oTagContents, bpstl::string const& oKeyId, bpstl::string const& oValueId);
      tVoid vAddDataToMap(tU32 u32TagIndex, bpstl::vector<bpstl::string> &oTagList, bpstl::vector<clSDS_TagContents> &oTagContents,  VariableData& oData);
      tVoid vAddTagDataToMap(tU32 u32TagIndex, bpstl::vector<bpstl::string> &oTagList, const clSDS_TagContents& oTagContent,  VariableData& oData);
      tVoid vAddToMap(bpstl::string const& oKey, bpstl::string const& oValue,  VariableData& oData) const;
      bpstl::string oGetKey(bpstl::vector<clSDS_TagContents> &oTagContent, const VariableData& oData) const;
      bpstl::string oGetValue(bpstl::vector<clSDS_TagContents> &oTagContent, const VariableData& oData) const;
      tVoid vGotoNextTag(tU32 u32TagIndex, bpstl::vector<bpstl::string> &oTagList, clSDS_TagContents& oTagContent,  VariableData& oData);
      bpstl::vector<uint> getLocalIds(bpstl::vector<clSDS_TagContents>& oChildrenTags) const;
      Sds_TextId getTextIDForLocalId(const bpstl::string& localIdString) const;
      Sds_TextId getTextIDForLocalId(uint localId) const;
      bpstl::string formatTagValue(bpstl::string tagValue, bpstl::vector<uint> localIDs) const;
      tBool hasLocalIDValue(bpstl::vector<clSDS_TagContents> voChildrenTags) const;
      bpstl::string getLocalIDValue(bpstl::string rawLocalID) const;
      tVoid printListItems(const bpstl::vector<clSDS_ListItems>& listContents) const;

      /*The below functions help in reading the values from XML for a defined pattern
      Assume the below XML structure
      <Parent1><Parent2><Parent3><data>DATA1</data></Parent3><Parent3><data>DATA2</data></Parent3></Parent2></Parent1>
      Now the oGetListofElements function can be used to retrieve the data based on the XML format.
      ie: Once the XML definition <Parent1><Parent2><Parent3><data> is provided, then it will return a list
      containing two values for this example as DATA1 and DATA2 Other below functions are its helper functions*/
      bpstl::vector<bpstl::string> oGetListofElements(bpstl::vector<bpstl::string> &oTagList, bpstl::vector<clSDS_TagContents> &oTagContents) ;
      tVoid vAddDataToList(tU32 u32TagIndex, bpstl::vector<bpstl::string> &oTagList, bpstl::vector<clSDS_TagContents> &oTagContents, bpstl::vector<bpstl::string> &oDataList);
      tVoid vAddTagDataToList(tU32 u32TagIndex, bpstl::vector<bpstl::string> &oTagList, const clSDS_TagContents& oTagContent, bpstl::vector<bpstl::string> &oDataList) const;
      tBool bIsLastTag(tU32 u32TagIndex, const bpstl::vector<bpstl::string> &oTagList) const;
      tVoid vGotoNextTag(tU32 u32TagIndex, bpstl::vector<bpstl::string> &oTagList, clSDS_TagContents& oTagContent, bpstl::vector<bpstl::string> &oDataList);
      bpstl::string _phoneNumberDigits;
      bpstl::string _templateId;
      clSDS_XMLDoc* _pXMLDoc;
      bpstl::vector<clSDS_ListItems> _oListContents;
      tVoid vGetPhoneNumberDigits(bpstl::vector<clSDS_TagContents> &oTagContents, const VariableData& oData);
};


#endif
