/*********************************************************************//**
 * \file       clSDS_Method_PhoneSetContact.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_PhoneSetContact_h
#define clSDS_Method_PhoneSetContact_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "application/clSDS_PhonebookListClient.h"

class clSDS_PhonebookList;

class clSDS_Method_PhoneSetContact : public clServerMethod, clSDS_PhonebookListClient
{
   public:
      virtual ~clSDS_Method_PhoneSetContact();
      clSDS_Method_PhoneSetContact(ahl_tclBaseOneThreadService* pService, clSDS_PhonebookList* pclPhonebookList);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      virtual tVoid vPhonebookListAvailable();
      tVoid vRequestContactEntriesFromPhone(sds2hmi_fi_tcl_e8_PHN_SelectionType::tenType enSelectionType, tU32 u32ValueId);

      clSDS_PhonebookList* _pPhonebookList;
};


#endif
