/*********************************************************************//**
 * \file       clSDS_EcnrHandler.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_EcnrHandler_h
#define clSDS_EcnrHandler_h


#include "org/bosch/ecnr/serviceProxy.h"


using namespace org::bosch::ecnr::service;


/**
 * see nincg3\ai_audio\components\ECNR\config\rnaivi\ecnr-configuration-data.h
 * #define ECNR_APPID_SDS                          (unsigned char) 3
 * #define SDS_DATASET_2                           (unsigned short) 402
 */
#define ECNR_APP_ID 		3
#define ECNR_CONFIG_ID		402


class clSDS_EcnrHandler
   : public EcnrInitializeCallbackIF
   , public EcnrDestroyCallbackIF
   , public EcnrStartAudioExtCallbackIF
   , public EcnrStopAudioCallbackIF
{
   public:
      clSDS_EcnrHandler(::boost::shared_ptr< ServiceProxy > ecnrProxy);
      virtual ~clSDS_EcnrHandler();

      void initialize();
      void destroy();
      void startAudio();
      void stopAudio();

   private:

      virtual void onEcnrInitializeError(const ::boost::shared_ptr< ServiceProxy >& proxy, const ::boost::shared_ptr< EcnrInitializeError >& error);
      virtual void onEcnrInitializeResponse(const ::boost::shared_ptr< ServiceProxy >& proxy, const ::boost::shared_ptr< EcnrInitializeResponse >& response);

      virtual void onEcnrDestroyError(const ::boost::shared_ptr< ServiceProxy >& proxy, const ::boost::shared_ptr< EcnrDestroyError >& error);
      virtual void onEcnrDestroyResponse(const ::boost::shared_ptr< ServiceProxy >& proxy, const ::boost::shared_ptr< EcnrDestroyResponse >& response);

      virtual void onEcnrStartAudioExtError(const ::boost::shared_ptr< ServiceProxy >& proxy, const ::boost::shared_ptr< EcnrStartAudioExtError >& error);
      virtual void onEcnrStartAudioExtResponse(const ::boost::shared_ptr< ServiceProxy >& proxy, const ::boost::shared_ptr< EcnrStartAudioExtResponse >& response);

      virtual void onEcnrStopAudioError(const ::boost::shared_ptr< ServiceProxy >& proxy, const ::boost::shared_ptr< EcnrStopAudioError >& error);
      virtual void onEcnrStopAudioResponse(const ::boost::shared_ptr< ServiceProxy >& proxy, const ::boost::shared_ptr< EcnrStopAudioResponse >& response);

      boost::shared_ptr< ServiceProxy > _ecnrProxy;
};


#endif
