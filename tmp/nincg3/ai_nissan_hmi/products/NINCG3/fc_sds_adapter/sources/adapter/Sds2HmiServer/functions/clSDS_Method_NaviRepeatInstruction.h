/*********************************************************************//**
 * \file       clSDS_Method_NaviRepeatInstruction.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_NaviRepeatInstruction_h
#define clSDS_Method_NaviRepeatInstruction_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"
#include "external/sds2hmi_fi.h"
#include "application/GuiService.h"


using namespace org::bosch::cm::navigation::NavigationService;


class clSDS_Method_NaviRepeatInstruction
   : public clServerMethod
   , public RetriggerAcousticOutputCallbackIF
{
   public:

      virtual ~clSDS_Method_NaviRepeatInstruction();
      clSDS_Method_NaviRepeatInstruction(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< NavigationServiceProxy> pNaviProxy, GuiService& guiService);

      virtual void onRetriggerAcousticOutputError(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< RetriggerAcousticOutputError >& error);

      virtual void onRetriggerAcousticOutputResponse(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< RetriggerAcousticOutputResponse >& response);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      boost::shared_ptr<NavigationServiceProxy> _navigationProxy;
      GuiService& _guiService;
};


#endif
