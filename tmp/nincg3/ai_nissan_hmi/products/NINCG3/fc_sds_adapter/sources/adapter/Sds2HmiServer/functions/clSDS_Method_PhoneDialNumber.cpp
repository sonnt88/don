/*********************************************************************//**
 * \file       clSDS_Method_PhoneDialNumber.cpp
 *
 * clSDS_Method_PhoneDialNumber method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_PhoneDialNumber.h"
#include "application/clSDS_Iconizer.h"
#include "application/clSDS_Userwords.h"
#include "application/clSDS_ListScreen.h"
#include "application/clSDS_PhonebookList.h"
#include "application/clSDS_QuickDialList.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_PhoneDialNumber.cpp.trc.h"
#endif


using namespace MOST_Tel_FI;
using namespace most_Tel_fi_types;
using namespace MOST_PhonBk_FI;
using namespace most_PhonBk_fi_types;


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_PhoneDialNumber::~clSDS_Method_PhoneDialNumber()
{
   _pListScreen = NULL;
   _pPhonebookList = NULL;
   _pUserwords = NULL;
   _pQuickDialList = NULL;
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_PhoneDialNumber::clSDS_Method_PhoneDialNumber(
   ahl_tclBaseOneThreadService* pServer,
   clSDS_ListScreen* pListScreen,
   clSDS_PhonebookList* pPhonebookList,
   clSDS_Userwords* pUserwords,
   ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > telephoneProxy,
   ::boost::shared_ptr< MOST_PhonBk_FIProxy > phonebookProxy,
   clSDS_QuickDialList* pQuickDialList)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_PHONEDIALNUMBER, pServer)
   , _telephoneProxy(telephoneProxy)
   , _phonebookProxy(phonebookProxy)
   , _pPhonebookList(pPhonebookList)
   , _pListScreen(pListScreen)
   , _pUserwords(pUserwords)
   , _pQuickDialList(pQuickDialList)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_PhoneDialNumber::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgPhoneDialNumberMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   ETG_TRACE_COMP((
                     "PhoneDialNumber.vMethodStart(type=%d index=%d number='%s')",
                     oMessage.nSelectionType.enType,
                     oMessage.nIDValue,
                     oMessage.sPhoneNumber.szValue));
   vInvokeDialNumber(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneDialNumber::vInvokeDialNumber(const sds2hmi_sdsfi_tclMsgPhoneDialNumberMethodStart& oMessage)
{
   switch (oMessage.nSelectionType.enType)
   {
      case sds2hmi_fi_tcl_e8_GEN_SelectionType::FI_EN_BYVAL:
         if (oMessage.sPhoneNumber.szValue != OSAL_NULL)
         {
            vDialNumberByValue(oMessage.sPhoneNumber.szValue);
         }
         break;

      case sds2hmi_fi_tcl_e8_GEN_SelectionType::FI_EN_BYLISTNUMBER:
         vDialByIndex(oMessage.nIDValue);
         break;

      case sds2hmi_fi_tcl_e8_GEN_SelectionType::FI_EN_BYUSW:
         vDialNumberByUWID(oMessage.nIDValue);
         break;

      default:
         break;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneDialNumber::vDialNumberByValue(std::string strPhoneNumber)
{
   if (strPhoneNumber != "")
   {
      strPhoneNumber = clSDS_Iconizer::oRemoveIconPrefix(strPhoneNumber);
      _telephoneProxy->sendDialStart(*this, strPhoneNumber, T_e8_TelEchoCancellationNoiseReductionSetting__e8ECNR_NOCHANGE);
      vSendMethodResult();
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneDialNumber::vDialByIndex(tU32 u32Index)
{
   if (u32Index == 0)
   {
      vDialNumberByValue(_pPhonebookList->oGetPhoneNumbers().oPBDetails_TelNumber[0]);
   }
   else
   {
      vDialNumberByValue(_pListScreen->oGetSelectedItem(u32Index));
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneDialNumber::vDialNumberByUWID(tU32 u32UwId)
{
   if (_phonebookProxy->isAvailable())
   {
      _phonebookProxy->sendGetContactDetailsExtendedStart(*this, u32UwId);
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_PhoneDialNumber::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& /*proxy*/,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_PhoneDialNumber::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& /*proxy*/,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneDialNumber::onDialError(
   const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< DialError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_PhoneDialNumber::onDialResult(
   const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< DialResult >& /*result*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_PhoneDialNumber::onGetContactDetailsExtendedError(
   const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedError >& /*error*/)
{
   vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneDialNumber::onGetContactDetailsExtendedResult(
   const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedResult >& result)
{
   std::string phoneNumber = _pQuickDialList->getPhoneNumber(result->getOContactDetailsExtended());
   if (_telephoneProxy->isAvailable())
   {
      if (phoneNumber != "")
      {
         _telephoneProxy->sendDialStart(*this, phoneNumber, T_e8_TelEchoCancellationNoiseReductionSetting__e8ECNR_NOCHANGE);
         vSendMethodResult();
      }
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}
