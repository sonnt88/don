/*********************************************************************//**
 * \file       clSDS_Method_NaviGetWaypointListInfo.cpp
 *
 * clSDS_Method_NaviGetWaypointListInfo method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_NaviGetWaypointListInfo.h"


using namespace org::bosch::cm::navigation::NavigationService;


#define WAYPOINT_LIST_SIZE_LIMIT    15


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_NaviGetWaypointListInfo::~clSDS_Method_NaviGetWaypointListInfo()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_NaviGetWaypointListInfo::clSDS_Method_NaviGetWaypointListInfo(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr<NavigationServiceProxy> naviProxy)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_NAVIGETWAYPOINTLISTINFO, pService)
   , _naviProxy(naviProxy)
   , _waypointListSize(0)
   , _bWayListRequested(FALSE)
   , _bAddAsWayPoint(FALSE)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviGetWaypointListInfo::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _naviProxy)
   {
      _naviProxy->sendWaypointListDeregisterAll();
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviGetWaypointListInfo::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _naviProxy)
   {
      _naviProxy->sendWaypointListRegister(*this);
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviGetWaypointListInfo::onWaypointListError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< WaypointListError >& /*error*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviGetWaypointListInfo::onWaypointListUpdate(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< WaypointListUpdate >& update)
{
   _waypointListSize = update->getWaypointList().size();
   if (_bWayListRequested == TRUE)
   {
      vSendResult();
      _bWayListRequested = FALSE;
   }
   if (_bAddAsWayPoint == TRUE)
   {
      _naviProxy->sendApplyWaypointListChangeRequest(*this);
      _bAddAsWayPoint = FALSE;
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviGetWaypointListInfo::onGetWaypointListError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< GetWaypointListError >& /*error*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviGetWaypointListInfo::onGetWaypointListResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< GetWaypointListResponse >& /*response*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_Method_NaviGetWaypointListInfo::waypointListIsFull() const
{
   return (_waypointListSize == WAYPOINT_LIST_SIZE_LIMIT);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviGetWaypointListInfo::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   _bWayListRequested = TRUE;
   vSendGetWaypointListRequest();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviGetWaypointListInfo::vSendGetWaypointListRequest()
{
   _naviProxy->sendGetWaypointListRequest(*this);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviGetWaypointListInfo::vSendResult()
{
   sds2hmi_sdsfi_tclMsgNaviGetWaypointListInfoMethodResult oMessage;
   oMessage.bWaypointMustBeDeleted = waypointListIsFull();
   vSendMethodResult(oMessage);
}


void clSDS_Method_NaviGetWaypointListInfo::onSetAddAsWayPoint(tBool bAddAsWayPoint)
{
   _bAddAsWayPoint = bAddAsWayPoint;
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviGetWaypointListInfo::onApplyWaypointListChangeResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< ApplyWaypointListChangeResponse >& /*response*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Method_NaviGetWaypointListInfo::onApplyWaypointListChangeError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< ApplyWaypointListChangeError >& /*error*/)
{
}
