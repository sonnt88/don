/*********************************************************************//**
 * \file       clSDS_AddressBookList.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_AddressBookList.h"
#include "application/clSDS_StringVarList.h"


using namespace org::bosch::cm::navigation::NavigationService;


/**************************************************************************//**
*Destructor
******************************************************************************/
clSDS_AddressBookList::~clSDS_AddressBookList()
{
}


/**************************************************************************//**
*Constructor
******************************************************************************/
clSDS_AddressBookList::clSDS_AddressBookList(::boost::shared_ptr< NavigationServiceProxy > naviProxy)
   : _naviProxy(naviProxy)
   , _selectedIndex(0)
{
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_AddressBookList::bSelectElement(tU32 u32SelectedIndex)
{
   if (u32SelectedIndex > 0)
   {
      _selectedIndex = u32SelectedIndex - 1;
      clSDS_StringVarList::vSetVariable("$(Address)", oGetItem(_selectedIndex));
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_AddressBookList::vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType)
{
   if (listType == sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_ADDRESSBOOK)
   {
      _naviProxy->sendGetAddressbookDestinationsRequest(*this);
   }
   else
   {
      vClear();
      notifyListObserver();
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_AddressBookList::onGetAddressbookDestinationsError(
   const ::boost::shared_ptr< NavigationServiceProxy >&/* proxy*/,
   const ::boost::shared_ptr< GetAddressbookDestinationsError >& /*error*/)
{
   vClear();
   notifyListObserver();
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_AddressBookList::onGetAddressbookDestinationsResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< GetAddressbookDestinationsResponse >& response)
{
   vClear();
   _addressBook = response->getAddressList();
   notifyListObserver();
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_AddressBookList::vClear()
{
   _addressBook.clear();
   _selectedIndex = 0;
}


/**************************************************************************//**
*
******************************************************************************/
std::string clSDS_AddressBookList::oGetItem(tU32 u32Index)
{
   if (u32Index < _addressBook.size())
   {
      return _addressBook[u32Index].getData();
   }
   else
   {
      return "";
   }
}


/**************************************************************************//**
*
******************************************************************************/
std::vector<clSDS_ListItems> clSDS_AddressBookList::oGetItems(tU32 u32StartIndex, tU32 u32EndIndex)
{
   std::vector<clSDS_ListItems> oListItems;
   for (tU32 u32Index = u32StartIndex; u32Index < std::min(u32EndIndex, u32GetSize()); u32Index++)
   {
      clSDS_ListItems oListItem;
      oListItem.oDescription.szString = oGetItem(u32Index);
      oListItems.push_back(oListItem);
   }
   return oListItems;
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_AddressBookList::u32GetSize()
{
   return _addressBook.size();
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_AddressBookList::vStartGuidance()
{
   _naviProxy->sendStartGuidanceToAddressbookDestinationRequest(*this, _selectedIndex);
}


/**************************************************************************//**
 *Add as way point to current route guidance to  Navi.
******************************************************************************/
void clSDS_AddressBookList::vAddasWayPoint()
{
   _naviProxy->sendStartGuidanceToAddressbookDestinationRequest(*this, _selectedIndex);
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_AddressBookList::onStartGuidanceToAddressbookDestinationError(
   const ::boost::shared_ptr<  NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr<  StartGuidanceToAddressbookDestinationError >& /*error*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_AddressBookList:: onStartGuidanceToAddressbookDestinationResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< StartGuidanceToAddressbookDestinationResponse >& /*response*/)
{
}


/**************************************************************************//**
 *
******************************************************************************/

std::string clSDS_AddressBookList::oGetHMIListDescriptionItems(tU32 u32Index)
{
   return oGetItem(u32Index);
}
