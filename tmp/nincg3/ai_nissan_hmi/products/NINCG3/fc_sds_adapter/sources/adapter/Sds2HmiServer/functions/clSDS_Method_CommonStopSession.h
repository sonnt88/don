/*********************************************************************//**
 * \file       clSDS_Method_CommonStopSession.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_CommonStopSession_h
#define clSDS_Method_CommonStopSession_h


#include "Sds2HmiServer/framework/clServerMethod.h"


class GuiService;
class SdsAudioSource;
class clSDS_MenuManager;


class clSDS_Method_CommonStopSession : public clServerMethod
{
   public:
      virtual ~clSDS_Method_CommonStopSession();
      clSDS_Method_CommonStopSession(
         ahl_tclBaseOneThreadService* pService,
         SdsAudioSource& audioSource,
         clSDS_MenuManager* pMenuManager,
         GuiService& guiService);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      SdsAudioSource& _audioSource;
      clSDS_MenuManager* _pMenuManager;
      GuiService& _guiService;
};


#endif
