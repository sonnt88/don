/*********************************************************************//**
 * \file       AudioSourceHandler.h
 *
 * AcrHandler class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef AudioSourceHandler_h_
#define AudioSourceHandler_h_

#include "application/clSDS_SDSStatusObserver.h"

class clSDS_SDSStatus;
class SdsAudioSource;

class AudioSourceHandler : public clSDS_SDSStatusObserver
{
   public:

      AudioSourceHandler(clSDS_SDSStatus* pSDSStatus, SdsAudioSource& audioSource);
      ~AudioSourceHandler();

      void vSDSStatusChanged();

   private:

      clSDS_SDSStatus* _pSDSStatus;
      SdsAudioSource& _audioSource;
};


#endif
