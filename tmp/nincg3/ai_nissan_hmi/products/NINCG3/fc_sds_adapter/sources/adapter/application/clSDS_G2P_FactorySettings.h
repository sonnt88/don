/*********************************************************************//**
 * \file       clSDS_G2P_FactorySettings.h
 *
 * clSDS_G2P_FactorySettings functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_G2P_FactorySettings_h
#define clSDS_G2P_FactorySettings_h


#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"

class clSDS_SdsControl;

class clSDS_G2P_FactorySettings
{
   public:
      virtual ~clSDS_G2P_FactorySettings();
      clSDS_G2P_FactorySettings(clSDS_SdsControl* pSdsControl);
      tVoid vSetG2PFactorySettings() const;
   private:
      clSDS_SdsControl* _pSdsControl;
};


#endif
