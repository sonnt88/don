/*********************************************************************//**
 * \file       clSDS_PhonebookList.cpp
 *
 * clSDS_PhonebookList class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_PhonebookList.h"
#include "application/clSDS_Iconizer.h"
#include "application/clSDS_PhonebookListClient.h"
#include "application/clSDS_PhoneNumberFormatter.h"
#include "application/clSDS_StringVarList.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_PhonebookList.cpp.trc.h"
#endif


using namespace MOST_PhonBk_FI;
using namespace most_PhonBk_fi_types;


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_PhonebookList::~clSDS_PhonebookList()
{
   _pRequester = NULL;
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_PhonebookList::clSDS_PhonebookList(::boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy > phonebookProxy)
   : _phonebookProxy(phonebookProxy)
   , _pRequester(NULL)
   , _enSelectionType(clSDS_PhonebookList::EN_SELECTION_FIRST)
   , _bNoMoreEntries(FALSE)
   , _noMoreEntriesFlag(false)
   , _u16PBEntryCount(0)
   , _u32StartIndex(0)
   , _entryIdRequested(false)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_PhonebookList::onPhonebookDetailsResponse(const ::most_PhonBk_fi_types::T_PhonBkContactDetails& oContactDetails_)
{
   newPhonebookDetails(oContactDetails_);

   if (_requestors.size())
   {
      _requestors.back()->vPhonebookListAvailable();
      _requestors.pop_back();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_PhonebookList::newPhonebookDetails(const ::most_PhonBk_fi_types::T_PhonBkContactDetails& oContactDetails_)
{
   _oPBDetails.oPBDetails_Name = oContactDetails_.getSFirstName();
   std::string cellnum1(oContactDetails_.getSCellNumber1().c_str());
   std::string cellnum2(oContactDetails_.getSCellNumber2().c_str());
   std::string homenum1(oContactDetails_.getSHomeNumber1().c_str());
   std::string homenum2(oContactDetails_.getSHomeNumber2().c_str());
   std::string worknum1(oContactDetails_.getSWorkNumber1().c_str());
   std::string worknum2(oContactDetails_.getSWorkNumber2().c_str());
   std::string othernum(oContactDetails_.getSOtherNumber().c_str());

   _oPBDetails.u8NumOfItems = 0;

   if (!cellnum1.empty() && _oPBDetails.u8NumOfItems < MAX_NUMBER_OF_ENTRIES_IN_PBDETAILS)
   {
      _oPBDetails.oPBDetails_TelNumber[_oPBDetails.u8NumOfItems] = oContactDetails_.getSCellNumber1().c_str();

      _oPBDetails.u8NumOfItems++;
   }
   if (!cellnum2.empty() && _oPBDetails.u8NumOfItems < MAX_NUMBER_OF_ENTRIES_IN_PBDETAILS)
   {
      _oPBDetails.oPBDetails_TelNumber[_oPBDetails.u8NumOfItems] = oContactDetails_.getSCellNumber2().c_str();
      _oPBDetails.u8NumOfItems++;
   }
   if (!homenum1.empty() && _oPBDetails.u8NumOfItems < MAX_NUMBER_OF_ENTRIES_IN_PBDETAILS)
   {
      _oPBDetails.oPBDetails_TelNumber[_oPBDetails.u8NumOfItems] = oContactDetails_.getSHomeNumber1().c_str();
      _oPBDetails.u8NumOfItems++;
   }
   if (!homenum2.empty() && _oPBDetails.u8NumOfItems < MAX_NUMBER_OF_ENTRIES_IN_PBDETAILS)
   {
      _oPBDetails.oPBDetails_TelNumber[_oPBDetails.u8NumOfItems] = oContactDetails_.getSHomeNumber2().c_str();
      _oPBDetails.u8NumOfItems++;
   }
   if (!worknum1.empty() && _oPBDetails.u8NumOfItems < MAX_NUMBER_OF_ENTRIES_IN_PBDETAILS)
   {
      _oPBDetails.oPBDetails_TelNumber[_oPBDetails.u8NumOfItems] = oContactDetails_.getSWorkNumber1().c_str();
      _oPBDetails.u8NumOfItems++;
   }
   if (!worknum2.empty() && _oPBDetails.u8NumOfItems < MAX_NUMBER_OF_ENTRIES_IN_PBDETAILS)
   {
      _oPBDetails.oPBDetails_TelNumber[_oPBDetails.u8NumOfItems] = oContactDetails_.getSWorkNumber2().c_str();
      _oPBDetails.u8NumOfItems++;
   }
   if (!othernum.empty() && _oPBDetails.u8NumOfItems < MAX_NUMBER_OF_ENTRIES_IN_PBDETAILS)
   {
      _oPBDetails.oPBDetails_TelNumber[_oPBDetails.u8NumOfItems] = oContactDetails_.getSOtherNumber().c_str();
      _oPBDetails.u8NumOfItems++;
   }

   _bNoMoreEntries = noMoreEntries(oContactDetails_);
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_PhonebookList::noMoreEntries(const ::most_PhonBk_fi_types::T_PhonBkContactDetails& /*oContactDetails*/) const
{
   tU16 u32PBEntryCount = _u16PBEntryCount;

   switch (_enSelectionType)
   {
      case clSDS_PhonebookList::EN_SELECTION_FIRST:
         return (u32PBEntryCount == 1);

      case clSDS_PhonebookList::EN_SELECTION_NEXT:
         // return (u32PBEntryCount <= pPBDetails->u32PBDetails_Pos);
         return true;

      case clSDS_PhonebookList::EN_SELECTION_PREVIOUS:
         // return (oContactDetails_. == 1);
         return true;

      default:
         return false;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_PhonebookList::sendCreatePhoneBookListStart()
{
   if (_phonebookProxy->isAvailable())
   {
      _phonebookProxy->sendCreatePhoneBookListStart(*this,  _phonebookProxy->getDevicePhoneBookFeatureSupport().getU8DeviceHandle(), most_PhonBk_fi_types::T_e8_PhonBkPhoneBookSortType__e8PB_LIST_SORT_NUMBER_FIRSTNAME);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_PhonebookList::onCreatePhoneBookListError(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/, const ::boost::shared_ptr< MOST_PhonBk_FI::CreatePhoneBookListError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_PhonebookList::onCreatePhoneBookListResult(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/, const ::boost::shared_ptr< MOST_PhonBk_FI::CreatePhoneBookListResult >& result)
{
//   _u16PBEntryCount = 0 -> raa8hi: isn't it necessary????

   if (result->hasU16ListLength())
   {
      _u16PBEntryCount = result->getU16ListLength();
   }
   onPhoneBookListResult();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_PhonebookList::onPhoneBookListResult()
{
   if (_u16PBEntryCount == 0)
   {
      _oPBDetails.oPBDetails_Name = "";
      _oPBDetails.u8NumOfItems = 0;
      _bNoMoreEntries = true;
      if (_pRequester)
      {
         _pRequester->vPhonebookListAvailable();
      }
      return;
   }

   if (_u32StartIndex == 0)
   {
      _u32StartIndex++;
   }
   sendGetContactDetails(_u32StartIndex);
   _requestors.push_back(_pRequester);
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_PhonebookList::sendGetContactDetails(tU32 u32Index)
{
   if (_phonebookProxy->isAvailable())
   {
      _phonebookProxy->sendGetContactDetailsStart(*this, u32Index, most_PhonBk_fi_types::T_e8_PhonBkContactDetailFilter__e8CDF_TELEPHONE);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_PhonebookList::onGetContactDetailsError(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/, const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_PhonebookList::onGetContactDetailsResult(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/, const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsResult >& result)
{
   T_PhonBkContactDetails oContactDetailsResult = result->getOContactDetails();
   onPhonebookDetailsResponse(oContactDetailsResult);
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_PhonebookList::vRequestPhonebookDetailsFromPhone(clSDS_PhonebookListClient* pRequester, tU32 u32StartIndex,
      clSDS_PhonebookList::tenSelectionType enSelection, tBool bisEntryIdRequested)
{
   _pRequester = pRequester ;
   _u32StartIndex = u32StartIndex;
   _enSelectionType = enSelection;
   _entryIdRequested = bisEntryIdRequested;
   sendCreatePhoneBookListStart();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_PhonebookList::bIsEntryAvailable()const
{
   return (_oPBDetails.oPBDetails_Name != "") ||
          (_oPBDetails.u8NumOfItems > 0);
}


/**************************************************************************//**
 *
 ******************************************************************************/
const bpstl::string& clSDS_PhonebookList::oGetPhonebookName() const
{
   return _oPBDetails.oPBDetails_Name;
}


/**************************************************************************//**
 *
 ******************************************************************************/
const tsCMPhone_ReqPBDetails& clSDS_PhonebookList::oGetPhoneNumbers() const
{
   return _oPBDetails;
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_PhonebookList::bGetNoMoreEntriesFlag() const
{
   return _bNoMoreEntries;
}


/**************************************************************************//**
 *
 ******************************************************************************/
tU32 clSDS_PhonebookList::u32GetContactDetailCount() const
{
   return _oPBDetails.u8NumOfItems;
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_PhonebookList::onDevicePhoneBookFeatureSupportError(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< MOST_PhonBk_FI::DevicePhoneBookFeatureSupportError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_PhonebookList::onDevicePhoneBookFeatureSupportStatus(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< MOST_PhonBk_FI::DevicePhoneBookFeatureSupportStatus >& /*status*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_PhonebookList::vUpdateHeadLineTagForSingleContact() const
{
   if (1 == u32GetContactDetailCount())
   {
      bpstl::string oNumber(oGetPhoneNumbers().oPBDetails_TelNumber[0]);
      bpstl::string oName(oGetPhoneNumbers().oPBDetails_Name);
      tU8 u8IconType = oGetPhoneNumbers().u8PBDetails_IconType[0];

      clSDS_StringVarList::vSetVariable("$(Contact)", clSDS_Iconizer::oAddIconInfix(oName, u8IconType, clSDS_PhoneNumberFormatter::oFormatNumber(oNumber)));
      clSDS_StringVarList::vSetVariable("$(ContactName)", oName);
      clSDS_StringVarList::vSetVariable("$(PhBookIconNumber)", clSDS_Iconizer::oAddIconPrefix(u8IconType, clSDS_PhoneNumberFormatter::oFormatNumber(oNumber)));
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_PhonebookList::onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
                                      const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _phonebookProxy)
   {
      _phonebookProxy->sendDevicePhoneBookFeatureSupportUpReg(*this);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_PhonebookList::onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
                                        const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _phonebookProxy)
   {
      _phonebookProxy->sendDevicePhoneBookFeatureSupportRelUpRegAll();
   }
}
