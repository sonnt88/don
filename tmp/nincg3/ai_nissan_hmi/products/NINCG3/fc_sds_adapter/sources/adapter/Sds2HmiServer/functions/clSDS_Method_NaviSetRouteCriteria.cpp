/*********************************************************************//**
 * \file       clSDS_Method_NaviSetRouteCriteria.cpp
 *
 * clSDS_Method_NaviSetRouteCriteria method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_NaviSetRouteCriteria.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_NaviSetRouteCriteria.cpp.trc.h"
#endif

/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_NaviSetRouteCriteria::~clSDS_Method_NaviSetRouteCriteria()
{
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_NaviSetRouteCriteria::clSDS_Method_NaviSetRouteCriteria(ahl_tclBaseOneThreadService* pService,
      ::boost::shared_ptr< NavigationServiceProxy > pSds2NaviProxy
      , GuiService& guiService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_NAVISETROUTECRITERIA, pService)
   , _sds2NaviProxy(pSds2NaviProxy)
   , _guiService(guiService)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_NaviSetRouteCriteria::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgNaviSetRouteCriteriaMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   vSendReCalculateRouteRequest(oMessage);
   vSendMethodResult();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_NaviSetRouteCriteria::vSendReCalculateRouteRequest(const sds2hmi_sdsfi_tclMsgNaviSetRouteCriteriaMethodStart& oMessage)
{
   RouteCriterion	routeCriterionType;

   ETG_TRACE_USR4((" Requested route criterion type %d", oMessage.tRouteOptions.enType)); // TODO Remove this trace once the functionality is tested

   switch (oMessage.tRouteOptions.enType)
   {
      case sds2hmi_fi_tcl_e8_NAV_RouteCriteria::FI_EN_USE_ECONOMIC_ROUTE:
      {
         routeCriterionType	=	RouteCriterion__ROUTE_CRITERION_ECONOMIC;
         _sds2NaviProxy->sendSetRouteCriterionRequest(*this, routeCriterionType);
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
      }
      break;
      case sds2hmi_fi_tcl_e8_NAV_RouteCriteria::FI_EN_USE_FAST_ROUTE:
      {
         routeCriterionType	=	RouteCriterion__ROUTE_CRITERION_FAST;
         _sds2NaviProxy->sendSetRouteCriterionRequest(*this, routeCriterionType);
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
      }
      break;
      case sds2hmi_fi_tcl_e8_NAV_RouteCriteria::FI_EN_USE_SHORT_ROUTE:
      {
         routeCriterionType	=	RouteCriterion__ROUTE_CRITERION_SHORT;
         _sds2NaviProxy->sendSetRouteCriterionRequest(*this, routeCriterionType);
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
      }
      break;
      default:
      {
      }
      break;
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetRouteCriteria::onSetRouteCriterionResponse(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< SetRouteCriterionResponse >& /*response*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_NaviSetRouteCriteria::onSetRouteCriterionError(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< SetRouteCriterionError >& /*error*/)
{
}
