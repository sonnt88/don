/*********************************************************************//**
 * \file       clSDS_POIList.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_POIList_h
#define clSDS_POIList_h


#include "application/clSDS_List.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"

#define SYSTEM_S_IMPORT_INTERFACE_STRING
#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#include "stl_pif.h"


using namespace org::bosch::cm::navigation::NavigationService;


class clSDS_POIList : public clSDS_List
{
   public:
      virtual ~clSDS_POIList();

      clSDS_POIList();
      tU32 u32GetSize();
      bpstl::vector<clSDS_ListItems> oGetItems(tU32 u32StartIndex, tU32 u32EndIndex);
      tBool bSelectElement(tU32 u32SelectedIndex);
      tVoid vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType);
      tVoid vHandlePOIList(const std::vector< AddressListElement >& oPOIList);

   private:
      bpstl::string oGetItem(tU32 u32Index);
      std::vector<AddressListElement > _poiList;
};


#endif
