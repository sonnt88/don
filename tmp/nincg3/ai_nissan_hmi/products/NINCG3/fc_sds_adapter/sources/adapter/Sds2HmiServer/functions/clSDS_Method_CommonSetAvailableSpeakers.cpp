/*********************************************************************//**
 * \file       clSDS_Method_CommonSetAvailableSpeakers.cpp
 *
 * clSDS_Method_CommonSetAvailableSpeakers method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_CommonSetAvailableSpeakers.h"
#include "external/sds2hmi_fi.h"
#include "application/clSDS_LanguageMediator.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_CommonSetAvailableSpeakers::~clSDS_Method_CommonSetAvailableSpeakers()
{
   _pLanguageMediator = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_CommonSetAvailableSpeakers::clSDS_Method_CommonSetAvailableSpeakers(ahl_tclBaseOneThreadService* pService, clSDS_LanguageMediator* pLanguageMediator)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_COMMONSETAVAILABLESPEAKERS, pService),
     _pLanguageMediator(pLanguageMediator)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_CommonSetAvailableSpeakers::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgCommonSetAvailableSpeakersMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   vSendMethodResult();
   if (_pLanguageMediator != NULL)
   {
      _pLanguageMediator->setAvailableSpeakers(oMessage.LanguageAndSpeaker);
   }
}
