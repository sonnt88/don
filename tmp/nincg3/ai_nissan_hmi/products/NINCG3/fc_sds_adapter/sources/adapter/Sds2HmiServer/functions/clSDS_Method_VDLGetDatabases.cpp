/*********************************************************************//**
 * \file       clSDS_Method_VDLGetDatabases.cpp
 *
 * clSDS_Method_VDLGetDatabases method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_VDLGetDatabases.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_VDLGetDatabases::~clSDS_Method_VDLGetDatabases()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_VDLGetDatabases::clSDS_Method_VDLGetDatabases(ahl_tclBaseOneThreadService* pService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_VDLGETDATABASES, pService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_VDLGetDatabases::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   sds2hmi_sdsfi_tclMsgVDLGetDatabasesMethodResult oResult;
   sds2hmi_fi_tcl_DeviceDatabase oDeviceDataBase;
//   clDataPool dataPool;

   oDeviceDataBase.DeviceID = 1;
   bpstl::string oString;
//   dataPool.vGetString(DPSMARTPHONE__DATA_BASE_PATH, oString);
   sds2hmi_fi_tclString oStrDataBase;
   oStrDataBase.bSet(oString.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   oDeviceDataBase.Database.push_back(oStrDataBase);
   oResult.Slots.push_back(oDeviceDataBase);

   vSendMethodResult(oResult);
}
