/*********************************************************************//**
 * \file       clSDS_ConfigurationFlags.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef clSDS_ConfigurationFlags_h
#define clSDS_ConfigurationFlags_h


#include <map>
#include <string>


class clSDS_ConfigurationFlags
{
   public:
      clSDS_ConfigurationFlags();
      virtual ~clSDS_ConfigurationFlags();

      static bool getNavKey();
      static bool getNavAdrsKey();
      static bool getNavStateAvailable();
      static bool getNavCountryAvailable();

      static bool getPOIKey();
      static bool getPOICatKey();

      static bool getAudHDKey();
      static bool getAudRDSKey();
      static bool getAudDABKey();
      static bool getAudSXMKey();
      static unsigned int getAudFreqTypeKey();

      static bool getInfKey();
      static bool getInfTrafficKey();
      static bool getInfHEVKey();

      static bool getBluetoothPhoneEnabledKey();

      static void setDynamicVariable(const std::string& varName, const std::string& varValue);

      static std::map<std::string, std::string> getConfigurationFlags();

   private:
      static std::map<std::string, std::string> dynamicVariables;
};


#endif
