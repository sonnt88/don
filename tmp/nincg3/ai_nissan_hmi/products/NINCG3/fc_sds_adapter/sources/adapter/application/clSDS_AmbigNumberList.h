/*********************************************************************//**
 * \file       clSDS_AmbigNumberList.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef CLSDS_AMBIGNUMBERLIST_H_
#define CLSDS_AMBIGNUMBERLIST_H_
#include "application/clSDS_List.h"

class clSDS_AmbigNumberList : public clSDS_List
{
   public:
      clSDS_AmbigNumberList();
      virtual ~clSDS_AmbigNumberList();

      void setAmbigNumberList(bpstl::vector<bpstl::string> contactIDs);

   private:
      virtual tU32 u32GetSize();
      virtual bpstl::vector<clSDS_ListItems> oGetItems(tU32 u32StartIndex, tU32 u32EndIndex);
      virtual tBool bSelectElement(tU32 u32SelectedIndex);
      virtual tVoid vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType);
      virtual bpstl::string oGetSelectedItem(tU32 u32Index);

      bpstl::string oGetItem(tU32 u32Index) const;

      bpstl::vector<bpstl::string> _ambigContactNumbers;
};


#endif /* CLSDS_AMBIGNUMBERLIST_H_ */
