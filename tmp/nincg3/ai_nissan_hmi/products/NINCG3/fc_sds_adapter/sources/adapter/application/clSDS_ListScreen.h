/*********************************************************************//**
 * \file       clSDS_ListScreen.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_ListScreen_h
#define clSDS_ListScreen_h


#define SYSTEM_S_IMPORT_INTERFACE_STRING
#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#include "stl_pif.h"


#include "application/clSDS_List.h"
#include "view_db/Sds_ViewDB.h"


class clSDS_ListScreen
{
   public:
      ~clSDS_ListScreen();
      clSDS_ListScreen();

      tBool bIsListScreen(Sds_ViewId enScreenId);
      tVoid vSetCurrentList(Sds_ViewId enScreenId);
      tVoid vResetPageNumber();
      tVoid vIncrementPageNumber();
      tVoid vDecrementPageNumber();
      bpstl::vector<clSDS_ListItems> vGetListScreenContents();
      tVoid vSetListScreenContents(bpstl::vector<clSDS_ListItems> listItems);
      tBool bHasPreviousPage() const;
      tBool bHasNextPage() const;
      tU8 u8GetNumOfElementsOnPage() const;
      tVoid vAddToList(Sds_ViewId enScreenId, clSDS_List* pList);
      tU32 u32GetPageNumber() const;
      tBool bSelectElement(tU32 u32SelectedIndex);
      bpstl::string oGetSelectedItem(tU32 u32Index);
      std::vector<sds2hmi_fi_tcl_HMIElementDescription> getHmiElementDescription(int index);

   private:
      clSDS_List* _pCurrentList;
      clSDS_List* pGetList(Sds_ViewId enScreenId);
      bpstl::map<Sds_ViewId, clSDS_List*> _oListMap;
};


#endif
