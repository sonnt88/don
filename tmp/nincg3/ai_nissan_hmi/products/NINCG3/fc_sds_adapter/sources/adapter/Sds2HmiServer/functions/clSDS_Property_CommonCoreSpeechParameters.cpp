/*
 * clSDS_Property_CommonCoreSpeechParameters.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 */

#include "clSDS_Property_CommonCoreSpeechParameters.h"
#include "external/sds2hmi_fi.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Property_CommonCoreSpeechParameters.cpp.trc.h"
#endif


/**************************************************************************//**
* Destructor
******************************************************************************/

clSDS_Property_CommonCoreSpeechParameters::~clSDS_Property_CommonCoreSpeechParameters()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Property_CommonCoreSpeechParameters::clSDS_Property_CommonCoreSpeechParameters(ahl_tclBaseOneThreadService* pService,
      GuiService& guiService): clServerProperty(SDS2HMI_SDSFI_C_U16_COMMONCORESPEECHPARAMETERS, pService),
   _guiService(guiService)
{
}


/**************************************************************************//**
*
******************************************************************************/

tVoid clSDS_Property_CommonCoreSpeechParameters::vGet(amt_tclServiceData* /*pInMsg*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonCoreSpeechParameters::vSet(amt_tclServiceData* pInMsg)
{
   ETG_TRACE_USR1(("clSDS_Property_CommonCoreSpeechParameters Set Entered"));
   sds2hmi_sdsfi_tclMsgCommonCoreSpeechParametersSet oMessage;
   vGetDataFromAmt(pInMsg, oMessage);

   std::vector<sds_gui_fi::SdsGuiService::SpeechVersionParameter> speechVersionParameter;
   sds_gui_fi::SdsGuiService::SpeechVersionParameter speechVersion;

   std::vector < sds2hmi_fi_tcl_CoreSpeechParameter, std::allocator<sds2hmi_fi_tcl_CoreSpeechParameter> > coreSpeechParameter;

   coreSpeechParameter = oMessage.CoreSpeechParameterList;

   for (unsigned int u32Index = 0; u32Index < coreSpeechParameter.size(); u32Index++)
   {
      switch (coreSpeechParameter.at(u32Index).CoreSpeechParameter.enType)
      {
         case sds2hmi_fi_tcl_e8_CoreSpeechParameters::FI_EN_COREPARAM_REC_ACO_MODEL_VERSION:
            speechVersion.setCorespeechparameter(sds_gui_fi::SdsGuiService::CoreSpeechParameter__COREPARAM_REC_ACO_MODEL_VERSION);
            speechVersion.setValue(coreSpeechParameter.at(u32Index).Value);
            speechVersionParameter.push_back(speechVersion);
            break;
         case sds2hmi_fi_tcl_e8_CoreSpeechParameters::FI_EN_COREPARAM_REC_VOCON_ENG_VERSION:
            speechVersion.setCorespeechparameter(sds_gui_fi::SdsGuiService::CoreSpeechParameter__COREPARAM_REC_VOCON_ENG_VERSION);
            speechVersion.setValue(coreSpeechParameter.at(u32Index).Value);
            speechVersionParameter.push_back(speechVersion);
            break;
         case sds2hmi_fi_tcl_e8_CoreSpeechParameters::FI_EN_COREPARAM_TTS_ENGINE:
            speechVersion.setCorespeechparameter(sds_gui_fi::SdsGuiService::CoreSpeechParameter__COREPARAM_TTS_ENGINE);
            speechVersion.setValue(coreSpeechParameter.at(u32Index).Value);
            speechVersionParameter.push_back(speechVersion);
            break;
         case sds2hmi_fi_tcl_e8_CoreSpeechParameters::FI_EN_COREPARAM_TTS_VOICE_GENDER:
            speechVersion.setCorespeechparameter(sds_gui_fi::SdsGuiService::CoreSpeechParameter__COREPARAM_TTS_VOICE_GENDER);
            speechVersion.setValue(coreSpeechParameter.at(u32Index).Value);
            speechVersionParameter.push_back(speechVersion);
            break;
         case sds2hmi_fi_tcl_e8_CoreSpeechParameters::FI_EN_COREPARAM_TTS_VOICE_NAME:
            speechVersion.setCorespeechparameter(sds_gui_fi::SdsGuiService::CoreSpeechParameter__COREPARAM_TTS_VOICE_NAME);
            speechVersion.setValue(coreSpeechParameter.at(u32Index).Value);
            speechVersionParameter.push_back(speechVersion);
            break;
         case sds2hmi_fi_tcl_e8_CoreSpeechParameters::FI_EN_COREPARAM_TTS_VOICE_VERSION:
            speechVersion.setCorespeechparameter(sds_gui_fi::SdsGuiService::CoreSpeechParameter__COREPARAM_TTS_VOICE_VERSION);
            speechVersion.setValue(coreSpeechParameter.at(u32Index).Value);
            speechVersionParameter.push_back(speechVersion);
            break;
         default:
            break;
      }
   }

   if (!speechVersionParameter.empty())
   {
      _guiService.setSpeechVersion(speechVersionParameter);
   }

   sds2hmi_sdsfi_tclMsgCommonCoreSpeechParametersStatus oStatusMessage;
   oStatusMessage.CoreSpeechParameterList = coreSpeechParameter;
   vStatus(oStatusMessage);
}


/**************************************************************************//**
*
******************************************************************************/

tVoid clSDS_Property_CommonCoreSpeechParameters::vUpreg(amt_tclServiceData* /*pInMsg*/)
{
}
