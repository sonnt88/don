/*********************************************************************//**
 * \file       clSDS_Property_CommonSDSConfiguration.cpp
 *
 * Common SDS Configuration property implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "Sds2HmiServer/functions/clSDS_Property_CommonSDSConfiguration.h"
#include "application/clSDS_ConfigurationFlags.h"
#include "application/clSDS_KDSConfiguration.h"
#include "application/SettingsService.h"
#include "application/StringUtils.h"
#include "external/sds2hmi_fi.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Property_CommonSDSConfiguration.cpp.trc.h"
#endif


using namespace tcu_main_fi;


/**************************************************************************//**
*
******************************************************************************/
clSDS_Property_CommonSDSConfiguration::~clSDS_Property_CommonSDSConfiguration()
{
}


/**************************************************************************//**
*
******************************************************************************/
clSDS_Property_CommonSDSConfiguration::clSDS_Property_CommonSDSConfiguration(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr< Tcu_main_fiProxy > pTcuFiProxy,
   SettingsService* settingsService)
   : clServerProperty(SDS2HMI_SDSFI_C_U16_COMMONSDSCONFIGURATION, pService)
   , _tcuFiProxy(pTcuFiProxy)
{
   _sdsConfigData.phoneBookNBestMatch = true;
   _sdsConfigData.audioNBestMatch = true;
   _sdsConfigData.beepOnlyMode = false;
   _sdsConfigData.tcuType = 0;

   if (settingsService)
   {
      settingsService->addObserver(this);
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonSDSConfiguration::vGet(amt_tclServiceData* /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonSDSConfiguration::vSet(amt_tclServiceData* /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonSDSConfiguration::vUpreg(amt_tclServiceData* /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
*
******************************************************************************/
static void traceXmlString(const bpstl::string& str)
{
   const size_t chunkSize = 100;
   for (size_t pos = 0; pos < str.size(); pos += chunkSize)
   {
      bpstl::string chunk = str.substr(pos, chunkSize);
      ETG_TRACE_USR1(("SDS Config '%s'", chunk.c_str()));
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_CommonSDSConfiguration::vSendStatus()
{
   bpstl::string staticString = clSDS_XMLStringCreation::getStaticXmlString();
   bpstl::string dynamicString = clSDS_XMLStringCreation::getDynamicXmlString(_sdsConfigData);

   ETG_TRACE_USR1(("SDS Config === Static ==="));
   traceXmlString(staticString);
   ETG_TRACE_USR1(("SDS Config === Dynamic ==="));
   traceXmlString(dynamicString);

   sds2hmi_sdsfi_tclMsgCommonSDSConfigurationStatus oMessage;
   oMessage.SDSConfiguration.StaticConfig.bSet(staticString.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   oMessage.SDSConfiguration.DynamicConfig.bSet(dynamicString.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   vStatus(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonSDSConfiguration::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _tcuFiProxy)
   {
      _tcuFiProxy->sendProvisionServiceUpReg(*this);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonSDSConfiguration::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _tcuFiProxy)
   {
      _tcuFiProxy->sendProvisionServiceRelUpRegAll();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonSDSConfiguration::onProvisionServiceError(
   const ::boost::shared_ptr< Tcu_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< ProvisionServiceError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonSDSConfiguration::onProvisionServiceStatus(
   const ::boost::shared_ptr< Tcu_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< ProvisionServiceStatus >& status)
{
   if (status->hasProvisionService())
   {
      const bool tcuAvailable = status->getProvisionService().getBOperatorService();

      if (tcuAvailable)
      {
         _sdsConfigData.tcuType = clSDS_KDSConfiguration::getOpeningAnimationType();
      }
      else
      {
         _sdsConfigData.tcuType = 0; //TODO raa8hi Tbd value 0
      }

      clSDS_ConfigurationFlags::setDynamicVariable("E_INF_CNT_TO_TCU_IN_INFO", StringUtils::toString(_sdsConfigData.tcuType));

      vSendStatus();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonSDSConfiguration::updateBeepOnlyMode(bool beepOnlyModeStatus)
{
   _sdsConfigData.beepOnlyMode = beepOnlyModeStatus;
   vSendStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonSDSConfiguration::updateNBestMatchAudio(bool audioNBest)
{
   _sdsConfigData.audioNBestMatch = audioNBest;
   vSendStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_CommonSDSConfiguration::updateNBestMatchPhoneBook(bool phoneNBest)
{
   _sdsConfigData.phoneBookNBestMatch = phoneNBest;
   vSendStatus();
}
