/*********************************************************************//**
 * \file       clSDS_Method_MediaGetDataBase.cpp
 *
 * clSDS_Method_MediaGetDataBase method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_MediaGetDataBase.h"
#include "external/sds2hmi_fi.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_MediaGetDataBase::~clSDS_Method_MediaGetDataBase()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_MediaGetDataBase::clSDS_Method_MediaGetDataBase(ahl_tclBaseOneThreadService* pService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_MEDIAGETDATABASE, pService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_MediaGetDataBase::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   sds2hmi_sdsfi_tclMsgMediaGetDatabaseMethodResult oResult;
   oResult.DeviceID = 0;
   oResult.Filename.bSet("/dev/ffs2/dynamic/media/db/MyMedia.db", sds2hmi_fi_tclString::FI_EN_UTF8);
   vSendMethodResult(oResult);
}
