/*********************************************************************//**
 * \file       AhlService.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/framework/AhlService.h"
#include "Sds2HmiServer/framework/clFunction.h"


DEFINE_CLASS_LOGGER_AND_LEVEL("sdshmi/Sds2HmiServer/framework", AhlService, Info);


AhlService::AhlService(ahl_tclBaseOneThreadApp* app, tU16 serviceId, tU16 majorVersion, tU16 minorVersion)
   : ahl_tclBaseOneThreadService(app, serviceId, majorVersion, minorVersion)
{
   LOG_INFO("ahl service created");
}


AhlService::~AhlService()
{
}


tVoid AhlService::vMyDispatchMessage(amt_tclServiceData* message)
{
   std::list<clFunction*>::iterator iter = _functions.begin();
   while (iter != _functions.end())
   {
      clFunction* function = *iter;
      if (message->u16GetFunctionID() == function->u16GetFunctionID())
      {
         LOG_INFO("service data message dispatched to function 0x%04x", function->u16GetFunctionID());
         function->vHandleMessage(message);
         return;
      }
      ++iter;
   }
   LOG_WARN("service data message not handled");
}


void  AhlService::vAddFunction(clFunction* function)
{
   _functions.push_back(function);
}
