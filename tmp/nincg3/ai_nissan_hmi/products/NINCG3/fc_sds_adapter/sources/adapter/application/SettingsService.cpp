/*********************************************************************//**
 * \file       SettingsService.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/SettingsService.h"
#include "application/SettingsObserver.h"
#include "application/clSDS_PromptModeObserver.h"
#include "application/clSDS_SdsConfigObserver.h"


using namespace sds_gui_fi::SettingsService;


SettingsService::SettingsService()
   : SettingsServiceStub("SettingsServicePort")
   , _pSdsConfigObserver(NULL)
   , _promptModeObserver(NULL)
{
}


SettingsService::~SettingsService()
{
   _promptModeObserver = NULL;
   _pSdsConfigObserver = NULL;
}


void SettingsService::onBeepOnlyModeSet(const ::boost::shared_ptr< BeepOnlyModeSet >& payload)
{
   if (_pSdsConfigObserver)
   {
      _pSdsConfigObserver->updateBeepOnlyMode(payload->getBeepOnlyMode());
   }
   setBeepOnlyMode(payload->getBeepOnlyMode());
}


void SettingsService::onShortPromptSet(const ::boost::shared_ptr< ShortPromptSet >& payload)
{
   if (_promptModeObserver)
   {
      _promptModeObserver->promptModeChanged(payload->getShortPrompt());
   }
   setShortPrompt(payload->getShortPrompt());
}


void SettingsService::onBestMatchAudioSet(const ::boost::shared_ptr< BestMatchAudioSet >& payload)
{
   if (_pSdsConfigObserver)
   {
      _pSdsConfigObserver->updateNBestMatchAudio(payload->getBestMatchAudio());
   }
   setBestMatchAudio(payload->getBestMatchAudio());
}


void SettingsService::onBestMatchPhoneBookSet(const ::boost::shared_ptr< BestMatchPhoneBookSet >& payload)
{
   if (_pSdsConfigObserver)
   {
      _pSdsConfigObserver->updateNBestMatchPhoneBook(payload->getBestMatchPhoneBook());
   }
   setBestMatchPhoneBook(payload->getBestMatchPhoneBook());
}


void SettingsService::onVoicePreferenceSet(const ::boost::shared_ptr< VoicePreferenceSet >& payload)
{
   setVoicePreference(payload->getVoicePreference());
   notifyObservers();
}


void SettingsService::onSpeechRateUpdateRequest(const ::boost::shared_ptr< SpeechRateUpdateRequest >& /*request*/)
{
}


void SettingsService::addPromptModeObserver(clSDS_PromptModeObserver* obs)
{
   NORMAL_M_ASSERT(_promptModeObserver == NULL);
   _promptModeObserver = obs;
}


void SettingsService::addObserver(clSDS_SdsConfigObserver* obs)
{
   NORMAL_M_ASSERT(_pSdsConfigObserver == NULL);
   _pSdsConfigObserver = obs;
}


void SettingsService::addObserver(SettingsObserver* pObserver)
{
   _observers.push_back(pObserver);
}


void SettingsService::notifyObservers()
{
   for (size_t i = 0; i < _observers.size(); ++i)
   {
      if (_observers[i])
      {
         _observers[i]->onSettingsChanged();
      }
   }
}
