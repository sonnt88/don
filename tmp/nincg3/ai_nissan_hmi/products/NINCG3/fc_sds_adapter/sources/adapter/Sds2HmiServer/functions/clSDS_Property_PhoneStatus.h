/*********************************************************************//**
 * \file       clSDS_Property_PhoneStatus.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Property_PhoneStatus_h
#define clSDS_Property_PhoneStatus_h


#include "Sds2HmiServer/framework/clServerProperty.h"
#include "external/sds2hmi_fi.h"
#include "MOST_BTSet_FIProxy.h"
#include "MOST_Tel_FIProxy.h"
#include "MOST_PhonBk_FIProxy.h"


class clSDS_PhoneStatusObserver;
class clSDS_Userwords;


class clSDS_Property_PhoneStatus
   : public clServerProperty
   , public asf::core::ServiceAvailableIF
   , public MOST_BTSet_FI::BluetoothOnOffCallbackIF
   , public MOST_Tel_FI::CallStatusNoticeCallbackIF
   , public MOST_BTSet_FI::DeviceListCallbackIF
   , public MOST_PhonBk_FI::DownloadStateCallbackIF
   , public MOST_PhonBk_FI::PreferredPhoneBookSortOrderCallbackIF
   , public MOST_PhonBk_FI::CreateContactListCallbackIF
   , public MOST_PhonBk_FI::ListChangeCallbackIF
{
   public:
      virtual ~clSDS_Property_PhoneStatus();
      clSDS_Property_PhoneStatus(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy > pSds2BtSetProxy,
         ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > pSds2TelProxy,
         ::boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy> pPhoneBookProxy,
         clSDS_Userwords* userWords);
      uint8 getDeviceHandle() const;
      sds2hmi_fi_tcl_e8_PHN_Status::tenType getPhoneStatus() const;

      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      //callback for BluetoothOnOff from BTSettings  FI
      virtual void onBluetoothOnOffError(
         const ::boost::shared_ptr< MOST_BTSet_FI::MOST_BTSet_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_BTSet_FI::BluetoothOnOffError >& error);
      virtual void onBluetoothOnOffStatus(
         const ::boost::shared_ptr< MOST_BTSet_FI::MOST_BTSet_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_BTSet_FI::BluetoothOnOffStatus >& status);

      //callback for onCallStatusNotice from Telephone FI
      virtual void onCallStatusNoticeError(
         const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Tel_FI::CallStatusNoticeError >& error);
      virtual void onCallStatusNoticeStatus(
         const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Tel_FI::CallStatusNoticeStatus >& status);

      //callback for DeviceList from BTSettings FI
      virtual void onDeviceListError(
         const ::boost::shared_ptr< MOST_BTSet_FI::MOST_BTSet_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_BTSet_FI::DeviceListError >& error);
      virtual void onDeviceListStatus(
         const ::boost::shared_ptr< MOST_BTSet_FI::MOST_BTSet_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_BTSet_FI::DeviceListStatus >& status);

      //callback for DownloadStateStatus from Phonebook FI
      virtual void onDownloadStateStatus(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::DownloadStateStatus >& status);
      virtual void onDownloadStateError(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::DownloadStateError >& error);

      //callback for PreferredPhoneBookSortOrder from Phonebook FI
      virtual void onPreferredPhoneBookSortOrderError(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::PreferredPhoneBookSortOrderError >& error);
      virtual void onPreferredPhoneBookSortOrderStatus(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::PreferredPhoneBookSortOrderStatus >& status);

      //callback for CreateContactListCallbackIF from Phonebook FI
      virtual void onCreateContactListError(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::CreateContactListError >& error);
      virtual void onCreateContactListResult(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::CreateContactListResult >& result);

      //callback for ListChangeCallbackIF from Phonebook FI
      virtual void onListChangeError(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::ListChangeError >& error);
      virtual void onListChangeStatus(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::ListChangeStatus >& status);

      void setPhoneStatusObserver(clSDS_PhoneStatusObserver* obs);

   protected:
      virtual tVoid vGet(amt_tclServiceData* pInMsg);
      virtual tVoid vSet(amt_tclServiceData* pInMsg);
      virtual tVoid vUpreg(amt_tclServiceData* pInMsg);

   private:
      struct DeviceDescriptor
      {
         uint32 id;
         std::string name;
      };

      tVoid vSendPhoneStatus();
      tVoid vSynchronizeStoredDeviceStatus();
      tVoid vTracePhoneStatus(const sds2hmi_sdsfi_tclMsgPhoneStatusStatus& oMessage) const;
      void handlePhoneStatus();
      void eGetSdsTypePhonebookStatus(const ::most_PhonBk_fi_types::T_PhonBkDownloadStateStreamItem oDownloadStatus);
      sds2hmi_fi_tcl_DeviceStatus getDeviceStatus(const DeviceDescriptor& device) const;
      void updateContactLists();

      boost::shared_ptr< MOST_BTSet_FI::MOST_BTSet_FIProxy > _sds2BtSetProxy;
      boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy > _telephoneProxy;
      boost::shared_ptr<MOST_PhonBk_FI::MOST_PhonBk_FIProxy> _phoneBookProxy;

      uint8 _inCallDeviceHandle;
      bool _btStatus;
      bool _callPresent;
      bool _handsetMode;
      bool _userConnectedState;

      sds2hmi_fi_tcl_e8_DeviceStatus _connectedPhonedeviceStatus;
      sds2hmi_fi_tcl_e8_PHN_Status _phoneStatus;
      sds2hmi_fi_tcl_e8_PHN_Transorder _preferredSortingOrder;

      most_Tel_fi_types::T_e8_TelCallStatus _firstCallStatus;
      most_Tel_fi_types::T_e8_TelCallStatus _secondCallStatus;

      DeviceDescriptor _connectedDevice;
      std::vector<DeviceDescriptor> _deviceList;
      std::map<uint16, uint16> _listHandlesMap; //<ListHandle, DeviceID>

      std::vector<clSDS_PhoneStatusObserver*> _phoneStatusObserverList;
      clSDS_Userwords* _pUserwords;
};


#endif
