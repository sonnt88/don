/*********************************************************************//**
 * \file       clSDS_Method_PhoneSetPhoneSetting.cpp
 *
 * clSDS_Method_PhoneSetPhoneSetting method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_PhoneSetPhoneSetting.h"
#include "Sds2HmiServer/functions/clSDS_Property_PhoneStatus.h"
#include "external/sds2hmi_fi.h"


using namespace MOST_BTSet_FI;
using namespace MOST_Tel_FI;
using namespace most_Tel_fi_types;


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_PhoneSetPhoneSetting::~clSDS_Method_PhoneSetPhoneSetting()
{
   _pPhoneStatus = NULL;
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_PhoneSetPhoneSetting::clSDS_Method_PhoneSetPhoneSetting(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy > bluetoothSettingsProxy,
   ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > telephoneProxy,
   clSDS_Property_PhoneStatus* pPhoneStatus, GuiService& guiService)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_PHONESETPHONESETTING, pService)
   , _guiService(guiService)
   , _bluetoothSettingsProxy(bluetoothSettingsProxy)
   , _telephoneProxy(telephoneProxy)
   , _pPhoneStatus(pPhoneStatus)
   , _btOnOffState(false)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_PhoneSetPhoneSetting::setBluetooth()
{
   if (_bluetoothSettingsProxy->isAvailable())
   {
      _bluetoothSettingsProxy->sendSwitchBluetoothOnOffStart(*this, _btOnOffState);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_PhoneSetPhoneSetting::onSwitchBluetoothOnOffError(
   const ::boost::shared_ptr< MOST_BTSet_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< SwitchBluetoothOnOffError >& /*error*/)
{
   vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_PhoneSetPhoneSetting::onSwitchBluetoothOnOffResult(
   const ::boost::shared_ptr< MOST_BTSet_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< SwitchBluetoothOnOffResult >& /*result*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_PhoneSetPhoneSetting::setHandsfreeMode()
{
   if (_pPhoneStatus != NULL)
   {
      if (_pPhoneStatus->getPhoneStatus() == sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_HANDSET_MODE)
      {
         _telephoneProxy->sendTransferCallToVehicleStart(*this , _pPhoneStatus->getDeviceHandle());
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SDS_PHONE_HANDSFREE_STATE);
      }
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_PhoneSetPhoneSetting::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgPhoneSetPhoneSettingMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);

   switch (oMessage.nPhoneSetting.enType)
   {
      case sds2hmi_fi_tcl_e8_PHN_Setting::FI_EN_BLUETOOTH_ON:
         _btOnOffState = true;
         setBluetooth();
         break;

      case sds2hmi_fi_tcl_e8_PHN_Setting::FI_EN_BLUETOOTH_OFF:
         _btOnOffState = false;
         setBluetooth();
         break;

      case sds2hmi_fi_tcl_e8_PHN_Setting::FI_EN_SWITCH_TO_HANDSFREE:
         setHandsfreeMode();
         vSendMethodResult();
         break;

      default:
         break;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneSetPhoneSetting::onTransferCallToVehicleError(
   const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< TransferCallToVehicleError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneSetPhoneSetting::onTransferCallToVehicleResult(
   const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< TransferCallToVehicleResult >& /*result*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneSetPhoneSetting::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _bluetoothSettingsProxy)
   {
      // register for property to receive acknowledgment via onBluetoothOnOffStatus
      // for sendSwitchBluetoothOnOffStart
      _bluetoothSettingsProxy->sendBluetoothOnOffUpReg(*this);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneSetPhoneSetting::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _bluetoothSettingsProxy)
   {
      _bluetoothSettingsProxy->sendBluetoothOnOffRelUpRegAll();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneSetPhoneSetting::onBluetoothOnOffError(
   const ::boost::shared_ptr< MOST_BTSet_FI::MOST_BTSet_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_BTSet_FI::BluetoothOnOffError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneSetPhoneSetting::onBluetoothOnOffStatus(
   const ::boost::shared_ptr< MOST_BTSet_FI::MOST_BTSet_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_BTSet_FI::BluetoothOnOffStatus >& status)
{
   if (status->hasBBTOnOff() && (status->getBBTOnOff() == _btOnOffState))
   {
      vSendMethodResult();
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}
