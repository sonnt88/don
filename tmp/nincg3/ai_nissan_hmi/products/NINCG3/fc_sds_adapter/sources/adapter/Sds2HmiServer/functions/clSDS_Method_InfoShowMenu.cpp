/*********************************************************************//**
 * \file       clSDS_Method_InfoShowMenu.cpp
 *
 * clSDS_Method_InfoShowMenu method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_InfoShowMenu.h"
#include "SdsAdapter_Trace.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_InfoShowMenu.cpp.trc.h"
#endif


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_InfoShowMenu::~clSDS_Method_InfoShowMenu()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_InfoShowMenu::clSDS_Method_InfoShowMenu(
   ahl_tclBaseOneThreadService* pService, GuiService& guiService,
   ::boost::shared_ptr< NavigationServiceProxy > pSds2NaviProxy)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_INFOSHOWMENU, pService)
   , _guiService(guiService)
   , _sds2NaviProxy(pSds2NaviProxy)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_InfoShowMenu::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgInfoShowMenuMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);

   if (bShowInfoScreen(oMessage))
   {
      vSendMethodResult();
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_Method_InfoShowMenu::bShowInfoScreen(const sds2hmi_sdsfi_tclMsgInfoShowMenuMethodStart& oMessage)
{
   ETG_TRACE_USR1(("MenuType %d", oMessage.eMenu.enType));

   switch (oMessage.eMenu.enType)
   {
      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_TRAFFIC:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_INFO_TRAFFIC);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_CURRENT_WEATHER:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_INFO_CURRENT_WEATHER_REPORT);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_WEATHER_MAP:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_INFO_WEATHER_REPORT_MAP);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_5DAY_FORECAST:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_INFO_WEATHER_5_DAY_FORECAST);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_6HOUR_FORECAST:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_INFO_WEATHER_6_HOUR_FORECAST);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_MOVIES_FAV_THEATERS:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_INFO_MOVIE_FAV_THEATERS);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_MOVIES_LIST_THEATERS:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_INFO_MOVIE_LIST_THEATERS);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_MOVIES_LIST_MOVIES:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_INFO_MOVIE_LIST_MOVIE);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_STOCKS_FAVOURITES:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_INFO_STOCK);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_TCU_VOICE_MENU:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_TCU_VOICE_MENU);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_SPORTS_FOOTBALL:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_SPORTS_FOOTBALL);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_SPORTS_BASEBALL:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_SPORTS_BASEBALL);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_SPORTS_BASKETBALL:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_SPORTS_BASKETBALL);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_SPORTS_HOCKEY:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_SPORTS_ICEHOCKEY);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_SPORTS_GOLF:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_SPORTS_GOLF);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_SPORTS_AUTORACING:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_SPORTS_MOTORSPORTS);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_SPORTS_SOCCER:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_SPORTS_SOCCER);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_SPORTS_FAVOURITES:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_SPORTS_FAVOURITES);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_ENERGY_FLOW:
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_ENERGY_FLOW);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_CURRENT_LOCATION:
         _sds2NaviProxy->sendShowWhereAmIScreenRequest(*this);
         return TRUE;

      case sds2hmi_fi_tcl_e8_Info_Menu::FI_EN_INFO_ADJUST_LOCATION:
         _sds2NaviProxy->sendShowAdjustCurrentLocationScreenRequest(*this);
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
         return TRUE;

      default:
         return FALSE;
   }
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_InfoShowMenu::onShowWhereAmIScreenResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< ShowWhereAmIScreenResponse >&  /*response*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_InfoShowMenu::onShowWhereAmIScreenError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< ShowWhereAmIScreenError >& /*error*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_InfoShowMenu::onShowAdjustCurrentLocationScreenError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< ShowAdjustCurrentLocationScreenError >& /*error*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_InfoShowMenu::onShowAdjustCurrentLocationScreenResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< ShowAdjustCurrentLocationScreenResponse >& /*response*/)
{
}
