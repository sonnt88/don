/*********************************************************************//**
 * \file       clSDS_Method_CommonSelectListElement.cpp
 *
 * clSDS_Method_CommonSelectListElement method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_CommonSelectListElement.h"
#include "external/sds2hmi_fi.h"
#include "application/clSDS_ListScreen.h"
#include "application/clSDS_StringVarList.h"
#include "application/StringUtils.h"

#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_CommonSelectListElement.cpp.trc.h"
#endif


/**************************************************************************//**
*Destructor
******************************************************************************/
clSDS_Method_CommonSelectListElement::~clSDS_Method_CommonSelectListElement()
{
   _pListScreen = NULL;
}


/**************************************************************************//**
*Constructor
******************************************************************************/
clSDS_Method_CommonSelectListElement::clSDS_Method_CommonSelectListElement(ahl_tclBaseOneThreadService* pService, clSDS_ListScreen* pListScreen)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_COMMONSELECTLISTELEMENT, pService), _pListScreen(pListScreen), _u32AbsoluteIndex(0)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_CommonSelectListElement::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgCommonSelectListElementMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);

   ETG_TRACE_USR4(("clSDS_Method_CommonSelectListElement::vMethodStart - nLine = %d", oMessage.nLine));

   if ((_pListScreen != NULL) && (oMessage.nLine > 0) && (oMessage.nLine < 6))
   {
      _u32AbsoluteIndex = _pListScreen->u32GetPageNumber() * 5 + oMessage.nLine;

      clSDS_StringVarList::vSetVariable("$(Number)", StringUtils::toString(oMessage.nLine));

      tBool bSendResponse = _pListScreen->bSelectElement(_u32AbsoluteIndex);

      if (bSendResponse)
      {
         vSendResponse();
      }
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_CommonSelectListElement::vSendResponse()
{
   sds2hmi_sdsfi_tclMsgCommonSelectListElementMethodResult oResult;
   oResult.nLine = (tU8)_u32AbsoluteIndex;
   ETG_TRACE_USR4(("clSDS_Method_CommonSelectListElement::vSendResponse - nLine = %d", oResult.nLine));
   vSendMethodResult(oResult);
}
