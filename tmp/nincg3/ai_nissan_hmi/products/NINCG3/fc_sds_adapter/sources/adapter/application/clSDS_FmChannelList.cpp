/*********************************************************************//**
 * \file       clSDS_FmChannelList.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "clSDS_FmChannelList.h"
#include "application/clSDS_TunerStateObserver.h"
#include "application/StringUtils.h"
#include "application/clSDS_KDSConfiguration.h"
#include <ctime>

#ifdef DP_DATAPOOL_ID
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_generic_if.h"
#endif

#include "SdsAdapter_Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_FmChannelList.cpp.trc.h"
#endif


using namespace tuner_main_fi;
using namespace tuner_main_fi_types;


/**************************************************************************//**
 *
 ******************************************************************************/
clSDS_FmChannelList::~clSDS_FmChannelList()
{
   _pTunerStateObserver = NULL;
}


/**************************************************************************//**
 *
 ******************************************************************************/
clSDS_FmChannelList::clSDS_FmChannelList(
   ::boost::shared_ptr< sds_fm_fi::SdsFmService::SdsFmServiceProxy > sdsFmProxy,
   ::boost::shared_ptr<Tuner_main_fiProxy > tunerProxy)
   : _sdsFmProxy(sdsFmProxy)
   , _tunerProxy(tunerProxy)
   , _pTunerStateObserver(NULL)
   , _requestChannelList(false)
   , _releaseChannelList(true)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _tunerProxy)
   {
      ETG_TRACE_USR1(("clSDS_FmChannelList::onAvailable for port %s", proxy->getPortName().c_str()));

      setTimer();
      _tunerProxy->sendFID_TUN_G_GET_CONFIG_LISTUpReg(*this);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _tunerProxy)
   {
      _tunerProxy->sendFID_TUN_G_GET_CONFIG_LISTRelUpRegAll();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::onStoreRDSChannelNamesError(
   const ::boost::shared_ptr< sds_fm_fi::SdsFmService::SdsFmServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< sds_fm_fi::SdsFmService::StoreRDSChannelNamesError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::onStoreRDSChannelNamesResponse(
   const ::boost::shared_ptr< sds_fm_fi::SdsFmService::SdsFmServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< sds_fm_fi::SdsFmService::StoreRDSChannelNamesResponse >& /*response*/)
{
   // Notify SDS MW on tuner status as READY when DB updating is completed
   if (_pTunerStateObserver != NULL)
   {
      _pTunerStateObserver->vTunerStatusChanged(sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_FM, sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_READY);
   }
   ETG_TRACE_USR1(("clSDS_FmChannelList::onStoreRDSChannelNamesResponse : tuner status ready"));
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::onStoreHDChannelNamesError(
   const ::boost::shared_ptr< sds_fm_fi::SdsFmService::SdsFmServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< sds_fm_fi::SdsFmService::StoreHDChannelNamesError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::onStoreHDChannelNamesResponse(
   const ::boost::shared_ptr< sds_fm_fi::SdsFmService::SdsFmServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< sds_fm_fi::SdsFmService::StoreHDChannelNamesResponse >& /*response*/)
{
   // Notify SDS MW on tuner status as READY when DB updating is completed
   if (_pTunerStateObserver != NULL)
   {
      _pTunerStateObserver->vTunerStatusChanged(sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_HD_FM, sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_READY);
   }
   ETG_TRACE_USR1(("clSDS_FmChannelList::onStoreHDChannelNamesResponse : tuner status ready"));
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::setFmChannelListObserver(clSDS_TunerStateObserver* pTunerStateObserver)
{
   _pTunerStateObserver = pTunerStateObserver;
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::onFID_TUN_G_GET_CONFIG_LISTError(
   const ::boost::shared_ptr< Tuner_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< FID_TUN_G_GET_CONFIG_LISTError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::onFID_TUN_G_GET_CONFIG_LISTStatus(
   const ::boost::shared_ptr< Tuner_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< FID_TUN_G_GET_CONFIG_LISTStatus >& status)
{
   // only when methodStart is sent from clSDS_FmChannelList client, check on status
   if (_requestChannelList)
   {
      _requestChannelList = false;
      sds_fm_fi::SdsFmService::FMChannelItem storeFmChannelElement;
      const T_TunerConfigListElementList& resultConfigListElements = status->getTun_ConfigListElements();

      if (status->getE8ConfigListID() == T_e8_Tun_ConfigListID__TUN_TUN_CONFIG_LIST_FM)
      {
         if (resultConfigListElements.size())
         {
            _fmChannelList.clear();

            // **************************************************** for RDS stations  ****************************************************************//

            if (clSDS_KDSConfiguration::getMarketRegionType() == clSDS_KDSConfiguration::OTHER_EUR)
            {
               for (::std::vector< T_TunerConfigListElement >::const_iterator itr = resultConfigListElements.begin(); itr != resultConfigListElements.end(); ++itr)
               {
                  if (itr->getSPSName().size())
                  {
                     storeFmChannelElement.setObjectID(itr->getU32PI());
                     storeFmChannelElement.setStationName(itr->getSPSName());
                     storeFmChannelElement.setFrequency(itr->getU32Frequency());
                     _fmChannelList.push_back(storeFmChannelElement);
                  }
               }
               // check if channel list is updated
               if (isFMListUpdated())
               {
                  updateFmObjectIDToChannelNameMap();
                  // send tuner status to SDS MW as UPDATING
                  _pTunerStateObserver->vTunerStatusChanged(sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_FM, sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_UPDATING);
                  ETG_TRACE_USR4(("clSDS_FmChannelList::onFID_TUN_G_GET_CONFIG_LISTStatus : tuner status updating"));
                  // send trigger to sds_fm to store channel list in FM DB
                  _sdsFmProxy->sendStoreRDSChannelNamesRequest(*this, _fmChannelList);
               }
            }

            //************************************************************ for HD stations ************************************************************//
            std::string stationName = "";

            if (clSDS_KDSConfiguration::getMarketRegionType() == clSDS_KDSConfiguration::USA)
            {
               for (::std::vector< T_TunerConfigListElement >::const_iterator itr = resultConfigListElements.begin(); itr != resultConfigListElements.end(); ++itr)

               {
                  if (itr->getSPSName().size() && itr->getE8ModulationType() == T_e8_Tun_ModulationType__TUN_MODULATION_HD)
                  {
                     stationName = itr->getSPSName();
                     storeFmChannelElement.setFrequency(itr->getU32Frequency());
                     if (itr->getB8HDNumOfAudioPrograms().getTUN_HD_AUDIOPROGRAM_MPS())
                     {
                        storeFmChannelElement.setAudioProgram(T_e8_Tun_HDAudioProgram__TUN_AUDIO_PROGRAM_ONE);
                     }
                     else if (itr->getB8HDNumOfAudioPrograms().getTUN_HD_AUDIOPROGRAM_SPS1())
                     {
                        stationName.append(" HD1");
                        storeFmChannelElement.setAudioProgram(T_e8_Tun_HDAudioProgram__TUN_AUDIO_PROGRAM_TWO);
                     }
                     else if (itr->getB8HDNumOfAudioPrograms().getTUN_HD_AUDIOPROGRAM_SPS2())
                     {
                        stationName.append(" HD2");
                        storeFmChannelElement.setAudioProgram(T_e8_Tun_HDAudioProgram__TUN_AUDIO_PROGRAM_THREE);
                     }
                     else if (itr->getB8HDNumOfAudioPrograms().getTUN_HD_AUDIOPROGRAM_SPS3())
                     {
                        stationName.append(" HD3");
                        storeFmChannelElement.setAudioProgram(T_e8_Tun_HDAudioProgram__TUN_AUDIO_PROGRAM_FOUR);
                     }
                     else if (itr->getB8HDNumOfAudioPrograms().getTUN_HD_AUDIOPROGRAM_SPS4())
                     {
                        stationName.append(" HD4");
                        storeFmChannelElement.setAudioProgram(T_e8_Tun_HDAudioProgram__TUN_AUDIO_PROGRAM_FIVE);
                     }
                     else if (itr->getB8HDNumOfAudioPrograms().getTUN_HD_AUDIOPROGRAM_SPS5())
                     {
                        stationName.append(" HD5");
                        storeFmChannelElement.setAudioProgram(T_e8_Tun_HDAudioProgram__TUN_AUDIO_PROGRAM_SIX);
                     }
                     else if (itr->getB8HDNumOfAudioPrograms().getTUN_HD_AUDIOPROGRAM_SPS6())
                     {
                        stationName.append(" HD6");
                        storeFmChannelElement.setAudioProgram(T_e8_Tun_HDAudioProgram__TUN_AUDIO_PROGRAM_SEVEN);
                     }
                     else if (itr->getB8HDNumOfAudioPrograms().getTUN_HD_AUDIOPROGRAM_SPS7())
                     {
                        stationName.append(" HD7");
                        storeFmChannelElement.setAudioProgram(T_e8_Tun_HDAudioProgram__TUN_AUDIO_PROGRAM_EIGHT);
                     }
                     storeFmChannelElement.setStationName(stationName);
                     _fmChannelList.push_back(storeFmChannelElement);
                  }
               }
               setObjectIdsforHDStationList();
               // check if channel list is updated
               if (isFMHDListUpdated())
               {
                  updateFmObjectIDToChannelNameMap();
                  // send tuner status to SDS MW as UPDATING
                  _pTunerStateObserver->vTunerStatusChanged(sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_HD_FM, sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_UPDATING);
                  ETG_TRACE_USR4(("clSDS_FmChannelList::onFID_TUN_G_GET_CONFIG_LISTStatus : tuner status updating"));
                  // send trigger to sds_fm to store channel list in FM DB
                  _sdsFmProxy->sendStoreHDChannelNamesRequest(*this, _fmChannelList);
               }
            }
         }
      }

      // send sendFID_TUN_S_STATIONLIST_EXITStart
      _tunerProxy->sendFID_TUN_S_STATIONLIST_EXITStart(*this, T_e8_Tun_ConfigListID__TUN_TUN_CONFIG_LIST_FM);
      _releaseChannelList = false;
   }  // if (_requestChannelList)
}


/**************************************************************************//**
 *
 ******************************************************************************/
bool clSDS_FmChannelList::isFMHDListUpdated()
{
   bool stationFound = false;
   for (std::vector<sds_fm_fi::SdsFmService::FMChannelItem>::const_iterator it = _fmChannelList.begin(); it != _fmChannelList.end(); ++it)
   {
      stationFound = false;
      for (std::map<uint32 , std::string>::const_iterator itr_map = _fmObjectIDToChannelNameMap.begin(); itr_map != _fmObjectIDToChannelNameMap.end(); ++itr_map)
      {
         if (it->getStationName() == itr_map->second)
         {
            stationFound = true;
            break;
         }
      }
      if (!stationFound)
      {
         return true;
      }
   }
   return false;
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::onFID_TUN_S_GET_CONFIG_LISTError(
   const ::boost::shared_ptr< Tuner_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< FID_TUN_S_GET_CONFIG_LISTError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::onFID_TUN_S_GET_CONFIG_LISTResult(
   const ::boost::shared_ptr< Tuner_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< FID_TUN_S_GET_CONFIG_LISTResult >& result)
{
   if (result->getTunMakeConfigListElem().getU8NumOfElementsInList() == 0 && _requestChannelList)
   {
      _tunerProxy->sendFID_TUN_S_STATIONLIST_EXITStart(*this, T_e8_Tun_ConfigListID__TUN_TUN_CONFIG_LIST_FM);
      _releaseChannelList = false;
      _requestChannelList = false;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::onFID_TUN_S_STATIONLIST_EXITError(
   const ::boost::shared_ptr< Tuner_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< FID_TUN_S_STATIONLIST_EXITError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::onFID_TUN_S_STATIONLIST_EXITResult(
   const ::boost::shared_ptr< Tuner_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< FID_TUN_S_STATIONLIST_EXITResult >& /*result*/)
{
   _releaseChannelList = true;
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::updateFmObjectIDToChannelNameMap()
{
   _fmObjectIDToChannelNameMap.clear();
   for (std::vector<sds_fm_fi::SdsFmService::FMChannelItem>::const_iterator it = _fmChannelList.begin(); it != _fmChannelList.end(); ++it)
   {
      _fmObjectIDToChannelNameMap.insert(std::pair<uint32, std::string>(it->getObjectID(), it->getStationName()));
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
uint32 clSDS_FmChannelList::getFrequencyForObjectID(uint32 objectID)
{
   for (std::vector<sds_fm_fi::SdsFmService::FMChannelItem>::const_iterator it = _fmChannelList.begin(); it != _fmChannelList.end(); ++it)
   {
      if (it->getObjectID() == objectID)
      {
         return it->getFrequency();
      }
   }
   return 0;
}


/**************************************************************************//**
 *
 ******************************************************************************/
uint32 clSDS_FmChannelList::getAudioProgramForObjectID(uint32 objectID)
{
   for (std::vector<sds_fm_fi::SdsFmService::FMChannelItem>::const_iterator it = _fmChannelList.begin(); it != _fmChannelList.end(); ++it)
   {
      if (it->getObjectID() == objectID)
      {
         return it->getAudioProgram();
      }
   }
   return 0;
}


/**************************************************************************//**
 *
 ******************************************************************************/
bool clSDS_FmChannelList::isFMListUpdated()
{
   std::map<uint32, std::string>::const_iterator itr_map;

   for (std::vector<sds_fm_fi::SdsFmService::FMChannelItem>::const_iterator it = _fmChannelList.begin(); it != _fmChannelList.end(); ++it)
   {
      itr_map = _fmObjectIDToChannelNameMap.find(it->getObjectID());
      if (_fmObjectIDToChannelNameMap.end() == itr_map)
      {
         return true;
      }
      else
      {
         std::string newStationName = it->getStationName();
         std::string storedStationName = itr_map->second;
         if (StringUtils::trim(newStationName) != StringUtils::trim(storedStationName))
         {
            return true;
         }
      }
   }
   return false;
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::onExpired(asf::core::Timer& /*timer*/, boost::shared_ptr< asf::core::TimerPayload> /*data*/)
{
   //ETG_TRACE_USR4(("clSDS_FmChannelList::onExpired"));
   setTimer();
   // sendFID_TUN_S_GET_CONFIG_LISTStart to be sent only after onFID_TUN_S_STATIONLIST_EXITResult to avoid un-updated list status from Tuner.
   if (_releaseChannelList && !_requestChannelList)
   {
      T_Tun_MakeConfigList  fmtunMakeConfigList;
      T_b8_Tun_Digital_Filters u8DigitalFilters;
      fmtunMakeConfigList.setEConfigListID(T_e8_Tun_ConfigListID__TUN_TUN_CONFIG_LIST_FM);

      //read from KDS
      dp_tclKdsAMFMTunerParameter _fmAmTuner;
      tU8 sortMode = (tU8)T_e8_Tun_SortingCriteria__TUN_TUN_SORT_PI_ASCENDING;
      _fmAmTuner.u8GetFMListSortingDefault(sortMode);
      fmtunMakeConfigList.setESortingCriteria((T_e8_Tun_SortingCriteria)sortMode);

      fmtunMakeConfigList.setU32PTY(0xffffffff);
      u8DigitalFilters.setSTATIONLIST_WITH_HDMPS_SPS(true);
      fmtunMakeConfigList.setU8DigitalFilters(u8DigitalFilters);

      // fetch FM channel List from Tuner every 5 seconds
      if (_tunerProxy->isAvailable())
      {
         _tunerProxy->sendFID_TUN_S_GET_CONFIG_LISTStart(*this, fmtunMakeConfigList);
         _requestChannelList = true;
      }
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::setTimer()
{
   _timer.start(*this, 30000, 1);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_FmChannelList::setObjectIdsforHDStationList()
{
   // generate random numbers and set them as object IDs for HD stations
   time_t seconds;
   //Get value from system clock and place in seconds variable.
   time(&seconds);
   srand((unsigned int) seconds);
   for (std::vector<sds_fm_fi::SdsFmService::FMChannelItem>::iterator it = _fmChannelList.begin(); it != _fmChannelList.end(); ++it)
   {
      it->setObjectID(rand());
   }
}
