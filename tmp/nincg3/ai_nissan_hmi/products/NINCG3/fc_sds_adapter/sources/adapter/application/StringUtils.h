/*********************************************************************//**
 * \file       StringUtils.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef StringUtils_h
#define StringUtils_h

#include <string>


class StringUtils
{
   public:
      StringUtils();
      virtual ~StringUtils();

      static std::string toString(unsigned int u32Number);

      static std::string& trim(std::string& str);
};


#endif /* StringUtils_h */
