/*********************************************************************//**
 * \file       EarlyStartupPlayer.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "application/EarlyStartupPlayer.h"
#include "application/SdsAudioSource.h"
#include <string.h>

#include "SdsAdapter_Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/EarlyStartupPlayer.cpp.trc.h"
#endif

//TODO raa8hi
// - check audio settings in order to use the allocated audio channel
// - send notiication to apphmi_sds that the playback is finished


EarlyStartupPlayer::EarlyStartupPlayer(SdsAudioSource& sdsAudioSource)
   : _sdsAudioSource(sdsAudioSource)
   , _audioChannelRequested(false)
{
   _sdsAudioSource.addObserver(this);
}


EarlyStartupPlayer::~EarlyStartupPlayer()
{
}


void EarlyStartupPlayer::onAudioSourceStateChanged(arl_tenActivity state)
{
   ETG_TRACE_USR4(("EarlyStartupPlayer::onAudioSourceStateChanged state = %d", state));

   if (_audioChannelRequested)
   {
      if (state == ARL_EN_ISRC_ACT_ON)
      {
         startPlayback();
      }
      else // ARL_EN_ISRC_ACT_OFF or ARL_EN_ISRC_ACT_PAUSE
      {
         requestStopPlayback();
      }
   }
}


void EarlyStartupPlayer::onExpired(asf::core::Timer& timer, boost::shared_ptr<asf::core::TimerPayload> data)
{
   ETG_TRACE_USR4(("EarlyStartupPlayer::onExpired"));

   bool exitFlag = false;

   if (gstreamerData.bus)
   {
      // Check for End Of Stream or error messages on bus
      // The global exit_flag will be set in case of EOS or error. Exit if the flag is set
      gstreamerData.message = gst_bus_poll(gstreamerData.bus, (GstMessageType)(GST_MESSAGE_EOS | GST_MESSAGE_ERROR), 100);

      if (gstreamerData.message)
      {
         if (GST_MESSAGE_TYPE(gstreamerData.message))
         {
            exitFlag = check_bus_cb(&gstreamerData);
         }
         gst_message_unref(gstreamerData.message);
      }
      if (exitFlag)
      {
         requestStopPlayback();
         ETG_TRACE_USR4(("EarlyStartupPlayer::onExpired - playback ended"));
      }
      else
      {
         startTimer();
      }
   }
}


void EarlyStartupPlayer::requestStartPlayback()
{
   if (_audioChannelRequested == false) // avoid starting playback multiple times
   {
      ETG_TRACE_USR4(("EarlyStartupPlayer::requestStartPlayback"));

      _audioChannelRequested = true;

      _sdsAudioSource.sendAudioRouteRequest(ARL_SRC_VRU, ARL_EN_ISRC_ACT_ON, true);
   }
};


void EarlyStartupPlayer::requestStopPlayback()
{
   if (_audioChannelRequested == true)
   {
      ETG_TRACE_USR4(("EarlyStartupPlayer::requestStopPlayback"));

      _audioChannelRequested = false;

      stopPlayback();

      _sdsAudioSource.sendAudioRouteRequest(ARL_SRC_VRU, ARL_EN_ISRC_ACT_OFF, true);
   }
}


void EarlyStartupPlayer::startPlayback()
{
   ETG_TRACE_USR4(("EarlyStartupPlayer::startPlayback"));

   std::string filepath = FILE_LOCATION_PATH;

   gst_init(NULL, NULL);

   memset(gstreamerData.filelocation, 0, sizeof(gstreamerData.filelocation));
   std::strcpy(gstreamerData.filelocation, filepath.c_str());

   if (!create_pipeline(&gstreamerData))
   {
      delete_pipeline(&gstreamerData);
   }

   if (init_audio_playback_pipeline(&gstreamerData))
   {
      if (!add_bin_playback_to_pipe(&gstreamerData))
      {
         delete_pipeline(&gstreamerData);
      }

      start_playback_pipe(&gstreamerData);
   }
}


void EarlyStartupPlayer::stopPlayback()
{
   ETG_TRACE_USR4(("EarlyStartupPlayer::stopPlayback"));

   remove_bin_playback_from_pipe(&gstreamerData);
   delete_pipeline(&gstreamerData);
}


// Create the pipeline element
bool EarlyStartupPlayer::create_pipeline(gstData* data)
{
   data->pipeline = gst_pipeline_new("audio_pipeline");
   if (data->pipeline == NULL)
   {
      return false;
   }
   gst_element_set_state(data->pipeline, GST_STATE_NULL);
   return true;
}


// Callback function for dynamically linking the "wavparse" element and "alsasink" element
void EarlyStartupPlayer::on_pad_added(GstElement* /*src_element*/, GstPad* src_pad, gpointer data)
{
   ETG_TRACE_USR4(("EarlyStartupPlayer::on_pad_added"));

   GstElement* sink_element = (GstElement*) data;
   GstPad* sink_pad = gst_element_get_static_pad(sink_element, "sink");
   gst_pad_link(src_pad, sink_pad);

   gst_object_unref(sink_pad);
   sink_pad = NULL;
}


// Setup the pipeline
bool EarlyStartupPlayer::init_audio_playback_pipeline(gstData* data)
{
   ETG_TRACE_USR4(("EarlyStartupPlayer::init_audio_playback_pipeline"));

   if (data == NULL)
   {
      return false;
   }

   data->file_source = gst_element_factory_make("filesrc", "filesource");

   if (strstr(data->filelocation, ".mp3"))
   {
      data->audio_decoder = gst_element_factory_make("mad", "audiomp3decoder");
   }

   if (strstr(data->filelocation, ".wav"))
   {
      data->audio_decoder = gst_element_factory_make("wavparse", "audiowavdecoder");
   }

   data->audioconvert = gst_element_factory_make("audioconvert", "audioconverter");

   data->alsasink = gst_element_factory_make("alsasink", "audiosink");

   if (!data->file_source || !data->audio_decoder || !data->audioconvert || !data->alsasink)
   {
      return false;
   }

   g_object_set(G_OBJECT(data->file_source), "location", data->filelocation, NULL);
   g_object_set(G_OBJECT(data->alsasink), "device", "AdevAcousticoutSpeech", NULL);

   data->bin_playback = gst_bin_new("bin_playback");

   if (strstr(data->filelocation, ".mp3"))
   {
      gst_bin_add_many(GST_BIN(data->bin_playback), data->file_source, data->audio_decoder, data->audioconvert, data->alsasink, NULL);

      if (gst_element_link_many(data->file_source, data->audio_decoder, NULL) != TRUE)
      {
         return false;
      }

      if (gst_element_link_many(data->audio_decoder, data->audioconvert, NULL) != TRUE)
      {
         return false;
      }

      if (gst_element_link_many(data->audioconvert, data->alsasink, NULL) != TRUE)
      {
         return false;
      }

      ETG_TRACE_USR4(("EarlyStartupPlayer::init_audio_playback_pipeline for MP3 - SUCCESS"));
   }

   if (strstr(data->filelocation, ".wav"))
   {
      gst_bin_add_many(GST_BIN(data->bin_playback), data->file_source, data->audio_decoder, data->alsasink, NULL);

      if (gst_element_link_many(data->file_source, data->audio_decoder, NULL) != TRUE)
      {
         return false;
      }

      gst_element_link_many(data->audio_decoder, data->alsasink, NULL);

      g_signal_connect(data->audio_decoder, "pad-added", G_CALLBACK(on_pad_added), data->alsasink);

      ETG_TRACE_USR4(("EarlyStartupPlayer::init_audio_playback_pipeline for WAV - SUCCESS"));
   }

   return true;
}


// Starts the pipeline
void EarlyStartupPlayer::start_playback_pipe(gstData* data)
{
   gst_element_set_state(data->pipeline, GST_STATE_PLAYING);

   gstreamerData.bus = gst_element_get_bus(gstreamerData.pipeline);

   startTimer();
}


// Add the pipeline to the bin
bool EarlyStartupPlayer::add_bin_playback_to_pipe(gstData* data)
{
   if ((gst_bin_add(GST_BIN(data->pipeline), data->bin_playback)) != TRUE)
   {
      return false;
   }

   if (gst_element_set_state(data->pipeline, GST_STATE_NULL) == GST_STATE_CHANGE_SUCCESS)
   {
      ETG_TRACE_USR4(("EarlyStartupPlayer::add_bin_playback_to_pipe - SUCCESS"));
      return true;
   }
   else
   {
      return false;
   }
}


// Disconnect the pipeline and the bin
void EarlyStartupPlayer::remove_bin_playback_from_pipe(gstData* data)
{
   gst_element_set_state(data->pipeline, GST_STATE_NULL);
   gst_element_set_state(data->bin_playback, GST_STATE_NULL);
   gst_bin_remove(GST_BIN(data->pipeline), data->bin_playback);
}


// Cleanup
void EarlyStartupPlayer::delete_pipeline(gstData* data)
{
   if (data->bus)
   {
      gst_object_unref(data->bus);
      data->bus = NULL;
   }
   if (data->pipeline)
   {
      gst_element_set_state(data->pipeline, GST_STATE_NULL);

      gst_object_unref(data->pipeline);
      data->pipeline = NULL;
   }
}


// Function for checking the specific message on bus
// We look for EOS or Error messages
bool EarlyStartupPlayer::check_bus_cb(gstData* data)
{
   GError* err = NULL;
   gchar* dbg = NULL;

   ETG_TRACE_USR4(("EarlyStartupPlayer::check_bus_cb - message = %s", GST_MESSAGE_TYPE_NAME(data->message)));

   switch (GST_MESSAGE_TYPE(data->message))
   {
      case GST_MESSAGE_EOS:
         return true;

      case GST_MESSAGE_ERROR:
         gst_message_parse_error(data->message, &err, &dbg);
         if (err)
         {
            ETG_TRACE_USR4(("EarlyStartupPlayer::check_bus_cb - GST_MESSAGE_ERROR - err = %s", err->message));
            g_error_free(err);
         }
         if (dbg)
         {
            ETG_TRACE_USR4(("EarlyStartupPlayer::check_bus_cb - GST_MESSAGE_ERROR - dbg = %s", dbg));
            g_free(dbg);
         }
         return true;

      default:
         break;
   }

   return false;
}


void EarlyStartupPlayer::startTimer()
{
   _timer.start(*this, 500);
}
