/*********************************************************************//**
 * \file       clSDS_Method_TunerSelectStation.cpp
 *
 * clSDS_Method_TunerSelectStation method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_TunerSelectStation.h"
#include "AppHmi_MasterBase/AudioInterface/AudioDefines.h"
#include "application/GuiService.h"
#include "application/clSDS_FmChannelList.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_TunerSelectStation.cpp.trc.h"
#endif


#define DP_S_IMPORT_INTERFACE_FI
#include "dp_generic_if.h"

#define MAX_CHANNEL_NUMBER 1000
#define MAX_PRESET_SXM 19
#define INITIALISE_SUBSRC_ID -1


using namespace NS_AUDIOSRCCHG;
using namespace sxm_audio_main_fi;
using namespace sxm_audio_main_fi_types;
using namespace tuner_main_fi;
using namespace tuner_main_fi_types;
using namespace tunermaster_main_fi;
using namespace tunermaster_main_fi_types;
using namespace sds_gui_fi::SdsGuiService;


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_TunerSelectStation::~clSDS_Method_TunerSelectStation()
{
   _pFmChannelList = NULL;
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_TunerSelectStation::clSDS_Method_TunerSelectStation(
   ahl_tclBaseOneThreadService* pService,
   boost::shared_ptr<AudioSourceChangeProxy > audioSourceChangeProxy,
   ::boost::shared_ptr<Tuner_main_fiProxy > tunerProxy,
   GuiService& guiService,
   ::boost::shared_ptr<Sxm_audio_main_fiProxy > sxmAudioProxy,
   ::boost::shared_ptr< Tunermaster_main_fiProxy > tunerMasterProxy,
   clSDS_FmChannelList* pFmChannelList)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_TUNERSELECTSTATION, pService)
   , _guiService(guiService)
   , _audioSourceChangeProxy(audioSourceChangeProxy)
   , _sxmAudioProxy(sxmAudioProxy)
   , _tunerMasterProxy(tunerMasterProxy)
   , _tunerProxy(tunerProxy)
   , _pFmChannelList(pFmChannelList)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_TunerSelectStation::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgTunerSelectStationMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   vProcessMessage(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TunerSelectStation::vProcessMessage(const sds2hmi_sdsfi_tclMsgTunerSelectStationMethodStart& oMessage)
{
   ETG_TRACE_USR1(("oMessage.SelectionType.enType%d", oMessage.SelectionType.enType));
   switch (oMessage.SelectionType.enType)
   {
      case sds2hmi_fi_tcl_e8_TUN_SelectionType::FI_EN_BYFREQUENCY:
      case sds2hmi_fi_tcl_e8_TUN_SelectionType::FI_EN_BYCHANNEL:
         if (bTunerStation(oMessage))
         {
            vSendMethodResult();
            break;
         }
         else
         {
            if (oMessage.Band.enType == sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_XM)
            {
               if (_sxmAudioProxy->isAvailable() == false)
               {
                  vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
               }
               else
               {
                  vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_VALUEOUTOFRANGE);
               }
            }
            else
            {
               vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
            }

            break;
         }

      case   sds2hmi_fi_tcl_e8_TUN_SelectionType::FI_EN_BYPRESET:
         if (bTuneToPresetChannel(oMessage))
         {
            vSendMethodResult();
            break;
         }
         else
         {
            vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_PRESETNOTAVAILABLE);
            break;
         }
      case sds2hmi_fi_tcl_e8_TUN_SelectionType::FI_EN_BYID:
         if (bTuneToChannelName(oMessage))
         {
            vSendMethodResult();
         }
         else
         {
            vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
         }
         break;
      default:
         vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
         break;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_Method_TunerSelectStation::bTunerStation(const sds2hmi_sdsfi_tclMsgTunerSelectStationMethodStart& oMessage)
{
   switch (oMessage.Band.enType)
   {
      case sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_FM:
         _guiService.sendEventSignal(Event__SPEECH_DIALOG_SDS_PLAY_FM);
         activateTunerSource(SRC_TUNER_FM);
         ETG_TRACE_USR4(("FM frequency = %s", oMessage.Frequency.szValue));
         _tunerMasterProxy->sendFID_TUNMSTR_S_DIRECT_FREQUENCY_INPUTStart(*this, T_e8_Band__TUN_MSTR_BAND_FM,
               (tU32)(OSAL_dAsciiToDouble(oMessage.Frequency.szValue) * 1000));
         return TRUE;

      case sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_AM_MW:
         _guiService.sendEventSignal(Event__SPEECH_DIALOG_SDS_PLAY_AM);
         activateTunerSource(SRC_TUNER_AM);
         ETG_TRACE_USR4(("AM frequency = %s", oMessage.Frequency.szValue));
         _tunerMasterProxy->sendFID_TUNMSTR_S_DIRECT_FREQUENCY_INPUTStart(*this, T_e8_Band__TUN_MSTR_BAND_MW,
               (tU32)(OSAL_dAsciiToDouble(oMessage.Frequency.szValue)));
         return TRUE;

      case sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_XM:

         if ((oMessage.Value < MAX_CHANNEL_NUMBER) && (_sxmAudioProxy->isAvailable() == true))
         {
            _guiService.sendEventSignal(Event__SPEECH_DIALOG_SDS_PLAY_XM);
            activateTunerSource(SRC_TUNER_XM);
            const uint16 cid = (uint16)oMessage.Value;
            _sxmAudioProxy->sendSelectChannelStart(*this, T_e8_SelectChannelMode__XMTUN_SELECT_CID, cid, 0, 0);
            return TRUE;
         }
         return FALSE;

      default:
         return FALSE;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tBool clSDS_Method_TunerSelectStation::bTuneToPresetChannel(const sds2hmi_sdsfi_tclMsgTunerSelectStationMethodStart& oMessage)
{
   unsigned char numberofpresetsFM = 0xFF;
   unsigned char numberofpresetsAM = 0xFF;
   if (DP_S32_NO_ERR == DP_s32GetConfigItem("GenericTunerParameter", "NumberOfPresetsPerBankFM", &numberofpresetsFM, 1))
   {
      ETG_TRACE_USR4(("Number of presets for FM read from KDS"));
   }
   if (DP_S32_NO_ERR == DP_s32GetConfigItem("GenericTunerParameter", "NumberOfPresetsPerBankMW", &numberofpresetsAM, 1))
   {
      ETG_TRACE_USR4(("Number of presets for AM read from KDS"));
   }

   const uint8 preset = (uint8)oMessage.Value;

   switch (oMessage.Band.enType)
   {
      case sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_FM:
         if (oMessage.Value > 0 && oMessage.Value <= numberofpresetsFM)
         {
            _guiService.sendEventSignal(Event__SPEECH_DIALOG_SDS_PLAY_FM);
            activateTunerSource(SRC_TUNER_FM);
            _tunerMasterProxy->sendFID_TUNMSTR_S_PRESET_HANDLINGStart(*this, T_e8_Preset_Action__TUNMSTR_PresetRecall, preset, T_e8_List__BANK_FM1, T_e8_Band__TUN_MSTR_BAND_FM);
            return TRUE;
         }
         return FALSE;

      case sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_AM_MW:
         if (oMessage.Value > 0 && oMessage.Value <= numberofpresetsAM)
         {
            _guiService.sendEventSignal(Event__SPEECH_DIALOG_SDS_PLAY_AM);
            activateTunerSource(SRC_TUNER_AM);
            _tunerMasterProxy->sendFID_TUNMSTR_S_PRESET_HANDLINGStart(*this, T_e8_Preset_Action__TUNMSTR_PresetRecall, preset, T_e8_List__BANK_AM_MW1, T_e8_Band__TUN_MSTR_BAND_MW);
            return TRUE;
         }
         return FALSE;

      case sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_XM:
         if (_sxmAudioProxy->isAvailable() == true)
         {
            if (oMessage.Value > 0 && oMessage.Value <= MAX_PRESET_SXM)

            {
               _guiService.sendEventSignal(Event__SPEECH_DIALOG_SDS_PLAY_XM);
               activateTunerSource(SRC_TUNER_XM);
               _sxmAudioProxy->sendRecallPresetStart(*this, preset);
               return TRUE;
            }
            return FALSE;
         }
         return FALSE;

      default:
         return FALSE;
   }
}


bool clSDS_Method_TunerSelectStation::bTuneToChannelName(const sds2hmi_sdsfi_tclMsgTunerSelectStationMethodStart& oMessage)
{
   switch (oMessage.Band.enType)
   {
      case sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_FM:  // for RDS
      {
         _guiService.sendEventSignal(Event__SPEECH_DIALOG_SDS_PLAY_FM);
         activateTunerSource(SRC_TUNER_FM);
         T_Tun_Set_PI setPIPara;
         setPIPara.setU8AbsOrRel(T_e8_Tun_Abs_Or_Rel__TUN_PI_ABS);
         setPIPara.setU32Frequency(_pFmChannelList->getFrequencyForObjectID(oMessage.Value));
         setPIPara.setU16PI((uint16)oMessage.Value);
         _tunerProxy->sendFID_TUN_S_SET_PIStart(*this, setPIPara);
      }
      return true;

      case sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_HD_FM:
      {
         _guiService.sendEventSignal(Event__SPEECH_DIALOG_SDS_PLAY_FM);
         activateTunerSource(SRC_TUNER_FM);
         uint32 freq = _pFmChannelList->getFrequencyForObjectID(oMessage.Value);
         T_e8_Tun_HDAudioProgram audioProg = (T_e8_Tun_HDAudioProgram)_pFmChannelList->getAudioProgramForObjectID(oMessage.Value);
         _tunerProxy->sendFID_TUN_S_SEL_HD_AUDIOPRGMDIRECTStart(*this, freq, audioProg);
      }
      return true;

      case sds2hmi_fi_tcl_e8_TUN_Band::FI_EN_XM:
         return (bTunerStation(oMessage) == TRUE);

      default:
         return false;
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TunerSelectStation::activateTunerSource(unsigned int sourceid)
{
   _srcData.setSrcId((int32)sourceid);     // TODO jnd2hi: is it necessary that _srcData is a member??
   _srcData.setSubSrcId(INITIALISE_SUBSRC_ID);
   _audioSourceChangeProxy->sendActivateSourceRequest(*this, _srcData);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TunerSelectStation::onFID_TUNMSTR_S_DIRECT_FREQUENCY_INPUTResult(const ::boost::shared_ptr< tunermaster_main_fi::Tunermaster_main_fiProxy >& /*proxy*/,
      const ::boost::shared_ptr< tunermaster_main_fi::FID_TUNMSTR_S_DIRECT_FREQUENCY_INPUTResult >& /*result*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TunerSelectStation::onFID_TUNMSTR_S_DIRECT_FREQUENCY_INPUTError(const ::boost::shared_ptr< tunermaster_main_fi::Tunermaster_main_fiProxy >& /*proxy*/,
      const ::boost::shared_ptr< tunermaster_main_fi::FID_TUNMSTR_S_DIRECT_FREQUENCY_INPUTError >& /*Error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TunerSelectStation::onActivateSourceError(const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/,
      const ::boost::shared_ptr< ActivateSourceError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TunerSelectStation::onActivateSourceResponse(const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/,
      const ::boost::shared_ptr< ActivateSourceResponse >&/*response*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TunerSelectStation::onSelectChannelError(const ::boost::shared_ptr<Sxm_audio_main_fiProxy >& /*proxy*/,
      const ::boost::shared_ptr< SelectChannelError >& /*result*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TunerSelectStation::onSelectChannelResult(const ::boost::shared_ptr< Sxm_audio_main_fiProxy >& /*proxy*/,
      const ::boost::shared_ptr< SelectChannelResult >& /*result*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TunerSelectStation::onFID_TUNMSTR_S_PRESET_HANDLINGError(const ::boost::shared_ptr< Tunermaster_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< FID_TUNMSTR_S_PRESET_HANDLINGError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TunerSelectStation::onFID_TUNMSTR_S_PRESET_HANDLINGResult(const ::boost::shared_ptr< Tunermaster_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< FID_TUNMSTR_S_PRESET_HANDLINGResult >& /*result*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TunerSelectStation::onRecallPresetError(const ::boost::shared_ptr< Sxm_audio_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< RecallPresetError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TunerSelectStation::onRecallPresetResult(const ::boost::shared_ptr< Sxm_audio_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< RecallPresetResult >& /*result*/)
{
}


void clSDS_Method_TunerSelectStation::onFID_TUN_S_SET_PIError(const ::boost::shared_ptr< Tuner_main_fiProxy >& /*proxy*/,
      const ::boost::shared_ptr< FID_TUN_S_SET_PIError >& /*error*/)
{
}


void clSDS_Method_TunerSelectStation::onFID_TUN_S_SET_PIResult(const ::boost::shared_ptr< Tuner_main_fiProxy >& /*proxy*/,
      const ::boost::shared_ptr< FID_TUN_S_SET_PIResult >& /*result*/)
{
}


void clSDS_Method_TunerSelectStation::onFID_TUN_S_SEL_HD_AUDIOPRGMDIRECTError(const ::boost::shared_ptr< Tuner_main_fiProxy >& /*proxy*/,
      const ::boost::shared_ptr< FID_TUN_S_SEL_HD_AUDIOPRGMDIRECTError >& /*error*/)
{
}


void clSDS_Method_TunerSelectStation::onFID_TUN_S_SEL_HD_AUDIOPRGMDIRECTResult(const ::boost::shared_ptr< Tuner_main_fiProxy >& /*proxy*/,
      const ::boost::shared_ptr< FID_TUN_S_SEL_HD_AUDIOPRGMDIRECTResult >& /*result*/)
{
}
