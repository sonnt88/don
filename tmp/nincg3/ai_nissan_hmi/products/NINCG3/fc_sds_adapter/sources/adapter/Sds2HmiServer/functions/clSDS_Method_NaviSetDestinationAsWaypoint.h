/*********************************************************************//**
 * \file       clSDS_Method_NaviSetDestinationAsWaypoint.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_NaviSetDestinationAsWaypoint_h
#define clSDS_Method_NaviSetDestinationAsWaypoint_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"
#include "Sds2HmiServer/functions/clSDS_Method_NaviGetWaypointListInfo.h"
#include "external/sds2hmi_fi.h"
#include "application/GuiService.h"


class clSDS_Method_NaviGetWaypointListInfo;
class clSDS_NaviListItems;

class clSDS_Method_NaviSetDestinationAsWaypoint
   : public clServerMethod
   , public org::bosch::cm::navigation::NavigationService::InsertWaypointCallbackIF
   , public org::bosch::cm::navigation::NavigationService::ReplaceWaypointCallbackIF
   , public org::bosch::cm::navigation::NavigationService::ShowDeleteWaypointListScreenCallbackIF
{
   public:
      virtual ~clSDS_Method_NaviSetDestinationAsWaypoint();
      clSDS_Method_NaviSetDestinationAsWaypoint(
         ahl_tclBaseOneThreadService* pService,
         clSDS_Method_NaviGetWaypointListInfo* pWayPointListInfo,
         ::boost::shared_ptr<org::bosch::cm::navigation::NavigationService::NavigationServiceProxy> pNaviProxy,
         GuiService& guiService,
         clSDS_NaviListItems* poNaviListItems);

      //InsertWaypointCallbackIF
      virtual void onInsertWaypointError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::InsertWaypointError >& error);
      virtual void onInsertWaypointResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::InsertWaypointResponse >& response);

      //ReplaceWaypointCallbackIF
      virtual void onReplaceWaypointError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::ReplaceWaypointError >& error);
      virtual void onReplaceWaypointResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::ReplaceWaypointResponse >& response);

      //ShowDeleteWaypointListScreen callback IF
      virtual void onShowDeleteWaypointListScreenError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::ShowDeleteWaypointListScreenError >& error);
      virtual void onShowDeleteWaypointListScreenResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::ShowDeleteWaypointListScreenResponse >& response);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      clSDS_Method_NaviGetWaypointListInfo* _pMethod_NaviGetWaypointListInfo;
      boost::shared_ptr<org::bosch::cm::navigation::NavigationService::NavigationServiceProxy> _navigationProxy;
      GuiService& _guiService;
      clSDS_NaviListItems* _pNaviListItems;
};


#endif
