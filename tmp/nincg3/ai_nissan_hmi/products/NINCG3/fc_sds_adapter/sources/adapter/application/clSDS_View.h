/*********************************************************************//**
 * \file       clSDS_View.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_View_h
#define clSDS_View_h


#include "view_db/Sds_ViewDB.h"
#include "application/clSDS_ListItems.h"
#include "sds_gui_fi/SdsGuiServiceStub.h"
#include "sds_gui_fi/PopUpServiceStub.h"

#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"


class clSDS_View
{
   public:
      virtual ~clSDS_View();
      clSDS_View();

      tVoid vCreateView(
         sds_gui_fi::PopUpService::PopupRequestSignal& viewPopupRequestSignal,
         Sds_ViewId enViewId,
         bpstl::vector<clSDS_ListItems>& oListItems,
         const bpstl::map<bpstl::string, bpstl::string>& oScreenVariableData,
         uint32 cursorIndex);

      tVoid vCreateViewHeader(
         sds_gui_fi::PopUpService::PopupRequestSignal& viewPopupRequestSignal,
         Sds_HeaderTemplateId enTemplateId,
         const bpstl::map<bpstl::string, bpstl::string>& oHeaderValueMap,
         Sds_ViewId enViewId = SDS_VIEW_ID_LIMIT) const;

      unsigned int getCommandListCount() const;

   private:
      tVoid vInitView(sds_gui_fi::PopUpService::PopupRequestSignal& viewPopupRequestSignal) const;

      tVoid vWriteHeadline(
         sds_gui_fi::PopUpService::PopupRequestSignal& viewPopupRequestSignal,
         Sds_HeaderTemplateId enTemplateId,
         bpstl::map<bpstl::string, bpstl::string> const& oHeaderValueMap,
         Sds_ViewId enViewId) const;

      bpstl::string oResolveVariableContent(
         bpstl::string oTemplateString,
         bpstl::map<bpstl::string, bpstl::string> const& oHeaderValueMap) const;

      tVoid vWriteCommands(
         sds_gui_fi::PopUpService::PopupRequestSignal& viewPopupRequestSignal,
         bpstl::map<bpstl::string, bpstl::string> const& oScreenVariableData,
         Sds_ViewId enViewId);

      bpstl::string oMakeVariableString(bpstl::string const& oString) const;

      tVoid vWriteListData(
         sds_gui_fi::PopUpService::PopupRequestSignal& viewPopupRequestSignal,
         bpstl::vector<clSDS_ListItems>& oListItems) const;

      tBool bIsIndexWithInPage(tU32 u32NumOfListItems) const;

      sds_gui_fi::PopUpService::TextAttribute colorToTextAttribute(clSDS_ListItems::tenColorofText colorOfText) const;

      bpstl::string oResolveNumberFormat(const bpstl::string& oTemplateString) const;

      tVoid vWriteSubCommands(
         sds_gui_fi::PopUpService::PopupRequestSignal& viewPopupRequestSignal,
         bpstl::map<bpstl::string, bpstl::string> const& variables,
         Sds_ViewId enViewId,
         tU32 cursorIndex) const;

      tVoid fillTextFieldData(
         sds_gui_fi::PopUpService::TextField& textField,
         bpstl::string tagName,
         bpstl::string value,
         sds_gui_fi::PopUpService::TextAttribute colorOfText) const;

      unsigned int _commandListCount;
};


#endif
