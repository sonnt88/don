/*********************************************************************//**
 * \file       clSDS_Method_CommonGetListInfo.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef clSDS_Method_CommonGetListInfo_h
#define clSDS_Method_CommonGetListInfo_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "midw_ext_sxm_fuel_fi_types.h"
#include "application/clSDS_ListInfoObserver.h"

class clSDS_List;
class clSDS_NaviListItems;

class clSDS_Method_CommonGetListInfo : public clServerMethod, public clSDS_ListInfoObserver
{
   public:
      virtual ~clSDS_Method_CommonGetListInfo();
      clSDS_Method_CommonGetListInfo(ahl_tclBaseOneThreadService* pService, clSDS_NaviListItems* pNaviListItems);
      void vSendListAvailability(tU32 u32ListCount);
      tVoid vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::tenType, clSDS_List*);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      tVoid vProcessMessageByListType(sds2hmi_fi_tcl_e8_HMI_ListType::tenType enListType);
      sds2hmi_fi_tcl_e8_HMI_ListSize::tenType enGetListAvailability(tU32 u32ListCount) const;
      virtual void vListUpdated(clSDS_List* pList);
      void vSetNaviHMIListType(sds2hmi_fi_tcl_e8_HMI_ListType::tenType enListType);
      bpstl::map<sds2hmi_fi_tcl_e8_HMI_ListType::tenType, clSDS_List*> _oListMap;
      clSDS_List* _pCurrentList;
      clSDS_NaviListItems* _pNaviListItems;
};


#endif
