/*********************************************************************//**
 * \file       clSDS_Property_VDLStatus.cpp
 *
 * Common Action Request property implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Property_VDLStatus.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Property_VDLStatus.cpp.trc.h"
#endif


#define SMART_PHONE_INTEGRATION_VDL_DEVICE_ID 1
//#define SPORT_VDL_DEVICE_ID 2


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Property_VDLStatus::~clSDS_Property_VDLStatus()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Property_VDLStatus::clSDS_Property_VDLStatus(ahl_tclBaseOneThreadService* pService)
   : clServerProperty(SDS2HMI_SDSFI_C_U16_VDLSTATUS, pService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_VDLStatus::vSet(amt_tclServiceData* /*pInMsg*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_VDLStatus::vGet(amt_tclServiceData* /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_VDLStatus::vUpreg(amt_tclServiceData* /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_VDLStatus::vVdlDeviceConnectionLost()
{
   sds2hmi_sdsfi_tclMsgVDLStatusStatus oMessage;
   vStatus(oMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_VDLStatus::vSendStatus()
{
   sds2hmi_sdsfi_tclMsgVDLStatusStatus oMessage;
   vUpdateDeviceList(oMessage.Slots);
   vStatus(oMessage);
   vTraceVDLStatus(oMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_VDLStatus::vUpdateDeviceList(bpstl::vector<sds2hmi_fi_tcl_DeviceStatus>& /*oDeviceList*/) const
{
//   if (dataPool.bGetValue(DPSMARTPHONE__CONNECTION_STATUS))
//   {
//      vUpdateSmartPhoneIntegrationDeviceList(oDeviceList);
//   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_VDLStatus::vUpdateSmartPhoneIntegrationDeviceList(bpstl::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const
{
   sds2hmi_fi_tcl_DeviceStatus oDeviceStatus;
   oDeviceStatus.DeviceID = SMART_PHONE_INTEGRATION_VDL_DEVICE_ID;
   oDeviceStatus.DeviceName.bSet("SmartPhoneIntegration", sds2hmi_fi_tclString::FI_EN_UTF8);
   oDeviceStatus.Status.enType = eGetDeviceStatus();
   oDeviceStatus.Type.enType = sds2hmi_fi_tcl_e8_MPL_SourceType::FI_EN_SOURCE_TYPE_UNKNOWN;
   oDeviceList.push_back(oDeviceStatus);
}


/**************************************************************************//**
*
******************************************************************************/
sds2hmi_fi_tcl_e8_DeviceStatus::tenType clSDS_Property_VDLStatus::eGetDeviceStatus() const
{
//	 TODO jnd2hi determine origin of midw_ext_fi_tcl_e8_DeviceStatus
//   switch (dataPool.u8GetValue(DPSMARTPHONE__DATA_BASE_STATUS))
//   {
//      case (tU8)midw_ext_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_UNKNOWN:
//         return sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_UNKNOWN;
//
//      case (tU8)midw_ext_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_VERIFYING:
//         return sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_VERIFYING;
//
//      case (tU8)midw_ext_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_UPDATING:
//         return sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_UPDATING;
//
//      case (tU8)midw_ext_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_READY:
//         return sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_READY;
//
//      case (tU8)midw_ext_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_DISABLED:
//         return sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_DISABLED;
//
//      default:
   return sds2hmi_fi_tcl_e8_DeviceStatus::FI_EN_DEVICE_UNKNOWN;
//   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_VDLStatus::vTraceVDLStatus(const sds2hmi_sdsfi_tclMsgVDLStatusStatus& oMessage) const
{
   for (tU32 u32Index = 0; u32Index < oMessage.Slots.size(); u32Index++)
   {
      sds2hmi_fi_tcl_DeviceStatus oDeviceStatus(oMessage.Slots[u32Index]);
      ETG_TRACE_USR1(("DeviceID %d, DeviceStaus %d, DeviceType %d",
                      oDeviceStatus.DeviceID,
                      oDeviceStatus.Status.enType,
                      oDeviceStatus.Type.enType));
   }
}
