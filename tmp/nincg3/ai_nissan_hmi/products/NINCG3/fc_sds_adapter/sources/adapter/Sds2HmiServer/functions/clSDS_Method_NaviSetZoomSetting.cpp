/*********************************************************************//**
 * \file       clSDS_Method_NaviSetZoomSetting.cpp
 *
 * clSDS_Method_NaviSetZoomSetting method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_NaviSetZoomSetting.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_NaviSetZoomSetting.cpp.trc.h"
#endif

using namespace org::bosch::cm::navigation::NavigationService;

/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_NaviSetZoomSetting::~clSDS_Method_NaviSetZoomSetting()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_NaviSetZoomSetting::clSDS_Method_NaviSetZoomSetting(ahl_tclBaseOneThreadService* pService,
      ::boost::shared_ptr< NavigationServiceProxy > pSds2NaviProxy, GuiService& guiService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_NAVISETZOOMSETTING, pService)
   , _sds2NaviProxy(pSds2NaviProxy), _guiService(guiService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviSetZoomSetting::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgNaviSetZoomSettingMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   vSendZoomRequest(oMessage);
   vSendMethodResult();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_NaviSetZoomSetting::vSendZoomRequest(const sds2hmi_sdsfi_tclMsgNaviSetZoomSettingMethodStart& oMessage)
{
   uint8	zoomlevel	=	(uint8)oMessage.ZoomLevel.enType;

   switch (oMessage.ZoomAction.enType)
   {
      case sds2hmi_fi_tcl_e8_NAV_ZoomAction::FI_EN_ZOOM_IN:
      {
         ETG_TRACE_USR4((" Zoom In level %d", zoomlevel)); // TODO Remove this trace after the zoom level issue is solved
         _sds2NaviProxy->sendSetZoomOutStepRequest(*this, zoomlevel);
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
      }
      break;
      case sds2hmi_fi_tcl_e8_NAV_ZoomAction::FI_EN_ZOOM_OUT:
      {
         ETG_TRACE_USR4((" Zoom Out level %d", zoomlevel)); // TODO Remove this trace after the zoom level issue is solved
         _sds2NaviProxy->sendSetZoomInStepRequest(*this, zoomlevel);
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
      }
      break;
      case sds2hmi_fi_tcl_e8_NAV_ZoomAction::FI_EN_ZOOM_STREETS:
      {
         ETG_TRACE_USR4((" Zoom street level"));
         MapCameraMode mapCameraMode = MapCameraMode__MAP_MODE_CARSOR;
         _sds2NaviProxy->sendShowMapScreenCarsorLockModeRequest(*this, mapCameraMode);
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
      }
      break;
      default:
      {
      }
      break;
   }
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetZoomSetting::onSetZoomInStepResponse(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< SetZoomInStepResponse >& /*response*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetZoomSetting::onSetZoomInStepError(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< SetZoomInStepError >& /*error*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetZoomSetting::onSetZoomOutStepResponse(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< SetZoomOutStepResponse >& /*response*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetZoomSetting::onSetZoomOutStepError(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< SetZoomOutStepError >& /*error*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetZoomSetting::onShowMapScreenCarsorLockModeError(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< ShowMapScreenCarsorLockModeError >& /*error*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviSetZoomSetting::onShowMapScreenCarsorLockModeResponse(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
      const ::boost::shared_ptr< ShowMapScreenCarsorLockModeResponse >& /*response*/)
{
}
