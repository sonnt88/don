/*********************************************************************//**
 * \file       clSDS_Method_PhoneGetDataBases.cpp
 *
 * clSDS_Method_PhoneGetDataBases method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_PhoneGetDataBases.h"
#include "external/sds2hmi_fi.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_PhoneGetDataBases::~clSDS_Method_PhoneGetDataBases()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_PhoneGetDataBases::clSDS_Method_PhoneGetDataBases(ahl_tclBaseOneThreadService* pService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_PHONEGETDATABASES, pService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_PhoneGetDataBases::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   sds2hmi_sdsfi_tclMsgPhoneGetDatabasesMethodResult oResult;
   sds2hmi_fi_tcl_DeviceDatabase oDeviceDataBase;
   sds2hmi_fi_tclString oString;
   oString.bSet("/dev/ffs2/dynamic/connectivity/connectivity_mw/phonebook.db", sds2hmi_fi_tclString::FI_EN_UTF8);
   oDeviceDataBase.Database.push_back(oString);
   oResult.Devices.push_back(oDeviceDataBase);
   vSendMethodResult(oResult);
}
