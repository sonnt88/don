/*********************************************************************//**
 * \file       clSDS_Method_PhoneDialNumber.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_PhoneDialNumber_h
#define clSDS_Method_PhoneDialNumber_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "MOST_Tel_FIProxy.h"
#include "MOST_PhonBk_FIProxy.h"


class clSDS_ListScreen;
class clSDS_PhonebookList;
class clSDS_Userwords;
class clSDS_QuickDialList;


class clSDS_Method_PhoneDialNumber
   : public clServerMethod
   , public asf::core::ServiceAvailableIF
   , public MOST_Tel_FI::DialCallbackIF
   , public MOST_PhonBk_FI::GetContactDetailsExtendedCallbackIF
{
   public:
      virtual ~clSDS_Method_PhoneDialNumber();
      clSDS_Method_PhoneDialNumber(
         ahl_tclBaseOneThreadService* pService, clSDS_ListScreen* pListScreen,
         clSDS_PhonebookList* pPhonebookList,
         clSDS_Userwords* pUserwords,
         ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > telephoneProxy,
         ::boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy > phonebookProxy,
         clSDS_QuickDialList* pQuickDialList);

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onDialError(
         const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Tel_FI::DialError >& error);
      virtual void onDialResult(
         const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Tel_FI::DialResult >& result);

      virtual void onGetContactDetailsExtendedError(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedError >& error);
      virtual void onGetContactDetailsExtendedResult(
         const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedResult >& result);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      void vInvokeDialNumber(const sds2hmi_sdsfi_tclMsgPhoneDialNumberMethodStart& oMessage);
      void vDialNumberByValue(std::string strPhoneNumber);
      void vDialByIndex(tU32 u32Index);
      void vDialNumberByUWID(tU32 u32UwId);

      boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy > _telephoneProxy;
      boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy > _phonebookProxy;
      clSDS_PhonebookList* _pPhonebookList;
      clSDS_ListScreen* _pListScreen;
      clSDS_Userwords* _pUserwords;
      clSDS_QuickDialList* _pQuickDialList;
};


#endif
