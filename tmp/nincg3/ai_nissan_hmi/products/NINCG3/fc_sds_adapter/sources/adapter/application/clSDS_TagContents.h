/*********************************************************************//**
 * \file       clSDS_TagContents.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_TagContents_h
#define clSDS_TagContents_h


#define SYSTEM_S_IMPORT_INTERFACE_STRING
#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#include "stl_pif.h"


class clSDS_TagContents
{
   public:
      virtual ~clSDS_TagContents();
      clSDS_TagContents();
      bpstl::string oTagName;
      bpstl::string oTagValue;
      bpstl::vector<clSDS_TagContents> voChildrenTags;
};


#endif
