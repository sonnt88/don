/*********************************************************************//**
 * \file       clSDS_Method_CommonShowDialog.cpp
 *
 * clSDS_Method_CommonShowDialog method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_CommonShowDialog.h"
#include "application/clSDS_MenuManager.h"
#include "application/clSDS_ListScreen.h"
#include "application/clSDS_ScreenData.h"
#include "application/clSDS_View.h"
#include "application/GuiService.h"
#include "application/SdsPhoneService.h"
#include "Sds2HmiServer/functions/clSDS_Property_PhoneStatus.h"
#include "SdsAdapter_Trace.h"

#define ETG_ENABLED
#include "trace_interface.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_I_TTFIS_CMD_PREFIX      "SdsAdapter_"
#define ETG_I_TRACE_CHANNEL         TR_TTFIS_SAAL
#define ETG_DEFAULT_TRACE_CLASS     TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_CommonShowDialog.cpp.trc.h"
#endif


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_CommonShowDialog::~clSDS_Method_CommonShowDialog()
{
   _pListScreen = NULL;
   _pMenuManager = NULL;
   _pGuiService = NULL;
   _pSdsPhoneService = NULL;
   _pProperty_PhoneStatus = NULL;
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_CommonShowDialog::clSDS_Method_CommonShowDialog(
   ahl_tclBaseOneThreadService* pService,
   clSDS_MenuManager* pMenuManager,
   clSDS_ListScreen* pListScreen,
   clSDS_Property_PhoneStatus* pProperty_PhoneStatus,
   GuiService* pGuiService,
   SdsPhoneService* pSdsPhoneService)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_COMMONSHOWDIALOG, pService)
   , _pMenuManager(pMenuManager)
   , _pGuiService(pGuiService)
   , _pSdsPhoneService(pSdsPhoneService)
   , _pListScreen(pListScreen)
   , _pProperty_PhoneStatus(pProperty_PhoneStatus)
   , _enViewID(SDS_VIEW_ID_LIMIT)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_CommonShowDialog::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgCommonShowDialogMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   vTraceXmlData(oMessage.sXMLStream.szValue);
   clSDS_ScreenData oScreenData(oMessage.sXMLStream.szValue);
   Sds_ViewId viewId = oScreenData.enReadScreenId();
   vSetsdsViewID(viewId);
   if (_pListScreen->bIsListScreen(viewId))
   {
      _pListScreen->vSetCurrentList(viewId);
      vUpdatePageNumber(oMessage.nPopUp_UpDateType);

      if (SR_GLO_Nbest_List == viewId && sds2hmi_fi_tcl_e8_PopUp_UpdateType::FI_EN_UPDATE_ALL == oMessage.nPopUp_UpDateType.enType)
      {
         _pListScreen->vSetListScreenContents(oScreenData.oGetListItemsofView());
      }

      bpstl::vector<clSDS_ListItems> oListContents = _pListScreen->vGetListScreenContents();
      oScreenData.vStoreListContents(oListContents);
   }
   _pMenuManager->vShowSDSView(oScreenData);
   if (viewId == SR_PHO_DialNumber2 || viewId == SR_PHO_DialNumberSendText)
   {
      vUpdateSpokenDigitString(oScreenData, viewId);
   }
   vSendResult(oScreenData);
}


/**************************************************************************//**
 *.
 ******************************************************************************/
tVoid clSDS_Method_CommonShowDialog::vUpdatePageNumber(sds2hmi_fi_tcl_e8_PopUp_UpdateType nPopUp_UpDateType)
{
   switch (nPopUp_UpDateType.enType)
   {
      case sds2hmi_fi_tcl_e8_PopUp_UpdateType::FI_EN_UPDATE_ALL:
         _pListScreen->vResetPageNumber();
         break;

      case sds2hmi_fi_tcl_e8_PopUp_UpdateType::FI_EN_UPDATE_LIST_NEXT:
         _pListScreen->vIncrementPageNumber();
         break;

      case sds2hmi_fi_tcl_e8_PopUp_UpdateType::FI_EN_UPDATE_LIST_PREV:
         _pListScreen->vDecrementPageNumber();
         break;

      case sds2hmi_fi_tcl_e8_PopUp_UpdateType::FI_EN_UPDATE_TOFIRSTPAGE:
         _pListScreen->vResetPageNumber();
         break;

      case sds2hmi_fi_tcl_e8_PopUp_UpdateType::FI_EN_UPDATE_TOLASTPAGE:
         while (_pListScreen->bHasNextPage())
         {
            _pListScreen->vIncrementPageNumber();
         }
         break;

      case sds2hmi_fi_tcl_e8_PopUp_UpdateType::FI_EN_UPDATE_RESTORE_PREVIOUS:
      case sds2hmi_fi_tcl_e8_PopUp_UpdateType::FI_EN_UPDATE_HEADLINE:
         // do nothing
         break;

      default:
         NORMAL_M_ASSERT_ALWAYS();   //unexpected update type
         _pListScreen->vResetPageNumber();	//lint !e527 Unreachable code - jnd2hi: behind assertion

         break;
   }
}


/**************************************************************************//**
 *.
 ******************************************************************************/
tVoid clSDS_Method_CommonShowDialog ::vSendResult(const clSDS_ScreenData& oScreenData)
{
   sds2hmi_sdsfi_tclMsgCommonShowDialogMethodResult oResult;
   Sds_ViewId viewId = oScreenData.enReadScreenId();
   if (_pListScreen->bIsListScreen(viewId))
   {
      oResult.nListCount  = _pListScreen->u8GetNumOfElementsOnPage();
      oResult.nResultOptions.u8Value  = u8GetNextPreviousOptions();
      oResult.nPageNumber = (tU16)_pListScreen->u32GetPageNumber() + 1;
   }
   else
   {
      oResult.nListCount = (tU8)(oScreenData.u32GetNumberOfElements());
      oResult.nPageNumber = oResult.nListCount / 5;		// added for SR_GLO_Nbest_List screen
   }
   oResult.PrevDomain.enType = getPreviousDomain(viewId);
   oResult.NextDomain.enType = getNextDomain(viewId);
   vSendMethodResult(oResult);
}


/**************************************************************************//**
 *.
 ******************************************************************************/
sds2hmi_fi_tcl_e8_MainDomains::tenType clSDS_Method_CommonShowDialog::getPreviousDomain(Sds_ViewId viewId) const
{
   if (_pListScreen->bHasPreviousPage())
   {
      return sds2hmi_fi_tcl_e8_MainDomains::FI_EN_DOMAIN_NONE;
   }
   else
   {
      switch (viewId)
      {
         case SR_PHO_Main:
            return sds2hmi_fi_tcl_e8_MainDomains::FI_EN_DOMAIN_INFORMATION;

         case SR_NAV_Main:
            if (isPhoneAvailable())
            {
               return sds2hmi_fi_tcl_e8_MainDomains::FI_EN_DOMAIN_PHONE;
            }
            else
            {
               return sds2hmi_fi_tcl_e8_MainDomains::FI_EN_DOMAIN_INFORMATION;
            }

         case SR_AUD_Main:
            return sds2hmi_fi_tcl_e8_MainDomains::FI_EN_DOMAIN_NAVIGATION;

         case SR_INF_Main:
            return sds2hmi_fi_tcl_e8_MainDomains::FI_EN_DOMAIN_AUDIO;

         default:
            return sds2hmi_fi_tcl_e8_MainDomains::FI_EN_DOMAIN_NONE;
      }
   }
}


/**************************************************************************//**
 *.
 ******************************************************************************/
sds2hmi_fi_tcl_e8_MainDomains::tenType clSDS_Method_CommonShowDialog::getNextDomain(Sds_ViewId viewId) const
{
   if (_pListScreen->bHasNextPage())
   {
      return sds2hmi_fi_tcl_e8_MainDomains::FI_EN_DOMAIN_NONE;
   }
   else
   {
      switch (viewId)
      {
         case SR_PHO_Main:
            return sds2hmi_fi_tcl_e8_MainDomains::FI_EN_DOMAIN_NAVIGATION;

         case SR_NAV_Main:
            return sds2hmi_fi_tcl_e8_MainDomains::FI_EN_DOMAIN_AUDIO;

         case SR_AUD_Main:
            return sds2hmi_fi_tcl_e8_MainDomains::FI_EN_DOMAIN_INFORMATION;

         case SR_INF_Main:
            if (isPhoneAvailable())
            {
               return sds2hmi_fi_tcl_e8_MainDomains::FI_EN_DOMAIN_PHONE;
            }
            else
            {
               return sds2hmi_fi_tcl_e8_MainDomains::FI_EN_DOMAIN_NAVIGATION;
            }

         default:
            return sds2hmi_fi_tcl_e8_MainDomains::FI_EN_DOMAIN_NONE;
      }
   }
}


/**************************************************************************//**
 *.
 ******************************************************************************/
tU8 clSDS_Method_CommonShowDialog ::u8GetNextPreviousOptions() const
{
   if (_pListScreen->bHasPreviousPage() && _pListScreen->bHasNextPage())
   {
      return NEXT_PREVIOUS_PAGE;
   }
   if (_pListScreen->bHasNextPage())
   {
      return NEXT_PAGE;
   }
   if (_pListScreen->bHasPreviousPage())
   {
      return PREVIOUS_PAGE;
   }
   return SINGLE_PAGE;
}


/**************************************************************************//**
 *.
 ******************************************************************************/
tVoid clSDS_Method_CommonShowDialog::vTraceXmlData(const bpstl::string& oXmlData) const
{
   const int MAX_TRACE_SENTENCE_WORDS = 200;
   size_t sentence_no = (oXmlData.size() / MAX_TRACE_SENTENCE_WORDS);
   sentence_no++;
   size_t pos = 0;
   while (sentence_no > 0)
   {
      std::string xmlSubStr = oXmlData.substr(pos, MAX_TRACE_SENTENCE_WORDS);
      if (!(xmlSubStr.empty()))
      {
         ETG_TRACE_USR4(("XML Stream-%d = %s", pos, xmlSubStr.c_str()));
      }
      sentence_no--;
      pos += MAX_TRACE_SENTENCE_WORDS;
   }
}


/**************************************************************************//**
 *.
 ******************************************************************************/
tVoid clSDS_Method_CommonShowDialog::vUpdateSpokenDigitString(const clSDS_ScreenData& oScreenData, Sds_ViewId enViewId)
{
   ETG_TRACE_USR1(("clSDS_Method_CommonShowDialog::vUpdateSpokenDigitString"));
   if (enViewId == SR_PHO_DialNumber2 || enViewId == SR_PHO_DialNumberSendText)
   {
      ETG_TRACE_USR1(("%s", oScreenData.getPhNumDigit().c_str()));
      bpstl::string _SpokenDigits = oScreenData.getPhNumDigit();
      if (_SpokenDigits != "" && _pSdsPhoneService)
      {
         ETG_TRACE_USR1(("sendSpokenDigitsSignal"));
         _pSdsPhoneService->sendSpokenDigitsSignal(_SpokenDigits);
         _pGuiService->sendSpokenDigitsSignal(_SpokenDigits);
      }
   }
}


/**************************************************************************//**
 *.
 ******************************************************************************/
tVoid clSDS_Method_CommonShowDialog::vSetsdsViewID(Sds_ViewId enViewId)
{
   _enViewID = enViewId;
}


/**************************************************************************//**
 * TODO jnd2hi remove this function; clSDS_Method_CommonShowDialog should not
 * expose the view id; better move to clSDS_View.
 ******************************************************************************/
Sds_ViewId clSDS_Method_CommonShowDialog::GetsdsViewID() const
{
   return _enViewID;
}


/**************************************************************************//**
 *.
 ******************************************************************************/
bool clSDS_Method_CommonShowDialog::isPhoneAvailable() const
{
   if (_pProperty_PhoneStatus)
   {
      switch (_pProperty_PhoneStatus->getPhoneStatus())
      {
         case sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_BUSY:
         case sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_IDLE:
         case sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_DIALLING:
         case sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_INCALL:
         case sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_INCALL_HOLD:
         case sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_RINGING:
            return true;

         case sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_NOT_AVAILABLE:
         case sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_PAIRING:
         case sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_PHONE_NOT_READY:
         case sds2hmi_fi_tcl_e8_PHN_Status::FI_EN_UNKNOWN:
         default:
            return false;
      }
   }
   return false;
}
