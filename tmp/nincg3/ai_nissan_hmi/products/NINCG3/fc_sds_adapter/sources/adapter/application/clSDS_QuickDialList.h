/*********************************************************************//**
 * \file       clSDS_QuickDialList.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_QuickDialList_h
#define clSDS_QucikDialList_h


#define SYSTEM_S_IMPORT_INTERFACE_STRING
#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#include "stl_pif.h"

#include "application/clSDS_List.h"
#include "asf/core/Proxy.h"
#include "MOST_PhonBk_FIProxy.h"
#include "sds_gui_fi/SdsPhoneServiceProxy.h"

class clSDS_QuickDialList
   : public clSDS_List
   , public MOST_PhonBk_FI::GetContactDetailsExtendedCallbackIF
   , public MOST_PhonBk_FI::CreateContactListCallbackIF
   , public MOST_PhonBk_FI::RequestPhoneBookListSliceExtendedCallbackIF
{
   public:
      virtual ~clSDS_QuickDialList();
      clSDS_QuickDialList(::boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy> phonebookProxy);

      bpstl::vector<clSDS_ListItems> oGetItems(tU32 u32StartIndex, tU32 u32EndIndex);
      tU32 u32GetSize();
      tBool bSelectElement(tU32 u32SelectedIndex);
      bpstl::string oGetSelectedItem(tU32 u32Index);
      void listUserWords(const sds2hmi_sdsfi_tclMsgCommonSetAvailableUserwordsMethodStart& oMessage);
      void handleQuickDialList(const ::boost::shared_ptr< sds_gui_fi::SdsPhoneService::QuickDialListUpdateRequest >& request) const;
      std::string getPhoneNumber(::most_PhonBk__fi_types_Extended::T_PhonBkContactDetailsExtended contactDetailsExtd) const;
      sds2hmi_fi_tcl_e8_PHN_NumberType::tenType getPhoneNumberType(::most_PhonBk__fi_types_Extended::T_PhonBkContactDetailsExtended contactDetailsExtd) const;

      //Callbacks for GetContactDetailsExtended from PhoneBk FI
      virtual void onGetContactDetailsExtendedError(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
            const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedError >& error);

      virtual void onGetContactDetailsExtendedResult(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
            const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedResult >& result);

      //Callback for CreateContactList

      virtual void onCreateContactListError(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
                                            const ::boost::shared_ptr< MOST_PhonBk_FI::CreateContactListError >& error);

      virtual void onCreateContactListResult(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
                                             const ::boost::shared_ptr< MOST_PhonBk_FI::CreateContactListResult >& result);

      //Callback for RequestPhoneBookListSlice

      virtual void onRequestPhoneBookListSliceExtendedError(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
            const ::boost::shared_ptr< MOST_PhonBk_FI::RequestPhoneBookListSliceExtendedError >& error);

      virtual void onRequestPhoneBookListSliceExtendedResult(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy,
            const ::boost::shared_ptr< MOST_PhonBk_FI::RequestPhoneBookListSliceExtendedResult >& result);

   private:
      tVoid vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType);
      std::string oGetItem(tU32 u32Index);
      std::string oGetName(tU32 u32Index);
      uint8 oGetNumberType(tU32 u32Index);
      std::string oGetRelationshipType(tU32 u32Index);
      std::string oGetPhoneNumber(tU32 u32Index);

      // _quickDialList -->stores contact iD and relationship type received from Phone HMI
      struct quickDialList
      {
         uint32 contactID;
         std::string relationShip;
      };
      std::vector<quickDialList> _quickDialList;
      struct quickDialListForDisplay
      {
         std::string firstName;
         std::string lastName;
         std::string phoneNumber;
         sds2hmi_fi_tcl_e8_PHN_NumberType::tenType phoneNumberType;
         std::string relationshipType;
      };
      std::vector<quickDialListForDisplay> _quickDialListForDisplay;

      boost::shared_ptr<MOST_PhonBk_FI::MOST_PhonBk_FIProxy> _phonebookProxy;
      uint8 _contactHandleIndex;
      std::vector<uint32> _contactHandle;
      sds2hmi_sdsfi_tclMsgCommonSetAvailableUserwordsMethodStart _oMessage;
      uint16 _quickDialListSize;
      uint8 _quickDialCount;
      void getQuickDialList();
      void setQuickDialList(const ::most_PhonBk__fi_types_Extended::T_PhonBkPhoneBookListSliceResultExtended& PhoneBookListSliceResult);
};


#endif
