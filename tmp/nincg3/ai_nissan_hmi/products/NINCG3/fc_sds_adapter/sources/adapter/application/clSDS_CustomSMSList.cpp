/*********************************************************************//**
 * \file       clSDS_CustomSMSList.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_CustomSMSList.h"
#include "clSDS_TextMsgContent.h"

/**************************************************************************//**
*Destructor
******************************************************************************/
clSDS_CustomSMSList::~clSDS_CustomSMSList()
{
}


/**************************************************************************//**
*Constructor
******************************************************************************/
clSDS_CustomSMSList::clSDS_CustomSMSList(::boost::shared_ptr< ::MOST_Msg_FI::MOST_Msg_FIProxy > pSds2MsgProxy)
   : _messageFIProxy(pSds2MsgProxy)
   , _predefinedCustomMsgList()
{
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_CustomSMSList::u32GetSize()
{
   return _predefinedCustomMsgList.size();
}


/**************************************************************************//**
*
******************************************************************************/
std::vector<clSDS_ListItems> clSDS_CustomSMSList::oGetItems(tU32 u32StartIndex, tU32 u32EndIndex)
{
   std::vector<clSDS_ListItems> oListItems;
   for (tU32 u32Index = u32StartIndex; u32Index < u32EndIndex; u32Index++)
   {
      clSDS_ListItems oListItem;
      oListItem.oDescription.szString = oGetItem(u32Index);
      oListItems.push_back(oListItem);
   }
   return oListItems;
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_CustomSMSList::bSelectElement(tU32 u32SelectedIndex)
{
   clSDS_TextMsgContent::setTextMsgContent(_predefinedCustomMsgList[u32SelectedIndex - 1]);
   return true;
}


/**************************************************************************//**
*
******************************************************************************/
std::string clSDS_CustomSMSList::oGetItem(tU32 u32Index)
{
   if (u32Index < u32GetSize())
   {
      return _predefinedCustomMsgList[u32Index];
   }
   return "";
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_CustomSMSList::vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType)
{
   if (listType == sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_CUSTOM_SMS)
   {
      notifyListObserver();
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_CustomSMSList::onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                                      const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _messageFIProxy)
   {
      _messageFIProxy->sendPredefinedMessageListExtendedUpReg(*this);
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_CustomSMSList::onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                                        const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _messageFIProxy)
   {
      _messageFIProxy->sendPredefinedMessageListExtendedRelUpRegAll();
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_CustomSMSList::onPredefinedMessageListExtendedError(const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< MOST_Msg_FI::PredefinedMessageListExtendedError >& /*error*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_CustomSMSList::onPredefinedMessageListExtendedStatus(const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< MOST_Msg_FI::PredefinedMessageListExtendedStatus >& status)
{
   _predefinedCustomMsgList.clear();
   const ::most_Msg_fi_types_Extended::T_MsgPredefinedMessageListExtendedResult& result = status->getOPredefinedMessageListExtendedResult();
   for (::std::vector< most_Msg_fi_types_Extended::T_MsgPredefinedMessageListExtendedResultItem >::const_iterator itr = result.begin(); itr != result.end(); ++itr)
   {
      if (itr->getE8PredefinedMessageCategory() == most_Msg_fi_types_Extended::T_e8_MsgPredefinedMessageCategory__e8PRE_MSG_CAT_USER_DEF)
      {
         _predefinedCustomMsgList.push_back(itr->getSPredefinedMessageText());
      }
   }
}
