/*********************************************************************//**
 * \file       clSDS_Property_TunerStatus.cpp
 *
 * Common Action Request property implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Property_TunerStatus.h"
#include "external/sds2hmi_fi.h"
#include "SdsAdapter_Trace.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Property_TunerStatus::~clSDS_Property_TunerStatus()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Property_TunerStatus::clSDS_Property_TunerStatus(ahl_tclBaseOneThreadService* pService): clServerProperty(SDS2HMI_SDSFI_C_U16_TUNERSTATUS, pService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_TunerStatus::vSet(amt_tclServiceData* /*pInMsg*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_TunerStatus::vGet(amt_tclServiceData*  /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_TunerStatus::vUpreg(amt_tclServiceData*  /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_TunerStatus::vSendStatus()
{
   sds2hmi_sdsfi_tclMsgTunerStatusStatus oMessage;

   oMessage.DeviceList = _deviceList;

   vStatus(oMessage);
}


tVoid clSDS_Property_TunerStatus::vTunerStatusChanged(sds2hmi_fi_tcl_e8_TUN_Band::tenType devID, sds2hmi_fi_tcl_e8_DeviceStatus::tenType devStatus)
{
   bool found = false;
   for (uint i = 0; i < _deviceList.size(); i++)
   {
      if (_deviceList[i].DeviceID == devID)
      {
         _deviceList[i].Status.enType = devStatus;
         found = true;
         break;
      }
   }

   if (false == found)
   {
      sds2hmi_fi_tcl_DeviceStatus deviceStatus;
      deviceStatus.DeviceID = devID;
      deviceStatus.Status.enType = devStatus;
      _deviceList.push_back(deviceStatus);
   }

   vSendStatus();
}
