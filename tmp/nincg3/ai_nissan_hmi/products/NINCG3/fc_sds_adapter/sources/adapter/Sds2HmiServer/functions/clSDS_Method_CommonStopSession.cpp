/*********************************************************************//**
 * \file       clSDS_Method_CommonStopSession.cpp
 *
 * clSDS_Method_CommonStopSession method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_CommonStopSession.h"
#include "external/sds2hmi_fi.h"
#include "application/GuiService.h"
#include "application/SdsAudioSource.h"
#include "application/clSDS_MenuManager.h"


using namespace sds_gui_fi::SdsGuiService;


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_CommonStopSession::~clSDS_Method_CommonStopSession()
{
   _pMenuManager = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_CommonStopSession::clSDS_Method_CommonStopSession(
   ahl_tclBaseOneThreadService* pService,
   SdsAudioSource& audioSource,
   clSDS_MenuManager* pMenuManager,
   GuiService& guiService)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_COMMONSTOPSESSION, pService)
   , _audioSource(audioSource)
   , _pMenuManager(pMenuManager)
   , _guiService(guiService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_CommonStopSession::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   _guiService.sendEventSignal(Event__SPEECH_DIALOG_SDS_STOP_SESSION);
   _audioSource.sendAudioRouteRequest(ARL_SRC_VRU, ARL_EN_ISRC_ACT_OFF);
   _pMenuManager->vCloseGUI();
   vSendMethodResult();
}
