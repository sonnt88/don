/*********************************************************************//**
 * \file       clSDS_MenuManager.cpp
 *
 * clSDS_MenuManager class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_MenuManager.h"
#include "application/clSDS_Display.h"
#include "application/clSDS_SdsControl.h"
#include "application/clSDS_View.h"
#include "application/GuiService.h"
#include "application/PopUpService.h"
#include "SdsAdapter_Trace.h"

#define G_GLO_SETTINGS_ID 2010
#define G_GLO_HELP_ID 2000

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_MenuManager.cpp.trc.h"
#endif


using namespace sds_gui_fi::SdsGuiService;


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_MenuManager::~clSDS_MenuManager()
{
   _pDisplay = NULL;
   _pSdsControl = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_MenuManager::clSDS_MenuManager(
   clSDS_Display* pDisplay,
   clSDS_SdsControl* pSdsControl)

   : _pDisplay(pDisplay)
   , _pSdsControl(pSdsControl)
{
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_MenuManager::vShowSDSView(clSDS_ScreenData& oScreenData)
{
   if (_pDisplay)
   {
      _pDisplay->vShowSDSView(oScreenData);
   }
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_MenuManager::vCloseGUI(tBool bError) const
{
   if (_pDisplay)
   {
      _pDisplay->vCloseView(bError);
   }
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_MenuManager::onElementSelected(unsigned int selectedIndex)
{
   if (_pSdsControl && _pDisplay)
   {
      if (_pDisplay->hasCommandList())
      {
         _pSdsControl->sendSelectRequest((int)(strtol(Sds_ViewDB_getGrammarId(_pDisplay->enGetScreenId(), selectedIndex, _pDisplay->getScreenVariableData()).c_str(), NULL, 10)));
      }
      else
      {
         _pSdsControl->sendListSelectRequest(convertToSdsListIndex(selectedIndex));
      }
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_MenuManager::onCursorMoved(unsigned int cursorIndex)
{
   if (_pSdsControl && _pDisplay)
   {
      _pDisplay->cusorMoved(cursorIndex);
      _promptId = Sds_ViewDB_getPromptId(_pDisplay->enGetScreenId(), cursorIndex, _pDisplay->getScreenVariableData());
      _pSdsControl->sendEnterManualMode();
      _pSdsControl->sendFocusMoved((int)(strtol(Sds_ViewDB_getGrammarId(_pDisplay->enGetScreenId(), cursorIndex, _pDisplay->getScreenVariableData()).c_str(), NULL, 10)));
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_MenuManager::onRequestNextPage()
{
   if (_pSdsControl)
   {
      _pSdsControl->sendNextPageRequest();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_MenuManager::onRequestPreviousPage()
{
   if (_pSdsControl)
   {
      _pSdsControl->sendPreviousPageRequest();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_MenuManager::onSettingsRequest()
{
   if (_pSdsControl)
   {
      _pSdsControl->sendSelectRequest(G_GLO_SETTINGS_ID);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_MenuManager::onHelpRequest()
{
   if (_pSdsControl)
   {
      _pSdsControl->sendSelectRequest(G_GLO_HELP_ID);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
std::string clSDS_MenuManager::getPromptId() const
{
   return _promptId;
}


/**************************************************************************//**
 *
 ******************************************************************************/
unsigned int clSDS_MenuManager::convertToSdsListIndex(unsigned int absoluteIndex) const
{
   return (absoluteIndex + 1);
}
