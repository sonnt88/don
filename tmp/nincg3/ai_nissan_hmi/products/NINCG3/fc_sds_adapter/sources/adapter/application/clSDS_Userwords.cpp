/*********************************************************************//**
 * \file       clSDS_Userwords.cpp
 *
 * clSDS_Userwords class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_Userwords.h"
#include "application/clSDS_SdsControl.h"
#include "application/clSDS_SDSStatus.h"
#include "application/clSDS_QuickDialList.h"
#include "application/clSDS_Userword_Entry.h"
#include "application/clSDS_Userword_Profile.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Userwords.cpp.trc.h"
#endif


#define SDS_USER_UNKNOWN            0xff
#define PHONE_PROFILE_UNKNOWN       0xff
#define AVAILABLE_PROFILES_UNKNOWN  0xffffffff
#define MAXIMUM_NUMBER_OF_PROFILES  7


using namespace sds_gui_fi::SdsPhoneService;


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Userwords::~clSDS_Userwords()
{
   for (tU32 profileIndex = 0; profileIndex < _profileList.size(); profileIndex++)
   {
      try
      {
         delete _profileList[profileIndex];
      }
      catch (...)
      {
         ETG_TRACE_FATAL(("Exception in destructor"));
      }
   }
   _pSDSStatus = NULL;
   _pSdsControl = NULL;
   _pQuickDialList = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Userwords::clSDS_Userwords(
   clSDS_SDSStatus* pSDSStatus,
   clSDS_SdsControl* pSdsControl,
   clSDS_QuickDialList* pQuickDialList)

   : _pSDSStatus(pSDSStatus)
   , _pSdsControl(pSdsControl)
   , _pQuickDialList(pQuickDialList)
   , _u32ActiveProfileNum(PHONE_PROFILE_UNKNOWN)
   , _u32PBStatus(0)
   , _bUpdatedBySDS(FALSE)
   , _bUpdatedByPhone(FALSE)
   , _u32MaxProfileCount(MAXIMUM_NUMBER_OF_PROFILES)  // requirement from configstore
   , _u32CurrentSDSUser(SDS_USER_UNKNOWN)
   , _u32AvailableProfiles(AVAILABLE_PROFILES_UNKNOWN)
   , _userWordListID(0)
{
   if (_pSDSStatus != NULL)
   {
      _pSDSStatus->vRegisterObserver(this);
   }
   // create the maximum amount of profiles
   clSDS_Userword_Profile* pProfile = NULL;
   for (tU32 u32ProfileNum = 0; u32ProfileNum < MAXIMUM_NUMBER_OF_PROFILES; u32ProfileNum++)
   {
      pProfile = pCreateProfile(u32ProfileNum);
      if (pProfile)
      {
         _profileList.push_back(pProfile);
      }
   }
}


/**************************************************************************//**
*
******************************************************************************/
clSDS_Userword_Profile* clSDS_Userwords::pCreateProfile(tU32 u32ProfileNum) const
{
   if (0 < _u32MaxProfileCount)
   {
      return new clSDS_Userword_Profile(u32ProfileNum);
   }
   return NULL;
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_Userwords::bIsVoiceControlIdle() const
{
   //if (_pVoiceControlStatus != NULL)
   //{
   //   if (_pVoiceControlStatus->bIsIdle() ||
   //       _pVoiceControlStatus->bIsDialogOpen())
   //   {
   //      return TRUE;
   //   }
   //}
   //return FALSE;

   return _pSDSStatus && _pSDSStatus->bIsIdle();
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_Userwords::bIsSpeechDialogRunning() const
{
   if (_pSDSStatus &&
         (_pSDSStatus->bIsActive() ||
          _pSDSStatus->bIsDialogOpen() ||
          _pSDSStatus->bIsListening()))
   {
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_Userwords::bIsPhonebookAvailable() const
{
   if ((_u32PBStatus == 1)
         && (_u32ActiveProfileNum < _u32MaxProfileCount)
         && (0 < _u32MaxProfileCount))
   {
      return TRUE;
   }
   return FALSE;
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_Userwords::vSDSStatusChanged()
{
   /*if (bSetSDSUser() == FALSE)*/
   {
      bSetSDSUser();
      vSynchroniseWithPhoneBook();
   }
}


/**************************************************************************//**
*
******************************************************************************/
clSDS_Userword_Profile* clSDS_Userwords::pGetProfile(tU32 u32ProfileNum)
{
   for (tU32 u32ListIndex = 0; u32ListIndex < _profileList.size(); u32ListIndex++)
   {
      if (u32ProfileNum == _profileList[u32ListIndex]->u32GetProfileNumber())
      {
         return _profileList[u32ListIndex];
      }
   }
   return NULL;
}


/**************************************************************************//**
* Update based on AvailableUserwords Property Status
* Number of profiles and userwords are set up based on the first valid
* property update
******************************************************************************/
tVoid clSDS_Userwords::vNewAvailableUserwords(const tAvailableUserWords& oUserwords)
{
   //_Userwords = &oUserwords;
   vSynchroniseWithSDS(oUserwords);
   vSynchroniseWithPhoneBook();
}


/**************************************************************************//**
* Update profiles based on the message content of the property update.
******************************************************************************/
tVoid clSDS_Userwords::vSynchroniseWithSDS(const tAvailableUserWords& oUserwords)
{
   if (bIsVoiceControlIdle() || bIsSpeechDialogRunning())
   {
      for (tU32 userindex = 0; userindex < oUserwords.size(); userindex++)
      {
         sds2hmi_fi_tcl_Userword oSDS_user = oUserwords[userindex];
         clSDS_Userword_Profile* pProfile = pGetProfile(oSDS_user.UWProfile - 1);
         if (pProfile)
         {
            pProfile->vSynchroniseWithAvailableUserwords(oSDS_user);
            _bUpdatedBySDS = TRUE;
         }
      }
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userwords::vNewAvailableProfiles(tU32 u32AvailableProfiles)
{
   if (_u32AvailableProfiles != u32AvailableProfiles)
   {
      _u32AvailableProfiles = u32AvailableProfiles;
      vSynchroniseProfiles();
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userwords::vSynchroniseProfiles()
{
   if (_u32AvailableProfiles != AVAILABLE_PROFILES_UNKNOWN
         && bIsVoiceControlIdle())
   {
      vDeleteNotUsedProfiles();
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userwords::vNewPhoneBookState(const tsCMPhone_ReqPBProfileDetails* ptrReqPBProfileDetails)
{
   _u32PBStatus = (tU32)ptrReqPBProfileDetails->bIsNewPhonebook;
   vSetActiveProfile((tU32)ptrReqPBProfileDetails->u8PhonebookHandle);

   // If bSetUser returns false, it means that the message was not sent
   // Due to a blocking condition or no change. In this case sync can be
   // attempted immediately
   // If bSetUser returns true, the CCA message was sent, its methodresult
   // will trigger the synchronisation
   /*if (bSetSDSUser() == FALSE)*/
   {
      bSetSDSUser();
      vSynchroniseWithPhoneBook();
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userwords::vSetActiveProfile(tU32 u32ActiveProfileNum)
{
   _u32ActiveProfileNum = u32ActiveProfileNum;
   bSetSDSUser();
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_Userwords::bSetSDSUser()
{
   tU32 u32NewSDSUser = _u32ActiveProfileNum;
   if ((_u32CurrentSDSUser != u32NewSDSUser)
         && (bIsVoiceControlIdle() == TRUE)
         && (PHONE_PROFILE_UNKNOWN != _u32ActiveProfileNum))
   {
      if (_pSdsControl)
      {
         _pSdsControl->vSetUWProfile(u32NewSDSUser);
      }
      _u32CurrentSDSUser = u32NewSDSUser;
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userwords::vSynchroniseWithPhoneBook() const
{
   if (_bUpdatedBySDS
         && bIsVoiceControlIdle()
         && PHONE_PROFILE_UNKNOWN != _u32ActiveProfileNum
         && (_u32CurrentSDSUser == _u32ActiveProfileNum + 1)
         && bIsPhonebookAvailable())
   {
// TODO jnd2hi: either repair synchronization or remove it entirely
//      phone.vPhoneRequestPBEntryAvailability(pCreateUserwordListForPhone());
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userwords::vDeleteNotUsedProfiles()
{
   tU32 u32BitMask = 1;
   for (tU32 u32Index = 0; u32Index < _u32MaxProfileCount; u32Index++)
   {
      if ((_u32AvailableProfiles & (u32BitMask << u32Index)) == 0)
      {
         clSDS_Userword_Profile* pProfile = pGetProfile(u32Index);
         if (pProfile)
         {
            if (0 < pProfile->u32GetEntryCount())
            {
               vDeleteAllUserwords(u32Index + 1);
               //break;
            }
         }
      }
   }
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_Userwords::bGetNameAndPhoneNumberByUWID(tU32 u32UserwordID,
      bpstl::string& strName,
      bpstl::string& strNumber)
{
   clSDS_Userword_Profile* pProfile = pGetProfile(_u32ActiveProfileNum);
   if (pProfile)
   {
      return pProfile->bGetNameAndPhoneNumberByUWID(u32UserwordID, strName, strNumber);
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_Userwords::u32GetUWIDByNameAndNumber(const bpstl::string strName, const bpstl::string strNumber)
{
   clSDS_Userword_Profile* pProfile = pGetProfile(_u32ActiveProfileNum);
   if (pProfile)
   {
      return pProfile->u32GetUWIDByNameAndNumber(strName, strNumber);
   }
   return 0;
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_Userwords::u32CreateNewUWIDForNameAndNumber(bpstl::string strName, bpstl::string strNumber)
{
   clSDS_Userword_Profile* pProfile = pGetProfile(_u32ActiveProfileNum);
   if (pProfile)
   {
      return pProfile->u32CreateNewUWIDForNameAndNumber(strName, strNumber);
   }
   return 0;
}


/**************************************************************************//**
*
******************************************************************************/
toCMPhone_UserwordList* clSDS_Userwords::pCreateUserwordListForPhone(tVoid)
{
   toCMPhone_UserwordList* pUWList = OSAL_NEW toCMPhone_UserwordList;
   clSDS_Userword_Profile* pProfile = pGetProfile(_u32ActiveProfileNum);
   if (NULL == pProfile)
   {
      return pUWList;
   }

   for (tU32 u32index = 0; u32index < pProfile->u32GetEntryCount(); u32index++)
   {
      tsCMPhone_UserwordEntry oUWEntry;
      pProfile->vCreateUserwordEntryForPhone(u32index, oUWEntry);
      if (NULL != pUWList)
      {
         pUWList->push_back(oUWEntry);
      }
   }
   return pUWList;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userwords::vProcessUserwordListFromPhone(toCMPhone_UserwordList* pUserwordList)
{
   clSDS_Userword_Profile* pProfile = pGetProfile(_u32ActiveProfileNum);
   if (pProfile == NULL || !bIsPhonebookAvailable())
   {
      return;
   }
   for (tU32 u32UWindex = 0; u32UWindex < pUserwordList->size(); u32UWindex++)
   {
      const tsCMPhone_UserwordEntry& listEntry = pUserwordList->at(u32UWindex);
      if (0 == listEntry.bAvail)
      {
         tU32 u32UWID = pProfile->u32GetUWIDByNameAndNumber(listEntry.oName, listEntry.oNumber);
         if (u32UWID != 0)
         {
            vDeleteUserword(u32UWID);
            return;
         }
      }
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userwords::vRespondUserwordAvailabilityReqFromPhone(tsCMPhone_UserwordEntry* psUserwordEntry)
{
   clSDS_Userword_Profile* pProfile = pGetProfile(_u32ActiveProfileNum);
   if (pProfile && bIsPhonebookAvailable())
   {
      psUserwordEntry->bAvail = pProfile->bEntryExists(psUserwordEntry->oName, psUserwordEntry->oNumber);
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userwords::vDeleteUserword(tU32 u32UWID)
{
   /*if ((NULL != _pLanguageAndSpeaker) && (NULL != _pDeleteUserWord))
   {
      const sds_fi_tcl_LanguageAndSpeaker& language = _pLanguageAndSpeaker->oGet();
      _pDeleteUserWord->vDeletePhoneUserWord((tU16) (_u32ActiveProfileNum + 1),
                                               (tU16) u32UWID,
                                               language.Language);
   }*/
   if (bIsVoiceControlIdle() && (_pSdsControl != NULL))
   {
      _pSdsControl->vDeleteUserWord(u32UWID);
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userwords::vDeleteAllUserwords(tU32 u32UWProfile)
{
   /*if ((NULL != _pLanguageAndSpeaker) && (NULL != _pDeleteUserWord))
   {
      const clSDS_Property_LanguageAndSpeaker::tsds_fi_tcl_LanguageAndSpeaker& language
                                                   = _pLanguageAndSpeaker->oGet();

      _pDeleteAllUserWords->vDeleteAllPhoneUserWords((tU16) u32UserId,
                                               language.Language);
   }*/
   if (bIsVoiceControlIdle() && (_pSdsControl != NULL))
   {
      _pSdsControl->vDeleteAllUserWords(u32UWProfile);
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Userwords::handleQuickDialList(const ::boost::shared_ptr< QuickDialListUpdateRequest >& request) const
{
   if (_pQuickDialList != NULL)
   {
      _pQuickDialList->handleQuickDialList(request);
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Userwords::handleUserWordActions(const ::boost::shared_ptr< VoiceTagActionRequest >& request)
{
   _userWordListID = request->getUniqueContactID();
   vSetActiveProfile(request->getPhoneProfileID());
   switch (request->getUserAction())
   {
      case UserWordAction__RECORD:
         recordUserWord(_userWordListID);
         break;

      case UserWordAction__REPLACE:
         replaceUserWord(_userWordListID);
         break;

      case UserWordAction__DELETE:
         vDeleteUserword(_userWordListID);
         break;

      case UserWordAction__PLAY:
         playUserWord(_userWordListID);
         break;

      default:
         break;
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Userwords::recordUserWord(unsigned long u32UWID)
{
   if (_pSdsControl != NULL)
   {
      _pSdsControl->recordUserWord(u32UWID);
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Userwords::replaceUserWord(unsigned long u32UWID)
{
   if (_pSdsControl != NULL)
   {
      _pSdsControl->replaceUserWord(u32UWID);
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clSDS_Userwords::playUserWord(unsigned long u32UWID)
{
   if (_pSdsControl != NULL)
   {
      _pSdsControl->playUserWord(u32UWID);
   }
}


/**************************************************************************//**
*
******************************************************************************/
uint32 clSDS_Userwords::getContactID() const
{
   return _userWordListID;
}
