/*********************************************************************//**
 * \file       clSDS_ListItems.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_ListItems_h
#define clSDS_ListItems_h


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"


class clSDS_ListItems
{
   public:
      enum tenColorofText
      {
         NORMAL = 0,
         COMMAND,
         GREYED_OUT,
         PRICE_AGE_CURRENT,
         PRICE_AGE_1_DAY,
         PRICE_AGE_2_DAYS,
         PRICE_AGE_3_DAYS_PLUS
      };

      tBool bIsListScreenWithoutIndex;

      struct stColumn
      {
         stColumn() : enTextColor(NORMAL) {}
         bpstl::string szString;
         tenColorofText enTextColor;
      };

      virtual ~clSDS_ListItems();
      clSDS_ListItems();

      stColumn oCommand;
      stColumn oDescription;
      stColumn oDistance;
      stColumn oDirectionSymbol;
      stColumn oPrice;
      stColumn oLastPriceUpdate;
};


#endif
