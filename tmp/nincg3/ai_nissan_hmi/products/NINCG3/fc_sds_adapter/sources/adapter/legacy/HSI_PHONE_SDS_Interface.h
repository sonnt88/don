/*********************************************************************//**
 * \file       HSI_PHONE_SDS_Interface.h
 *
 * FILE :         clHSI_CMPhone.h
 * SW-COMPONENT:   HSI
 * DESCRIPTION:
 * AUTHOR:
 * COPYRIGHT:      (c) 2007 Blaupunkt Werke
 * HISTORY:
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef HSI_PHONE_SDS_Struct_H
#define HSI_PHONE_SDS_Struct_H


#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"


enum CMPhoneMessageId
{
   CMMSG_SDS_PHONEBOOK_STATE,
   CMMSG_SDS_RESPONSE_PB_ENTRY_AVAILABILITY,
   CMMSG_SDS_REQUEST_USERWORD_AVAILABILITY,
   CMMSG_SDS_PHONE_PROFILE_AVAILABILITY
};


#define MAX_NUMBER_OF_ENTRIES_IN_PBDETAILS 5

struct tsCMPhone_UserwordEntry
{
   bpstl::string oName;
   bpstl::string oNumber;
   tBool bAvail;
};


typedef bpstl::vector<tsCMPhone_UserwordEntry >   toCMPhone_UserwordList;

struct tsCMPhone_PBEntry
{
   tU32 u32Key;
   tU8 u8NumOfRows;
   tU8 u8RPD;
   tU8 u8SDSRequestNo;
};


struct tsCMPhone_ReqPBEntries
{
   tU32 u32Pos;
   bpstl::string oPbName;
   tU8 u8TelNmbQuantity;
   tU8 u8SDSRequestNo;
};


typedef bpstl::vector<tsCMPhone_ReqPBEntries >  toCMPhone_ReqPBEntriesList;

struct tsCMPhone_ReqPBDetails
{
   bpstl::string oPBDetails_Name;
   tU32 u32PBDetails_Pos;
   tU8 u8NumOfItems;
   tU8 u8PBDetails_IconType[MAX_NUMBER_OF_ENTRIES_IN_PBDETAILS];
   bpstl::string oPBDetails_TelNumber[MAX_NUMBER_OF_ENTRIES_IN_PBDETAILS];
};


struct tsCMPhone_ReqPBProfileDetails
{
   tBool bIsNewPhonebook;
   tU8 u8PhonebookHandle;
   tU8 u8DownloadState;
};


struct tsCMPhone_ReqPBEntries_ForG2PName
{
   tU32 u32PBNameID;
   bpstl::string oPbName;
   tU32 u32PBNumberID;
   bpstl::string oPbNumber;
};


typedef bpstl::vector<tsCMPhone_ReqPBEntries_ForG2PName >  toCMPhone_ReqPBEntries_ForG2PNameList;


#endif
