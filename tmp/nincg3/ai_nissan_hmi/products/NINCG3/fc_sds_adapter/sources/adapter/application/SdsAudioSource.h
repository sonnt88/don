/*********************************************************************//**
 * \file       SdsAudioSource.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef SdsAudioSource_h
#define SdsAudioSource_h


#include "org/bosch/ecnr/serviceProxy.h"
#include "org/bosch/audproc/serviceProxy.h"

#define ARL_S_IMPORT_INTERFACE_ASF_STR
#include "audio_routing_asf_lib.h"


class SdsAudioSourceObserver;
class clSDS_EcnrHandler;
class clSDS_AudprocHandler;
class PopUpService;


class SdsAudioSource : public arl_tclISource_ASF
{
   public:
      SdsAudioSource(ahl_tclBaseOneThreadApp* ahlApp
                     , ::boost::shared_ptr< org::bosch::ecnr::service::ServiceProxy > ecnrProxy
                     , ::boost::shared_ptr< org::bosch::audproc::service::ServiceProxy > audprocProxy
                     , PopUpService* popUpService);
      SdsAudioSource(const SdsAudioSource& rhs);               // copy constructor without implementation
      SdsAudioSource& operator =(const SdsAudioSource& rhs);   // assignment operator without implementation

      virtual ~SdsAudioSource();

      /*!
       * \brief   CALLED BY AUDIO-ROUTING-LIB:
       *          Application specific function on Source Activity start.
       * \param   enSrcNum:  (I) Source Number.
       * \param   SubSource: (I) Sub source ID (deviceID)
       * \param   rfcoSrcActivity: (I) Source Activity
       * \retval  tBool: TRUE, if source activity was successful, FALSE otherwise
       */
      virtual tBool bOnSrcActivity(arl_tenSource enSrcNum,  tU16 SubSource, const arl_tSrcActivity& rfcoSrcActivity);

      /*!
       * \brief   CALLED BY AUDIO-ROUTING-LIB:
       *          Application specific function after Allocate is processed.
       * \param   [enSrcNum]:  (I) Source Number.
       * \param   rfcoAllocRoute: (I) Reference to Allocate route result
       * \retval  tBool: TRUE, if Application performed operations successfully,
       *          FALSE otherwise
       */
      virtual tBool bOnAllocate(arl_tenSource enSrcNum, const arl_tAllocRouteResult& rfcoAllocRoute);

      /**
       * \brief   CALLED BY AUDIO-ROUTING-LIB:
       *          May be overridden by Player App to Release Resources.
       *          Application specific function after DeAllocate is processed.
       * \param   enSrcNum:  (I) Source Number.
       * \retval  tBool: TRUE, if Application performed operations successfully,
       *          FALSE otherwise
       */
      virtual tBool bOnDeAllocate(arl_tenSource enSrcNum);

      /**
       * \brief   CALLED BY AUDIO-ROUTING-LIB:
       * \param   enSrcNum:  (I) Source Number.
       * \retval  tBool: TRUE, if Application performed operations successfully,
       *          FALSE otherwise
       */
      virtual tBool bOnMuteState(arl_tenSource enSrcNum, arl_tenMuteState muteState);

      /**
       * \brief Used in order to add observers
       */
      virtual tVoid addObserver(SdsAudioSourceObserver* observer);

      tVoid sendAudioRouteRequest(arl_tenSource enSource, arl_tenActivity enRequest, bool earlyPlaybackRequest = false);

   private:
      /**
       * \brief Used in order to notify observers about the audio state changes
       */
      tVoid notifyObservers(arl_tenActivity state);
      arl_tenActivity midwToArlActivityValue(midw_fi_tcl_e8_SrcActivity::tenType srcActivity);

      bpstl::vector<SdsAudioSourceObserver*> _audioObservers;
      clSDS_EcnrHandler* _pEcnrHandler;
      clSDS_AudprocHandler* _pAudprocHandler;

      bool _earlyPlaybackRequest;
};


#endif
