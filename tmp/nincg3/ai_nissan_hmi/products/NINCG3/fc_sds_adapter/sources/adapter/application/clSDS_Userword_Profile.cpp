/*********************************************************************//**
 * \file       clSDS_Userword_Profile.cpp
 *
 * clSDS_Userword_Profile class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_Userword_Profile.h"
#include "application/clSDS_Iconizer.h"
#include "application/clSDS_UserwordProfileStore.h"
#include "external/sds2hmi_fi.h"


#define MAX_VOICETAGS_PER_PROFILE            20


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Userword_Profile::~clSDS_Userword_Profile()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Userword_Profile::clSDS_Userword_Profile(tU32 u32ProfileNum)
   : _u32ProfileNum(u32ProfileNum)
{
   clSDS_UserwordProfileStore profileStore(u32ProfileNum);
   profileStore.vLoad(_oUserwordEntryList);
}


/**************************************************************************//**
*
******************************************************************************/
clSDS_Userword_Entry* clSDS_Userword_Profile::pGetEntryByUWID(tU32 u32UWID)
{
   for (tU32 u32ListIndex = 0; u32ListIndex < u32GetEntryCount(); u32ListIndex++)
   {
      if (_oUserwordEntryList[u32ListIndex].u32GetUWID() == u32UWID)
      {
         return &_oUserwordEntryList[u32ListIndex];
      }
   }
   return NULL;
}


/**************************************************************************//**
*
******************************************************************************/
const clSDS_Userword_Entry* clSDS_Userword_Profile::pGetEntryByNameAndNumber(
   const bpstl::string& strName,
   const bpstl::string& strNumber)
{
   for (tU32 u32Index = 0; u32Index < _oUserwordEntryList.size(); u32Index++)
   {
      const clSDS_Userword_Entry& oEntry = _oUserwordEntryList[u32Index];

      if ((oEntry.oGetName() == strName) &&
            (clSDS_Iconizer::oRemoveIconPrefix(strNumber) == clSDS_Iconizer::oRemoveIconPrefix(oEntry.oGetNumber())))
      {
         return &oEntry;
      }
   }
   return NULL;
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_Userword_Profile::bGetNameAndPhoneNumberByUWID(tU32 u32UserwordID,
      bpstl::string& strName,
      bpstl::string& strNumber)
{
   for (tU32 u32Index = 0; u32Index < _oUserwordEntryList.size(); u32Index++)
   {
      const clSDS_Userword_Entry& oEntry = _oUserwordEntryList[u32Index];
      if (oEntry.u32GetUWID() == u32UserwordID)
      {
         strName = oEntry.oGetName();
         strNumber = oEntry.oGetNumber();
         return TRUE;
      }
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_Userword_Profile::u32CreateNewUWIDForNameAndNumber(bpstl::string strName, bpstl::string strNumber)
{
   if (_oUserwordEntryList.size() != MAX_VOICETAGS_PER_PROFILE)
   {
      //clSDS_Userword_Entry _oEntry;
      _oEntry.vSetAvail(TRUE);
      _oEntry.vSetUWID(u32GetFreeUWID());
      _oEntry.vSetName(strName);
      _oEntry.vSetNumber(strNumber);
      //_oUserwordEntryList.push_back(oEntry);
      return _oEntry.u32GetUWID();
   }
   else
   {
      return 0;
   }
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_Userword_Profile::u32GetEntryCount() const
{
   return _oUserwordEntryList.size();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userword_Profile::vCreateUserwordEntryForPhone(tU32 u32Index,
      tsCMPhone_UserwordEntry& oUWEntry)
{
   NORMAL_M_ASSERT(u32Index < _oUserwordEntryList.size());

   const clSDS_Userword_Entry& oSdsUWEntry = _oUserwordEntryList[u32Index];

   if (0 != oSdsUWEntry.u32GetUWID())
   {
      oUWEntry.oName = oSdsUWEntry.oGetName();
      oUWEntry.oNumber = oSdsUWEntry.oGetNumber();
   }
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_Userword_Profile::u32GetFreeUWID()
{
   tU32 u32UserwordID = (_u32ProfileNum << 12) + 1;// profile number shifted to bits 16-13

   while (NULL != pGetEntryByUWID(u32UserwordID))
   {
      u32UserwordID++;
   }
   return u32UserwordID;
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_Userword_Profile::u32GetUWIDByNameAndNumber(const bpstl::string& strName,
      const bpstl::string& strNumber)
{
   const clSDS_Userword_Entry* pEntry = pGetEntryByNameAndNumber(strName, strNumber);
   if (NULL != pEntry)
   {
      return pEntry->u32GetUWID();
   }
   return 0;
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_Userword_Profile::bEntryExists(const bpstl::string& strName, const bpstl::string& strNumber)
{
   return (0 != u32GetUWIDByNameAndNumber(strName, strNumber));
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userword_Profile::vSynchroniseWithAvailableUserwords(sds2hmi_fi_tcl_Userword oSDS_user)
{
   // search each stored item in the received list by memory id
   // if found, UWID is set (again or first time) and the entry is kept
   // if not found the entry must be marked to be deleted
   //tU32 u32HMIListIndex = 0;
   //for (u32HMIListIndex = 0; u32HMIListIndex < _oUserwordEntryList.size(); u32HMIListIndex++)
   //{
   //   tBool bFound = FALSE;
   //   _oUserwordEntryList[u32HMIListIndex].u32GetMemoryID();
   //   for (tU32 u32SdsListIndex = 0; u32SdsListIndex < oSDS_user.PhoneUWs.size(); u32SdsListIndex++)
   //   {
   //      sds2hmi_fi_tcl_PhnUW phoneUW = oSDS_user.PhoneUWs[u32SdsListIndex];
   //      if (phoneUW.LabelType == _oUserwordEntryList[u32HMIListIndex].u32GetMemoryID())
   //      {
   //         _oUserwordEntryList[u32HMIListIndex].vSetUWID(phoneUW.UserwordID);
   //         bFound = TRUE;
   //         break;
   //      }
   //   }
   //   if (FALSE == bFound)
   //   {
   //      _oUserwordEntryList[u32HMIListIndex].vMarkAsNotUsed();
   //   }
   //}
   //vDeleteNotUsedEntries();
   //// at this point HMI has only entries which exist also in SDS
   //// but SDS might have entries, which are not in HMI
   //// this second update step cares about that.
   //// Entries will not be deleted immediately, they will be removed
   //// when synchronising with phonebook (due to no name and number)
   //for (tU32 u32SdsListIndex = 0; u32SdsListIndex < oSDS_user.PhoneUWs.size(); u32SdsListIndex++)
   //{
   //   tBool bFound = FALSE;
   //   sds2hmi_fi_tcl_PhnUW phoneUW = oSDS_user.PhoneUWs[u32SdsListIndex];
   //   for (u32HMIListIndex = 0; u32HMIListIndex < _oUserwordEntryList.size(); u32HMIListIndex++)
   //   {
   //      if (_oUserwordEntryList[u32HMIListIndex].u32GetUWID() == phoneUW.UserwordID)
   //      {
   //         bFound = TRUE;
   //         break;
   //      }
   //   }
   //   if (FALSE == bFound)
   //   {
   //      // an empty will be added with UWID but no name number strings
   //      // it will cause the entry to be deleted from SDS during next sync
   //      clSDS_Userword_Entry oNewEntry;
   //      oNewEntry.vSetMemoryID(u32GetFreeMemoryID());
   //      oNewEntry.vSetUWID(phoneUW.UserwordID);
   //      oNewEntry.vSetName("");
   //      oNewEntry.vSetNumber("");
   //      _oUserwordEntryList.push_back(oNewEntry);
   //   }
   //}

//search each stored item in the received list by UWID
//   if found, the entry is kept
//   if not found the entry must be marked to be deleted
   for (tU32 u32HMIListIndex = 0; u32HMIListIndex < _oUserwordEntryList.size(); u32HMIListIndex++)
   {
      tBool bFound = FALSE;
      for (tU32 u32SdsListIndex = 0; u32SdsListIndex < oSDS_user.PhoneUWs.size(); u32SdsListIndex++)
      {
         //tU32 phoneUW = oSDS_user.PhoneUWs[u32SdsListIndex];
         if (oSDS_user.PhoneUWs[u32SdsListIndex] == _oUserwordEntryList[u32HMIListIndex].u32GetUWID())
         {
            bFound = TRUE;
            break;
         }
      }
      if (FALSE == bFound)
      {
         _oUserwordEntryList[u32HMIListIndex].vMarkAsNotUsed();
      }
   }
   vDeleteNotUsedEntries();

   // at this point HMI has only entries which exist also in SDS
   // but SDS might have entries, which are not in HMI
   // this second update step cares about that.
   // Entries will not be deleted immediately, they will be removed
   // when synchronising with phonebook (due to no name and number)
   for (tU32 u32SdsListIndex = 0; u32SdsListIndex < oSDS_user.PhoneUWs.size(); u32SdsListIndex++)
   {
      tBool bFound = FALSE;
      tU32 phoneUW = oSDS_user.PhoneUWs[u32SdsListIndex];
      for (tU32 u32HMIListIndex = 0; u32HMIListIndex < _oUserwordEntryList.size(); u32HMIListIndex++)
      {
         if (_oUserwordEntryList[u32HMIListIndex].u32GetUWID() == phoneUW)
         {
            bFound = TRUE;
            break;
         }
      }
      if (FALSE == bFound)
      {
         // either the entry is newly recorded - so add it to the list, get the stored name and number
         // if not, an empty will be added with UWID but no name number strings
         // it will cause the entry to be deleted from SDS during next sync
         clSDS_Userword_Entry oNewEntry;
         oNewEntry.vSetAvail(TRUE);
         oNewEntry.vSetUWID(phoneUW);
         if (isEntryNewlyRecorded(phoneUW))
         {
            oNewEntry.vSetName(_oEntry.oGetName());
            oNewEntry.vSetNumber(_oEntry.oGetNumber());
         }
         else
         {
            oNewEntry.vSetName("");
            oNewEntry.vSetNumber("");
         }
         _oUserwordEntryList.push_back(oNewEntry);
      }
   }

   clSDS_UserwordProfileStore profileStore(_u32ProfileNum);
   profileStore.vSave(_oUserwordEntryList);
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_Userword_Profile::isEntryNewlyRecorded(tU32 tU32UWID) const
{
   if (tU32UWID == _oEntry.u32GetUWID())
   {
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Userword_Profile::vDeleteNotUsedEntries()
{
   bpstl::vector<clSDS_Userword_Entry>::iterator itEntry = _oUserwordEntryList.begin();

   while (itEntry != _oUserwordEntryList.end())
   {
      if (itEntry->bGeAvail() == FALSE)
      {
         itEntry = _oUserwordEntryList.erase(itEntry);
      }
      else
      {
         ++itEntry;
      }
   }
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_Userword_Profile::u32GetProfileNumber() const
{
   return _u32ProfileNum;
}
