/*********************************************************************//**
 * \file       clSDS_SdsConfigObserver.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef clSDS_SdsConfigObserver_h
#define clSDS_SdsConfigObserver_h


class clSDS_SdsConfigObserver
{
   public:
      clSDS_SdsConfigObserver();
      virtual ~clSDS_SdsConfigObserver();

      virtual void updateNBestMatchAudio(bool audioNBest) = 0;
      virtual void updateNBestMatchPhoneBook(bool phoneNBest) = 0;
      virtual void updateBeepOnlyMode(bool beepOnlyModeStatus) = 0;
};


#endif
