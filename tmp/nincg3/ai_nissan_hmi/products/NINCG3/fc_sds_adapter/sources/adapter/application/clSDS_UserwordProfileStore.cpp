/*********************************************************************//**
 * \file       clSDS_UserwordProfileStore.cpp
 *
 * clSDS_UserwordProfileStore class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_UserwordProfileStore.h"

#ifndef UTFUTIL_S_IMPORT_INTERFACE_GENERIC
#define UTFUTIL_S_IMPORT_INTERFACE_GENERIC
#include "utf_if.h"
#endif

#define SDS_UW_PROF_SZCONFIGFILEDIR "/dev/ffs/sdshmi/uw"
#define SDS_UW_PROF_U16MAJOR 1
#define SDS_UW_PROF_U8MINOR 0


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_UserwordProfileStore::clSDS_UserwordProfileStore(tU32 u32ProfileNum)
   : ahl_tclBaseConfigStore(SDS_UW_PROF_SZCONFIGFILEDIR, SDS_UW_PROF_U16MAJOR, SDS_UW_PROF_U8MINOR),
     _pUserwordList(NULL)
{
   if (u32ProfileNum > 7)
   {
      NORMAL_M_ASSERT_ALWAYS();  // requirement from configstore
      return;	//lint !e527 Unreachable code - jnd2hi: behind assertion
   }
   vSetUserId((tU8)u32ProfileNum);
}


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_UserwordProfileStore::~clSDS_UserwordProfileStore()
{
   _pUserwordList = NULL;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_UserwordProfileStore::vLoad(bpstl::vector<clSDS_Userword_Entry>& oUserwordList)
{
   _pUserwordList = &oUserwordList;
   vLoadUserData();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_UserwordProfileStore::vSave(const bpstl::vector<clSDS_Userword_Entry>& oUserwordList)
{
   _pUserwordList = const_cast<bpstl::vector<clSDS_Userword_Entry>*>(&oUserwordList);
   vStoreUserData();
}


/**************************************************************************//**
* Returns a default file image which is used if the config file doesn't exist.
******************************************************************************/
tVoid clSDS_UserwordProfileStore::vGetUserDefaultData(tPCU8& rpcu8Buf, tU32& ru32Length)
{
   static const tU8 au8Default[] = { 0x00, 0x00, 0x00, 0x00 };
   rpcu8Buf = au8Default;
   ru32Length = sizeof au8Default;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_UserwordProfileStore::vSetUserData(tPCU8 pcu8Buf, tU32 u32Length)
{
   if (u32Length < 4)
   {
      NORMAL_M_ASSERT_ALWAYS();  // unexpected data size
      return;	//lint !e527 Unreachable code - jnd2hi: behind assertion
   }

   if (pcu8Buf != NULL)
   {
      tPCU8 pcu8ItemPtr = pcu8Buf;
      tU32 u32NumberOfEntries = 0;
      OSAL_pvMemoryCopy(&u32NumberOfEntries, pcu8ItemPtr, sizeof(tU32));
      pcu8ItemPtr += sizeof(tU32);
      u32Length -= sizeof(tU32);

      if (_pUserwordList != NULL)
      {
         for (tU32 u32Index = 0; u32Index < u32NumberOfEntries; u32Index++)
         {
            clSDS_Userword_Entry oEntry;
            oEntry.vSetupFromBuffer(pcu8ItemPtr, u32Length);
            pcu8ItemPtr += oEntry.u32GetBufferSize();
            u32Length -= oEntry.u32GetBufferSize();
            _pUserwordList->push_back(oEntry);
         }
      }
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_UserwordProfileStore::vGetUserData(tPCU8& rpcu8Buf, tU32& ru32Length)
{
   tU32 u32BufferSize = u32GetUserDataBufferSize();
   ru32Length = u32BufferSize;

   tU8* pBuffer = (tU8*) OSAL_pvMemoryAllocate(u32BufferSize);
   if (pBuffer != NULL)
   {
      rpcu8Buf = pBuffer;
      tU8* pu8Item = pBuffer;
      tU32 u32NumberOfItems = _pUserwordList->size();
      // write number of items
      OSAL_pvMemoryCopy(pu8Item, &u32NumberOfItems, sizeof(u32NumberOfItems));
      pu8Item += sizeof(u32NumberOfItems);
      u32BufferSize -= sizeof(u32NumberOfItems);
      // write items
      for (tU32 u32Index = 0; u32Index < u32NumberOfItems; u32Index++)
      {
         _pUserwordList->at(u32Index).vGetBuffer(pu8Item, u32BufferSize);
         pu8Item += _pUserwordList->at(u32Index).u32GetBufferSize();
         u32BufferSize -= _pUserwordList->at(u32Index).u32GetBufferSize();
      }
   }
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_UserwordProfileStore::u32GetUserDataBufferSize()
{
   tU32 u32BufferSize = sizeof(tU32);   // size for number of items field
   if (_pUserwordList != NULL)
   {
      tU32 u32NumberOfItems = _pUserwordList->size();
      for (tU32 u32Index = 0; u32Index < u32NumberOfItems; u32Index++)
      {
         u32BufferSize += _pUserwordList->at(u32Index).u32GetBufferSize();
      }
   }
   return u32BufferSize;
}
