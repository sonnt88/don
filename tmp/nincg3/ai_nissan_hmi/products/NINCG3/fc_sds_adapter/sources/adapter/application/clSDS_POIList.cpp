/*********************************************************************//**
 * \file       clSDS_POIList.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_POIList.h"
#include "application/clSDS_StringVarList.h"


/**************************************************************************//**
*Destructor
******************************************************************************/
clSDS_POIList::~clSDS_POIList()
{
   _poiList.clear();
}


/**************************************************************************//**
*Constructor
******************************************************************************/
clSDS_POIList::clSDS_POIList()
{
   _poiList.clear();
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_POIList::vHandlePOIList(const std::vector< AddressListElement >& oPOIList)
{
   _poiList = oPOIList;
}


/**************************************************************************//**
*
******************************************************************************/
tU32 clSDS_POIList::u32GetSize()
{
   return _poiList.size();
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::vector<clSDS_ListItems> clSDS_POIList::oGetItems(tU32 u32StartIndex, tU32 u32EndIndex)
{
   std::vector<clSDS_ListItems> oListItems;
   for (tU32 u32Index = u32StartIndex; u32Index < std::min(u32EndIndex, u32GetSize()); u32Index++)
   {
      clSDS_ListItems oListItem;
      oListItem.oDescription.szString = oGetItem(u32Index);
      oListItems.push_back(oListItem);
   }
   return oListItems;
}


/**************************************************************************//**
*
******************************************************************************/
tBool clSDS_POIList::bSelectElement(tU32 u32SelectedIndex)
{
   if (u32SelectedIndex > 0)
   {
      clSDS_StringVarList::vSetVariable("$(Address)", oGetItem(u32SelectedIndex - 1));
      return TRUE;
   }
   return FALSE;
}


/**************************************************************************//**
*
******************************************************************************/
bpstl::string clSDS_POIList::oGetItem(tU32 u32Index)
{
   if (u32Index < _poiList.size())
   {
      return _poiList[u32Index].getData();
   }
   else
   {
      return "";
   }
}


tVoid clSDS_POIList::vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType /*listType*/)
{
}
