/*********************************************************************//**
 * \file       SdsPhoneService.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef SdsPhoneService_h
#define SdsPhoneService_h

#include "sds_gui_fi/SdsPhoneServiceStub.h"

class clSDS_Userwords;

class SdsPhoneService : public sds_gui_fi::SdsPhoneService::SdsPhoneServiceStub
{
   public:
      SdsPhoneService(clSDS_Userwords* pUserWords);
      virtual ~SdsPhoneService();

      virtual void onQuickDialListUpdateRequest(const ::boost::shared_ptr< sds_gui_fi::SdsPhoneService::QuickDialListUpdateRequest >& request);
      virtual void onVoiceTagActionRequest(const ::boost::shared_ptr< sds_gui_fi::SdsPhoneService::VoiceTagActionRequest >& request);

   private:
      clSDS_Userwords* _pUserWords;
};


#endif /* SdsPhoneService_h */
