/*********************************************************************//**
 * \file       Sds2HmiServer.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef Sds2HmiServer_h
#define Sds2HmiServer_h


#include "asf/core/Blob.h"
#include "asf/core/Logger.h"
#include "asf/core/Proxy.h"
#include "bosch/cm/ai/hmi/masteraudioservice/AudioSourceChangeProxy.h"
#include "clock_main_fiProxy.h"
#include "MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy.h"
#include "MIDW_EXT_SXM_FUEL_FIProxy.h"
#include "MIDW_EXT_SXM_PHONETICS_FIProxy.h"
#include "MOST_BTSet_FIProxy.h"
#include "MOST_Msg_FIProxy.h"
#include "MOST_PhonBk_FIProxy.h"
#include "MOST_Tel_FIProxy.h"
#include "mplay_MediaPlayer_FIProxy.h"
#include "org/bosch/audproc/serviceProxy.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"
#include "org/bosch/ecnr/serviceProxy.h"
#include "sds_fm_fi/SdsFmServiceProxy.h"
#include "sds_sxm_fi/SdsSxmServiceProxy.h"
#include "smartphoneint_main_fiProxy.h"
#include "sxm_audio_main_fiProxy.h"
#include "tcu_main_fiProxy.h"
#include "tuner_main_fiProxy.h"
#include "tunermaster_main_fiProxy.h"
#include "VEHICLE_MAIN_FIProxy.h"
#include "CMB_ACR_FIProxy.h"

class AhlApp;
class AhlService;
class AudioSourceHandler;
class EarlyStartupPlayer;
class clSDS_AddressBookList;
class clSDS_AmbigContactList;
class clSDS_AmbigNumberList;
class clSDS_ContactNumberList;
class clSDS_Display;
class clSDS_FuelPriceList;
class clSDS_G2P_FactorySettings;
class clSDS_IsVehicleMoving;
class clSDS_LanguageMediator;
class clSDS_ListScreen;
class clSDS_MenuManager;
class clSDS_MyAppsDataBase;
class clSDS_MyAppsList;
class clSDS_MultipleDestinationsList;
class clSDS_NBestList;
class clSDS_NaviListItems;
class clSDS_POIList;
class clSDS_PhonebookList;
class clSDS_PreviousDestinationsList;
class clSDS_QuickDialList;
class clSDS_ReadSmsList;
class clSDS_RecentCallsList;
class clSDS_SDSStatus;
class clSDS_SessionControl;
class clSDS_SXMAudioChannelList;
class clSDS_TunerBandRange;
class clSDS_Userwords;
class GuiService;
class PopUpService;
class SettingsService;
class SdsAudioSource;
class SdsPhoneService;
class clSDS_FmChannelList;
class clSDS_CustomSMSList;
class clSDS_FormatTimeDate;
class clSDS_Method_AppsLaunchApplication;
class clSDS_Method_CommonGetHmiListDescription;
class clSDS_Method_CommonGetListInfo;
class clSDS_Method_CommonSetActiveApplication;
class clSDS_Method_CommonSetAvailableUserwords;
class clSDS_Method_CommonShowDialog;
class clSDS_Method_ContactsGetAmbiguityList;
class clSDS_Method_MediaGetAmbiguityList;
class clSDS_Method_MediaGetDeviceInfo;
class clSDS_Method_MediaPlay;
class clSDS_Method_NaviSelectDestListEntry;
class clSDS_Method_NaviSetDestinationItem;
class clSDS_Method_NaviGetWaypointListInfo;
class clSDS_Method_NaviSetDestinationAsWaypoint;
class clSDS_Method_NaviSetMapMode;
class clSDS_Method_PhoneDialNumber;
class clSDS_Method_PhoneDialContact;
class clSDS_Method_PhoneGetContactListEntries;
class clSDS_Method_PhoneSendDTMFDigits;
class clSDS_Method_PhoneSetPhoneSetting;
class clSDS_Method_PhoneStartPairing;
class clSDS_Method_PhoneRedialLastNumber;
class clSDS_Method_TextMsgCallbackSender;
class clSDS_Method_TextMsgGetContent;
class clSDS_Method_TextMsgGetInfo;
class clSDS_Method_TextMsgSelectMessage;
class clSDS_Method_TextMsgSend;
class clSDS_Method_TextMsgSetContent;
class clSDS_Method_TextMsgSetNumber;
class clSDS_Method_TunerSelectBandMemBank;
class clSDS_Method_TunerSelectStation;
class clSDS_Property_CommonActionRequest;
class clSDS_Property_CommonCoreSpeechParameters;
class clSDS_Property_CommonSDSConfiguration;
class clSDS_Property_CommonSettingsRequest;
class clSDS_Property_CommonStatus;
class clSDS_Property_ConnectedDeviceStatus;
class clSDS_Property_MediaStatus;
class clSDS_Property_NaviCurrentCountryState;
class clSDS_Property_NaviStatus;
class clSDS_Property_PhoneStatus;
class clSDS_Property_SpecialAppStatus;
class clSDS_Property_TextMsgStatus;
class clSDS_Property_TunerStatus;
class clSDS_Method_PhoneSwitchCall;
class TraceCommandAdapter;
class AcrHandler;


class Sds2HmiServer : public asf::core::ServiceAvailableIF
{
   public:
      Sds2HmiServer();
      Sds2HmiServer(const Sds2HmiServer& rhs); // not implemented!
      virtual ~Sds2HmiServer();
      virtual void onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange) ;
      virtual void onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange) ;
      Sds2HmiServer& operator=(const Sds2HmiServer& rhs); // not implemented!

   private:
      void buildRegistry() const;
      AhlService* createSds2HmiService(AhlApp* ahlApp);
      void addServiceAvailableDelegate(asf::core::ServiceAvailableIF* obs);
      void registerServiceAvailableDelegates();
      void createproxies();

      AhlApp* _pAhlApp;
      AhlService* _pSds2HmiService;
      AudioSourceHandler* _pAudioSourceHandler;
      clSDS_AddressBookList* _pAddressBookList;
      clSDS_AmbigContactList* _pAmbigContactList;
      clSDS_AmbigNumberList* _pAmbigNumberList;
      clSDS_ContactNumberList* _pContactNumberList;
      clSDS_CustomSMSList* _pCustomSMSList;
      clSDS_Display* _pDisplay;
      EarlyStartupPlayer* _pEarlyStartupPlayer;
      clSDS_FmChannelList* _pFmChannelList;
      clSDS_FormatTimeDate* _pFormatTimeDate;
      clSDS_FuelPriceList* _pFuelPriceList;
      clSDS_G2P_FactorySettings* _pG2P_FactorySetttings;
      clSDS_IsVehicleMoving* _pIsVehicleMoving;
      clSDS_LanguageMediator* _pLanguageMediator;
      clSDS_ListScreen* _pListScreen;
      clSDS_MenuManager* _pMenuManager;
      clSDS_MultipleDestinationsList* _pMultipleDestinationList;
      clSDS_MyAppsDataBase* _pMyAppsDataBase;
      clSDS_NaviListItems* _pNaviListItems;
      clSDS_NBestList* _pNBestList;
      clSDS_PhonebookList* _pPhonebookList;
      clSDS_POIList* _pPOIList;
      clSDS_PreviousDestinationsList* _pPreviousDestinationsList;
      clSDS_QuickDialList* _pQuickDialList;
      clSDS_ReadSmsList* _pReadSmsList;
      clSDS_RecentCallsList* _pRecentCallsList;
      clSDS_SDSStatus* _pSDSStatus;
      clSDS_SessionControl* _pSessionControl;
      clSDS_SXMAudioChannelList* _pSxmAudioChannelList;
      clSDS_TunerBandRange* _pTunerBandRange;
      clSDS_Userwords* _pUserwords;
      AcrHandler* _pAcrHandler;

      GuiService* _pGuiService;
      PopUpService* _pPopUpService;
      SdsAudioSource* _pSdsAudioSource;
      SdsPhoneService* _pSdsPhoneService;
      SettingsService* _pSettingsService;
      TraceCommandAdapter* _pTraceCommandAdapter;

      clSDS_Method_AppsLaunchApplication* _pMethodAppsLaunchApplication;
      clSDS_Method_CommonGetHmiListDescription* _pMethodCommonGetHmiListDescription;
      clSDS_Method_CommonGetListInfo* _pMethodCommonGetListInfo;
      clSDS_Method_CommonSetActiveApplication* _pMethodCommonSetActiveApplication;
      clSDS_Method_CommonSetAvailableUserwords* _pMethodCommonSetAvailableUserwords;
      clSDS_Method_CommonShowDialog* _pMethodCommonShowDialog;
      clSDS_Method_ContactsGetAmbiguityList* _pMethodContactsGetAmbiguityList;
      clSDS_Method_MediaGetAmbiguityList* _pMethodMediaGetAmbiguityList;
      clSDS_Method_MediaGetDeviceInfo* _pMethodMediaGetDeviceInfo;
      clSDS_Method_MediaPlay* _pMethodMediaPlay;
      clSDS_Method_NaviGetWaypointListInfo* _pMethodNaviGetWaypointListInfo;
      clSDS_Method_NaviSelectDestListEntry* _pMethodNaviSelectDestListEntry;
      clSDS_Method_NaviSetDestinationAsWaypoint* _pMethodNaviSetDestinationAsWaypoint;
      clSDS_Method_NaviSetDestinationItem* _pMethodNaviSetDestinationItem;
      clSDS_Method_NaviSetMapMode* _pMethodNaviSetMapMode;
      clSDS_Method_PhoneDialContact* _pMethodPhoneDialContact;
      clSDS_Method_PhoneDialNumber* _pMethodPhoneDialNumber;
      clSDS_Method_PhoneGetContactListEntries* _pMethodPhoneGetContactListEntries;
      clSDS_Method_PhoneRedialLastNumber* _pMethodPhoneRedialLastNumber;
      clSDS_Method_PhoneSetPhoneSetting* _pMethodPhoneSetPhoneSetting;
      clSDS_Method_PhoneStartPairing* _pMethodPhoneStartPairing;
      clSDS_Method_PhoneSwitchCall* _pMethodPhoneSwitchCall;
      clSDS_Method_TextMsgCallbackSender* _pMethodTextMsgCallbackSender;
      clSDS_Method_TextMsgGetContent* _pMethodTextMsgGetContent;
      clSDS_Method_TextMsgGetInfo* _pMethodTextMsgGetInfo;
      clSDS_Method_TextMsgSelectMessage* _pMethodTextMsgSelectMessage;
      clSDS_Method_TextMsgSend* _pMethodTextMsgSend;
      clSDS_Method_TextMsgSetContent* _pMethodTextMsgSetContent;
      clSDS_Method_TextMsgSetNumber* _pMethodTextMsgSetNumber;
      clSDS_Method_TunerSelectBandMemBank* _pMethodTunerSelectBandMemBank;
      clSDS_Method_TunerSelectStation* _pMethodTunerSelectStation;

      clSDS_Property_CommonActionRequest* _pPropertyCommonActionRequest;
      clSDS_Property_CommonCoreSpeechParameters* _pPropertyCommonCoreSpeechParameters;
      clSDS_Property_CommonSDSConfiguration* _pPropertyCommonSDSConfiguration;
      clSDS_Property_CommonSettingsRequest* _pPropertyCommonSettingsRequest;
      clSDS_Property_CommonStatus* _pPropertyCommonStatus;
      clSDS_Property_ConnectedDeviceStatus* _pPropertyConnectedDeviceStatus;
      clSDS_Property_MediaStatus* _pPropertyMediaStatus;
      clSDS_Property_NaviCurrentCountryState* _pPropertyNaviCurrentCountryState;
      clSDS_Property_NaviStatus* _pPropertyNaviStatus;
      clSDS_Property_PhoneStatus* _pPropertyPhoneStatus;
      clSDS_Property_SpecialAppStatus* _pPropertySpecialAppStatus;
      clSDS_Property_TextMsgStatus* _pPropertyTextMsgStatus;
      clSDS_Property_TunerStatus* _pPropertyTunerStatus;

      boost::shared_ptr< :: bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::AudioSourceChangeProxy > _audioSourceChangeProxy;
      boost::shared_ptr< MOST_BTSet_FI::MOST_BTSet_FIProxy > _bluetoothSettingsProxy;
      boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy> _phonebookProxy;
      boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy > _telephoneProxy;
      boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy > _msgProxy;
      boost::shared_ptr< tuner_main_fi::Tuner_main_fiProxy > _tunerProxy;
      boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy > _sxmFuelProxy;
      boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy> _mediaPlayerProxy;
      boost::shared_ptr< tcu_main_fi::Tcu_main_fiProxy > _tcuProxy;
      boost::shared_ptr< MIDW_EXT_SXM_PHONETICS_FI::MIDW_EXT_SXM_PHONETICS_FIProxy > _sxmPhoneticsProxy;
      boost::shared_ptr< tunermaster_main_fi::Tunermaster_main_fiProxy > _tunerMasterProxy;
      boost::shared_ptr< VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy > _vehicleProxy;
      boost::shared_ptr< sxm_audio_main_fi::Sxm_audio_main_fiProxy > _sxmAudioProxy;
      boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > _naviProxy;
      boost::shared_ptr< sds_sxm_fi::SdsSxmService::SdsSxmServiceProxy > _sdsSxmProxy;
      boost::shared_ptr< sds_fm_fi::SdsFmService::SdsFmServiceProxy > _sdsFmProxy;
      boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy > _sxmCanadianFuelProxy;
      boost::shared_ptr< ::clock_main_fi::Clock_main_fiProxy > _clockProxy;
      boost::shared_ptr< ::smartphoneint_main_fi::Smartphoneint_main_fiProxy > _smatphoneintProxy;
      boost::shared_ptr< org::bosch::ecnr::service::ServiceProxy > _ecnrProxy;
      boost::shared_ptr< org::bosch::audproc::service::ServiceProxy > _audprocProxy;
      boost::shared_ptr< CMB_ACR_FI::CMB_ACR_FIProxy >_acrProxy;

      std::vector <asf::core::ServiceAvailableIF* > _serviceAvailableDelegates;
};


#endif
