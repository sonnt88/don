/*********************************************************************//**
 * \file       clSDS_Method_PhoneGetContactListEntries.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_PhoneGetContactListEntries.h"
#include "application/clSDS_Iconizer.h"
#include "application/clSDS_PhonebookList.h"
#include "application/clSDS_Userwords.h"
#include "application/StringUtils.h"
#include "SdsAdapter_Trace.h"
#include "legacy/HSI_PHONE_SDS_Interface.h"
#include "application/clSDS_QuickDialList.h"

#ifndef UTFUTIL_S_IMPORT_INTERFACE_GENERIC
#define UTFUTIL_S_IMPORT_INTERFACE_GENERIC
#include "utf_if.h"
#endif

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_PhoneGetContactListEntries.cpp.trc.h"
#endif


using namespace MOST_PhonBk_FI;
using namespace most_PhonBk_fi_types;


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_PhoneGetContactListEntries::~clSDS_Method_PhoneGetContactListEntries()
{
   _pPhonebookList = NULL;
   _pUserwords = NULL;
   _pQuickDialList = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_PhoneGetContactListEntries::clSDS_Method_PhoneGetContactListEntries(
   ahl_tclBaseOneThreadService* pService,
   clSDS_PhonebookList* pPhonebookList,
   clSDS_Userwords* pUserwords,
   ::boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy> phonebookProxy,
   clSDS_QuickDialList* pQuickDialList)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_PHONEGETCONTACTLISTENTRIES, pService)
   , _phonebookProxy(phonebookProxy)
   , _pPhonebookList(pPhonebookList)
   , _pUserwords(pUserwords)
   , _pQuickDialList(pQuickDialList)
   , _u16StartIndex(0)
   , _e8SelectionType(sds2hmi_fi_tcl_e8_GEN_SelectionType::FI_EN_UNKNOWN)
{
}


/**************************************************************************//**
*
******************************************************************************/
static clSDS_PhonebookList::tenSelectionType eGetPhonebookListSelectionType(sds2hmi_fi_tcl_e8_GEN_SelectionType::tenType e8SelectionType)
{
   switch (e8SelectionType)
   {
      case sds2hmi_fi_tcl_e8_GEN_SelectionType::FI_EN_BYVAL:
         return clSDS_PhonebookList::EN_SELECTION_FIRST;

      case sds2hmi_fi_tcl_e8_GEN_SelectionType::FI_EN_NEXT:
         return clSDS_PhonebookList::EN_SELECTION_NEXT;

      case sds2hmi_fi_tcl_e8_GEN_SelectionType::FI_EN_PREV:
         return clSDS_PhonebookList::EN_SELECTION_PREVIOUS;

      default:
         return clSDS_PhonebookList::EN_SELECTION_NEXT;
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_PhoneGetContactListEntries::vRequestPhonebookDetails()
{
   if (_pPhonebookList != NULL)
   {
      _pPhonebookList->vRequestPhonebookDetailsFromPhone(this, _u16StartIndex, eGetPhonebookListSelectionType(_e8SelectionType), FALSE);
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_PhoneGetContactListEntries::vRequestUserwordDetails()
{
   uint32 contactIDforUserWord = _pUserwords->getContactID();
   if (_phonebookProxy->isAvailable())
   {
      _phonebookProxy->sendGetContactDetailsExtendedStart(*this, contactIDforUserWord);
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_PhoneGetContactListEntries::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgPhoneGetContactListEntriesMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);

   switch (oMessage.nListEntryType.enType)
   {
      case sds2hmi_fi_tcl_e8_PHN_ListType::FI_EN_PHONEBOOK:
         _e8SelectionType = oMessage.nSelectionType.enType;
         _u16StartIndex = oMessage.nCurrentFirstID;
         vRequestPhonebookDetails();
         break;

      case sds2hmi_fi_tcl_e8_PHN_ListType::FI_EN_USERWORD_LIST:
         vRequestUserwordDetails();
         break;

      default:
         vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION); // This case is not possible.
         break;
   }
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_Method_PhoneGetContactListEntries::vPhonebookListAvailable()
{
   if ((_pPhonebookList != NULL))
   {
      if (_pPhonebookList->bIsEntryAvailable())
      {
         vPhonebookListSendMethodResult();
      }
      else
      {
         vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_LISTEMPTY);
      }
   }
}


/***********************************************************************//**
*
***************************************************************************/
sds2hmi_fi_tcl_PhoneEntry clSDS_Method_PhoneGetContactListEntries::oGetPhoneEntryData() const
{
   sds2hmi_fi_tcl_PhoneEntry oEntry;
   if (NULL != _pPhonebookList)
   {
      oEntry.FirstName.bSet(_pPhonebookList->oGetPhonebookName().c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   }
   oEntry.LastName.bSet("", sds2hmi_fi_tclString::FI_EN_UTF8);
   oEntry.PhoneNumber.bSet(oGetUniquePhoneNumber().c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   oEntry.nPhnBookListID = 0;
   oEntry.nUswID = 0;
   oEntry.nUswListID = 0;
   return oEntry;
}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_Method_PhoneGetContactListEntries::vPhonebookListSendMethodResult()
{
   if (_pPhonebookList != NULL)
   {
      sds2hmi_fi_tcl_PhoneEntry oEntry = oGetPhoneEntryData();
      sds2hmi_sdsfi_tclMsgPhoneGetContactListEntriesMethodResult oMessage;
      oMessage.tPhoneBookEntries.push_back(oEntry);
      oMessage.nNumberOfEntries = (tU16)oMessage.tPhoneBookEntries.size();
      oMessage.bNoMoreEntries = _pPhonebookList->bGetNoMoreEntriesFlag();
      vSendMethodResult(oMessage);
      vTraceMethodResult(oMessage);
   }
}


/***********************************************************************//**
* Returns the phone number in case there is only a single phone number for
* this entry. If there are multiple phone numbers, an empty string is
* returned.
***************************************************************************/
bpstl::string clSDS_Method_PhoneGetContactListEntries::oGetUniquePhoneNumber() const
{
   if (_pPhonebookList != NULL)
   {
      if (_pPhonebookList->oGetPhoneNumbers().u8NumOfItems == 1)
      {
         const tsCMPhone_ReqPBDetails& oNumberList = _pPhonebookList->oGetPhoneNumbers();
         bpstl::string oNumber = oNumberList.oPBDetails_TelNumber[0].c_str();
         return clSDS_Iconizer::oAddIconPrefix(oNumberList.u8PBDetails_IconType[0], oNumber);
      }
   }
   return "";
}


/***********************************************************************//**
*
***************************************************************************/
//static bpstl::string makeDebugString(const sds2hmi_fi_tcl_PhoneEntry& tPhoneBookEntry)
//{
//   bpstl::string oTraceString;
//   tString pString = tPhoneBookEntry.FirstName.szGet(sds2hmi_fi_tclString::FI_EN_UTF8);
//
//   oTraceString = "First:";
//   oTraceString.append(pString);
//   OSAL_DELETE[] pString;
//
//   oTraceString.append(" Last:");
//   pString = tPhoneBookEntry.LastName.szGet(sds2hmi_fi_tclString::FI_EN_UTF8);
//   oTraceString.append(pString);
//   OSAL_DELETE[] pString;
//
//   oTraceString.append(" Number:");
//   pString = tPhoneBookEntry.PhoneNumber.szGet(sds2hmi_fi_tclString::FI_EN_UTF8);
//   oTraceString.append(pString);
//   OSAL_DELETE[] pString;
//
//   oTraceString.append(" Type:");
//   oTraceString.append(StringUtils::toString((tU32)(tPhoneBookEntry.PhoneNumberType.enType)));
//
//   oTraceString.append(" nUswID:");
//   oTraceString.append(StringUtils::toString(tPhoneBookEntry.nUswID));
//
//   oTraceString.append(" nUswListID:");
//   oTraceString.append(StringUtils::toString(tPhoneBookEntry.nUswListID));
//
//   oTraceString.append(" nPhnBookListID:");
//   oTraceString.append(StringUtils::toString(tPhoneBookEntry.nPhnBookListID));
//
//   return oTraceString;
//}


/***********************************************************************//**
*
***************************************************************************/
tVoid clSDS_Method_PhoneGetContactListEntries::vTraceMethodResult(const sds2hmi_sdsfi_tclMsgPhoneGetContactListEntriesMethodResult& /*oMessage*/) const
{
// TODO jnd2hi rework trace
//   ET_TRACE_BIN(
//      TR_CLASS_SDSADP_DETAILS,
//      TR_LEVEL_HMI_INFO,
//      ET_EN_T16 _ TRACE_CCA_VALUE _
//      ET_EN_T16 _ _pService->u16GetServiceId() _
//      ET_EN_T16 _ u16GetFunctionID() _
//      ET_EN_T16 _ 0 _
//      ET_EN_T16 _ oMessage.nNumberOfEntries _
//      ET_EN_T8 _ oMessage.bNoMoreEntries _
//      ET_EN_DONE);
//
//   for (tU32 u32Index = 0; u32Index < oMessage.tPhoneBookEntries.size(); u32Index++)
//   {
//      ET_TRACE_BIN(
//         TR_CLASS_SDSADP_DETAILS,
//         TR_LEVEL_HMI_INFO,
//         ET_EN_T16 _ TRACE_CCA_VALUE _
//         ET_EN_T16 _ _pService->u16GetServiceId() _
//         ET_EN_T16 _ u16GetFunctionID() _
//         ET_EN_STRING _ makeDebugString(oMessage.tPhoneBookEntries[u32Index]).c_str() _
//         ET_EN_DONE);
//   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneGetContactListEntries::onGetContactDetailsExtendedError(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneGetContactListEntries::onGetContactDetailsExtendedResult(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedResult >& result)
{
   std::string phoneNumber = _pQuickDialList->getPhoneNumber(result->getOContactDetailsExtended());
   sds2hmi_fi_tcl_e8_PHN_NumberType::tenType phoneNumberType = _pQuickDialList->getPhoneNumberType(result->getOContactDetailsExtended());
   std::string firstname = result->getOContactDetailsExtended().getSFirstName();
   std::string lastname = result->getOContactDetailsExtended().getSLastName();

   sds2hmi_fi_tcl_PhoneEntry oEntry;
   oEntry.FirstName.bSet(firstname.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   oEntry.LastName.bSet(lastname.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   oEntry.PhoneNumber.bSet(phoneNumber.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   oEntry.PhoneNumberType.enType = phoneNumberType;
   oEntry.nUswID = 0;
   oEntry.nUswListID = _pUserwords->getContactID();

   sds2hmi_sdsfi_tclMsgPhoneGetContactListEntriesMethodResult oMessage;
   oMessage.tPhoneBookEntries.push_back(oEntry);
   oMessage.nNumberOfEntries = (tU16)oMessage.tPhoneBookEntries.size();
   oMessage.bNoMoreEntries = TRUE;
   vSendMethodResult(oMessage);
   vTraceMethodResult(oMessage);
}
