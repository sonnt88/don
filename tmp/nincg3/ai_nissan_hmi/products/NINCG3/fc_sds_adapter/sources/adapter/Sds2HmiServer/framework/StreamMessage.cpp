/*********************************************************************//**
 * \file       StreamMessage.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/framework/StreamMessage.h"


StreamMessage::StreamMessage(const tU8* pStreamData)
{
   tU32 u32Size = *((const tU32*)(pStreamData + AMT_C_U32_BASEMSG_SIZE));  //lint !e826 Suspicious pointer-to-pointer conversion (area too small)

   vSetDynMsgSize(0);
   vAddDynMsgSize(u32Size);

   // Allocate in Shared Memory
   bAllocateMessage();

   vSetStreamU8(0, pStreamData, u32Size, u32Size);
}


StreamMessage::~StreamMessage()
{
}
