/*********************************************************************//**
 * \file       clSDS_Method_PhoneDialContact.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "Sds2HmiServer/functions/clSDS_Method_PhoneDialContact.h"
#include "application/clSDS_StringVarList.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_PhoneDialContact.cpp.trc.h"
#endif


using namespace MOST_Tel_FI;
using namespace MOST_PhonBk_FI;
using namespace most_PhonBk_fi_types;


clSDS_Method_PhoneDialContact::clSDS_Method_PhoneDialContact(ahl_tclBaseOneThreadService* pService
      , ::boost::shared_ptr< MOST_Tel_FIProxy > pTelProxy
      , ::boost::shared_ptr< MOST_PhonBk_FIProxy > pPhoneBkProxy)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_PHONEDIALCONTACT, pService)
   , _telephoneProxy(pTelProxy)
   , _phoneBookProxy(pPhoneBkProxy)
{
   _numberType.enType = sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_UNKNOWN;
}


clSDS_Method_PhoneDialContact::~clSDS_Method_PhoneDialContact()
{
}


void clSDS_Method_PhoneDialContact::vMethodStart(amt_tclServiceData* pInMsg)
{
   sds2hmi_sdsfi_tclMsgPhoneDialContactMethodStart oMessage;
   vGetDataFromAmt(pInMsg, oMessage);

   _numberType = oMessage.LocationType;

   ETG_TRACE_USR4(("clSDS_Method_PhoneDialContact::vMethodStart - numberType = %d", oMessage.LocationType.enType));
   ETG_TRACE_USR4(("clSDS_Method_PhoneDialContact::vMethodStart - contactID = %d", oMessage.ContactID));

   if (_phoneBookProxy->isAvailable())
   {
      _phoneBookProxy->sendGetContactDetailsStart(*this, oMessage.ContactID, T_e8_PhonBkContactDetailFilter__e8CDF_TELEPHONE);
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}


void clSDS_Method_PhoneDialContact::onDialError(const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/, const ::boost::shared_ptr< DialError >& /*error*/)
{
   vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
}


void clSDS_Method_PhoneDialContact::onDialResult(const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/, const ::boost::shared_ptr< DialResult >& /*result*/)
{
   vSendMethodResult();
}


void clSDS_Method_PhoneDialContact::onGetContactDetailsError(const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/, const ::boost::shared_ptr< GetContactDetailsError >& /*error*/)
{
   vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
}


void clSDS_Method_PhoneDialContact::onGetContactDetailsResult(const ::boost::shared_ptr< MOST_PhonBk_FIProxy >& /*proxy*/, const ::boost::shared_ptr< GetContactDetailsResult >& result)
{
   if (_telephoneProxy->isAvailable())
   {
      std::string phoneNumber = getPhoneNumber(result->getOContactDetails());

      clSDS_StringVarList::vSetVariable("$(Number)", phoneNumber);

      _telephoneProxy->sendDialStart(*this, phoneNumber, ::most_Tel_fi_types::T_e8_TelEchoCancellationNoiseReductionSetting__e8ECNR_NOCHANGE);
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
   }
}


std::string clSDS_Method_PhoneDialContact::getPhoneNumber(T_PhonBkContactDetails contactDetails) const
{
   std::string phoneNumber;

   switch (_numberType.enType)
   {
      case sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_HOME1:
         phoneNumber = contactDetails.getSHomeNumber1();
         break;

      case sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_HOME2:
         phoneNumber = contactDetails.getSHomeNumber2();
         break;

      case sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_OFFICE1:
         phoneNumber = contactDetails.getSWorkNumber1();
         break;

      case sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_OFFICE2:
         phoneNumber = contactDetails.getSWorkNumber2();
         break;
      case sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_MOBILE1:
         phoneNumber = contactDetails.getSCellNumber1();
         break;

      case sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_MOBILE2:
         phoneNumber = contactDetails.getSCellNumber2();
         break;

      case sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_OTHER:
         phoneNumber = contactDetails.getSOtherNumber();
         break;

      case sds2hmi_fi_tcl_e8_PHN_NumberType::FI_EN_UNKNOWN: //PREFERRED
         phoneNumber = contactDetails.getSPreferredNumber();
         break;

      default:
         break;
   }

   ETG_TRACE_USR4(("clSDS_Method_PhoneDialContact::getPhoneNumber - phoneNumber = %s", phoneNumber.c_str()));

   return phoneNumber;
}
