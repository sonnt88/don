/*********************************************************************//**
 * \file       clSDS_Property_SdsStatus.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Property_SdsStatus_h
#define clSDS_Property_SdsStatus_h


#include "Sds2HmiServer/framework/clServerProperty.h"
#include "external/sds2hmi_fi.h"


class clSDS_SDSStatus;


class clSDS_Property_SdsStatus : public clServerProperty
{
   public:
      virtual ~clSDS_Property_SdsStatus();
      clSDS_Property_SdsStatus(ahl_tclBaseOneThreadService* pService, clSDS_SDSStatus* pSDSStatus);

   protected:
      virtual tVoid vGet(amt_tclServiceData* pInMsg);
      virtual tVoid vSet(amt_tclServiceData* pInMsg);
      virtual tVoid vUpreg(amt_tclServiceData* pInMsg);

   private:
      tVoid vSendSDSStatus();
      sds2hmi_fi_tcl_e8_SDS_Status::tenType _SdsStatus;
      clSDS_SDSStatus* _pSDSStatus;
};


#endif
