/*********************************************************************//**
 * \file       clSDS_SdsControl.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef clSDS_SdsControl_h
#define clSDS_SdsControl_h


class clSDS_SdsControl
{
   public:

      virtual ~clSDS_SdsControl()
      {
      }
      clSDS_SdsControl()
      {
      }

      virtual void vSendPTTLongPressEvent() = 0 ;
      virtual void vSendBackEvent() = 0;
      virtual void vAbortDialog() = 0;
      virtual void vSendPttEvent(tU32 u32Context) = 0;
      virtual void vSetSpeaker(tU16 u16SpeakerId) = 0;
      virtual void vRequestAllSpeakers() = 0;
      virtual void vDeleteUserWord(tU32 tU32UWID) = 0;
      virtual void vDeleteAllUserWords(tU32 tU32UWProfile) = 0;
      virtual void vSetUWProfile(tU32 u32NewSDSUser) = 0;
      virtual void vClearPrivateData() = 0;
      virtual void sendEnterManualMode() = 0;
      virtual void sendFocusMoved(unsigned int focusIndex) = 0;
      virtual void sendNextPageRequest() = 0;
      virtual void sendPreviousPageRequest() = 0;
      virtual void sendListSelectRequest(unsigned int selectedIndex) = 0;
      virtual void sendSelectRequest(unsigned int selectedIndex) = 0;
      virtual void sendCancelDialog() = 0;
      virtual void recordUserWord(unsigned long userWordListID) = 0;
      virtual void replaceUserWord(unsigned long userWordListID) = 0;
      virtual void playUserWord(unsigned long userWordListID) = 0;
      virtual void sendEnterPauseMode() = 0;
      virtual void sendExitPauseMode() = 0;
};


#endif
