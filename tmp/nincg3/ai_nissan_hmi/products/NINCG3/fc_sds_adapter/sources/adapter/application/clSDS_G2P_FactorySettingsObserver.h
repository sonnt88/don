/*********************************************************************//**
 * \file       clSDS_G2P_FactorySettingsObserver.h
 *
 * clSDS_G2P_FactorySettingsObserver (AIF)
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_G2P_FactorySettingsObserver_h
#define clSDS_G2P_FactorySettingsObserver_h


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"


class clSDS_G2P_FactorySettingsObserver
{
   public:
      virtual ~clSDS_G2P_FactorySettingsObserver();
      clSDS_G2P_FactorySettingsObserver();
      virtual tVoid vSetG2P_FactorySettings() = 0;
};


#endif
