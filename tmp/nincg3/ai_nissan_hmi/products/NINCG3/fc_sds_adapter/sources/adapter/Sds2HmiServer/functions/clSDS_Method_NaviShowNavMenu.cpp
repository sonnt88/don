/*********************************************************************//**
 * \file       clSDS_Method_NaviShowNavMenu.cpp
 *
 * clSDS_Method_NaviShowNavMenu method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_NaviShowNavMenu.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_NaviShowNavMenu.cpp.trc.h"
#endif


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_NaviShowNavMenu::~clSDS_Method_NaviShowNavMenu()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_NaviShowNavMenu::clSDS_Method_NaviShowNavMenu(
   ahl_tclBaseOneThreadService* pService,
   GuiService& guiService,
   ::boost::shared_ptr< NavigationServiceProxy > naviProxy)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_NAVISHOWNAVMENU, pService)
   , _naviProxy(naviProxy)
   , _guiService(guiService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_NaviShowNavMenu::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgNaviShowNavMenuMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);

   switch (oMessage.nMenuType.enType)
   {
      case sds2hmi_fi_tcl_e8_NAV_MenuType::FI_EN_ADJUST_LOCATION:
         _naviProxy->sendShowAdjustCurrentLocationScreenRequest(*this);
         _guiService.sendEventSignal(sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE);
         vSendMethodResult();
         break;

      default:
         ETG_TRACE_FATAL(("unsupported request to method NaviShowNavMenu"));
         vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
         break;
   }
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviShowNavMenu::onShowAdjustCurrentLocationScreenError(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< ShowAdjustCurrentLocationScreenError >& /*error*/)
{
}


/***********************************************************************//**
*
***************************************************************************/
void clSDS_Method_NaviShowNavMenu::onShowAdjustCurrentLocationScreenResponse(
   const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/,
   const ::boost::shared_ptr< ShowAdjustCurrentLocationScreenResponse >& /*response*/)
{
}
