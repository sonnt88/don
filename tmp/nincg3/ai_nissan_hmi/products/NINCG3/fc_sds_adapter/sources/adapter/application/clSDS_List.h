/*********************************************************************//**
 * \file       clSDS_List.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_List_h
#define clSDS_List_h

#define SYSTEM_S_IMPORT_INTERFACE_MAP
#define SYSTEM_S_IMPORT_INTERFACE_STRING
#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#include "stl_pif.h"


#define NUMBER_OF_ITEMS_PER_PAGE 5


#include "application/clSDS_ListItems.h"
#include "application/clSDS_ListInfoObserver.h"
#include "external/sds2hmi_fi.h"


struct stRange
{
   tU32 u32StartIndex;
   tU32 u32EndIndex;
};


class clSDS_List
{
   public:
      virtual ~clSDS_List();
      clSDS_List(tU8 u8NumberOfItemPerPage = NUMBER_OF_ITEMS_PER_PAGE);

      virtual tVoid vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType) = 0;
      virtual tU32 u32GetSize() = 0;
      virtual bpstl::vector<clSDS_ListItems> oGetItems(tU32 u32StartIndex, tU32 u32EndIndex) = 0;
      virtual tBool bSelectElement(tU32 u32SelectedIndex) = 0;
      virtual bpstl::string oGetSelectedItem(tU32 u32Index);
      virtual void setItems(bpstl::vector<clSDS_ListItems> items);
      virtual std::string oGetHMIListDescriptionItems(tU32 u32Index);
      virtual std::vector<sds2hmi_fi_tcl_HMIElementDescription> getHmiElementDescription(int index);

      tVoid vResetPageNumber();
      tVoid vIncrementPageNumber();
      tVoid vDecrementPageNumber();
      tU32 u32GetPageNumber() const;
      stRange stGetRangeOfPage();
      tU8 u8GetNumOfElementsOnPage();
      tBool bHasNextPage();
      tBool bHasPreviousPage() const;
      tVoid setListObserver(clSDS_ListInfoObserver* pListInfoObserver);
      tVoid notifyListObserver();

   private:
      tU8 _u8NumberOfItemPerPage;
      tU32 _u32PageNumber;
      tU32 u32PageStartIndex() const;
      tU32 u32PageEndIndex() const;
      clSDS_ListInfoObserver* _pListInfoObserver;
};


#endif
