/*********************************************************************//**
 * \file       clSDS_View.cpp
 *
 * clSDS_View class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_View.h"
#include "application/clSDS_ConfigurationFlags.h"
#include "application/clSDS_ListItems.h"
#include "application/clSDS_StringVarList.h"
#include "application/clSDS_SDSStatus.h"
#include "application/clSDS_PhoneNumberFormatter.h"
#include "application/StringUtils.h"
#include "view_db/Sds_ViewDB.h"


#define MAX_LINES_PER_VIEW  10
#define FOOTER_ELEMENT_START_INDEX 10


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_View::~clSDS_View()
{
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_View::clSDS_View(): _commandListCount(0)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_View::vCreateView(
   sds_gui_fi::PopUpService::PopupRequestSignal& viewPopupRequestSignal,
   Sds_ViewId enViewId,
   bpstl::vector<clSDS_ListItems>& oListItems,
   const bpstl::map<bpstl::string, bpstl::string>& oScreenVariableData,
   uint32 cursorIndex)
{
   vInitView(viewPopupRequestSignal);
   if (enViewId < SDS_VIEW_ID_LIMIT)
   {
      std::string layout;
      layout.push_back(Sds_ViewDB_cGetLayout(enViewId));
      viewPopupRequestSignal.setLayout(layout);
      if (Sds_ViewDB_oGetHelpline(enViewId).size())
      {
         viewPopupRequestSignal.getTextFieldsMutable().push_back(sds_gui_fi::PopUpService::TextField("help", Sds_ViewDB_oGetHelpline(enViewId), sds_gui_fi::PopUpService::TextAttribute__NORMAL));
      }
      if (Sds_ViewDB_oGetInfo(enViewId).size())
      {
         viewPopupRequestSignal.getTextFieldsMutable().push_back(sds_gui_fi::PopUpService::TextField("info", Sds_ViewDB_oGetInfo(enViewId), sds_gui_fi::PopUpService::TextAttribute__NORMAL));
      }

      bpstl::map<bpstl::string, bpstl::string> variables;
      variables = clSDS_ConfigurationFlags::getConfigurationFlags();
      variables.insert(oScreenVariableData.begin(), oScreenVariableData.end());

      vWriteCommands(viewPopupRequestSignal, variables, enViewId);
      vWriteSubCommands(viewPopupRequestSignal, variables, enViewId, cursorIndex);
      vWriteListData(viewPopupRequestSignal, oListItems);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_View::vCreateViewHeader(
   sds_gui_fi::PopUpService::PopupRequestSignal& viewPopupRequestSignal,
   Sds_HeaderTemplateId enTemplateId,
   const bpstl::map<bpstl::string, bpstl::string>& oHeaderValueMap,
   Sds_ViewId enViewId) const
{
   vWriteHeadline(viewPopupRequestSignal, enTemplateId, oHeaderValueMap, enViewId);
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_View::vWriteListData(
   sds_gui_fi::PopUpService::PopupRequestSignal& viewPopupRequestSignal,
   bpstl::vector<clSDS_ListItems>& oListItems) const
{
   const char* oCommandString[] = { "1.", "2.", "3.", "4.", "5.", "6.", "7.", "8.", "9.", "10." };

   ::std::vector< sds_gui_fi::PopUpService::TextField >& textFields(viewPopupRequestSignal.getTextFieldsMutable());

   for (tU32 u32Index = 0;
         (u32Index < oListItems.size() && bIsIndexWithInPage(u32Index));
         u32Index++)
   {
      const clSDS_ListItems& listItem = oListItems[u32Index];

      if (!(oListItems[u32Index].bIsListScreenWithoutIndex))
      {
         sds_gui_fi::PopUpService::TextField textFieldCommand;
         fillTextFieldData(textFieldCommand,
                           bpstl::string("cmd") + StringUtils::toString(u32Index),
                           oCommandString[u32Index],
                           colorToTextAttribute(listItem.oCommand.enTextColor));
         textFields.push_back(textFieldCommand);
      }
      else
      {
         if (listItem.oCommand.szString != "")
         {
            sds_gui_fi::PopUpService::TextField textFieldCommand;
            fillTextFieldData(textFieldCommand,
                              bpstl::string("cmd") + StringUtils::toString(u32Index),
                              listItem.oCommand.szString,
                              colorToTextAttribute(listItem.oCommand.enTextColor));
            textFields.push_back(textFieldCommand);
         }
      }

      if (listItem.oDescription.szString != "")
      {
         sds_gui_fi::PopUpService::TextField textFieldDescription;
         fillTextFieldData(textFieldDescription,
                           bpstl::string("descr")  + StringUtils::toString(u32Index),
                           listItem.oDescription.szString,
                           colorToTextAttribute(listItem.oDescription.enTextColor));
         textFields.push_back(textFieldDescription);
      }
      if (listItem.oDistance.szString != "")
      {
         sds_gui_fi::PopUpService::TextField textFieldDistance;
         fillTextFieldData(textFieldDistance,
                           bpstl::string("dist") + StringUtils::toString(u32Index),
                           listItem.oDistance.szString,
                           colorToTextAttribute(listItem.oDistance.enTextColor));
         textFields.push_back(textFieldDistance);
      }
      if (listItem.oDirectionSymbol.szString != "")
      {
         sds_gui_fi::PopUpService::TextField textFieldDirectionSymbol;
         fillTextFieldData(textFieldDirectionSymbol,
                           bpstl::string("dir") + StringUtils::toString(u32Index),
                           listItem.oDirectionSymbol.szString,
                           colorToTextAttribute(listItem.oDirectionSymbol.enTextColor));
         textFields.push_back(textFieldDirectionSymbol);
      }
      if (listItem.oPrice.szString != "")
      {
         sds_gui_fi::PopUpService::TextField textFieldPrice;
         fillTextFieldData(textFieldPrice,
                           bpstl::string("price") + StringUtils::toString(u32Index),
                           listItem.oPrice.szString,
                           colorToTextAttribute(listItem.oPrice.enTextColor));
         textFields.push_back(textFieldPrice);
      }
      if (listItem.oLastPriceUpdate.szString != "")
      {
         sds_gui_fi::PopUpService::TextField textFieldLastPriceUpdate;
         fillTextFieldData(textFieldLastPriceUpdate,
                           bpstl::string("priceU") + StringUtils::toString(u32Index),
                           listItem.oLastPriceUpdate.szString,
                           colorToTextAttribute(listItem.oLastPriceUpdate.enTextColor));
         textFields.push_back(textFieldLastPriceUpdate);
      }
   }
}


tVoid clSDS_View::fillTextFieldData(
   sds_gui_fi::PopUpService::TextField& textField,
   bpstl::string tagName,
   bpstl::string value,
   sds_gui_fi::PopUpService::TextAttribute colorOfText) const
{
   textField.setTagName(tagName);
   textField.setString(value);
   textField.setAttrib(colorOfText);
}


/***********************************************************************//**
 *
 ***************************************************************************/
sds_gui_fi::PopUpService::TextAttribute clSDS_View::colorToTextAttribute(clSDS_ListItems::tenColorofText colorOfText) const
{
   sds_gui_fi::PopUpService::TextAttribute textAttrib;

   switch (colorOfText)
   {
      case clSDS_ListItems::NORMAL:
         textAttrib = sds_gui_fi::PopUpService::TextAttribute__NORMAL;
         break;

      case clSDS_ListItems::COMMAND:
         textAttrib = sds_gui_fi::PopUpService::TextAttribute__COMMAND;
         break;

      case clSDS_ListItems::GREYED_OUT:
         textAttrib = sds_gui_fi::PopUpService::TextAttribute__GREYED_OUT;
         break;

      case clSDS_ListItems::PRICE_AGE_CURRENT:
         textAttrib = sds_gui_fi::PopUpService::TextAttribute__AGE_ONE_DAY;
         break;

      case clSDS_ListItems::PRICE_AGE_1_DAY:
         textAttrib = sds_gui_fi::PopUpService::TextAttribute__AGE_TWO_DAYS;
         break;

      case clSDS_ListItems::PRICE_AGE_3_DAYS_PLUS:
         textAttrib = sds_gui_fi::PopUpService::TextAttribute__AGE_THREE_PLUS_DAYS;
         break;

      default:
         textAttrib = sds_gui_fi::PopUpService::TextAttribute__NORMAL;
         break;
   }

   return textAttrib;
}


/***********************************************************************//**
 *
 ***************************************************************************/
tBool clSDS_View::bIsIndexWithInPage(tU32 u32NumOfListItems) const
{
   if (u32NumOfListItems > MAX_LINES_PER_VIEW)
   {
      return FALSE;
   }
   return TRUE;
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_View::vWriteHeadline(
   sds_gui_fi::PopUpService::PopupRequestSignal& viewPopupRequestSignal,
   Sds_HeaderTemplateId enTemplateId,
   bpstl::map<bpstl::string, bpstl::string> const& oHeaderValueMap,
   Sds_ViewId enViewId) const
{
   bpstl::string oHeaderString;

   if (enTemplateId != SDS_HEADER_TEMPLATE_ID_LIMIT)
   {
      oHeaderString = Sds_ViewDB_oGetTemplateString(enTemplateId);
   }
   else  // for manual mode always a template ID is expected.
   {
      oHeaderString = Sds_ViewDB_oGetHeadline(enViewId);
   }
   viewPopupRequestSignal.setHeader(oResolveVariableContent(oHeaderString, oHeaderValueMap).c_str());
}


/***********************************************************************//**
 *
 ***************************************************************************/
bpstl::string clSDS_View::oResolveVariableContent(
   bpstl::string oHeaderString,
   bpstl::map<bpstl::string, bpstl::string> const& headerVars) const
{
   bpstl::map<bpstl::string, bpstl::string>::const_iterator iter;
   for (iter = headerVars.begin(); iter != headerVars.end(); ++iter)
   {
      bpstl::string oStringToReplace = oMakeVariableString(iter->first);
      if (oHeaderString.find(oStringToReplace) != std::string::npos)
      {
         oHeaderString.replace(oHeaderString.find(oStringToReplace), oStringToReplace.size(), iter->second);
      }
   }
   oHeaderString = clSDS_StringVarList::oResolveVariables(oHeaderString);
   return oResolveNumberFormat(oHeaderString);
}


/***********************************************************************//**
 *
 ***************************************************************************/
bpstl::string clSDS_View::oResolveNumberFormat(const bpstl::string& oTemplateString) const
{
   bpstl::string startTag = "#FormatNumber(";
   bpstl::string::size_type startTagPos = oTemplateString.find(startTag);
   if (startTagPos != bpstl::string::npos)
   {
      bpstl::string::size_type endTagPos = oTemplateString.find(")", startTagPos);
      if (endTagPos != bpstl::string::npos)
      {
         bpstl::string::size_type numStart = startTagPos + startTag.size();
         bpstl::string::size_type numLen = endTagPos - (startTagPos + startTag.size());
         bpstl::string oNumber = oTemplateString.substr(numStart, numLen);
         bpstl::string oFormattedNumber = clSDS_PhoneNumberFormatter::oFormatNumber(oNumber);
         return oTemplateString.substr(0, startTagPos) + oFormattedNumber + oTemplateString.substr(endTagPos + 1);
      }
   }
   return oTemplateString;
}


/***********************************************************************//**
 *
 ***************************************************************************/
bpstl::string clSDS_View::oMakeVariableString(bpstl::string const& oString) const
{
   return "$(" + oString + ")";
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_View::vWriteCommands(sds_gui_fi::PopUpService::PopupRequestSignal& viewPopupRequestSignal, bpstl::map<bpstl::string, bpstl::string> const& variables, Sds_ViewId enViewId)
{
   bpstl::vector<SelectableText> oCommandList = Sds_ViewDB_oGetCommandList(variables, enViewId);
   ::std::vector< sds_gui_fi::PopUpService::TextField >& oTextFields(viewPopupRequestSignal.getTextFieldsMutable());

   tU32 u32LoopCount = (oCommandList.size() > FOOTER_ELEMENT_START_INDEX) ? FOOTER_ELEMENT_START_INDEX : oCommandList.size();
   _commandListCount = u32LoopCount;
   for (tU32 u32Index = 0; u32Index < u32LoopCount; u32Index++)
   {
      bpstl::string resolved = clSDS_StringVarList::oResolveVariables(oCommandList[u32Index].text);
      if (resolved.empty())
      {
         continue;
      }
      bpstl::string cmdTagName = bpstl::string("cmd") + StringUtils::toString(u32Index);

      clSDS_ListItems::tenColorofText attrib = clSDS_ListItems::COMMAND;
      if (!oCommandList[u32Index].isSelectable)
      {
         attrib = clSDS_ListItems::GREYED_OUT;
      }

      sds_gui_fi::PopUpService::TextField otextField;
      otextField.setTagName(cmdTagName);
      otextField.setString(resolved);
      otextField.setAttrib(colorToTextAttribute(attrib));
      oTextFields.push_back(otextField);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_View::vWriteSubCommands(sds_gui_fi::PopUpService::PopupRequestSignal& viewPopupRequestSignal, bpstl::map<bpstl::string, bpstl::string> const& variables, Sds_ViewId viewId, tU32 cursorIndex) const
{
   bpstl::vector<bpstl::string> subCommandList = Sds_ViewDB_oGetSubCommandList(variables, viewId, cursorIndex);
   ::std::vector< sds_gui_fi::PopUpService::TextField >& textFields = viewPopupRequestSignal.getTextFieldsMutable();

   for (size_t i = 0 ; i < subCommandList.size(); i++)
   {
      bpstl::string resolved = clSDS_StringVarList::oResolveVariables(subCommandList[i]);
      bpstl::string cmdTagName = bpstl::string("subCmd") + StringUtils::toString(i);

      sds_gui_fi::PopUpService::TextField textField;
      textField.setTagName(cmdTagName);
      textField.setString(resolved);
      textField.setAttrib(colorToTextAttribute(clSDS_ListItems::NORMAL));
      textFields.push_back(textField);
   }
}


/***********************************************************************//**
 *
 ***************************************************************************/
tVoid clSDS_View::vInitView(sds_gui_fi::PopUpService::PopupRequestSignal& viewPopupRequestSignal) const
{
   viewPopupRequestSignal.clearHeader();
   viewPopupRequestSignal.clearLayout();
   viewPopupRequestSignal.clearTextFields();
}


/***********************************************************************//**
 *
 ***************************************************************************/
unsigned int clSDS_View::getCommandListCount() const
{
   return _commandListCount;
}
