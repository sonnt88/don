/*********************************************************************//**
 * \file       SdsAudioSource.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/SdsAudioSource.h"
#include "application/SdsAudioSourceObserver.h"
#include "application/clSDS_EcnrHandler.h"
#include "application/clSDS_AudprocHandler.h"
#include "application/PopUpService.h"


SdsAudioSource::SdsAudioSource(
   ahl_tclBaseOneThreadApp* ahlApp,
   ::boost::shared_ptr< org::bosch::ecnr::service::ServiceProxy > ecnrProxy,
   ::boost::shared_ptr< org::bosch::audproc::service::ServiceProxy > audprocProxy,
   PopUpService* popUpService)

   : arl_tclISource_ASF(ahlApp)
   , _pEcnrHandler(new clSDS_EcnrHandler(ecnrProxy))
   , _pAudprocHandler(new clSDS_AudprocHandler(audprocProxy, popUpService))
{
}


SdsAudioSource::~SdsAudioSource()
{
   try
   {
      delete _pEcnrHandler;
      _pEcnrHandler = NULL;

      delete _pAudprocHandler;
      _pAudprocHandler = NULL;
   }
   catch (...)
   {
   }
}


tBool SdsAudioSource::bOnSrcActivity(arl_tenSource enSrcNum,  tU16 /*SubSource*/, const arl_tSrcActivity& rfcoSrcActivity)
{
   if (ARL_SRC_VRU != enSrcNum)
   {
      return FALSE;
   }

   arl_tenActivity arlSourceActivity = midwToArlActivityValue(rfcoSrcActivity.enType);

   if (_earlyPlaybackRequest == false //no need for ecnr and audproc handling for early playback case
         && NULL != _pEcnrHandler
         && NULL != _pAudprocHandler)
   {
      if (rfcoSrcActivity.enType == midw_fi_tcl_e8_SrcActivity::FI_EN_ON)
      {
         _pEcnrHandler->startAudio();
         _pAudprocHandler->startAudio();
      }
      else if (rfcoSrcActivity.enType == midw_fi_tcl_e8_SrcActivity::FI_EN_OFF)
      {
         _pEcnrHandler->stopAudio();
         _pAudprocHandler->stopAudio();
      }
      else if (rfcoSrcActivity.enType == midw_fi_tcl_e8_SrcActivity::FI_EN_PAUSE)
      {
         _pEcnrHandler->stopAudio();
         _pAudprocHandler->stopAudio();
      }
   }

   vSourceActivityResult(enSrcNum, arlSourceActivity);
   notifyObservers(arlSourceActivity);

   return TRUE;
}


tBool SdsAudioSource::bOnAllocate(arl_tenSource enSrcNum, const arl_tAllocRouteResult& /*rfcoAllocRoute*/)
{
   if (ARL_SRC_VRU == enSrcNum
         && NULL != _pEcnrHandler
         && NULL != _pAudprocHandler
         && _earlyPlaybackRequest == false)
   {
      _pEcnrHandler->initialize();
      _pAudprocHandler->initialize();
   }

   return TRUE;
}


tBool SdsAudioSource::bOnDeAllocate(arl_tenSource enSrcNum)
{
   if (ARL_SRC_VRU == enSrcNum
         && NULL != _pEcnrHandler
         && NULL != _pAudprocHandler
         && _earlyPlaybackRequest == false)
   {
      _pEcnrHandler->destroy();
      _pAudprocHandler->destroy();
   }

   return TRUE;
}


tBool SdsAudioSource::bOnMuteState(arl_tenSource /*enSrcNum*/, arl_tenMuteState /*muteState*/)
{
   return TRUE;
}


tVoid SdsAudioSource::addObserver(SdsAudioSourceObserver* observer)
{
   if (observer != NULL)
   {
      _audioObservers.push_back(observer);
   }
}


tVoid SdsAudioSource::notifyObservers(arl_tenActivity state)
{
   bpstl::vector<SdsAudioSourceObserver*>::iterator iter = _audioObservers.begin();
   while (iter != _audioObservers.end())
   {
      if (*iter != NULL)
      {
         (*iter)->onAudioSourceStateChanged(state);
      }
      ++iter;
   }
}


tVoid SdsAudioSource::sendAudioRouteRequest(arl_tenSource enSource, arl_tenActivity enRequest, bool earlyPlaybackRequest)
{
   _earlyPlaybackRequest = earlyPlaybackRequest;

   bSetAudioRouteRequest(enSource, enRequest);
}


arl_tenActivity SdsAudioSource::midwToArlActivityValue(midw_fi_tcl_e8_SrcActivity::tenType srcActivity)
{
   if (srcActivity == midw_fi_tcl_e8_SrcActivity::FI_EN_ON)
   {
      return ARL_EN_ISRC_ACT_ON;
   }

   if (srcActivity == midw_fi_tcl_e8_SrcActivity::FI_EN_OFF)
   {
      return ARL_EN_ISRC_ACT_OFF;
   }

   if (srcActivity == midw_fi_tcl_e8_SrcActivity::FI_EN_PAUSE)
   {
      return ARL_EN_ISRC_ACT_PAUSE;
   }

   return ARL_EN_ISRC_ACT_OFF;
}
