/*********************************************************************//**
 * \file       clSDS_NaviListItems.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_NaviListItems_h
#define clSDS_NaviListItems_h


#include "external/sds2hmi_fi.h"

#define SYSTEM_S_IMPORT_INTERFACE_MAP
#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"


class clSDS_NaviList;


class clSDS_NaviListItems
{
   public:
      virtual ~ clSDS_NaviListItems();
      clSDS_NaviListItems();
      enum enNaviListType
      {
         EN_NAVI_LIST_OSDE_ADDRESSES,
         EN_NAVI_LIST_HOME_DESTINATION,
         EN_NAVI_LIST_WORK_DESTINATION,
         EN_NAVI_LIST_PREV_DESTINATIONS,
         EN_NAVI_LIST_FUEL_PRICES,
         EN_NAVI_LIST__ADDRESSBOOK,
         EN_NAVI_LIST_UNKNOWN
      };
      void vSetNaviHMIListType(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType);
      void vSetNaviDestListType(sds2hmi_fi_tcl_e8_NAV_ListType::tenType listType);
      void vSetNaviSelectionCriterionListType(sds2hmi_fi_tcl_e16_SelectionCriterionType::tenType listType);
      enNaviListType vGetNaviListType() const;
      void vResetNaviListType();
      void vStartGuidance();
      void vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::tenType lNaviListType, clSDS_NaviList* lNaviList);
      void vAddasWayPoint();

   private:
      clSDS_NaviList* _pNaviList;
      enNaviListType _enCurrentNaviListType;
      std::map<sds2hmi_fi_tcl_e8_HMI_ListType::tenType, clSDS_NaviList*> _oNaviListMap;
};


#endif
