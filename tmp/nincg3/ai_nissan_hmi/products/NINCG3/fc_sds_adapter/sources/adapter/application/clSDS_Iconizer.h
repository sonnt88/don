/*********************************************************************//**
 * \file       clSDS_Iconizer.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Iconizer_h
#define clSDS_Iconizer_h


#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"


class clSDS_Iconizer
{
   public:

      enum en_SDS_callList_PhoneType
      {
         PHONENUMBER_TYPE_UNKNOWN = 0UL,
         PHONENUMBER_TYPE_GENERAL = 1UL,
         PHONENUMBER_TYPE_MOBILE = 2UL,
         PHONENUMBER_TYPE_OFFICE = 3UL,
         PHONENUMBER_TYPE_HOME = 4UL,
         PHONENUMBER_TYPE_FAX = 5UL,
         PHONENUMBER_TYPE_PAGER = 6UL,
         PHONENUMBER_TYPE_CAR = 7UL,
         PHONENUMBER_TYPE_SIM = 8UL
      };

      virtual ~clSDS_Iconizer();
      clSDS_Iconizer();

      static bpstl::string oAddIconPrefix(tU8 u8IconType, const bpstl::string& oNumber);
      static bpstl::string oAddIconInfix(const bpstl::string& oName, tU8 u8IconType, const bpstl::string& oNumber);
      static bpstl::string oRemoveIconPrefix(const bpstl::string& oNumber);

   private:

      static bpstl::string oGetIconTypeString(tU8 u8IconType);
};


#endif
