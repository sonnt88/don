/*********************************************************************//**
 * \file       clSDS_Property_NaviCurrentCountryState.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Property_NaviCurrentCountryState_h
#define clSDS_Property_NaviCurrentCountryState_h


#include "external/sds2hmi_fi.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"
#include "Sds2HmiServer/framework/clServerProperty.h"


class clSDS_Property_NaviCurrentCountryState
   : public clServerProperty
   , public asf::core::ServiceAvailableIF
   , public org::bosch::cm::navigation::NavigationService::PositionInformationCallbackIF
{
   public:
      virtual ~clSDS_Property_NaviCurrentCountryState();
      clSDS_Property_NaviCurrentCountryState(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > pNaviProxy);

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onPositionInformationError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::PositionInformationError >& error);
      virtual void onPositionInformationUpdate(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::PositionInformationUpdate >& update);

      static sds2hmi_fi_tcl_e16_ISOCountryCode::tenType convertNaviStringtoISOCountryCode(std::string countrycode);

   protected:
      virtual void vGet(amt_tclServiceData* pInMsg);
      virtual void vSet(amt_tclServiceData* pInMsg);
      virtual void vUpreg(amt_tclServiceData* pInMsg);

   private:
      //void vRemoveCountryTag(bpstl::string& oString);
      void vSendCountryStateStatus();

      std::string _naviCountryCode;
      std::string _countryName;
      std::string _statecode;
      std::string _stateName;
      std::string _cityName;
      boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > _naviProxy;
};


#endif
