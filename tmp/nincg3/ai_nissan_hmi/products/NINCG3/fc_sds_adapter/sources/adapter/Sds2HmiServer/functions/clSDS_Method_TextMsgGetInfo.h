/*********************************************************************//**
 * \file       clSDS_Method_TextMsgGetInfo.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_TextMsgGetInfo_h
#define clSDS_Method_TextMsgGetInfo_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"

#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"

class clSDS_ReadSmsList;
class clSDS_TextMsgContent;
class clSDS_Method_TextMsgGetInfo : public clServerMethod
{
   public:
      virtual ~clSDS_Method_TextMsgGetInfo();
      clSDS_Method_TextMsgGetInfo(ahl_tclBaseOneThreadService* pService, clSDS_ReadSmsList* pSmsList);

   private:

      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      tVoid vGetCurrentSmsInformation(sds2hmi_sdsfi_tclMsgTextMsgGetInfoMethodResult& oMessage) const;
      tBool bMoreThanOneSmsAvailable() const;
      tBool bCurrentSmsIsFirstSms() const;
      tBool bCurrentSmsIsLastSms() const;
      tVoid vGetCurrentSmsDetails(bpstl::string& oString) const;
      tVoid vGetCurrentSmsIndex(bpstl::string& oString) const;
      tVoid vSaveCurrentSmsNumberForCallBackAndSendText() const;
      tVoid vTraceMethodResult(const sds2hmi_sdsfi_tclMsgTextMsgGetInfoMethodResult& oMessage) const;

      clSDS_ReadSmsList* _pReadSmsList;
};


#endif
