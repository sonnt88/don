/*********************************************************************//**
 * \file       clSDS_Method_CommonSetAvailableSpeakers.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_CommonSetAvailableSpeakers_h
#define clSDS_Method_CommonSetAvailableSpeakers_h


#include "Sds2HmiServer/framework/clServerMethod.h"


class clSDS_LanguageMediator;


class clSDS_Method_CommonSetAvailableSpeakers : public clServerMethod
{
   public:
      virtual ~clSDS_Method_CommonSetAvailableSpeakers();
      clSDS_Method_CommonSetAvailableSpeakers(ahl_tclBaseOneThreadService* pService, clSDS_LanguageMediator* pLanguageMediator);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);

      clSDS_LanguageMediator* _pLanguageMediator;
};


#endif
