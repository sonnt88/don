/*********************************************************************//**
 * \file       clSDS_Method_MediaPlay.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_MediaPlay_h
#define clSDS_Method_MediaPlay_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "bosch/cm/ai/hmi/masteraudioservice/AudioSourceChangeProxy.h"
#include "mplay_MediaPlayer_FIProxy.h"
#include "application/GuiService.h"


#define MPLAY_MAX_DEVICES 11


class clSDS_Method_MediaGetAmbiguityList;
class clSDS_Method_MediaGetDeviceInfo;


class clSDS_Method_MediaPlay
   : public clServerMethod
   , public asf::core::ServiceAvailableIF
   , public bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::GetSourceListCallbackIF
   , public bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::ActivateSourceCallbackIF
   , public mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsCallbackIF
   , public bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::SourceListChangedCallbackIF
   , public	mplay_MediaPlayer_FI:: PlayMediaPlayerObjectCallbackIF
   , public mplay_MediaPlayer_FI::PlayItemFromListCallbackIF
   , public mplay_MediaPlayer_FI::GetMediaObjectCallbackIF

{
   public:
      virtual ~clSDS_Method_MediaPlay();
      clSDS_Method_MediaPlay(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::AudioSourceChangeProxy > poaudioSourceChangeProxy,
         ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy > pSdsMediaProxy,
         clSDS_Method_MediaGetAmbiguityList* pMediaGetAmbiguityList,
         GuiService& guiService,
         clSDS_Method_MediaGetDeviceInfo* mediaGetDeviceInfo);

      tVoid vMediaPlayStatus(tBool bResult);
      tVoid vMediaObjectValidationResponse(tBool bResult);
      sds2hmi_fi_tcl_e8_MPL_SourceType::tenType enGetHmiSourceType(tU32 u32SdsSourceId);

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onGetSourceListError(
         const ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::AudioSourceChangeProxy >& proxy,
         const ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::GetSourceListError >& error);

      virtual void onGetSourceListResponse(
         const ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::AudioSourceChangeProxy >& proxy,
         const ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::GetSourceListResponse >& response);

      virtual void onActivateSourceError(
         const ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::AudioSourceChangeProxy >& proxy,
         const ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::ActivateSourceError >& error);

      virtual void onActivateSourceResponse(
         const ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::AudioSourceChangeProxy >& proxy,
         const ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::ActivateSourceResponse >& response);

      virtual void onMediaPlayerDeviceConnectionsStatus(
         const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
         const boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsStatus >& status);

      virtual void onMediaPlayerDeviceConnectionsError(
         const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
         const boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsError >& error);

      virtual void onSourceListChangedError(
         const ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::AudioSourceChangeProxy >& proxy,
         const ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::SourceListChangedError >& error);

      virtual void onSourceListChangedSignal(
         const ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::AudioSourceChangeProxy >& proxy,
         const ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::SourceListChangedSignal >& signal);

      virtual void onPlayMediaPlayerObjectError(
         const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
         const ::boost::shared_ptr< mplay_MediaPlayer_FI::PlayMediaPlayerObjectError >& error);

      virtual void onPlayMediaPlayerObjectResult(
         const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
         const ::boost::shared_ptr< mplay_MediaPlayer_FI::PlayMediaPlayerObjectResult >& result);

      virtual void onPlayItemFromListError(
         const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
         const ::boost::shared_ptr< mplay_MediaPlayer_FI::PlayItemFromListError >& error);

      virtual void onPlayItemFromListResult(
         const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
         const ::boost::shared_ptr< mplay_MediaPlayer_FI::PlayItemFromListResult >& result);

      virtual void requestSourceActivation(int32 SrcID, int32 SubSrcID);

      virtual void onGetMediaObjectError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
                                         const ::boost::shared_ptr< mplay_MediaPlayer_FI::GetMediaObjectError >& error);

      virtual void onGetMediaObjectResult(const ::boost::shared_ptr<mplay_MediaPlayer_FI:: Mplay_MediaPlayer_FIProxy >& proxy,
                                          const ::boost::shared_ptr< mplay_MediaPlayer_FI::GetMediaObjectResult >& result);

   private:
      // TODO jnd2hi: try to remove this enum entirely
      // originally 'enAudioSources::enAudioSources' from legacy header AudioSources.h
      // moved here, renamed, unused values removed
      enum AudioSource
      {
         NONE = 0,
         MEDIA_IPOD = 28,
         MEDIA_DB_PLAYER = 45
      };

      struct SourceDetailInfo
      {
         int32 sourceType; //The type of source as Mediaplayer type
         int32 sourceID;
         int32 subSourceID;   // Device ID for activation, in case of CDDA ID is 0
      };

      struct trDevice
      {
         bool bIsConnected;
         unsigned int uiDeviceTag;
         unsigned int uiDeviceType;
      };

      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      tVoid vPlaySource(tU32 u32SdsSourceId);
      tVoid vCdPlayByTrack(tU32 u32TrackId);
      tVoid vProcessMessage(const sds2hmi_sdsfi_tclMsgMediaPlayMethodStart& oMessage);
      tVoid vProcessMediaRequests(const sds2hmi_sdsfi_tclMsgMediaPlayMethodStart& oMessage);
      void activateSource(tU32 u32Index);
      AudioSource enGetMediaAudioSource();

      boost::shared_ptr<mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy> _mediaPlayerProxy;
      boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::AudioSourceChangeProxy > _audioSourceChangeProxy;
      GuiService& _guiService;
      clSDS_Method_MediaGetAmbiguityList* _pMediaGetAmbiguityList;
      clSDS_Method_MediaGetDeviceInfo* _pMediaGetDeviceInfo;
      std::vector< SourceDetailInfo > _sourcelist;
      SourceDetailInfo _sourceInfoArray[10]; // TODO jnd2hi: replace array with vector
      trDevice _oDeviceList[MPLAY_MAX_DEVICES]; // TODO jnd2hi: replace array with vector
      int _count;
      tU32 _sourceListSize;
      tU32 _u32MediaId;
      unsigned int _tagID;
      MPlay_fi_types::T_e8_MPlayDeviceType _e8DeviceType;
};


#endif
