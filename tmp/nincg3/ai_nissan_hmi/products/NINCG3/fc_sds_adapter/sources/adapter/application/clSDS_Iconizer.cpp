/*********************************************************************//**
 * \file       clSDS_Iconizer.cpp
 *
 * clSDS_Iconizer class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_Iconizer.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Iconizer::~clSDS_Iconizer()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Iconizer::clSDS_Iconizer()
{
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::string clSDS_Iconizer::oAddIconPrefix(tU8 u8IconType, const bpstl::string& oNumber)
{
   return oGetIconTypeString(u8IconType) + " " + oNumber;
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::string clSDS_Iconizer::oAddIconInfix(const bpstl::string& oName, tU8 u8IconType, const bpstl::string& oNumber)
{
   return oName + " " + oGetIconTypeString(u8IconType) + " " + oNumber;
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::string clSDS_Iconizer::oRemoveIconPrefix(const bpstl::string& oNumber)
{
   if ((oNumber.size() > 4) &&
         (oNumber[0] == '\xEF') &&
         (oNumber[1] == '\xA2'))
   {
      return oNumber.substr(4);
   }
   return oNumber;
}


/***********************************************************************//**
*
***************************************************************************/
bpstl::string clSDS_Iconizer::oGetIconTypeString(tU8 u8IconType)
{
   switch (u8IconType)
   {
      case PHONENUMBER_TYPE_GENERAL:
         return "\xEF\xA2\x94";

      case PHONENUMBER_TYPE_MOBILE:
         return "\xEF\xA2\x95";

      case PHONENUMBER_TYPE_OFFICE:
         return "\xEF\xA2\x97";

      case PHONENUMBER_TYPE_HOME:
         return "\xEF\xA2\x9C";

      case PHONENUMBER_TYPE_FAX:
         return "\xEF\xA2\x93";

      case PHONENUMBER_TYPE_PAGER:
         return "\xEF\xA2\x98";

      case PHONENUMBER_TYPE_CAR:
         return "\xEF\xA2\x92";

      case PHONENUMBER_TYPE_SIM:
         return "\xEF\xA2\x9A";

      case PHONENUMBER_TYPE_UNKNOWN:
      default:
         return "\xEF\xA2\x91";
   }
}
