#ifndef SDSHMI_TRACE_H
#define SDSHMI_TRACE_H


#define ETG_S_IMPORT_INTERFACE_GENERIC
#include "etg_if.h"


// trace classes based on TR_COMP_SAAL (256 * 70) = 0x4600 - see mc_trace.h

#define TR_CLASS_SDSADP_DETAILS    0x4600
#define TR_CLASS_SDSADP_ASF        0x4601
#define TR_CLASS_CCA_FW            0x460F


#endif
