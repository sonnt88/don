/*********************************************************************//**
 * \file       clSDS_MyAppsList.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_MyAppsList_h
#define clSDS_MyAppsList_h


#define SYSTEM_S_IMPORT_INTERFACE_STRING
#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#include "stl_pif.h"


#include "application/clSDS_List.h"
#include "external/sds2hmi_fi.h"
#include "application/clSDS_MyAppsDataBase.h"
#include "application/clSDS_MyAppsDataBaseObserver.h"


class clSDS_MyAppsDataBase;


class clSDS_MyAppsList : public clSDS_List, public clSDS_MyAppsDataBaseObserver
{
   public:
      virtual ~clSDS_MyAppsList();
      clSDS_MyAppsList(clSDS_MyAppsDataBase* pMyAppsDataBase);
      tU32 u32GetSize();
      bpstl::vector<clSDS_ListItems> oGetItems(tU32 u32StartIndex, tU32 u32EndIndex);
      tBool bSelectElement(tU32 u32SelectedIndex);
      tVoid vMenuLanguageChanged(const sds2hmi_fi_tcl_SDSLanguageID& oMenuLanguage);
      tVoid vMyAppsDataBaseStatusChange();
      virtual tVoid vMyAppsListAvailable();

   private:
      clSDS_ListItems oGetListItem(tU32 u32Index) const;
      tVoid vLoadMyAppsList();
      tVoid vClearMyAppsList() const;
      tVoid vUpdateCurrentLanguage();
      tVoid vFillMyAppsListDatapool(bpstl::vector<stMyappNames>& oMyAppsNameList);
      sds2hmi_fi_tcl_SDSLanguageID _oCurrentMenuLanguage;
      clSDS_MyAppsDataBase* _pMyAppsDataBase;
      tBool _bIsMyAppsListAvailable;
};


#endif
