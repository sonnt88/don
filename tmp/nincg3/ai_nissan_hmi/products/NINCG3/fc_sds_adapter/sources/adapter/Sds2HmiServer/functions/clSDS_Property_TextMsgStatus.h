/*********************************************************************//**
 * \file       clSDS_Property_TextMsgStatus.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Property_TextMsgStatus_h
#define clSDS_Property_TextMsgStatus_h


#include "Sds2HmiServer/framework/clServerProperty.h"
#include "external/sds2hmi_fi.h"
#include "MOST_Msg_FIProxy.h"
#include "application/clSDS_ReadTextListObserver.h"


class clSDS_Property_TextMsgStatus
   : public clServerProperty
   , public asf::core::ServiceAvailableIF
   , public MOST_Msg_FI::MapDeviceCapabilitiesCallbackIF
   , public clSDS_ReadTextListObserver
{
   public:
      virtual ~clSDS_Property_TextMsgStatus();
      clSDS_Property_TextMsgStatus(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< ::MOST_Msg_FI::MOST_Msg_FIProxy > msgProxy);

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onMapDeviceCapabilitiesError(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::MapDeviceCapabilitiesError >& error);
      virtual void onMapDeviceCapabilitiesStatus(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::MapDeviceCapabilitiesStatus >& status);

      virtual void textListChanged(unsigned int size);

   protected:
      virtual tVoid vGet(amt_tclServiceData* pInMsg);
      virtual tVoid vSet(amt_tclServiceData* pInMsg);
      virtual tVoid vUpreg(amt_tclServiceData* pInMsg);

   private:
      sds2hmi_fi_tcl_e8_SMS_Status::tenType getSMSProfileStatus(uint16 messageTypes, uint16 mapFeatures) const;
      tVoid vSendTextMsgStatus();

      boost::shared_ptr< ::MOST_Msg_FI::MOST_Msg_FIProxy > _msgProxy;
      sds2hmi_fi_tcl_e8_SMS_Status _smsStatus;
      tBool _messagesAvailable;
};


#endif
