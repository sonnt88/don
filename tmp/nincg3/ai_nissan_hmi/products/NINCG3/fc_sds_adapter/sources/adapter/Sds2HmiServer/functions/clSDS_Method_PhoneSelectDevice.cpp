/*********************************************************************//**
 * \file       clSDS_Method_PhoneSelectDevice.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_PhoneSelectDevice.h"
#include "external/sds2hmi_fi.h"
#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_PhoneSelectDevice.cpp.trc.h"
#endif


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_PhoneSelectDevice::~clSDS_Method_PhoneSelectDevice()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_PhoneSelectDevice::clSDS_Method_PhoneSelectDevice(ahl_tclBaseOneThreadService* pService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_PHONESELECTPHONE, pService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_PhoneSelectDevice::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgPhoneSelectPhoneMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   ETG_TRACE_FATAL(("unsupported request to method PhoneSelectDevice"));
   vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
}
