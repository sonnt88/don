/*********************************************************************//**
 * \file       clSDS_Method_MediaGetAmbiguityList.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_MediaGetAmbiguityList_h
#define clSDS_Method_MediaGetAmbiguityList_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "mplay_MediaPlayer_FIProxy.h"


class clSDS_Method_MediaGetAmbiguityList
   : public clServerMethod
   , public mplay_MediaPlayer_FI::GetMediaObjectCallbackIF
{
   public:
      virtual ~clSDS_Method_MediaGetAmbiguityList();
      clSDS_Method_MediaGetAmbiguityList(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy > mediaPlayerProxy);

      unsigned int vGetTagID() const;
      MPlay_fi_types::T_e8_MPlayDeviceType vGetE8DeviceType() const;

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);

      virtual void onGetMediaObjectError(
         const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
         const ::boost::shared_ptr< mplay_MediaPlayer_FI::GetMediaObjectError >& error);
      virtual void onGetMediaObjectResult(
         const ::boost::shared_ptr<mplay_MediaPlayer_FI:: Mplay_MediaPlayer_FIProxy >& proxy,
         const ::boost::shared_ptr< mplay_MediaPlayer_FI::GetMediaObjectResult >& result);

      tVoid vRequestMediaMetadata();
      tVoid vSendResult();
      tVoid vProcessMessageByCategory(MPlay_fi_types::T_e8_MPlayCategoryType category, tBool bIsMediaObjectValid);
      tVoid vProcessSongData();
      tVoid vProcessAlbumData();
      tVoid vProcessArtistData();
      tVoid vStoreAmbigInfo(const bpstl::string& oMediaInfo);
      tVoid vTraceMediaAmbiguityResult();

      boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy > _mediaPlayerProxy;
      MPlay_fi_types::T_e8_MPlayDeviceType _e8DeviceType;
      bpstl::vector<sds2hmi_fi_tcl_TypedMediaID> _mediaIds;
      bpstl::vector<sds2hmi_fi_tcl_MediaIDString> _stringIdPairs;
      std::string _artist;
      std::string _album;
      std::string _song;
      unsigned int _tagID;
};


#endif
