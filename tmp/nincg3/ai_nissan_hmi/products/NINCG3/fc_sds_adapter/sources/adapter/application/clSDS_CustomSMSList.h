/*********************************************************************//**
 * \file       clSDS_CustomSMSList.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_CustomSMSList_h
#define clSDS_CustomSMSList_h

#include "application/clSDS_List.h"
#include "MOST_Msg_FIProxy.h"
#include "asf/core/Proxy.h"

class clSDS_CustomSMSList
   : public clSDS_List
   , public MOST_Msg_FI::PredefinedMessageListExtendedCallbackIF
   , public asf::core::ServiceAvailableIF
{
   public:
      virtual ~clSDS_CustomSMSList();
      clSDS_CustomSMSList(::boost::shared_ptr< ::MOST_Msg_FI::MOST_Msg_FIProxy > pSds2MsgProxy);
      tU32 u32GetSize();
      std::vector<clSDS_ListItems> oGetItems(tU32 u32StartIndex, tU32 u32EndIndex);
      tBool bSelectElement(tU32 u32SelectedIndex);

      virtual void onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                               const asf::core::ServiceStateChange& stateChange) ;

      virtual void onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                                 const asf::core::ServiceStateChange& stateChange) ;

      virtual void onPredefinedMessageListExtendedError(const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
            const ::boost::shared_ptr< MOST_Msg_FI::PredefinedMessageListExtendedError >& error);

      virtual void onPredefinedMessageListExtendedStatus(const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
            const ::boost::shared_ptr< MOST_Msg_FI::PredefinedMessageListExtendedStatus >& status);

   private:
      std::string oGetItem(tU32 u32Index);
      tVoid vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType);
      boost::shared_ptr< ::MOST_Msg_FI::MOST_Msg_FIProxy > _messageFIProxy;
      std::vector<std::string> _predefinedCustomMsgList;
};


#endif
