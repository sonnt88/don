/*********************************************************************//**
 * \file       clSDS_Method_NaviSetDestinationItem.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_NaviSetDestinationItem_h
#define clSDS_Method_NaviSetDestinationItem_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "asf/core/Proxy.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"
#include "application/clSDS_MultipleDestinationsList.h"
#include "application/clSDS_NaviAmbiguitylistObserver.h"
#include "application/clSDS_POIList.h"
#include "Sds2HmiServer/functions/clSDS_Method_CommonShowDialog.h"
#include "view_db/Sds_ViewDB.h"


using namespace org::bosch::cm::navigation::NavigationService;


class clSDS_MultipleDestinationsList;
class clSDS_POIList;
class clSDS_NaviListItems;
class clSDS_Method_CommonShowDialog;


class clSDS_Method_NaviSetDestinationItem
   : public clServerMethod
   , public clSDS_NaviAmbiguitylistObserver
   , public asf::core::ServiceAvailableIF
   , public SdsCheckAddressCallbackIF
   , public SetLocationWithSdsInputCallbackIF
   , public SdsAddressWithOptionsCallbackIF
   , public SelectSdsRefinementCallbackIF
   , public RequestFreeTextSearchResultsCallbackIF
   , public SetLocationWithFreeTextSearchInputCallbackIF
   , public DestinationInformationCallbackIF
{
   public:

      virtual ~clSDS_Method_NaviSetDestinationItem();

      clSDS_Method_NaviSetDestinationItem(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< NavigationServiceProxy> naviProxy,
         clSDS_MultipleDestinationsList* pMultipleDestinationList,
         clSDS_POIList* pPOIList,
         clSDS_NaviListItems* pNaviListItems,
         clSDS_Method_CommonShowDialog* pCommonShowDialog);

      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      // callbacks by SdsCheckAddressCallbackIF
      virtual void onSdsCheckAddressError(
         const ::boost::shared_ptr<NavigationServiceProxy>& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::SdsCheckAddressError >& error);
      virtual void onSdsCheckAddressResponse(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< SdsCheckAddressResponse >& sdsCheckAddressResponse);

      // SetLocationWithSdsInputCallbackIF
      virtual void onSetLocationWithSdsInputError(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< SetLocationWithSdsInputError >& error);
      virtual void onSetLocationWithSdsInputResponse(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< SetLocationWithSdsInputResponse >& response);

      // SdsAddressWithOptionsCallbackIF
      virtual void onSdsAddressWithOptionsUpdate(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< SdsAddressWithOptionsUpdate >& update);
      virtual void onSdsAddressWithOptionsError(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< SdsAddressWithOptionsError >& error);

      //SdsSelectRefinementCallbackIF
      virtual void onSelectSdsRefinementError(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< SelectSdsRefinementError >& error);
      virtual void onSelectSdsRefinementResponse(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< SelectSdsRefinementResponse >& response);

      //DestinationInformationCallbackIF
      virtual void onDestinationInformationError(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< DestinationInformationError >& error);
      virtual void onDestinationInformationUpdate(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< DestinationInformationUpdate >& update);

      //RequestFreeTextSearchResultsCallbackIF
      virtual void onRequestFreeTextSearchResultsError(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< RequestFreeTextSearchResultsError >& error);
      virtual void onRequestFreeTextSearchResultsResponse(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< RequestFreeTextSearchResultsResponse >& response);

      //SetLocationWithFreeTextSearchInputCallbackIF
      virtual void onSetLocationWithFreeTextSearchInputError(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< SetLocationWithFreeTextSearchInputError >& error);
      virtual void onSetLocationWithFreeTextSearchInputResponse(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< SetLocationWithFreeTextSearchInputResponse >& response);

      sds2hmi_fi_tcl_e16_SelectionCriterionType::tenType GetNavSelectionCriterionType() const;
      tVoid vGetAddressOptionResultFromNavi(const ::boost::shared_ptr< NavigationServiceProxy >& proxy);
      tVoid vRespondToPoiListReply();
      tVoid vRespondToPoiCityStateAmbiguity();
      tVoid vHouseNumberResolved();
      tVoid vNaviReplyForOSDEAddressSearch(tU32 u32NaviResult);
      void vAmbiguityListResolved();

   private:
      enum enResultType
      {
         ADDRESS_MULTIPLE_RESULT_FOR_HOUSENR_OUTRANGE,
         ADDRESS_NOHOUSENO_RESULT,
         ADDRESS_HOUSENO_OUTRANGE_RESULT,
         ADDRESS_MULTIPLE_RESULT,
         ADDRESS_RESULT,
         JUNCTION_RESULT,
         JUNCTION_MULTIPLE_RESULT,
         POI_MULTIPLE_RESULT,
         NO_RESULT
      };

      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      tVoid vSetNavSelectionCriterionType();
      tVoid vTraceMethodStartLocationDescription();
      tBool bIsOsdeRequest() const;
      tBool bIsPOISearchRequest() const;
      tVoid vAddressRequestToNavi();
      tVoid oGetOsdeStringFromDescriptor();
      tVoid oGetPOIStringFromDescriptor();
      tVoid vHandleHouseNrRequest(const std::string& oString);
      tVoid vHandlePOIRequest();
      std::string oGetHouseNrFromDescriptor() const;
      std::string oGetHouseNumberFromDescriptor(const std::string& oString) const;
      std::string oGetFirstStreetNameFromDescriptor(const std::string& oString) const;
      std::string oGetSecondStreetNameFromDescriptor(const std::string& oString) const;
      std::string oValidateFirstStreetName(const std::string& oString);
      std::string oValidateSecondStreetName(const std::string& oString);
      std::string oGetCityNameFromDescriptor(const std::string& oString) const;
      std::string oGetStateNameFromDescriptor(const std::string& oString) const;
      std::string oGetCountryNameFromDescriptor(const std::string& oString) const;
      std::string oGetCategoryNameFromDescriptor(const std::string& oString) const;
      std::string oGetBrandNameFromDescriptor(const std::string& oString) const;
      std::string oGetBuildingNameFromDescriptor(const std::string& oString) const;
      tVoid vSendsdsCheckAddressRequest();
      tVoid vRequestPOIListToNavi();
      tVoid vSetRemainingAddressInformation();
      tVoid vSetHouseNumberInformation(const std::string& oHouseNumber);
      tVoid vSetFirstStreetInformation(const std::string& oStreetName);
      tVoid vSetSecondStreetInformation(const std::string& oStreetName);
      tVoid vSetCityInformation(const std::string& oCityName);
      tVoid vSetStateInformation(const std::string& oStateName);
      tVoid vSetCountryInformation(const std::string& oCountryName);
      tVoid vHandleVerifiedSdsAddress(::std::vector< SDSAddressElement >& oVerifiedAddress, const SDSAddressOptions& sdsAddressOptions, ::std::vector< SDSAddressElementType >& ElementTypes);
      tVoid vHandleElementTypeResult(::std::vector< SDSAddressElementType >& ElementTypes);
      tVoid vSetDestinationResult(enResultType oResultType);
      tVoid vPrepareSetDestinationResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult, enResultType oResultType) const;
      tVoid vGetLocationDescription(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult,
                                    sds2hmi_fi_tcl_e16_SelectionCriterionType& oSelectionCriterienType);
      std::string oGetOsdeLocationDescriptor() const;
      std::string oGetStateDescriptor() const;
      std::string oGetTownDescriptor() const;
      std::string oGetStreetDescriptor() const;
      std::string oGetPOIString();
      std::string oGetHouseNumberString();
      std::string oGetPOISearchString() const;
      tVoid vResetResultValues();
      tVoid vValidateSdsAddressOptions(const SDSAddressOptions& sdsAddressOptions);
      tVoid vSendListentrySelectionToNavi(tU8 ambigousValue);
      tVoid vSetDestinationResultforAmbiguity();
      tVoid vSetDestinationResultforNonAmbiguity();
      tVoid vHandlePOIResult(tU16 numberOfElements, tBool bGuidancepossible);
      tVoid vSetAddressNoHouseNrResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const;
      tVoid vSetAddressResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const;
      tVoid vSetMultipleAddressResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const;
      tVoid vSetJunctionResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const;
      tVoid vSetMultipleJunctionResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const;
      tVoid vSetAddressHouseNrRangeResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const;
      tVoid vSetPOIMultipleResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const;
      tVoid vSetPOIResult(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult, tBool bGuidancepossible) const;

      struct DescrtiptorDecider
      {
         std::string CountryName;
         std::string CityName;
         std::string StateName;
         std::string StreetName;
         std::string CrossStreetName;
         std::string HouseNr;
         tBool bNavigable;
         tBool bAmbigous;
         tBool bHouseNrAvailable;
         tBool bHouseNrValid;
         tBool bCrossRoadAvailable;
      };
      struct POINames
      {
         std::string BrandName;
         std::string CategoryName;
         std::string BuildingName;
      };

      sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodStart _oMethodStart;
      sds2hmi_fi_tcl_e16_SelectionCriterionType::tenType _enNavSelectionCriterionType;
      boost::shared_ptr<NavigationServiceProxy> _navigationProxy;
      std::vector< SDSAddressElement > _oRequestedsdsAddress;
      DescrtiptorDecider _oResultDescriptor;
      POINames _oPOIName;
      clSDS_MultipleDestinationsList* _pMultipleDestiantionList;
      clSDS_POIList* _pPOIList;
      clSDS_NaviListItems* _pNaviListItems;
      clSDS_Method_CommonShowDialog* _pCommonShowDialog;
      size_t _destinationInformationSize;
      tVoid vPOIRequestToNavi();
      tVoid vTraceLocationDescription(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult);
      tBool bIsAddressRequest();
      tBool bIsHouseNumberRequest();
      tVoid vSetHouseNumber(const bpstl::string& oString);
      tBool bNoHouseNumberSentFromSDS(const bpstl::string& oHouseNumber) const;
      tVoid vSendMethodResultAsHouseNumberAmbiguious();
      tVoid vPrepareMethodResultAsHouseNumberAmbiguous(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const;
      tVoid vSendMethodResultAsHouseNumberNotValid();
      tVoid vPrepareMethodResultAsHouseNumberInvalid(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const;
      tVoid vSendMethodResultAsAddressCombinationNotValid();
      tVoid vPrepareMethodResultAsAddressCombinationInvalid(sds2hmi_sdsfi_tclMsgNaviSetDestinationItemMethodResult& oResult) const;
      tVoid vResolveAmbiguity();
      tVoid vHandleOSDESearchResult();
      tBool bIsValidAddress() const;
      bpstl::string oRemoveDescriptorInformation(bpstl::string oString, const bpstl::string& oDescriptorType) const;
      bpstl::string oRemoveEmptySpaces(bpstl::string oString) const;
      bpstl::string oSearchCarDealerBrands(const bpstl::string& oPOIString);
      bpstl::string oGetPOIStringWithNaviCategory(const bpstl::string& oPOIString);
      bpstl::map<bpstl::string, bpstl::string> _oSDStoNaviMap;
};


#endif
