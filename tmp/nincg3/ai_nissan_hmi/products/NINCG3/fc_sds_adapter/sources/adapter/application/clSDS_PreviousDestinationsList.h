/*********************************************************************//**
 * \file       clSDS_PreviousDestinationsList.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_PreviousDestinationsList_h
#define clSDS_PreviousDestinationsList_h


#define SYSTEM_S_IMPORT_INTERFACE_STRING
#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#include "stl_pif.h"

#include "application/clSDS_List.h"
#include <application/clSDS_NaviList.h>
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"


class clSDS_PreviousDestinationsList : public clSDS_List
   , public clSDS_NaviList
   , public org::bosch::cm::navigation::NavigationService::GetLastDestinationsCallbackIF
   , public org::bosch::cm::navigation::NavigationService::StartGuidanceToLastDestinationCallbackIF

{
   public:
      virtual ~clSDS_PreviousDestinationsList();
      clSDS_PreviousDestinationsList(::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > ponaviProxy);
      tBool bSelectElement(tU32 u32SelectedIndex);
      tU32 u32GetSize();
      std::vector<clSDS_ListItems> oGetItems(tU32 u32StartIndex, tU32 u32EndIndex);

      virtual void onGetLastDestinationsError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::GetLastDestinationsError >& error);

      virtual void onGetLastDestinationsResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::GetLastDestinationsResponse >& response);

      // Call back function form StartGuidanceToLastDestinationCallbackIF
      virtual void onStartGuidanceToLastDestinationError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::StartGuidanceToLastDestinationError >& error);

      virtual void onStartGuidanceToLastDestinationResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::StartGuidanceToLastDestinationResponse >& response);

   private:
      std::string oGetItem(tU32 u32Index);
      tVoid vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType);
      unsigned long vGetPreviousDestinationIndex()const;
      void oClearPreviousDestinationListItems();
      void oSetPreviousDestinationListCount(unsigned long previousDestinationList);
      void oSetPreviousDestinationIndex(unsigned long previousDestinationPOI);
      boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > _naviProxy;
      unsigned long _previousDestinationCount;
      unsigned long _previousDestinationPOIIndexValue;
      std::vector< org::bosch::cm::navigation::NavigationService::AddressListElement > _previousDestinationName;
      void vStartGuidance();
      void vAddasWayPoint();
};


#endif
