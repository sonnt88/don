/*********************************************************************//**
 * \file       clSDS_Method_NaviSetRouteCriteria.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_NaviSetRouteCriteria_h
#define clSDS_Method_NaviSetRouteCriteria_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"
#include "application/GuiService.h"


using namespace org::bosch::cm::navigation::NavigationService;


class clSDS_Method_NaviSetRouteCriteria
   : public clServerMethod
   , public SetRouteCriterionCallbackIF
{
   public:
      virtual ~clSDS_Method_NaviSetRouteCriteria();
      clSDS_Method_NaviSetRouteCriteria(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< NavigationServiceProxy > pSds2NaviProxy,
         GuiService& guiService);

      virtual void onSetRouteCriterionError(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< SetRouteCriterionError >& error);
      virtual void onSetRouteCriterionResponse(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< SetRouteCriterionResponse >& response);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      void vSendReCalculateRouteRequest(const sds2hmi_sdsfi_tclMsgNaviSetRouteCriteriaMethodStart& oMessage);
      boost::shared_ptr< NavigationServiceProxy > _sds2NaviProxy;
      GuiService& _guiService;
};


#endif
