/*********************************************************************//**
 * \file       clSDS_Property_WeatherStatus.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Property_WeatherStatus_h
#define clSDS_Property_WeatherStatus_h


#include "Sds2HmiServer/framework/clServerProperty.h"


class clSDS_Property_WeatherStatus : public clServerProperty
{
   public:
      virtual ~clSDS_Property_WeatherStatus();
      clSDS_Property_WeatherStatus(ahl_tclBaseOneThreadService* pService);

   protected:
      virtual tVoid vGet(amt_tclServiceData* pInMsg);
      virtual tVoid vSet(amt_tclServiceData* pInMsg);
      virtual tVoid vUpreg(amt_tclServiceData* pInMsg);
};


#endif
