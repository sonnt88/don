/*********************************************************************//**
 * \file       clSDS_Property_MediaStatus.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Property_MediaStatus_h
#define clSDS_Property_MediaStatus_h


#include "Sds2HmiServer/framework/clServerProperty.h"
#include "external/sds2hmi_fi.h"
#include "mplay_MediaPlayer_FIProxy.h"
#include "smartphoneint_main_fiProxy.h"


#define MPLAY_MAX_DEVICES 11


class clSDS_Property_MediaStatus
   : public clServerProperty
   , public asf::core::ServiceAvailableIF
   , public mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsCallbackIF
   , public smartphoneint_main_fi::DeviceStatusInfoCallbackIF
   , public smartphoneint_main_fi::GetDeviceInfoListCallbackIF
{
   public:
      virtual ~clSDS_Property_MediaStatus();
      clSDS_Property_MediaStatus(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy > pSds2MediaProxy,
         ::boost::shared_ptr< ::smartphoneint_main_fi::Smartphoneint_main_fiProxy > smartphoneProxy);

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onMediaPlayerDeviceConnectionsStatus(
         const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
         const boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsStatus >& status);

      virtual void onMediaPlayerDeviceConnectionsError(
         const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
         const boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsError >& error);

      virtual void onDeviceStatusInfoError(
         const ::boost::shared_ptr< ::smartphoneint_main_fi::Smartphoneint_main_fiProxy >& proxy,
         const ::boost::shared_ptr< ::smartphoneint_main_fi::DeviceStatusInfoError >& error);

      virtual void onDeviceStatusInfoStatus(
         const ::boost::shared_ptr< ::smartphoneint_main_fi::Smartphoneint_main_fiProxy >& proxy,
         const ::boost::shared_ptr< ::smartphoneint_main_fi::DeviceStatusInfoStatus >& status);

      virtual void onGetDeviceInfoListError(
         const ::boost::shared_ptr< ::smartphoneint_main_fi::Smartphoneint_main_fiProxy >& proxy,
         const ::boost::shared_ptr< ::smartphoneint_main_fi::GetDeviceInfoListError >& error);

      virtual void onGetDeviceInfoListResult(
         const ::boost::shared_ptr< ::smartphoneint_main_fi::Smartphoneint_main_fiProxy >& proxy,
         const ::boost::shared_ptr< ::smartphoneint_main_fi::GetDeviceInfoListResult >& result);

   protected:
      virtual tVoid vGet(amt_tclServiceData* pInMsg);
      virtual tVoid vSet(amt_tclServiceData* pInMsg);
      virtual tVoid vUpreg(amt_tclServiceData* pInMsg);

   private:
      struct trDevice
      {
         bool bIsConnected;
         unsigned int uiDeviceTag;
         unsigned int uiDeviceType;
         std::string sDeviceName;
         uint8 indexedState;
      };

      tVoid vSendStatus();
      sds2hmi_fi_tcl_e8_MPL_SourceType::tenType eGetMediaPlayerSource(tU32 u32Index) const;
      sds2hmi_fi_tcl_e8_DeviceStatus::tenType eGetDeviceStatus(tU32 u32Index) const;
      tBool bUsbSourceAvailabe(tU32 u32Index) const;
      tBool bIpodSourceAvailabe(tU32) const;
      tBool bMediaSourceAvailabe(tU32 u32Index) const;
      tBool bBTSourceAvailabe(tU32 u32Index) const;
      tBool bCdAvailable(tU32 u32Index) const;
      tBool bAuxSourceAvailable() const;
      tVoid vUpdateDeviceList(bpstl::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const;
      tVoid vUpdateMediaDeviceStatus(sds2hmi_fi_tcl_DeviceStatus& oDeviceStatus, tU32 u32Index) const;
      tVoid vUpdateDeviceListWithMedia(bpstl::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const;
      tVoid vUpdateDeviceListWithBTAudio(bpstl::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const;
      tVoid vUpdateDeviceListWithAux(bpstl::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const;
      tVoid vUpdateDeviceListWithCD(bpstl::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const;
      void updateDeviceListWithCarPlayAudio(std::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const;
      void updateDeviceListWithAndroidAutoAudio(std::vector<sds2hmi_fi_tcl_DeviceStatus>& oDeviceList) const;
      tVoid vTraceMediaStatus(const sds2hmi_sdsfi_tclMsgMediaStatusStatus& oMessage) const;

      bool isCarPlayAvailable(::std::vector< ::smartphoneint_main_fi_types::T_DeviceDetails > deviceList) const;
      bool isAndroidAutoAvailable(::std::vector< ::smartphoneint_main_fi_types::T_DeviceDetails > deviceList) const;
      void updateCarPlayAndroidAutoStatus(bool carPlay, bool androidAuto);

      boost::shared_ptr<mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy>_sds2MediaProxy;
      boost::shared_ptr<smartphoneint_main_fi::Smartphoneint_main_fiProxy > _smartphoneProxy;
      trDevice _oDeviceList[MPLAY_MAX_DEVICES];
      bool _carPlayAvailable;
      bool _androidAutoAvailable;
};


#endif
