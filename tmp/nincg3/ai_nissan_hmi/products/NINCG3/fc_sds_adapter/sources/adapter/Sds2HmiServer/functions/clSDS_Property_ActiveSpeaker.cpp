/*********************************************************************//**
 * \file       clSDS_Property_ActiveSpeaker.cpp
 *
 * Common Action Request property implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Property_ActiveSpeaker.h"
#include "external/sds2hmi_fi.h"
#include "application/clSDS_LanguageMediator.h"
#include "SdsAdapter_Trace.h"

/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Property_ActiveSpeaker::~clSDS_Property_ActiveSpeaker()
{
   _pLanguageMediator = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Property_ActiveSpeaker::clSDS_Property_ActiveSpeaker(ahl_tclBaseOneThreadService* pService, clSDS_LanguageMediator* pLanguageMediator)
   : clServerProperty(SDS2HMI_SDSFI_C_U16_SDS_ACTIVESPEAKER, pService),
     _pLanguageMediator(pLanguageMediator)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_ActiveSpeaker::vSet(amt_tclServiceData* pInMsg)
{
   sds2hmi_sdsfi_tclMsgSDS_ActiveSpeakerSet oMessage;
   vGetDataFromAmt(pInMsg, oMessage);
   tU16 u16ActiveSpeaker = oMessage.SpeakerId;
   sds2hmi_sdsfi_tclMsgSDS_ActiveSpeakerStatus oStatusMessage;
   oStatusMessage.SpeakerId = u16ActiveSpeaker;
   vStatus(oStatusMessage);

   if (_pLanguageMediator != NULL)
   {
      _pLanguageMediator->setActiveSpeaker(u16ActiveSpeaker);
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_ActiveSpeaker::vGet(amt_tclServiceData* /*pInMsg*/)
{
   sds2hmi_sdsfi_tclMsgSDS_ActiveSpeakerStatus oStatusMessage;
   oStatusMessage.SpeakerId = 0;
   vStatus(oStatusMessage);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_ActiveSpeaker::vUpreg(amt_tclServiceData* /*pInMsg*/)
{
   sds2hmi_sdsfi_tclMsgSDS_ActiveSpeakerStatus oMessage;
   oMessage.SpeakerId = 0;
   vStatus(oMessage);
}
