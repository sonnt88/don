/*********************************************************************//**
 * \file       clFunction.h
 *
 * Base class for CCA functions.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clFunction_h
#define clFunction_h


#define AMT_S_IMPORT_INTERFACE_GENERIC
#include "amt_if.h"

#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "fi_msgfw_if.h"


class clFunction
{
   public:
      virtual ~clFunction();
      clFunction(tU16 u16FunctionID, ahl_tclBaseOneThreadService* pService);
      virtual tVoid vHandleMessage(amt_tclServiceData* pInMsg) = 0;
      tU16 u16GetFunctionID(tVoid) const;

   protected:
      ahl_tclBaseOneThreadService* _pService;
      tVoid vSendMessage(tU16 u16DestAppID, tU16 u16CmdCtr, tU16 u16RegisterId, const fi_tclTypeBase& oOutData, tU16 u16Fid, tU8 u8OpCode);
      tVoid vSendEmptyMessage(tU16 u16DestAppID, tU16 u16CmdCtr, tU16 u16RegisterId, tU16 u16Fid, tU8 u8Opcode);
      tVoid vGetDataFromAmt(amt_tclServiceData* pFIMsg, fi_tclTypeBase& oFIData);
      tVoid vSendErrorMessage(tU16 u16DestAppID, tU16 u16CmdCtr, tU16 u16RegisterId, tU16 u16Errorcode);

   private:
      tU16 _u16FunctionID;
      tU16 getMajorVersion();
};


#endif
