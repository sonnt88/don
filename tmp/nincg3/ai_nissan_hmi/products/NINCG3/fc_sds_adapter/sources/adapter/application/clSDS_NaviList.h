/*********************************************************************//**
 * \file       clSDS_NaviList.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef CLSDS_NAVILIST_H_
#define CLSDS_NAVILIST_H_

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

class clSDS_NaviList
{
   public:
      clSDS_NaviList();
      virtual ~clSDS_NaviList();
      virtual void vStartGuidance() = 0;
      virtual void vAddasWayPoint() = 0;
};


#endif
