/*********************************************************************//**
 * \file       clSDS_Method_PhoneRedialLastNumber.cpp
 *
 * clSDS_Method_PhoneRedialLastNumber method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_PhoneRedialLastNumber.h"
#include "external/sds2hmi_fi.h"


using namespace MOST_Tel_FI;
using namespace most_Tel_fi_types;


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_PhoneRedialLastNumber::~clSDS_Method_PhoneRedialLastNumber()
{
   _pRecentCallsList = NULL;
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_PhoneRedialLastNumber::clSDS_Method_PhoneRedialLastNumber(
   ahl_tclBaseOneThreadService* pService,
   clSDS_RecentCallsList* pRecentCallsList,
   ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > pTelProxy)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_PHONEREDIALLASTNUMBER, pService)
   , _pRecentCallsList(pRecentCallsList)
   , _telephoneProxy(pTelProxy)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_PhoneRedialLastNumber::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   if (_pRecentCallsList != NULL)
   {
      dialNumber(_pRecentCallsList->getLastDialedNumber());
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneRedialLastNumber::dialNumber(std::string oLastDialedNumber)
{
   if (oLastDialedNumber != "")
   {
      _telephoneProxy->sendDialStart(*this, oLastDialedNumber, T_e8_TelEchoCancellationNoiseReductionSetting__e8ECNR_NOCHANGE);
      vSendMethodResult();
   }
   else
   {
      vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_NONUMBERTOREDIAL);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneRedialLastNumber::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& /*proxy*/,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_PhoneRedialLastNumber::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& /*proxy*/,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_PhoneRedialLastNumber::onDialError(
   const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< DialError >& /*error*/)
{
}


/***********************************************************************//**
 *
 ***************************************************************************/
void clSDS_Method_PhoneRedialLastNumber::onDialResult(
   const ::boost::shared_ptr< MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< DialResult >& /*result*/)
{
}
