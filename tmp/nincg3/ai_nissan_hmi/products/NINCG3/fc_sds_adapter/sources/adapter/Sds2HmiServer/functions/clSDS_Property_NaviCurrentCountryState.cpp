/*********************************************************************//**
 * \file       clSDS_Property_NaviCurrentCountryState.cpp
 *
 * Common Action Request property implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Property_NaviCurrentCountryState.h"
#include "SdsAdapter_Trace.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Property_NaviCurrentCountryState.cpp.trc.h"
#endif


using namespace org::bosch::cm::navigation::NavigationService;


#define ARRAY_SIZE(array)	(sizeof (array) / sizeof (array)[0])


struct CountrycodeMapping
{
   std::string naviCountryCode;
   tU32 sdsISOCountryCode;
};


static const CountrycodeMapping countryCodesMap[] =
{
   {"AFG", 1223},  {"ALB", 1410},  {"DZA", 4929},  {"ASM", 1649},  {"AND", 1649},  {"AGO", 1649},  {"AIA", 1649},  {"ATG", 1649},  {"ARG", 1649},  {"ARM", 1649},
   {"ABW", 1111},  {"AUS", 1715},  {"AUT", 1716},  {"AZE", 1861},  {"BHS", 2323},  {"BHR", 2322},  {"BGD", 2276},  {"BRB", 2626},  {"BLR", 2450},  {"BEL", 2220},
   {"BLZ", 2428},  {"BEN", 2222},  {"BMU", 2485},  {"BTN", 2702},  {"BOL", 2540},  {"BIH", 2344},  {"BWA", 2785},  {"BRA", 2625},  {"BRN", 2638},  {"BGR", 2290},
   {"BFA", 2241},  {"BDI", 2185},  {"KHM", 11533}, {"CMR", 3506},  {"CAN", 3118},  {"CPV", 3606},  {"CYM", 3885},  {"CAF", 3110},  {"TCD", 20580}, {"CHL", 3340},
   {"CHN", 3342},  {"COL", 3564},  {"COM", 3565},  {"COG", 3559},  {"COD", 3556},  {"COK", 3563},  {"CRI", 3657},  {"CIV", 3382},  {"HRV", 8790},  {"CUB", 3746},
   {"CYP", 3888},  {"CZE", 3909},  {"DNK", 4555},  {"DJI", 4425},  {"DMA", 4513},  {"DOM", 4589},  {"ECU", 5237},  {"EGY", 5369},  {"SLV", 19862}, {"GNQ", 7633},
   {"ERI", 5705},  {"EST", 5748},  {"ETH", 5768},  {"FLK", 6539},  {"FRO", 6735},  {"FJI", 6473},  {"FIN", 6446},  {"FRA", 6721},  {"GUF", 7846},  {"PYF", 17190},
   {"GAB", 7202},  {"GMB", 7586},  {"GEO", 7343},  {"DEU", 4277},  {"GHA", 7425},  {"GIB", 7458},  {"GRC", 7747},  {"GRL", 7756},  {"GRD", 7748},  {"GLP", 7568},
   {"GUM", 7853},  {"GTM", 7821},  {"GIN", 7470},  {"GNB", 7618},  {"GUY", 7865},  {"HTI", 8841},  {"VAT", 22580}, {"HND", 8644},  {"HKG", 8551},  {"HUN", 8878},
   {"ISL", 9836},  {"IND", 9668},  {"IDN", 9358},  {"INT", 9684},  {"IRN", 9806},  {"IRQ", 9809},  {"IRL", 9804},  {"ISR", 9842},  {"ITA", 9857},  {"JAM", 10285},
   {"JPN", 10766}, {"JOR", 10738}, {"KAZ", 11322}, {"KEN", 11438}, {"KIR", 11570}, {"PRK", 16971}, {"KOR", 11762}, {"KWT", 12020}, {"KGZ", 11514}, {"LAO", 12335},
   {"LVA", 12993}, {"LBN", 12366}, {"LSO", 12911}, {"LBR", 12370}, {"LBY", 12377}, {"LIE", 12581}, {"LTU", 12949}, {"LUX", 12984}, {"MAC", 13347}, {"MKD", 13668},
   {"MDG", 13447}, {"MWI", 14057}, {"MYS", 14131}, {"MDV", 13462}, {"MLI", 13705}, {"MLT", 13716}, {"MHL", 13580}, {"MTQ", 13969}, {"MRT", 13908}, {"MUS", 14003},
   {"MYT", 14132}, {"MEX", 13496}, {"FSM", 6765},  {"MDA", 13441}, {"MCO", 13423}, {"MNG", 13767}, {"MNE", 13765}, {"MSR", 13938}, {"MAR", 13362}, {"MOZ", 13818},
   {"MMR", 13746}, {"NAM", 14381}, {"NRU", 14933}, {"NPL", 14860}, {"NLD", 14724}, {"ANT", 1492},  {"NCL", 14444}, {"NZL", 15180}, {"NIC", 14627}, {"NER", 14514},
   {"NGA", 14561}, {"NIU", 14645}, {"NFK", 14539}, {"MNP", 13776}, {"NOR", 14834}, {"PSE", 16997}, {"OMN", 15790}, {"PAK", 16427}, {"PLW", 16791}, {"PAN", 16430},
   {"PNG", 16839}, {"PRY", 16985}, {"PER", 16562}, {"PHL", 16652}, {"PCN", 16494}, {"POL", 16876}, {"PRT", 16980}, {"PRI", 16969}, {"QAT", 17460}, {"REU", 18613},
   {"ROU", 18933}, {"RUS", 19123}, {"RWA", 19169}, {"KNA", 11713}, {"LCA", 12385}, {"VCT", 22644}, {"WSM", 24173}, {"SMR", 19890}, {"STP", 20112}, {"SAU", 19509},
   {"SEN", 19630}, {"SRB", 20034}, {"SYC", 20259}, {"SLE", 19845}, {"SGP", 19696}, {"SVK", 20171}, {"SVN", 20174}, {"SLB", 19842}, {"SOM", 19949}, {"ZAF", 26662},
   {"ESP", 5744},  {"LKA", 12641}, {"SHN", 19726}, {"SPM", 19981}, {"SDN", 19598}, {"SUR", 20146}, {"SJM", 19789}, {"SWZ", 20218}, {"SWE", 20197}, {"CHE", 3333},
   {"SYR", 20274}, {"TWN", 21230}, {"TJK", 20811}, {"TZA", 21313}, {"THA", 20737}, {"TLS", 20883}, {"TGO", 20719}, {"TKL", 20844}, {"TON", 20974}, {"TTO", 21135},
   {"TUN", 21166}, {"TUR", 21170}, {"TKM", 20845}, {"TCA", 20577}, {"TUV", 21174}, {"UGA", 21729}, {"UKR", 21874}, {"ARE", 1605},  {"GBR", 7250},  {"USA", 22113},
   {"URY", 22105}, {"UZB", 22338}, {"VUT", 23220}, {"VEN", 22702}, {"VNM", 22989}, {"VGB", 22754}, {"VIR", 22834}, {"WLF", 23942}, {"ESH", 5736},  {"YEM", 25773},
   {"YUG", 26279}, {"ZMB", 27042}, {"ZWE", 27365}
};


/**************************************************************************//**

******************************************************************************/
clSDS_Property_NaviCurrentCountryState::~clSDS_Property_NaviCurrentCountryState()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Property_NaviCurrentCountryState::clSDS_Property_NaviCurrentCountryState(ahl_tclBaseOneThreadService* pService,
      ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > pNaviProxy)
   : clServerProperty(SDS2HMI_SDSFI_C_U16_NAVICURRENTCOUNTRYSTATE, pService)
   , _naviProxy(pNaviProxy)
{
   _naviCountryCode = "USA";
   _countryName = "UNITED STATES";
   _statecode = "MI";
   _stateName = "Michigan";
   _cityName = "Detroit";
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_NaviCurrentCountryState::vSet(amt_tclServiceData* /*pInMsg*/)
{
}


/**************************************************************************//**
*  Not noticed of get call for this property yet.
******************************************************************************/
tVoid clSDS_Property_NaviCurrentCountryState::vGet(amt_tclServiceData* /*pInMsg*/)
{
   vSendCountryStateStatus();
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Property_NaviCurrentCountryState::vUpreg(amt_tclServiceData* /*pInMsg*/)
{
   vSendCountryStateStatus();
}


#if 0

/**************************************************************************//**
* Removes the Country Tag coming from Navi.
******************************************************************************/
tVoid clSDS_Property_NaviCurrentCountryState::vRemoveCountryTag(bpstl::string& oString)
{
   size_t pos = oString.find(" (");
   if (pos == oString.npos)
   {
      pos = oString.find("(");
   }
   if (pos != oString.npos)
   {
      oString.erase(pos);
   }
}


/**************************************************************************//**
* Decodes the state code coming from Navi.
******************************************************************************/
tVoid clSDS_Property_NaviCurrentCountryState::vDecodeStateCode(tU32 u32StateCode, bpstl::string& oStateCode)
{
   tString szTempStateCode = OSAL_NEW tChar[4];
   if (szTempStateCode != NULL)
   {
      szTempStateCode[0] = u32StateCode >> 24;
      szTempStateCode[1] = u32StateCode >> 16;
      szTempStateCode[2] = u32StateCode >> 8;
      szTempStateCode[3] = u32StateCode;
   }
   oStateCode = szTempStateCode;
   OSAL_DELETE [] szTempStateCode;
}


#endif

/***********************************************************************//**

 ***************************************************************************/
void clSDS_Property_NaviCurrentCountryState::onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
      const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_naviProxy == proxy)
   {
      _naviProxy->sendPositionInformationDeregisterAll();
   }
}


/***********************************************************************//**

 ***************************************************************************/
void clSDS_Property_NaviCurrentCountryState::onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy,
      const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_naviProxy == proxy)
   {
      ETG_TRACE_USR4(("clSDS_Property_NaviCurrentCountryState. Registering for PositionInformation"));
      _naviProxy->sendPositionInformationRegister(*this);
      _naviProxy->sendPositionInformationGet(*this);
   }
}


/***********************************************************************//**

 ***************************************************************************/
void clSDS_Property_NaviCurrentCountryState::onPositionInformationUpdate(const ::boost::shared_ptr< NavigationServiceProxy >&  /*proxy*/,
      const ::boost::shared_ptr< PositionInformationUpdate >& update)
{
   _naviCountryCode = update->getPositionInformation().getCountryCode();
   ETG_TRACE_USR4((" Update country code %s:", _naviCountryCode.c_str()));

   _countryName = update->getPositionInformation().getCountry();
   ETG_TRACE_USR4((" Update country name %s:", _countryName.c_str()));

   _statecode = update->getPositionInformation().getStateCode();
   ETG_TRACE_USR4((" Update state code %s:", _statecode.c_str()));

   _stateName = update->getPositionInformation().getProvince();
   ETG_TRACE_USR4((" Update state name %s:", _stateName.c_str()));

   _cityName = update->getPositionInformation().getCity();
   ETG_TRACE_USR4((" Update city name %s:", _cityName.c_str()));

   vSendCountryStateStatus();
}


/***********************************************************************//**
 *  Position Information error callback form  Navi
 ***************************************************************************/
void clSDS_Property_NaviCurrentCountryState::onPositionInformationError(const ::boost::shared_ptr< NavigationServiceProxy >&  /*proxy*/,
      const ::boost::shared_ptr< PositionInformationError >& /*error*/)
{
}


/***********************************************************************//**

 ***************************************************************************/
void clSDS_Property_NaviCurrentCountryState::vSendCountryStateStatus()
{
   sds2hmi_sdsfi_tclMsgNaviCurrentCountryStateStatus oMessage;
   oMessage.Country.enType = convertNaviStringtoISOCountryCode(_naviCountryCode);
   oMessage.CountryName.bSet(_countryName.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   oMessage.State.bSet(_statecode.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   oMessage.StateName.bSet(_stateName.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   oMessage.CityName.bSet(_cityName.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   vStatus(oMessage);
}


/***********************************************************************//**

 ***************************************************************************/
sds2hmi_fi_tcl_e16_ISOCountryCode::tenType clSDS_Property_NaviCurrentCountryState::convertNaviStringtoISOCountryCode(std::string countrycode)
{
   sds2hmi_fi_tcl_e16_ISOCountryCode::tenType u32ISOCountrycode = sds2hmi_fi_tcl_e16_ISOCountryCode::FI_EN_ISO_ALPHA_3_USA;
   for (size_t i = 0; i < ARRAY_SIZE(countryCodesMap); i++)
   {
      if (countryCodesMap[i].naviCountryCode.compare(countrycode) == 0)
      {
         u32ISOCountrycode = (sds2hmi_fi_tcl_e16_ISOCountryCode::tenType)countryCodesMap[i].sdsISOCountryCode;
         break;
      }
   }
   ETG_TRACE_USR4((" countrycode %s:", countrycode.c_str()));
   ETG_TRACE_USR4((" u32ISOCountrycode %d:", u32ISOCountrycode));
   return u32ISOCountrycode;
}
