/*********************************************************************//**
 * \file       clSDS_Method_ContactsGetAmbiguityList.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef clSDS_Method_ContactsGetAmbiguityList_h
#define clSDS_Method_ContactsGetAmbiguityList_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "MOST_PhonBk_FIProxy.h"


class clSDS_AmbigContactList;
class clSDS_AmbigNumberList;


class clSDS_Method_ContactsGetAmbiguityList
   : public clServerMethod
   , public MOST_PhonBk_FI::GetContactDetailsCallbackIF
{
   public:
      clSDS_Method_ContactsGetAmbiguityList(ahl_tclBaseOneThreadService* pService
                                            , ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy> phoneBookProxy
                                            , clSDS_AmbigContactList* ambigContactList
                                            , clSDS_AmbigNumberList* ambigNumberList);
      virtual ~clSDS_Method_ContactsGetAmbiguityList();

      virtual void vMethodStart(amt_tclServiceData* pInMsg);

   private:

      void resolveAmbiguity();

      virtual void onGetContactDetailsError(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy, const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsError >& error);
      virtual void onGetContactDetailsResult(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy, const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsResult >& result);

      void setResultEntriesAndType(tU32 contactID, most_PhonBk_fi_types::T_PhonBkContactDetails contactDetails, sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& result);
      void setResultType(sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& result);

      void setAmbigContacts(std::vector<most_PhonBk_fi_types::T_PhonBkContactDetails>  resultEntries);
      void setAmbigNumbers(std::vector< sds2hmi_fi_tcl_CON_AmbiguityResultEntry > resultEntries);

      std::string getContactName(most_PhonBk_fi_types::T_PhonBkContactDetails contactDetails) const;
      void updateHeaderTagWithContactName(most_PhonBk_fi_types::T_PhonBkContactDetails contactDetails) const;

      void setHomeEntries(tU32 contactID, most_PhonBk_fi_types::T_PhonBkContactDetails contactDetails, sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& result) const;
      void setMobileEntries(tU32 contactID, most_PhonBk_fi_types::T_PhonBkContactDetails contactDetails, sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& result) const;
      void setOfficeEntries(tU32 contactID, most_PhonBk_fi_types::T_PhonBkContactDetails contactDetails, sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& result) const;
      void setOtherEntries(tU32 contactID, most_PhonBk_fi_types::T_PhonBkContactDetails contactDetails, sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& result) const;
      void setPreferredEntries(tU32 contactID, most_PhonBk_fi_types::T_PhonBkContactDetails contactDetails, sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& result) const;
      void setAllLocationsEntries(tU32 contactID, most_PhonBk_fi_types::T_PhonBkContactDetails contactDetails, sds2hmi_sdsfi_tclMsgContactsGetAmbiguityListMethodResult& result) const;

      boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy> _phoneBookProxy;
      clSDS_AmbigContactList* _pAmbigContactList;
      clSDS_AmbigNumberList* _pAmbigNumberList;

      sds2hmi_fi_tcl_e8_PHN_NumberType _locationType;
      bool _requireAllLocationIfNoExactMatch;
      std::vector<tU32> _contactIDList;
      std::vector<most_PhonBk_fi_types::T_PhonBkContactDetails> _contactDetailList;
};


#endif
