/*********************************************************************//**
 * \file       clSDS_Method_TextMsgSetNumber.cpp
 *
 * clSDS_Method_TextMsgSetNumber method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_TextMsgSetNumber.h"
#include "application/clSDS_Iconizer.h"
#include "application/clSDS_ListScreen.h"
#include "application/clSDS_PhonebookList.h"
#include "application/clSDS_Userwords.h"
#include "external/sds2hmi_fi.h"
#include "application/clSDS_TextMsgContent.h"
#include "application/clSDS_QuickDialList.h"


using namespace MOST_PhonBk_FI;
using namespace most_PhonBk_fi_types;


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Method_TextMsgSetNumber::~clSDS_Method_TextMsgSetNumber()
{
   _pUserwords = NULL;
   _pPhonebookList = NULL;
   _pListScreen = NULL;
   _pQuickDialList = NULL;
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Method_TextMsgSetNumber::clSDS_Method_TextMsgSetNumber(
   ahl_tclBaseOneThreadService* pService,
   clSDS_ListScreen* pListScreen,
   clSDS_PhonebookList* pPhonebookList,
   clSDS_Userwords* pUserwords,
   ::boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy> phonebookProxy,
   clSDS_QuickDialList* pQuickDialList)

   : clServerMethod(SDS2HMI_SDSFI_C_U16_TEXTMSGSETNUMBER, pService)
   , _phonebookProxy(phonebookProxy)
   , _pPhonebookList(pPhonebookList)
   , _pListScreen(pListScreen)
   , _pUserwords(pUserwords)
   , _pQuickDialList(pQuickDialList)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_TextMsgSetNumber::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgTextMsgSetNumberMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   switch (oMessage.nSelectionType.enType)
   {
      case sds2hmi_fi_tcl_e8_GEN_SelectionType::FI_EN_BYVAL:
         if (oMessage.sPhoneNumber.szValue != OSAL_NULL)
         {
            vSetSMSTargetNumberByValue(oMessage.sPhoneNumber.szValue);
            vSaveCurrentNumberForCallBack(oMessage.sPhoneNumber.szValue);
         }
         break;

      case sds2hmi_fi_tcl_e8_GEN_SelectionType::FI_EN_BYUSW:
         vSetSMSTargetNumberByUWID(oMessage.ValueID);
         break;

      case sds2hmi_fi_tcl_e8_GEN_SelectionType::FI_EN_BYLISTNUMBER:
         vSetSMSTargetNumberByContact(oMessage.ValueID);
         break;

      default:
         break;
   }
   vSendMethodResult();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_TextMsgSetNumber::vSetSMSTargetNumberByValue(tCString strPhoneNumber) const
{
   std::string oNumber = clSDS_Iconizer::oRemoveIconPrefix(strPhoneNumber);
   setMessageString(oNumber);
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_TextMsgSetNumber::vSaveCurrentNumberForCallBack(tCString strPhoneNumber) const
{
   std::string oNumber = clSDS_Iconizer::oRemoveIconPrefix(strPhoneNumber);
   setMessageString(oNumber);
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_TextMsgSetNumber::vSetSMSTargetNumberByUWID(tU32 u32UwId)
{
   if (_phonebookProxy->isAvailable())
   {
      _phonebookProxy->sendGetContactDetailsExtendedStart(*this, u32UwId);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Method_TextMsgSetNumber::vSetSMSTargetNumberByContact(tU32 u32Index)
{
   std::string oPhoneNumber;
   oPhoneNumber = (u32Index == 0) ? _pPhonebookList->oGetPhoneNumbers().oPBDetails_TelNumber[0] : _pListScreen->oGetSelectedItem(u32Index);
   setMessageString(oPhoneNumber);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TextMsgSetNumber::setMessageString(std::string strNumber) const
{
   clSDS_TextMsgContent::setPhoneNumber(strNumber);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TextMsgSetNumber::onGetContactDetailsExtendedError(
   const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedError >& /*error*/)
{
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_TextMsgSetNumber::onGetContactDetailsExtendedResult(
   const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedResult >& result)
{
   std::string phoneNumber = _pQuickDialList->getPhoneNumber(result->getOContactDetailsExtended());
   setMessageString(phoneNumber);
}
