/*********************************************************************//**
 * \file       clSDS_PhoneStatus.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_PhoneStatus_h
#define clSDS_PhoneStatus_h


#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"


class clSDS_PhoneStatusObserver;


class clSDS_PhoneStatus
{
   public:
      virtual ~clSDS_PhoneStatus();
      clSDS_PhoneStatus();
      tVoid vRegisterObserver(clSDS_PhoneStatusObserver* pObserver);
      tVoid vOnPhoneStatusChanged(tU32 u32PhoneStatus);

   private:
      tVoid vNotifyObservers(tU32 u32PhoneStatus);
      bpstl::vector<clSDS_PhoneStatusObserver*> _observers;
};


#endif
