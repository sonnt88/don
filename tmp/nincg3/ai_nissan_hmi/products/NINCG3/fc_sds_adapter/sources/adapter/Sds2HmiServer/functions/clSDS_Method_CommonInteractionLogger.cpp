/*********************************************************************//**
 * \file       clSDS_Method_CommonInteractionLogger.cpp
 *
 * clSDS_Method_CommonInteractionLogger method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_CommonInteractionLogger.h"
#include "external/sds2hmi_fi.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_CommonInteractionLogger::~clSDS_Method_CommonInteractionLogger()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_CommonInteractionLogger::clSDS_Method_CommonInteractionLogger(ahl_tclBaseOneThreadService* pService)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_COMMONINTERACTIONLOGGER, pService)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_CommonInteractionLogger::vMethodStart(amt_tclServiceData* /*pInMessage*/)
{
   vSendMethodResult();
}
