/*********************************************************************//**
 * \file       AhlService.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef AhlService_h
#define AhlService_h


#include "asf/core/Logger.h"
#include <list>

#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"


class clFunction;


class AhlService : public ahl_tclBaseOneThreadService
{
   public:
      AhlService();  // declaration without implementaton!
      AhlService(ahl_tclBaseOneThreadApp* app, tU16 serviceId, tU16 majorVersion, tU16 minorVersion);
      virtual ~AhlService();
      void vAddFunction(clFunction* function);

   private:
      std::list<clFunction*> _functions;
      virtual tVoid vMyDispatchMessage(amt_tclServiceData* pServiceDataMessage);
      DECLARE_CLASS_LOGGER();
};


#endif
