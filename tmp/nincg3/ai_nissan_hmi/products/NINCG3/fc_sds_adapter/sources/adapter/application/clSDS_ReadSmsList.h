/*********************************************************************//**
 * \file       clSDS_ReadSmsList.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_ReadSmsList_h
#define clSDS_ReadSmsList_h


#include "application/clSDS_List.h"
#include "application/clSDS_FormatTimeDate.h"
#include "application/clSDS_PhoneStatusObserver.h"
#include "MOST_Msg_FIProxy.h"
#include "clock_main_fiProxy.h"

class clSDS_ReadTextListObserver;

class clSDS_ReadSmsList
   : public clSDS_List
   , public clSDS_PhoneStatusObserver
   , public asf::core::ServiceAvailableIF
   , public MOST_Msg_FI::MessagingDeviceConnectionCallbackIF
   , public MOST_Msg_FI::CreateMessageListCallbackIF
   , public MOST_Msg_FI::RequestSliceMessageListCallbackIF
   , public MOST_Msg_FI::ReleaseMessageListCallbackIF
   , public MOST_Msg_FI::MessageListChangeCallbackIF
   , public clock_main_fi::LocalTimeDate_MinuteUpdateCallbackIF
{
   public :
      clSDS_ReadSmsList(
         ::boost::shared_ptr< ::MOST_Msg_FI::MOST_Msg_FIProxy > pSds2MsgProxy,
         ::boost::shared_ptr< ::clock_main_fi::Clock_main_fiProxy > pClockproxy,
         clSDS_FormatTimeDate* pFormatTimeDate);
      clSDS_ReadSmsList();    // default constructor without implementation
      ~clSDS_ReadSmsList();

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onMessagingDeviceConnectionError(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::MessagingDeviceConnectionError >& error);
      virtual void onMessagingDeviceConnectionStatus(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::MessagingDeviceConnectionStatus >& status);

      virtual void onCreateMessageListError(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::CreateMessageListError >& error);
      virtual void onCreateMessageListResult(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::CreateMessageListResult >& result);

      virtual void onRequestSliceMessageListError(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::RequestSliceMessageListError >& error);
      virtual void onRequestSliceMessageListResult(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::RequestSliceMessageListResult >& result);

      virtual void onReleaseMessageListError(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::ReleaseMessageListError >& error);
      virtual void onReleaseMessageListResult(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::ReleaseMessageListResult >& result);

      virtual void onMessageListChangeError(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::MessageListChangeError >& error);
      virtual void onMessageListChangeStatus(
         const ::boost::shared_ptr< MOST_Msg_FI::MOST_Msg_FIProxy >& proxy,
         const ::boost::shared_ptr< MOST_Msg_FI::MessageListChangeStatus >& status);

      virtual void onLocalTimeDate_MinuteUpdateError(
         const ::boost::shared_ptr< clock_main_fi::Clock_main_fiProxy >& proxy,
         const ::boost::shared_ptr< clock_main_fi::LocalTimeDate_MinuteUpdateError >& error);
      virtual void onLocalTimeDate_MinuteUpdateStatus(
         const ::boost::shared_ptr< clock_main_fi::Clock_main_fiProxy >& proxy,
         const ::boost::shared_ptr< clock_main_fi::LocalTimeDate_MinuteUpdateStatus >& status);

      virtual tU32 u32GetSize();
      virtual std::vector<clSDS_ListItems> oGetItems(tU32 u32StartIndex, tU32 u32EndIndex);
      virtual tBool bSelectElement(tU32 u32SelectedIndex);
      unsigned int getSelectedSmsIndex() const;
      std::string getSelectedPhoneNumber() const;
      std::string getSelectedContactName() const;
      std::string getTimeDate() const;
      most_Msg_fi_types::T_MsgMessageHandle getMessageHandle() const;
      void selectNextMessage();
      void selectPreviousMessage();
      virtual void phoneStatusChanged(uint8 deviceHandle, most_BTSet_fi_types::T_e8_BTSetDeviceStatus status);
      void setTextListObserver(clSDS_ReadTextListObserver* obs);
      virtual tVoid vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType);

   private:
      void requestSliceMessageList();
      std::string oGetItem(tU32 u32Index);
      struct smsInfo
      {
         most_Msg_fi_types::T_MsgMessageHandle messageHandle;
         std::string formattedTimeDate;
         std::string contactName;
         std::string phoneNumber;
      };
      boost::shared_ptr< ::MOST_Msg_FI::MOST_Msg_FIProxy > _messageProxy;
      boost::shared_ptr< ::clock_main_fi::Clock_main_fiProxy > _clockproxy;
      clSDS_FormatTimeDate* _pFormatTimeDate;
      std::vector<smsInfo> _smsListInfo;
      unsigned int _index;
      uint16 _listHandle;
      uint8 _deviceHandle;
      struct date
      {
         int day;
         int month;
         int year;
      };
      date _currentDate;
      clSDS_ReadTextListObserver* _pReadTextListObserver;
};


#endif
