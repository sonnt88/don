/*********************************************************************//**
 * \file       clSDS_AddressBookList.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_AddressBookList_h
#define clSDS_AddressBookList_h


#define SYSTEM_S_IMPORT_INTERFACE_STRING
#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#include "stl_pif.h"

#include "org/bosch/cm/navigation/NavigationServiceProxy.h"
#include "application/clSDS_List.h"
#include "application/clSDS_NaviList.h"


class clSDS_AddressBookList
   : public clSDS_List
   , public org::bosch::cm::navigation::NavigationService::GetAddressbookDestinationsCallbackIF
   , public org::bosch::cm::navigation::NavigationService::StartGuidanceToAddressbookDestinationCallbackIF
   , public clSDS_NaviList
{
   public:
      virtual ~clSDS_AddressBookList();
      clSDS_AddressBookList(::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > naviProxy);
      tBool bSelectElement(tU32 u32SelectedIndex);
      std::vector<clSDS_ListItems> oGetItems(tU32 u32StartIndex, tU32 u32EndIndex);
      tU32 u32GetSize();

      virtual void onGetAddressbookDestinationsError(
         const ::boost::shared_ptr<org::bosch::cm::navigation::NavigationService:: NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::GetAddressbookDestinationsError >& error);

      virtual void onGetAddressbookDestinationsResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::GetAddressbookDestinationsResponse >& response);

      virtual void onStartGuidanceToAddressbookDestinationError(
         const ::boost::shared_ptr<  org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr<  org::bosch::cm::navigation::NavigationService::StartGuidanceToAddressbookDestinationError >& error);

      virtual void onStartGuidanceToAddressbookDestinationResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::StartGuidanceToAddressbookDestinationResponse >& response);

      std::string oGetHMIListDescriptionItems(tU32 u32Index);
   private:
      void vStartGuidance();
      void vAddasWayPoint();
      std::string oGetItem(tU32 u32Index);
      tVoid vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType);
      void vClear();
      boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > _naviProxy;
      std::vector< org::bosch::cm::navigation::NavigationService::AddressListElement > _addressBook;
      uint32 _selectedIndex;
};


#endif
