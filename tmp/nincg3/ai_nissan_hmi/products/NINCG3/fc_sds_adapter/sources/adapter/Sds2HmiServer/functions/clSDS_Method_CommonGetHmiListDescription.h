/*********************************************************************//**
 * \file       clSDS_Method_CommonGetHmiListDescription.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_CommonGetHmiListDescription_h
#define clSDS_Method_CommonGetHmiListDescription_h

#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"

class clSDS_List;


class clSDS_Method_CommonGetHmiListDescription : public clServerMethod
{
   public:
      virtual ~clSDS_Method_CommonGetHmiListDescription();
      clSDS_Method_CommonGetHmiListDescription(ahl_tclBaseOneThreadService* pService);
      void vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::tenType type, clSDS_List* pList);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);

      bool bCheckHmiListDescriptionType(
         const sds2hmi_sdsfi_tclMsgCommonGetHMIListDescriptionMethodStart& oMessage,
         sds2hmi_sdsfi_tclMsgCommonGetHMIListDescriptionMethodResult& oResultListDescription);

      void vSetAddressHomeEntry(
         sds2hmi_fi_tcl_HMIElementDescrptionList& oHMIElementDescrptionList,
         const std::string& oAddressEntry);

      void vSetPOINameInformation(
         sds2hmi_fi_tcl_HMIElementDescrptionList& oHMIElementDescrptionList,
         const std::string& oPOIName);

      void vSetPOIBrandName(
         sds2hmi_fi_tcl_HMIElementDescrptionList& oHMIElementDescrptionList,
         const std::string& oPOIBrandName);

      void vSetPOICategoryName(
         sds2hmi_fi_tcl_HMIElementDescrptionList& oHMIElementDescrptionList,
         const std::string& oPOICategoryName);

      void vSetAddressEntry(
         sds2hmi_fi_tcl_HMIElementDescrptionList& oHMIElementDescrptionList,
         const std::string& oAddressString)const;

      void vSetAddressBookItems(sds2hmi_sdsfi_tclMsgCommonGetHMIListDescriptionMethodResult& oResult);
      std::string oGetCategoryNameFromDescriptor(const std::string& oString)const;
      std::string oGetBrandNameFromDescriptor(const std::string& oString) const;
      std::string oRemoveDescriptorInformation(std::string oString, const std::string& oDescriptorType)const;
      std::map<sds2hmi_fi_tcl_e8_HMI_ListType::tenType, clSDS_List*> _oListMap;
      clSDS_List* _pCurrentList;
      bool _isDescriptorTagAddressEntry;
};


#endif
