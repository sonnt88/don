/*********************************************************************//**
 * \file       clSDS_FuelPriceList.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_FuelPriceList_h
#define clSDS_FuelPriceList_h


#include "MIDW_EXT_SXM_FUEL_FIProxy.h"
#include "clSDS_List.h"
#include "MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"
#include "clSDS_NaviList.h"
#include "legacy/FuelCommonSources.h"
#include "application/clSDS_KDSConfiguration.h"

class clSDS_Method_CommonGetListInfo;


class clSDS_FuelPriceList :
   public clSDS_List
   , public clSDS_NaviList
   , public asf::core::ServiceAvailableIF
   , public MIDW_EXT_SXM_FUEL_FI::GetFuelInfoListCallbackIF
   , public MIDW_EXT_SXM_FUEL_FI::SxmListModeCallbackIF
   , public MIDW_EXT_SXM_FUEL_FI::FuelInfoListsStatusCallbackIF
   , public MIDW_EXT_SXM_FUEL_FI::FuelBrandNameListCallbackIF
   , public MIDW_EXT_SXM_FUEL_FI::FuelTypeListCallbackIF
   , public MIDW_EXT_SXM_FUEL_FI::SxmDataServiceStatusCallbackIF
   , public MIDW_EXT_SXM_FUEL_FI::GetFuelStationInfoCallbackIF
   , public MIDW_EXT_SXM_CANADIAN_FUEL_FI::SxmDataServiceStatusCallbackIF
   , public MIDW_EXT_SXM_CANADIAN_FUEL_FI::CanadianFuelInfoListsStatusCallbackIF
   , public MIDW_EXT_SXM_CANADIAN_FUEL_FI::CanadianFuelBrandNameListCallbackIF
   , public MIDW_EXT_SXM_CANADIAN_FUEL_FI::CanadianFuelTypeListCallbackIF
   , public MIDW_EXT_SXM_CANADIAN_FUEL_FI::SxmListModeCallbackIF
   , public MIDW_EXT_SXM_CANADIAN_FUEL_FI::GetCanadianFuelInfoListCallbackIF
   , public MIDW_EXT_SXM_CANADIAN_FUEL_FI::GetCanadianFuelStationInfoCallbackIF
   , public org::bosch::cm::navigation::NavigationService::PositionInformationCallbackIF
   , public org::bosch::cm::navigation::NavigationService::StartGuidanceToPosWGS84CallbackIF
   , public org::bosch::cm::navigation::NavigationService::SetLocationWithCoordinatesCallbackIF
{
   public:
      virtual ~clSDS_FuelPriceList();
      clSDS_FuelPriceList(
         ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy > sxmFuelProxy,
         ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy > sxmCanadianFuelProxy,
         ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > naviProxy);

      unsigned long u32GetSize();

      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onGetFuelInfoListError(
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::GetFuelInfoListError >& error) ;
      virtual void onGetFuelInfoListResult(
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::GetFuelInfoListResult >& result);

      virtual void onSxmListModeError(
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::SxmListModeError >& error);
      virtual void onSxmListModeStatus(
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::SxmListModeStatus >& status);

      virtual void onFuelInfoListsStatusError(
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::FuelInfoListsStatusError >& error);
      virtual void onFuelInfoListsStatusStatus(
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::FuelInfoListsStatusStatus >& status);

      virtual void onFuelBrandNameListError(
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::FuelBrandNameListError >& error);
      virtual void onFuelBrandNameListStatus(
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::FuelBrandNameListStatus >& status);

      virtual void onFuelTypeListError(
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::FuelTypeListError >& error);
      virtual void onFuelTypeListStatus(
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::FuelTypeListStatus >& status);

      virtual void onSxmDataServiceStatusError(
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::SxmDataServiceStatusError >& error);
      virtual void onSxmDataServiceStatusStatus(
         const ::boost::shared_ptr<MIDW_EXT_SXM_FUEL_FI:: MIDW_EXT_SXM_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::SxmDataServiceStatusStatus >& status);

      virtual void onGetFuelStationInfoError(
         const ::boost::shared_ptr<  MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr<  MIDW_EXT_SXM_FUEL_FI::GetFuelStationInfoError >& error);
      virtual void onGetFuelStationInfoResult(
         const ::boost::shared_ptr<  MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr<  MIDW_EXT_SXM_FUEL_FI:: GetFuelStationInfoResult >& result);

      virtual void onCanadianFuelInfoListsStatusError(
         const ::boost::shared_ptr<MIDW_EXT_SXM_CANADIAN_FUEL_FI::MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::CanadianFuelInfoListsStatusError >& error);
      virtual void onCanadianFuelInfoListsStatusStatus(
         const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::CanadianFuelInfoListsStatusStatus >& status);

      virtual void onCanadianFuelBrandNameListError(
         const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr<MIDW_EXT_SXM_CANADIAN_FUEL_FI:: CanadianFuelBrandNameListError >& error);

      virtual void onCanadianFuelBrandNameListStatus(
         const ::boost::shared_ptr<MIDW_EXT_SXM_CANADIAN_FUEL_FI:: MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr<MIDW_EXT_SXM_CANADIAN_FUEL_FI:: CanadianFuelBrandNameListStatus >& status);

      virtual void onCanadianFuelTypeListError(const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& proxy,
            const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::CanadianFuelTypeListError >& error);
      virtual void onCanadianFuelTypeListStatus(const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& proxy,
            const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::CanadianFuelTypeListStatus >& status);

      virtual void onSxmListModeError(const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& proxy,
                                      const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::SxmListModeError >& error);
      virtual void onSxmListModeStatus(const ::boost::shared_ptr<MIDW_EXT_SXM_CANADIAN_FUEL_FI:: MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& proxy,
                                       const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::SxmListModeStatus >& status);

      virtual void onSxmDataServiceStatusError(
         const ::boost::shared_ptr<MIDW_EXT_SXM_CANADIAN_FUEL_FI::MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::SxmDataServiceStatusError >& error);
      virtual void onSxmDataServiceStatusStatus(
         const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr<MIDW_EXT_SXM_CANADIAN_FUEL_FI:: SxmDataServiceStatusStatus >& status);

      virtual void onGetCanadianFuelInfoListError(
         const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::GetCanadianFuelInfoListError >& error);

      virtual void onGetCanadianFuelInfoListResult(
         const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::GetCanadianFuelInfoListResult >& result);

      virtual void onGetCanadianFuelStationInfoError(
         const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::GetCanadianFuelStationInfoError >& error);

      virtual void onGetCanadianFuelStationInfoResult(
         const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy >& proxy,
         const ::boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::GetCanadianFuelStationInfoResult >& result);

      virtual void onPositionInformationError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::PositionInformationError >& error);
      virtual void onPositionInformationUpdate(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::PositionInformationUpdate >& update);

      virtual void onStartGuidanceToPosWGS84Error(
         const ::boost::shared_ptr<org::bosch::cm::navigation::NavigationService::NavigationServiceProxy>& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::StartGuidanceToPosWGS84Error >& error);

      virtual void onStartGuidanceToPosWGS84Response(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::StartGuidanceToPosWGS84Response >& response);

      virtual void onSetLocationWithCoordinatesError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::SetLocationWithCoordinatesError >& error);

      virtual void onSetLocationWithCoordinatesResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::SetLocationWithCoordinatesResponse >& response);

      tBool bSelectElement(tU32 u32SelectedIndex);
      std::vector<clSDS_ListItems> oGetItems(tU32 u32StartIndex, tU32 u32EndIndex);

   private:
      void vStartGuidance();
      void vAddasWayPoint();
      void onFuelSortByPriceRegion();
      void onFuelSortByDistanceRegion();
      void onStartUSAFuelPriceList(midw_ext_sxm_fuel_fi_types::T_e8_SortType sortType);
      void onStartCanadianFuelPriceList(midw_ext_sxm_canadian_fuel_fi_types::T_e8_CanFuelSortType sortType);
      void vUpdateHeadLineTagForFuelPrice() const;
      void onStartStationInfoByRegion(unsigned long selectedIndex);

      std::string oGetFuelPriceValue(tU32 u32Index) const;
      std::string oGetFuelAge(tU8 u8FuelAge) const;
      clSDS_ListItems::tenColorofText enGetFuelAgeColor(tU8 u8FuelAge) const;
      clSDS_ListItems oGetListItem(tU32 u32Index) const;
      std::string oGetBrandOrFuelStationName(tU32 u32Index) const;
      std::string oGetDistance(tU32  u32Index) const;
      std::string oGetLastPriceUpdate(tU32 u32Index) const;
      clSDS_ListItems::tenColorofText enGetLastPriceTextColor(tU32 u32Index) const;
      unsigned char oGetDirectionGUI(tU32 u32Index)const;
      std::string oGetDistanceUnit(enDistanceUnits u8DistanceUnit)const;
      enHeadingType oGetDirection(int dLatitude, int dLongitude)const;
      tVoid vGetListInfo(sds2hmi_fi_tcl_e8_HMI_ListType::tenType);
      unsigned long oGetSelectedIndex() const;

      void oSetFuelListCount(unsigned long u32FuelStationCount);
      void oSetUSAFuelInfoList(const midw_ext_sxm_fuel_fi_types::T_FuelInfoList& fuelInfoList);
      void oSetCanadianFuelInfoList(const midw_ext_sxm_canadian_fuel_fi_types::T_CanFuelInfoList& canadianFuelInfoList);
      void oClearFuelListItems();
      std::string vConvertDistaneIntoString(unsigned long u32DistToPOIMts);
      double vConvertlatlongTodouble(signed int latlong)const;
      bool bCheckLocationBoundary(double lat, double lon)const;
      double vCalculateAirDistanceWithDirection(double dDestinationLatitude,
            double dDestinationLongitude,
            double dCurrentLatitude,
            double dCurrentLongitude)const;
      double vRadians2degrees(double angle_degrees)const;
      double vDegrees2radians(double angle_degrees)const;
      double carRelativeDirection(double currentDirection, double carDirection)const;
      int formatDirectionInfo(double dActualAngle)const;
      double makeCircular(double degree)const;

      boost::shared_ptr< MIDW_EXT_SXM_FUEL_FI::MIDW_EXT_SXM_FUEL_FIProxy > _sxmFuelProxy;
      boost::shared_ptr< MIDW_EXT_SXM_CANADIAN_FUEL_FI::MIDW_EXT_SXM_CANADIAN_FUEL_FIProxy > _sxmCanadianFuelProxy;
      boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > _naviProxy;
      unsigned long _fuelListCount;
      enDistanceUnits _distanceUnits;
      std::string _currentCountryCode;
      unsigned long _fuelListNumberPOI;
      unsigned char _vehicleEngineType;
      clSDS_KDSConfiguration::VehicleDistanceUnitType _vehicleDistanceUnits;
      std::vector<stFuelListItems> _fuelListItems;
      stFuelStation _fuelStationInfo;
};


#endif
