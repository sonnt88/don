/*********************************************************************//**
 * \file       clSDS_UserwordProfileStore.h
 *
 * Userwords related to one phone profile
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_UserwordProfileStore_h
#define clSDS_UserwordProfileStore_h


#include "application/clSDS_Userword_Entry.h"

#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"

#define AHL_S_IMPORT_INTERFACE_CONFIG_STORE
#include "ahl_if.h"


class clSDS_UserwordProfileStore: public ahl_tclBaseConfigStore
{
   public:
      virtual ~clSDS_UserwordProfileStore();
      clSDS_UserwordProfileStore(tU32 u32ProfileNum);

      tVoid vLoad(bpstl::vector<clSDS_Userword_Entry>& oUserwordList);
      tVoid vSave(const bpstl::vector<clSDS_Userword_Entry>& oUserwordList);

   private:

      // passes an array pointer to a byte stream of stored values
      virtual tVoid vSetUserData(tPCU8 pcu8Buf, tU32 u32Length);
      // requests a pointer to an array, containing the values to be stored
      virtual tVoid vGetUserData(tPCU8& rpcu8Buf, tU32& ru32Length);
      // an array, containing the default values for this configuration files
      virtual tVoid vGetUserDefaultData(tPCU8& rpcu8Buf, tU32& ru32Length);
      // calculates total buffer size by summarizing sizes of each element
      tU32 u32GetUserDataBufferSize();

      // --------- intentionally kept empty
      virtual tVoid vSetMachineData(tPCU8 /*pcu8Buf*/, tU32 /*u32Length*/) {};
      virtual tVoid vGetMachineData(tPCU8& /*rpcu8Buf*/, tU32& /*ru32Length*/) {};
      virtual tVoid vGetMachineDefaultData(tPCU8& /*rpcu8Buf*/, tU32& /*ru32Length*/) {};
      // --------- intentionally kept empty

      bpstl::vector<clSDS_Userword_Entry>* _pUserwordList;
};


#endif
