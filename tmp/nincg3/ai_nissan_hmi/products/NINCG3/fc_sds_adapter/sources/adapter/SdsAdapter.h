/*********************************************************************//**
 * \file       SdsAdapter.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef SdsAdapter_h
#define SdsAdapter_h


#include "asf/core/BaseComponent.h"
#include "Sds2HmiServer/Sds2HmiServer.h"


class Sds2HmiServer;


namespace adapter {


class SdsAdapter : public asf::core::BaseComponent
{
   public:
      SdsAdapter();
      virtual ~SdsAdapter();

   private:
      Sds2HmiServer _sds2HmiServer;
};


}


#endif
