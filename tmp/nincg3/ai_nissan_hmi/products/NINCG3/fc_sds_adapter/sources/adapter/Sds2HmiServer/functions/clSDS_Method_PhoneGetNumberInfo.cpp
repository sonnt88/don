/*********************************************************************//**
 * \file       clSDS_Method_PhoneGetNumberInfo.cpp
 *
 * clSDS_Method_PhoneGetNumberInfo method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_PhoneGetNumberInfo.h"
#include "external/sds2hmi_fi.h"
#include "application/clSDS_Userwords.h"
#include "application/clSDS_StringVarList.h"

#include "SdsAdapter_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_Method_PhoneGetNumberInfo.cpp.trc.h"
#endif
/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_PhoneGetNumberInfo::~clSDS_Method_PhoneGetNumberInfo()
{
   _pUserwordsNameAndNumberInfo = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_PhoneGetNumberInfo::clSDS_Method_PhoneGetNumberInfo(ahl_tclBaseOneThreadService* pService,
      clSDS_Userwords* pUserword, boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy > pPhoneBkProxy)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_PHONEGETNUMBERINFO, pService)
   , _phoneBookProxy(pPhoneBkProxy)
{
   _pUserwordsNameAndNumberInfo = pUserword;
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_PhoneGetNumberInfo::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgPhoneGetNumberInfoMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   vGetCallerNameAndNumberForUserword(oMessage.nUswID);
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_PhoneGetNumberInfo::vGetCallerNameAndNumberForUserword(uint32 userwordID)
{
   if (_phoneBookProxy->isAvailable())
   {
      _phoneBookProxy->sendGetContactDetailsExtendedStart(*this, userwordID);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneGetNumberInfo::onGetContactDetailsExtendedError(
   const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedError >& /*error*/)
{
   vSendErrorMessage(SDS2HMI_SDSFI_C_U16_ERROR_CANNOTCOMPLETEACTION);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Method_PhoneGetNumberInfo::onGetContactDetailsExtendedResult(
   const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsExtendedResult >& result)
{
   sds2hmi_sdsfi_tclMsgPhoneGetNumberInfoMethodResult oMessage;
   std::string strName  = result->getOContactDetailsExtended().getSFirstName();
   if (result->getOContactDetailsExtended().getSLastName() != "")
   {
      strName.append(" ");
      strName.append(result->getOContactDetailsExtended().getSLastName());
   }
   clSDS_StringVarList::vSetVariable("$(UW)", strName);
   ETG_TRACE_USR1(("clSDS_Method_PhoneGetNumberInfo: Contact Name : %s", strName.c_str()));
   oMessage.sResult.bSet(strName.c_str(), sds2hmi_fi_tclString::FI_EN_UTF8);
   vSendMethodResult(oMessage);
}
