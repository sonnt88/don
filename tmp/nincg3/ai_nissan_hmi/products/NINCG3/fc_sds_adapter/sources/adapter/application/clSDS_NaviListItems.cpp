/*********************************************************************//**
 * \file       clSDS_NaviListItems.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "clSDS_NaviListItems.h"
#include "application/clSDS_NaviList.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_NaviListItems::~clSDS_NaviListItems()
{
   _pNaviList = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_NaviListItems::clSDS_NaviListItems()
   : _pNaviList(NULL)
   , _enCurrentNaviListType(EN_NAVI_LIST_UNKNOWN)
{
}


/**************************************************************************//**
* set Navilist type as commonGetListInfoItems(HMI List Type).
******************************************************************************/
void clSDS_NaviListItems::vSetNaviHMIListType(sds2hmi_fi_tcl_e8_HMI_ListType::tenType listType)
{
   std::map<sds2hmi_fi_tcl_e8_HMI_ListType::tenType, clSDS_NaviList*>::iterator it;
   it = _oNaviListMap.find(listType);
   _pNaviList = it->second;

   switch (listType)
   {
      case sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_NAV_PREV_DESTINATIONS:
         _enCurrentNaviListType = EN_NAVI_LIST_PREV_DESTINATIONS;
         break;
      case sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_FUEL_PRICES:
      case sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_FUEL_DISTANCES:
         _enCurrentNaviListType = EN_NAVI_LIST_FUEL_PRICES;
         break;
      case sds2hmi_fi_tcl_e8_HMI_ListType::FI_EN_LIST_ADDRESSBOOK:
         _enCurrentNaviListType = EN_NAVI_LIST__ADDRESSBOOK;
         break;
      default:
         _enCurrentNaviListType = EN_NAVI_LIST_UNKNOWN;
         _pNaviList = NULL;
         break;
   }
}


/**************************************************************************//**
*  set Navi ListType form NaviDestListType
******************************************************************************/
void clSDS_NaviListItems::vSetNaviDestListType(sds2hmi_fi_tcl_e8_NAV_ListType::tenType listType)
{
   _pNaviList = NULL;
   switch (listType)
   {
      case sds2hmi_fi_tcl_e8_NAV_ListType::FI_EN_HOME_DEST:
         _enCurrentNaviListType = EN_NAVI_LIST_HOME_DESTINATION;
         break;
      case sds2hmi_fi_tcl_e8_NAV_ListType::FI_EN_WORK_DEST:
         _enCurrentNaviListType = EN_NAVI_LIST_WORK_DESTINATION;
         break;
      default:
         _enCurrentNaviListType = EN_NAVI_LIST_UNKNOWN;
         break;
   }
}


/**************************************************************************//**
 *  Set Navi list type as form NaviCriterionList
******************************************************************************/
void clSDS_NaviListItems::vSetNaviSelectionCriterionListType(sds2hmi_fi_tcl_e16_SelectionCriterionType::tenType listType)
{
   _pNaviList = NULL;
   switch (listType)
   {
      case sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_ADDRESSES:
      case sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_HOUSENUMBER:
      case sds2hmi_fi_tcl_e16_SelectionCriterionType::FI_EN_OSDE_POI:
         _enCurrentNaviListType = EN_NAVI_LIST_OSDE_ADDRESSES;
         break;
      default:
         _enCurrentNaviListType = EN_NAVI_LIST_UNKNOWN;
         break;
   }
}


/**************************************************************************//**
*  Get list type as Navi ListType
******************************************************************************/
clSDS_NaviListItems::enNaviListType clSDS_NaviListItems::vGetNaviListType() const
{
   return _enCurrentNaviListType;
}


/**************************************************************************//**
*  reset Navi List Type.
******************************************************************************/
void clSDS_NaviListItems::vResetNaviListType()
{
   _enCurrentNaviListType = EN_NAVI_LIST_UNKNOWN;
   _pNaviList = NULL;
}


/**************************************************************************//**
*  Navi Items Start Guidances.
******************************************************************************/
void  clSDS_NaviListItems::vStartGuidance()
{
   if (_pNaviList)
   {
      _pNaviList->vStartGuidance();
   }
}


/**************************************************************************//**
*  Add to list NaviItems.
******************************************************************************/
void clSDS_NaviListItems::vAddList(sds2hmi_fi_tcl_e8_HMI_ListType::tenType lNaviListType, clSDS_NaviList* lNaviList)
{
   if (lNaviList != NULL)
   {
      _oNaviListMap[lNaviListType] = lNaviList;
   }
}


/**************************************************************************//**
*   NaviItems Add as way Point
******************************************************************************/
void clSDS_NaviListItems::vAddasWayPoint()
{
   if (_pNaviList)
   {
      _pNaviList->vAddasWayPoint();
   }
}
