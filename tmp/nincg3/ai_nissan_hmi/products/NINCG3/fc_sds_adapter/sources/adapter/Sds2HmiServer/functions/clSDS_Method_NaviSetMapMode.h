/*********************************************************************//**
 * \file       clSDS_Method_NaviSetMapMode.h
 *
 * NaviSetMapMode method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/


#ifndef clSDS_Method_NaviSetMapMode_h
#define clSDS_Method_NaviSetMapMode_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "asf/core/Proxy.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"
#include "application/GuiService.h"


class clSDS_Method_NaviSetMapMode
   : public clServerMethod
   , public asf::core::ServiceAvailableIF
   , public org::bosch::cm::navigation::NavigationService::MapRepresentationCallbackIF
   , public org::bosch::cm::navigation::NavigationService::SetMapRepresentationCallbackIF
   , public org::bosch::cm::navigation::NavigationService::ShowMapScreenWithMapViewModeCallbackIF
   , public org::bosch::cm::navigation::NavigationService::SetPoiIconCategoriesInMapCallbackIF
   , public org::bosch::cm::navigation::NavigationService::GetPoiIconCategoriesInMapCallbackIF
{
   public:
      virtual ~clSDS_Method_NaviSetMapMode();
      clSDS_Method_NaviSetMapMode(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > pSds2NaviProxy,
         GuiService& guiService);

      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onMapRepresentationError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::MapRepresentationError >& error);

      virtual void onMapRepresentationUpdate(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::MapRepresentationUpdate >& update);

      virtual void onSetMapRepresentationError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::SetMapRepresentationError >& error);

      virtual void onSetMapRepresentationResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::SetMapRepresentationResponse >& response);

      virtual void onShowMapScreenWithMapViewModeError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::ShowMapScreenWithMapViewModeError >& error);

      virtual void onShowMapScreenWithMapViewModeResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::ShowMapScreenWithMapViewModeResponse >& response);

      virtual void onGetPoiIconCategoriesInMapError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::GetPoiIconCategoriesInMapError >& error);

      virtual void onGetPoiIconCategoriesInMapResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::GetPoiIconCategoriesInMapResponse >& response);

      virtual void onSetPoiIconCategoriesInMapError(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::SetPoiIconCategoriesInMapError >& error);

      virtual void onSetPoiIconCategoriesInMapResponse(
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::SetPoiIconCategoriesInMapResponse >& response);

   private:
      enum MapmodeType
      {
         UNKNOWN = 0,
         VIEW_2D = 1,
         VIEW_3D = 2,
         NORTH_UP = 3,
         HEADING = 4
      };

      enum POIIconReqType
      {
         UNKNOWNTYPE = 0,
         ShowIcon = 1,
         RemoveIcon = 2,
      };

      virtual void vMethodStart(amt_tclServiceData* pInMsg);
      void vGetMapOrientationfromSDS(const sds2hmi_sdsfi_tclMsgNaviSetMapModeMethodStart& oMessage);
      void vGetTargetMapOrientation(MapmodeType enMapmodeType);
      void vSendMapOrientationtoNavi(org::bosch::cm::navigation::NavigationService::MapRepresentation enTargetMapRepresentation);
      void vSendMapviewRequest(const sds2hmi_fi_tcl_e8_NAV_TBT_Symbols& oNavTBTSymbols);
      void vHandlePOIIconRequest(const sds2hmi_fi_tcl_Nav_IconSetting& oNavIconSettings);
      void vSendShowallPOIIconRequest();
      void vSendShowPOIIconCategoryRequest(const sds2hmi_fi_tcl_e16_SelectionCriterionType& oSelectionCriteriontype);
      void vSendRemoveallPOIIconRequest();
      void vSendRemovePOIIconCategoryRequest(const sds2hmi_fi_tcl_e16_SelectionCriterionType& oSelectionCriteriontype);
      void vGetPOIIConCategory(const sds2hmi_fi_tcl_e16_SelectionCriterionType&	oSelectioncriteriontype,
                               org::bosch::cm::navigation::NavigationService::MapPOIIconCategory& enmappoiicons) const;
      boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy > _naviProxy;
      GuiService& _guiService;
      org::bosch::cm::navigation::NavigationService::MapRepresentation _mapRepresentation ;
      sds2hmi_fi_tcl_e16_SelectionCriterionType _selectionCriteriontype;
      POIIconReqType	_poiIconReq;
};


#endif
