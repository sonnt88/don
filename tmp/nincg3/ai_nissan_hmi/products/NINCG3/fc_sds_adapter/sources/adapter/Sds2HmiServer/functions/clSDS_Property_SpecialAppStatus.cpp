/*********************************************************************//**
 * \file       clSDS_Property_SpecialAppStatus.cpp
 *
 * Common Action Request property implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Property_SpecialAppStatus.h"
#include "external/sds2hmi_fi.h"


/**************************************************************************//**
 * Destructor
 ******************************************************************************/
clSDS_Property_SpecialAppStatus::~clSDS_Property_SpecialAppStatus()
{
}


/**************************************************************************//**
 * Constructor
 ******************************************************************************/
clSDS_Property_SpecialAppStatus::clSDS_Property_SpecialAppStatus(
   ahl_tclBaseOneThreadService* pService,
   ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > pSds2TelProxy)
   : clServerProperty(SDS2HMI_SDSFI_C_U16_SPECIALAPPSTATUS, pService)
   , _telephoneProxy(pSds2TelProxy)
{
   _siriStatus = 0;
   _voiceRecActive = false;
   _voiceRecSupported = false;
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_SpecialAppStatus::vSet(amt_tclServiceData* /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_SpecialAppStatus::vGet(amt_tclServiceData* /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_SpecialAppStatus::vUpreg(amt_tclServiceData* /*pInMsg*/)
{
   vSendStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
tVoid clSDS_Property_SpecialAppStatus::vSendStatus()
{
   sds2hmi_sdsfi_tclMsgSpecialAppStatusStatus oMessage;
   switch (_siriStatus)
   {
      case 0:
         if (_voiceRecSupported)
         {
            oMessage.AppTypeAndStatus.SplAppId.enType = sds2hmi_fi_tcl_e8_SpecialAppId::FI_EN_APP_VOICEASSIST;
            oMessage.AppTypeAndStatus.SplAppStatus.enType = sds2hmi_fi_tcl_e8_SpecialAppStatus::FI_EN_APP_STAT_AVAIALABLE_ACTIVE;
         }
         break;

      case 1:
         oMessage.AppTypeAndStatus.SplAppId.enType = sds2hmi_fi_tcl_e8_SpecialAppId::FI_EN_APP_SIRI;
         oMessage.AppTypeAndStatus.SplAppStatus.enType = sds2hmi_fi_tcl_e8_SpecialAppStatus::FI_EN_APP_STAT_AVAIALABLE_ACTIVE;
         break;

      case 2:
         oMessage.AppTypeAndStatus.SplAppId.enType = sds2hmi_fi_tcl_e8_SpecialAppId::FI_EN_APP_SIRI;
         oMessage.AppTypeAndStatus.SplAppStatus.enType = sds2hmi_fi_tcl_e8_SpecialAppStatus::FI_EN_APP_STAT_AVAILABLE_NOT_ACTIVE;
         break;

      default:
         oMessage.AppTypeAndStatus.SplAppId.enType = sds2hmi_fi_tcl_e8_SpecialAppId::FI_EN_APP_UNKNOWN;
         oMessage.AppTypeAndStatus.SplAppStatus.enType = sds2hmi_fi_tcl_e8_SpecialAppStatus::FI_EN_APP_STAT_UNKNOWN;
         break;
   }
   vStatus(oMessage);
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_SpecialAppStatus::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _telephoneProxy)
   {
      _telephoneProxy->sendBTDeviceVoiceRecognitionExtendedUpReg(*this);
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_SpecialAppStatus::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _telephoneProxy)
   {
      _telephoneProxy->sendBTDeviceVoiceRecognitionRelUpRegAll();
   }
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_SpecialAppStatus::onBTDeviceVoiceRecognitionExtendedStatus(
   const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_Tel_FI::BTDeviceVoiceRecognitionExtendedStatus >& status)
{
   _voiceRecActive = status->getBBTDeviceVoiceRecActive();
   _voiceRecSupported = status->getBBTDeviceVoiceRecSupported();
   _siriStatus = uint8(status->getE8SiriAvailabilityState());
   vSendStatus();
}


/**************************************************************************//**
 *
 ******************************************************************************/
void clSDS_Property_SpecialAppStatus::onBTDeviceVoiceRecognitionExtendedError(
   const ::boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy >& /*proxy*/,
   const ::boost::shared_ptr< MOST_Tel_FI::BTDeviceVoiceRecognitionExtendedError >& /*error*/)
{
}
