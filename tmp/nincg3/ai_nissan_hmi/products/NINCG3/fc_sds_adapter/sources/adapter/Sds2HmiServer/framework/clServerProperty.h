/*********************************************************************//**
 * \file       clServerProperty.h
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clServerProperty_h
#define clServerProperty_h


#include "Sds2HmiServer/framework/clFunction.h"

#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#include "stl_pif.h"

#define AIL_S_IMPORT_INTERFACE_GENERIC
#include "ail_if.h"

#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#include "fi_msgfw_if.h"


class clServerProperty : public clFunction
{
   public:
      virtual ~clServerProperty();
      clServerProperty(tU16 u16FunctionID, ahl_tclBaseOneThreadService* pService);
      virtual tVoid vHandleMessage(amt_tclServiceData* pInMsg);

   protected:
      virtual tVoid vGet(amt_tclServiceData* pInMsg) = 0;
      virtual tVoid vSet(amt_tclServiceData* pInMsg) = 0;
      virtual tVoid vUpreg(amt_tclServiceData* pInMsg) = 0;
      tVoid vStatus(const fi_tclTypeBase& oPayload);

   private:
      ahl_tclNotificationTable _notificationTable;
      tU16 _u16DestAppID;
      tU16 _u16RegisterID;
      tU16 _u16RequestCmdCtr;
      tU32 _u32RequestType;
      tVoid vAddToNotificationTable(const amt_tclServiceData* pInMessage);
      tVoid vRemoveFromNotificationTable(const amt_tclServiceData* pInMessage);
      tVoid vStatusToRegisteredClients(const fi_tclTypeBase& oOutData);
      tVoid vSaveResponseParameters(const amt_tclServiceData* pInMessage);
};


#endif
