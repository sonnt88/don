/*********************************************************************//**
 * \file       clSDS_Method_NaviStartGuidance.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_NaviStartGuidance_h
#define clSDS_Method_NaviStartGuidance_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"
#include "application/GuiService.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"


using namespace org::bosch::cm::navigation::NavigationService;


class clSDS_NaviListItems;


class clSDS_Method_NaviStartGuidance
   : public clServerMethod
   , public StartGuidanceCallbackIF
   , public StartGuidanceToHomeLocationCallbackIF
   , public StartGuidanceToWorkLocationCallbackIF

{
   public:
      virtual ~clSDS_Method_NaviStartGuidance();
      clSDS_Method_NaviStartGuidance(
         ahl_tclBaseOneThreadService* pService,
         GuiService& guiService,
         clSDS_NaviListItems* pNaviListItems,
         ::boost::shared_ptr<NavigationServiceProxy> pNaviProxy);

      virtual void onStartGuidanceError(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< StartGuidanceError >& error);
      virtual void onStartGuidanceResponse(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< StartGuidanceResponse >& response);

      virtual void onStartGuidanceToHomeLocationError(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< StartGuidanceToHomeLocationError >& error);
      virtual void onStartGuidanceToHomeLocationResponse(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< StartGuidanceToHomeLocationResponse >& response);

      virtual void onStartGuidanceToWorkLocationError(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< StartGuidanceToWorkLocationError >& error);
      virtual void onStartGuidanceToWorkLocationResponse(
         const ::boost::shared_ptr< NavigationServiceProxy >& proxy,
         const ::boost::shared_ptr< StartGuidanceToWorkLocationResponse >& response);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      clSDS_NaviListItems* _pNaviListItems;
      GuiService& _guiService;
      boost::shared_ptr< NavigationServiceProxy > _navigationProxy;
};


#endif
