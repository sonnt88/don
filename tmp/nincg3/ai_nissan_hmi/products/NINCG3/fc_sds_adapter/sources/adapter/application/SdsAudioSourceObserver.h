/*********************************************************************//**
 * \file       SdsAudioSourceObserver.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef SdsAudioSourceObserver_h
#define SdsAudioSourceObserver_h

#define ARL_S_ALREADY_INCLUDE_ASF_STR
#include "include/arl_tclISource_ASF.h"

class SdsAudioSourceObserver
{
   public:
      SdsAudioSourceObserver();
      virtual ~SdsAudioSourceObserver();

      virtual void onAudioSourceStateChanged(arl_tenActivity state) = 0;
};


#endif /* SdsAudioSourceObserver_h */
