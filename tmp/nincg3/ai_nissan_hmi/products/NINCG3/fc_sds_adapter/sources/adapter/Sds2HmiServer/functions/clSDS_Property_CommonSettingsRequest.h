/*********************************************************************//**
 * \file       clSDS_Property_CommonSettingsRequest.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Property_CommonSettingsRequest_h
#define clSDS_Property_CommonSettingsRequest_h


#include "Sds2HmiServer/framework/clServerProperty.h"
#include "application/clSDS_PromptModeObserver.h"

class SettingsService;


class clSDS_Property_CommonSettingsRequest : public clServerProperty,
   public clSDS_PromptModeObserver
{
   public:
      virtual ~clSDS_Property_CommonSettingsRequest();
      clSDS_Property_CommonSettingsRequest(ahl_tclBaseOneThreadService* pService, SettingsService* settingsService);
      virtual void promptModeChanged(bool promptMode);

   protected:
      virtual tVoid vGet(amt_tclServiceData* pInMsg);
      virtual tVoid vSet(amt_tclServiceData* pInMsg);
      virtual tVoid vUpreg(amt_tclServiceData* pInMsg);

   private:
      tVoid vSendStatus();
      bool _promptMode;
      sds2hmi_fi_tclU32* _pofiU32Value ;
      sds2hmi_fi_tcl_SDS_SettingsUnion oSettingsUnion;
};


#endif
