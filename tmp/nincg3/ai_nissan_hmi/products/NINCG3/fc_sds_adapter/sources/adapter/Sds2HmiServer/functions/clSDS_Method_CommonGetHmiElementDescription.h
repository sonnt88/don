/*********************************************************************//**
 * \file       clSDS_Method_CommonGetHmiElementDescription.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_CommonGetHmiElementDescription_h
#define clSDS_Method_CommonGetHmiElementDescription_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "external/sds2hmi_fi.h"


class clSDS_MenuManager;
class clSDS_ListScreen;

class clSDS_Method_CommonGetHmiElementDescription : public clServerMethod
{
   public:
      virtual ~clSDS_Method_CommonGetHmiElementDescription();
      clSDS_Method_CommonGetHmiElementDescription(
         ahl_tclBaseOneThreadService* pService,
         clSDS_MenuManager* pSDS_MenuManager,
         clSDS_ListScreen* poListScreen);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      void getHmiDescription(
         sds2hmi_sdsfi_tclMsgCommonGetHMIElementDescriptionMethodStart& oMessage,
         sds2hmi_sdsfi_tclMsgCommonGetHMIElementDescriptionMethodResult& oResult);
      clSDS_MenuManager* _pSDS_MenuManager;
      clSDS_ListScreen* m_poListScreen;
};


#endif
