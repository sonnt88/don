/*********************************************************************//**
 * \file       clSDS_Method_TunerSelectBandMemBank.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_TunerSelectBandMemBank_h
#define clSDS_Method_TunerSelectBandMemBank_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "bosch/cm/ai/hmi/masteraudioservice/AudioSourceChangeProxy.h"
#include "application/GuiService.h"
#include "external/sds2hmi_fi.h"


#define NS_AUDIOSRCCHG           bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange


class clSDS_Method_TunerSelectBandMemBank
   : public clServerMethod
   , public NS_AUDIOSRCCHG::ActivateSourceCallbackIF
{
   public:
      virtual ~clSDS_Method_TunerSelectBandMemBank();
      clSDS_Method_TunerSelectBandMemBank(
         ahl_tclBaseOneThreadService* pService,
         boost::shared_ptr< NS_AUDIOSRCCHG::AudioSourceChangeProxy > proxy,
         GuiService& guiService);

      virtual void onActivateSourceError(
         const ::boost::shared_ptr< NS_AUDIOSRCCHG::AudioSourceChangeProxy >& proxy,
         const ::boost::shared_ptr< NS_AUDIOSRCCHG::ActivateSourceError >& error);
      virtual void onActivateSourceResponse(
         const ::boost::shared_ptr< NS_AUDIOSRCCHG::AudioSourceChangeProxy >& proxy,
         const ::boost::shared_ptr< NS_AUDIOSRCCHG::ActivateSourceResponse >& response);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      void vActivateTunerSource(const sds2hmi_fi_tcl_e8_TUN_Band& band);
      boost::shared_ptr< NS_AUDIOSRCCHG::AudioSourceChangeProxy > _audioSrcChgProxy;
      GuiService& _guiService;
      NS_AUDIOSRCCHG::sourceData _srcData;
};


#endif
