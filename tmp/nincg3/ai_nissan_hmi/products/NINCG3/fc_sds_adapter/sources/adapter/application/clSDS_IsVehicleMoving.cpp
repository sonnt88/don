/*********************************************************************//**
 * \file       clSDS_IsVehicleMoving.cpp
 *
 * clSDS_IsVehicleMoving class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "application/clSDS_IsVehicleMoving.h"
#include "application/clSDS_IsVehicleMovingObserver.h"


/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_IsVehicleMoving::~clSDS_IsVehicleMoving()
{
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_IsVehicleMoving::clSDS_IsVehicleMoving()
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_IsVehicleMoving::vRegisterObserver(clSDS_IsVehicleMovingObserver* pObserver)
{
   if (pObserver != NULL)
   {
      _observers.push_back(pObserver);
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_IsVehicleMoving::vNotifyObservers()
{
   bpstl::vector<clSDS_IsVehicleMovingObserver*>::iterator iter = _observers.begin();
   while (iter != _observers.end())
   {
      if (*iter != NULL)
      {
         (*iter)->vVehicleSpeedChanged();
      }
      ++iter;
   }
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_IsVehicleMoving::vOnVehicleSpeedChange()
{
   vNotifyObservers();
}
