/*********************************************************************//**
 * \file       Sds_ViewDB.h
 *
 * Exported interface of the view data base.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef Sds_ViewDB_h
#define Sds_ViewDB_h


//lint -efile(451,Sds_ViewDB.dat) repeatedly included but does not have a standard include guard - jnd2hi


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#define SYSTEM_S_IMPORT_INTERFACE_STRING
#define SYSTEM_S_IMPORT_INTERFACE_MAP
#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#include "stl_pif.h"


enum Sds_ViewId
{
#define SDS_VIEW(id, layout, headline, helpline, info)               id,
#include "Sds_ViewDB.dat"
#undef SDS_VIEW
   SDS_VIEW_ID_LIMIT
};


enum Sds_ConditionId
{
#define SDS_COND(id, expression)                                     id,
#include "Sds_ViewDB.dat"
#undef SDS_COND
   SDS_CONDITION_ID_LIMIT
};


enum Sds_HeaderTemplateId
{
#define SDS_HEADER_TEMPLATES(id, textId, templateName)               id,
#include "Sds_ViewDB.dat"
#undef SDS_HEADER_TEMPLATES
   SDS_HEADER_TEMPLATE_ID_LIMIT
};


struct SelectableText
{
   std::string text;
   bool isSelectable;
};


const tChar* Sds_ViewDB_pcGetName(Sds_ViewId enViewId);
tChar Sds_ViewDB_cGetLayout(Sds_ViewId enViewId);
bpstl::string Sds_ViewDB_oGetTemplateString(Sds_HeaderTemplateId enTemplateId);
bpstl::string Sds_ViewDB_oGetHeadline(Sds_ViewId enViewId);
bpstl::vector<SelectableText> Sds_ViewDB_oGetCommandList(bpstl::map<bpstl::string, bpstl::string> const& conditions, Sds_ViewId enViewId);
Sds_ViewId Sds_ViewDB_enFindViewId(const bpstl::string& oScreenId);
Sds_HeaderTemplateId Sds_ViewDB_enFindTemplateId(const bpstl::string& oTemplateId);
tBool Sds_ViewDB_bIsConditionTrue(Sds_ConditionId conditionId, bpstl::map<bpstl::string, bpstl::string> const& variables);
bpstl::vector<bpstl::string> Sds_ViewDB_oGetSubCommandList(bpstl::map<bpstl::string, bpstl::string> const& variables, Sds_ViewId viewId, tU32 cursorIndex);
bpstl::string Sds_ViewDB_oGetHelpline(Sds_ViewId enViewId);
bpstl::string Sds_ViewDB_oGetInfo(Sds_ViewId enViewId);
std::string Sds_ViewDB_getGrammarId(Sds_ViewId enViewId, unsigned int itemIndex, bpstl::map<bpstl::string, bpstl::string> screenVariableData);
std::string Sds_ViewDB_getPromptId(Sds_ViewId enViewId, unsigned int itemIndex, bpstl::map<bpstl::string, bpstl::string> screenVariableData);


#endif
