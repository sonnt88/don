/*********************************************************************//**
 * \file       clSDS_Method_TextMsgSelectMessage.cpp
 *
 * clSDS_Method_TextMsgSelectMessage method class implementation
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "Sds2HmiServer/functions/clSDS_Method_TextMsgSelectMessage.h"
#include "external/sds2hmi_fi.h"

/**************************************************************************//**
* Destructor
******************************************************************************/
clSDS_Method_TextMsgSelectMessage::~clSDS_Method_TextMsgSelectMessage()
{
   _pReadSmsList = NULL;
}


/**************************************************************************//**
* Constructor
******************************************************************************/
clSDS_Method_TextMsgSelectMessage::clSDS_Method_TextMsgSelectMessage(ahl_tclBaseOneThreadService* pService,  clSDS_ReadSmsList* pSmsList)
   : clServerMethod(SDS2HMI_SDSFI_C_U16_TEXTMSGSELECTMESSAGE, pService)
   , _pReadSmsList(pSmsList)
{
}


/**************************************************************************//**
*
******************************************************************************/
tVoid clSDS_Method_TextMsgSelectMessage::vMethodStart(amt_tclServiceData* pInMessage)
{
   sds2hmi_sdsfi_tclMsgTextMsgSelectMessageMethodStart oMessage;
   vGetDataFromAmt(pInMessage, oMessage);
   switch (oMessage.nSelectionType.enType)
   {
      case sds2hmi_fi_tcl_e8_GEN_SelectionType::FI_EN_NEXT:
         _pReadSmsList->selectNextMessage();
         break;

      case sds2hmi_fi_tcl_e8_GEN_SelectionType::FI_EN_PREV:
         _pReadSmsList->selectPreviousMessage();
         break;

      default:
         break;
   }
   vSendMethodResult();
}
