/*********************************************************************//**
 * \file       clSDS_PhonebookList.h
 *
 * List Phonebook functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_PhonebookList_h
#define clSDS_PhonebookList_h

#include "legacy/HSI_PHONE_SDS_Interface.h"
#include "MOST_PhonBk_FIProxy.h"

#define SYSTEM_S_IMPORT_INTERFACE_VECTOR
#define SYSTEM_S_IMPORT_INTERFACE_STRING
#include "stl_pif.h"


class clSDS_PhonebookListClient;


class clSDS_PhonebookList
   : public asf::core::ServiceAvailableIF
   , public MOST_PhonBk_FI::CreatePhoneBookListCallbackIF
   , public MOST_PhonBk_FI::GetContactDetailsCallbackIF
   , public MOST_PhonBk_FI::DevicePhoneBookFeatureSupportCallbackIF
{
   public:
      enum tenSelectionType
      {
         EN_SELECTION_FIRST,
         EN_SELECTION_NEXT,
         EN_SELECTION_PREVIOUS
      };
      virtual ~clSDS_PhonebookList();
      clSDS_PhonebookList(::boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy > phonebookProxy);

      void onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy, const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy, const asf::core::ServiceStateChange& stateChange);

      virtual void onCreatePhoneBookListError(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy, const ::boost::shared_ptr< MOST_PhonBk_FI::CreatePhoneBookListError >& error);
      virtual void onCreatePhoneBookListResult(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy, const ::boost::shared_ptr< MOST_PhonBk_FI::CreatePhoneBookListResult >& result);

      virtual void onGetContactDetailsError(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy, const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsError >& error);
      virtual void onGetContactDetailsResult(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy, const ::boost::shared_ptr< MOST_PhonBk_FI::GetContactDetailsResult >& result);

      virtual void onDevicePhoneBookFeatureSupportError(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy, const ::boost::shared_ptr< MOST_PhonBk_FI::DevicePhoneBookFeatureSupportError >& error);
      virtual void onDevicePhoneBookFeatureSupportStatus(const ::boost::shared_ptr< MOST_PhonBk_FI::MOST_PhonBk_FIProxy >& proxy, const ::boost::shared_ptr< MOST_PhonBk_FI::DevicePhoneBookFeatureSupportStatus >& status);

      virtual tVoid onPhonebookDetailsResponse(const ::most_PhonBk_fi_types::T_PhonBkContactDetails& oContactDetails_);

      tVoid vRequestPhonebookDetailsFromPhone(clSDS_PhonebookListClient* pRequester, tU32 u32StartIndex,
                                              clSDS_PhonebookList::tenSelectionType enSelection,
                                              tBool isEntryIdRequested = FALSE);

      tU32 u32GetContactDetailCount() const;
      const bpstl::string& oGetPhonebookName() const;
      const tsCMPhone_ReqPBDetails& oGetPhoneNumbers() const;
      tBool bGetNoMoreEntriesFlag() const;

      tBool bIsEntryAvailable() const;
      tVoid vUpdateHeadLineTagForSingleContact() const;

   private:
      tVoid sendCreatePhoneBookListStart();
      tVoid sendGetContactDetails(tU32 u32Index);
      tVoid onPhoneBookListResult();

      tVoid newPhonebookDetails(const ::most_PhonBk_fi_types::T_PhonBkContactDetails& oContactDetails_);

      tBool noMoreEntries(const ::most_PhonBk_fi_types::T_PhonBkContactDetails& oContactDetails_) const;

      boost::shared_ptr< ::MOST_PhonBk_FI::MOST_PhonBk_FIProxy > _phonebookProxy;
      bpstl::vector<clSDS_PhonebookListClient*> _requestors;
      clSDS_PhonebookListClient* _pRequester;
      tsCMPhone_ReqPBDetails _oPBDetails;
      tenSelectionType _enSelectionType;
      tBool _bNoMoreEntries;
      unsigned int _noMoreEntriesFlag;
      tU16 _u16PBEntryCount;
      tU32 _u32StartIndex;
      tBool _entryIdRequested;
};


#endif
