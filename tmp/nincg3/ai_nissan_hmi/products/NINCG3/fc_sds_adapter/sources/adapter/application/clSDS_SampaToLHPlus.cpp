/*********************************************************************//**
 * \file       clSDS_SampaToLHPlus.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "application/clSDS_SampaToLHPlus.h"
#include "external/sds2hmi_fi.h"


using namespace std;

static const char* const sampa_rules_EN[] = {"aI", "aU", "dZ", "e", "N", "o", "OI", "r", "tS", "V", "3", "3`", "4",
      "=n", "=l", "@", "@`", "{", "\"", "%", "?", ".", " "
                                            };

static const char* const lh_rules_EN[]
= {"a&I", "a&U", "d&Z", "e&I", "nK", "o&U", "O&I", "R+", "t&S", "^", "E0", "E0R+", "r6",
   "n%)", "l%)", "$", "$R+", "@", "\'", "'2", "?", ".", "_"
  };

static const char* const sampa_rules_FR[] = {"@", "2", "9", "e~", "{~", "o~", "2~", "N", "J", "H", "ts", "dz", "\"", " "};

static const char* const lh_rules_FR[] = {"$", "e+", "E+", "e%~", "@%~", "o%~", "e+%~", "nK", "n~", "h\\", "t&s", "d&z", "\'", "_"};

static const char* const sampa_rules_SP[] = {"N", "J", "4", "j\\", "tS", "tZ", "\"", " "};

static const char* const lh_rules_SP[] = {"nK", "n~", "r6", "J", "t&S", "t&Z", "\'", "_"};

clSDS_SampaToLHPlus::clSDS_SampaToLHPlus()
{
}


clSDS_SampaToLHPlus::~clSDS_SampaToLHPlus()
{
}


std::string clSDS_SampaToLHPlus::translate(const int language_code, const ::std::string& sampa_word_string)
{
   std::string ret = "";

   switch (language_code)
   {
      case sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_ENG:
         ret = translate_EN(sampa_word_string);
         break;

      case sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_SPA:
         ret = translate_SP(sampa_word_string);
         break;

      case sds2hmi_fi_tcl_e16_ISO639_3_SDSLanguageCode::FI_EN_ISO_639_3_FRA:
         ret = translate_FR(sampa_word_string);
         break;

      default:
         break;
   }

   return ret;
}


std::string clSDS_SampaToLHPlus::getTranslation_EN(const char* c)
{
   for (unsigned int i = 0; i < RULES_EN; i++)
   {
      if (strcmp(c, sampa_rules_EN[i]) == 0)
      {
         return lh_rules_EN[i];
      }
   }
   return c;
}


std::string clSDS_SampaToLHPlus::getTranslation_FR(const char* c)
{
   for (unsigned int i = 0; i < RULES_FR; i++)
   {
      if (strcmp(c, sampa_rules_FR[i]) == 0)
      {
         return lh_rules_FR[i];
      }
   }
   return c;
}


std::string clSDS_SampaToLHPlus::getTranslation_SP(const char* c)
{
   for (unsigned int i = 0; i < RULES_SP; i++)
   {
      if (strcmp(c, sampa_rules_SP[i]) == 0)
      {
         return lh_rules_SP[i];
      }
   }
   return c;
}


std::string clSDS_SampaToLHPlus::translate_EN(const ::std::string& sampa_word_string)
{
   string res;

   char c[2];
   unsigned int i = 0;
   while (i < sampa_word_string.size())
   {
      c[0] = sampa_word_string[i];
      c[1] = 0;

      ++i;

      switch (c[0])
      {
         case 'a':
            if (sampa_word_string[i] == 'I')
            {
               res.append(getTranslation_EN("aI"));
               ++i;
            }
            else if (sampa_word_string[i] == 'U')
            {
               res.append(getTranslation_EN("aU"));
               ++i;
            }
            else
            {
               res.append(getTranslation_EN(c));
            }
            break;
         case 'd':
            if (sampa_word_string[i] == 'Z')
            {
               res.append(getTranslation_EN("dZ"));
               ++i;
            }
            else
            {
               res.append(getTranslation_EN(c));
            }
            break;
         case 'O':
            if (sampa_word_string[i] == 'I')
            {
               res.append(getTranslation_EN("OI"));
               ++i;
            }
            else
            {
               res.append(getTranslation_EN(c));
            }
            break;
         case 't':
            if (sampa_word_string[i] == 'S')
            {
               res.append(getTranslation_EN("tS"));
               ++i;
            }
            else
            {
               res.append(getTranslation_EN(c));
            }
            break;
         case '3':
            if (sampa_word_string[i] == '`')
            {
               res.append(getTranslation_EN("3`"));
               ++i;
            }
            else
            {
               res.append(getTranslation_EN(c));
            }
            break;
         case '=':
            if (sampa_word_string[i] == 'n')
            {
               res.append(getTranslation_EN("=n"));
               ++i;
            }
            else if (sampa_word_string[i] == 'l')
            {
               res.append(getTranslation_EN("=l"));
               ++i;
            }
            else
            {
               res.append(getTranslation_EN(c));
            }
            break;
         case '@':
            if (sampa_word_string[i] == '`')
            {
               res.append(getTranslation_EN("@`"));
               ++i;
            }
            else
            {
               res.append(getTranslation_EN(c));
            }
            break;
         default:
            res.append(getTranslation_EN(c));
            break;
      }
   }

   return res;
}


std::string clSDS_SampaToLHPlus::translate_FR(const ::std::string& sampa_word_string)
{
   string res;

   char c[2];
   unsigned int i = 0;
   while (i < sampa_word_string.size())
   {
      c[0] = sampa_word_string[i];
      c[1] = 0;

      ++i;

      switch (c[0])
      {
         case 'e':
            if (sampa_word_string[i] == '~')
            {
               res.append(getTranslation_FR("e~"));
               ++i;
            }
            else
            {
               res.append(getTranslation_FR(c));
            }
            break;
         case '{':
            if (sampa_word_string[i] == '~')
            {
               res.append(getTranslation_FR("{~"));
               ++i;
            }
            else
            {
               res.append(getTranslation_EN(c));
            }
            break;
         case 'o':
            if (sampa_word_string[i] == '~')
            {
               res.append(getTranslation_FR("o~"));
               ++i;
            }
            else
            {
               res.append(getTranslation_FR(c));
            }
            break;
         case '2':
            if (sampa_word_string[i] == '~')
            {
               res.append(getTranslation_FR("2~"));
               ++i;
            }
            else
            {
               res.append(getTranslation_FR(c));
            }
            break;
         case 't':
            if (sampa_word_string[i] == 's')
            {
               res.append(getTranslation_FR("ts"));
               ++i;
            }
            else
            {
               res.append(getTranslation_FR(c));
            }
            break;
         case 'd':
            if (sampa_word_string[i] == 'z')
            {
               res.append(getTranslation_FR("dz"));
               ++i;
            }
            else
            {
               res.append(getTranslation_FR(c));;
            }
            break;
         default:
            res.append(getTranslation_FR(c));
            break;
      }
   }

   return res;
}


std::string clSDS_SampaToLHPlus::translate_SP(const ::std::string& sampa_word_string)
{
   string res;

   char c[2];
   unsigned int i = 0;
   while (i < sampa_word_string.size())
   {
      c[0] = sampa_word_string[i];
      c[1] = 0;
      ++i;

      switch (c[0])
      {
         case 'j':
            if (sampa_word_string[i] == '\\')
            {
               res.append(getTranslation_SP("j\\"));
               ++i;
            }
            else
            {
               res.append(getTranslation_SP(c));
            }
            break;
         case 't':
            if (sampa_word_string[i] == 'S')
            {
               res.append(getTranslation_SP("tS"));
               ++i;
            }
            else if (sampa_word_string[i] == 'Z')
            {
               res.append(getTranslation_SP("tZ"));
               ++i;
            }
            else
            {
               res.append(getTranslation_SP(c));
            }
            break;

         default:
            res.append(getTranslation_SP(c));
            break;
      }
   }

   return res;
}
