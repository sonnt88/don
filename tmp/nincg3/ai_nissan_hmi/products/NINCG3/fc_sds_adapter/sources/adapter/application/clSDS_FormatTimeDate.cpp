/*********************************************************************//**
 * \file       clSDS_FormatTimeDate.cpp
 *
 * See .cpp file for description.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#include "clSDS_FormatTimeDate.h"
#include "view_db/Sds_TextDB.h"
#include <ctime>

#define UTFUTIL_S_IMPORT_INTERFACE_GENERIC
#include "utf_if.h"

#include "SdsAdapter_Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_SDSADP_DETAILS
#include "trcGenProj/Header/clSDS_FormatTimeDate.cpp.trc.h"
#endif


using namespace clock_main_fi;
using namespace clock_main_fi_types;


clSDS_FormatTimeDate::~clSDS_FormatTimeDate()
{
}


clSDS_FormatTimeDate::clSDS_FormatTimeDate(::boost::shared_ptr< ::Clock_main_fiProxy > pClockproxy)
   : _clockproxy(pClockproxy)
   , _timeFormat(VDCLK_TEN_TimeFormat__VDCLK_EN_TF_Mode12)
   , _dateFormat(VDCLK_TEN_DateFormat__VDCLK_EN_DF_dd_mm_yyyy)
   , _receivedSmsHour(0)
   , _receivedSmsMinute(0)
{
}


void clSDS_FormatTimeDate::onAvailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _clockproxy)
   {
      _clockproxy->sendLocalTimeDate_MinuteUpdateUpReg(*this);
      _clockproxy->sendTimeFormatUpReg(*this);
      _clockproxy->sendDateFormatUpReg(*this);
   }
}


void clSDS_FormatTimeDate::onUnavailable(
   const boost::shared_ptr<asf::core::Proxy>& proxy,
   const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _clockproxy)
   {
      _clockproxy->sendLocalTimeDate_MinuteUpdateRelUpRegAll();
      _clockproxy->sendTimeFormatRelUpRegAll();
      _clockproxy->sendDateFormatRelUpRegAll();
   }
}


void clSDS_FormatTimeDate::onTimeFormatError(
   const ::boost::shared_ptr< Clock_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< TimeFormatError >& /*error*/)
{
}


void clSDS_FormatTimeDate::onTimeFormatStatus(
   const ::boost::shared_ptr< Clock_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< TimeFormatStatus >& status)
{
   _timeFormat = status->getEnTimeFormat();
   ETG_TRACE_USR1(("clSDS_FormatTimeDate::TIME_FORMAT %d", _timeFormat));
}


void clSDS_FormatTimeDate::onDateFormatError(
   const ::boost::shared_ptr< Clock_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< DateFormatError >& /*error*/)
{
}


void clSDS_FormatTimeDate::onDateFormatStatus(
   const ::boost::shared_ptr< Clock_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< DateFormatStatus >& status)
{
   _dateFormat = status->getEnDateFormat();
   ETG_TRACE_USR1(("clSDS_FormatTimeDate::DATE_FORMAT %d", _dateFormat));
}


void clSDS_FormatTimeDate::onLocalTimeDate_MinuteUpdateError(
   const ::boost::shared_ptr< Clock_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< LocalTimeDate_MinuteUpdateError >& /*error*/)
{
}


void clSDS_FormatTimeDate::onLocalTimeDate_MinuteUpdateStatus(
   const ::boost::shared_ptr< Clock_main_fiProxy >& /*proxy*/,
   const ::boost::shared_ptr< LocalTimeDate_MinuteUpdateStatus >& status)
{
   _currentDate.day = status->getU8Day();
   _currentDate.month = status->getU8Month();
   _currentDate.year = status->getS16Year();
}


void clSDS_FormatTimeDate::formatTimeDate(std::string hour, std::string minute, std::string day, std::string month, std::string year, std::string& oString)
{
   _receivedDate.day = std::atoi(day.c_str());
   _receivedDate.month = std::atoi(month.c_str());
   _receivedDate.year = std::atoi(year.c_str());

   _receivedSmsHour = std::atoi(hour.c_str());
   _receivedSmsMinute = std::atoi(minute.c_str());

   int numberOfDays = getDifferenceOfDays();
   if (numberOfDays == 0)
   {
      getSmsReceivedTime(oString);
   }
   else if (numberOfDays == -1)
   {
      oString.assign(Sds_TextDB_vGetText(SDS_TEXT_YESTERDAY));
   }
   else if (numberOfDays < -1 && numberOfDays >= -6)
   {
      getReceivedDay(oString);
   }
   else
   {
      getSmsReceivedDate(oString);
   }
}


int clSDS_FormatTimeDate::getDifferenceOfDays() const
{
   int difference = 0;
   // calculated using century 1900
   // Seconds, Minutes, Hours, Day, Month, Year - 1900, Day of week, Days in year.[0-365], DST.  [-1/0/1]
   struct std::tm currentTime = {0, 0, 0, _currentDate.day, _currentDate.month, _currentDate.year - 1900, 0, 0, 0, 0, "" };
   struct std::tm receivedTime = {0, 0, 0, _receivedDate.day, _receivedDate.month, _receivedDate.year - 1900, 0, 0, 0, 0, "" };
   std::time_t curr_time = std::mktime(&currentTime);
   std::time_t rec_time = std::mktime(&receivedTime);
   if (curr_time != (std::time_t)(-1) && rec_time != (std::time_t)(-1))
   {
      difference = std::difftime(rec_time, curr_time) / (60 * 60 * 24);
   }
   return difference;
}


void clSDS_FormatTimeDate::getSmsReceivedTime(std::string& oString)
{
   if (_timeFormat == clock_main_fi_types::VDCLK_TEN_TimeFormat__VDCLK_EN_TF_Mode12)
   {
      getTimeIn12HourFormat(oString);
   }
   else
   {
      getTimeIn24HourFormat(oString);
   }
}


void clSDS_FormatTimeDate::getTimeIn12HourFormat(std::string& oString)
{
   char acHourMinute[25] = "";
   char acTimeWithAmPm[25] = "";

   if (_receivedSmsHour >= 12)
   {
      _receivedSmsHour = (_receivedSmsHour == 12) ? _receivedSmsHour : (_receivedSmsHour - 12);
      UTF8_s32SaveNPrintFormat(acHourMinute, sizeof acHourMinute, "%d:%02d", _receivedSmsHour, _receivedSmsMinute);
      UTF8_s32SaveNPrintFormat(acTimeWithAmPm, sizeof acTimeWithAmPm, "%sPM", acHourMinute);
   }
   else
   {
      _receivedSmsHour = (_receivedSmsHour == 0) ? 12 : _receivedSmsHour;
      UTF8_s32SaveNPrintFormat(acHourMinute, sizeof acHourMinute, "%d:%02d", _receivedSmsHour, _receivedSmsMinute);
      UTF8_s32SaveNPrintFormat(acTimeWithAmPm, sizeof acTimeWithAmPm, "%sAM", acHourMinute);
   }
   oString.assign(acTimeWithAmPm);
}


void clSDS_FormatTimeDate::getTimeIn24HourFormat(std::string& oString)
{
   tChar acSmsReceivedTime[25]  = "";
   UTF8_s32SaveNPrintFormat(acSmsReceivedTime, sizeof acSmsReceivedTime, "%d:%02d", _receivedSmsHour, _receivedSmsMinute);
   oString.assign(acSmsReceivedTime);
}


void clSDS_FormatTimeDate::getSmsReceivedDate(std::string& oString)
{
   char acSmsReceivedDate[50]  = "";
   switch (_dateFormat)
   {
      case VDCLK_TEN_DateFormat__VDCLK_EN_DF_dd_mm_yyyy:
         UTF8_s32SaveNPrintFormat(acSmsReceivedDate, sizeof acSmsReceivedDate, "%02d/%02d/%4d", _receivedDate.day, _receivedDate.month, _receivedDate.year);
         break;

      case VDCLK_TEN_DateFormat__VDCLK_EN_DF_mm_dd_yyyy:
         UTF8_s32SaveNPrintFormat(acSmsReceivedDate, sizeof acSmsReceivedDate, "%02d/%02d/%4d", _receivedDate.month, _receivedDate.day, _receivedDate.year);
         break;

      case VDCLK_TEN_DateFormat__VDCLK_EN_DF_yyyy_mm_dd:
         UTF8_s32SaveNPrintFormat(acSmsReceivedDate, sizeof acSmsReceivedDate, "%04d/%02d/%2d", _receivedDate.year, _receivedDate.month, _receivedDate.day);
         break;

      case VDCLK_TEN_DateFormat__VDCLK_EN_DF_dd_mm_yyyy_dash:
         UTF8_s32SaveNPrintFormat(acSmsReceivedDate, sizeof acSmsReceivedDate, "%02d-%02d-%4d", _receivedDate.day, _receivedDate.month, _receivedDate.year);
         break;

      case VDCLK_TEN_DateFormat__VDCLK_EN_DF_dd_mm_yyyy_dot:
         UTF8_s32SaveNPrintFormat(acSmsReceivedDate, sizeof acSmsReceivedDate, "%02d.%02d.%4d", _receivedDate.day, _receivedDate.month, _receivedDate.year);
         break;

      case VDCLK_TEN_DateFormat__VDCLK_EN_DF_yyyy_dd_mm_dash:
         UTF8_s32SaveNPrintFormat(acSmsReceivedDate, sizeof acSmsReceivedDate, "%04d-%02d-%2d", _receivedDate.year, _receivedDate.day, _receivedDate.month);
         break;

      case VDCLK_TEN_DateFormat__VDCLK_EN_DF_yyyy_mm_mdd_JPN:
         break;

      case VDCLK_TEN_DateFormat__VDCLK_EN_DF_yyyy_mm_dd_KOR:
         break;

      default:
         break;
   }
   oString.append(acSmsReceivedDate);
}


void clSDS_FormatTimeDate::getReceivedDay(std::string& oString) const
{
   std::tm timeIn = { 0, 0, 0, _receivedDate.day, _receivedDate.month - 1, _receivedDate.year - 1900, 0, 0, 0, 0, "" }; // 1-based day, 0-based month, year since 1900
   std::time_t timeTemp = std::mktime(& timeIn);
   std::tm const* pTimeOut = std::localtime(& timeTemp);
   Sds_TextId weekday[] = { SDS_TEXT_SUNDAY, SDS_TEXT_MONDAY, SDS_TEXT_TUESDAY, SDS_TEXT_WEDNESDAY, SDS_TEXT_THURSDAY, SDS_TEXT_FRIDAY, SDS_TEXT_SATURDAY };
   if (pTimeOut != NULL)
   {
      oString.append(Sds_TextDB_vGetText(weekday[pTimeOut->tm_wday]));
   }
}
