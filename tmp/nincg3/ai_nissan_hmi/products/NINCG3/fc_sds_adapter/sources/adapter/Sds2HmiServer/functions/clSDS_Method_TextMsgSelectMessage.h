/*********************************************************************//**
 * \file       clSDS_Method_TextMsgSelectMessage.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_TextMsgSelectMessage_h
#define clSDS_Method_TextMsgSelectMessage_h


#include "Sds2HmiServer/framework/clServerMethod.h"
#include "application/clSDS_ReadSmsList.h"

class clSDS_Method_TextMsgSelectMessage : public clServerMethod
{
   public:
      virtual ~clSDS_Method_TextMsgSelectMessage();
      clSDS_Method_TextMsgSelectMessage(ahl_tclBaseOneThreadService* pService, clSDS_ReadSmsList* pSmsList);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      clSDS_ReadSmsList* _pReadSmsList;
};


#endif
