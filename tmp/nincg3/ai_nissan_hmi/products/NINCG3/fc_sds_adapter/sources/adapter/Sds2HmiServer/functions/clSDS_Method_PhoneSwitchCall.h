/*********************************************************************//**
 * \file       clSDS_Method_PhoneSwitchCall.h
 *
 * VC FC Service Message handler functions
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/
#ifndef clSDS_Method_PhoneSwitchCall_h
#define clSDS_Method_PhoneSwitchCall_h


#include "MOST_Tel_FIProxy.h"
#include "Sds2HmiServer/framework/clServerMethod.h"


class clSDS_Method_PhoneSwitchCall
   : public clServerMethod
   , public asf::core::ServiceAvailableIF
   , public MOST_Tel_FI::SwapCallCallbackIF
   , public MOST_Tel_FI::CallStatusNoticeCallbackIF
{
   public:
      virtual ~clSDS_Method_PhoneSwitchCall();
      clSDS_Method_PhoneSwitchCall(
         ahl_tclBaseOneThreadService* pService,
         ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > pSds2TelProxy);

      void onAvailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(
         const boost::shared_ptr<asf::core::Proxy>& proxy,
         const asf::core::ServiceStateChange& stateChange);

      virtual void onSwapCallError(
         const ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< ::MOST_Tel_FI::SwapCallError >& error);
      virtual void onSwapCallResult(
         const ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< ::MOST_Tel_FI::SwapCallResult >& result);

      virtual void onCallStatusNoticeError(
         const ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeError >& error);
      virtual void onCallStatusNoticeStatus(
         const ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy >& proxy,
         const ::boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeStatus >& status);

   private:
      virtual tVoid vMethodStart(amt_tclServiceData* pInMsg);
      void sendSwapCall();
      void evaluateActiveCallPresent(const boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeStatus >& status);
      void evaluateHoldCallPresent(const boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeStatus >& status);
      boost::shared_ptr< MOST_Tel_FI::MOST_Tel_FIProxy > _telephoneProxy;
      bool _bHoldCallPresent;
      uint16 _u16HoldCallInstance;
      bool _bActiveCallPresent;
      uint16 _u16ActiveCallInstance;
};


#endif
