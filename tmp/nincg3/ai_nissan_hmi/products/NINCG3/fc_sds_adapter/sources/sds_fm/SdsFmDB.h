/*********************************************************************//**
 * \file       SdsFmDB.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef SdsFmDB_h
#define SdsFmDB_h

#include "asf/core/BaseComponent.h"


class Sds2FmDbService;


namespace sds_fm {


class SdsFmDB : public asf::core::BaseComponent
{
   public:
      SdsFmDB();
      virtual ~SdsFmDB();

   private:
      Sds2FmDbService* _sds2FmDbService;
};


};


#endif
