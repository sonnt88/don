#ifndef SDSFMDB_TRACE_H
#define SDSFMDB_TRACE_H


#define ETG_S_IMPORT_INTERFACE_GENERIC
#include "etg_if.h"


// trace classes based on TR_COMP_SAAL (256 * 70) = 0x4600 - see mc_trace.h

#define TR_CLASS_SDSFMDB			0x4621


#endif /* SDSFMDB_TRACE_H */
