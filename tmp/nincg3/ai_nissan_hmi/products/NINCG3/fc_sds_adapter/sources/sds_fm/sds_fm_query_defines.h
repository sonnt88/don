/*********************************************************************//**
 * \file       sds_fm_query_defines.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef sds_fm_query_defines_h
#define sds_fm_query_defines_h


/*****************************************************************************/
/* FM specific DEFINES                                                                   */
/*****************************************************************************/

#define FILE_SYSTEM_SEPERATOR						"/"

#define SPEECH_FMRDSTUNER_DATABASE_NAME              "FM_RDS_tunerDB.db"
#define SPEECH_FMHDTUNER_DATABASE_NAME              "FM_HD_tunerDB.db"
#define SPEECH_FMTUNER_DATABASE_NAME_SIZE			23

#define SPEECH_FMTUNER_DATABASE_PATH_LOC            "/tmp/speech/radio"
#define SPEECH_FMRDSTUNER_DATABASE_PATH_LOC           "/dev/ramdisk/speech/radio/FM_RDS_tunerDB.db"
#define SPEECH_FMHDTUNER_DATABASE_PATH_LOC            "/dev/ramdisk/speech/radio/FM_HD_tunerDB.db"
#define SPEECH_FMTUNER_DATABASE_PATH_LOC_SIZE		59

#define FM_RDS_STATION_LIST_TABLE             "FMStationList"


#define QUERY_CREATE_FM_STATION_LIST_TABLE      "CREATE TABLE FMStationList( object_id INTEGER, frequency INTEGER, shortstation_name CHAR(100), audioProgram INTEGER);"

#define FORMAT_QUERY_UPDATE_FM_STATION_LIST						"INSERT INTO FMStationList (object_id, frequency, shortstation_name, audioProgram) VALUES ('%lu','%lu','%s','%lu');"

#define QUERY_CREATE_FM_CHECKSUM_TABLE			"CREATE TABLE FM_Checksum( fm_timestamp DATETIME PRIMARY KEY DEFAULT CURRENT_TIMESTAMP);"
#define FORMAT_QUERY_UPDATE_FM_TIME_STAMP	  	"INSERT OR REPLACE INTO FM_Checksum (fm_timestamp) values (CURRENT_TIMESTAMP);"
#define FORMAT_QUERY_DELETE_FM_TIME_STAMP	  	"DELETE FROM FM_Checksum;"
#define QUERY_VIEW_FM_CHECKSUM				  	"CREATE VIEW FMChecksum AS SELECT fm_timestamp FROM FM_Checksum;"


#define QUERY_PRAGAMA_JOURNAL_MODE_MEM				     "PRAGMA journal_mode = MEMORY;"

#define QUERY_PRAGAMA_SYNCHRONOUS_OFF					 "PRAGMA synchronous = OFF;"

#define QUERY_BEGIN_TRANSACTION							 "BEGIN TRANSACTION;"

#define QUERY_END_TRANSACTION							 "END TRANSACTION;"

#define QUERY_PRAGAMA_SYNCHRONOUS_OFF					 "PRAGMA synchronous = OFF;"

#define QUERY_VIEW_FM_RDS_SHORT_NAME                     "CREATE VIEW RDSShortname AS SELECT shortstation_name AS RDSShortname, object_id AS StationID, object_id AS RDS_PI FROM FMStationList WHERE shortstation_name != '' ;"
#define QUERY_VIEW_FM_HD_SHORT_NAME                      "CREATE VIEW FMShortname AS SELECT shortstation_name AS FMShortname, object_id AS  StationID FROM FMStationList WHERE shortstation_name != '' ;"
#define QUERY_VIEW_XM_CHANNEL                            "CREATE VIEW XMChannel AS  SELECT channel_name AS XMChannelname,channel_object_id AS XMChannelID FROM XMStationList WHERE channel_name != '';"
#define QUERY_DELETE_ALL_DATA_FM_STATION_LIST            "delete from FMStationList;"


#define MAX_QUERY_LEN       1024 /* = 9 KB. Note: Normally 8 KB is required but for SAAL 1KB is sufficient
for blob data. */

#define SQLITE_ESCAPE_SEQUENCE   39		//Ascii value for ' is 39

#define STR_MAX 500 // TODO: check this value

#define FILE_SEPERATOR_SIZE				1
#endif /* sds_fm_query_defines_h */
