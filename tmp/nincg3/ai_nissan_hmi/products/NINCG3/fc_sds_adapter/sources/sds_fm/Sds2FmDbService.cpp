/*********************************************************************//**
 * \file       Sds2FmDbService.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "Sds2FmDbService.h"
#include "FmDatabaseHandler.h"

#include "SdsFmDB_Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSFMDB
#include "trcGenProj/Header/Sds2FmDbService.cpp.trc.h"
#endif

Sds2FmDbService::Sds2FmDbService() : SdsFmServiceStub("FmDbServicePort")
{
   _fmDatabaseHandler = new FmDatabaseHandler();
}


Sds2FmDbService::~Sds2FmDbService()
{
   if (_fmDatabaseHandler != NULL)
   {
      delete _fmDatabaseHandler;
      _fmDatabaseHandler = NULL;
   }
}


void Sds2FmDbService::onStoreRDSChannelNamesRequest(const ::boost::shared_ptr< sds_fm_fi::SdsFmService::StoreRDSChannelNamesRequest >& request)
{
   //sqlite update into database
   if (_fmDatabaseHandler)
   {
      if (_fmDatabaseHandler->blBeginTransactionForFMRDS())
      {
         _fmDatabaseHandler->vDeleteAllFromFMRDSStationList();
         std::vector< sds_fm_fi::SdsFmService::FMChannelItem >::const_iterator it = request->getChannelList().begin();
         while (it != request->getChannelList().end())
         {
            FMChannelListItem listItem(*it);
            _fmDatabaseHandler->bUpdateRecordForFMRDSStationList(&listItem);
            ++it;
         }
         _fmDatabaseHandler->bUpdateChecksumForFMRDS();
         _fmDatabaseHandler->blEndTransactionForFMRDS();
         sendStoreRDSChannelNamesResponse();
      }
      else
      {
         sendStoreRDSChannelNamesError(DBUS_ERROR_FAILED, "SQLITE begin transaction failed");
      }
   }
   else
   {
      sendStoreRDSChannelNamesError(DBUS_ERROR_FAILED, "_fmDatabaseHandler is null");
   }
}


void Sds2FmDbService::onStoreHDChannelNamesRequest(const ::boost::shared_ptr< sds_fm_fi::SdsFmService::StoreHDChannelNamesRequest >& request)
{
   if (_fmDatabaseHandler)
   {
      if (_fmDatabaseHandler->blBeginTransactionForFMHD())
      {
         _fmDatabaseHandler->vDeleteAllFromFMHDSStationList();
         std::vector< sds_fm_fi::SdsFmService::FMChannelItem >::const_iterator it = request->getChannelList().begin();
         while (it != request->getChannelList().end())
         {
            FMChannelListItem listItem(*it);
            _fmDatabaseHandler->bUpdateRecordForHDStationList(&listItem);
            ++it;
         }
         _fmDatabaseHandler->bUpdateChecksumForFMHD();
         _fmDatabaseHandler->blEndTransactionForFMHD();
         sendStoreHDChannelNamesResponse();
      }
      else
      {
         sendStoreHDChannelNamesError(DBUS_ERROR_FAILED, "SQLITE begin transaction failed");
      }
   }
   else
   {
      sendStoreHDChannelNamesError(DBUS_ERROR_FAILED, "_fmDatabaseHandler is null");
   }
}
