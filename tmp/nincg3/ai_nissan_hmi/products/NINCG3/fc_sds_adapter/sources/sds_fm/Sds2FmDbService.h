/*********************************************************************//**
 * \file       Sds2FmDbService.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef FMDatabaseHandler_h
#define FMDatabaseHandler_h

#include "sds_fm_fi/SdsFmServiceStub.h"

class FmDatabaseHandler;

class Sds2FmDbService : public sds_fm_fi::SdsFmService::SdsFmServiceStub
{
   public:
      Sds2FmDbService();
      virtual ~Sds2FmDbService();

      virtual void onStoreRDSChannelNamesRequest(const ::boost::shared_ptr< sds_fm_fi::SdsFmService::StoreRDSChannelNamesRequest >& request);
      virtual void onStoreHDChannelNamesRequest(const ::boost::shared_ptr< sds_fm_fi::SdsFmService::StoreHDChannelNamesRequest >& request);
   private:
      FmDatabaseHandler* _fmDatabaseHandler;
};


#endif /* FMDatabaseHandler_h */
