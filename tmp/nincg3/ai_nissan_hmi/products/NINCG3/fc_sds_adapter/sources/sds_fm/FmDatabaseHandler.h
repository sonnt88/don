/*********************************************************************//**
 * \file       FmDatabaseHandler.h
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#ifndef FmDatabaseHandler_h
#define FmDatabaseHandler_h


#include "sds_fm_fi/SdsFmService.h"

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"


#define SHORT_STATION_NAME_LEN (20+1)			//1 Extra char for NULL
#define SAAL_OBJECTID_NOTFOUND       0xFFFF // If object ID is not found, 0xFFFF will be returned


struct sqlite3;
struct sqlite3_stmt;


class FMChannelListItem
{
   public:
      tU32	m_u32ObjectID;      // Unique ID of the requested channel
      tU32	m_u32Frequency;     // freq of active station
      tU8  m_sShortStationName[SHORT_STATION_NAME_LEN];
      tU32 m_audioProgram;
      FMChannelListItem()
      {
         m_u32ObjectID = SAAL_OBJECTID_NOTFOUND;
         m_u32Frequency = 0x0000;
         OSAL_pvMemorySet(m_sShortStationName, 0, SHORT_STATION_NAME_LEN);
         m_audioProgram = 0;
      }
      FMChannelListItem(const FMChannelListItem& oTunerStationListItem)
      {
         m_u32ObjectID = oTunerStationListItem.m_u32ObjectID;
         m_u32Frequency = oTunerStationListItem.m_u32Frequency;
         if (OSAL_NULL != OSAL_szStringNCopy(m_sShortStationName, oTunerStationListItem.m_sShortStationName, SHORT_STATION_NAME_LEN))
         {
            m_sShortStationName[SHORT_STATION_NAME_LEN - 1] = '\0';
         }
         m_audioProgram = oTunerStationListItem.m_audioProgram;
      }
      FMChannelListItem(const sds_fm_fi::SdsFmService::FMChannelItem& oTunerStationListItem)
      {
         m_u32ObjectID = oTunerStationListItem.getObjectID();
         m_u32Frequency = oTunerStationListItem.getFrequency();
         if (OSAL_NULL != OSAL_szStringNCopy(m_sShortStationName, oTunerStationListItem.getStationName().c_str() , SHORT_STATION_NAME_LEN))
         {
            m_sShortStationName[SHORT_STATION_NAME_LEN - 1] = '\0';
         }
         m_audioProgram = oTunerStationListItem.getAudioProgram();
      }
};


class FmDatabaseHandler
{
   public:
      FmDatabaseHandler();
      virtual ~FmDatabaseHandler();

      tVoid vDeleteAllFromFMRDSStationList();
      tVoid vDeleteAllFromFMHDSStationList();

      tBool blBeginTransactionForFMRDS(tVoid);
      tBool blBeginTransactionForFMHD(tVoid);
      tBool blEndTransactionForFMRDS(tVoid);
      tBool blEndTransactionForFMHD(tVoid);

      tBool bUpdateRecordForFMRDSStationList(FMChannelListItem* pFMStationListItem);
      tBool bUpdateRecordForHDStationList(FMChannelListItem* pFMStationListItem);
      tBool bUpdateChecksumForFMRDS(tVoid);
      tBool bUpdateChecksumForFMHD(tVoid);

   private:
      tBool blIsFolderExists(tCString szFilePath);
      tBool blIsFileExists(tCString szFilePath);
      tBool bCreateFolder(tCString szFilePath);
      tBool blMakePath(tCString strPath);

      tU32 u32CreateFMRDSTunerDB(tCString szDbFilePath);
      tU32 u32CreateFMHDTunerDB(tCString szDbFilePath);

      tU32 u32CreateFMRDSTunerTables(tVoid);
      tU32 u32CreateFMHDTunerTables(tVoid);

      tU32 u32CreateFMRDSTunerViews(tVoid);
      tU32 u32CreateFMHDTunerViews(tVoid);

      tVoid vProcessStringsForEscapeSequence(tCString strSource, tString strDest);

      tU32 u32SetSynchronousModeOff(sqlite3* pDbHandle);
      tU32 u32SetJournalModeMemory(sqlite3* pDbHandle);
      tU32 u32BeginTransaction(sqlite3* pDbHandle);
      tU32 u32EndTransaction(sqlite3* pDbHandle);

      tU32 u32ExecFMTunerSelectQuery(tU32&  rfu32ObjectId);

      void vSetUpFMRDSDatabase();
      void vSetUpFMHDDatabase();

      tBool m_bIsFMDBExists;

      sqlite3* m_pFMRDShandle;      // Database handle for RDS
      sqlite3* m_pFMHDhandle;       // Database handle for HD
};


#endif
