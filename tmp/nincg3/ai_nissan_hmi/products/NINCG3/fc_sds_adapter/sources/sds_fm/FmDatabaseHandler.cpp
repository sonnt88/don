/*********************************************************************//**
 * \file       FmDatabaseHandler.cpp
 *
 * This file is part of the SdsAdapter component.
 *
 * \copyright  (c) 2016        Robert Bosch GmbH
 * \copyright  (c) 2016        Robert Bosch Engineering and
 *                             Business Solutions Private Ltd.
 *
 * The reproduction, distribution and utilization of this file as
 * well as the communication of its contents to others without express
 * authorization is prohibited. Offenders will be held liable for the
 * payment of damages. All rights reserved in the event of the grant
 * of a patent, utility model or design.
 ************************************************************************/

#include "FmDatabaseHandler.h"
#include "sqlite3.h"
#include "sds_fm_query_defines.h"

#if (OSAL_CONF==OSAL_LINUX)
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#endif

#include "SdsFmDB_Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SDSFMDB
#include "trcGenProj/Header/FmDatabaseHandler.cpp.trc.h"
#endif


FmDatabaseHandler::FmDatabaseHandler()
{
   m_bIsFMDBExists = false;
   m_pFMRDShandle = NULL;
   m_pFMHDhandle = NULL;

   vSetUpFMRDSDatabase();
   vSetUpFMHDDatabase();
}


FmDatabaseHandler::~FmDatabaseHandler()
{
   tU32 u32Ret  =   SQLITE_ERROR;
   if (OSAL_NULL != m_pFMRDShandle)
   {
      u32Ret = sqlite3_close(m_pFMRDShandle);
      if (SQLITE_OK != u32Ret)
      {
         ETG_TRACE_USR4(("FmDatabaseHandler::~FmDatabaseHandler() m_pFMRDShandle sqlite3_close failed(%d) for FM RDS", u32Ret));
      }
      m_pFMRDShandle = OSAL_NULL;
   }
   if (OSAL_NULL != m_pFMHDhandle)
   {
      u32Ret = sqlite3_close(m_pFMHDhandle);
      if (SQLITE_OK != u32Ret)
      {
         ETG_TRACE_USR4(("FmDatabaseHandler::~FmDatabaseHandler() m_pFMHDhandle sqlite3_close failed(%d) for FM HD", u32Ret));
      }
      m_pFMHDhandle = OSAL_NULL;
   }
   m_bIsFMDBExists  = false;
}


void FmDatabaseHandler::vSetUpFMRDSDatabase()
{
   tBool blFilePathExists = false;
   blFilePathExists = blIsFolderExists(SPEECH_FMTUNER_DATABASE_PATH_LOC);

   if (!blFilePathExists)
   {
      blFilePathExists = blMakePath(SPEECH_FMTUNER_DATABASE_PATH_LOC);
   }

   if (blFilePathExists)
   {
      tChar strStringPath[SPEECH_FMTUNER_DATABASE_PATH_LOC_SIZE] = SPEECH_FMTUNER_DATABASE_PATH_LOC;
      if (OSAL_NULL == OSAL_szStringNConcat(strStringPath, FILE_SYSTEM_SEPERATOR, FILE_SEPERATOR_SIZE))
      {
         ETG_TRACE_USR4(("FmDatabaseHandler::vSetUpFMRDSDatabase():StringCopy failed"));
      }
      if (OSAL_NULL == OSAL_szStringNConcat(strStringPath, SPEECH_FMRDSTUNER_DATABASE_NAME, SPEECH_FMTUNER_DATABASE_NAME_SIZE))
      {
         ETG_TRACE_USR4(("FmDatabaseHandler::vSetUpFMRDSDatabase():StringCopy failed"));
      }
      if (!blIsFileExists(strStringPath))
      {
         ETG_TRACE_USR4(("FmDatabaseHandler::vSetUpFMRDSDatabase():data base doesn't exist."));
         tU32 u32Ret = u32CreateFMRDSTunerDB(strStringPath);
         if ((SQLITE_ERROR != u32Ret) && (OSAL_NULL != m_pFMRDShandle))
         {
            u32Ret = u32SetSynchronousModeOff(m_pFMRDShandle);
            u32Ret = u32SetJournalModeMemory(m_pFMRDShandle);
            u32Ret = u32CreateFMRDSTunerTables();
            u32Ret = u32CreateFMRDSTunerViews();
            if (SQLITE_ERROR != u32Ret)
            {
               m_bIsFMDBExists = true;
               ETG_TRACE_USR4(("FmDatabaseHandler::vSetUpFMRDSDatabase():data base created successfully."));
            }
         }
      }
      else
      {
         ETG_TRACE_USR4(("FmDatabaseHandler::vSetUpFMRDSDatabase():data base exists."));
         tU32 u32Ret = u32CreateFMRDSTunerDB(strStringPath);
         if (SQLITE_ERROR != u32Ret)
         {
            m_bIsFMDBExists = true;
         }
      }
   }
}


void FmDatabaseHandler::vSetUpFMHDDatabase()
{
   tBool blFilePathExists = false;
   blFilePathExists = blIsFolderExists(SPEECH_FMTUNER_DATABASE_PATH_LOC);

   if (!blFilePathExists)
   {
      blFilePathExists = blMakePath(SPEECH_FMTUNER_DATABASE_PATH_LOC);
   }

   if (blFilePathExists)
   {
      tChar strStringPath[SPEECH_FMTUNER_DATABASE_PATH_LOC_SIZE] = SPEECH_FMTUNER_DATABASE_PATH_LOC;
      if (OSAL_NULL == OSAL_szStringNConcat(strStringPath, FILE_SYSTEM_SEPERATOR, FILE_SEPERATOR_SIZE))
      {
         ETG_TRACE_USR4(("FmDatabaseHandler::vSetUpFMHDDatabase():StringCopy failed"));
      }
      if (OSAL_NULL == OSAL_szStringNConcat(strStringPath, SPEECH_FMHDTUNER_DATABASE_NAME, SPEECH_FMTUNER_DATABASE_NAME_SIZE))
      {
         ETG_TRACE_USR4(("FmDatabaseHandler::vSetUpFMHDDatabase():StringCopy failed"));
      }
      if (!blIsFileExists(strStringPath))
      {
         ETG_TRACE_USR4(("FmDatabaseHandler::vSetUpFMHDDatabase():data base doesn't exist."));
         tU32 u32Ret = u32CreateFMHDTunerDB(strStringPath);
         if ((SQLITE_ERROR != u32Ret) && (OSAL_NULL != m_pFMHDhandle))
         {
            u32Ret = u32SetSynchronousModeOff(m_pFMHDhandle);
            u32Ret = u32SetJournalModeMemory(m_pFMHDhandle);
            u32Ret = u32CreateFMHDTunerTables();
            u32Ret = u32CreateFMHDTunerViews();
            if (SQLITE_ERROR != u32Ret)
            {
               m_bIsFMDBExists = true;
               ETG_TRACE_USR4(("FmDatabaseHandler::vSetUpFMHDDatabase():data base created successfully."));
            }
         }
      }
      else
      {
         ETG_TRACE_USR4(("FmDatabaseHandler::vSetUpFMHDDatabase():data base exists."));
         tU32 u32Ret = u32CreateFMHDTunerDB(strStringPath);
         if (SQLITE_ERROR != u32Ret)
         {
            m_bIsFMDBExists = true;
         }
      }
   }
}


tU32 FmDatabaseHandler::u32CreateFMRDSTunerDB(tCString szDbFilePath)
{
   ETG_TRACE_USR4(("FmDatabaseHandler::u32CreateFMRDSTunerDB(): szDbFilePath = %s.", szDbFilePath));
   tU32 u32Ret = SQLITE_ERROR;
   u32Ret = sqlite3_open(szDbFilePath, &m_pFMRDShandle);
   if ((u32Ret != SQLITE_OK) || (OSAL_NULL == m_pFMRDShandle))
   {
      sqlite3_close(m_pFMRDShandle);
      m_pFMRDShandle = OSAL_NULL;
      u32Ret = SQLITE_ERROR;
   }
   return u32Ret;
}


tU32 FmDatabaseHandler::u32CreateFMHDTunerDB(tCString szDbFilePath)
{
   ETG_TRACE_USR4(("FmDatabaseHandler::u32CreateFMHDTunerDB(): szDbFilePath = %s.", szDbFilePath));
   tU32 u32Ret = SQLITE_ERROR;
   u32Ret = sqlite3_open(szDbFilePath, &m_pFMHDhandle);
   if ((u32Ret != SQLITE_OK) || (OSAL_NULL == m_pFMHDhandle))
   {
      sqlite3_close(m_pFMHDhandle);
      m_pFMHDhandle = OSAL_NULL;
      u32Ret = SQLITE_ERROR;
   }
   return u32Ret;
}


tBool FmDatabaseHandler::blIsFileExists(tCString szFilePath)
{
   tBool blFilePathExists = FALSE;
#if (OSAL_CONF==OSAL_LINUX)
   if (OSAL_NULL != szFilePath)
   {
      struct stat  st;
      if (stat(szFilePath, &st) == 0)
      {
         if (S_ISREG(st.st_mode))
         {
            blFilePathExists = true;
            ETG_TRACE_USR4(("FmDatabaseHandler::blIsFileExists(): file path exists"));
         }
      }
   }
#endif
   return blFilePathExists;
}


tBool FmDatabaseHandler::bCreateFolder(tCString szFilePath)
{
   tBool blRet = false;
#if (OSAL_CONF==OSAL_LINUX)
   if (OSAL_NULL != szFilePath)
   {
      if (!blIsFolderExists(szFilePath))
      {
         if (mkdir(szFilePath, 0777) == 0)
         {
            ETG_TRACE_USR4(("FmDatabaseHandler::bCreateFolder(): folder created"));
            blRet = true;
         }
      }
      else
      {
         blRet = true;
      }
   }
#endif
   return blRet;
}


tBool FmDatabaseHandler::blMakePath(tCString strPath)
{
   tBool          blStatus = false;
   if (OSAL_NULL != strPath)
   {
      tChar*           pp;
      tChar*           sp;
      tChar*         copypath = strdup(strPath);

      blStatus = true;
      pp = copypath;
      while ((blStatus == true) && ((sp = (tChar*)OSAL_ps8StringSearchChar(pp, '/')) != 0))
      {
         if (sp != pp)
         {
            *sp = '\0';
            blStatus = bCreateFolder(copypath);
            *sp = '/';
         }
         pp = sp + 1;
      }
      if (blStatus)
      {
         blStatus = bCreateFolder(strPath);
      }
      free(copypath);
   }
   return blStatus;
}


tU32 FmDatabaseHandler::u32CreateFMRDSTunerTables(tVoid)
{
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != m_pFMRDShandle)
   {
      u32Ret = sqlite3_exec(m_pFMRDShandle, QUERY_CREATE_FM_STATION_LIST_TABLE, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         u32Ret =  SQLITE_ERROR;
      }
      else
      {
         u32Ret = sqlite3_exec(m_pFMRDShandle, QUERY_CREATE_FM_CHECKSUM_TABLE, OSAL_NULL, OSAL_NULL, OSAL_NULL);
         if (u32Ret != SQLITE_OK)
         {
            u32Ret =  SQLITE_ERROR;
         }
      }
   }
   return u32Ret;
}


tU32 FmDatabaseHandler::u32CreateFMHDTunerTables(tVoid)
{
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != m_pFMHDhandle)
   {
      u32Ret = sqlite3_exec(m_pFMHDhandle, QUERY_CREATE_FM_STATION_LIST_TABLE, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         u32Ret =  SQLITE_ERROR;
      }
      else
      {
         u32Ret = sqlite3_exec(m_pFMHDhandle, QUERY_CREATE_FM_CHECKSUM_TABLE, OSAL_NULL, OSAL_NULL, OSAL_NULL);
         if (u32Ret != SQLITE_OK)
         {
            u32Ret =  SQLITE_ERROR;
         }
      }
   }
   return u32Ret;
}


tU32 FmDatabaseHandler::u32CreateFMRDSTunerViews(tVoid)
{
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != m_pFMRDShandle)
   {
      u32Ret = sqlite3_exec(m_pFMRDShandle, QUERY_VIEW_FM_RDS_SHORT_NAME, OSAL_NULL, OSAL_NULL, OSAL_NULL);

      if (u32Ret != SQLITE_OK)
      {
         u32Ret =  SQLITE_ERROR;
      }

      if (SQLITE_OK == u32Ret)
      {
         u32Ret = sqlite3_exec(m_pFMRDShandle, QUERY_VIEW_FM_CHECKSUM, OSAL_NULL, OSAL_NULL, OSAL_NULL);
         if (SQLITE_OK != u32Ret)
         {
            u32Ret =  SQLITE_ERROR;
         }
      }
   }
   return u32Ret;
}


tU32 FmDatabaseHandler::u32CreateFMHDTunerViews(tVoid)
{
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != m_pFMHDhandle)
   {
      u32Ret = sqlite3_exec(m_pFMHDhandle, QUERY_VIEW_FM_HD_SHORT_NAME, OSAL_NULL, OSAL_NULL, OSAL_NULL);

      if (u32Ret != SQLITE_OK)
      {
         u32Ret =  SQLITE_ERROR;
      }

      if (SQLITE_OK == u32Ret)
      {
         u32Ret = sqlite3_exec(m_pFMHDhandle, QUERY_VIEW_FM_CHECKSUM, OSAL_NULL, OSAL_NULL, OSAL_NULL);
         if (SQLITE_OK != u32Ret)
         {
            u32Ret =  SQLITE_ERROR;
         }
      }
   }
   return u32Ret;
}


tVoid FmDatabaseHandler::vProcessStringsForEscapeSequence(tCString strSource, tString strDest)
{
   if (OSAL_NULL != strSource && OSAL_NULL != strDest)
   {
      tU16 l_u16StrLength = OSAL_u32StringLength(strSource);
      if (l_u16StrLength >= STR_MAX)
      {
         l_u16StrLength = STR_MAX - 1;
      }
      if (OSAL_NULL != OSAL_ps8StringSearchChar(strSource, SQLITE_ESCAPE_SEQUENCE))
      {
         tU8 l_u8CntDest = 0;
         for (tU8 l_u8CntSrc = 0; l_u8CntSrc < l_u16StrLength; l_u8CntSrc++)
         {
            if (strSource[l_u8CntSrc] != SQLITE_ESCAPE_SEQUENCE)
            {
               strDest[l_u8CntDest++] = strSource[l_u8CntSrc];
            }
            else
            {
               strDest[l_u8CntDest++] = SQLITE_ESCAPE_SEQUENCE;
               strDest[l_u8CntDest++] = SQLITE_ESCAPE_SEQUENCE;
            }
         }
         strDest[l_u8CntDest] = 0;
      }
      else
      {
         if (OSAL_NULL == OSAL_szStringNCopy(strDest, strSource, l_u16StrLength))
         {
            ETG_TRACE_USR4(("Copy failed"));
         }
      }
   }
}


tU32 FmDatabaseHandler::u32SetSynchronousModeOff(sqlite3* pDbHandle)
{
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != pDbHandle)
   {
      u32Ret = sqlite3_exec(pDbHandle, QUERY_PRAGAMA_SYNCHRONOUS_OFF, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         u32Ret = SQLITE_ERROR;
      }
   }
   return u32Ret;
}


tU32 FmDatabaseHandler::u32SetJournalModeMemory(sqlite3* pDbHandle)
{
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != pDbHandle)
   {
      u32Ret = sqlite3_exec(pDbHandle, QUERY_PRAGAMA_JOURNAL_MODE_MEM, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         u32Ret = SQLITE_ERROR;
      }
   }
   return u32Ret;
}


tU32 FmDatabaseHandler::u32BeginTransaction(sqlite3* pDbHandle)
{
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != pDbHandle)
   {
      u32Ret = sqlite3_exec(pDbHandle, QUERY_BEGIN_TRANSACTION, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         u32Ret = SQLITE_ERROR;
      }
   }
   return u32Ret;
}


tU32 FmDatabaseHandler::u32EndTransaction(sqlite3* pDbHandle)
{
   tU32 u32Ret = SQLITE_ERROR;
   if (OSAL_NULL != pDbHandle)
   {
      u32Ret = sqlite3_exec(pDbHandle, QUERY_END_TRANSACTION, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         u32Ret = SQLITE_ERROR;
      }
   }
   return u32Ret;
}


tVoid FmDatabaseHandler::vDeleteAllFromFMRDSStationList()
{
   tU32 u32Ret                     =   SQLITE_ERROR;
   if (OSAL_NULL != m_pFMRDShandle)
   {
      u32Ret = sqlite3_exec(m_pFMRDShandle, QUERY_DELETE_ALL_DATA_FM_STATION_LIST, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         //Error
      }
   }
}


tVoid FmDatabaseHandler::vDeleteAllFromFMHDSStationList()
{
   tU32 u32Ret                     =   SQLITE_ERROR;
   if (OSAL_NULL != m_pFMHDhandle)
   {
      u32Ret = sqlite3_exec(m_pFMHDhandle, QUERY_DELETE_ALL_DATA_FM_STATION_LIST, OSAL_NULL, OSAL_NULL, OSAL_NULL);
      if (u32Ret != SQLITE_OK)
      {
         //Error
      }
   }
}


tBool FmDatabaseHandler::blBeginTransactionForFMRDS(tVoid)
{
   tBool l_blRet =   false;
   if (u32BeginTransaction(m_pFMRDShandle) != SQLITE_ERROR)
   {
      l_blRet = true;
   }
   return l_blRet;
}


tBool FmDatabaseHandler::blBeginTransactionForFMHD(tVoid)
{
   tBool l_blRet =   false;
   if (u32BeginTransaction(m_pFMHDhandle) != SQLITE_ERROR)
   {
      l_blRet = true;
   }
   return l_blRet;
}


tBool FmDatabaseHandler::blEndTransactionForFMRDS(tVoid)
{
   tBool l_blRet =   false;
   if (u32EndTransaction(m_pFMRDShandle) != SQLITE_ERROR)
   {
      l_blRet = true;
   }
   return l_blRet;
}


tBool FmDatabaseHandler::blEndTransactionForFMHD(tVoid)
{
   tBool l_blRet =   false;
   if (u32EndTransaction(m_pFMHDhandle) != SQLITE_ERROR)
   {
      l_blRet = true;
   }
   return l_blRet;
}


tBool FmDatabaseHandler::bUpdateRecordForFMRDSStationList(FMChannelListItem* pFMStationListItem)
{
   tBool blRet = false;
   if (OSAL_NULL != pFMStationListItem)
   {
      tU32 u32Ret                     =   SQLITE_ERROR;
      if (OSAL_NULL != m_pFMRDShandle)
      {
         tString szQuery    =   OSAL_NEW tChar[MAX_QUERY_LEN];
         if (OSAL_NULL != szQuery)
         {
            tChar l_strName[STR_MAX] = {0};
            vProcessStringsForEscapeSequence((tCString)pFMStationListItem->m_sShortStationName, l_strName);
            OSAL_s32PrintFormat(szQuery, FORMAT_QUERY_UPDATE_FM_STATION_LIST,
                                pFMStationListItem->m_u32ObjectID,
                                pFMStationListItem->m_u32Frequency,
                                l_strName,
                                0
                               );
            u32Ret = sqlite3_exec(m_pFMRDShandle, szQuery, OSAL_NULL, OSAL_NULL, OSAL_NULL);
            if (u32Ret == SQLITE_OK)
            {
               blRet = true;
            }
            OSAL_DELETE []szQuery;
         }
      }
   }
   return blRet;
}


tBool FmDatabaseHandler::bUpdateRecordForHDStationList(FMChannelListItem* pFMStationListItem)
{
   ETG_TRACE_USR1(("FmDatabaseHandler::bUpdateRecordForHDStationList"));
   tBool blRet = false;
   if (OSAL_NULL != pFMStationListItem)
   {
      tU32 u32Ret                     =   SQLITE_ERROR;
      if (OSAL_NULL != m_pFMHDhandle)
      {
         tString szQuery    =   OSAL_NEW tChar[MAX_QUERY_LEN];
         if (OSAL_NULL != szQuery)
         {
            tChar l_strName[STR_MAX] = {0};
            vProcessStringsForEscapeSequence((tCString)pFMStationListItem->m_sShortStationName, l_strName);
            OSAL_s32PrintFormat(szQuery, FORMAT_QUERY_UPDATE_FM_STATION_LIST,
                                pFMStationListItem->m_u32ObjectID,
                                pFMStationListItem->m_u32Frequency,
                                l_strName,
                                pFMStationListItem->m_audioProgram
                               );
            u32Ret = sqlite3_exec(m_pFMHDhandle, szQuery, OSAL_NULL, OSAL_NULL, OSAL_NULL);
            if (u32Ret == SQLITE_OK)
            {
               blRet = true;
            }
            OSAL_DELETE []szQuery;
         }
      }
   }
   return blRet;
}


tBool FmDatabaseHandler::bUpdateChecksumForFMRDS(tVoid)
{
   tBool blRet = false;
   tU32 u32Ret                     =   SQLITE_ERROR;
   if (OSAL_NULL != m_pFMRDShandle)
   {
      tString szQuery    =   OSAL_NEW tChar[MAX_QUERY_LEN];
      if (OSAL_NULL != szQuery)
      {
         OSAL_s32PrintFormat(szQuery, FORMAT_QUERY_DELETE_FM_TIME_STAMP);
         ETG_TRACE_USR4(("SAAL_Tuner_tclDbHandler ::bUpdateChecksumForFM() szQuery=%s.", szQuery));
         u32Ret = sqlite3_exec(m_pFMRDShandle, szQuery, OSAL_NULL, OSAL_NULL, OSAL_NULL);
         OSAL_s32PrintFormat(szQuery, FORMAT_QUERY_UPDATE_FM_TIME_STAMP);
         ETG_TRACE_USR4(("SAAL_Tuner_tclDbHandler ::bUpdateChecksumForFM() szQuery=%s.", szQuery));
         u32Ret = sqlite3_exec(m_pFMRDShandle, szQuery, OSAL_NULL, OSAL_NULL, OSAL_NULL);
         if (u32Ret == SQLITE_OK)
         {
            blRet = true;
         }
         OSAL_DELETE []szQuery;
      }
   }
   return blRet;
}


tBool FmDatabaseHandler::bUpdateChecksumForFMHD(tVoid)
{
   tBool blRet = false;
   tU32 u32Ret                     =   SQLITE_ERROR;
   if (OSAL_NULL != m_pFMHDhandle)
   {
      tString szQuery    =   OSAL_NEW tChar[MAX_QUERY_LEN];
      if (OSAL_NULL != szQuery)
      {
         OSAL_s32PrintFormat(szQuery, FORMAT_QUERY_DELETE_FM_TIME_STAMP);
         ETG_TRACE_USR4(("SAAL_Tuner_tclDbHandler ::bUpdateChecksumForFM() szQuery=%s.", szQuery));
         u32Ret = sqlite3_exec(m_pFMHDhandle, szQuery, OSAL_NULL, OSAL_NULL, OSAL_NULL);
         OSAL_s32PrintFormat(szQuery, FORMAT_QUERY_UPDATE_FM_TIME_STAMP);
         ETG_TRACE_USR4(("SAAL_Tuner_tclDbHandler ::bUpdateChecksumForFM() szQuery=%s.", szQuery));
         u32Ret = sqlite3_exec(m_pFMHDhandle, szQuery, OSAL_NULL, OSAL_NULL, OSAL_NULL);
         if (u32Ret == SQLITE_OK)
         {
            blRet = true;
         }
         OSAL_DELETE []szQuery;
      }
   }
   return blRet;
}


tBool FmDatabaseHandler::blIsFolderExists(tCString szFilePath)
{
   tBool blFilePathExists = false;
#if (OSAL_CONF==OSAL_LINUX)
   if (OSAL_NULL != szFilePath)
   {
      struct stat  st;
      if (stat(szFilePath, &st) == 0)
      {
         if (S_ISDIR(st.st_mode))
         {
            blFilePathExists = true;
         }
      }
   }
#endif
   return blFilePathExists;
}
