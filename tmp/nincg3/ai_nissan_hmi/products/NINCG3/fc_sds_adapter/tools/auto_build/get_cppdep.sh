#!/bin/bash
# ==================================================================
# get the current version of the 'cppdep' tool from the Stash server
# ==================================================================

base_dir=~/temp
cppdep_dir=$base_dir/cppdep

# clone cppdep repository if it doesn't exist
if [ ! -d $cppdep_dir ]; then
    mkdir -p $base_dir
    pushd $base_dir &> /dev/null
    git clone ssh://git@sourcecode.socialcoding.bosch.com:7999/~jnd2hi/cppdep.git
    popd &> /dev/null
fi

# update cppdep repository
pushd $cppdep_dir &> /dev/null
git pull
popd &> /dev/null

