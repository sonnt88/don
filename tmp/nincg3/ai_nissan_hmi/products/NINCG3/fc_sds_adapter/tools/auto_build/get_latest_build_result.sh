#!/bin/bash
# ============================================================================
# Find the latest successful build created by 'build_latest_nincg3.sh' and
# create an archive with the results for upload onto the documentation server.
# ============================================================================

base_dir=~/temp

# list only PASSED files latest first and get the first listed item
latest_passed=$(ls -t ~/samba/auto_build/*/timestamp_end.log | grep -m 1 -E ".")

latest_dir=$(dirname $latest_passed)
if [ ! -d "$latest_dir" ]; then
    echo "no build results found"
    exit 1
fi

# create archive
pushd $latest_dir &> /dev/null
tar -czf $base_dir/latest_build_result.tar.gz .
popd &> /dev/null

