#!/bin/bash
# ============================================================================
# Perform a dependency analysis of the SdsAdapter component sources with
# 'cppdep' and prepare an archive with the results for upload onto the
# documentation server.
# ============================================================================

script_dir=$(dirname $0)
$script_dir/get_cppdep.sh

base_dir=~/temp
cppdep_dir=$base_dir/cppdep
hmi_dir=$base_dir/ai_nissan_hmi
result_dir=$base_dir/cppdep_sds_adapter

# run cppdep analysis on SdsAdapter
rm -rf $result_dir &> /dev/null
mkdir $result_dir
pushd $result_dir &> /dev/null
python $cppdep_dir/make_package_xml.py $hmi_dir/products/NINCG3/fc_sds_adapter/sources/adapter > adapter.xml
# patch toolchain directory in package description file
sed -i 's#/opt/tooling/.*#/opt/tooling/bosch/OSTC_2.1.9/i686-pc-linux-gnu#g' adapter.xml
python $cppdep_dir/cppdep.py -f adapter.xml > results.txt
grep "\[cycle\]" results.txt > cycles.txt
for file in *.dot; do
    dot -O -Tpng $file
done
mkdir dot
mkdir png
mv *.dot dot
mv *.png png
popd &> /dev/null

# create archive
pushd $base_dir &> /dev/null
tar -czf cppdep_sds_adapter.tar.gz cppdep_sds_adapter
popd &> /dev/null

