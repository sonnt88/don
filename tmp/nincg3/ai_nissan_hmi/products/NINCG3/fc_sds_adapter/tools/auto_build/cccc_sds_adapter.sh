#!/bin/bash
# ============================================================================
# Perform a code metrics analysis of the SdsAdapter component sources with
# 'cccc' and prepare an archive with the results for upload onto the
# documentation server.
# ============================================================================

base_dir=~/temp
source_dir=$base_dir/ai_nissan_hmi/products/NINCG3/fc_sds_adapter/sources/adapter
result_dir=$base_dir/cccc_sds_adapter

rm -rf $result_dir &> /dev/null
mkdir -p $result_dir
files=$(find $source_dir -name '*.cpp' -o -name '*.h')
cccc --outdir=$result_dir --html_outfile=$result_dir/index.html --lang=c++ $files &> $result_dir/cccc.log

# create archive
pushd $base_dir &> /dev/null
tar -czf cccc_sds_adapter.tar.gz cccc_sds_adapter
popd &> /dev/null

