#!/bin/bash
# ============================================================================
# Perform a code metrics analysis of the SdsAdapter component sources with
# 'cccc' and prepare an archive with the results for upload onto the
# documentation server.
# ============================================================================

base_dir=~/temp
source_dir=$base_dir/ai_nissan_hmi/products/NINCG3/Apps/AppHmi_Sds/App
result_dir=$base_dir/cccc_apphmi_sds

rm -rf $result_dir &> /dev/null
mkdir -p $result_dir
files=$(find $source_dir -name '*.cpp' -o -name '*.h')
cccc --outdir=$result_dir --html_outfile=$result_dir/index.html --lang=c++ $files &> $result_dir/cccc.log

# create archive
pushd $base_dir &> /dev/null
tar -czf cccc_apphmi_sds.tar.gz cccc_apphmi_sds
popd &> /dev/null

