#!/bin/bash
# ============================================================================
# Updates existing nincg3 repository and performs an incremental build.
# Upon encountering a new project release, all build results are cleared.
# The build is started when a change is detected in either the nincg3 root
# repository or the ai_nissan_hmi repository.
#
# The following builds are performed:
# - sds_adapter for ARM
# - lint sds_adapter for ARM
# - prj_overall for x86 to validate LSIM build
#
# The build results for each build are stored in a separate directory.
# ============================================================================

root_dir=$1
generated_dir=${root_dir}_GEN/ai_projects/generated
result_root=~/samba/auto_build

if [ -z "$root_dir" ]; then
    echo "usage: build_latest_nincg3.sh <path to root repository>"
    exit 1
fi

date --rfc-3339=seconds
echo "updating git repositories ..."
cd $root_dir
echo ""                                                       &>> $result_root/git_update.log
date --rfc-3339=seconds                                       &>> $result_root/git_update.log
git fetch --all --recurse-submodules --prune                  &>> $result_root/git_update.log
git checkout origin/nissan_ncg3_int                           &>> $result_root/git_update.log
git submodule foreach git checkout origin/nissan_ncg3_int     &>> $result_root/git_update.log
echo ""                                                       &>> $result_root/git_update.log

pushd $root_dir/ai_projects &> /dev/null
label=$(cat cc_label.txt | grep -o -E -m1 "AI_PRJ_[._A-Za-z0-9]+" | grep -o -E -m1 ".*")
popd &> /dev/null

pushd $root_dir &> /dev/null
root_commit=$(git rev-parse --short HEAD)
popd &> /dev/null

pushd $root_dir/ai_nissan_hmi &> /dev/null
hmi_commit=$(git rev-parse --short HEAD)
popd &> /dev/null

result_dir=$result_root/${label}_${root_commit}_${hmi_commit}

if [ -d "$result_dir" ]; then 
    echo "repositories have not changed - nothing to do"
    exit 0
fi

if [ -z "$(ls $result_root/ | grep $label)" ]; then 
    # relase label has changed - need to remove all build results
    echo "new project release - deleting build results ..."
    rm -rf $generated_dir
fi

mkdir -p $result_dir
echo "logging results to '$result_dir'"

date --rfc-3339=seconds &> $result_dir/timestamp_start.log

echo "root dir: $root_dir"           >  $result_dir/variables.log
echo "generated dir: $generated_dir" >> $result_dir/variables.log
echo "label: $label"                 >> $result_dir/variables.log
echo "root commit: $root_commit"     >> $result_dir/variables.log
echo "hmi commit: $hmi_commit"       >> $result_dir/variables.log

echo "logging repository and submodule states ..."
pushd $root_dir &> /dev/null
git log -1 &> $result_dir/git_root_commit.log
git submodule status &> $result_dir/git_submodule_status.log
popd &> /dev/null

pushd $root_dir/ai_projects &> /dev/null

echo "setting up build environment ..."
cp ./tools/prj_build/set_env.sh $result_dir/
sed -i 's#(bash|-bash)#(bash|-bash|*.sh)#g' $result_dir/set_env.sh
source $result_dir/set_env.sh &> $result_dir/set_env.log

echo "building sds_adapter and apphmi_sds for ARM ..."
perl ${_SWTOOLROOT}/tools/build/BuildProduct.pl --env=gen3armmake --mode=release --buildmode=build --altenv=eclipse sds_adapter apphmi_sds &> $result_dir/build_arm.log

echo "linting sds_adapter and apphmi_sds ARM build ..."
perl $_SWTOOLROOT/tools/build/BuildProduct.pl --env=gen3armmake --mode=release --buildmode=build --altenv=eclipse sds_adapter apphmi_sds --noprecreate --alldeps=none --lint &> $result_dir/build_arm_lint.log
perl $_SWROOT/ai_projects/tools/prj_build/catalogue_filtered_lint.pl $_SWBUILDROOT/generated/logfiles/gen3armmake/sds_adapter/sds_adapter-rnaivi_out_r/lint_gen3armmake.log -s &> $result_dir/build_arm_lint_sorted.log
perl $_SWROOT/ai_projects/tools/prj_build/catalogue_filtered_lint.pl $_SWBUILDROOT/generated/logfiles/gen3armmake/apphmi_asf_applications/apphmi_sds-cgi2-rnaivi_out_r/lint_gen3armmake.log -s &>> $result_dir/build_arm_lint_sorted.log
cp $generated_dir/logfiles/gen3armmake/sds_adapter/sds_adapter-rnaivi_out_r/lint_gen3armmake.log_catalogued.log $result_dir/sds_adapter_lint.log
cp $generated_dir/logfiles/gen3armmake/apphmi_asf_applications/apphmi_sds-cgi2-rnaivi_out_r/lint_gen3armmake.log_catalogued.log $result_dir/apphmi_sds_lint.log

# filter lint result for listing in Eclipse console
# rename 'Warning' and 'Info' to 'warning' so that warning lookup works from Eclipse console
cat $result_dir/sds_adapter_lint.log | grep "^\s*/.*/sources/adapter/" | sed "s#: Warning #: warning #g" | sed "s#: Info #: warning : Info #g" | sed -r "s#\s*/home/.+/ai_nissan_hmi/products/NINCG3/fc_sds_adapter/sources/##g" > $result_dir/sds_adapter_lint_console.log
cat $result_dir/apphmi_sds_lint.log | grep "^\s*/.*/AppHmi_Sds/App/Core/" | sed "s#: Warning #: warning #g" | sed "s#: Info #: warning : Info #g" | sed -r "s#\s*/home/.+/ai_nissan_hmi/products/NINCG3/Apps/AppHmi_Sds/##g" > $result_dir/apphmi_sds_lint_console.log


echo "building prj_overall for x86 ..."
perl ${_SWTOOLROOT}/tools/build/BuildProduct.pl --env=gen3x86make --mode=debug --buildmode=build --altenv=eclipse prj_overall &> $result_dir/build_x86.log

popd &> /dev/null

errors=$(grep -A50 "some errors occured" $result_dir/*)
if [ ! -z "$errors" ]; then
    touch $result_dir/FAILED
    echo "FAILED"
    echo $errors
else
    touch $result_dir/PASSED
    echo "PASSED"
fi

date --rfc-3339=seconds &> $result_dir/timestamp_end.log

