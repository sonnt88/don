#!/bin/bash
# ============================================================================
# Perform a dependency analysis of the SdsAdapter component sources with
# 'cppdep' and prepare an archive with the results for upload onto the
# documentation server.
# ============================================================================

script_dir=$(dirname $0)
$script_dir/get_cppdep.sh

base_dir=~/temp
cppdep_dir=$base_dir/cppdep
hmi_dir=$base_dir/ai_nissan_hmi
result_dir=$base_dir/cppdep_apphmi_sds

# run cppdep analysis on SdsAdapter
rm -rf $result_dir &> /dev/null
mkdir $result_dir
pushd $result_dir &> /dev/null
python $cppdep_dir/make_package_xml.py $hmi_dir/products/NINCG3/Apps/AppHmi_Sds/App > apphmi_sds.xml
# patch toolchain directory in package description file
sed -i 's#/opt/tooling/.*#/opt/tooling/bosch/OSTC_2.1.9/i686-pc-linux-gnu#g' apphmi_sds.xml
python $cppdep_dir/cppdep.py -f apphmi_sds.xml > results.txt
grep "\[cycle\]" results.txt > cycles.txt
for file in *.dot; do
    dot -O -Tpng $file
done
mkdir dot
mkdir png
mv *.dot dot
mv *.png png
popd &> /dev/null

# create archive
pushd $base_dir &> /dev/null
tar -czf cppdep_apphmi_sds.tar.gz cppdep_apphmi_sds
popd &> /dev/null

