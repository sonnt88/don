#!/bin/bash
# ============================================
# build the asciidoc-based docs for SdsAdapter
# ============================================

script_dir=$(dirname $0)
$script_dir/get_ai_nissan_hmi.sh

base_dir=~/temp
hmi_dir=$base_dir/ai_nissan_hmi
doc_dir=$hmi_dir/products/NINCG3/fc_sds_adapter/doc

# run cppdep analysis on SdsAdapter
$doc_dir/build_docs.sh --uml

# create archive
pushd $doc_dir &> /dev/null
tar -cf $base_dir/sds_adapter_docs.tar sds_adapter.chunked sds_adapter.pdf sds_adapter.html index.html
find . -name '*.adoc' | xargs tar -uf $base_dir/sds_adapter_docs.tar
find . -name '*.png' -o -name '*.jpg' | xargs tar -uf $base_dir/sds_adapter_docs.tar
gzip -f $base_dir/sds_adapter_docs.tar
popd &> /dev/null

