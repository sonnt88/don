#!/bin/bash
# ==========================================================================
# This script must be deployed on the HTTP server hosting the documentation.
# It is meant to be called on a regular timely basis by cron.
#
# It remotely calls the various scripts for providing current build results
# on the documentation server.
# ==========================================================================

# latest build result
tool_dir=/home/jnd2hi/temp/ai_nissan_hmi/products/NINCG3/fc_sds_adapter/tools/auto_build
ssh hi-z0af4.hi.de.bosch.com $tool_dir/get_latest_build_result.sh

dir_name=latest_build_result
archive=$dir_name.tar.gz
rm -rf ~/sds_adapter/code_analysis/$dir_name &> /dev/null
mkdir -p ~/sds_adapter/code_analysis/$dir_name
cd ~/sds_adapter/code_analysis/$dir_name
scp hi-z0af4.hi.de.bosch.com:temp/$archive .
tar -x --overwrite -z -f $archive
rm $archive

# copy lint results of apphmi_sds
rm -rf ~/apphmi_sds/code_analysis/lint &> /dev/null
mkdir -p ~/apphmi_sds/code_analysis/lint
mv ~/sds_adapter/code_analysis/$dir_name/apphmi_sds_lint.log          ~/apphmi_sds/code_analysis/lint/
mv ~/sds_adapter/code_analysis/$dir_name/apphmi_sds_lint_console.log  ~/apphmi_sds/code_analysis/lint/
cp ~/sds_adapter/code_analysis/$dir_name/git_*                        ~/apphmi_sds/code_analysis/lint/

