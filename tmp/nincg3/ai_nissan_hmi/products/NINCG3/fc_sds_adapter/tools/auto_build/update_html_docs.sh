#!/bin/bash
# ==========================================================================
# This script must be deployed on the HTTP server hosting the documentation.
# It is meant to be called on a regular timely basis by cron.
#
# It remotely calls the various scripts for building the documentation, then
# copies over the compressed archives with the build results and extracts
# them to the appropriate directories of the HTTP server.
# ==========================================================================

function clone_and_update_ai_nissan_hmi()
{
    # define the directories on the build server
    base_dir=/home/jnd2hi/temp
    hmi_dir=$base_dir/ai_nissan_hmi
    branch=nissan_ncg3_int

    # clone and update repo on build server via ssh
    ssh hi-z0af4.hi.de.bosch.com "
        if [ ! -d $hmi_dir ]; then
            mkdir -p $base_dir
            cd $base_dir
            git clone --branch $branch gitolite@hi-z0af4.hi.de.bosch.com:ai_nissan_hmi.1 $hmi_dir
        fi

        cd $hmi_dir
        git reset --hard
        git clean -xdf
        git fetch --all --prune
        git checkout origin/$branch --quiet"
}


clone_and_update_ai_nissan_hmi

tool_dir=/home/jnd2hi/temp/ai_nissan_hmi/products/NINCG3/fc_sds_adapter/tools/auto_build

# build docs for sds_adapter
ssh hi-z0af4.hi.de.bosch.com $tool_dir/build_sds_adapter_docs.sh
mkdir -p ~/sds_adapter/handbook
cd ~/sds_adapter/handbook
scp hi-z0af4.hi.de.bosch.com:temp/sds_adapter_docs.tar.gz .
tar -x --overwrite -z -f sds_adapter_docs.tar.gz
rm sds_adapter_docs.tar.gz
mv ~/sds_adapter/handbook/index.html ~/sds_adapter/

# build docs for apphmi_sds
ssh hi-z0af4.hi.de.bosch.com "cd temp/ai_nissan_hmi/products/NINCG3/Apps/AppHmi_Sds/Doc; ./build_docs.sh"
cd ~/apphmi_sds
scp hi-z0af4.hi.de.bosch.com:temp/ai_nissan_hmi/products/NINCG3/Apps/AppHmi_Sds/Doc/SdsHmiApplication_docs.tar.gz .
tar -x --overwrite -z -f SdsHmiApplication_docs.tar.gz
rm SdsHmiApplication_docs.tar.gz

# cppdep sds_adapter
dir_name=cppdep_sds_adapter
archive=$dir_name.tar.gz
ssh hi-z0af4.hi.de.bosch.com $tool_dir/cppdep_sds_adapter.sh
mkdir -p ~/sds_adapter/code_analysis
rm -rf ~/sds_adapter/code_analysis/$dir_name &> /dev/null
cd ~/sds_adapter/code_analysis
scp hi-z0af4.hi.de.bosch.com:temp/$archive .
tar -x --overwrite -z -f $archive
rm $archive

# cppdep apphmi_sds
dir_name=cppdep_apphmi_sds
archive=$dir_name.tar.gz
ssh hi-z0af4.hi.de.bosch.com $tool_dir/cppdep_apphmi_sds.sh
mkdir -p ~/apphmi_sds/code_analysis
rm -rf ~/apphmi_sds/code_analysis/$dir_name &> /dev/null
cd ~/apphmi_sds/code_analysis
scp hi-z0af4.hi.de.bosch.com:temp/$archive .
tar -x --overwrite -z -f $archive
rm $archive

# cccc sds_adapter
dir_name=cccc_sds_adapter
archive=$dir_name.tar.gz
ssh hi-z0af4.hi.de.bosch.com $tool_dir/cccc_sds_adapter.sh
mkdir -p ~/sds_adapter/code_analysis
rm -rf ~/sds_adapter/code_analysis/$dir_name &> /dev/null
cd ~/sds_adapter/code_analysis
scp hi-z0af4.hi.de.bosch.com:temp/$archive .
tar -x --overwrite -z -f $archive
rm $archive

# cccc apphmi_sds
dir_name=cccc_apphmi_sds
archive=$dir_name.tar.gz
ssh hi-z0af4.hi.de.bosch.com $tool_dir/cccc_apphmi_sds.sh
mkdir -p ~/apphmi_sds/code_analysis
rm -rf ~/apphmi_sds/code_analysis/$dir_name &> /dev/null
cd ~/apphmi_sds/code_analysis
scp hi-z0af4.hi.de.bosch.com:temp/$archive .
tar -x --overwrite -z -f $archive
rm $archive

