#!/bin/bash

# LSIM must be rebooted after executing this script for the changes to take effect

target=root@172.17.0.11

# repair sporadically no TTFis output - NCG3D-8629
ssh $target "systemctl disable trace-ttfis-backend.service"

# sync file system changes
ssh $target "sync; sync"

