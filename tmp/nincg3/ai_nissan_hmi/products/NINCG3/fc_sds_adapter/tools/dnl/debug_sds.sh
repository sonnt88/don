#!/bin/bash

remote=$1
state=$2

if [ ! "$remote" == "lsim3" -a ! "$remote" == "target" ]; then
    echo "error: you must specify the remote machine as 'lsim3' or 'target'"
    exit 1
fi

if [ ! "$state" == "on" -a ! "$state" == "off" ]; then
    echo "error: debugger state must be either 'on' or 'off'"
    exit 1
fi

if [ "$_SWNAVIROOT" == "" ]; then
    echo "error: this script must be executed from build environment"
    exit 1
fi

ssh $remote "mount -o rw,remount /"

if [ "$state" == "on" ]; then
    # insert gdbserver command before executable
    ssh $remote "sed -i -E 's#(-c.+)(/opt/bosch/sds)#\1gdbserver :2345 \2#g' /lib/systemd/system/rbcm-sds_adapter.service"
else
    # remove gdbserver command before executable
    ssh $remote "sed -i 's#gdbserver :2345 ##g' /lib/systemd/system/rbcm-sds_adapter.service"
fi

ssh $remote "sync; sync"
