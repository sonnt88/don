#!/bin/bash

remote=$1

if [ ! "$remote" == "lsim3" -a ! "$remote" == "target" ]; then
    echo "error: you must specify the remote machine as 'lsim3' or 'target'"
    exit 1
fi

ssh $remote "mount -o rw,remount /"

# create logger configuration file
logConfig=/opt/bosch/hmibase/logConfigInfo.json
ssh $remote "echo '{' > $logConfig"
ssh $remote "echo '    \"loggerLevel\" : {' >> $logConfig"
ssh $remote "echo '        \"*\" : \"Info\"' >> $logConfig"
ssh $remote "echo '    },' >> $logConfig"
ssh $remote "echo '    \"disableColoring\" : true' >> $logConfig"
ssh $remote "echo '}' >> $logConfig"

# replace logger configuration
ssh $remote "sed -i 's#logConfig.json#logConfigInfo.json#g' /lib/systemd/system/rbcm-apphmi_sds.service"
ssh $remote "sed -i 's#logConfig.json#logConfigInfo.json#g' /lib/systemd/system/rbcm-sds_adapter.service"

ssh $remote "sync; sync"

