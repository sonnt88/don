#!/bin/bash

ssh lsim3 "mount -o rw,remount /"

ssh lsim3 "rm /opt/bosch/sds/bin/sds*_out.*"                    &> /dev/null
ssh lsim3 "rm /opt/bosch/sds/bin/procsds*"                      &> /dev/null
ssh lsim3 "rm /lib/systemd/system/*sds*.service"                &> /dev/null
ssh lsim3 "rm -rf /var/opt/bosch/static/SDS/*"                  &> /dev/null
ssh lsim3 "rm -rf /var/opt/bosch/dynamic/ffs/registry/*sds*"    &> /dev/null

ssh lsim3 "sync; sync"
