#!/bin/bash

commit=SDS_BIN_16.0V19_2

remote=$1
scriptDir=$(dirname $0)

if [ "$2" == "--sdb" ]; then
    echo "error: option --sdb no longer supported - use --sds-bin instead"
    exit 1
fi

if [ ! "$remote" == "lsim3" -a ! "$remote" == "target" -a ! "$remote" == "root@172.17.0.1" -a ! "$remote" == "root@172.17.0.11" ]; then
    echo "error: you must specify the remote machine as 'lsim3' or 'target'"
    exit 1
fi

if [ "$_SWNAVIROOT" == "" ]; then
    echo "error: this script must be executed from build environment"
    exit 1
fi


function move_copy()
{
    src=$1
    remote=$2
    dest=$3

    if [ -e $src ]; then
        ssh $remote "mv $dest $dest.old" &> /dev/null
        scp $src $remote:$dest
    else
        echo "warning: file not found: $src"
    fi
}


ssh $remote "mount -o rw,remount /"

if [ "$remote" == "lsim3" -o "$remote" == "root@172.17.0.11" ]; then
    move_copy $_SWBUILDROOT/generated/bin/gen3x86make/debug/stripped/sds_adapter-rnaivi_stripped_out.out                               $remote /opt/bosch/sds/bin/sds_adapter_out.out
    move_copy $_SWBUILDROOT/generated/bin/gen3x86make/debug/stripped/apphmi_sds-cgi2-rnaivi_stripped_out.out                           $remote /opt/bosch/base/bin/apphmi_sds_out.out
    move_copy $_SWBUILDROOT/generated/bin/gen3x86make/debug/stripped/libasf_sds_gui_fi-asf_stripped_so.so                              $remote /opt/bosch/base/lib/libasf_sds_gui_fi-asf_so.so
    move_copy $_SWBUILDROOT/generated/bin/gen3x86make/debug/stripped/libasf_sds_sxm_fi-asf_stripped_so.so                              $remote /opt/bosch/base/lib/libasf_sds_sxm_fi-asf_so.so
    move_copy $_SWBUILDROOT/generated/bin/gen3x86make/debug/stripped/libasf_sds_fm_fi-asf_stripped_so.so                               $remote /opt/bosch/base/lib/libasf_sds_fm_fi-asf_so.so
    move_copy $_SWBUILDROOT/generated/bin/gen3x86make/debug/stripped/libasf_navigationservice-asf_stripped_so.so                       $remote /opt/bosch/base/lib/libasf_navigationservice-asf_so.so
    move_copy $_SWBUILDROOT/generated/bin/gen3x86make/debug/stripped/libasf_audproc_introspection_fi-asf_stripped_so.so                $remote /opt/bosch/base/lib/libasf_audproc_introspection_fi-asf_so.so
    move_copy $_SWBUILDROOT/generated/bin/gen3x86make/debug/stripped/libasf_ecnr_sse_introspection_fi-asf_stripped_so.so               $remote /opt/bosch/base/lib/libasf_ecnr_sse_introspection_fi-asf_so.so
    move_copy $_SWBUILDROOT/generated/bin/gen3x86make/debug/stripped/libasf_sds_cmb_acr_fi-asf_stripped_so.so                          $remote /opt/bosch/base/lib/libasf_sds_cmb_acr_fi-asf_so.so
    move_copy $_SWNAVIROOT/../ai_nissan_hmi/products/NINCG3/fc_sds_adapter/config/StartupScripts/rbcm-sds_adapter_lsim.service         $remote /lib/systemd/system/rbcm-sds_adapter.service
    source $scriptDir/patch_lsim.sh
else
    move_copy $_SWBUILDROOT/generated/bin/gen3armmake/release/stripped/sds_adapter-rnaivi_stripped_out.out                             $remote /opt/bosch/sds/bin/sds_adapter_out.out
    move_copy $_SWBUILDROOT/generated/bin/gen3armmake/release/stripped/apphmi_sds-cgi2-rnaivi_stripped_out.out                         $remote /opt/bosch/base/bin/apphmi_sds_out.out
    move_copy $_SWBUILDROOT/generated/bin/gen3armmake/release/stripped/libasf_sds_gui_fi-asf_stripped_so.so                            $remote /opt/bosch/base/lib/libasf_sds_gui_fi-asf_so.so
    move_copy $_SWBUILDROOT/generated/bin/gen3armmake/release/stripped/libasf_sds_sxm_fi-asf_stripped_so.so                            $remote /opt/bosch/base/lib/libasf_sds_sxm_fi-asf_so.so
    move_copy $_SWBUILDROOT/generated/bin/gen3armmake/release/stripped/libasf_sds_fm_fi-asf_stripped_so.so                             $remote /opt/bosch/base/lib/libasf_sds_fm_fi-asf_so.so
    move_copy $_SWBUILDROOT/generated/bin/gen3armmake/release/stripped/libasf_navigationservice-asf_stripped_so.so                     $remote /opt/bosch/base/lib/libasf_navigationservice-asf_so.so
    move_copy $_SWBUILDROOT/generated/bin/gen3armmake/release/stripped/libasf_audproc_introspection_fi-asf_stripped_so.so              $remote /opt/bosch/base/lib/libasf_audproc_introspection_fi-asf_so.so
    move_copy $_SWBUILDROOT/generated/bin/gen3armmake/release/stripped/libasf_ecnr_sse_introspection_fi-asf_stripped_so.so             $remote /opt/bosch/base/lib/libasf_ecnr_sse_introspection_fi-asf_so.so
    move_copy $_SWBUILDROOT/generated/bin/gen3armmake/release/stripped/libasf_sds_cmb_acr_fi-asf_stripped_so.so                        $remote /opt/bosch/base/lib/libasf_sds_cmb_acr_fi-asf_so.so
    move_copy $_SWNAVIROOT/../ai_nissan_hmi/products/NINCG3/fc_sds_adapter/config/StartupScripts/rbcm-sds_adapter_target.service       $remote /lib/systemd/system/rbcm-sds_adapter.service
fi

scp $_SWNAVIROOT/../ai_nissan_hmi/products/NINCG3/fc_sds_adapter/config/registry/sds_adapter.reg                                       $remote:/opt/bosch/base/registry/
scp $_SWNAVIROOT/../ai_nissan_hmi/products/NINCG3/fc_sds_adapter/config/logging/sds_adapter_log_config.json                            $remote:/opt/bosch/sds/bin/


if [ "$2" == "--sds-bin" ]; then
    source $scriptDir/clone_and_copy_sds_bin.sh $remote $commit
else
    echo "skipping copying of SDS binaries and SDB data - use option --sds-bin to copy"
fi


ssh $remote "sync; sync"

