#!/bin/bash

remote=$1
commit=$2

if [ ! "$remote" == "lsim3" -a ! "$remote" == "target" -a ! "$remote" == "root@172.17.0.1" -a ! "$remote" == "root@172.17.0.11" ]; then
    echo "error: you must specify the remote machine as 'lsim3' or 'target'"
    exit 1
fi

if [ -z "$commit" ]; then
    echo "error: you must specify a commit id or tag"
    exit 1
fi

sdsdir=~/samba/sds/sds_bin

if [ ! -e $sdsdir ]; then
    echo "cloning SDS binary repository"
    mkdir -p ~/samba/sds
    pushd ~/samba/sds > /dev/null
    git clone gitserver:users/jnd2hi/sds_bin
    popd > /dev/null
fi

echo "using binaries of $commit"
echo "updating SDS binary repository"
pushd $sdsdir > /dev/null
git fetch --all
git checkout --quiet $commit
popd > /dev/null

$sdsdir/sds_to_remote.sh $remote $commit

