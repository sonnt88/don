#!/bin/bash

remote=$1

function usage_and_exit()
{
    prog=$(basename $0)
    echo ""
    echo "$prog - determine some key information about SDS on target or LSIM"
    echo ""
    echo "usage: $prog <remote>"
    echo ""
    echo "example: $prog lsim3"
    exit 1
}


if [ ! "$remote" == "lsim3" -a ! "$remote" == "target" -a ! "$remote" == "root@172.17.0.1" -a ! "$remote" == "root@172.17.0.11" ]; then
    echo "error: you must specify the remote machine as 'lsim3', 'target', 'root@172.17.0.1' or 'root@172.17.0.11'"
    usage_and_exit
fi


ssh $remote "pstree | grep sds"
ssh $remote "ls -l /var/opt/bosch/sdb"
ssh $remote "ls -ld /var/opt/bosch/sdb/*/"

