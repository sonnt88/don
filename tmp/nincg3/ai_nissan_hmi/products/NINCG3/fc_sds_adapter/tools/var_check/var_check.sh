#!/usr/bin/python

import os
import re
import subprocess
import sys


def _sorted(lst):
    return sorted(lst, key=lambda s: s.lower())

def printTitle(text):
    print "=" * len(text)
    print text
    print "=" * len(text)

def varCheck():
    scriptDir = os.path.dirname(sys.argv[0])
    sourceDir = os.path.abspath(os.path.join(scriptDir, "../../sources/adapter"))
    files = findFiles(sourceDir)
    grepResult = grepVariables(files)
    viewVars = makeCrossRef(grepResult, "view_db")
    sourceVars = makeCrossRef(grepResult, "!view_db")

    undefined = findUndefined(viewVars, sourceVars)
    unused = findUnused(viewVars, sourceVars)
    used = findUsed(viewVars, sourceVars)

    printTitle("following variables are used by view data, but not defined in source code")
    print "\n".join([var for var in undefined])
    print
    printTitle("following variables are defined in source code, but not used by view data")
    printCrossRef(unused)
    print
    printTitle("following variables are defined in source code and used by view data")
    printCrossRef(used)

def findUndefined(viewVars, sourceVars):
    undefined = []
    for var in viewVars:
        if var not in sourceVars:
            undefined.append(var)
    return _sorted(undefined)

def findUnused(viewVars, sourceVars):
    unused = {}
    for var in sourceVars:
        if var not in viewVars:
            unused[var] = sourceVars[var]
    return unused

def findUsed(viewVars, sourceVars):
    used = {}
    for var in sourceVars:
        if var in viewVars:
            used[var] = sourceVars[var]
    return used

def makeCrossRef(grepResult, filenameFilter):
    crossRef = {}
    for filename, matchString in grepResult:
        if (filenameFilter[0] == "!" and filenameFilter[1:] not in filename) or (filenameFilter[0] != "!" and filenameFilter in filename):
            if matchString in crossRef:
                crossRef[matchString].add(filename)
            else:
                crossRef[matchString] = set((filename,))
    return crossRef

def findFiles(dirName):
    command = ["find", dirName]
    proc = subprocess.Popen(command, stdout=subprocess.PIPE)
    files, _err = proc.communicate()
    return files

def grepVariables(files):
    command = ["xargs", "grep", "--only-matching", "--extended-regexp", r'\$\(\w+\)']
    proc = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    proc.stdin.write(files)
    out, _err = proc.communicate()
    grepResult = []
    for line in out.splitlines():
        filename, match = line.split(":", 1)
        grepResult.append((filename, match.strip('"')))
    return grepResult

def printCrossRef(crossRef):
    for var in _sorted(crossRef.keys()):
        print var
        for filename in _sorted(crossRef[var]):
            print "\t" + filename

if __name__ == "__main__":
    varCheck()

