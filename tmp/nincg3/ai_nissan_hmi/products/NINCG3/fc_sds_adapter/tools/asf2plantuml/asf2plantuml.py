#!/usr/bin/python

import os
import sys
import re
import textwrap


def makeComponentDiagram(filename):
    text = open(filename).read()
    component = findComponent(text)
    provided = findProvided(text)
    required = findRequired(text)
    uml = "@startuml\n"
    uml += 'component c as "%s"\n' % component
    for index, interface in enumerate(provided):
        uml += 'interface provided_%d as "%s"\n' % (index, wrap(interface))
        uml += '[c] -up- provided_%d\n' % (index)
    for index, interface in enumerate(required):
        uml += 'interface required_%d as "%s"\n' % (index, wrap(interface))
        uml += '[c] .down.> required_%d\n' % (index)
    uml += "@enduml\n"
    return uml

def findComponent(text):
    mo = re.search(r"^\s*component\s+([.\w]+)", text, re.M)
    if mo:
        return mo.group(1)
    return ""

def findProvided(text):
    return re.findall(r"^\s*provides\s+([.\w]+)", text, re.M)

def findRequired(text):
    return re.findall(r"^\s*requires\s+([.\w]+)", text, re.M)

def wrap(text, width=20):
    text = text.replace(".", ". ").replace("_", "_ ")
    text = textwrap.fill(text, width)
    return text.replace("\n", r"\n").replace(" ", "")

if __name__ == "__main__":
    filename = sys.argv[1]
    uml = makeComponentDiagram(filename)
    print uml
