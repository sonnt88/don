/**
 * @file <HmiDataHandler.h>
 * @author <Gobika Sethupalanichamy - ECV3>A-IVI
 * @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
 * @addtogroup <AppHmi_Common>
 */


#ifndef HMI_DATAHANDLER_H
#define HMI_DATAHANDLER_H


#include "bosch/cm/ai/nissan/hmi/hmidataservice/HmiDataProxy.h"

#define HMIDATASERVICE_COMMON ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData //Instead of namespace, Macro is used



namespace HmiDataCommonHandler
{

/*
 * ISwipeDirectionStatusClientPropertyCB is used to
 *  - Provide interface to communicate the HmiDataServiceClientProperty update
 *  	to HmiDataService service client users
 */
class ISwipeDirectionStatusClientPropertyCB
{
   public:
      ISwipeDirectionStatusClientPropertyCB() {}
      virtual ~ISwipeDirectionStatusClientPropertyCB() {}
      virtual void handleSwipeDirectionPropertyUpdate(const uint8 swipeDirectionType) = 0;
};

class HmiDataHandler: public ::asf::core::ServiceAvailableIF,
   public HMIDATASERVICE_COMMON::SwipeDirectionStatusCallbackIF
{
   public:
      /**
       *  Member Function Declaration
       *
       */

      HmiDataHandler();
      virtual ~HmiDataHandler();
      /**
       *  Virtual methods from  ServiceAvailableIF
       *
       */
      void onAvailable(const boost::shared_ptr< asf::core::Proxy >& proxy, \
                       const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(const boost::shared_ptr< asf::core::Proxy >& proxy, \
                         const asf::core::ServiceStateChange& stateChange);
      /**
       *  Virtual methods from  SwipeDirectionStatusCallbackIF
       *
       */

      void onSwipeDirectionStatusError(const ::boost::shared_ptr< HMIDATASERVICE_COMMON::HmiDataProxy >& proxy, const ::boost::shared_ptr< HMIDATASERVICE_COMMON::SwipeDirectionStatusError >& error);
      void onSwipeDirectionStatusUpdate(const ::boost::shared_ptr< HMIDATASERVICE_COMMON::HmiDataProxy >& proxy, const ::boost::shared_ptr< HMIDATASERVICE_COMMON::SwipeDirectionStatusUpdate >& update);

      static HmiDataHandler* GetInstance();

      void vRegisterforUpdate(ISwipeDirectionStatusClientPropertyCB* client);
      void vUnRegisterforUpdate(ISwipeDirectionStatusClientPropertyCB* client);



   protected:
      /**
       *  Member Variables Declaration
       */
      ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::HmiDataProxy > _hmiDataProxy;
      void registerBaseProperties();

   private:
      /**
       *  Member Variables Declaration
       */
      static HmiDataHandler* _commonHmiDataHandler;
      std::vector<ISwipeDirectionStatusClientPropertyCB*> _SwipeClientCallback;


};

}// namespace HmiDataHandler

#endif //HMIDATAHANDLER_H
