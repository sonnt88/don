/**
 * @file <VehicleDataHandler.h>
 * @author <Gobika Sethupalanichamy - ECV3>A-IVI
 * @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
 * @addtogroup <AppHmi_Common>
 */


#include "HmiDataHandler.h"
#include "AppBase/StartupSync/StartupSync.h"
#include "ProjectBaseMsgs.h"
#include "hmi_trace_if.h"
#include "../Common_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_HMIDATA
#include "trcGenProj/Header/HmiDataHandler.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN

using namespace std;
using namespace ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData;


namespace HmiDataCommonHandler {
HmiDataHandler* HmiDataHandler::_commonHmiDataHandler = NULL;
/*
 * @Constructor
 */
HmiDataHandler::HmiDataHandler() :
   _hmiDataProxy(HmiDataProxy::createProxy("hmiDataServicePort", *this))

{
   ETG_TRACE_USR4(("HmiDataHandler::HmiDataHandler Constructor"));
   _commonHmiDataHandler = this;
}


/*
 * GetInstance - To get the common hmiDatahandler instance
 * @param[in] None
 * @param[out] None
 * @return HmiDataHandler
 */
HmiDataHandler* HmiDataHandler::GetInstance()
{
   ETG_TRACE_USR4(("HmiDataHandler::GetInstance"));
   return _commonHmiDataHandler;
}


/*
 * @Destructor
 */
HmiDataHandler::~HmiDataHandler()
{
   _commonHmiDataHandler = NULL;
}


/*
 * onAvailable - To Handle Service Availability
 * @param[in] proxy
 * @param[in] stateChange
 * @param[out] None
 * @return void
 */
void HmiDataHandler::onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy, \
                                 const asf::core::ServiceStateChange& stateChange)
{
   ETG_TRACE_USR4(("HmiDataHandler::onAvailable received"));
   if (_hmiDataProxy && (proxy == _hmiDataProxy))
   {
      ETG_TRACE_USR4(("HmiDataHandler::onAvailable received handled"));
      //Register for the swipe direction property update
      _hmiDataProxy->sendSwipeDirectionStatusRegister(*this);
   }
   StartupSync::getInstance().onAvailable(proxy, stateChange);
}


/*
 * onUnavailable - To Handle Service Unavailability
 * @param[in] proxy
 * @param[in] stateChange
 * @param[out] None
 * @return void
 */
void HmiDataHandler::onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy, \
                                   const asf::core::ServiceStateChange& stateChange)
{
   if (_hmiDataProxy && (proxy == _hmiDataProxy))
   {
      //Unregister for all the required properties update
      _hmiDataProxy->sendSwipeDirectionStatusDeregisterAll();
   }
   StartupSync::getInstance().onUnavailable(proxy, stateChange);
}


/*
 * vRegisterforUpdate -This function is used to register all the clients for the property update
 * @param[in] Client pointer
 * @param[out] None
 * @return void
 */

void HmiDataHandler::vRegisterforUpdate(ISwipeDirectionStatusClientPropertyCB* client)
{
   std::vector< ISwipeDirectionStatusClientPropertyCB* >::const_iterator itr = std::find(_SwipeClientCallback.begin(), \
         _SwipeClientCallback.end(), client);
   if (itr == _SwipeClientCallback.end()) //for safety
   {
      //add instance
      _SwipeClientCallback.push_back(client);
   }
   else
   {
      //Info:User already registered the instance
   }
}


/*
 * vUnRegisterforUpdate -This function is used to Unregister for the language status update for all the registered clients
 * @param[in] iLanguageUpdate
 * @param[out] None
 * @return void
 */
void HmiDataHandler::vUnRegisterforUpdate(ISwipeDirectionStatusClientPropertyCB* client)
{
   std::vector<ISwipeDirectionStatusClientPropertyCB*>::iterator itInst = _SwipeClientCallback.begin();
   for (; itInst != _SwipeClientCallback.end(); ++itInst)
   {
      if (client == *itInst)
      {
         _SwipeClientCallback.erase(itInst);
         break;
      }
   }
}


void HmiDataHandler::onSwipeDirectionStatusError(const ::boost::shared_ptr< HmiDataProxy >& /*proxy*/, const ::boost::shared_ptr< SwipeDirectionStatusError >& /*error*/)
{
   ETG_TRACE_ERR(("HmiDataHandler:onSwipeDirectionStatusError is called"));
}


void HmiDataHandler::onSwipeDirectionStatusUpdate(const ::boost::shared_ptr< HmiDataProxy >& /*proxy*/, const ::boost::shared_ptr< SwipeDirectionStatusUpdate >& update)
{
   ETG_TRACE_USR4(("HmiDataHandler:onSwipeDirectionStatusUpdate is called"));
   uint8 swipeDirection = update->getSwipeDirectionStatus();
   (COURIER_MESSAGE_NEW(SetScrollDirectionMsg)(swipeDirection))->Post();
   std::vector<ISwipeDirectionStatusClientPropertyCB*>::iterator itInst = _SwipeClientCallback.begin();
   for (; itInst != _SwipeClientCallback.end(); ++itInst)
   {
      (*itInst)->handleSwipeDirectionPropertyUpdate(swipeDirection);
   }
   ETG_TRACE_USR4(("SwipeDirectionStatusUpdate : %d", swipeDirection));
}


} // namespace HmiDataHandler
