/**
 * @file <DefSetServiceBase.h>
 * @author RBEI/COB/ECV3
 * @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
 * @addtogroup common
 */


#ifndef DEFSETSERVICEBASE_HEADER
#define DEFSETSERVICEBASE_HEADER

#include "asf/core/BaseComponent.h"
#include "asf/core/ComponentFactory.h"
#include "org/bosch/cm/diagnosis/dbus/Diagnosis1/SystemSettings1Stub.h"

using namespace ::asf::core;
using namespace ::org::bosch::cm::diagnosis::dbus::Diagnosis1::SystemSettings1;


namespace App {
namespace Core {


class iDefSetServiceBase
{
   public:
      virtual ~iDefSetServiceBase() {};
      virtual void reqPrepareResponse() = 0;
      virtual void reqExecuteResponse() = 0;
      virtual void reqFinalizeResponse() = 0;
};


class DefSetServiceBase:
   public SystemSettings1Stub
{
   public:

      DefSetServiceBase(const std::string& portName);
      virtual ~DefSetServiceBase();

   public:
      void vRegisterforUpdate(iDefSetServiceBase* client);
      void vUnRegisterforUpdate(iDefSetServiceBase* client);

   public:
      void sendPrepareResponse(const int& response, iDefSetServiceBase* client);
      void sendExecuteResponse(const int& response, iDefSetServiceBase* client);
      void sendFinalizeResponse(const int& response, iDefSetServiceBase* client);

   public:
      virtual void onPrepareSystemSettingRequest(const ::boost::shared_ptr< PrepareSystemSettingRequest >& request);
      virtual void onExecuteSystemSettingRequest(const ::boost::shared_ptr< ExecuteSystemSettingRequest >& request);
      virtual void onFinalizeSystemSettingRequest(const ::boost::shared_ptr< FinalizeSystemSettingRequest >& request);

   public:
      static DefSetServiceBase* GetInstance();
      static void s_Intialize(const std::string& portName);
      static void s_Destrory();

   private:
      static DefSetServiceBase* _defSetServiceBase;

      std::vector<iDefSetServiceBase*> _defSetServiceBaseCallback;

      void sendPrepareRequest();
      void sendExecuteRequest();
      void sendFinalizeRequest();

      void setPrepReqRespFlagReqPosition(unsigned int reqPosition, bool value);
      void setExecReqRespFlagReqPosition(unsigned int reqPosition, bool value);
      void setFinalReqRespFlagReqPosition(unsigned int reqPosition, bool value);

      int prepareResponseReceived;
      int executeResponseReceived;
      int finalizeResponseReceived;

      PrepareSystemSettingRequest& rObjPrepareSystemSettingRequest;
      ExecuteSystemSettingRequest& rObjExecuteSystemSettingRequest;
      FinalizeSystemSettingRequest& rObjFinalizeSystemSettingRequest;

      std::vector<bool> flagPrepReqRespRepository;
      std::vector<bool> flagExecReqRespRepository;
      std::vector<bool> flagFinalReqRespRepository;
};


}
}


#endif //DEFSETSERVICEBASE_HEADER
