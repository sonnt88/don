#include "DefSetServiceBase.h"
#include "org/bosch/cm/diagnosis/dbus/Diagnosis1/SystemSettings1Stub.h" //lint !e451 !e537 repeatedly included header file without standard include guard

#include "hmi_trace_if.h"
#include "../Common_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_DEFSETSERVICEBASE
#include "trcGenProj/Header/DefSetServiceBase.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN


using namespace org::bosch::cm::diagnosis::dbus::Diagnosis1::SystemSettings1;


namespace App {
namespace Core {

DefSetServiceBase* DefSetServiceBase::_defSetServiceBase = NULL;

DefSetServiceBase::DefSetServiceBase(const std::string& portName = " ") //port should come from derived service class
   : SystemSettings1Stub(portName),
     prepareResponseReceived(0),
     executeResponseReceived(0),
     finalizeResponseReceived(0),
     rObjPrepareSystemSettingRequest(const_cast<PrepareSystemSettingRequest&>(PrepareSystemSettingRequest::getDefaultInstance())),
     rObjExecuteSystemSettingRequest(const_cast<ExecuteSystemSettingRequest&>(ExecuteSystemSettingRequest::getDefaultInstance())),
     rObjFinalizeSystemSettingRequest(const_cast<FinalizeSystemSettingRequest&>(FinalizeSystemSettingRequest::getDefaultInstance()))


{
   ETG_TRACE_USR4(("DefSetServiceBase::DefSetServiceBase Constructor"));

   flagPrepReqRespRepository.clear();
   flagExecReqRespRepository.clear();
   flagFinalReqRespRepository.clear();

   rObjPrepareSystemSettingRequest.clear();
   rObjExecuteSystemSettingRequest.clear();
   rObjFinalizeSystemSettingRequest.clear();

   _defSetServiceBase = this; //use the object created from service derived class
}


DefSetServiceBase::~DefSetServiceBase()
{
   prepareResponseReceived = 0;
   finalizeResponseReceived = 0;
   flagPrepReqRespRepository.clear();
   flagExecReqRespRepository.clear();
   flagFinalReqRespRepository.clear();

   rObjPrepareSystemSettingRequest.clear();
   rObjExecuteSystemSettingRequest.clear();
   rObjFinalizeSystemSettingRequest.clear();
}


/*
 * GetInstance - To get the defsetservicebase instance
 * @param[in] None
 * @param[out] None
 * @return DefSetServiceBase
 */
DefSetServiceBase* DefSetServiceBase::GetInstance()
{
   if (_defSetServiceBase == NULL)
   {
      ETG_TRACE_USR4(("DefSetServiceBase::_defSetServiceBase is NULL")); //should never reach here
      _defSetServiceBase = new DefSetServiceBase(); //port should come from respective hmi-service class --need to check if NULL can be returned here
   }
   return _defSetServiceBase;
}


/*
 * s_Intialize - To create defsetservicebase instance with service port
 * @param[in] None
 * @param[out] None
 * @return None
 */
void DefSetServiceBase::s_Intialize(const std::string& portName)
{
   _defSetServiceBase = new DefSetServiceBase(portName);
}


/*
 * s_Destrory - To destroy defsetservicebase instance
 * @param[in] None
 * @param[out] None
 * @return None
 */

void DefSetServiceBase::s_Destrory()
{
   if (_defSetServiceBase != NULL)
   {
      delete _defSetServiceBase;
      _defSetServiceBase = NULL;
   }
}


/*
 * onPrepareSystemSettingRequest - diag client prepare request for factory restore
 * @param[in] rPrepareSystemSettingRequest
 * @param[out] None
 * @return -
 */
void
DefSetServiceBase::onPrepareSystemSettingRequest(const ::boost::shared_ptr< PrepareSystemSettingRequest >& rPrepareSystemSettingRequest)
{
   ETG_TRACE_USR4(("DefSetServiceBase::onPrepareSystemSettingRequest"));

   rObjPrepareSystemSettingRequest = *rPrepareSystemSettingRequest;

   sendPrepareRequest();
}


/*
 * onExecuteSystemSettingRequest - diag client execute request for factory restore
 * @param[in] rExecuteSystemSettingRequest
 * @param[out] None
 * @return -
 */

void
DefSetServiceBase::onExecuteSystemSettingRequest(const ::boost::shared_ptr< ExecuteSystemSettingRequest >& rExecuteSystemSettingRequest)
{
   ETG_TRACE_USR4(("DefSetServiceBase::onExecuteSystemSettingRequest"));

   rObjExecuteSystemSettingRequest = *rExecuteSystemSettingRequest;

   sendExecuteRequest();
}


/*
 * onFinalizeSystemSettingRequest - diag client finalize request for factory restore
 * @param[in] rFinalizeSystemSettingRequest
 * @param[out] None
 * @return -
 */

void
DefSetServiceBase::onFinalizeSystemSettingRequest(const ::boost::shared_ptr< FinalizeSystemSettingRequest >& rFinalizeSystemSettingRequest)
{
   ETG_TRACE_USR4(("DefSetServiceBase::onFinalizeSystemSettingRequest"));

   rObjFinalizeSystemSettingRequest = *rFinalizeSystemSettingRequest;

   sendFinalizeRequest();
}


/*
 * vRegisterforUpdate -This function is used to register all the clients for the property update
 * @param[in] iDefSetServiceBase Client will be respective modules of application ex-STM,DTM in case of testmode
 * @param[out] None
 * @return void
 */

void DefSetServiceBase::vRegisterforUpdate(iDefSetServiceBase* client)
{
   ETG_TRACE_USR4(("DefSetServiceBase::vRegisterforUpdate"));
   std::vector< iDefSetServiceBase* >::const_iterator itr = std::find(_defSetServiceBaseCallback.begin(), \
         _defSetServiceBaseCallback.end(), client);
   if (itr == _defSetServiceBaseCallback.end()) //for safety
   {
      //add instance
      _defSetServiceBaseCallback.push_back(client);
   }
   else
   {
      //Info:User already registered the instance
   }
}


/*
 * vUnRegisterforUpdate -This function is used to Unregister all the registered clients
 * @param[in] iDefSetServiceBase
 * @param[out] None
 * @return void
 */
void DefSetServiceBase::vUnRegisterforUpdate(iDefSetServiceBase* client)
{
   ETG_TRACE_USR4(("DefSetServiceBase::vUnRegisterforUpdate"));
   std::vector<iDefSetServiceBase*>::iterator itr = _defSetServiceBaseCallback.begin();
   for (; itr != _defSetServiceBaseCallback.end(); ++itr)
   {
      if (client == *itr)
      {
         _defSetServiceBaseCallback.erase(itr);
         break;
      }
   }
}


/*
 * setPrepReqRespFlagReqPosition -set bool value in a particular position for vector flagPrepReqRespRepository
 * @param[in] reqPosition,value
 * @param[out] None
 * @return void
 */

void DefSetServiceBase::setPrepReqRespFlagReqPosition(unsigned int reqPosition, bool value)
{
   ETG_TRACE_USR4(("DefSetServiceBase::setPrepReqRespFlagReqPosition"));

   ETG_TRACE_USR4(("DefSetServiceBase::setPrepReqRespFlagReqPosition::reqPosition = %d", reqPosition));
   ETG_TRACE_USR4(("DefSetServiceBase::setPrepReqRespFlagReqPosition::flagPrepReqRespRepository.size = %d", flagPrepReqRespRepository.size()));

   if (reqPosition < flagPrepReqRespRepository.size())
   {
      flagPrepReqRespRepository[reqPosition] = value;
   }
   else
   {
      ETG_TRACE_USR4(("DefSetServiceBase::setPrepReqRespFlagReqPosition::reqPosition- out of bound"));
   }
}


/*
 * setExecReqRespFlagReqPosition -set bool value in a particular position for vector flagExecReqRespRepository
 * @param[in] reqPosition,value
 * @param[out] None
 * @return void
 */
void DefSetServiceBase::setExecReqRespFlagReqPosition(unsigned int reqPosition, bool value)
{
   ETG_TRACE_USR4(("DefSetServiceBase::setExecReqRespFlagReqPosition"));

   ETG_TRACE_USR4(("DefSetServiceBase::setExecReqRespFlagReqPosition::reqPosition = %d", reqPosition));
   ETG_TRACE_USR4(("DefSetServiceBase::setExecReqRespFlagReqPosition::flagExecReqRespRepository.size = %d", flagExecReqRespRepository.size()));

   if (reqPosition < flagExecReqRespRepository.size())
   {
      flagExecReqRespRepository[reqPosition] = value;
   }
   else
   {
      ETG_TRACE_USR4(("DefSetServiceBase::setExecReqRespFlagReqPosition::reqPosition- out of bound"));
   }
}


/*
 * setFinalReqRespFlagReqPosition -set bool value in a particular position for vector flagFinalReqRespRepository
 * @param[in] reqPosition,value
 * @param[out] None
 * @return void
 */
void DefSetServiceBase::setFinalReqRespFlagReqPosition(unsigned int reqPosition, bool value)
{
   ETG_TRACE_USR4(("DefSetServiceBase::setFinalReqRespFlagReqPosition"));

   ETG_TRACE_USR4(("DefSetServiceBase::setFinalReqRespFlagReqPosition::reqPosition = %d", reqPosition));
   ETG_TRACE_USR4(("DefSetServiceBase::setFinalReqRespFlagReqPosition::flagFinalReqRespRepository.size = %d", flagFinalReqRespRepository.size()));

   if (reqPosition < flagFinalReqRespRepository.size())
   {
      flagFinalReqRespRepository[reqPosition] = value;
   }
   else
   {
      ETG_TRACE_USR4(("DefSetServiceBase::setFinalReqRespFlagReqPosition::reqPosition- out of bound"));
   }
}


/*
 * sendPrepareRequest - used to send prepare to all registered clients
 * @param[in] none
 * @param[out] None
 * @return void
 */
void DefSetServiceBase::sendPrepareRequest()
{
   ETG_TRACE_USR4(("DefSetServiceBase::sendPrepareRequest"));

   for (std::vector<iDefSetServiceBase*>::iterator itr = _defSetServiceBaseCallback.begin(); itr != _defSetServiceBaseCallback.end(); ++itr)
   {
      setPrepReqRespFlagReqPosition(itr - _defSetServiceBaseCallback.begin(), true);
      (*itr)->reqPrepareResponse();
   }
}


/*
 * sendPrepareResponse - used to receive prepare to all registered clients, and send PrepareResponse to diag-client
 * @param[in] none
 * @param[out] None
 * @return void
 */
void DefSetServiceBase::sendPrepareResponse(const int& response, iDefSetServiceBase* client)
{
   ETG_TRACE_USR4(("DefSetServiceBase::sendPrepareResponse"));

   std::vector< PrepareSystemSettingResponseExtendedDataStruct > extendedData;
   if (response == 1)
   {
      prepareResponseReceived = 1;
   }

   std::vector<iDefSetServiceBase*>::iterator itr = find(_defSetServiceBaseCallback.begin(), _defSetServiceBaseCallback.end(), client);
   if (itr != _defSetServiceBaseCallback.end())
   {
      setPrepReqRespFlagReqPosition(itr - _defSetServiceBaseCallback.begin(), false);
   }
   else
   {
      ETG_TRACE_USR4(("DefSetServiceBase::setPrepReqRespFlagReqPosition--no client"));
   }

   std::vector<bool>::iterator it = find(flagPrepReqRespRepository.begin(), flagPrepReqRespRepository.end(), true);

   if (it == flagPrepReqRespRepository.end())
   {
      ETG_TRACE_USR4(("DefSetServiceBase::sendPrepareResponse--all clients processed"));

      //prepareResponseReceived is now hardcoded to 0, till all the hmi servers are validated for its correct usage

      sendPrepareSystemSettingResponse(rObjPrepareSystemSettingRequest.getSysSetID(), rObjPrepareSystemSettingRequest.getSysSetType(), extendedData , 0/*prepareResponseReceived*/, rObjPrepareSystemSettingRequest.getCookie());
      rObjPrepareSystemSettingRequest.clear();
      flagPrepReqRespRepository.clear();
      prepareResponseReceived = 0;
   }
}


/*
 * sendExecuteRequest - used to send execute to all registered clients
 * @param[in] none
 * @param[out] None
 * @return void
 */
void DefSetServiceBase::sendExecuteRequest()
{
   ETG_TRACE_USR4(("DefSetServiceBase::sendExecuteRequest"));

   for (std::vector<iDefSetServiceBase*>::iterator itr = _defSetServiceBaseCallback.begin(); itr != _defSetServiceBaseCallback.end(); ++itr)
   {
      setExecReqRespFlagReqPosition(itr - _defSetServiceBaseCallback.begin(), true);
      (*itr)->reqExecuteResponse();
   }
}


/*
 * sendExecuteResponse - used to receive prepare to all registered clients, and send ExecuteResponse to diag-client
 * @param[in] none
 * @param[out] None
 * @return void
 */
void DefSetServiceBase::sendExecuteResponse(const int& response, iDefSetServiceBase* client)
{
   ETG_TRACE_USR4(("DefSetServiceBase::sendExecuteResponse"));

   std::vector< ExecuteSystemSettingResponseExtendedDataStruct > extendedData;

   if (response == 1)
   {
      executeResponseReceived = 1;
   }

   std::vector<iDefSetServiceBase*>::iterator itr = find(_defSetServiceBaseCallback.begin(), _defSetServiceBaseCallback.end(), client);
   if (itr != _defSetServiceBaseCallback.end())
   {
      setExecReqRespFlagReqPosition(itr - _defSetServiceBaseCallback.begin(), false);
   }
   else
   {
      ETG_TRACE_USR4(("DefSetServiceBase::setExecReqRespFlagReqPosition--no client"));
   }

   std::vector<bool>::iterator it = find(flagExecReqRespRepository.begin(), flagExecReqRespRepository.end(), true);

   if (it == flagExecReqRespRepository.end())
   {
      ETG_TRACE_USR4(("DefSetServiceBase::sendExecuteResponse--all clients processed"));

      //executeResponseReceived is now hardcoded to 0, till all the hmi servers are validated for its correct usage
      sendExecuteSystemSettingResponse(rObjExecuteSystemSettingRequest.getSysSetID(), rObjExecuteSystemSettingRequest.getSysSetType(), extendedData , 0 /*executeResponseReceived*/, rObjExecuteSystemSettingRequest.getCookie());
      rObjExecuteSystemSettingRequest.clear();
      flagExecReqRespRepository.clear();
      executeResponseReceived = 0;
   }
}


/*
 * sendFinalizeRequest - used to send prepare to all registered clients
 * @param[in] none
 * @param[out] None
 * @return void
 */
void DefSetServiceBase::sendFinalizeRequest()

{
   ETG_TRACE_USR4(("DefSetServiceBase::sendFinalizeRequest"));

   for (std::vector<iDefSetServiceBase*>::iterator itr = _defSetServiceBaseCallback.begin(); itr != _defSetServiceBaseCallback.end(); ++itr)
   {
      (*itr)->reqFinalizeResponse();
   }
}


/*
 * sendFinalizeResponse - used to receive prepare to all registered clients, and send FinalizeResponse to diag-client
 * @param[in] none
 * @param[out] None
 * @return void
 */
void DefSetServiceBase::sendFinalizeResponse(const int& response, iDefSetServiceBase* client)
{
   ETG_TRACE_USR4(("DefSetServiceBase::sendFinalizeResponse"));

   std::vector< FinalizeSystemSettingResponseExtendedDataStruct > extendedData;

   if (response == 1)
   {
      finalizeResponseReceived = 1;
   }

   std::vector<iDefSetServiceBase*>::iterator itr = find(_defSetServiceBaseCallback.begin(), _defSetServiceBaseCallback.end(), client);
   if (itr != _defSetServiceBaseCallback.end())
   {
      setFinalReqRespFlagReqPosition(itr - _defSetServiceBaseCallback.begin(), false);
   }
   else
   {
      ETG_TRACE_USR4(("DefSetServiceBase::setFinalReqRespFlagReqPosition--no client"));
   }

   std::vector<bool>::iterator it = find(flagFinalReqRespRepository.begin(), flagFinalReqRespRepository.end(), true);

   if (it == flagFinalReqRespRepository.end())
   {
      ETG_TRACE_USR4(("DefSetServiceBase::sendFinalizeResponse--all clients processed"));
      //finalizeResponseReceived is now hardcoded to 0, till all the hmi servers are validated for its correct usage
      sendFinalizeSystemSettingResponse(rObjFinalizeSystemSettingRequest.getSysSetID(), rObjFinalizeSystemSettingRequest.getSysSetType(), extendedData , 0/*finalizeResponseReceived*/, rObjFinalizeSystemSettingRequest.getCookie());
      rObjFinalizeSystemSettingRequest.clear();
      flagFinalReqRespRepository.clear();
      finalizeResponseReceived = 0;
   }
}


}
}
