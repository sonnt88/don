/**
 * @file PopupTimer.cpp
 * @Author ECV2-Binu John
 * @Copyright (c) 2015 Robert Bosch Car Multimedia Gmbh
 * @addtogroup common
 */
#include "hall_std_if.h"
#include "PopupTimer.h"

using namespace Util;

namespace PopupTimerHandler {
PopupTimer::PopupTimer()
{
}


PopupTimer::~PopupTimer()
{
   _popupTimerMap.clear();
}


void PopupTimer::deletepopupTimer(::Util::Timer* popupTimer)
{
   if (popupTimer != NULL)
   {
      popupTimer->stop();
      delete popupTimer;
   }
}


bool PopupTimer::onCourierMessage(const StartPopupTimerReqMsg& oMsg)
{
   bool bRet = false;
   ::Util::Timer* popupTimer;
   uint32_t viewId = oMsg.GetViewId();
   uint32_t timerValue = oMsg.GetTimeout();
   ::std::map<uint32_t, ::Util::Timer*>::const_iterator itr;

   //if timer exist then stop before start
   itr = _popupTimerMap.find(viewId);
   if (itr != _popupTimerMap.end())
   {
      deletepopupTimer(itr->second);
      _popupTimerMap.erase(viewId);
   }

   //start the timer
   popupTimer = new Timer(timerValue);
   if (popupTimer != NULL)
   {
      popupTimer->start();
      _popupTimerMap[viewId] = popupTimer; //todo:
      bRet = true;
   }
   return bRet;
}


bool PopupTimer::onCourierMessage(const StopPopupTimerReqMsg& oMsg)
{
   bool bRet = false;
   ::Util::Timer* popupTimer;
   uint32_t viewId = oMsg.GetViewId();

   ::std::map<uint32_t, ::Util::Timer*>::const_iterator itr = _popupTimerMap.find(viewId);
   if (itr != _popupTimerMap.end())
   {
      popupTimer = itr->second;
      //remove from map
      _popupTimerMap.erase(itr->first);
      deletepopupTimer(popupTimer);
   }
   return bRet;
}


bool PopupTimer::onCourierMessage(const RestartPopupTimerReqMsg& oMsg)
{
   bool bRet = false;
   ::std::map<uint32_t, ::Util::Timer*>::const_iterator itr = _popupTimerMap.find(oMsg.GetViewId());
   if ((itr != _popupTimerMap.end()) && (NULL != itr->second))
   {
      itr->second->restart();
      bRet = true;
   }
   return bRet;
}


uint32_t PopupTimer::getViewName(::Util::Timer* popupTimerval)
{
   // Iterate through all elements and search for view name
   std::map<uint32_t, ::Util::Timer*>::iterator it = _popupTimerMap.begin();
   while (it != _popupTimerMap.end())
   {
      if (it->second == popupTimerval)
      {
         return it->first;
      }
      it++;
   }
   return 0;
}


bool PopupTimer::onCourierMessage(const TimerExpiredMsg& oMsg)
{
   bool bRet = false;
   ::Util::Timer* popupTimer = oMsg.GetTimer();
   if (popupTimer != NULL)
   {
      uint32_t viewId = getViewName(popupTimer);
      //Check the received timer is created by Popup timer, if it is true then consume the TimerExpiredMsg
      // otherwiese send the TimerExpiredMsg to others -sve2cob
      if (viewId != 0)
      {
         POST_MSG((COURIER_MESSAGE_NEW(ExpiredPopupTimerResMsg)(viewId)));
         deletepopupTimer(popupTimer);
         _popupTimerMap.erase(viewId);
         bRet = true;
      }
   }
   return bRet;
}


}
