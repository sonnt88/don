/**
 * @file PopupTimer.h
 * @Author ECV2-Binu John
 * @Copyright (c) 2015 Robert Bosch Car Multimedia Gmbh
 * @addtogroup common
 */
#ifndef POPUPTIMER_H
#define POPUPTIMER_H

#include "ProjectBaseMsgs.h"
#include <map>

namespace PopupTimerHandler {
class PopupTimer
{
   public:
      PopupTimer();
      virtual ~PopupTimer();

      COURIER_MSG_MAP_BEGIN(0)
      ON_COURIER_MESSAGE(StartPopupTimerReqMsg)
      ON_COURIER_MESSAGE(StopPopupTimerReqMsg)
      ON_COURIER_MESSAGE(RestartPopupTimerReqMsg)
      ON_COURIER_MESSAGE(TimerExpiredMsg)
      COURIER_MSG_MAP_END()

   protected:
      bool onCourierMessage(const StartPopupTimerReqMsg& oMsg);
      bool onCourierMessage(const StopPopupTimerReqMsg& oMsg);
      bool onCourierMessage(const RestartPopupTimerReqMsg& oMsg);
      //message from timer utility
      bool onCourierMessage(const TimerExpiredMsg& oMsg);
      void deletepopupTimer(::Util::Timer* popupTimer);
      uint32_t getViewName(::Util::Timer* popupTimerval);

   private:
      ::std::map<uint32_t, ::Util::Timer*> _popupTimerMap;
};


}

#endif
