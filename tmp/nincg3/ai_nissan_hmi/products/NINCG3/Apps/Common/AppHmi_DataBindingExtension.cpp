/**
 * @file <AppHmi_DataBindingExtension.cpp>
 * @author <Binu John> <A-IVI>
 *
 * Note : This file is a temporary fix till SESA delivers the official change
 */
#if !defined(AppHmi_DatabindingExtension_h)
#define AppHmi_DatabindingExtension_h

#include <FeatStd/Util/StringBuffer.h>
#include <Candera/EngineBase/Common/Color.h>

namespace FeatStd {

#ifdef _MSC_VER
template<> FeatStd::Internal::Win32::Win32Types::UInt32 StringBufferAppender<Candera::Color>::Append(StringBuffer& stringBuffer, Candera::Color const& object)
#else
template<> FeatStd::Internal::Posix::PosixTypes::UInt32 StringBufferAppender<Candera::Color>::Append(StringBuffer& stringBuffer, Candera::Color const& object)
#endif
{
   stringBuffer.AppendObject<Float>(object.GetData().red);
   stringBuffer.AppendObject<Float>(object.GetData().green);
   stringBuffer.AppendObject<Float>(object.GetData().blue);
   return stringBuffer.AppendObject<Float>(object.GetData().alpha);
}


}
#endif
