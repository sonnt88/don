/*
 * Common_Trace.h
 *
 *  Created on: Sep 2, 2015
 *      Author: gug1cob
 */

#ifndef COMMON_TRACE_H_
#define COMMON_TRACE_H_

// --- TRACE classes
#define TR_CLASS_APPHMI_COMMON                   (TR_COMP_UI_APP_HMI_MASTER + 16)    //0X1510 - Start after end of TR_COMP_UI_APP_HMI_MASTER i.e. 0x150F
#define TR_CLASS_APPHMI_COMMON_FOCUS             (TR_CLASS_APPHMI_COMMON + 1)        //0X1511 - Trace for App HMI common focus adaptation
#define TR_CLASS_APPHMI_COMMON_VEHICLEDATA       (TR_CLASS_APPHMI_COMMON + 2)        //0X1512 - Trace for App HMI common Vehicledata
#define TR_CLASS_APPHMI_COMMON_STATUSBAR         (TR_CLASS_APPHMI_COMMON + 3)        //0X1513 - Trace for App HMI common Statusbar
#define TR_CLASS_APPHMI_COMMON_CONTEXTSWITCH     (TR_CLASS_APPHMI_COMMON + 4)        //0X1514 - Trace for App HMI common Context Switch
#define TR_CLASS_APPHMI_COMMON_DEFSETSERVICEBASE (TR_CLASS_APPHMI_COMMON + 5)        //0X1515 - Trace for App HMI common Restore Factory
#define TR_CLASS_APPHMI_COMMON_LISTREGISTRY      (TR_CLASS_APPHMI_COMMON + 6)        //0X1516 - Trace for App HMI common list registry
#define TR_CLASS_APPHMI_COMMON_HMIDATA     		 (TR_CLASS_APPHMI_COMMON + 7)        //0X1517 - Trace for App HMI common Data Handler
#define TR_CLASS_APPHMI_COMMON_USERINTERFACECTRL (TR_CLASS_APPHMI_COMMON + 8)        //0X1518 - Trace for App HMI common User Interface Control
#define TR_CLASS_APPHMI_COMMON_APPMANAGER        (TR_CLASS_APPHMI_COMMON + 9)        //0X1519 - Trace for App HMI common App Manager Lib
#define TR_CLASS_APPHMI_COMMON_PHONESETTINGCTRL  (TR_CLASS_APPHMI_COMMON + 10)       //0X151A - Trace for App HMI common phone setting Control
#define TR_CLASS_APPHMI_COMMON_HMIINFO	         (TR_CLASS_APPHMI_COMMON + 11) 		 //0X151B - Trace for App HMI common HMI InfoService
//extend needed clases from TR_CLASS_HMI_APP_COMMON

#endif /* COMMON_TRACE_H_ */
