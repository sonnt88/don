/**
 * @file HeaderTypes.h
 * @Author ECV2-Binu John
 * @Copyright (c) 2016 Robert Bosch Car Multimedia Gmbh
 * @addtogroup common
 */

#ifndef HEADERTYPES_H_
#define HEADERTYPES_H_

#include "asf/core/Types.h"
#include "ProjectBaseTypes.h" //HeaderTypeEnum


struct HeaderIconPriority
{
   HeaderTypeEnum headerType;
   bool bETC;
   bool bTCU;
   bool bPhoneSignal;
   bool bPhonebookDnld;
   bool bSMS;
   bool bSWUpdate;
   bool bPhoneBattery;
   bool bBT;
   bool bDTV;
   bool bTA;
};


enum enHeaderStatus
{
#define HEADER_ICON_PATH(status, iconpath) status,
#include "HeaderConfig.dat"
#undef HEADER_ICON_PATH
   HEADER_STATUS_TYPE_LIMIT
};


#endif
