/*
 * StatusBarHandler.cpp
 *
 * Author: CM-PJCB
 */

#include "hall_std_if.h"
#include "StatusBarHandler.h"
#include "hmi_trace_if.h"
#include "../Common_Trace.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS     TR_CLASS_APPHMI_COMMON_STATUSBAR
#include "trcGenProj/Header/StatusBarHandler.cpp.trc.h"
#endif

#define UNDEFINED 0xFF
#define STATUS_BAR_MAX_ENTRIES 4
// feature name to show the corresponding icons in header
#define  ETC     "ETC"
#define  TCU     "TCU"
#define  PH_SIG  "PHONE_SIGNAL"
#define  PH_DNLD  "PHONE_DOWNLOAD"
#define  SMS      "SMS"
#define  SW_UPD   "SW_UPDATE"
#define  PH_BAT   "PHONE_BATTERY"
#define  BT       "BT"
#define  DTV      "DTV"
#define  TA       "TA"

//using namespace std;
using namespace ::datacollector_main_fi;
using namespace ::datacollector_main_fi_types;

namespace StatusBarCommonHandler {

static const HeaderIconPriority aIconPriority[] =
{
#define HEADER_ICON_APPLICABLE(headertype, ETC, TCU, PH_SIG, PH_DNLD, SMS, SW_UPD, PH_BAT, BT, DTV, TA)    {headertype, ETC, TCU, PH_SIG, PH_DNLD, SMS, SW_UPD, PH_BAT, BT, DTV, TA},
#include "HeaderConfig.dat"
#undef HEADER_ICON_APPLICABLE
};


static const char* g_IconPath[HEADER_STATUS_TYPE_LIMIT] =
{
#define HEADER_ICON_PATH(status,iconpath) iconpath,
#include "HeaderConfig.dat"
#undef HEADER_ICON_PATH
};


StatusBarHandler* StatusBarHandler::_statusBarHandler = NULL;

StatusBarHandler::StatusBarHandler() :
   _dataCollectorProxy(::Datacollector_main_fiProxy::createProxy("datacollector_main_fiPort", *this)),
   m_CurrentHours(UNDEFINED),
   m_CurrentMinutes(UNDEFINED),
   m_CurrentTimeFormatType(UNDEFINED),
   m_CurrentTAInfo(UNDEFINED),
   m_CurrentPhoneSignalStrength(UNDEFINED),
   m_CurrentBTConnectionState(UNDEFINED),
   m_CurrentBatteryResidualQuantity(UNDEFINED),
   m_CurrentUnreadedSMSInfo(UNDEFINED),
   m_CurrentTCUStatus(false),
   m_CurrentSwUpdateStatus(UNDEFINED),
   m_CurrentHVACTempInfo(UNDEFINED),
   m_CurrentHVACTempUnitInfo(UNDEFINED),
   m_CurrentAudioInfo(UNDEFINED),
   m_MeterConnectionStatus(0),
   m_bNewSMSNotification(false)
{
   ETG_I_REGISTER_FILE();
   StartupSync::getInstance().registerPropertyRegistrationIF(this);
   _StatusBarCallback.clear();
   vPopulateHeaderPriorityMap();
   ETG_TRACE_USR4(("StatusBarHandler::Constructor"));
}


StatusBarHandler::~StatusBarHandler()
{
   _StatusBarCallback.clear();
}


/*
 * onAvailable - To Handle Service Availability
 * @param[in] proxy
 * @param[in] stateChange
 * @param[out] None
 * @return void
 */
void StatusBarHandler::registerProperties(const boost::shared_ptr<asf::core::Proxy>& proxy, \
      const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _dataCollectorProxy)
   {
      _dataCollectorProxy->sendProperty_HMIStatusBarInfoUpReg(*this);
      _dataCollectorProxy->sendProperty_ConnectionUpReg(*this);
   }
}


/*
 * onUnavailable - To Handle Service Unavailability
 * @param[in] proxy
 * @param[in] stateChange
 * @param[out] None
 * @return void
 */
void StatusBarHandler::deregisterProperties(const boost::shared_ptr<asf::core::Proxy>& proxy, \
      const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _dataCollectorProxy)
   {
      _dataCollectorProxy->sendProperty_HMIStatusBarInfoRelUpRegAll();
      _dataCollectorProxy->sendProperty_ConnectionRelUpRegAll();
   }
}


/*
 * GetInstance - To get the StatusBarHandler instance
 * @param[in] None
 * @param[out] None
 * @return StatusBarHandler
 */
StatusBarHandler* StatusBarHandler::GetInstance()
{
   if (_statusBarHandler == NULL)
   {
      _statusBarHandler = new StatusBarHandler();
   }
   return _statusBarHandler;
}


/*
 * onCourierMessage - To Handle GUI startup message
 * when the GUI is ready, request all clients to read the current Statusbar data
 * @param[in] GuiStartupFinishedUpdMsg
 * @param[out] None
 * @return bool
 */
bool StatusBarHandler::onCourierMessage(const GuiStartupFinishedUpdMsg& /*msg*/)
{
   ETG_TRACE_USR4(("StatusBarHandler::GuiStartupFinishedUpdMsg"));
   sendStatusLineDefaultDataUpdateToAllRegisteredClients();
   sendSignalStrengthUpdateToAllRegisteredClients(m_CurrentPhoneSignalStrength);
   sendBatteryInfoUpdateToAllRegisteredClients(m_CurrentBatteryResidualQuantity);
   sendBTConnectionUpdateToAllRegisteredClients(m_CurrentBTConnectionState);
   sendTAInfoUpdateToAllRegisteredClients(m_CurrentTAInfo);
   sendUnreadedSMSInfoUpdateToAllRegisteredClients(m_CurrentUnreadedSMSInfo);
   sendTCUStatusUpdateToAllRegisteredClients(m_CurrentTCUStatus);
   sendHVACTemperatureInfoUpdateToAllRegisteredClients(m_CurrentHVACTempInfo, m_CurrentHVACTempUnitInfo);
   sendSwUpdateStatusUpdateToAllRegisteredClients(m_CurrentSwUpdateStatus);

   if (m_CurrentHours == UNDEFINED || m_CurrentMinutes == UNDEFINED)
   {
      sendTimeInfoUpdateToAllRegisteredClients(0, 0, 0);
   }
   else
   {
      sendTimeInfoUpdateToAllRegisteredClients(m_CurrentHours, m_CurrentMinutes, m_CurrentTimeFormatType);
   }

   sendMeterConnectionUpdateToAllRegisteredClients(m_MeterConnectionStatus);
   StartupSync::getInstance().onGuiStartupFinished();

   return false;
}


/*
 * vRegisterforUpdate -This function is used to register all the clients for the property update
 * @param[in] IStatusBarHandler
 * @param[out] None
 * @return void
 */
void StatusBarHandler::vRegisterforUpdate(IStatusBarHandler* client)
{
   std::vector< IStatusBarHandler* >::const_iterator itr = std::find(_StatusBarCallback.begin(), \
         _StatusBarCallback.end(), client);
   if (itr == _StatusBarCallback.end()) //for safety
   {
      //add instance
      _StatusBarCallback.push_back(client);
   }
   else
   {
      //Info:User already registered the instance
   }
}


/*
 * vUnRegisterforUpdate -This function is used to Unregister for the status line update for all the registered clients
 * @param[in] IStatusBarHandler
 * @param[out] None
 * @return void
 */
void StatusBarHandler::vUnRegisterforUpdate(IStatusBarHandler* client)
{
   std::vector<IStatusBarHandler*>::iterator itr = _StatusBarCallback.begin();
   for (; itr != _StatusBarCallback.end(); ++itr)
   {
      if (client == *itr)
      {
         _StatusBarCallback.erase(itr);
         break;
      }
   }
}


/**
 *  Virtual function implemented to get update of onProperty_HMIStatusBarInfoStatus and update the data to each registered client.
 *
 * @param[in]      : proxy : Client side representation of the CCA Functional Interface Datacollector_main_fiProxy
 * @param[in]      : status : Status update for the Property_HMIStatusBarInfoStatus
 * @return         : void
 */
void StatusBarHandler::onProperty_HMIStatusBarInfoStatus(const ::boost::shared_ptr< ::Datacollector_main_fiProxy >& proxy, const ::boost::shared_ptr< ::Property_HMIStatusBarInfoStatus >& status)
{
   if (proxy == _dataCollectorProxy)
   {
      /* Clock */
      if (m_CurrentHours != status->getClockTimeInfo().getCurrentHours()
            || m_CurrentMinutes != status->getClockTimeInfo().getCurrentMinutes()
            || m_CurrentTimeFormatType != status->getClockTimeInfo().getTimeFormatType())
      {
         m_CurrentHours = status->getClockTimeInfo().getCurrentHours();
         m_CurrentMinutes = status->getClockTimeInfo().getCurrentMinutes();
         m_CurrentTimeFormatType = status->getClockTimeInfo().getTimeFormatType();

         sendTimeInfoUpdateToAllRegisteredClients(m_CurrentHours, m_CurrentMinutes, m_CurrentTimeFormatType);
      }

      /* TA */
      if (m_CurrentTAInfo != status->getTAInfo())
      {
         m_CurrentTAInfo = status->getTAInfo();
         sendTAInfoUpdateToAllRegisteredClients(m_CurrentTAInfo);
      }

      /* Bluetooth */
      if (m_CurrentBTConnectionState != status->getPhoneInfo().getBTConnectionState())
      {
         m_CurrentBTConnectionState = status->getPhoneInfo().getBTConnectionState();
         sendBTConnectionUpdateToAllRegisteredClients(m_CurrentBTConnectionState);
      }

      if (m_CurrentBatteryResidualQuantity != status->getPhoneInfo().getBatteryResidualQuantity())
      {
         m_CurrentBatteryResidualQuantity = status->getPhoneInfo().getBatteryResidualQuantity();
         sendBatteryInfoUpdateToAllRegisteredClients(m_CurrentBatteryResidualQuantity);
      }

      if (m_CurrentPhoneSignalStrength != status->getPhoneInfo().getPhoneSignalStrength())
      {
         m_CurrentPhoneSignalStrength = status->getPhoneInfo().getPhoneSignalStrength();
         sendSignalStrengthUpdateToAllRegisteredClients(m_CurrentPhoneSignalStrength);
      }

      /* SMS */
      if (m_bNewSMSNotification != status->getSMSInfo().getNewSMSNotification())
      {
         m_bNewSMSNotification = status->getSMSInfo().getNewSMSNotification();
      }
      if (m_CurrentUnreadedSMSInfo != status->getSMSInfo().getNumberOfUnreadSMS())
      {
         m_CurrentUnreadedSMSInfo = status->getSMSInfo().getNumberOfUnreadSMS();
         sendUnreadedSMSInfoUpdateToAllRegisteredClients(m_CurrentUnreadedSMSInfo);
      }

      /* TCU */
      if (m_CurrentTCUStatus != status->getTCUStatus())
      {
         m_CurrentTCUStatus = status->getTCUStatus();
         sendTCUStatusUpdateToAllRegisteredClients(m_CurrentTCUStatus);
      }

      /* Software Update */
      /*if (m_CurrentSwUpdateStatus != status->getDownloadStatus())
      {
         m_CurrentSwUpdateStatus = status->getDownloadStatus();
         sendSwUpdateStatusUpdateToAllRegisteredClients(m_CurrentSwUpdateStatus);
      }*/

      /* HVAC */
      if (m_CurrentHVACTempInfo != status->getHVACInfo().getHVACTemperature() || m_CurrentHVACTempUnitInfo != status->getHVACInfo().getTemperatureUnit())
      {
         m_CurrentHVACTempInfo = status->getHVACInfo().getHVACTemperature();
         m_CurrentHVACTempUnitInfo = status->getHVACInfo().getHVACTemperature();
         sendHVACTemperatureInfoUpdateToAllRegisteredClients(m_CurrentHVACTempInfo, m_CurrentHVACTempUnitInfo);
      }
   }
}


/**
 *  Virtual function implemented to get update of onProperty_HMIStatusBarInfoError
 *
 * @param[in]      : proxy : Client side representation of the CCA Functional Interface Datacollector_main_fiProxy
 * @param[in]      : status : Status update for the onProperty_HMIStatusBarInfoError
 * @return         : void
 */
void StatusBarHandler::onProperty_HMIStatusBarInfoError(const ::boost::shared_ptr< ::Datacollector_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::Property_HMIStatusBarInfoError >& /*error*/)
{
}


/**
 *  Virtual function implemented to get update of onProperty_ConnectionStatus and update the data to each registered client.
 *
 * @param[in]      : proxy : Client side representation of the CCA Functional Interface Datacollector_main_fiProxy
 * @param[in]      : status : Status update for the Property_ConnectionStatus
 * @return         : void
 */
void StatusBarHandler::onProperty_ConnectionStatus(const ::boost::shared_ptr< ::datacollector_main_fi::Datacollector_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::datacollector_main_fi::Property_ConnectionStatus >& status)
{
   bool bMeterConnectionStatus = false;
   if (true == status->hasArConnectionStatusList())
   {
      ::std::vector< T_datacollector_UnitConnectionStatus> dcUnitConnectionStatus(status->getArConnectionStatusList());

      //search for meter unit status in the list
      for (unsigned int i = 0; i < dcUnitConnectionStatus.size(); i++)
      {
         if (::T_e8_datacollector_UnitType__Audio == dcUnitConnectionStatus[i].getE8UnitType())
         {
            //set the Meter connection status
            if (true == dcUnitConnectionStatus[i].getBConnected())
            {
               bMeterConnectionStatus = true;
            }
            break;
         }
      }
      if (bMeterConnectionStatus != m_MeterConnectionStatus)
      {
         m_MeterConnectionStatus = bMeterConnectionStatus;
         sendMeterConnectionUpdateToAllRegisteredClients(m_MeterConnectionStatus);
      }
   }
}


/**
 *  Virtual function implemented to get update of onProperty_ConnectionError
 *
 * @param[in]      : proxy : Client side representation of the CCA Functional Interface Datacollector_main_fiProxy
 * @param[in]      : status : Status update for the onProperty_ConnectionError
 * @return         : void
 */
void StatusBarHandler::onProperty_ConnectionError(const ::boost::shared_ptr< ::datacollector_main_fi::Datacollector_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::datacollector_main_fi::Property_ConnectionError >& /*error*/)
{
}


/**
 *  Virtual function implemented to get update of onSetInterrupt_to_MeterResult
 *
 * @param[in]      : proxy : Client side representation of the CCA Functional Interface Datacollector_main_fiProxy
 * @param[in]      : status : Status update for the onProperty_ConnectionError
 * @return         : void
 */
void StatusBarHandler::onSetInterrupt_to_MeterResult(const ::boost::shared_ptr< Datacollector_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< SetInterrupt_to_MeterResult >& /*result*/)
{
}


/**
 *  Virtual function implemented to get update of onSetInterrupt_to_MeterError
 *
 * @param[in]      : proxy : Client side representation of the CCA Functional Interface Datacollector_main_fiProxy
 * @param[in]      : status : Status update for the onProperty_ConnectionError
 * @return         : void
 */
void StatusBarHandler::onSetInterrupt_to_MeterError(const ::boost::shared_ptr< Datacollector_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< SetInterrupt_to_MeterError >& /*error*/)
{
}


/*
 * setInterruptToMeter - Set FC_Meter interrupt to inform about SWC Keys
 * @param[in] int8 audioSrc - current audio source who triggered the interrupt
 * (see enum T_e8_datacollector_Audio_Source in datacollector_main_fi_typeConst.h)
 *
 * @param[in] int8 interruptType - which interruptType should be triggered
 * (see enum T_e8_datacollector_Interrupt_Type in datacollector_main_fi_typeConst.h)
 * @param[in] int8 interruptMode - which interruptMode should be triggered
 * (either true or false)
 * @return void
 */
void StatusBarHandler::setInterruptToMeter(int8 audioSrc, int8 interruptType, bool interruptMode)
{
   if (_dataCollectorProxy)
   {
      _dataCollectorProxy->sendSetInterrupt_to_MeterStart(*this, interruptMode, (T_e8_datacollector_Audio_Source)audioSrc, (T_e8_datacollector_Interrupt_Type)interruptType);
   }
}


/*
 * sendSignalStrengthUpdateToAllRegisteredClients -
 *                               This function is used to send the signal update status update to all the registered clients.
 * @param[in] SignalStrength
 * @param[out] None
 * @return void
 */
void StatusBarHandler::sendSignalStrengthUpdateToAllRegisteredClients(uint8 SignalStrength)
{
   ETG_TRACE_USR2(("StatusBarHandler::sendSignalStrengthUpdateToAllRegisteredClients: %d", SignalStrength));
   std::vector<IStatusBarHandler*>::iterator itr = _StatusBarCallback.begin();
   for (; itr != _StatusBarCallback.end(); ++itr)
   {
      (*itr)->updatePhoneSignalStrength(SignalStrength);
   }
}


/*
 * sendBatteryInfoUpdateToAllRegisteredClients -
 *                               This function is used to send the battery update status update to all the registered clients.
 * @param[in] BatteryInfo
 * @param[out] None
 * @return void
 */
void StatusBarHandler::sendBatteryInfoUpdateToAllRegisteredClients(uint8 BatteryInfo)
{
   ETG_TRACE_USR2(("StatusBarHandler::sendBatteryInfoUpdateToAllRegisteredClients: %d", BatteryInfo));
   std::vector<IStatusBarHandler*>::iterator itr = _StatusBarCallback.begin();
   for (; itr != _StatusBarCallback.end(); ++itr)
   {
      (*itr)->updateHeaderPhoneBatteryInfo(BatteryInfo);
   }
}


/*
 * sendBTConnectionUpdateToAllRegisteredClients -
 *                               This function is used to send the BT connection update status update to all the registered clients.
 * @param[in] BatteryInfo
 * @param[out] None
 * @return void
 */
void StatusBarHandler::sendBTConnectionUpdateToAllRegisteredClients(uint8 BTConnectInfo)
{
   ETG_TRACE_USR2(("StatusBarHandler::sendBTConnectionUpdateToAllRegisteredClients: %d", BTConnectInfo));
   std::vector<IStatusBarHandler*>::iterator itr = _StatusBarCallback.begin();
   for (; itr != _StatusBarCallback.end(); ++itr)
   {
      (*itr)->updateHeaderPhoneBTConnectInfo(BTConnectInfo);
   }
}


/*
 * sendTAInfoUpdateToAllRegisteredClients -
 *                               This function is used to send the TA Info update status update to all the registered clients.
 * @param[in] TAInfo
 * @param[out] None
 * @return void
 */
void StatusBarHandler::sendTAInfoUpdateToAllRegisteredClients(uint8 TAInfo)
{
   ETG_TRACE_USR2(("StatusBarHandler::sendTAInfoUpdateToAllRegisteredClients: %d", TAInfo));
   std::vector<IStatusBarHandler*>::iterator itr = _StatusBarCallback.begin();
   for (; itr != _StatusBarCallback.end(); ++itr)
   {
      (*itr)->updateHeaderTAInfo(TAInfo);
   }
}


/*
 * sendUnreadedSMSInfoUpdateToAllRegisteredClients -
 *                               This function is used to send the SMS Info update status update to all the registered clients.
 * @param[in] uint8 UnreadedSMSInfo - Number of unreaded messages in the inbox
 * @param[out] None
 * @return void
 */
void StatusBarHandler::sendUnreadedSMSInfoUpdateToAllRegisteredClients(uint8 UnreadedSMSInfo)
{
   ETG_TRACE_USR2(("StatusBarHandler::sendUnreadedSMSInfoUpdateToAllRegisteredClients: %d", UnreadedSMSInfo));
   std::vector<IStatusBarHandler*>::iterator itr = _StatusBarCallback.begin();
   for (; itr != _StatusBarCallback.end(); ++itr)
   {
      (*itr)->updateHeaderUnreadedSMSInfo(UnreadedSMSInfo);
   }
}


/*
 * sendTCUStatusUpdateToAllRegisteredClients -
 *                               This function is used to send the TCUStatus update status update to all the registered clients.
 * @param[in] bool: TCUStatus - true = TCU ON ; false = TCU OFF
 * @param[out] None
 * @return void
 */
void StatusBarHandler::sendTCUStatusUpdateToAllRegisteredClients(bool TCUStatus)
{
   ETG_TRACE_USR2(("StatusBarHandler::sendTCUStatusUpdateToAllRegisteredClients: %d", TCUStatus));
   std::vector<IStatusBarHandler*>::iterator itr = _StatusBarCallback.begin();
   for (; itr != _StatusBarCallback.end(); ++itr)
   {
      (*itr)->updateHeaderTCUStatus(TCUStatus);
   }
}


/*
 * sendSwUpdateUpdateToAllRegisteredClients -
 *                               This function is used to send the SwUpdate update status update to all the registered clients.
 * @param[in] uint8: SwUpdateStatus:
 * 0 - Failed
 * 1 - Downloading -> Should be used by HMI StatusBar.
 * 2 - Copying
 * 3 - Success
 * @param[out] None
 * @return void
 */
void StatusBarHandler::sendSwUpdateStatusUpdateToAllRegisteredClients(uint8 SwUpdateStatus)
{
   ETG_TRACE_USR2(("StatusBarHandler::sendSwUpdateStatusUpdateToAllRegisteredClients: %d", SwUpdateStatus));
   std::vector<IStatusBarHandler*>::iterator itr = _StatusBarCallback.begin();
   for (; itr != _StatusBarCallback.end(); ++itr)
   {
      (*itr)->updateHeaderSwUpdateStatus(SwUpdateStatus);
   }
}


/*
 * sendHVACTemperatureStatusUpdateToAllRegisteredClients -
 *                               This function is used to send the HVAC Temperature Info status update to all the registered clients.
 * @param[in] uint8: HVACTempInfo: temperature
 * @param[in] uint8: HVACTempUnitInfo: Unit - 0 Unknown ; 1 Celsius ; 2 Fahrenheit
 * @param[out] None
 * @return void
 */
void StatusBarHandler::sendHVACTemperatureInfoUpdateToAllRegisteredClients(uint8 HVACTempInfo, uint8 HVACTempUnitInfo)
{
   ETG_TRACE_USR2(("StatusBarHandler::sendHVACTemperatureInfoUpdateToAllRegisteredClients: %d", HVACTempInfo));
   char temp[6] = {};

   if (HVACTempUnitInfo == 1)
   {
      snprintf(temp, sizeof(temp), "%u°C", HVACTempInfo);
   }
   else if (HVACTempUnitInfo == 2)
   {
      snprintf(temp, sizeof(temp), "%u°F", HVACTempInfo);
   }

   std::vector<IStatusBarHandler*>::iterator itr = _StatusBarCallback.begin();
   for (; itr != _StatusBarCallback.end(); ++itr)
   {
      (*itr)->updateHeaderHVACTemperatureInfo(temp);
   }
}


/*
 * sendTimeInfoUpdateToAllRegisteredClients -
 *                               This function is used to send the Time Info update status update to all the registered clients.
 * @param[in] BatteryInfo
 * @param[out] None
 * @return void
 */
void StatusBarHandler::sendTimeInfoUpdateToAllRegisteredClients(uint8 Hours, uint8 Minutes, uint8 /*TimeFormatType*/)
{
   //TODO: Cross check the time format requirement.
   char time[10];
   snprintf(time, sizeof(time), "%u:%02u", Hours, Minutes);

   ETG_TRACE_USR2(("StatusBarHandler::sendTimeInfoUpdateToAllRegisteredClients: %s", time));

   std::vector<IStatusBarHandler*>::iterator itr = _StatusBarCallback.begin();
   for (; itr != _StatusBarCallback.end(); ++itr)
   {
      (*itr)->updateHeaderTimeInfo(time);
   }
}


/*
 * sendMeterConnectionUpdateToAllRegisteredClients -
 *                               This function is used to send the Meter Connection Info update status update to all the registered clients.
 * @param[in] BatteryInfo
 * @param[out] None
 * @return void
 */
void StatusBarHandler::sendMeterConnectionUpdateToAllRegisteredClients(bool  MeterConnectionStatus)
{
   ETG_TRACE_USR2(("StatusBarHandler::sendMeterConnectionUpdateToAllRegisteredClients: %d", MeterConnectionStatus));
   std::vector<IStatusBarHandler*>::iterator itr = _StatusBarCallback.begin();
   for (; itr != _StatusBarCallback.end(); ++itr)
   {
      (*itr)->updateMeterConnectionInfo(MeterConnectionStatus);
   }
}


/*
 * sendStatusLineDefaultDataUpdateToAllRegisteredClients -
 *                               This function is used to send the Default Data Update to all the registered clients.
 * @param[in] BatteryInfo
 * @param[out] None
 * @return void
 */
void StatusBarHandler::sendStatusLineDefaultDataUpdateToAllRegisteredClients()
{
   ETG_TRACE_USR2(("StatusBarHandler::sendStatusLineDefaultDataUpdateToAllRegisteredClients"));
   std::vector<IStatusBarHandler*>::iterator itr = _StatusBarCallback.begin();
   for (; itr != _StatusBarCallback.end(); ++itr)
   {
      (*itr)->updateStatusLineDefaultData();
   }
}


/*
 * getTimeHours - To get current Clock Hours from Datacollector
 * @param[in] None
 * @param[out] None
 * @return uint8 - containing current Hours value
 */
uint8 StatusBarHandler::getTimeHours()
{
   return m_CurrentHours;
}


/*
 * getTimeHours - To get current Clock Minutes from Datacollector
 * @param[in] None
 * @param[out] None
 * @return uint8 - containing current Minutes value
 */
uint8 StatusBarHandler::getTimeMinutes()
{
   return m_CurrentMinutes;
}


/*
 * getTimeFormatType - To get current Clock TimeFormat from Datacollector
 * @param[in] None
 * @param[out] None
 * @return uint8 - containing current Minutes value
 */
uint8 StatusBarHandler::getTimeFormatType()
{
   return m_CurrentTimeFormatType;
}


/*
 * getTAInfo - To get status of Traffic Announcement Information from Datacollector
 * @param[in] None
 * @param[out] None
 * @return uint8 - status of Traffic Announcement
 */
uint8 StatusBarHandler::getTAInfo()
{
   return m_CurrentTAInfo;
}


/*
 * getBTConnectionState - To get status of Bluetooth Connection State from Datacollector
 * @param[in] None
 * @param[out] None
 * @return uint8 - status of Bluetooth Connection State
 */
uint8 StatusBarHandler::getBTConnectionState()
{
   return m_CurrentBTConnectionState;
}


/*
 * getBatteryResidualQuantity - To get status of Battery Residual Quantity from Datacollector
 * @param[in] None
 * @param[out] None
 * @return uint8 - containing current Battery Residual Quantity status in blocks of total percentage
 */
uint8 StatusBarHandler::getBatteryResidualQuantity()
{
   return m_CurrentBatteryResidualQuantity;
}


/*
 * getPhoneSignalStrength - To get status of Phone Signal Strength from Datacollector
 * @param[in] None
 * @param[out] None
 * @return uint8 - containing current Phone Signal Strength in blocks of total percentage
 */
uint8 StatusBarHandler::getPhoneSignalStrength()
{
   return m_CurrentPhoneSignalStrength;
}


//utility function
template <typename T>
std::string NumberToString(T Number)
{
   std::stringstream ss;
   ss << Number;
   return ss.str();
}


/*
 * vPopulateHeaderPriorityMap - Populate the popup priority table for each app to display the repective icon in header
 * @param[in] None
 * @param[out] None
 * @return void
 */
void StatusBarHandler::vPopulateHeaderPriorityMap()
{
   const uint8 itemSize = sizeof(aIconPriority) / sizeof(HeaderIconPriority);
   for (uint8 item = 0; item < itemSize; ++item)
   {
      uint8 uHeaderType = aIconPriority[item].headerType;
      bool bETC = aIconPriority[item].bETC;
      bool bTCU = aIconPriority[item].bTCU;
      bool bPhoneSignal = aIconPriority[item].bPhoneSignal;
      bool bPhonebookDnld = aIconPriority[item].bPhonebookDnld;
      bool bSMS = aIconPriority[item].bSMS;
      bool bSWUpdate = aIconPriority[item].bSWUpdate;
      bool bPhoneBattery = aIconPriority[item].bPhoneBattery;
      bool bBT = aIconPriority[item].bBT;
      bool bDTV = aIconPriority[item].bDTV;
      bool bTA = aIconPriority[item].bTA;
      // add entries to map
      if (bETC == 1)
      {
         _appPriorityMap.insert(std::make_pair(uHeaderType, ETC));
      }
      if (bTCU == 1)
      {
         _appPriorityMap.insert(std::make_pair(uHeaderType, TCU));
      }
      if (bPhoneSignal == 1)
      {
         _appPriorityMap.insert(std::make_pair(uHeaderType, PH_SIG));
      }
      if (bPhonebookDnld == 1)
      {
         _appPriorityMap.insert(std::make_pair(uHeaderType, PH_DNLD));
      }
      if (bSMS == 1)
      {
         _appPriorityMap.insert(std::make_pair(uHeaderType, SMS));
      }
      if (bSWUpdate == 1)
      {
         _appPriorityMap.insert(std::make_pair(uHeaderType, SW_UPD));
      }
      if (bPhoneBattery == 1)
      {
         _appPriorityMap.insert(std::make_pair(uHeaderType, PH_BAT));
      }
      if (bBT == 1)
      {
         _appPriorityMap.insert(std::make_pair(uHeaderType, BT));
      }
      if (bDTV == 1)
      {
         _appPriorityMap.insert(std::make_pair(uHeaderType, DTV));
      }
      if (bTA == 1)
      {
         _appPriorityMap.insert(std::make_pair(uHeaderType, TA));
      }
   }
   ETG_TRACE_USR4(("StatusBarHandler::vPopulateHeaderPriorityMap: %d", _appPriorityMap.size()));
}


/*
 * getETCHeaderIconInfo - retrieve icons based on ETC status
 * @param[in] None
 * @param[out] HeaderInfo
 * @return struct containing icon path and value if present
 */
HeaderInfo StatusBarHandler::getETCHeaderIconInfo()
{
   HeaderInfo info;
   //todo: add icon path once ETC status is available
   info.sIconPath = "";
   info.sdata = "";
   return info;
}


/*
 * getTCUHeaderIconInfo - retrieve icons based on TCU status
 * @param[in] None
 * @param[out] HeaderInfo
 * @return struct containing icon path and value if present
 */
HeaderInfo StatusBarHandler::getTCUHeaderIconInfo()
{
   HeaderInfo info;
   if (m_CurrentTCUStatus == true)
   {
      info.sIconPath = g_IconPath[TCU_ENABLE];
   }
   else
   {
      info.sIconPath = g_IconPath[TCU_DISABLE];
   }
   info.sdata = "";
   return info;
}


/*
 * getPhoneSignalHeaderIconInfo - retrieve icons based on phone signal status
 * @param[in] None
 * @param[out] HeaderInfo
 * @return struct containing icon path and value if present
 */
HeaderInfo StatusBarHandler::getPhoneSignalHeaderIconInfo()
{
   HeaderInfo info;
   switch (m_CurrentPhoneSignalStrength)
   {
      case 0:
         info.sIconPath = g_IconPath[PHONE_SIG_0];
         break;
      case 1:
         info.sIconPath = g_IconPath[PHONE_SIG_1];
         break;
      case 2:
         info.sIconPath = g_IconPath[PHONE_SIG_2];
         break;
      case 3:
         info.sIconPath = g_IconPath[PHONE_SIG_3];
         break;
      case 4:
         info.sIconPath = g_IconPath[PHONE_SIG_4];
         break;
      case 5:
         info.sIconPath = g_IconPath[PHONE_SIG_5];
         break;
      default:
         info.sIconPath = "";
         break;
   }
   info.sdata = "";
   return info;
}


/*
 * getPhoneDownloadHeaderIconInfo - retrieve icons based on phone download status
 * @param[in] None
 * @param[out] HeaderInfo
 * @return struct containing icon path and value if present
 */
HeaderInfo StatusBarHandler::getPhoneDownloadHeaderIconInfo()
{
   HeaderInfo info;
   //todo: add icon path once status is available
   info.sIconPath = "";
   info.sdata = "";
   return info;
}


/*
 * getSMSHeaderIconInfo - retrieve icons based on SMS status
 * @param[in] None
 * @param[out] HeaderInfo
 * @return struct containing icon path and value if present
 */
HeaderInfo StatusBarHandler::getSMSHeaderIconInfo()
{
   HeaderInfo info;
   info.sIconPath = "";
   info.sdata = "";
   if (m_bNewSMSNotification == true)
   {
      info.sIconPath = g_IconPath[SMS_NEW];
   }
   else if (m_CurrentUnreadedSMSInfo)
   {
      info.sIconPath = g_IconPath[SMS_UNREAD];
      info.sdata = NumberToString(m_CurrentUnreadedSMSInfo);
   }
   return info;
}


/*
 * getSwUpdateHeaderIconInfo - retrieve icons based on SW Update status
 * @param[in] None
 * @param[out] HeaderInfo
 * @return struct containing icon path and value if present
 */
HeaderInfo StatusBarHandler::getSwUpdateHeaderIconInfo()
{
   HeaderInfo info;
   switch (m_CurrentSwUpdateStatus)
   {
      case 0:
         info.sIconPath = g_IconPath[SW_UPD_0];
         break;
      case 1:
         info.sIconPath = g_IconPath[SW_UPD_1];
         break;
      case 2:
         info.sIconPath = g_IconPath[SW_UPD_2];
         break;
      case 3:
         info.sIconPath = g_IconPath[SW_UPD_3];
         break;
      default:
         info.sIconPath = "";
         break;
   }
   info.sdata = "";
   return info;
}


/*
 * getPhoneBatteryHeaderIconInfo - retrieve icons based on phone battery status
 * @param[in] None
 * @param[out] HeaderInfo
 * @return struct containing icon path and value if present
 */
HeaderInfo StatusBarHandler::getPhoneBatteryHeaderIconInfo()
{
   HeaderInfo info;
   switch (m_CurrentBatteryResidualQuantity)
   {
      case 0:
         info.sIconPath = g_IconPath[PHONE_BAT_0];
         break;
      case 1:
         info.sIconPath = g_IconPath[PHONE_BAT_1];
         break;
      case 2:
         info.sIconPath = g_IconPath[PHONE_BAT_2];
         break;
      case 3:
         info.sIconPath = g_IconPath[PHONE_BAT_3];
         break;
      case 4:
         info.sIconPath = g_IconPath[PHONE_BAT_4];
         break;
      case 5:
         info.sIconPath = g_IconPath[PHONE_BAT_5];
         break;
      default:
         info.sIconPath = "";
         break;
   }
   info.sdata = "";
   ETG_TRACE_USR4(("StatusBarHandler::getPhoneBatteryHeaderIconInfo Phone Battery icon %s", info.sIconPath.c_str()));
   return info;
}


/*
 * getBTHeaderIconInfo - retrieve icons based on BT status
 * @param[in] None
 * @param[out] HeaderInfo
 * @return struct containing icon path and value if present
 */
HeaderInfo StatusBarHandler::getBTHeaderIconInfo()
{
   HeaderInfo info;
   if (m_CurrentBTConnectionState == 1)
   {
      info.sIconPath = g_IconPath[BT_ENABLE];
   }
   else
   {
      info.sIconPath = "";
   }
   info.sdata = "";
   return info;
}


/*
 * getDTVHeaderIconInfo - retrieve icons based on DTV status
 * @param[in] None
 * @param[out] HeaderInfo
 * @return struct containing icon path and value if present
 */
HeaderInfo StatusBarHandler::getDTVHeaderIconInfo()
{
   HeaderInfo info;
   //todo: add icon path once status is available
   info.sIconPath = "";
   info.sdata = "";
   return info;
}


/*
 * getTAHeaderIconInfo - retrieve icons based on TA status
 * @param[in] None
 * @param[out] HeaderInfo
 * @return struct containing icon path and value if present
 */
HeaderInfo StatusBarHandler::getTAHeaderIconInfo()
{
   HeaderInfo info;
   if (m_CurrentTAInfo == 1)
   {
      info.sIconPath = g_IconPath[TA_ENABLE_TP_ENABLE];
      info.sdata = "";
   }
   else
   {
      info.sIconPath = g_IconPath[TA_ENABLE_TP_DISABLE];
      info.sdata = "";
   }
   return info;
}


/*
 * getHeaderIconPath - To get the icon path to be displayed on header based on the requested type
 * @param[in] reqHeaderType
 * @param[out] std::vector<HeaderInfo>
 * @return vector of struct comprising of correct icon path and text based on the popup priority table.
 */
std::vector<HeaderInfo> StatusBarHandler::getHeaderIconPath(HeaderTypeEnum reqHeaderType)
{
   std::vector<HeaderInfo> iconData;
   uint8 uMaxEntries = 0;
   if (reqHeaderType >= 0)
   {
      std::multimap<uint8, std::string>::iterator m_it = _appPriorityMap.find(reqHeaderType);
      uint8 cnt = _appPriorityMap.count(reqHeaderType);
      for (uint8 i = 0; i < cnt; ++m_it, ++i)
      {
         if (strcmp(((*m_it).second).c_str(), ETC) == 0)
         {
            HeaderInfo data = StatusBarHandler::getETCHeaderIconInfo();
            if (data.sIconPath != "")
            {
               iconData.push_back(data);
               ++uMaxEntries;
            }
         }

         if (strcmp(((*m_it).second).c_str(), TCU) == 0)
         {
            HeaderInfo data = StatusBarHandler::getTCUHeaderIconInfo();
            if (data.sIconPath != "")
            {
               iconData.push_back(data);
               ++uMaxEntries;
            }
         }

         if (strcmp(((*m_it).second).c_str(), PH_SIG) == 0)
         {
            HeaderInfo data = StatusBarHandler::getPhoneSignalHeaderIconInfo();
            if (data.sIconPath != "")
            {
               iconData.push_back(data);
               ++uMaxEntries;
            }
         }

         if (strcmp(((*m_it).second).c_str(), PH_DNLD) == 0)
         {
            HeaderInfo data = StatusBarHandler::getPhoneDownloadHeaderIconInfo();
            if (data.sIconPath != "")
            {
               iconData.push_back(data);
               ++uMaxEntries;
            }
         }

         if (strcmp(((*m_it).second).c_str(), SMS) == 0)
         {
            HeaderInfo data = StatusBarHandler::getSMSHeaderIconInfo();
            if (data.sIconPath != "")
            {
               iconData.push_back(data);
               ++uMaxEntries;
            }
         }

         if (strcmp(((*m_it).second).c_str(), SW_UPD) == 0)
         {
            // // todo: Enable when SW_UPD images are available
            //iconData.push_back(StatusBarHandler::getSwUpdateHeaderIconInfo());
         }

         if (strcmp(((*m_it).second).c_str(), PH_BAT) == 0)
         {
            HeaderInfo data = StatusBarHandler::getPhoneBatteryHeaderIconInfo();
            if (data.sIconPath != "")
            {
               iconData.push_back(data);
               ++uMaxEntries;
            }
         }

         if (strcmp(((*m_it).second).c_str(), BT) == 0)
         {
            HeaderInfo data = StatusBarHandler::getBTHeaderIconInfo();
            if (data.sIconPath != "")
            {
               iconData.push_back(data);
               ++uMaxEntries;
            }
         }

         if (strcmp(((*m_it).second).c_str(), DTV) == 0)
         {
            HeaderInfo data = StatusBarHandler::getDTVHeaderIconInfo();
            if (data.sIconPath != "")
            {
               iconData.push_back(data);
               ++uMaxEntries;
            }
         }

         if (strcmp(((*m_it).second).c_str(), TA) == 0)
         {
            //todo: Enable when TA images are available
            //iconData.push_back(StatusBarHandler::getTAHeaderIconInfo());
         }

         //only 4 icons can be displayed in status bar
         if (uMaxEntries > STATUS_BAR_MAX_ENTRIES)
         {
            break;
         }
      }
   }
   return iconData;
}


} // namespace StatusBarHandler
