/*
 * StatusBarHandler.h
 *
 * Author: CM-PJCB
 */

#ifndef STATUSBARHANDLER_H
#define STATUSBARHANDLER_H

#include <sstream>
#include "datacollector_main_fiProxy.h"
#include "BaseContract/generated/BaseMsgs.h"
#include "IStatusBarHandler.h"
#include "AppBase/ServiceAvailableIF.h"
//header types
#include "HeaderTypes.h"

namespace StatusBarCommonHandler {

struct HeaderInfo
{
   std::string sIconPath;
   std::string sdata;
};


template <typename T>
std::string NumberToString(T Number);

class StatusBarHandler :
   public hmibase::ServiceAvailableIF,
   public StartupSync::PropertyRegistrationIF,
   public ::datacollector_main_fi::Property_HMIStatusBarInfoCallbackIF,
   public ::datacollector_main_fi::SetInterrupt_to_MeterCallbackIF,
   public ::datacollector_main_fi::Property_ConnectionCallbackIF
{
   public:
      StatusBarHandler();
      virtual ~StatusBarHandler();

      void vRegisterforUpdate(IStatusBarHandler* client);
      void vUnRegisterforUpdate(IStatusBarHandler* client);

      virtual void registerProperties(const boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange);
      virtual void deregisterProperties(const boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange);

      virtual void onSetInterrupt_to_MeterError(const ::boost::shared_ptr< ::datacollector_main_fi::Datacollector_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::datacollector_main_fi::SetInterrupt_to_MeterError >& /*error*/);
      virtual void onSetInterrupt_to_MeterResult(const ::boost::shared_ptr< ::datacollector_main_fi::Datacollector_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::datacollector_main_fi::SetInterrupt_to_MeterResult >& /*result*/);

      virtual void onProperty_HMIStatusBarInfoError(const ::boost::shared_ptr< ::datacollector_main_fi::Datacollector_main_fiProxy >& proxy, const ::boost::shared_ptr< ::datacollector_main_fi::Property_HMIStatusBarInfoError >& /*error*/);
      virtual void onProperty_HMIStatusBarInfoStatus(const ::boost::shared_ptr< ::datacollector_main_fi::Datacollector_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::datacollector_main_fi::Property_HMIStatusBarInfoStatus >& status);

      virtual void onProperty_ConnectionError(const ::boost::shared_ptr< ::datacollector_main_fi::Datacollector_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::datacollector_main_fi::Property_ConnectionError >& /*error*/);
      virtual void onProperty_ConnectionStatus(const ::boost::shared_ptr< ::datacollector_main_fi::Datacollector_main_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::datacollector_main_fi::Property_ConnectionStatus >& status);

      //todo: Start Check if it really requires.
      uint8 getTimeHours();
      uint8 getTimeMinutes();
      uint8 getTimeFormatType();
      uint8 getTAInfo();
      uint8 getBTConnectionState();
      uint8 getBatteryResidualQuantity();
      uint8 getPhoneSignalStrength();

      void vPopulateHeaderPriorityMap();
      HeaderInfo getETCHeaderIconInfo();
      HeaderInfo getTCUHeaderIconInfo();
      HeaderInfo getPhoneSignalHeaderIconInfo();
      HeaderInfo getPhoneDownloadHeaderIconInfo();
      HeaderInfo getSMSHeaderIconInfo();
      HeaderInfo getSwUpdateHeaderIconInfo();
      HeaderInfo getPhoneBatteryHeaderIconInfo();
      HeaderInfo getBTHeaderIconInfo();
      HeaderInfo getDTVHeaderIconInfo();
      HeaderInfo getTAHeaderIconInfo();
      std::vector<HeaderInfo> getHeaderIconPath(HeaderTypeEnum reqHeaderType);

      void setInterruptToMeter(int8 audioSrc, int8 interruptType, bool interruptMode = true);

      bool onCourierMessage(const GuiStartupFinishedUpdMsg& msg);
      COURIER_MSG_MAP_BEGIN(0)
      ON_COURIER_MESSAGE(GuiStartupFinishedUpdMsg)
      COURIER_MSG_MAP_END()

      static StatusBarHandler* GetInstance();

   private:

      ::boost::shared_ptr< ::datacollector_main_fi::Datacollector_main_fiProxy > _dataCollectorProxy;

      void sendSignalStrengthUpdateToAllRegisteredClients(uint8 SignalStrength);
      void sendBatteryInfoUpdateToAllRegisteredClients(uint8 BatteryInfo);
      void sendBTConnectionUpdateToAllRegisteredClients(uint8 BTConnectInfo);
      void sendTAInfoUpdateToAllRegisteredClients(uint8 TAInfo);
      void sendUnreadedSMSInfoUpdateToAllRegisteredClients(uint8 SMSInfo);
      void sendTCUStatusUpdateToAllRegisteredClients(bool TCUStatus);
      void sendSwUpdateStatusUpdateToAllRegisteredClients(uint8 SwUpdateStatus);
      void sendHVACTemperatureInfoUpdateToAllRegisteredClients(uint8 HVACTempInfo, uint8 HVACTempUnitInfo);
      void sendTimeInfoUpdateToAllRegisteredClients(uint8 Hours, uint8 Minutes, uint8 TimeFormatType);
      void sendMeterConnectionUpdateToAllRegisteredClients(bool  MeterConnectionStatus);
      void sendStatusLineDefaultDataUpdateToAllRegisteredClients();

      /* private variables */
      /* Clock */
      uint8 m_CurrentHours;
      uint8 m_CurrentMinutes;
      uint8 m_CurrentTimeFormatType;

      /* TA */
      uint8 m_CurrentTAInfo;

      /* Bluetooth */
      uint8 m_CurrentBTConnectionState;
      uint8 m_CurrentBatteryResidualQuantity;
      uint8 m_CurrentPhoneSignalStrength;

      /* SMS */
      uint8 m_CurrentUnreadedSMSInfo;
      bool m_bNewSMSNotification;

      /* TCU */
      bool m_CurrentTCUStatus;

      /* Download */
      uint8 m_CurrentSwUpdateStatus;

      /* HVAC */
      uint8 m_CurrentHVACTempInfo;
      uint8 m_CurrentHVACTempUnitInfo;

      /* AudioInfo */
      uint8 m_CurrentAudioInfo;

      bool m_MeterConnectionStatus;

      static StatusBarHandler* _statusBarHandler;
      std::vector<IStatusBarHandler*> _StatusBarCallback;
      std::multimap<uint8, std::string> _appPriorityMap;
};


} // namespace StatusBarHandler
#endif /* STATUSBARHANDLER_H */
