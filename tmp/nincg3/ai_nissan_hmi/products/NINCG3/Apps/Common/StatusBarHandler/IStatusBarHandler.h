/*
 * IStatusBarHandler.h
 *
 * Author: ajk1kor
 */

#ifndef ISTATUSBARHANDLER_H_
#define ISTATUSBARHANDLER_H_


class IStatusBarHandler
{
   public:
      virtual ~IStatusBarHandler() {}
      virtual void updatePhoneSignalStrength(uint8 SignalStrength) = 0;
      virtual void updateHeaderPhoneBatteryInfo(uint8 BatteryInfo) = 0;
      virtual void updateHeaderPhoneBTConnectInfo(uint8 BTConnectInfo) = 0;
      virtual void updateHeaderTAInfo(uint8 TAInfo) = 0;
      virtual void updateHeaderUnreadedSMSInfo(uint8 /*SMSInfo*/) {};
      virtual void updateHeaderTCUStatus(bool /*TCUStatus*/) {};
      virtual void updateHeaderSwUpdateStatus(uint8 /*SwUpdateStatus*/) {};
      virtual void updateHeaderHVACTemperatureInfo(std::string /*HVACTempInfo*/) {};
      virtual void updateHeaderTimeInfo(std::string TimeInfo) = 0;
      virtual void updateMeterConnectionInfo(bool MeterConnectionStatus) = 0;
      virtual void updateStatusLineDefaultData() = 0;
};


#endif /* ISTATUSBARHANDLER_H_ */
