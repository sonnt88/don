/**
 * @file <VehicleDataHandler.h>
 * @author A-IVI HMI Team
 * @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
 * @addtogroup <AppHmi_Common>
 */


#include "gui_std_if.h"
#include "AppBase/StartupSync/StartupSync.h"
#include "VehicleDataHandler.h"
#include "ProjectBaseMsgs.h"


#ifdef DP_DATAPOOL_ID
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_generic_if.h" //for read kds
#endif

#include "hmi_trace_if.h"
#include "../Common_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_VEHICLEDATA
#include "trcGenProj/Header/VehicleDataHandler.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN

using namespace std;
using namespace ::VEHICLE_MAIN_FI;
using namespace ::vehicle_main_fi_types;


namespace VehicleDataCommonHandler {
VehicleDataHandler* VehicleDataHandler::_commonVehicleDataHandler = NULL;
/*
 * @Constructor
 */
VehicleDataHandler::VehicleDataHandler() :
   _vehicleProxy(VEHICLE_MAIN_FIProxy::createProxy("vehicleMainFiPort", *this)),
   _region(UNUSED),
   _languageIndex(UNUSED),
   _speedLockState(false),
   _guiInitialized(false)
{
   StartupSync::getInstance().registerPropertyRegistrationIF(this);
#ifndef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1 //Feature only for Scope2
   _unitsDataHandlerClientCallback.clear();
#endif
   //Initialize the ISO Codes in _languageISOCodeMap for corresponding language
   initializeLanguageISOCodeMap();
   readRegionFromKDS();
   _commonVehicleDataHandler = this;
   ETG_TRACE_USR4(("VehicleDataHandler::VehicleDataHandler Constructor"));
}


/*
 * GetInstance - To get the common languagehandler instance
 * @param[in] None
 * @param[out] None
 * @return VehicleDataHandler
 */
VehicleDataHandler* VehicleDataHandler::GetInstance()
{
   ETG_TRACE_USR4(("VehicleDataHandler::GetInstance"));
   if (_commonVehicleDataHandler == NULL)
   {
      _commonVehicleDataHandler = new VehicleDataHandler();
   }
   return _commonVehicleDataHandler;
}


/*
 * @Destructor
 */
VehicleDataHandler::~VehicleDataHandler()
{
   if (!(_languageISOCodeMap.empty()))
   {
      _languageISOCodeMap.clear(); //clear the _languageISOCodeMap
   }

   if (_commonVehicleDataHandler != NULL)
   {
      delete _commonVehicleDataHandler;
      _commonVehicleDataHandler = NULL;
   }
#ifndef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1 //Feature only for Scope2
   _unitsDataHandlerClientCallback.clear();
   vector<IUnitsDataHandler*>(_unitsDataHandlerClientCallback).swap(_unitsDataHandlerClientCallback);
#endif
}


/*
 * onAvailable - To Handle Service Availability
 * @param[in] proxy
 * @param[in] stateChange
 * @param[out] None
 * @return void
 */
void VehicleDataHandler::registerProperties(const boost::shared_ptr<asf::core::Proxy>& proxy, \
      const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_vehicleProxy && (proxy == _vehicleProxy))
   {
      ETG_TRACE_USR4(("VehicleDataHandler::registerProperties"));
      //Register for all the required properties update
      //property to Get currently configured language id
      _vehicleProxy->sendLanguageUpReg(*this);
      //property to Set/Get Language Syn source ID. Source can be either HMI or Meter
      _vehicleProxy->sendLanguageSyncSourceUpReg(*this);
      //property to Get Speed from V-CAN
      _vehicleProxy->sendSpeedUpReg(*this);
      //property to Get ReverseGear Status from the GPIO pin
      _vehicleProxy->sendReverseGearUpReg(*this);
      //property to Get Handbrake Status
      _vehicleProxy->sendHandBrakeUpReg(*this);
      //property to Get SpeedLimit Status
      _vehicleProxy->sendSpeedLimitUpReg(*this);
#ifndef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1 //Feature only for Scope2
      if (_vehicleProxy)
      {
         _vehicleProxy->sendDistanceUnitUpReg(*this);
         _vehicleProxy->sendFuelConsumptionUnitUpReg(*this);
         _vehicleProxy->sendTemperatureUnitUpReg(*this);
      }
#endif
   }
}


/*
 * onUnavailable - To Handle Service Unavailability
 * @param[in] proxy
 * @param[in] stateChange
 * @param[out] None
 * @return void
 */
void VehicleDataHandler::deregisterProperties(const boost::shared_ptr<asf::core::Proxy>& proxy, \
      const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_vehicleProxy && (proxy == _vehicleProxy))
   {
      //Unregister for all the required properties update
      _vehicleProxy->sendRelUpRegAll();
   }
}


/*
 * onCourierMessage - To Handle GUI startup message
 * when the GUI is ready, request all clients to read language id from dp and then set culture message
 * @param[in] GuiStartupFinishedUpdMsg
 * @param[out] None
 * @return bool
 */
bool VehicleDataHandler::onCourierMessage(const GuiStartupFinishedUpdMsg& /*msg*/)
{
   uint8 languageIndex = 0;
   _guiInitialized = true;
   std::vector<iLanguageUpdate*>::iterator itLanguageInst = _languageUpdateCallback.begin();
   for (; itLanguageInst != _languageUpdateCallback.end(); ++itLanguageInst)
   {
      languageIndex = (*itLanguageInst)->readLanguageOnStartUp();
      //if language index is invalid, read from KDS
      if (languageIndex == INVALID_LANGUAGE)
      {
         languageIndex = getLanguageIndexfromKDS();
      }
   }
   sendLanguageUpdateToGUI(languageIndex);

   sendSpeedLockUpdate();

   //StartupSync::getInstance().onGuiStartupFinished();
   return false;
}


/*
 * getLanguageIndexfromKDS - To read language settings from KDS
 * @param[in] None
 * @param[out] None
 * @return uint8 u8Language
 */
uint8 VehicleDataHandler::getLanguageIndexfromKDS()
{
   dp_tclKdsSystemConfiguration1 languageConfig;
   uint8 u8Language = INVALID_LANGUAGE;
   uint8 kdsError = 0;
   kdsError = languageConfig.u8GetDefaultLanguage(u8Language); //todo: kdsError to be handled
   if (kdsError != DP_U8_ELEM_STATUS_VALID)
   {
      u8Language = INVALID_LANGUAGE;
   }
   return u8Language;
}


/*
 * vRegisterforUpdate -This function is used to register all the clients for the property update
 * @param[in] GuiStartupFinishedUpdMsg
 * @param[out] None
 * @return void
 */
//todo: if the instance is not singleton,please remove list handling
void VehicleDataHandler::vRegisterforUpdate(iLanguageUpdate* client)
{
   std::vector< iLanguageUpdate* >::const_iterator itr = std::find(_languageUpdateCallback.begin(), \
         _languageUpdateCallback.end(), client);
   if (itr == _languageUpdateCallback.end()) //for safety
   {
      //add instance
      _languageUpdateCallback.push_back(client);
   }
   else
   {
      //Info:User already registered the instance
   }
}


/*
 * vUnRegisterforUpdate -This function is used to Unregister for the language status update for all the registered clients
 * @param[in] iLanguageUpdate
 * @param[out] None
 * @return void
 */
void VehicleDataHandler::vUnRegisterforUpdate(iLanguageUpdate* client)
{
   std::vector<iLanguageUpdate*>::iterator itLanguageInst = _languageUpdateCallback.begin();
   for (; itLanguageInst != _languageUpdateCallback.end(); ++itLanguageInst)
   {
      if (client == *itLanguageInst)
      {
         _languageUpdateCallback.erase(itLanguageInst);
         break;
      }
   }
}


/*
 * sendLanguageUpdate -This function is used to send the language status update to allt he registered clients
 * @param[in] iLanguageUpdate
 * @param[out] None
 * @return void
 */
void VehicleDataHandler::sendLanguageUpdate(uint8 languageIndex, std::vector<T_Language_SourceTable> languageSourceTable)
{
   std::vector<iLanguageUpdate*>::iterator itLanguageInst = _languageUpdateCallback.begin();
   ETG_TRACE_USR4(("VehicleDataHandler::sendLanguageUpdate"));
   for (; ((itLanguageInst != _languageUpdateCallback.end()) && (*itLanguageInst != NULL)); ++itLanguageInst)
   {
      (*itLanguageInst)->writeLanguageintoDP(languageIndex);
      //Info: Added languageSourceTable valid check for T_e8_Language_SourceId__System SourceId to avoid the
      //		start up reset because of languageSourceTable data unavailabilty for T_e8_Language_SourceId__System SourceId(-> Because all the applications are reffering the T_e8_Language_SourceId__System SourceId)
      //TODO: @Prema: Please remove this here once HMI applications are added the required SourceId availability check in updateLanguageTable
      //		 before accessing the languageSourceTable vector for required SourceId -sve2cob
      if (languageSourceTable.size() > T_e8_Language_SourceId__System)
      {
         (*itLanguageInst)->updateLanguageTable(languageSourceTable);
      }
      else
      {
         ETG_TRACE_ERR(("VehicleDataHandler:ERROR:languageSourceTable doesn't have the content for T_e8_Language_SourceId__System SourceId"));
      }
   }
}


/*
 * sendLanguageUpdate - Handle to send the Language update to Framework and Model
 * @param[in] isoLangCode
 * @param[out] none
 * @return bool
 */
void VehicleDataHandler::sendLanguageUpdateToGUI(uint8 languageindex)
{
   //pmy5cob- Setculture is forfully send as "en_US" Since the language xmls are not available for EUR region
   //This is temporary solution for NCG3D-6629 - TODO : remove once the xls files for NAR is ready.
#ifndef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
   if (_region != KDS_REGION_NAM)
   {
      COURIER_MESSAGE_NEW(Courier::SetCultureReqMsg)(Courier::ItemId("en_US"))->Post();
      return;
   }
#endif
   std::map< ::vehicle_main_fi_types::T_e8_Language_Code, std::string>::iterator itr = \
         _languageISOCodeMap.find((T_e8_Language_Code)languageindex);
   //todo : if the received language index from the registered component is 0, read language index from KDS and update the same into datapool and also reflect in GUI
   if (itr != _languageISOCodeMap.end())
   {
      //send Language update to Framework to change all static text
      COURIER_MESSAGE_NEW(Courier::SetCultureReqMsg)(Courier::ItemId((itr->second).c_str()))->Post();
   }
}


/*
 * findISOCodeByLanguageIndex - find the matching language ISO code by the input of the language index
 * @param[in] languageindex
 * @param[out] matchingISOCode
 * @return bool
 */
bool VehicleDataHandler::findISOCodeByLanguageIndex(uint8 languageindex, std::string& matchingISOCode)
{
   bool bLanguageIndexIsInList = false;

   std::map< ::vehicle_main_fi_types::T_e8_Language_Code, std::string>::iterator itr = \
         _languageISOCodeMap.find((T_e8_Language_Code)languageindex);
   if (itr != _languageISOCodeMap.end())
   {
      bLanguageIndexIsInList = true;
      matchingISOCode = (itr->second);
   }
   return bLanguageIndexIsInList;
}


/*
 * onLanguageStatus - ASF framework callback which is triggered on every setLanguage()
 * @param[in] proxy
 * @param[in] status
 * @param[out] None
 * @return voidl
 * todo : 17-07-15, As of now, after setLanguage, onLanguageStatus() is received twice from the VD_Vehicle data.
 * First update represents, previously configured Language ID and the second one represents currently configured ID.
 * both time we are sending SetCultureMsg to HMI WHICH HAS TO BE OPTIMIZED.L
 */
void VehicleDataHandler::onLanguageStatus(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& /*proxy*/,  const ::boost::shared_ptr< LanguageStatus >& status)
{
   ETG_TRACE_USR4(("onLanguageStatus"));
   std::vector<T_Language_SourceTable> langTable = status->getLangTable();
   uint8 langIndex = status->getLanguage();
   //update to all registered clients
   sendLanguageUpdate(langIndex, langTable);
   if (_languageIndex != langIndex)
   {
      _languageIndex = langIndex;
      //update to GUI
      sendLanguageUpdateToGUI(langIndex);
   }
}


/*
 * onLanguageError - ASF framework callback which is triggered if there is any error in setLanguage()
 * @param[in] proxy
 * @param[in] error
 * @param[out] None
 * @return void
 */
void VehicleDataHandler::onLanguageError(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& /*proxy*/, const ::boost::shared_ptr< LanguageError >& /*error*/)
{
}


/*
 * onLanguageSyncSourceStatus - ASF framework callback which is triggered on  setLanguageSyncSource
 * @param[in] proxy
 * @param[in] status
 * @param[out] None
 * @return void
 */
void VehicleDataHandler::onLanguageSyncSourceStatus(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& /*proxy*/, const ::boost::shared_ptr< LanguageSyncSourceStatus >& status)
{
   uint8 synSrcid = status->getLanguage();
   updateSyncSourceId(synSrcid);
}


/*
 * onLanguageSyncSourceError - ASF framework callback which is triggered when there is an error in  setLanguageSyncSource
 * @param[in] error
 * @param[in] status
 * @param[out] None
 * @return void
 */
void VehicleDataHandler::onLanguageSyncSourceError(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& /*proxy*/, const ::boost::shared_ptr< LanguageSyncSourceError >& /*error*/)
{
}


/*
 * initializeLanguageISOCodeMap - Handle to Initialize the ISO Codes in _languageISOCodeMap
 *                            for corresponding language
 * @param[in] None
 * @param[out] None
 * @return void
 */
void VehicleDataHandler::initializeLanguageISOCodeMap()
{
   //todo : "xx-XX" to be completed
   _languageISOCodeMap[T_e8_Language_Code__Unknown]                                                = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__Albanese]                                               = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__Arabic]                                                 = std::string("ar_AR");
   _languageISOCodeMap[T_e8_Language_Code__Bulgarian]                                              = std::string("bg_BG");
   _languageISOCodeMap[T_e8_Language_Code__Chinese_Cantonese_Simplified_Chinese_character]         = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__Chinese_Cantonese_Traditional_Chinese_character]        = std::string("zh_HK"); //29 - zh_HK - Cantonese
   _languageISOCodeMap[T_e8_Language_Code__Chinese_Mandarin_Simplified_Chinese_character]          = std::string("zh_CN"); //28 - zh_CN - Mandarin
   _languageISOCodeMap[T_e8_Language_Code__Croatian]                                               = std::string("hr_HR"); //12 - hr_HR - Hrvatski
   _languageISOCodeMap[T_e8_Language_Code__Czech]                                                  = std::string("cs_CS"); //2 - cs_CZ - Cestina
   _languageISOCodeMap[T_e8_Language_Code__Danish]                                                 = std::string("da_DA");
   _languageISOCodeMap[T_e8_Language_Code__Dutch]                                                  = std::string("nl_NL");//8 - es_ES - EspaÃ±ol
   _languageISOCodeMap[T_e8_Language_Code__English_Australian]                                     = std::string("en_AU");
   _languageISOCodeMap[T_e8_Language_Code__English_Canadian]                                       = std::string("en_CA");
   _languageISOCodeMap[T_e8_Language_Code__English_India]                                          = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__English_UK]                                             = std::string("en_GB"); //6 - en_GB - English UK
   _languageISOCodeMap[T_e8_Language_Code__English_US]                                             = std::string("en_US"); //1 - bs_BA - Bosanski
   _languageISOCodeMap[T_e8_Language_Code__English_US_for_JPN]                                     = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__English_US_for_PRC]                                     = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__Estonian]                                               = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__Finnish]                                                = std::string("fi_FI"); //25 - fi_FI - Suomi
   _languageISOCodeMap[T_e8_Language_Code__Flemish]                                                = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__French]                                                 = std::string("fr_FR"); //10 - fr_FR - FranÃ§ais
   _languageISOCodeMap[T_e8_Language_Code__French_Canadian]                                        = std::string("fr_CA"); //11 - fr_CA - FranÃ§ais quÃ©bÃ©cois
   _languageISOCodeMap[T_e8_Language_Code__German]                                                 = std::string("de_DE"); //4 - de_DE - Deutsch
   _languageISOCodeMap[T_e8_Language_Code__Greek]                                                  = std::string("el_EL"); //5 - el_GR - Greek
   _languageISOCodeMap[T_e8_Language_Code__Hebrew]                                                 = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__Hindi]                                                  = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__Hungarian]                                              = std::string("hu_HU"); //14 - hu_HU - Magyar
   _languageISOCodeMap[T_e8_Language_Code__Indonesian]                                             = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__Italian]                                                = std::string("it_IT");
   _languageISOCodeMap[T_e8_Language_Code__Japanese]                                               = std::string("ja_JA");
   _languageISOCodeMap[T_e8_Language_Code__Korean]                                                 = std::string("ko_KO");
   _languageISOCodeMap[T_e8_Language_Code__Latvian]                                                = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__Lithuanian]                                             = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__Malay]                                                  = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__Norwegian]                                              = std::string("no_NO"); //16 - no_NO - Norsk
   _languageISOCodeMap[T_e8_Language_Code__Persian_Farsi_Iranian]                                  = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__Philippines]                                            = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__Polish]                                                 = std::string("pl_PL"); //17 - pl_PL - Polski
   _languageISOCodeMap[T_e8_Language_Code__Portuguese]                                             = std::string("pt_PT"); //18 - pt_PT - PortuguÃªs
   _languageISOCodeMap[T_e8_Language_Code__Portuguese_Brazilian]                                   = std::string("pt_BR"); //19 - pt_BR - PortuguÃªs brasileiro
   _languageISOCodeMap[T_e8_Language_Code__Romanian]                                               = std::string("ro_RO"); //20 - ro_RO - Romana
   _languageISOCodeMap[T_e8_Language_Code__Russian]                                                = std::string("ru_RU");  //21 - ru_RU - Russian
   _languageISOCodeMap[T_e8_Language_Code__Serbian]                                                = std::string("sr_RS"); //24 - sr_RS - Serbian
   _languageISOCodeMap[T_e8_Language_Code__Slovakian]                                              = std::string("sk_SK"); //22 - sk_SK - Slovencina
   _languageISOCodeMap[T_e8_Language_Code__Slovenian]                                              = std::string("sl_SL"); //23 - sl_SL - Slovenscina
   _languageISOCodeMap[T_e8_Language_Code__Spanish]                                                = std::string("es_ES");
   _languageISOCodeMap[T_e8_Language_Code__Spanish_Latin_American]                                 = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__Spanish_Mexican]                                        = std::string("es_MX"); //3 - da_DK - Dansk (Danish - Denmark)
   _languageISOCodeMap[T_e8_Language_Code__Swedish]                                                = std::string("sv_SE"); //26 - sv_SE - Svenska
   _languageISOCodeMap[T_e8_Language_Code__Taiwanese]                                              = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__Thai]                                                   = std::string("th_TH");//7 - en_US - English US
   _languageISOCodeMap[T_e8_Language_Code__Turkish]                                                = std::string("tr_TR");//9 - es_MX - EspaÃ±ol mexicano
   _languageISOCodeMap[T_e8_Language_Code__Ukrainian]                                              = std::string("uk_UK");
   _languageISOCodeMap[T_e8_Language_Code__Vietnamese]                                             = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__UnSupported]                                            = std::string("xx_XX");
   _languageISOCodeMap[T_e8_Language_Code__Unused]                                                 = std::string("xx_XX");
}


#ifndef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1 //Feature only for Scope2

void VehicleDataHandler::vRegisterforUnitData(IUnitsDataHandler* client)
{
   std::vector< IUnitsDataHandler* >::const_iterator itr = std::find(_unitsDataHandlerClientCallback.begin(), \
         _unitsDataHandlerClientCallback.end(), client);
   if (itr == _unitsDataHandlerClientCallback.end()) //for safety
   {
      //add instance
      _unitsDataHandlerClientCallback.push_back(client);
      //client->currentUnitSettingsStatus(_UnitInfoTable);
   }
   else
   {
      //Info:User already registered the instance
   }
}


void VehicleDataHandler::sendUnitsUpdate()
{
   //Update status to all registered(external) client(s)
   std::vector<IUnitsDataHandler*>::iterator itUnitDataInst = _unitsDataHandlerClientCallback.begin();
   for (; itUnitDataInst != _unitsDataHandlerClientCallback.end(); ++itUnitDataInst)
   {
      (*itUnitDataInst)->currentUnitSettingsStatus(_UnitInfoTable);
   }
}


void VehicleDataHandler::onDistanceUnitStatus(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& proxy, const ::boost::shared_ptr< DistanceUnitStatus >& status)
{
   if (_vehicleProxy && (proxy == _vehicleProxy))
   {
      _UnitInfoTable.distanceUnit = status->getE8DistanceUnit();;
      sendUnitsUpdate();
   }
}


void VehicleDataHandler::onDistanceUnitError(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& /*proxy*/, const ::boost::shared_ptr< DistanceUnitError >& /*error*/)
{
}


void VehicleDataHandler::onFuelConsumptionUnitStatus(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& proxy, const ::boost::shared_ptr< FuelConsumptionUnitStatus >& status)
{
   if (_vehicleProxy && (proxy == _vehicleProxy))
   {
      _UnitInfoTable.fuelUnit = status->getE8FuelConsumpUnit();
      sendUnitsUpdate();
   }
}


void VehicleDataHandler::onFuelConsumptionUnitError(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& /*proxy*/, const ::boost::shared_ptr< FuelConsumptionUnitError >& /*error*/)
{
}


void VehicleDataHandler::onTemperatureUnitStatus(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& proxy, const ::boost::shared_ptr< TemperatureUnitStatus >& status)
{
   if (_vehicleProxy && (proxy == _vehicleProxy))
   {
      _UnitInfoTable.temperatureUnit = status->getE8TempUnit();;
      sendUnitsUpdate();
   }
}


void VehicleDataHandler::onTemperatureUnitError(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& /*proxy*/, const ::boost::shared_ptr< TemperatureUnitError >& /*error*/)
{
}


#endif


// Speed Lock Functions begin


/*
 * vRegisterforSpeedLock - This register function is invoked by all client files who needs to get the update for Speed Lock Status
 * @param[in] ISpeedLockState*
 * @param[out] None
 * @return void
 */
void VehicleDataHandler::vRegisterforSpeedLock(ISpeedLockState* client)
{
   vector< ISpeedLockState* >::const_iterator itr = find(_speedLockUpdateCB.begin(), \
         _speedLockUpdateCB.end(), client);
   if (itr == _speedLockUpdateCB.end()) //for safety
   {
      _speedLockUpdateCB.push_back(client);
   }
   else
   {
      ETG_TRACE_USR4(("Already a registered client for vRegisterforSpeedLock "));
   }
   sendSpeedLockUpdate();
}


/*
 * vRegisterforSpeedLock - This register function is invoked by all client files who needs to get the update for Speed Lock Status
 * @param[in] ISpeedLockState*
 * @param[out] None
 * @return void
 */
void VehicleDataHandler::vUnRegisterforSpeedLock(ISpeedLockState* client)
{
   vector<ISpeedLockState*>::iterator itr = _speedLockUpdateCB.begin();
   for (; itr != _speedLockUpdateCB.end(); ++itr)
   {
      if (client == *itr)
      {
         _speedLockUpdateCB.erase(itr);
         break;
      }
   }
}


/**
 * sendSpeedLockUpdate - To provide the state of the speed lock at start up
 * @param[in] :vehiclespeed
 * @param[out] :none
 * @return bool
 */
void VehicleDataHandler::sendSpeedLockUpdate()
{
   ETG_TRACE_USR4(("sendSpeedLockUpdate : Send the current speed lock at the start up "));
   if (_guiInitialized)
   {
      // To send the Speed lock status at start up for GUIStartUpfinished applications
      std::vector<ISpeedLockState*>::iterator itSpeedInst = _speedLockUpdateCB.begin();
      for (; itSpeedInst != _speedLockUpdateCB.end(); ++itSpeedInst)
      {
         (*itSpeedInst)->currentSpeedLockState(_speedLockState);
      }
   }
}


/**
 * checkSpeedLockState - To provide the state of the speed lock based on the threshold value
 * @param[in] :vehiclespeed
 * @param[out] :none
 * @return bool
 */
bool VehicleDataHandler:: checkSpeedLockState(uint32 vehiclespeed)
{
   uint32 thresholdSpeed = (_region == KDS_REGION_NAM) ? LOCKOUT_THRESHOLD_NAM : LOCKOUT_THRESHOLD_NON_NAM;
   // As provided in vehicle main fi "SpeedValue = Actual value * 100" Actual value = speedvalue received from VCAN
   if ((vehiclespeed / 100) > thresholdSpeed)
   {
      return SPEEDLOCK_ACTIVE;
   }
   else
   {
      return SPEEDLOCK_INACTIVE;
   }
}


/**
 * readRegionFromKDS - To read current language region from KDS
 * @param[in] none
 * @param[out] none
 * @return none
 */
void VehicleDataHandler::readRegionFromKDS()
{
   uint8 u8Region = 0xFF;
   _region = KDS_REGION_OTHER;

   if (DP_S32_NO_ERR != DP_s32GetConfigItem("VehicleInformation", "DestinationRegion1", &u8Region, 1))
   {
      ETG_TRACE_ERR(("VehicleDataHandler:readRegionFromKDS Error"));
   }
   else
   {
      if (u8Region == KDS_REGION_USA || u8Region == KDS_REGION_CAN || u8Region == KDS_REGION_MEX)
      {
         _region = KDS_REGION_NAM;
      }
   }
}


void VehicleDataHandler::onSpeedStatus(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& proxy, const ::boost::shared_ptr< SpeedStatus >& status)
{
   if (_vehicleProxy && (proxy == _vehicleProxy))
   {
      //If CAN is not ready or unavailable, speed value will be 65535 which is an invalid value. Ignore this value
      if (status->getSpeedValue() != SPEED_DEFAULTVALUE)
      {
         bool speedLockState = checkSpeedLockState(status->getSpeedValue());
         ETG_TRACE_USR4(("VehicleDataHandler::onSpeedStatus speed value : %d speedLockState:%d", status->getSpeedValue(), speedLockState));
         if (_speedLockState != speedLockState)
         {
            _speedLockState = speedLockState;
            vector<ISpeedLockState*>::iterator itr = _speedLockUpdateCB.begin();
            for (; (itr != _speedLockUpdateCB.end() && (*itr != NULL)); ++itr)
            {
               //Update status to all registered(external) client(s)
               (*itr)->currentSpeedLockState(_speedLockState);
            }
         }
      }
   }
}


void VehicleDataHandler::onSpeedError(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& /*proxy*/, const ::boost::shared_ptr< SpeedError >& /*error*/)
{
}


// Speed Lock Functions End


// Reverse Gear Functions begin

/*
 * vRegisterforReverseGear - This register function is invoked by all client files who needs to get the update for Reverse Gear Status
 * @param[in] IReverseGearState*
 * @param[out] None
 * @return void
 */
void VehicleDataHandler::vRegisterforReverseGear(IReverseGearState* client)
{
   vector< IReverseGearState* >::const_iterator itr = find(_reverseGearUpdateCB.begin(), \
         _reverseGearUpdateCB.end(), client);
   if (itr == _reverseGearUpdateCB.end()) //for safety
   {
      _reverseGearUpdateCB.push_back(client);
   }
   else
   {
      ETG_TRACE_USR4(("Already a registered client for vRegisterforReverseGear "));
   }
}


/*
 * vRegisterforReverseGear - This register function is invoked by all client files who needs to get the update for Reverse Gear Status
 * @param[in] IReverseGearState*
 * @param[out] None
 * @return void
 */
void VehicleDataHandler::vUnRegisterforReverseGear(IReverseGearState* client)
{
   vector<IReverseGearState*>::iterator itr = _reverseGearUpdateCB.begin();
   for (; itr != _reverseGearUpdateCB.end(); ++itr)
   {
      if (client == *itr)
      {
         _reverseGearUpdateCB.erase(itr);
         break;
      }
   }
}


void VehicleDataHandler::onReverseGearStatus(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& proxy, const ::boost::shared_ptr< ReverseGearStatus >& status)
{
   if (_vehicleProxy && (proxy == _vehicleProxy))
   {
      vector<IReverseGearState*>::iterator itr = _reverseGearUpdateCB.begin();
      for (; itr != _reverseGearUpdateCB.end(); ++itr)
      {
         //Update status to all registered(external) client(s)
         (*itr)->currentReverseGearState(status->getState());
      }
   }
}


void VehicleDataHandler::onReverseGearError(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ReverseGearError >& /*error*/)
{
}


// Reverse Gear Functions End


//  HandBrake Functions begin

/*
 * vRegisterforHandBrake - This register function is invoked by all client files who needs to get the update for Hand Brake Status
 * @param[in] IHandBrakeState*
 * @param[out] None
 * @return void
 */
void VehicleDataHandler::vRegisterforHandBrake(IHandBrakeState* client)
{
   vector< IHandBrakeState* >::const_iterator itr = find(_handBrakeUpdateCB.begin(), \
         _handBrakeUpdateCB.end(), client);
   if (itr == _handBrakeUpdateCB.end()) //for safety
   {
      _handBrakeUpdateCB.push_back(client);
   }
   else
   {
      ETG_TRACE_USR4(("Already a registered client for vRegisterforHandBrake "));
   }
}


/*
 * vUnRegisterforHandBrake - This register function is invoked by all client files who needs to get the update for Hand Brake Status
 * @param[in] IHandBrakeState*
 * @param[out] None
 * @return void
 */
void VehicleDataHandler::vUnRegisterforHandBrake(IHandBrakeState* client)
{
   vector<IHandBrakeState*>::iterator itr = _handBrakeUpdateCB.begin();
   for (; itr != _handBrakeUpdateCB.end(); ++itr)
   {
      if (client == *itr)
      {
         _handBrakeUpdateCB.erase(itr);
         break;
      }
   }
}


void VehicleDataHandler::onHandBrakeStatus(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& proxy, const ::boost::shared_ptr< HandBrakeStatus >& status)
{
   if (_vehicleProxy && (proxy == _vehicleProxy))
   {
      vector<IHandBrakeState*>::iterator itr = _handBrakeUpdateCB.begin();
      for (; itr != _handBrakeUpdateCB.end(); ++itr)
      {
         //Update status to all registered(external) client(s)
         (*itr)->currentHandBrakeState(status->getE8Status());
      }
   }
}


void VehicleDataHandler::onHandBrakeError(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& /*proxy*/, const ::boost::shared_ptr< HandBrakeError >& /*error*/)
{
}


//   HandBrake Functions End


//  SpeedLimit Functions begin


/*
 * vRegisterforSpeedLimit - This register function is invoked by all client files who needs to get the update for Speed Limit Status from external camera
 * using by AppHmi_Navigation
 * @param[in] ISpeedLimitState*
 * @param[out] None
 * @return void
 */
void VehicleDataHandler::vRegisterforSpeedLimit(ISpeedLimitState* client)
{
   vector< ISpeedLimitState* >::const_iterator itr = find(_speedLimitUpdateCB.begin(), \
         _speedLimitUpdateCB.end(), client);
   if (itr == _speedLimitUpdateCB.end()) //for safety
   {
      _speedLimitUpdateCB.push_back(client);
   }
   else
   {
      ETG_TRACE_USR4(("Already a registered client for vRegisterforSpeedLimit "));
   }
}


/*
 * vUnRegisterforSpeedLimit - This register function is invoked by all client files who needs to get the update for Speed Limit Status from external camera
 * using by AppHmi_Navigation
 * @param[in] ISpeedLimitState*
 * @param[out] None
 * @return void
 */
void VehicleDataHandler::vUnRegisterforSpeedLimit(ISpeedLimitState* client)
{
   vector< ISpeedLimitState* >::iterator itr = _speedLimitUpdateCB.begin();
   for (; itr != _speedLimitUpdateCB.end(); ++itr)
   {
      if (client == *itr)
      {
         _speedLimitUpdateCB.erase(itr);
         break;
      }
   }
}


void VehicleDataHandler::onSpeedLimitError(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& /*proxy*/, const ::boost::shared_ptr< SpeedLimitError >& /*error*/)
{
}


void VehicleDataHandler::onSpeedLimitStatus(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& proxy, const ::boost::shared_ptr< SpeedLimitStatus >& status)
{
   if (_vehicleProxy && (proxy == _vehicleProxy))
   {
      vector< ISpeedLimitState* >::iterator itr = _speedLimitUpdateCB.begin();
      for (; itr != _speedLimitUpdateCB.end(); ++itr)
      {
         // Update status to all registered(external) client(s) if CAN data valid
         if (status->getDisplay() != 255)
         {
            (*itr)->currentSpeedLimitState(status->getSpeedLimit1(), status->getSpeedLimit2(), status->getDisplay(), status->getDisplayUnit());
         }
      }
   }
}


//  SpeedLimit Functions End
} // namespace VehicleDataHandler
