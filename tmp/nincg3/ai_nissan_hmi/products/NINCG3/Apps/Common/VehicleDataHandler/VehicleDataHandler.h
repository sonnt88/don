/**
 * @file <VehicleDataHandler.h>
 * @author A-IVI HMI Team
 * @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
 * @addtogroup <AppHmi_Common>
 */


#ifndef VEHICLE_DATAHANDLER_H
#define VEHICLE_DATAHANDLER_H


#include "VEHICLE_MAIN_FIProxy.h"
#include "BaseContract/generated/BaseMsgs.h"
#include "dp_tclKdsCustomer.h"
#include "AppBase/ServiceAvailableIF.h"

#define UNUSED             0xFF
#define INVALID_LANGUAGE   0
#define SPEED_DEFAULTVALUE 65535

/***
 * Threshold Values for Speed Lock Feature provided in Km/h
 */
#define LOCKOUT_THRESHOLD_NON_NAM 5
#define LOCKOUT_THRESHOLD_NAM 8

#define KDS_REGION_NAM 0
#define KDS_REGION_OTHER 1

#define KDS_REGION_USA  0
#define KDS_REGION_CAN  1
#define KDS_REGION_MEX  2

#define SPEEDLOCK_INACTIVE 0
#define SPEEDLOCK_ACTIVE 1

namespace VehicleDataCommonHandler {

#ifndef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1 //Feature only for Scope2

struct UnitInfoTable
{
   uint8 distanceUnit;
   uint8 fuelUnit;
   uint8 temperatureUnit;

   UnitInfoTable(): distanceUnit(::vehicle_main_fi_types::T_e8_Vehicle_DistanceUnit__Unknown), fuelUnit(::vehicle_main_fi_types::T_e8_Vehicle_FuelConsumptionUnit__Unknown),
      temperatureUnit(::vehicle_main_fi_types::T_e8_Vehicle_TemperatureUnit__Unknown)
   {}
};


class IUnitsDataHandler
{
   public:
      virtual ~IUnitsDataHandler() {};
      virtual void currentUnitSettingsStatus(UnitInfoTable unitTable) = 0;
};


#endif


class iLanguageUpdate
{
   public:
      virtual ~iLanguageUpdate() {};
      //This function has to be overwritten in the derived class to set the default language on system startup/reboot
      virtual uint8 readLanguageOnStartUp() = 0;
      //This function has to be overwritten in the derived class to update the language index in the respective datapool
      virtual void writeLanguageintoDP(uint8 languageIndex) = 0;
      virtual void updateLanguageTable(std::vector<vehicle_main_fi_types::T_Language_SourceTable> languageSourceTable) {};
};


class IReverseGearState
{
   public:
      virtual ~IReverseGearState() {};
      //This function has to be overwritten in the derived class to get and handle the updated reverse Gear State
      virtual void currentReverseGearState(uint8 State) = 0;
};


class IHandBrakeState
{
   public:
      virtual ~IHandBrakeState() {};
      //This function has to be overwritten in the derived class to get and handle the updated Hand Brake State
      virtual void currentHandBrakeState(uint8 State) = 0;
};


class ISpeedLockState
{
   public:
      virtual ~ISpeedLockState() {};
      //This function has to be overwritten in the derived class to get and handle the updated Speed Lock State
      virtual void currentSpeedLockState(bool State) = 0;
};


class ISpeedLimitState
{
   public:
      virtual ~ISpeedLimitState() {};
      //This function has to be overwritten in the derived class to get and handle the updated Speed Limit State
      virtual void currentSpeedLimitState(uint8 speedLimit1, uint8 speedLimit2, uint8 display, uint8 displayUnit) = 0;
};


class VehicleDataHandler: public hmibase::ServiceAvailableIF,
   public StartupSync::PropertyRegistrationIF,
   public ::VEHICLE_MAIN_FI::LanguageCallbackIF,
   public ::VEHICLE_MAIN_FI::LanguageSyncSourceCallbackIF,
   public ::VEHICLE_MAIN_FI::SpeedCallbackIF,
   public ::VEHICLE_MAIN_FI::ReverseGearCallbackIF,
   public ::VEHICLE_MAIN_FI::HandBrakeCallbackIF,
   public ::VEHICLE_MAIN_FI::SpeedLimitCallbackIF

#ifndef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1 //Feature only for Scope2
   , public ::VEHICLE_MAIN_FI::DistanceUnitCallbackIF,
public ::VEHICLE_MAIN_FI::FuelConsumptionUnitCallbackIF,
public ::VEHICLE_MAIN_FI::TemperatureUnitCallbackIF
#endif

{
   public:
      /**
       *  Member Function Declaration
       *
       */

      VehicleDataHandler();
      virtual ~VehicleDataHandler();
      virtual void updateAutoOptionStatus(bool /*bEnable*/)
      {
      };

      virtual void updateLanguageIndextoLanguageList(uint8 /*langIndex*/)
      {
      };

      virtual void updateSyncSourceId(uint32 /*syncSrcId*/)
      {
      };
      virtual void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, \
                                      const asf::core::ServiceStateChange& stateChange);
      virtual void deregisterProperties(const boost::shared_ptr< asf::core::Proxy >& proxy, \
                                        const asf::core::ServiceStateChange& stateChange);

      void vRegisterforUpdate(iLanguageUpdate* client);
      void vUnRegisterforUpdate(iLanguageUpdate* client);

      void onLanguageStatus(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, \
                            const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::LanguageStatus >& status);
      void onLanguageError(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, \
                           const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::LanguageError >& error);

      void onLanguageSyncSourceError(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, \
                                     const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::LanguageSyncSourceError >& error);
      void onLanguageSyncSourceStatus(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, \
                                      const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::LanguageSyncSourceStatus >& status);

      void vRegisterforSpeedLock(ISpeedLockState* client);
      void vUnRegisterforSpeedLock(ISpeedLockState* client);

      void vRegisterforReverseGear(IReverseGearState* client);
      void vUnRegisterforReverseGear(IReverseGearState* client);

      void vRegisterforHandBrake(IHandBrakeState* client);
      void vUnRegisterforHandBrake(IHandBrakeState* client);

      void vRegisterforSpeedLimit(ISpeedLimitState* client);
      void vUnRegisterforSpeedLimit(ISpeedLimitState* client);

      void onSpeedStatus(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy,
                         const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::SpeedStatus >& status);

      void onSpeedError(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy,
                        const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::SpeedError >& error);

      void onReverseGearStatus(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy,
                               const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::ReverseGearStatus >& status);

      void onReverseGearError(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy,
                              const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::ReverseGearError >& error);

      void onHandBrakeStatus(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy,
                             const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::HandBrakeStatus >& status);

      void onHandBrakeError(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy,
                            const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::HandBrakeError >& error);

      void onSpeedLimitError(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy,
                             const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::SpeedLimitError >& error);

      void onSpeedLimitStatus(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy,
                              const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::SpeedLimitStatus >& status);

#ifndef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1 //Feature only for Scope2
      virtual void updateUnitStatus(UnitInfoTable /*unitTable*/)
      {
      };
      void vRegisterforUnitData(IUnitsDataHandler* client);
      void onDistanceUnitError(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, \
                               const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::DistanceUnitError >& error);

      void onDistanceUnitStatus(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, \
                                const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::DistanceUnitStatus >& status);

      void onFuelConsumptionUnitError(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, \
                                      const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::FuelConsumptionUnitError >& error);

      void onFuelConsumptionUnitStatus(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, \
                                       const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::FuelConsumptionUnitStatus >& status);

      void onTemperatureUnitError(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, \
                                  const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::TemperatureUnitError >& error);

      void onTemperatureUnitStatus(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, \
                                   const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::TemperatureUnitStatus >& status);
#endif

      void sendLanguageUpdateToGUI(uint8 languageindex);
      static VehicleDataHandler* GetInstance();

      bool findISOCodeByLanguageIndex(uint8 languageindex, std::string& matchingISOCode);

      bool onCourierMessage(const GuiStartupFinishedUpdMsg& msg);
      //register for GUI ready msg
      //GuiStartupFinishedUpdMsg has to be bind in all the hall component in order to reflect the language changes at the time of bootup/system start up
      COURIER_MSG_MAP_BEGIN(0)
      ON_COURIER_MESSAGE(GuiStartupFinishedUpdMsg)
      COURIER_MSG_MAP_END()

   protected:
      /**
       *  Member Variables Declaration
       */
      ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy> _vehicleProxy;

   private:
      /*
       *  Member Function Declaration
       */
      void initializeLanguageISOCodeMap();
      void sendLanguageUpdate(uint8 languageindex, std::vector<vehicle_main_fi_types::T_Language_SourceTable> languageSourceTable);
      void sendSpeedLockUpdate();
      void readRegionFromKDS();
      bool checkSpeedLockState(uint32);
      uint8 getLanguageIndexfromKDS();

      /**
       *  Member Variables Declaration
       */
      static VehicleDataHandler* _commonVehicleDataHandler;
      std::map< ::vehicle_main_fi_types::T_e8_Language_Code, std::string> _languageISOCodeMap;
      std::vector<iLanguageUpdate*> _languageUpdateCallback;

      uint8 _region;
      uint8 _languageIndex;
      bool _speedLockState;
      bool _guiInitialized;

      std::vector<ISpeedLockState*> _speedLockUpdateCB;
      std::vector<IReverseGearState*> _reverseGearUpdateCB;
      std::vector<IHandBrakeState*> _handBrakeUpdateCB;
      std::vector<ISpeedLimitState*> _speedLimitUpdateCB;

#ifndef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1 //Feature only for Scope2
      void sendUnitsUpdate();

      std::vector<IUnitsDataHandler*> _unitsDataHandlerClientCallback;
      UnitInfoTable _UnitInfoTable;
#endif
};


}// namespace VehicleDataHandler

#endif //VEHICLEDATAHANDLER_H
