#ifndef CGIAPPCONTROLLERPROJECT_H
#define CGIAPPCONTROLLERPROJECT_H

#include "HMIAppCtrl/Proxy/ProxyHandler.h"
#include "CgiExtensions/CGIAppControllerBase.h"
#include <BaseContract/generated/BaseMsgs.h>
#include "ProjectBaseMsgs.h"
#include "ProjectBaseTypes.h"

namespace hmibase {
namespace services {
namespace hmiappctrl {
class ProxyHandler;
}


}
}


class CGIAppControllerProject : public CGIAppControllerBase
{
   public:

      using CGIAppControllerBase::onCourierMessage;

      explicit CGIAppControllerProject(hmibase::services::hmiappctrl::ProxyHandler& proxyHandler): CGIAppControllerBase(proxyHandler)
      {
      }

      virtual ~CGIAppControllerProject() {};

      bool onCourierMessage(const ButtonReactionMsg& msg);
      bool onCourierMessage(const HKStatusChangedUpdMsg& msg);
      bool onCourierMessage(const RenderingCompleteMsg& msg);
      bool onCourierMessage(const UserInterfaceLockStateAppRes& msg);
      bool onCourierMessage(const SbCurrentStatusUpdMsg& msg);
      bool onCourierMessage(const SurfaceStateChangedUpdMsg& msg);
      COURIER_MSG_MAP_BEGIN(1)
      COURIER_DUMMY_CASE(0)
      ON_COURIER_MESSAGE(ButtonReactionMsg)
      ON_COURIER_MESSAGE(HKStatusChangedUpdMsg)
      ON_COURIER_MESSAGE(RenderingCompleteMsg)
      ON_COURIER_MESSAGE(SurfaceStateChangedUpdMsg)
      ON_COURIER_MESSAGE(SbCurrentStatusUpdMsg)
      COURIER_MSG_MAP_END_DELEGATE(CGIAppControllerBase)

   protected:
      virtual void lockStateApplicationUpdate(int applicationId, bool lockState);
};


#endif // CGIAPPCONTROLLERPROJECT_H
