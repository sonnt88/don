#include "gui_std_if.h"
#include "CGIAppControllerProject.h"
#include "ProjectBaseMsgs.h"
#include "ProjectBaseTypes.h"
#ifndef WIN32 //to exclude NavHMI WinSim Build
#include "ProjectSettings.h"
#endif

bool CGIAppControllerProject::onCourierMessage(const ButtonReactionMsg& msg)
{
   if (msg.GetEnReaction() == enPress)
   {
      POST_MSG((COURIER_MESSAGE_NEW(PlayBeepReqMsg)(hmibase::BEEPTYPE_CLICK)));
   }
   return false;
}


bool CGIAppControllerProject::onCourierMessage(const RenderingCompleteMsg& msg)
{
   bool isMsgConsumed = false;
   POST_MSG((COURIER_MESSAGE_NEW(ActiveRenderedView)(msg.GetSurfaceId(), msg.GetViewName())));
   POST_MSG((COURIER_MESSAGE_NEW(ActiveSceneRenderedMsg)(msg.GetSurfaceId(), msg.GetViewName())));
   isMsgConsumed = CGIAppControllerBase::onCourierMessage(msg);
   return isMsgConsumed;
}


bool CGIAppControllerProject::onCourierMessage(const SurfaceStateChangedUpdMsg& msg)
{
   POST_MSG((COURIER_MESSAGE_NEW(SurfaceStateChangedUpdAppsMsg)(msg.GetSurfaceId(), msg.GetState(), msg.GetViewId())));
   if (_hmiAppCtrlProxyHandler.isDirectApplicationActivationEnabled() == false)
   {
      // Work aound for dynamic rendering support
      if (((msg.GetState() == hmibase::SURFACESTATE_DEREGISTERED || (msg.GetState() == hmibase::SURFACESTATE_REGISTERED))
            || (msg.GetState() == hmibase::SURFACESTATE_VISIBLE || (msg.GetState() == hmibase::SURFACESTATE_INVISIBLE)))
            && (msg.GetSurfaceId() == ScreenBrokerClient::GetInstance().SurfaceId()))
      {
#if !defined(_MSC_VER)
         _hmiAppCtrlProxyHandler.getHMIAppCtrlProxy()->sendApplicationSwitchCompleteRequest(
            _hmiAppCtrlProxyHandler, (int) msg.GetSurfaceId(), (int) msg.GetState());
#endif
      }
   }
   else
   {
      if ((msg.GetState() == hmibase::SURFACESTATE_VISIBLE || msg.GetState() == hmibase::SURFACESTATE_INVISIBLE)
            && (msg.GetSurfaceId() == ScreenBrokerClient::GetInstance().SurfaceId()))
      {
#if !defined(_MSC_VER)
         _hmiAppCtrlProxyHandler.getHMIAppCtrlProxy()->sendApplicationSwitchCompleteRequest(
            _hmiAppCtrlProxyHandler, (int) msg.GetSurfaceId(), (int) msg.GetState());
#endif
      }
   }


//For Poupname - HmiInfo Service
   // SURFACESTATE_DEREGISTERED and SURFACESTATE_REGISTERED are checked- Work aound for dynamic rendering support
   if (((msg.GetState() == hmibase::SURFACESTATE_VISIBLE) || (msg.GetState() == hmibase::SURFACESTATE_INVISIBLE) || (msg.GetState() == hmibase::SURFACESTATE_DEREGISTERED) || (msg.GetState() == hmibase::SURFACESTATE_REGISTERED)) && \
         (((msg.GetSurfaceId()  >= SURFACEID_CENTER_POPUP_SURFACE_MASTER) && (msg.GetSurfaceId()  <= SURFACEID_CENTER_POPUP_SURFACE_SDS)) || \
          ((msg.GetSurfaceId()  >= SURFACEID_TOP_POPUP_SURFACE_MASTER) && (msg.GetSurfaceId()  <= SURFACEID_TOP_POPUP_SURFACE_SDS))))
   {
#ifndef WIN32 //to exclude NavHMI WinSim Build
      ScreenBrokerClient::GetInstance().RequestCurrentStatus(ScreenBrokerProtocol::CurrentStatusRequestId::TopActiveVisiblePopup);
#endif
   }

   return false;
}


bool CGIAppControllerProject::onCourierMessage(const SbCurrentStatusUpdMsg& msg)
{
   bool isMsgConsumed = false;
   POST_MSG((COURIER_MESSAGE_NEW(PopUpStatusUpdMsg)(msg.GetStatus(), msg.GetSurfaceId() , msg.GetViewId().CStr())));
   isMsgConsumed = CGIAppControllerBase::onCourierMessage(msg);
   return isMsgConsumed;
}


bool CGIAppControllerProject::onCourierMessage(const UserInterfaceLockStateAppRes& msg)
{
   lockStateApplicationUpdate(msg.GetApplicationId(), msg.GetLockState());
   return true;
}


bool CGIAppControllerProject::onCourierMessage(const HKStatusChangedUpdMsg& msg)
{
   if (msg.GetHKState() == hmibase::HARDKEYSTATE_DOWN)
   {
      switch (msg.GetHKCode())
      {
         case HARDKEYCODE_HK_PHONE:
         case HARDKEYCODE_HK_MEDIA:
         case HARDKEYCODE_HK_FM_AM:
         case HARDKEYCODE_HK_CD:
         case HARDKEYCODE_HK_AUX:
         case HARDKEYCODE_HK_SETTINGS:
         case HARDKEYCODE_HK_INFO:
         case HARDKEYCODE_HK_MENU:
         case HARDKEYCODE_HK_NAV:
         case HARDKEYCODE_HK_MAP:
         case HARDKEYCODE_HK_XMTUNER:
         case HARDKEYCODE_HK_AUDIO:
         {
            POST_MSG((COURIER_MESSAGE_NEW(HardKeyPressNotificationMsg)((Candera::UInt8)1, (Candera::UInt8)msg.GetHKCode(), (Candera::UInt8)msg.GetHKState())));
         }
         break;
         default :
            break;
      }
   }
   CGIAppControllerBase::onCourierMessage(msg);
   return false;
}


void CGIAppControllerProject::lockStateApplicationUpdate(int /*applicationId*/, bool /*lockState*/)
{
}
