/*
* NS_PHONESETTING::PhoneSettingProxy.h
*
*  Created on: Aug 31, 2015
*      Author: HMI Master
*/

#ifndef PHONESETTINGPROXY_H_
#define PHONESETTINGPROXY_H_

#include "bosch/cm/ai/nissan/hmi/phonesetting/PhoneSettingClientBase.h"
#include "IPhoneSettingCtrlProxyImpl.h"
#define NS_PHONESETTING bosch::cm::ai::nissan::hmi::phonesetting::PhoneSetting

class PhoneSettingCtrlProxy
	: public asf::core::ServiceAvailableIF
	, public NS_PHONESETTING::PhoneSettingClientBase
{

public:
	PhoneSettingCtrlProxy();
	virtual ~PhoneSettingCtrlProxy();

	// ServiceAvailableIF
	virtual void onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange);
	virtual void onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange);

	// Callback 'CallVolumeSettingStatusCallbackIF'

	virtual void onCallVolumeSettingStatusError(const ::boost::shared_ptr< NS_PHONESETTING::PhoneSettingProxy >& /*proxy*/, const ::boost::shared_ptr< NS_PHONESETTING::CallVolumeSettingStatusError >& /*error*/) {}

	virtual void onCallVolumeSettingStatusSignal(const ::boost::shared_ptr< NS_PHONESETTING::PhoneSettingProxy >& proxy, const ::boost::shared_ptr< NS_PHONESETTING::CallVolumeSettingStatusSignal >& signal);

	// Callback 'DeRegisterAllPhoneSettingCallbackIF'

	virtual void onDeRegisterAllPhoneSettingError(const ::boost::shared_ptr< NS_PHONESETTING::PhoneSettingProxy >& /*proxy*/, const ::boost::shared_ptr< NS_PHONESETTING::DeRegisterAllPhoneSettingError >& /*error*/)  {}

	virtual void onDeRegisterAllPhoneSettingResponse(const ::boost::shared_ptr< NS_PHONESETTING::PhoneSettingProxy >& proxy, const ::boost::shared_ptr< NS_PHONESETTING::DeRegisterAllPhoneSettingResponse >& response);

	// Callback 'DeRegisterPhoneSettingUpdateCallbackIF'

	virtual void onDeRegisterPhoneSettingUpdateError(const ::boost::shared_ptr< NS_PHONESETTING::PhoneSettingProxy >& /*proxy*/, const ::boost::shared_ptr< NS_PHONESETTING::DeRegisterPhoneSettingUpdateError >& /*error*/)  {}

	virtual void onDeRegisterPhoneSettingUpdateResponse(const ::boost::shared_ptr< NS_PHONESETTING::PhoneSettingProxy >& proxy, const ::boost::shared_ptr< NS_PHONESETTING::DeRegisterPhoneSettingUpdateResponse >& response);

	// Callback 'RegisterPhoneSettingUpdateCallbackIF'

	virtual void onRegisterPhoneSettingUpdateError(const ::boost::shared_ptr< NS_PHONESETTING::PhoneSettingProxy >& /*proxy*/, const ::boost::shared_ptr< NS_PHONESETTING::RegisterPhoneSettingUpdateError >& /*error*/)  {}

	virtual void onRegisterPhoneSettingUpdateResponse(const ::boost::shared_ptr< NS_PHONESETTING::PhoneSettingProxy >& proxy, const ::boost::shared_ptr< NS_PHONESETTING::RegisterPhoneSettingUpdateResponse >& response);

	// Callback 'SetPhoneCallVolumeCallbackIF'

	virtual void onSetPhoneCallVolumeError(const ::boost::shared_ptr< NS_PHONESETTING::PhoneSettingProxy >& /*proxy*/, const ::boost::shared_ptr< NS_PHONESETTING::SetPhoneCallVolumeError >& /*error*/)  {}

	virtual void onSetPhoneCallVolumeResponse(const ::boost::shared_ptr< NS_PHONESETTING::PhoneSettingProxy >& proxy, const ::boost::shared_ptr< NS_PHONESETTING::SetPhoneCallVolumeResponse >& response);

	bool PhoneSettingUpdateRegistrationReq(uint32 applicationId, int FunctionId);
	bool PhoneSettingUpdateDeregistrationReq(uint32 applicationId, int FunctionId);
	bool PhoneSettingUpdateDeRegistrationAllReq(uint32 applicationId);
	bool setPhoneCallVolumeReq(uint32 applicationId, int volumeLevel);

	inline void setPhoneSettingProxyImpl(IPhoneSettingProxyImpl* pIPhoneSettingProxyImpl)
	{
		_pIPhoneSettingProxyImpl = pIPhoneSettingProxyImpl;
	}

private:
	::boost::shared_ptr< NS_PHONESETTING::PhoneSettingProxy > _phoneSettingProxy;
	IPhoneSettingProxyImpl* _pIPhoneSettingProxyImpl;

};

#undef NS_PHONESETTING
#endif /* PHONESETTINGPROXY_H_ */
