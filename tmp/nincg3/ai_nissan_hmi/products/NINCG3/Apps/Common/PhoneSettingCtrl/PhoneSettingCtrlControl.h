/*
 * PhoneSettingCtrlControl.h
 *
 *  Created on: Dec 15, 2015
 *      Author: Rachana Sahay
 */

#ifndef PhoneSettingCONTROL_H_
#define PhoneSettingCONTROL_H_

#include "IPhoneSettingCtrlProxyImpl.h"

class PhoneSettingCtrlProxy;
class IPhoneSettingObserver;

class PhoneSettingCtrlControl: public IPhoneSettingProxyImpl
{
   public:
      PhoneSettingCtrlControl();
      virtual ~PhoneSettingCtrlControl();
      static PhoneSettingCtrlControl* getInstance();

      virtual void onServiceAvailable(const asf::core::ServiceStateChange& stateChange);
      virtual void onServiceUnavailable(const asf::core::ServiceStateChange& stateChange);

	  void registerPhoneSettingUpdate(IPhoneSettingObserver* pIPhoneSettingObserver);
      void deregisterPhoneSettingUpdate(IPhoneSettingObserver* pIPhoneSettingObserver);

      bool PhoneSettingUpdateRegistrationReq(uint32 applicationId, int FunctionId);
	  virtual void PhoneSettingUpdateRegistrationRes(int reponse);

      bool PhoneSettingUpdateDeregistrationReq(uint32 applicationId, int FunctionId);
	  virtual void allPhoneSettingUpdateDeRegistrationRes(int reponse);

      bool PhoneSettingUpdateDeRegistrationAllReq(uint32 applicationId);
	  virtual void PhoneSettingUpdateDeRegistrationAllRes(int response);

	  virtual bool setPhoneCallVolume(uint32 applicationId, int volumeLevel);
	  virtual void setPhoneCallVolumeRes(int reponse); 

	  virtual void NotifyPhoneCallVolumeUpdate(uint32 applicationId, int volumeLevel);

	  //void phoneCallVolumeUpdate(int applicationId, int volumeLevel) {}	  

   private:
      PhoneSettingCtrlProxy* _pPhoneSettingCtrlProxy;
	  ::std::vector<IPhoneSettingObserver*> _ObserverList;
};


#endif /* PhoneSettingCONTROL_H_ */
