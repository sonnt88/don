/*
* PhoneSettingCtrlProxy.cpp
*
*  Created on: Sep 4, 2015
*      Author: goc1kor
*/

#include "hmi_trace_if.h"
#include "../Common_Trace.h"
#include "PhoneSettingCtrlProxy.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_PHONESETTINGCTRL
#include "trcGenProj/Header/PhoneSettingCtrlProxy.cpp.trc.h"
#endif


using namespace bosch::cm::ai::nissan::hmi::phonesetting::PhoneSetting;

PhoneSettingCtrlProxy::PhoneSettingCtrlProxy() 
	: _phoneSettingProxy(PhoneSettingProxy::createProxy("phoneSettingPort", *this))
	, _pIPhoneSettingProxyImpl(NULL)
{
	ETG_TRACE_USR4(("PhoneSettingCtrlProxy object created"));
}

PhoneSettingCtrlProxy::~PhoneSettingCtrlProxy()
{
	ETG_TRACE_USR4(("PhoneSettingCtrlProxy destroyed"));
	_phoneSettingProxy.reset();

	delete _pIPhoneSettingProxyImpl;
	_pIPhoneSettingProxyImpl = NULL;
}


void PhoneSettingCtrlProxy::onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
	ETG_TRACE_USR4(("PhoneSettingCtrlProxy onAvailable entered"));
	if (_phoneSettingProxy == proxy)
	{
		_phoneSettingProxy->sendCallVolumeSettingStatusRegister(*this);
	}
	else
	{
		ETG_TRACE_USR4(("PhoneSettingCtrlProxy WARNING!! onAvailable proxy incorrect"));
	}
}
void PhoneSettingCtrlProxy::onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
	ETG_TRACE_USR4(("PhoneSettingCtrlProxy onUnavailable entered"));
	if (_phoneSettingProxy == proxy)
	{
		_phoneSettingProxy->sendCallVolumeSettingStatusDeregisterAll();
	}
}

void PhoneSettingCtrlProxy::onSetPhoneCallVolumeResponse(const ::boost::shared_ptr< PhoneSettingProxy >& /*proxy*/, const ::boost::shared_ptr< SetPhoneCallVolumeResponse >& response)
{
	ETG_TRACE_USR4(("onSetPhoneCallVolumeResponse entered"));
	if (_pIPhoneSettingProxyImpl)
		_pIPhoneSettingProxyImpl->setPhoneCallVolumeRes(response->getSuccess());
}

void PhoneSettingCtrlProxy::onRegisterPhoneSettingUpdateResponse(const ::boost::shared_ptr< PhoneSettingProxy >& /*proxy*/, const ::boost::shared_ptr< RegisterPhoneSettingUpdateResponse >& /*response*/)
{
	ETG_TRACE_USR4(("onRegisterPhoneSettingUpdateResponse entered"));
}

void PhoneSettingCtrlProxy::onDeRegisterPhoneSettingUpdateResponse(const ::boost::shared_ptr< PhoneSettingProxy >& /*proxy*/, const ::boost::shared_ptr< DeRegisterPhoneSettingUpdateResponse >& /*response*/)
{
	ETG_TRACE_USR4(("onDeRegisterPhoneSettingUpdateResponse entered"));
}

void PhoneSettingCtrlProxy::onDeRegisterAllPhoneSettingResponse(const ::boost::shared_ptr< PhoneSettingProxy >& /*proxy*/, const ::boost::shared_ptr< DeRegisterAllPhoneSettingResponse >& /*response*/)
{
	ETG_TRACE_USR4(("onDeRegisterAllPhoneSettingResponse entered"));
}

void PhoneSettingCtrlProxy::onCallVolumeSettingStatusSignal(const ::boost::shared_ptr< PhoneSettingProxy >& /*proxy*/, const ::boost::shared_ptr< CallVolumeSettingStatusSignal >& signal)
{
	ETG_TRACE_USR4(("onCallVolumeSettingStatusSignal entered"));
	if (_pIPhoneSettingProxyImpl)
		_pIPhoneSettingProxyImpl->NotifyPhoneCallVolumeUpdate(signal->getApplicationId(), signal->getVolumeLevel());
}

bool PhoneSettingCtrlProxy::PhoneSettingUpdateRegistrationReq(uint32 applicationId, int FunctionId)
{
	ETG_TRACE_USR4(("PhoneSettingUpdateRegistrationReq entered"));
	if (_phoneSettingProxy->isAvailable())
	{
		_phoneSettingProxy->sendRegisterPhoneSettingUpdateRequest(*this, applicationId, FunctionId);
	}

	return true;
}

bool PhoneSettingCtrlProxy::PhoneSettingUpdateDeregistrationReq(uint32 applicationId, int FunctionId)
{
	ETG_TRACE_USR4(("PhoneSettingUpdateDeregistrationReq entered"));
	if (_phoneSettingProxy->isAvailable())
	{
		_phoneSettingProxy->sendDeRegisterPhoneSettingUpdateRequest(*this, applicationId, FunctionId);
	}

	return true;
}

bool PhoneSettingCtrlProxy::PhoneSettingUpdateDeRegistrationAllReq(uint32 applicationId)
{
	ETG_TRACE_USR4(("PhoneSettingUpdateDeRegistrationAllReq entered"));
	if (_phoneSettingProxy->isAvailable())
	{
		_phoneSettingProxy->sendDeRegisterAllPhoneSettingRequest(*this, applicationId);
	}
	else
	{
		ETG_TRACE_USR4(("WARNING!! PhoneSettingUpdateDeRegistrationAllReq _phoneSettingProxy->isAvailable()"));
	}

	return true;
}

bool PhoneSettingCtrlProxy::setPhoneCallVolumeReq(uint32 applicationId, int volumeLevel)
{
	ETG_TRACE_USR4(("setPhoneCallVolumeReq entered"));
	if (_phoneSettingProxy->isAvailable())
	{
		_phoneSettingProxy->sendSetPhoneCallVolumeRequest(*this, applicationId, volumeLevel);
	}
	else
	{
		ETG_TRACE_USR1(("WARNING!! setPhoneCallVolumeReq ServiceState %u", _phoneSettingProxy->getServiceState()));
	}

	return true;
}
