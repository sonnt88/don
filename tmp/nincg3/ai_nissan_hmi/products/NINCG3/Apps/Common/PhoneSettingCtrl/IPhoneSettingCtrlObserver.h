/*
 * IPhoneSettingObserver.h
 *
 *  Created on: Sep 4, 2015
 *      Author: goc1kor
 */

#ifndef IPHONESETTINGOBSERVER_H_
#define IPHONESETTINGOBSERVER_H_

class IPhoneSettingObserver
{
   public:
      virtual ~IPhoneSettingObserver() {}

      virtual void phoneCallVolumeUpdate(uint32 applicationId, int volumeLevel) = 0;
      virtual void onPhoneSettingCtrlServiceAvailable() = 0;
      virtual void onPhoneSettingCtrlServiceUnavailable() = 0;
};


#endif /* IPHONESETTINGOBSERVER_H_ */
