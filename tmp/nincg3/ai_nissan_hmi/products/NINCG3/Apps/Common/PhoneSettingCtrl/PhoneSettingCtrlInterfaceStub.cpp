/*
 * PhoneSettingCtrlInterfaceStub.cpp
 *
 *  Created on: Sep 3, 2015
 *      Author: goc1kor
 */

#include "hmi_trace_if.h"
#include "../Common_Trace.h"
#include "ProjectBaseTypes.h"
#include "PhoneSettingCtrlInterfaceStub.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_PHONESETTINGCTRL
#include "trcGenProj/Header/PhoneSettingCtrlInterfaceStub.cpp.trc.h"
#endif

using namespace bosch::cm::ai::nissan::hmi::phonesetting::PhoneSetting;

PhoneSettingCtrlInterfaceStub::PhoneSettingCtrlInterfaceStub(): PhoneSettingStub("phoneSettingServerPort")
{
	// no registrations done yet
	_mapFunctionIdVolLevel.clear();

	// all the setting volume levels are 0 by default
	for (uint32 u32FctId = NS_PHONESETTING::enFunctionID__PHONESETTING_FCTID_CALL_VOLUME; u32FctId < NS_PHONESETTING::enFunctionID__PHONESETTING_FCT_ID_MAX; u32FctId++)
	{
		_mapFunctionIdVolLevel[u32FctId] = 0;
	}	
}


PhoneSettingCtrlInterfaceStub::~PhoneSettingCtrlInterfaceStub()
{
	_mapFunctionIdVolLevel.clear();
}

void PhoneSettingCtrlInterfaceStub::onRegisterPhoneSettingUpdateRequest(const ::boost::shared_ptr< NS_PHONESETTING::RegisterPhoneSettingUpdateRequest >& request)
{
	bool bRes = false;
	uint32 appId = request->getApplicationId();
	uint32 FctId = request->getFunctionId();

	ETG_TRACE_USR1(("onRegisterPhoneSettingUpdateRequest"));
	const regList oTempRegList(appId, FctId);

	::std::vector<regList>::iterator it = ::std::find(_registrationList.begin(), _registrationList.end(), oTempRegList);
	if ( it == _registrationList.end() ) 
	{
		// if not found add the registration entry to the vector
		_registrationList.push_back(oTempRegList);
		bRes = true;
	}
	
	// send the response
	sendRegisterPhoneSettingUpdateResponse(bRes);
	
	// send the first notification after registration successful
	if ( bRes)
		sendCallVolumeSettingStatusSignal(appId, _mapFunctionIdVolLevel[FctId]);
}

void PhoneSettingCtrlInterfaceStub::onDeRegisterPhoneSettingUpdateRequest (const ::boost::shared_ptr< NS_PHONESETTING::DeRegisterPhoneSettingUpdateRequest >& request)
{
	ETG_TRACE_USR1(("onDeRegisterPhoneSettingUpdateRequest"));
	bool bRes = false;
	uint32 appId = request->getApplicationId();
	uint32 FctId = request->getFunctionId();

	regList oTempRegList(appId, FctId);

	::std::vector<regList>::iterator it = ::std::find(_registrationList.begin(), _registrationList.end(), oTempRegList);
	if ( it != _registrationList.end() ) 
	{
		_registrationList.erase(it);
		bRes = true;
	}
	
	// send the response
	sendDeRegisterPhoneSettingUpdateResponse(bRes);
}

void PhoneSettingCtrlInterfaceStub::onDeRegisterAllPhoneSettingRequest (const ::boost::shared_ptr< NS_PHONESETTING::DeRegisterAllPhoneSettingRequest >& request)
{
	ETG_TRACE_USR1(("onDeRegisterAllPhoneSettingRequest"));
	bool bRes = false;
	uint32 appId = request->getApplicationId();

	::std::vector<regList>::iterator it = _registrationList.begin();
	for ( ; it != _registrationList.end(); it++ )
	{
		if ( it->_applicationId == appId )
		{
			_registrationList.erase(it);
			bRes = true;
			break;
		}
	}
	// send the response
	sendDeRegisterAllPhoneSettingResponse(bRes);
}

void PhoneSettingCtrlInterfaceStub::onSetPhoneCallVolumeRequest (const ::boost::shared_ptr< NS_PHONESETTING::SetPhoneCallVolumeRequest >& request)
{
	ETG_TRACE_USR1(("onSetPhoneCallVolumeRequest entered"));
	ETG_TRACE_USR1(("onSetPhoneCallVolumeRequest appId %u volumelevel %d", request->getApplicationId(), request->getVolumeLevel()));
	uint32 appId = request->getApplicationId();
	uint32 FctId = NS_PHONESETTING::enFunctionID__PHONESETTING_FCTID_CALL_VOLUME;
	int volumeLevel = request->getVolumeLevel();

	ETG_TRACE_USR1(("onSetPhoneCallVolumeRequest map-size %u", _mapFunctionIdVolLevel.size()));

	::std::map<uint32, int>::iterator it = _mapFunctionIdVolLevel.begin();

	for (; it != _mapFunctionIdVolLevel.end(); it++ )
	{
		if ( (it->first == FctId) && (it->second != volumeLevel))
		{
			it->second = volumeLevel;
			// update the new volume level to the clients
			sendCallVolumeSettingStatusSignal(appId, volumeLevel);
		}
	}
}

