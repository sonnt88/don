/*
 * PhonesettingStub.h
 *
 *  Created on: Aug 31, 2015
 *      Author: HMI Master
 */

#ifndef PHONESETTINGSTUB_H_
#define PHONESETTINGSTUB_H_

#include "bosch/cm/ai/nissan/hmi/phonesetting/PhoneSettingStub.h"
#include "bosch/cm/ai/nissan/hmi/phonesetting/PhoneSettingConst.h"
#define NS_PHONESETTING bosch::cm::ai::nissan::hmi::phonesetting::PhoneSetting

#define SYSTEM_S_IMPORT_INTERFACE_ALGORITHM
#include "stl_pif.h"

class PhoneSettingCtrlInterfaceStub: public NS_PHONESETTING::PhoneSettingStub
{
public:
	PhoneSettingCtrlInterfaceStub();
	virtual ~PhoneSettingCtrlInterfaceStub();

	virtual void onRegisterPhoneSettingUpdateRequest(const ::boost::shared_ptr< NS_PHONESETTING::RegisterPhoneSettingUpdateRequest >& request);
	virtual void onDeRegisterPhoneSettingUpdateRequest (const ::boost::shared_ptr< NS_PHONESETTING::DeRegisterPhoneSettingUpdateRequest >& request);
	virtual void onDeRegisterAllPhoneSettingRequest (const ::boost::shared_ptr< NS_PHONESETTING::DeRegisterAllPhoneSettingRequest >& request);     
	virtual void onSetPhoneCallVolumeRequest (const ::boost::shared_ptr< NS_PHONESETTING::SetPhoneCallVolumeRequest >& request);

private:
	struct regList
	{
		uint32 _applicationId;
		uint32 _FunctionId;

		regList() // default constructor
			: _applicationId(APPID_APPHMI_UNKNOWN)
			, _FunctionId(NS_PHONESETTING::enFunctionID__PHONESETTING_FCTID_UNKNOWN)
		{
		}

		regList(uint32 applicationId, uint32 FunctionId)
			: _applicationId(applicationId)
			, _FunctionId(FunctionId)
		{
		}

		bool operator== (const regList& oList) const
		{
			return ((oList._applicationId == _applicationId) && (oList._FunctionId == _FunctionId));
		}
	};

	::std::map<uint32, int> _mapFunctionIdVolLevel;
	::std::vector<regList> _registrationList;
};


#endif /* PHONESETTINGSTUB_H_ */
