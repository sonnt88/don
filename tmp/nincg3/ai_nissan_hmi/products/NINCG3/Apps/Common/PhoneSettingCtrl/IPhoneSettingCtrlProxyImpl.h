/*
 * IPhoneSettingProxyImpl.h
 *
 *  Created on: Sep 6, 2015
 *      Author: goc1kor
 */

#ifndef IPHONESETTINGPROXYIMPL_H_
#define IPHONESETTINGPROXYIMPL_H_

class IPhoneSettingProxyImpl
{
   public:
      virtual ~IPhoneSettingProxyImpl() {}

      virtual void onServiceAvailable(const asf::core::ServiceStateChange& stateChange) = 0;
      virtual void onServiceUnavailable(const asf::core::ServiceStateChange& stateChange) = 0;

	  virtual bool setPhoneCallVolume(uint32 applicationId, int volumeLevel) = 0;
	  virtual void setPhoneCallVolumeRes(int reponse) = 0;
	  virtual void PhoneSettingUpdateDeRegistrationAllRes(int response) = 0;
	  virtual void allPhoneSettingUpdateDeRegistrationRes(int reponse) = 0;
	  virtual void PhoneSettingUpdateRegistrationRes(int reponse) = 0;
	  virtual void NotifyPhoneCallVolumeUpdate(uint32 applicationId, int volumeLevel) = 0;
};


#endif /* IPHONESETTINGPROXYIMPL_H_ */
