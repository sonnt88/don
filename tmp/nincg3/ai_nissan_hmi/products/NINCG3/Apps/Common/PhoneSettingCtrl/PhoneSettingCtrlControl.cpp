/*
* PhoneSettingCtrlControl.cpp
*
*  Created on: Sep 7, 2015
*      Author: goc1kor
*/

#include "ProjectBaseTypes.h"
#include "PhoneSettingCtrlProxy.h"
#include "PhoneSettingCtrlControl.h"
#include "IPhoneSettingCtrlObserver.h"
#include "hmi_trace_if.h"
#include "../Common_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_PHONESETTINGCTRL
#include "trcGenProj/Header/PhoneSettingCtrlControl.cpp.trc.h"
#endif

static PhoneSettingCtrlControl* _pInterfaceControl = NULL;

PhoneSettingCtrlControl::PhoneSettingCtrlControl()
{
	_pPhoneSettingCtrlProxy = new PhoneSettingCtrlProxy();
	if (_pPhoneSettingCtrlProxy != NULL)
		_pPhoneSettingCtrlProxy->setPhoneSettingProxyImpl(this);
	_pInterfaceControl = this;
	_ObserverList.clear();
}


PhoneSettingCtrlControl::~PhoneSettingCtrlControl()
{
	if (_pPhoneSettingCtrlProxy != NULL)
	{
		delete _pPhoneSettingCtrlProxy;
		_pPhoneSettingCtrlProxy = NULL;
	}
	_pInterfaceControl = NULL;
}


PhoneSettingCtrlControl* PhoneSettingCtrlControl::getInstance(void)
{
	return _pInterfaceControl;
}


void PhoneSettingCtrlControl::onServiceAvailable(const asf::core::ServiceStateChange& /*stateChange*/)
{
	ETG_TRACE_ERR(("PhoneSetting Control: NotifyPhoneCallVolumeUpdate _ObserverList.size %u", _ObserverList.size()));
	::std::vector<IPhoneSettingObserver*> List = _ObserverList;
	::std::vector<IPhoneSettingObserver*>::const_iterator itr = List.begin();
	ETG_TRACE_ERR(("PhoneSetting Control: NotifyPhoneCallVolumeUpdate List.size %u", List.size()));
	for ( ; itr != List.end(); itr++)
	{
		(*itr)->onPhoneSettingCtrlServiceAvailable();
	}
}
void PhoneSettingCtrlControl::onServiceUnavailable(const asf::core::ServiceStateChange& /*stateChange*/)
{
	ETG_TRACE_ERR(("PhoneSetting Control: NotifyPhoneCallVolumeUpdate _ObserverList.size %u", _ObserverList.size()));
	::std::vector<IPhoneSettingObserver*> List = _ObserverList;
	::std::vector<IPhoneSettingObserver*>::const_iterator itr = List.begin();
	ETG_TRACE_ERR(("PhoneSetting Control: NotifyPhoneCallVolumeUpdate List.size %u", List.size()));
	for ( ; itr != List.end(); itr++)
	{
		(*itr)->onPhoneSettingCtrlServiceUnavailable();
	}
}

void PhoneSettingCtrlControl::registerPhoneSettingUpdate(IPhoneSettingObserver* pIPhoneSettingObserver)
{
	::std::vector<IPhoneSettingObserver*>::iterator itr = ::std::find(_ObserverList.begin(), _ObserverList.end(), pIPhoneSettingObserver);
	ETG_TRACE_ERR(("PhoneSetting Control:   registerPhoneSettingUpdate called"));
	if (pIPhoneSettingObserver != NULL && itr == _ObserverList.end())
	{
		ETG_TRACE_ERR(("PhoneSetting Control: _ObserverList updated"));
		_ObserverList.push_back(pIPhoneSettingObserver);
	}
}

void PhoneSettingCtrlControl::deregisterPhoneSettingUpdate(IPhoneSettingObserver* pIPhoneSettingObserver)
{
	::std::vector<IPhoneSettingObserver*>::iterator itr = ::std::find(_ObserverList.begin(), _ObserverList.end(), pIPhoneSettingObserver);
	if (itr != _ObserverList.end())
	{
		_ObserverList.erase(itr);
	}
}

bool PhoneSettingCtrlControl::PhoneSettingUpdateRegistrationReq(uint32 applicationId, int FunctionId)
{
	bool bRet = false;

	if ( _pPhoneSettingCtrlProxy != NULL && applicationId < APPID_APPHMI_UNKNOWN)
	{
		bRet = _pPhoneSettingCtrlProxy->PhoneSettingUpdateRegistrationReq(applicationId, FunctionId);
	}

	return bRet;
}

void PhoneSettingCtrlControl::PhoneSettingUpdateRegistrationRes(int response)
{
	ETG_TRACE_USR1(("PhoneSettingRegistrationRes response %d", response));
	(void) response;
}

bool PhoneSettingCtrlControl::PhoneSettingUpdateDeregistrationReq(uint32 applicationId, int FunctionId)
{
	bool bRet = false;

	if ( _pPhoneSettingCtrlProxy != NULL && applicationId < APPID_APPHMI_UNKNOWN )
	{
		bRet = _pPhoneSettingCtrlProxy->PhoneSettingUpdateDeregistrationReq(applicationId, FunctionId);		
	}

	return bRet;
}

void PhoneSettingCtrlControl::allPhoneSettingUpdateDeRegistrationRes(int response)
{
	ETG_TRACE_USR1(("allPhoneSettingDeRegistrationRes response %d", response));
	(void) response;
}

bool PhoneSettingCtrlControl::PhoneSettingUpdateDeRegistrationAllReq(uint32 applicationId)
{
	bool bRet = false;

	if ( _pPhoneSettingCtrlProxy != NULL && applicationId < APPID_APPHMI_UNKNOWN)
	{
		bRet = _pPhoneSettingCtrlProxy->PhoneSettingUpdateDeRegistrationAllReq(applicationId);
	}

	return bRet;
}

void PhoneSettingCtrlControl::PhoneSettingUpdateDeRegistrationAllRes(int response)
{
	ETG_TRACE_USR1(("PhoneSettingDeRegistrationAllRes response %d", response));
	(void) response;
}

bool PhoneSettingCtrlControl::setPhoneCallVolume(uint32 applicationId, int volumeLevel)
{
	bool bRet = false;
	ETG_TRACE_USR1(("setPhoneCallVolume applicationId: %u volumeLevel: %d", applicationId, volumeLevel));

	if ( _pPhoneSettingCtrlProxy != NULL && applicationId < APPID_APPHMI_UNKNOWN)
	{
		bRet = _pPhoneSettingCtrlProxy->setPhoneCallVolumeReq(applicationId, volumeLevel);
	}

	return bRet;
}

void PhoneSettingCtrlControl::setPhoneCallVolumeRes(int response)
{
	ETG_TRACE_USR1(("setPhoneCallVolumeRes response %d", response));
	(void) response;
}

void PhoneSettingCtrlControl::NotifyPhoneCallVolumeUpdate(uint32 applicationId, int volumeLevel)
{
	ETG_TRACE_ERR(("PhoneSetting Control: NotifyPhoneCallVolumeUpdate applicationId %u volumeLevel %d", applicationId, volumeLevel));
	ETG_TRACE_ERR(("PhoneSetting Control: NotifyPhoneCallVolumeUpdate _ObserverList.size %u", _ObserverList.size()));
	::std::vector<IPhoneSettingObserver*> List = _ObserverList;
	::std::vector<IPhoneSettingObserver*>::const_iterator itr = List.begin();
	ETG_TRACE_ERR(("PhoneSetting Control: NotifyPhoneCallVolumeUpdate List.size %u", List.size()));
	for ( ; itr != List.end(); itr++)
	{
		(*itr)->phoneCallVolumeUpdate(applicationId, volumeLevel);
	}
}
