#ifndef KEYMAPPING_H
#define KEYMAPPING_H

#include "AppBase/ScreenBrokerClient/KeyMappingBase.h"
#include <map>

class KeyMapping : public KeyMappingBase
{
   public:
      ///
      KeyMapping();

      ///
      uint32_t GetHmiKeyCode(uint32_t key, uint32_t userData) const;

      ///
      uint32_t GetOrigKeyCode(uint32_t key, uint32_t userData) const;

      ///
      uint32_t GetHmiEncoderCode(uint32_t encoder, uint32_t userData) const;

      ///
      uint32_t GetOrigEncoderCode(uint32_t encoder, uint32_t userData) const;

      //returns structure of existing long press timers
      KeyMappingBase::HkTimers GetTimerValuesForKey(uint32_t key, uint32_t userData) const;

      //returns repeat key timeout if this key is a repeat key
      unsigned int GetRepeatTimeoutForKey(uint32_t key, uint32_t userData) const;

      //checks if this key is defined as abort key for animations
      bool IsAbortKey(uint32_t key, uint32_t userData) const;

   private:

      //structure used to record the app key and long press details
      struct HkLongPress
      {
         uint32_t key;
         KeyMappingBase::HkTimers timers;
         bool isRepeatKey;
      };

      typedef std::map<uint32_t, struct HkLongPress> KeyMap;
      typedef std::map<uint32_t, uint32_t> EncoderMap;
      typedef std::vector<uint32_t> AbortKeyList;

      KeyMap _mKeyMap;
      EncoderMap _mEncoderMap;
      AbortKeyList _mAbortKeyList;

      bool _mAnyKeyIsAbortKey;
      unsigned int _mRepeatTimeout;

      ///
      void CreateKeyMap();
      void UpdateMapKey(uint32_t OrigKey,
                        uint32_t Appkey,
                        unsigned int long1,
                        unsigned int long2,
                        unsigned int long3,
                        unsigned int long4,
                        unsigned int long5,
                        unsigned int long6,
                        unsigned int long7,
                        unsigned int long8,
                        unsigned int long9,
                        unsigned int long10,
                        bool isRepeatKey);

      ///
      void CreateEncoderMap();

      ///
      void CreateAbortKeyList();
};


#endif
