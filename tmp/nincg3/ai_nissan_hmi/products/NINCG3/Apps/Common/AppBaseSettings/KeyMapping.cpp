#include "sys_std_if.h"
#include "hmi_trace_if.h"

#include "BaseContract/generated/BaseMsgs.h"
#include "ProjectBaseTypes.h"
#include "KeyMapping.h"


//////// TRACE IF ///////////////////////////////////////////////////
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_HMI_FW_INPUT
#include "trcGenProj/Header/KeyMapping.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN


//project specific key IDs additional to the key IDs defined in Linux input.h
//these IDs are send by the V850 key driver via INC to Linux. First to
//input_inc kernel driver then via evdev to Wayland/ILM

//valid key codes are ((>= 0x300) && (< 0x3ff))

//fascia keys
#define KEY_RN_FAC_PHONE                    0x300
#define KEY_RN_FAC_CD                       0x301
#define KEY_RN_FAC_CAMERA                   0x302
#define KEY_RN_FAC_AUX                      0x303
#define KEY_RN_FAC_PREV                     0x304
#define KEY_RN_FAC_NEXT                     0x305
#define KEY_RN_FAC_RADIO                    0x306
#define KEY_RN_FAC_INFO                     0x307
#define KEY_RN_FAC_SETUP                    0x308
#define KEY_RN_FAC_MAP                      0x309
#define KEY_RN_FAC_BACK                     0x30A
#define KEY_RN_FAC_DAY_NIGHT                0x30B
#define KEY_RN_FAC_NAV                      0x30C
#define KEY_RN_FAC_ENTER                    0x30D
#define KEY_RN_FAC_XM                       0x30E
#define KEY_RN_FAC_POWER                    0x30F
#define KEY_RN_FAC_EJECT                    0x310
#define KEY_RN_FAC_FMAM                     0x311
#define KEY_RN_FAC_MENU                     0x312
#define KEY_RN_FAC_APP                      0x313
#define KEY_RN_FAC_AUDIO                    0x314
#define KEY_RN_FAC_DISP                     0x315
#define KEY_RN_FAC_VOLUME_UP                0x316
#define KEY_RN_FAC_VOLUME_DOWN              0x317

//facia long press keys
#define KEY_RN_FAC_MENU_LONGPRESS           0x2D0
#define KEY_RN_FAC_SETUP_LONGPRESS          0x2D1
#define KEY_RN_FAC_POWER_LONGPRESS          0x2D2

//MFL keys v1
#define KEY_SWC_MODE                        0x64
#define KEY_SWC_OK                          0x65
#define KEY_SWC_SKIP_PLUS                   0x66
#define KEY_SWC_SKIP_MINUS                  0x67
#define KEY_SWC_PTT                         0x68
#define KEY_SWC_VOLUME_DOWN                 0x6A
#define KEY_SWC_VOLUME_UP                   0x6B
#define KEY_SWC_HANGUP                      0x6C
#define KEY_SWC_BACK                        0x6D
#define KEY_SWC_UP                          0x6F
#define KEY_SWC_DOWN                        0x70
#define KEY_SWC_MUTE                        0x71
#define KEY_SWC_AUDIO_SOURCE_UP             0x72
#define KEY_SWC_AUDIO_SOURCE_DOWN           0x73
//#define KEY_SWC_DOUBLE_LEFT                 0x353
//#define KEY_SWC_DOUBLE_RIGHT                0x354
//#define KEY_SWC_PHONE                       0x357
//#define KEY_SWC_OFFHOOK                     0x35e
//#define KEY_SWC_HANGUP_MODE                 0x35f
//#define KEY_SWC_OFFHOOK_PTT                 0x361

//virtual keys
#define KEY_VIRT_ENG_TESTMODE               0x3A0
#define KEY_VIRT_SCREENSHOT                 0x3A1

//Joystick keys
#define KEY_JOYSTICK_UP                     0x3C0
#define KEY_JOYSTICK_DOWN                   0x3C1
#define KEY_JOYSTICK_LEFT                   0x3C2
#define KEY_JOYSTICK_RIGHT                  0x3C3
#define KEY_JOYSTICK_UPPER_LEFT             0x3C4
#define KEY_JOYSTICK_UPPER_RIGHT            0x3C5
#define KEY_JOYSTICK_LOWER_LEFT             0x3C6
#define KEY_JOYSTICK_LOWER_RIGHT            0x3C7
#define KEY_JOYSTICK_ENTER                  0x3C8
#define KEY_JOYSTICK_MAP                    0x3C9
#define KEY_JOYSTICK_OPTION                 0x3CA
#define KEY_JOYSTICK_HOME                   0x3CB
#define KEY_JOYSTICK_BACK                   0x3CC

//Joystick long press keys
#define KEY_JOYSTICK_OPTION_LONGPRESS       0x3B0
#define KEY_JOYSTICK_HOME_LONGPRESS         0x3B1
#define KEY_JOYSTICK_BACK_LONGPRESS         0x3B2
#define KEY_JOYSTICK_ENTER_LONGPRESS        0x3B3

#ifndef REL_JOYSTICK_WHEEL
#define REL_JOYSTICK_WHEEL                   0x0A
#endif
// ------------------------------------------------------------------------
KeyMapping::KeyMapping()
   : _mAnyKeyIsAbortKey(false),
     _mRepeatTimeout(500)
{
   CreateKeyMap();
   CreateEncoderMap();
   CreateAbortKeyList();
}


// ------------------------------------------------------------------------
uint32_t KeyMapping::GetHmiKeyCode(uint32_t key, uint32_t userData) const
{
   // userData not used in this implementation
   (void)userData;

   uint32_t unifiedKeyCode = hmibase::appbase::keymapbase::KEY_CODE_INVALID;
   std::map<uint32_t, HkLongPress>::const_iterator itr;

   itr = _mKeyMap.find(key);
   if (itr != _mKeyMap.end())
   {
      unifiedKeyCode = itr->second.key;
   }

   if (unifiedKeyCode == hmibase::appbase::keymapbase::KEY_CODE_INVALID)
   {
      ETG_TRACE_ERR(("KeyMapping::GetHmiKeyCode hmi-keycode not found for orig-keycode '%d'!", key));
   }
   else
   {
      ETG_TRACE_USR4(("KeyMapping::GetHmiKeyCode Mapped orig-keycode: '%d' to hmi-keycode: %d!",
                      key,
                      unifiedKeyCode));
   }

   return unifiedKeyCode;
}


// ------------------------------------------------------------------------
uint32_t KeyMapping::GetOrigKeyCode(uint32_t key, uint32_t userData) const
{
   // userData not used in this implementation
   (void)userData;

   uint32_t origKeyCode = 0;
   std::map<uint32_t, HkLongPress>::const_iterator itr;

   for (itr = _mKeyMap.begin(); itr != _mKeyMap.end(); ++itr)
   {
      if ((itr->second.key) == key)
      {
         origKeyCode = itr->first;
         break;
      }
   }

   if (origKeyCode == 0)
   {
      ETG_TRACE_ERR(("KeyMapping::GetOrigKeyCode orig-keycode not found for hmi-keycode '%d'!", key));
   }
   else
   {
      ETG_TRACE_USR4(("KeyMapping::GetOrigKeyCode Mapped hmi-keycode: '%d' to orig-keycode: %d!",
                      key,
                      origKeyCode));
   }

   return origKeyCode;
}


// ------------------------------------------------------------------------
uint32_t KeyMapping::GetHmiEncoderCode(uint32_t encoder, uint32_t userData) const
{
   // userData not used in this implementation
   (void)userData;

   uint32_t unifiedEncoderCode = hmibase::appbase::keymapbase::ENCODER_CODE_INVALID;
   std::map<uint32_t, uint32_t>::const_iterator itr;

   itr = _mEncoderMap.find(encoder);
   if (itr != _mEncoderMap.end())
   {
      unifiedEncoderCode = itr->second;
   }

   if (unifiedEncoderCode == hmibase::appbase::keymapbase::ENCODER_CODE_INVALID)
   {
      ETG_TRACE_ERR(("KeyMapping::GetHmiEncoderCode hmi-encoder-code not found for orig-encoder-code '%d'!", encoder));
   }
   else
   {
      ETG_TRACE_USR4(("KeyMapping::GetHmiEncoderCode Mapped orig-encoder-code: '%d' to hmi-encoder-code: %d!",
                      encoder,
                      unifiedEncoderCode));
   }
   return unifiedEncoderCode;
}


// ------------------------------------------------------------------------
uint32_t KeyMapping::GetOrigEncoderCode(uint32_t encoder, uint32_t userData) const
{
   // userData not used in this implementation
   (void)userData;

   uint32_t origEncoderCode = 0;
   bool bMappingFound = false;
   std::map<uint32_t, uint32_t>::const_iterator itr;

   for (itr = _mEncoderMap.begin(); itr != _mEncoderMap.end(); ++itr)
   {
      if ((itr->second) == encoder)
      {
         origEncoderCode = itr->first;
         bMappingFound = true;
         break;
      }
   }

   if (!bMappingFound)
   {
      ETG_TRACE_ERR(("KeyMapping::GetOrigEncoderCode orig-encoder-code not found for hmi-encoder-code '%d'!", encoder));
   }
   else
   {
      ETG_TRACE_USR4(("KeyMapping::GetOrigEncoderCode Mapped hmi-encoder-code: '%d' to orig-encoder-code: %d!",
                      encoder,
                      origEncoderCode));
   }
   return origEncoderCode;
}


// ------------------------------------------------------------------------
void KeyMapping::CreateKeyMap()
{
   // final key mapping on V850
   UpdateMapKey(KEY_RN_FAC_RADIO, (uint32_t)HARDKEYCODE_HK_FM_AM, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_FMAM, (uint32_t)HARDKEYCODE_HK_FM_AM, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_XM, (uint32_t)HARDKEYCODE_HK_XMTUNER, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_CD, (uint32_t)HARDKEYCODE_HK_CD, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_AUX, (uint32_t)HARDKEYCODE_HK_AUX, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_EJECT, (uint32_t)HARDKEYCODE_HK_EJECT, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_SETUP, (uint32_t)HARDKEYCODE_HK_SETTINGS, 15000, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_PHONE, (uint32_t)HARDKEYCODE_HK_PHONE, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_NAV, (uint32_t)HARDKEYCODE_HK_NAV, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_MENU, (uint32_t)HARDKEYCODE_HK_MENU, 1500, 15000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_INFO, (uint32_t)HARDKEYCODE_HK_INFO, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_APP, (uint32_t)HARDKEYCODE_HK_INFO, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_MAP, (uint32_t)HARDKEYCODE_HK_MAP, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_BACK, (uint32_t)HARDKEYCODE_HK_ESC, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_DAY_NIGHT, (uint32_t)HARDKEYCODE_HK_ILLUM, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_CAMERA, (uint32_t)HARDKEYCODE_HK_CAMERA, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_POWER, (uint32_t)HARDKEYCODE_HK_MUTE, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_ENTER, (uint32_t)HARDKEYCODE_HK_SELECT, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_NEXT, (uint32_t)HARDKEYCODE_HK_NEXT, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_PREV, (uint32_t)HARDKEYCODE_HK_PREVIOUS, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_DISP, (uint32_t)HARDKEYCODE_HK_DISP, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_AUDIO, (uint32_t)HARDKEYCODE_HK_AUDIO, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_VOLUME_UP, (uint32_t)HARDKEYCODE_HK_VOLUME_UP, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_VOLUME_DOWN, (uint32_t)HARDKEYCODE_HK_VOLUME_DOWN, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);

   //Fascia Long Press Keys
   UpdateMapKey(KEY_RN_FAC_MENU_LONGPRESS, (uint32_t)HARDKEYCODE_HK_MENU_LONGPRESS, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_SETUP_LONGPRESS, (uint32_t)HARDKEYCODE_HK_SETUP_LONGPRESS, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_RN_FAC_POWER_LONGPRESS, (uint32_t)HARDKEYCODE_HK_POWER_LONGPRESS, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);

   //virtual keys
   UpdateMapKey(KEY_VIRT_ENG_TESTMODE, (uint32_t)HARDKEYCODE_VIRT_TESTMODE, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_VIRT_SCREENSHOT, (uint32_t)HARDKEYCODE_VIRT_SCREENSHOT, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);

   // Mapping for Joystick
   UpdateMapKey(KEY_JOYSTICK_UP, (uint32_t)HARDKEYCODE_JOYSTICK_UP, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_JOYSTICK_DOWN, (uint32_t)HARDKEYCODE_JOYSTICK_DOWN, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_JOYSTICK_LEFT, (uint32_t)HARDKEYCODE_JOYSTICK_LEFT, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_JOYSTICK_RIGHT, (uint32_t)HARDKEYCODE_JOYSTICK_RIGHT, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_JOYSTICK_UPPER_LEFT, (uint32_t)HARDKEYCODE_JOYSTICK_UPPER_LEFT, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_JOYSTICK_UPPER_RIGHT, (uint32_t)HARDKEYCODE_JOYSTICK_UPPER_RIGHT, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_JOYSTICK_LOWER_LEFT, (uint32_t)HARDKEYCODE_JOYSTICK_LOWER_LEFT, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_JOYSTICK_LOWER_RIGHT, (uint32_t)HARDKEYCODE_JOYSTICK_LOWER_RIGHT, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_JOYSTICK_ENTER, (uint32_t)HARDKEYCODE_JOYSTICK_ENTER, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_JOYSTICK_MAP, (uint32_t)HARDKEYCODE_JOYSTICK_MAP, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_JOYSTICK_OPTION, (uint32_t)HARDKEYCODE_JOYSTICK_OPTION, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_JOYSTICK_HOME, (uint32_t)HARDKEYCODE_JOYSTICK_HOME, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_JOYSTICK_BACK, (uint32_t)HARDKEYCODE_JOYSTICK_BACK, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);

   // Mapping for Joystick Long Press Keys
   UpdateMapKey(KEY_JOYSTICK_ENTER_LONGPRESS, (uint32_t)HARDKEYCODE_JOYSTICK_ENTER_LONGPRESS, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_JOYSTICK_OPTION_LONGPRESS, (uint32_t)HARDKEYCODE_JOYSTICK_OPTION_LONGPRESS, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_JOYSTICK_HOME_LONGPRESS, (uint32_t)HARDKEYCODE_JOYSTICK_HOME_LONGPRESS, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_JOYSTICK_BACK_LONGPRESS, (uint32_t)HARDKEYCODE_JOYSTICK_BACK_LONGPRESS, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);

   UpdateMapKey(KEY_SWC_SKIP_PLUS, (uint32_t)HARDKEYCODE_SWC_NEXT, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_SWC_SKIP_MINUS, (uint32_t)HARDKEYCODE_SWC_PREV, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_SWC_OK, (uint32_t)HARDKEYCODE_SWC_SWITCH_AUDIO, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_SWC_BACK, (uint32_t)HARDKEYCODE_SWC_MENU_BACK, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_SWC_MODE, (uint32_t)HARDKEYCODE_SWC_SWITCH_AUDIO, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_SWC_MUTE, (uint32_t)HARDKEYCODE_SWC_MUTE_DEMUTE, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_SWC_PTT, (uint32_t)HARDKEYCODE_SWC_PTT, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_SWC_HANGUP, (uint32_t)HARDKEYCODE_SWC_PHONE, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_SWC_VOLUME_UP, (uint32_t)HARDKEYCODE_SWC_VOLUME_UP, 2000, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_SWC_VOLUME_DOWN, (uint32_t)HARDKEYCODE_SWC_VOLUME_DOWN, 2000, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_SWC_UP, (uint32_t)HARDKEYCODE_SWC_UP, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_SWC_DOWN, (uint32_t)HARDKEYCODE_SWC_DOWN, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_SWC_MUTE, (uint32_t)HARDKEYCODE_SWC_MUTE, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_SWC_AUDIO_SOURCE_UP, (uint32_t)HARDKEYCODE_SWC_AUDIO_SOURCE_UP, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
   UpdateMapKey(KEY_SWC_AUDIO_SOURCE_DOWN, (uint32_t)HARDKEYCODE_SWC_AUDIO_SOURCE_DOWN, 1500, 1000, 0, 2000, 0, 0, 0, 0, 0, 0, false);
}


// ------------------------------------------------------------------------
void KeyMapping::CreateEncoderMap()
{
   _mEncoderMap[REL_HWHEEL] = (uint32_t)ENCCODE_LEFT_ENCODER;
   _mEncoderMap[REL_WHEEL] = (uint32_t)ENCCODE_RIGHT_ENCODER;
   _mEncoderMap[REL_JOYSTICK_WHEEL] = (uint32_t)ENCCODE_JOYSTICK_ENCODER;
}


// ------------------------------------------------------------------------
void KeyMapping::CreateAbortKeyList()
{
   //_mAbortKeyList.push_back((uint32_t)HARDKEYCODE_HMI_BASE_2);
   // _mAbortKeyList.push_back((uint32_t)HARDKEYCODE_HMI_BASE_4);
}


// ------------------------------------------------------------------------
void KeyMapping::UpdateMapKey(uint32_t OrigKey,
                              uint32_t Appkey,
                              unsigned int long1,
                              unsigned int long2,
                              unsigned int long3,
                              unsigned int long4,
                              unsigned int long5,
                              unsigned int long6,
                              unsigned int long7,
                              unsigned int long8,
                              unsigned int long9,
                              unsigned int long10,
                              bool isRepeatKey)
{
   HkLongPress keyUpdate;
   keyUpdate.key = Appkey;
   keyUpdate.timers.long1 = long1;
   keyUpdate.timers.long2 = long2;
   keyUpdate.timers.long3 = long3;
   keyUpdate.timers.long4 = long4;
   keyUpdate.timers.long5 = long5;
   keyUpdate.timers.long6 = long6;
   keyUpdate.timers.long7 = long7;
   keyUpdate.timers.long8 = long8;
   keyUpdate.timers.long9 = long9;
   keyUpdate.timers.long10 = long10;
   keyUpdate.isRepeatKey = isRepeatKey;
   _mKeyMap[OrigKey] = keyUpdate;
}


// ------------------------------------------------------------------------
KeyMappingBase::HkTimers KeyMapping::GetTimerValuesForKey(uint32_t key, uint32_t userData) const
{
   // userData not used in this implementation
   (void)userData;

   HkTimers lpTimers;
   memset(&lpTimers, 0, sizeof(lpTimers));
   bool keyFound = false;
   std::map<uint32_t, HkLongPress>::const_iterator itr;

   for (itr = _mKeyMap.begin(); itr != _mKeyMap.end(); ++itr)
   {
      if ((itr->second.key) == key)
      {
         lpTimers = itr->second.timers;
         keyFound = true;
         break;
      }
   }

   if (keyFound)
   {
      ETG_TRACE_USR4(("KeyMapping::GetTimerValuesForKey key %d found with these timer values: Long1 = %d, Long2 = %d, Long3 = %d, Long4 = %d, Long5 = %d.",
                      key, lpTimers.long1, lpTimers.long2, lpTimers.long3, lpTimers.long4, lpTimers.long5));
      ETG_TRACE_USR4(("KeyMapping::GetTimerValuesForKey key %d found with these timer values: Long6 = %d, Long7 = %d, Long8 = %d, Long9 = %d, Long10 = %d.",
                      key, lpTimers.long6, lpTimers.long7, lpTimers.long8, lpTimers.long9, lpTimers.long10));
   }
   else
   {
      ETG_TRACE_ERR(("KeyMapping::GetTimerValuesForKey key not found '%d'!", key));
   }

   return lpTimers;
}


// ------------------------------------------------------------------------
unsigned int KeyMapping::GetRepeatTimeoutForKey(uint32_t key, uint32_t userData) const
{
   // userData not used in this implementation
   (void)userData;

   unsigned int repeatTimeout = 0;
   bool keyFound = false;
   bool isRepeatKey = false;
   std::map<uint32_t, HkLongPress>::const_iterator itr;

   for (itr = _mKeyMap.begin(); itr != _mKeyMap.end(); ++itr)
   {
      if ((itr->second.key) == key)
      {
         if (itr->second.isRepeatKey)
         {
            //yes, this is a repeat key
            repeatTimeout = _mRepeatTimeout;
            isRepeatKey = true;
         }
         keyFound = true;
         break;
      }
   }

   if (keyFound)
   {
      if (isRepeatKey)
      {
         ETG_TRACE_USR4(("KeyMapping::GetRepeatTimeoutForKey key %d is a repeat key with this timer value: %d.",
                         key, repeatTimeout));
      }
      else
      {
         ETG_TRACE_USR4(("KeyMapping::GetRepeatTimeoutForKey key %d is no repeat key.",
                         key));
      }
   }
   else
   {
      ETG_TRACE_ERR(("KeyMapping::GetRepeatTimeoutForKey key not found '%d'!", key));
   }

   return repeatTimeout;
}


// ------------------------------------------------------------------------
bool KeyMapping::IsAbortKey(uint32_t key, uint32_t userData) const
{
   // userData not used in this implementation
   (void)userData;
   bool bReturn = false;

   if (_mAnyKeyIsAbortKey)
   {
      //every key shell do an abort, independent of the key code
      bReturn = true;
   }
   else
   {
      //only specific keys are abort keys
      if (key != hmibase::appbase::keymapbase::KEY_CODE_INVALID)
      {
         for (unsigned int i = 0; i < _mAbortKeyList.size(); ++i)
         {
            if ((_mAbortKeyList[i]) == key)
            {
               bReturn = true;
               break;
            }
         }
      }
      else
      {
         ETG_TRACE_ERR(("KeyMapping::IsAbortKey invalid key code: '%d'!",
                        key));
      }
   }
   return bReturn;
}
