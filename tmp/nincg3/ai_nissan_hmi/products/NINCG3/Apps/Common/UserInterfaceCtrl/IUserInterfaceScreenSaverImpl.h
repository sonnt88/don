/*
 * IUserInterfaceScreenSaverImpl.h
 *
 *  Created on: Sep 4, 2015
 *      Author: pad1cob
 */

#ifndef IUserInterfaceScreenSaverImpl_h
#define IUserInterfaceScreenSaverImpl_h

#include "bosch/cm/ai/nissan/hmi/userinterfacectrl/UserInterfaceCtrl.h"

class IUserInterfaceScreenSaverImpl
{
   public:
      virtual ~IUserInterfaceScreenSaverImpl() {}
      virtual void vShowScreenSaver() = 0;
      virtual void vSetAvailableScreenSavers(::std::vector< bosch::cm::ai::nissan::hmi::userinterfacectrl::UserInterfaceCtrl::ScreenSaverInfo > screenSaver) = 0;
};


#endif /* IUserInterfaceScreenSaverImpl_h */
