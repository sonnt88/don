///////////////////////////////////////////////////////////
//  UserInterfaceScreenSaver.cpp
//  Implementation of the Class UserInterfaceScreenSaver
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#include "UserInterfaceScreenSaver.h"
#include "IUserInterfaceScreenSaver.h"
#include "IUserInterfaceScreenSaverImpl.h"
#include "hmi_trace_if.h"
#include "../Common_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_USERINTERFACECTRL
#include "trcGenProj/Header/UserInterfaceScreenSaver.cpp.trc.h"
#endif

UserInterfaceScreenSaver* UserInterfaceScreenSaver::_pUserInterfaceScreenSaver = NULL;

UserInterfaceScreenSaver::UserInterfaceScreenSaver()
   : _screenSaverTimer(*this, 6000)
   , _screenSaverMode(SCREENSAVER_DISABLED)
   , bAllowScreenSaver(false)
   , _pScreenSaverImplementer(NULL)
   , _pScreenSaverImpl(NULL)
   , timerToken(0)
   , _bAppInForeGround(false)
{
   _pUserInterfaceScreenSaver = this;
   _screenSaverTimer.stop();
}


UserInterfaceScreenSaver::~UserInterfaceScreenSaver()
{
   _pScreenSaverImplementer = NULL;
}


UserInterfaceScreenSaver* UserInterfaceScreenSaver::getInstance()
{
   if (_pUserInterfaceScreenSaver == NULL)
   {
      _pUserInterfaceScreenSaver = new UserInterfaceScreenSaver();
   }
   return _pUserInterfaceScreenSaver;
}


void UserInterfaceScreenSaver::onInputEvent()
{
   ETG_TRACE_USR4(("Userinterface: ScreenSaver :           vOnInputEvent"));
   if (_screenSaverTimer.isActive())
   {
      _screenSaverTimer.stop();
   }
   if (_screenSaverMode == SCREENSAVER_ENABLED && bAllowScreenSaver && _bAppInForeGround)
   {
      timerToken = _screenSaverTimer.start();
   }
}


void UserInterfaceScreenSaver::vBlockScreenSaver()
{
   ETG_TRACE_USR4(("Userinterface: ScreenSaver :           vBlockScreenSaver"));
   bAllowScreenSaver = false;
   if (_screenSaverTimer.isActive())
   {
      _screenSaverTimer.stop();
   }
}


void UserInterfaceScreenSaver::vSetScreenSaverRequestor(IUserInterfaceScreenSaverImpl* pScreenSaverImpl)
{
   _pScreenSaverImpl = pScreenSaverImpl;
}


void UserInterfaceScreenSaver::vAllowScreenSaver()
{
   ETG_TRACE_USR4(("Userinterface: ScreenSaver :           vAllowScreenSaver"));
   bAllowScreenSaver = true;
   if (SCREENSAVER_ENABLED == _screenSaverMode)
   {
      _screenSaverTimer.stop();
      timerToken =  _screenSaverTimer.start();
   }
}


void UserInterfaceScreenSaver::vShowScreenSaver()
{
   if (_pScreenSaverImpl != NULL)
   {
      _pScreenSaverImpl->vShowScreenSaver();
   }
}


void UserInterfaceScreenSaver::vOnNewScreenSaverMode(ScreenSaverMode screenSaverMode, uint32 applicationId)
{
   ETG_TRACE_USR4(("Userinterface: ScreenSaver :           vOnNewScreenSaverMode : %d", screenSaverMode));
   ETG_TRACE_USR4(("Userinterface: ScreenSaver :           vOnNewScreenSaverMode : %d", applicationId));
   _screenSaverMode = screenSaverMode;
   if (_bAppInForeGround && bAllowScreenSaver && SCREENSAVER_ENABLED == _screenSaverMode)
   {
      _screenSaverTimer.stop();
      timerToken =  _screenSaverTimer.start();
   }
   //Send the new screen saver mode update to lib users
   if (NULL != _pScreenSaverImplementer)
   {
      _pScreenSaverImplementer->vOnNewScreenSaverMode(screenSaverMode, applicationId);
   }
}


void UserInterfaceScreenSaver::onExpired(asf::core::Timer& timer, boost::shared_ptr<asf::core::TimerPayload> /*payload*/)
{
   ETG_TRACE_USR4(("Userinterface: ScreenSaver :onExpired"));
   if (timerToken == timer.getAct())
   {
      if (_bAppInForeGround && bAllowScreenSaver && SCREENSAVER_ENABLED == _screenSaverMode)
      {
         vShowScreenSaver();
      }
   }
}


void UserInterfaceScreenSaver::vSupportScreenSaver(uint32 contextId, uint32 priority)
{
   if (!bIsScreenSaverAvailable(contextId))
   {
      bosch::cm::ai::nissan::hmi::userinterfacectrl::UserInterfaceCtrl::ScreenSaverInfo oScreenData;
      oScreenData.setContextId(contextId);
      oScreenData.setPriority(priority);
      _availableScreenSavers.push_back(oScreenData);
      if (_pScreenSaverImpl != NULL)
      {
         _pScreenSaverImpl->vSetAvailableScreenSavers(_availableScreenSavers);
      }
   }
}


bool UserInterfaceScreenSaver::bIsScreenSaverAvailable(uint32 contextId)
{
   for (uint32 index = 0; index < _availableScreenSavers.size(); index++)
   {
      if (_availableScreenSavers[index].getContextId() == contextId)
      {
         return true;
      }
   }
   return false;
}
