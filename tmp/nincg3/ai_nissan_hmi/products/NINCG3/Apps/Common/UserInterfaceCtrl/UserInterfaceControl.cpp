/*
 * UserInterfaceControl.cpp
 *
 *  Created on: Sep 7, 2015
 *      Author: goc1kor
 */

#include "UserInterfaceProxy.h"
#include "UserInterfaceControl.h"
#include "UserInterfaceScreenSaver.h"
#include "IUserInterfaceObserver.h"
#include "hmi_trace_if.h"
#include "../Common_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_USERINTERFACECTRL
#include "trcGenProj/Header/UserInterfaceControl.cpp.trc.h"
#endif

static UserInterfaceControl* _pInterfaceControl = NULL;


UserInterfaceControl::UserInterfaceControl(UserInterfaceScreenSaver* pScreenSaver, uint32 applicationId): _applicationId(applicationId)
   , _pScreenSaver(pScreenSaver)
{
   _pUserInterfaceProxy = new UserInterfaceProxy();
   _RequestedLockState = -1;
   _RequestedHKReg = -1;
   _RequestedHKDereg = -1;
   _RegisteredHKs.clear();
   _QueuedHKRegRequests.clear();
   _pUserInterfaceProxy->setUserInterfaceProxyImpl(this);
   _pInterfaceControl = this;
}


UserInterfaceControl::UserInterfaceControl(): _applicationId(APPID_APPHMI_UNKNOWN)
   , _pScreenSaver(NULL)
{
   _pUserInterfaceProxy = new UserInterfaceProxy();
   _RequestedLockState = -1;
   _RequestedHKReg = -1;
   _RequestedHKDereg = -1;
   _RegisteredHKs.clear();
   _QueuedHKRegRequests.clear();
   _pUserInterfaceProxy->setUserInterfaceProxyImpl(this);
   _pInterfaceControl = this;
}


UserInterfaceControl::~UserInterfaceControl()
{
   if (_pUserInterfaceProxy != NULL)
   {
      delete _pUserInterfaceProxy;
      _pUserInterfaceProxy = NULL;
   }
   _pInterfaceControl = NULL;
   _PendingHKRegRequests.clear();
   _QueuedHKRegRequests.clear();
   _RegisteredHKs.clear();
   _PendingHKDeregRequests.clear();
   _ObserverList.clear();
}


UserInterfaceControl* UserInterfaceControl::getInstance(void)
{
   return _pInterfaceControl;
}


void UserInterfaceControl::registerLockApplicationUpdate(IUserInterfaceObserver* pIUserInterfaceObserver)
{
   ::std::vector<IUserInterfaceObserver*>::iterator itr = ::std::find(_ObserverList.begin(), _ObserverList.end(), pIUserInterfaceObserver);
   ETG_TRACE_ERR(("UI Control:   registerLockApplicationUpdate called"));
   if (pIUserInterfaceObserver != NULL && itr == _ObserverList.end())
   {
      _ObserverList.push_back(pIUserInterfaceObserver);
   }
}


void UserInterfaceControl::deregisterLockApplicationUpdate(IUserInterfaceObserver* pIUserInterfaceObserver)
{
   ::std::vector<IUserInterfaceObserver*>::iterator itr = ::std::find(_ObserverList.begin(), _ObserverList.end(), pIUserInterfaceObserver);
   if (itr != _ObserverList.end())
   {
      _ObserverList.erase(itr);
   }
}


void UserInterfaceControl::setApplicationId(int applicationId)
{
   if ((applicationId > 0) && (applicationId < APPID_APPHMI_UNKNOWN))
   {
      _applicationId = applicationId;
   }
}


void UserInterfaceControl::setScreenSaverMode(int screenSaverMode)
{
   if (_pUserInterfaceProxy != NULL)
   {
      _pUserInterfaceProxy->sendScreenSaverMode(screenSaverMode, _applicationId);
   }
}


bool UserInterfaceControl::hardKeyRegistrationReq(int keyCode, int priority)
{
   bool bRet = false;
   ETG_TRACE_USR4(("hardKeyRegistrationReq : Thread ID : %d", (unsigned int)pthread_self()));
   if (_pUserInterfaceProxy != NULL && _applicationId < APPID_APPHMI_UNKNOWN)
   {
      int _lastRequestedHKReg = _RequestedHKReg;
      _PendingHKRegRequests[keyCode] = priority;
      _RequestedHKReg = keyCode;
      ::std::vector<int>::iterator itr = ::std::find(_PendingHKDeregRequests.begin(), _PendingHKDeregRequests.end(), keyCode) ;
      if (itr != _PendingHKDeregRequests.end())
      {
         _PendingHKDeregRequests.erase(itr);
      }
      bool bReg = _pUserInterfaceProxy->sendHardKeyRegistration(_applicationId, keyCode, priority);
      if (bReg)
      {
         ETG_TRACE_USR4(("UI Control:   RegisterHKRequest : keyCode %d, application %d, sent successful", ETG_CENUM(hmibase::HardKeyCodeEnum, keyCode), ETG_CENUM(enApplicationId, _applicationId)));
      }
      else
      {
         _RequestedHKReg = _lastRequestedHKReg;
         ETG_TRACE_USR4(("UI Control:   RegisterHKRequest : keyCode %d, application %d, failed", ETG_CENUM(hmibase::HardKeyCodeEnum, keyCode), ETG_CENUM(enApplicationId, _applicationId)));
      }
      bRet = true;
   }
   return bRet;
}


bool UserInterfaceControl::hardKeyDeregistrationReq(int keyCode)
{
   ETG_TRACE_USR4(("hardKeyDeregistrationReq : Thread ID : %d", (unsigned int)pthread_self()));
   bool bRet = false;
   ::std::map<int, int>::iterator itr_m = _RegisteredHKs.find(keyCode);
   int _lastRequestedHKDereg = _RequestedHKDereg;
   removePendingHKReg(keyCode);
   if ((_pUserInterfaceProxy != NULL) && (_applicationId < APPID_APPHMI_UNKNOWN) && (itr_m != _RegisteredHKs.end()))
   {
      ::std::vector<int>::iterator itr_v = ::std::find(_PendingHKDeregRequests.begin(), _PendingHKDeregRequests.end(), keyCode);
      if (itr_v == _PendingHKDeregRequests.end())
      {
         _PendingHKDeregRequests.push_back(keyCode);
      }
      if (_RequestedHKDereg == -1)
      {
         _RequestedHKDereg = keyCode;
         bRet = _pUserInterfaceProxy->sendHardKeyDeregistration(_applicationId, keyCode);
      }
   }
   if (!bRet)
   {
      _RequestedHKDereg = _lastRequestedHKDereg;
   }
   return bRet;
}


bool UserInterfaceControl::hardKeyDeRegistrationAllReq()
{
   bool bRet = false;
   ETG_TRACE_USR4(("hardKeyDeRegistrationAllReq : Thread ID : %d", (unsigned int)pthread_self()));
   if (_pUserInterfaceProxy != NULL && _applicationId < APPID_APPHMI_UNKNOWN)
   {
      bRet = _pUserInterfaceProxy->sendAllHardKeyDeregistration(_applicationId);
      if (bRet)
      {
         _PendingHKRegRequests.clear();
         _PendingHKDeregRequests.clear();
         _RegisteredHKs.clear();
         _RequestedHKReg = -1;
         _RequestedHKDereg = -1;
      }
   }
   return bRet;
}


bool UserInterfaceControl::lockApplicationChangeReq(bool lockState)
{
   bool bRet = false;
   if (_pUserInterfaceProxy != NULL && _applicationId < APPID_APPHMI_UNKNOWN)
   {
      _RequestedLockState = lockState;
      bRet = _pUserInterfaceProxy->sendLockApplicationChange(_applicationId, lockState);
   }
   else
   {
      ETG_TRACE_ERR(("UI Control:   lockApplicationChangeReq failed : lockState %d from application %d", lockState, _applicationId));
   }
   return bRet;
}


void UserInterfaceControl::applicationLockStateUpdate(int applicationId, int lockState)
{
   sendLockApplicationStateUpdate(applicationId, lockState);
   if ((_RequestedLockState != -1) && (applicationId == _applicationId) && (_RequestedLockState != lockState))
   {
      lockApplicationChangeReq(_RequestedLockState);
   }
}


void UserInterfaceControl::onNewScreenSaverMode(int screenSaverMode, uint32 applicationId)
{
   if (_pScreenSaver != 0)
   {
      _pScreenSaver->vOnNewScreenSaverMode((ScreenSaverMode)screenSaverMode, applicationId);
   }
}


void UserInterfaceControl::hardKeyRegistrationUpdate(int applicationId, int KeyCode)
{
   ETG_TRACE_USR4(("hardKeyRegistrationUpdate : Thread ID : %d", (unsigned int)pthread_self()));
   if (APPID_APPHMI_UNKNOWN == applicationId)
   {
      ::std::map<int, int>::iterator itr = _QueuedHKRegRequests.find(KeyCode);
      if (itr != _QueuedHKRegRequests.end())
      {
         hardKeyRegistrationReq(itr->first, itr->second);
      }
   }
   else if (applicationId != _applicationId)
   {
      ::std::map<int, int>::iterator itr = _RegisteredHKs.find(KeyCode);
      if (itr != _RegisteredHKs.end())
      {
         _QueuedHKRegRequests[itr->first] = itr->second;
         _RegisteredHKs.erase(itr);
      }
   }
   if (!_PendingHKDeregRequests.empty())
   {
      hardKeyDeregistrationReq(_PendingHKDeregRequests.front());
   }
}


void UserInterfaceControl::hardKeyRegistrationRes(int reponse)
{
   ETG_TRACE_USR4(("hardKeyRegistrationRes : Thread ID : %d", (unsigned int)pthread_self()));
   if (reponse)
   {
      ::std::map<int, int>::iterator itr = _PendingHKRegRequests.find(_RequestedHKReg);
      if (itr != _PendingHKRegRequests.end())
      {
         ETG_TRACE_USR4(("UI Control:   RegisterHKResponse : keyCode %d, application %d, received", ETG_CENUM(hmibase::HardKeyCodeEnum, itr->first), ETG_CENUM(enApplicationId, _applicationId)));
         _RegisteredHKs[itr->first] = itr->second;
         _PendingHKRegRequests.erase(itr);
      }
      itr = _QueuedHKRegRequests.find(_RequestedHKReg);
      if (itr != _QueuedHKRegRequests.end())
      {
         ETG_TRACE_USR4(("UI Control:   RegisterHKResponse : keyCode %d, application %d, received", ETG_CENUM(hmibase::HardKeyCodeEnum, itr->first), ETG_CENUM(enApplicationId, _applicationId)));
         _RegisteredHKs[itr->first] = itr->second;
         _QueuedHKRegRequests.erase(itr);
      }
   }
   else // response if false if requested is rejected due to priority
   {
      ::std::map<int, int>::iterator itr = _PendingHKRegRequests.find(_RequestedHKReg);
      if (itr != _PendingHKRegRequests.end())
      {
         _QueuedHKRegRequests[itr->first] = itr->second;
         _PendingHKRegRequests.erase(itr);
      }
   }
   _RequestedHKReg = -1;
   if (!_PendingHKRegRequests.empty())
   {
      ETG_TRACE_USR4(("UI Control:   RegisterHKResponse : look for pending requests"));
      hardKeyRegistrationReq(_PendingHKRegRequests.begin()->first, _PendingHKRegRequests.begin()->second);
   }
}


void UserInterfaceControl::hardKeyDeregistrationRes(int reponse)
{
   if (reponse && (_RequestedHKDereg != -1))
   {
      ::std::map<int, int>::iterator itr = _RegisteredHKs.find(_RequestedHKDereg);
      ::std::vector<int>::iterator itr_v = ::std::find(_PendingHKDeregRequests.begin(), _PendingHKDeregRequests.end(), _RequestedHKDereg);
      if (itr != _RegisteredHKs.end())
      {
         _RegisteredHKs.erase(itr);
      }
      if (itr_v != _PendingHKDeregRequests.end())
      {
         _PendingHKDeregRequests.erase(itr_v);
      }
      _RequestedHKDereg = -1;
   }
   else
   {
      if (_PendingHKDeregRequests.size())
      {
         int keycode = _PendingHKDeregRequests.back();
         _PendingHKDeregRequests.pop_back();
         hardKeyDeregistrationReq(keycode);
      }
   }
}


void UserInterfaceControl::allHardKeyDeregistrationRes(int reponse)
{
   if (reponse)
   {
      _RegisteredHKs.clear();
      _RequestedHKDereg = -1;
   }
}


void UserInterfaceControl::applicationLockStateRes(int reponse)
{
   if (_RequestedLockState == reponse)
   {
      _RequestedLockState = -1;
   }
}


void UserInterfaceControl::vShowScreenSaver()
{
   _pUserInterfaceProxy->showScreenSaver();
}


void UserInterfaceControl::vSetAvailableScreenSavers(::std::vector< bosch::cm::ai::nissan::hmi::userinterfacectrl::UserInterfaceCtrl::ScreenSaverInfo > screenSaver)
{
   _pUserInterfaceProxy->sendAvailableScreenSavers(screenSaver);
}


void UserInterfaceControl::removePendingHKReg(int keyCode)
{
   ::std::map<int, int>::iterator itr = _PendingHKRegRequests.find(keyCode);
   if (itr != _PendingHKRegRequests.end())
   {
      _PendingHKRegRequests.erase(itr);
   }
   ::std::map<int, int>::iterator itr1 = _QueuedHKRegRequests.find(keyCode);
   if (itr1 != _QueuedHKRegRequests.end())
   {
      _QueuedHKRegRequests.erase(itr1);
   }
   if (keyCode == _RequestedHKReg)
   {
      _RequestedHKReg = -1;
   }
}


void UserInterfaceControl::sendLockApplicationStateUpdate(int applicationId, int lockState)
{
   ::std::vector<IUserInterfaceObserver*> observerList = _ObserverList;
   ::std::vector<IUserInterfaceObserver*>::const_iterator itr = observerList.begin();
   for (; itr != observerList.end(); itr++)
   {
      if (*itr != NULL)
      {
         (*itr)->lockStateApplicationUpdate(applicationId, (lockState != 0));
      }
   }
}
