/*
 * UserInterfaceControl.h
 *
 *  Created on: Sep 4, 2015
 *      Author: goc1kor
 */

#ifndef USERINTERFACECONTROL_H_
#define USERINTERFACECONTROL_H_

#include "ProjectBaseTypes.h"
#include "IUserInterfaceProxyImpl.h"
#include "IUserInterfaceScreenSaverImpl.h"

class UserInterfaceProxy;
class IUserInterfaceObserver;
class UserInterfaceScreenSaver;

class UserInterfaceControl: public IUserInterfaceProxyImpl , public IUserInterfaceScreenSaverImpl
{
   public:
      UserInterfaceControl(UserInterfaceScreenSaver* pScreenSaver, uint32 applicationId = APPID_APPHMI_UNKNOWN);
      UserInterfaceControl();
      virtual ~UserInterfaceControl();
      static UserInterfaceControl* getInstance();

      void registerLockApplicationUpdate(IUserInterfaceObserver* pIUserInterfaceObserver);
      void deregisterLockApplicationUpdate(IUserInterfaceObserver* pIUserInterfaceObserver);

      void setApplicationId(int applicationId);
      void setScreenSaverMode(int screenSaverMode);
      bool hardKeyRegistrationReq(int keyCode, int priority);
      bool hardKeyDeregistrationReq(int keyCode);
      bool hardKeyDeRegistrationAllReq();
      bool lockApplicationChangeReq(bool lockState);

      virtual void applicationLockStateUpdate(int applicationId, int lockState);
      virtual void hardKeyRegistrationUpdate(int applicationId, int KeyCode);
      virtual void hardKeyRegistrationRes(int reponse);
      virtual void hardKeyDeregistrationRes(int reponse);
      virtual void allHardKeyDeregistrationRes(int reponse);
      virtual void applicationLockStateRes(int reponse);
      virtual void onNewScreenSaverMode(int screenSaverMode, uint32 applicationId);

      virtual void vShowScreenSaver();
      virtual void vSetAvailableScreenSavers(::std::vector< bosch::cm::ai::nissan::hmi::userinterfacectrl::UserInterfaceCtrl::ScreenSaverInfo > screenSaver);

   private:
      UserInterfaceProxy* _pUserInterfaceProxy;

      void removePendingHKReg(int keyCode);
      void sendLockApplicationStateUpdate(int applicationId, int lockState);
      ::std::map<int, int> _PendingHKRegRequests;
      ::std::map<int, int> _QueuedHKRegRequests;
      ::std::map<int, int> _RegisteredHKs;
      ::std::vector<int> _PendingHKDeregRequests;
      ::std::vector<IUserInterfaceObserver*> _ObserverList;
      int _RequestedLockState;
      int _RequestedHKReg;
      int _RequestedHKDereg;
      uint32 _applicationId;

      UserInterfaceScreenSaver* _pScreenSaver;
};


#endif /* USERINTERFACECONTROL_H_ */
