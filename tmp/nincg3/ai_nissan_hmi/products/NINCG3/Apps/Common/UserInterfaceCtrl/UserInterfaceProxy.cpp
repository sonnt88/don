/*
 * UserInterfaceProxy.cpp
 *
 *  Created on: Sep 4, 2015
 *      Author: goc1kor
 */

#include "hmi_trace_if.h"
#include "../Common_Trace.h"
#include "UserInterfaceProxy.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_USERINTERFACECTRL
#include "trcGenProj/Header/UserInterfaceProxy.cpp.trc.h"
#endif


using namespace bosch::cm::ai::nissan::hmi::userinterfacectrl::UserInterfaceCtrl;

UserInterfaceProxy::UserInterfaceProxy() : _userInterfaceCtrlProxy(UserInterfaceCtrlProxy::createProxy("userInterfacePort", *this)),
   _pIUserInterfaceProxyImpl(NULL)
{
   StartupSync::getInstance().registerPropertyRegistrationIF(this);
   _hardKeyRegReqId = 0;
   _hardKeyDeregReqId = 0;
   _allHardKeyDeregReqId = 0;
   _lockAppChangeReqId = 0;
}


UserInterfaceProxy::~UserInterfaceProxy()
{
   _userInterfaceCtrlProxy.reset();
}


void UserInterfaceProxy::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_userInterfaceCtrlProxy == proxy)
   {
      _userInterfaceCtrlProxy->sendApplicationLockStateRegister(*this);
      _userInterfaceCtrlProxy->sendHardKeyRegistrationChangedRegister(*this);
      _userInterfaceCtrlProxy->sendScreenSaverModeRegister(*this);
      //Get the startup data in service because dBus will not send
      // startup data update during register
      _userInterfaceCtrlProxy->sendScreenSaverModeGet(*this);
   }
}


void UserInterfaceProxy::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_userInterfaceCtrlProxy == proxy)
   {
      _userInterfaceCtrlProxy->sendApplicationLockStateDeregisterAll();
      _userInterfaceCtrlProxy->sendHardKeyRegistrationChangedDeregisterAll();
      _userInterfaceCtrlProxy->sendScreenSaverModeDeregisterAll();
   }
}


void UserInterfaceProxy::onApplicationLockStateSignal(const ::boost::shared_ptr< UserInterfaceCtrlProxy >& /*proxy*/, const ::boost::shared_ptr< ApplicationLockStateSignal >& signal)
{
   if (_pIUserInterfaceProxyImpl != NULL)
   {
      _pIUserInterfaceProxyImpl->applicationLockStateUpdate(signal->getApplicationId(), signal->getLockState());
   }
}


void UserInterfaceProxy::onDeRegisterAllHardKeysResponse(const ::boost::shared_ptr< UserInterfaceCtrlProxy >& /*proxy*/, const ::boost::shared_ptr< DeRegisterAllHardKeysResponse >& response)
{
   _allHardKeyDeregReqId = 0;
   if (_pIUserInterfaceProxyImpl != NULL)
   {
      _pIUserInterfaceProxyImpl->allHardKeyDeregistrationRes(response->getKeysDeRegistered());
   }
}


void UserInterfaceProxy::onDeRegisterHardKeyResponse(const ::boost::shared_ptr< UserInterfaceCtrlProxy >& /*proxy*/, const ::boost::shared_ptr< DeRegisterHardKeyResponse >& response)
{
   _hardKeyDeregReqId = 0;
   if (_pIUserInterfaceProxyImpl != NULL)
   {
      _pIUserInterfaceProxyImpl->hardKeyDeregistrationRes(response->getKeyDeRegistered());
   }
}


void UserInterfaceProxy::onHardKeyRegistrationChangedSignal(const ::boost::shared_ptr< UserInterfaceCtrlProxy >& /*proxy*/, const ::boost::shared_ptr< HardKeyRegistrationChangedSignal >& signal)
{
   if (_pIUserInterfaceProxyImpl != NULL)
   {
      _pIUserInterfaceProxyImpl->hardKeyRegistrationUpdate(signal->getApplicationId(), signal->getKeyCode());
   }
}


void UserInterfaceProxy::onLockApplicationChangeResponse(const ::boost::shared_ptr< UserInterfaceCtrlProxy >& /*proxy*/, const ::boost::shared_ptr< LockApplicationChangeResponse >& response)
{
//	if (_lockAppChangeReqId == response->getAct())
   if (_lockAppChangeReqId == 1)
   {
      _lockAppChangeReqId = 0;
      if (_pIUserInterfaceProxyImpl != NULL)
      {
         _pIUserInterfaceProxyImpl->applicationLockStateRes(response->getLocked());
      }
      ETG_TRACE_USR4(("UI Proxy:   onLockApplicationChangeResponse passed : ACT for response %d", response->getAct()));
   }
   else
   {
      ETG_TRACE_ERR(("UI Proxy:   onLockApplicationChangeResponse failed : ACT returned on last request %d  and ACT for response %d", _lockAppChangeReqId, response->getAct()));
   }
}


void UserInterfaceProxy::onRegisterHardKeyResponse(const ::boost::shared_ptr< UserInterfaceCtrlProxy >& /*proxy*/, const ::boost::shared_ptr< RegisterHardKeyResponse >& response)
{
   if (_hardKeyRegReqId)
   {
      _hardKeyRegReqId = 0;
      if (_pIUserInterfaceProxyImpl != NULL)
      {
         _pIUserInterfaceProxyImpl->hardKeyRegistrationRes(response->getKeyRegistered());
      }
      ETG_TRACE_USR4(("UI Proxy:   onRegisterHardKeyResponse passed : ACT for response %d", response->getAct()));
   }
   else
   {
      ETG_TRACE_ERR(("UI Proxy:   onRegisterHardKeyResponse failed : ACT returned on last request %d  and ACT for response %d", _hardKeyRegReqId, response->getAct()));
   }
}


bool UserInterfaceProxy::sendHardKeyRegistration(int applicationId, int keyCode, int priority)
{
   bool bRet = false;
   if ((_hardKeyRegReqId == 0) && _userInterfaceCtrlProxy->isAvailable())
   {
      _hardKeyRegReqId = 1;
      _userInterfaceCtrlProxy->sendRegisterHardKeyRequest(*this, keyCode, applicationId, priority);
      ETG_TRACE_USR4(("UI Proxy:   RegisterHKRequest passed : ACT returned on request %d for keyCode %d from application %d", _hardKeyRegReqId, keyCode, applicationId));
      bRet = true;
   }
   else
   {
      ETG_TRACE_ERR(("UI Proxy:   RegisterHKRequest failed : ACT returned on last request %d", _hardKeyRegReqId));
   }
   return bRet;
}


bool UserInterfaceProxy::sendScreenSaverMode(uint32 screenSaverMode,  uint32 applicationId)
{
   ETG_TRACE_USR4(("UI Proxy:   sendScreenSaverMode : %d ", screenSaverMode));
   ETG_TRACE_USR4(("UI Proxy:   applicationId : %d ", applicationId));
   if (_userInterfaceCtrlProxy->isAvailable())
   {
      _userInterfaceCtrlProxy->sendRequestScreenSaverModeRequest(*this, screenSaverMode, applicationId);
   }
   return true;
}


bool UserInterfaceProxy::showScreenSaver()
{
   ETG_TRACE_USR4(("UI Proxy:   showScreenSaver : "));
   if (_userInterfaceCtrlProxy->isAvailable())
   {
      _userInterfaceCtrlProxy->sendShowScreenSaverRequest(*this);
   }
   return true;
}


void UserInterfaceProxy::sendAvailableScreenSavers(::std::vector< bosch::cm::ai::nissan::hmi::userinterfacectrl::UserInterfaceCtrl::ScreenSaverInfo > screenSavers)
{
   ETG_TRACE_USR4(("UI Proxy:   sendAvailableScreenSavers : "));
   if (_userInterfaceCtrlProxy->isAvailable())
   {
      _userInterfaceCtrlProxy->sendSetAvailableScreenSaversRequest(*this, screenSavers);
   }
}


bool UserInterfaceProxy::sendHardKeyDeregistration(int applicationId, int keyCode)
{
   bool bRet = false;
   if ((_hardKeyDeregReqId == 0) && _userInterfaceCtrlProxy->isAvailable())
   {
      _hardKeyDeregReqId = 1;
      _userInterfaceCtrlProxy->sendDeRegisterHardKeyRequest(*this, keyCode, applicationId);
      bRet = true;
   }
   return bRet;
}


bool UserInterfaceProxy::sendAllHardKeyDeregistration(int applicationId)
{
   bool bRet = false;
   if ((_allHardKeyDeregReqId == 0) && _userInterfaceCtrlProxy->isAvailable())
   {
      _allHardKeyDeregReqId = 1;
      _userInterfaceCtrlProxy->sendDeRegisterAllHardKeysRequest(*this, applicationId);
      bRet = true;
   }
   return bRet;
}


bool UserInterfaceProxy::sendLockApplicationChange(int applicationId, bool lockState)
{
   bool bRet = false;
   if ((_lockAppChangeReqId == 0) && _userInterfaceCtrlProxy->isAvailable())
   {
//      _lockAppChangeReqId = _userInterfaceCtrlProxy->sendLockApplicationChangeRequest(*this, applicationId, lockState);
      _lockAppChangeReqId = 1;
      _userInterfaceCtrlProxy->sendLockApplicationChangeRequest(*this, applicationId, lockState);

      ETG_TRACE_USR4(("UI Proxy:   sendLockApplicationChange passed : ACT returned on request %d for lockState %d from application %d", _lockAppChangeReqId, lockState, applicationId));
      bRet = true;
   }
   else
   {
      ETG_TRACE_ERR(("UI Proxy:   sendLockApplicationChange failed : ACT returned on last request %d for lockState %d from application %d", _lockAppChangeReqId, lockState, applicationId));
   }
   return bRet;
}


void UserInterfaceProxy::onScreenSaverModeUpdate(const ::boost::shared_ptr< UserInterfaceCtrlProxy >& /*proxy*/, const ::boost::shared_ptr< ScreenSaverModeUpdate >& update)
{
   if (_pIUserInterfaceProxyImpl != NULL)
   {
      ETG_TRACE_USR4(("UI Proxy:   onScreenSaverModeUpdate : %d ", update->getScreenSaverMode().getScreenSaverMode()));
      _pIUserInterfaceProxyImpl->onNewScreenSaverMode(update->getScreenSaverMode().getScreenSaverMode(), update->getScreenSaverMode().getApplicationId());
   }
}
