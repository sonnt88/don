/*
 * IUserInterfaceObserver.h
 *
 *  Created on: Sep 4, 2015
 *      Author: goc1kor
 */

#ifndef IUSERINTERFACEOBSERVER_H_
#define IUSERINTERFACEOBSERVER_H_

class IUserInterfaceObserver
{
   public:
      virtual ~IUserInterfaceObserver() {}

      virtual void lockStateApplicationUpdate(int applicationId, bool lockState) = 0;
};


#endif /* IUSERINTERFACEOBSERVER_H_ */
