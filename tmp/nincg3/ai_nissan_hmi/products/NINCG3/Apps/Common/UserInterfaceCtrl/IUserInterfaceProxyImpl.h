/*
 * IUserInterfaceProxyImpl.h
 *
 *  Created on: Sep 6, 2015
 *      Author: goc1kor
 */

#ifndef IUSERINTERFACEPROXYIMPL_H_
#define IUSERINTERFACEPROXYIMPL_H_

class IUserInterfaceProxyImpl
{
   public:
      virtual ~IUserInterfaceProxyImpl() {}

      virtual void applicationLockStateUpdate(int applicationId, int lockState) = 0;
      virtual void hardKeyRegistrationUpdate(int applicationId, int KeyCode) = 0;
      virtual void hardKeyRegistrationRes(int reponse) = 0;
      virtual void hardKeyDeregistrationRes(int reponse) = 0;
      virtual void allHardKeyDeregistrationRes(int reponse) = 0;
      virtual void applicationLockStateRes(int reponse) = 0;
      virtual void onNewScreenSaverMode(int screenSaverMode, uint32 applicationId) = 0;
};


#endif /* IUSERINTERFACEPROXYIMPL_H_ */
