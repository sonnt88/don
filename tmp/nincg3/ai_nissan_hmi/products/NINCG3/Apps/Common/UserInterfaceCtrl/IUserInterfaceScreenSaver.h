/*
 * IUserInterfaceScreenSaver.h
 *
 *  Created on: Sep 4, 2015
 *      Author: pad1cob
 */

#ifndef IUserInterfaceScreenSaver_H
#define IUserInterfaceScreenSaver_H

class IUserInterfaceScreenSaver
{
   public:
      virtual ~IUserInterfaceScreenSaver() {}
      IUserInterfaceScreenSaver() {}
      virtual void vOnNewScreenSaverMode(const uint8 screenSaverMode, const uint32 applicationId) = 0;
};


#endif // IUserInterfaceScreenSaver_H
