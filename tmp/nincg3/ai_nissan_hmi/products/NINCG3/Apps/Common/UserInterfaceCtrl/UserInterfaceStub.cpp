/*
 * UserInterfaceStub.cpp
 *
 *  Created on: Sep 3, 2015
 *      Author: goc1kor
 */

#include "ProjectBaseTypes.h"
#include "UserInterfaceStub.h"

using namespace bosch::cm::ai::nissan::hmi::userinterfacectrl::UserInterfaceCtrl;

UserInterfaceStub::UserInterfaceStub(): UserInterfaceCtrlStub("userInterfaceServerPort")
{
   _keyRegContainer.clear();
   _lockedApplication = APPID_APPHMI_UNKNOWN;
   _visibleApplication = APPID_APPHMI_UNKNOWN;
}


UserInterfaceStub::~UserInterfaceStub()
{
}


void UserInterfaceStub::onRegisterHardKeyRequest(const ::boost::shared_ptr< RegisterHardKeyRequest >& request)
{
   bool bRes = false;
   ::std::map<int, RegDetails>::iterator itr = _keyRegContainer.find(request->getKeyCode());
   if ((itr == _keyRegContainer.end()) || (itr->second._priority >= request->getPriority()))
   {
      _keyRegContainer[request->getKeyCode()] = RegDetails(request->getApplicationId(), request->getPriority());
      bRes = true;
   }
   sendRegisterHardKeyResponse(bRes);
   if (bRes)
   {
      sendHardKeyRegistrationChangedSignal(request->getKeyCode(), request->getApplicationId());
   }
}


void UserInterfaceStub::onDeRegisterHardKeyRequest(const ::boost::shared_ptr< DeRegisterHardKeyRequest >& request)
{
   bool bRes = false;
   ::std::map<int, RegDetails>::iterator itr = _keyRegContainer.find(request->getKeyCode());
   if (request->getApplicationId() == itr->second._applicationId)
   {
      if (itr != _keyRegContainer.end())
      {
         _keyRegContainer.erase(itr);
         bRes = true;
      }
   }
   sendDeRegisterHardKeyResponse(bRes);
   if (bRes)
   {
      sendHardKeyRegistrationChangedSignal(request->getKeyCode(), APPID_APPHMI_UNKNOWN);
   }
}


void UserInterfaceStub::onRequestScreenSaverModeRequest(const ::boost::shared_ptr< RequestScreenSaverModeRequest >& request)
{
   ScreenSaverModeInfo screenSaverMode = ScreenSaverModeInfo(request->getScreenSaverMode(), request->getApplicationId());
   setScreenSaverMode(screenSaverMode);
}


void UserInterfaceStub::onDeRegisterAllHardKeysRequest(const ::boost::shared_ptr< DeRegisterAllHardKeysRequest >& request)
{
   int applicationId = request->getApplicationId();
   ::std::map<int, RegDetails> keyRegList = _keyRegContainer;
   ::std::map<int, RegDetails>::iterator itr = keyRegList.begin();
   for (; itr != keyRegList.end(); itr++)
   {
      if (applicationId == itr->second._applicationId)
      {
         _keyRegContainer.erase(itr->first);
      }
   }
   sendDeRegisterAllHardKeysResponse(true);
}


void UserInterfaceStub::onLockApplicationChangeRequest(const ::boost::shared_ptr< LockApplicationChangeRequest >& request)
{
   bool bLockState = false;
   bool appLockchanged = false;
//   if (_visibleApplication == request->getApplicationId())
//   {
   if (request->getLockState())
   {
      _lockedApplication = request->getApplicationId();
      bLockState = true;
      appLockchanged = true;
   }
   else if (request->getApplicationId() == _lockedApplication)
   {
      _lockedApplication = APPID_APPHMI_UNKNOWN;
      appLockchanged = true;
   }
//   }
   sendLockApplicationChangeResponse(bLockState);
   if (appLockchanged)
   {
      updateApplicationLockState(request->getApplicationId(), bLockState);
   }
}


void UserInterfaceStub::updateApplicationLockState(int appId, int lockState)
{
   sendApplicationLockStateSignal(appId, lockState);
}


int UserInterfaceStub::getRegisteredApp(int keyCode)
{
   int nRet = APPID_APPHMI_UNKNOWN;
   ::std::map<int, RegDetails>::iterator itr = _keyRegContainer.find(keyCode);
   if (itr != _keyRegContainer.end())
   {
      nRet = itr->second._applicationId;
   }
   return nRet;
}


int UserInterfaceStub::getLockedApplication()
{
   return _lockedApplication;
}


void UserInterfaceStub::onShowScreenSaverRequest(const ::boost::shared_ptr< ShowScreenSaverRequest >& request)
{
   vShowScreenSaverContext(getHighestPrioContext());
}


void UserInterfaceStub::onSetAvailableScreenSaversRequest(const ::boost::shared_ptr< SetAvailableScreenSaversRequest >& request)
{
   _availableScreenSavers = request->getAvailableScreenSavers();
}


uint32 UserInterfaceStub::getHighestPrioContext()
{
   if (_availableScreenSavers.size())
   {
      uint32 contextId = _availableScreenSavers[0].getContextId();
      for (uint32 index = 1; index < _availableScreenSavers.size(); index++)
      {
         if (contextId < _availableScreenSavers[index].getContextId())
         {
            contextId =  _availableScreenSavers[index].getContextId();
         }
      }
      return contextId;
   }
   return 0;
}


void UserInterfaceStub::vShowScreenSaverContext(uint32)
{
}
