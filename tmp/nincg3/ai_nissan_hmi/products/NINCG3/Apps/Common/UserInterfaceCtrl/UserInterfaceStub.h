/*
 * UserInterfaceStub.h
 *
 *  Created on: Aug 31, 2015
 *      Author: HMI Master
 */

#ifndef USERINTERFACECTRLSTUB_H_
#define USERINTERFACECTRLSTUB_H_

#include "bosch/cm/ai/nissan/hmi/userinterfacectrl/UserInterfaceCtrlStub.h"
#define NS_USERINTERFACECNTRL bosch::cm::ai::nissan::hmi::userinterfacectrl::UserInterfaceCtrl

class UserInterfaceStub: NS_USERINTERFACECNTRL::UserInterfaceCtrlStub
{
   public:
      UserInterfaceStub();
      virtual ~UserInterfaceStub();

      virtual void onRegisterHardKeyRequest(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::RegisterHardKeyRequest >& request);
      virtual void onDeRegisterHardKeyRequest(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::DeRegisterHardKeyRequest >& request);
      virtual void onDeRegisterAllHardKeysRequest(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::DeRegisterAllHardKeysRequest >& request);
      virtual void onLockApplicationChangeRequest(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::LockApplicationChangeRequest >& request);
      virtual void onShowScreenSaverRequest(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::ShowScreenSaverRequest >& request) ;
      virtual void onSetAvailableScreenSaversRequest(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::SetAvailableScreenSaversRequest >& request) ;
      virtual void onRequestScreenSaverModeRequest(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::RequestScreenSaverModeRequest >& request) ;

      int getRegisteredApp(int keyCode);
      int getLockedApplication();
      inline void setVisibleApplication(int appId)
      {
         _visibleApplication = appId;
      }
      inline int getVisibleApplication()
      {
         return _visibleApplication;
      }

      virtual void updateApplicationLockState(int appId, int lockState);
      virtual void vShowScreenSaverContext(uint32 contextId);

   private:
      uint32 getHighestPrioContext();

      struct RegDetails
      {
         int _applicationId;
         int _priority;
         RegDetails() : _applicationId(APPID_APPHMI_UNKNOWN), _priority(-1)
         { }
         RegDetails(int appId, int priority) : _applicationId(appId), _priority(priority)
         { }
      };

      ::std::map<int, RegDetails> _keyRegContainer;
      int _lockedApplication;
      int _visibleApplication;
      ::std::vector< NS_USERINTERFACECNTRL::ScreenSaverInfo > _availableScreenSavers;
};


#endif /* USERINTERFACECTRLSTUB_H_ */
