///////////////////////////////////////////////////////////
//  UserInterfaceScreenSaver.h
//  Implementation of the Class UserInterfaceScreenSaver
//  Created on:      23-Dec-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(UserInterfaceScreenSaver_h)
#define UserInterfaceScreenSaver_h

/**
 * interface class for context proxies to provide contexts to requestors
 * @author pad1cob
 * @version 1.0
 * @created 23-Dec-2015 5:44:42 PM
 */


#include "asf/core/Types.h"
#include <vector>
#include <map>
#include "asf/core/Timer.h"
#include "bosch/cm/ai/nissan/hmi/userinterfacectrl/UserInterfaceCtrl.h"
#include "AppBase/ScreenBrokerClient/IUserInputObserver.h"

class IUserInterfaceScreenSaver;
class IUserInterfaceScreenSaverImpl;

enum ScreenSaverMode
{
   SCREENSAVER_ENABLED,
   SCREENSAVER_DISABLED,
};


class UserInterfaceScreenSaver : public asf::core::TimerCallbackIF , public IUserInputObserver
{
   public:
      UserInterfaceScreenSaver();
      virtual ~UserInterfaceScreenSaver();

      void setUserInterfaceScreenSaverImplementer(IUserInterfaceScreenSaver* screenSaverImplementer)
      {
         _pScreenSaverImplementer = screenSaverImplementer;
      }

      virtual void onInputEvent();
      virtual void vBlockScreenSaver();
      virtual void vAllowScreenSaver();
      void vSetApplicationState(bool bAppInForeGround)
      {
         _bAppInForeGround = bAppInForeGround;
      };
      virtual void vOnNewScreenSaverMode(ScreenSaverMode screenSaverMode, uint32 applicationId);
      virtual void onExpired(asf::core::Timer& timer, boost::shared_ptr<asf::core::TimerPayload> payload);
      virtual void vSupportScreenSaver(uint32 contextId, uint32 priority);

      virtual ScreenSaverMode getScreenSaverMode()
      {
         return _screenSaverMode;
      };
      void vSetScreenSaverRequestor(IUserInterfaceScreenSaverImpl* pScreenSaverImpl);

      static UserInterfaceScreenSaver* _pUserInterfaceScreenSaver;
      static UserInterfaceScreenSaver* getInstance();

   protected:
      virtual void vShowScreenSaver();

   private:

      bool bIsScreenSaverAvailable(uint32 contextId);
      asf::core::Timer _screenSaverTimer;
      ScreenSaverMode _screenSaverMode;
      bool bAllowScreenSaver;
      IUserInterfaceScreenSaver* _pScreenSaverImplementer; //to notify the UserInterfaceScreenSaver updates to lib users
      IUserInterfaceScreenSaverImpl* _pScreenSaverImpl;
      ::std::vector< bosch::cm::ai::nissan::hmi::userinterfacectrl::UserInterfaceCtrl::ScreenSaverInfo > _availableScreenSavers;
      act_t timerToken;
      bool _bAppInForeGround;
};


#endif // !defined(UserInterfaceScreenSaver_h)
