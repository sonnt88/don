/*
 * UserInterfaceProxy.h
 *
 *  Created on: Aug 31, 2015
 *      Author: HMI Master
 */

#ifndef USERINTERFACEPROXY_H_
#define USERINTERFACEPROXY_H_

#include "bosch/cm/ai/nissan/hmi/userinterfacectrl/UserInterfaceCtrlClientBase.h"
#include "IUserInterfaceProxyImpl.h"
#include "AppBase/ServiceAvailableIF.h"
#define NS_USERINTERFACECNTRL bosch::cm::ai::nissan::hmi::userinterfacectrl::UserInterfaceCtrl

class UserInterfaceProxy: public hmibase::ServiceAvailableIF
   , public StartupSync::PropertyRegistrationIF
   , public NS_USERINTERFACECNTRL::UserInterfaceCtrlClientBase
{
   public:
      UserInterfaceProxy();
      virtual ~UserInterfaceProxy();

      // ServiceAvailableIF
      virtual void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange);
      virtual void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange);

      // Callback 'ApplicationLockStateCallbackIF'

      virtual void onApplicationLockStateError(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::UserInterfaceCtrlProxy >& proxy, const ::boost::shared_ptr< NS_USERINTERFACECNTRL::ApplicationLockStateError >& error) {}

      virtual void onApplicationLockStateSignal(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::UserInterfaceCtrlProxy >& proxy, const ::boost::shared_ptr< NS_USERINTERFACECNTRL::ApplicationLockStateSignal >& signal);

      // Callback 'DeRegisterAllHardKeysCallbackIF'

      virtual void onDeRegisterAllHardKeysError(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::UserInterfaceCtrlProxy >& proxy, const ::boost::shared_ptr< NS_USERINTERFACECNTRL::DeRegisterAllHardKeysError >& error)
      {
         _allHardKeyDeregReqId = 0;
      }

      virtual void onDeRegisterAllHardKeysResponse(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::UserInterfaceCtrlProxy >& proxy, const ::boost::shared_ptr< NS_USERINTERFACECNTRL::DeRegisterAllHardKeysResponse >& response);

      // Callback 'DeRegisterHardKeyCallbackIF'

      virtual void onDeRegisterHardKeyError(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::UserInterfaceCtrlProxy >& proxy, const ::boost::shared_ptr< NS_USERINTERFACECNTRL::DeRegisterHardKeyError >& error)
      {
         _hardKeyDeregReqId = 0;
      }

      virtual void onDeRegisterHardKeyResponse(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::UserInterfaceCtrlProxy >& proxy, const ::boost::shared_ptr< NS_USERINTERFACECNTRL::DeRegisterHardKeyResponse >& response);

      // Callback 'HardKeyRegistrationChangedCallbackIF'

      virtual void onHardKeyRegistrationChangedError(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::UserInterfaceCtrlProxy >& proxy, const ::boost::shared_ptr< NS_USERINTERFACECNTRL::HardKeyRegistrationChangedError >& error) {}

      virtual void onHardKeyRegistrationChangedSignal(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::UserInterfaceCtrlProxy >& proxy, const ::boost::shared_ptr< NS_USERINTERFACECNTRL::HardKeyRegistrationChangedSignal >& signal);

      // Callback 'LockApplicationChangeCallbackIF'

      virtual void onLockApplicationChangeError(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::UserInterfaceCtrlProxy >& proxy, const ::boost::shared_ptr< NS_USERINTERFACECNTRL::LockApplicationChangeError >& error)
      {
         _lockAppChangeReqId = 0;
      }

      virtual void onLockApplicationChangeResponse(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::UserInterfaceCtrlProxy >& proxy, const ::boost::shared_ptr< NS_USERINTERFACECNTRL::LockApplicationChangeResponse >& response);

      // Callback 'RegisterHardKeyCallbackIF'

      virtual void onRegisterHardKeyError(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::UserInterfaceCtrlProxy >& proxy, const ::boost::shared_ptr< NS_USERINTERFACECNTRL::RegisterHardKeyError >& error)
      {
         _hardKeyRegReqId = 0;
      }

      // Callback 'ScreenSaverModeCallbackIF'

      virtual void onScreenSaverModeError(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::UserInterfaceCtrlProxy >& proxy, const ::boost::shared_ptr< NS_USERINTERFACECNTRL::ScreenSaverModeError >& error) {}

      virtual void onScreenSaverModeUpdate(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::UserInterfaceCtrlProxy >& /*proxy*/, const ::boost::shared_ptr< NS_USERINTERFACECNTRL::ScreenSaverModeUpdate >& update);

      virtual void onRegisterHardKeyResponse(const ::boost::shared_ptr< NS_USERINTERFACECNTRL::UserInterfaceCtrlProxy >& proxy, const ::boost::shared_ptr< NS_USERINTERFACECNTRL::RegisterHardKeyResponse >& response);

      bool sendHardKeyRegistration(int applicationId, int keyCode, int priority);
      bool sendHardKeyDeregistration(int applicationId, int keyCode);
      bool sendAllHardKeyDeregistration(int applicationId);
      bool sendLockApplicationChange(int applicationId, bool lockState);
      bool sendScreenSaverMode(uint32 screenSaverMode, uint32 applicationId);
      bool showScreenSaver();
      void sendAvailableScreenSavers(::std::vector< bosch::cm::ai::nissan::hmi::userinterfacectrl::UserInterfaceCtrl::ScreenSaverInfo > screenSavers);
      inline void setUserInterfaceProxyImpl(IUserInterfaceProxyImpl* pIUserInterfaceProxyImpl)
      {
         _pIUserInterfaceProxyImpl = pIUserInterfaceProxyImpl;
      }
   private:
      ::boost::shared_ptr< NS_USERINTERFACECNTRL::UserInterfaceCtrlProxy > _userInterfaceCtrlProxy;
      IUserInterfaceProxyImpl* _pIUserInterfaceProxyImpl;
      act_t _hardKeyRegReqId;
      act_t _hardKeyDeregReqId;
      act_t _allHardKeyDeregReqId;
      act_t _lockAppChangeReqId;
};


#undef NS_USERINTERFACECNTRL
#endif /* USERINTERFACEPROXY_H_ */
