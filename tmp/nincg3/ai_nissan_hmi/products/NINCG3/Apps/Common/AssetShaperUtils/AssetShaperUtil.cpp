/*
 * AssetShaperUtil.cpp
 *
 * @copyright (c) <2016> Robert Bosch Car Multimedia GmbH
 *  Created on: Jan 27, 2016
 *      Author: gug1cob, mel1hi
 */

#include "Common/AssetShaperUtils/AssetShaperUtil.h"
#include "hall_std_if.h"

#include "AppBase/HmiAsfComponentBase.h"

//To read car variant for display aspect ratio
#ifdef DP_DATAPOOL_ID
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_generic_if.h" //for read kds
#endif

#include "hmi_trace_if.h"
#include "../Common_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_VEHICLEDATA
#include "trcGenProj/Header/AssetShaperUtil.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN

namespace Rnaivi {

AssetShaperUtil::AssetShaperUtil()
{
}


AssetShaperUtil::~AssetShaperUtil()
{
}


bool AssetShaperUtil::assetExists(const std::string& path)
{
   struct stat info;
   if (stat(path.c_str(), &info) != 0)
   {
      return false;
   }
   else
   {
      return true;
   }
}


void AssetShaperUtil::getWorkingDir(std::string& cwd)
{
   char cCurrentPath[FILENAME_MAX];
   memset(cCurrentPath, 0, FILENAME_MAX);

   if (getcwd(cCurrentPath, sizeof(cCurrentPath) - 1))
   {
      cwd = cCurrentPath;
   }
}


uint16 AssetShaperUtil::readVariant()
{
   uint8 u8Region[2] = {0};
   uint16 vehicleType = 0;

   if (DP_S32_NO_ERR == DP_s32GetConfigItem("VehicleInformation", "VehicleType", u8Region, 2))
   {
      vehicleType = u8Region[0];
      vehicleType = (((vehicleType << 8) & 0xFF00) | (u8Region[1] & 0xFF));

      ETG_TRACE_USR4(("readVariant: Region Code: %d %d Vehicle Type: 0x%x", u8Region[0], u8Region[1], vehicleType));
   }
   else
   {
      ETG_TRACE_FATAL(("readVariant: Error reading from KDS"));
   }
   return vehicleType;
}


std::string AssetShaperUtil::getVariant(uint16 vehicleType)
{
   switch (vehicleType)
   {
      case VEHICLE_L42P:
         return "/AppHmi_Resources_Asset_15_9.bin";

      case VEHICLE_P32R:
         return "/AppHmi_Resources_Asset_17_9.bin";

      default:
         return "/AppHmi_Resources_Asset_15_9.bin";
   }
   return "";
}


std::string AssetShaperUtil::getResourceAssetFileName()
{
   std::string assetFileName;

   uint16 vehicleType = readVariant();

   assetFileName = DEFAULT_ASSET_PATH;
   assetFileName += getVariant(vehicleType);

   if (!assetExists(assetFileName))
   {
      std::string defaultcwd;

      getWorkingDir(defaultcwd);
      assetFileName = defaultcwd;
      assetFileName += getVariant(vehicleType);

      if (!assetExists(assetFileName))
      {
         ETG_TRACE_FATAL(("getResourceAssetFileName: Error Opening ASSET File %s does not exist!", assetFileName.c_str()));
         return "";
      }
   }

   ETG_TRACE_USR1(("Resource ASSET filename: %s", assetFileName.c_str()));
   return assetFileName;
}


std::vector<std::string>  AssetShaperUtil::getAssetFiles(std::string assetFileName)
{
   std::vector<std::string> assetFileNames;

   // For the first ASSET application specific
   assetFileNames.push_back(assetFileName);

   // For variant partitioned ASSET
   std::string resourcesAssetFile = getResourceAssetFileName();

   assetFileNames.push_back(resourcesAssetFile);

   return assetFileNames;
}


std::string AssetShaperUtil::getDefaultAssetFileName()
{
   std::string assetFileName = DEFAULT_ASSET_PATH;
   assetFileName += HmiAsfComponentBase::getAppName();
   assetFileName += "_Asset.bin";

   if (!assetExists(assetFileName))
   {
      std::string defaultcwd;

      getWorkingDir(defaultcwd);

      assetFileName.clear();
      assetFileName = defaultcwd;
      assetFileName += "/";
      assetFileName += HmiAsfComponentBase::getAppName();
      assetFileName += "_Asset.bin";

      if (!assetExists(assetFileName))
      {
         ETG_TRACE_FATAL(("getResourceAssetFileName: Error Opening ASSET File %s does not exist!", assetFileName.c_str()));
         return "";
      }
   }

   ETG_TRACE_USR1(("Default Application ASSET filename: %s", assetFileName.c_str()));

   return assetFileName;
}


} /* namespace Rnaivi */
