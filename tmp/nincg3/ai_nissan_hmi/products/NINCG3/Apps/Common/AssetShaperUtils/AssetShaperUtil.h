/*
 * AssetShaperUtil.h
 *
 *  Created on: Jan 27, 2016
 *      Author: gug1cob
 */

#ifndef ASSETSHAPERUTIL_H_
#define ASSETSHAPERUTIL_H_

#include "sys_std_if.h"
#include "asf/core/Types.h"

// Default ASSET Path
#define DEFAULT_ASSET_PATH "/opt/bosch/base/bin/"

namespace Rnaivi {

// See AllianceVehicleType in AIVI Diagnostic Services
enum AIVIVehicleType
{
   VEHICLE_P32R = 0x3252,
   VEHICLE_L42P = 0x4250
};


// See CMDisplayOrientationType in PDD
enum AIVIDisplayOrientationType
{
   DISP_LANDSCAPE = 0x00,
   DISP_PORTRAIT = 0x01
};


// See CMDisplayAspectRatioType in PDD
enum AIVIisplayAspectRatioType
{
   ASPECT_UKNOWN = 0x00,
   ASPECT_15_9 = 0x01,
   ASPECT_17_9 = 0x01
};


class AssetShaperUtil
{
   public:
      AssetShaperUtil();
      virtual ~AssetShaperUtil();
      static std::string getDefaultAssetFileName();
      static std::vector<std::string> getAssetFiles(std::string assetFileName);

   private:

      static bool assetExists(const std::string& path);
      static void getWorkingDir(std::string& cwd);
      static uint16 readVariant();
      static std::string getVariant(uint16 vehicleType);
      static std::string getResourceAssetFileName();
};


} /* namespace Rnaivi */
#endif /* ASSETSHAPERUTIL_H_ */
