/*
 * HmiInfoStubHandler.h
 *
 *  Created on: March 22, 2016
 *      Author: HMI Testmode team
 */

#ifndef HMIINFOSTUBHANDLER_H_
#define HMIINFOSTUBHANDLER_H_

#include "bosch/cm/ai/nissan/hmi/hmiinfoservice/HmiInfoServiceStub.h"
#include "ProjectBaseMsgs.h"

#define HMI_INFO_SERVICE_NS bosch::cm::ai::nissan::hmi::hmiinfoservice::HmiInfoService

namespace App {
namespace Core {

class HmiInfoStubHandler: public HMI_INFO_SERVICE_NS::HmiInfoServiceStub
{
   public:
      HmiInfoStubHandler();
      virtual ~HmiInfoStubHandler();
      virtual void onSetActiveRenderedViewRequest(const ::boost::shared_ptr< HMI_INFO_SERVICE_NS::SetActiveRenderedViewRequest >& request) ;

      bool onCourierMessage(const ActiveAppIDMsg&);

      COURIER_MSG_MAP_BEGIN(0)
      ON_COURIER_MESSAGE(ActiveAppIDMsg)
      COURIER_MSG_MAP_DELEGATE_DEF_BEGIN()
      COURIER_MSG_MAP_DELEGATE_END()
   private:
      std::map< unsigned int, std::string > _hmiRenderedViewMap;
      unsigned int _activeApplicationID;
      unsigned int _previousApplictionID;
};


}
}


#endif /* HMIINFOSTUBHANDLER_H_ */
