/*
 * HmiInfoStubHandler.cpp
 *
 *  Created on: April 04, 2016
 *      Author: HMI Testmode team
 */
#include "hall_std_if.h"
#include "HmiInfoStubHandler.h"
#include "hmi_trace_if.h"
#include "../Common_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_HMIINFO
#include "trcGenProj/Header/HmiInfoStubHandler.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN

using namespace bosch::cm::ai::nissan::hmi::hmiinfoservice::HmiInfoService;

namespace App {
namespace Core {

/**
 * Constructor to create an instance and HmiInfoServiceStub port
 */

HmiInfoStubHandler::HmiInfoStubHandler() : HmiInfoServiceStub("hmiInfoServicePort")
{
   ETG_TRACE_USR4((" HmiInfoStubHandler constructor is invoked "));
   //TODO - For loop Map iniatlization from enum enApplicationId
   _hmiRenderedViewMap.clear();
   _hmiRenderedViewMap.insert(std::make_pair(APPID_APPHMI_MASTER , ""));
   _hmiRenderedViewMap.insert(std::make_pair(APPID_APPHMI_TUNER , ""));
   _hmiRenderedViewMap.insert(std::make_pair(APPID_APPHMI_NAVIGATION , ""));
   _hmiRenderedViewMap.insert(std::make_pair(APPID_APPHMI_MEDIA , ""));
   _hmiRenderedViewMap.insert(std::make_pair(APPID_APPHMI_SYSTEM , ""));
   _hmiRenderedViewMap.insert(std::make_pair(APPID_APPHMI_SXM , ""));
   _hmiRenderedViewMap.insert(std::make_pair(APPID_APPHMI_TELEMATICS , ""));
   _hmiRenderedViewMap.insert(std::make_pair(APPID_APPHMI_TESTMODE , ""));
   _hmiRenderedViewMap.insert(std::make_pair(APPID_APPHMI_PHONE , ""));
   _hmiRenderedViewMap.insert(std::make_pair(APPID_APPHMI_HOMESCREEN , ""));
   _hmiRenderedViewMap.insert(std::make_pair(APPID_APPHMI_VEHICLE , ""));
   _hmiRenderedViewMap.insert(std::make_pair(APPID_APPHMI_SDS , ""));
   _activeApplicationID = APPID_APPHMI_UNKNOWN;
   _previousApplictionID = APPID_APPHMI_UNKNOWN;
}


/**
 * Destructor
 */
HmiInfoStubHandler::~HmiInfoStubHandler()
{
   ETG_TRACE_USR4((" HmiInfoStubHandler Destructor is invoked "));
   _hmiRenderedViewMap.clear();
   _activeApplicationID = APPID_APPHMI_UNKNOWN;
   _previousApplictionID = APPID_APPHMI_UNKNOWN;
}


/**
 * onSetActiveRenderedViewRequest method
 * @param[in] SetActiveRenderedViewRequest object
 */
void HmiInfoStubHandler::onSetActiveRenderedViewRequest(const ::boost::shared_ptr< ::SetActiveRenderedViewRequest >& request)
{
   ETG_TRACE_USR4((" HmiInfoStubHandler::onSetVisibleForegroundSceneRequest is invoked "));

   std::map< unsigned int, std::string >::iterator hmiRenderedViewMapIterator;
   ETG_TRACE_USR4((" HmiInfoStubHandler::onSetVisibleForegroundSceneRequest - SurfaceID = %d " , request->getSurfaceId()));
   //Enums are filtered only for scene surface IDs
   if ((request->getSurfaceId() >= SURFACEID_MAIN_SURFACE_MASTER) && (request->getSurfaceId() <= SURFACEID_MAIN_SURFACE_SDS))
   {
      ETG_TRACE_USR4((" HmiInfoStubHandler::onSetVisibleForegroundSceneRequest - SCENE"));
      //Extract appID from surfaceID
      int appId = (request->getSurfaceId() % 100);
      ETG_TRACE_USR4((" HmiInfoStubHandler::onSetVisibleForegroundSceneRequest - SurfaceID = %d : AppId = %d " , request->getSurfaceId() , appId));

      hmiRenderedViewMapIterator = _hmiRenderedViewMap.find(appId);
      if (hmiRenderedViewMapIterator != _hmiRenderedViewMap.end())
      {
         if (request->hasViewName())
         {
            //save view name in _hmiRenderedViewMap
            hmiRenderedViewMapIterator->second = request->getViewName();
         }
         ETG_TRACE_USR4(("HmiInfoStubHandler::onSetActiveRenderedViewRequest-SceneName = %s", request->getViewName().c_str()));
      }

      //SetActiveRenderedViewRequest is called even though ActiveAppIDMsg is not triggered
      //In that case check for appId from SetActiveRenderedViewRequest msg and comapre against _activeApplicationID which is set in ActiveAppIDMsg
      if ((_activeApplicationID != APPID_APPHMI_UNKNOWN) && (appId == _activeApplicationID))
      {
         hmiRenderedViewMapIterator = _hmiRenderedViewMap.find(_activeApplicationID);
         //fetch active rendered scene name from _hmiRenderedViewMap
         if (hmiRenderedViewMapIterator != _hmiRenderedViewMap.end())
         {
            //sendCurrentVisibleSceneGetUpdate() will be triggered automatically when new view name is set
            setCurrentVisibleScene(hmiRenderedViewMapIterator->second);
            ETG_TRACE_USR4(("HmiInfoStubHandler::sendActiveRenderedViewUpdate is called"));
         }
      }
   }
   else if (((request->getSurfaceId() >= SURFACEID_CENTER_POPUP_SURFACE_MASTER) && (request->getSurfaceId() <= SURFACEID_CENTER_POPUP_SURFACE_SDS))
            || ((request->getSurfaceId() >= SURFACEID_TOP_POPUP_SURFACE_MASTER) && (request->getSurfaceId() <= SURFACEID_TOP_POPUP_SURFACE_SDS)))
   {
      ETG_TRACE_USR4((" HmiInfoStubHandler::onSetVisibleForegroundSceneRequest - POPUP"));
      if ((request->hasViewName()) && (request->getViewName() != "\0"))
      {
         ETG_TRACE_USR4(("HmiInfoStubHandler::onSetActiveRenderedViewRequest-SceneName = %s", request->getViewName().c_str()));
         //Center Popup and Top Poup is set and sendCurrentVisibleSceneGetUpdate() will be triggered automatically
         setCurrentVisibleScene(request->getViewName());
         ETG_TRACE_USR4(("HmiInfoStubHandler::sendActiveRenderedViewUpdate is called with PopupName"));
      }
      else
      {
         //When popup goes Invisible - ViewName is Set as 0 and  active rendered scene name is fetched from _hmiRenderedViewMap
         //This else loop condition is not satisfied as Surface ID seems to be zero. So an workaround is done below.
         if (_activeApplicationID != APPID_APPHMI_UNKNOWN)
         {
            hmiRenderedViewMapIterator = _hmiRenderedViewMap.find(_activeApplicationID);
            //fetch active rendered view name from _hmiRenderedViewMap
            if (hmiRenderedViewMapIterator != _hmiRenderedViewMap.end())
            {
               //sendCurrentVisibleSceneGetUpdate() will be triggered automatically when new view name is set
               setCurrentVisibleScene(hmiRenderedViewMapIterator->second);
               ETG_TRACE_USR4(("HmiInfoStubHandler::sendActiveRenderedViewUpdate is called with previous SceneName"));
            }
         }
      }
   }
   else if (request->getSurfaceId() == 0) //workaround for dynamic renderTarget Loading :for invisble popup state-surface ID seems to be Zero
   {
      if (_activeApplicationID != APPID_APPHMI_UNKNOWN)
      {
         hmiRenderedViewMapIterator = _hmiRenderedViewMap.find(_activeApplicationID);
         // active rendered scene name is fetched from _hmiRenderedViewMap
         if (hmiRenderedViewMapIterator != _hmiRenderedViewMap.end())
         {
            //sendCurrentVisibleSceneGetUpdate() will be triggered automatically when new view name is set
            setCurrentVisibleScene(hmiRenderedViewMapIterator->second);
            ETG_TRACE_USR4(("HmiInfoStubHandler::sendActiveRenderedViewUpdate is called with previous SceneName and SurfaceID is zero"));
         }
      }
   }

   //sendSetActiveRenderedViewResponse is mandatory to be fired to complete request
   sendSetActiveRenderedViewResponse();
}


/**
 * onCourierMessage - To setActiveRenderedView on application switch
 * @param[in] ActiveAppIDMsg& oMsg
 * @parm[out] None
 * @return bool
 */
bool HmiInfoStubHandler::onCourierMessage(const ActiveAppIDMsg& oMsg)
{
   ETG_TRACE_USR4(("HmiInfoStubHandler::onCourierMessage::ActiveAppIDMsg is invoked"));
   std::map< unsigned int, std::string >::iterator hmiRenderedViewMapIterator;
   _activeApplicationID = oMsg.GetApplicationId();

   //To make sure on applciation context switch : setActiveRenderedView method is called
   if (_activeApplicationID != _previousApplictionID)
   {
      hmiRenderedViewMapIterator = _hmiRenderedViewMap.find(_activeApplicationID);
      if (hmiRenderedViewMapIterator != _hmiRenderedViewMap.end())
      {
         //sendCurrentVisibleSceneGetUpdate() will be triggered automatically when new view name is set
         setCurrentVisibleScene(hmiRenderedViewMapIterator->second);
         ETG_TRACE_USR4(("HmiInfoStubHandler::sendActiveRenderedViewUpdate is called"));
      }

      _previousApplictionID = _activeApplicationID ;
   }

   return true;
}


}
}
