/**
 * @file <HmiInfoProxy.h>
 * @author <Testmode - ECV3>A-IVI
 * @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
 * @addtogroup <AppHmi_Common>
 */

#include "hall_std_if.h"
#include "HmiInfoProxy.h"
#include "hmi_trace_if.h"
#include "../Common_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_HMIINFO
#include "trcGenProj/Header/HmiInfoProxy.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN

using namespace ::bosch::cm::ai::nissan::hmi::hmiinfoservice::HmiInfoService;

HmiInfoProxy* HmiInfoProxy::_hmiInfoProxy = NULL;

/**
 * Constructor to create an instance and HmiInfoServiceProxy
 */

HmiInfoProxy::HmiInfoProxy(): _hmiInfoServiceProxy(::HmiInfoServiceProxy::createProxy("hmiInfoClientPort" , *this))
{
   ETG_I_REGISTER_FILE();
   ETG_TRACE_USR4(("HmiInfoProxy::HmiInfoProxy is invoked"));
   _hmiInfoProxy = this;
   StartupSync::getInstance().registerPropertyRegistrationIF(this);
}


/**
 * Destructor
 */
HmiInfoProxy::~HmiInfoProxy()
{
   ETG_TRACE_USR4(("HmiInfoProxy::~HmiInfoProxy is invoked"));
}


/**
 * GetInstance to provide singleton instance
 */
HmiInfoProxy* HmiInfoProxy::GetInstance()
{
   ETG_TRACE_USR4(("HmiInfoProxy::GetInstance is invoked"));
   if (_hmiInfoProxy == NULL)
   {
      ETG_TRACE_USR4(("HmiInfoProxy::_HmiInfoProxy is NULL")); //should never reach here
      _hmiInfoProxy = new HmiInfoProxy();
   }
   return _hmiInfoProxy;
}


/**
 * s_Destrory to delete instance
 */
void HmiInfoProxy::s_Destrory()
{
   ETG_TRACE_USR4(("HmiInfoProxy::s_Destrory is invoked"));
   if (_hmiInfoProxy != NULL)
   {
      delete _hmiInfoProxy;
      _hmiInfoProxy = NULL;
   }
}


/*
 * onAvailable - To Handle Service Availability
 * @param[in] proxy
 * @param[in] stateChange
 * @param[out] None
 * @return void
 */
void HmiInfoProxy::onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy, const asf::core::ServiceStateChange& stateChange)
{
   ETG_TRACE_USR4(("HmiInfoProxy::onAvailable received"));
   StartupSync::getInstance().onAvailable(proxy, stateChange);
}


/*
 * onUnavailable - To Handle Service Unavailability
 * @param[in] proxy
 * @param[in] stateChange
 * @param[out] None
 * @return void
 */
void HmiInfoProxy::onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy, const asf::core::ServiceStateChange& stateChange)
{
   ETG_TRACE_USR4(("HmiInfoProxy::onUnavailable received"));
   StartupSync::getInstance().onUnavailable(proxy, stateChange);
}


/**
 * To invoke the registerProperties function of the dependent classes.
 *
 * @param[in] <proxy> proxies to middleware server
 * @param     <stateChange> --todo: need to add details
 */
void HmiInfoProxy::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& /*proxy*/, const asf::core::ServiceStateChange& /*stateChange*/)
{
   ETG_TRACE_USR4(("RegisterProperties ---- HmiInfoProxy"));
}


/**
 * To invoke the deregisterProperties function of the dependent classes.
 *
 * @param[in] <proxy> proxies to middleware server
 * @param     <stateChange> --todo: need to add details
 */
void HmiInfoProxy::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& /*proxy*/, const asf::core::ServiceStateChange& /*stateChange*/)
{
   ETG_TRACE_USR4(("DeregisterProperties ---- HmiInfoProxy"));
}


/**
 * SetActiveRenderedView Error callback
 * @param[in] HmiInfoServiceProxy object and SetActiveRenderedViewError Object
 */
void HmiInfoProxy::onSetActiveRenderedViewError(const ::boost::shared_ptr< ::HmiInfoServiceProxy >& /*proxy*/, const ::boost::shared_ptr< ::SetActiveRenderedViewError >& /*error*/)
{
   ETG_TRACE_USR4(("HmiInfoProxy::onSetActiveRenderedViewError is invoked"));
}


/**
 * SetActiveRenderedView Response callback
 * @param[in] HmiInfoServiceProxy object and SetActiveRenderedViewResponse Object
 */
void HmiInfoProxy::onSetActiveRenderedViewResponse(const ::boost::shared_ptr< ::HmiInfoServiceProxy >& /*proxy*/, const ::boost::shared_ptr< ::SetActiveRenderedViewResponse >& /*response*/)
{
   ETG_TRACE_USR4(("HmiInfoProxy::onSetActiveRenderedViewResponse is invoked"));
}


/**
 * onCourierMessage - To sendSetActiveRenderedViewRequest
 * @param[in] ActiveSceneRenderedMsg& oMsg
 * @parm[out] None
 * @return bool
 */
bool HmiInfoProxy::onCourierMessage(const ActiveSceneRenderedMsg& oMsg)
{
   ETG_TRACE_USR4(("HmiInfoProxy::onCourierMessage::ActiveSceneRenderedMsg is invoked"));
   _hmiInfoServiceProxy->sendSetActiveRenderedViewRequest(*this , oMsg.GetViewName().GetCString() , oMsg.GetSurfaceId());
   return true;
}


bool HmiInfoProxy::onCourierMessage(const PopUpStatusUpdMsg& oMsg)
{
   ETG_TRACE_USR4(("HmiInfoProxy::onCourierMessage::PopUpStatusUpdMsg is invoked"));
   if (oMsg.GetPopupVisibility() == true)
   {
      _hmiInfoServiceProxy->sendSetActiveRenderedViewRequest(*this , oMsg.GetPopupName().GetCString(), oMsg.GetSurfaceId());
      ETG_TRACE_USR4(("HmiInfoProxy::onCourierMessage::PopUpStatusUpdMsg-GetPopupVisibility:true"));
   }
   else
   {
      _hmiInfoServiceProxy->sendSetActiveRenderedViewRequest(*this , "\0", oMsg.GetSurfaceId());
      ETG_TRACE_USR4(("HmiInfoProxy::onCourierMessage::PopUpStatusUpdMsg-GetPopupVisibility:false"));
   }
   return true;
}
