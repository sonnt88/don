/**
 * @file <HmiInfoProxy.h>
 * @author <Testmode - ECV3>A-IVI
 * @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
 * @addtogroup <AppHmi_Common>
 */


#ifndef HMI_INFOPROXY_H
#define HMI_INFOPROXY_H

#include "ProjectBaseMsgs.h"
#include "bosch/cm/ai/nissan/hmi/hmiinfoservice/HmiInfoServiceProxy.h"


class IHmiInfoProxy
{
   public:
      IHmiInfoProxy() { }
      virtual ~IHmiInfoProxy() { }
      virtual bool OnMessage(const Courier::Message& Msg) = 0;
};


class HmiInfoProxy: public ::asf::core::ServiceAvailableIF,
   public ::bosch::cm::ai::nissan::hmi::hmiinfoservice::HmiInfoService::SetActiveRenderedViewCallbackIF,
   public IHmiInfoProxy,
   public StartupSync::PropertyRegistrationIF
{
   public:
      /**
       *  Member Function Declaration
       *
       */

      HmiInfoProxy();
      virtual ~HmiInfoProxy();

      static HmiInfoProxy* GetInstance();
      static void s_Destrory();

      /**
       *  Virtual methods from  ServiceAvailableIF
       *
       */
      void onAvailable(const boost::shared_ptr< asf::core::Proxy >& proxy, \
                       const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(const boost::shared_ptr< asf::core::Proxy >& proxy, \
                         const asf::core::ServiceStateChange& stateChange);
      /**
       *  Virtual methods from  PropertyRegistrationIF
       *
       */
      void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, \
                              const asf::core::ServiceStateChange& stateChange);
      void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, \
                                const asf::core::ServiceStateChange& stateChange);

      /**
       *  Virtual methods from SetActiveRenderedViewCallbackIF
       *
       */
      virtual void onSetActiveRenderedViewError(const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmiinfoservice::HmiInfoService::HmiInfoServiceProxy >& proxy, \
            const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmiinfoservice::HmiInfoService::SetActiveRenderedViewError >& error) ;

      virtual void onSetActiveRenderedViewResponse(const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmiinfoservice::HmiInfoService::HmiInfoServiceProxy >& proxy, \
            const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmiinfoservice::HmiInfoService::SetActiveRenderedViewResponse >& response) ;

      bool onCourierMessage(const ActiveSceneRenderedMsg&);
      bool onCourierMessage(const PopUpStatusUpdMsg&);

      COURIER_MSG_MAP_BEGIN(0)
      ON_COURIER_MESSAGE(ActiveSceneRenderedMsg)
      ON_COURIER_MESSAGE(PopUpStatusUpdMsg)
      COURIER_MSG_MAP_DELEGATE_DEF_BEGIN()
      COURIER_MSG_MAP_DELEGATE_END()

   protected:
      /**
       *  Member Variables Declaration
       */

   private:
      /**
       *  Member Variables Declaration
       */
      static HmiInfoProxy* _hmiInfoProxy;
      ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmiinfoservice::HmiInfoService::HmiInfoServiceProxy > _hmiInfoServiceProxy;
};


#endif //HMI_INFOSERVICE_H
