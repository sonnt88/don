/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#include "gui_std_if.h"
#include "hmi_trace_if.h"

///////////////////////////////////////////////////////////////////////////////
//focus manager includes
#include "JoyStickController.h"
#include "Focus/FCommon.h"
#include "Focus/FContainer.h"
#include "Focus/FDataSet.h"
#include "Focus/FData.h"
#include "Focus/FActiveViewGroup.h"
#include "Focus/FGroupTraverser.h"
#include "Focus/FManagerConfig.h"
#include "Focus/FManager.h"
#include "JoyStickUtil.h"

#include "AppUtils/Trace/TraceUtils.h"
#include "Common/Common_Trace.h"
//////// TRACE IF ///////////////////////////////////////////////////
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_FOCUS
#include "trcGenProj/Header/JoyStickController.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN


bool JoyStickController::onMessage(Focus::FSession& session, Focus::FGroup& group, const Focus::FMessage& msg)
{
   if (msg.GetId() == JoystickStatusChangedUpdMsg::ID)
   {
      const JoystickStatusChangedUpdMsg* joystickMsg = Courier::message_cast<const JoystickStatusChangedUpdMsg*>(&msg);
      if (joystickMsg != NULL)
      {
         ETG_TRACE_USR4(("JoyStickController::onMessage() direction=%d, src=%u, visible=%u",
                         joystickMsg->GetDirection(), joystickMsg->GetSource(), _manager.isFocusVisible()));

         if (_manager.getActivityTimer() != NULL)
         {
            _manager.getActivityTimer()->restart();
         }

         //if focus is not visible, generate a new sequence so that preserve focus can correctly reuse the current focus
         if (!_manager.isFocusVisible())
         {
            _manager.generateNewSequenceId();
         }

         bool result = moveFocus(session, group, static_cast<hmibase::FocusDirectionEnum>(joystickMsg->GetDirection()));

         if (result && !_manager.isFocusVisible())
         {
            _manager.setFocusVisible(true);
         }

         //message is consumed even if focus was not changed, this could change if necessary
         return result;
      }
   }
   return false;
}


bool JoyStickController::moveFocus(Focus::FSession& session, Focus::FGroup& group, hmibase::FocusDirectionEnum direction)
{
   ETG_TRACE_USR4(("JoyStickController::moveFocus direction=%d, group=%s", direction, FID_STR(group.getId())));

   bool result = false;

   Focus::FGroup* newGroup = NULL;
   if (_manager.getAvgManager() != NULL)
   {
      Focus::FWidget* originWidget = dynamic_cast<Focus::FWidget*>(_manager.getAvgManager()->getFocus(session));
      if (originWidget == NULL)
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "JoyStickController::moveFocus No widget currently focused!"));
         return false;
      }

      //first search is with a restricted search area
      newGroup = JoyStickUtil::tryGetNewFocusGroup(_manager, session, direction, *originWidget, RestrictedSearchAreaAdjuster());

      if (newGroup != NULL)
      {
         ETG_TRACE_USR4(("JoyStickController::moveFocus focus set to group=%s", FID_STR(newGroup->getId())));
         _manager.getAvgManager()->setFocus(session, newGroup);
         result = true;
      }
      else
      {
         Focus::FWidget* newFocus = NULL;
         newFocus = JoyStickUtil::tryGetNewFocus(_manager, group, direction, *originWidget, RestrictedSearchAreaAdjuster());
         if (newFocus != NULL)
         {
            _manager.getAvgManager()->setFocus(session, newFocus);
            result = true;
         }
      }
   }
   return result;
}
