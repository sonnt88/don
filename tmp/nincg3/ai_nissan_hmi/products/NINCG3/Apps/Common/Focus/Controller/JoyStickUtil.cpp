/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#include "gui_std_if.h"
#include "hall_std_if.h"

///////////////////////////////////////////////////////////////////////////////
//Focus manager includes
#include "JoyStickUtil.h"
#include "Focus/FCommon.h"
#include "Focus/FContainer.h"
#include "Focus/FDataSet.h"
#include "Focus/FData.h"
#include "Focus/FActiveViewGroup.h"
#include "Focus/FGroupTraverser.h"
#include "Focus/FManagerConfig.h"
#include "Focus/FManager.h"
#include "ProjectBaseTypes.h"
///////////////////////////////////////////////////////////////////////////////
//TRACE IF
#include "hmi_trace_if.h"
#include "Common/Common_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_FOCUS
#include "trcGenProj/Header/JoyStickUtil.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN

///////////////////////////////////////////////////////////////////////////////
//To read car variant for joystick presence.
#ifdef DP_DATAPOOL_ID
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_generic_if.h" //for read kds
#endif

#define VEHICLE_L42N 0x424E

void RestrictedSearchAreaAdjuster::adjustUp(const Focus::FRectangle& origin, Focus::FRectangle& searchArea) const
{
   searchArea.SetHeight(origin.GetTop());
}


void RestrictedSearchAreaAdjuster::adjustRight(const Focus::FRectangle& origin, Focus::FRectangle& searchArea) const
{
   searchArea.SetLeft(origin.GetLeft() + origin.GetWidth());
}


void RestrictedSearchAreaAdjuster::adjustDown(const Focus::FRectangle& origin, Focus::FRectangle& searchArea) const
{
   searchArea.SetTop(origin.GetTop() + origin.GetHeight());
}


void RestrictedSearchAreaAdjuster::adjustLeft(const Focus::FRectangle& origin, Focus::FRectangle& searchArea) const
{
   searchArea.SetWidth(origin.GetLeft());
}


Focus::FRectangle JoystickSearchAreaAdjuster::calculateSearchArea(const Focus::FRectangle& origin, hmibase::FocusDirectionEnum direction) const
{
   Focus::FRectangle searchArea(0.0f, 0.0f, -1.0f, -1.0f);

   switch (direction)
   {
      case hmibase::Up:
         adjustUp(origin, searchArea);
         break;

      case hmibase::UpperRight:
         adjustRight(origin, searchArea);
         adjustUp(origin, searchArea);
         break;

      case hmibase::Right:
         adjustRight(origin, searchArea);
         break;

      case hmibase::LowerRight:
         adjustRight(origin, searchArea);
         adjustDown(origin, searchArea);
         break;

      case hmibase::Down:
         adjustDown(origin, searchArea);
         break;

      case hmibase::LowerLeft:
         adjustLeft(origin, searchArea);
         adjustDown(origin, searchArea);
         break;

      case hmibase::Left:
         adjustLeft(origin, searchArea);
         break;

      case hmibase::UpperLeft:
         adjustLeft(origin, searchArea);
         adjustUp(origin, searchArea);
         break;

      default:
         break;
   }

   return searchArea;
}


/*
class ExtendedSearchAreaAdjuster : public JoystickSearchAreaAdjuster
{
public:
	virtual ~ExtendedSearchAreaAdjuster() {}

private:
	virtual void adjustUp(const Focus::FRectangle& origin, Focus::FRectangle& searchArea) const
	{
		searchArea.SetHeight(origin.GetTop() + origin.GetHeight());
	}

	virtual void adjustRight(const Focus::FRectangle& origin, Focus::FRectangle& searchArea) const
	{
		searchArea.SetLeft(origin.GetLeft());
	}

	virtual void adjustDown(const Focus::FRectangle& origin, Focus::FRectangle& searchArea) const
	{
		searchArea.SetTop(origin.GetTop());
	}

	virtual void adjustLeft(const Focus::FRectangle& origin, Focus::FRectangle& searchArea) const
	{
		searchArea.SetWidth(origin.GetLeft() + origin.GetWidth());
	}
};*/

class DirectionalGroupFinderVisitor : public Focus::FGroupVisitor
{
   public:
      DirectionalGroupFinderVisitor(const Focus::FRectangle& searchArea, Focus::FGroup& originGroup, const Focus::FRectangle& originRectangle)
         : _searchArea(searchArea), _originGroup(originGroup), _group(NULL), _distance(0.0f)
      {
         _originX = getMiddleX(originRectangle);
         _originY = getMiddleY(originRectangle);
      }

      Focus::FGroup* getGroup() const
      {
         return _group;
      }
      float getDistance() const
      {
         return _distance;
      }

   private:
      DirectionalGroupFinderVisitor(const DirectionalGroupFinderVisitor&);
      DirectionalGroupFinderVisitor& operator=(const DirectionalGroupFinderVisitor&);

      virtual void visitGroup(Focus::FGroup& group)
      {
         Focus::FRectangle* rectangle = group.getData<Focus::FRectangle>();
         if (isValid(group) && (&_originGroup != &group) && (rectangle != NULL))
         {
            float groupX = getMiddleX(*rectangle);
            float groupY = getMiddleY(*rectangle);

            if (isInRange(_searchArea.GetLeft(), _searchArea.GetWidth(), groupX)
                  && isInRange(_searchArea.GetTop(), _searchArea.GetHeight(), groupY))
            {
               //remember the closest group
               float distance = getDistance(_originX, _originY, groupX, groupY);
               if ((_group == NULL) || (distance < _distance))
               {
                  _group = &group;
                  _distance = distance;

                  ETG_TRACE_USR4(("DirectionalGroupFinderVisitor distance=%f, group=%s",
                                  distance, FID_STR(group.getId())));
               }
            }
         }
      }

      float getDistance(float x1, float y1, float x2, float y2) const
      {
         return Candera::Math::SquareRoot((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
      }

      float getMiddleX(const Focus::FRectangle& r) const
      {
         return r.GetLeft() + r.GetWidth() / 2;
      }
      float getMiddleY(const Focus::FRectangle& r) const
      {
         return r.GetTop() + r.GetHeight() / 2;
      }

      bool isInRange(float min, float range, float value) const
      {
         return (min <= value) && ((range == -1.0f) || (value <= min + range));
      }

      Focus::FRectangle _searchArea;

      Focus::FGroup& _originGroup;
      float _originX;
      float _originY;

      Focus::FGroup* _group;
      float _distance;
};


class DirectionalWidgetFinderVisitor : public Focus::FWidgetVisitor
{
   public:
      DirectionalWidgetFinderVisitor(const Focus::FRectangle& searchArea, Focus::FWidget& originWidget, const Focus::FRectangle& originRectangle)
         : _searchArea(searchArea), _originWidget(originWidget), _widget(NULL), _distance(0.0f)
      {
         _originX = getMiddleX(originRectangle);
         _originY = getMiddleY(originRectangle);
      }

      Focus::FWidget* getWidget() const
      {
         return _widget;
      }
      float getDistance() const
      {
         return _distance;
      }

   private:
      DirectionalWidgetFinderVisitor(const DirectionalWidgetFinderVisitor&);
      DirectionalWidgetFinderVisitor& operator=(const DirectionalWidgetFinderVisitor&);

      virtual void visitWidget(Focus::FWidget& widget)
      {
         Focus::FRectangle* rectangle = widget.getData<Focus::FRectangle>();
         if (isValid(widget) && (&_originWidget != &widget) && (rectangle != NULL))
         {
            float widgetX = getMiddleX(*rectangle);
            float widgetY = getMiddleY(*rectangle);

            if (isInRange(_searchArea.GetLeft(), _searchArea.GetWidth(), widgetX)
                  && isInRange(_searchArea.GetTop(), _searchArea.GetHeight(), widgetY))
            {
               //remember the closest widget
               float distance = getDistance(_originX, _originY, widgetX, widgetY);
               if ((_widget == NULL) || (distance < _distance))
               {
                  _widget = &widget;
                  _distance = distance;

                  ETG_TRACE_USR4(("DirectionalWidgetFinderVisitor distance=%f, widget=%s",
                                  distance, FID_STR(widget.getId())));
               }
            }
         }
      }

      float getDistance(float x1, float y1, float x2, float y2) const
      {
         return Candera::Math::SquareRoot((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
      }

      float getMiddleX(const Focus::FRectangle& r) const
      {
         return r.GetLeft() + r.GetWidth() / 2;
      }
      float getMiddleY(const Focus::FRectangle& r) const
      {
         return r.GetTop() + r.GetHeight() / 2;
      }

      bool isInRange(float min, float range, float value) const
      {
         return (min <= value) && ((range == -1.0f) || (value <= min + range));
      }

      Focus::FRectangle _searchArea;

      Focus::FWidget& _originWidget;
      float _originX;
      float _originY;

      Focus::FWidget* _widget;
      float _distance;
};


/*
Focus::FWidgetConfig* JoyStickUtil::getConfiguredGroup(Focus::FWidgetConfig& group)
{
	Focus::FGroupData* groupData = group.Data.get<Focus::FGroupData>();
	if (groupData != NULL)
	{
		//is a FGroup, now check the group kind
		std::string name = group.getId();
		if (std::string::npos == name.find("/")) //TO-DO: Change the method to find the configured group
		{
			//is not a list, but a configured FocusGroup2D widget
			return &group;
		}
		else
		{
			//try to get the configured group
			Focus::FWidgetConfig* parent = group.getParent();
			if (parent != NULL)
				return getConfiguredGroup(*parent);
		}
	}

	return NULL;
}*/

Focus::FGroup* JoyStickUtil::getConfiguredGroup(Focus::FGroup& group)
{
   std::string name = group.getId();
   if (std::string::npos == name.find("/"))
   {
      return &group;
   }
   else
   {
      Focus::FGroup* parent = group.getParent();
      if (parent != NULL)
      {
         return getConfiguredGroup(*parent);
      }
   }

   return NULL;
}


Focus::FGroup* JoyStickUtil::tryGetNewFocusGroup(Focus::FManager& _manager, Focus::FSession& session, hmibase::FocusDirectionEnum direction,
      Focus::FWidget& /*Widget*/, const JoystickSearchAreaAdjuster& searchAreaAdjuster)
{
   Focus::FRectangle* originRectangle = NULL;

   Focus::FWidget* originWidget = dynamic_cast<Focus::FWidget*>(_manager.getAvgManager()->getFocus(session));
   /*if (originWidget && originWidget->Config.getParent())
   {
   	Focus::FWidgetConfig* currentConfiguredGroup = getConfiguredGroup(*originWidget->Config.getParent());

   	if (currentConfiguredGroup)
   		originRectangle = currentConfiguredGroup->Data.get<Focus::FRectangle>();;
   }*/
   if (originWidget && originWidget->getParent())
   {
      Focus::FGroup* currentConfiguredGroup = getConfiguredGroup(*originWidget->getParent());

      if (currentConfiguredGroup)
      {
         originRectangle = currentConfiguredGroup->getData<Focus::FRectangle>();
      }
   }

   if (originRectangle == NULL)
   {
      return NULL;
   }

   Focus::FRectangle searchArea = searchAreaAdjuster.calculateSearchArea(*originRectangle, direction);
   Focus::FGroup* rootGroup = session.ActiveViewGroup.getRootGroup();

   if (rootGroup)
   {
      DirectionalGroupFinderVisitor groupVisitor(searchArea, *rootGroup, *originRectangle);
      Focus::FDefaultGroupTraverser traverser(groupVisitor, *rootGroup, NULL, true);

      //traverse all descendents to find the nearest neighbour
      while (traverser.advance())
      {
         //nothing to do, just traverse
      }

      return groupVisitor.getGroup();
   }
   return NULL;
}


Focus::FWidget* JoyStickUtil::tryGetNewFocus(Focus::FManager& _manager, Focus::FGroup& group, hmibase::FocusDirectionEnum direction,
      Focus::FWidget& originWidget, const JoystickSearchAreaAdjuster& searchAreaAdjuster)
{
   Focus::FId name = group.getId();
   Focus::FRectangle* originRectangle = originWidget.getData<Focus::FRectangle>();
   if (originRectangle == NULL)
   {
      ETG_TRACE_USR4(("FJoyStickController::tryGetNewFocus No bounds for currently focused widget %s!",
                      FID_STR(originWidget.getId())));
      return NULL;
   }

   Focus::FRectangle searchArea = searchAreaAdjuster.calculateSearchArea(*originRectangle, direction);

   DirectionalWidgetFinderVisitor widgetVisitor(searchArea, originWidget, *originRectangle);

   //if focus is not visible check origin and use it if is valid
   if (!_manager.isFocusVisible() && widgetVisitor.isValid(originWidget))
   {
      return &originWidget;
   }

   Focus::FDefaultGroupTraverser traverser(widgetVisitor, group, NULL, true);

   //traverse all descendents to find the nearest neighbour
   while (traverser.advance())
   {
      //nothing to do, just traverse
   }

   return widgetVisitor.getWidget();
}


tU16 JoyStickUtil::readVarient()
{
   tU8 u8Region[2] = {0};
   tU16 vehicleType = 0;

   if (DP_S32_NO_ERR == DP_s32GetConfigItem("VehicleInformation", "VehicleType", u8Region, 2))
   {
      vehicleType = u8Region[0];
      vehicleType = (((vehicleType << 8) & 0xFF00) | (u8Region[1] & 0xFF));

      ETG_TRACE_USR4(("JoyStickUtil::readVarient %d %d 0x%x", u8Region[0], u8Region[1], vehicleType));
   }
   else
   {
      ETG_TRACE_USR4(("JoyStickUtil::readVarient Error"));
   }
   return vehicleType;
}


bool JoyStickUtil::isJoyStickPresent()
{
   bool result = false;
   tU16 vehicleType = readVarient();

   //Joy stick is present only for L42N from switch matrix
   if (vehicleType != 0 && vehicleType == VEHICLE_L42N)
   {
      result = true;
   }

   return result;
}


class IndicatorWidgetFinderVisitor : public Focus::FWidgetVisitor
{
   public:
      IndicatorWidgetFinderVisitor()
      {
         _isIndicatorList = false;
      }

      virtual bool isIndicatorList()
      {
         return _isIndicatorList;
      }

   private:
      IndicatorWidgetFinderVisitor(const IndicatorWidgetFinderVisitor&);
      IndicatorWidgetFinderVisitor& operator=(const IndicatorWidgetFinderVisitor&);

      virtual void visitWidget(Focus::FWidget& widget)
      {
         Focus::FWidgetData* listElementData = widget.getData<Focus::FWidgetData>();
         if (listElementData != NULL)
         {
            if (listElementData->ControllerSetId == FOCUS_CONTROLLER_SET_ID_LIST_ENTER_KEY)
            {
               _isIndicatorList = true;
            }
         }
      }

      bool _isIndicatorList;
};


bool JoyStickUtil::isListHasIndicators(Focus::FGroup& group)
{
   IndicatorWidgetFinderVisitor indicatorVisitor;
   Focus::FDefaultGroupTraverser traverser(indicatorVisitor, group, NULL, true);

   while (traverser.advance())
   {
      //nothing to do, just traverse
   }

   return indicatorVisitor.isIndicatorList();
}
