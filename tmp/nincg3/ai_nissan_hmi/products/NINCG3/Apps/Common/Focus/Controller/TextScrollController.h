/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#ifndef __MY_TEXT_SCROLL_CONTROLLER_H__
#define __MY_TEXT_SCROLL_CONTROLLER_H__

#include <Focus/FCommon.h>
#include <Focus/FController.h>
#include "Common/Focus/FeatureDefines.h"

namespace Focus {
class FSession;
class FWidgetConfig;
class FWidget;
}


class TextScrollController : public Focus::FController
{
   public:
      TextScrollController(bool invertedRotary = false, bool pagewiseScrolling = false);

      virtual bool onMessage(Focus::FSession& session, Focus::FWidget& group, const Focus::FMessage& msg);

   private:
      TextScrollController(const TextScrollController&);
      TextScrollController& operator=(const TextScrollController&);

      bool _pagewiseScrolling;
      bool _invertedRotary;
};


#endif
