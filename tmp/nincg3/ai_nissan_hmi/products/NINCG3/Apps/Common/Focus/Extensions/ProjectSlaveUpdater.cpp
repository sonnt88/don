/**
*  @file   ProjectSlaveUpdater.cpp
*  @author ECV - vma6cob
*  @copyright (c) 2016 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_app_common
*/

#include "gui_std_if.h"
#include "ProjectSlaveUpdater.h"
#include <Focus/FCommon.h>
#include <Focus/FTask.h>
#include "AppBase/ScreenBrokerClient/ScreenBrokerClient.h"
#include "../RnaiviFocus.h"
#include "Common/Focus/Utils/FocusSurface.h"
#include <Focus/Default/FDefaultAvgBuilder.h>

#include "Common/Common_Trace.h"
//////// TRACE IF ///////////////////////////////////////////////////
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_FOCUS
#include "trcGenProj/Header/ProjectSlaveUpdater.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN

using namespace Focus;

FTask::Result ProjectSlaveUpdater::execute()
{
   //FTask::Result result = Base::execute();

   FAppStateSharedPtr appState = _manager.getCurrentAppState();
   if ((!appState.PointsToNull())
         && (appState->CurrentFocusInfo.ViewId != Constants::InvalidViewId)
         && (appState->CurrentFocusInfo.WidgetId != Constants::InvalidId))
   {
      Focus::FSession* session = _manager.getSession();
      if (session != NULL)
      {
         ETG_TRACE_USR4(("ProjectSlaveUpdater::execute:got session App:%d", RnaiviFocus::getAppId()));
         Focus::FSessionFocusVisibilityUpdateConfig* visibilityUpdateConfig = session->Data.get<Focus::FSessionFocusVisibilityUpdateConfig>();

         if (visibilityUpdateConfig != NULL)
         {
            ETG_TRACE_USR4(("ProjectSlaveUpdater::execute:shld update:%d App:%d", visibilityUpdateConfig->ShouldUpdate, RnaiviFocus::getAppId()));
            if ((visibilityUpdateConfig->ShouldUpdate))
            {
               _manager.setFocusVisible(true);
            }
         }
      }
      _manager.setFocusVisible(true);
   }
   else
   {
      _manager.setFocusVisible(false);
   }

   unsigned int surfaceId = Rnaivi::FocusSurface::getLastVisibleSurface();
   ETG_TRACE_USR4(("ProjectSlaveUpdater::execute:%d App:%d", Focus::FManager::getInstance().isFocusVisible(), RnaiviFocus::getAppId()));

   if (surfaceId != 0 && Focus::FManager::getInstance().isFocusVisible())
   {
      ETG_TRACE_USR4(("ProjectSlaveUpdater::surfaceid:%d App:%d", surfaceId, RnaiviFocus::getAppId()));
      ScreenBrokerClient::GetInstance().SetInputFocusPublic(surfaceId, true, true);
   }
   return FTask::Completed;
}


bool ProjectSlaveUpdater::isSlaveFocusVisible()
{
   bool result = true;

   ETG_TRACE_USR4(("ProjectSlaveUpdater::isSlaveFocusVisible:%d App:%d", Focus::FManager::getInstance().isFocusVisible(), RnaiviFocus::getAppId()));

   FocusManagerConfigurator* configurator = dynamic_cast<FocusManagerConfigurator*>(RnaiviFocus::getConfigurator());
   if (configurator)
   {
      result = configurator->isAnyItemInFocus();
      ETG_TRACE_USR4(("ProjectSlaveUpdater::isSlaveFocusVisible on View:%d", result));
   }

   return result;
}
