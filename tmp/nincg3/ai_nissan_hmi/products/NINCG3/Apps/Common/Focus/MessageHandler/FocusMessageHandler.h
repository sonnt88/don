/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#ifndef FOCUSMESSAGEHANDLER_H_
#define FOCUSMESSAGEHANDLER_H_

#include <Focus/FManager.h>
#include <Focus/FManagerConfig.h>
#include "Common/Focus/FeatureDefines.h"
#include "Common/Focus/Utils/FocusSurface.h"

///////////////////////////////////////////////////////////////////////////////
class FocusMessageReceiverControl : public Focus::FMsgReceiver
{
   public:
      FocusMessageReceiverControl(Focus::FManager& manager, AppViewHandler& viewHandler) : _manager(manager), _viewHandler(viewHandler) {}

      virtual bool onMessage(const Focus::FMessage& msg);

   private:
      FocusMessageReceiverControl(const FocusMessageReceiverControl&);
      FocusMessageReceiverControl& operator=(const FocusMessageReceiverControl&);

      Focus::FManager& _manager;
      AppViewHandler& _viewHandler;
};


///////////////////////////////////////////////////////////////////////////////
class IpcAdapter;
class FoucsIPCPayloadMessage : public Focus::FMsgReceiver
{
   public:
      FoucsIPCPayloadMessage(Focus::FManager& manager, IpcAdapter& ipcAdapter) :
         _manager(manager),
         _ipcAdapter(ipcAdapter)
      {}

      virtual bool onMessage(const Focus::FMessage& msg);

   private:
      FoucsIPCPayloadMessage(const FoucsIPCPayloadMessage&);
      FoucsIPCPayloadMessage& operator=(const FoucsIPCPayloadMessage&);

      Focus::FManager& _manager;
      IpcAdapter& _ipcAdapter;

      Rnaivi::FocusSurface _focusSurface;
};


#endif /* FOCUSMESSAGEHANDLER_H_ */
