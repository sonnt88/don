/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#include "gui_std_if.h"
#include "hall_std_if.h"

#include "Common/Focus/Utils/FUtils.h"
#include "CgiExtensions/AppViewHandler.h"
#include "Common/Focus/RnaiviFocus.h"

//To read car variant for joystick presence.
#ifdef DP_DATAPOOL_ID
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_generic_if.h" //for read kds
#endif

#include "Common/Common_Trace.h"
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_APPHMI_COMMON_FOCUS
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#include "trcGenProj/Header/FUtils.cpp.trc.h"
#endif

namespace Rnaivi {

FUtils::FUtils()
{
   // TODO Auto-generated constructor stub
}


FUtils::~FUtils()
{
   // TODO Auto-generated destructor stub
}


class ButtonWidgetFinderCallback : public WidgetCheckCallback
{
   public:
      ButtonWidgetFinderCallback(const Courier::Identifier& id) : id(id), _widget(NULL) { }
      virtual ~ButtonWidgetFinderCallback()
      {
         _widget = NULL;
      }

      virtual bool CheckWidget(Candera::Widget2D* widget)
      {
         if ((widget != NULL) && Courier::Identifier(widget->GetName()) == id)
         {
            _widget = widget;
            return true;
         }
         return false;
      }

      Candera::Widget2D* GetWidget() const
      {
         return _widget;
      }

   private:
      Courier::Identifier id;
      Candera::Widget2D* _widget;
};


const char* FUtils::findDestinationWidget(const Courier::Identifier& id, const Courier::ViewId& viewId, AppViewHandler& viewHandler)
{
   ButtonWidgetFinderCallback callback(id);
   WidgetCheckReqMsg checkerMsg(&callback);
   if (viewHandler.FindView(viewId) != NULL)
   {
      viewHandler.FindView(viewId)->OnMessage(checkerMsg);
      if (callback.GetWidget() != NULL)
      {
         return callback.GetWidget()->GetName();
      }
   }

   return NULL;
}


Courier::ViewId FUtils::_view = Courier::ViewId("");
Candera::String FUtils::_widget = "";

void FUtils::saveTouchedButton(Courier::ViewId const& aView, Candera::String const& aWidget)
{
   _view = aView;
   _widget = aWidget;
}


void FUtils::getTouchedButton(Courier::ViewId& aView, Candera::String& aWidget)
{
   aView = _view;
   aWidget = _widget;
}


bool isTunerMainView(std::string viewName)
{
   std::string tunerMainViewList[] = {"Tuner#Scenes#RADIO__MAIN",
                                      "Tuner#Scenes#RADIO__AM_MAIN"
                                     };//only for NAR

   for (int i = 0; i < (sizeof(tunerMainViewList) / sizeof(tunerMainViewList[0])); i++)
   {
      if (tunerMainViewList[i] == viewName)
      {
         return true;
      }
   }

   return false;
}


bool isAudioMainView(std::string viewName)
{
   std::string audioMainViewList[] = {"Media#Scenes#MEDIA__CD_CDMP3_MAIN",
                                      "Media#Scenes#MEDIA__AUX__USB_MAIN",
                                      "Media#Scenes#MEDIA__AUX__IPOD_MAIN",
                                      "Media#Scenes#MEDIA__AUX__BTAUDIO_MAIN"
                                     };

   for (int i = 0; i < (sizeof(audioMainViewList) / sizeof(audioMainViewList[0])); i++)
   {
      if (audioMainViewList[i] == viewName)
      {
         return true;
      }
   }

   return false;
}


const unsigned int JPN_REGION = 0x19;
const unsigned int INVALID_REGION = 0xFF;
const unsigned int OTH_EUR_REGION = 0x06;
const unsigned int TKY_REGION = 0x04;
const unsigned int RUS_REGION = 0x05;
const unsigned int MEX_REGION = 0x02;
const unsigned int GCC_REGION = 0x0A;
const unsigned int ASR_REGION = 0x0C;
const unsigned int NZE_REGION = 0x0C;
const unsigned int SAF_REGION = 0x10;
static uint8 currentRegion = INVALID_REGION;

bool isTuneScrollRegion()
{
   if (currentRegion == INVALID_REGION)
   {
      if (DP_S32_NO_ERR != DP_s32GetConfigItem("VehicleInformation", "DestinationRegion1", &currentRegion, 1))
      {
         ETG_TRACE_ERR(("isTuneScrollRegion:readRegionFromKDS Error"));
      }

      ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "isTuneScrollRegion:readRegionFromKDS: %d", currentRegion));
   }

   if ( //currentRegion == OTH_EUR_REGION ||
      currentRegion == TKY_REGION
      || currentRegion == RUS_REGION
      || currentRegion == MEX_REGION
      || currentRegion == GCC_REGION
      || currentRegion == ASR_REGION
      || currentRegion == NZE_REGION
      || currentRegion == SAF_REGION)
   {
      return true;
   }

   return false;
}


bool FUtils::isNoFocusView(Courier::ViewId const& viewId)
{
   bool result = false;

   if ((isTunerMainView(viewId.CStr()) || isAudioMainView(viewId.CStr())) && isTuneScrollRegion())
   {
      result = true;
   }

   ETG_TRACE_USR4_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "FUtils::isNoFocusView: result=%d view=%s", result, viewId.CStr()));
   return result;
}


bool FUtils::isValidView(const Focus::FMessage& msg)
{
   bool result = true;
   if (Courier::ViewResMsg::ID == msg.GetId())
   {
      const Courier::ViewResMsg* viewResMsg = Courier::message_cast<const Courier::ViewResMsg*>(&msg);
      if (viewResMsg != NULL)
      {
         ETG_TRACE_USR4_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "FUtils::isValidView Action:%d View:%s", viewResMsg->GetViewAction(), viewResMsg->GetViewId().CStr()));

         if (isNoFocusView(viewResMsg->GetViewId()))
         {
            result = false;
         }
      }
   }
   ETG_TRACE_USR4_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "FUtils::isValidView: result=%d", result));
   return result;
}


bool FUtils::isNewView(const Focus::FMessage& msg)
{
   bool result = false;

   if (Courier::ViewResMsg::ID == msg.GetId())
   {
      const Courier::ViewResMsg* viewResMsg = Courier::message_cast<const Courier::ViewResMsg*>(&msg);
      if (viewResMsg != NULL)
      {
         ETG_TRACE_USR4_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "FUtils::isNewView Action:%d View:%s", viewResMsg->GetViewAction(), viewResMsg->GetViewId().CStr()));

         if ((viewResMsg->GetViewAction() == Courier::ViewAction::Create) || (viewResMsg->GetViewAction() == Courier::ViewAction::CreateAll))
         {
            result = true;
         }
      }
   }

   ETG_TRACE_USR4_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "FUtils::isNewView: result=%d", result));
   return result;
}


} /* namespace Rnaivi */
