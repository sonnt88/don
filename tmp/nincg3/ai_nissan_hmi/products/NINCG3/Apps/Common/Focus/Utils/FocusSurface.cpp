/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2016                     */
/* ------------------------------------------------------------------------ */

#include "Common/Focus/Utils/FocusSurface.h"
#include <algorithm>

namespace Rnaivi {

hmibase::hmiappstates FocusSurface::_currentAppState = hmibase::IN_BACKGROUND;
unsigned int FocusSurface::_currentAppSurfaceId = 0;

FocusSurface::FocusSurface()
{
   // TODO Auto-generated constructor stub
}


FocusSurface::~FocusSurface()
{
   // TODO Auto-generated destructor stub
}


bool ignoreSurface(unsigned int surfaceID)
{
   bool result = false;
#define STATUS_BAR_SURFACE 90
   if (surfaceID == STATUS_BAR_SURFACE)
   {
      result = true;
   }

   return result;
}


void FocusSurface::Update(unsigned int surfaceID, int state)
{
   std::vector<unsigned int>::iterator iter = std::find(_activeSurface.begin(), _activeSurface.end(), surfaceID);

   if (iter != _activeSurface.end())
   {
      if (state != hmibase::SURFACESTATE_VISIBLE)
      {
         _activeSurface.erase(iter);
      }
   }
   else
   {
      if (state == hmibase::SURFACESTATE_VISIBLE)
      {
         _activeSurface.push_back(surfaceID);
      }
   }

   if (state == hmibase::SURFACESTATE_VISIBLE && !ignoreSurface(surfaceID))
   {
      _currentAppSurfaceId = surfaceID;
   }

   if (_activeSurface.empty())
   {
      _currentAppSurfaceId = 0;
      _currentAppState = hmibase::IN_BACKGROUND;
   }
   else
   {
      _currentAppState = hmibase::IN_FOREGROUND;
   }
}


bool FocusSurface::isVisible()
{
   return !_activeSurface.empty();
}


} /* namespace Rnaivi */
