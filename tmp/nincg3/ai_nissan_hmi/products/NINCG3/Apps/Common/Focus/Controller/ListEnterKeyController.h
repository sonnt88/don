/**
* @file ListRotaryController.h
* @author RBEI/ECV3 RNAIVI
* @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
* @addtogroup Apphmi_Common
*/

#ifndef LISTENTERYKEYCONTROLLER_H_
#define LISTENTERYKEYCONTROLLER_H_

//#include <Focus/FCommon.h>
#include <Focus/FController.h>

namespace Rnaivi
{

class ListEnterKeyController: public Focus::FController
{
   public:
      ListEnterKeyController();
      virtual ~ListEnterKeyController();

      virtual bool onMessage(Focus::FSession&, Focus::FWidget&, const Focus::FMessage&);
};

} /* namespace Rnaivi */
#endif /* LISTENTERYKEYCONTROLLER_H_ */
