#ifndef __RNAIVI_FOCUS_H__
#define __RNAIVI_FOCUS_H__

#include <Focus/Default/FDefaultManagerConfig.h>
#include "Controller/ListRotaryController.h"
#include "Common/Focus/FeatureDefines.h"
#include "IPC/IpcAdapter.h"
#include "IPC/IpcHandler.h"
#include <pthread.h>
#include <sys/shm.h>

class AppViewHandler;
class DefaultHKtoEnterKeyConverter;
class SetEncoderDirection;
class FocusConsistencyChecker;
class FocusMessageReceiverControl;
class FoucsIPCPayloadMessage;
class RnaiviFocus;

class RnaiviTimer : public Focus::FTimerListener
{
   public:
      RnaiviTimer() {}
      virtual ~RnaiviTimer() {}

      virtual void onRestarted() {}
      virtual void onStopped() {}
      virtual void onExpired();
};


class FocusManagerConfigurator : public Focus::FDefaultManagerConfigurator
{
      typedef Focus::FDefaultManagerConfigurator Base;
   public:
      FocusManagerConfigurator(Focus::FManager& manager, AppViewHandler& viewHandler);
      virtual ~FocusManagerConfigurator();

      virtual bool initialize();
      virtual bool finalize();

      Rnaivi::ListRotaryController* getReversibleListController()
      {
         return _reversibleListController;
      }

      Rnaivi::ListRotaryController* getListController()
      {
         return _listController;
      }
      bool isAnyItemInFocus();
      void hideOtherAppFocus();

   protected:
      virtual bool initializeControllers();
      virtual bool finalizeControllers();

      virtual bool initializeInputMsgChecker();
      virtual bool finalizeInputMsgChecker();

//#ifdef ENABLE_FOCUS_ON_HK_BACK
      virtual bool initializeConsistencyChecker();
      virtual bool finalizeConsistencyChecker();

      virtual bool initializeVisibilityManager();
//#endif

#ifdef FOCUS_ACROSS_APPLICATION
      virtual bool initializeIpcManager();
      virtual bool finalizeIpcManager();

      virtual bool initializeTaskFactory();
      virtual bool finalizeTaskFactory();
#endif

   private:
      FocusManagerConfigurator(const FocusManagerConfigurator&);
      FocusManagerConfigurator& operator=(const FocusManagerConfigurator&);

      DefaultHKtoEnterKeyConverter* _hkConverter;
      FoucsIPCPayloadMessage* _focusIpcPayloadMsg;
      FocusMessageReceiverControl* _focusReceiverControl;

      Rnaivi::ListRotaryController* _reversibleListController;
      Rnaivi::ListRotaryController* _listController;

      RnaiviTimer _timer;
      FocusConsistencyChecker* _consistencyChecker;

      AppViewHandler& _viewHandler;
      IpcHandler* _ipcHandler;
      IpcAdapter* _ipcAdapter;
      Focus::FDefaultIpcManager* _ipcManager;
};


class RnaiviFocus
{
   public:
      static Focus::FDefaultManagerConfigurator* createConfigurator(AppViewHandler& viewHandler);
      static Focus::FDefaultManagerConfigurator* getConfigurator();
      static bool isAnyItemInFocus(AppViewHandler& viewHandler);
      static void hideFocus();
      static void hideAllViewFocus();
      static void hideAllFocus()
      {
         hideAllViewFocus();
         FocusManagerConfigurator* configurator = dynamic_cast<FocusManagerConfigurator*>(RnaiviFocus::getConfigurator());
         if (configurator)
         {
            configurator->hideOtherAppFocus();
         }
      }

      static bool isFocusVisible(Focus::FManager& manager)
      {
         return ((manager.getCurrentFocus().ViewId != Focus::Constants::InvalidViewId) || manager.isFocusVisible());
      }

      //extension for Focus IPC support
      static void setAppId(unsigned int);
      static unsigned int getAppId()
      {
         return _appId;
      }

   private:
      static Focus::FDefaultManagerConfigurator* _configuraotr;
      static unsigned int _appId;
};


#endif
