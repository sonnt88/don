/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#include "gui_std_if.h"

#include "TextScrollController.h"
#include "ProjectBaseTypes.h"
#include "ProjectBaseMsgs.h"

#include <CgiExtensions/AppViewHandler.h>
#include <Focus/FCommon.h>
#include <Focus/FConfigInfo.h>
#include <Focus/FController.h>
#include <Focus/FDataSet.h>
#include <Focus/FSession.h>
#include <Focus/FActiveViewGroup.h>
#include <Focus/FManager.h>
#include <Focus/FGroupTraverser.h>
#include <Focus/Default/FDefaultAvgManager.h>

#include <Widgets/2D/WidgetFinder2D.h>
#include <Widgets/2D/FlexList/FlexListWidget2D.h>
#include <Widgets/2D/FlexList/FlexListWidget2D.h>

#define ETG_S_IMPORT_INTERFACE_GENERIC
#include "Widgets/widget_etg_if.h"

#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_HMI_FW_FOCUS
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#include "trcGenProj/Header/TextScrollController.cpp.trc.h"
#endif

COURIER_LOG_SET_REALM(Candera::Diagnostics::LogRealm::User);

TextScrollController::TextScrollController(bool invertedRotary, bool pagewiseScrolling)
   : _invertedRotary(invertedRotary), _pagewiseScrolling(pagewiseScrolling)
{
}


bool TextScrollController::onMessage(Focus::FSession& /*session*/, Focus::FWidget& /*group*/, const Focus::FMessage& msg)
{
   bool result = false;

   switch (msg.GetId())
   {
      case EncoderStatusChangedUpdMsg::ID:
      {
         const EncoderStatusChangedUpdMsg* encoderMsg = Courier::message_cast<const EncoderStatusChangedUpdMsg*>(&msg);
         if (encoderMsg != NULL)
         {
            ListChangeMsg* listChangeMsg = NULL;
            if (encoderMsg->GetEncSteps() > 0)
            {
               listChangeMsg = COURIER_MESSAGE_NEW(ListChangeMsg)(0, ListChangePageDown, 1);
            }
            else if (encoderMsg->GetEncSteps() < 0)
            {
               listChangeMsg = COURIER_MESSAGE_NEW(ListChangeMsg)(0, ListChangePageUp, 1);
            }

            if (listChangeMsg != NULL)
            {
               listChangeMsg->Post();
               result = true;
            }
         }

         break;
      }

#ifdef SUPPORT_JOYSTICK_TEXT_SCROLL
      case JoystickStatusChangedUpdMsg::ID:
      {
         const JoystickStatusChangedUpdMsg* joystickMsg = Courier::message_cast<const JoystickStatusChangedUpdMsg*>(&msg);

         if (joystickMsg != NULL)
         {
            ListChangeMsg* listChangeMsg = NULL;
            if (joystickMsg->GetDirection() == hmibase::Left)
            {
               listChangeMsg = COURIER_MESSAGE_NEW(ListChangeMsg)(0, ListChangeUp, 1);
            }
            else if (joystickMsg->GetDirection() == hmibase::Right)
            {
               listChangeMsg = COURIER_MESSAGE_NEW(ListChangeMsg)(0, ListChangeDown, 1);
            }

            if (listChangeMsg != NULL)
            {
               listChangeMsg->Post();
               result = true;
            }
         }
         break;
      }
#endif

      default:
         break;
   }

   return result;
}
