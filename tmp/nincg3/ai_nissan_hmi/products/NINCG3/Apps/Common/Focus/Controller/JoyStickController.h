/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#ifndef __JOYSTICK_CONTROLLER_H__
#define __JOYSTICK_CONTROLLER_H__

/*****************************************************************************/
/* FOCUS MANAGER INCLUDES                                                    */
/*****************************************************************************/
#include <Focus/FCommon.h>
#include <Focus/FController.h>

/**************************************************************************/
/* FORWARD DECLARATIONS                                                   */
/**************************************************************************/
class FManager;
class FGroup;
class FWidget;
class FSession;

/**************************************************************************/
/* HEADER DECLARATIONS                                                    */
/**************************************************************************/
#include "Common/Focus/FeatureDefines.h"

/**************************************************************************/
/* ROTARY CONTROLLER                                                      */
/* Handles the joystick related messages.                                 */
/**************************************************************************/

namespace Focus {
class FSession;
class FWidgetConfig;
class FGroup;
}


class JoyStickController : public Focus::FController
{
   public:
      JoyStickController(Focus::FManager& manager) : _manager(manager) {}
      virtual ~JoyStickController() {}

      virtual bool onMessage(Focus::FSession& session, Focus::FGroup& group, const Focus::FMessage& msg);

   private:
      JoyStickController(const JoyStickController&);
      JoyStickController& operator=(const JoyStickController&);

      /* Moves the focus in the specified direction. */
      bool moveFocus(Focus::FSession& session, Focus::FGroup& group, hmibase::FocusDirectionEnum direction);

      Focus::FManager& _manager;
};


#endif
