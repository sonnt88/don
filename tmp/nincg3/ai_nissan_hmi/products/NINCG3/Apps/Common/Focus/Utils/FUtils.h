/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */

#ifndef FUTILS_H_
#define FUTILS_H_

#include <Focus/FCommon.h>
#include <Focus/FManager.h>
#include <Focus/FActiveViewGroup.h>
#include <Focus/FSession.h>

namespace Rnaivi {

class FUtils
{
   public:
      FUtils();
      virtual ~FUtils();

      static bool isCurrentFocusAvailable(Focus::FManager& manager, Focus::FSession& session, Focus::FGroup& group)
      {
         if (manager.getAvgManager() != NULL)
         {
            return manager.isFocusVisible() && manager.getAvgManager()->isCurrentFocusAvailable(session, &group);
         }
         return false;
      }

      static const char* findDestinationWidget(const Courier::Identifier& id, const Courier::ViewId& viewId, AppViewHandler& viewHandler);
      static void saveTouchedButton(Courier::ViewId const& aView, Candera::String const& aWidget);
      static void getTouchedButton(Courier::ViewId& aView, Candera::String& aWidget);
      static bool isValidView(const Focus::FMessage& msg);
      static bool isNewView(const Focus::FMessage& msg);
      static bool isNoFocusView(Courier::ViewId const& viewId);

      static bool isValid(Focus::FViewId& viewId, Focus::FId& widgetID)
      {
         if (viewId != Focus::Constants::InvalidViewId && widgetID != Focus::Constants::InvalidId)
         {
            return true;
         }

         return false;
      }
   private:

      static Courier::ViewId _view;
      static Candera::String _widget;
};


} /* namespace Rnaivi */
#endif /* FUTILS_H_ */
