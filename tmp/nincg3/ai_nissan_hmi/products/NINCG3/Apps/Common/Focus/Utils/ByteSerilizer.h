/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#ifndef __RNAIVI_BYTE_SERILIZER_H__
#define __RNAIVI_BYTE_SERILIZER_H__

#include "Focus/FCommon.h"
#include "Focus/FData.h"
#include "Focus/Default/FDefaultSerializer.h"

//ByteSerilizer is not thread safe, serialization to be done in a single thread
//For deserialization, the data is provided from queue as such. So it is not an issue from sending and receiving point
class ByteSerilizer
{
   public:
      ByteSerilizer();
      virtual ~ByteSerilizer();

      bool populateByteStream(Focus::Serialize::InfoSer& newInfoSer);

      const char* getByteStream() const
      {
         return _text;
      }
      int getStreamLength() const
      {
         return _totalSize;
      }

      //MANDATE: SIZE OF data HAS TO BE AT-LEAST OF length SIZE
      void deserilize(Focus::Serialize::InfoSer& newInfoSer, char* data, int length);

      void printDebug(Focus::Serialize::InfoSer& info);
   protected:
      bool addNumber(int val);
      bool addString(const char* str, int len);

      int getNumber(int& value);
      void getString(std::string& retStr, int length);

   private:
      char* _text;
      unsigned int _totalSize;
      bool _result;

      char* _readData;
      unsigned int _readPosition;
      unsigned int _readLength;
};


#endif
