/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#include "gui_std_if.h"

#include "ProjectBaseMsgs.h"
#include "FocusConsistencyChecker.h"
#include "Common/Focus/Utils/FUtils.h"
#include <Focus/Default/FDefaultCrtAppExchange.h>
#include <Focus/Default/FDefaultAvgBuilder.h>
#include "Common/Focus/Utils/FocusSurface.h"

#include "Common/Common_Trace.h"

//For hide focus
#include "Common/Focus/RnaiviFocus.h"

#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_APPHMI_COMMON_FOCUS
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#include "trcGenProj/Header/FocusConsistencyChecker.cpp.trc.h"
#endif

//#define SHOW_FOCUS_ON_ENTERING_VIEW

///////////////////////////////////////////////////////////////////////////////
FocusConsistencyChecker::FocusConsistencyChecker(Focus::FManager& manager) : _manager(manager), _timer(NULL)
{
   static bool registered = false;
   if (!registered)
   {
      registered = true;
      //this message receiver receives the messages through the focus manager which belongs to the view component
      Courier::ActivationResMsg::Subscribe(Courier::ComponentType::View);
      Courier::ViewResMsg::Subscribe(Courier::ComponentType::View);
   }

   _currentViewId = Focus::Constants::InvalidViewId;
   _timer = new Util::Timer();
}


///////////////////////////////////////////////////////////////////////////////
FocusConsistencyChecker::~FocusConsistencyChecker()
{
   if (_timer != NULL)
   {
      _timer->stop();
      delete _timer;
      _timer = NULL;
   }
}


///////////////////////////////////////////////////////////////////////////////
void FocusConsistencyChecker::restartTimer()
{
   if (_timer != NULL)
   {
      _timer->setTimeout(0, 100);
      _timer->restart();
   }
}


///////////////////////////////////////////////////////////////////////////////
void FocusConsistencyChecker::stopTimer()
{
   if (_timer != NULL)
   {
      _timer->stop();
   }
}


///////////////////////////////////////////////////////////////////////////////
bool FocusConsistencyChecker::onMessage(const Focus::FMessage& msg)
{
   switch (msg.GetId())
   {
      case Courier::ActivationResMsg::ID:
      case Courier::ViewResMsg::ID:
      {
         //restart the timer everytime a view is hidden or shown
         if (_manager.isFocusVisible())
         {
            restartTimer();
         }
#ifdef SHOW_FOCUS_ON_ENTERING_VIEW
         FocusActionEnum action = FOCUS_SHOW;
         FocusTimerActionEnum timerAction = FOCUS_TIMER_STOP;
         FocusReqMsg* focusReqMsg = COURIER_MESSAGE_NEW(FocusReqMsg)(action, timerAction, Courier::ViewId(""), "");

         if (focusReqMsg != NULL)
         {
            focusReqMsg->Post();
         }
#endif

         if (msg.GetId() == Courier::ViewResMsg::ID)
         {
            const Courier::ViewResMsg* viewMsg = Courier::message_cast<const Courier::ViewResMsg*>(&msg);
            if (viewMsg != NULL)
            {
               ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "FocusConsistencyChecker::onMessage AppId:%d action:%d view=%s"
                                   , RnaiviFocus::getAppId(), viewMsg->GetViewAction(), viewMsg->GetViewId().CStr()));

               if (viewMsg->GetViewAction() == Courier::ViewAction::Create || viewMsg->GetViewAction() == Courier::ViewAction::CreateAll)
               {
                  _currentViewId = viewMsg->GetViewId();
               }
            }
         }
      }

      break;

      case TimerExpiredMsg::ID:
      {
         const TimerExpiredMsg* timerMsg = Courier::message_cast<const TimerExpiredMsg*>(&msg);
         if ((timerMsg != NULL) && (timerMsg->GetTimer() != NULL) && (timerMsg->GetTimer() == _timer))
         {
            checkFocus();
         }
         break;
      }

      default:
         break;
   }
   return false;
}


bool FocusConsistencyChecker::checkFocus()
{
   Focus::FTaskManager* taskManager = _manager.getTaskManager();
   Focus::FSessionManager* sessionManager = _manager.getSessionManager();
   Focus::FAvgManager* avgManager = _manager.getAvgManager();

   if ((taskManager != NULL) && (sessionManager != NULL) && (sessionManager->getSession() == NULL)
         && ((_manager.getCurrentFocus().ViewId != Focus::Constants::InvalidViewId) || _manager.isFocusVisible()))
   {
      Focus::FAppCurrentFocusInfo currentFocus = _manager.getCurrentFocus();
      ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "FocusConsistencyChecker::checkConsistency AppId:%d widget=%s"
                          , RnaiviFocus::getAppId(), FID_STR(currentFocus.WidgetId)));

      //begin a new session
      if ((sessionManager->beginSession(Focus::FSession::Master)) && (sessionManager->getSession() != NULL))
      {
         //add tasks to collect data from the visible views, merge the state info and to build the active view group
         taskManager->clearTasks();
         taskManager->addTask(FOCUS_NEW(Focus::FDefaultCourierInfoCollector)(_manager));
         taskManager->addTask(FOCUS_NEW(Focus::FDefaultCrtAppState2SessionUpdater)(_manager));
         taskManager->addTask(FOCUS_NEW(Focus::FDefaultAvgBuilder)(_manager));
         if (taskManager->executeTasks() == Focus::FTask::Completed)
         {
            _manager.printSessionDebugInfo();

            //check if the current focus in still available
            bool isCurrentFocusAvailable = avgManager->isCurrentFocusAvailable(*sessionManager->getSession());

            sessionManager->getSession()->Data.set(Focus::FSessionFocusVisibilityUpdateConfig(false));

            ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "FocusConsistencyChecker::checkConsistency isFocusVisible:%d isCurrentFocusAvailable=%u v=%s"
                                , _manager.isFocusVisible(), isCurrentFocusAvailable, currentFocus.ViewId.CStr()));

            ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "FocusConsistencyChecker::checkConsistency current v=%s"
                                , _currentViewId.CStr()));

            //occurs only if the new view has focus!
            {
               //check if the pointer is assigned to this view's surface.
               checkPointerSurface(_currentViewId);
            }

            if (isCurrentFocusAvailable)
            {
               taskManager->addTask(FOCUS_NEW(Focus::FAvg2FocusInfoUpdater)(_manager));
               taskManager->addTask(FOCUS_NEW(Focus::FDefaultSession2CrtAppStateUpdater)(_manager));
               taskManager->addTask(FOCUS_NEW(Focus::FDefaultCourierInfoPublisher)(_manager));
               taskManager->executeTasks();
            }
            else
            {
               //_manager.hideFocus();
               //RnaiviFocus::hideFocus();
               Focus::FAppStateSharedPtr appState = _manager.getCurrentAppState();
               if (!appState.PointsToNull())
               {
                  if (currentFocus.ViewId != Focus::Constants::InvalidViewId)
                  {
                     Focus::FViewState* viewState = _manager.getCurrentAppState()->getChild(currentFocus.ViewId);
                     //Focus::FViewState* viewState = _manager.getCurrentAppState()->getChild(_currentViewId);
                     if (viewState != NULL)
                     {
                        Focus::FViewFocusVisible* viewFocusVisible = viewState->getData<Focus::FViewFocusVisible>();
                        if (viewFocusVisible != NULL && viewFocusVisible->Visible == true)
                        {
                           ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "FocusConsistencyChecker::checkConsistency HIDE-FOCUS"));
                           RnaiviFocus::hideFocus();
                        }
                     }
                  }
               }
            }
         }
      }
      sessionManager->endSession();
   }
   return true;
}


void FocusConsistencyChecker::checkPointerSurface(::Courier::ViewId /*currentViewId*/)
{
   //get view's surface (1)
   unsigned int surfaceId = Rnaivi::FocusSurface::getLastVisibleSurface();
   //get actual pointer surface (2)
   //to do: ilm_getPropertiesOfSurface provide the detail, but ilm_init needs to be done in common!
   //if not assigned
   //assign the pointer (1)

   ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "FocusConsistencyChecker::checkPointerSurface: update pointer surface to %d", surfaceId));
   ScreenBrokerClient::GetInstance().SetInputFocusPublic(surfaceId, true, true);
}


///////////////////////////////////////////////////////////////////////////////
/*
void FocusConsistencyChecker::checkConsistency()
{
   Focus::FTaskManager* taskManager = _manager.getTaskManager();
   Focus::FSessionManager* sessionManager = _manager.getSessionManager();
   Focus::FAvgManager* avgManager = _manager.getAvgManager();

   if (_manager.isFocusVisible() && (taskManager != NULL)
         && (sessionManager != NULL) && (sessionManager->getSession() == NULL))
   {
      Focus::FAppCurrentFocusInfo currentFocus = _manager.getCurrentFocus();
      ETG_TRACE_USR4(("DefaultFocusConsistencyChecker::checkConsistency widget=%s", FID_STR(currentFocus.WidgetId)));

      if ((currentFocus.ViewId != Focus::Constants::InvalidViewId)
            && (currentFocus.WidgetId != Focus::Constants::InvalidId))
      {
         //begin a new session
         sessionManager->beginSession(Focus::FSession::Master);
         Focus::FSession* session = sessionManager->getSession();
         if (session != NULL)
         {
            //add tasks to collect data from the visible views, merge the state info and to build the active view group
            taskManager->clearTasks();
            taskManager->addTask(new Focus::FDefaultCourierInfoCollector(_manager));
            taskManager->addTask(new Focus::FDefaultCrtAppState2SessionUpdater(_manager));
            taskManager->addTask(new Focus::FDefaultAvgBuilder(_manager));
            if (taskManager->executeTasks() == Focus::FTask::Completed)
            {
               _manager.printSessionDebugInfo();

               //check if the current focus in avg matches the one published
               Focus::Focusable* newFocus = avgManager->getFocus(*session);
               ETG_TRACE_USR4(("DefaultFocusConsistencyChecker::checkConsistency newWidget=%s",
                               (newFocus != NULL ? FID_STR(newFocus->getId()) : "")));

               bool hideFocus = false;
               if ((newFocus == NULL) || (newFocus->getId() != currentFocus.WidgetId))
               {
                  hideFocus = true;
               }
               else
               {
                  //for widgets we also check the view (for groups not because they span across views)
                  Focus::FWidget* widget = dynamic_cast<Focus::FWidget*>(newFocus);
                  if ((widget != NULL) && (widget->Config.View.getId() != currentFocus.ViewId))
                  {
                     hideFocus = true;
                  }
               }
               if (hideFocus)
               {
                  RnaiviFocus::hideFocus();
               }
            }
         }
         sessionManager->endSession();
      }
   }
}


*/
