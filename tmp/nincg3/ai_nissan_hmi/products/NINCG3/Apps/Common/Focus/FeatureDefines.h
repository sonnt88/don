/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */

#ifndef FEATUREDEFINES_H_
#define FEATUREDEFINES_H_

/*Supported features - Enable with Caution, test the feature after enabling
ENABLE_PREVIOUS_NEXT_PAGE_FOCUS
   - Moves focus to next page first element or previous page last element when Page down/up SK pressed respectively

SUPPORT_JOYSTICK_TEXT_SCROLL

REMOVE_FOCUS_ON_TOUCH
SHOW_FOCUS_ON_ENTERING_VIEW

SET_FOCUS_NOT_PRESERVED
   - Focus will not be preserved. This setting will be over written by CGI configuration

ENABLE_FOCUS_TIMER
   - Enables a focus laps timer, below defines needs to be enabled for specific operation after timeout
   HIDE_FOCUS_TIMER
      - Hides focus after timeout
   _TEST_FOCUS_REQUEST_MESSAGE
      - For debugging purpose.
FOCUS_ACROSS_APPLICATION
   - Focus to move to a surface visible from across applications
*/

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
#define SET_FOCUS_NOT_PRESERVED
#define HIDE_FOCUS_ON_SURFACE_BACKGROUND
#endif

#ifndef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1 //For all other scopes except Scope 1 (Assuming similar focus handling except Scope 1 for other AIVI lines)
#define ENABLE_BROADCAST_RECIEVE_FROM_ALL_APPS
#define FOCUS_ACROSS_APPLICATION
#define REMOVE_FOCUS_ON_TOUCH
#endif


#endif /* FEATUREDEFINES_H_ */
