/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#ifndef __DEFAULT_FOCUS_CONSISTENCY_CHECKER_H__
#define __DEFAULT_FOCUS_CONSISTENCY_CHECKER_H__

#include <Focus/FManager.h>
#include <Focus/FManagerConfig.h>
#include "Common/Focus/FeatureDefines.h"

///////////////////////////////////////////////////////////////////////////////
class FocusConsistencyChecker : public Focus::FConsistencyChecker, public Focus::FMsgReceiver
{
   public:
      FocusConsistencyChecker(Focus::FManager& manager);
      virtual ~FocusConsistencyChecker();

      virtual bool onMessage(const Focus::FMessage& msg);
      void checkConsistency();
      void restartTimer();
      void stopTimer();
      bool checkFocus();
   private:
      FocusConsistencyChecker(const FocusConsistencyChecker&);
      FocusConsistencyChecker& operator=(const FocusConsistencyChecker&);

      void checkPointerSurface(::Courier::ViewId);

      Focus::FManager& _manager;
      Util::Timer* _timer;

      ::Courier::ViewId _currentViewId;
};


#endif
