/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#include "gui_std_if.h"

///////////////////////////////////////////////////////////////////////////////
//focus manager includes
#include "ByteSerilizer.h"

#include "Common/Common_Trace.h"
//////// TRACE IF ///////////////////////////////////////////////////
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_FOCUS
#include "trcGenProj/Header/ByteSerilizer.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN


#define MAX_SERIAL_BUFFER (1024*2) //2KB

static char* getSerilizeBuffer()
{
   static char buffer[MAX_SERIAL_BUFFER];
   memset(buffer, 0, MAX_SERIAL_BUFFER);
   return buffer;
}


ByteSerilizer::ByteSerilizer()
{
   _text = getSerilizeBuffer();
   _totalSize = 0;
   _readPosition = 0;
   _result = false;
   _readData = NULL;
   _readLength = 0;
}


ByteSerilizer::~ByteSerilizer()
{
   _text = NULL;
   _readData = NULL;
}


bool ByteSerilizer::addNumber(int val)
{
   _result = false;
   //TO-DO: CHECK FOR SIZE OF INT
   int size = sizeof(val);
   if ((_totalSize + size) < MAX_SERIAL_BUFFER)
   {
      _text[_totalSize++] = (val >> 24) & 0xFF;
      _text[_totalSize++] = (val >> 16) & 0xFF;
      _text[_totalSize++] = (val >> 8) & 0xFF;
      _text[_totalSize++] = val & 0xFF;

      _result = true;
   }
   return _result;
}


bool ByteSerilizer::addString(const char* str, int len)
{
   _result = false;
   if ((_totalSize + len) < MAX_SERIAL_BUFFER)
   {
      memcpy(_text + _totalSize, str, len);
      _totalSize += len;
      _result = true;
   }
   return _result;
}


int ByteSerilizer::getNumber(int& value)
{
   if (_readData != NULL && ((_readPosition + 4) <= _readLength))
   {
      int a = _readData[_readPosition++] << 24 & 0xFF000000;
      int b = _readData[_readPosition++] << 16 & 0x00FF0000;
      int c = _readData[_readPosition++] << 8 & 0x0000FF00;
      int d = _readData[_readPosition++] & 0xFF;
      value = (a | b | c | d);
      return value;
   }
   return 0;
}


void ByteSerilizer::getString(std::string& retStr, int length)
{
   retStr.clear();
   if (_readData != NULL && (length > 0) && ((_readPosition + length) < _readLength))
   {
      std::string localString(&_readData[_readPosition], length);
      retStr = localString;
      _readPosition += length;
   }
}


bool ByteSerilizer::populateByteStream(Focus::Serialize::InfoSer& info)
{
   _result = false;

   //Names
   addNumber(info.Names.size());
   for (unsigned int i = 0; i < info.Names.size(); i++)
   {
      addNumber((info.Names[i]).size());
      addString((info.Names[i]).c_str(), (info.Names[i]).length());
   }

   //AppIndex
   addNumber(info.AppIndex.size());
   for (Focus::Serialize::InfoSer::AppIndexType::const_iterator it = info.AppIndex.begin(); it != info.AppIndex.end(); ++it)
   {
      if ((*it) != NULL)
      {
         addNumber((*it)->NameIndex);
         addNumber((*it)->OwnerIndex);
      }
   }

   //ViewIndex
   addNumber(info.ViewIndex.size());
   for (Focus::Serialize::InfoSer::ViewIndexType::const_iterator it = info.ViewIndex.begin(); it != info.ViewIndex.end(); ++it)
   {
      if ((*it) != NULL)
      {
         addNumber((*it)->NameIndex);
         addNumber((*it)->OwnerIndex);
      }
   }

   //WidgetIndex
   addNumber(info.WidgetIndex.size());
   for (Focus::Serialize::InfoSer::WidgetIndexType::const_iterator it = info.WidgetIndex.begin(); it != info.WidgetIndex.end(); ++it)
   {
      if ((*it) != NULL)
      {
         addNumber((*it)->NameIndex);
         addNumber((*it)->OwnerIndex);
      }
   }

   //AppConfig
   addNumber(info.AppConfig.size());
   for (Focus::Serialize::InfoSer::AppConfigType::const_iterator it = info.AppConfig.begin(); it != info.AppConfig.end(); ++it)
   {
      if ((*it) != NULL)
      {
         addNumber((*it)->ItemIndex);
      }
   }

   //ViewConfig
   addNumber(info.ViewConfig.size());
   for (Focus::Serialize::InfoSer::ViewConfigType::const_iterator it = info.ViewConfig.begin(); it != info.ViewConfig.end(); ++it)
   {
      if ((*it) != NULL)
      {
         addNumber((*it)->ItemIndex);
      }
   }

   //WidgetConfig
   addNumber(info.WidgetConfig.size());
   for (Focus::Serialize::InfoSer::WidgetConfigType::const_iterator it = info.WidgetConfig.begin(); it != info.WidgetConfig.end(); ++it)
   {
      if ((*it) != NULL)
      {
         addNumber((*it)->ItemIndex);
         addNumber((*it)->ParentGroupViewNameIndex);
         addNumber((*it)->ParentGroupWidgetNameIndex);
      }
   }

   //WidgetData
   addNumber(info.WidgetData.size());
   for (Focus::Serialize::InfoSer::WidgetDataType::const_iterator it = info.WidgetData.begin(); it != info.WidgetData.end(); ++it)
   {
      if ((*it) != NULL)
      {
         addNumber((*it)->ItemIndex);
         addNumber((*it)->Data.Enabled);
         addNumber((*it)->Data.SequenceNr);
         addNumber((*it)->Data.Order);
         addNumber((*it)->Data.EffectiveOrder);
         addNumber((*it)->Data.ControllerSetId);
      }
   }

   //Group data
   addNumber(info.GroupData.size());
   for (Focus::Serialize::InfoSer::GroupDataType::const_iterator it = info.GroupData.begin(); it != info.GroupData.end(); ++it)
   {
      if ((*it) != NULL)
      {
         addNumber((*it)->ItemIndex);
         addNumber((*it)->Data.Configured);
         addNumber((*it)->Data.DefaultOrder);
         addNumber((*it)->Data.Layer);
         addNumber((*it)->Data.PreserveFocus);
         addNumber((*it)->Data.WrapAround);
      }
   }

   //ListData
   addNumber(info.ListData.size());
   for (Focus::Serialize::InfoSer::ListDataType::const_iterator it = info.ListData.begin(); it != info.ListData.end(); ++it)
   {
      if ((*it) != NULL)
      {
         addNumber((*it)->ItemIndex);
         addNumber((*it)->Data.ListId);
         addNumber((*it)->Data.FirstVisibleIndex);
         addNumber((*it)->Data.VisibleItemCount);
         addNumber((*it)->Data.TotalItemCount);
      }
   }

   //GroupState
   addNumber(info.GroupState.size());
   for (Focus::Serialize::InfoSer::GroupStateType::const_iterator it = info.GroupState.begin(); it != info.GroupState.end(); ++it)
   {
      if ((*it) != NULL)
      {
         addNumber((*it)->ItemIndex);
         addNumber((*it)->SequenceId);
         addNumber((*it)->CurrentFocusWidgetNameIndex);
      }
   }

   //ViewState
   addNumber(info.ViewState.size());
   for (Focus::Serialize::InfoSer::ViewStateType::const_iterator it = info.ViewState.begin(); it != info.ViewState.end(); ++it)
   {
      if ((*it) != NULL)
      {
         addNumber((*it)->ItemIndex);
         addNumber((*it)->FocusVisible);
      }
   }

   //AppState
   addNumber(info.AppState.size());
   for (Focus::Serialize::InfoSer::AppStateType::const_iterator it = info.AppState.begin(); it != info.AppState.end(); ++it)
   {
      if ((*it) != NULL)
      {
         addNumber((*it)->ItemIndex);
         addNumber((*it)->CurrentFocusViewNameIndex);
         addNumber((*it)->CurrentFocusWidgetNameIndex);
      }
   }

   return _result;
}


template <typename TItem>
static TItem* createData()
{
   return FOCUS_NEW(TItem);
}


void ByteSerilizer::deserilize(Focus::Serialize::InfoSer& newInfoSer, char* data, int dataLength)
{
   int namesToGet = 0, size = 0;

   if (data == NULL)
   {
      return;
   }

   _readData = data;
   _readLength = dataLength;
   _readPosition = 0;

   getNumber(namesToGet);
   for (int i = 0; i < namesToGet; i++)
   {
      std::string name;
      getNumber(size);
      getString(name, size);
      newInfoSer.Names.push_back(name);
   }

   //AppIndex
   getNumber(size);
   for (int i = 0; i < size; ++i)
   {
      int OwnerIndex = 0, NameIndex = 0;
      Focus::Serialize::AppIndexSer* newApp = createData<Focus::Serialize::AppIndexSer>();

      if (newApp != NULL)
      {
         newApp->NameIndex = getNumber(NameIndex);
         newApp->OwnerIndex = getNumber(OwnerIndex);

         newInfoSer.AppIndex.push_back(newApp);
      }
   }

   //ViewIndex
   getNumber(size);
   for (int i = 0; i < size; ++i)
   {
      int OwnerIndex = 0, NameIndex = 0;
      Focus::Serialize::ViewIndexSer* data = createData<Focus::Serialize::ViewIndexSer>();

      if (data != NULL)
      {
         data->NameIndex = getNumber(NameIndex);
         data->OwnerIndex = getNumber(OwnerIndex);

         newInfoSer.ViewIndex.push_back(data);
      }
   }

   //WidgetIndex
   getNumber(size);
   for (int i = 0; i < size; ++i)
   {
      int OwnerIndex = 0, NameIndex = 0;
      Focus::Serialize::WidgetIndexSer* data = createData<Focus::Serialize::WidgetIndexSer>();

      if (data != NULL)
      {
         data->NameIndex = getNumber(NameIndex);
         data->OwnerIndex = getNumber(OwnerIndex);

         newInfoSer.WidgetIndex.push_back(data);
      }
   }

   //AppConfig
   getNumber(size);
   for (int i = 0; i < size; ++i)
   {
      int ItemIndex = 0;
      Focus::Serialize::AppConfigSer* newApp = createData<Focus::Serialize::AppConfigSer>();

      if (newApp != NULL)
      {
         newApp->ItemIndex = getNumber(ItemIndex);

         newInfoSer.AppConfig.push_back(newApp);
      }
   }

   //ViewConfig
   getNumber(size);
   for (int i = 0; i < size; ++i)
   {
      int ItemIndex = 0;
      Focus::Serialize::ViewConfigSer* data = createData<Focus::Serialize::ViewConfigSer>();

      if (data != NULL)
      {
         data->ItemIndex = getNumber(ItemIndex);

         newInfoSer.ViewConfig.push_back(data);
      }
   }

   //WidgetConfig
   getNumber(size);
   for (int i = 0; i < size; ++i)
   {
      int ItemIndex = 0, ParentGroupViewNameIndex = 0, ParentGroupWidgetNameIndex = 0;
      Focus::Serialize::WidgetConfigSer* data = createData<Focus::Serialize::WidgetConfigSer>();

      if (data != NULL)
      {
         data->ItemIndex = getNumber(ItemIndex);
         data->ParentGroupViewNameIndex = getNumber(ParentGroupViewNameIndex);
         data->ParentGroupWidgetNameIndex = getNumber(ParentGroupWidgetNameIndex);

         newInfoSer.WidgetConfig.push_back(data);
      }
   }

   //WidgetData
   getNumber(size);
   for (int i = 0; i < size; ++i)
   {
      int ItemIndex = 0, Enabled = 0, SequenceNr = 0, Order = 0, EffectiveOrder = 0, ControllerSetId = 0;

      Focus::Serialize::WidgetDataSer* data = createData<Focus::Serialize::WidgetDataSer>();

      if (data != NULL)
      {
         data->ItemIndex = getNumber(ItemIndex);
         data->Data.Enabled = getNumber(Enabled);
         data->Data.SequenceNr = getNumber(SequenceNr);
         data->Data.Order = getNumber(Order);
         data->Data.EffectiveOrder = getNumber(EffectiveOrder);
         data->Data.ControllerSetId = getNumber(ControllerSetId);

         newInfoSer.WidgetData.push_back(data);
      }
   }

   //Group data
   getNumber(size);
   for (int i = 0; i < size; ++i)
   {
      int ItemIndex = 0, Configured = 0, DefaultOrder = 0, Layer = 0, PreserveFocus = 0, WrapAround = 0;

      Focus::Serialize::GroupDataSer* data = createData<Focus::Serialize::GroupDataSer>();

      if (data != NULL)
      {
         data->ItemIndex = getNumber(ItemIndex);
         data->Data.Configured = getNumber(Configured);
         data->Data.DefaultOrder = getNumber(DefaultOrder);
         data->Data.Layer = getNumber(Layer);
         data->Data.PreserveFocus = getNumber(PreserveFocus);
         data->Data.WrapAround = getNumber(WrapAround);

         newInfoSer.GroupData.push_back(data);
      }
   }

   //ListData
   getNumber(size);
   for (int i = 0; i < size; ++i)
   {
      int ItemIndex = 0, ListId = 0, FirstVisibleIndex = 0, VisibleItemCount = 0, TotalItemCount = 0;

      Focus::Serialize::ListDataSer* data = createData<Focus::Serialize::ListDataSer>();

      if (data != NULL)
      {
         data->ItemIndex = getNumber(ItemIndex);
         data->Data.ListId = getNumber(ListId);
         data->Data.FirstVisibleIndex = getNumber(FirstVisibleIndex);
         data->Data.VisibleItemCount = getNumber(VisibleItemCount);
         data->Data.TotalItemCount = getNumber(TotalItemCount);

         newInfoSer.ListData.push_back(data);
      }
   }

   //GroupState
   getNumber(size);
   for (int i = 0; i < size; ++i)
   {
      int ItemIndex = 0, SequenceId = 0, CurrentFocusWidgetNameIndex = 0;

      Focus::Serialize::GroupStateSer* data = createData<Focus::Serialize::GroupStateSer>();

      if (data != NULL)
      {
         data->ItemIndex = getNumber(ItemIndex);
         data->SequenceId = getNumber(SequenceId);
         data->CurrentFocusWidgetNameIndex = getNumber(CurrentFocusWidgetNameIndex);

         newInfoSer.GroupState.push_back(data);
      }
   }

   //ViewState
   getNumber(size);
   for (int i = 0; i < size; ++i)
   {
      int ItemIndex = 0, FocusVisible = 0;

      Focus::Serialize::ViewStateSer* data = createData<Focus::Serialize::ViewStateSer>();

      if (data != NULL)
      {
         data->ItemIndex = getNumber(ItemIndex);
         data->FocusVisible = getNumber(FocusVisible);

         newInfoSer.ViewState.push_back(data);
      }
   }

   //AppState
   getNumber(size);
   for (int i = 0; i < size; ++i)
   {
      int ItemIndex = 0;
      int CurrentFocusViewNameIndex = 0;
      int CurrentFocusWidgetNameIndex = 0;

      Focus::Serialize::AppStateSer* data = createData<Focus::Serialize::AppStateSer>();

      if (data != NULL)
      {
         data->ItemIndex = getNumber(ItemIndex);
         data->CurrentFocusViewNameIndex = getNumber(CurrentFocusViewNameIndex);
         data->CurrentFocusWidgetNameIndex = getNumber(CurrentFocusWidgetNameIndex);

         newInfoSer.AppState.push_back(data);
      }
   }
}


void ByteSerilizer::printDebug(Focus::Serialize::InfoSer& info)
{
   unsigned int i = 0;

   ETG_TRACE_USR4(("\ninfo.Names: %d\n", info.Names.size()));
   for (i = 0; i < info.Names.size(); i++)
   {
      ETG_TRACE_USR4(("  %d: %s\n", i, (info.Names[i]).c_str()));
   }

   ETG_TRACE_USR4(("info.AppIndex: %d\n", info.AppIndex.size()));
   i = 0;
   for (Focus::Serialize::InfoSer::AppIndexType::const_iterator it = info.AppIndex.begin(); it != info.AppIndex.end(); ++it, ++i)
   {
      ETG_TRACE_USR4(("  %d: %d, %d\n", i, (*it)->NameIndex, (*it)->OwnerIndex));
   }

   ETG_TRACE_USR4(("info.ViewIndex: %d\n", info.ViewIndex.size()));
   i = 0;
   for (Focus::Serialize::InfoSer::ViewIndexType::const_iterator it = info.ViewIndex.begin(); it != info.ViewIndex.end(); ++it, ++i)
   {
      ETG_TRACE_USR4(("  %d: %d, %d \n", i, (*it)->NameIndex, (*it)->OwnerIndex));
   }

   ETG_TRACE_USR4(("info.WidgetIndex: %d\n", info.WidgetIndex.size()));
   i = 0;
   for (Focus::Serialize::InfoSer::WidgetIndexType::const_iterator it = info.WidgetIndex.begin(); it != info.WidgetIndex.end(); ++it, ++i)
   {
      ETG_TRACE_USR4(("  %d: %d, %d\n", i, (*it)->NameIndex, (*it)->OwnerIndex));
   }

   ETG_TRACE_USR4(("info.AppConfig: %d\n", info.AppConfig.size()));
   i = 0;
   for (Focus::Serialize::InfoSer::AppConfigType::const_iterator it = info.AppConfig.begin(); it != info.AppConfig.end(); ++it, ++i)
   {
      ETG_TRACE_USR4(("  %d: %d\n", i, (*it)->ItemIndex));
   }

   ETG_TRACE_USR4(("info.ViewConfig: %d\n", info.ViewConfig.size()));
   i = 0;
   for (Focus::Serialize::InfoSer::ViewConfigType::const_iterator it = info.ViewConfig.begin(); it != info.ViewConfig.end(); ++it, ++i)
   {
      ETG_TRACE_USR4(("  %d: %d\n", i, (*it)->ItemIndex));
   }

   ETG_TRACE_USR4(("info.WidgetConfig: %d\n", info.WidgetConfig.size()));
   i = 0;
   for (Focus::Serialize::InfoSer::WidgetConfigType::const_iterator it = info.WidgetConfig.begin(); it != info.WidgetConfig.end(); ++it, ++i)
   {
      ETG_TRACE_USR4(("  %d: %d, %d, %d\n", i, (*it)->ItemIndex, (*it)->ParentGroupViewNameIndex, (*it)->ParentGroupWidgetNameIndex));
   }

   ETG_TRACE_USR4(("info.WidgetData: %d\n", info.WidgetData.size()));
   i = 0;
   for (Focus::Serialize::InfoSer::WidgetDataType::const_iterator it = info.WidgetData.begin(); it != info.WidgetData.end(); ++it, ++i)
   {
      Focus::Serialize::WidgetDataSer& widgetData = *(*it);
      ETG_TRACE_USR4(("  %d: %d, %d, %d, %d, %d, %d \n", i, widgetData.ItemIndex, widgetData.Data.Enabled, widgetData.Data.SequenceNr, widgetData.Data.Order, widgetData.Data.EffectiveOrder, widgetData.Data.ControllerSetId));
   }

   ETG_TRACE_USR4(("info.GroupData: %d\n", info.GroupData.size()));
   i = 0;
   for (Focus::Serialize::InfoSer::GroupDataType::const_iterator it = info.GroupData.begin(); it != info.GroupData.end(); ++it, ++i)
   {
      Focus::Serialize::GroupDataSer& groupData = *(*it);
      ETG_TRACE_USR4(("  %d: %d, %d, %d %d, %d, %d\n", i, groupData.ItemIndex, groupData.Data.Configured, groupData.Data.DefaultOrder, groupData.Data.Layer, groupData.Data.PreserveFocus, groupData.Data.WrapAround));
   }

   ETG_TRACE_USR4(("info.ListData: %d\n", info.ListData.size()));
   i = 0;
   for (Focus::Serialize::InfoSer::ListDataType::const_iterator it = info.ListData.begin(); it != info.ListData.end(); ++it, ++i)
   {
      Focus::Serialize::ListDataSer& listData = *(*it);
      ETG_TRACE_USR4(("  %d: %d, %d, %d, %d, %d \n", i, listData.ItemIndex, listData.Data.ListId, listData.Data.FirstVisibleIndex, listData.Data.VisibleItemCount, listData.Data.TotalItemCount));
   }

   ETG_TRACE_USR4(("info.GroupState: %d\n", info.GroupState.size()));
   i = 0;
   for (Focus::Serialize::InfoSer::GroupStateType::const_iterator it = info.GroupState.begin(); it != info.GroupState.end(); ++it, ++i)
   {
      Focus::Serialize::GroupStateSer& groupState = *(*it);
      ETG_TRACE_USR4(("  %d: %d, %d, %d \n", i, groupState.ItemIndex, groupState.SequenceId, groupState.CurrentFocusWidgetNameIndex));
   }

   ETG_TRACE_USR4(("info.ViewState: %d\n", info.ViewState.size()));
   i = 0;
   for (Focus::Serialize::InfoSer::ViewStateType::const_iterator it = info.ViewState.begin(); it != info.ViewState.end(); ++it, ++i)
   {
      Focus::Serialize::ViewStateSer& viewState = *(*it);
      ETG_TRACE_USR4(("  %d: %d, %d \n", i, viewState.ItemIndex, viewState.FocusVisible));
   }

   ETG_TRACE_USR4(("info.AppState: %d\n", info.AppState.size()));
   i = 0;
   for (Focus::Serialize::InfoSer::AppStateType::const_iterator it = info.AppState.begin(); it != info.AppState.end(); ++it, ++i)
   {
      Focus::Serialize::AppStateSer& appState = *(*it);
      ETG_TRACE_USR4(("  %d: %d, %d, %d \n", i, appState.ItemIndex, appState.CurrentFocusViewNameIndex, appState.CurrentFocusWidgetNameIndex));
   }
}
