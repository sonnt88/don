/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */

#include "gui_std_if.h"
#include "ProjectBaseMsgs.h"
#include "IpcAdapter.h"
#include "MessageQueue.h"
#include "ConnectionHandler.h"
#include <Focus/FConfigInfo.h>
#include "../RnaiviFocus.h"
#include "../Utils/ByteSerilizer.h"
#include "../Utils/FocusDefines.h"
#include "Common/Common_Trace.h"
#include "../FeatureDefines.h"
#include "Common/Focus/Utils/FocusSurface.h"

//////// TRACE IF ///////////////////////////////////////////////////
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_FOCUS
#include "trcGenProj/Header/IpcAdapter.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN

int appIdList[] =
{
// creating seperate app Ids based on variant because
// queues will be created only for that applications
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
   APPID_APPHMI_MASTER,
   APPID_APPHMI_TUNER,
   APPID_APPHMI_MEDIA,
   APPID_APPHMI_SYSTEM,
   APPID_APPHMI_TESTMODE,
   APPID_APPHMI_PHONE,
   APPID_APPHMI_VEHICLE
#else
   APPID_APPHMI_MASTER,
   APPID_APPHMI_TUNER,
   APPID_APPHMI_NAVIGATION,
   APPID_APPHMI_MEDIA,
   APPID_APPHMI_SYSTEM,
   APPID_APPHMI_SXM,
   APPID_APPHMI_TELEMATICS,
   APPID_APPHMI_TESTMODE,
   APPID_APPHMI_PHONE,
   APPID_APPHMI_HOMESCREEN,
   APPID_APPHMI_VEHICLE,
   APPID_APPHMI_SDS,
#endif
};


using namespace Focus;
using namespace Focus::Serialize;

IpcAdapter::IpcAdapter(Focus::FManager& manager, AppViewHandler& viewHandler) :
   _manager(manager),
   _viewHandler(viewHandler)
{
   memset(_msgBuffer, 0, MESSAGE_BUFFER_SIZE);
   _sourceAppId = 0;
}


IpcAdapter::~IpcAdapter()
{
}


bool IpcAdapter::sendOtherAppsInfoRequest()
{
   FAppId appId = _manager.getCurrentAppId();
   ETG_TRACE_USR4(("IpcAdapter::sendOtherAppsInfoRequest currAppId:%d", appId));

   InfoSer serializedInfo;
   serializedInfo.clear();

   int payLoadMsgSize = packPayloadData(static_cast<char>(appId), static_cast<char>(CURRENT_APP_INFO_REQUEST), serializedInfo);

   broadcastMsg(payLoadMsgSize);
   return true;
}


bool IpcAdapter::sendCurrentAppInfo(FAppConfigSharedPtr appConfig, FAppStateSharedPtr appState)
{
   ETG_TRACE_USR4(("IpcAdapter::sendCurrentAppInfo:%d", _manager.getCurrentAppId()));
   unsigned int appId = _manager.getCurrentAppId();

   if (isViewOnSurface())
   {
      FConfigInfo configInfo;
      configInfo.add(appConfig.GetSharedInstance().getId(), appConfig); // target App's id

      FStateInfo stateInfo;
      stateInfo.add(appState.GetSharedInstance().getId(), appState);   // target App's id

      InfoSer serializedInfo;
      FDefaultSerializer serializer(&serializedInfo);

      bool serStatus = serializer.serialize(configInfo, stateInfo);
      ETG_TRACE_USR4(("IpcAdapter::sendCurrentAppInfo status:%d config:%d State:%d", serStatus, configInfo.count(), stateInfo.count()));

      serializedInfo = *(serializer.getInfo());

      unsigned int payLoadMsgSize = packPayloadData(static_cast<char>(appId), static_cast<char>(OTHER_APP_INFO_RECEIVED), serializedInfo);

      sendMsg(Rnaivi::ConnectionHandler::getAppQueueName(_sourceAppId).c_str(), payLoadMsgSize);

      serializedInfo.clear();
   }

   return true;
}


void IpcAdapter::sendInvalidResponse()
{
   unsigned int appId = _manager.getCurrentAppId();
   ETG_TRACE_USR4(("IpcAdapter::sendInvalidResponse:%d", appId));
   InfoSer serializedInfo;
   serializedInfo.clear();

   unsigned int payLoadMsgSize = packPayloadData(static_cast<char>(appId), static_cast<char>(OTHER_APP_INVALID_DATA), serializedInfo);

   sendMsg(Rnaivi::ConnectionHandler::getAppQueueName(_sourceAppId).c_str(), payLoadMsgSize);
}


bool IpcAdapter::sendFocusHideReq()
{
   unsigned int appId = _manager.getCurrentAppId();
   ETG_TRACE_USR4(("IpcAdapter::sendFocusHideReq:%d", appId));

   unsigned int payLoadMsgSize = packPayloadData(static_cast<char>(appId), static_cast<char>(OTHER_APP_HIDE_FOCUS_REQ));

   //sendMsg(Rnaivi::ConnectionHandler::getAppQueueName(_sourceAppId).c_str(), payLoadMsgSize);
   broadcastMsg(payLoadMsgSize, false);

   return true;
}


bool IpcAdapter::sendOtherAppState(FAppStateSharedPtr appState)
{
   ETG_TRACE_USR4(("IpcAdapter::sendOtherAppState:%d", _manager.getCurrentAppId()));

   unsigned int appId = _manager.getCurrentAppId();

   FConfigInfo configInfo;
   FStateInfo stateInfo;
   stateInfo.add(appId, appState);

   InfoSer serializedInfo;
   FDefaultSerializer serializer(&serializedInfo);

   serializer.serialize(configInfo, stateInfo);

   serializedInfo = *(serializer.getInfo());

   int payLoadMsgSize = packPayloadData(static_cast<char>(appId), static_cast<char>(CURRENT_APP_STATE_RECIEVED), serializedInfo);

   serializedInfo.clear();

   broadcastMsg(payLoadMsgSize);

   return true;
}


void IpcAdapter::processIncomingMessages(char* incomingMsg, unsigned int msgLen)
{
   if (incomingMsg)
   {
      FocusIPCMessageTypeEnum msgType = static_cast<FocusIPCMessageTypeEnum>(*(incomingMsg + APP_ID_LEN));
      ETG_TRACE_USR4(("IpcAdapter::processIncomingMessages from srcId:%d tgtapp:%d msgType; %d", *(incomingMsg), _manager.getCurrentAppId(), msgType));

      switch (msgType)
      {
         case CURRENT_APP_INFO_REQUEST:
         {
            _sourceAppId = *(incomingMsg);
            if (isViewOnSurface() && _sourceAppId != RnaiviFocus::getAppId())
            {
               ETG_TRACE_USR4(("IpcAdapter::notifyCurrentAppInfoRequestReceived"));
               if (_manager.getSessionManager())
               {
                  _manager.getSessionManager()->abortSession();
               }
               notifyCurrentAppInfoRequestReceived();
            }
            else
            {
               sendInvalidResponse();
            }
            break;
         }
         case OTHER_APP_INFO_RECEIVED:
         {
            _sourceAppId = *(incomingMsg);

            FAppConfigSharedPtr appConfig;
            FAppStateSharedPtr appState;

            unPackPayloadData(incomingMsg, msgLen, appConfig, appState);

            notifyOtherAppInfoReceived(appConfig, appState);
#ifdef ENABLE_BROADCAST_RECIEVE_FROM_ALL_APPS
            removeAppIdFromMap(_sourceAppId);

            checkIfOtherAppInfoRecievedCompleted();
#else
            notifyOtherAppsInfoReceiveCompleted();
#endif
            break;
         }
         case CURRENT_APP_STATE_RECIEVED:
         {
            _sourceAppId = *(incomingMsg);
            if (isViewOnSurface() && _sourceAppId != RnaiviFocus::getAppId())
            {
               ETG_TRACE_USR4(("IpcAdapter::notifyCurrentAppStateReceived"));
               FAppConfigSharedPtr appConfig;
               FAppStateSharedPtr appState;

               unPackPayloadData(incomingMsg, msgLen, appConfig, appState);

               if (_manager.getSessionManager())
               {
                  _manager.getSessionManager()->abortSession();
               }
               notifyCurrentAppStateReceived(appState);
            }
            break;
         }
         case OTHER_APP_INVALID_DATA:
         {
#ifdef ENABLE_BROADCAST_RECIEVE_FROM_ALL_APPS
            int appId = *(incomingMsg);
            removeAppIdFromMap(appId);
            checkIfOtherAppInfoRecievedCompleted();
#else
            /*FAppConfigSharedPtr appConfig;
            FAppStateSharedPtr appState;
            unPackPayloadData(incomingMsg, msgLen, appConfig, appState);

            notifyOtherAppInfoReceived(appConfig, appState);*/

            notifyOtherAppsInfoReceiveCompleted();
#endif
            break;
         }
         case OTHER_APP_HIDE_FOCUS_REQ:
         {
            //RnaiviFocus::hideFocus();
            RnaiviFocus::hideAllViewFocus();

            break;
         }
         default:
            break;
      }
      delete incomingMsg;
   }
}


unsigned int IpcAdapter::packPayloadData(char sourceId, char enumMsgType, InfoSer& serializedInfo)
{
   ETG_TRACE_USR4(("IpcAdapter::packPayloadData msgType:%d, srcId:%d", enumMsgType, sourceId));

   ByteSerilizer serializer;
   serializer.populateByteStream(serializedInfo);

//   serializer.printDebug(serializedInfo);

   memset(_msgBuffer, 0 , sizeof(_msgBuffer));
   memcpy(_msgBuffer, &sourceId, APP_ID_LEN);
   memcpy((_msgBuffer + APP_ID_LEN), &enumMsgType, IPC_MSG_TYPE_LEN);
   memcpy((_msgBuffer + APP_ID_LEN + IPC_MSG_TYPE_LEN), serializer.getByteStream(), serializer.getStreamLength());

   unsigned int msgSize = serializer.getStreamLength() + APP_ID_LEN + IPC_MSG_TYPE_LEN;

   ETG_TRACE_USR4(("IpcAdapter::end packing msgSize:%d", msgSize));

   return msgSize;
}


unsigned int IpcAdapter::packPayloadData(char sourceId, char enumMsgType)
{
   ETG_TRACE_USR4(("IpcAdapter::packPayloadData short msgType:%d, srcId:%d", enumMsgType, sourceId));

   memset(_msgBuffer, 0 , sizeof(_msgBuffer));
   memcpy(_msgBuffer, &sourceId, APP_ID_LEN);
   memcpy((_msgBuffer + APP_ID_LEN), &enumMsgType, IPC_MSG_TYPE_LEN);
//   memcpy((_msgBuffer + APP_ID_LEN + IPC_MSG_TYPE_LEN), serializer.getByteStream(), serializer.getStreamLength());

   unsigned int msgSize = APP_ID_LEN + IPC_MSG_TYPE_LEN;

   ETG_TRACE_USR4(("IpcAdapter::end packing msgSize:%d", msgSize));

   return msgSize;
}


void IpcAdapter::unPackPayloadData(char* incomingMsg, unsigned int msgLen, FAppConfigSharedPtr& appConfig, FAppStateSharedPtr& appState)
{
   InfoSer infodeSer;

   char payloadMsg[MAX_MSG_LENGTH];
   memcpy(payloadMsg, (incomingMsg + APP_ID_LEN + IPC_MSG_TYPE_LEN), (msgLen - (APP_ID_LEN + IPC_MSG_TYPE_LEN)));

   ByteSerilizer byteDeserializer;
   byteDeserializer.deserilize(infodeSer, payloadMsg, (msgLen - (APP_ID_LEN + IPC_MSG_TYPE_LEN)));

//   byteDeserializer.printDebug(infodeSer);

   Focus::Serialize::FDefaultSerializer deSerializer(&infodeSer);

   FConfigInfo configInfo;
   FStateInfo stateInfo;
   deSerializer.deserialize(configInfo, stateInfo);

   appConfig = configInfo.getAt(0);

   appState = stateInfo.getAt(0);
}


bool IpcAdapter::sendMsg(const char* queue_name, unsigned int msgLen)
{
   bool result = false;
   ETG_TRACE_USR4(("IpcAdapter::sendMsg :%s", queue_name));
   IConnection* connection = new Rnaivi::MessageQueue;

   if (connection)
   {
      result = connection->quickWrite(queue_name, _msgBuffer, msgLen);

      delete connection;
   }
   return result;
}


void IpcAdapter::broadcastMsg(unsigned int payloadMsgLen, bool expectResponse)
{
   for (unsigned int applistIdx = 0; applistIdx < (sizeof(appIdList) / sizeof(appIdList[0])); applistIdx++)
   {
      int appId = appIdList[applistIdx];
      if (appId != _manager.getCurrentAppId())
      {
         ETG_TRACE_USR4(("sending msg to appId:%d", appId));

         bool sendSuccess = sendMsg(Rnaivi::ConnectionHandler::getAppQueueName(appId).c_str(), payloadMsgLen);
#ifdef ENABLE_BROADCAST_RECIEVE_FROM_ALL_APPS
         if (sendSuccess && expectResponse)
         {
            updateAppMsgStatusInMap(appId);
         }
#endif
      }
   }
}


void IpcAdapter::updateAppMsgStatusInMap(int appId)
{
   std::vector<int>::iterator iter = std::find(_appMsgMap.begin(), _appMsgMap.end(), appId);

   if (iter == _appMsgMap.end())
   {
      _appMsgMap.push_back(appId);   // updates status of every App and msg send/recv status
   }
}


void IpcAdapter::checkIfOtherAppInfoRecievedCompleted()
{
   std::vector<int>::iterator iter = _appMsgMap.begin();

   if (iter == _appMsgMap.end()) //if all other app info are recieved
   {
      ETG_TRACE_USR4(("notifyOtherAppsInfoReceiveCompleted:%d", RnaiviFocus::getAppId()));
      notifyOtherAppsInfoReceiveCompleted();
   }
}


void IpcAdapter::removeAppIdFromMap(int appId)
{
   std::vector<int>::iterator iter = std::find(_appMsgMap.begin(), _appMsgMap.end(), appId);
   if (iter != _appMsgMap.end())
   {
      _appMsgMap.erase(iter);
   }
}


bool IpcAdapter::isViewOnSurface(/*unsigned int surfaceId*/) const
{
   if (Rnaivi::FocusSurface::getAppState() == hmibase::IN_FOREGROUND)
   {
      return true;
   }

   return false;
}
