/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#include "gui_std_if.h"

///////////////////////////////////////////////////////////////////////////////
//focus manager includes
#include "DefaultRotaryController.h"
#include "Focus/FCommon.h"
#include "Focus/FContainer.h"
#include "Focus/FDataSet.h"
#include "Focus/FData.h"
#include "Focus/FActiveViewGroup.h"
#include "Focus/FGroupTraverser.h"
#include "Focus/FManagerConfig.h"
#include "Focus/FManager.h"
#include "JoyStickUtil.h"

#include "Common/Focus/Utils/FUtils.h"

#include "Common/Common_Trace.h"
#include "hmi_trace_if.h"
#include "AppUtils/Trace/TraceUtils.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_FOCUS
#include "trcGenProj/Header/DefaultRotaryController.cpp.trc.h"
#endif

namespace Focus {

DefaultRotaryController::DefaultRotaryController(FManager& manager) : _manager(manager)
{
   _isJoyStickPresent = JoyStickUtil::isJoyStickPresent();
}


bool DefaultRotaryController::onMessage(FSession& session, FGroup& group, const FMessage& msg)
{
   if (msg.GetId() == EncoderStatusChangedUpdMsg::ID)
   {
      const EncoderStatusChangedUpdMsg* encoderMsg = Courier::message_cast<const EncoderStatusChangedUpdMsg*>(&msg);
      if (encoderMsg != NULL)//encode code is checked in focus input checker component
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "FDefaultRotaryController::onMessage() steps=%d, src=%u, visible=%u",
                             encoderMsg->GetEncSteps(), encoderMsg->GetEncCode(), _manager.isFocusVisible()));

         if (_manager.getActivityTimer() != NULL)
         {
            _manager.getActivityTimer()->restart();
         }

         //if focus is not visible, generate a new sequence so that preserve focus can correctly reuse the current focus
         if (!Rnaivi::FUtils::isCurrentFocusAvailable(_manager, session, group))
         {
            _manager.generateNewSequenceId();
         }

         bool result = moveFocus(session, group, encoderMsg->GetEncSteps());

         if (result && !Rnaivi::FUtils::isCurrentFocusAvailable(_manager, session, group))
         {
            _manager.setFocusVisible(true);
         }

         //message is consumed even if focus was not changed, this could change if necessary
         return true;
      }
   }

   return false;
}


bool DefaultRotaryController::moveFocus(FSession& session, FGroup& group, int steps)
{
   ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "FDefaultRotaryController::moveFocus steps=%d, group=%s",
                       steps, FID_STR(group.getId())));

   FWidget* newFocus = NULL;
   if (_manager.getAvgManager() != NULL)
   {
      Focusable* f = _manager.getAvgManager()->getFocus(session);
      if (f == NULL)
      {
         return false;
      }

      FWidgetVisitor widgetVisitor;

      bool ascending = true;
      if (steps < 0)
      {
         ascending = false;
         steps = -steps;
      }

      FGroup* parent = &group;

      //Focus group has to be strictly applied, even though joy stick is not present.
      //if (_isJoyStickPresent)
      {
         ETG_TRACE_USR4(("DefaultRotaryController::moveFocus joy stick present"));
         if (f->getParent() == NULL)
         {
            return false;
         }
         parent = JoyStickUtil::getConfiguredGroup(*(f->getParent()));
      }

      if (parent == NULL)
      {
         return false;
      }
      ETG_TRACE_USR4(("DefaultRotaryController::moveFocus considering group: %s", FID_STR(parent->getId())));
      //FDefaultGroupTraverser traverser(widgetVisitor, group, f, ascending);
      FDefaultGroupTraverser traverser(widgetVisitor, *parent, f, ascending);

      //prevents infinite loops with wrap around
      size_t lastResetCount = Constants::InvalidIndex;

      //if focus is not visible check old focus
      if (!Rnaivi::FUtils::isCurrentFocusAvailable(_manager, session, group))
      {
         //also set steps to 1 and check the current/default focus
         steps = 1;
         f->visit(widgetVisitor);
      }

      while (static_cast<int>(widgetVisitor.getCount()) < steps)
      {
         //advanced to next
         if (traverser.advance())
         {
            //nothing to do
         }
         //no more elements
         else
         {
            //if wrap around reset the traverser
            FGroupData* groupData = parent->getData<FGroupData>();
            if ((groupData != NULL) && (groupData->WrapAround) && (lastResetCount != widgetVisitor.getCount()))
            {
               //prevent infinite loops
               lastResetCount = widgetVisitor.getCount();

               traverser.reset();
            }
            else
            {
               break;
            }
         }
      }

      newFocus = widgetVisitor.getFocusable();
      if (newFocus != NULL)
      {
         _manager.getAvgManager()->setFocus(session, newFocus);
      }
   }
   return newFocus != NULL;
}


}
