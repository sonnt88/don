/**
*  @file   ProjectTaskFactory.cpp
*  @author ECV - vma6cob
*  @copyright (c) 2016 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_app_common
*/

#include "gui_std_if.h"

#include <Focus/Default/FDefaultAvgBuilder.h>
#include <Focus/Default/FDefaultCrtAppExchange.h>
#include <Focus/Default/FDefaultInputMsgProcessor.h>
#include <Focus/Default/FDefaultOtherAppsExchange.h>
#include "ProjectTaskFactory.h"
#include "ProjectSlaveUpdater.h"

using namespace Focus;

bool ProjectTaskFactory::configurePublishingTasks(FTaskManager& taskManager, FSession& session)
{
   switch (session.getMode())
   {
      case FSession::Master:
      {
         taskManager.addTask(FOCUS_NEW(FDefaultSession2CrtAppStateUpdater)(_manager));
         taskManager.addTask(FOCUS_NEW(FDefaultCourierInfoPublisher)(_manager));
         taskManager.addTask(FOCUS_NEW(FDefaultIpcOtherAppsStatePublisher)(_manager));
         break;
      }

      case FSession::QueryingSlave:
      {
         taskManager.addTask(FOCUS_NEW(FDefaultIpcCrtAppInfoPublisher)(_manager));
         break;
      }

      case FSession::UpdatingSlave:
      {
         taskManager.addTask(FOCUS_NEW(FDefaultSession2CrtAppStateUpdater)(_manager));
         taskManager.addTask(FOCUS_NEW(ProjectSlaveUpdater)(_manager));
         taskManager.addTask(FOCUS_NEW(FDefaultCourierInfoPublisher)(_manager));
         break;
      }

      default:
         break;
   }

   return true;
}
