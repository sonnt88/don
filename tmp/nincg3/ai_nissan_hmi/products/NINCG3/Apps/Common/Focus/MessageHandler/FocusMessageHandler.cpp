/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#include "gui_std_if.h"

#include "Common/Focus/RnaiviFocus.h"
#include "Common/Focus/Controller/ListRotaryController.h"
#include "Common/Focus/Utils/FUtils.h"
#include "FocusMessageHandler.h"
#include "ProjectBaseMsgs.h"
#include "ProjectBaseTypes.h"

//////// TRACE IF ///////////////////////////////////////////////////
#include "Common/Common_Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_FOCUS
#include "trcGenProj/Header/FocusMessageHandler.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN


bool FocusMessageReceiverControl::onMessage(const Focus::FMessage& msg)
{
   ETG_TRACE_USR4(("FocusMessageReceiverControl::onMessage App:%d ctlMsg=%s", RnaiviFocus::getAppId(), msg.GetName()));
   bool result = false;

   switch (msg.GetId())
   {
      case ButtonReactionMsg::ID:
      {
         const ButtonReactionMsg* buttonReaction = Courier::message_cast<const ButtonReactionMsg*>(&msg);

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
         if (buttonReaction && (buttonReaction->GetEnReaction() == enRelease || buttonReaction->GetEnReaction() == enLongPressRelease))
         {
            (COURIER_MESSAGE_NEW(ListFocusLockDataResetReqMsg)())->Post();
         }
#endif
         if (buttonReaction && (buttonReaction->GetEnReaction() == enPress))
         {
            //set the button to be focused but not show
            const char* widgetName = Rnaivi::FUtils::findDestinationWidget(buttonReaction->GetSender(), buttonReaction->GetView(), _viewHandler);
            if (widgetName != NULL)
            {
               //Rnaivi::FUtils::saveTouchedButton(buttonReaction->GetView(), widgetName);
               /*FocusReqMsg* focusReqMsg = COURIER_MESSAGE_NEW(FocusReqMsg)(FOCUS_SET, FOCUS_TIMER_STOP, buttonReaction->GetView(), widgetName);
               if (focusReqMsg != NULL)
               {
                  focusReqMsg->Post();
               }*/
            }
         }
      }
      break;

#ifdef REMOVE_FOCUS_ON_TOUCH
      case Courier::TouchMsg::ID:
      {
         const Courier::TouchMsg* touchMsg = Courier::message_cast<const Courier::TouchMsg*>(&msg);

         if (touchMsg != NULL && touchMsg->GetState() == Courier::TouchMsgState::Down)
         {
            RnaiviFocus::hideAllFocus();
         }
      }
      break;
#endif
#ifdef ENABLE_PREVIOUS_NEXT_PAGE_FOCUS
      case ListChangeMsg::ID:
      {
         const ListChangeMsg* listchangeMsg = Courier::message_cast<const ListChangeMsg*>(&msg);
         if (listchangeMsg != NULL)
         {
            ETG_TRACE_USR4(("FocusMessageReceiverControl::ListChangeMsg id: %d", listchangeMsg->GetListChangeType()));
            switch (listchangeMsg->GetListChangeType())
            {
               case ListChangePageUp:
               case ListChangePageDown:
                  ListFocusReqMsg* listFocusReqMsg = COURIER_MESSAGE_NEW(ListFocusReqMsg)(listchangeMsg->GetListId(), listchangeMsg->GetListChangeType());
                  if (listFocusReqMsg != NULL)
                  {
                     ETG_TRACE_USR4(("FocusMessageReceiverControl::ListFocusReqMsg posted"));
                     listFocusReqMsg->Post();
                  }
                  break;
               default:
                  break;
            }
         }
      }
      break;
#endif
      // TO DO:Need to check why HK_Back event is consumed in same screen
      /*case HKStatusChangedUpdMsg::ID:
      {
         const HKStatusChangedUpdMsg* hkMsg = Courier::message_cast<const HKStatusChangedUpdMsg*>(&msg);
         if (hkMsg->GetHKCode() == HARDKEYCODE_HK_ESC)
         {
            FocusActionEnum action = FOCUS_SHOW;
            FocusTimerActionEnum timerAction = FOCUS_TIMER_STOP;
            FocusReqMsg* focusReqMsg = COURIER_MESSAGE_NEW(FocusReqMsg)(action, timerAction, Courier::ViewId(""), "");

            if (focusReqMsg != NULL)
            {
               focusReqMsg->Post();
            }
         }
      }
      break;*/
      case  SetScrollDirectionMsg::ID:
      {
         ETG_TRACE_USR4(("SetEncoderDirection::onMessage id=%s", msg.GetName()));

         const SetScrollDirectionMsg* dirMsg = Courier::message_cast<const SetScrollDirectionMsg*>(&msg);
         if (dirMsg != NULL)
         {
            result = true;
            unsigned int direction = dirMsg->GetDirection();

            FocusManagerConfigurator* configurator = dynamic_cast<FocusManagerConfigurator*>(RnaiviFocus::getConfigurator());
            if (configurator)
            {
               if (direction)
               {
                  ETG_TRACE_USR4(("List rotation direction forward"));
                  if (configurator->getReversibleListController())
                  {
                     configurator->getReversibleListController()->setDirection(false);
                  }
               }
               else
               {
                  ETG_TRACE_USR4(("List rotation direction reverse"));
                  if (configurator->getReversibleListController())
                  {
                     configurator->getReversibleListController()->setDirection(true);
                  }
               }
            }
         }
      }
      break;
      default:
         break;
   }
   return result;
}


///////////////////////////////////////////////////////////////////////////////
bool FoucsIPCPayloadMessage::onMessage(const Focus::FMessage& msg)
{
   bool result = false;
   switch (msg.GetId())
   {
      case FocusIPCMsgPayloadUpdMsg::ID:
      {
         ETG_TRACE_USR4(("FoucsIPCPayloadMessage::onMessage Appid=%d", RnaiviFocus::getAppId()));
         const FocusIPCMsgPayloadUpdMsg* focusIpcMsg = Courier::message_cast<const FocusIPCMsgPayloadUpdMsg*>(&msg);

         _ipcAdapter.processIncomingMessages(focusIpcMsg->GetPayload(), focusIpcMsg->GetPayloadMsgLength());
         result = true;
      }
      break;
      case SurfaceStateChangedUpdMsg::ID:
      {
         const SurfaceStateChangedUpdMsg* surfaceMsg = Courier::message_cast<const SurfaceStateChangedUpdMsg*>(&msg);
         if (surfaceMsg != NULL)
         {
            ETG_TRACE_USR4(("SurfaceStateChangedUpdMsg::ID state=%d id=%d AppId=%d view=%s", surfaceMsg->GetState(), surfaceMsg->GetSurfaceId(), RnaiviFocus::getAppId(), (surfaceMsg->GetViewId()).CStr()));

            _focusSurface.Update(surfaceMsg->GetSurfaceId(), surfaceMsg->GetState());

            ETG_TRACE_USR4(("SurfaceStateChangedUpdMsg::ID SurfaceState=%d", _focusSurface.getAppState()));

            if (_focusSurface.getAppState() == hmibase::IN_BACKGROUND)
            {
#ifdef HIDE_FOCUS_ON_SURFACE_BACKGROUND
               RnaiviFocus::hideFocus();
#endif
            }
         }
      }
      break;
      default:
         break;
   }
   return result;
}
