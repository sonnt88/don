/*
 * FocusDefines.h
 *
 *  Created on: Dec 1, 2015
 *      Author: vma6cob
 */

#ifndef FOCUSDEFINES_H_
#define FOCUSDEFINES_H_

const unsigned int APP_ID_LEN = 1;
const unsigned int IPC_MSG_TYPE_LEN = 1;

const unsigned int MAX_MSG_IN_QUEUE = 13;
const unsigned int MAX_MSG_LENGTH = 2048;

const unsigned int MAX_INCOMING_MSG_LENGTH = (MAX_MSG_LENGTH + APP_ID_LEN + IPC_MSG_TYPE_LEN);


#endif /* FOCUSDEFINES_H_ */
