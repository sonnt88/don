/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */

#ifndef CONNECTIONHANDLER_H_
#define CONNECTIONHANDLER_H_

#include "IConnection.h"


namespace Rnaivi {

class ConnectionHandler
{
   public:
      ConnectionHandler(int);
      virtual ~ConnectionHandler();
      IConnection* createConnection();
      void closeConnection();
      static IConnection* getConnection();
      bool isClosed();
      static std::string getAppQueueName(int);

   private:
      static IConnection* _connection;
      bool _connectionOpen;
      std::string _appQueueName;
};


} /* namespace Rnaivi */
#endif /* CONNECTIONHANDLER_H_ */
