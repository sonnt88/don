/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#ifndef __FOCUS_ROTARY_CONTROLLER_H__
#define __FOCUS_ROTARY_CONTROLLER_H__

/*****************************************************************************/
/* FOCUS MANAGER INCLUDES                                                    */
/*****************************************************************************/
#include "Focus/FCommon.h"
#include "Focus/FController.h"

namespace Focus {
/**************************************************************************/
/* FORWARD DECLARATIONS                                                   */
/**************************************************************************/
class FManager;
class FGroup;
class FSession;

/**************************************************************************/
/* HEADER DECLARATIONS                                                    */
/**************************************************************************/
#include "Common/Focus/FeatureDefines.h"

/**************************************************************************/
/* ROTARY CONTROLLER                                                      */
/* Handles the rotary encoder related messages.                           */
/**************************************************************************/
class DefaultRotaryController : public FController
{
   public:
      DefaultRotaryController(FManager& manager);
      virtual ~DefaultRotaryController() {}

      virtual bool onMessage(FSession& session, FGroup& group, const FMessage& msg);

   private:
      DefaultRotaryController(const DefaultRotaryController&);
      DefaultRotaryController& operator=(const DefaultRotaryController&);

      /* Moves the focus with the specified steps (positive or negative). */
      bool moveFocus(FSession& session, FGroup& group, int steps);

      FManager& _manager;

      bool _isJoyStickPresent;
};


}
#endif
