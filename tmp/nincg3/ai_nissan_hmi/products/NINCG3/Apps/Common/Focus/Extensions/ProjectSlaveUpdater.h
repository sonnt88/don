/**
*  @file   ProjectSlaveUpdater.h
*  @author ECV - vma6cob
*  @copyright (c) 2016 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_app_common
*/

#ifndef PROJECTSLAVEUPDATER_H_
#define PROJECTSLAVEUPDATER_H_

#include <Focus/Default/FDefaultOtherAppsExchange.h>
#include <Focus/FManager.h>

class ProjectSlaveUpdater : public Focus::FDefaultSlaveAppUpdater
{
      typedef FDefaultSlaveAppUpdater Base;

   public:
      ProjectSlaveUpdater(Focus::FManager& manager) :
         Base(manager) {}
      virtual ~ProjectSlaveUpdater() {}

      virtual FTask::Result execute();

   private:
      ProjectSlaveUpdater(const ProjectSlaveUpdater&);
      ProjectSlaveUpdater& operator=(const ProjectSlaveUpdater&);

      bool isSlaveFocusVisible();
};


#endif /* PROJECTSLAVEUPDATER_H_ */
