/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#include "gui_std_if.h"

#include "ProjectBaseConstants.h"
#include "ProjectBaseMsgs.h"
#include "ProjectBaseTypes.h"

#include "RnaiviFocus.h"
#include "FocusInput.h"
#include "MessageHandler/FocusMessageHandler.h"
#include "Extensions/ProjectTaskFactory.h"

//To find if any item is focused
#include <Focus/FStateInfo.h>
#include "CgiExtensions/AppViewHandler.h"

#include <Focus/FManagerConfig.h>
#include <Focus/FCommon.h>
#include <Focus/Default/FDefaultCrtAppExchange.h>
#include <Focus/Default/FDefaultAvgBuilder.h>
#include <Focus/FManagerConfig.h>

#include <Widgets/2D/FlexList/FlexListWidget2D.h>
#include "Controller/ListEnterKeyController.h"
#include "Controller/FocusRequestController.h"
#include "Controller/DefaultRotaryController.h"
#include "MessageHandler/FocusConsistencyChecker.h"

#include <Focus/Default/FDefaultEnterKeyController.h>
#include <Focus/Default/FDefaultRotaryController.h>
#include <Focus/Default/FDefaultVisibilityManager.h>

#include "Controller/JoyStickController.h"
#include "Controller/TextScrollController.h"

#include "Common/Common_Trace.h"
//////// TRACE IF ///////////////////////////////////////////////////
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_FOCUS
#include "trcGenProj/Header/RnaiviFocus.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN

///////////////////////////////////////////////////////////////////////////////
FocusManagerConfigurator::FocusManagerConfigurator(Focus::FManager& manager, AppViewHandler& viewHandler) :
   Base(manager, &viewHandler),
   _viewHandler(viewHandler),
   _hkConverter(NULL),
   _focusReceiverControl(NULL),
   _consistencyChecker(NULL)
{
   _reversibleListController = NULL;
   _listController = NULL;
   _ipcHandler = new IpcHandler;
   _ipcAdapter = new IpcAdapter(_manager, _viewHandler);
   setCurrentAppId(RnaiviFocus::getAppId()); // Sets the current appID to focus manager
   _focusIpcPayloadMsg = NULL;
   _ipcManager = NULL;

   _consistencyChecker = new FocusConsistencyChecker(_manager);
}


///////////////////////////////////////////////////////////////////////////////
FocusManagerConfigurator::~FocusManagerConfigurator()
{
   _hkConverter = NULL;
   _focusReceiverControl = NULL;
   _listController = NULL;
   _reversibleListController = NULL;
   //_consistencyChecker = NULL;
   _focusIpcPayloadMsg = NULL;
   _ipcManager = NULL;

   if (_consistencyChecker != NULL)
   {
      delete _consistencyChecker;
      _consistencyChecker = NULL;
   }

   if (_ipcHandler != NULL)
   {
      delete _ipcHandler;
      _ipcHandler = NULL;
   }

   if (_ipcAdapter != NULL)
   {
      delete _ipcAdapter;
      _ipcAdapter = NULL;
   }
}


///////////////////////////////////////////////////////////////////////////////
bool FocusManagerConfigurator::initialize()
{
   //Removing focus persistence similar to LCN2Kai, focus should start from default position once focus is lost.
   //This is applicable only to Scope1, for Scope2 it is required to preserve focus ref. F-M01-02-09_Focus requirement_Ver1.4.xlsx
   //setPreserveFocus(false);

   bool result = Base::initialize();

   if (_hkConverter == NULL)
   {
      _hkConverter = new DefaultHKtoEnterKeyConverter(_manager, _viewHandler);
      if (_hkConverter != NULL)
      {
         _manager.addMsgReceiver(*_hkConverter);
      }
   }

   if (_focusReceiverControl == NULL)
   {
      _focusReceiverControl = new FocusMessageReceiverControl(_manager, _viewHandler);
      if (_focusReceiverControl != NULL)
      {
         _manager.addMsgReceiver(*_focusReceiverControl);
      }
   }
   //if (_consistencyChecker == NULL)
   {
      //_consistencyChecker = new FocusConsistencyChecker(_manager);
      if (_consistencyChecker != NULL)
      {
         _manager.addMsgReceiver(*_consistencyChecker);
      }
   }

   // Message handler for focus pay load message
   if (_focusIpcPayloadMsg == NULL && _ipcAdapter != NULL)
   {
      _focusIpcPayloadMsg = new FoucsIPCPayloadMessage(_manager, *_ipcAdapter);
      if (_focusIpcPayloadMsg != NULL)
      {
         _manager.addMsgReceiver(*_focusIpcPayloadMsg);
      }
   }
#ifdef ENABLE_FOCUS_TIMER
   //Reverting hide focus on timer operation, ref. F-M01-02-09_Focus requirement_Ver1.4.xlsx and LCN2Kai
   _manager.getActivityTimer()->addListener(_timer);
#endif

#ifdef SET_FOCUS_NOT_PRESERVED
   //Setting preserve focus to false only for scope 1
   setPreserveFocus(false);
#endif

   return result;
}


///////////////////////////////////////////////////////////////////////////////
bool FocusManagerConfigurator::finalize()
{
   if (_hkConverter != NULL)
   {
      _manager.removeMsgReceiver(*_hkConverter);
      delete _hkConverter;
      _hkConverter = NULL;
   }

   if (_focusReceiverControl != NULL)
   {
      _manager.removeMsgReceiver(*_focusReceiverControl);
      delete _focusReceiverControl;
      _focusReceiverControl = NULL;
   }

   if (_consistencyChecker != NULL)
   {
      _manager.removeMsgReceiver(*_consistencyChecker);
      //delete _consistencyChecker;
      //_consistencyChecker = NULL;
   }

   if (_focusIpcPayloadMsg != NULL)
   {
      _manager.removeMsgReceiver(*_focusIpcPayloadMsg);
      delete _focusIpcPayloadMsg;
      _focusIpcPayloadMsg = NULL;
   }
#ifdef ENABLE_FOCUS_TIMER
   _manager.getActivityTimer()->removeListener(_timer);
#endif

   bool result = Base::finalize();
   return result;
}


///////////////////////////////////////////////////////////////////////////////
bool FocusManagerConfigurator::initializeInputMsgChecker()
{
   setInputMsgChecker(new DefaultInputMsgChecker(_viewHandler));
   return true;
}


//#ifdef ENABLE_FOCUS_ON_HK_BACK
///////////////////////////////////////////////////////////////////////////////
bool FocusManagerConfigurator::initializeConsistencyChecker()
{
   //return Base::initializeConsistencyChecker();
   //setConsistencyChecker(FOCUS_NEW(FocusConsistencyChecker)(_manager));
   setConsistencyChecker(_consistencyChecker);
   return true;
}


///////////////////////////////////////////////////////////////////////////////
bool FocusManagerConfigurator::finalizeConsistencyChecker()
{
   if (_manager.getConsistencyChecker() != NULL)
   {
      //FOCUS_DELETE(_manager.getConsistencyChecker());
      setConsistencyChecker(NULL);
   }
   return true;
}


///////////////////////////////////////////////////////////////////////////////
bool FocusManagerConfigurator::initializeVisibilityManager()
{
   //return Base::initializeVisibilityManager();
   setVisibilityManager(FOCUS_NEW(Focus::FViewSpecificVisibilityManager)(_manager));
   return true;
}


//#endif

///////////////////////////////////////////////////////////////////////////////
bool FocusManagerConfigurator::finalizeInputMsgChecker()
{
   if (_manager.getInputMsgChecker() != NULL)
   {
      delete _manager.getInputMsgChecker();
      setInputMsgChecker(NULL);
   }
   return true;
}


#ifdef FOCUS_ACROSS_APPLICATION

///////////////////////////////////////////////////////////////////////////////
bool FocusManagerConfigurator::initializeIpcManager()
{
   if (_ipcAdapter)
   {
      _ipcManager = new Focus::FDefaultIpcManager(_manager, *_ipcAdapter);
      //_ipcManager = new Rnaivi::IpcManager(_manager, *_ipcAdapter);

      if (_ipcManager)
      {
         setIpcManager(_ipcManager);
      }
   }

   return true;
}


///////////////////////////////////////////////////////////////////////////////
bool FocusManagerConfigurator::finalizeIpcManager()
{
   if (_ipcManager != NULL)
   {
      delete _ipcManager;
   }
   setIpcManager(NULL);
   return true;
}


bool FocusManagerConfigurator::initializeTaskFactory()
{
   setTaskFactory(new ProjectTaskFactory(_manager));
   return true;
}


bool FocusManagerConfigurator::finalizeTaskFactory()
{
   if (_manager.getTaskFactory() != NULL)
   {
      delete _manager.getTaskFactory();
   }
   setTaskFactory(NULL);
   return true;
}


#endif

///////////////////////////////////////////////////////////////////////////////
bool FocusManagerConfigurator::initializeControllers()
{
   //Base::initializeControllers();
   setRootGroupControllerSetId(DefaultRootGroupControllerSetId);
   registerController(DefaultRootGroupControllerSetId, FOCUS_NEW(Focus::FDefaultEnterKeyController)(_manager));
   //registerController(DefaultRootGroupControllerSetId, new Focus::FDefaultRotaryController(_manager));
   registerController(DefaultRootGroupControllerSetId, FOCUS_NEW(Focus::DefaultRotaryController)(_manager));
   //registerController(DefaultRootGroupControllerSetId, new FDefaultJoystickController(_manager));
   registerController(DefaultRootGroupControllerSetId, FOCUS_NEW(JoyStickController)(_manager));

   registerController(DefaultRootGroupControllerSetId, FOCUS_NEW(DefaultFocusReqHandler)(_manager));

   //Default vertical list controller
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
   _listController = new Rnaivi::ListRotaryController();
#else//other scopes page wise scrolling
   _listController = new Rnaivi::ListRotaryController(false, true);
#endif

   registerController(FOCUS_CONTROLLER_SET_ID_DEFAULT_VERTICAL_LIST, _listController);
   FlexListWidget2D::SetDefaultFocusControllers(FOCUS_CONTROLLER_SET_ID_DEFAULT_VERTICAL_LIST);

   //Enter key controller
   registerController(FOCUS_CONTROLLER_SET_ID_LIST_ENTER_KEY, FOCUS_NEW(Rnaivi::ListEnterKeyController));
   registerController(FOCUS_CONTROLLER_SET_ID_DEFAULT_TEXT_SCROLL, FOCUS_NEW(TextScrollController));

   //Reversible list controller
   _reversibleListController = new Rnaivi::ListRotaryController();
   registerController(FOCUS_CONTROLLER_SET_ID_REVERSIBLE_VERTICAL_LIST, _reversibleListController);

   return true;
}


///////////////////////////////////////////////////////////////////////////////
bool FocusManagerConfigurator::finalizeControllers()
{
   Base::finalizeControllers();
   clearControllerSet(FOCUS_CONTROLLER_SET_ID_DEFAULT_VERTICAL_LIST, false);
   if (_listController != NULL)
   {
      delete _listController;
      _listController = NULL;
   }

   clearControllerSet(FOCUS_CONTROLLER_SET_ID_LIST_ENTER_KEY, true);

   clearControllerSet(FOCUS_CONTROLLER_SET_ID_REVERSIBLE_VERTICAL_LIST, false);
   if (_reversibleListController != NULL)
   {
      delete _reversibleListController;
      _reversibleListController = NULL;
   }

   clearControllerSet(FOCUS_CONTROLLER_SET_ID_DEFAULT_TEXT_SCROLL, true);

   return true;
}


///////////////////////////////////////////////////////////////////////////////
bool FocusManagerConfigurator::isAnyItemInFocus()
{
   return RnaiviFocus::isAnyItemInFocus(_viewHandler);
}


void FocusManagerConfigurator::hideOtherAppFocus()
{
   if (_ipcAdapter != NULL)
   {
      _ipcAdapter->sendFocusHideReq();
   }
}


void RnaiviFocus::hideAllViewFocus()
{
   Focus::FManager& _manager = Focus::FManager::getInstance();

   Focus::FTaskManager* taskManager = _manager.getTaskManager();
   Focus::FSessionManager* sessionManager = _manager.getSessionManager();

   ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "hideAllViewFocus App:%d", RnaiviFocus::getAppId()));

   if ((taskManager != NULL) && (sessionManager != NULL) && (sessionManager->getSession() == NULL)
         && (_manager.isFocusVisible()))
   {
      //begin a new session
      if ((sessionManager->beginSession(Focus::FSession::Master)) && (sessionManager->getSession() != NULL))
      {
         //add tasks to collect data from the visible views, merge the state info and to build the active view group
         taskManager->clearTasks();
         taskManager->addTask(FOCUS_NEW(Focus::FDefaultCourierInfoCollector)(_manager));
         taskManager->addTask(FOCUS_NEW(Focus::FDefaultCrtAppState2SessionUpdater)(_manager));
         taskManager->addTask(FOCUS_NEW(Focus::FDefaultAvgBuilder)(_manager));
         if (taskManager->executeTasks() == Focus::FTask::Completed)
         {
            _manager.printSessionDebugInfo();
            sessionManager->getSession()->Data.set(Focus::FSessionFocusVisibilityUpdateConfig(false));
            Focus::FAppStateSharedPtr appState = _manager.getCurrentAppState();
            if (!appState.PointsToNull())
            {
               //Do it for all views
               ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "hideAllViewFocus::FOCUS_HIDE All Views"));
               for (size_t i = 0; i < appState->getChildCount(); ++i)
               {
                  Focus::FViewState* viewState = appState->getChild(i);
                  if ((viewState != NULL)) //&& _manager.isViewActive(viewState->getId()))
                  {
                     viewState->setData(Focus::FViewFocusVisible(false));
                  }
               }
            }

            {
               taskManager->addTask(FOCUS_NEW(Focus::FAvg2FocusInfoUpdater)(_manager));
               taskManager->addTask(FOCUS_NEW(Focus::FDefaultSession2CrtAppStateUpdater)(_manager));
               taskManager->addTask(FOCUS_NEW(Focus::FDefaultCourierInfoPublisher)(_manager));
               taskManager->executeTasks();
            }
         }
      }

      _manager.printSessionDebugInfo();
      sessionManager->endSession();
   }

   (COURIER_MESSAGE_NEW(ListFocusLockDataResetReqMsg)())->Post();
   ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "hideAllViewFocus END"));
}


///////////////////////////////////////////////////////////////////////////////
Focus::FDefaultManagerConfigurator* RnaiviFocus::_configuraotr = NULL;
unsigned int RnaiviFocus::_appId = APPID_APPHMI_UNKNOWN;

///////////////////////////////////////////////////////////////////////////////
Focus::FDefaultManagerConfigurator* RnaiviFocus::createConfigurator(AppViewHandler& viewHandler)
{
   ETG_TRACE_USR4(("RnaiviFocus::createConfigurator"));
   _configuraotr = new FocusManagerConfigurator(Focus::FManager::getInstance(), viewHandler);
   return _configuraotr;
}


///////////////////////////////////////////////////////////////////////////////
Focus::FDefaultManagerConfigurator* RnaiviFocus::getConfigurator()
{
   return _configuraotr;
}


///////////////////////////////////////////////////////////////////////////////
void RnaiviFocus::setAppId(unsigned int appId)
{
   ETG_TRACE_USR4(("RnaiviTimer::setAppId:%d", appId));
   _appId = appId;
}


///////////////////////////////////////////////////////////////////////////////
bool RnaiviFocus::isAnyItemInFocus(AppViewHandler& viewHandler)
{
   Focus::FManager& manager = Focus::FManager::getInstance();
   if (manager.isFocusVisible())
   {
      Focus::FAppCurrentFocusInfo _currentFocusInfo = manager.getCurrentFocus();
      ETG_TRACE_USR4(("RnaiviFocus::isAnyItemInFocus App:%d", RnaiviFocus::getAppId()));
      ETG_TRACE_USR4(("RnaiviFocus::isAnyItemInFocus view:%s", _currentFocusInfo.ViewId.CStr()));
      ETG_TRACE_USR4(("RnaiviFocus::isAnyItemInFocus widget:%s", _currentFocusInfo.WidgetId.c_str()));

      if (0 != viewHandler.FindWidget(
               _currentFocusInfo.ViewId,
#if (COURIER_VERSION_MAJOR >= 3)
               Courier::CompositePath(),
#endif
               Courier::ItemId(_currentFocusInfo.WidgetId.c_str())))
      {
         ETG_TRACE_USR4(("RnaiviFocus::isAnyItemInFocus Valid"));
         return true;
      }
   }
   return false;
}


///////////////////////////////////////////////////////////////////////////////
/**
 * hideFocus - Hide a visible focus on items (It is also unlock and clear the last focus locked list informations)
 * @param[in] none
 * @parm[out] none
 * @return void
 */
void RnaiviFocus::hideFocus()
{
   Focus::FManager& manager = Focus::FManager::getInstance();

   if (manager.isFocusVisible())
   {
      manager.hideFocus();

      //If any item has focus locked with enter key, unlock and clear the last focus locked list informations
      (COURIER_MESSAGE_NEW(ListFocusLockDataResetReqMsg)())->Post();
   }
}


///////////////////////////////////////////////////////////////////////////////
void RnaiviTimer::onExpired()
{
#ifdef _TEST_FOCUS_REQUEST_MESSAGE
   FocusActionEnum action = FOCUS_SHOW;
   FocusTimerActionEnum timerAction = FOCUS_TIMER_STOP;
   const char* view = "Media#Scenes#MEDIA_AUX__BTAUDIO_MAIN_DEFAULT";
   const char* widget = "Button_TA/Button";
   Focus::FManager::getInstance().getOutputMsgHandler()->postMessage(COURIER_MESSAGE_NEW(FocusReqMsg)(action, timerAction, Courier::ViewId(view), widget));
#endif

#ifdef HIDE_FOCUS_TIMER

   ETG_TRACE_USR4(("RnaiviTimer::onExpired"));
   if (Focus::FManager::getInstance().isFocusVisible())
   {
      RnaiviFocus::hideFocus();
   }
#endif
}
