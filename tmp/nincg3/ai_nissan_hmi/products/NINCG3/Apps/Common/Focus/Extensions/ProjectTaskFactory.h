/**
*  @file   ProjectTaskFactory.h
*  @author ECV - vma6cob
*  @copyright (c) 2016 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_app_common
*/

#ifndef PROJECTTASKFACTORY_H_
#define PROJECTTASKFACTORY_H_

#include <Focus/Default/FDefaultTaskFactory.h>
#include <Focus/FTask.h>
#include <Focus/FSession.h>
#include <Focus/FManager.h>

class ProjectTaskFactory : public Focus::FDefaultTaskFactory
{
      typedef Focus::FDefaultTaskFactory Base;
   public:
      ProjectTaskFactory(Focus::FManager& manager) :
         Base(manager), _manager(manager) {}
      virtual ~ProjectTaskFactory() {}

      virtual bool configurePublishingTasks(Focus::FTaskManager& taskManager, Focus::FSession& session);

   private:
      Focus::FManager& _manager;
};


#endif /* PROJECTTASKFACTORY_H_ */
