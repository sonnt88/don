/**
* @file ListRotaryController.h
* @author RBEI/ECV3 RNAIVI
* @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
* @addtogroup Apphmi_Common
*/

#ifndef LISTROTARYCONTROLLER_H_
#define LISTROTARYCONTROLLER_H_

#include <Focus/FCommon.h>
#include <Focus/FController.h>

#include "ProjectBaseMsgs.h"
#include <Widgets/2D/FlexList/FlexScrollable.h>
#include "Common/Focus/FeatureDefines.h"

class ListFocusChangeReqMsg;
class ListFocusResetReqMsg;
class EncoderStatusChangedUpdMsg;

namespace Rnaivi {

class ListRotaryController: public Focus::FController
{
   public:
      ListRotaryController(bool invertedRotary = false, bool pagewiseScrolling = false);
      virtual ~ListRotaryController();

      virtual bool configureWidget(Focus::FSession&, Focus::FWidgetConfig&, Focus::FFrameworkWidget&);
      virtual bool onMessage(Focus::FSession& session, Focus::FGroup&, const Focus::FMessage&);

      void setDirection(bool forward = true)
      {
         _invertedRotary = !forward;
      }

   private:

      ListRotaryController(const ListRotaryController&);
      ListRotaryController& operator=(const ListRotaryController&);

      bool onFocusChangeReqMessage(Focus::FSession& session, Focus::FGroup& group, const ListFocusChangeReqMsg& msg);
      bool onFocusResetReqMessage(Focus::FSession& session, Focus::FGroup& group, const ListFocusResetReqMsg& msg);
      bool onRotaryMessage(Focus::FSession& session, Focus::FGroup& group, int encsteps, bool singelArrowList = false);
      bool onJoyStickMsgMessage(Focus::FSession& session, Focus::FGroup& group, const JoystickStatusChangedUpdMsg& msg);
      bool onListFocusReqMsgMessage(Focus::FSession& session, Focus::FGroup& group, const ListFocusReqMsg& msg);

      bool moveFocus(Focus::FSession& session, Focus::FGroup& group, int steps);

      bool handleIncompleteFocusMove(Focus::FSession& session, Focus::FGroup& group, int correction);
      static void onListWidgetContentUpdated(Candera::UInt32 listId, Candera::Bool success);
      bool isValidDirection(hmibase::FocusDirectionEnum);

      bool _pagewiseScrolling;
      bool _invertedRotary;
      ::Candera::ListScrollingOrientation _scrollingDirection;
      static Courier::IViewHandler* _viewHandler;
};


} /* namespace Rnaivi */
#endif /* LISTROTARYCONTROLLER_H_ */
