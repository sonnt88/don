/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#include "gui_std_if.h"
#include "hmi_trace_if.h"
#include "Common/Common_Trace.h"

#include "ProjectBaseMsgs.h"
#include "ProjectBaseTypes.h"
#include <Focus/Default/FDefaultCrtAppExchange.h>
#include <Focus/Default/FDefaultAvgBuilder.h>

#include "Common/Focus/RnaiviFocus.h"
#include "FocusRequestController.h"

#include <Focus/FGroupTraverser.h>
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_FOCUS
#include "trcGenProj/Header/FocusRequestController.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN

bool DefaultFocusReqHandler::onMessage(Focus::FSession& session, Focus::FGroup& group, const Focus::FMessage& msg)
{
   (void)group;

   bool consumed = false;
   if (msg.GetId() == FocusReqMsg::ID)
   {
      const FocusReqMsg* reqMsg = Courier::message_cast<const FocusReqMsg*>(&msg);
      if (reqMsg != NULL)
      {
         ETG_TRACE_USR4(("FocusReqHandler::onMessage action=%d, timer=%d, view=%s",
                         reqMsg->GetAction(), reqMsg->GetTimerAction(), reqMsg->GetView().CStr()));

         ETG_TRACE_USR4(("FocusReqHandler::onMessage widget=%s", reqMsg->GetWidget().GetCString()));

         if ((_manager.getActivityTimer() != NULL) && (reqMsg->GetTimerAction() != FOCUS_TIMER_NO_ACTION))
         {
            _manager.getActivityTimer()->stop();
         }

         switch (reqMsg->GetAction())
         {
            case FOCUS_SHOW:
            {
               if (!reqMsg->GetWidget().IsEmpty() && reqMsg->GetView() != Courier::ViewId(""))
               {
                  ETG_TRACE_USR4(("DefaultFocusReqHandler::setting focus"));
                  _manager.setFocus(session, reqMsg->GetView(), reqMsg->GetWidget().GetCString());
               }
               if (!RnaiviFocus::isFocusVisible(_manager))
               {
                  _manager.generateNewSequenceId();
                  _manager.setFocusVisible(true);
                  //To Do: If the current focus item is disabled, move to the next item
                  //moveFocus(session, group, 1);
               }
            }
            break;

            case FOCUS_SET:
            {
               //prevent avg builder to overwrite the visibility info of the views at the end of a session
               session.Data.set(Focus::FSessionFocusVisibilityUpdateConfig(false));
               _manager.setFocus(session, reqMsg->GetView(), reqMsg->GetWidget().GetCString());
            }
            break;

            case FOCUS_HIDE:
            {
               //prevent avg builder to overwrite the visibility info of the views at the end of a session
               session.Data.set(Focus::FSessionFocusVisibilityUpdateConfig(false));

               Focus::FAppStateSharedPtr appState = _manager.getCurrentAppState();
               if (!appState.PointsToNull())
               {
                  if (reqMsg->GetView() != Focus::Constants::InvalidViewId)
                  {
                     Focus::FViewState* viewState = _manager.getCurrentAppState()->getChild(reqMsg->GetView());
                     if (viewState != NULL)
                     {
                        viewState->setData(Focus::FViewFocusVisible(false));
                     }
                  }
                  else
                  {
                     //Do it for all views
                     ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "DefaultFocusReqHandler::FOCUS_HIDE All Views"));
                     for (size_t i = 0; i < appState->getChildCount(); ++i)
                     {
                        Focus::FViewState* viewState = appState->getChild(i);
                        if ((viewState != NULL)) //&& _manager.isViewActive(viewState->getId()))
                        {
                           viewState->setData(Focus::FViewFocusVisible(false));
                        }
                     }
                  }
               }
               else
               {
                  _manager.setFocusVisible(false);
               }
               ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "DefaultFocusReqHandler::FOCUS_HIDE AppId: %d", RnaiviFocus::getAppId()));
            }
            break;

            default:
               break;
         }

         if ((_manager.getActivityTimer() != NULL) && (reqMsg->GetTimerAction() == FOCUS_TIMER_RESTART))
         {
            _manager.getActivityTimer()->restart();
         }
         consumed = true;
      }
   }
   return consumed;
}


bool DefaultFocusReqHandler::moveFocus(Focus::FSession& session, Focus::FGroup& group, int steps)
{
   ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "DefaultFocusReqHandler::moveFocus steps=%d, group=%s",
                       steps, FID_STR(group.getId())));

   Focus::FManager& manager = Focus::FManager::getInstance();
   Focus::FWidget* newFocus = NULL;
   if (manager.getAvgManager() != NULL)
   {
      Focus::Focusable* f = manager.getAvgManager()->getFocus(session);
      if (f == NULL)
      {
         return false;
      }

      Focus::FWidgetVisitor widgetVisitor;

      bool ascending = true;
      if (steps < 0)
      {
         ascending = false;
         steps = -steps;
      }

      ETG_TRACE_USR4(("DefaultFocusReqHandler::moveFocus considering group: %s", FID_STR(group.getId())));
      Focus::FDefaultGroupTraverser traverser(widgetVisitor, group, f, ascending);

      //prevents infinite loops with wrap around
      size_t lastResetCount = Focus::Constants::InvalidIndex;

      //if focus is not visible check old focus
      if (!manager.isFocusVisible())
      {
         //also set steps to 1 and check the current/default focus
         steps = 1;
         f->visit(widgetVisitor);
      }

      while (static_cast<int>(widgetVisitor.getCount()) < steps)
      {
         //advanced to next
         if (traverser.advance())
         {
            //nothing to do
         }
         //no more elements
         else
         {
            //if wrap around reset the traverser
            Focus::FGroupData* groupData = group.getData<Focus::FGroupData>();
            if ((groupData != NULL) && (groupData->WrapAround) && (lastResetCount != widgetVisitor.getCount()))
            {
               //prevent infinite loops
               lastResetCount = widgetVisitor.getCount();

               traverser.reset();
            }
            else
            {
               break;
            }
         }
      }

      newFocus = widgetVisitor.getFocusable();
      if (newFocus != NULL)
      {
         manager.getAvgManager()->setFocus(session, newFocus);
      }
   }
   return newFocus != NULL;
}
