/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#ifndef __RNAIVI_FOCUS_REQ_HANDLER_H__
#define __RNAIVI_FOCUS_REQ_HANDLER_H__

#include <Focus/FManager.h>
#include <Focus/FManagerConfig.h>
#include "Common/Focus/FeatureDefines.h"

///////////////////////////////////////////////////////////////////////////////
class DefaultFocusReqHandler : public Focus::FController
{
   public:
      DefaultFocusReqHandler(Focus::FManager& manager) : _manager(manager) {}

      virtual bool onMessage(Focus::FSession& session, Focus::FGroup& group, const Focus::FMessage& msg);

   private:
      DefaultFocusReqHandler(const DefaultFocusReqHandler&);
      DefaultFocusReqHandler& operator=(const DefaultFocusReqHandler&);
      bool moveFocus(Focus::FSession& session, Focus::FGroup& group, int steps);

      Focus::FManager& _manager;
};


#endif
