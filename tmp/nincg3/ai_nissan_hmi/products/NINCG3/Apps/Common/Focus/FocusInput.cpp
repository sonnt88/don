/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#include "gui_std_if.h"

#include "FocusInput.h"

//For Setting encoder direction
#include "Common/Focus/RnaiviFocus.h"
#include "Common/Focus/Controller/ListRotaryController.h"
#include "ProjectBaseMsgs.h"
#include "ProjectBaseTypes.h"

//////// TRACE IF ///////////////////////////////////////////////////
#include "Common/Common_Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_FOCUS
#include "trcGenProj/Header/FocusInput.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN


///////////////////////////////////////////////////////////////////////////////
bool DefaultInputMsgChecker::isFocusInputMessage(const Focus::FMessage& msg) const
{
   bool result = false;
   ETG_TRACE_USR4(("DefaultInputMsgChecker::isFocusInputMessage id=%s", msg.GetName()));
   switch (msg.GetId())
   {
      case HKStatusChangedUpdMsg::ID:
      {
         const HKStatusChangedUpdMsg* hkMsg = Courier::message_cast<const HKStatusChangedUpdMsg*>(&msg);
         if (hkMsg != NULL)
         {
            result = isFocusInputHKMessage(*hkMsg);
         }
         break;
      }

      case EnterKeyStatusChangedUpdMsg::ID:
      {
         const EnterKeyStatusChangedUpdMsg* enterKeyMsg = Courier::message_cast<const EnterKeyStatusChangedUpdMsg*>(&msg);
         if (enterKeyMsg != NULL)
         {
            result = isFocusInputEnterKeyMessage(*enterKeyMsg);
         }
         break;
      }

      case EncoderStatusChangedUpdMsg::ID:
      {
         const EncoderStatusChangedUpdMsg* encoderMsg = Courier::message_cast<const EncoderStatusChangedUpdMsg*>(&msg);
         if (encoderMsg != NULL)
         {
            result = isFocusInputEncoderMessage(*encoderMsg);
         }
         break;
      }

      case JoystickStatusChangedUpdMsg::ID:
      {
         const JoystickStatusChangedUpdMsg* joystickMsg = Courier::message_cast<const JoystickStatusChangedUpdMsg*>(&msg);
         if (joystickMsg != NULL)
         {
            result = isFocusInputJoystickMessage(*joystickMsg);
         }
         break;
      }

      case FocusReqMsg::ID:            //Sets focus on specific element
      case ListFocusReqMsg::ID:        //Sets focus to top or bottom node in a list on Page up/down keys using next 2 messages
      case ListFocusResetReqMsg::ID:   //Reset focus to top or bottom node in a list
      case ListFocusChangeReqMsg::ID:  //Used for correction in list focus update
      case ListFocusScrollReqMsg::ID:  //Line Up/Down SK acts as encoder focus move
      {
         result = true;
         break;
      }

      default:
         break;
   }

   return result;
}


///////////////////////////////////////////////////////////////////////////////
bool DefaultInputMsgChecker::isFocusInputHKMessage(const HKStatusChangedUpdMsg& msg) const
{
   ETG_TRACE_USR4(("DefaultInputMsgChecker::isFocusInputHKMessage HKCode=%d State=%d App Id: %d", msg.GetHKCode(), msg.GetHKState(), RnaiviFocus::getAppId()));

   switch (msg.GetHKCode())
   {
         //todo: add here more Enter keys, also check the existing ones
      case HARDKEYCODE_SWC_UP:
      case HARDKEYCODE_SWC_DOWN:
      {
         if (RnaiviFocus::getAppId() == APPID_APPHMI_SDS)
         {
            return true;
         }
      }
      break;

      case HARDKEYCODE_SWC_OK:
      case HARDKEYCODE_HK_SELECT:
      {
         setHkState(msg.GetHKState());
         //check if any item is focused
         if (RnaiviFocus::isAnyItemInFocus(_viewHandler))
         {
            return true;
         }
         else
         {
            //when no item is focused and HK enter is pressed, send a notifier message
            EnterKeyNoFocusStateUpdMsg* hkEnterKeyNoFocusMsg = NULL;
            hkEnterKeyNoFocusMsg = COURIER_MESSAGE_NEW(EnterKeyNoFocusStateUpdMsg)(msg.GetHKState(), msg.GetHKCode());
            if (hkEnterKeyNoFocusMsg != NULL)
            {
               hkEnterKeyNoFocusMsg->Post();
            }
         }
      }
      break;
      case HARDKEYCODE_JOYSTICK_ENTER:
      {
         setHkState(msg.GetHKState());
         return true;
      }
      break;
      case HARDKEYCODE_SWC_ROTARY_ENCODER_BUTTON:
      case HARDKEYCODE_JOYSTICK_UP:
      case HARDKEYCODE_JOYSTICK_DOWN:
      case HARDKEYCODE_JOYSTICK_LEFT:
      case HARDKEYCODE_JOYSTICK_RIGHT:
      case HARDKEYCODE_JOYSTICK_UPPER_LEFT:
      case HARDKEYCODE_JOYSTICK_UPPER_RIGHT:
      case HARDKEYCODE_JOYSTICK_LOWER_LEFT:
      case HARDKEYCODE_JOYSTICK_LOWER_RIGHT:
      {
         return true;
      }
      break;

      default:
         break;
   }

   ETG_TRACE_USR4(("DefaultInputMsgChecker::isFocusInputMessage None"));
   return false;
}


hmibase::HardKeyStateEnum DefaultInputMsgChecker::_hkState = hmibase::HARDKEYSTATE_UNKNOWN;
void DefaultInputMsgChecker::setHkState(const hmibase::HardKeyStateEnum state)
{
   _hkState = state;
}


///////////////////////////////////////////////////////////////////////////////
bool DefaultInputMsgChecker::isFocusInputEnterKeyMessage(const EnterKeyStatusChangedUpdMsg&) const
{
   return true;
}


///////////////////////////////////////////////////////////////////////////////
bool DefaultInputMsgChecker::isFocusInputEncoderMessage(const EncoderStatusChangedUpdMsg& msg) const
{
   return (msg.GetEncCode() == ENCCODE_RIGHT_ENCODER || msg.GetEncCode() == ENCCODE_JOYSTICK_ENCODER);
}


///////////////////////////////////////////////////////////////////////////////
bool DefaultInputMsgChecker::isFocusInputJoystickMessage(const JoystickStatusChangedUpdMsg&) const
{
   return true;
}


///////////////////////////////////////////////////////////////////////////////
bool DefaultHKtoEnterKeyConverter::onMessage(const Focus::FMessage& msg)
{
   if (msg.GetId() == HKStatusChangedUpdMsg::ID)
   {
      const HKStatusChangedUpdMsg* hkMsg = Courier::message_cast<const HKStatusChangedUpdMsg*>(&msg);
      if ((hkMsg != NULL) && (_manager.getOutputMsgHandler() != NULL))
      {
         if (hkMsg->GetHKState() == hmibase::HARDKEYSTATE_DOWN)
         {
            hideFocusOnHk(hkMsg->GetHKCode());
         }
         switch (hkMsg->GetHKCode())
         {
               //todo: add here more Enter keys, also check the existing ones
            case HARDKEYCODE_SWC_UP:
            case HARDKEYCODE_SWC_DOWN:
            {
               if (RnaiviFocus::getAppId() == APPID_APPHMI_SDS)
               {
                  int step = ((hkMsg->GetHKCode() == HARDKEYCODE_SWC_UP) ? -1 : 1);
                  ETG_TRACE_USR4(("DefaultHKtoEnterKeyConverter::onMessage SWC Key: %d", step));
                  _manager.getOutputMsgHandler()->postMessage(COURIER_NEW(EncoderStatusChangedUpdMsg)(ENCCODE_RIGHT_ENCODER, step, 0));
                  return true;
               }
            }
            break;

            case HARDKEYCODE_SWC_OK:
            case HARDKEYCODE_HK_SELECT:
            case HARDKEYCODE_JOYSTICK_ENTER:
            {
               //check if any item is focused, dont convert if not focused
               if (RnaiviFocus::isAnyItemInFocus(_viewHandler))
               {
                  _manager.getOutputMsgHandler()->postMessage(COURIER_NEW(EnterKeyStatusChangedUpdMsg)(hkMsg->GetHKState(), hkMsg->GetHKCode()));
                  return true;
               }
               ETG_TRACE_USR4(("DefaultHKtoEnterKeyConverter::onMessage ignored"));
            }
            break;
            case HARDKEYCODE_SWC_ROTARY_ENCODER_BUTTON:
            {
               _manager.getOutputMsgHandler()->postMessage(COURIER_NEW(EnterKeyStatusChangedUpdMsg)(hkMsg->GetHKState(), hkMsg->GetHKCode()));
               return true;
            }
            break;
            case HARDKEYCODE_JOYSTICK_UP:
            case HARDKEYCODE_JOYSTICK_DOWN:
            case HARDKEYCODE_JOYSTICK_LEFT:
            case HARDKEYCODE_JOYSTICK_RIGHT:
            case HARDKEYCODE_JOYSTICK_UPPER_LEFT:
            case HARDKEYCODE_JOYSTICK_UPPER_RIGHT:
            case HARDKEYCODE_JOYSTICK_LOWER_LEFT:
            case HARDKEYCODE_JOYSTICK_LOWER_RIGHT:
            {
               if (hkMsg->GetHKState() == hmibase::HARDKEYSTATE_DOWN)
               {
                  _manager.getOutputMsgHandler()->postMessage(COURIER_NEW(JoystickStatusChangedUpdMsg)(hmibase::FocusDirectionEnum(hkMsg->GetHKCode() - HARDKEYCODE_JOYSTICK_UP), hkMsg->GetHKCode()));
                  return true;
               }
            }
            break;
            default:
               break;
         }
      }
   }
   return false;
}


///////////////////////////////////////////////////////////////////////////////
void DefaultHKtoEnterKeyConverter::hideFocusOnHk(Courier::UInt32 hKCode) const
{
   bool hideFocus = true;

   ETG_TRACE_USR4(("DefaultHKtoEnterKeyConverter::hideFocusOnHk code: %d App: %d", hKCode, RnaiviFocus::getAppId()));

   switch (hKCode)
   {
      case HARDKEYCODE_SWC_OK:
      case HARDKEYCODE_HK_ESC:
      case HARDKEYCODE_HK_SELECT:
      case HARDKEYCODE_JOYSTICK_ENTER:
      case HARDKEYCODE_SWC_ROTARY_ENCODER_BUTTON:
      case HARDKEYCODE_JOYSTICK_UP:
      case HARDKEYCODE_JOYSTICK_DOWN:
      case HARDKEYCODE_JOYSTICK_LEFT:
      case HARDKEYCODE_JOYSTICK_RIGHT:
      case HARDKEYCODE_JOYSTICK_UPPER_LEFT:
      case HARDKEYCODE_JOYSTICK_UPPER_RIGHT:
      case HARDKEYCODE_JOYSTICK_LOWER_LEFT:
      case HARDKEYCODE_JOYSTICK_LOWER_RIGHT:
      {
         hideFocus = false;
      }
      break;
      default:
         break;
   }

   if (hideFocus)//&& RnaiviFocus::isFocusVisible(_manager))
   {
      ETG_TRACE_USR4(("DefaultHKtoEnterKeyConverter::hideFocusOnHk hide focus"));

      RnaiviFocus::hideAllFocus();
   }
}
