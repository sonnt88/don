/**
* @file ListRotaryController.h
* @author RBEI/ECV3 RNAIVI
* @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
* @addtogroup Apphmi_Common
*/
#include "gui_std_if.h"
#include "hmi_trace_if.h"

#include "Common/Focus/Controller/ListEnterKeyController.h"
#include "Common/Common_Trace.h"

#include <Focus/FActiveViewGroup.h>
#include "ProjectBaseMsgs.h" //to use project base messages -sve2cob

//////// TRACE IF ///////////////////////////////////////////////////
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_FOCUS
#include "trcGenProj/Header/ListEnterKeyController.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN

namespace Rnaivi
{

ListEnterKeyController::ListEnterKeyController()
{
}

ListEnterKeyController::~ListEnterKeyController()
{

}

bool ListEnterKeyController::onMessage(Focus::FSession& /*session*/, Focus::FWidget& widget, const Focus::FMessage& msg)
{
   ETG_TRACE_USR4(("ListEnterKeyController::onMessage widget id=%s", msg.GetName()));
   bool result = false;
   switch (msg.GetId())
   {
      case EnterKeyStatusChangedUpdMsg::ID:
      {
         ListProviderEventInfo info;
         const char* name = widget.getId().c_str();
         ETG_TRACE_USR4(("ListEnterKeyController::onMessage name=%s", name));

         if (ListProviderEventInfo::GetItemIdentifierInfo(Courier::Identifier(name), info))
         {
            unsigned int listId = info.getListId(); // the list id for generic access
            unsigned int hdlRow = info.getHdlRow(); // normaly the index
            unsigned int hdlCol = info.getHdlCol(); // if more than 1 active element in one list row, e.g. Button in a button line

            ETG_TRACE_USR1(("ListEnterKeyController::onMessage listId=%d, hdlRow=%d, hdlCol=%d",
                            listId, hdlRow, hdlCol));

            //If EnterKeyStatusChangedUpdMsg comes here means current list item is following Focus_Progress(i.e. Focus group set is 12) group
            //So requesting ListRegistry perform necessary actions for received list item via ListFocusLockReqMsg
            //(i.e. lock(if it is not locked) or unlock(if it is already locked) the focus) -sve2cob
            const EnterKeyStatusChangedUpdMsg& oMsg = static_cast<const EnterKeyStatusChangedUpdMsg& >(msg);
            if(oMsg.GetState() == ::hmibase::HARDKEYSTATE_UP) //Send the message only hard key state is in release state
            	(COURIER_MESSAGE_NEW(ListFocusLockReqMsg)(listId, hdlRow, hdlCol))->Post();
         }
         result = true;
      }
      break;
      default:
    	  break;
   }
   return result;
}

}/* namespace Rnaivi */
