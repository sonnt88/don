/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */

#include "gui_std_if.h"

#include "ConnectionHandler.h"
#include "AppBase/HmiAsfComponentBase.h"
#include "MessageQueue.h"
#include "../RnaiviFocus.h"
#include <boost/lexical_cast.hpp>

#define CLOSE_CONNECTION "CLOSE_CONNECTION"

namespace Rnaivi {

IConnection* ConnectionHandler::_connection = NULL;

ConnectionHandler::ConnectionHandler(int appId)
{
   _connectionOpen = false;
   _appQueueName = getAppQueueName(appId);
}


ConnectionHandler::~ConnectionHandler()
{
   if (_connection)
   {
      _connection->Close();
      delete _connection;
      _connection = NULL;
   }
}


IConnection* ConnectionHandler::createConnection()
{
   if (!_connection)
   {
      _connection = new Rnaivi::MessageQueue();
      _connectionOpen = _connection->Open(_appQueueName.c_str(), IpcChannelOpenMode::Create);
   }
   return _connection;
}


void ConnectionHandler::closeConnection()
{
   if (_connection)
   {
      _connection->quickWrite(_appQueueName.c_str(), (const char*) CLOSE_CONNECTION, strlen(CLOSE_CONNECTION));
   }
   _connectionOpen = false;
}


IConnection* ConnectionHandler::getConnection()
{
   return _connection;
}


bool ConnectionHandler::isClosed()
{
   return (!_connectionOpen);
}


std::string ConnectionHandler::getAppQueueName(int appId)
{
   std::string appQueueName;
   appQueueName = boost::lexical_cast<std::string>(appId);
   appQueueName =  "focusQueue" + appQueueName;
   return appQueueName;
}


} /* namespace Rnaivi */
