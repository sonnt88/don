/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */

#ifndef RNAIVIIPCCHANNEL_H_
#define RNAIVIIPCCHANNEL_H_

#include "IConnection.h"

namespace Rnaivi {

class MessageQueue: public IConnection
{
   public:
      MessageQueue();
      virtual ~MessageQueue();

      IpcChannelStatus::Enum GetChannelState();
      //bool Init(/*IpcChannelConfiguration * config*/);
      bool Open(const char* name, IpcChannelOpenMode::Enum openMode = IpcChannelOpenMode::Open);
      bool Close();
      bool Write(const char* buffer, unsigned int msgLength);
      bool Read(char* buffer, unsigned int bufferSize, int& bytesRead);

   private:
      /** Allowed max. length for message queue name. */
      static const unsigned int _MaxMsgQueueNameLength = 255;

      /** Constructs the messageq queue names in the form of "/<name>_1" and "/<name>_2" */
      void SetName(const char* name, IpcChannelOpenMode::Enum openMode = IpcChannelOpenMode::Open);

      /** Open the message queues. */
      bool OpenQ();

      /** Create the message queues. */
      bool Create();

      /** OS handle for the message queue to read from. */
      mqd_t _IncomingMsgQueueHandle;

      /** Name of incoming message queue. */
      char  _IncomingMsgQueueName[_MaxMsgQueueNameLength];
};


} /* namespace Rnaivi */
#endif /* RNAIVIIPCCHANNEL_H_ */
