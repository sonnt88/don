/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */

#ifndef RNAIVIIPCADAPTER_H_
#define RNAIVIIPCADAPTER_H_

#include <Focus/FIpc.h>
#include <Focus/FData.h>
#include <Focus/Default/FDefaultSerializer.h>
#include "ProjectBaseTypes.h"
#include <Focus/Default/FDefaultIpcManager.h>
#include "../Utils/FocusDefines.h"
#include "CgiExtensions/AppViewHandler.h"

#define MESSAGE_BUFFER_SIZE (MAX_MSG_LENGTH + APP_ID_LEN + IPC_MSG_TYPE_LEN)

class IConnection;
class IpcAdapter : public Focus::FIpcAdapter
{
   public:
      IpcAdapter(Focus::FManager& _manager, AppViewHandler& viewHandler);
      virtual ~IpcAdapter();

      bool sendOtherAppsInfoRequest();
      bool sendOtherAppState(Focus::FAppStateSharedPtr appState);
      bool sendCurrentAppInfo(Focus::FAppConfigSharedPtr appConfig, Focus::FAppStateSharedPtr appState);
      bool sendFocusHideReq();

      void processIncomingMessages(char* payLoadMsg, unsigned int msgLen);

   private:
      unsigned int packPayloadData(char, char, Focus::Serialize::InfoSer& serializedInfo);
      unsigned int packPayloadData(char, char);

      void unPackPayloadData(char* incomingMsg, unsigned int msgLen, Focus::FAppConfigSharedPtr& appConfig, Focus::FAppStateSharedPtr& appState);
      bool sendMsg(const char*, unsigned int);
      void broadcastMsg(unsigned int, bool expectResponse = true);
      void updateAppMsgStatusInMap(int);
      void removeAppIdFromMap(int);
      void checkIfOtherAppInfoRecievedCompleted();
      bool isViewOnSurface(/*unsigned int surfaceId*/) const;
      void sendInvalidResponse();

      Focus::FManager& _manager;
      AppViewHandler& _viewHandler;

      char _msgBuffer[MESSAGE_BUFFER_SIZE];
      unsigned int _sourceAppId;
      std::vector<int> _appMsgMap;
};


#endif /* RNAIVIIPCADAPTER_H_ */
