/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */

#ifndef RNAICIIPCMANAGER_H_
#define RNAICIIPCMANAGER_H_

#include "asf/threading/Thread.h"
#include "ConnectionHandler.h"
#include <Focus/Default/FDefaultIpcManager.h>
#include <pthread.h>

class IpcHandler
{
   public:
      IpcHandler();
      virtual ~IpcHandler();
      int Run();

      //required for message post
      COURIER_MSG_MAP_BEGIN(1)
      COURIER_DUMMY_CASE(0)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_MAP_DELEGATE_END()

   private:
      void processMessage(char*, uint32);
      asf::threading::Thread* _hmiThread;
      Rnaivi::ConnectionHandler _connectionHandler;
};


#endif /* RNAICIIPCMANAGER_H_ */
