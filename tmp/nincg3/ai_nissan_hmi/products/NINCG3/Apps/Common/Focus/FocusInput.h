/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#ifndef __DEFAULT_FOCUS_INPUT_H__
#define __DEFAULT_FOCUS_INPUT_H__

#include <Focus/FManager.h>
#include <Focus/FManagerConfig.h>
#include "Common/Focus/FeatureDefines.h"

///////////////////////////////////////////////////////////////////////////////
class DefaultInputMsgChecker : public Focus::FInputMsgChecker
{
   public:
      DefaultInputMsgChecker(AppViewHandler& viewHandler): _viewHandler(viewHandler) {}
      virtual bool isFocusInputMessage(const Focus::FMessage& msg) const;

   private:
      bool isFocusInputHKMessage(const HKStatusChangedUpdMsg& msg) const;
      bool isFocusInputEnterKeyMessage(const EnterKeyStatusChangedUpdMsg& msg) const;
      bool isFocusInputEncoderMessage(const EncoderStatusChangedUpdMsg& msg) const;
      bool isFocusInputJoystickMessage(const JoystickStatusChangedUpdMsg& msg) const;
      static void setHkState(const hmibase::HardKeyStateEnum);

      AppViewHandler& _viewHandler;
      static hmibase::HardKeyStateEnum _hkState;
};


///////////////////////////////////////////////////////////////////////////////
class DefaultHKtoEnterKeyConverter : public Focus::FMsgReceiver
{
   public:
      DefaultHKtoEnterKeyConverter(Focus::FManager& manager, AppViewHandler& viewHandler) : _manager(manager), _viewHandler(viewHandler) {}

      virtual bool onMessage(const Focus::FMessage& msg);

   private:
      DefaultHKtoEnterKeyConverter(const DefaultHKtoEnterKeyConverter&);
      DefaultHKtoEnterKeyConverter& operator=(const DefaultHKtoEnterKeyConverter&);
      void hideFocusOnHk(Courier::UInt32 hKCode) const;

      Focus::FManager& _manager;
      AppViewHandler& _viewHandler;
};


#endif
