/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#include "gui_std_if.h"
#include <mqueue.h>
#include <errno.h>
#include "Common/Focus/IPC/MessageQueue.h"
#include "../Utils/FocusDefines.h"
#include "../RnaiviFocus.h"
#include <pthread.h>

#include "Common/Common_Trace.h"
//////// TRACE IF ///////////////////////////////////////////////////
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_FOCUS
#include "trcGenProj/Header/MessageQueue.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN

namespace Rnaivi {

#define CHECK(x, rc) \
    do { \
        if (!(x)) { \
            fprintf(stderr, "%s:%d: ", __func__, __LINE__); \
            perror(#x); \
            rc = false; \
        } \
    } while (0) \
 
#define STD_MSG_PRIORITY 10

MessageQueue::MessageQueue()
{
   _IncomingMsgQueueHandle = static_cast<mqd_t>(-1);
   strcpy(_IncomingMsgQueueName, "/");
}


MessageQueue::~MessageQueue()
{
}


// ------------------------------------------------------------------------
IpcChannelStatus::Enum MessageQueue::GetChannelState()
{
   if ((static_cast<int>(_IncomingMsgQueueHandle) == -1))
   {
      return IpcChannelStatus::Invalid;
   }
   return IpcChannelStatus::Open;
}


void MessageQueue::SetName(const char* name, IpcChannelOpenMode::Enum /*openMode*/)
{
   strcpy(_IncomingMsgQueueName, "/");
   strcat(_IncomingMsgQueueName, name);
   ETG_TRACE_USR4(("MessageQueue name:%s", _IncomingMsgQueueName));
}


// ------------------------------------------------------------------------
bool MessageQueue::OpenQ()
{
   _IncomingMsgQueueHandle = mq_open(_IncomingMsgQueueName, O_RDWR | O_NONBLOCK);
   if (static_cast<int>(_IncomingMsgQueueHandle) == -1)
   {
      ETG_TRACE_USR4(("MessageQueue Open error:%d", errno));
      return false;
   }
   return true;
}


// ------------------------------------------------------------------------
bool MessageQueue::Create()
{
   struct mq_attr incomingMsgQueueAttrib;

   // create incoming message queue
   incomingMsgQueueAttrib.mq_flags = 0;
   incomingMsgQueueAttrib.mq_maxmsg = MAX_MSG_IN_QUEUE;
   incomingMsgQueueAttrib.mq_msgsize = MAX_MSG_LENGTH;
   incomingMsgQueueAttrib.mq_curmsgs = 0;

   _IncomingMsgQueueHandle = mq_open(_IncomingMsgQueueName, O_RDONLY | O_CREAT, S_IRWXU | S_IRWXG | S_IRWXO, &incomingMsgQueueAttrib);
   if (static_cast<int>(_IncomingMsgQueueHandle) == -1)
   {
      ETG_TRACE_USR4(("MessageQueue Create error:%d", errno));
      return false;
   }
   ETG_TRACE_USR4(("MessageQueue successfully created:%s", _IncomingMsgQueueName));

   return true;
}


// ------------------------------------------------------------------------
bool MessageQueue::Open(const char* name, IpcChannelOpenMode::Enum openMode)
{
   //LOG_DEBUG("RnaiviIpcChannel Open %s\n", name);
   SetName(name, openMode);

   switch (openMode)
   {
      case IpcChannelOpenMode::Open:
         return OpenQ();

      case IpcChannelOpenMode::Create:
         return Create();

      default:
         break;
   }
   return false;
}


// ------------------------------------------------------------------------
bool MessageQueue::Close()
{
   bool lRc = true;
   CHECK(((mqd_t) - 1 != mq_close(_IncomingMsgQueueHandle)), lRc);

   return lRc;
}


// ------------------------------------------------------------------------
bool MessageQueue::Write(const char* buffer, unsigned int msgLength)
{
   int retVal = mq_send(_IncomingMsgQueueHandle, reinterpret_cast<const char*>(buffer), msgLength, STD_MSG_PRIORITY);

   if (retVal == -1)
   {
      ETG_TRACE_USR4(("MessageQueue Write error:%d", errno));
      return false;
   }

   return true;
}


// ------------------------------------------------------------------------
bool MessageQueue::Read(char* buffer, unsigned int bufferSize, int& bytesRead)
{
   ETG_TRACE_USR4(("MessageQueue read queue:%s", _IncomingMsgQueueName));
   unsigned int msgPrio;

   bytesRead = mq_receive(_IncomingMsgQueueHandle, reinterpret_cast<char*>(buffer), bufferSize, &msgPrio);

   if (bytesRead == -1)
   {
      if (errno == EAGAIN)
      {
         // means that queue was empty
         bytesRead = 0;
         return true;
      }
      else
      {
         //LOG_SYSCALL_ERROR("mq_receive");
         return false;
      }
   }
   return true;
}


} /* namespace Rnaivi */
