/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2016                     */
/* ------------------------------------------------------------------------ */

#ifndef FOCUSSURFACE_H_
#define FOCUSSURFACE_H_

#include "ProjectBaseTypes.h"
#include <vector>

namespace Rnaivi {

class FocusSurface
{
   public:
      void Update(unsigned int surfaceID, int state);
      bool isVisible();

      static hmibase::hmiappstates getAppState()
      {
         return _currentAppState;
      }
      static unsigned int getLastVisibleSurface()
      {
         return _currentAppSurfaceId;
      }

      FocusSurface();
      virtual ~FocusSurface();

   private:
      static hmibase::hmiappstates _currentAppState;
      static unsigned int _currentAppSurfaceId;
      std::vector<unsigned int> _activeSurface;
};


} /* namespace Rnaivi */
#endif /* FOCUSSURFACE_H_ */
