/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */
#ifndef __JOYSTICK_UTIL_H__
#define __JOYSTICK_UTIL_H__

/*****************************************************************************/
/* FOCUS MANAGER INCLUDES                                                    */
/*****************************************************************************/
#include <Focus/FCommon.h>
#include <Focus/FController.h>

/**************************************************************************/
/* FORWARD DECLARATIONS                                                   */
/**************************************************************************/
namespace Focus {
class FSession;
class FWidget;
class FWidgetConfig;
class FGroup;
}


class JoystickSearchAreaAdjuster
{
   public:
      virtual ~JoystickSearchAreaAdjuster() {}
      Focus::FRectangle calculateSearchArea(const Focus::FRectangle& origin, hmibase::FocusDirectionEnum direction) const;

   protected:
      virtual void adjustUp(const Focus::FRectangle& origin, Focus::FRectangle& searchArea) const = 0;
      virtual void adjustRight(const Focus::FRectangle& origin, Focus::FRectangle& searchArea) const = 0;
      virtual void adjustDown(const Focus::FRectangle& origin, Focus::FRectangle& searchArea) const = 0;
      virtual void adjustLeft(const Focus::FRectangle& origin, Focus::FRectangle& searchArea) const = 0;
};


class RestrictedSearchAreaAdjuster : public JoystickSearchAreaAdjuster
{
   public:
      virtual ~RestrictedSearchAreaAdjuster() {}

   protected:
      virtual void adjustUp(const Focus::FRectangle& origin, Focus::FRectangle& searchArea) const;
      virtual void adjustRight(const Focus::FRectangle& origin, Focus::FRectangle& searchArea) const;
      virtual void adjustDown(const Focus::FRectangle& origin, Focus::FRectangle& searchArea) const;
      virtual void adjustLeft(const Focus::FRectangle& origin, Focus::FRectangle& searchArea) const;
};


class JoyStickUtil
{
   public:
      static Focus::FGroup* tryGetNewFocusGroup(Focus::FManager& _manager, Focus::FSession& session, hmibase::FocusDirectionEnum direction,
            Focus::FWidget& originWidget, const JoystickSearchAreaAdjuster& searchAreaAdjuster);

      static Focus::FWidget* tryGetNewFocus(Focus::FManager& _manager, Focus::FGroup& group, hmibase::FocusDirectionEnum direction,
                                            Focus::FWidget& originWidget, const JoystickSearchAreaAdjuster& searchAreaAdjuster);

      static Focus::FGroup* getConfiguredGroup(Focus::FGroup& group);

      //static Focus::FWidgetConfig* getConfiguredGroup(Focus::FWidgetConfig& group);

      static tU16 readVarient();
      static bool isJoyStickPresent();

      static bool isListHasIndicators(Focus::FGroup& group);
};


#endif
