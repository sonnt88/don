/**
 * @file ListRotaryController.cpp
 * @author RBEI/ECV3 RNAIVI
 * @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 * @addtogroup Apphmi_Common
 */

#include "gui_std_if.h"

#include "hmi_trace_if.h"
#include "Common/Common_Trace.h"

#include "Common/Focus/Controller/ListRotaryController.h"
#include "Common/Focus/RnaiviFocus.h"
#include "Common/Focus/Utils/FUtils.h"

#include <Widgets/2D/FlexList/ListContentUpdater.h>

#include <Focus/FConfigInfo.h>
#include <Focus/FData.h>
#include <Focus/FManager.h>
#include <Focus/FGroupTraverser.h>
#include <Focus/Default/FDefaultAvgManager.h>
#include <Widgets/2D/FlexList/FlexListWidget2D.h>
#include <Widgets/2D/Button/ButtonWidget2D.h>

#include "ProjectBaseTypes.h"
#include "ProjectBaseMsgs.h"
#include "JoyStickUtil.h"

//////// TRACE IF ///////////////////////////////////////////////////
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_FOCUS
#include "trcGenProj/Header/ListRotaryController.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN


namespace Rnaivi {

Courier::IViewHandler* ListRotaryController::_viewHandler = NULL;

ListRotaryController::ListRotaryController(bool invertedRotary, bool pagewiseScrolling)
   : _invertedRotary(invertedRotary), _pagewiseScrolling(pagewiseScrolling)
{
   _scrollingDirection = ::Candera::Vertical;
}


ListRotaryController::~ListRotaryController()
{
}


bool ListRotaryController::configureWidget(Focus::FSession& /*session*/, Focus::FWidgetConfig& /*handle*/, Focus::FFrameworkWidget& widget)
{
   ETG_TRACE_USR4(("ListRotaryController::configureWidget"));

   _viewHandler = (widget.GetParentView() != NULL) ? widget.GetParentView()->GetViewHandler() : NULL;

   ListContentUpdater::SetContentUpdateCallback(onListWidgetContentUpdated);

   FlexListWidget2D* flexListWidget2D = dynamic_cast<FlexListWidget2D*>(&widget);

   if (flexListWidget2D != NULL)
   {
      _scrollingDirection = flexListWidget2D->GetScrollingOrientation();
   }

   return true;
}


static bool isWidgetValid(Focus::Focusable& f)
{
   Focus::FWidgetVisitor visitor;
   f.visit(visitor);
   //if it is valid (count == 1)
   return (visitor.getCount() > 0);
}


bool ListRotaryController::onMessage(Focus::FSession& session, Focus::FGroup& group, const Focus::FMessage& msg)
{
   ETG_TRACE_USR4(("ListRotaryController::onMessage id=%s", msg.GetName()));
   bool result = false;
   switch (msg.GetId())
   {
      case ListFocusResetReqMsg::ID:
      {
         const ListFocusResetReqMsg* focusResetMsg = Courier::message_cast<const ListFocusResetReqMsg*>(&msg);
         if (focusResetMsg != NULL)
         {
            result = onFocusResetReqMessage(session, group, *focusResetMsg);
         }
         break;
      }
      case ListFocusChangeReqMsg::ID:
      {
         const ListFocusChangeReqMsg* focusChangeMsg = Courier::message_cast<const ListFocusChangeReqMsg*>(&msg);
         if (focusChangeMsg != NULL)
         {
            result = onFocusChangeReqMessage(session, group, *focusChangeMsg);
         }
         break;
      }
#ifdef ENABLE_PREVIOUS_NEXT_PAGE_FOCUS
      case ListFocusReqMsg::ID:
      {
         const ListFocusReqMsg* listFocusRequestmsg = Courier::message_cast<const ListFocusReqMsg*>(&msg);
         if (listFocusRequestmsg != NULL)
         {
            result = onListFocusReqMsgMessage(session, group, *listFocusRequestmsg);
         }
         break;
      }
#endif
      case ListFocusScrollReqMsg::ID:
      {
         const ListFocusScrollReqMsg* listFocusScrollReqMsg = Courier::message_cast<const ListFocusScrollReqMsg*>(&msg);
         if (listFocusScrollReqMsg != NULL)
         {
            ETG_TRACE_USR4(("ListFocusScrollReqMsg ListId:%d Steps:%d ", listFocusScrollReqMsg->GetListId(), listFocusScrollReqMsg->GetSteps()));
            result = onRotaryMessage(session, group, listFocusScrollReqMsg->GetSteps(), true);
         }
         break;
      }

      case EncoderStatusChangedUpdMsg::ID:
      {
         const EncoderStatusChangedUpdMsg* encoderMsg = Courier::message_cast<const EncoderStatusChangedUpdMsg*>(&msg);
         if (encoderMsg != NULL)
         {
            ETG_TRACE_USR4(("EncoderStatusChangedUpdMsg mEncCode:%d mEncSteps:%d ", encoderMsg->GetEncCode(), encoderMsg->GetEncSteps()));
            result = onRotaryMessage(session, group, encoderMsg->GetEncSteps());
         }
         break;
      }

      case JoystickStatusChangedUpdMsg::ID:
      {
         const JoystickStatusChangedUpdMsg* joyStickMsg = Courier::message_cast<const JoystickStatusChangedUpdMsg*>(&msg);
         if (joyStickMsg != NULL)
         {
            ETG_TRACE_USR4(("JoystickStatusChangedUpdMsg Direction %d", joyStickMsg->GetDirection()));
            result = onJoyStickMsgMessage(session, group, *joyStickMsg);
         }
         break;
      }

      default:
         break;
   }

   return result;
}


bool ListRotaryController::onListFocusReqMsgMessage(Focus::FSession& /*session*/, Focus::FGroup& /*group*/, const ListFocusReqMsg& msg)
{
   int position = 0;
   bool result = false;

   ListFocusResetReqMsg* listFocusResetMsg = NULL;
   ListFocusChangeReqMsg* listFocusChangeMsg = NULL;
   switch (msg.GetListChangeType())
   {
      case ListChangePageUp:
      {
         ETG_TRACE_USR4(("ListFocusReqMsg::ListChangePageUp"));
         position = 1;
         listFocusResetMsg = COURIER_MESSAGE_NEW(ListFocusResetReqMsg)(msg.GetListId(), LIST_FOCUS_RESET_TO_LAST_VISIBLE);
         listFocusChangeMsg = COURIER_MESSAGE_NEW(ListFocusChangeReqMsg)(msg.GetListId(), position);
         result = true;

         break;
      }
      case ListChangePageDown:
      {
         ETG_TRACE_USR4(("ListFocusReqMsg::ListChangePageDown"));
         position = 1;
         listFocusResetMsg = COURIER_MESSAGE_NEW(ListFocusResetReqMsg)(msg.GetListId(), LIST_FOCUS_RESET_TO_FIRST_VISIBLE);
         listFocusChangeMsg = COURIER_MESSAGE_NEW(ListFocusChangeReqMsg)(msg.GetListId(), position);
         result = true;
         break;
      }

      case ListChangeDown :
      case ListChangeUp:
      {
         //TO DO: position to be checked
         position = 1;
         listFocusChangeMsg = COURIER_MESSAGE_NEW(ListFocusChangeReqMsg)(msg.GetListId(), position);
         result = true;
         break;
      }

      default:
         break;
   }

   if (result)
   {
      Focus::FManager& manager = Focus::FManager::getInstance();
      if (manager.getSessionManager() != NULL)
      {
         manager.getSessionManager()->requestSessionSuspend();
      }
      if (listFocusResetMsg != NULL)
      {
         listFocusResetMsg->Post();
      }
      if (listFocusChangeMsg != NULL)
      {
         listFocusChangeMsg->Post();
      }
   }

   return result;
}


bool ListRotaryController::onFocusResetReqMessage(Focus::FSession& session, Focus::FGroup& group, const ListFocusResetReqMsg& msg)
{
   bool result = false;

   ETG_TRACE_USR1_CLS((TR_CLASS_HMI_FW_FOCUS, "ListRotaryController::onFocusResetReqMessage listId=%u, action=%d, childCount=%u",
                       msg.GetListId(), msg.GetAction(), group.Children.count()));

   Focus::FListData* listData = group.getData<Focus::FListData>();
   if ((listData != NULL) && (msg.GetListId() == static_cast<unsigned int>(listData->ListId))
         && (group.Children.count() > 0))
   {
      Focus::Focusable* newFocus = NULL;
      int steps = 0;//used to move focus in case new focus is not valid
      switch (msg.GetAction())
      {
         case LIST_FOCUS_RESET_TO_DEFAULT:
         case LIST_FOCUS_RESET_TO_FIRST_VISIBLE:
         {
            newFocus = group.Children.get(0);
            steps = 1;
            break;
         }
         case LIST_FOCUS_RESET_TO_LAST_VISIBLE:
         {
            newFocus = group.Children.get(group.Children.count() - 1);
            steps = -1;
            break;
         }
         default:
            break;
      }
      if (newFocus != NULL)
      {
         Focus::FManager& manager = Focus::FManager::getInstance();
         Focus::FDefaultAvgManager avgManager(manager);
         avgManager.setFocus(session, newFocus);

         bool isValid = isWidgetValid(*newFocus);

         ETG_TRACE_USR1_CLS((TR_CLASS_HMI_FW_FOCUS, "DefaultListRotaryController::onFocusResetReqMessage isValid=%u, newFocus=%s",
                             isValid, FID_STR(newFocus->getId())));

         int actualSteps = 0;
         //if item is not valid we need to skip it
         if (!isValid)
         {
            actualSteps = avgManager.moveFocus(session, group, steps);
         }
         if ((isValid || (actualSteps != 0)) && !manager.isFocusVisible())
         {
            manager.setFocusVisible(true);
         }
      }
      result = true;
   }

   return result;
}


bool ListRotaryController::onFocusChangeReqMessage(Focus::FSession& session, Focus::FGroup& group, const ListFocusChangeReqMsg& msg)
{
   bool result = false;

   ETG_TRACE_USR1_CLS((TR_CLASS_HMI_FW_FOCUS, "ListRotaryController::onFocusChangeReqMessage listId=%u, steps=%d",
                       msg.GetListId(), msg.GetSteps()));

   Focus::FListData* listData = group.getData<Focus::FListData>();
   if ((listData != NULL) && (msg.GetListId() == static_cast<unsigned int>(listData->ListId)))
   {
      Focus::FManager& manager = Focus::FManager::getInstance();
      Focus::FDefaultAvgManager avgManager(manager);
      int actualSteps = avgManager.moveFocus(session, group, msg.GetSteps());
      if ((actualSteps != 0) && (!manager.isFocusVisible()))
      {
         manager.setFocusVisible(true);
      }

      result = true;
   }

   return result;
}


bool ListRotaryController::onRotaryMessage(Focus::FSession& session, Focus::FGroup& group, int encsteps, bool singelArrowList)
{
   bool result = false;
   Focus::FManager& manager = Focus::FManager::getInstance();

   if (manager.getActivityTimer() != NULL)
   {
      manager.getActivityTimer()->restart();
   }

   Focus::FDefaultAvgManager avgManager(manager);

   int steps = encsteps;
   if (_invertedRotary)
   {
      steps = -steps;
   }

   //if focus is not visible check old focus
   if (!Rnaivi::FUtils::isCurrentFocusAvailable(manager, session, group))
   {
      //we need a new sequence for the preserve focus
      manager.generateNewSequenceId();

      //get default/current focus
      Focus::Focusable* f = avgManager.getFocus(session, &group);
      if (f != NULL)
      {
         if (isWidgetValid(*f))
         {
            //make focus visible
            manager.setFocusVisible(true);
            avgManager.setFocus(session, f);
            result = true;
         }
         //otherwise proceed with 1 step movement of the focus which skips disabled items
         else
         {
            steps = (steps < 0) ? -1 : 1;
         }
      }
   }

   if (!result || singelArrowList)
   {
      int actualSteps = avgManager.moveFocus(session, group, steps);
      //if focus is not visible yet make it visible
      if ((actualSteps != 0) && (!Rnaivi::FUtils::isCurrentFocusAvailable(manager, session, group)))
      {
         manager.setFocusVisible(true);
      }
      if (steps != actualSteps)
      {
         result = handleIncompleteFocusMove(session, group, steps - actualSteps);
      }
      else
      {
         result = true;
      }
   }
   return result;
}


bool ListRotaryController::onJoyStickMsgMessage(Focus::FSession& session, Focus::FGroup& group, const JoystickStatusChangedUpdMsg& msg)
{
   bool result = false;

   Focus::FManager& manager = Focus::FManager::getInstance();

   Focus::FDefaultAvgManager avgManager(manager);

   Focus::FGroup* newGroup = NULL;
   if (manager.getAvgManager() != NULL)
   {
      Focus::FWidget* originWidget = dynamic_cast<Focus::FWidget*>(manager.getAvgManager()->getFocus(session));
      if (originWidget == NULL)
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "ListRotaryController::onJoyStickMsgMessage::origin widget null"));
         return false;
      }

      //first search is with a restricted search area
      hmibase::FocusDirectionEnum joystickDirection = msg.GetDirection();
      newGroup = JoyStickUtil::tryGetNewFocusGroup(manager, session, joystickDirection, *originWidget, RestrictedSearchAreaAdjuster());

      if (newGroup != NULL)
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "ListRotaryController::onJoyStickMsgMessage::New group NOT null"));
         result = false;
      }
      else
      {
         if (manager.getActivityTimer() != NULL)
         {
            manager.getActivityTimer()->restart();
         }

         const Focus::FViewId& viewId = manager.getCurrentFocus().ViewId;
         const Focus::FId& widgetId = manager.getCurrentFocus().WidgetId;

         //if focus is not visible check old focus
         if (!manager.isFocusVisible())
         {
            //we need a new sequence for the preserve focus
            manager.generateNewSequenceId();

            //get default/current focus
            Focus::Focusable* f = avgManager.getFocus(session, &group);
            if (f != NULL)
            {
               if (isWidgetValid(*f))
               {
                  //make focus visible
                  manager.setFocusVisible(true);
                  avgManager.setFocus(session, f);
                  result = true;
               }
               else
               {
                  //handle here when first item is disabled
               }
            }
         }

         if (!result)
         {
            if (isValidDirection(joystickDirection))
            {
               int steps = (joystickDirection == hmibase::Up || joystickDirection == hmibase::Left) ? -1 : 1;
               if (JoyStickUtil::isListHasIndicators(group))
               {
                  ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "ListRotaryController::onJoyStickMsgMessage - Scroll list with indicator"));
                  int actualSteps = avgManager.moveFocus(session, group, steps);
                  //if focus is not visible yet make it visible
                  if ((actualSteps != 0) && (!manager.isFocusVisible()))
                  {
                     manager.setFocusVisible(true);
                  }
                  if (steps != actualSteps)
                  {
                     handleIncompleteFocusMove(session, group, steps - actualSteps);
                  }
                  result = true;

                  //to avoid flicker when previous lock state row gets back to normal after focus is shifted to next element
                  //Only applicable for list with indicator
                  if (_viewHandler != NULL)
                  {
                     ButtonWidget2D* widget = dynamic_cast<ButtonWidget2D*>(_viewHandler->FindWidget(
                                                 viewId,
#if (COURIER_VERSION_MAJOR >= 3)
                                                 Courier::CompositePath(),
#endif
                                                 Courier::ItemId(widgetId.c_str())));
                     if (widget != NULL)
                     {
                        if (widget->IsActive())
                        {
                           if (manager.getSessionManager() != NULL)
                           {
                              manager.getSessionManager()->requestSessionSuspend();
                           }
                           //If any item has focus locked with enter key, unlock and clear the last focus locked list informations
                           (COURIER_MESSAGE_NEW(ListFocusLockDataResetReqMsg)())->Post();
                        }
                     }
                  }
               }
               else
               {
                  handleIncompleteFocusMove(session, group, steps);
                  result = true;
               }
            }
         }
      }
   }

   return result;
}


bool ListRotaryController::isValidDirection(hmibase::FocusDirectionEnum joystickDirection)
{
   bool result = false;
   switch (_scrollingDirection)
   {
      case ::Candera::Horizontal:
      {
         if (joystickDirection == hmibase::Left || joystickDirection == hmibase::Right)
         {
            result = true;
         }
      }
      break;

      case ::Candera::Vertical:
      {
         if (joystickDirection == hmibase::Up || joystickDirection == hmibase::Down)
         {
            result = true;
         }
      }
      break;

      default:
         break;
   }
   return result;
}


bool ListRotaryController::moveFocus(Focus::FSession& session, Focus::FGroup& group, int steps)
{
   ETG_TRACE_USR1_CLS((TR_CLASS_APPHMI_COMMON_FOCUS, "ListRotaryController::moveFocus steps=%d, group=%s",
                       steps, FID_STR(group.getId())));

   Focus::FManager& manager = Focus::FManager::getInstance();
   Focus::FWidget* newFocus = NULL;
   if (manager.getAvgManager() != NULL)
   {
      Focus::Focusable* f = manager.getAvgManager()->getFocus(session);
      if (f == NULL)
      {
         return false;
      }

      Focus::FWidgetVisitor widgetVisitor;

      bool ascending = true;
      if (steps < 0)
      {
         ascending = false;
         steps = -steps;
      }

      ETG_TRACE_USR4(("ListRotaryController::moveFocus considering group: %s", FID_STR(group.getId())));
      Focus::FDefaultGroupTraverser traverser(widgetVisitor, group, f, ascending);

      //prevents infinite loops with wrap around
      size_t lastResetCount = Focus::Constants::InvalidIndex;

      //if focus is not visible check old focus
      if (!manager.isFocusVisible())
      {
         //also set steps to 1 and check the current/default focus
         steps = 1;
         f->visit(widgetVisitor);
      }

      while (static_cast<int>(widgetVisitor.getCount()) < steps)
      {
         //advanced to next
         if (traverser.advance())
         {
            //nothing to do
         }
         //no more elements
         else
         {
            //if wrap around reset the traverser
            Focus::FGroupData* groupData = group.getData<Focus::FGroupData>();
            if ((groupData != NULL) && (groupData->WrapAround) && (lastResetCount != widgetVisitor.getCount()))
            {
               //prevent infinite loops
               lastResetCount = widgetVisitor.getCount();

               traverser.reset();
            }
            else
            {
               break;
            }
         }
      }

      newFocus = widgetVisitor.getFocusable();
      if (newFocus != NULL)
      {
         manager.getAvgManager()->setFocus(session, newFocus);
      }
   }
   return newFocus != NULL;
}


bool ListRotaryController::handleIncompleteFocusMove(Focus::FSession& session, Focus::FGroup& group, int correction)
{
   Focus::FManager& manager = Focus::FManager::getInstance();

   //no correction necessary
   if (correction == 0)
   {
      return false;
   }

   Focus::FGroupData* groupData = group.getData<Focus::FGroupData>();
   Focus::FListData* listData = group.getData<Focus::FListData>();
   if ((groupData == NULL) || (listData == NULL))
   {
      ETG_TRACE_USR1(("ListRotaryController::handleIncompleteFocusMove No list data"));
      return false;
   }

   ETG_TRACE_USR1(("ListRotaryController::handleIncompleteFocusMove correction=%d, listId=%d, firstVisibleIndex=%d, visibleItems=%d, totalItems=%d",
                   correction, listData->ListId, listData->FirstVisibleIndex, listData->VisibleItemCount, listData->TotalItemCount));

   //invalid list data or no scroll possible due to few elements in the list
   if ((listData->ListId < 0) || (listData->FirstVisibleIndex < 0)
         || (listData->VisibleItemCount <= 0) || (listData->TotalItemCount <= 0)
         || (listData->TotalItemCount < listData->VisibleItemCount))
   {
      if (_invertedRotary)
      {
         //consume the message here, since wrap around is false and default rotary controller should not process the message
         return true;
      }
      else
      {
         return false;
      }
   }

   bool result = false;
   ListChangeMsg* listChangeMsg = NULL;
   ListFocusResetReqMsg* listFocusResetMsg = NULL;
   if (correction > 0)
   {
      //can scroll down
      if (listData->FirstVisibleIndex + listData->VisibleItemCount < listData->TotalItemCount)
      {
         result = true;
         if (_pagewiseScrolling)
         {
            listChangeMsg = COURIER_MESSAGE_NEW(ListChangeMsg)(listData->ListId, ListChangePageDown, 1);
            if (listData->TotalItemCount - (listData->FirstVisibleIndex + listData->VisibleItemCount) >= listData->VisibleItemCount)
            {
               listFocusResetMsg = COURIER_MESSAGE_NEW(ListFocusResetReqMsg)(listData->ListId, LIST_FOCUS_RESET_TO_FIRST_VISIBLE);
               --correction;//one step will be "consumed" by the ListFocusResetReqMsg
            }
         }
         else
         {
            listChangeMsg = COURIER_MESSAGE_NEW(ListChangeMsg)(listData->ListId, ListChangeDown, correction);
         }
      }
      //can't scroll down due to last item visible
      else
      {
         //reset to first item visible, since wrap around is true and last item visible
         if (groupData->WrapAround)
         {
            if (listData->TotalItemCount > listData->VisibleItemCount)
            {
               //applied when total items are more than visible items list have to be reset
               result = true;
               listChangeMsg = COURIER_MESSAGE_NEW(ListChangeMsg)(listData->ListId, ListChangeSet, 0);
               --correction;//one step will be "consumed" by the ListFocusResetReqMsg
            }
            else
            {
               //just set the focus to the top element
               //ListFocusResetReqMsg listFocusResetMsg(listData->ListId, LIST_FOCUS_RESET_TO_FIRST_VISIBLE);
               //return onFocusResetReqMessage(session, group, listFocusResetMsg);
               return moveFocus(session, group, correction);
            }
         }
         //nothing to do, since last item visible and wrap around is false
         else
         {
            //actually nothing to do, except inverted rotary is true
            if (_invertedRotary)
            {
               //consume the message here, since wrap around is false and default rotary controller should not process the message
               return true;
            }
         }
      }
   }
   //if correction < 0
   else
   {
      //can scroll up
      if (listData->FirstVisibleIndex > 0)
      {
         result = true;
         if (_pagewiseScrolling)
         {
            listChangeMsg = COURIER_MESSAGE_NEW(ListChangeMsg)(listData->ListId, ListChangePageUp, -1);
            if (listData->FirstVisibleIndex >= listData->VisibleItemCount)
            {
               listFocusResetMsg = COURIER_MESSAGE_NEW(ListFocusResetReqMsg)(listData->ListId, LIST_FOCUS_RESET_TO_LAST_VISIBLE);
               ++correction;//one step will be "consumed" by the ListFocusResetReqMsg
            }
         }
         else
         {
            listChangeMsg = COURIER_MESSAGE_NEW(ListChangeMsg)(listData->ListId, ListChangeUp, -correction);
         }
      }
      //can't scroll up due to first item visible
      else
      {
         //reset to last item visible, since wrap around is true and first item visible
         if (groupData->WrapAround)
         {
            if (listData->TotalItemCount > listData->VisibleItemCount)
            {
               result = true;
               int position = listData->TotalItemCount - listData->VisibleItemCount;
               listChangeMsg = COURIER_MESSAGE_NEW(ListChangeMsg)(listData->ListId, ListChangeSet, position);
               listFocusResetMsg = COURIER_MESSAGE_NEW(ListFocusResetReqMsg)(listData->ListId, LIST_FOCUS_RESET_TO_LAST_VISIBLE);
               ++correction;//one step will be "consumed" by the ListFocusResetReqMsg
            }
            else
            {
               //ListFocusResetReqMsg listFocusResetMsg(listData->ListId, LIST_FOCUS_RESET_TO_LAST_VISIBLE);
               //return onFocusResetReqMessage(session, group, listFocusResetMsg);
               return moveFocus(session, group, correction);
            }
         }
         //nothing to do, since first item visible and wrap around is false
         else
         {
            //actually nothing to do, except inverted rotary is true
            if (_invertedRotary)
            {
               //consume the message here, since wrap around is false and default rotary controller should not process the message
               return true;
            }
         }
      }
   }

   if (result)
   {
      if (manager.getSessionManager() != NULL)
      {
         manager.getSessionManager()->requestSessionSuspend();
      }
      if (listChangeMsg != NULL)
      {
         ETG_TRACE_USR4(("ListRotaryController::handleIncompleteFocusMove post listChange type;%d, Value; %d", listChangeMsg->GetListChangeType(), listChangeMsg->GetValue()));
         listChangeMsg->Post();
      }
      if (listFocusResetMsg != NULL)
      {
         listFocusResetMsg->Post();
      }
      if (correction != 0)
      {
         ListFocusChangeReqMsg* listFocusChangeMsg = COURIER_MESSAGE_NEW(ListFocusChangeReqMsg)(listData->ListId, correction);
         if (listFocusChangeMsg != NULL)
         {
            listFocusChangeMsg->Post();
         }
      }
   }
   return result;
}


void ListRotaryController::onListWidgetContentUpdated(Candera::UInt32 /*listId*/, Candera::Bool /*success*/)
{
   Focus::FManager& manager = Focus::FManager::getInstance();
   Focus::FSessionManager* sessionManager = manager.getSessionManager();
   ETG_TRACE_USR4(("ListRotaryController::onListWidgetContentUpdated"));
   if (sessionManager != NULL)
   {
      //if visible content is updated while the session is suspended
      //=>we clear the suspend flag and refresh the session
      if (sessionManager->isSessionSuspended())
      {
         ETG_TRACE_USR4(("ListRotaryController::onListWidgetContentUpdated Refresh session"));
         sessionManager->clearSessionSuspend();

         RefreshFocusSessionReqMsg refreshMsg;
         sessionManager->onMessage(refreshMsg);
      }
      //if visible content is updated while no session exists
      //=>check if the current focus is still valid (widget exists)
      else if (sessionManager->getSession() == NULL && (manager.getConsistencyChecker() != NULL))
      {
         ETG_TRACE_USR4(("ListRotaryController::onListWidgetContentUpdated Check consistency"));
         manager.getConsistencyChecker()->checkFocus();

         /*const Focus::FViewId& viewId = manager.getCurrentFocus().ViewId;
         const Focus::FId& widgetId = manager.getCurrentFocus().WidgetId;
         if ((viewId != Focus::Constants::InvalidViewId)
               && (widgetId != Focus::Constants::InvalidId)
               && (_viewHandler != NULL)
               && (_viewHandler->FindWidget(
                      viewId,
         #if (COURIER_VERSION_MAJOR >= 3)
                      Courier::CompositePath(),
         #endif
                      Courier::ItemId(widgetId.c_str())) == NULL))
         {
            ETG_TRACE_USR4(("ListRotaryController::onListWidgetContentUpdated Current focused widget '%s' not found!", FID_STR(widgetId)));
            //manager.hideFocus();
            RnaiviFocus::hideFocus();
         }*/
      }
      else
      {
         //nothing to do
      }
   }
}


}
/* namespace Rnaivi */
