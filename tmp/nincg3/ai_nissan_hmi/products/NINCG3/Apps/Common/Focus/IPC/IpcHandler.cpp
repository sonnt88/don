/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */

#include "gui_std_if.h"
#include "IpcHandler.h"
#include "IpcAdapter.h"
#include "ProjectBaseMsgs.h"
#include "ProjectBaseTypes.h"
#include "../RnaiviFocus.h"
#include "ConnectionHandler.h"
#include "../Utils/FocusDefines.h"
#include <boost/lexical_cast.hpp>
#include <pthread.h>

#include "Common/Common_Trace.h"
//////// TRACE IF ///////////////////////////////////////////////////
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_FOCUS
#include "trcGenProj/Header/IpcHandler.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN

void* IPCMQThreadFunc(void* args)
{
   ETG_TRACE_USR4(("IpcHandler::ThreadFunc"));
   IpcHandler* obj = reinterpret_cast<IpcHandler*>(args);

   if (obj != 0)
   {
      ETG_TRACE_USR4(("IpcHandler::calling Run"));
      obj->Run();
   }
   return NULL;
}


IpcHandler::IpcHandler() :
   _connectionHandler(RnaiviFocus::getAppId()/*_manager.getCurrentAppId()*/)
{
   ETG_TRACE_USR4(("IpcHandler::IpcHandler"));

   std::string threadName = "GUI_F_MQ_IPC";
   _hmiThread = new asf::threading::Thread(threadName, IPCMQThreadFunc, (void*)this);
}


IpcHandler::~IpcHandler()
{
   if (_hmiThread)
   {
      _connectionHandler.closeConnection();

      _hmiThread->join();
      delete _hmiThread;
      _hmiThread = 0;
   }
}


int IpcHandler::Run()
{
   ETG_TRACE_USR4(("IpcHandler::Run"));
   //create the queue first
   IConnection* connection = _connectionHandler.createConnection(); 	//TODO: check for proper connection

   bool terminate = false;

   char buffer[MAX_INCOMING_MSG_LENGTH];
   memset(buffer, 0, MAX_INCOMING_MSG_LENGTH);
   int appId = RnaiviFocus::getAppId();

   while (!terminate)
   {
      //obtain message
      int bytesRead = 0;
      bool readStatus = connection->Read(buffer, MAX_INCOMING_MSG_LENGTH, bytesRead);
      ETG_TRACE_USR4(("IpcHandler::read status:%d src: %d, crtApp:%d", readStatus, *buffer, RnaiviFocus::getAppId()));
      terminate = _connectionHandler.isClosed();

      if (!terminate)
      {
         ETG_TRACE_USR4(("IpcHandler::buffer size:%d", bytesRead));
         char* payLoad = (char*) new char[bytesRead + 1];
         memset(payLoad, 0, bytesRead + 1);
         memcpy(payLoad, buffer, bytesRead);
         processMessage(payLoad, bytesRead);
      }
   }
}


void IpcHandler::processMessage(char* msg, uint32 bytesRead)
{
   POST_MSG(COURIER_MESSAGE_NEW(FocusIPCMsgPayloadUpdMsg)(msg, bytesRead));
}
