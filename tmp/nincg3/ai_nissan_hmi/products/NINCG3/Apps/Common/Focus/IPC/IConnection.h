/* ------------------------------------------------------------------------ */
/* This software is property of Robert Bosch GmbH.                          */
/* Unauthorized duplication and disclosure to third parties is prohibited.  */
/* All rights reserved.                                                     */
/* Copyright (C) Robert Bosch Car Multimedia GmbH, 2015                     */
/* ------------------------------------------------------------------------ */

#ifndef ICONNECTION_H_
#define ICONNECTION_H_

/** Open mode for IpcChannel. */
namespace IpcChannelOpenMode {
enum Enum
{
   Invalid,

   Open,    // Open a channel that was created by the remote process.

   Create   // Create a channel that can be opened by the remote process.
};


}  // namespace IpcChannelOpenMode

/** Status of IpcChannel. */
namespace IpcChannelStatus {
enum Enum
{
   Invalid,    /// The channel can't be used for communication.

   Open        /// The channel is open and can be used to communicate.
};


}  // namespace IpcChannelStatus

class IConnection // TODO: Add a namespace
{
   public:
      virtual ~IConnection() {};
      virtual IpcChannelStatus::Enum GetChannelState() = 0;
      //virtual bool Init(/*IpcChannelConfiguration * config*/) = 0;
      virtual bool Open(const char* name, IpcChannelOpenMode::Enum openMode = IpcChannelOpenMode::Open) = 0;
      virtual bool Close() = 0;
      virtual bool Write(const char* buffer, unsigned int msgLength) = 0;
      virtual bool Read(char* buffer, unsigned int bufferSize, int& bytesRead) = 0;

      bool quickWrite(const char* QueueName, const char* buffer, unsigned int msgLength)
      {
         if (Open(QueueName))
         {
            bool writeSuccess = Write(buffer, msgLength);
            bool closeSuccess = Close();

            return (writeSuccess && closeSuccess);
         }
         return false;
      }
};


#endif /* ICONNECTION_H_ */
