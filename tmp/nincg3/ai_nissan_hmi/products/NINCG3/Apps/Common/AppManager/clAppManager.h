///////////////////////////////////////////////////////////
//  clAppManager.h
//  Implementation of the Class clAppManager
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(clAppManager_h)
#define clAppManager_h

/**
 * interface class for controlling application switch
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 5:44:42 PM
 */

#include "clAppManagerInterface.h"
#include "clSession.h"
#include "clDisplayControl.h"
#include "clDisplayStatus.h"
#include "asf/core/Types.h"
#include <vector>
#include <map>

enum enApplicationModes
{
   APPMODE_UNDEFINED = 0,
   APPMODE_SOURCE,
   APPMODE_UTILITY,
   APPMODE_CLOCK,
   APPMODE_EMERGENCY
};


class clAppManager
{
   public:
      clAppManager();
      virtual ~clAppManager();

      /**
       * Initialize operation for context provider
       */
      void vInit();
      /**
       *
       * @param targetContext
       */

      virtual void vSwitchAppRequestOnHardKey(uint32 applicationId);
      virtual void vSwitchAppRequestOnContext(uint32 contextId, uint32 applicationId, uint32 priority, bool bTemporaryContext ,  bool bForwardContext = false);
      virtual void vSwitchAppRequestOnAudioSourceChange(uint32 applicationId, bool bIsNewDeviceAdded = false);
      virtual void vOnNewEntertainmentMuteState(bool bAudioMuted);
      virtual void vOnContextClosed(uint32 contextId);
      virtual void vSetApplicationMode(uint32 applicationId, uint32 applicationMode);
      virtual void vOnNewActiveAudioApplication(uint32 applicationId);
      virtual void vReset();
      virtual void vOnApplicationSwitchComplete(uint32 applicationId);
      virtual bool bIsAllowBackRequest();
      virtual void allowBackRequest();

   protected:
      /**
       *
       * @param sourceContext
       * @param targetContext
       */

   private:
      void  vSynchronize();
      void  vSynchronizeOnNoActiveSource();
      void  vSynchronizeOnActiveContextSession();
      void  vSynchronizeOnActiveHardKeySession();
      void  vSynchronizeOnAudioFollowingSession();
      void  vSwitchBackGroundApp(bool bUpdateVisibleApplication = true);
      void  vSwitchToAppInBackground();
      void  vSwitchForeGroundApp();
      void  vSwitchAppToBackground();
      void  vSwitchQueuedToBackground();
      void  vOnNewActiveSession();
      void  vPrintSessionState();
      void  vSwitchToClock();
      void  switchToLastActiveApplication();
      void  vSwitchToActiveAudioApp();
      void  vSwitchActiveAppToForeground();

      bool  bIsSourceMode(uint32 applicationId);
      bool  bIsClockMode(uint32 applicationId);

      clSession requestedSession;
      clSession activeSession;
      clSession backGroundSession;
      clSession queuedSession;
      clDisplayControl displayControl;
      clDisplayStatus displayStatus;
      ::std::map<uint32, uint32> applicationModes;
};


#endif // !defined(EA_FAF598AF_B4F9_4bb1_91CE_ABBA6F93217B__INCLUDED_)
