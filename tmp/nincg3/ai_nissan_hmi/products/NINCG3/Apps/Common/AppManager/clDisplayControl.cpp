/**************************************************************************//**
* \file     clDisplayControl.cpp
*
*           clDisplayControl method class implementation
*
* \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
******************************************************************************/

#include "clDisplayControl.h"
#include "interface/clDisplayControlInterface.h"

static clDisplayControlInterface* m_poDisplay;

/**************************************************************************//**
* Constructor
******************************************************************************/
clDisplayControl::clDisplayControl()
{
}


/**************************************************************************//**
* Destructor
******************************************************************************/
clDisplayControl::~clDisplayControl()
{
}


/**************************************************************************//**
*
******************************************************************************/
void clDisplayControl::vSetDisplayControlImpl(clDisplayControlInterface* poImpl)
{
   m_poDisplay = poImpl;
}


/**************************************************************************//**
*
******************************************************************************/
void clDisplayControl::switchApp(uint32 applicationId)
{
   if (m_poDisplay)
   {
      m_poDisplay->switchApp(applicationId);
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clDisplayControl::switchClock(bool bShowClock)
{
   if (m_poDisplay)
   {
      m_poDisplay->switchClock(bShowClock);
   }
}
