/**************************************************************************//**
* \file     clMock_DisplayControlImpl.h
*
*
* \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
******************************************************************************/
#ifndef clMock_DisplayControlImpl_h
#define clMock_DisplayControlImpl_h


#include "asf/core/Types.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "interface/clDisplayControlInterface.h"

class clMock_DisplayControlImpl : public clDisplayControlInterface
{
   public:
      virtual ~clMock_DisplayControlImpl() {};
      clMock_DisplayControlImpl() {};

      MOCK_METHOD1(switchApp,  void(uint32 applicationId));
      MOCK_METHOD1(switchClock,  void(bool bShowClock));
};


#endif // clMock_DisplayControlImpl_h
