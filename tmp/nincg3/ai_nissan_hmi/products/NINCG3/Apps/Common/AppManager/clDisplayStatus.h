/**************************************************************************//**
* \file     clDisplayStatus.h
*
*
* \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
******************************************************************************/
#ifndef clDisplayStatus_h
#define clDisplayStatus_h

#include "asf/core/Types.h"
#include "interface/clDisplayStatusRecieverInterface.h"

class clDisplayStatus
{
   public:
      virtual ~clDisplayStatus();
      clDisplayStatus();

      void vOnNewActiveBGApplication(uint32 applicationId);
      void vOnNewActiveFGApplication(uint32 applicationId);
      void vSetDisplayStatusRecieverImpl(clDisplayStatusRecieverInterface* poImpl);
};


#endif // clDisplayStatus_h
