///////////////////////////////////////////////////////////
//  clClockMode_Test.h
//  Implementation of the Class clClockMode_Test
//  Created on:      27-Mar-2015 15:43:29
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#ifndef clClockMode_Test_h
#define clClockMode_Test_h

#include "gtest/gtest.h"
#include "clAppManager.h"
#include "mocks/clMock_DisplayStatusRecieverImpl.h"
#include "mocks/clMock_DisplayControlImpl.h"
#include "clDisplayStatus.h"
#include "clDisplayControl.h"

using testing::Test;

class clClockMode_Test  : public testing::Test
{
   public:
      clClockMode_Test()
      {
      }
      virtual ~clClockMode_Test()
      {
      }
      void SetUp()
      {
         ::testing::Test::RecordProperty("Component", "APPMANAGER");
         status.vSetDisplayStatusRecieverImpl(&oDisplayStatusMock);
         control.vSetDisplayControlImpl(&oDisplayControlMock);
      }
      void TearDown()
      {
      }
      clDisplayStatus status;
      clDisplayControl  control;
      clMock_DisplayControlImpl oDisplayControlMock;
      clMock_DisplayStatusRecieverImpl oDisplayStatusMock;
};


#endif // !defined(clClockMode_Test_h)
