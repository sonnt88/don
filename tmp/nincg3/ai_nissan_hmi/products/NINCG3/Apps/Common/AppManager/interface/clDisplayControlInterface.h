/**************************************************************************//**
* \file     clDisplayControlInterface.h
*
*
* \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
******************************************************************************/
#ifndef clDisplayControlInterface_h
#define clDisplayControlInterface_h


#include "asf/core/Types.h"


class clDisplayControlInterface
{
   public:
      virtual ~clDisplayControlInterface() {};
      clDisplayControlInterface() {};

      virtual void switchApp(uint32 applicationId) = 0;
      virtual void switchClock(bool bShowClock) = 0;
};


#endif // clDisplayControlInterface_h
