/**************************************************************************//**
* \file     clMock_DisplayStatusRecieverImpl.h
*
*
* \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
******************************************************************************/
#ifndef clMock_DisplayStatusRecieverImpl_h
#define clMock_DisplayStatusRecieverImpl_h


#include "asf/core/Types.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "interface/clDisplayStatusRecieverInterface.h"


class clMock_DisplayStatusRecieverImpl : public clDisplayStatusRecieverInterface
{
   public:
      virtual ~clMock_DisplayStatusRecieverImpl() {};
      clMock_DisplayStatusRecieverImpl() {};

      MOCK_METHOD1(vOnNewActiveBGApplication,  void(uint32 applicationId));
      MOCK_METHOD1(vOnNewActiveFGApplication,  void(uint32 applicationId));
};


#endif // clMock_DisplayStatusRecieverImpl_h
