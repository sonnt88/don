///////////////////////////////////////////////////////////
//  clClockMode_Test.cpp
//  Implementation of the Class clClockMode_Test
//  Created on:      27-Mar-2015 15:43:29
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#include "test/clClockMode_Test.h"

#include "clAppManager.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"


using testing::_;
using testing::ElementsAre;
using testing::Property;
using testing::Matcher;
using testing::Return;
using testing::SetArgPointee;
using testing::Mock;
using testing::TestWithParam;
using testing::Values;
using std::tr1::tuple_element;


TEST_F(clClockMode_Test, vRequestClockMode_NoActiveSession_ClockAcceptedAndInForeGround)
{
   RecordProperty("USECASE", "Clock is being requested when no source is active.");
   clAppManager oManager;
   EXPECT_CALL(oDisplayControlMock, switchApp(0x5)).Times(1);   // Clock switched and not active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x5)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnContext(0x200, 0x5, 0xff, true);
}


TEST_F(clClockMode_Test, vRequestClockMode_HighPrioSessionIsActive_ClockAcceptedAndInBackGround)
{
   RecordProperty("USECASE", "Clock is being requested when RVC is active.: Expected RVC is still on top and clock comes on background.");
   clAppManager oManager;
   EXPECT_CALL(oDisplayControlMock, switchApp(0x10)).Times(1);   // RVC Active
   EXPECT_CALL(oDisplayControlMock, switchApp(0x5)).Times(1);   // RVC Active, Clock switched but not active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x10)).Times(2);   // RVC Active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // First request no application in background
   oManager.vSwitchAppRequestOnContext(0x100, 0x10, 0x80, true); // Request High prio RVC
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x5)).Times(1);   // Clock app as background application.
   oManager.vSwitchAppRequestOnContext(0x200, 0x5, 0xff, true);
}


TEST_F(clClockMode_Test, vRequestClockModeExit_HighPrioSessionIsActive_ClockAddedAndRemovedAndInBackGround)
{
   RecordProperty("USECASE", "Clock is being requested when RVC is active, then remove clock.: Expected RVC is still on top and clock comes on background and then removed from background.");
   clAppManager oManager;
   EXPECT_CALL(oDisplayControlMock, switchApp(0x10)).Times(1);   // RVC Active
   EXPECT_CALL(oDisplayControlMock, switchApp(0x5)).Times(1);   // RVC Active, Clock switched but not active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x10)).Times(2);   // RVC Active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // First request no application in background
   oManager.vSwitchAppRequestOnContext(0x100, 0x10, 0x80, true); // Request High prio RVC
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x5)).Times(1);   // Clock app as background application.
   oManager.vSwitchAppRequestOnContext(0x200, 0x5, 0xff, true);

   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x10)).Times(1);   // RVC Active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // no Application in Background
   oManager.vOnContextClosed(0x200);
}


TEST_F(clClockMode_Test, vRequestClockMode_ActiveHighPrioSessionEnded_ClockIsPutToForeground)
{
   RecordProperty("USECASE", "Clock is being requested when RVC is active. Now RVC is completed: Expected that clock comes to FG.");
   clAppManager oManager;
   EXPECT_CALL(oDisplayControlMock, switchApp(0x10)).Times(1);   // RVC Active
   EXPECT_CALL(oDisplayControlMock, switchApp(0x5)).Times(1);   // RVC Active, Clock switched but not active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x10)).Times(2);   // RVC Active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // First request no application in background
   oManager.vSwitchAppRequestOnContext(0x100, 0x10, 0x80, true); // Request High prio RVC
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x5)).Times(1);   // Clock app as background application.
   oManager.vSwitchAppRequestOnContext(0x200, 0x5, 0xff, true);

   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x5)).Times(1);   // Clock Active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // no Application in Background
   oManager.vOnContextClosed(0x100);
}


TEST_F(clClockMode_Test, RequestFM_ActiveClockModeSession_FMActivated)
{
   RecordProperty("USECASE", "While in Clock mode press the HK RADIO");
   clAppManager oManager;
   EXPECT_CALL(oDisplayControlMock, switchApp(0x5)).Times(1);   // Clock switched and not active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x5)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnContext(0x200, 0x5, 0xff, true);

   EXPECT_CALL(oDisplayControlMock, switchApp(0x101)).Times(1);   // FM switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x101)).Times(1);   // FM activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnHardKey(0x101);
}


TEST_F(clClockMode_Test, RequestClockMode_FMActive_ClockModeEntered)
{
   RecordProperty("USECASE", "While in HK RADIO, long press HK_ILLUM key");
   clAppManager oManager;

   EXPECT_CALL(oDisplayControlMock, switchApp(0x101)).Times(1);   // FM switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x101)).Times(1);   // FM activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnHardKey(0x101);

   EXPECT_CALL(oDisplayControlMock, switchApp(0x5)).Times(1);   // Clock switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x5)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnContext(0x200, 0x5, 0xff, true);
}


TEST_F(clClockMode_Test, RequestClockMode_ClockOnMuteActive_ClockModeEntered)
{
   RecordProperty("USECASE", "While in HK RADIO, press HK_AUDIO_OFF and then long press HK_ILLUM key");
   clAppManager oManager;

   EXPECT_CALL(oDisplayControlMock, switchApp(0x101)).Times(1);   // FM switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x101)).Times(1);   // FM activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnHardKey(0x101);

   EXPECT_CALL(oDisplayControlMock, switchApp(0x5)).Times(1);   // Clock switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x5)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnContext(0x201, 0x5, 0xff, true);

   EXPECT_CALL(oDisplayControlMock, switchApp(0x5)).Times(1);   // Clock switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x5)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnContext(0x200, 0x5, 0xff, true);
}


TEST_F(clClockMode_Test, CloseClockMode_ClockOnMuteActiveandClockRequested_ClockOnMuteEntered)
{
   RecordProperty("USECASE", "While in HK RADIO, press HK_AUDIO_OFF and then long press HK_ILLUM key, now short press HK_ILLUM key");
   clAppManager oManager;

   EXPECT_CALL(oDisplayControlMock, switchApp(0x101)).Times(1);   // FM switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x101)).Times(1);   // FM activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnHardKey(0x101);

   EXPECT_CALL(oDisplayControlMock, switchApp(0x5)).Times(1);   // Clock mute switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x5)).Times(1);   // Clock mute activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnContext(0x201, 0x5, 0xff, true);

   EXPECT_CALL(oDisplayControlMock, switchApp(0x5)).Times(1);   // Clock switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x5)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnContext(0x200, 0x5, 0xff, true);

   EXPECT_CALL(oDisplayControlMock, switchApp(0x5)).Times(1);   // Clock switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(0x5)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(0x0)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnContext(0x201, 0x5, 0xff, true);
}
