///////////////////////////////////////////////////////////
//  clAppManager.cpp
//  Implementation of the Class clAppManager
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#include "clAppManager.h"
#include "hmi_trace_if.h"
#include "../Common_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_APPMANAGER
#include "trcGenProj/Header/clAppManager.cpp.trc.h"
#endif


clAppManager::clAppManager()
{
   requestedSession.reset();
   activeSession.reset();
   applicationModes.clear();
}


clAppManager::~clAppManager()
{
}


void clAppManager::vSwitchAppRequestOnHardKey(uint32 applicationId)
{
   ETG_TRACE_USR4(("APPMGR:   vSwitchAppRequestOnHardKey      applicationId      : %d ", ETG_CENUM(enApplicationId, applicationId)));
   requestedSession.reset();
   requestedSession.triggerType = HARDKEY;
   requestedSession.applicationId = applicationId;
   requestedSession.priority = 0xFF;
   requestedSession.state = REQUESTED;
   vSynchronize();
}


void clAppManager::vSwitchAppRequestOnContext(uint32 contextId, uint32 applicationId, uint32 priority, bool bTemporaryContext ,  bool bForwardContext)
{
   ETG_TRACE_USR4(("APPMGR:   vSwitchAppRequestOnContext      applicationId     : %d ", ETG_CENUM(enApplicationId, applicationId)));
   ETG_TRACE_USR4(("APPMGR:   vSwitchAppRequestOnContext      contextId         : %d ", ETG_CENUM(enContextId, contextId)));
   ETG_TRACE_USR4(("APPMGR:   vSwitchAppRequestOnContext      priority          : %d ", priority));
   ETG_TRACE_USR4(("APPMGR:   vSwitchAppRequestOnContext      bForwardContext   : %d ", bForwardContext));
   ETG_TRACE_USR4(("APPMGR:   vSwitchAppRequestOnContext      bTemporaryContext : %d ", bTemporaryContext));
   requestedSession.reset();
   requestedSession.triggerType = CONTEXTSWITCH;
   requestedSession.applicationId = applicationId;
   requestedSession.contextId = contextId;
   requestedSession.priority = priority;
   requestedSession.state = REQUESTED;
   requestedSession.bTemporaryContext = bTemporaryContext;
   requestedSession.bForwardContext = bForwardContext;
   vSynchronize();
}


void clAppManager::vSwitchAppRequestOnAudioSourceChange(uint32 applicationId, bool bIsNewDeviceAdded)
{
   ETG_TRACE_USR4(("APPMGR:   vSwitchAppRequestOnAudioSourceChange  applicationId : %d ", ETG_CENUM(enApplicationId, applicationId)));
   requestedSession.reset();
   requestedSession.triggerType = AUDIOFOLLOWING;
   requestedSession.applicationId = applicationId;
   requestedSession.priority = 0xFF;
   requestedSession.state = REQUESTED;
   requestedSession.bIsNewDeviceAdded = bIsNewDeviceAdded;
   vSynchronize();
}


void clAppManager::vSynchronize()
{
   if (activeSession.bIsProcessedRequest())
   {
      if (requestedSession.bIsCompletedRequest()  && activeSession.visibleApplicationId == activeSession.applicationId)
      {
         activeSession.state  = COMPLETED;
         vSynchronize();
      }
      if (requestedSession.bIsPendingRequest() && requestedSession.bIsApplicationSwitchComplete())
      {
         if (requestedSession.applicationId == activeSession.applicationId || activeSession.applicationId == 0)
         {
            activeSession.state  = COMPLETED;
            activeSession.visibleApplicationId = requestedSession.applicationId;
            if (queuedSession.bIsPendingRequest())
            {
               requestedSession.state = queuedSession.state;
               requestedSession.triggerType = queuedSession.triggerType;
               requestedSession.bAudioMuted = queuedSession.bAudioMuted;
               requestedSession.applicationId = queuedSession.applicationId;
               requestedSession.priority = queuedSession.priority;
               requestedSession.bForwardContext = queuedSession.bForwardContext;
               requestedSession.contextId = queuedSession.contextId;
               requestedSession.bTemporaryContext = queuedSession.bTemporaryContext;
               requestedSession.bIsNewDeviceAdded = queuedSession.bIsNewDeviceAdded;
               queuedSession.reset();
               queuedSession.state = COMPLETED;
               vSynchronize();
            }
         }
      }
      else if (requestedSession.bIsPendingRequest() && (queuedSession.bIsCompletedRequest()  || (queuedSession.bIsPendingRequest() &&  queuedSession.priority >= requestedSession.priority &&
               !requestedSession.bIsEntertainmentMuteRequest())))
      {
         if (queuedSession.bIsPendingRequest() && queuedSession.bIsEntertainmentMuteRequest())
         {
            activeSession.bAudioMuted = queuedSession.bAudioMuted;
         }
         if (requestedSession.bIsPendingRequest() && (requestedSession.bIsContextRequest() || requestedSession.bIsAudioFollowingRequest() || requestedSession.bIsHardKeyRequest()) \
               && (activeSession.priority < requestedSession.priority))
         {
            vSwitchBackGroundApp(false);
         }
         else
         {
            if (queuedSession.bIsPendingRequest() && queuedSession.bIsContextRequest() && !queuedSession.bIsForwardContext() && requestedSession.bIsContextCompleteRequest())
            {
               vSwitchQueuedToBackground();
            }
            queuedSession.reset();
            queuedSession.state = requestedSession.state;
            queuedSession.triggerType = requestedSession.triggerType;
            queuedSession.bAudioMuted = requestedSession.bAudioMuted;
            queuedSession.applicationId = requestedSession.applicationId;
            queuedSession.priority = requestedSession.priority;
            queuedSession.bForwardContext = requestedSession.bForwardContext;
            queuedSession.contextId = requestedSession.contextId;
            queuedSession.bTemporaryContext = requestedSession.bTemporaryContext;
            queuedSession.bIsNewDeviceAdded = requestedSession.bIsNewDeviceAdded;
            requestedSession.state = COMPLETED;
            vSynchronize();
         }
      }
   }
   else if (requestedSession.bIsPendingRequest())
   {
      if (requestedSession.bIsApplicationSwitchComplete())
      {
         if (requestedSession.applicationId != activeSession.applicationId && queuedSession.applicationId == 0 && activeSession.applicationId != 0)
         {
            vSwitchActiveAppToForeground();
         }
         else
         {
            requestedSession.state = COMPLETED;
            activeSession.applicationId = requestedSession.applicationId;
            vSynchronize();
         }
      }
      else if (requestedSession.bIsContextCompleteRequest())
      {
         if (backGroundSession.contextId == requestedSession.contextId)
         {
            backGroundSession.reset();
            requestedSession.state = COMPLETED;
            activeSession.AllowBackContext();
            vSynchronize();
         }
         else if (activeSession.contextId == requestedSession.contextId)
         {
            if (backGroundSession.applicationId && (!bIsSourceMode(backGroundSession.applicationId) || !activeSession.bAudioMuted))
            {
               if (!bIsSourceMode(backGroundSession.applicationId) || (bIsSourceMode(backGroundSession.applicationId) && activeSession.activeAudioApplicationId == backGroundSession.applicationId))
               {
                  vSwitchToAppInBackground();
               }
               else
               {
                  vSwitchToActiveAudioApp();
               }
               activeSession.AllowBackContext();
            }
            else if (backGroundSession.applicationId && (activeSession.bAudioMuted) && bIsSourceMode(backGroundSession.applicationId)) //else if (backGroundSession.applicationId && (activeSession.bAudioMuted))
            {
               displayControl.switchClock(true);
               activeSession.reset();
            }
            else
            {
               requestedSession.state = COMPLETED;
               activeSession.priority = 0xff;
               vSynchronize();
            }
         }
      }
      else if (requestedSession.bIsEntertainmentMuteRequest() && requestedSession.bAudioMuted !=  activeSession.bAudioMuted)
      {
         if (activeSession.applicationId)
         {
            if (requestedSession.bAudioMuted && ((bIsSourceMode(activeSession.applicationId))))
            {
               displayControl.switchClock(true);
               vSwitchAppToBackground();
               activeSession.reset();
            }
            else if (!requestedSession.bAudioMuted   && (bIsClockMode(activeSession.applicationId) ||  activeSession.applicationId == 0))
            {
               if (backGroundSession.applicationId && !activeSession.bIsHighPriorityContextActive())
               {
                  if (!bIsSourceMode(backGroundSession.applicationId) || (bIsSourceMode(backGroundSession.applicationId) && activeSession.activeAudioApplicationId == backGroundSession.applicationId))
                  {
                     vSwitchToAppInBackground();
                  }
                  else
                  {
                     vSwitchToActiveAudioApp();
                  }
                  activeSession.AllowBackContext();
               }
               else if (!activeSession.bIsHighPriorityContextActive())
               {
                  switchToLastActiveApplication();
               }
               displayControl.switchClock(false);
            }
            activeSession.bAudioMuted = requestedSession.bAudioMuted;
            if (requestedSession.bIsPendingRequest())
            {
               requestedSession.state = COMPLETED;
               vSynchronize();
            }
         }
         else
         {
            queuedSession.state = requestedSession.state;
            queuedSession.triggerType = requestedSession.triggerType;
            queuedSession.bAudioMuted = requestedSession.bAudioMuted;
            requestedSession.state = COMPLETED;
            vSynchronize();
         }
      }
      else if (activeSession.bIsContextRequest())
      {
         vSynchronizeOnActiveContextSession();
      }
      else if (activeSession.bIsHardKeyRequest())
      {
         vSynchronizeOnActiveHardKeySession();
      }
      else if (activeSession.bIsAudioFollowingRequest())
      {
         vSynchronizeOnAudioFollowingSession();
      }
      else
      {
         vSynchronizeOnNoActiveSource();
      }
   }
   else if (requestedSession.bIsCompletedRequest() && queuedSession.bIsCompletedRequest())
   {
      vOnNewActiveSession();
   }
   vPrintSessionState();
}


void clAppManager::vSynchronizeOnActiveContextSession()
{
   if (activeSession.contextId != requestedSession.contextId || activeSession.applicationId !=  requestedSession.applicationId)
   {
      if (activeSession.bIsHighPriorityContextActive())
      {
         if (requestedSession.bIsContextRequest() && (requestedSession.bIsForwardContext()))
         {
            if (requestedSession.priority < activeSession.priority)
            {
               if (!bIsSourceMode(requestedSession.applicationId) || (bIsSourceMode(requestedSession.applicationId) && !activeSession.bAudioMuted))
               {
                  if (!bIsSourceMode(requestedSession.applicationId) || activeSession.activeAudioApplicationId == requestedSession.applicationId)
                  {
                     vSwitchForeGroundApp();
                  }
                  else
                  {
                     vSwitchToActiveAudioApp();
                  }
                  activeSession.AllowBackContext();
               }
               else
               {
                  displayControl.switchClock(true);
                  activeSession.reset();
                  vSwitchBackGroundApp(false);
               }
            }
            else
            {
               vSwitchBackGroundApp();
               activeSession.IgnoreBackContext();
            }
         }
         else if (requestedSession.bIsContextRequest() && !requestedSession.bIsForwardContext() && !activeSession.bIgnoreBackContext())
         {
            vSwitchBackGroundApp(false);
            /*if (!activeSession.bIgnoreBackContext())
            {
               vSwitchBackGroundApp();
            }
            else
            {
               activeSession.triggerType = TRIGGERTYPE_NONE;
            }
            */
         }
         else if (requestedSession.bIsContextRequest() && !requestedSession.bIsForwardContext() && activeSession.bIgnoreBackContext())
         {
            activeSession.AllowBackContext();
         }
         else if (requestedSession.bIsHardKeyRequest() ||  requestedSession.bIsAudioFollowingRequest())
         {
            if (requestedSession.bIsHardKeyRequest())
            {
               backGroundSession.reset();
            }
            vSwitchBackGroundApp();
            activeSession.IgnoreBackContext();
         }
      }
      else
      {
         if (requestedSession.bIsContextRequest() && (requestedSession.bIsForwardContext() || (!requestedSession.bIsForwardContext() && !activeSession.bIgnoreBackContext())))
         {
            if (!bIsSourceMode(requestedSession.applicationId) || (bIsSourceMode(requestedSession.applicationId) && !activeSession.bAudioMuted))
            {
               if (!bIsSourceMode(requestedSession.applicationId) || activeSession.activeAudioApplicationId == requestedSession.applicationId)
               {
                  vSwitchForeGroundApp();
               }
               else
               {
                  vSwitchToActiveAudioApp();
               }
               activeSession.AllowBackContext();
            }
            else
            {
               displayControl.switchClock(true);
               activeSession.reset();
               vSwitchBackGroundApp(false);
            }
         }
         else if (requestedSession.bIsContextRequest() &&  !requestedSession.bIsForwardContext() && activeSession.bIgnoreBackContext())
         {
            if (!bIsSourceMode(backGroundSession.applicationId) || (bIsSourceMode(backGroundSession.applicationId) && !activeSession.bAudioMuted))
            {
               if (!bIsSourceMode(backGroundSession.applicationId) || (bIsSourceMode(backGroundSession.applicationId) && activeSession.activeAudioApplicationId == backGroundSession.applicationId))
               {
                  vSwitchToAppInBackground();
               }
               else
               {
                  vSwitchToActiveAudioApp();
               }
               activeSession.AllowBackContext();
            }
            else
            {
               displayControl.switchClock(true);
               activeSession.reset();
            }
         }
         else if (requestedSession.bIsHardKeyRequest())
         {
            backGroundSession.reset();
            vSwitchForeGroundApp();
            activeSession.IgnoreBackContext();
         }
         else if (requestedSession.bIsAudioFollowingRequest())
         {
            if (!activeSession.bAudioMuted || requestedSession.bIsNewDeviceAdded)
            {
               vSwitchForeGroundApp();
               activeSession.IgnoreBackContext();
            }
            else
            {
               vSwitchToClock();
               vSwitchBackGroundApp(false);
            }
         }
      }
   }
   if (requestedSession.bIsPendingRequest())
   {
      requestedSession.state = COMPLETED;
      vSynchronize();
   }
}


void clAppManager::vSynchronizeOnActiveHardKeySession()
{
   if (requestedSession.bIsContextRequest() && requestedSession.bIsForwardContext())
   {
      if (!bIsSourceMode(requestedSession.applicationId) || (bIsSourceMode(requestedSession.applicationId) && !activeSession.bAudioMuted))
      {
         if (!bIsSourceMode(requestedSession.applicationId) || activeSession.activeAudioApplicationId == requestedSession.applicationId)
         {
            vSwitchForeGroundApp();
         }
         else
         {
            vSwitchToActiveAudioApp();
         }
         activeSession.AllowBackContext();
      }
      else
      {
         displayControl.switchClock(true);
         activeSession.reset();
         vSwitchBackGroundApp(false);
      }
   }
   else if (requestedSession.bIsAudioFollowingRequest()  || requestedSession.bIsHardKeyRequest())
   {
      if (requestedSession.bIsHardKeyRequest())
      {
         backGroundSession.reset();
      }
      vSwitchForeGroundApp();
      activeSession.AllowBackContext();
   }
   if (requestedSession.bIsPendingRequest())
   {
      requestedSession.state = COMPLETED;
      vSynchronize();
   }
}


void clAppManager::vSynchronizeOnAudioFollowingSession()
{
   if ((requestedSession.bIsContextRequest() && requestedSession.bIsForwardContext()) || requestedSession.bIsHardKeyRequest() || requestedSession.bIsAudioFollowingRequest())
   {
      vSwitchForeGroundApp();
      activeSession.AllowBackContext();
      if (requestedSession.bIsHardKeyRequest())
      {
         backGroundSession.reset();
      }
   }
}


void clAppManager::vSynchronizeOnNoActiveSource()
{
   if (!queuedSession.bIsPendingRequest())
   {
      vSwitchForeGroundApp();
      activeSession.AllowBackContext();
   }
   else
   {
      if (queuedSession.bAudioMuted && (bIsSourceMode(backGroundSession.applicationId)))
      {
         displayControl.switchClock(true);
         activeSession.reset();
      }
      else if (!queuedSession.bAudioMuted && backGroundSession.applicationId)
      {
         vSwitchToAppInBackground();
         activeSession.AllowBackContext();
      }
      activeSession.bAudioMuted = queuedSession.bAudioMuted;
      queuedSession.state = COMPLETED;
   }
}


void clAppManager::vSetApplicationMode(uint32 applicationId, uint32 applicationMode)
{
   applicationModes[applicationId] = applicationMode;
}


void clAppManager::vSwitchForeGroundApp()
{
   ETG_TRACE_USR4(("APPMGR:   vSwitchForeGroundApp      applicationId     : %d ", ETG_CENUM(enApplicationId, requestedSession.applicationId)));
   displayControl.switchApp(requestedSession.applicationId);
   activeSession.applicationId = requestedSession.applicationId;
   activeSession.triggerType = requestedSession.triggerType;
   activeSession.contextId = requestedSession.contextId;
   activeSession.priority = requestedSession.priority;
   activeSession.bForwardContext = requestedSession.bForwardContext;
   activeSession.bTemporaryContext = requestedSession.bTemporaryContext;
   requestedSession.state =  COMPLETED;
   activeSession.state =  PROCESSED;
   vSynchronize();
}


void clAppManager::vSwitchActiveAppToForeground()
{
   ETG_TRACE_USR4(("APPMGR:   vSwitchActiveAppToForeground      applicationId     : %d ", ETG_CENUM(enApplicationId, activeSession.applicationId)));
   displayControl.switchApp(activeSession.applicationId);
   requestedSession.state =  COMPLETED;
   activeSession.state =  PROCESSED;
   vSynchronize();
}


void clAppManager::vSwitchToActiveAudioApp()
{
   ETG_TRACE_USR4(("APPMGR:   vSwitchToActiveAudioApp      applicationId     : %d ", ETG_CENUM(enApplicationId, activeSession.activeAudioApplicationId)));
   displayControl.switchApp(activeSession.activeAudioApplicationId);
   activeSession.applicationId = activeSession.activeAudioApplicationId;
   activeSession.triggerType = AUDIOFOLLOWING;
   activeSession.contextId = 0;
   activeSession.priority = 0xff;
   activeSession.bForwardContext = false;
   activeSession.bTemporaryContext = false;
   requestedSession.state =  COMPLETED;
   activeSession.state =  PROCESSED;
   vSynchronize();
}


void clAppManager::vSwitchBackGroundApp(bool bUpdateVisibleApplication)
{
   if (bUpdateVisibleApplication)
   {
      displayControl.switchApp(requestedSession.applicationId);
   }
   ETG_TRACE_USR4(("APPMGR:   vSwitchBackGroundApp      applicationId     : %d ", ETG_CENUM(enApplicationId, requestedSession.applicationId)));
   backGroundSession.applicationId = requestedSession.applicationId;
   backGroundSession.contextId = requestedSession.contextId;
   backGroundSession.triggerType = requestedSession.triggerType;
   backGroundSession.priority = requestedSession.priority;
   backGroundSession.bForwardContext = requestedSession.bForwardContext;
   requestedSession.state =  COMPLETED;
   backGroundSession.state =  COMPLETED;
   backGroundSession.bVisibleApplicationUpdated = bUpdateVisibleApplication;
   backGroundSession.bTemporaryContext = requestedSession.bTemporaryContext;
   vSynchronize();
}


void clAppManager::vSwitchToAppInBackground()
{
   ETG_TRACE_USR4(("APPMGR:   vSwitchToAppInBackground      applicationId     : %d ", ETG_CENUM(enApplicationId, backGroundSession.applicationId)));
   requestedSession.state =  COMPLETED;
   activeSession.applicationId = backGroundSession.applicationId ;
   activeSession.triggerType = backGroundSession.triggerType;
   activeSession.contextId = backGroundSession.contextId;
   activeSession.priority = backGroundSession.priority ;
   activeSession.bForwardContext = backGroundSession.bForwardContext ;
   activeSession.state = PROCESSED;
   activeSession.bTemporaryContext = backGroundSession.bTemporaryContext;
   displayControl.switchApp(activeSession.applicationId);
   backGroundSession.reset();
   vSynchronize();
}


void clAppManager::vSwitchAppToBackground()
{
   ETG_TRACE_USR4(("APPMGR:   vSwitchAppToBackground      applicationId     : %d ", ETG_CENUM(enApplicationId, activeSession.applicationId)));
   requestedSession.state =  COMPLETED;
   backGroundSession.applicationId = activeSession.applicationId ;
   backGroundSession.triggerType = activeSession.triggerType;
   backGroundSession.contextId = activeSession.contextId;
   backGroundSession.priority = activeSession.priority ;
   backGroundSession.bForwardContext = activeSession.bForwardContext ;
   backGroundSession.bTemporaryContext = activeSession.bTemporaryContext;
   backGroundSession.state = COMPLETED;
   vSynchronize();
}


void clAppManager::vSwitchQueuedToBackground()
{
   ETG_TRACE_USR4(("APPMGR:   vSwitchQueuedToBackground      applicationId     : %d ", ETG_CENUM(enApplicationId, queuedSession.applicationId)));
   backGroundSession.applicationId = queuedSession.applicationId ;
   backGroundSession.triggerType = queuedSession.triggerType;
   backGroundSession.contextId = queuedSession.contextId;
   backGroundSession.priority = queuedSession.priority ;
   backGroundSession.bForwardContext = queuedSession.bForwardContext ;
   backGroundSession.bTemporaryContext = queuedSession.bTemporaryContext;
   backGroundSession.state = COMPLETED;
}


void clAppManager::vPrintSessionState()
{
   ETG_TRACE_USR4(("APPMGR:   REQUESTEDSESSION            applicationID     : %d ", ETG_CENUM(enApplicationId, requestedSession.applicationId)));
   ETG_TRACE_USR4(("APPMGR:   REQUESTEDSESSION            contextId         : %d ", ETG_CENUM(enContextId, requestedSession.contextId)));
   ETG_TRACE_USR4(("APPMGR:   REQUESTEDSESSION            TriggerType       : %d ", ETG_CENUM(enTriggerType, requestedSession.triggerType)));
   ETG_TRACE_USR4(("APPMGR:   REQUESTEDSESSION            state             : %d ", ETG_CENUM(enSessionState, requestedSession.state)));
   ETG_TRACE_USR4(("APPMGR:   REQUESTEDSESSION            bForwardContext   : %d ", requestedSession.bForwardContext));
   ETG_TRACE_USR4(("APPMGR:   REQUESTEDSESSION            priority          : %d ", requestedSession.priority));
   ETG_TRACE_USR4(("APPMGR:   REQUESTEDSESSION            bAudioMuted       : %d ", requestedSession.bAudioMuted));

   ETG_TRACE_USR4(("APPMGR:   BACKGROUNDSESSION           applicationID     : %d ", ETG_CENUM(enApplicationId, backGroundSession.applicationId)));
   ETG_TRACE_USR4(("APPMGR:   BACKGROUNDSESSION           contextId         : %d ", ETG_CENUM(enContextId, backGroundSession.contextId)));
   ETG_TRACE_USR4(("APPMGR:   BACKGROUNDSESSION           TriggerType       : %d ", ETG_CENUM(enTriggerType, backGroundSession.triggerType)));
   ETG_TRACE_USR4(("APPMGR:   BACKGROUNDSESSION           state             : %d ", ETG_CENUM(enSessionState, backGroundSession.state)));
   ETG_TRACE_USR4(("APPMGR:   BACKGROUNDSESSION           bForwardContext   : %d ", backGroundSession.bForwardContext));
   ETG_TRACE_USR4(("APPMGR:   BACKGROUNDSESSION           priority          : %d ", backGroundSession.priority));
   ETG_TRACE_USR4(("APPMGR:   BACKGROUNDSESSION           bAudioMuted       : %d ", backGroundSession.bAudioMuted));

   ETG_TRACE_USR4(("APPMGR:   ACTIVESESSION               applicationID     : %d ", ETG_CENUM(enApplicationId, activeSession.applicationId)));
   ETG_TRACE_USR4(("APPMGR:   ACTIVESESSION               contextId         : %d ", ETG_CENUM(enContextId, activeSession.contextId)));
   ETG_TRACE_USR4(("APPMGR:   ACTIVESESSION               TriggerType       : %d ", ETG_CENUM(enTriggerType, activeSession.triggerType)));
   ETG_TRACE_USR4(("APPMGR:   ACTIVESESSION               state             : %d ", ETG_CENUM(enSessionState, activeSession.state)));
   ETG_TRACE_USR4(("APPMGR:   ACTIVESESSION               bForwardContext   : %d ", activeSession.bForwardContext));
   ETG_TRACE_USR4(("APPMGR:   ACTIVESESSION               priority          : %d ", activeSession.priority));
   ETG_TRACE_USR4(("APPMGR:   ACTIVESESSION               bAudioMuted       : %d ", activeSession.bAudioMuted));

   ETG_TRACE_USR4(("APPMGR:   QUEUEDSESSION               applicationID     : %d ", ETG_CENUM(enApplicationId, queuedSession.applicationId)));
   ETG_TRACE_USR4(("APPMGR:   QUEUEDSESSION               contextId         : %d ", ETG_CENUM(enContextId, queuedSession.contextId)));
   ETG_TRACE_USR4(("APPMGR:   QUEUEDSESSION               TriggerType       : %d ", ETG_CENUM(enTriggerType, queuedSession.triggerType)));
   ETG_TRACE_USR4(("APPMGR:   QUEUEDSESSION               state             : %d ", ETG_CENUM(enSessionState, queuedSession.state)));
   ETG_TRACE_USR4(("APPMGR:   QUEUEDSESSION               bForwardContext   : %d ", queuedSession.bForwardContext));
   ETG_TRACE_USR4(("APPMGR:   QUEUEDSESSION               priority          : %d ", queuedSession.priority));
   ETG_TRACE_USR4(("APPMGR:   QUEUEDSESSION               bAudioMuted       : %d ", queuedSession.bAudioMuted));
}


void clAppManager::vOnNewActiveSession()
{
   displayStatus.vOnNewActiveFGApplication(activeSession.applicationId);
   displayStatus.vOnNewActiveBGApplication(backGroundSession.applicationId);
}


void clAppManager::vSwitchToClock()
{
   displayControl.switchClock(true);
}


void clAppManager::vOnContextClosed(uint32 contextId)
{
   if (backGroundSession.contextId == contextId  || activeSession.contextId == contextId)
   {
      requestedSession.reset();
      requestedSession.triggerType = CONTEXTCOMPLETE;
      requestedSession.contextId = contextId;
      requestedSession.state = REQUESTED;
      vSynchronize();
   }
}


void clAppManager::vOnNewEntertainmentMuteState(bool bAudioMuted)
{
   ETG_TRACE_USR4(("APPMGR:   vOnNewEntertainmentMuteState      bAudioMuted     : %d ", bAudioMuted));
   requestedSession.reset();
   requestedSession.triggerType = ENTERTAINMENTMUTE;
   requestedSession.bAudioMuted = bAudioMuted;
   requestedSession.state = REQUESTED;
   vSynchronize();
}


void clAppManager::switchToLastActiveApplication()
{
   ETG_TRACE_USR4(("APPMGR:   switchToLastActiveApplication      						"));
   displayControl.switchApp(0);
   activeSession.reset();
}


void clAppManager::vOnNewActiveAudioApplication(uint32 applicationId)
{
   ETG_TRACE_USR4(("APPMGR:   vOnNewActiveAudioApplication      %d", ETG_CENUM(enApplicationId, applicationId)));
   activeSession.activeAudioApplicationId = applicationId;
}


bool clAppManager::bIsSourceMode(uint32 applicationId)
{
   if (applicationModes.find(applicationId) != applicationModes.end() && applicationModes[applicationId] == APPMODE_SOURCE)
   {
      return true;
   }
   return false;
}


bool clAppManager::bIsClockMode(uint32 applicationId)
{
   if (applicationModes.find(applicationId) != applicationModes.end() && applicationModes[applicationId] == APPMODE_CLOCK)
   {
      return true;
   }
   return false;
}


void clAppManager::vReset()
{
   ETG_TRACE_USR4(("APPMGR:   vReset      "));
   activeSession.reset();
   backGroundSession.reset();
   requestedSession.reset();
   queuedSession.reset();
}


void clAppManager::vOnApplicationSwitchComplete(uint32 applicationId)
{
   ETG_TRACE_USR4(("APPMGR:   vOnApplicationSwitchComplete      applicationId      : %d ", ETG_CENUM(enApplicationId, applicationId)));
   requestedSession.reset();
   requestedSession.triggerType = APPLICATIONSWITCHCOMPLETE;
   requestedSession.applicationId = applicationId;
   requestedSession.state = REQUESTED;
   vSynchronize();
}


bool clAppManager::bIsAllowBackRequest()
{
   return !activeSession.bIgnoreBackContext();
}


void clAppManager::allowBackRequest()
{
   activeSession.AllowBackContext();
}
