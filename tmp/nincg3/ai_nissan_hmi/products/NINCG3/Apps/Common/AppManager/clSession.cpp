///////////////////////////////////////////////////////////
//  clSession.cpp
//  Implementation of the Class clSession
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#include "clSession.h"
#include "hmi_trace_if.h"
#include "../Common_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_APPMANAGER
#include "trcGenProj/Header/clSession.cpp.trc.h"
#endif


clSession::clSession()
   : triggerType(TRIGGERTYPE_NONE)
   , applicationId(0)
   , visibleApplicationId(0)
   , activeAudioApplicationId(0)
   , contextId(0)
   , priority(0xFF)
   , bForwardContext(true)
   , bAudioMuted(false)
   , state(SESSIONSTATE_NONE)
   , bAllowbackContext(true)
   , bIsNewDeviceAdded(false)
   , bVisibleApplicationUpdated(false)
   , bTemporaryContext(false)
{
}


clSession::~clSession()
{
}


bool clSession::bIsPendingRequest()
{
   if (state == REQUESTED)
   {
      return true;
   }
   return false;
}


bool clSession::bIsInProgessRequest()
{
   if (state == INPROGRESS)
   {
      return true;
   }
   return false;
}


bool clSession::bIsCompletedRequest()
{
   if (state == COMPLETED)
   {
      return true;
   }
   return false;
}


bool clSession::bIsProcessedRequest()
{
   if (state == PROCESSED)
   {
      return true;
   }
   return false;
}


bool clSession::bIsEntertainmentMuteRequest()
{
   if (triggerType == ENTERTAINMENTMUTE)
   {
      return true;
   }
   return false;
}


bool clSession::bIsContextRequest()
{
   if (triggerType == CONTEXTSWITCH)
   {
      return true;
   }
   return false;
}


bool clSession::bIsHardKeyRequest()
{
   if (triggerType == HARDKEY)
   {
      return true;
   }
   return false;
}


bool clSession::bIsContextCompleteRequest()
{
   if (triggerType == CONTEXTCOMPLETE)
   {
      return true;
   }
   return false;
}


bool clSession::bIsAudioFollowingRequest()
{
   if (triggerType == AUDIOFOLLOWING)
   {
      return true;
   }
   return false;
}


bool clSession::bIsHighPriorityContextActive()
{
   if (priority < 0xFF)
   {
      return true;
   }
   return false;
}


bool clSession::bIsForwardContext()
{
   return bForwardContext;
}


bool clSession::bAllowAudioFollowing()
{
   return true;
}


bool clSession::bIgnoreBackContext()
{
   return !bAllowbackContext;
}


bool clSession::bIsSourceMode()
{
   return true;
}


void clSession::IgnoreBackContext()
{
   bAllowbackContext = false;
}


void clSession::AllowBackContext()
{
   bAllowbackContext = true;
}


bool clSession::bIsApplicationSwitchComplete()
{
   if (triggerType == APPLICATIONSWITCHCOMPLETE)
   {
      return true;
   }
   return false;
}


void clSession::reset()
{
   triggerType = TRIGGERTYPE_NONE;
   applicationId = 0;
   contextId = 0;
   priority = 0xFF;
   bForwardContext = true ;
   state = COMPLETED;
   bAllowbackContext = true;
   bIsNewDeviceAdded = false;
   bTemporaryContext = false;
}
