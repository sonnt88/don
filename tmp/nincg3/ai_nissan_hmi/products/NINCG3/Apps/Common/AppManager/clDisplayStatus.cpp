/**************************************************************************//**
* \file     clDisplayStatus.cpp
*
*           clDisplayStatus method class implementation
*
* \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
******************************************************************************/

#include "clDisplayStatus.h"
#include "interface/clDisplayStatusRecieverInterface.h"

static clDisplayStatusRecieverInterface* m_poDisplayStatusReciever;


/**************************************************************************//**
* Constructor
******************************************************************************/
clDisplayStatus::clDisplayStatus()
{
}


/**************************************************************************//**
* Destructor
******************************************************************************/
clDisplayStatus::~clDisplayStatus()
{
}


/**************************************************************************//**
*
******************************************************************************/
void clDisplayStatus::vSetDisplayStatusRecieverImpl(clDisplayStatusRecieverInterface* poImpl)
{
   m_poDisplayStatusReciever = poImpl;
}


/**************************************************************************//**
*
******************************************************************************/
void clDisplayStatus::vOnNewActiveBGApplication(uint32 applicationId)
{
   if (m_poDisplayStatusReciever)
   {
      m_poDisplayStatusReciever->vOnNewActiveBGApplication(applicationId);
   }
}


/**************************************************************************//**
*
******************************************************************************/
void clDisplayStatus::vOnNewActiveFGApplication(uint32 applicationId)
{
   if (m_poDisplayStatusReciever)
   {
      m_poDisplayStatusReciever->vOnNewActiveFGApplication(applicationId);
   }
}
