///////////////////////////////////////////////////////////
//  clSession.h
//  Implementation of the Class clSession
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(clSession_h)
#define clSession_h

/**
 * interface class for controlling application switch
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 5:44:42 PM
 */


#include "asf/core/Types.h"
#include <vector>


enum enTriggerType
{
   HARDKEY,
   CONTEXTSWITCH,
   AUDIOFOLLOWING,
   CONTEXTCOMPLETE,
   ENTERTAINMENTMUTE,
   APPLICATIONSWITCHCOMPLETE,
   TRIGGERTYPE_NONE
};


enum enSessionState
{
   REQUESTED,
   INPROGRESS,
   PROCESSED,
   COMPLETED,
   SESSIONSTATE_NONE
};


class clSession
{
   public:
      clSession();
      virtual ~clSession();

      bool bIsPendingRequest();
      bool bIsInProgessRequest();
      bool bIsCompletedRequest();
      bool bIsProcessedRequest();

      bool bIsContextRequest();
      bool bIsHardKeyRequest();
      bool bIsApplicationSwitchComplete();
      bool bIsContextCompleteRequest();
      bool bIsAudioFollowingRequest();
      bool bAllowAudioFollowing();
      bool bIsEntertainmentMuteRequest();

      bool bIsHighPriorityContextActive();
      bool bIsForwardContext();
      bool bIgnoreBackContext();
      bool bIsSourceMode();

      void IgnoreBackContext();
      void AllowBackContext();
      void reset();

      enTriggerType triggerType;
      uint32 applicationId;
      uint32 activeAudioApplicationId;
      uint32 contextId;
      uint32 priority;
      uint32 visibleApplicationId;
      bool bForwardContext;
      bool bAudioMuted;
      bool bIsNewDeviceAdded;
      bool bVisibleApplicationUpdated;
      bool bTemporaryContext;
      enSessionState state;

   private:
      bool bAllowbackContext;
};


#endif // !defined(EA_FAF598AF_B4F9_4bb1_91CE_ABBA6F93217B__INCLUDED_)
