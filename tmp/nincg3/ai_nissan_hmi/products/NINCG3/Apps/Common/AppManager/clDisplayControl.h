/**************************************************************************//**
* \file     clDisplayControl.h
*
*
* \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
******************************************************************************/
#ifndef clDisplayControl_h
#define clDisplayControl_h


#include "asf/core/Types.h"
#include "interface/clDisplayControlInterface.h"


class clDisplayControl
{
   public:
      virtual ~clDisplayControl();
      clDisplayControl();

      void switchApp(uint32 applicationId);
      void switchClock(bool bShowClock = true);
      void vSetDisplayControlImpl(clDisplayControlInterface* poImpl);
};


#endif // clDisplayControl_h
