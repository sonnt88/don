///////////////////////////////////////////////////////////
//  clClockOnMute_Test.h
//  Implementation of the Class clClockOnMute_Test
//  Created on:      27-Mar-2015 15:43:29
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#ifndef clClockOnMute_Test_h
#define clClockOnMute_Test_h

#include "gtest/gtest.h"
#include "clAppManager.h"
#include "mocks/clMock_DisplayStatusRecieverImpl.h"
#include "mocks/clMock_DisplayControlImpl.h"
#include "clDisplayStatus.h"
#include "clDisplayControl.h"


#define TEST_APPHMI_MEDIA 0x10
#define TEST_APPHMI_SYSTEM 0x11
#define TEST_APPHMI_TUNER 0x12
#define TEST_APPHMI_SETUP 0x13
#define TEST_APPHMI_VEHICLE 0x14
#define TEST_APPHMI_NONE 0x0


using testing::Test;

class clClockOnMute_Test  : public testing::Test
{
   public:
      clClockOnMute_Test()
      {
      }
      virtual ~clClockOnMute_Test()
      {
      }
      void SetUp()
      {
         ::testing::Test::RecordProperty("Component", "APPMANAGER");
         status.vSetDisplayStatusRecieverImpl(&oDisplayStatusMock);
         control.vSetDisplayControlImpl(&oDisplayControlMock);
         oManager.vSetApplicationMode(TEST_APPHMI_MEDIA, APPMODE_SOURCE);
         oManager.vSetApplicationMode(TEST_APPHMI_SYSTEM, APPMODE_UTILITY);
         oManager.vSetApplicationMode(TEST_APPHMI_TUNER, APPMODE_SOURCE);
         oManager.vSetApplicationMode(TEST_APPHMI_SETUP, APPMODE_SOURCE);
         oManager.vSetApplicationMode(TEST_APPHMI_VEHICLE, APPMODE_UTILITY);
      }
      void TearDown()
      {
      }
      clDisplayStatus status;
      clDisplayControl  control;
      clMock_DisplayControlImpl oDisplayControlMock;
      clMock_DisplayStatusRecieverImpl oDisplayStatusMock;
      clAppManager oManager;
};


#endif // !defined(clClockOnMute_Test_h)
