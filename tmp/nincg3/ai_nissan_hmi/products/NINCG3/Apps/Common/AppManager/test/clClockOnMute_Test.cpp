///////////////////////////////////////////////////////////
//  clClockOnMute_Test.cpp
//  Implementation of the Class clClockOnMute_Test
//  Created on:      27-Mar-2015 15:43:29
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#include "test/clClockOnMute_Test.h"

#include "clAppManager.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"


using testing::_;
using testing::ElementsAre;
using testing::Property;
using testing::Matcher;
using testing::Return;
using testing::SetArgPointee;
using testing::Mock;
using testing::TestWithParam;
using testing::Values;
using std::tr1::tuple_element;


#define TEST_CONTEXT_CLOCKMODE   0x100
#define TEST_CONTEXT_CLOCKONMUTE 0x101
#define TEST_CONTEXT_RVC         0x102
#define TEST_CONTEXT_SETUPMAIN   0x103
#define TEST_CONTEXT_SYSTEM_MAIN 0x104


TEST_F(clClockOnMute_Test, DisconnectActiveSource_ClockModeActive_ClockModeRemainsActive)
{
   RecordProperty("USECASE", "INSERTUSB->USB_PLAYING -> HK_AUDIO_OFF -> DISCONNECT_USB");

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_MEDIA)).Times(1);   // Clock switched and not active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_MEDIA)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnAudioSourceChange(TEST_APPHMI_MEDIA);

   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_MEDIA)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_MEDIA)).Times(1);   // no app in BG
   EXPECT_CALL(oDisplayControlMock, switchClock(true)).Times(1);   // request to enable clock
   oManager.vOnNewEntertainmentMuteState(true);

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_SYSTEM)).Times(1);   // Clock switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_MEDIA)).Times(1);   // no app in BG

   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_CLOCKONMUTE, TEST_APPHMI_SYSTEM, 0xff, true);

   EXPECT_CALL(oDisplayControlMock, switchClock(true)).Times(1);   // request to enable clock
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_TUNER)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnAudioSourceChange(TEST_APPHMI_TUNER);
}


TEST_F(clClockOnMute_Test, ConnectNewSource_ClockModeActive_ClockModeClosedAndNewSourceEntered)
{
   RecordProperty("USECASE", "INSERTUSB->USB_PLAYING -> HK_AUDIO_OFF -> DISCONNECT_USB -> CONNECT->USB");

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_MEDIA)).Times(1);   // Clock switched and not active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_MEDIA)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnAudioSourceChange(TEST_APPHMI_MEDIA);

   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_MEDIA)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_MEDIA)).Times(1);   // no app in BG
   EXPECT_CALL(oDisplayControlMock, switchClock(true)).Times(1);   // request to enable clock
   oManager.vOnNewEntertainmentMuteState(true);

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_SYSTEM)).Times(1);   // Clock switched and not active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_MEDIA)).Times(1);   // no app in BG

   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_CLOCKONMUTE, TEST_APPHMI_SYSTEM, 0xff, true);

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_MEDIA)).Times(1);
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_MEDIA)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_MEDIA)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnAudioSourceChange(TEST_APPHMI_MEDIA, true);
}


TEST_F(clClockOnMute_Test, EnterToMediaViaContextSwitch_MuteActiveFromUtilityApplication_ClockModeIsTriggered)
{
   RecordProperty("USECASE", "MEDIA USB PLAYING -> ENTER SYSTEM -> HK_AUDIO_OFF -> DISCONNECT_USB -> AUDIO FOLLOWING TO BT -> CLOCKMODE IS ON -> DEMUTE -> ENTERS TO MEDIA");

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG

   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_SYSTEM_MAIN, TEST_APPHMI_SYSTEM, 0xff, true);

   EXPECT_CALL(oDisplayControlMock, switchClock(_)).Times(0);   // request to enable clock
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vOnNewEntertainmentMuteState(true);

   EXPECT_CALL(oDisplayControlMock, switchClock(true)).Times(1);   // request to enable clock
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_MEDIA)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnAudioSourceChange(TEST_APPHMI_MEDIA);

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_SYSTEM)).Times(1);   // Clock switched and not active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_MEDIA)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_CLOCKONMUTE, TEST_APPHMI_SYSTEM, 0xff, true);
   oManager.vSetApplicationMode(TEST_APPHMI_SYSTEM, APPMODE_CLOCK);

   EXPECT_CALL(oDisplayControlMock, switchClock(false)).Times(1);   // Clock switched and not active
   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_MEDIA)).Times(1);   // Clock switched and not active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_MEDIA)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vOnNewEntertainmentMuteState(false);
}


TEST_F(clClockOnMute_Test, EnterToMediaViaHK_MuteActiveFromUtilityApplication_NoClockOnMute)
{
   RecordProperty("USECASE", "MEDIA USB PLAYING -> ENTER SYSTEM -> HK_AUDIO_OFF -> PRESS HK_AUX -> Enter into MEDIA and start playback");

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG

   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_SYSTEM_MAIN, TEST_APPHMI_SYSTEM, 0xff, true);

   EXPECT_CALL(oDisplayControlMock, switchClock(_)).Times(0);   // request to enable clock
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vOnNewEntertainmentMuteState(true);

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_MEDIA)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_MEDIA)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnHardKey(TEST_APPHMI_MEDIA);
}


TEST_F(clClockOnMute_Test, SourceChangeWhileMuted_MuteActiveFromUtilityApplicationRVCActive_ComesToRequiredApplication)
{
   RecordProperty("USECASE", "USB playing -> HK_MUTE -> RVC ON -> HK_TUNER -> RVC OFF -> DEMUTE -> Goes to Tuner");

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG

   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_SYSTEM_MAIN, TEST_APPHMI_SYSTEM, 0xff, true);

   EXPECT_CALL(oDisplayControlMock, switchClock(_)).Times(0);   // request to enable clock
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vOnNewEntertainmentMuteState(true);

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_MEDIA)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_MEDIA)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnHardKey(TEST_APPHMI_MEDIA);

   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_MEDIA)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vOnNewEntertainmentMuteState(false);
}


TEST_F(clClockOnMute_Test, DeMuteTriggerAfterEnterToMediaViaHK_MuteActiveFromUtilityApplication_NoClockOnMute)
{
   RecordProperty("USECASE", "INSERT USB -> MEDIA USB PLAYING -> ENTER SYSTEM -> HK_AUDIO_OFF -> RVC ON -> HK_AUX -> HK_AUDIO_OFF -> RVC OFF ");

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_MEDIA)).Times(1);   // Clock switched and not active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_MEDIA)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnAudioSourceChange(TEST_APPHMI_MEDIA);

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG

   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_SYSTEM_MAIN, TEST_APPHMI_SYSTEM, 0xff, true);

   EXPECT_CALL(oDisplayControlMock, switchClock(_)).Times(0);   // request to enable clock
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vOnNewEntertainmentMuteState(true);

   EXPECT_CALL(oDisplayControlMock, switchClock(_)).Times(0);   // request to enable clock
   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_VEHICLE)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_VEHICLE)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_RVC, TEST_APPHMI_VEHICLE, 0x80, true);

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_MEDIA)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_VEHICLE)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_MEDIA)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnHardKey(TEST_APPHMI_MEDIA);

   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_VEHICLE)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_MEDIA)).Times(1);   // no app in BG
   oManager.vOnNewEntertainmentMuteState(false);

   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_MEDIA)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vOnContextClosed(TEST_CONTEXT_RVC);
}


TEST_F(clClockOnMute_Test, DeMuteTriggerAfterRVCOFF_MuteActiveFromUtilityApplicationAndMediaHKPressed_ClockOnMuteActivated)
{
   RecordProperty("USECASE", "INSERT USB -> MEDIA USB PLAYING -> ENTER SYSTEM -> HK_AUDIO_OFF -> RVC ON -> HK_AUX -> RVC OFF : ClockMode is activated ");

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_MEDIA)).Times(1);   // Clock switched and not active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_MEDIA)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnAudioSourceChange(TEST_APPHMI_MEDIA);

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG

   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_SYSTEM_MAIN, TEST_APPHMI_SYSTEM, 0xff, true);

   EXPECT_CALL(oDisplayControlMock, switchClock(_)).Times(0);   // request to enable clock
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vOnNewEntertainmentMuteState(true);

   EXPECT_CALL(oDisplayControlMock, switchClock(_)).Times(0);   // request to enable clock
   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_VEHICLE)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_VEHICLE)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_RVC, TEST_APPHMI_VEHICLE, 0x80, true);

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_MEDIA)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_VEHICLE)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_MEDIA)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnHardKey(TEST_APPHMI_MEDIA);

   EXPECT_CALL(oDisplayControlMock, switchClock(true)).Times(1);   // request to enable clock
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_NONE)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_MEDIA)).Times(1);   // no app in BG
   oManager.vOnContextClosed(TEST_CONTEXT_RVC);

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_SYSTEM)).Times(1);   // Clock switched and not active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_MEDIA)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_CLOCKONMUTE, TEST_APPHMI_SYSTEM, 0xff, true);
   oManager.vSetApplicationMode(TEST_APPHMI_SYSTEM, APPMODE_CLOCK);
}


TEST_F(clClockOnMute_Test, RemoveEntertainmentMute_MuteActiveFromUtilityApplicationAndMediaHKPressedAndRVCWasActivated_MediaWasActivated)
{
   RecordProperty("USECASE", "INSERT USB -> MEDIA USB PLAYING -> ENTER SYSTEM -> HK_AUDIO_OFF -> RVC ON -> HK_AUX -> RVC OFF  -> HK_AUDIO_OFF :Entered Media ");

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_MEDIA)).Times(1);   // Clock switched and not active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_MEDIA)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnAudioSourceChange(TEST_APPHMI_MEDIA);

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG

   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_SYSTEM_MAIN, TEST_APPHMI_SYSTEM, 0xff, true);

   EXPECT_CALL(oDisplayControlMock, switchClock(_)).Times(0);   // request to enable clock
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vOnNewEntertainmentMuteState(true);

   EXPECT_CALL(oDisplayControlMock, switchClock(_)).Times(0);   // request to enable clock
   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_VEHICLE)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_VEHICLE)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_RVC, TEST_APPHMI_VEHICLE, 0x80, true);

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_MEDIA)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_VEHICLE)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_MEDIA)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnHardKey(TEST_APPHMI_MEDIA);

   EXPECT_CALL(oDisplayControlMock, switchClock(true)).Times(1);   // request to enable clock
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_NONE)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_MEDIA)).Times(1);   // no app in BG
   oManager.vOnContextClosed(TEST_CONTEXT_RVC);

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_SYSTEM)).Times(1);   // Clock switched and not active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // Clock activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_MEDIA)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_CLOCKONMUTE, TEST_APPHMI_SYSTEM, 0xff, true);
   oManager.vSetApplicationMode(TEST_APPHMI_SYSTEM, APPMODE_CLOCK);

   EXPECT_CALL(oDisplayControlMock, switchClock(false)).Times(1);   // request to enable clock
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_MEDIA)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vOnNewEntertainmentMuteState(false);
}
