///////////////////////////////////////////////////////////
//  clReportedBugs_Test.cpp
//  Implementation of the Class clReportedBugs_Test
//  Created on:      27-Mar-2015 15:43:29
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#include "test/clReportedBugs_Test.h"

#include "clAppManager.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"


using testing::_;
using testing::ElementsAre;
using testing::Property;
using testing::Matcher;
using testing::Return;
using testing::SetArgPointee;
using testing::Mock;
using testing::TestWithParam;
using testing::Values;
using std::tr1::tuple_element;


#define TEST_CONTEXT_CLOCKMODE   0x100
#define TEST_CONTEXT_CLOCKONMUTE 0x101
#define TEST_CONTEXT_RVC         0x102
#define TEST_CONTEXT_SETUPMAIN   0x103
#define TEST_CONTEXT_SYSTEM_MAIN 0x104
#define TEST_CONTEXT_AVM         0x105


TEST_F(clReportedBugs_Test, BackFromContextSwitch_AVMExitedViaAnotherHK_BackFromContextSwitchIgnored)
{
   RecordProperty("USECASE", "Entered AVM via CAMERA HK. -> Press RADIO  (Radio switch happens before context back): Expected goto RADIO");
   RecordProperty("BUG", "NCG3D-1831");

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_SYSTEM_MAIN, TEST_APPHMI_SYSTEM, 0xff, true);

   EXPECT_CALL(oDisplayControlMock, switchClock(_)).Times(0);
   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_VEHICLE)).Times(1);
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_VEHICLE)).Times(1);
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);
   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_AVM, TEST_APPHMI_VEHICLE, 0x80, true);

   EXPECT_CALL(oDisplayControlMock, switchClock(_)).Times(0);
   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_TUNER)).Times(1);
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_VEHICLE)).Times(1);
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_TUNER)).Times(1);
   oManager.vSwitchAppRequestOnHardKey(TEST_APPHMI_TUNER);

   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_VEHICLE)).Times(1);
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_TUNER)).Times(1);
   oManager.vSwitchAppRequestOnContext(0, TEST_APPHMI_SYSTEM, 0xff, false);

   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_TUNER)).Times(1);
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);
   oManager.vOnContextClosed(TEST_CONTEXT_AVM);
}


TEST_F(clReportedBugs_Test, BackFromContextSwitch_AVMExitedViaAnotherHKAfterBackContextSwitch_HKTriggeredActionTaken)
{
   RecordProperty("USECASE", "Entered AVM via CAMERA HK. -> Press RADIO  (Radio switch happens after context back): Expected goto RADIO");
   RecordProperty("BUG", "NCG3D-1831");

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_SYSTEM_MAIN, TEST_APPHMI_SYSTEM, 0xff, true);

   EXPECT_CALL(oDisplayControlMock, switchClock(_)).Times(0);
   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_VEHICLE)).Times(1);
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_VEHICLE)).Times(1);
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);
   oManager.vSwitchAppRequestOnContext(TEST_CONTEXT_AVM, TEST_APPHMI_VEHICLE, 0x80, true);

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_SYSTEM)).Times(1);
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_VEHICLE)).Times(1);
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_SYSTEM)).Times(1);
   oManager.vSwitchAppRequestOnContext(0, TEST_APPHMI_SYSTEM, 0xff, false);

   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_SYSTEM)).Times(1);
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);
   oManager.vOnContextClosed(TEST_CONTEXT_AVM);

   EXPECT_CALL(oDisplayControlMock, switchClock(_)).Times(0);
   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_TUNER)).Times(1);
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_TUNER)).Times(1);
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);
   oManager.vSwitchAppRequestOnHardKey(TEST_APPHMI_TUNER);
}


TEST_F(clReportedBugs_Test, HKPress_AudioSourceFollowingActive_HKTriggeredActionTaken)
{
   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_TUNER)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN switched and  active
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_TUNER)).Times(1);   // TEST_CONTEXT_SYSTEM_MAIN activated in FG
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);   // no app in BG
   oManager.vSwitchAppRequestOnAudioSourceChange(TEST_APPHMI_TUNER);

   EXPECT_CALL(oDisplayControlMock, switchApp(TEST_APPHMI_MEDIA)).Times(1);
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveFGApplication(TEST_APPHMI_MEDIA)).Times(1);
   EXPECT_CALL(oDisplayStatusMock, vOnNewActiveBGApplication(TEST_APPHMI_NONE)).Times(1);
   oManager.vSwitchAppRequestOnHardKey(TEST_APPHMI_MEDIA);
}
