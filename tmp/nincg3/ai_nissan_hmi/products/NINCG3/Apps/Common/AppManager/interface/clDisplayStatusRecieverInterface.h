/**************************************************************************//**
* \file     clDisplayStatusRecieverInterface.h
*
*
* \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
******************************************************************************/
#ifndef clDisplayStatusRecieverInterface_h
#define clDisplayStatusRecieverInterface_h


#include "asf/core/Types.h"


class clDisplayStatusRecieverInterface
{
   public:
      virtual ~clDisplayStatusRecieverInterface() {};
      clDisplayStatusRecieverInterface() {};

      virtual void vOnNewActiveBGApplication(uint32 applicationId) = 0;
      virtual void vOnNewActiveFGApplication(uint32 applicationId) = 0;
};


#endif // clDisplayStatusRecieverInterface_h
