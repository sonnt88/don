///////////////////////////////////////////////////////////
//  clAppManagerInterface.h
//  Implementation of the Class clAppManagerInterface
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(clAppManagerInterface_h)
#define clAppManagerInterface_h

/**
 * interface class for context proxies to control application switch
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 5:44:42 PM
 */

#include "asf/core/Types.h"


class clAppManagerInterface
{
   public:
      clAppManagerInterface() {};
      virtual ~clAppManagerInterface() {};
      virtual void vSwitchAppRequestOnHardKey(uint32 applicationId);
      virtual void vSwitchAppRequestOnContext(uint32 contextId, uint32 applicationId, uint32 priority);
      virtual void vSwitchAppRequestOnAudioSourceChange(uint32 applicationId) = 0;
};


#endif // !defined(EA_FAF598AF_B4F9_4bb1_91CE_ABBA6F93217B__INCLUDED_)
