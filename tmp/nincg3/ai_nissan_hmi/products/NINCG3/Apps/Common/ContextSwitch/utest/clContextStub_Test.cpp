/**************************************************************************//**
* \file     clContextStub_Test.cpp
*
*           clContextStub_Test method class implementation
*
* \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
******************************************************************************/

#include "clContextStub_Test.h"
#include "utest/Spy/clSpy_ContextStub.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchStub.h"



using testing::_;
using testing::ElementsAre;
using testing::Property;
using testing::Matcher;
using testing::Return;
using testing::SetArgPointee;
using testing::Mock;
using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch;
using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;


TEST_F(clContextStub_Test, onRequestContextRequest_NotAvailableContext_sendRequestContextResponseFalse)
{

   clSpy_ContextStub* oMockStub = new clSpy_ContextStub();
   EXPECT_CALL(*oMockStub, sendRequestContextResponse(false)).Times(1);
   RequestContextRequest* poRequest = new RequestContextRequest();
   poRequest->setSourceContext(10);
   poRequest->setTargetContext(20);
   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest));
   delete oMockStub;
}



TEST_F(clContextStub_Test, onRequestContextRequest_NotAvailableContext_sendVOnNewContextSignalNotCalled)
{

   clSpy_ContextStub* oMockStub = new clSpy_ContextStub();
   EXPECT_CALL(*oMockStub, sendVOnNewContextSignal(_, _)).Times(0);
   RequestContextRequest* poRequest = new RequestContextRequest();
   poRequest->setSourceContext(10);
   poRequest->setTargetContext(20);
   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest));
   delete oMockStub;
}


TEST_F(clContextStub_Test, onRequestContextRequest_AvailableContext_sendRequestContextResponseTrue)
{
   clSpy_ContextStub* oMockStub = new clSpy_ContextStub();
   EXPECT_CALL(*oMockStub, sendRequestContextResponse(true)).Times(1);
   RequestContextRequest* poRequest = new RequestContextRequest();
   poRequest->setSourceContext(10);
   poRequest->setTargetContext(20);
   ::std::vector<uint32> availableContexts;
   availableContexts.push_back(20);
   SetAvailableContextsRequest* poAavailableContexts = new SetAvailableContextsRequest();
   poAavailableContexts->setAvailableContext(availableContexts);
   oMockStub->onSetAvailableContextsRequest((const ::boost::shared_ptr< ContextSwitch::SetAvailableContextsRequest >)(poAavailableContexts));
   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest));
   delete oMockStub;
}


TEST_F(clContextStub_Test, onRequestContextRequest_LastContext_SwitchToLastContext)
{
   clSpy_ContextStub* oMockStub = new clSpy_ContextStub();
   EXPECT_CALL(*oMockStub, vSwitchApp(_)).Times(2);
   RequestContextRequest* poRequest = new RequestContextRequest();
   poRequest->setSourceContext(10);
   poRequest->setTargetContext(20);
   ::std::vector<uint32> availableContexts;
   availableContexts.push_back(20);
   SetAvailableContextsRequest* poAavailableContexts = new SetAvailableContextsRequest();
   poAavailableContexts->setAvailableContext(availableContexts);
   oMockStub->onSetAvailableContextsRequest((const ::boost::shared_ptr< ContextSwitch::SetAvailableContextsRequest >)(poAavailableContexts));

   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest));
   SwitchAppRequest* poSwitchAppRequest = new SwitchAppRequest();
   poSwitchAppRequest->setApplicationId(0x10);
   poSwitchAppRequest->setPriority(0xFF);
   poSwitchAppRequest->setTargetContext(20);
   oMockStub->onSwitchAppRequest((const ::boost::shared_ptr< ContextSwitch::SwitchAppRequest >)(poSwitchAppRequest));
   RequestContextRequest* poRequest1 = new RequestContextRequest();
   poRequest1->setSourceContext(0);
   poRequest1->setTargetContext(0);
   availableContexts.clear();
   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest1));
   delete oMockStub;
}



TEST_F(clContextStub_Test, onRequestContextRequest_AvailableContext_sendVOnNewContextSignalCalled)
{
   clSpy_ContextStub* oMockStub = new clSpy_ContextStub();
   EXPECT_CALL(*oMockStub, sendVOnNewContextSignal(10, 20)).Times(1);
   RequestContextRequest* poRequest = new RequestContextRequest();
   poRequest->setSourceContext(10);
   poRequest->setTargetContext(20);
   ::std::vector<uint32> availableContexts;
   availableContexts.push_back(20);
   SetAvailableContextsRequest* poAavailableContexts = new SetAvailableContextsRequest();
   poAavailableContexts->setAvailableContext(availableContexts);
   oMockStub->onSetAvailableContextsRequest((const ::boost::shared_ptr< ContextSwitch::SetAvailableContextsRequest >)(poAavailableContexts));
   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest));
   delete oMockStub;
}



TEST_F(clContextStub_Test, onSwitchAppRequest_NoLastApp_SwitchContext)
{
   clSpy_ContextStub* oMockStub = new clSpy_ContextStub();
   EXPECT_CALL(*oMockStub, vSwitchApp(_)).Times(1);
   EXPECT_CALL(*oMockStub, sendSwitchAppResponse(true)).Times(1);
   SwitchAppRequest* poSwitchAppRequest = new SwitchAppRequest();
   poSwitchAppRequest->setApplicationId(0x10);
   poSwitchAppRequest->setPriority(0xFF);
   poSwitchAppRequest->setTargetContext(20);
   oMockStub->onSwitchAppRequest((const ::boost::shared_ptr< ContextSwitch::SwitchAppRequest >)(poSwitchAppRequest));

   delete oMockStub;
}


TEST_F(clContextStub_Test, onRequestContextRequest_LastContextAfterTemporarySource_SwitchToLastPermanentContext)
{
   clSpy_ContextStub* oMockStub = new clSpy_ContextStub();
   EXPECT_CALL(*oMockStub, vSwitchApp(0x10)).Times(2);
   EXPECT_CALL(*oMockStub, vSwitchApp(0x11)).Times(1);

   RequestContextRequest* poRequest = new RequestContextRequest();
   poRequest->setSourceContext(5);
   poRequest->setTargetContext(20);
   ::std::vector<uint32> availableContexts;
   availableContexts.push_back(20);
   availableContexts.push_back(21);
   SetAvailableContextsRequest* poAavailableContexts = new SetAvailableContextsRequest();
   poAavailableContexts->setAvailableContext(availableContexts);
   oMockStub->onSetAvailableContextsRequest((const ::boost::shared_ptr< ContextSwitch::SetAvailableContextsRequest >)(poAavailableContexts));

   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest));

   SwitchAppRequest* poSwitchAppRequest = new SwitchAppRequest();
   poSwitchAppRequest->setApplicationId(0x10);
   poSwitchAppRequest->setPriority(0xFF);
   poSwitchAppRequest->setTargetContext(20);

   oMockStub->onSwitchAppRequest((const ::boost::shared_ptr< ContextSwitch::SwitchAppRequest >)(poSwitchAppRequest));


   RequestContextRequest* poRequest2 = new RequestContextRequest();
   poRequest2->setSourceContext(0);
   poRequest2->setTargetContext(21);
   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest2));


   SwitchAppRequest* poSwitchAppRequest1 = new SwitchAppRequest();
   poSwitchAppRequest1->setApplicationId(0x11);
   poSwitchAppRequest1->setPriority(0x80);
   poSwitchAppRequest1->setTargetContext(21);
   oMockStub->onSwitchAppRequest((const ::boost::shared_ptr< ContextSwitch::SwitchAppRequest >)(poSwitchAppRequest1));


   RequestContextRequest* poRequest1 = new RequestContextRequest();
   poRequest1->setSourceContext(0);
   poRequest1->setTargetContext(0);
   availableContexts.clear();
   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest1));
   delete oMockStub;
}




TEST_F(clContextStub_Test, onRequestContextRequest_LastContextAfterPermanentSource_SwitchToLastPermanentContext)
{
   clSpy_ContextStub* oMockStub = new clSpy_ContextStub();
   EXPECT_CALL(*oMockStub, vSwitchApp(0x10)).Times(1);
   EXPECT_CALL(*oMockStub, vSwitchApp(0x11)).Times(2);

   RequestContextRequest* poRequest = new RequestContextRequest();
   poRequest->setSourceContext(10);
   poRequest->setTargetContext(20);
   ::std::vector<uint32> availableContexts;
   availableContexts.push_back(20);
   SetAvailableContextsRequest* poAavailableContexts = new SetAvailableContextsRequest();
   poAavailableContexts->setAvailableContext(availableContexts);
   oMockStub->onSetAvailableContextsRequest((const ::boost::shared_ptr< ContextSwitch::SetAvailableContextsRequest >)(poAavailableContexts));

   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest));

   SwitchAppRequest* poSwitchAppRequest = new SwitchAppRequest();
   poSwitchAppRequest->setApplicationId(0x10);
   poSwitchAppRequest->setPriority(0xFF);
   poSwitchAppRequest->setTargetContext(20);

   oMockStub->onSwitchAppRequest((const ::boost::shared_ptr< ContextSwitch::SwitchAppRequest >)(poSwitchAppRequest));

   SwitchAppRequest* poSwitchAppRequest1 = new SwitchAppRequest();
   poSwitchAppRequest1->setApplicationId(0x11);
   poSwitchAppRequest1->setPriority(0xFF);
   poSwitchAppRequest1->setTargetContext(21);
   oMockStub->onSwitchAppRequest((const ::boost::shared_ptr< ContextSwitch::SwitchAppRequest >)(poSwitchAppRequest1));

   RequestContextRequest* poRequest1 = new RequestContextRequest();
   poRequest1->setSourceContext(0);
   poRequest1->setTargetContext(0);
   availableContexts.clear();
   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest1));
   delete oMockStub;
}





TEST_F(clContextStub_Test, onSwitchAppRequest_TemporaryContextActive_DontSwitchApp)
{
   clSpy_ContextStub* oMockStub = new clSpy_ContextStub();
   EXPECT_CALL(*oMockStub, vSwitchApp(0x10)).Times(0);
   EXPECT_CALL(*oMockStub, vSwitchApp(0x11)).Times(1);

   RequestContextRequest* poRequest = new RequestContextRequest();
   poRequest->setSourceContext(11);
   poRequest->setTargetContext(21);
   ::std::vector<uint32> availableContexts;
   availableContexts.push_back(20);
   SetAvailableContextsRequest* poAavailableContexts = new SetAvailableContextsRequest();
   poAavailableContexts->setAvailableContext(availableContexts);
   oMockStub->onSetAvailableContextsRequest((const ::boost::shared_ptr< ContextSwitch::SetAvailableContextsRequest >)(poAavailableContexts));

   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest));

   SwitchAppRequest* poSwitchAppRequest1 = new SwitchAppRequest();
   poSwitchAppRequest1->setApplicationId(0x11);
   poSwitchAppRequest1->setPriority(0x80);
   poSwitchAppRequest1->setTargetContext(21);
   oMockStub->onSwitchAppRequest((const ::boost::shared_ptr< ContextSwitch::SwitchAppRequest >)(poSwitchAppRequest1));

   RequestContextRequest* poRequest1 = new RequestContextRequest();
   poRequest1->setSourceContext(10);
   poRequest1->setTargetContext(20);
   availableContexts.clear();
   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest1));

   SwitchAppRequest* poSwitchAppRequest = new SwitchAppRequest();
   poSwitchAppRequest->setApplicationId(0x10);
   poSwitchAppRequest->setPriority(0xFF);
   poSwitchAppRequest->setTargetContext(20);

   oMockStub->onSwitchAppRequest((const ::boost::shared_ptr< ContextSwitch::SwitchAppRequest >)(poSwitchAppRequest));
   delete oMockStub;
}


TEST_F(clContextStub_Test, onSwitchAppRequest_TemporaryContextActive_sendSwitchAppResponseTrue)
{
   clSpy_ContextStub* oMockStub = new clSpy_ContextStub();
   EXPECT_CALL(*oMockStub, vSwitchApp(0x10)).Times(0);
   EXPECT_CALL(*oMockStub, vSwitchApp(0x11)).Times(1);
   EXPECT_CALL(*oMockStub, sendSwitchAppResponse(true)).Times(2);

   RequestContextRequest* poRequest = new RequestContextRequest();
   poRequest->setSourceContext(11);
   poRequest->setTargetContext(21);
   ::std::vector<uint32> availableContexts;
   availableContexts.push_back(20);
   SetAvailableContextsRequest* poAavailableContexts = new SetAvailableContextsRequest();
   poAavailableContexts->setAvailableContext(availableContexts);
   oMockStub->onSetAvailableContextsRequest((const ::boost::shared_ptr< ContextSwitch::SetAvailableContextsRequest >)(poAavailableContexts));

   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest));

   SwitchAppRequest* poSwitchAppRequest1 = new SwitchAppRequest();
   poSwitchAppRequest1->setApplicationId(0x11);
   poSwitchAppRequest1->setPriority(0x80);
   poSwitchAppRequest1->setTargetContext(21);
   oMockStub->onSwitchAppRequest((const ::boost::shared_ptr< ContextSwitch::SwitchAppRequest >)(poSwitchAppRequest1));

   RequestContextRequest* poRequest1 = new RequestContextRequest();
   poRequest1->setSourceContext(10);
   poRequest1->setTargetContext(20);
   availableContexts.clear();
   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest1));

   SwitchAppRequest* poSwitchAppRequest = new SwitchAppRequest();
   poSwitchAppRequest->setApplicationId(0x10);
   poSwitchAppRequest->setPriority(0xFF);
   poSwitchAppRequest->setTargetContext(20);

   oMockStub->onSwitchAppRequest((const ::boost::shared_ptr< ContextSwitch::SwitchAppRequest >)(poSwitchAppRequest));
   delete oMockStub;
}





TEST_F(clContextStub_Test, onSwitchAppRequest_TemporaryContextActiveAndThenTriggerBack_SwitchToNewPermanentApp)
{
   clSpy_ContextStub* oMockStub = new clSpy_ContextStub();
   EXPECT_CALL(*oMockStub, vSwitchApp(0x10)).Times(1);
   EXPECT_CALL(*oMockStub, vSwitchApp(0x11)).Times(1);

   RequestContextRequest* poRequest = new RequestContextRequest();
   poRequest->setSourceContext(11);
   poRequest->setTargetContext(21);
   ::std::vector<uint32> availableContexts;
   availableContexts.push_back(20);
   SetAvailableContextsRequest* poAavailableContexts = new SetAvailableContextsRequest();
   poAavailableContexts->setAvailableContext(availableContexts);
   oMockStub->onSetAvailableContextsRequest((const ::boost::shared_ptr< ContextSwitch::SetAvailableContextsRequest >)(poAavailableContexts));

   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest));

   SwitchAppRequest* poSwitchAppRequest1 = new SwitchAppRequest();
   poSwitchAppRequest1->setApplicationId(0x11);
   poSwitchAppRequest1->setPriority(0x80);
   poSwitchAppRequest1->setTargetContext(21);
   oMockStub->onSwitchAppRequest((const ::boost::shared_ptr< ContextSwitch::SwitchAppRequest >)(poSwitchAppRequest1));

   RequestContextRequest* poRequest1 = new RequestContextRequest();
   poRequest1->setSourceContext(10);
   poRequest1->setTargetContext(20);
   availableContexts.clear();
   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest1));

   SwitchAppRequest* poSwitchAppRequest = new SwitchAppRequest();
   poSwitchAppRequest->setApplicationId(0x10);
   poSwitchAppRequest->setPriority(0xFF);
   poSwitchAppRequest->setTargetContext(20);

   oMockStub->onSwitchAppRequest((const ::boost::shared_ptr< ContextSwitch::SwitchAppRequest >)(poSwitchAppRequest));

   RequestContextRequest* poRequest3 = new RequestContextRequest();
   poRequest3->setSourceContext(0);
   poRequest3->setTargetContext(0);
   availableContexts.clear();
   oMockStub->onRequestContextRequest((const ::boost::shared_ptr< ContextSwitch::RequestContextRequest >)(poRequest3));

   delete oMockStub;
}
