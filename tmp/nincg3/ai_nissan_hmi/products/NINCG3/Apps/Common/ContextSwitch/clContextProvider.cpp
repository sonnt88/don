///////////////////////////////////////////////////////////
//  clContextProvider.cpp
//  Implementation of the Class clContextProvider
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#include "ProjectBaseTypes.h"
#include "clContextProvider.h"
#include "proxy/clContextProxy.h"
#include "clContext.h"

using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;

#include "hmi_trace_if.h"
#include "../Common_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_CONTEXTSWITCH
#include "trcGenProj/Header/clContextProvider.cpp.trc.h"
#endif


clContextProvider::clContextProvider()
   : frontContextId(0)
   , bPendingContextSwitch(false)
{
}


clContextProvider::~clContextProvider()
{
}


void clContextProvider::vInit()
{
   vSendAvailableContexts();
}


void clContextProvider::vSendAvailableContexts()
{
   clContextProxy::instance()->vSetAvailableContexts(_myAvailableContexts);
}


void clContextProvider::vOnNewContext(uint32 sourceContextId, uint32 targetContextId)
{
   if (bIsMyContext(targetContextId))
   {
      ETG_TRACE_USR4(("PROVIDER:   vOnNewContext      sourceContextId : %d ", ETG_CENUM(enContextId, sourceContextId)));
      ETG_TRACE_USR4(("PROVIDER:   vOnNewContext      targetContextId : %d ", ETG_CENUM(enContextId, targetContextId)));
      vStoreContext(sourceContextId, targetContextId);
      if (!bOnNewContext(sourceContextId, targetContextId))
      {
         clContextProxy::instance()->requestContextAck(targetContextId, ContextState__REJECTED);
      }
   }
}


bool clContextProvider::bIsMyContext(unsigned short contextId)
{
   for (int u32Index = 0; u32Index < _myAvailableContexts.size(); u32Index++)
   {
      if (_myAvailableContexts[u32Index].id == contextId)
      {
         return true;
      }
   }
   return false;
}


clContext clContextProvider::getContext(unsigned short contextId)
{
   for (int u32Index = 0; u32Index < _myAvailableContexts.size(); u32Index++)
   {
      if (_myAvailableContexts[u32Index].id == contextId)
      {
         return _myAvailableContexts[u32Index];
      }
   }
   clContext oContext;
   return oContext;
}


uint32 clContextProvider::getPriority(unsigned short contextId)
{
   for (int u32Index = 0; u32Index < _myAvailableContexts.size(); u32Index++)
   {
      if (_myAvailableContexts[u32Index].id == contextId)
      {
         return _myAvailableContexts[u32Index].priority;
      }
   }
   return 255;
}


void clContextProvider::vSwitchBack()
{
   ETG_TRACE_USR4(("PROVIDER:   vSwitchBack        backContextId : %d ", ETG_CENUM(enContextId, backContext.sourceContextId)));

   if ((frontContextId == backContext.targetContextId) || (frontContextId == 0))
   {
      clContextProxy::instance()->vRequestContext(0, backContext.sourceContextId);
   }
   else
   {
      clContextProxy::instance()->vRequestContext(0, 0);   // if back from application and not the same context request 0
   }
}


bool clContextProvider::bOnNewContext(uint32 /*sourceContextId*/, uint32 /*targetContextId*/)
{
   return true;
}


void clContextProvider::vStoreContext(uint32 sourceContextId, uint32 targetContextId)
{
   if (!(backContext.sourceContextId != 0 && sourceContextId == 0))
   {
      backContext.sourceContextId = sourceContextId;
      backContext.targetContextId = targetContextId;
   }
   frontContextId = targetContextId;
   ETG_TRACE_USR4(("PROVIDER:   vStoreContext      backContextId  : %d ", ETG_CENUM(enContextId, backContext.sourceContextId)));
   ETG_TRACE_USR4(("PROVIDER:   vStoreContext      frontContextId : %d ", ETG_CENUM(enContextId, frontContextId)));
}


void clContextProvider::vOnNewActiveContext(uint32 contextId, ContextState enState)
{
   if (bIsMyRequest(contextId))
   {
      vOnBackRequestResponse(enState);
   }
   else if (bIsMyContext(contextId) && frontContextId == 0)
   {
      frontContextId = contextId;
   }
}


bool clContextProvider::bIsMyRequest(uint32 contextId)
{
   if (contextId == backContext.sourceContextId && contextId != 0)
   {
      return true;
   }
   return false;
}


void clContextProvider::vOnBackRequestResponse(ContextState /*enState */)
{
}


void clContextProvider::vOnSwitchAppResponse(uint32 bAppSwitched)
{
   if (!bAppSwitched)
   {
      clContextProxy::instance()->requestContextAck(frontContextId, ContextState__REJECTED);
   }
}


void clContextProvider::storeAvailableContexts(::std::vector<clContext> availableContexts)
{
   _myAvailableContexts = availableContexts;
}


void clContextProvider::vOnNewContextRequestState(uint32 /*contextId*/, ContextState enState)
{
   ETG_TRACE_USR4(("PROVIDER:   vOnNewContextRequestState   ContextId  : %d ", ETG_CENUM(enContextId, frontContextId)));
   ETG_TRACE_USR4(("PROVIDER:   vOnNewContextRequestState ContextState : %d ", ETG_CENUM(ContextState, enState)));
   clContextProxy::instance()->requestContextAck(frontContextId, enState);
   if (enState == ContextState__ACCEPTED)
   {
      if (bIsRequiredViewRendered())
      {
         vSwitchContext();
      }
      else
      {
         bPendingContextSwitch = true;
      }
   }
   else if ((enState == ContextState__COMPLETED) && (frontContextId ==  backContext.targetContextId || frontContextId == 0))
   {
      backContext.sourceContextId = 0;
      backContext.targetContextId = 0;
   }
   else if (enState == ContextState__COMPLETED)
   {
      frontContextId = 0;
   }
}


void clContextProvider::vAddAvailableContext(uint32 contextId, enContextType type, uint32 surfaceId, ::std::string viewName)
{
   if (!bIsContextAvailable(contextId))
   {
      clContext context;
      context.id = contextId;
      context.type = type;
      context.priority = 0xFF;
      context.surfaceId = surfaceId;
      context.viewName = viewName;
      context.sourceId = SRCID_INVALID;
      _myAvailableContexts.push_back(context);
      vSendAvailableContexts();
   }
}


void clContextProvider::vRemoveContext(uint32 contextId)
{
   clContextProxy::instance()->vRemoveContext(contextId);
   vRemoveMyContext(contextId);
}


bool clContextProvider::bIsContextAvailable(uint32 contextId)
{
   for (uint32 index = 0; index < _myAvailableContexts.size(); index++)
   {
      if (_myAvailableContexts[index].id == contextId)
      {
         return true;
      }
   }
   return false;
}


void clContextProvider::vRemoveMyContext(uint32 contextId)
{
   ::std::vector<clContext>::iterator iter = _myAvailableContexts.begin();
   for (; iter != _myAvailableContexts.end();)
   {
      if (iter->id == contextId)
      {
         iter = _myAvailableContexts.erase(iter);
      }
      else
      {
         iter++;
      }
   }
}


void clContextProvider::vClearContexts()
{
   //frontContextId = 0;
   //backContext.sourceContextId = 0;
   //backContext.targetContextId = 0;
}


bool clContextProvider::bIsRequiredViewRendered()
{
   if ((_activeRenderedView.find(getSurfaceId(frontContextId)) != _activeRenderedView.end() && _activeRenderedView[getSurfaceId(frontContextId)] == getViewName(frontContextId)) || frontContextId == 0 || getViewName(frontContextId) == "")
   {
      return true;
   }
   return false;
}


void  clContextProvider::vOnActiveRenderedView(::std::string viewName, uint32 surfaceId)
{
   ETG_TRACE_USR4(("PROVIDER:   vOnActiveRenderedView   ContextId  : %d ", ETG_CENUM(enContextId, frontContextId)));
   ETG_TRACE_USR4(("PROVIDER:   vOnActiveRenderedView   viewName   : %s ", viewName.c_str()));
   ETG_TRACE_USR4(("PROVIDER:   vOnActiveRenderedView   surfaceId  : %d ", surfaceId));

   _activeRenderedView[surfaceId] = viewName;

   if (bPendingContextSwitch && bIsRequiredViewRendered())
   {
      vSwitchContext();
   }
}


void  clContextProvider::vSwitchContext()
{
   clContextProxy::instance()->vSwitchContext(frontContextId, getPriority(frontContextId));
   bPendingContextSwitch = false;
}


::std::string clContextProvider::getViewName(uint32 contextId)
{
   for (uint32 index = 0; index < _myAvailableContexts.size(); index++)
   {
      if (_myAvailableContexts[index].id == contextId)
      {
         return _myAvailableContexts[index].viewName;
      }
   }
   return "";
}


uint32 clContextProvider::getSurfaceId(uint32 contextId)
{
   for (uint32 index = 0; index < _myAvailableContexts.size(); index++)
   {
      if (_myAvailableContexts[index].id == contextId)
      {
         return _myAvailableContexts[index].surfaceId;
      }
   }
   return 0;
}
