///////////////////////////////////////////////////////////
//  ContextType.h
//  Implementation of the Class ContextType
//  Created on:      13-Jul-2015 11:45:15 AM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(EA_307A928A_6BC3_47e7_B61B_4A2BD495AA9D__INCLUDED_)
#define EA_307A928A_6BC3_47e7_B61B_4A2BD495AA9D__INCLUDED_

/**
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 11:45:15 AM
 */
enum ContextType
{
   TEMPORARY = 0,
   PERMANENT = 1
};
#endif // !defined(EA_307A928A_6BC3_47e7_B61B_4A2BD495AA9D__INCLUDED_)
