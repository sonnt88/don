/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypes.h"

/**
 * todo
 */

namespace bosch
{
namespace cm
{
namespace ai
{
namespace nissan
{
namespace hmi
{
namespace contextswitchservice
{
namespace ContextSwitchTypes
{


} // namespace ContextSwitchTypes
} // namespace contextswitchservice
} // namespace hmi
} // namespace nissan
} // namespace ai
} // namespace cm
} // namespace bosch
