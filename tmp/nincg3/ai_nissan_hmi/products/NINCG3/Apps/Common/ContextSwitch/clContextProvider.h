///////////////////////////////////////////////////////////
//  clContextProvider.h
//  Implementation of the Class clContextProvider
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(clContextProvider_h)
#define clContextProvider_h

/**
 * interface class for context proxies to provide contexts to requestors
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 5:44:42 PM
 */

#include "clContextProviderInterface.h"
#include "clContext.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"
#include "asf/core/Types.h"
#include <vector>
#include <map>


using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;


class clContextProvider : public clContextProviderInterface
{
   public:
      clContextProvider();
      virtual ~clContextProvider();

      /**
       * Initialize operation for context provider
       */
      void vInit();
      /**
       *
       * @param targetContext
       */

      virtual void vOnNewContext(uint32 sourceContextId, uint32 targetContextId);
      virtual void vOnNewActiveContext(uint32 contextId, ContextState enState);
      virtual void vOnSwitchAppResponse(uint32 bAppSwitched);
      virtual void vClearContexts();

      void storeAvailableContexts(::std::vector<clContext> availableContexts);
      void vOnNewContextRequestState(uint32 contextId, ContextState enState);
      void vOnActiveRenderedView(::std::string viewName, uint32 surfaceId = 0);

      void vAddAvailableContext(uint32 contextId, enContextType type = DYNAMIC, uint32 surfaceId = 0, ::std::string viewName = "");
      void vRemoveContext(uint32 contextId);

      struct contextPair
      {
         contextPair() : sourceContextId(0), targetContextId(0) {};
         uint32 sourceContextId;
         uint32 targetContextId;
      };

   protected:
      /**
       *
       * @param sourceContext
       * @param targetContext
       */

      void vSwitchBack();

      virtual bool bOnNewContext(uint32 sourceContextId, uint32 targetContextId);
      void vStoreContext(uint32 sourceContextId, uint32 targetContextId);
      virtual void vOnBackRequestResponse(ContextState enState);

      inline uint32 getFrontContext()
      {
         return frontContextId;
      }
      contextPair backContext;

   private:

      bool bIsMyContext(unsigned short contextId);
      clContext getContext(unsigned short contextId);
      uint32 getPriority(unsigned short contextId);
      void vOnNewContext(unsigned short sourceContextId, unsigned short targetContextId, void* contextData);
      bool bIsMyRequest(uint32 contextId);
      void vSendAvailableContexts();
      bool bIsContextAvailable(uint32 contextId);
      void vRemoveMyContext(uint32 contextId);
      void vSwitchContext();
      ::std::string getViewName(uint32 contextId);
      uint32 getSurfaceId(uint32 contextId);
      bool bIsRequiredViewRendered();

      uint32 frontContextId;
      ::std::vector<clContext> _myAvailableContexts;
      ::std::map<uint32, ::std::string>_activeRenderedView;
      bool bPendingContextSwitch;
};


#endif // !defined(EA_FAF598AF_B4F9_4bb1_91CE_ABBA6F93217B__INCLUDED_)
