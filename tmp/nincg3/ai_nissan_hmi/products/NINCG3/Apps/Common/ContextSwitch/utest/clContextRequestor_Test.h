/**************************************************************************//**
* \file     clContextRequestor_Test.h
*
*
* \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
******************************************************************************/
#ifndef clContextRequestor_Test_h
#define clContextRequestor_Test_h

#include "gmock/gmock.h"
#include "gtest/gtest.h"

class clContextRequestor_Test : public testing::Test
{
   public:
      clContextRequestor_Test()
      {

      }
      virtual ~clContextRequestor_Test()
      {

      }
      void SetUp()
      {
      }
      void TearDown()
      {
      }
};

#endif // clContextRequestor_Test_h
