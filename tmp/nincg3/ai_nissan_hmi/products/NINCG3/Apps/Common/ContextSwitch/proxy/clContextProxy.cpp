///////////////////////////////////////////////////////////
//  clContextProxy.cpp
//  Implementation of the Class clContextProxy
//  Created on:      13-Jul-2015 11:44:47 AM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#include "clContextProxy.h"
#include "clContextProviderInterface.h"
#include "clContextRequestorInterface.h"
#include "hmi_trace_if.h"
#include "../Common_Trace.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_CONTEXTSWITCH
#include "trcGenProj/Header/clContextProxy.cpp.trc.h"
#endif


using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch;
using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;

clContextProxy* clContextProxy::m_poProxy = NULL;


clContextProxy::clContextProxy()
   : _contextSwitchProxy(ContextSwitchProxy::createProxy("contextSwitchPort", *this))
   , _applicationId(0)
{
   StartupSync::getInstance().registerPropertyRegistrationIF(this);
}


clContextProxy::~clContextProxy()
{
}


clContextProxy* clContextProxy::instance()
{
   if (clContextProxy::m_poProxy == NULL)
   {
      clContextProxy::m_poProxy = new clContextProxy();
   }
   return clContextProxy::m_poProxy;
}


void clContextProxy::vSetProviderImpl(clContextProviderInterface* _poProviderImpl)
{
   _contextProviderImpl = _poProviderImpl;
}


void clContextProxy::vSetRequestorImpl(clContextRequestorInterface* _poRequestorImpl)
{
   _contextRequestorImpl = _poRequestorImpl;
}


void clContextProxy::vRequestContext(uint32 sourceContextId, uint32 targetContextId)
{
   ETG_TRACE_USR1(("PROXY:   vRequestContext      _applicationId   : %d ", ETG_CENUM(enApplicationId, (tU8)_applicationId)));
   ETG_TRACE_USR1(("PROXY:   vRequestContext      sourceContextId  : %d ", ETG_CENUM(enContextId, sourceContextId)));
   ETG_TRACE_USR1(("PROXY:   vRequestContext      targetContextId  : %d ", ETG_CENUM(enContextId, targetContextId)));
   _contextSwitchProxy->sendRequestContextRequest(*this, sourceContextId, targetContextId);
}


void clContextProxy::vClearContexts()
{
   ETG_TRACE_USR1(("PROXY:   vClearContexts      _applicationId   : %d ", ETG_CENUM(enApplicationId, (tU8)_applicationId)));
   _contextSwitchProxy->sendClearContextStackRequest(*this, 0);
}


void clContextProxy::vSwitchContext(uint32 contextId, uint32 priority)
{
   ETG_TRACE_USR1(("PROXY:   vSwitchContext      _applicationId   : %d ", ETG_CENUM(enApplicationId, (tU8)_applicationId)));
   ETG_TRACE_USR1(("PROXY:   vSwitchContext       contextId       : %d ", ETG_CENUM(enContextId, contextId)));
   ETG_TRACE_USR1(("PROXY:   vSwitchContext       priority        : %d ", priority));
   _contextSwitchProxy->sendSwitchAppRequest(*this, _applicationId, priority, contextId);
}


void clContextProxy::requestContextAck(uint32 contextId, ContextState enState)
{
   ETG_TRACE_USR1(("PROXY:   requestContextAck      _applicationId   : %d ", ETG_CENUM(enApplicationId, (tU8)_applicationId)));
   ETG_TRACE_USR1(("PROXY:   requestContextAck       contextId       : %d ", ETG_CENUM(enContextId, contextId)));
   ETG_TRACE_USR1(("PROXY:   requestContextAck       ContextState    : %d ", ETG_CENUM(ContextState, (tU8)enState)));
   _contextSwitchProxy->sendRequestContextAckRequest(*this, enState, contextId);
}


void clContextProxy::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if ((_contextSwitchProxy) && (proxy == _contextSwitchProxy))
   {
      _contextSwitchProxy->sendActiveContextRegister(*this);
      _contextSwitchProxy->sendVOnNewContextRegister(*this);
      _contextSwitchProxy->sendAvailableContextsRegister(*this);
      _contextSwitchProxy->sendClearContextsRegister(*this);
      _contextProviderImpl->vInit();
   }
}


void clContextProxy::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if ((_contextSwitchProxy) && (proxy == _contextSwitchProxy))
   {
      _contextSwitchProxy->sendVOnNewContextDeregisterAll();
      _contextSwitchProxy->sendActiveContextDeregisterAll();
      _contextSwitchProxy->sendAvailableContextsDeregisterAll();
      _contextSwitchProxy->sendClearContextsDeregisterAll();
   }
}


void clContextProxy::vSetAvailableContexts(::std::vector<clContext> & availableContexts)
{
   ::std::vector<ContextInfo> contexts;
   for (uint32 index = 0; index <  availableContexts.size(); index++)
   {
      ContextInfo context(availableContexts[index].id, (ContextType) availableContexts[index].type, availableContexts[index].priority, availableContexts[index].surfaceId, availableContexts[index].sourceId);
      contexts.push_back(context);
   }
   _contextSwitchProxy->sendSetAvailableContextsRequest(*this, contexts);
}


void clContextProxy::onActiveContextSignal(const ::boost::shared_ptr< ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< ContextSwitch::ActiveContextSignal >& signal)
{
   ETG_TRACE_USR1(("PROXY:   onActiveContextSignal      _applicationId   : %d ", ETG_CENUM(enApplicationId, (tU8)_applicationId)));
   ETG_TRACE_USR1(("PROXY:   onActiveContextSignal       contextId       : %d ", ETG_CENUM(enContextId, signal->getContext())));
   ETG_TRACE_USR1(("PROXY:   onActiveContextSignal       ContextState    : %d ", ETG_CENUM(ContextState, (tU8)signal->getContextStatus())));
   ETG_TRACE_USR1(("PROXY:   onActiveContextSignal       ContextType    : %d ",  ETG_CENUM(ContextType, (tU8)signal->getType())));
   _contextRequestorImpl->vOnNewActiveContext(signal->getContext(), signal->getContextStatus(), signal->getType());
   _contextProviderImpl->vOnNewActiveContext(signal->getContext(), signal->getContextStatus());
}


void clContextProxy::onRequestContextResponse(const ::boost::shared_ptr< ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< RequestContextResponse >& response)
{
   _contextRequestorImpl->vOnRequestContextResponse(response->getBValidContext());
}


void clContextProxy::onSwitchAppResponse(const ::boost::shared_ptr< ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< SwitchAppResponse >& response)
{
   _contextProviderImpl->vOnSwitchAppResponse(response->getBAppSwitched());
}


void clContextProxy::onVOnNewContextSignal(const ::boost::shared_ptr< ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< VOnNewContextSignal >& signal)
{
   _contextProviderImpl->vOnNewContext(signal->getSourceContext(), signal->getTargetContext());
}


void clContextProxy::onAvailableContextsSignal(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< ContextSwitch::AvailableContextsSignal >& signal)
{
   _contextRequestorImpl->vOnAvailableContexts(signal->getAvailableContext());
}


void clContextProxy::onClearContextsSignal(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< ContextSwitch::ClearContextsSignal >& signal)
{
   _contextProviderImpl->vClearContexts();
}


void clContextProxy::onRequestContextAckResponse(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< ContextSwitch::RequestContextAckResponse >& /*response*/)
{
}


void clContextProxy::vSetApplication(uint32 appId)
{
   _applicationId = appId;
}


void clContextProxy::vRemoveContext(uint32 contextId)
{
   _contextSwitchProxy->sendRemoveContextRequest(*this, contextId);
}


void clContextProxy::onActiveContextError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< ContextSwitch::ActiveContextError >& /*error*/)
{
   ETG_TRACE_FATAL(("PROXY:   onActiveContextError !!! "));
}


void clContextProxy::onAvailableContextsError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< ContextSwitch::AvailableContextsError >& /*error*/)
{
   ETG_TRACE_FATAL(("PROXY:   onAvailableContextsError !!! "));
}


void clContextProxy::onRemoveContextError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< ContextSwitch::RemoveContextError >& /*error*/)
{
   ETG_TRACE_FATAL(("PROXY:   onRemoveContextError !!! "));
}


void clContextProxy::onRequestContextAckError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< ContextSwitch::RequestContextAckError >& /*error*/)
{
   ETG_TRACE_FATAL(("PROXY:   onRequestContextAckError !!! "));
}


void clContextProxy::onRequestContextError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< ContextSwitch::RequestContextError >& /*error*/)
{
   ETG_TRACE_FATAL(("PROXY:   onRequestContextError !!! "));
}


void clContextProxy::onSetAvailableContextsError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< ContextSwitch::SetAvailableContextsError >& /*error*/)
{
   ETG_TRACE_FATAL(("PROXY:   onSetAvailableContextsError !!! "));
}


void clContextProxy::onSwitchAppError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< ContextSwitch::SwitchAppError >& /*error*/)
{
   ETG_TRACE_FATAL(("PROXY:   onSwitchAppError !!! "));
}


void clContextProxy::onVOnNewContextError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< ContextSwitch::VOnNewContextError >& /*error*/)
{
   ETG_TRACE_FATAL(("PROXY:   onVOnNewContextError !!! "));
}


void clContextProxy::onClearContextsError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< ContextSwitch::ClearContextsError >& /*error*/)
{
   ETG_TRACE_FATAL(("PROXY:   onClearContextsError !!! "));
}


void clContextProxy::onClearContextStackError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< ContextSwitch::ClearContextStackError >& /*error*/)
{
   ETG_TRACE_FATAL(("PROXY:   onClearContextStackError !!! "));
}


void clContextProxy::onClearContextStackResponse(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& /*proxy*/, const ::boost::shared_ptr< ContextSwitch::ClearContextStackResponse >& /*response*/)
{
   ETG_TRACE_USR4(("PROXY:   onClearContextStackResponse !!! "));
}
