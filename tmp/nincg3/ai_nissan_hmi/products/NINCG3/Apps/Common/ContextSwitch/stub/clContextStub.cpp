///////////////////////////////////////////////////////////
//  clContextStub.cpp
//  Implementation of the Class clContextStub
//  Created on:      13-Jul-2015 11:44:47 AM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#include "clContextStub.h"
#include "clContextProviderInterface.h"
#include "clContextRequestorInterface.h"
#include "ProjectBaseTypes.h"
#include "hmi_trace_if.h"
#include "../Common_Trace.h"
#include "AppUtils/Ticker.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_CONTEXTSWITCH
#include "trcGenProj/Header/clContextStub.cpp.trc.h"
#endif


#define NORMAL_PRIORITY 0xFF


using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch;
using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;

clContextStub* clContextStub::m_poStub = NULL;


clContextStub::clContextStub()
   : ContextSwitchStub("contextSwitchServerPort")
{
   clContextStub::m_poStub = this;
   lastNonContextApplication = 0;
}


clContextStub::~clContextStub()
{
   clContextStub::m_poStub = 0;
}


clContextStub* clContextStub::instance()
{
   if (clContextStub::m_poStub == NULL)
   {
      clContextStub::m_poStub = new clContextStub();
   }
   return clContextStub::m_poStub;
}


void clContextStub::onRequestContextRequest(const ::boost::shared_ptr< RequestContextRequest >& request)
{
   ETG_TRACE_USR4(("CONTEXTSWITCH: PERFORMANCE  Request for context switch  received at Master  %u ms", hmibase::utils::Ticker::getTickCountMsec()));

   ETG_TRACE_USR1(("STUB:   onRequestContextRequest       sourceContext     : %d ", ETG_CENUM(enContextId, request->getSourceContext())));
   ETG_TRACE_USR1(("STUB:   onRequestContextRequest       targetContext     : %d ", ETG_CENUM(enContextId, request->getTargetContext())));
   if (bIsConditionToActivateLastApp(request->getSourceContext(), request->getTargetContext()))
   {
      vSwitchToLastContext();
   }
   else if (bIsAvailableContext(request->getTargetContext()))
   {
      sendRequestContextResponse(true);
      sendVOnNewContextSignal(request->getSourceContext(), request->getTargetContext());
      ETG_TRACE_USR4(("CONTEXTSWITCH: PERFORMANCE  Draw requested to application  %u ms", hmibase::utils::Ticker::getTickCountMsec()));
   }
   else
   {
      sendRequestContextResponse(false);
   }
}


void clContextStub::onSetAvailableContextsRequest(const ::boost::shared_ptr< SetAvailableContextsRequest >& request)
{
   bool contextsModified = false;
   for (uint32 index = 0; index < request->getAvailableContext().size(); index++)
   {
      if (! bIsAvailableContext(request->getAvailableContext()[index]))
      {
         contextsModified = true;
         ETG_TRACE_USR1(("STUB:   onSetAvailableContextsRequest       Adding context     : %d ", ETG_CENUM(enContextId, request->getAvailableContext()[index].getContextId())));
         _availableContexts.push_back(request->getAvailableContext()[index]);
      }
   }
   if (contextsModified)
   {
      ::std::vector<uint32> contexts;
      for (uint32 index = 0; index < _availableContexts.size(); index ++)
      {
         contexts.push_back(_availableContexts[index].getContextId());
      }
      sendAvailableContextsSignal(contexts);
   }
}


void clContextStub::onSwitchAppRequest(const ::boost::shared_ptr< SwitchAppRequest >& request)
{
   ETG_TRACE_USR4(("CONTEXTSWITCH: PERFORMANCE  Draw completed from application  %u ms", hmibase::utils::Ticker::getTickCountMsec()));
   ETG_TRACE_USR1(("STUB:   onSwitchAppRequest       applicationId     : %d ", ETG_CENUM(enApplicationId, request->getApplicationId())));
   ETG_TRACE_USR1(("STUB:   onSwitchAppRequest       targetContext     : %d ", ETG_CENUM(enContextId, request->getTargetContext())));
   ETG_TRACE_USR1(("STUB:   onSwitchAppRequest       priority          : %d ", request->getPriority()));

   if (!bIsActiveContextRequested(request->getTargetContext()))
   {
      vStoreLastContext(_activeAppContext);
   }
   sendSwitchAppResponse(true);
   vSwitchApp(request->getTargetContext(), request->getApplicationId(), request->getPriority(), true, getContextType(request->getTargetContext()),  GetSourceofContext(request->getTargetContext()));
   vStoreActiveContext(request);
}


void clContextStub::onRequestContextAckRequest(const ::boost::shared_ptr< RequestContextAckRequest >& request)
{
   sendActiveContextSignal(request->getTargetContext(), request->getContextStatus(),  getContextType(request->getTargetContext()));
   stContext contextState;
   contextState.contextId = request->getTargetContext();
   contextState.contextStatus = request->getContextStatus();
   contextState.contextType = getContextType(request->getTargetContext());
   vOnNewContextState(contextState);
}


void clContextStub::onClearContextStackRequest(const ::boost::shared_ptr< ClearContextStackRequest >& /*request*/)
{
   clearContexts();
}


void clContextStub::onRemoveContextRequest(const ::boost::shared_ptr< RemoveContextRequest >& request)
{
   if (bIsAvailableContext(request->getContextId()))
   {
      ETG_TRACE_USR1(("STUB:   onRemoveContextRequest       Removing context     : %d ", ETG_CENUM(enContextId, request->getContextId())));
      vRemoveContext(request->getContextId());
      ::std::vector<uint32> contexts;
      for (uint32 index = 0; index < _availableContexts.size(); index ++)
      {
         contexts.push_back(_availableContexts[index].getContextId());
      }
      sendAvailableContextsSignal(contexts);
   }
}


bool clContextStub::bIsAvailableContext(ContextInfo targetContextId)
{
   for (uint32 index = 0; index < _availableContexts.size(); index++)
   {
      if (_availableContexts[index].getContextId() == targetContextId.getContextId())
      {
         return true;
      }
   }
   return false;
}


void clContextStub::vRemoveContext(uint32 targetContextId)
{
   ::std::vector<ContextInfo>::iterator iter = _availableContexts.begin();
   for (; iter != _availableContexts.end(); iter++)
   {
      if (iter->getContextId() == targetContextId)
      {
         _availableContexts.erase(iter);
         break;
      }
   }
}


bool clContextStub::bIsAvailableContext(uint32 contextId)
{
   for (uint32 index = 0; index < _availableContexts.size(); index++)
   {
      if (_availableContexts[index].getContextId() == contextId)
      {
         return true;
      }
   }
   return false;
}


ContextType clContextStub::getContextType(uint32 contextId)
{
   for (uint32 index = 0; index < _availableContexts.size(); index++)
   {
      if (_availableContexts[index].getContextId() == contextId)
      {
         return _availableContexts[index].getType();
      }
   }
   return ContextType__STATIC;
}


bool clContextStub::bIsOkToSwitchApp(uint32 /*appId*/, uint32 priority, uint32 /*targetContextId*/)
{
   if (_activeAppContext)
   {
      if (priority <= _activeAppContext->getPriority())
      {
         return true;
      }
      return false;
   }
   return true;
}


bool clContextStub::bIsConditionToActivateLastApp(uint32 sourceContextId, uint32 targetContextId)
{
   if (targetContextId == 0 && sourceContextId == 0)
   {
      return true;
   }
   return false;
}


void clContextStub::vStoreActiveContext(const ::boost::shared_ptr< SwitchAppRequest >& request)
{
   if (contextStack.size())
   {
      ::boost::shared_ptr< SwitchAppRequest > appContext ;
      appContext = contextStack.back();
      if (appContext->getPriority() >= request->getPriority())
      {
         _activeAppContext = request;
      }
      else
      {
         _activeAppContext = appContext;
         contextStack.pop_back();
         vAddToStack(request);
      }
   }
   else
   {
      _activeAppContext = request;
   }
}


void clContextStub::vAddToStack(const ::boost::shared_ptr< SwitchAppRequest >& request)
{
   bool bAdded = false;
   ::std::vector<boost::shared_ptr< SwitchAppRequest >  >::iterator iter;
   for (iter = contextStack.begin(); iter != contextStack.end(); iter++)
   {
      if (request->getPriority() > (*iter)->getPriority())
      {
         bAdded = true;
         contextStack.insert(iter, request);
         ETG_TRACE_USR1(("STUB:   vAddToStack       Element added at  : %d ", iter - contextStack.begin()));
         break;
      }
   }
   if (!bAdded)
   {
      contextStack.push_back(request);
      ETG_TRACE_USR1(("STUB:   vAddToStack       Element added at  : %d ", iter - contextStack.begin()));
   }
   ETG_TRACE_USR1(("STUB:   vAddToStack       applicationId     : %d ", ETG_CENUM(enApplicationId, request->getApplicationId())));
   ETG_TRACE_USR1(("STUB:   vAddToStack       targetContext     : %d ", ETG_CENUM(enContextId, request->getTargetContext())));
   ETG_TRACE_USR1(("STUB:   vAddToStack       priority          : %d ", request->getPriority()));
}


bool clContextStub::bIsLowerPriority(boost::shared_ptr< SwitchAppRequest >  currentContext, boost::shared_ptr< SwitchAppRequest >  nextContext)
{
   return currentContext->getPriority() > nextContext->getPriority();
}


void clContextStub::vStoreLastContext(const ::boost::shared_ptr< SwitchAppRequest >& request)
{
   if (request && request->getPriority() == NORMAL_PRIORITY && !bIsTemporaryContext(request->getTargetContext()))
   {
      //ETG_TRACE_USR4(("STUB:   vStoreLastContext      request->getApplicationId() : %d ", ETG_CENUM(enApplicationId, (tU8)request->getApplicationId())));
      //ETG_TRACE_USR4(("STUB:   vStoreLastContext      request->getTargetContext() : %d ", ETG_CENUM(enContextId, request->getTargetContext())));
      _lastAppContext = request;
   }
   if (request)
   {
      ETG_TRACE_USR4(("STUB:   vStoreLastContext      contextStack -> App Id     : %d ", ETG_CENUM(enApplicationId, (tU8)request->getApplicationId())));
      ETG_TRACE_USR4(("STUB:   vStoreLastContext      contextStack -> Context Id : %d ", ETG_CENUM(enContextId, request->getTargetContext())));
      contextStack.push_back(request);
   }
}


void clContextStub::vSwitchToLastContext()
{
   ETG_TRACE_USR4(("STUB:   vSwitchToLastContext       lastNonContextApplication          : %d ", ETG_CENUM(enApplicationId, lastNonContextApplication)));
   if (contextStack.size())
   {
      ::boost::shared_ptr< SwitchAppRequest > appContext ;
      appContext = contextStack.back();
      contextStack.pop_back();
      ETG_TRACE_USR4(("STUB:   vSwitchToLastContext      contextStack -> App Id     : %d ", ETG_CENUM(enApplicationId, (tU8)appContext->getApplicationId())));
      ETG_TRACE_USR4(("STUB:   vSwitchToLastContext      contextStack -> Context Id : %d ", ETG_CENUM(enContextId, appContext->getTargetContext())));

      sendActiveContextSignal(appContext->getTargetContext(), ContextState__ACCEPTED, getContextType(appContext->getTargetContext()));
      _activeAppContext = appContext;
      vSwitchApp(appContext->getTargetContext(), appContext->getApplicationId(), appContext->getPriority(), false , getContextType(appContext->getTargetContext()), GetSourceofContext(appContext->getTargetContext()));
   }
   else
   {
      sendActiveContextSignal(0, ContextState__ACCEPTED, ContextType__STATIC);
      sendActiveContextSignal(0, ContextState__COMPLETED, ContextType__STATIC);
      vSwitchApp(0, lastNonContextApplication, 0xff, false, ContextType__STATIC, SRCID_INVALID);
      lastNonContextApplication = 0;
   }
}


void clContextStub::vStoreAppToSwitch(::boost::shared_ptr< SwitchAppRequest > switchAppData)
{
   if (switchAppData)
   {
      ETG_TRACE_USR4(("STUB:   vStoreAppToSwitch      switchAppData->getApplicationId() : %d ", ETG_CENUM(enApplicationId, (tU8)switchAppData->getApplicationId())));
      ETG_TRACE_USR4(("STUB:   vStoreAppToSwitch      switchAppData->getContextId()     : %d ", ETG_CENUM(enContextId, switchAppData->getTargetContext())));
      ETG_TRACE_USR4(("STUB:   vStoreAppToSwitch      surface                           : %d ", getSurfaceOfContext(switchAppData->getTargetContext())));

      if (switchAppData->getTargetContext() != 0 && getSurfaceOfContext(switchAppData->getTargetContext()) != 0)
      {
         _contextToSwitch = switchAppData;
      }
      else
      {
         vSwitchApp(switchAppData->getTargetContext(), switchAppData->getApplicationId(), switchAppData->getPriority(), true, getContextType(switchAppData->getTargetContext()),  GetSourceofContext(switchAppData->getTargetContext()));
      }
   }
}


void clContextStub::vOnNewSurfaceRendered(uint32 surfaceId)
{
   ETG_TRACE_USR4(("STUB:   vOnNewSurfaceRendered      surfaceId->getApplicationId() : %d ", surfaceId));
   if (_contextToSwitch && getSurfaceOfContext(_contextToSwitch->getTargetContext()) == surfaceId)
   {
      ETG_TRACE_USR4(("STUB:   vOnNewSurfaceRendered      getSurfaceOfContext     : %d ", getSurfaceOfContext(_contextToSwitch->getTargetContext())));
      vSwitchApp(_contextToSwitch->getTargetContext(), _contextToSwitch->getApplicationId(), _contextToSwitch->getPriority(), true, getContextType(_contextToSwitch->getTargetContext()), GetSourceofContext(_contextToSwitch->getTargetContext()));
      _contextToSwitch.reset();
   }
}


void clContextStub::vSwitchApp(uint32 /*appId*/)
{
}


void clContextStub::vOnContextClosed(uint32 /*contextId*/)
{
}


void clContextStub::vSwitchApp(uint32 /*contextId*/, uint32 /*appId*/, uint32 /*priority*/, bool /*bIsForwardContext*/,  ContextType /*contextType*/, uint32 /*sourceId*/)
{
}


uint32 clContextStub::getSurfaceOfContext(uint32 contextId)
{
   ::std::vector<ContextInfo>::iterator iter =  _availableContexts.begin();
   for (; iter != _availableContexts.end(); iter++)
   {
      if (contextId == iter->getContextId())
      {
         return iter->getSurfaceId();
      }
   }
   return 0;
}


void clContextStub::vOnNewActiveApplication(uint32 appId)
{
   ETG_TRACE_USR4(("CONTEXTSWITCH: PERFORMANCE  Request for context switch  completed at Master  %u ms", hmibase::utils::Ticker::getTickCountMsec()));
   ETG_TRACE_USR4(("STUB:   vOnNewActiveApplication      appId %d ", ETG_CENUM(enApplicationId, (tU8)appId)));
   if ((!_activeAppContext ||  _activeAppContext->getApplicationId() != appId))
   {
      lastNonContextApplication = appId;
   }
}


bool clContextStub::bIsTemporaryContext(uint32 contextId)
{
   ::std::vector<ContextInfo>::iterator iter =  _availableContexts.begin();
   for (; iter != _availableContexts.end(); iter++)
   {
      if (contextId == iter->getContextId() && iter->getType() == ContextType__TEMPORARY)
      {
         return true;
      }
   }
   return false;
}


bool clContextStub::bIsActiveContextRequested(uint32 contextId)
{
   if (_activeAppContext && _activeAppContext->getTargetContext() == contextId)
   {
      return true;
   }
   return false;
}


void clContextStub::clearContexts()
{
   //Clearing the last app context information
   //during app switch via Hard Keys
   _lastAppContext.reset();
   _activeAppContext.reset();
   contextStack.clear();
   sendClearContextsSignal(1);
}


void clContextStub::vOnNewContextState(stContext& context)
{
   if (context.contextStatus == ContextState__REJECTED || context.contextStatus == ContextState__COMPLETED)
   {
      if (bIsContextInStack(context.contextId))
      {
         vRemoveContext(context);
      }
      if (bIsActiveContextRequested(context.contextId))
      {
         _activeAppContext.reset();
      }
      if (context.contextStatus == ContextState__COMPLETED)
      {
         vOnContextClosed(context.contextId);
      }
   }
   else if (context.contextStatus == ContextState__ACCEPTED)
   {
      vOnNewActiveContext(context.contextId, context.contextType);
   }
}


void clContextStub::vOnNewActiveContext(uint32 /*contextId*/, ContextType /*contextType*/)
{
}


bool clContextStub::bIsContextInStack(uint32 contextId)
{
   ::std::vector<boost::shared_ptr< SwitchAppRequest >  >::iterator iter;
   for (iter = contextStack.begin(); iter != contextStack.end(); iter++)
   {
      if ((*iter)->getTargetContext() == contextId)
      {
         return true;
      }
   }
   return false;
}


void clContextStub::vRemoveContext(stContext& context)
{
   ::std::vector<boost::shared_ptr< SwitchAppRequest >  >::iterator iter;
   for (iter = contextStack.begin(); iter != contextStack.end(); iter++)
   {
      if ((*iter)->getTargetContext() == context.contextId)
      {
         iter = contextStack.erase(iter);
         break;
      }
   }
}


bool clContextStub::bIsContextAvailableForSource(uint32 sourceId)
{
   ETG_TRACE_USR4(("STUB:   bIsContextAvailableForSource      sourceId %d ", sourceId));
   ::std::vector<ContextInfo>::iterator iter =  _availableContexts.begin();
   for (; iter != _availableContexts.end(); iter++)
   {
      if (sourceId == iter->getSourceId())
      {
         return true;
      }
   }
   return false;
}


uint32 clContextStub::GetContextOfSource(uint32 sourceId)
{
   ETG_TRACE_USR4(("STUB:   GetContextOfSource      sourceId %d ", sourceId));
   ::std::vector<ContextInfo>::iterator iter =  _availableContexts.begin();
   for (; iter != _availableContexts.end(); iter++)
   {
      if (sourceId == iter->getSourceId())
      {
         ETG_TRACE_USR4(("STUB:   GetContextOfSource      contextId %d ", iter->getContextId()));
         return iter->getContextId();
      }
   }
   return 0;
}


uint32 clContextStub::GetSourceofContext(uint32 contextId)
{
   ETG_TRACE_USR4(("STUB:   GetSourceofContext      contextId %d ", contextId));
   ::std::vector<ContextInfo>::iterator iter =  _availableContexts.begin();
   for (; iter != _availableContexts.end(); iter++)
   {
      if (contextId == iter->getContextId())
      {
         ETG_TRACE_USR4(("STUB:   GetSourceofContext      sourceId %d ", iter->getSourceId()));
         return iter->getSourceId();
      }
   }
   return SRCID_INVALID;
}
