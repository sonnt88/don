/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#ifndef BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHTYPESDBUS_H
#define BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHTYPESDBUS_H

#include <dbus/dbus.h>

#endif // BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHTYPESDBUS_H
