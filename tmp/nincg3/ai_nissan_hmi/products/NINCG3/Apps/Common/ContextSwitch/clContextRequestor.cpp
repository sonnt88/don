///////////////////////////////////////////////////////////
//  clContextRequestor.cpp
//  Implementation of the Class clContextRequestor
//  Created on:      13-Jul-2015 5:44:20 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#include "clContextRequestor.h"
#include "proxy/clContextProxy.h"

#include "hmi_trace_if.h"
#include "../Common_Trace.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_CONTEXTSWITCH
#include "trcGenProj/Header/clContextRequestor.cpp.trc.h"
#endif


clContextRequestor::clContextRequestor()
   : myContextId(0)
   , requestedContextId(0)
{
}


clContextRequestor::~clContextRequestor()
{
}


void clContextRequestor::vInit()
{
}


void clContextRequestor::vRequestContext(uint32 sourceContextId, uint32 targetContextId)
{
   ETG_TRACE_USR4(("REQUESTOR:   vRequestContext     SourceContextID : %d ", ETG_CENUM(enContextId, sourceContextId)));
   ETG_TRACE_USR4(("REQUESTOR:   vRequestContext     TargetContextID : %d ", ETG_CENUM(enContextId, targetContextId)));
   vStoreContextRequest(sourceContextId, targetContextId);
   clContextProxy::instance()->vRequestContext(sourceContextId, targetContextId);
}


void clContextRequestor::vOnNewActiveContext(uint32 targetContextId, ContextSwitchTypes::ContextState enContextState, ContextSwitchTypes::ContextType enContextType)
{
   ETG_TRACE_USR4(("REQUESTOR:   vOnNewActiveContext targetContextId : %d ", ETG_CENUM(enContextId, targetContextId)));
   ETG_TRACE_USR4(("REQUESTOR:   vOnNewActiveContext enContextState  : %d ", ETG_CENUM(ContextState, enContextState)));
   ETG_TRACE_USR4(("REQUESTOR:   vOnNewActiveContext enContextState  : %d ", ETG_CENUM(ContextType, enContextType)));
   if (bIsMyRequest(targetContextId))
   {
      vOnRequestResponse(enContextState);
   }
}


void clContextRequestor::vStoreContextRequest(uint32 sourceContextId, uint32 targetContextId)
{
   myContextId = sourceContextId;
   requestedContextId = targetContextId;
}


bool clContextRequestor::bIsMyRequest(uint32 targetContextId)
{
   ETG_TRACE_USR4(("REQUESTOR:   bIsMyRequest        targetContextId : %d ", ETG_CENUM(enContextId, targetContextId)));
   if (requestedContextId == targetContextId)
   {
      return true;
   }
   return false;
}


void clContextRequestor::vOnRequestResponse(ContextSwitchTypes::ContextState enContextState)
{
}


void clContextRequestor::vOnRequestContextResponse(uint32 bValidContext)
{
   if (!bValidContext)
   {
      vReset();
   }
}


void clContextRequestor::vReset()
{
   myContextId = 0;
   requestedContextId = 0;
}


void clContextRequestor::vClearContexts()
{
   clContextProxy::instance()->vClearContexts();
}


void clContextRequestor::vOnAvailableContexts(::std::vector< uint32 > availableContexts)
{
   _currentAvailableContexts = availableContexts;
   vOnNewAvailableContexts();
}


bool clContextRequestor::bIsContextAvailable(uint32 contextId)
{
   ETG_TRACE_USR4(("REQUESTOR:   bIsContextAvailable       contextId : %d ", ETG_CENUM(enContextId, contextId)));
   if (::std::find(_currentAvailableContexts.begin(), _currentAvailableContexts.end(), contextId) != _currentAvailableContexts.end())
   {
      return true;
   }
   return false;
}
