/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#ifndef BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHSTUB_H
#define BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHSTUB_H

#include "asf/core/Logger.h"
#include "asf/core/Types.h"
#include "asf/dbus/DBusStub.h"
#include "boost/shared_ptr.hpp"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitch.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"

#include <gmock/gmock.h>

namespace bosch
{
namespace cm
{
namespace ai
{
namespace nissan
{
namespace hmi
{
namespace contextswitchservice
{
namespace ContextSwitch
{


class ContextSwitchStub
{
   public:

      ContextSwitchStub(const std::string& portName) {};
      virtual ~ContextSwitchStub() {};

      MOCK_METHOD2(sendVOnNewContextSignal, void (uint32 sourceContext, uint32 targetContext));
      MOCK_METHOD2(sendActiveContextSignal, void (uint32 context, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState contextStatus));
      MOCK_METHOD1(onRequestContextRequest, void (const ::boost::shared_ptr< RequestContextRequest >& request));
      MOCK_METHOD2(sendRequestContextResponse, void(uint32 bValidContext, act_t act));
      MOCK_METHOD1(sendRequestContextResponse, void(uint32 bValidContext));
      MOCK_METHOD3(sendRequestContextError, void(const std::string& errorName, const std::string& errorMessage, act_t act));
      MOCK_METHOD2(sendRequestContextError, void(const std::string& errorName, const std::string& errorMessage));
      MOCK_METHOD1(onSetAvailableContextsRequest, void (const ::boost::shared_ptr< SetAvailableContextsRequest >& request));
      MOCK_METHOD1(sendSetAvailableContextsResponse, void(act_t act));
      MOCK_METHOD0(sendSetAvailableContextsResponse, void());
      MOCK_METHOD3(sendSetAvailableContextsError, void(const std::string& errorName, const std::string& errorMessage, act_t act));
      MOCK_METHOD2(sendSetAvailableContextsError, void(const std::string& errorName, const std::string& errorMessage));
      MOCK_METHOD1(onSwitchAppRequest, void(const ::boost::shared_ptr< SwitchAppRequest >& request));
      MOCK_METHOD2(sendSwitchAppResponse, void(uint32 bAppSwitched, act_t act));
      MOCK_METHOD1(sendSwitchAppResponse, void(uint32 bAppSwitched));
      MOCK_METHOD3(sendSwitchAppError, void(const std::string& errorName, const std::string& errorMessage, act_t act));
      MOCK_METHOD2(sendSwitchAppError, void(const std::string& errorName, const std::string& errorMessage));
      MOCK_METHOD1(onRequestContextAckRequest, void(const ::boost::shared_ptr< RequestContextAckRequest >& request));
      MOCK_METHOD1(sendRequestContextAckResponse, void (act_t act));
      MOCK_METHOD0(sendRequestContextAckResponse, void ());
      MOCK_METHOD3(sendRequestContextAckError, void(const std::string& errorName, const std::string& errorMessage, act_t act));
      MOCK_METHOD2(sendRequestContextAckError, void(const std::string& errorName, const std::string& errorMessage));
      MOCK_METHOD1(sendAvailableContextsSignal, void (const ::std::vector< uint32 >& availableContext));

};

} // namespace ContextSwitch
} // namespace contextswitchservice
} // namespace hmi
} // namespace nissan
} // namespace ai
} // namespace cm
} // namespace bosch

#endif // BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHSTUB_H
