/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesJson.h"


// disabled lint warning 616: control flows into case/default (break)
//lint -e616

// disabled lint warning 715: symbol not referenced
//lint -e715

//lint -e10

// disabled lint warning 40: undecleared identifier (intptr)
//lint -e40

//lint -e429
//lint -e613
//lint +e616
//lint +e715
//lint +e10
//lint +e40
//lint +e429
//lint +e613

