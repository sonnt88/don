/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#ifndef BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHTYPESCONST_H
#define BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHTYPESCONST_H

/**
 * todo
 */

namespace bosch
{
namespace cm
{
namespace ai
{
namespace nissan
{
namespace hmi
{
namespace contextswitchservice
{
namespace ContextSwitchTypes
{

/**
 * Represents the possible states of a Context Switch sequence
 */
enum ContextState
{

   /**
    * If the meaning of "ACCEPTED" isn't clear, then there should be a description here.
    */
   ContextState__ACCEPTED,
   /**
    * If the meaning of "REJECTED" isn't clear, then there should be a description here.
    */
   ContextState__REJECTED,
   /**
    * If the meaning of "COMPLETED" isn't clear, then there should be a description here.
    */
   ContextState__COMPLETED,
   /**
    * If the meaning of "INVALID" isn't clear, then there should be a description here.
    */
   ContextState__INVALID

};

bool ContextState_Parse(const char* str, int length, ContextState& value);

const char* ContextState_Name(ContextState value);

bool ContextState_IsValid(ContextState value);

} // namespace ContextSwitchTypes
} // namespace contextswitchservice
} // namespace hmi
} // namespace nissan
} // namespace ai
} // namespace cm
} // namespace bosch

#endif // BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHTYPESCONST_H
