///////////////////////////////////////////////////////////
//  clContextRequestor.h
//  Implementation of the Class clContextRequestor
//  Created on:      13-Jul-2015 5:44:20 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(clContextRequestor_h)
#define clContextRequestor_h

/**
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 5:44:20 PM
 */

#include "clContextRequestorInterface.h"
#include "asf/core/Types.h"
#include "clContext.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"


using namespace bosch::cm::ai::nissan::hmi::contextswitchservice;


class clContextRequestor : public clContextRequestorInterface
{
   public:
      clContextRequestor();
      virtual ~clContextRequestor();

      /**
       * Initialize the application wrt requestor
       */
      void vInit();
      /**
       *
       * @param sourceContext
       * @param targetContext
       */
      void vRequestContext(uint32 sourceContextId, uint32 targetContextId);
      virtual void vOnNewActiveContext(uint32 targetContextId, ContextSwitchTypes::ContextState enContextState, ContextSwitchTypes::ContextType enContextType);
      void vOnRequestContextResponse(uint32 bValidContext);
      void vOnAvailableContexts(::std::vector< uint32 > availableContexts);
      void vClearContexts();

   protected:

      bool bIsContextAvailable(uint32 contextId);
      virtual void vOnRequestResponse(ContextSwitchTypes::ContextState enContextState);
      virtual void vOnNewAvailableContexts() {};

   private:
      void vStoreContextRequest(uint32 sourceContextId, uint32 targetContextId);
      bool bIsMyRequest(uint32 targetContextId);
      void vReset();
      uint32 myContextId;
      uint32 requestedContextId;
      ::std::vector< uint32 > _currentAvailableContexts;
};


#endif // !defined(EA_D478FD03_EC34_490a_BB23_6FC06868F8B9__INCLUDED_)
