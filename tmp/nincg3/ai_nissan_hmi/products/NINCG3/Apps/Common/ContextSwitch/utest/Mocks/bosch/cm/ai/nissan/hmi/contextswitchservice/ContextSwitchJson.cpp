/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#include "asf/core/Types.h"
#include "asf/stream/json.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitch.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchJson.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"
#include "yajl/yajl_gen.h"
#include <cstring>
#include <string>
#include <vector>

using namespace ::asf::stream::json;


// disabled lint warning 616: control flows into case/default (break)
//lint -e616

// disabled lint warning 715: symbol not referenced
//lint -e715

//lint -e10

// disabled lint warning 40: undecleared identifier (intptr)
//lint -e40

//lint -e429
//lint -e613
// Json serialization of "RequestContextRequest"

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextRequest& value, yajl_gen& g, ::asf::stream::json::JsonSerializationOptions options)
{
   yajl_gen_map_open(g);
   bool isSparse = (options & JSON_SERIALIZATION_OPTION_SPARSE) > 0;
   if (!isSparse || value.hasSourceContext())
   {
      yajl_gen_string(g, (const unsigned char*)  "sourceContext" , 13);
      yajl_gen_integer(g, (long long)value.getSourceContext());
   }
   if (!isSparse || value.hasTargetContext())
   {
      yajl_gen_string(g, (const unsigned char*)  "targetContext" , 13);
      yajl_gen_integer(g, (long long)value.getTargetContext());
   }
   yajl_gen_map_close(g);
}

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextRequest& value, ::std::string& jsonString, ::asf::stream::json::JsonSerializationOptions options)
{
   yajl_gen g;
   g = yajl_gen_alloc(NULL);
   yajl_gen_config(g, yajl_gen_indent_string, "\t");
   yajl_gen_config(g, yajl_gen_beautify, 1);

   serializeJson(value, g, options);

   const unsigned char* buf;
   size_t len;
   yajl_gen_get_buf(g, &buf, &len);
   jsonString = std::string((const char*) buf, len);

   yajl_gen_free(g);
}

// Json serialization of "RequestContextResponse"

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextResponse& value, yajl_gen& g, ::asf::stream::json::JsonSerializationOptions options)
{
   yajl_gen_map_open(g);
   bool isSparse = (options & JSON_SERIALIZATION_OPTION_SPARSE) > 0;
   if (!isSparse || value.hasBValidContext())
   {
      yajl_gen_string(g, (const unsigned char*)  "bValidContext" , 13);
      yajl_gen_integer(g, (long long)value.getBValidContext());
   }
   yajl_gen_map_close(g);
}

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextResponse& value, ::std::string& jsonString, ::asf::stream::json::JsonSerializationOptions options)
{
   yajl_gen g;
   g = yajl_gen_alloc(NULL);
   yajl_gen_config(g, yajl_gen_indent_string, "\t");
   yajl_gen_config(g, yajl_gen_beautify, 1);

   serializeJson(value, g, options);

   const unsigned char* buf;
   size_t len;
   yajl_gen_get_buf(g, &buf, &len);
   jsonString = std::string((const char*) buf, len);

   yajl_gen_free(g);
}

// Json serialization of "SetAvailableContextsRequest"

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SetAvailableContextsRequest& value, yajl_gen& g, ::asf::stream::json::JsonSerializationOptions options)
{
   yajl_gen_map_open(g);
   bool isSparse = (options & JSON_SERIALIZATION_OPTION_SPARSE) > 0;
   if (!isSparse || value.hasAvailableContext())
   {
      yajl_gen_string(g, (const unsigned char*)  "availableContext" , 16);
      yajl_gen_array_open(g);
      const ::std::vector< uint32 >& o1 = value.getAvailableContext();
      {
         ::std::vector< uint32 >::const_iterator o0;
         for (o0 = o1.begin(); o0 != o1.end(); ++o0)
         {
            yajl_gen_integer(g, (long long)(*o0));
         }
      }
      yajl_gen_array_close(g);
   }
   yajl_gen_map_close(g);
}

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SetAvailableContextsRequest& value, ::std::string& jsonString, ::asf::stream::json::JsonSerializationOptions options)
{
   yajl_gen g;
   g = yajl_gen_alloc(NULL);
   yajl_gen_config(g, yajl_gen_indent_string, "\t");
   yajl_gen_config(g, yajl_gen_beautify, 1);

   serializeJson(value, g, options);

   const unsigned char* buf;
   size_t len;
   yajl_gen_get_buf(g, &buf, &len);
   jsonString = std::string((const char*) buf, len);

   yajl_gen_free(g);
}

// Json serialization of "SwitchAppRequest"

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SwitchAppRequest& value, yajl_gen& g, ::asf::stream::json::JsonSerializationOptions options)
{
   yajl_gen_map_open(g);
   bool isSparse = (options & JSON_SERIALIZATION_OPTION_SPARSE) > 0;
   if (!isSparse || value.hasApplicationId())
   {
      yajl_gen_string(g, (const unsigned char*)  "applicationId" , 13);
      yajl_gen_integer(g, (long long)value.getApplicationId());
   }
   if (!isSparse || value.hasPriority())
   {
      yajl_gen_string(g, (const unsigned char*)  "priority" , 8);
      yajl_gen_integer(g, (long long)value.getPriority());
   }
   if (!isSparse || value.hasTargetContext())
   {
      yajl_gen_string(g, (const unsigned char*)  "targetContext" , 13);
      yajl_gen_integer(g, (long long)value.getTargetContext());
   }
   yajl_gen_map_close(g);
}

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SwitchAppRequest& value, ::std::string& jsonString, ::asf::stream::json::JsonSerializationOptions options)
{
   yajl_gen g;
   g = yajl_gen_alloc(NULL);
   yajl_gen_config(g, yajl_gen_indent_string, "\t");
   yajl_gen_config(g, yajl_gen_beautify, 1);

   serializeJson(value, g, options);

   const unsigned char* buf;
   size_t len;
   yajl_gen_get_buf(g, &buf, &len);
   jsonString = std::string((const char*) buf, len);

   yajl_gen_free(g);
}

// Json serialization of "SwitchAppResponse"

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SwitchAppResponse& value, yajl_gen& g, ::asf::stream::json::JsonSerializationOptions options)
{
   yajl_gen_map_open(g);
   bool isSparse = (options & JSON_SERIALIZATION_OPTION_SPARSE) > 0;
   if (!isSparse || value.hasBAppSwitched())
   {
      yajl_gen_string(g, (const unsigned char*)  "bAppSwitched" , 12);
      yajl_gen_integer(g, (long long)value.getBAppSwitched());
   }
   yajl_gen_map_close(g);
}

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SwitchAppResponse& value, ::std::string& jsonString, ::asf::stream::json::JsonSerializationOptions options)
{
   yajl_gen g;
   g = yajl_gen_alloc(NULL);
   yajl_gen_config(g, yajl_gen_indent_string, "\t");
   yajl_gen_config(g, yajl_gen_beautify, 1);

   serializeJson(value, g, options);

   const unsigned char* buf;
   size_t len;
   yajl_gen_get_buf(g, &buf, &len);
   jsonString = std::string((const char*) buf, len);

   yajl_gen_free(g);
}

// Json serialization of "RequestContextAckRequest"

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextAckRequest& value, yajl_gen& g, ::asf::stream::json::JsonSerializationOptions options)
{
   yajl_gen_map_open(g);
   bool isSparse = (options & JSON_SERIALIZATION_OPTION_SPARSE) > 0;
   if (!isSparse || value.hasContextStatus())
   {
      yajl_gen_string(g, (const unsigned char*)  "contextStatus" , 13);
      const char* s = ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState_Name(value.getContextStatus());
      if (s != NULL)
      {
         yajl_gen_string(g, (const unsigned char*)  s, strlen(s));
      }
      else
      {
         yajl_gen_integer(g, (long long)value.getContextStatus());
      }
   }
   if (!isSparse || value.hasTargetContext())
   {
      yajl_gen_string(g, (const unsigned char*)  "targetContext" , 13);
      yajl_gen_integer(g, (long long)value.getTargetContext());
   }
   yajl_gen_map_close(g);
}

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextAckRequest& value, ::std::string& jsonString, ::asf::stream::json::JsonSerializationOptions options)
{
   yajl_gen g;
   g = yajl_gen_alloc(NULL);
   yajl_gen_config(g, yajl_gen_indent_string, "\t");
   yajl_gen_config(g, yajl_gen_beautify, 1);

   serializeJson(value, g, options);

   const unsigned char* buf;
   size_t len;
   yajl_gen_get_buf(g, &buf, &len);
   jsonString = std::string((const char*) buf, len);

   yajl_gen_free(g);
}

// Json serialization of "VOnNewContextSignal"

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::VOnNewContextSignal& value, yajl_gen& g, ::asf::stream::json::JsonSerializationOptions options)
{
   yajl_gen_map_open(g);
   bool isSparse = (options & JSON_SERIALIZATION_OPTION_SPARSE) > 0;
   if (!isSparse || value.hasSourceContext())
   {
      yajl_gen_string(g, (const unsigned char*)  "sourceContext" , 13);
      yajl_gen_integer(g, (long long)value.getSourceContext());
   }
   if (!isSparse || value.hasTargetContext())
   {
      yajl_gen_string(g, (const unsigned char*)  "targetContext" , 13);
      yajl_gen_integer(g, (long long)value.getTargetContext());
   }
   yajl_gen_map_close(g);
}

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::VOnNewContextSignal& value, ::std::string& jsonString, ::asf::stream::json::JsonSerializationOptions options)
{
   yajl_gen g;
   g = yajl_gen_alloc(NULL);
   yajl_gen_config(g, yajl_gen_indent_string, "\t");
   yajl_gen_config(g, yajl_gen_beautify, 1);

   serializeJson(value, g, options);

   const unsigned char* buf;
   size_t len;
   yajl_gen_get_buf(g, &buf, &len);
   jsonString = std::string((const char*) buf, len);

   yajl_gen_free(g);
}

// Json serialization of "ActiveContextSignal"

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::ActiveContextSignal& value, yajl_gen& g, ::asf::stream::json::JsonSerializationOptions options)
{
   yajl_gen_map_open(g);
   bool isSparse = (options & JSON_SERIALIZATION_OPTION_SPARSE) > 0;
   if (!isSparse || value.hasContext())
   {
      yajl_gen_string(g, (const unsigned char*)  "context" , 7);
      yajl_gen_integer(g, (long long)value.getContext());
   }
   if (!isSparse || value.hasContextStatus())
   {
      yajl_gen_string(g, (const unsigned char*)  "contextStatus" , 13);
      const char* s = ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState_Name(value.getContextStatus());
      if (s != NULL)
      {
         yajl_gen_string(g, (const unsigned char*)  s, strlen(s));
      }
      else
      {
         yajl_gen_integer(g, (long long)value.getContextStatus());
      }
   }
   yajl_gen_map_close(g);
}

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::ActiveContextSignal& value, ::std::string& jsonString, ::asf::stream::json::JsonSerializationOptions options)
{
   yajl_gen g;
   g = yajl_gen_alloc(NULL);
   yajl_gen_config(g, yajl_gen_indent_string, "\t");
   yajl_gen_config(g, yajl_gen_beautify, 1);

   serializeJson(value, g, options);

   const unsigned char* buf;
   size_t len;
   yajl_gen_get_buf(g, &buf, &len);
   jsonString = std::string((const char*) buf, len);

   yajl_gen_free(g);
}

//lint +e616
//lint +e715
//lint +e10
//lint +e40
//lint +e429
//lint +e613

