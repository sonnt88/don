/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#ifndef BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHTYPES_H
#define BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHTYPES_H

#include "asf/core/Types.h"

/**
 * todo
 */

namespace bosch
{
namespace cm
{
namespace ai
{
namespace nissan
{
namespace hmi
{
namespace contextswitchservice
{
namespace ContextSwitchTypes
{

// forward declarations
// type definitions
/**
 * If the meaning of "SwitchId" isn't clear, then there should be a description here.
 */
typedef uint32 SwitchId;

/**
 * If the meaning of "ContextId" isn't clear, then there should be a description here.
 */
typedef uint32 ContextId;

/**
 * If the meaning of "AppId" isn't clear, then there should be a description here.
 */
typedef uint32 AppId;

} // namespace ContextSwitchTypes
} // namespace contextswitchservice
} // namespace hmi
} // namespace nissan
} // namespace ai
} // namespace cm
} // namespace bosch

#endif // BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHTYPES_H
