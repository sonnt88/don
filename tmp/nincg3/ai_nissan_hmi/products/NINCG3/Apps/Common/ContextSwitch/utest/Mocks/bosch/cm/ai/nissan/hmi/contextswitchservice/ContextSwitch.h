/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#ifndef BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCH_H
#define BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCH_H

#include "asf/core/Logger.h"
#include "asf/core/Payload.h"
#include "asf/core/Types.h"
#include "asf/dbus/DBusTypes.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"
#include <cstring>
#include <vector>

/**
 * todo
 */

namespace bosch
{
namespace cm
{
namespace ai
{
namespace nissan
{
namespace hmi
{
namespace contextswitchservice
{
namespace ContextSwitch
{

// method IDs
static const int ID_requestContext = 0;
static const int ID_setAvailableContexts = 1;
static const int ID_switchApp = 2;
static const int ID_requestContextAck = 3;

// signal IDs
static const int ID_vOnNewContext = 4;
static const int ID_activeContext = 5;

// property IDs

// forward declarations
class RequestContextRequest;

class RequestContextResponse;

class SetAvailableContextsRequest;

class SwitchAppRequest;

class SwitchAppResponse;

class RequestContextAckRequest;

class VOnNewContextSignal;

class ActiveContextSignal;

// type definitions
/**
 * If the meaning of "RequestContextError" isn't clear, then there should be a description here.
 */
typedef ::asf::dbus::DBusTypes::DBusError RequestContextError;

/**
 * Method to set available contexts of any particular application
 */
typedef ::asf::dbus::DBusTypes::DefaultMessage SetAvailableContextsResponse;

/**
 * If the meaning of "SetAvailableContextsError" isn't clear, then there should be a description here.
 */
typedef ::asf::dbus::DBusTypes::DBusError SetAvailableContextsError;

/**
 * If the meaning of "SwitchAppError" isn't clear, then there should be a description here.
 */
typedef ::asf::dbus::DBusTypes::DBusError SwitchAppError;

/**
 * Method to set Context change acknowledgement
 */
typedef ::asf::dbus::DBusTypes::DefaultMessage RequestContextAckResponse;

/**
 * If the meaning of "RequestContextAckError" isn't clear, then there should be a description here.
 */
typedef ::asf::dbus::DBusTypes::DBusError RequestContextAckError;

/**
 * If the meaning of "VOnNewContextError" isn't clear, then there should be a description here.
 */
typedef ::asf::dbus::DBusTypes::DBusError VOnNewContextError;

/**
 * If the meaning of "ActiveContextError" isn't clear, then there should be a description here.
 */
typedef ::asf::dbus::DBusTypes::DBusError ActiveContextError;

/**
 * Method to request a context to activate an application
 * 		the message results in a signal triggered to all clients registered
 */
class RequestContextRequest  : public ::asf::core::Payload
{
   public:

      /**
       * Default constructor
       */
      inline  RequestContextRequest();

      /**
       * Copy constructor
       */
      inline  RequestContextRequest(const RequestContextRequest& c);

      /**
       * All fields constructor
       */
      //lint -efunc(1003,[*::]RequestContextRequest)
      //lint -efunc(1025,[*::]RequestContextRequest)
      //lint -efunc(119,[*::]RequestContextRequest)
      inline  RequestContextRequest(uint32 sourceContext_, uint32 targetContext_);

      /**
       * Destructor
       */
      inline virtual  ~RequestContextRequest();

      /**
       * Assignment operator
       */
      inline RequestContextRequest& operator = (const RequestContextRequest& rhs);

      /**
       * Equality operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if both objects are equal, otherwise returns false
       */
      inline bool operator == (const RequestContextRequest& rhs) const;

      /**
       * Inequality operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if both objects are not equal, otherwise returns false
       */
      inline bool operator != (const RequestContextRequest& rhs) const;

      /**
       * Less than operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if this object is less than the rhs object
       */
      inline bool operator < (const RequestContextRequest& rhs) const;

      /**
       * Creates and returns a clone of this object.
       *
       * This method is intended to be used when the type of the object
       * is not known by the caller, e.g. you have a pointer to a base
       * class. In all other cases you should make use of the copy
       * constructor.
       *
       * @return The clone of this object
       */
      inline ::asf::core::Payload* clone();

      /**
       * Greater than operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if this object is greater than the rhs object
       */
      inline bool operator > (const RequestContextRequest& rhs) const;

      /**
       * Clear discards all contents from this structure.
       *
       * All fields will be set to their default value. The hasXXX() methods
       * will return false for all fields.
       */
      inline void clear();

      // ---- Field accessors -------------------------------------------

      // API of field "sourceContext"

      static const int kSourceContext = 0;

      /**
       * Clears the field "sourceContext".
       *
       * The field will be set to it's default value. The hasSourceContext()
       * method will return false.
       */
      inline void clearSourceContext();

      /**
       * Checks whether the field "sourceContext" was set before.
       *
       * @return Returns true if the field was set before (a field get set by calling
       * the setter method setSourceContext()). Otherwise it will return false.
       */
      inline bool hasSourceContext() const;

      /**
       * Returns the value of the member "sourceContext".
       *
       * If the meaning of "sourceContext" isn't clear, then there should be a description here.
       *
       * @return The value of the field "sourceContext"
       */
      inline uint32 getSourceContext() const;

      /**
       * Sets the value of the member "sourceContext".
       *
       * If the meaning of "sourceContext" isn't clear, then there should be a description here.
       *
       * @param sourceContext The value which will be set
       */
      inline void setSourceContext(uint32 sourceContext_);

      // API of field "targetContext"

      static const int kTargetContext = 1;

      /**
       * Clears the field "targetContext".
       *
       * The field will be set to it's default value. The hasTargetContext()
       * method will return false.
       */
      inline void clearTargetContext();

      /**
       * Checks whether the field "targetContext" was set before.
       *
       * @return Returns true if the field was set before (a field get set by calling
       * the setter method setTargetContext()). Otherwise it will return false.
       */
      inline bool hasTargetContext() const;

      /**
       * Returns the value of the member "targetContext".
       *
       * If the meaning of "targetContext" isn't clear, then there should be a description here.
       *
       * @return The value of the field "targetContext"
       */
      inline uint32 getTargetContext() const;

      /**
       * Sets the value of the member "targetContext".
       *
       * If the meaning of "targetContext" isn't clear, then there should be a description here.
       *
       * @param targetContext The value which will be set
       */
      inline void setTargetContext(uint32 targetContext_);

      static const RequestContextRequest& getDefaultInstance();

   private:

      static RequestContextRequest* _defaultInstance;

      static void deleteDefaultInstance();

      inline void set_has_sourceContext();

      inline void clear_has_sourceContext();

      inline void set_has_targetContext();

      inline void clear_has_targetContext();

      uint32 _has_bits_[(2 + 31) / 32];

      uint32 _sourceContext;

      uint32 _targetContext;

};

/**
 * Method to request a context to activate an application
 * 		the message results in a signal triggered to all clients registered
 */
class RequestContextResponse  : public ::asf::core::Payload
{
   public:

      /**
       * Default constructor
       */
      inline  RequestContextResponse();

      /**
       * Copy constructor
       */
      inline  RequestContextResponse(const RequestContextResponse& c);

      /**
       * All fields constructor
       */
      //lint -efunc(1003,[*::]RequestContextResponse)
      //lint -efunc(1025,[*::]RequestContextResponse)
      //lint -efunc(119,[*::]RequestContextResponse)
      inline  RequestContextResponse(uint32 bValidContext_);

      /**
       * Destructor
       */
      inline virtual  ~RequestContextResponse();

      /**
       * Assignment operator
       */
      inline RequestContextResponse& operator = (const RequestContextResponse& rhs);

      /**
       * Equality operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if both objects are equal, otherwise returns false
       */
      inline bool operator == (const RequestContextResponse& rhs) const;

      /**
       * Inequality operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if both objects are not equal, otherwise returns false
       */
      inline bool operator != (const RequestContextResponse& rhs) const;

      /**
       * Less than operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if this object is less than the rhs object
       */
      inline bool operator < (const RequestContextResponse& rhs) const;

      /**
       * Creates and returns a clone of this object.
       *
       * This method is intended to be used when the type of the object
       * is not known by the caller, e.g. you have a pointer to a base
       * class. In all other cases you should make use of the copy
       * constructor.
       *
       * @return The clone of this object
       */
      inline ::asf::core::Payload* clone();

      /**
       * Greater than operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if this object is greater than the rhs object
       */
      inline bool operator > (const RequestContextResponse& rhs) const;

      /**
       * Clear discards all contents from this structure.
       *
       * All fields will be set to their default value. The hasXXX() methods
       * will return false for all fields.
       */
      inline void clear();

      // ---- Field accessors -------------------------------------------

      // API of field "bValidContext"

      static const int kBValidContext = 0;

      /**
       * Clears the field "bValidContext".
       *
       * The field will be set to it's default value. The hasBValidContext()
       * method will return false.
       */
      inline void clearBValidContext();

      /**
       * Checks whether the field "bValidContext" was set before.
       *
       * @return Returns true if the field was set before (a field get set by calling
       * the setter method setBValidContext()). Otherwise it will return false.
       */
      inline bool hasBValidContext() const;

      /**
       * Returns the value of the member "bValidContext".
       *
       * If the meaning of "bValidContext" isn't clear, then there should be a description here.
       *
       * @return The value of the field "bValidContext"
       */
      inline uint32 getBValidContext() const;

      /**
       * Sets the value of the member "bValidContext".
       *
       * If the meaning of "bValidContext" isn't clear, then there should be a description here.
       *
       * @param bValidContext The value which will be set
       */
      inline void setBValidContext(uint32 bValidContext_);

      static const RequestContextResponse& getDefaultInstance();

   private:

      static RequestContextResponse* _defaultInstance;

      static void deleteDefaultInstance();

      inline void set_has_bValidContext();

      inline void clear_has_bValidContext();

      uint32 _has_bits_[(1 + 31) / 32];

      uint32 _bValidContext;

};

/**
 * Method to set available contexts of any particular application
 */
class SetAvailableContextsRequest  : public ::asf::core::Payload
{
   public:

      /**
       * Default constructor
       */
      inline  SetAvailableContextsRequest();

      /**
       * Copy constructor
       */
      inline  SetAvailableContextsRequest(const SetAvailableContextsRequest& c);

      /**
       * All fields constructor
       */
      //lint -efunc(1003,[*::]SetAvailableContextsRequest)
      //lint -efunc(1025,[*::]SetAvailableContextsRequest)
      //lint -efunc(119,[*::]SetAvailableContextsRequest)
      inline  SetAvailableContextsRequest(const ::std::vector< uint32 >& availableContext_);

      /**
       * Destructor
       */
      inline virtual  ~SetAvailableContextsRequest();

      /**
       * Assignment operator
       */
      inline SetAvailableContextsRequest& operator = (const SetAvailableContextsRequest& rhs);

      /**
       * Equality operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if both objects are equal, otherwise returns false
       */
      inline bool operator == (const SetAvailableContextsRequest& rhs) const;

      /**
       * Inequality operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if both objects are not equal, otherwise returns false
       */
      inline bool operator != (const SetAvailableContextsRequest& rhs) const;

      /**
       * Less than operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if this object is less than the rhs object
       */
      inline bool operator < (const SetAvailableContextsRequest& rhs) const;

      /**
       * Creates and returns a clone of this object.
       *
       * This method is intended to be used when the type of the object
       * is not known by the caller, e.g. you have a pointer to a base
       * class. In all other cases you should make use of the copy
       * constructor.
       *
       * @return The clone of this object
       */
      inline ::asf::core::Payload* clone();

      /**
       * Greater than operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if this object is greater than the rhs object
       */
      inline bool operator > (const SetAvailableContextsRequest& rhs) const;

      /**
       * Clear discards all contents from this structure.
       *
       * All fields will be set to their default value. The hasXXX() methods
       * will return false for all fields.
       */
      inline void clear();

      // ---- Field accessors -------------------------------------------

      // API of field "availableContext"

      static const int kAvailableContext = 0;

      /**
       * Clears the field "availableContext".
       *
       * The field will be set to it's default value. The hasAvailableContext()
       * method will return false.
       */
      inline void clearAvailableContext();

      /**
       * Checks whether the field "availableContext" was set before.
       *
       * @return Returns true if the field was set before (a field get set by calling
       * the setter method setAvailableContext()). Otherwise it will return false.
       */
      inline bool hasAvailableContext() const;

      /**
       * Returns the value of the member "availableContext".
       *
       * If the meaning of "availableContext" isn't clear, then there should be a description here.
       *
       * @return The value of the field "availableContext"
       */
      inline const ::std::vector< uint32 >& getAvailableContext() const;

      /**
       * Retrieves the value of the the field "availableContext" as a mutable object.
       * Any modifications on the returned object will affect the containing object.
       *
       * @return The value of the field "availableContext".
       */
      inline ::std::vector< uint32 >& getAvailableContextMutable();

      /**
       * Sets the value of the member "availableContext".
       *
       * If the meaning of "availableContext" isn't clear, then there should be a description here.
       *
       * @param availableContext The value which will be set
       */
      inline void setAvailableContext(const ::std::vector< uint32 >& availableContext_);

      static const SetAvailableContextsRequest& getDefaultInstance();

   private:

      static SetAvailableContextsRequest* _defaultInstance;

      static void deleteDefaultInstance();

      static ::std::vector< uint32 >* _Uint32List_DefaultInstance;

      static const ::std::vector< uint32 >& getUint32List_DefaultInstance();

      static void deleteUint32List_DefaultInstance();

      inline void set_has_availableContext();

      inline void clear_has_availableContext();

      uint32 _has_bits_[(1 + 31) / 32];

      ::std::vector< uint32 > _availableContext;

};

/**
 * Method to request an app change due to context switch
 */
class SwitchAppRequest  : public ::asf::core::Payload
{
   public:

      /**
       * Default constructor
       */
      inline  SwitchAppRequest();

      /**
       * Copy constructor
       */
      inline  SwitchAppRequest(const SwitchAppRequest& c);

      /**
       * All fields constructor
       */
      //lint -efunc(1003,[*::]SwitchAppRequest)
      //lint -efunc(1025,[*::]SwitchAppRequest)
      //lint -efunc(119,[*::]SwitchAppRequest)
      inline  SwitchAppRequest(uint32 applicationId_, uint32 priority_, uint32 targetContext_);

      /**
       * Destructor
       */
      inline virtual  ~SwitchAppRequest();

      /**
       * Assignment operator
       */
      inline SwitchAppRequest& operator = (const SwitchAppRequest& rhs);

      /**
       * Equality operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if both objects are equal, otherwise returns false
       */
      inline bool operator == (const SwitchAppRequest& rhs) const;

      /**
       * Inequality operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if both objects are not equal, otherwise returns false
       */
      inline bool operator != (const SwitchAppRequest& rhs) const;

      /**
       * Less than operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if this object is less than the rhs object
       */
      inline bool operator < (const SwitchAppRequest& rhs) const;

      /**
       * Creates and returns a clone of this object.
       *
       * This method is intended to be used when the type of the object
       * is not known by the caller, e.g. you have a pointer to a base
       * class. In all other cases you should make use of the copy
       * constructor.
       *
       * @return The clone of this object
       */
      inline ::asf::core::Payload* clone();

      /**
       * Greater than operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if this object is greater than the rhs object
       */
      inline bool operator > (const SwitchAppRequest& rhs) const;

      /**
       * Clear discards all contents from this structure.
       *
       * All fields will be set to their default value. The hasXXX() methods
       * will return false for all fields.
       */
      inline void clear();

      // ---- Field accessors -------------------------------------------

      // API of field "applicationId"

      static const int kApplicationId = 0;

      /**
       * Clears the field "applicationId".
       *
       * The field will be set to it's default value. The hasApplicationId()
       * method will return false.
       */
      inline void clearApplicationId();

      /**
       * Checks whether the field "applicationId" was set before.
       *
       * @return Returns true if the field was set before (a field get set by calling
       * the setter method setApplicationId()). Otherwise it will return false.
       */
      inline bool hasApplicationId() const;

      /**
       * Returns the value of the member "applicationId".
       *
       * If the meaning of "applicationId" isn't clear, then there should be a description here.
       *
       * @return The value of the field "applicationId"
       */
      inline uint32 getApplicationId() const;

      /**
       * Sets the value of the member "applicationId".
       *
       * If the meaning of "applicationId" isn't clear, then there should be a description here.
       *
       * @param applicationId The value which will be set
       */
      inline void setApplicationId(uint32 applicationId_);

      // API of field "priority"

      static const int kPriority = 1;

      /**
       * Clears the field "priority".
       *
       * The field will be set to it's default value. The hasPriority()
       * method will return false.
       */
      inline void clearPriority();

      /**
       * Checks whether the field "priority" was set before.
       *
       * @return Returns true if the field was set before (a field get set by calling
       * the setter method setPriority()). Otherwise it will return false.
       */
      inline bool hasPriority() const;

      /**
       * Returns the value of the member "priority".
       *
       * If the meaning of "priority" isn't clear, then there should be a description here.
       *
       * @return The value of the field "priority"
       */
      inline uint32 getPriority() const;

      /**
       * Sets the value of the member "priority".
       *
       * If the meaning of "priority" isn't clear, then there should be a description here.
       *
       * @param priority The value which will be set
       */
      inline void setPriority(uint32 priority_);

      // API of field "targetContext"

      static const int kTargetContext = 2;

      /**
       * Clears the field "targetContext".
       *
       * The field will be set to it's default value. The hasTargetContext()
       * method will return false.
       */
      inline void clearTargetContext();

      /**
       * Checks whether the field "targetContext" was set before.
       *
       * @return Returns true if the field was set before (a field get set by calling
       * the setter method setTargetContext()). Otherwise it will return false.
       */
      inline bool hasTargetContext() const;

      /**
       * Returns the value of the member "targetContext".
       *
       * If the meaning of "targetContext" isn't clear, then there should be a description here.
       *
       * @return The value of the field "targetContext"
       */
      inline uint32 getTargetContext() const;

      /**
       * Sets the value of the member "targetContext".
       *
       * If the meaning of "targetContext" isn't clear, then there should be a description here.
       *
       * @param targetContext The value which will be set
       */
      inline void setTargetContext(uint32 targetContext_);

      static const SwitchAppRequest& getDefaultInstance();

   private:

      static SwitchAppRequest* _defaultInstance;

      static void deleteDefaultInstance();

      inline void set_has_applicationId();

      inline void clear_has_applicationId();

      inline void set_has_priority();

      inline void clear_has_priority();

      inline void set_has_targetContext();

      inline void clear_has_targetContext();

      uint32 _has_bits_[(3 + 31) / 32];

      uint32 _applicationId;

      uint32 _priority;

      uint32 _targetContext;

};

/**
 * Method to request an app change due to context switch
 */
class SwitchAppResponse  : public ::asf::core::Payload
{
   public:

      /**
       * Default constructor
       */
      inline  SwitchAppResponse();

      /**
       * Copy constructor
       */
      inline  SwitchAppResponse(const SwitchAppResponse& c);

      /**
       * All fields constructor
       */
      //lint -efunc(1003,[*::]SwitchAppResponse)
      //lint -efunc(1025,[*::]SwitchAppResponse)
      //lint -efunc(119,[*::]SwitchAppResponse)
      inline  SwitchAppResponse(uint32 bAppSwitched_);

      /**
       * Destructor
       */
      inline virtual  ~SwitchAppResponse();

      /**
       * Assignment operator
       */
      inline SwitchAppResponse& operator = (const SwitchAppResponse& rhs);

      /**
       * Equality operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if both objects are equal, otherwise returns false
       */
      inline bool operator == (const SwitchAppResponse& rhs) const;

      /**
       * Inequality operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if both objects are not equal, otherwise returns false
       */
      inline bool operator != (const SwitchAppResponse& rhs) const;

      /**
       * Less than operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if this object is less than the rhs object
       */
      inline bool operator < (const SwitchAppResponse& rhs) const;

      /**
       * Creates and returns a clone of this object.
       *
       * This method is intended to be used when the type of the object
       * is not known by the caller, e.g. you have a pointer to a base
       * class. In all other cases you should make use of the copy
       * constructor.
       *
       * @return The clone of this object
       */
      inline ::asf::core::Payload* clone();

      /**
       * Greater than operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if this object is greater than the rhs object
       */
      inline bool operator > (const SwitchAppResponse& rhs) const;

      /**
       * Clear discards all contents from this structure.
       *
       * All fields will be set to their default value. The hasXXX() methods
       * will return false for all fields.
       */
      inline void clear();

      // ---- Field accessors -------------------------------------------

      // API of field "bAppSwitched"

      static const int kBAppSwitched = 0;

      /**
       * Clears the field "bAppSwitched".
       *
       * The field will be set to it's default value. The hasBAppSwitched()
       * method will return false.
       */
      inline void clearBAppSwitched();

      /**
       * Checks whether the field "bAppSwitched" was set before.
       *
       * @return Returns true if the field was set before (a field get set by calling
       * the setter method setBAppSwitched()). Otherwise it will return false.
       */
      inline bool hasBAppSwitched() const;

      /**
       * Returns the value of the member "bAppSwitched".
       *
       * If the meaning of "bAppSwitched" isn't clear, then there should be a description here.
       *
       * @return The value of the field "bAppSwitched"
       */
      inline uint32 getBAppSwitched() const;

      /**
       * Sets the value of the member "bAppSwitched".
       *
       * If the meaning of "bAppSwitched" isn't clear, then there should be a description here.
       *
       * @param bAppSwitched The value which will be set
       */
      inline void setBAppSwitched(uint32 bAppSwitched_);

      static const SwitchAppResponse& getDefaultInstance();

   private:

      static SwitchAppResponse* _defaultInstance;

      static void deleteDefaultInstance();

      inline void set_has_bAppSwitched();

      inline void clear_has_bAppSwitched();

      uint32 _has_bits_[(1 + 31) / 32];

      uint32 _bAppSwitched;

};

/**
 * Method to set Context change acknowledgement
 */
class RequestContextAckRequest  : public ::asf::core::Payload
{
   public:

      /**
       * Default constructor
       */
      inline  RequestContextAckRequest();

      /**
       * Copy constructor
       */
      inline  RequestContextAckRequest(const RequestContextAckRequest& c);

      /**
       * All fields constructor
       */
      //lint -efunc(1003,[*::]RequestContextAckRequest)
      //lint -efunc(1025,[*::]RequestContextAckRequest)
      //lint -efunc(119,[*::]RequestContextAckRequest)
      inline  RequestContextAckRequest(::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState contextStatus_, uint32 targetContext_);

      /**
       * Destructor
       */
      inline virtual  ~RequestContextAckRequest();

      /**
       * Assignment operator
       */
      inline RequestContextAckRequest& operator = (const RequestContextAckRequest& rhs);

      /**
       * Equality operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if both objects are equal, otherwise returns false
       */
      inline bool operator == (const RequestContextAckRequest& rhs) const;

      /**
       * Inequality operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if both objects are not equal, otherwise returns false
       */
      inline bool operator != (const RequestContextAckRequest& rhs) const;

      /**
       * Less than operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if this object is less than the rhs object
       */
      inline bool operator < (const RequestContextAckRequest& rhs) const;

      /**
       * Creates and returns a clone of this object.
       *
       * This method is intended to be used when the type of the object
       * is not known by the caller, e.g. you have a pointer to a base
       * class. In all other cases you should make use of the copy
       * constructor.
       *
       * @return The clone of this object
       */
      inline ::asf::core::Payload* clone();

      /**
       * Greater than operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if this object is greater than the rhs object
       */
      inline bool operator > (const RequestContextAckRequest& rhs) const;

      /**
       * Clear discards all contents from this structure.
       *
       * All fields will be set to their default value. The hasXXX() methods
       * will return false for all fields.
       */
      inline void clear();

      // ---- Field accessors -------------------------------------------

      // API of field "contextStatus"

      static const int kContextStatus = 0;

      /**
       * Clears the field "contextStatus".
       *
       * The field will be set to it's default value. The hasContextStatus()
       * method will return false.
       */
      inline void clearContextStatus();

      /**
       * Checks whether the field "contextStatus" was set before.
       *
       * @return Returns true if the field was set before (a field get set by calling
       * the setter method setContextStatus()). Otherwise it will return false.
       */
      inline bool hasContextStatus() const;

      /**
       * Returns the value of the member "contextStatus".
       *
       * If the meaning of "contextStatus" isn't clear, then there should be a description here.
       *
       * @return The value of the field "contextStatus"
       */
      inline ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState getContextStatus() const;

      /**
       * Sets the value of the member "contextStatus".
       *
       * If the meaning of "contextStatus" isn't clear, then there should be a description here.
       *
       * @param contextStatus The value which will be set
       */
      inline void setContextStatus(::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState contextStatus_);

      // API of field "targetContext"

      static const int kTargetContext = 1;

      /**
       * Clears the field "targetContext".
       *
       * The field will be set to it's default value. The hasTargetContext()
       * method will return false.
       */
      inline void clearTargetContext();

      /**
       * Checks whether the field "targetContext" was set before.
       *
       * @return Returns true if the field was set before (a field get set by calling
       * the setter method setTargetContext()). Otherwise it will return false.
       */
      inline bool hasTargetContext() const;

      /**
       * Returns the value of the member "targetContext".
       *
       * If the meaning of "targetContext" isn't clear, then there should be a description here.
       *
       * @return The value of the field "targetContext"
       */
      inline uint32 getTargetContext() const;

      /**
       * Sets the value of the member "targetContext".
       *
       * If the meaning of "targetContext" isn't clear, then there should be a description here.
       *
       * @param targetContext The value which will be set
       */
      inline void setTargetContext(uint32 targetContext_);

      static const RequestContextAckRequest& getDefaultInstance();

   private:

      static RequestContextAckRequest* _defaultInstance;

      static void deleteDefaultInstance();

      inline void set_has_contextStatus();

      inline void clear_has_contextStatus();

      inline void set_has_targetContext();

      inline void clear_has_targetContext();

      uint32 _has_bits_[(2 + 31) / 32];

      ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState _contextStatus;

      uint32 _targetContext;

};

/**
 * Indicate a new context request to clients
 */
class VOnNewContextSignal  : public ::asf::core::Payload
{
   public:

      /**
       * Default constructor
       */
      inline  VOnNewContextSignal();

      /**
       * Copy constructor
       */
      inline  VOnNewContextSignal(const VOnNewContextSignal& c);

      /**
       * All fields constructor
       */
      //lint -efunc(1003,[*::]VOnNewContextSignal)
      //lint -efunc(1025,[*::]VOnNewContextSignal)
      //lint -efunc(119,[*::]VOnNewContextSignal)
      inline  VOnNewContextSignal(uint32 sourceContext_, uint32 targetContext_);

      /**
       * Destructor
       */
      inline virtual  ~VOnNewContextSignal();

      /**
       * Assignment operator
       */
      inline VOnNewContextSignal& operator = (const VOnNewContextSignal& rhs);

      /**
       * Equality operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if both objects are equal, otherwise returns false
       */
      inline bool operator == (const VOnNewContextSignal& rhs) const;

      /**
       * Inequality operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if both objects are not equal, otherwise returns false
       */
      inline bool operator != (const VOnNewContextSignal& rhs) const;

      /**
       * Less than operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if this object is less than the rhs object
       */
      inline bool operator < (const VOnNewContextSignal& rhs) const;

      /**
       * Creates and returns a clone of this object.
       *
       * This method is intended to be used when the type of the object
       * is not known by the caller, e.g. you have a pointer to a base
       * class. In all other cases you should make use of the copy
       * constructor.
       *
       * @return The clone of this object
       */
      inline ::asf::core::Payload* clone();

      /**
       * Greater than operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if this object is greater than the rhs object
       */
      inline bool operator > (const VOnNewContextSignal& rhs) const;

      /**
       * Clear discards all contents from this structure.
       *
       * All fields will be set to their default value. The hasXXX() methods
       * will return false for all fields.
       */
      inline void clear();

      // ---- Field accessors -------------------------------------------

      // API of field "sourceContext"

      static const int kSourceContext = 0;

      /**
       * Clears the field "sourceContext".
       *
       * The field will be set to it's default value. The hasSourceContext()
       * method will return false.
       */
      inline void clearSourceContext();

      /**
       * Checks whether the field "sourceContext" was set before.
       *
       * @return Returns true if the field was set before (a field get set by calling
       * the setter method setSourceContext()). Otherwise it will return false.
       */
      inline bool hasSourceContext() const;

      /**
       * Returns the value of the member "sourceContext".
       *
       * If the meaning of "sourceContext" isn't clear, then there should be a description here.
       *
       * @return The value of the field "sourceContext"
       */
      inline uint32 getSourceContext() const;

      /**
       * Sets the value of the member "sourceContext".
       *
       * If the meaning of "sourceContext" isn't clear, then there should be a description here.
       *
       * @param sourceContext The value which will be set
       */
      inline void setSourceContext(uint32 sourceContext_);

      // API of field "targetContext"

      static const int kTargetContext = 1;

      /**
       * Clears the field "targetContext".
       *
       * The field will be set to it's default value. The hasTargetContext()
       * method will return false.
       */
      inline void clearTargetContext();

      /**
       * Checks whether the field "targetContext" was set before.
       *
       * @return Returns true if the field was set before (a field get set by calling
       * the setter method setTargetContext()). Otherwise it will return false.
       */
      inline bool hasTargetContext() const;

      /**
       * Returns the value of the member "targetContext".
       *
       * If the meaning of "targetContext" isn't clear, then there should be a description here.
       *
       * @return The value of the field "targetContext"
       */
      inline uint32 getTargetContext() const;

      /**
       * Sets the value of the member "targetContext".
       *
       * If the meaning of "targetContext" isn't clear, then there should be a description here.
       *
       * @param targetContext The value which will be set
       */
      inline void setTargetContext(uint32 targetContext_);

      static const VOnNewContextSignal& getDefaultInstance();

   private:

      static VOnNewContextSignal* _defaultInstance;

      static void deleteDefaultInstance();

      inline void set_has_sourceContext();

      inline void clear_has_sourceContext();

      inline void set_has_targetContext();

      inline void clear_has_targetContext();

      uint32 _has_bits_[(2 + 31) / 32];

      uint32 _sourceContext;

      uint32 _targetContext;

};

/**
 * Indicate the current active context
 */
class ActiveContextSignal  : public ::asf::core::Payload
{
   public:

      /**
       * Default constructor
       */
      inline  ActiveContextSignal();

      /**
       * Copy constructor
       */
      inline  ActiveContextSignal(const ActiveContextSignal& c);

      /**
       * All fields constructor
       */
      //lint -efunc(1003,[*::]ActiveContextSignal)
      //lint -efunc(1025,[*::]ActiveContextSignal)
      //lint -efunc(119,[*::]ActiveContextSignal)
      inline  ActiveContextSignal(uint32 context_, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState contextStatus_);

      /**
       * Destructor
       */
      inline virtual  ~ActiveContextSignal();

      /**
       * Assignment operator
       */
      inline ActiveContextSignal& operator = (const ActiveContextSignal& rhs);

      /**
       * Equality operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if both objects are equal, otherwise returns false
       */
      inline bool operator == (const ActiveContextSignal& rhs) const;

      /**
       * Inequality operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if both objects are not equal, otherwise returns false
       */
      inline bool operator != (const ActiveContextSignal& rhs) const;

      /**
       * Less than operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if this object is less than the rhs object
       */
      inline bool operator < (const ActiveContextSignal& rhs) const;

      /**
       * Creates and returns a clone of this object.
       *
       * This method is intended to be used when the type of the object
       * is not known by the caller, e.g. you have a pointer to a base
       * class. In all other cases you should make use of the copy
       * constructor.
       *
       * @return The clone of this object
       */
      inline ::asf::core::Payload* clone();

      /**
       * Greater than operator
       *
       * @param rhs Ride hand side object of the comparison
       *
       * @return True if this object is greater than the rhs object
       */
      inline bool operator > (const ActiveContextSignal& rhs) const;

      /**
       * Clear discards all contents from this structure.
       *
       * All fields will be set to their default value. The hasXXX() methods
       * will return false for all fields.
       */
      inline void clear();

      // ---- Field accessors -------------------------------------------

      // API of field "context"

      static const int kContext = 0;

      /**
       * Clears the field "context".
       *
       * The field will be set to it's default value. The hasContext()
       * method will return false.
       */
      inline void clearContext();

      /**
       * Checks whether the field "context" was set before.
       *
       * @return Returns true if the field was set before (a field get set by calling
       * the setter method setContext()). Otherwise it will return false.
       */
      inline bool hasContext() const;

      /**
       * Returns the value of the member "context".
       *
       * If the meaning of "context" isn't clear, then there should be a description here.
       *
       * @return The value of the field "context"
       */
      inline uint32 getContext() const;

      /**
       * Sets the value of the member "context".
       *
       * If the meaning of "context" isn't clear, then there should be a description here.
       *
       * @param context The value which will be set
       */
      inline void setContext(uint32 context_);

      // API of field "contextStatus"

      static const int kContextStatus = 1;

      /**
       * Clears the field "contextStatus".
       *
       * The field will be set to it's default value. The hasContextStatus()
       * method will return false.
       */
      inline void clearContextStatus();

      /**
       * Checks whether the field "contextStatus" was set before.
       *
       * @return Returns true if the field was set before (a field get set by calling
       * the setter method setContextStatus()). Otherwise it will return false.
       */
      inline bool hasContextStatus() const;

      /**
       * Returns the value of the member "contextStatus".
       *
       * If the meaning of "contextStatus" isn't clear, then there should be a description here.
       *
       * @return The value of the field "contextStatus"
       */
      inline ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState getContextStatus() const;

      /**
       * Sets the value of the member "contextStatus".
       *
       * If the meaning of "contextStatus" isn't clear, then there should be a description here.
       *
       * @param contextStatus The value which will be set
       */
      inline void setContextStatus(::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState contextStatus_);

      static const ActiveContextSignal& getDefaultInstance();

   private:

      static ActiveContextSignal* _defaultInstance;

      static void deleteDefaultInstance();

      inline void set_has_context();

      inline void clear_has_context();

      inline void set_has_contextStatus();

      inline void clear_has_contextStatus();

      uint32 _has_bits_[(2 + 31) / 32];

      uint32 _context;

      ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState _contextStatus;

};

inline  RequestContextRequest::RequestContextRequest()  :
   _sourceContext(((uint32) 0)),
   _targetContext(((uint32) 0))
{
   ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

inline  RequestContextRequest::RequestContextRequest(const RequestContextRequest& c)  :
   _sourceContext(((uint32) 0)),
   _targetContext(((uint32) 0))
{
   ::memset(_has_bits_, 0, sizeof(_has_bits_));
   *this = c;
}

inline  RequestContextRequest::RequestContextRequest(uint32 sourceContext_, uint32 targetContext_)  :
   _sourceContext(sourceContext_),
   _targetContext(targetContext_)
{
   ::memset(_has_bits_, 0xFFu, sizeof(_has_bits_));
}

inline  RequestContextRequest::~RequestContextRequest()
{
}

inline RequestContextRequest& RequestContextRequest::operator = (const RequestContextRequest& rhs)
{
   if (rhs.hasSourceContext())
   {
      setSourceContext(rhs.getSourceContext());
   }
   else
   {
      clearSourceContext();
   }
   if (rhs.hasTargetContext())
   {
      setTargetContext(rhs.getTargetContext());
   }
   else
   {
      clearTargetContext();
   }
   return *this;
}

inline bool RequestContextRequest::operator == (const RequestContextRequest& rhs) const
{
   return (((!hasSourceContext() && !rhs.hasSourceContext()) || getSourceContext() == rhs.getSourceContext()) &&
           ((!hasTargetContext() && !rhs.hasTargetContext()) || getTargetContext() == rhs.getTargetContext()));
}

inline bool RequestContextRequest::operator != (const RequestContextRequest& rhs) const
{
   return !(*this == rhs);
}

inline bool RequestContextRequest::operator < (const RequestContextRequest& rhs) const
{
   if (hasSourceContext() || rhs.hasSourceContext())
   {
      if (getSourceContext() < rhs.getSourceContext())
      {
         return true;
      }
      if (getSourceContext() > rhs.getSourceContext())
      {
         return false;
      }
   }
   if (hasTargetContext() || rhs.hasTargetContext())
   {
      if (getTargetContext() < rhs.getTargetContext())
      {
         return true;
      }
      if (getTargetContext() > rhs.getTargetContext())
      {
         return false;
      }
   }
   return false;
}

inline ::asf::core::Payload* RequestContextRequest::clone()
{
   return new RequestContextRequest(*this);
}

inline bool RequestContextRequest::operator > (const RequestContextRequest& rhs) const
{
   return (rhs < *this && *this != rhs);
}

inline void RequestContextRequest::clear()
{
   clearSourceContext();
   clearTargetContext();
}

inline void RequestContextRequest::clearSourceContext()
{
   if (hasSourceContext())
   {
      clear_has_sourceContext();
      _sourceContext = uint32(((uint32) 0));
   }
}

inline bool RequestContextRequest::hasSourceContext() const
{
   return (_has_bits_[0] & (1 << 0)) > 0;
}

inline uint32 RequestContextRequest::getSourceContext() const
{
   return _sourceContext;
}

inline void RequestContextRequest::setSourceContext(uint32 sourceContext_)
{
   set_has_sourceContext();
   this->_sourceContext = sourceContext_;
}

inline void RequestContextRequest::clearTargetContext()
{
   if (hasTargetContext())
   {
      clear_has_targetContext();
      _targetContext = uint32(((uint32) 0));
   }
}

inline bool RequestContextRequest::hasTargetContext() const
{
   return (_has_bits_[0] & (1 << 1)) > 0;
}

inline uint32 RequestContextRequest::getTargetContext() const
{
   return _targetContext;
}

inline void RequestContextRequest::setTargetContext(uint32 targetContext_)
{
   set_has_targetContext();
   this->_targetContext = targetContext_;
}

inline void RequestContextRequest::set_has_sourceContext()
{
   _has_bits_[0] |= 1 << 0;
}

inline void RequestContextRequest::clear_has_sourceContext()
{
   _has_bits_[0] &= ~(1 << 0);
}

inline void RequestContextRequest::set_has_targetContext()
{
   _has_bits_[0] |= 1 << 1;
}

inline void RequestContextRequest::clear_has_targetContext()
{
   _has_bits_[0] &= ~(1 << 1);
}

inline  RequestContextResponse::RequestContextResponse()  :
   _bValidContext(((uint32) 0))
{
   ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

inline  RequestContextResponse::RequestContextResponse(const RequestContextResponse& c)  :
   _bValidContext(((uint32) 0))
{
   ::memset(_has_bits_, 0, sizeof(_has_bits_));
   *this = c;
}

inline  RequestContextResponse::RequestContextResponse(uint32 bValidContext_)  :
   _bValidContext(bValidContext_)
{
   ::memset(_has_bits_, 0xFFu, sizeof(_has_bits_));
}

inline  RequestContextResponse::~RequestContextResponse()
{
}

inline RequestContextResponse& RequestContextResponse::operator = (const RequestContextResponse& rhs)
{
   if (rhs.hasBValidContext())
   {
      setBValidContext(rhs.getBValidContext());
   }
   else
   {
      clearBValidContext();
   }
   return *this;
}

inline bool RequestContextResponse::operator == (const RequestContextResponse& rhs) const
{
   return (((!hasBValidContext() && !rhs.hasBValidContext()) || getBValidContext() == rhs.getBValidContext()));
}

inline bool RequestContextResponse::operator != (const RequestContextResponse& rhs) const
{
   return !(*this == rhs);
}

inline bool RequestContextResponse::operator < (const RequestContextResponse& rhs) const
{
   if (hasBValidContext() || rhs.hasBValidContext())
   {
      if (getBValidContext() < rhs.getBValidContext())
      {
         return true;
      }
      if (getBValidContext() > rhs.getBValidContext())
      {
         return false;
      }
   }
   return false;
}

inline ::asf::core::Payload* RequestContextResponse::clone()
{
   return new RequestContextResponse(*this);
}

inline bool RequestContextResponse::operator > (const RequestContextResponse& rhs) const
{
   return (rhs < *this && *this != rhs);
}

inline void RequestContextResponse::clear()
{
   clearBValidContext();
}

inline void RequestContextResponse::clearBValidContext()
{
   if (hasBValidContext())
   {
      clear_has_bValidContext();
      _bValidContext = uint32(((uint32) 0));
   }
}

inline bool RequestContextResponse::hasBValidContext() const
{
   return (_has_bits_[0] & (1 << 0)) > 0;
}

inline uint32 RequestContextResponse::getBValidContext() const
{
   return _bValidContext;
}

inline void RequestContextResponse::setBValidContext(uint32 bValidContext_)
{
   set_has_bValidContext();
   this->_bValidContext = bValidContext_;
}

inline void RequestContextResponse::set_has_bValidContext()
{
   _has_bits_[0] |= 1 << 0;
}

inline void RequestContextResponse::clear_has_bValidContext()
{
   _has_bits_[0] &= ~(1 << 0);
}

inline  SetAvailableContextsRequest::SetAvailableContextsRequest()  :
   _availableContext()
{
   ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

inline  SetAvailableContextsRequest::SetAvailableContextsRequest(const SetAvailableContextsRequest& c)  :
   _availableContext()
{
   ::memset(_has_bits_, 0, sizeof(_has_bits_));
   *this = c;
}

inline  SetAvailableContextsRequest::SetAvailableContextsRequest(const ::std::vector< uint32 >& availableContext_)  :
   _availableContext(availableContext_)
{
   ::memset(_has_bits_, 0xFFu, sizeof(_has_bits_));
}

inline  SetAvailableContextsRequest::~SetAvailableContextsRequest()
{
}

inline SetAvailableContextsRequest& SetAvailableContextsRequest::operator = (const SetAvailableContextsRequest& rhs)
{
   if (rhs.hasAvailableContext())
   {
      setAvailableContext(rhs.getAvailableContext());
   }
   else
   {
      clearAvailableContext();
   }
   return *this;
}

inline bool SetAvailableContextsRequest::operator == (const SetAvailableContextsRequest& rhs) const
{
   return (((!hasAvailableContext() && !rhs.hasAvailableContext()) || getAvailableContext() == rhs.getAvailableContext()));
}

inline bool SetAvailableContextsRequest::operator != (const SetAvailableContextsRequest& rhs) const
{
   return !(*this == rhs);
}

inline bool SetAvailableContextsRequest::operator < (const SetAvailableContextsRequest& rhs) const
{
   if (hasAvailableContext() || rhs.hasAvailableContext())
   {
      if (getAvailableContext() < rhs.getAvailableContext())
      {
         return true;
      }
      if (getAvailableContext() > rhs.getAvailableContext())
      {
         return false;
      }
   }
   return false;
}

inline ::asf::core::Payload* SetAvailableContextsRequest::clone()
{
   return new SetAvailableContextsRequest(*this);
}

inline bool SetAvailableContextsRequest::operator > (const SetAvailableContextsRequest& rhs) const
{
   return (rhs < *this && *this != rhs);
}

inline void SetAvailableContextsRequest::clear()
{
   clearAvailableContext();
}

inline void SetAvailableContextsRequest::clearAvailableContext()
{
   if (hasAvailableContext())
   {
      clear_has_availableContext();
      _availableContext.clear();
   }
}

inline bool SetAvailableContextsRequest::hasAvailableContext() const
{
   return (_has_bits_[0] & (1 << 0)) > 0;
}

inline const ::std::vector< uint32 >& SetAvailableContextsRequest::getAvailableContext() const
{
   return _availableContext;
}

inline ::std::vector< uint32 >& SetAvailableContextsRequest::getAvailableContextMutable()
{
   set_has_availableContext();
   return _availableContext;
}

inline void SetAvailableContextsRequest::setAvailableContext(const ::std::vector< uint32 >& availableContext_)
{
   set_has_availableContext();
   this->_availableContext = availableContext_;
}

inline void SetAvailableContextsRequest::set_has_availableContext()
{
   _has_bits_[0] |= 1 << 0;
}

inline void SetAvailableContextsRequest::clear_has_availableContext()
{
   _has_bits_[0] &= ~(1 << 0);
}

inline  SwitchAppRequest::SwitchAppRequest()  :
   _applicationId(((uint32) 0)),
   _priority(((uint32) 0)),
   _targetContext(((uint32) 0))
{
   ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

inline  SwitchAppRequest::SwitchAppRequest(const SwitchAppRequest& c)  :
   _applicationId(((uint32) 0)),
   _priority(((uint32) 0)),
   _targetContext(((uint32) 0))
{
   ::memset(_has_bits_, 0, sizeof(_has_bits_));
   *this = c;
}

inline  SwitchAppRequest::SwitchAppRequest(uint32 applicationId_, uint32 priority_, uint32 targetContext_)  :
   _applicationId(applicationId_),
   _priority(priority_),
   _targetContext(targetContext_)
{
   ::memset(_has_bits_, 0xFFu, sizeof(_has_bits_));
}

inline  SwitchAppRequest::~SwitchAppRequest()
{
}

inline SwitchAppRequest& SwitchAppRequest::operator = (const SwitchAppRequest& rhs)
{
   if (rhs.hasApplicationId())
   {
      setApplicationId(rhs.getApplicationId());
   }
   else
   {
      clearApplicationId();
   }
   if (rhs.hasPriority())
   {
      setPriority(rhs.getPriority());
   }
   else
   {
      clearPriority();
   }
   if (rhs.hasTargetContext())
   {
      setTargetContext(rhs.getTargetContext());
   }
   else
   {
      clearTargetContext();
   }
   return *this;
}

inline bool SwitchAppRequest::operator == (const SwitchAppRequest& rhs) const
{
   return (((!hasApplicationId() && !rhs.hasApplicationId()) || getApplicationId() == rhs.getApplicationId()) &&
           ((!hasPriority() && !rhs.hasPriority()) || getPriority() == rhs.getPriority()) &&
           ((!hasTargetContext() && !rhs.hasTargetContext()) || getTargetContext() == rhs.getTargetContext()));
}

inline bool SwitchAppRequest::operator != (const SwitchAppRequest& rhs) const
{
   return !(*this == rhs);
}

inline bool SwitchAppRequest::operator < (const SwitchAppRequest& rhs) const
{
   if (hasApplicationId() || rhs.hasApplicationId())
   {
      if (getApplicationId() < rhs.getApplicationId())
      {
         return true;
      }
      if (getApplicationId() > rhs.getApplicationId())
      {
         return false;
      }
   }
   if (hasPriority() || rhs.hasPriority())
   {
      if (getPriority() < rhs.getPriority())
      {
         return true;
      }
      if (getPriority() > rhs.getPriority())
      {
         return false;
      }
   }
   if (hasTargetContext() || rhs.hasTargetContext())
   {
      if (getTargetContext() < rhs.getTargetContext())
      {
         return true;
      }
      if (getTargetContext() > rhs.getTargetContext())
      {
         return false;
      }
   }
   return false;
}

inline ::asf::core::Payload* SwitchAppRequest::clone()
{
   return new SwitchAppRequest(*this);
}

inline bool SwitchAppRequest::operator > (const SwitchAppRequest& rhs) const
{
   return (rhs < *this && *this != rhs);
}

inline void SwitchAppRequest::clear()
{
   clearApplicationId();
   clearPriority();
   clearTargetContext();
}

inline void SwitchAppRequest::clearApplicationId()
{
   if (hasApplicationId())
   {
      clear_has_applicationId();
      _applicationId = uint32(((uint32) 0));
   }
}

inline bool SwitchAppRequest::hasApplicationId() const
{
   return (_has_bits_[0] & (1 << 0)) > 0;
}

inline uint32 SwitchAppRequest::getApplicationId() const
{
   return _applicationId;
}

inline void SwitchAppRequest::setApplicationId(uint32 applicationId_)
{
   set_has_applicationId();
   this->_applicationId = applicationId_;
}

inline void SwitchAppRequest::clearPriority()
{
   if (hasPriority())
   {
      clear_has_priority();
      _priority = uint32(((uint32) 0));
   }
}

inline bool SwitchAppRequest::hasPriority() const
{
   return (_has_bits_[0] & (1 << 1)) > 0;
}

inline uint32 SwitchAppRequest::getPriority() const
{
   return _priority;
}

inline void SwitchAppRequest::setPriority(uint32 priority_)
{
   set_has_priority();
   this->_priority = priority_;
}

inline void SwitchAppRequest::clearTargetContext()
{
   if (hasTargetContext())
   {
      clear_has_targetContext();
      _targetContext = uint32(((uint32) 0));
   }
}

inline bool SwitchAppRequest::hasTargetContext() const
{
   return (_has_bits_[0] & (1 << 2)) > 0;
}

inline uint32 SwitchAppRequest::getTargetContext() const
{
   return _targetContext;
}

inline void SwitchAppRequest::setTargetContext(uint32 targetContext_)
{
   set_has_targetContext();
   this->_targetContext = targetContext_;
}

inline void SwitchAppRequest::set_has_applicationId()
{
   _has_bits_[0] |= 1 << 0;
}

inline void SwitchAppRequest::clear_has_applicationId()
{
   _has_bits_[0] &= ~(1 << 0);
}

inline void SwitchAppRequest::set_has_priority()
{
   _has_bits_[0] |= 1 << 1;
}

inline void SwitchAppRequest::clear_has_priority()
{
   _has_bits_[0] &= ~(1 << 1);
}

inline void SwitchAppRequest::set_has_targetContext()
{
   _has_bits_[0] |= 1 << 2;
}

inline void SwitchAppRequest::clear_has_targetContext()
{
   _has_bits_[0] &= ~(1 << 2);
}

inline  SwitchAppResponse::SwitchAppResponse()  :
   _bAppSwitched(((uint32) 0))
{
   ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

inline  SwitchAppResponse::SwitchAppResponse(const SwitchAppResponse& c)  :
   _bAppSwitched(((uint32) 0))
{
   ::memset(_has_bits_, 0, sizeof(_has_bits_));
   *this = c;
}

inline  SwitchAppResponse::SwitchAppResponse(uint32 bAppSwitched_)  :
   _bAppSwitched(bAppSwitched_)
{
   ::memset(_has_bits_, 0xFFu, sizeof(_has_bits_));
}

inline  SwitchAppResponse::~SwitchAppResponse()
{
}

inline SwitchAppResponse& SwitchAppResponse::operator = (const SwitchAppResponse& rhs)
{
   if (rhs.hasBAppSwitched())
   {
      setBAppSwitched(rhs.getBAppSwitched());
   }
   else
   {
      clearBAppSwitched();
   }
   return *this;
}

inline bool SwitchAppResponse::operator == (const SwitchAppResponse& rhs) const
{
   return (((!hasBAppSwitched() && !rhs.hasBAppSwitched()) || getBAppSwitched() == rhs.getBAppSwitched()));
}

inline bool SwitchAppResponse::operator != (const SwitchAppResponse& rhs) const
{
   return !(*this == rhs);
}

inline bool SwitchAppResponse::operator < (const SwitchAppResponse& rhs) const
{
   if (hasBAppSwitched() || rhs.hasBAppSwitched())
   {
      if (getBAppSwitched() < rhs.getBAppSwitched())
      {
         return true;
      }
      if (getBAppSwitched() > rhs.getBAppSwitched())
      {
         return false;
      }
   }
   return false;
}

inline ::asf::core::Payload* SwitchAppResponse::clone()
{
   return new SwitchAppResponse(*this);
}

inline bool SwitchAppResponse::operator > (const SwitchAppResponse& rhs) const
{
   return (rhs < *this && *this != rhs);
}

inline void SwitchAppResponse::clear()
{
   clearBAppSwitched();
}

inline void SwitchAppResponse::clearBAppSwitched()
{
   if (hasBAppSwitched())
   {
      clear_has_bAppSwitched();
      _bAppSwitched = uint32(((uint32) 0));
   }
}

inline bool SwitchAppResponse::hasBAppSwitched() const
{
   return (_has_bits_[0] & (1 << 0)) > 0;
}

inline uint32 SwitchAppResponse::getBAppSwitched() const
{
   return _bAppSwitched;
}

inline void SwitchAppResponse::setBAppSwitched(uint32 bAppSwitched_)
{
   set_has_bAppSwitched();
   this->_bAppSwitched = bAppSwitched_;
}

inline void SwitchAppResponse::set_has_bAppSwitched()
{
   _has_bits_[0] |= 1 << 0;
}

inline void SwitchAppResponse::clear_has_bAppSwitched()
{
   _has_bits_[0] &= ~(1 << 0);
}

inline  RequestContextAckRequest::RequestContextAckRequest()  :
   _contextStatus(::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState__ACCEPTED),
   _targetContext(((uint32) 0))
{
   ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

inline  RequestContextAckRequest::RequestContextAckRequest(const RequestContextAckRequest& c)  :
   _contextStatus(::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState__ACCEPTED),
   _targetContext(((uint32) 0))
{
   ::memset(_has_bits_, 0, sizeof(_has_bits_));
   *this = c;
}

inline  RequestContextAckRequest::RequestContextAckRequest(::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState contextStatus_, uint32 targetContext_)  :
   _contextStatus(contextStatus_),
   _targetContext(targetContext_)
{
   {
      LOG_SYSTEM_LOGGER(); //lint !e578   suppress "symbol _logger hides symbol _logger"
      LOG_ASSERT_MSG_STATIC(::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState_IsValid(contextStatus_), "Invalid value %d for enumeration '%s' in field '%s' of '%s'", contextStatus_, "ContextState", "contextStatus", "RequestContextAckRequest");
   }
   ::memset(_has_bits_, 0xFFu, sizeof(_has_bits_));
}

inline  RequestContextAckRequest::~RequestContextAckRequest()
{
}

inline RequestContextAckRequest& RequestContextAckRequest::operator = (const RequestContextAckRequest& rhs)
{
   if (rhs.hasContextStatus())
   {
      setContextStatus(rhs.getContextStatus());
   }
   else
   {
      clearContextStatus();
   }
   if (rhs.hasTargetContext())
   {
      setTargetContext(rhs.getTargetContext());
   }
   else
   {
      clearTargetContext();
   }
   return *this;
}

inline bool RequestContextAckRequest::operator == (const RequestContextAckRequest& rhs) const
{
   return (((!hasContextStatus() && !rhs.hasContextStatus()) || getContextStatus() == rhs.getContextStatus()) &&
           ((!hasTargetContext() && !rhs.hasTargetContext()) || getTargetContext() == rhs.getTargetContext()));
}

inline bool RequestContextAckRequest::operator != (const RequestContextAckRequest& rhs) const
{
   return !(*this == rhs);
}

inline bool RequestContextAckRequest::operator < (const RequestContextAckRequest& rhs) const
{
   if (hasContextStatus() || rhs.hasContextStatus())
   {
      if ((uint32) getContextStatus() < (uint32) rhs.getContextStatus())
      {
         return true;
      }
      if ((uint32) getContextStatus() > (uint32) rhs.getContextStatus())
      {
         return false;
      }
   }
   if (hasTargetContext() || rhs.hasTargetContext())
   {
      if (getTargetContext() < rhs.getTargetContext())
      {
         return true;
      }
      if (getTargetContext() > rhs.getTargetContext())
      {
         return false;
      }
   }
   return false;
}

inline ::asf::core::Payload* RequestContextAckRequest::clone()
{
   return new RequestContextAckRequest(*this);
}

inline bool RequestContextAckRequest::operator > (const RequestContextAckRequest& rhs) const
{
   return (rhs < *this && *this != rhs);
}

inline void RequestContextAckRequest::clear()
{
   clearContextStatus();
   clearTargetContext();
}

inline void RequestContextAckRequest::clearContextStatus()
{
   if (hasContextStatus())
   {
      clear_has_contextStatus();
      _contextStatus = ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState(::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState__ACCEPTED);
   }
}

inline bool RequestContextAckRequest::hasContextStatus() const
{
   return (_has_bits_[0] & (1 << 0)) > 0;
}

inline ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState RequestContextAckRequest::getContextStatus() const
{
   return _contextStatus;
}

inline void RequestContextAckRequest::setContextStatus(::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState contextStatus_)
{
   {
      LOG_SYSTEM_LOGGER(); //lint !e578   suppress "symbol _logger hides symbol _logger"
      LOG_ASSERT_MSG_STATIC(::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState_IsValid(contextStatus_), "Invalid value %d for enumeration '%s' in field '%s' of '%s'", contextStatus_, "ContextState", "contextStatus", "RequestContextAckRequest");
   }
   set_has_contextStatus();
   this->_contextStatus = contextStatus_;
}

inline void RequestContextAckRequest::clearTargetContext()
{
   if (hasTargetContext())
   {
      clear_has_targetContext();
      _targetContext = uint32(((uint32) 0));
   }
}

inline bool RequestContextAckRequest::hasTargetContext() const
{
   return (_has_bits_[0] & (1 << 1)) > 0;
}

inline uint32 RequestContextAckRequest::getTargetContext() const
{
   return _targetContext;
}

inline void RequestContextAckRequest::setTargetContext(uint32 targetContext_)
{
   set_has_targetContext();
   this->_targetContext = targetContext_;
}

inline void RequestContextAckRequest::set_has_contextStatus()
{
   _has_bits_[0] |= 1 << 0;
}

inline void RequestContextAckRequest::clear_has_contextStatus()
{
   _has_bits_[0] &= ~(1 << 0);
}

inline void RequestContextAckRequest::set_has_targetContext()
{
   _has_bits_[0] |= 1 << 1;
}

inline void RequestContextAckRequest::clear_has_targetContext()
{
   _has_bits_[0] &= ~(1 << 1);
}

inline  VOnNewContextSignal::VOnNewContextSignal()  :
   _sourceContext(((uint32) 0)),
   _targetContext(((uint32) 0))
{
   ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

inline  VOnNewContextSignal::VOnNewContextSignal(const VOnNewContextSignal& c)  :
   _sourceContext(((uint32) 0)),
   _targetContext(((uint32) 0))
{
   ::memset(_has_bits_, 0, sizeof(_has_bits_));
   *this = c;
}

inline  VOnNewContextSignal::VOnNewContextSignal(uint32 sourceContext_, uint32 targetContext_)  :
   _sourceContext(sourceContext_),
   _targetContext(targetContext_)
{
   ::memset(_has_bits_, 0xFFu, sizeof(_has_bits_));
}

inline  VOnNewContextSignal::~VOnNewContextSignal()
{
}

inline VOnNewContextSignal& VOnNewContextSignal::operator = (const VOnNewContextSignal& rhs)
{
   if (rhs.hasSourceContext())
   {
      setSourceContext(rhs.getSourceContext());
   }
   else
   {
      clearSourceContext();
   }
   if (rhs.hasTargetContext())
   {
      setTargetContext(rhs.getTargetContext());
   }
   else
   {
      clearTargetContext();
   }
   return *this;
}

inline bool VOnNewContextSignal::operator == (const VOnNewContextSignal& rhs) const
{
   return (((!hasSourceContext() && !rhs.hasSourceContext()) || getSourceContext() == rhs.getSourceContext()) &&
           ((!hasTargetContext() && !rhs.hasTargetContext()) || getTargetContext() == rhs.getTargetContext()));
}

inline bool VOnNewContextSignal::operator != (const VOnNewContextSignal& rhs) const
{
   return !(*this == rhs);
}

inline bool VOnNewContextSignal::operator < (const VOnNewContextSignal& rhs) const
{
   if (hasSourceContext() || rhs.hasSourceContext())
   {
      if (getSourceContext() < rhs.getSourceContext())
      {
         return true;
      }
      if (getSourceContext() > rhs.getSourceContext())
      {
         return false;
      }
   }
   if (hasTargetContext() || rhs.hasTargetContext())
   {
      if (getTargetContext() < rhs.getTargetContext())
      {
         return true;
      }
      if (getTargetContext() > rhs.getTargetContext())
      {
         return false;
      }
   }
   return false;
}

inline ::asf::core::Payload* VOnNewContextSignal::clone()
{
   return new VOnNewContextSignal(*this);
}

inline bool VOnNewContextSignal::operator > (const VOnNewContextSignal& rhs) const
{
   return (rhs < *this && *this != rhs);
}

inline void VOnNewContextSignal::clear()
{
   clearSourceContext();
   clearTargetContext();
}

inline void VOnNewContextSignal::clearSourceContext()
{
   if (hasSourceContext())
   {
      clear_has_sourceContext();
      _sourceContext = uint32(((uint32) 0));
   }
}

inline bool VOnNewContextSignal::hasSourceContext() const
{
   return (_has_bits_[0] & (1 << 0)) > 0;
}

inline uint32 VOnNewContextSignal::getSourceContext() const
{
   return _sourceContext;
}

inline void VOnNewContextSignal::setSourceContext(uint32 sourceContext_)
{
   set_has_sourceContext();
   this->_sourceContext = sourceContext_;
}

inline void VOnNewContextSignal::clearTargetContext()
{
   if (hasTargetContext())
   {
      clear_has_targetContext();
      _targetContext = uint32(((uint32) 0));
   }
}

inline bool VOnNewContextSignal::hasTargetContext() const
{
   return (_has_bits_[0] & (1 << 1)) > 0;
}

inline uint32 VOnNewContextSignal::getTargetContext() const
{
   return _targetContext;
}

inline void VOnNewContextSignal::setTargetContext(uint32 targetContext_)
{
   set_has_targetContext();
   this->_targetContext = targetContext_;
}

inline void VOnNewContextSignal::set_has_sourceContext()
{
   _has_bits_[0] |= 1 << 0;
}

inline void VOnNewContextSignal::clear_has_sourceContext()
{
   _has_bits_[0] &= ~(1 << 0);
}

inline void VOnNewContextSignal::set_has_targetContext()
{
   _has_bits_[0] |= 1 << 1;
}

inline void VOnNewContextSignal::clear_has_targetContext()
{
   _has_bits_[0] &= ~(1 << 1);
}

inline  ActiveContextSignal::ActiveContextSignal()  :
   _context(((uint32) 0)),
   _contextStatus(::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState__ACCEPTED)
{
   ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

inline  ActiveContextSignal::ActiveContextSignal(const ActiveContextSignal& c)  :
   _context(((uint32) 0)),
   _contextStatus(::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState__ACCEPTED)
{
   ::memset(_has_bits_, 0, sizeof(_has_bits_));
   *this = c;
}

inline  ActiveContextSignal::ActiveContextSignal(uint32 context_, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState contextStatus_)  :
   _context(context_),
   _contextStatus(contextStatus_)
{
   {
      LOG_SYSTEM_LOGGER(); //lint !e578   suppress "symbol _logger hides symbol _logger"
      LOG_ASSERT_MSG_STATIC(::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState_IsValid(contextStatus_), "Invalid value %d for enumeration '%s' in field '%s' of '%s'", contextStatus_, "ContextState", "contextStatus", "ActiveContextSignal");
   }
   ::memset(_has_bits_, 0xFFu, sizeof(_has_bits_));
}

inline  ActiveContextSignal::~ActiveContextSignal()
{
}

inline ActiveContextSignal& ActiveContextSignal::operator = (const ActiveContextSignal& rhs)
{
   if (rhs.hasContext())
   {
      setContext(rhs.getContext());
   }
   else
   {
      clearContext();
   }
   if (rhs.hasContextStatus())
   {
      setContextStatus(rhs.getContextStatus());
   }
   else
   {
      clearContextStatus();
   }
   return *this;
}

inline bool ActiveContextSignal::operator == (const ActiveContextSignal& rhs) const
{
   return (((!hasContext() && !rhs.hasContext()) || getContext() == rhs.getContext()) &&
           ((!hasContextStatus() && !rhs.hasContextStatus()) || getContextStatus() == rhs.getContextStatus()));
}

inline bool ActiveContextSignal::operator != (const ActiveContextSignal& rhs) const
{
   return !(*this == rhs);
}

inline bool ActiveContextSignal::operator < (const ActiveContextSignal& rhs) const
{
   if (hasContext() || rhs.hasContext())
   {
      if (getContext() < rhs.getContext())
      {
         return true;
      }
      if (getContext() > rhs.getContext())
      {
         return false;
      }
   }
   if (hasContextStatus() || rhs.hasContextStatus())
   {
      if ((uint32) getContextStatus() < (uint32) rhs.getContextStatus())
      {
         return true;
      }
      if ((uint32) getContextStatus() > (uint32) rhs.getContextStatus())
      {
         return false;
      }
   }
   return false;
}

inline ::asf::core::Payload* ActiveContextSignal::clone()
{
   return new ActiveContextSignal(*this);
}

inline bool ActiveContextSignal::operator > (const ActiveContextSignal& rhs) const
{
   return (rhs < *this && *this != rhs);
}

inline void ActiveContextSignal::clear()
{
   clearContext();
   clearContextStatus();
}

inline void ActiveContextSignal::clearContext()
{
   if (hasContext())
   {
      clear_has_context();
      _context = uint32(((uint32) 0));
   }
}

inline bool ActiveContextSignal::hasContext() const
{
   return (_has_bits_[0] & (1 << 0)) > 0;
}

inline uint32 ActiveContextSignal::getContext() const
{
   return _context;
}

inline void ActiveContextSignal::setContext(uint32 context_)
{
   set_has_context();
   this->_context = context_;
}

inline void ActiveContextSignal::clearContextStatus()
{
   if (hasContextStatus())
   {
      clear_has_contextStatus();
      _contextStatus = ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState(::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState__ACCEPTED);
   }
}

inline bool ActiveContextSignal::hasContextStatus() const
{
   return (_has_bits_[0] & (1 << 1)) > 0;
}

inline ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState ActiveContextSignal::getContextStatus() const
{
   return _contextStatus;
}

inline void ActiveContextSignal::setContextStatus(::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState contextStatus_)
{
   {
      LOG_SYSTEM_LOGGER(); //lint !e578   suppress "symbol _logger hides symbol _logger"
      LOG_ASSERT_MSG_STATIC(::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState_IsValid(contextStatus_), "Invalid value %d for enumeration '%s' in field '%s' of '%s'", contextStatus_, "ContextState", "contextStatus", "ActiveContextSignal");
   }
   set_has_contextStatus();
   this->_contextStatus = contextStatus_;
}

inline void ActiveContextSignal::set_has_context()
{
   _has_bits_[0] |= 1 << 0;
}

inline void ActiveContextSignal::clear_has_context()
{
   _has_bits_[0] &= ~(1 << 0);
}

inline void ActiveContextSignal::set_has_contextStatus()
{
   _has_bits_[0] |= 1 << 1;
}

inline void ActiveContextSignal::clear_has_contextStatus()
{
   _has_bits_[0] &= ~(1 << 1);
}

} // namespace ContextSwitch
} // namespace contextswitchservice
} // namespace hmi
} // namespace nissan
} // namespace ai
} // namespace cm
} // namespace bosch

#endif // BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCH_H
