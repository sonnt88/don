///////////////////////////////////////////////////////////
//  clSpy_ContextRequestor.h
//  Implementation of the Class clSpy_ContextRequestor
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(clSpy_ContextRequestor_h)
#define clSpy_ContextRequestor_h

/**
 * interface class for context proxies to provide contexts to requestors
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 5:44:42 PM
 */

#include "clContextRequestor.h"
#include "clContext.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"
#include "asf/core/Types.h"


using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;


class clSpy_ContextRequestor : public clContextRequestor
{

   public:
      clSpy_ContextRequestor() {};
      virtual ~clSpy_ContextRequestor() {};

      MOCK_METHOD1(vOnRequestResponse, void(ContextState enState));

};
#endif // !defined(EA_FAF598AF_B4F9_4bb1_91CE_ABBA6F93217B__INCLUDED_)
