/**************************************************************************//**
* \file     clContextProxy_Test.cpp
*
*           clContextProxy_Test method class implementation
*
* \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
******************************************************************************/

#include "clContextProxy_Test.h"
#include "clContextProxy.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchProxy.h"
#include "clMock_ContextProvider.h"
#include "clMock_ContextRequestor.h"


using testing::_;
using testing::ElementsAre;
using testing::Property;
using testing::Matcher;
using testing::Return;
using testing::SetArgPointee;
using testing::Mock;
using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch;
using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;

ContextSwitchProxy* ContextSwitchProxy::myProxy = NULL;


TEST_F(clContextProxy_Test, vRequestContext_correctSourceAndTargetContext_RequestSentToServer)
{
   //ContextSwitchProxy oMockProxy;
   ContextSwitchProxy* oMockProxy = new ContextSwitchProxy();
   EXPECT_CALL(*oMockProxy, sendRequestContextRequest(_, _, _)).Times(1);
   clContextProxy::instance()->vRequestContext(32, 23);
   delete oMockProxy;
}


TEST_F(clContextProxy_Test, onActiveContextSignal_correctContextId_SentContextStatusToProviderAndRequestor)
{
   ContextSwitchProxy* oMockProxy = new ContextSwitchProxy();
   clMock_ContextProvider oMockProvider ;
   clMock_ContextRequestor oMockRequestor ;

   EXPECT_CALL(oMockProvider, vOnNewActiveContext(_, _)).Times(1);
   EXPECT_CALL(oMockRequestor, vOnNewActiveContext(_, _)).Times(1);

   ActiveContextSignal* signal = new ActiveContextSignal();
   signal->setContext(10);
   signal->setContextStatus(ContextState__ACCEPTED);
   clContextProxy::instance()->vSetRequestorImpl(&oMockRequestor);
   clContextProxy::instance()->vSetProviderImpl(&oMockProvider);
   clContextProxy::instance()->onActiveContextSignal((const ::boost::shared_ptr< ContextSwitchProxy >)(oMockProxy), (const ::boost::shared_ptr< ContextSwitch::ActiveContextSignal >)(signal));

}
