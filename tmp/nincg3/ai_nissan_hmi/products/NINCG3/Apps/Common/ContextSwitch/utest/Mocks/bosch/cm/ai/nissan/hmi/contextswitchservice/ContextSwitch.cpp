/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#include "asf/core/Internal.h"
#include "asf/core/Types.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitch.h"
#include <cstdlib>
#include <vector>

/**
 * todo
 */

namespace bosch
{
namespace cm
{
namespace ai
{
namespace nissan
{
namespace hmi
{
namespace contextswitchservice
{
namespace ContextSwitch
{

RequestContextRequest* RequestContextRequest::_defaultInstance = NULL;

void RequestContextRequest::deleteDefaultInstance()
{
   delete RequestContextRequest::_defaultInstance;
}

const RequestContextRequest& RequestContextRequest::getDefaultInstance()
{
   if (_defaultInstance != NULL)
   {
      return *_defaultInstance;
   }

   if (_defaultInstance == NULL)
   {
      _defaultInstance = new RequestContextRequest();
      ::asf::core::registerShutdownHook(RequestContextRequest::deleteDefaultInstance);
   }

   return *_defaultInstance;
}


RequestContextResponse* RequestContextResponse::_defaultInstance = NULL;

void RequestContextResponse::deleteDefaultInstance()
{
   delete RequestContextResponse::_defaultInstance;
}

const RequestContextResponse& RequestContextResponse::getDefaultInstance()
{
   if (_defaultInstance != NULL)
   {
      return *_defaultInstance;
   }

   if (_defaultInstance == NULL)
   {
      _defaultInstance = new RequestContextResponse();
      ::asf::core::registerShutdownHook(RequestContextResponse::deleteDefaultInstance);
   }

   return *_defaultInstance;
}


SetAvailableContextsRequest* SetAvailableContextsRequest::_defaultInstance = NULL;

void SetAvailableContextsRequest::deleteDefaultInstance()
{
   delete SetAvailableContextsRequest::_defaultInstance;
}

const SetAvailableContextsRequest& SetAvailableContextsRequest::getDefaultInstance()
{
   if (_defaultInstance != NULL)
   {
      return *_defaultInstance;
   }

   if (_defaultInstance == NULL)
   {
      _defaultInstance = new SetAvailableContextsRequest();
      ::asf::core::registerShutdownHook(SetAvailableContextsRequest::deleteDefaultInstance);
   }

   return *_defaultInstance;
}

::std::vector< uint32 >* SetAvailableContextsRequest::_Uint32List_DefaultInstance = NULL;

void SetAvailableContextsRequest::deleteUint32List_DefaultInstance()
{
   delete SetAvailableContextsRequest::_Uint32List_DefaultInstance;
}

const ::std::vector< uint32 >& SetAvailableContextsRequest::getUint32List_DefaultInstance()
{
   if (_Uint32List_DefaultInstance != NULL)
   {
      return *_Uint32List_DefaultInstance;
   }

   if (_Uint32List_DefaultInstance == NULL)
   {
      _Uint32List_DefaultInstance = new ::std::vector< uint32 >();
      ::asf::core::registerShutdownHook(SetAvailableContextsRequest::deleteUint32List_DefaultInstance);
   }

   return *_Uint32List_DefaultInstance;
}


SwitchAppRequest* SwitchAppRequest::_defaultInstance = NULL;

void SwitchAppRequest::deleteDefaultInstance()
{
   delete SwitchAppRequest::_defaultInstance;
}

const SwitchAppRequest& SwitchAppRequest::getDefaultInstance()
{
   if (_defaultInstance != NULL)
   {
      return *_defaultInstance;
   }

   if (_defaultInstance == NULL)
   {
      _defaultInstance = new SwitchAppRequest();
      ::asf::core::registerShutdownHook(SwitchAppRequest::deleteDefaultInstance);
   }

   return *_defaultInstance;
}


SwitchAppResponse* SwitchAppResponse::_defaultInstance = NULL;

void SwitchAppResponse::deleteDefaultInstance()
{
   delete SwitchAppResponse::_defaultInstance;
}

const SwitchAppResponse& SwitchAppResponse::getDefaultInstance()
{
   if (_defaultInstance != NULL)
   {
      return *_defaultInstance;
   }

   if (_defaultInstance == NULL)
   {
      _defaultInstance = new SwitchAppResponse();
      ::asf::core::registerShutdownHook(SwitchAppResponse::deleteDefaultInstance);
   }

   return *_defaultInstance;
}


RequestContextAckRequest* RequestContextAckRequest::_defaultInstance = NULL;

void RequestContextAckRequest::deleteDefaultInstance()
{
   delete RequestContextAckRequest::_defaultInstance;
}

const RequestContextAckRequest& RequestContextAckRequest::getDefaultInstance()
{
   if (_defaultInstance != NULL)
   {
      return *_defaultInstance;
   }

   if (_defaultInstance == NULL)
   {
      _defaultInstance = new RequestContextAckRequest();
      ::asf::core::registerShutdownHook(RequestContextAckRequest::deleteDefaultInstance);
   }

   return *_defaultInstance;
}


VOnNewContextSignal* VOnNewContextSignal::_defaultInstance = NULL;

void VOnNewContextSignal::deleteDefaultInstance()
{
   delete VOnNewContextSignal::_defaultInstance;
}

const VOnNewContextSignal& VOnNewContextSignal::getDefaultInstance()
{
   if (_defaultInstance != NULL)
   {
      return *_defaultInstance;
   }

   if (_defaultInstance == NULL)
   {
      _defaultInstance = new VOnNewContextSignal();
      ::asf::core::registerShutdownHook(VOnNewContextSignal::deleteDefaultInstance);
   }

   return *_defaultInstance;
}


ActiveContextSignal* ActiveContextSignal::_defaultInstance = NULL;

void ActiveContextSignal::deleteDefaultInstance()
{
   delete ActiveContextSignal::_defaultInstance;
}

const ActiveContextSignal& ActiveContextSignal::getDefaultInstance()
{
   if (_defaultInstance != NULL)
   {
      return *_defaultInstance;
   }

   if (_defaultInstance == NULL)
   {
      _defaultInstance = new ActiveContextSignal();
      ::asf::core::registerShutdownHook(ActiveContextSignal::deleteDefaultInstance);
   }

   return *_defaultInstance;
}

} // namespace ContextSwitch
} // namespace contextswitchservice
} // namespace hmi
} // namespace nissan
} // namespace ai
} // namespace cm
} // namespace bosch
