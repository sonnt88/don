/**************************************************************************//**
* \file     clContextStub_Test.h
*
*
* \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
******************************************************************************/
#ifndef clContextStub_Test_h
#define clContextStub_Test_h

#include "gmock/gmock.h"
#include "gtest/gtest.h"

class clContextStub_Test : public testing::Test
{
   public:
      clContextStub_Test()
      {

      }
      virtual ~clContextStub_Test()
      {

      }
      void SetUp()
      {
      }
      void TearDown()
      {
      }
};

#endif // clContextStub_Test_h
