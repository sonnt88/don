/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#ifndef BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHJSON_H
#define BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHJSON_H

#include "asf/stream/json.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitch.h"
#include <string>


// Json serialization of RequestContextRequest

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextRequest& value, ::std::string& jsonString, ::asf::stream::json::JsonSerializationOptions options = (::asf::stream::json::JsonSerializationOptions) 0);

// Json serialization of RequestContextResponse

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextResponse& value, ::std::string& jsonString, ::asf::stream::json::JsonSerializationOptions options = (::asf::stream::json::JsonSerializationOptions) 0);

// Json serialization of SetAvailableContextsRequest

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SetAvailableContextsRequest& value, ::std::string& jsonString, ::asf::stream::json::JsonSerializationOptions options = (::asf::stream::json::JsonSerializationOptions) 0);

// Json serialization of SwitchAppRequest

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SwitchAppRequest& value, ::std::string& jsonString, ::asf::stream::json::JsonSerializationOptions options = (::asf::stream::json::JsonSerializationOptions) 0);

// Json serialization of SwitchAppResponse

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SwitchAppResponse& value, ::std::string& jsonString, ::asf::stream::json::JsonSerializationOptions options = (::asf::stream::json::JsonSerializationOptions) 0);

// Json serialization of RequestContextAckRequest

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextAckRequest& value, ::std::string& jsonString, ::asf::stream::json::JsonSerializationOptions options = (::asf::stream::json::JsonSerializationOptions) 0);

// Json serialization of VOnNewContextSignal

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::VOnNewContextSignal& value, ::std::string& jsonString, ::asf::stream::json::JsonSerializationOptions options = (::asf::stream::json::JsonSerializationOptions) 0);

// Json serialization of ActiveContextSignal

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::ActiveContextSignal& value, ::std::string& jsonString, ::asf::stream::json::JsonSerializationOptions options = (::asf::stream::json::JsonSerializationOptions) 0);

#endif // BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHJSON_H
