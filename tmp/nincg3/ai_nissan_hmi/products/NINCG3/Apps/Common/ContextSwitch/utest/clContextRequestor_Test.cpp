/**************************************************************************//**
* \file     clContextRequestor_Test.cpp
*
*           clContextRequestor_Test method class implementation
*
* \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
******************************************************************************/

#include "clContextRequestor_Test.h"
#include "clContextRequestor.h"
#include "utest/Spy/clSpy_ContextRequestor.h"


#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchProxy.h"
#include "clMock_ContextRequestor.h"


using testing::_;
using testing::ElementsAre;
using testing::Property;
using testing::Matcher;
using testing::Return;
using testing::SetArgPointee;
using testing::Mock;


using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch;
using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;



TEST_F(clContextRequestor_Test, vRequestContext_SendContextRequest)
{
   ContextSwitchProxy* oMockProxy = new ContextSwitchProxy();
   EXPECT_CALL(*oMockProxy, sendRequestContextRequest(_, _, _)).Times(1);
   clContextRequestor oRequestor;
   oRequestor.vRequestContext(0, 20);
   delete oMockProxy;

}


TEST_F(clContextRequestor_Test, vOnNewActiveContext_NotMyRequest_vOnRequestResponseNotCalled)
{
   ContextSwitchProxy* oMockProxy = new ContextSwitchProxy();
   clSpy_ContextRequestor oRequestor;
   EXPECT_CALL(oRequestor, vOnRequestResponse(ContextState__REJECTED)).Times(0);
   oRequestor.vOnNewActiveContext(10, ContextState__REJECTED);
   delete oMockProxy;
}


TEST_F(clContextRequestor_Test, vOnNewActiveContext_ContextisZero_vOnRequestResponseNotCalled)
{
   ContextSwitchProxy* oMockProxy = new ContextSwitchProxy();
   clSpy_ContextRequestor oRequestor;
   EXPECT_CALL(oRequestor, vOnRequestResponse(ContextState__REJECTED)).Times(0);
   oRequestor.vOnNewActiveContext(0, ContextState__REJECTED);
   delete oMockProxy;

}


TEST_F(clContextRequestor_Test, vOnNewActiveContext_MyRequestRejected_vOnRequestResponseCalled)
{
   ContextSwitchProxy* oMockProxy = new ContextSwitchProxy();
   EXPECT_CALL(*oMockProxy, sendRequestContextRequest(_, _, _)).Times(1);
   clSpy_ContextRequestor oRequestor;
   EXPECT_CALL(oRequestor, vOnRequestResponse(ContextState__REJECTED)).Times(1);
   oRequestor.vRequestContext(0, 20);
   oRequestor.vOnNewActiveContext(20, ContextState__REJECTED);
   delete oMockProxy;

}


TEST_F(clContextRequestor_Test, vOnNewActiveContext_MyRequestAccepted_vOnRequestResponseCalled)
{
   ContextSwitchProxy* oMockProxy = new ContextSwitchProxy();
   EXPECT_CALL(*oMockProxy, sendRequestContextRequest(_, _, _)).Times(1);
   clSpy_ContextRequestor oRequestor;
   EXPECT_CALL(oRequestor, vOnRequestResponse(ContextState__ACCEPTED)).Times(1);
   oRequestor.vRequestContext(0, 20);
   oRequestor.vOnNewActiveContext(20, ContextState__ACCEPTED);
   delete oMockProxy;

}