///////////////////////////////////////////////////////////
//  clContextRequestorInterface.h
//  Implementation of the Class clContextRequestorInterface
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(clContextRequestorInterface_h)
#define clContextRequestorInterface_h

/**
 * interface class for context proxies to provide contexts to requestors
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 5:44:42 PM
 */

#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"
#include "asf/core/Types.h"
#include <vector>


using namespace bosch::cm::ai::nissan::hmi::contextswitchservice;
using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;


class clContextRequestorInterface
{
   public:
      clContextRequestorInterface() {};
      virtual ~clContextRequestorInterface() {};
      virtual void vOnNewActiveContext(uint32 targetContextId, ContextSwitchTypes::ContextState enContextState, ContextSwitchTypes::ContextType enContextType) = 0;
      virtual void vOnRequestContextResponse(uint32 bValidContext) = 0;
      virtual void vOnAvailableContexts(::std::vector< uint32 > availableContexts) = 0;
};


#endif // !defined(EA_FAF598AF_B4F9_4bb1_91CE_ABBA6F93217B__INCLUDED_)
