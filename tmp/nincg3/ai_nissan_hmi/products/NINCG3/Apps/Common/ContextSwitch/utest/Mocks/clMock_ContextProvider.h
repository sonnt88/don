///////////////////////////////////////////////////////////
//  clMock_ContextProvider.h
//  Implementation of the Class clMock_ContextProvider
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(clMock_ContextProvider_h)
#define clMock_ContextProvider_h

/**
 * interface class for context proxies to provide contexts to requestors
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 5:44:42 PM
 */

#include "clContextProviderInterface.h"
#include "clContext.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"
#include "asf/core/Types.h"


using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;


class clMock_ContextProvider : public clContextProviderInterface
{

   public:
      clMock_ContextProvider() {};
      virtual ~clMock_ContextProvider() {};
      MOCK_METHOD0(vInit, void());
      MOCK_METHOD2(vOnNewContext, void(uint32 sourceContextId, uint32 targetContextId));
      MOCK_METHOD2(vOnNewActiveContext, void(uint32 contextId, ContextState enState));
      MOCK_METHOD1(vOnSwitchAppResponse, void(uint32 bAppSwitched));
};
#endif // !defined(EA_FAF598AF_B4F9_4bb1_91CE_ABBA6F93217B__INCLUDED_)
