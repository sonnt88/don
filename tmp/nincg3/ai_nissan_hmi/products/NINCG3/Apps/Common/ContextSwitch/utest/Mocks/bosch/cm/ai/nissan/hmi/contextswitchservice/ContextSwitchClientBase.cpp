/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#include "boost/shared_ptr.hpp"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitch.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchClientBase.h"

namespace bosch
{
namespace cm
{
namespace ai
{
namespace nissan
{
namespace hmi
{
namespace contextswitchservice
{
namespace ContextSwitch
{

/*static*/ ::asf::core::Logger ContextSwitchClientBase::_logger("bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitch/ContextSwitchClientBase");

// disabled lint warning 715: symbol not referenced
//lint -e715


// Callback 'ActiveContextCallbackIF'

void ContextSwitchClientBase::onActiveContextError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ActiveContextError >& error)
{
   LOG_FATAL("void onActiveContextError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ActiveContextError >& error) not yet implemented");
}

void ContextSwitchClientBase::onActiveContextSignal(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ActiveContextSignal >& signal)
{
   LOG_FATAL("void onActiveContextSignal(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ActiveContextSignal >& signal) not yet implemented");
}

// Callback 'RequestContextAckCallbackIF'

void ContextSwitchClientBase::onRequestContextAckError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< RequestContextAckError >& error)
{
   LOG_FATAL("void onRequestContextAckError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< RequestContextAckError >& error) not yet implemented");
}

void ContextSwitchClientBase::onRequestContextAckResponse(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< RequestContextAckResponse >& response)
{
   LOG_FATAL("void onRequestContextAckResponse(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< RequestContextAckResponse >& response) not yet implemented");
}

// Callback 'RequestContextCallbackIF'

void ContextSwitchClientBase::onRequestContextError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< RequestContextError >& error)
{
   LOG_FATAL("void onRequestContextError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< RequestContextError >& error) not yet implemented");
}

void ContextSwitchClientBase::onRequestContextResponse(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< RequestContextResponse >& response)
{
   LOG_FATAL("void onRequestContextResponse(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< RequestContextResponse >& response) not yet implemented");
}

// Callback 'SetAvailableContextsCallbackIF'

void ContextSwitchClientBase::onSetAvailableContextsError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< SetAvailableContextsError >& error)
{
   LOG_FATAL("void onSetAvailableContextsError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< SetAvailableContextsError >& error) not yet implemented");
}

void ContextSwitchClientBase::onSetAvailableContextsResponse(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< SetAvailableContextsResponse >& response)
{
   LOG_FATAL("void onSetAvailableContextsResponse(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< SetAvailableContextsResponse >& response) not yet implemented");
}

// Callback 'SwitchAppCallbackIF'

void ContextSwitchClientBase::onSwitchAppError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< SwitchAppError >& error)
{
   LOG_FATAL("void onSwitchAppError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< SwitchAppError >& error) not yet implemented");
}

void ContextSwitchClientBase::onSwitchAppResponse(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< SwitchAppResponse >& response)
{
   LOG_FATAL("void onSwitchAppResponse(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< SwitchAppResponse >& response) not yet implemented");
}

// Callback 'VOnNewContextCallbackIF'

void ContextSwitchClientBase::onVOnNewContextError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< VOnNewContextError >& error)
{
   LOG_FATAL("void onVOnNewContextError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< VOnNewContextError >& error) not yet implemented");
}

void ContextSwitchClientBase::onVOnNewContextSignal(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< VOnNewContextSignal >& signal)
{
   LOG_FATAL("void onVOnNewContextSignal(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< VOnNewContextSignal >& signal) not yet implemented");
}

//lint +e715

} // namespace ContextSwitch
} // namespace contextswitchservice
} // namespace hmi
} // namespace nissan
} // namespace ai
} // namespace cm
} // namespace bosch
