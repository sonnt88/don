/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#ifndef BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHPROXY_H
#define BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHPROXY_H

#include <gmock/gmock.h>
#include "asf/core/Logger.h"
#include "asf/core/Types.h"
#include "asf/dbus/DBusConnector.h"
#include "asf/dbus/DBusProxy.h"
#include "asf/dbus/DefaultTypesDBus.h"
#include "boost/shared_ptr.hpp"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitch.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"
#include <dbus/dbus.h>
#include <vector>
#include <streambuf>
#include <iostream>

namespace bosch
{
namespace cm
{
namespace ai
{
namespace nissan
{
namespace hmi
{
namespace contextswitchservice
{
namespace ContextSwitch
{




class ContextSwitchProxy;

class ActiveContextCallbackIF
{
   public:
      virtual ~ActiveContextCallbackIF() {}

      virtual void onActiveContextError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ActiveContextError >& error) = 0;

      virtual void onActiveContextSignal(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ActiveContextSignal >& signal) = 0;
};

class RequestContextAckCallbackIF
{
   public:
      virtual ~RequestContextAckCallbackIF() {}

      virtual void onRequestContextAckError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< RequestContextAckError >& error) = 0;

      virtual void onRequestContextAckResponse(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< RequestContextAckResponse >& response) = 0;
};

class RequestContextCallbackIF
{
   public:
      virtual ~RequestContextCallbackIF() {}

      virtual void onRequestContextError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< RequestContextError >& error) = 0;

      virtual void onRequestContextResponse(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< RequestContextResponse >& response) = 0;
};

class SetAvailableContextsCallbackIF
{
   public:
      virtual ~SetAvailableContextsCallbackIF() {}

      virtual void onSetAvailableContextsError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< SetAvailableContextsError >& error) = 0;

      virtual void onSetAvailableContextsResponse(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< SetAvailableContextsResponse >& response) = 0;
};

class SwitchAppCallbackIF
{
   public:
      virtual ~SwitchAppCallbackIF() {}

      virtual void onSwitchAppError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< SwitchAppError >& error) = 0;

      virtual void onSwitchAppResponse(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< SwitchAppResponse >& response) = 0;
};

class VOnNewContextCallbackIF
{
   public:
      virtual ~VOnNewContextCallbackIF() {}

      virtual void onVOnNewContextError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< VOnNewContextError >& error) = 0;

      virtual void onVOnNewContextSignal(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< VOnNewContextSignal >& signal) = 0;
};



class ContextSwitchProxy
{
   public:
      static ContextSwitchProxy* myProxy;

      static ::boost::shared_ptr< ContextSwitchProxy > createProxy(const std::string& portName, ::asf::core::ServiceAvailableIF& serviceAvailable)
      {
         if (ContextSwitchProxy::myProxy == 0)
         {
            return ::boost::shared_ptr<ContextSwitchProxy>(new ContextSwitchProxy());
         }
         return ::boost::shared_ptr<ContextSwitchProxy>(ContextSwitchProxy::myProxy);
      }

      /**
       * Create a new instance of ContextSwitchProxy.
       */
      static ::boost::shared_ptr< ContextSwitchProxy > createProxy(const std::string& portName,
            const std::string& busName,
            const std::string& objectPath,
            ::DBusBusType busType,
            ::asf::core::ServiceAvailableIF& serviceAvailable)
      {
         if (ContextSwitchProxy::myProxy == 0)
         {
            return ::boost::shared_ptr<ContextSwitchProxy>(new ContextSwitchProxy());
         }
         return ::boost::shared_ptr<ContextSwitchProxy>(ContextSwitchProxy::myProxy);
      }
      static ContextSwitchProxy* createProxy()
      {
         if (ContextSwitchProxy::myProxy == 0)
         {
            return (new ContextSwitchProxy());
         }
         return (ContextSwitchProxy::myProxy);
      }
      virtual ~ContextSwitchProxy()
      {
         ContextSwitchProxy::myProxy  = 0;
      };
      ContextSwitchProxy()
      {
         ContextSwitchProxy::myProxy = this;
      };

      MOCK_METHOD1(sendVOnNewContextRegister, act_t(VOnNewContextCallbackIF& cb));
      MOCK_METHOD1(sendVOnNewContextDeregister, bool(act_t act));
      MOCK_METHOD0(sendVOnNewContextDeregisterAll, void());
      MOCK_METHOD1(sendActiveContextRegister, act_t(ActiveContextCallbackIF& cb));
      MOCK_METHOD1(sendActiveContextDeregister, bool(act_t act));
      MOCK_METHOD0(sendActiveContextDeregisterAll, void());
      MOCK_METHOD3(sendRequestContextRequest, act_t(RequestContextCallbackIF& cb, uint32 sourceContext, uint32 targetContext));
      MOCK_METHOD2(sendSetAvailableContextsRequest, act_t(SetAvailableContextsCallbackIF& cb, const ::std::vector< uint32 >& availableContext));
      MOCK_METHOD4(sendSwitchAppRequest, act_t(SwitchAppCallbackIF& cb, uint32 applicationId, uint32 priority, uint32 targetContext));
      MOCK_METHOD3(sendRequestContextAckRequest, act_t(RequestContextAckCallbackIF& cb, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState contextStatus, uint32 targetContext));
      MOCK_METHOD0(sendDeregisterAll, void());


};




} // namespace ContextSwitch
} // namespace contextswitchservice
} // namespace hmi
} // namespace nissan
} // namespace ai
} // namespace cm
} // namespace bosch

#endif // BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHPROXY_H
