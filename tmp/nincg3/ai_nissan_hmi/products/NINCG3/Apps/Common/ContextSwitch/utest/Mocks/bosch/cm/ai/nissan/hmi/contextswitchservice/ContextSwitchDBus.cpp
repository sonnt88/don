/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#include "asf/core/Types.h"
#include "asf/dbus/DBusAssert.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitch.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"
#include <dbus/dbus.h>
#include <vector>

static ::asf::core::Logger& _logger =  ::asf::core::Logger::_systemLogger;


// D-Bus serialize and deserialize functions of RequestContextRequest

void serializeDBus(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextRequest& in, DBusMessageIter* out)
{
   uint32 o0 = in.getSourceContext();
   DBUS_ASSERT(dbus_message_iter_append_basic(out, DBUS_TYPE_UINT32, &o0));
   uint32 o1 = in.getTargetContext();
   DBUS_ASSERT(dbus_message_iter_append_basic(out, DBUS_TYPE_UINT32, &o1));
}

bool deserializeDBus(DBusMessageIter* in, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextRequest& out)
{
   {
      uint32 sourceContextValue;
      uint32& sourceContextTmpValue = sourceContextValue;
      dbus_message_iter_get_basic(in, &sourceContextTmpValue);
      out.setSourceContext(sourceContextValue);
   }
   if (dbus_message_iter_has_next(in))
   {
      dbus_message_iter_next(in);
   }
   else
   {
      return true;
   }

   {
      uint32 targetContextValue;
      uint32& targetContextTmpValue = targetContextValue;
      dbus_message_iter_get_basic(in, &targetContextTmpValue);
      out.setTargetContext(targetContextValue);
   }
   if (dbus_message_iter_has_next(in))
   {
      dbus_message_iter_next(in);
   }
   else
   {
      return true;
   }

   return true;
}

// D-Bus serialize and deserialize functions of RequestContextResponse

void serializeDBus(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextResponse& in, DBusMessageIter* out)
{
   uint32 o2 = in.getBValidContext();
   DBUS_ASSERT(dbus_message_iter_append_basic(out, DBUS_TYPE_UINT32, &o2));
}

bool deserializeDBus(DBusMessageIter* in, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextResponse& out)
{
   {
      uint32 bValidContextValue;
      uint32& bValidContextTmpValue = bValidContextValue;
      dbus_message_iter_get_basic(in, &bValidContextTmpValue);
      out.setBValidContext(bValidContextValue);
   }
   if (dbus_message_iter_has_next(in))
   {
      dbus_message_iter_next(in);
   }
   else
   {
      return true;
   }

   return true;
}

// D-Bus serialize and deserialize functions of SetAvailableContextsRequest

void serializeDBus(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SetAvailableContextsRequest& in, DBusMessageIter* out)
{
   DBusMessageIter o3ArrayIter;
   DBUS_ASSERT(dbus_message_iter_open_container(out, DBUS_TYPE_ARRAY, "u", &o3ArrayIter));
   const ::std::vector< uint32 >* o4 = &in.getAvailableContext();
   ::std::vector< uint32 >::const_iterator o5 = o4->end();
   for (::std::vector< uint32 >::const_iterator o6 = o4->begin(); o6 != o5; ++o6)
   {
      uint32 o7 = (*o6);
      DBUS_ASSERT(dbus_message_iter_append_basic(&o3ArrayIter, DBUS_TYPE_UINT32, &o7));
   }
   DBUS_ASSERT(dbus_message_iter_close_container(out, &o3ArrayIter));
}

bool deserializeDBus(DBusMessageIter* in, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SetAvailableContextsRequest& out)
{
   {
      ::std::vector< uint32 > availableContextValue;
      DBusMessageIter o8ArrayIter;
      dbus_message_iter_recurse(in, &o8ArrayIter);

      if (dbus_message_iter_get_arg_type(&o8ArrayIter) != DBUS_TYPE_INVALID)
      {
         do
         {
            uint32 o9;
            dbus_message_iter_get_basic(&o8ArrayIter, &o9);
            availableContextValue.push_back(o9);
         }
         while (dbus_message_iter_next(&o8ArrayIter));
      }
      out.setAvailableContext(availableContextValue);
   }
   if (dbus_message_iter_has_next(in))
   {
      dbus_message_iter_next(in);
   }
   else
   {
      return true;
   }

   return true;
}

// D-Bus serialize and deserialize functions of SwitchAppRequest

void serializeDBus(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SwitchAppRequest& in, DBusMessageIter* out)
{
   uint32 o10 = in.getApplicationId();
   DBUS_ASSERT(dbus_message_iter_append_basic(out, DBUS_TYPE_UINT32, &o10));
   uint32 o11 = in.getPriority();
   DBUS_ASSERT(dbus_message_iter_append_basic(out, DBUS_TYPE_UINT32, &o11));
   uint32 o12 = in.getTargetContext();
   DBUS_ASSERT(dbus_message_iter_append_basic(out, DBUS_TYPE_UINT32, &o12));
}

bool deserializeDBus(DBusMessageIter* in, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SwitchAppRequest& out)
{
   {
      uint32 applicationIdValue;
      uint32& applicationIdTmpValue = applicationIdValue;
      dbus_message_iter_get_basic(in, &applicationIdTmpValue);
      out.setApplicationId(applicationIdValue);
   }
   if (dbus_message_iter_has_next(in))
   {
      dbus_message_iter_next(in);
   }
   else
   {
      return true;
   }

   {
      uint32 priorityValue;
      uint32& priorityTmpValue = priorityValue;
      dbus_message_iter_get_basic(in, &priorityTmpValue);
      out.setPriority(priorityValue);
   }
   if (dbus_message_iter_has_next(in))
   {
      dbus_message_iter_next(in);
   }
   else
   {
      return true;
   }

   {
      uint32 targetContextValue;
      uint32& targetContextTmpValue = targetContextValue;
      dbus_message_iter_get_basic(in, &targetContextTmpValue);
      out.setTargetContext(targetContextValue);
   }
   if (dbus_message_iter_has_next(in))
   {
      dbus_message_iter_next(in);
   }
   else
   {
      return true;
   }

   return true;
}

// D-Bus serialize and deserialize functions of SwitchAppResponse

void serializeDBus(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SwitchAppResponse& in, DBusMessageIter* out)
{
   uint32 o13 = in.getBAppSwitched();
   DBUS_ASSERT(dbus_message_iter_append_basic(out, DBUS_TYPE_UINT32, &o13));
}

bool deserializeDBus(DBusMessageIter* in, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SwitchAppResponse& out)
{
   {
      uint32 bAppSwitchedValue;
      uint32& bAppSwitchedTmpValue = bAppSwitchedValue;
      dbus_message_iter_get_basic(in, &bAppSwitchedTmpValue);
      out.setBAppSwitched(bAppSwitchedValue);
   }
   if (dbus_message_iter_has_next(in))
   {
      dbus_message_iter_next(in);
   }
   else
   {
      return true;
   }

   return true;
}

// D-Bus serialize and deserialize functions of RequestContextAckRequest

void serializeDBus(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextAckRequest& in, DBusMessageIter* out)
{
   const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState  o14 = in.getContextStatus();
   DBUS_ASSERT(dbus_message_iter_append_basic(out, DBUS_TYPE_UINT32, &o14));
   uint32 o15 = in.getTargetContext();
   DBUS_ASSERT(dbus_message_iter_append_basic(out, DBUS_TYPE_UINT32, &o15));
}

bool deserializeDBus(DBusMessageIter* in, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextAckRequest& out)
{
   {
      ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState contextStatusValue = ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState__ACCEPTED;
      dbus_message_iter_get_basic(in, &contextStatusValue);
      out.setContextStatus(contextStatusValue);
   }
   if (dbus_message_iter_has_next(in))
   {
      dbus_message_iter_next(in);
   }
   else
   {
      return true;
   }

   {
      uint32 targetContextValue;
      uint32& targetContextTmpValue = targetContextValue;
      dbus_message_iter_get_basic(in, &targetContextTmpValue);
      out.setTargetContext(targetContextValue);
   }
   if (dbus_message_iter_has_next(in))
   {
      dbus_message_iter_next(in);
   }
   else
   {
      return true;
   }

   return true;
}

// D-Bus serialize and deserialize functions of VOnNewContextSignal

void serializeDBus(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::VOnNewContextSignal& in, DBusMessageIter* out)
{
   uint32 o16 = in.getSourceContext();
   DBUS_ASSERT(dbus_message_iter_append_basic(out, DBUS_TYPE_UINT32, &o16));
   uint32 o17 = in.getTargetContext();
   DBUS_ASSERT(dbus_message_iter_append_basic(out, DBUS_TYPE_UINT32, &o17));
}

bool deserializeDBus(DBusMessageIter* in, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::VOnNewContextSignal& out)
{
   {
      uint32 sourceContextValue;
      uint32& sourceContextTmpValue = sourceContextValue;
      dbus_message_iter_get_basic(in, &sourceContextTmpValue);
      out.setSourceContext(sourceContextValue);
   }
   if (dbus_message_iter_has_next(in))
   {
      dbus_message_iter_next(in);
   }
   else
   {
      return true;
   }

   {
      uint32 targetContextValue;
      uint32& targetContextTmpValue = targetContextValue;
      dbus_message_iter_get_basic(in, &targetContextTmpValue);
      out.setTargetContext(targetContextValue);
   }
   if (dbus_message_iter_has_next(in))
   {
      dbus_message_iter_next(in);
   }
   else
   {
      return true;
   }

   return true;
}

// D-Bus serialize and deserialize functions of ActiveContextSignal

void serializeDBus(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::ActiveContextSignal& in, DBusMessageIter* out)
{
   uint32 o18 = in.getContext();
   DBUS_ASSERT(dbus_message_iter_append_basic(out, DBUS_TYPE_UINT32, &o18));
   const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState  o19 = in.getContextStatus();
   DBUS_ASSERT(dbus_message_iter_append_basic(out, DBUS_TYPE_UINT32, &o19));
}

bool deserializeDBus(DBusMessageIter* in, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::ActiveContextSignal& out)
{
   {
      uint32 contextValue;
      uint32& contextTmpValue = contextValue;
      dbus_message_iter_get_basic(in, &contextTmpValue);
      out.setContext(contextValue);
   }
   if (dbus_message_iter_has_next(in))
   {
      dbus_message_iter_next(in);
   }
   else
   {
      return true;
   }

   {
      ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState contextStatusValue = ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes::ContextState__ACCEPTED;
      dbus_message_iter_get_basic(in, &contextStatusValue);
      out.setContextStatus(contextStatusValue);
   }
   if (dbus_message_iter_has_next(in))
   {
      dbus_message_iter_next(in);
   }
   else
   {
      return true;
   }

   return true;
}

