///////////////////////////////////////////////////////////
//  clMock_ContextRequestor.h
//  Implementation of the Class clMock_ContextRequestor
//  Created on:      13-Jul-2015 5:44:20 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(clMock_ContextRequestor_h)
#define clMock_ContextRequestor_h

/**
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 5:44:20 PM
 */

#include "clContextRequestorInterface.h"
#include "asf/core/Types.h"
#include "clContext.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"


using namespace bosch::cm::ai::nissan::hmi::contextswitchservice;


class clMock_ContextRequestor : public clContextRequestorInterface
{

   public:
      clMock_ContextRequestor() {};
      virtual ~clMock_ContextRequestor() {};
      MOCK_METHOD0(vInit, void());
      MOCK_METHOD2(vOnNewActiveContext, void(uint32 targetContextId, ContextSwitchTypes::ContextState enContextState));
      MOCK_METHOD1(vOnRequestContextResponse, void(uint32 bValidContext));

};
#endif // !defined(EA_D478FD03_EC34_490a_BB23_6FC06868F8B9__INCLUDED_)
