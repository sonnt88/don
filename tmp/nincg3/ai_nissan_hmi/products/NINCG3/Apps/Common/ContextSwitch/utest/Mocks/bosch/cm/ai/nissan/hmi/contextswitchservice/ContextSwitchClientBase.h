/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#ifndef BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHCLIENTBASE_H
#define BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHCLIENTBASE_H

#include "asf/core/Logger.h"
#include "boost/shared_ptr.hpp"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitch.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchProxy.h"

namespace bosch
{
namespace cm
{
namespace ai
{
namespace nissan
{
namespace hmi
{
namespace contextswitchservice
{
namespace ContextSwitch
{

class ContextSwitchClientBase :
   public RequestContextAckCallbackIF,
   public ActiveContextCallbackIF,
   public VOnNewContextCallbackIF,
   public SwitchAppCallbackIF,
   public RequestContextCallbackIF,
   public SetAvailableContextsCallbackIF
{
   public:
      virtual ~ContextSwitchClientBase() {}

      // Callback 'ActiveContextCallbackIF'

      virtual void onActiveContextError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ActiveContextError >& error);

      virtual void onActiveContextSignal(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ActiveContextSignal >& signal);

      // Callback 'RequestContextAckCallbackIF'

      virtual void onRequestContextAckError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< RequestContextAckError >& error);

      virtual void onRequestContextAckResponse(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< RequestContextAckResponse >& response);

      // Callback 'RequestContextCallbackIF'

      virtual void onRequestContextError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< RequestContextError >& error);

      virtual void onRequestContextResponse(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< RequestContextResponse >& response);

      // Callback 'SetAvailableContextsCallbackIF'

      virtual void onSetAvailableContextsError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< SetAvailableContextsError >& error);

      virtual void onSetAvailableContextsResponse(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< SetAvailableContextsResponse >& response);

      // Callback 'SwitchAppCallbackIF'

      virtual void onSwitchAppError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< SwitchAppError >& error);

      virtual void onSwitchAppResponse(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< SwitchAppResponse >& response);

      // Callback 'VOnNewContextCallbackIF'

      virtual void onVOnNewContextError(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< VOnNewContextError >& error);

      virtual void onVOnNewContextSignal(const ::boost::shared_ptr< ContextSwitchProxy >& proxy, const ::boost::shared_ptr< VOnNewContextSignal >& signal);

   private:

      DECLARE_CLASS_LOGGER();
};

} // namespace ContextSwitch
} // namespace contextswitchservice
} // namespace hmi
} // namespace nissan
} // namespace ai
} // namespace cm
} // namespace bosch

#endif // BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHCLIENTBASE_H
