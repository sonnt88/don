/**************************************************************************//**
* \file     clContextProxy_Test.h
*
*
* \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
******************************************************************************/
#ifndef clContextProxy_Test_h
#define clContextProxy_Test_h

#include "gmock/gmock.h"
#include "gtest/gtest.h"

class clContextProxy_Test : public testing::Test
{
   public:
      clContextProxy_Test()
      {

      }
      virtual ~clContextProxy_Test()
      {

      }
      void SetUp()
      {
      }
      void TearDown()
      {
      }
};

#endif // clContextProxy_Test_h
