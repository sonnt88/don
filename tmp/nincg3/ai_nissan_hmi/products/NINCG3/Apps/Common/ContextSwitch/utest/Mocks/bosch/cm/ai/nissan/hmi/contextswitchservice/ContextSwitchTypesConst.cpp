/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#include "asf/stream/json.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"

/**
 * todo
 */

using namespace ::asf::stream::json;

namespace bosch
{
namespace cm
{
namespace ai
{
namespace nissan
{
namespace hmi
{
namespace contextswitchservice
{
namespace ContextSwitchTypes
{

bool ContextState_Parse(const char* str, int length, ContextState& value)
{
   static stringEntry entries[] =
   {
      {7, "INVALID", 3U},
      {8, "ACCEPTED", 0U},
      {8, "REJECTED", 1U},
      {9, "COMPLETED", 2U},
   };

   return lookupInStringEntryTable(str, length, entries, 4, (unsigned int&) value);
}

const char* ContextState_Name(ContextState value)
{
   static const char* enumerationLiterals[] =
   {
      "ACCEPTED",
      "REJECTED",
      "COMPLETED",
      "INVALID",
   };

   if (ContextState_IsValid(value))
   {
      return enumerationLiterals[value];
   }
   else
   {
      return NULL;
   }
}

bool ContextState_IsValid(ContextState value)
{
   switch (value)
   {
      case ContextState__ACCEPTED:
      case ContextState__REJECTED:
      case ContextState__COMPLETED:
      case ContextState__INVALID:
         return true;
      default:
         return false;
   }
}

} // namespace ContextSwitchTypes
} // namespace contextswitchservice
} // namespace hmi
} // namespace nissan
} // namespace ai
} // namespace cm
} // namespace bosch
