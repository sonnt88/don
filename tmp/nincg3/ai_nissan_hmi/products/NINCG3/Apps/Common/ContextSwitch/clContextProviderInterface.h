///////////////////////////////////////////////////////////
//  clContextProviderInterface.h
//  Implementation of the Class clContextProviderInterface
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(clContextProviderInterface_h)
#define clContextProviderInterface_h

/**
 * interface class for context proxies to provide contexts to requestors
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 5:44:42 PM
 */

#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"
#include "asf/core/Types.h"

using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;

class clContextProviderInterface
{
   public:
      clContextProviderInterface() {};
      virtual ~clContextProviderInterface() {};
      virtual void vOnNewContext(uint32 sourceContextId, uint32 targetContextId) = 0;
      virtual void vOnSwitchAppResponse(uint32 bAppSwitched) = 0;
      virtual void vOnNewActiveContext(uint32 contextId, ContextState enState) = 0;
      virtual void vInit() = 0;
      virtual void vClearContexts() = 0;
};


#endif // !defined(EA_FAF598AF_B4F9_4bb1_91CE_ABBA6F93217B__INCLUDED_)
