/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#ifndef BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHDBUS_H
#define BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHDBUS_H

#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitch.h"
#include <dbus/dbus.h>


// D-Bus serialize and deserialize functions of RequestContextRequest

void serializeDBus(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextRequest& in, DBusMessageIter* out);

bool deserializeDBus(DBusMessageIter* in, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextRequest& out);

// D-Bus serialize and deserialize functions of RequestContextResponse

void serializeDBus(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextResponse& in, DBusMessageIter* out);

bool deserializeDBus(DBusMessageIter* in, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextResponse& out);

// D-Bus serialize and deserialize functions of SetAvailableContextsRequest

void serializeDBus(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SetAvailableContextsRequest& in, DBusMessageIter* out);

bool deserializeDBus(DBusMessageIter* in, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SetAvailableContextsRequest& out);

// D-Bus serialize and deserialize functions of SwitchAppRequest

void serializeDBus(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SwitchAppRequest& in, DBusMessageIter* out);

bool deserializeDBus(DBusMessageIter* in, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SwitchAppRequest& out);

// D-Bus serialize and deserialize functions of SwitchAppResponse

void serializeDBus(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SwitchAppResponse& in, DBusMessageIter* out);

bool deserializeDBus(DBusMessageIter* in, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SwitchAppResponse& out);

// D-Bus serialize and deserialize functions of RequestContextAckRequest

void serializeDBus(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextAckRequest& in, DBusMessageIter* out);

bool deserializeDBus(DBusMessageIter* in, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextAckRequest& out);

// D-Bus serialize and deserialize functions of VOnNewContextSignal

void serializeDBus(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::VOnNewContextSignal& in, DBusMessageIter* out);

bool deserializeDBus(DBusMessageIter* in, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::VOnNewContextSignal& out);

// D-Bus serialize and deserialize functions of ActiveContextSignal

void serializeDBus(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::ActiveContextSignal& in, DBusMessageIter* out);

bool deserializeDBus(DBusMessageIter* in, ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::ActiveContextSignal& out);

#endif // BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHDBUS_H
