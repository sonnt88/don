///////////////////////////////////////////////////////////
//  clSpy_ContextProvider.h
//  Implementation of the Class clSpy_ContextProvider
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(clSpy_ContextProvider_h)
#define clSpy_ContextProvider_h

/**
 * interface class for context proxies to provide contexts to requestors
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 5:44:42 PM
 */

#include "clContextProvider.h"
#include "clContext.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"
#include "asf/core/Types.h"


static const clContext aSupportedContexts[] =
{
#define SUPPORTED_CONTEXTS(context, contextType, priority, surfaceId, viewName)                  { context, contextType, priority, surfaceId, viewName },
#include "contexts.dat"
#undef SUPPORTED_CONTEXTS
};


using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;


class clSpy_ContextProvider : public clContextProvider
{
   public:
      void vSendContextTable()
      {
         ::std::vector<clContext> availableContexts;
         const int mapSize = sizeof aSupportedContexts / sizeof(clContext);
         for (int u32Index = 0; u32Index < mapSize; u32Index++)
         {
            availableContexts.push_back(aSupportedContexts[u32Index]);
         }
         storeAvailableContexts(availableContexts);
      }

      clSpy_ContextProvider()
      {
         vSendContextTable();
      };
      virtual ~clSpy_ContextProvider() {};

      MOCK_METHOD2(bOnNewContext, bool(uint32 sourceContextId, uint32 targetContextId));
      MOCK_METHOD1(vOnBackRequestResponse, void(ContextState enState));
};


#endif // !defined(EA_FAF598AF_B4F9_4bb1_91CE_ABBA6F93217B__INCLUDED_)
