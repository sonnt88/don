///////////////////////////////////////////////////////////
//  clContextStub.h
//  Implementation of the Class clContextStub
//  Created on:      13-Jul-2015 11:44:46 AM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(clContextStub_h)
#define clContextStub_h

/**
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 11:44:46 AM
 */

#include "asf/core/Types.h"
#include "clContext.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchStub.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitch.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypes.h"


using namespace bosch::cm::ai::nissan::hmi::contextswitchservice;
using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch;
using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;

struct stContext
{
   uint32 contextId;
   ContextState contextStatus;
   ContextType contextType;
};


class clContextStub :  public ContextSwitch::ContextSwitchStub
{
   public:
      virtual ~clContextStub();

      static clContextStub* instance();
      static clContextStub* m_poStub;

      virtual void onRequestContextRequest(const ::boost::shared_ptr< RequestContextRequest >& request) ;
      virtual void onSetAvailableContextsRequest(const ::boost::shared_ptr< SetAvailableContextsRequest >& request) ;
      virtual void onSwitchAppRequest(const ::boost::shared_ptr< SwitchAppRequest >& request) ;
      virtual void onRequestContextAckRequest(const ::boost::shared_ptr< RequestContextAckRequest >& request) ;
      virtual void onRemoveContextRequest(const ::boost::shared_ptr< RemoveContextRequest >& request) ;
      virtual void onClearContextStackRequest(const ::boost::shared_ptr< ClearContextStackRequest >& request) ;
      virtual void vOnNewActiveApplication(uint32 appId);
      virtual void vOnNewSurfaceRendered(uint32 surfaceId);
      bool bIsContextInStack(uint32 contextId);

   protected:
      virtual void vSwitchApp(uint32 appId);
      virtual void vSwitchApp(uint32 contextId, uint32 appId, uint32 priority, bool bIsForwardContext, ContextType contextType, uint32 sourceId);
      virtual void vOnNewActiveContext(uint32 contextId, ContextType contextType);
      virtual void vOnContextClosed(uint32 contextId);
      void vOnNewContextState(stContext& contextState);
      void clearContexts();
      bool bIsContextAvailableForSource(uint32 sourceId);
      uint32 GetContextOfSource(uint32 sourceId);
      clContextStub();

   private:
      bool bIsConditionToActivateLastApp(uint32 sourceContextId, uint32 targetContextId);
      void vSwitchToLastContext();
      bool bIsAvailableContext(ContextInfo targetContextId);
      bool bIsAvailableContext(uint32 contextId);
      void vStoreLastContext(const ::boost::shared_ptr< SwitchAppRequest >& context);
      void vStoreActiveContext(const ::boost::shared_ptr< SwitchAppRequest >& context);
      void vRemoveContext(uint32 targetContextId);
      bool bIsOkToSwitchApp(uint32 appId, uint32 priority, uint32 targetContextId);
      bool bIsTemporaryContext(uint32 contextId);
      void vStoreAppToSwitch(::boost::shared_ptr< SwitchAppRequest > switchAppData);
      uint32 getSurfaceOfContext(uint32 contextId);
      bool bIsActiveContextRequested(uint32 contextId);
      ContextType getContextType(uint32 contextId);
      void vAddContext(stContext& context);
      void vRemoveContext(stContext& context);
      void vAddToStack(const ::boost::shared_ptr< SwitchAppRequest >& request);
      bool bIsLowerPriority(boost::shared_ptr< SwitchAppRequest >  currentContext, boost::shared_ptr< SwitchAppRequest >  nextContext);
      uint32 GetSourceofContext(uint32 contextId);

      ::std::vector<ContextInfo> _availableContexts;
      ::boost::shared_ptr< SwitchAppRequest > _lastAppContext;
      ::boost::shared_ptr< SwitchAppRequest > _activeAppContext;
      ::boost::shared_ptr< SwitchAppRequest > _contextToSwitch;
      ::std::vector<boost::shared_ptr< SwitchAppRequest >  > contextStack;
      uint32 lastNonContextApplication;
};


#endif // !defined(EA_DCA45655_872E_4ba0_A673_8CB4B129886F__INCLUDED_)
