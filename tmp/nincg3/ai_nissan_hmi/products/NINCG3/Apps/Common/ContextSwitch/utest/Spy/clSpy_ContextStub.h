///////////////////////////////////////////////////////////
//  clSpy_ContextStub.h
//  Implementation of the Class clSpy_ContextStub
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(clSpy_ContextStub_h)
#define clSpy_ContextStub_h

/**
 * interface class for context proxies to provide contexts to requestors
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 5:44:42 PM
 */

#include "stub/clContextStub.h"



using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;


class clSpy_ContextStub : public clContextStub
{

   public:
      clSpy_ContextStub() {};
      virtual ~clSpy_ContextStub() {};

      MOCK_METHOD1(vSwitchApp, void(uint32 appId));



};
#endif // !defined(EA_FAF598AF_B4F9_4bb1_91CE_ABBA6F93217B__INCLUDED_)
