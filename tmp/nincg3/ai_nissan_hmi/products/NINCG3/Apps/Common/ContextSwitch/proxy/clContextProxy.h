///////////////////////////////////////////////////////////
//  clContextProxy.h
//  Implementation of the Class clContextProxy
//  Created on:      13-Jul-2015 11:44:46 AM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(clContextProxy_h)
#define clContextProxy_h

/**
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 11:44:46 AM
 */

#include "asf/core/Types.h"
#include "clContext.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchProxy.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchClientBase.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"
#include "AppBase/ServiceAvailableIF.h"


using namespace bosch::cm::ai::nissan::hmi::contextswitchservice;
using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;


class clContextRequestorInterface;
class clContextProviderInterface;


class clContextProxy
   : public hmibase::ServiceAvailableIF
   , public StartupSync::PropertyRegistrationIF
   , public ContextSwitch::ContextSwitchClientBase
{
   public:
      virtual ~clContextProxy();

      static clContextProxy* instance();
      static clContextProxy* m_poProxy;

      void vRequestContext(uint32 sourceContextId, uint32 targetContextId);
      void vClearContexts();
      void vSetAvailableContexts(::std::vector<clContext> & availableContexts);
      void vSwitchContext(uint32 contextId, uint32 priority);
      void requestContextAck(uint32 targetContextId, ContextState enState);
      void vSetApplication(uint32 appId);
      void vRemoveContext(uint32 contextId);

      void vSetProviderImpl(clContextProviderInterface* _poProviderImpl);
      void vSetRequestorImpl(clContextRequestorInterface* _poRequestorImpl);

      virtual void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);
      virtual void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);

      virtual void onActiveContextSignal(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::ActiveContextSignal >& signal);
      virtual void onRequestContextAckResponse(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::RequestContextAckResponse >& response) ;
      virtual void onClearContextStackResponse(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::ClearContextStackResponse >& response);

      virtual void onRequestContextResponse(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::RequestContextResponse >& response) ;
      virtual void onSwitchAppResponse(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::SwitchAppResponse >& response) ;
      virtual void onVOnNewContextSignal(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::VOnNewContextSignal >& signal) ;
      virtual void onAvailableContextsSignal(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::AvailableContextsSignal >& signal);
      virtual void onClearContextsSignal(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::ClearContextsSignal >& signal);

      virtual void onActiveContextError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::ActiveContextError >& error);
      virtual void onAvailableContextsError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::AvailableContextsError >& error);
      virtual void onRemoveContextError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::RemoveContextError >& error);
      virtual void onRequestContextAckError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::RequestContextAckError >& error) ;
      virtual void onRequestContextError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::RequestContextError >& error);
      virtual void onClearContextStackError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::ClearContextStackError >& error);
      virtual void onSwitchAppError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::SwitchAppError >& error) ;
      virtual void onVOnNewContextError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::VOnNewContextError >& error);
      virtual void onClearContextsError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::ClearContextsError >& error);
      virtual void onSetAvailableContextsError(const ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy >& proxy, const ::boost::shared_ptr< ContextSwitch::SetAvailableContextsError >& error);

   private:
      ::boost::shared_ptr< ContextSwitch::ContextSwitchProxy > _contextSwitchProxy;
      clContextProviderInterface* _contextProviderImpl;
      clContextRequestorInterface* _contextRequestorImpl;
      uint32 _applicationId;
      clContextProxy();
};


#endif // !defined(EA_DCA45655_872E_4ba0_A673_8CB4B129886F__INCLUDED_)
