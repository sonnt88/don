/**************************************************************************//**
* \file     clContextProvider_Test.cpp
*
*           clContextProvider_Test method class implementation
*
* \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
******************************************************************************/

#include "clContextProvider_Test.h"
#include "clContextProvider.h"
#include "utest/Spy/clSpy_ContextProvider.h"


#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchProxy.h"
#include "clMock_ContextProvider.h"
#include "clMock_ContextRequestor.h"


using testing::_;
using testing::ElementsAre;
using testing::Property;
using testing::Matcher;
using testing::Return;
using testing::SetArgPointee;
using testing::Mock;


using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch;
using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;



TEST_F(clContextProvider_Test, vOnNewContext_NotMyContext_DontAcceptContextRequest)
{
   ContextSwitchProxy* oMockProxy =  ContextSwitchProxy::createProxy();
   EXPECT_CALL(*oMockProxy, sendRequestContextAckRequest(_, _, _)).Times(0);
   clSpy_ContextProvider oProvider;
   oProvider.vOnNewContext(0, 20);
   delete oMockProxy;

}



TEST_F(clContextProvider_Test, vOnNewContext_MyContext_TriggervSwitchContext)
{
   ContextSwitchProxy* oMockProxy = ContextSwitchProxy::createProxy();
   EXPECT_CALL(*oMockProxy, sendRequestContextAckRequest(_, _, _)).Times(1);
   EXPECT_CALL(*oMockProxy, sendSwitchAppRequest(_, 0, 0x80, 0)).Times(1);
   clSpy_ContextProvider oProvider;
   EXPECT_CALL(oProvider, bOnNewContext(_, _)).WillOnce(Return(1));
   oProvider.vOnNewContext(0, 0);
   delete oMockProxy;
}


TEST_F(clContextProvider_Test, vOnNewContext_MyContext_AcceptContextRequest)
{
   ContextSwitchProxy* oMockProxy =  ContextSwitchProxy::createProxy();
   EXPECT_CALL(*oMockProxy, sendRequestContextAckRequest(_, ContextState__ACCEPTED, 0)).Times(1);
   clSpy_ContextProvider oProvider;
   EXPECT_CALL(oProvider, bOnNewContext(_, _)).WillOnce(Return(1));
   oProvider.vOnNewContext(0, 0);
   delete oMockProxy;

}




TEST_F(clContextProvider_Test, vOnNewContext_MyContextRejectedByDerivedApp_RejectContextRequest)
{
   ContextSwitchProxy* oMockProxy = new ContextSwitchProxy();
   EXPECT_CALL(*oMockProxy, sendRequestContextAckRequest(_, ContextState__REJECTED, 0)).Times(1);
   clSpy_ContextProvider oProvider;
   EXPECT_CALL(oProvider, bOnNewContext(_, _)).WillOnce(Return(0));
   oProvider.vOnNewContext(0, 0);
   delete oMockProxy;

}


TEST_F(clContextProvider_Test, vOnNewActiveContext_NotMyBackContext_vOnBackResponseNotCalled)
{
   ContextSwitchProxy* oMockProxy = new ContextSwitchProxy();
   clSpy_ContextProvider oProvider;
   EXPECT_CALL(oProvider, vOnBackRequestResponse(_)).Times(0);
   oProvider.vOnNewActiveContext(10, ContextState__ACCEPTED);
   delete oMockProxy;

}


TEST_F(clContextProvider_Test, vOnNewActiveContext_MyBackContextAccepted_vOnBackResponseCalled)
{
   ContextSwitchProxy* oMockProxy = new ContextSwitchProxy();
   clSpy_ContextProvider oProvider;
   oProvider.vOnNewContext(10, 0);
   EXPECT_CALL(oProvider, vOnBackRequestResponse(ContextState__ACCEPTED)).Times(1);
   oProvider.vOnNewActiveContext(10, ContextState__ACCEPTED);
   delete oMockProxy;

}


TEST_F(clContextProvider_Test, vOnNewActiveContext_MyBackContexttRejected_vOnBackResponseCalled)
{
   ContextSwitchProxy* oMockProxy = new ContextSwitchProxy();
   clSpy_ContextProvider oProvider;
   oProvider.vOnNewContext(10, 0);
   EXPECT_CALL(oProvider, vOnBackRequestResponse(ContextState__ACCEPTED)).Times(1);
   oProvider.vOnNewActiveContext(10, ContextState__ACCEPTED);
   delete oMockProxy;

}



TEST_F(clContextProvider_Test, vOnSwitchAppResponse_ContextRejected_requestContextAckRejectedResponse)
{
   ContextSwitchProxy* oMockProxy = new ContextSwitchProxy();
   clSpy_ContextProvider oProvider;
   oProvider.vOnNewContext(10, 0);
   EXPECT_CALL(*oMockProxy, sendRequestContextAckRequest(_, ContextState__REJECTED, 0)).Times(1);
   oProvider.vOnSwitchAppResponse(false);
   delete oMockProxy;
}



TEST_F(clContextProvider_Test, vOnSwitchAppResponse_ContextAccepted_requestContextAckAcceptedResponse)
{
   ContextSwitchProxy* oMockProxy = new ContextSwitchProxy();
   clSpy_ContextProvider oProvider;
   oProvider.vOnNewContext(10, 0);
   EXPECT_CALL(*oMockProxy, sendRequestContextAckRequest(_, ContextState__COMPLETED, 0)).Times(1);
   oProvider.vOnSwitchAppResponse(true);
   delete oMockProxy;
}
