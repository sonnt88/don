///////////////////////////////////////////////////////////
//  clContext.h
//  Implementation of the Class clContext
//  Created on:      13-Jul-2015 11:44:46 AM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(clContext_h)
#define clContext_h


#include "asf/core/Types.h"


/**
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 11:44:46 AM
 */


enum enContextType
{
   TEMPORARY,
   STATIC,
   DYNAMIC,
   CONTEXTTYPE_LIMIT
};


struct clContext
{
   uint32 id;
   enContextType type;
   unsigned char priority;
   uint32 surfaceId;
   ::std::string viewName;
   uint32 sourceId;
};


#endif // !defined(EA_DCA45655_872E_4ba0_A673_8CB4B129886F__INCLUDED_)
