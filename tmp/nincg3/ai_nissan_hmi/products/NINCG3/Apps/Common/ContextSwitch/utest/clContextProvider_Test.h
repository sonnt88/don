/**************************************************************************//**
* \file     clContextProvider_Test.h
*
*
* \remark   Copyright: 2010 Robert Bosch GmbH, Hildesheim
******************************************************************************/
#ifndef clContextProvider_Test_h
#define clContextProvider_Test_h

#include "gmock/gmock.h"
#include "gtest/gtest.h"

class clContextProvider_Test : public testing::Test
{
   public:
      clContextProvider_Test()
      {

      }
      virtual ~clContextProvider_Test()
      {

      }
      void SetUp()
      {
      }
      void TearDown()
      {
      }
};

#endif // clContextProvider_Test_h
