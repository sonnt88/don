/*****************************************************************************
 * (c) Robert Bosch Car Multimedia GmbH
 *
 * THIS FILE IS GENERATED. DO NOT EDIT.
 * ANY CHANGES WILL BE REPLACED ON THE NEXT GENERATOR EXECUTION
 ****************************************************************************/

#ifndef BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHJSONPRIVATE_H
#define BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHJSONPRIVATE_H

#include "asf/stream/json.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitch.h"
#include "yajl/yajl_gen.h"
#include <string>


// Json serialization of RequestContextRequest

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextRequest& value, yajl_gen& g, ::asf::stream::json::JsonSerializationOptions options);

::asf::stream::json::ParserStatus jsonParser__bosch__cm__ai__nissan__hmi__contextswitchservice__ContextSwitch__RequestContextRequest(::asf::stream::json::ParserEvent event, ::asf::stream::json::ParserContext* context, void* p1, void* p2);

// Json serialization of RequestContextResponse

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextResponse& value, yajl_gen& g, ::asf::stream::json::JsonSerializationOptions options);

::asf::stream::json::ParserStatus jsonParser__bosch__cm__ai__nissan__hmi__contextswitchservice__ContextSwitch__RequestContextResponse(::asf::stream::json::ParserEvent event, ::asf::stream::json::ParserContext* context, void* p1, void* p2);

// Json serialization of SetAvailableContextsRequest

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SetAvailableContextsRequest& value, yajl_gen& g, ::asf::stream::json::JsonSerializationOptions options);

::asf::stream::json::ParserStatus jsonParser__bosch__cm__ai__nissan__hmi__contextswitchservice__ContextSwitch__SetAvailableContextsRequest(::asf::stream::json::ParserEvent event, ::asf::stream::json::ParserContext* context, void* p1, void* p2);

// Json serialization of SwitchAppRequest

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SwitchAppRequest& value, yajl_gen& g, ::asf::stream::json::JsonSerializationOptions options);

::asf::stream::json::ParserStatus jsonParser__bosch__cm__ai__nissan__hmi__contextswitchservice__ContextSwitch__SwitchAppRequest(::asf::stream::json::ParserEvent event, ::asf::stream::json::ParserContext* context, void* p1, void* p2);

// Json serialization of SwitchAppResponse

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::SwitchAppResponse& value, yajl_gen& g, ::asf::stream::json::JsonSerializationOptions options);

::asf::stream::json::ParserStatus jsonParser__bosch__cm__ai__nissan__hmi__contextswitchservice__ContextSwitch__SwitchAppResponse(::asf::stream::json::ParserEvent event, ::asf::stream::json::ParserContext* context, void* p1, void* p2);

// Json serialization of RequestContextAckRequest

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::RequestContextAckRequest& value, yajl_gen& g, ::asf::stream::json::JsonSerializationOptions options);

::asf::stream::json::ParserStatus jsonParser__bosch__cm__ai__nissan__hmi__contextswitchservice__ContextSwitch__RequestContextAckRequest(::asf::stream::json::ParserEvent event, ::asf::stream::json::ParserContext* context, void* p1, void* p2);

// Json serialization of VOnNewContextSignal

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::VOnNewContextSignal& value, yajl_gen& g, ::asf::stream::json::JsonSerializationOptions options);

::asf::stream::json::ParserStatus jsonParser__bosch__cm__ai__nissan__hmi__contextswitchservice__ContextSwitch__VOnNewContextSignal(::asf::stream::json::ParserEvent event, ::asf::stream::json::ParserContext* context, void* p1, void* p2);

// Json serialization of ActiveContextSignal

void serializeJson(const ::bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitch::ActiveContextSignal& value, yajl_gen& g, ::asf::stream::json::JsonSerializationOptions options);

::asf::stream::json::ParserStatus jsonParser__bosch__cm__ai__nissan__hmi__contextswitchservice__ContextSwitch__ActiveContextSignal(::asf::stream::json::ParserEvent event, ::asf::stream::json::ParserContext* context, void* p1, void* p2);

#endif // BOSCH_CM_AI_NISSAN_HMI_CONTEXTSWITCHSERVICE_CONTEXTSWITCHJSONPRIVATE_H
