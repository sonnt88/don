/**
 * @file <CommonClockUtility.h>
 * @author A-IVI HMI System Team
 * @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
 * @addtogroup <AppHmi_Common>
 */

#include "hall_std_if.h"
#include "CommonClockUtility.h"
#include <stdlib.h>


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_CLOCKSETUP
#include "trcGenProj/Header/CommonClockUtility.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN


const uint8  TEXT_BUFFER_SIZE	= 10;
/*
 * @Constructor
 */
CommonClockUtility::CommonClockUtility()
{
}


/**
 * @Destructor
 */
CommonClockUtility::~CommonClockUtility()
{
}

/**
 * performDateFormatConversion - Handle to convert the display format of date with respect to dateformat
 * @param[in] year, month, day, dateformatIndex
 * @parm[out] dateText
 * @return string
 *
 */
std::string CommonClockUtility::performDateFormatConversion(uint8 dateFormatIndex, const DatePropertyInfo& stDatePropertyInfo) const
{
	std::string dateText = "";
    char itoa_txt[TEXT_BUFFER_SIZE] = {};
    snprintf(itoa_txt, sizeof(itoa_txt), "%d", stDatePropertyInfo.day);
    std::string dayText = static_cast<std::string>(itoa_txt);
    snprintf(itoa_txt, sizeof(itoa_txt), "%d", stDatePropertyInfo.month);
    std::string monthText = static_cast<std::string>(itoa_txt);
    snprintf(itoa_txt, sizeof(itoa_txt), "%d", stDatePropertyInfo.year);
    std::string yearText = static_cast<std::string>(itoa_txt);

	switch (dateFormatIndex)
	{
	case DD_MM_YY:
		dateText = (dayText + "/" + monthText + "/" + yearText);
		break;
	case MM_DD_YY:
		dateText = (monthText + "/" + dayText + "/" + yearText);
		break;
	case DD_MM_YY_DOT:
		dateText = (dayText + "." + monthText + "." + yearText);
		break;
	case DD_MM_YY_DASH:
		dateText = (dayText + "-" + monthText + "-" + yearText);
		break;
	case YYYY_DD_MM:
		dateText = (yearText + "-" + dayText + "-" + monthText);
		break;
	default:
		break;
	}
	return dateText.c_str();
}

/**
 * performEqualHourAndMeridiemCalc - Handle to calculate equal hour and meridian mode (if time format is in 12h)
 * @param[in] hour
 * @param[in] TimeDatePropertyResultInfo& stTimePropertyResult
 * @param[out] none
 * @return void
 */
std::string CommonClockUtility::performTimeFormatAndMeridiemCalc(const uint8 hour, const uint8 min, const Timeformat _timeformat) const
{
	MeridiemValue meridiemValue = performMeridiemModeCalc(_timeformat, hour);
	uint8 hourval = ((meridiemValue == INVALID_MERIDIEM_MODE) ? INVALID_HR : ((meridiemValue == TIME_24H_MODE) ? \
			hour : performEqualTweleveHourCalc(hour, meridiemValue)));

	std::string timeText = "";
	char itoa_txt[TEXT_BUFFER_SIZE] = {};
	snprintf(itoa_txt, sizeof(itoa_txt), "%d", hourval);
	std::string hourvalue = static_cast<std::string>(itoa_txt);
	snprintf(itoa_txt, sizeof(itoa_txt), "%d", min);
	std::string minvalue = static_cast<std::string>(itoa_txt);

	switch (meridiemValue)
	{
	case TIME_24H_MODE:
		timeText = hourvalue + ":" + minvalue;
		break;
	case ANTE_MERIDIEM_MODE:
		timeText = (hourvalue + ":" + minvalue + " " + "AM");// TODO: Add Text_ID once available
		break;
	case POST_MERIDIEM_MODE:
		timeText = (hourvalue + ":" + minvalue + " " + "PM");// TODO: Add Text_ID once available
		break;
	default:
		break;
	}
	return timeText.c_str();
}

/**
 * performMeridiemModeCalc - Handle to calculate Current Meridiem mode (if time format is in 12h)
 * @param[in] Timeformat _timeFormat
 * @param[in] hour
 * @parm[out] None
 * @return MeridiemValue
 *
 */
MeridiemValue CommonClockUtility::performMeridiemModeCalc(const Timeformat _timeformat, const uint8 hour) const
{
	MeridiemValue changedMeridiem;
	if ((performTwentyFourHourValidCheck(hour) == false) || (performTimeFormatValidCheck(static_cast<uint8>(_timeformat)) == false))
	{
		changedMeridiem =  INVALID_MERIDIEM_MODE;
	}
	else
	{
		changedMeridiem = ((_timeformat == TWELEVE_HR) ? ((hour >= MID_HOUR_IN_24HR_FORMAT) ? \
				POST_MERIDIEM_MODE : ANTE_MERIDIEM_MODE) : TIME_24H_MODE);
	}
	return changedMeridiem;
}

/**
 * performTwentyFourHourValidCheck - Handle to perform TwentyFourHour Valid Check
 * @param[in] currentHour
 * @param[out] None
 * @return bool
 */
bool CommonClockUtility::performTwentyFourHourValidCheck(const uint8 currentHour) const
{
   bool isValid = ((currentHour <= MAX_HOUR_IN_24HR_FORMAT) ? true : false);
   return isValid;
}

/**
 * performTimeFormatValidCheck - Handle to perform Time Format Valid Check
 * @param[in] currentTimeFormat
 * @param[out] None
 * @return bool
 */
bool CommonClockUtility::performTimeFormatValidCheck(const uint8 currentTimeFormat) const
{
   bool isValid = (((currentTimeFormat == TIME_FORMAT_12HR) || (currentTimeFormat == TIME_FORMAT_24HR)) ? \
         true : false);
   return isValid;
}

/**
 * performEqualTweleveHourCalc - Handle to calculate equal hour in 12h format (if time format is in 12h)
 * @param[in] hour
 * @param[in] MeridiemValue nMeridiemValue
 * @parm[out] None
 * @return uint8
 *
 */
uint8 CommonClockUtility::performEqualTweleveHourCalc(const uint8 hour, const MeridiemValue nMeridiemValue) const
{
   uint8 changedValue = ((performTwentyFourHourValidCheck(hour) == false) ? INVALID_HR : \
         ((nMeridiemValue == ANTE_MERIDIEM_MODE) ? ((hour == MIN_HOUR_IN_24HR_FORMAT) ? MAX_HOUR_IN_12HR_FORMAT : hour) : \
         ((hour > MAX_HOUR_IN_12HR_FORMAT) ? (hour - MAX_HOUR_IN_12HR_FORMAT) : hour)));
   return changedValue;
}



