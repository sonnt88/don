/**
 * @file <CommonClockUtility.h>
 * @author A-IVI HMI System Team
 * @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
 * @addtogroup <AppHmi_Common>
 */


#ifndef COMMON_CLOCK_UTILITY_H
#define COMMON_CLOCK_UTILITY_H

const uint8  INVALID_HR			            = 25;
const uint8  MAX_HOUR_IN_24HR_FORMAT       	= 23;
const uint8  MID_HOUR_IN_24HR_FORMAT        = 12;
const uint8  MIN_HOUR_IN_24HR_FORMAT       	= 0;
const uint8  MAX_HOUR_IN_12HR_FORMAT        = 12;
const uint8  MIN_HOUR_IN_12HR_FORMAT        = 1;
const uint8  TIME_FORMAT_12HR            	= 1;
const uint8  TIME_FORMAT_24HR    			= 2;


/*/**
 *  DateFormatTypeIndex - To handle Date Format
 */
enum DateFormatTypeIndex
{
	DD_MM_YY,
	MM_DD_YY,
	DD_MM_YY_DOT,
	DD_MM_YY_DASH,
	YYYY_DD_MM,
};

/*/**
 *  TimeFormat - To handle Time Format
 */
enum Timeformat
{
   TWELEVE_HR = 1u,
   TWENTYFOUR_HR = 2u,
};

/**
 *  MeridiemValue - To handle Meridiem values
 */
enum MeridiemValue
{
   ANTE_MERIDIEM_MODE,
   POST_MERIDIEM_MODE,
   TIME_24H_MODE,
   INVALID_MERIDIEM_MODE
};

/**
 *  DatePropertyInfo - To handle Date Info
 */
struct DatePropertyInfo
{
	uint8 day;
	uint8 month;
	int16 year;
};

class CommonClockUtility
{
   public:
	virtual~CommonClockUtility();
	CommonClockUtility();


	std::string performDateFormatConversion(uint8 dateFormatIndex, const DatePropertyInfo&) const;
	std::string performTimeFormatAndMeridiemCalc(const uint8 hour, const uint8 min, const Timeformat _timeformat) const;
	MeridiemValue performMeridiemModeCalc(const Timeformat _timeformat, const uint8 hour) const;
	bool performTwentyFourHourValidCheck(const uint8 currentHour) const;
	bool performTimeFormatValidCheck(const uint8 currentTimeFormat) const;
	uint8 performEqualTweleveHourCalc(const uint8 hour, const MeridiemValue nMeridiemValue) const;
};

#endif

