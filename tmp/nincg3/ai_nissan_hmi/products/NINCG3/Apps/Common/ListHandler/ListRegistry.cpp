/**
 * @file ListRegistry.cpp
 * @Author ECV2-Binu John
 * @Copyright (c) 2015 Robert Bosch Car Multimedia Gmbh
 * @addtogroup common
 */
#include "gui_std_if.h"
#include "hmi_trace_if.h" //to use trace macros
#include "Common/Common_Trace.h" //to use trace class
#include "ListRegistry.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_COMMON_LISTREGISTRY
#include "trcGenProj/Header/ListRegistry.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN


/**************************************************************************//**
* Destructor
******************************************************************************/
ListRegistry::~ListRegistry()
{
   ETG_I_REGISTER_FILE();
}


/**************************************************************************//**
* Constructor
******************************************************************************/
ListRegistry::ListRegistry()
{
}


/**
* addListImplementation - This function is to add the List Id and the corresponding class instance
* 						  to list Map for the creation of the list
* @param[in] listid
* @param[in] ListImplementation* imp
* @parm[out] void
*/
void ListRegistry::addListImplementation(uint32_t listid, ListImplementation* imp)
{
   if (_theInstance == 0)
   {
      s_initialize();
   }
   _listCallbackMap.insert(std::pair< uint32_t, ListImplementation*>(listid, imp));
}


/**
* removeListImplementation - This function is to remove the List Id from the list mao and the instance
* 							 will be destroyed when the list is empty
* @param[in] listid
* @param[in] ListImplementation* imp
* @parm[out] void
*/
void ListRegistry::removeListImplementation(uint32_t listid)
{
   std::map <uint32_t, ListImplementation*>::iterator iter = _listCallbackMap.find(listid);
   if (iter != _listCallbackMap.end())
   {
      _listCallbackMap.erase(iter);
   }
   if (_listCallbackMap.empty())
   {
      s_destroy();
   }
}


/**
* updateList - Add the items to the list and send the tSharedPtrDataProvider via ListDateProviderResMsg
* 			   courier Message to FlexiList Widget
* @param[in] ListDataInfo &listDataInfo
* @parm[out] bool
*/
bool ListRegistry::updateList(const ListDataInfo& listDataInfo)
{
   std::map <uint32_t, ListImplementation*>::iterator iter = _listCallbackMap.find(listDataInfo.listId);
   if (iter != _listCallbackMap.end())
   {
      tSharedPtrDataProvider dataProvider = (iter->second)->getListDataProvider(listDataInfo);
      if (!dataProvider.PointsToNull())
      {
         COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)->Post();
         return true;
      }
   }
   return false;
}


// ListRegistry class static instance intialization
ListRegistry* ListRegistry::_theInstance = 0;

bool ListRegistry::s_initialize()
{
   if (_theInstance == 0)
   {
      _theInstance = new ListRegistry();
      return true;
   }
   assert(false);
   return false;
}


void ListRegistry::s_destroy()
{
   if (_theInstance)
   {
      delete _theInstance;
      _theInstance = 0;
   }
}


/**
* s_getInstance - Interface used by apps to create the listregistry class instance
* @param[in] None
* @parm[out] ListRegistry&
*/
ListRegistry& ListRegistry::s_getInstance()
{
   if (_theInstance == 0)
   {
      s_initialize();
   }
   assert(_theInstance);
   return *_theInstance;
}


/**
* onCourierMessage - This courier message is triggered by button widget to inform the button press info
* @param[in] ButtonReactionMsg  oMsg
* @parm[out] bool
*/
bool ListImplementation::onCourierMessage(const ButtonReactionMsg& oMsg)
{
   //Forward the button press to unlock(if focus lock is in lock state) the list focus lock details
   performListFocusLockResetOnButtonPress(oMsg);
   //Based on the Sender parameter of the ButtonReactionMsg we can determine if the button is part of list item inside a FlexListWidget2D.
   //For those buttons we can obtain ListId, HdlRow, HdlCol and Flags which were associated to that ListItem.
   //See ListDataProviderBuilder
   ListProviderEventInfo info;
   if (ListProviderEventInfo::GetItemIdentifierInfo(oMsg.GetSender(), info))
   {
      unsigned int listId      = info.getListId();     // the list id for generic access
      unsigned int hdlRow      = info.getHdlRow();     // normaly the index
      unsigned int hdlCol      = info.getHdlCol();     // if more than 1 active element in one list row, e.g. Button in a button line
      //For each ButtonReactionMsg for buttons which are part of list items we post a ButtonListItemMsg with detailed info about the list item.
      POST_MSG((COURIER_MESSAGE_NEW(ButtonListItemUpdMsg)(listId, hdlRow, hdlCol, oMsg.GetEnReaction())));
      return true;
   }

   return false;
}


/**
* onCourierMessage - This message is received from the flexlist widget when it requires new data when the
* 					 list is displayed or scrolled
* @param[in] ListDateProviderReqMsg& oMsg
* @parm[out] bool
*/
bool ListImplementation::onCourierMessage(const ListDateProviderReqMsg& oMsg)
{
   ListDataInfo listDataInfo;
   listDataInfo.listId = oMsg.GetListId();
   listDataInfo.startIndex =  oMsg.GetStartIndex();
   listDataInfo.windowSize = oMsg.GetWindowElementSize();

   return ListRegistry::s_getInstance().updateList(listDataInfo);
}


/**
* onCourierMessage - To update the List focus lock request Info and perform the corresponding operation
* @param[in] 	: ListFocusLockReqMsg& oMsg
* @parm[out]	: None
* @return		: bool
*/
bool ListImplementation::onCourierMessage(const ListFocusLockReqMsg& oMsg)
{
   //If ListFocusLockReqMsg comes means current list item is following Focus_Progress group then
   //send ListFocusLockUpdMsg with FocusLockStatus, ListId, RowId, ColumnId
   //->Info: If focus is not locked send lock update for current item
   //			otherwise send unlock update and reset the all the values(i.e. listId, rowId, columnId)
   _focusLockStatus						= static_cast<enListFocusLockStatus>(((static_cast<uint8_t>(_focusLockStatus)) % 2) + 1); //Toggle between lock and unlock
   _currentFocusLockListInfo.listId		= oMsg.GetListId();
   _currentFocusLockListInfo.rowId 		= oMsg.GetRowId();
   _currentFocusLockListInfo.columnId 		= oMsg.GetColumnId();
   ETG_TRACE_USR4(("ListImplementation:Info:ListFocusLockReqMsg:FocusLockStatus =%d,ListId =%d,RowId =%d,ColumnId =%d", \
                   _focusLockStatus, _currentFocusLockListInfo.listId, _currentFocusLockListInfo.rowId, _currentFocusLockListInfo.columnId));
   POST_MSG((COURIER_MESSAGE_NEW(ListFocusLockUpdMsg)(static_cast<uint8_t>(_focusLockStatus), _currentFocusLockListInfo.listId, \
             _currentFocusLockListInfo.rowId, _currentFocusLockListInfo.columnId)));
   return true;
}


/**
* onCourierMessage - To update the List focus lock data reset request Info and perform the corresponding operation
* @param[in] 	: ListFocusLockDataResetReqMsg& oMsg
* @parm[out]	: None
* @return		: bool
*/
bool ListImplementation::onCourierMessage(const ListFocusLockDataResetReqMsg& /*oMsg*/)
{
   //Unlock and Clear the last focus locked list informations,
   //	whenever application request.
   clearLastListFocusLockInfo();
   return true;
}


/**
* performListFocusLockResetOnButtonPress - To reset/clear the last focus locked list informations on button press
* @param[in] 	: ListFocusLockReqMsg& oMsg
* @parm[out]	: None
* @return		: bool
*/
void ListImplementation::performListFocusLockResetOnButtonPress(const ButtonReactionMsg& oMsg)
{
   //Whenever any button is pressed, Unlock focus of current locked list
   //	(if focus lock is in lock state and not as locked list button)
   if (_focusLockStatus != List_Focus_UnLock)
   {
      ListProviderEventInfo info;
      bool isListButton = ListProviderEventInfo::GetItemIdentifierInfo(oMsg.GetSender(), info);
      if ((isListButton) && (info.getListId() == _currentFocusLockListInfo.listId) \
            && (info.getHdlRow() == _currentFocusLockListInfo.rowId))
      {
         ETG_TRACE_USR4(("ListImplementation:Info:Button press is for Focus locked button so no need to unlock"));
      }
      else
      {
         //Send unlock update to application
         ETG_TRACE_USR4(("ListImplementation:Info:Send Focus unlock update"));
         _focusLockStatus = List_Focus_UnLock;
         POST_MSG((COURIER_MESSAGE_NEW(ListFocusLockUpdMsg)(static_cast<uint8_t>(_focusLockStatus), _currentFocusLockListInfo.listId, \
                   _currentFocusLockListInfo.rowId, _currentFocusLockListInfo.columnId)));
      }
   }
   else
   {
      ETG_TRACE_USR4(("ListImplementation:Info:Focus is not locked for any list"));
   }
}


/**
* clearLastListFocusLockInfo - clear the last focus locked list informations and update the app
* @param[in] 	: ListFocusLockReqMsg& oMsg
* @parm[out]	: None
* @return		: bool
*/
void ListImplementation::clearLastListFocusLockInfo()
{
   //Unlocking(if focus lock is in lock state) and Clearing the all information about last focus locked list
   //	and update the application via ListFocusLockUpdMsg
   if (_focusLockStatus != List_Focus_UnLock)
   {
      //Send unlock update to application
      ETG_TRACE_USR4(("ListImplementation:Info:Send Focus unlock update"));
      _focusLockStatus 					= List_Focus_UnLock;
      POST_MSG((COURIER_MESSAGE_NEW(ListFocusLockUpdMsg)(static_cast<uint8_t>(_focusLockStatus), _currentFocusLockListInfo.listId, \
                _currentFocusLockListInfo.rowId, _currentFocusLockListInfo.columnId)));
   }
   _currentFocusLockListInfo.listId		= 0;
   _currentFocusLockListInfo.rowId 		= 0;
   _currentFocusLockListInfo.columnId 		= 0;
   //Send cleared update to application(-> for to clear SM variables)
   ETG_TRACE_USR4(("ListImplementation:Info:Send Focus clear update"));
   POST_MSG((COURIER_MESSAGE_NEW(ListFocusLockUpdMsg)(static_cast<uint8_t>(_focusLockStatus), _currentFocusLockListInfo.listId, \
             _currentFocusLockListInfo.rowId, _currentFocusLockListInfo.columnId)));
}
