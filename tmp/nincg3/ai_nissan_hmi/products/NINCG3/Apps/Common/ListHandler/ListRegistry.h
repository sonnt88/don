/**
 * @file ListRegistry.h
 * @Author ECV2-Binu John
 * @Copyright (c) 2015 Robert Bosch Car Multimedia Gmbh
 * @addtogroup common
 */
#ifndef ListRegistry_h
#define ListRegistry_h


#include "ProjectBaseMsgs.h" //to use project base messages -sve2cob


// info received from list widget
struct ListDataInfo
{
   uint32_t listId;
   uint32_t startIndex;
   uint32_t windowSize;
};


/**
 *  Enum enListFocusLockStatus is used to
 *  	- Maintain the focus lock status of current list.
 */
enum enListFocusLockStatus
{
   List_Focus_UnLock = 1,
   List_Focus_Lock
};


/**
 *  Struct stFocuslockInfo is used to
 *  	- Hold the focus lock list item info.
 */
struct stFocuslockInfo
{
   uint32_t listId;
   uint32_t rowId;
   uint32_t columnId;
};


class ListImplementation
{
   public:
      ListImplementation() : _focusLockStatus(List_Focus_UnLock)
      {
         _currentFocusLockListInfo.listId		= 0;
         _currentFocusLockListInfo.rowId 		= 0;
         _currentFocusLockListInfo.columnId 		= 0;
      }
      virtual ~ListImplementation()
      {
         _focusLockStatus = List_Focus_UnLock;
      }

      //interface to be used by apps to provide data to the list widget
      virtual tSharedPtrDataProvider getListDataProvider(const ListDataInfo& listDataInfo) = 0;

      // apps can override the below methods if the default handling needs to be extended
      virtual bool onCourierMessage(const ButtonReactionMsg& oMsg);
      virtual bool onCourierMessage(const ListDateProviderReqMsg& oMsg);

      //register for list widget messages
      COURIER_MSG_MAP_BEGIN(0)
      ON_COURIER_MESSAGE(ButtonReactionMsg)
      ON_COURIER_MESSAGE(ListDateProviderReqMsg)
      ON_COURIER_MESSAGE(ListFocusLockReqMsg)
      ON_COURIER_MESSAGE(ListFocusLockDataResetReqMsg)
      COURIER_MSG_MAP_END();

   private:
      bool onCourierMessage(const ListFocusLockReqMsg& oMsg);
      bool onCourierMessage(const ListFocusLockDataResetReqMsg& oMsg);
      void performListFocusLockResetOnButtonPress(const ButtonReactionMsg& oMsg);
      void clearLastListFocusLockInfo();
      enListFocusLockStatus _focusLockStatus; //to identify focus lock current status
      stFocuslockInfo _currentFocusLockListInfo; //to identify focus lock current list info
};


class ListRegistry
{
   public:
      ListRegistry();
      virtual ~ListRegistry();
      ListRegistry(const ListRegistry&);

      void addListImplementation(uint32_t listid, ListImplementation* imp);
      void removeListImplementation(uint32_t listid);
      bool updateList(const ListDataInfo& listDataInfo);

      static bool s_initialize();
      static void s_destroy();
      static ListRegistry& s_getInstance();

   private:
      ::std::map <uint32_t, ListImplementation*> _listCallbackMap;
      static ListRegistry* _theInstance;
};


#endif
