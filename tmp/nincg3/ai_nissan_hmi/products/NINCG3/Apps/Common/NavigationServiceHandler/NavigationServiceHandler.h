/**
 * @file <NavigationServiceHandler.h>
 * @author A-IVI HMI Team
 * @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
 * @addtogroup <AppHmi_Common>
 */


#ifndef NAVIGATIONSERVICE_HANDLER_H
#define NAVIGATIONSERVICE_HANDLER_H

#include "BaseContract/generated/BaseMsgs.h"
#include "AppBase/ServiceAvailableIF.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"
#include "ProjectBaseDataBinding.h"
#include "ProjectBaseMsgs.h"

namespace App {
namespace Core {

using namespace org::bosch::cm::navigation::NavigationService;


class INavigationService
{
   public:
      virtual ~INavigationService() {};
      //This function has to be overwritten in the derived class to get and handle the update for TBT.
      virtual void onNextManeuverDetailsUpdate(const bool& isRGActive, const TBTManeuverInfoData& TBTManeuverInfo) = 0;
};


class NavigationServiceHandler:
   public hmibase::ServiceAvailableIF
   , public StartupSync::PropertyRegistrationIF
   , public NavStatusCallbackIF
   , public NextManeuverDetailsCallbackIF
   , public ManeuverSymbolCallbackIF
   , public RoundaboutExitNumberCallbackIF
   , public RetriggerAcousticOutputCallbackIF
{
   public:
      /**
       *  Member Function Declaration
       *
       */

      NavigationServiceHandler();
      virtual ~NavigationServiceHandler();
      /**
       * Utility function that registers for notifications updates from NavigationService.
       */
      void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, \
                              const asf::core::ServiceStateChange& stateChange);
      /**
       * Utility function that deregisters for notifications updates from NavigationService.
       */
      void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, \
                                const asf::core::ServiceStateChange& stateChange);
      /**
       * Error handling function for NavStatus Property.
       */
      void onNavStatusError(const ::boost::shared_ptr< NavigationServiceProxy >& proxy, \
                            const ::boost::shared_ptr< NavStatusError >& error);
      /**
       * Update handling function for NavStatus Property.
       */
      void onNavStatusUpdate(const ::boost::shared_ptr< NavigationServiceProxy >& proxy, \
                             const ::boost::shared_ptr< NavStatusUpdate >& update);
      /**
       * Error handling function for ManeuverSymbol Property.
       */
      void onManeuverSymbolError(const ::boost::shared_ptr< NavigationServiceProxy >& proxy, \
                                 const ::boost::shared_ptr< ManeuverSymbolError >& error);
      /**
       * Update handling function for ManeuverSymbol Property.
       */
      void onManeuverSymbolUpdate(const ::boost::shared_ptr< NavigationServiceProxy >& proxy, \
                                  const ::boost::shared_ptr< ManeuverSymbolUpdate >& update);
      /**
       * Error handling function for RoundaboutExitNumber Property.
       */
      void onRoundaboutExitNumberError(const ::boost::shared_ptr< NavigationServiceProxy >& proxy, \
                                       const ::boost::shared_ptr< RoundaboutExitNumberError >& error);
      /**
       * Update handling function for RoundaboutExitNumber Property.
       */
      void onRoundaboutExitNumberUpdate(const ::boost::shared_ptr< NavigationServiceProxy >& proxy, \
                                        const ::boost::shared_ptr< RoundaboutExitNumberUpdate >& update);
      /**
       * Error handling function for NextManeuverDetails Property.
       */
      void onNextManeuverDetailsError(const ::boost::shared_ptr< NavigationServiceProxy >& proxy, \
                                      const ::boost::shared_ptr< NextManeuverDetailsError >& error);
      /**
       * Update handling function for NextManeuverDetails Property.
       */
      void onNextManeuverDetailsUpdate(const ::boost::shared_ptr< NavigationServiceProxy >& proxy, \
                                       const ::boost::shared_ptr< NextManeuverDetailsUpdate >& update);
      /**
       * Register call back function to receive updates.
       */
      void vRegisterforTBTNotification(INavigationService* client);
      /**
       * De-Register call back function to receive updates.
       */
      void vDeRegisterforTBTNotification(INavigationService* client);
      /**
       * Error handling function for RetriggerAcousticOutput Property.
       */
      void onRetriggerAcousticOutputError(const ::boost::shared_ptr< NavigationServiceProxy >& proxy, \
                                          const ::boost::shared_ptr< RetriggerAcousticOutputError >& error);
      /**
       * Response handling function for RetriggerAcousticOutput Property.
       */
      void onRetriggerAcousticOutputResponse(const ::boost::shared_ptr< NavigationServiceProxy >& proxy, \
                                             const ::boost::shared_ptr< RetriggerAcousticOutputResponse >& response);
      /**
       * Helper function to get Instance of NavigationServiceHandler class.
       */
      static NavigationServiceHandler* GetInstance();
      /**
       * Courier Message Handler
       */
      COURIER_MSG_MAP_BEGIN(0)
      ON_COURIER_MESSAGE(TBTRetriggerAcousticMsg);
      COURIER_MSG_MAP_END()

   protected:
      /**
       * Call back to handle Courier Message SxmWeatherNearInfoReqMsg Request
       */
      bool onCourierMessage(const TBTRetriggerAcousticMsg& oMsg);
   private:
      //Member Variables
      ::boost::shared_ptr<NavigationServiceProxy> _mPtrNavigationServiceProxy;
      static NavigationServiceHandler* _navigationServiceHandler;
      std::vector<INavigationService*> _navigationServiceobsr;
      TBTManeuverInfoData _mNavigationServiceInfoData;
      bool _isRGActive;
      /**
       * Helper function to notify updates to client
       */
      void vNotifyUpdates();
};


}
}// namespace

#endif //NAVIGATIONSERVICE_HANDLER_H
