/**
 * @file <NavigationServiceHandler.cpp>
 * @author A-IVI HMI Team
 * @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
 * @addtogroup <AppHmi_Common>
 */

#include "gui_std_if.h"
#include "AppBase/StartupSync/StartupSync.h"
#include "NavigationServiceHandler.h"

namespace App {
namespace Core {
NavigationServiceHandler* NavigationServiceHandler::_navigationServiceHandler = NULL;

/*
 * @Constructor : NavigationServiceHandler
 */
NavigationServiceHandler::NavigationServiceHandler()
   : _mPtrNavigationServiceProxy(NavigationServiceProxy::createProxy("NavigationPort", *this))
   , _isRGActive(false)
{
   StartupSync::getInstance().registerPropertyRegistrationIF(this);
   _navigationServiceobsr.clear();
   _mNavigationServiceInfoData.mDistanceToManeuver = "";
   _mNavigationServiceInfoData.mManeuverSymbolIndex = 51;//No Symbol
   _mNavigationServiceInfoData.mRoundAboutExitNumber = "";
   _mNavigationServiceInfoData.mShowManeuverInfo = false;
}


/*
 * @Destructor : NavigationServiceHandler
 */
NavigationServiceHandler::~NavigationServiceHandler()
{
   _navigationServiceobsr.clear();
}


/**
 * CallBack function for availability of NavigationService.
 * @param [in] : proxy - NavigationService class instance
 * @param [in] : stateChange - Current ASF state
 */
void NavigationServiceHandler::registerProperties(const boost::shared_ptr<asf::core::Proxy>& proxy, \
      const asf::core::ServiceStateChange& /*stateChange*/)
{
   if ((proxy == _mPtrNavigationServiceProxy) && (_mPtrNavigationServiceProxy))
   {
      _mPtrNavigationServiceProxy->sendNavStatusRegister(*this);
      _mPtrNavigationServiceProxy->sendManeuverSymbolRegister(*this);
      _mPtrNavigationServiceProxy->sendNextManeuverDetailsRegister(*this);
      _mPtrNavigationServiceProxy->sendRoundaboutExitNumberRegister(*this);
   }
}


/**
 *  CallBack function for unavailability of NavigationService.
 * @param [in] : proxy - NavigationService class instance
 * @param [in] : stateChange - Current ASF state
 */
void NavigationServiceHandler::deregisterProperties(const boost::shared_ptr<asf::core::Proxy>& proxy, \
      const asf::core::ServiceStateChange& /*stateChange*/)
{
   if ((proxy == _mPtrNavigationServiceProxy) && (_mPtrNavigationServiceProxy))
   {
      _mPtrNavigationServiceProxy->sendNavStatusDeregisterAll();
      _mPtrNavigationServiceProxy->sendManeuverSymbolDeregisterAll();
      _mPtrNavigationServiceProxy->sendNextManeuverDetailsDeregisterAll();
      _mPtrNavigationServiceProxy->sendRoundaboutExitNumberDeregisterAll();
   }
}


/**
 *  Error handling function for NavStatus Property.
 * @param [in] : proxy - NavigationService class instance
 * @param [in] : error -  Error message of "NavStatus".
 */
void NavigationServiceHandler::onNavStatusError(const ::boost::shared_ptr< NavigationServiceProxy >&  /*proxy*/, \
      const ::boost::shared_ptr< NavStatusError >& /*error*/)
{
}


/**
 * Update handling function for NavStatus Property.
 * @param [in] : proxy - NavigationService class instance
 * @param [in] : update - Update message of "NavStatus".
 */
void NavigationServiceHandler::onNavStatusUpdate(const ::boost::shared_ptr< NavigationServiceProxy >&  /*proxy*/, \
      const ::boost::shared_ptr< NavStatusUpdate >& update)
{
   if (update->getNavStatus() == NavStatus__NAVSTATUS_IDLE)
   {
      _isRGActive = false;
      vNotifyUpdates();
   }
   else if (update->getNavStatus() == NavStatus__NAVSTATUS_GUIDANCE_ACTIVE)
   {
      _isRGActive = true;
   }
   _mNavigationServiceInfoData.mShowManeuverInfo = _isRGActive;
}


/**
 *  Error handling function for ManeuverSymbol Property.
 * @param [in] : proxy - NavigationService class instance
 * @param [in] : error -  Error message of "ManeuverSymbol".
 */
void NavigationServiceHandler::onManeuverSymbolError(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/, \
      const ::boost::shared_ptr< ManeuverSymbolError >& /*error*/)
{
}


/**
 *  Update handling function for ManeuverSymbol Property.
 * @param [in] : proxy - NavigationService class instance
 * @param [in] : Update -  Update message of "ManeuverSymbol".
 */
void NavigationServiceHandler::onManeuverSymbolUpdate(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/, \
      const ::boost::shared_ptr< ManeuverSymbolUpdate >& update)
{
   if (update->getManeuverSymbol() <= 1)
   {
      _mNavigationServiceInfoData.mManeuverSymbolIndex = 51; //NoSymbol
   }
   else
   {
      _mNavigationServiceInfoData.mManeuverSymbolIndex = static_cast<Candera::UInt32>(update->getManeuverSymbol());
   }
}


/**
 *  Error handling function for RoundaboutExitNumber Property.
 * @param [in] : proxy - NavigationService class instance
 * @param [in] : error -  Error message of "RoundaboutExitNumber".
 */
void NavigationServiceHandler::onRoundaboutExitNumberError(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/, \
      const ::boost::shared_ptr< RoundaboutExitNumberError >& /*error*/)
{
}


/**
 *  Update handling function for RoundaboutExitNumber Property.
 * @param [in] : proxy - NavigationService class instance
 * @param [in] : Update -  Update message of "RoundaboutExitNumber".
 */
void NavigationServiceHandler::onRoundaboutExitNumberUpdate(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/, \
      const ::boost::shared_ptr< RoundaboutExitNumberUpdate >& update)
{
   std::ostringstream sRoundAboutExitNumber;
   sRoundAboutExitNumber << update->getRoundaboutExitNumber();
   _mNavigationServiceInfoData.mRoundAboutExitNumber = sRoundAboutExitNumber.str().c_str();
}


/**
 *  Error handling function for NextManeuverDetails Property.
 * @param [in] : proxy - NavigationService class instance
 * @param [in] : error -  Error message of "NextManeuverDetails".
 */
void NavigationServiceHandler::onNextManeuverDetailsError(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/, \
      const ::boost::shared_ptr< NextManeuverDetailsError >& /*error*/)
{
}


/**
 *  Update handling function for NextManeuverDetails Property.
 * @param [in] : proxy - NavigationService class instance
 * @param [in] : Update -  Update message of "NextManeuverDetails".
 */
void NavigationServiceHandler::onNextManeuverDetailsUpdate(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/, \
      const ::boost::shared_ptr< NextManeuverDetailsUpdate >& update)
{
   _mNavigationServiceInfoData.mDistanceToManeuver = update->getNextManeuverDetails().getDistanceToManeuver().c_str();
   //Send updates only after receiving Property NextManeuverDetails,has it is last in sequence.
   vNotifyUpdates();
}


/**
 * Register call back function to receive updates.
 * @param [in] : client - client instance
 */
void NavigationServiceHandler::vRegisterforTBTNotification(INavigationService* client)
{
   std::vector<INavigationService*>::const_iterator itr = std::find(_navigationServiceobsr.begin(), _navigationServiceobsr.end(), client);
   if (itr == _navigationServiceobsr.end())
   {
      _navigationServiceobsr.push_back(client);
   }
}


/**
 * De-Register call back function to receive updates.
 * @param [in] : client - client instance
 */
void NavigationServiceHandler::vDeRegisterforTBTNotification(INavigationService* client)
{
   std::vector<INavigationService*>::iterator itr;
   for (itr = _navigationServiceobsr.begin(); itr != _navigationServiceobsr.end(); ++itr)
   {
      if (client == *itr)
      {
         _navigationServiceobsr.erase(itr);
         break;
      }
   }
}


/**
 *  Error handling function for RetriggerAcousticOutput Property.
 * @param [in] : proxy - NavigationService class instance
 * @param [in] : error -  Error message of "RetriggerAcousticOutput".
 */
void NavigationServiceHandler::onRetriggerAcousticOutputError(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/, \
      const ::boost::shared_ptr< RetriggerAcousticOutputError >& /*error*/)
{
}


/**
 *  Response handling function for RetriggerAcousticOutput Property.
 * @param [in] : proxy - NavigationService class instance
 * @param [in] : error -  Response message of "RetriggerAcousticOutput".
 */
void NavigationServiceHandler::onRetriggerAcousticOutputResponse(const ::boost::shared_ptr< NavigationServiceProxy >& /*proxy*/, \
      const ::boost::shared_ptr< RetriggerAcousticOutputResponse >& /*response*/)
{
}


/**
 * Helper function to notify updates to client
 */
void NavigationServiceHandler::vNotifyUpdates()
{
   std::vector<INavigationService*>::const_iterator itr;
   for (itr = _navigationServiceobsr.begin(); itr != _navigationServiceobsr.end(); ++itr)
   {
      (*itr)->onNextManeuverDetailsUpdate(_isRGActive, _mNavigationServiceInfoData);
   }
}


/**
 * Helper function to get Instance of NavigationServiceHandler class.
 */
NavigationServiceHandler* NavigationServiceHandler::GetInstance()
{
   if (_navigationServiceHandler == NULL)
   {
      _navigationServiceHandler = new NavigationServiceHandler();
   }
   return _navigationServiceHandler;
}


/**
 * Callback to handle Courier Message TBTRetriggerAcousticMsg Request.
 *
 * @param [in] : Reference of Courier::TBTRetriggerAcousticMsg
 * @return     : Returns true when TBTRetriggerAcousticMsg is processed.
 */
bool NavigationServiceHandler::onCourierMessage(const TBTRetriggerAcousticMsg& oMsg)
{
   if (_mPtrNavigationServiceProxy)
   {
      _mPtrNavigationServiceProxy->sendRetriggerAcousticOutputRequest(*this);
   }
   return true;
}


//  SpeedLimit Functions End
}


} // namespace
