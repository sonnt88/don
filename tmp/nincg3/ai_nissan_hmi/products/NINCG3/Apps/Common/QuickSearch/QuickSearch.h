/*
 * QuickSearch.h
 *
 *  Created on: Apr 1, 2015
 *      Author: kng3kor
 */

#ifndef QUICKSEARCH_H_
#define QUICKSEARCH_H_


#define MIDW_COMMON_S_IMPORT_INTERFACE_UTF8_SORT
#include "midw_common_if.h"



enum enQSApplication
{
   enAppIpod = 0,
   enAppNotIpod
};

enum e_QUICK_SEARCH_LANG
{
   QUICK_SEARCH_UNKNOWN,
   QUICK_SEARCH_RUSSIN,
   QUICK_SEARCH_THAI,
   QUICK_SEARCH_LATIN,
   QUICK_SEARCH_ARABIC,
   QUICK_SEARCH_GREEK
};


class QuickSearch
{
   public:

      virtual ~QuickSearch();
      QuickSearch(enQSApplication eApp = enAppNotIpod);

      bool bUpdateQsLanguageAndCurrentGrpTable(e_QUICK_SEARCH_LANG enCurrentSystemLanguage, tclUtf8StringGroup::TableType enTableType);
      std::string oGetQSCurrentCharacter( std::string& oStr);
      std::string oGetQsNextCharacter();
      std::string oGetQsPreviousCharacter();
      e_QUICK_SEARCH_LANG enGetCurrentQSLanguage();
      int16 u16GetQSCurrentCharacterGroupIndex() const;

   private:

      tclUtf8StringGroup* m_QuickSearchStringGroup;
      int16 m_u16IdxQSCharGroupList;
      enQSApplication m_eApp;
      e_QUICK_SEARCH_LANG enCurrentLanguage;
};



#endif /* QUICKSEARCH_H_ */
