/*
 * QuickSearch.cpp
 *
 *  Created on: Apr 1, 2015
 *      Author: kng3kor
 */

#include "hall_std_if.h"
#include "QuickSearch.h"

/**
 * @Constructor
 */
QuickSearch::QuickSearch(enQSApplication eApp)
   : m_QuickSearchStringGroup(NULL)
   , m_eApp(eApp)
   , m_u16IdxQSCharGroupList(0)
   , enCurrentLanguage(QUICK_SEARCH_UNKNOWN)
{
   //create default table group for latin
   bUpdateQsLanguageAndCurrentGrpTable(QUICK_SEARCH_LATIN, tclUtf8StringGroup::enTableLatin);
}

/**
 * @Destructor
 */
QuickSearch::~QuickSearch()
{
   if (m_QuickSearchStringGroup != NULL)
   {
      delete m_QuickSearchStringGroup;
   }

}

/**
 * bUpdateQsLanguageAndCurrentGrpTable - To set current quick search language, and create current language table object
 *                                       this function need to call whenver system language is changed
 * @param[in] enCurrentSystemLanguage - Current language
 * @param[in] enTableType - current group table type
 * @return false if group table creation failed
 */
bool QuickSearch::bUpdateQsLanguageAndCurrentGrpTable(e_QUICK_SEARCH_LANG enCurrentSystemLanguage,
      tclUtf8StringGroup::TableType enTableType)
{
   bool bResult = false;
   if (m_QuickSearchStringGroup != NULL)
   {
      delete m_QuickSearchStringGroup;
      m_QuickSearchStringGroup = NULL;
   }

   m_QuickSearchStringGroup = new tclUtf8StringGroup(enTableType);
   if (m_QuickSearchStringGroup != NULL)
   {
      bResult = true;
   }
   enCurrentLanguage = enCurrentSystemLanguage;
   return bResult;
}



/**
 * vInitQuickSearch - This function is used to update base index of corrosponding charcter table
 * @param[in] enLang Quick Search Language
 * @parm[in] oStr - Current string for which index is requested
 * @return std::string&
 */
std::string QuickSearch::oGetQSCurrentCharacter( std::string& oStr)
{
   m_u16IdxQSCharGroupList = 0;
   std::string oCurrentQSChar("");
   const unsigned char* pQsInitChar = (const unsigned char*)oStr.c_str();

   if (m_QuickSearchStringGroup != NULL)
   {
      m_u16IdxQSCharGroupList = m_QuickSearchStringGroup->s32GetBaseIndex(pQsInitChar);

      oCurrentQSChar.assign((const char*)(*m_QuickSearchStringGroup)[m_u16IdxQSCharGroupList]);
   }

   if (oCurrentQSChar == "0")
   {
      char unicodeQSArr[4] = {0xef, 0xa1, 0xa0, 0x00};
      oCurrentQSChar.assign(unicodeQSArr);
   }
   return oCurrentQSChar;
}


/**
 * oGetNextCharacterGroup - update base index to next character and get next char
 * @param[in/out] void
 * @return std::string&
 */
std::string QuickSearch::oGetQsNextCharacter()
{
   std::string oNextQSChar("");

   if (m_QuickSearchStringGroup != NULL)
   {
      if (m_u16IdxQSCharGroupList >= m_QuickSearchStringGroup->s32GetMaxEntries() - 1)
      {
         m_u16IdxQSCharGroupList = 0;
      }
      else
      {
         m_u16IdxQSCharGroupList++;
      }

      oNextQSChar.assign((const char*)(*m_QuickSearchStringGroup)[m_u16IdxQSCharGroupList]);
   }

   if (oNextQSChar == "0")
   {
      char unicodeQSArr[4] = {0xef, 0xa1, 0xa0, 0x00};
      oNextQSChar.assign(unicodeQSArr);
   }
   return oNextQSChar;
}


/**
 * oGetQsPreviousCharacter - update base index to previous character and get previous char
 * @param[in/out] void
 * @return std::string&
 */
std::string QuickSearch::oGetQsPreviousCharacter()
{
   std::string oPreviousQSChar("");

   if (m_QuickSearchStringGroup != NULL)
   {
      if (m_u16IdxQSCharGroupList == 0)
      {
         m_u16IdxQSCharGroupList = m_QuickSearchStringGroup->s32GetMaxEntries() - 1;
      }
      else
      {
         m_u16IdxQSCharGroupList--;
      }
      oPreviousQSChar.assign((const char*)(*m_QuickSearchStringGroup)[m_u16IdxQSCharGroupList]);
   }
   if (oPreviousQSChar == "0")
   {
      char unicodeQSArr[4] = {0xef, 0xa1, 0xa0, 0x00};
      oPreviousQSChar.assign(unicodeQSArr);
   }
   return oPreviousQSChar;
}



/**
 * vInitQuickSearch - To get current quick search language
 * @param[in/out] void
 * @return e_QUICK_SEARCH_LANG
 */
e_QUICK_SEARCH_LANG QuickSearch::enGetCurrentQSLanguage()
{
   return enCurrentLanguage;
}


/**
 * vInitQuickSearch - To get current selected character base index
 * @param[in] void
 * @return int16
 */
int16 QuickSearch::u16GetQSCurrentCharacterGroupIndex() const
{
   return m_u16IdxQSCharGroupList;
}

