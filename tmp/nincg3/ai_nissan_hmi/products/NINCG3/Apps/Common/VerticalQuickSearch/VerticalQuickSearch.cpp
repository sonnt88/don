/*
 * VerticalQuickSearch.cpp
 *
 *  Created on: Jan 24, 2016
 *      Author: kng3kor
 */
#include "hall_std_if.h"
#include "VerticalQuickSearch.h"


#define MAX_DISPLAYED_CHR_INDEX_BAR 6
/**
 * @Constructor
 */
VerticalQuickSearch::VerticalQuickSearch(QuickSearchTableType tableType)
{
   _tableType = tableType;
   enCurrentLanguage = QUICK_SEARCH_UNKNOWN;
   m_QuickSearchStringTable = NULL;
   _QuickSearchLatinStringTable = new QuickSearchTableLatin();
   bUpdateQsLanguageAndCurrentGrpTable(QUICK_SEARCH_LATIN, enTableLatin);
}


/**
 * @Destructor
 */
VerticalQuickSearch::~VerticalQuickSearch()
{
   delete m_QuickSearchStringTable;
   m_QuickSearchStringTable = NULL;

   delete _QuickSearchLatinStringTable;
   _QuickSearchLatinStringTable = NULL;
}


/**
 * bUpdateQsLanguageAndCurrentGrpTable - To set current quick search language, and create current language table object
 *                                       this function need to call whenver system language is changed
 * @param[in] enCurrentSystemLanguage - Current language
 * @param[in] enTableType - current group table type
 * @return false if group table creation failed
 */
bool VerticalQuickSearch::bUpdateQsLanguageAndCurrentGrpTable(e_VERTICAL_QUICK_SEARCH_LANG enCurrentSystemLanguage,
      VerticalQuickSearch::QuickSearchTableType enTableType)
{
   bool bResult = false;
   if (m_QuickSearchStringTable != NULL)
   {
      delete m_QuickSearchStringTable;
      m_QuickSearchStringTable = NULL;
   }

   m_QuickSearchStringTable = getTableInstance(enTableType);

   if (m_QuickSearchStringTable != NULL)
   {
      bResult = true;
   }
   enCurrentLanguage = enCurrentSystemLanguage;
   return bResult;
}


QuickSearchTableBase* VerticalQuickSearch::getTableInstance(VerticalQuickSearch::QuickSearchTableType enTableType)
{
   QuickSearchTableBase* pVerticalQSTable = NULL;
   switch (enTableType)
   {
      case(VerticalQuickSearch::enTableRussain):
         pVerticalQSTable = new QuickSearchTableRussian;
         break;

      case(VerticalQuickSearch::enTableLatin):
         pVerticalQSTable = new QuickSearchTableLatin;
         break;
      case(VerticalQuickSearch::enTableArabic):
         pVerticalQSTable = new QuickSearchTableArabic;
         break;
      case(VerticalQuickSearch::enTableJapanese):
         pVerticalQSTable = new QuickSearchTableJapanese;
         break;
      default:
         break;
   }
   return pVerticalQSTable;
}


std::vector <std::string> VerticalQuickSearch::getChrListIndexBar(e_INDEX_BAR_LIST_GRP indexBarGrp)
{
   std::vector <std::string> indexBarList;
   QuickSearchTableBase* quickSearchStringTable = NULL;

   if (indexBarGrp == LATIN_GROUP)
   {
      quickSearchStringTable = _QuickSearchLatinStringTable;
   }
   else
   {
      quickSearchStringTable = m_QuickSearchStringTable;
   }

   if (quickSearchStringTable != NULL)
   {
      int numberOfSkipChar = quickSearchStringTable->getMaxEntries() / MAX_DISPLAYED_CHR_INDEX_BAR;
      if ((quickSearchStringTable->getMaxEntries() % MAX_DISPLAYED_CHR_INDEX_BAR) != 0)
      {
         numberOfSkipChar++;
      }

      int tableIndex = 0;
      for (int listIndex = 0; listIndex < MAX_DISPLAYED_CHR_INDEX_BAR;)
      {
         std::string str((const char*)quickSearchStringTable->getEntry(tableIndex));
         indexBarList.push_back(str);
         listIndex++;
         tableIndex = tableIndex + numberOfSkipChar;
         if ((tableIndex > (quickSearchStringTable->getMaxEntries() - 1)) && (listIndex < MAX_DISPLAYED_CHR_INDEX_BAR))
         {
            indexBarList.push_back((const char*) quickSearchStringTable->getEntry(quickSearchStringTable->getMaxEntries() - 1));
            break;
         }
      }
   }
   return indexBarList;
}


std::vector <std::string> VerticalQuickSearch::getChrListExtendedIndexBar(e_INDEX_BAR_LIST_GRP indexBarGrp)
{
   std::vector <std::string> indexBarList;
   QuickSearchTableBase* quickSearchStringTable = NULL;

   if (indexBarGrp == LATIN_GROUP)
   {
      quickSearchStringTable = _QuickSearchLatinStringTable;
   }
   else
   {
      quickSearchStringTable = m_QuickSearchStringTable;
   }

   if (quickSearchStringTable != NULL)
   {
      for (int index = 0; index < quickSearchStringTable->getMaxEntries(); index++)
      {
         indexBarList.push_back((const char*) quickSearchStringTable->getEntry(index));
      }
   }
   return indexBarList;
}
