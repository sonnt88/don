/*
 * VerticalQuickSearchTable.h
 *
 *  Created on: Jan 24, 2016
 *      Author: kng3kor
 */

#ifndef VERTICALQUICKSEARCHTABLE_H_
#define VERTICALQUICKSEARCHTABLE_H_

#define MAX_THRE_BYTE_CHR 3
#define MAX_FOUR_BYTE_CHR 4

static unsigned char QuickSearchChrList_Latin[][MAX_THRE_BYTE_CHR] =
{
#define VERTICAL_QUICKSEARCH_LATIN(FirstByte, SecondByte, ThirdByte)     {FirstByte, SecondByte, ThirdByte},
#include "VerticalQuickSearchTable.dat"
#undef VERTICAL_QUICKSEARCH_LATIN
};


static unsigned char QuickSearchChrList_Russian[][MAX_THRE_BYTE_CHR] =
{
#define VERTICAL_QUICKSEARCH_RUSSIAN(FirstByte, SecondByte, ThirdByte)     {FirstByte, SecondByte, ThirdByte},
#include "VerticalQuickSearchTable.dat"
#undef VERTICAL_QUICKSEARCH_RUSSIAN
};


static unsigned char QuickSearchChrList_Arabic[][MAX_THRE_BYTE_CHR] =
{
#define VERTICAL_QUICKSEARCH_ARABIC(FirstByte, SecondByte, ThirdByte)     {FirstByte, SecondByte, ThirdByte},
#include "VerticalQuickSearchTable.dat"
#undef VERTICAL_QUICKSEARCH_ARABIC
};


static unsigned char QuickSearchChrList_Japanese[][MAX_THRE_BYTE_CHR] =
{
#define VERTICAL_QUICKSEARCH_JAPANESE(FirstByte, SecondByte, ThirdByte)     {FirstByte, SecondByte, ThirdByte},
#include "VerticalQuickSearchTable.dat"
#undef VERTICAL_QUICKSEARCH_JAPANESE
};


class QuickSearchTableBase
{
   public:
      virtual ~QuickSearchTableBase() {};
      QuickSearchTableBase()
      {
         MaxChrLength = MAX_THRE_BYTE_CHR;
      };

      virtual int getMaxEntries()
      {
         return 0;
      };
      virtual int getMaxEntrySize()
      {
         return 0;
      };

      virtual unsigned char* getEntry(int pos)
      {
         (void)pos;
         return NULL;
      };

   protected:
      int MaxChrLength;
};


class QuickSearchTableLatin: public QuickSearchTableBase
{
   public:

      QuickSearchTableLatin(): QuickSearchTableBase()
      {
         MaxChrLength = MAX_THRE_BYTE_CHR;
         QuickSearchChrList = QuickSearchChrList_Latin;
      };

      virtual int getMaxEntries()
      {
         return sizeof(QuickSearchChrList_Latin) / (sizeof(char) * MaxChrLength);
      };
      virtual int getMaxEntrySize()
      {
         return MaxChrLength;
      };

      virtual unsigned char* getEntry(int pos)
      {
         return pos < getMaxEntries() ? (unsigned char*)QuickSearchChrList[pos] : 0 ;
      };

   protected:

      unsigned char(*QuickSearchChrList)[MAX_THRE_BYTE_CHR];
};


class QuickSearchTableRussian: public QuickSearchTableBase
{
   public:
      QuickSearchTableRussian(): QuickSearchTableBase()
      {
         MaxChrLength = MAX_THRE_BYTE_CHR;
         QuickSearchChrList = QuickSearchChrList_Russian;
      };

      virtual int getMaxEntries()
      {
         return sizeof(QuickSearchChrList_Russian) / (sizeof(char) * MaxChrLength);
      };
      virtual int getMaxEntrySize()
      {
         return MaxChrLength;
      };

      virtual unsigned char* getEntry(int pos)
      {
         return pos < getMaxEntries() ? (unsigned char*)QuickSearchChrList[pos] : 0 ;
      };

   protected:
      unsigned char(*QuickSearchChrList)[MAX_THRE_BYTE_CHR];
};


class QuickSearchTableArabic: public QuickSearchTableBase
{
   public:
      QuickSearchTableArabic(): QuickSearchTableBase()
      {
         MaxChrLength = MAX_THRE_BYTE_CHR;
         QuickSearchChrList = QuickSearchChrList_Arabic;
      };
      virtual int getMaxEntries()
      {
         return sizeof(QuickSearchChrList_Arabic) / (sizeof(char) * MaxChrLength);
      };
      virtual int getMaxEntrySize()
      {
         return MaxChrLength;
      };
      virtual unsigned char* getEntry(int pos)
      {
         return pos < getMaxEntries() ? (unsigned char*)QuickSearchChrList[pos] : 0 ;
      };
   protected:
      unsigned char(*QuickSearchChrList)[MAX_THRE_BYTE_CHR];
};


class QuickSearchTableJapanese: public QuickSearchTableBase
{
   public:
      QuickSearchTableJapanese(): QuickSearchTableBase()
      {
         MaxChrLength = MAX_THRE_BYTE_CHR;
         QuickSearchChrList = QuickSearchChrList_Japanese;
      };
      virtual int getMaxEntries()
      {
         return sizeof(QuickSearchChrList_Japanese) / (sizeof(char) * MaxChrLength);
      };
      virtual int getMaxEntrySize()
      {
         return MaxChrLength;
      };
      virtual unsigned char* getEntry(int pos)
      {
         return pos < getMaxEntries() ? (unsigned char*)QuickSearchChrList[pos] : 0 ;
      };
   protected:
      unsigned char(*QuickSearchChrList)[MAX_THRE_BYTE_CHR];
};


#endif /* VERTICALQUICKSEARCHTABLE_H_ */
