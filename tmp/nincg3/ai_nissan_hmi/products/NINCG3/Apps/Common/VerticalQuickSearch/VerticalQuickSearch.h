/*
 * VerticalQuickSearch.h
 *
 *  Created on: Jan 24, 2016
 *      Author: kng3kor
 */

#ifndef VERTICALQUICKSEARCH_H_
#define VERTICALQUICKSEARCH_H_

#include "VerticalQuickSearchTable.h"


class VerticalQuickSearch
{
   public:
      enum QuickSearchTableType
      {
         enTableRussain,
         enTableArabic,
         enTableJapanese,
         enTableLatin
      };

      enum e_VERTICAL_QUICK_SEARCH_LANG
      {
         QUICK_SEARCH_UNKNOWN,
         QUICK_SEARCH_RUSSIN,
         QUICK_SEARCH_ARABIC,
         QUICK_SEARCH_JAPANESE,
         QUICK_SEARCH_LATIN
      };

      enum e_INDEX_BAR_LIST_GRP
      {
         LATIN_GROUP,
         LANGUAGE_GROUP
      };

      virtual ~VerticalQuickSearch();
      VerticalQuickSearch(QuickSearchTableType tableType);
      bool bUpdateQsLanguageAndCurrentGrpTable(e_VERTICAL_QUICK_SEARCH_LANG enCurrentSystemLanguage, QuickSearchTableType enTableType);
      std::vector <std::string> getChrListIndexBar(e_INDEX_BAR_LIST_GRP);
      std::vector <std::string> getChrListExtendedIndexBar(e_INDEX_BAR_LIST_GRP indexBarGrp);

   private:

      QuickSearchTableBase* getTableInstance(QuickSearchTableType);
      QuickSearchTableType _tableType;
      QuickSearchTableBase* m_QuickSearchStringTable;
      QuickSearchTableLatin* _QuickSearchLatinStringTable;
      e_VERTICAL_QUICK_SEARCH_LANG enCurrentLanguage;
};


#endif /* VERTICALQUICKSEARCH_H_ */
