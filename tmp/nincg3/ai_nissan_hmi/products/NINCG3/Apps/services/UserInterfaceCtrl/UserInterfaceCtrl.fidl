package bosch.cm.ai.nissan.hmi.userinterfacectrl

<**
   @description : todo
**>
interface UserInterfaceCtrl
{
	<**
        @description : Property to state the Screen saver mode 
    **>
    attribute ScreenSaverModeInfo ScreenSaverMode readonly
	
	<**
		@description :Method to request registeration for a Key
		results in direct forwarding of falling back Key messages 
		of corresponding key codes to the specified pid
	**>
	method registerHardKey {
		in {
            UInt32 keyCode
			UInt32 applicationId
			UInt32 priority
	   	}
		out {
             UInt32 KeyRegistered
        }
	}

    <**
		@description :Method to request de-registeration for a Key
    **>
    method deRegisterHardKey {
        in {
            UInt32 keyCode
			UInt32 applicationId
    	}
    	out {
    		UInt32 KeyDeRegistered
    	}
    }

    <**
		@description :Method to request de-registeration for all registered Keys
    **>
    method deRegisterAllHardKeys {
        in {
			UInt32 applicationId
    	}
    	out {
    		UInt32 KeysDeRegistered
    	}
    }

    <**
        @description : Method to request lock or unlock for application surface change
    **>
    method lockApplicationChange {
        in {
			UInt32 applicationId
			UInt32 lockState
    	}
    	out {
    		UInt32 Locked
    	}
    }

   <**
        @description : Method to request for screenSaver
    **>
    method showScreenSaver {

    }
	
   <**
        @description : Method to provide available screen savers
    **>
    method setAvailableScreenSavers {
        in {
            ScreenSaverArray availableScreenSavers
    	}

    }
	
   <**
        @description : Method to enable screensaver mode
    **>
    method requestScreenSaverMode {
		in {
			UInt32 screenSaverMode
			UInt32 applicationId
    	}
    }	
    <**
        @description : Indicate change in Hardkey registration
    **>
    broadcast hardKeyRegistrationChanged {
    	out {
    	    UInt32 keyCode
			UInt32 applicationId
    	}
    }
  
    <**
        @description : Indicate change in application lock state
    **>
    broadcast applicationLockState {
    	out {
    	    UInt32 applicationId
			UInt32 lockState
    	}
    }

	array ScreenSaverArray of ScreenSaverInfo

    <**
        @description : Represents the contents of screen saver info structure 
    **>
   struct ScreenSaverInfo {
      UInt32 contextId
	  UInt32 priority
   }  
   
    <**
        @description : Represents the contents of screen saver mode info structure 
    **>
   struct ScreenSaverModeInfo {
      UInt32 screenSaverMode
	  UInt32 applicationId
   }
    
}
  