#ifndef PROJECT_H
#define PROJECT_H

#include "AppBase/app_base_defines.h"

//#include "Common/ProjectBaseContract/generated/ProjectBaseTypes.h"
#include "ProjectBaseTypes.h"

namespace hmi
{
namespace apps
{
// AppHmi_Master is predefined in the framework, see app_base_defines.h
//static const char* appHmi_Master     = "AppHmi_Master";
static const char* appHmi_Tuner      = "AppHmi_Tuner";
static const char* appHmi_Navigation = "AppHmi_Navigation";
static const char* appHmi_Media = "AppHmi_Media";
static const char* appHmi_Testmode = "AppHmi_Testmode";
static const char* appHmi_Phone = "AppHmi_Phone";
static const char* appHmi_System = "AppHmi_System";
static const char* appHmi_Sxm      = "AppHmi_Sxm";
static const char* appHmi_HomeScreen = "AppHmi_HomeScreen";
static const char* appHmi_Footerline = hmibase::appbase::appHmi_Master;
static const char* appHmi_Telematics = "AppHmi_Telematics";
static const char* appHmi_Vehicle = "AppHmi_Vehicle";
static const char* appHmi_Sds = "AppHmi_Sds";
static const char* appHmi_Spi = "AppHmi_Spi";
}

namespace main_display_config
{
static const int id = 3;
static const int width = 800;
static const int height = 480;
}

#ifdef VARIANT_S_FTR_ENABLE_SECOND_DISPLAY
namespace main_display_config
{
static const int id = 3;
static const int width = 400;
static const int height = 240;
}
#endif


}

#endif // PROJECT_H
