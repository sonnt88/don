#ifndef PROJECT_TRACE_H
#define PROJECT_TRACE_H

#ifndef HMI_TRACE_IF_INCLUDED
#error "Please include hmi_trace_if.h only"
#endif

// global define in di_linux_os/component/bosch_includes/trace/mc_trace.h
// predefined trace ids
//   TR_COMP_UI_APP_HMI_MASTER       //  ((256 * 21) +   0)
//   TR_COMP_UI_APP_HMI_PLAYGROUND   //  ((256 * 21) +  64)
//
// free trace ids for hmi applications (xx = 01-15)
//	 TR_COMP_UI_APP_HMI_xx

// predefined input channels
//  TR_TTFIS_UI_APP_HMI_MASTER     //((TR_tenTraceChan)276)
//  TR_TTFIS_UI_APP_HMI_PLAYGROUND //((TR_tenTraceChan)277)
//
// free input channels for hmi applications

// use macro to define basic trace classes and input channel
//   APPHMI_STANDARD_TRACE_SET__PREDEF_ID(component)
//   APPHMI_STANDARD_TRACE_SET(id,component)

#ifdef APPHMI_MASTER
APPHMI_STANDARD_TRACE_SET(MASTER, MASTER)
#include "App/AppHmi_Master_Trace.h"
#endif

#ifdef APPHMI_TUNER
// APPID_APPHMI_TUNER 02
// TR_COMP_UI_APP_HMI_02
// TR_TTFIS_UI_APP_HMI_02
APPHMI_STANDARD_TRACE_SET(02, TUNER)
#include "App/AppHmi_Tuner_Trace.h"
#endif

#ifdef APPHMI_NAVIGATION
// APPID_APPHMI_NAVIGATION 03
// TR_COMP_UI_APP_HMI_03
// TR_TTFIS_UI_APP_HMI_03
APPHMI_STANDARD_TRACE_SET(03, NAVIGATION)
#include "App/AppHmi_Navigation_Trace.h"
#endif

#ifdef APPHMI_MEDIA
// APPID_APPHMI_NAVIGATION 04
// TR_COMP_UI_APP_HMI_04
// TR_TTFIS_UI_APP_HMI_04
APPHMI_STANDARD_TRACE_SET(04, MEDIA)
#include "App/AppHmi_Media_Trace.h"
#endif

#ifdef APPHMI_SYSTEM
// APPID_APPHMI_SYSTEM 05
// TR_COMP_UI_APP_HMI_05
// TR_TTFIS_UI_APP_HMI_05
APPHMI_STANDARD_TRACE_SET(05, SYSTEM)
#include "App/AppHmi_System_Trace.h"
#endif

#ifdef APPHMI_SXM
// APPID_APPHMI_SXM 06
// TR_COMP_UI_APP_HMI_06
// TR_TTFIS_UI_APP_HMI_06
APPHMI_STANDARD_TRACE_SET(06, SXM)
#include "App/AppHmi_Sxm_Trace.h"
#endif

#ifdef APPHMI_TELEMATICS
// APPID_APPHMI_TELEMATICS 07
// TR_COMP_UI_APP_HMI_07
// TR_TTFIS_UI_APP_HMI_07
APPHMI_STANDARD_TRACE_SET(07, TELEMATICS)
#include "App/AppHmi_Telematics_Trace.h"
#endif

#ifdef APPHMI_TESTMODE
// APPID_APPHMI_TESTMODE 08
// TR_COMP_UI_APP_HMI_08
// TR_TTFIS_UI_APP_HMI_08
APPHMI_STANDARD_TRACE_SET(08, TESTMODE)
#include "App/AppHmi_Testmode_Trace.h"
#endif

#ifdef APPHMI_PHONE
// APPID_APPHMI_PHONE 09
// TR_COMP_UI_APP_HMI_09
// TR_TTFIS_UI_APP_HMI_09
APPHMI_STANDARD_TRACE_SET(09, PHONE)
#include "App/AppHmi_Phone_Trace.h"
#endif

#ifdef APPHMI_VEHICLE
// APPID_APPHMI_VEHICLE 11
// TR_COMP_UI_APP_HMI_11
// TR_TTFIS_UI_APP_HMI_11
APPHMI_STANDARD_TRACE_SET(11, VEHICLE)
#include "App/AppHmi_Vehicle_Trace.h"
#endif

#ifdef APPHMI_HOMESCREEN
// APPID_APPHMI_HOMESCREEN 10
// TR_COMP_UI_APP_HMI_10
// TR_TTFIS_UI_APP_HMI_10
APPHMI_STANDARD_TRACE_SET(10, HOMESCREEN)
#include "App/AppHmi_HomeScreen_Trace.h"
#endif

#ifdef APPHMI_SDS
// APPID_APPHMI_SDS 12
// TR_COMP_UI_APP_HMI_12
// TR_TTFIS_UI_APP_HMI_12
APPHMI_STANDARD_TRACE_SET(12, SDS)
#include "App/AppHmi_Sds_Trace.h"
#endif

#ifdef APPHMI_SPI
// APPID_APPHMI_SDS 12
// TR_COMP_UI_APP_HMI_12
// TR_TTFIS_UI_APP_HMI_12
APPHMI_STANDARD_TRACE_SET(13, SPI)
#include "App/AppHmi_Spi_Trace.h"
#endif

#endif // PROJECT_TRACE_H

