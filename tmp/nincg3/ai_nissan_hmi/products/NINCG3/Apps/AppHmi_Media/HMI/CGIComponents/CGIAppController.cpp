/**
 * This is the class registered as 'Controller' at Courier message framework
 *
 * when there is no handling for a message in the statemachine, the message comes next here
 */

#include "gui_std_if.h"


#include "BaseContract/generated/BaseTypes.h"
#include "AppHmi_MasterBase/AudioInterface/AudioDefines.h"
#include "CGIAppController.h"
#include "AppHmi_MediaStateMachineData.h"

#include "ProjectBaseTypes.h"

#include "hmi_trace_if.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_APPHMI_MEDIA_MAIN
#include "trcGenProj/Header/CGIAppController.cpp.trc.h"
#endif

#define HK_PRIORITY 0


CGIAppController::CGIAppController(hmibase::services::hmiappctrl::ProxyHandler& proxyHandler) : CGIAppControllerProject(proxyHandler)
{
   _retVal = false;
   _sourceID = 0;
   _mSpiRetVal = false;
   _mspiLockRetVal = false;
   _isPlaceholdercreated = false;
}


CGIAppController::~CGIAppController()
{
}


bool CGIAppController::onCourierMessage(const SourceChangeUpdMsg& oMsg)
{
   ETG_TRACE_USR1(("SourceChangeUpdMsg received"));

   _sourceID = oMsg.GetSourceId();
   {
      if (!_mspiLockRetVal)
      {
         ETG_TRACE_USR1(("CGIAppController: before _mspiLockRetVal=%d ", _mspiLockRetVal));
         _mspiLockRetVal = TRUE;
         ETG_TRACE_USR1(("CGIAppController: _mspiLockRetVal=%d ", _mspiLockRetVal));
      }
      /*Register HK Eject for all source*/
      POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceRegisterReq)(HARDKEYCODE_HK_EJECT, HK_PRIORITY)));

      if (_sourceID == SRC_MEDIA_AUX || _sourceID == SRC_MEDIA_PLAYER || _sourceID == SRC_MEDIA_CDDA || _sourceID == SRC_PHONE_BTAUDIO || _sourceID == SRC_SPI_ENTERTAIN)
      {
         ETG_TRACE_USR1(("SourceChangeUpdMsg received: Registration done"));
         POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceRegisterReq)(HARDKEYCODE_HK_NEXT, HK_PRIORITY)));
         POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceRegisterReq)(HARDKEYCODE_HK_PREVIOUS, HK_PRIORITY)));
         POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceRegisterReq)(HARDKEYCODE_SWC_NEXT, HK_PRIORITY)));
         POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceRegisterReq)(HARDKEYCODE_SWC_PREV, HK_PRIORITY)));
      }
      else
      {
         ETG_TRACE_USR1(("SourceChangeUpdMsg received: DeRegistration done"));
         POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceDeRegisterReq)(0)));
         POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceDeRegisterReq)(HARDKEYCODE_HK_NEXT)));
         POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceDeRegisterReq)(HARDKEYCODE_HK_PREVIOUS)));
         POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceDeRegisterReq)(HARDKEYCODE_SWC_NEXT)));
         POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceDeRegisterReq)(HARDKEYCODE_SWC_PREV)));
         _retVal = false;
      }
   }
   return true;
}


bool CGIAppController::onCourierMessage(const SpiDeviceConnectedUpdMsg& oMsg)
{
   ETG_TRACE_USR1(("SpiDeviceConnectedUpdMsg received"));
   {
      if (true == oMsg.GetSpiDeviceState())
      {
         if (!_mSpiRetVal)
         {
            ETG_TRACE_USR1(("SpiDeviceConnectedUpdMsg received: Registration done"));
            POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceRegisterReq)(HARDKEYCODE_SWC_PTT, HK_PRIORITY)));
            POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceRegisterReq)(HARDKEYCODE_SWC_PHONE, HK_PRIORITY)));
            POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceRegisterReq)(HARDKEYCODE_HK_PHONE, HK_PRIORITY)));
            _mSpiRetVal = true;
         }
      }
      else
      {
         if (_mSpiRetVal)
         {
            ETG_TRACE_USR1(("SpiDeviceConnectedUpdMsg received: DeRegistration done"));
            POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceDeRegisterReq)(HARDKEYCODE_SWC_PTT)));
            POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceDeRegisterReq)(HARDKEYCODE_SWC_PHONE)));
            POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceDeRegisterReq)(HARDKEYCODE_HK_PHONE)));
            _mSpiRetVal = false;
         }
      }
   }
   return true;
}


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
bool CGIAppController::onCourierMessage(const AuxKeyRegisterUpdMsg& oMsg)
{
   bool isAuxPressed = oMsg.GetAuxKeyStatus();
   if (isAuxPressed)
   {
      POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceRegisterReq)(HARDKEYCODE_HK_AUX, HK_PRIORITY)));
   }
   else
   {
      POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceDeRegisterReq)(HARDKEYCODE_HK_AUX)));
   }
   return true;
}


#endif

bool CGIAppController::onCourierMessage(const SpiPhoneAppStateUpdMsg& oMsg)
{
   ETG_TRACE_USR1(("SpiPhoneAppStateUpdMsg received"));
   if (true == oMsg.GetIsSpiPhoneappState())
   {
      ETG_TRACE_USR1(("SpiPhoneAppStateUpdMsg received: Registration done"));
      POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceRegisterReq)(HARDKEYCODE_HK_AUX, HK_PRIORITY)));
      POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceRegisterReq)(HARDKEYCODE_HK_SETTINGS, HK_PRIORITY)));
      POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceRegisterReq)(HARDKEYCODE_HK_FM_AM, HK_PRIORITY)));
      POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceRegisterReq)(HARDKEYCODE_HK_INFO, HK_PRIORITY)));
      POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceRegisterReq)(HARDKEYCODE_HK_DISP, HK_PRIORITY)));
      POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceRegisterReq)(HARDKEYCODE_HK_MENU, HK_PRIORITY)));
   }
   else
   {
      ETG_TRACE_USR1(("SpiPhoneAppStateUpdMsg received: DeRegistration done"));
      POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceDeRegisterReq)(HARDKEYCODE_HK_AUX)));
      POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceDeRegisterReq)(HARDKEYCODE_HK_SETTINGS)));
      POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceDeRegisterReq)(HARDKEYCODE_HK_FM_AM)));
      POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceDeRegisterReq)(HARDKEYCODE_HK_INFO)));
      POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceDeRegisterReq)(HARDKEYCODE_HK_DISP)));
      POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceDeRegisterReq)(HARDKEYCODE_HK_MENU)));
   }
   return true;
}


#if defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2)|| defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R)
bool CGIAppController::onCourierMessage(const Courier::ViewPlaceholderResMsg& oMsg)
{
   ETG_TRACE_USR4(("CGIAppController: ViewPlaceholderResMsg loadorUnload:%u,Succes:%u,Loaderror:%u", oMsg.GetLoad(), oMsg.GetSuccess(), oMsg.GetLoadError()));

   if (oMsg.GetLoad() && oMsg.GetSuccess())
   {
      _isPlaceholdercreated = true;
   }
   else
   {
      _isPlaceholdercreated = false;
   }
   return true;
}


bool CGIAppController::onCourierMessage(const RenderingCompleteMsg& msg)
{
   ETG_TRACE_USR4(("CGIAppController: RenderingCompleteMsg GetViewName:%s", msg.GetViewName().GetCString()));
   ETG_TRACE_USR4(("CGIAppController: RenderingCompleteMsg _isPlaceholdercreated:%d", _isPlaceholdercreated));

   if ((msg.GetSurfaceId() == Enum_SURFACEID_MAIN_SURFACE_MEDIA) && !_isPlaceholdercreated)
   {
      Courier::Message* lpNewMessage = COURIER_MESSAGE_NEW(Courier::ViewPlaceholderReqMsg)(Courier::ViewId(msg.GetViewName().GetCString()), true);
      if (lpNewMessage != 0)
      {
         lpNewMessage->Post();
      }
   }
   CGIAppControllerProject::onCourierMessage(msg);

   return false;
}


#endif
