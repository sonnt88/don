#include "gui_std_if.h"
#include "CgiExtensions/PopupConfig.h"

#include "CGIAppViewController_Media.h"

POPUP_TABLE_BEGIN()
//          popup ID, modality,                       prio, presentation time, min. presentation time, validity period, close on superseded, close on app leave, Surface Id, path in asset
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  180, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_AUX__POPUP_USB_CHECK_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  101, 0, 30000, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_AUX__USB_POPUP_QUICKSEARCH),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 6000, 500, 0, false, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__POPUP_USB_NO_AUDIO_FILE),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 6000, 500, 0, false, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__POPUP_AUX_USB_ERROR_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 6000, 500, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  60, 6000, 500, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 6000, 500, 0, false, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 6000, 500, 0, false, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_POPUP_IPOD_ERROR_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 6000, 500, 0, false, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 6000, 500, 0, false, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  180, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_AUX__POPUP_LOADING_FILE),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  300, 0, 0, 0, false, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_GLOBAL_WAIT_SCENE),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_AUDIO_OPERATION),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_ABNORMAL_STATUS),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_READ_ERROR),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_READING),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_EJECTING),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_LOADING),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::Application,  37, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__POPUP_SOURCE_INITIALISING),

#endif

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  206, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_AUX__POPUP_USB_CHECK_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  200, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_AUX__USB_POPUP_QUICKSEARCH),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__POPUP_USB_NO_AUDIO_FILE),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__POPUP_AUX_USB_ERROR_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_POPUP_IPOD_ERROR_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  207, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_AUX__POPUP_LOADING_FILE),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  309, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_GLOBAL_WAIT_SCENE),
                  POPUP_TABLE_ENTRY_EXT(ScreenBroker::Modality::System,  32, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_AUDIO_OPERATION, true),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_ABNORMAL_STATUS),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_READ_ERROR),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  207, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_READING),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  207, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_EJECTING),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  207, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_LOADING),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::Application,  37, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__POPUP_SOURCE_INITIALISING),
#endif


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_AUX__POPUP_USB_CHECK_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  200, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_AUX__USB_POPUP_QUICKSEARCH),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  200, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_AUX__POPUP_ABCSEARCH_KEYBOARD),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__POPUP_USB_NO_AUDIO_FILE),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__POPUP_AUX_USB_ERROR_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_POPUP_IPOD_ERROR_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  207, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA_AUX__POPUP_LOADING_FILE),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  309, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_GLOBAL_WAIT_SCENE),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  32, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_AUDIO_OPERATION),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_ABNORMAL_STATUS),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  208, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_READ_ERROR),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  207, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_READING),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  207, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_EJECTING),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  207, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_LOADING),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  207, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_OVERHEATED),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  207, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__IMPOSE_CD_DISC_ERROR),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  207, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__NOSOURCE_INTERACTIVECENTERPOPUP),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  207, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__FILEERROR_INTERACTIVECENTERPOPUP),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  207, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__SETPROFILEFAILED_INTERACTIVECENTERPOPUP),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  207, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__NOVIDEOFILE_TOASTPOPUP),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  207, 0, 0, 0, false, false, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_MEDIA__NOPHOTOFILE_TOASTPOPUP),
#endif

//SPI SCOPE1 PopUps
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  21, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO_POPUP_CARPLAY_DISCLAIMER_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  21, 0, 0, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO_POPUP_CARPLAY_REMEMBER_DISCLAIMER_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 5000, 2000, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO_POPUP_CARPLAY_ADVISORY_SYSIND),

//SPI SCOPE2 PopUps
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 6000, 2000, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO__POPUP_CARPLAY_START_FAIL),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 6000, 2000, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO__POPUP_CARPLAY_STARTING_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 6000, 2000, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO__POPUP_CARPLAY_STARTED_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  21, 0, 0, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 5000, 2000, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 0, 0, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 5000, 2000, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 0, 0, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO__POPUP_CARPLAY_ADVISORY_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 6000, 2000, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO__POPUP_ANDROID_START_FAIL),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 6000, 2000, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO__POPUP_ANDROID_STARTING_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 5000, 2000, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO__POPUP_ANDROID_STARTED_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  21, 0, 0, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 5000, 2000, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 0, 0, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 5000, 2000, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 0, 0, 0, true, true, SURFACEID_CENTER_POPUP_SURFACE_MEDIA, CGIAppViewController_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  37, 0, 0, 0, true, true, SURFACEID_TOP_POPUP_SURFACE_MEDIA, CGIAppViewController_SPI_IMPOSE_CARPLAY_CALL_INFO),

                  POPUP_TABLE_END()
