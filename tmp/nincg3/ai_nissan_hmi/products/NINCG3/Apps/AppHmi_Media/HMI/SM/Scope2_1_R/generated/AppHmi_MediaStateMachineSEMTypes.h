#ifndef _visualSTATE_APPHMI_MEDIASTATEMACHINESEMTYPES_H
#define _visualSTATE_APPHMI_MEDIASTATEMACHINESEMTYPES_H

/*
 * Id:        AppHmi_MediaStateMachineSEMTypes.h
 *
 * Function:  SEM Types Header File.
 *
 * Generated: Thu May 12 14:47:35 2016
 *
 * Coder 7, 3, 2, 2426
 * 
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include <limits.h>


#define VS_BOOL            bool      
#define VS_UCHAR           unsigned char      
#define VS_SCHAR           signed char        
#define VS_UINT            unsigned int       
#define VS_INT             signed int         
#define VS_FLOAT           float              
#define VS_DOUBLE          double             
#define VS_VOID            void               
#define VS_VOIDPTR         void*              

#define VS_UINT8           unsigned char
#define VS_INT8            signed char

#define VS_UINT16          unsigned short
#define VS_INT16           signed short
#define VS_UINT16_VAARG    VS_INT
#define VS_INT16_VAARG     VS_INT

#if ((INT_MAX == 32767) && (LONG_MAX == 2147483647L))
#define VS_UINT32          unsigned long
#define VS_INT32           signed long
#define VS_UINT32_VAARG    VS_INT32
#define VS_INT32_VAARG     VS_INT32
#elif (INT_MAX == 2147483647L)
#define VS_UINT32          unsigned int
#define VS_INT32           signed int
#define VS_UINT32_VAARG    VS_INT
#define VS_INT32_VAARG     VS_INT
#else
#define VS_UINT32          (unsupported data type)
#define VS_INT32           (unsupported data type)
#define VS_UINT32_VAARG    (unsupported data type)
#define VS_INT32_VAARG     (unsupported data type)
#endif


/*
 * SEM Variable Types.
 */
#define SEM_EVENT_TYPE                   VS_UINT8
#define SEM_ACTION_EXPRESSION_TYPE       VS_UINT16
#define SEM_GUARD_EXPRESSION_TYPE        VS_UINT8
#define SEM_EXPLANATION_TYPE             VS_UINT8
#define SEM_STATE_TYPE                   VS_UINT8
#define SEM_STATE_MACHINE_TYPE           VS_UINT8
#define SEM_INSTANCE_TYPE                VS_UINT8
#define SEM_RULE_INDEX_TYPE              VS_UINT16
#define SEM_INTERNAL_TYPE                VS_UINT16
#define SEM_SIGNAL_QUEUE_TYPE            VS_UINT8
#define SEM_ACTION_FUNCTION_TYPE         VS_UINT8
#define SEM_EVENT_GROUP_TYPE             VS_UINT8
#define SEM_EGTI_TYPE                    VS_UINT8
#define SEM_RULE_TABLE_INDEX_TYPE        VS_UINT16


#endif /* ifndef _visualSTATE_APPHMI_MEDIASTATEMACHINESEMTYPES_H */
