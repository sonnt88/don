#ifndef __AppHmi_MediaStateMachineSEMLIBB_H
#define __AppHmi_MediaStateMachineSEMLIBB_H

/*
 * Id:        AppHmi_MediaStateMachineSEMLibB.h
 *
 * Function:  Contains definitions needed for API functions.
 *
 * Generated: Tue Apr 26 23:56:42 2016
 *
 * Coder 7, 3, 2, 2426
 * 
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include <stddef.h>


/* Include SEM Library Definition Header File. */
#include "AppHmi_MediaStateMachineSEMBDef.h"


#ifndef VS_TRUE
#define VS_TRUE (1)
#endif


#ifndef VS_FALSE
#define VS_FALSE (0)
#endif


#ifdef VS_RUNTIME_INFO
typedef struct
{
  signed char pSignatureVersion[VS_SIGNATURE_VERSION_LENGTH];
  signed char pSignatureContent[VS_SIGNATURE_CONTENT_LENGTH];
} AppHmi_MediaStateMachineVSRunTimeInfo;
#endif


#ifndef VS_COMPLETION_CODES_DEFINED
#define VS_COMPLETION_CODES_DEFINED
enum
{
  /*
   * Status code:     SES_OKAY.
   *
   * Explanation:     Function performed successfully.
   *
   * Solution:        None.
   */
  SES_OKAY, /* 0 */


  /*
   * Status code:     SES_FOUND.
   *
   * Explanation:     The called function has returned an identifier index number.
   *
   * Solution:        Process the returned identifier index number. If the
   *                  function SEM_GetInput or SEM_GetOutput was called, the
   *                  function can be called again to find more events or
   *                  action expressions.
   */
  SES_FOUND, /* 1 */


  /*
   * Status code:     SES_ACTIVE.
   *
   * Explanation:     The completion code has one of the two expositions:
   *                  1)  A state/event deduction is started, while an event
   *                      inquiry is active. All inquired events have not been
   *                      returned by the function SEM_GetInput.
   *                  2)  An event inquiry is started, while a state/event
   *                      deduction is active. All deduced action expressions 
   *                      have not been returned by the function SEM_GetOutput 
   *                      and the SEM_NextState has not been called in order to 
   *                      complete the state/event deduction.
   *
   * Solution:        The completion code is a warning and perhaps the
   *                  application program should be rewritten. An event inquiry
   *                  and a state/event deduction should not be active at the
   *                  same time.
   */
  SES_ACTIVE, /* 2 */


  /*
   * Error code:      SES_CONTRADICTION.
   *
   * Explanation:     A contradiction has been detected between two states in a
   *                  state machine.
   *
   * Solution:        Check the VS System.
   */
  SES_CONTRADICTION, /* 3 */


  /*
   * Error code:      SES_RANGE_ERR.
   *
   * Explanation:     You are making a reference to an identifier that does not
   *                  exist. Note that the first index number is 0. If the 
   *                  VS System has 4 identifiers of the same type and you use a
   *                  function with the variable parameter = 4, the function 
   *                  will return an SES_RANGE_ERR error. In this case the 
   *                  highest variable index number is 3.
   *
   * Solution:        The application program will check the index parameters 
   *                  with one of the following symbols defined in the SEMBDef.h 
   *                  file:
   *
   *                    VS_NOF_EVENTS
   *                    VS_NOF_STATES
   *                    VS_NOF_ACTION_FUNCTIONS
   *                    VS_NOF_STATE_MACHINES
   */
  SES_RANGE_ERR, /* 4 */


  /*
   * Error code:      SES_TEXT_TOO_LONG.
   *
   * Explanation:     The requested text is longer than the specified maximum length.
   *
   * Solution:        Increase the maximum length.
   */
  SES_TEXT_TOO_LONG, /* 5 */


   /*
   * Error code:      SES_TYPE_ERR.
   *
   * Explanation:     A text function has been called with the wrong identifier
   *                  type or the specified text is not included in the VS System.
   *
   * Solution:        Use the identifier type symbols (EVENT_TYPE, STATE_TYPE
   *                  or ACTION_TYPE) defined in this file and remember
   *                  to include wanted text in the VS System.
   */
  SES_TYPE_ERR, /* 6 */


  /*
   * Error code:      SES_EMPTY.
   *
   * Explanation:     No events have been given to the SEM_Deduct function before
   *                  calling this function.
   *
   * Solution:        Call the SEM_Deduct function with an event number.
   */
  SES_EMPTY, /* 7 */


  /*
   * Error code:      SES_BUFFER_OVERFLOW.
   *
   * Explanation:     A destination buffer cannot hold the number of items found.
   *
   * Solution:        Call the function with an extended buffer as destination.
   */
  SES_BUFFER_OVERFLOW, /* 8 */


  /*
   * Error code:      SES_SIGNAL_QUEUE_FULL.
   *
   * Explanation:     Signal queue is full.
   *
   * Solution:        Increase the maximum signal queue size in the VS System or
   *                  via the VS Coder signal queue size option.
   */
  SES_SIGNAL_QUEUE_FULL, /* 9 */


  /*
   * Error code:      SES_NOT_INITIALIZED.
   *
   * Explanation:     The system has not been initialized.
   *
   * Solution:        Call the initialization function for the VS System.
   */
  SES_NOT_INITIALIZED /* 10 */
};
#endif /* VS_COMPLETION_CODES_DEFINED */


/*
 * Name        : SEM_InitAll
 *
 * Description : The function is a wrapper to all initialization functions.
 *               The function calls the following functions in the listed
 *               order (provided the specific function exists):
 *                 SEM_Init
 *                 SEM_InitExternalVariables
 *                 SEM_InitInternalVariables
 *                 SEM_InitSignalQueue
 *                 SEM_InitInstances
 *
 * Argument    : None.
 *
 * Return      : None.
 *
 * Portability : ANSI-C Compiler.
 */


/*
 * Name        : SEM_Init
 *
 * Description : Before calling any other functions this function must be
 *               called to initialize the VS System. In addition it is
 *               possible to call the initialization functions for the
 *               signal queue, internal variables and external variables.
 *
 * Argument    : None.
 *
 * Return      : None.
 *
 * Portability : ANSI-C Compiler.
 */


/*
 * Name        : SEM_InitSignalQueue
 *
 * Description : This function initializes the signal queue in a VS System.
 *               This function must be called together with the SEM_Init
 *               function. The function will only be avaliable if the signal
 *               queue is enabled and the VS System contains signals.
 *
 * Argument    : None.
 *
 * Return      : None.
 *
 * Portability : ANSI-C Compiler.
 */


/*
 * Name        : SEM_InitInternalVariables
 *
 * Description : This function initializes the internal variables in the VS
 *               System. This function must be called together with the
 *               SEM_Init function. The function will be auto generated by
 *               the VS Coder during the code generation of a VS System if
 *               any internal variables are present and the appropriate VS
 *               Coder option has been set.
 *
 * Argument    : None.
 *
 * Return      : None.
 *
 * Portability : ANSI-C Compiler.
 */


/*
 * Name        : SEM_Deduct
 *
 * Description : The function prepares the reception of action expressions,
 *               that can be deduced on the basis of the given event, the
 *               internal current state vector and the rules in the VS
 *               System.
 *               All action expressions are deduced by continuously calling
 *               the function SEM_GetOutput or one call to the
 *               SEM_GetOutputAll function.
 *               Before calling the SEM_Deduct function again, the
 *               SEM_NextState or SEM_NextStateChg must be called to enter
 *               the new states, if any.
 *
 * Argument    : EventNo:
 *                 Event number to be processed.
 *
 *               Variable number of arguments:
 *                 Used if at least one event has a parameter.
 *                 The function call must include one argument for each type
 *                 name declared in the parameter list for each event.
 *                 
 *                 This sample declaration is for an event with three
 *                 parameters:
 *                 
 *                   EventName (VS_UINT8 par1, VS_CHAR par2, VS_INT par3)
 *                 
 *                 How to call the SEM_Deduct function for the event
 *                 EventName:
 *                 
 *                   SEM_Deduct (EventName, par1, par2, par3);
 *
 * Return      : Completion code:
 *
 *                 SES_ACTIVE:
 *                   The function SEM_Inquiry has been called. All inquired
 *                   events have not been returned by the function
 *                   SEM_GetInput or SEM_GetInputAll. The state/event
 *                   deduction is okay, but the user should not call the
 *                   function SEM_GetInput before the functions
 *                   SEM_NextState or SEM_NextStateChg and SEM_Inquiry have
 *                   been called.
 *
 *                 SES_RANGE_ERR:
 *                   Event is out of range.
 *
 *                 SES_OKAY:
 *                   Success.
 *
 * Portability : ANSI-C Compiler.
 */


/*
 * Name        : SEM_GetOutput
 *
 * Description : The function finds an action expression, if any such one
 *               has been deduced on the basis of the event given to the
 *               function SEM_Deduct, the internal current state vector and
 *               the rules in the VS System.
 *               All action expressions are found by continuous calls to the
 *               SEM_GetOutput function.
 *               When the SEM_GetOutput returna the completion code
 *               SES_OKAY, all action expressions have been found.
 *               Use SEM_Action or SEM_TableAction to call the action
 *               expression functions.
 *
 * Argument    : ActionNo:
 *                 Pointer to store the deduced action expression.
 *
 * Return      : Completion code:
 *
 *                 SES_CONTRADICTION:
 *                   Contradiction detected, the VS System is not
 *                   consistent. Check the VS System. You will also get this
 *                   error code here if you forget to call SEM_Init
 *                   function.
 *
 *                 SES_EMPTY:
 *                   No event has been given to the SEM_Deduct function,
 *                   i.e. SEM_Deduct has not been called before calling the
 *                   SEM_GetOutput function.
 *
 *                 SES_FOUND:
 *                   An action expression has been found. The user can
 *                   continue to call the SEM_GetOutput in order to have
 *                   more action expressions found.
 *
 *                 SES_SIGNAL_QUEUE_FULL:
 *                   The signal queue is full. Increase the signal queue
 *                   size in the VS System.
 *
 *                 SES_OKAY:
 *                   Success. All action expressions have been found. The
 *                   user can now call the SEM_NextState or SEM_NextStateChg
 *                   function to change state.
 *                   If SEM_Deduct is called with the same event and
 *                   SEM_NextState and SEM_NextStateChg is not called, all
 *                   action expresssions can be found again by calling
 *                   SEM_GetOutput or SEM_GetOutputAll.
 *
 * Portability : ANSI-C Compiler.
 */


/*
 * Name        : SEM_NextStateChg
 *
 * Description : The function will update the internal current state vector,
 *               if any states can be found on the basis of the given event
 *               to the function SEM_Deduct, the internal current state
 *               vector and the rules in the VS System. The returned
 *               completion code will tell if the internal state vector was
 *               updated with a new value or not. This feature could be used
 *               to determine if the SEM_Inquiry function needs to be
 *               called.
 *               The function must be enabled by its VS Coder option.
 *
 * Argument    : None.
 *
 * Return      : Completion code:
 *
 *                 SES_CONTRADICTION:
 *                   Contradiction detected, the VS System is not
 *                   consistent. Check the VS System. Your will also get
 *                   this error code here if youforget to call SEM_Init.
 *
 *                 SES_EMPTY:
 *                   No event has been given to the SEM_Deduct function,
 *                   i.e. SEM_Deduct has not been called before calling the
 *                   SEM_NextState function.
 *
 *                 SES_FOUND:
 *                   Success. The internal state vector was updated.
 *
 *                 SES_OKAY:
 *                   Success. The internal state vector was updated to the
 *                   same value as before.
 *
 * Portability : ANSI-C Compiler.
 */


/*
 * Name        : SEM_NameAbs
 *
 * Description : The function gets a pointer to the ASCII name of the
 *               specified identifier.
 *               The function must be enabled by its VS Coder option.
 *
 * Argument    : IdentType:
 *                 Must contain the identifier type of the identifier
 *                 number. It can be EVENT_TYPE, STATE_TYPE or ACTION_TYPE.
 *
 *               IdentNo:
 *                 Must contain the index number of an identifier.
 *
 *               Text:
 *                 Must be a pointer to a text string. If the function
 *                 terminates successfully, the text string contains the
 *                 name of the specified identifier.
 *
 * Return      : Completion code:
 *
 *                 SES_RANGE_ERR:
 *                   Identifier number is out of range.
 *
 *                 SES_TYPE_ERR:
 *                   The identifier type is not correct. Remember to include
 *                   the wanted text in the VS System and specify the
 *                   correct identifier type when calling this function.
 *
 *                 SES_OKAY:
 *                   Success.
 *
 * Portability : ANSI-C Compiler.
 */


/*
 * Name        : SEM_State
 *
 * Description : The function will return the current state of the specified
 *               state machine.
 *               The function must be enabled by its VS Coder option.
 *
 * Argument    : StateMachineNo:
 *                 State machine number.
 *
 *               StateNo:
 *                 Pointer to store the current state of the specified state
 *                 machine.
 *
 * Return      : Completion code:
 *
 *                 SES_RANGE_ERR:
 *                   State machine index is out of range.
 *
 *                 SES_FOUND:
 *                   Success. State number index found.
 *
 * Portability : ANSI-C Compiler.
 */


/*
 * Name        : SEM_StateAll
 *
 * Description : The function will return the current state of all the state
 *               machines.
 *               The function must be enabled by its VS Coder option.
 *
 * Argument    : StateVector:
 *                 Pointer to the array to store the current state vector.
 *
 *               MaxSize:
 *                 Specifies the length of the destination array. Must be
 *                 equal to or longer than the number of state machines.
 *
 * Return      : Completion code:
 *
 *                 SES_BUFFER_OVERFLOW:
 *                   The specified maxsize is insufficient. The state vector
 *                   is not valid.
 *
 *                 SES_FOUND:
 *                   Success. All state number indexes found.
 *
 * Portability : ANSI-C Compiler.
 */


/*
 * Base class definition
 */
class AppHmi_MediaStateMachine
{
public:
  virtual ~AppHmi_MediaStateMachine () {}

  //Identifier types, used when getting texts and explanations.
  enum {EVENT_TYPE, STATE_TYPE, ACTION_TYPE};


  static const SEM_STATE_TYPE StateUndefined;

  static const SEM_EVENT_TYPE EventUndefined;

  static const SEM_EVENT_TYPE EventGroupUndefined;

  static const SEM_EVENT_TYPE EventTerminationId;

  static const SEM_ACTION_EXPRESSION_TYPE ActionExpressionTerminationId;

  void SEM_InitAll (void);

  void SEM_Init (void);

  void SEM_InitSignalQueue (void);

  unsigned char SEM_GetOutput (SEM_ACTION_EXPRESSION_TYPE *ActionNo);

  unsigned char SEM_NextStateChg (void);

  unsigned char SEM_NameAbs (unsigned char IdentType, SEM_EXPLANATION_TYPE IdentNo, char const **Text) const;

  unsigned char SEM_State (SEM_STATE_MACHINE_TYPE StateMachineNo, SEM_STATE_TYPE *StateNo) const;

  unsigned char SEM_StateAll (SEM_STATE_TYPE *StateVector, SEM_STATE_MACHINE_TYPE MaxSize) const;

  void SEM_InitInternalVariables (void);

  unsigned char SEM_Deduct (SEM_EVENT_TYPE EventNo, ...);

  VS_VOID VSAction (SEM_ACTION_EXPRESSION_TYPE i);

  virtual VS_VOID Notify_Init_Finished (VS_VOID) = 0;

  virtual VS_VOID acDisclaimerActiveMsg (VS_VOID) = 0;

  virtual VS_VOID acDisclaimerInactiveMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_AcceptCallUpdMsg (VS_UINT32 KeyState) = 0;

  virtual VS_VOID acPerform_AutoLaunchConnectOffUpdMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_AutoLaunchConnectOnUpdMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_BrowseBtnPressUpdMsg (VS_UINT32 Row) = 0;

  virtual VS_VOID acPerform_BrowseReqMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_ClearContext (VS_VOID) = 0;

  virtual VS_VOID acPerform_DisclaimerButtonNo (VS_VOID) = 0;

  virtual VS_VOID acPerform_EncoderNegativeRotationUpdMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_EncoderPositiveRotationUpdMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_EncoderPressUpdMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_EncoderPressedUpdMsg (VS_UINT32 EncPressedState) = 0;

  virtual VS_VOID acPerform_EncoderRotatonUpdMsg (VS_UINT32 EncCount) = 0;

  virtual VS_VOID acPerform_ExitBrowse (VS_VOID) = 0;

  virtual VS_VOID acPerform_FastForwardStartReqMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_FastForwardStopReqMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_FastRewindStartReqMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_FastRewindStopReqMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_FolderBrowserBtnPress (VS_UINT32 Row) = 0;

  virtual VS_VOID acPerform_FolderUpBtnPress (VS_VOID) = 0;

  virtual VS_VOID acPerform_HMISubStateON (VS_VOID) = 0;

  virtual VS_VOID acPerform_InitialiseFolderBrowser (VS_VOID) = 0;

  virtual VS_VOID acPerform_InitializeMediaParametersUpdMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_InterruptToMeterMsg (VS_BOOL InterruptMode, VS_UINT8 InterruptType) = 0;

  virtual VS_VOID acPerform_KeyEventReqMsg (VS_UINT32 KeyMode, VS_UINT32 KeyCode) = 0;

  virtual VS_VOID acPerform_LaunchCarPlay (VS_VOID) = 0;

  virtual VS_VOID acPerform_LaunchCarplayWithApp (VS_UINT8 LaunchAppType) = 0;

  virtual VS_VOID acPerform_MediaSpiStateActiveUpdMsg (VS_UINT8 IsSpiStateActive) = 0;

  virtual VS_VOID acPerform_MetaDataBrowserBackBtnPress (VS_VOID) = 0;

  virtual VS_VOID acPerform_MetaDataBrowserBtnPress (VS_UINT32 ListId, VS_UINT32 Row) = 0;

  virtual VS_VOID acPerform_PauseReqMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_PlayFromVolumeZeroStateMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_PlayPauseReqMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_PlayReqMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_QuickSearchBtnPressUpdMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_RandomReqMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_RejectCallUpdMsg (VS_UINT32 KeyState) = 0;

  virtual VS_VOID acPerform_RepeatReqMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_ResumeReqMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_ScreenHeaderReq (VS_UINT32 SceneName) = 0;

  virtual VS_VOID acPerform_SeekNextReqMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_SeekPrevReqMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_SendDipoPlayCommand (VS_VOID) = 0;

  virtual VS_VOID acPerform_SourceToggleReqMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_StopReqMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_SwitchtoCarPlayReq (VS_VOID) = 0;

  virtual VS_VOID acPerform_TAReqMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_UserInactivityTimerStopMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_onAppStateUpdMsg (VS_UINT32 AppState) = 0;

  virtual VS_VOID acPerform_onHK_AUX_CDEventMsg (VS_VOID) = 0;

  virtual VS_VOID acPerform_onHK_BackUpdMsg (VS_UINT32 KeyState) = 0;

  virtual VS_VOID acPostBeep (VS_INT8 BeepType) = 0;

  virtual VS_VOID acRememberUsrChoiceOnNoCarPlayMsg (VS_VOID) = 0;

  virtual VS_VOID acRestartAppPopupTimer (VS_UINT32 ViewId) = 0;

  virtual VS_VOID acStartAppPopupTimer (VS_UINT32 ViewId, VS_UINT32 Timeout) = 0;

  virtual VS_VOID acStopAppPopupTimer (VS_UINT32 ViewId) = 0;

  virtual VS_BOOL dgActiveApplicationState (VS_VOID) = 0;

  virtual VS_BOOL dgBlockClockScreenOnHKILLUMLongPress (VS_VOID) = 0;

  virtual VS_BOOL dgIsMediaActive (VS_VOID) = 0;

  virtual VS_BOOL dgIsSpiPhoneAppStatus (VS_VOID) = 0;

  virtual VS_BOOL dgSetDisclaimerState (VS_VOID) = 0;

  virtual VS_BOOL dgSpeechRecognitionActiveSource (VS_VOID) = 0;

  virtual VS_BOOL dgSpiAlertActiveSource (VS_VOID) = 0;

  virtual VS_BOOL dgSpiMediaActiveSource (VS_VOID) = 0;

  virtual VS_BOOL dgSpiSessionActive (VS_VOID) = 0;

  virtual VS_VOID gacCheckToClosePopup (VS_UINT32 PopupId) = 0;

  virtual VS_VOID gacContextSwitchBackRes (VS_UINT32 SwitchId) = 0;

  virtual VS_VOID gacContextSwitchCompleteRes (VS_UINT32 SwitchId) = 0;

  virtual VS_VOID gacContextSwitchDoneRes (VS_UINT32 SwitchId) = 0;

  virtual VS_VOID gacContextSwitchFailedRes (VS_UINT32 SwitchId) = 0;

  virtual VS_VOID gacContextSwitchOutReq (VS_UINT32 TargetContextId, VS_UINT32 SourceContextId, VS_UINT32 TargetAppId) = 0;

  virtual VS_VOID gacDecrementActiveAnimationCount (VS_VOID) = 0;

  virtual VS_VOID gacDeregisterForCloseOnTouchSessionReq (VS_UINT32 ViewId) = 0;

  virtual VS_VOID gacDisplayFooterLineReq (VS_VOID) = 0;

  virtual VS_VOID gacDisplayStatusLineReq (VS_INT32 DisplayRegionType) = 0;

  virtual VS_VOID gacHideFooterLineReq (VS_VOID) = 0;

  virtual VS_VOID gacHideStatusLineReq (VS_INT32 DisplayRegionType) = 0;

  virtual VS_VOID gacPerformListBtnRightEncoderChangeUpd (VS_INT8 Steps, VS_UINT32 ListId, VS_UINT32 RowId, VS_UINT32 ColumnId) = 0;

  virtual VS_VOID gacPerformListFocusLockDataResetUpd (VS_VOID) = 0;

  virtual VS_VOID gacPopupCreateAndSBShowReq (VS_UINT32 ViewId) = 0;

  virtual VS_VOID gacPopupFilterAllEnableReq (VS_VOID) = 0;

  virtual VS_VOID gacPopupFilterDisableReq (VS_VOID) = 0;

  virtual VS_VOID gacPopupFilterPrioEnableReq (VS_INT8 Prio) = 0;

  virtual VS_VOID gacPopupRestartTimerReq (VS_UINT32 ViewId) = 0;

  virtual VS_VOID gacPopupSBCloseReq (VS_UINT32 ViewId) = 0;

  virtual VS_VOID gacProfileChange (VS_INT8 Profile) = 0;

  virtual VS_VOID gacRegisterForCloseOnTouchSession (VS_UINT32 ViewId) = 0;

  virtual VS_VOID gacSceneTransitionEnter (VS_UINT32 ViewId, VS_UINT32 Hint) = 0;

  virtual VS_VOID gacSceneTransitionLeave (VS_UINT32 ViewId, VS_UINT32 Hint) = 0;

  virtual VS_VOID gacScrollListDown (VS_VOID) = 0;

  virtual VS_VOID gacScrollListFocusDown (VS_UINT32 ListId) = 0;

  virtual VS_VOID gacScrollListFocusUp (VS_UINT32 ListId) = 0;

  virtual VS_VOID gacScrollListUp (VS_VOID) = 0;

  virtual VS_VOID gacScrollPageDown (VS_VOID) = 0;

  virtual VS_VOID gacScrollPageUp (VS_VOID) = 0;

  virtual VS_VOID gacSetApplicationMode (VS_UINT32 ModeId) = 0;

  virtual VS_VOID gacStartSceneTransitionAnimation (VS_UINT32 FirstView, VS_UINT32 SecondView, VS_UINT32 TransitionType, VS_INT8 Pos) = 0;

  virtual VS_VOID gacTBTRetriggerAcousticOutput (VS_VOID) = 0;

  virtual VS_VOID gacViewClearReq (VS_UINT32 ViewId) = 0;

  virtual VS_VOID gacViewCreateAllReq (VS_UINT32 ViewId) = 0;

  virtual VS_VOID gacViewCreateAndShowReq (VS_UINT32 ViewId) = 0;

  virtual VS_VOID gacViewCreateReq (VS_UINT32 ViewId) = 0;

  virtual VS_VOID gacViewDestroyReq (VS_UINT32 ViewId) = 0;

  virtual VS_VOID gacViewHideAndDestroyReq (VS_UINT32 ViewId) = 0;

  virtual VS_VOID gacViewHideReq (VS_UINT32 ViewId) = 0;

  virtual VS_VOID gacViewShowReq (VS_UINT32 ViewId) = 0;

  virtual VS_VOID gacWaitAnimationStartReq (VS_VOID) = 0;

  virtual VS_VOID gacWaitAnimationStopReq (VS_VOID) = 0;

protected:
  AppHmi_MediaStateMachine () {}

private:


  typedef union
  {
    struct
    {
      VS_UINT8         VS_UINT8Var[0X00002];
    } DB17;
    struct
    {
      VS_UINT32        VS_UINT32Var[0X00002];
    } DB64;
    struct
    {
      VS_BOOL          VS_BOOLVar[0X00001];
    } DB71;
    struct
    {
      VS_INT8          VS_INT8Var[0X00002];
    } DB74;
    struct
    {
      VS_UINT32        VS_UINT32Var[0X00003];
      VS_UINT8         VS_UINT8Var[0X00001];
    } DB94;
    struct
    {
      VS_BOOL          VS_BOOLVar[0X00001];
    } DB96;
    struct
    {
      VS_UINT8         VS_UINT8Var[0X00002];
    } DB117;
    struct
    {
      VS_INT32         VS_INT32Var[0X00001];
    } DB123;
  } EventArgsData;

  EventArgsData EventArgsVar;


  typedef struct
  {
    VS_UINT8       AppHmi_MediaStateMachineStateMachineIndex[0X043];
    VS_UINT16      AppHmi_MediaStateMachineRuleData[0X00a5f];
    VS_UINT16      AppHmi_MediaStateMachineRuleIndex[0X00197];
    VS_UINT16      AppHmi_MediaStateMachineRuleTableIndex[0X0008a];
    VS_UCHAR       AppHmi_MediaStateMachineEventNames[0X0c2e];
    VS_UINT16      AppHmi_MediaStateMachineEventNamesIndex[0X00082];
    VS_UCHAR       AppHmi_MediaStateMachineStateNames[0X0201b];
    VS_UINT16      AppHmi_MediaStateMachineStateNamesIndex[0X00043];
    VS_UCHAR       AppHmi_MediaStateMachineActionNames[0X0693];
    VS_UINT16      AppHmi_MediaStateMachineActionNamesIndex[0X0003e];
  } VSDATA;


  enum DeductIterationEnum
  {
    kDIOriginalEvent = 2,
    kDITakeSignalOut = 7
  };


  enum SEMStateEnum
  {
    STATE_SEM_NOT_INITIALIZED = 0x00u,
    STATE_SEM_INITIALIZED     = 0x01u,
    STATE_SEM_PREPARE         = 0x02u,
    STATE_SEM_CONSULT         = 0x03u,
    STATE_SEM_OUTPUT          = 0x04u,
    STATE_SEM_OKAY            = 0x05u
  };


  typedef struct SEMDATA
  {
    VS_UINT8                                      Status;
    VS_UINT8                                      State;
    VS_UINT8                                      DIt;
    VS_UINT8                                      nAction;
    SEM_EVENT_TYPE                                EventNo;
    SEM_RULE_INDEX_TYPE                           _iRI;
    SEM_RULE_TABLE_INDEX_TYPE                     iFirstR;
    SEM_RULE_TABLE_INDEX_TYPE                     iLastR;
    VS_UINT8                                      Chg;
    SEM_STATE_TYPE                                CSV[VS_NOF_STATE_MACHINES];
    SEM_STATE_TYPE                                WSV[VS_NOF_STATE_MACHINES];
    SEM_EVENT_TYPE                                SQueue[5];
    SEM_SIGNAL_QUEUE_TYPE                         SPut;
    SEM_SIGNAL_QUEUE_TYPE                         SGet;
    SEM_SIGNAL_QUEUE_TYPE                         SUsed;
  } SEMDATA;

  SEM_EVENT_TYPE SEM_SignalQueueGet ();

  VS_UCHAR SEM_SignalQueuePut (SEM_EVENT_TYPE SignalNo);

  void DeductChangeState (VS_VOID);

  VS_BOOL VSGuard (SEM_GUARD_EXPRESSION_TYPE i);

  static VSDATA const VS;

  VS_UINT32 currentListId;

  VS_UINT32 getActiveSrc;

  VS_BOOL getIsRootFolder;

  VS_BOOL isTempContextActive;

  VS_INT screen_Type;

  VS_INT activeApplicationID;

  VS_INT Cs_SelfContextId;

  VS_INT IN_SwitchId;

  VS_BOOL IsSpiPhoneSourceActive;

  VS_INT TempContextStatus;

  SEMDATA SEM;
};


#endif
