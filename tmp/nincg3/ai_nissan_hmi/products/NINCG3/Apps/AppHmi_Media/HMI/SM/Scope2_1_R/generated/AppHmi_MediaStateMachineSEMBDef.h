#ifndef _visualSTATE_APPHMI_MEDIASTATEMACHINESEMBDEF_H
#define _visualSTATE_APPHMI_MEDIASTATEMACHINESEMBDEF_H

/*
 * Id:        AppHmi_MediaStateMachineSEMBDef.h
 *
 * Function:  SEM Defines Header File.
 *
 * Generated: Thu May 12 14:47:35 2016
 *
 * Coder 7, 3, 2, 2426
 * 
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include "AppHmi_MediaStateMachineSEMTypes.h"


/*
 * Number of Identifiers.
 */
#define VS_NOF_ACTION_EXPRESSIONS        0X00278u  /*   632 */
#define VS_NOF_ACTION_FUNCTIONS          0X05au  /*  90 */
#define VS_NOF_EVENT_GROUPS              0X001u  /*   1 */
#define VS_NOF_EVENTS                    0X0c7u  /* 199 */
#define VS_NOF_EXTERNAL_VARIABLES        0X000u  /*   0 */
#define VS_NOF_GUARD_EXPRESSIONS         0X091u  /* 145 */
#define VS_NOF_INSTANCES                 0X001u  /*   1 */
#define VS_NOF_INTERNAL_VARIABLES        0X00cu  /*  12 */
#define VS_NOF_SIGNALS                   0X00cu  /*  12 */
#define VS_NOF_STATE_MACHINES            0X02fu  /*  47 */
#define VS_NOF_STATES                    0X09cu  /* 156 */


/*
 * Functional expression type definitions
 */
typedef VS_BOOL (* VS_GUARDEXPR_TYPE) (VS_VOID);
typedef VS_VOID (* VS_ACTIONEXPR_TYPE) (VS_VOID);


#endif /* ifndef _visualSTATE_APPHMI_MEDIASTATEMACHINESEMBDEF_H */
