/**
 * receive messages subscribed with 'View' here, when it has not been consumed by any widget
 */

#include "gui_std_if.h"

#include "AppHmi_MediaStateMachine.h"
#include "CGIAppViewController_Media.h"

const char* CGIAppViewController_MEDIA__CD_CDMP3_MAIN::_name = "Media#Scenes#MEDIA__CD_CDMP3_MAIN";
const char* CGIAppViewController_MEDIA__AUX__USB_MAIN::_name = "Media#Scenes#MEDIA__AUX__USB_MAIN";
const char* CGIAppViewController_MEDIA__AUX__IPOD_MAIN::_name = "Media#Scenes#MEDIA__AUX__IPOD_MAIN";
const char* CGIAppViewController_MEDIA_AUX__BROWSER::_name = "Media#Scenes#MEDIA_AUX__BROWSER";
const char* CGIAppViewController_MEDIA_AUX__METADATA_BROWSE::_name = "Media#Scenes#MEDIA_AUX__METADATA_BROWSE";
const char* CGIAppViewController_MEDIA_AUX__LINEIN_MAIN::_name = "Media#Scenes#MEDIA_AUX__LINEIN_MAIN";
const char* CGIAppViewController_MEDIA_AUX__USB_FOLDER_BROWSE::_name = "Media#Scenes#MEDIA_AUX__USB_FOLDER_BROWSE";
const char* CGIAppViewController_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT::_name = "Media#Scenes#MEDIA_AUX__BTAUDIO_MAIN_DEFAULT";
const char* CGIAppViewController_MEDIA__AUX__BTAUDIO_MAIN::_name = "Media#Scenes#MEDIA__AUX__BTAUDIO_MAIN";
const char* CGIAppViewController_MEDIA__DTM_SYSTEM_DRIVES::_name = "Media#Scenes#MEDIA__DTM_SYSTEM_DRIVES";
const char* CGIAppViewController_MEDIA__LOADING_MAIN::_name = "Media#Scenes#MEDIA__LOADING_MAIN";
const char* CGIAppViewController_MEDIA__INFORMATION__SETTINGS::_name = "Media#Scenes#MEDIA__INFORMATION__SETTINGS";
const char* CGIAppViewController_MEDIA__AUX__USB_PHOTO_BROWSER::_name = "Media#Scenes#MEDIA__AUX__USB_PHOTO_BROWSER";

//Spi Scenes
const char* CGIAppViewController_INFO__CARPLAY_APP::_name = "Spi#Spi_Scene#INFO_CARPLAY_APP";
const char* CGIAppViewController_SETTINGS__CARPLAY::_name = "Spi#Spi_Scene#SETTINGS__CARPLAY";
const char* CGIAppViewController_CARPLAY_TUTORIAL::_name = "Spi#Spi_Scene#CARPLAY_TUTORIAL";


//PopUps
const char* CGIAppViewController_MEDIA_AUX__POPUP_USB_CHECK_SYSIND::_name = "MediaPopups#Popups#MEDIA_AUX__POPUP_USB_CHECK_SYSIND";
const char* CGIAppViewController_MEDIA_AUX__USB_POPUP_QUICKSEARCH::_name = "MediaPopups#Popups#MEDIA_AUX__USB_POPUP_QUICKSEARCH";
const char* CGIAppViewController_MEDIA_AUX__POPUP_ABCSEARCH_KEYBOARD::_name = "MediaPopups#Popups#MEDIA_AUX__POPUP_ABCSEARCH_KEYBOARD";
const char* CGIAppViewController_MEDIA__POPUP_USB_NO_AUDIO_FILE::_name = "MediaPopups#Popups#MEDIA__POPUP_USB_NO_AUDIO_FILE";
const char* CGIAppViewController_MEDIA__POPUP_AUX_USB_ERROR_SYSIND::_name = "MediaPopups#Popups#MEDIA__POPUP_AUX_USB_ERROR_SYSIND";
const char* CGIAppViewController_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND::_name = "MediaPopups#Popups#MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND";
const char* CGIAppViewController_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND::_name = "MediaPopups#Popups#MEDIA_POPUP_IPOD_COM_ERROR_SYSIND";
const char* CGIAppViewController_MEDIA_POPUP_IPOD_ERROR_SYSIND::_name = "MediaPopups#Popups#MEDIA_POPUP_IPOD_ERROR_SYSIND";
const char* CGIAppViewController_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND::_name = "MediaPopups#Popups#MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND";
const char* CGIAppViewController_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND::_name = "MediaPopups#Popups#MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND";
const char* CGIAppViewController_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND::_name = "MediaPopups#Popups#MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND";
const char* CGIAppViewController_MEDIA_AUX__POPUP_LOADING_FILE::_name = "MediaPopups#Popups#MEDIA_AUX__POPUP_LOADING_FILE";
const char* CGIAppViewController_GLOBAL_WAIT_SCENE::_name = "Global#Scenes#WAIT_SCENE";
const char* CGIAppViewController_MEDIA__POPUP_SOURCE_INITIALISING::_name = "MediaPopups#Popups#MEDIA__POPUP_SOURCE_INITIALISING";
const char* CGIAppViewController_MEDIA__NOSOURCE_INTERACTIVECENTERPOPUP::_name = "MediaPopups#Popups#MEDIA__NOSOURCE_INTERACTIVECENTERPOPUP";
const char* CGIAppViewController_MEDIA__FILEERROR_INTERACTIVECENTERPOPUP::_name = "MediaPopups#Popups#MEDIA__FILEERROR_INTERACTIVECENTERPOPUP";
const char* CGIAppViewController_MEDIA__SETPROFILEFAILED_INTERACTIVECENTERPOPUP::_name = "MediaPopups#Popups#MEDIA__SETPROFILEFAILED_INTERACTIVECENTERPOPUP";

//impose
const char* CGIAppViewController_MEDIA__IMPOSE_AUDIO_OPERATION::_name = "MediaPopups#Popups#MEDIA__IMPOSE_AUDIO_OPERATION";
const char* CGIAppViewController_MEDIA__IMPOSE_CD_ABNORMAL_STATUS::_name = "MediaPopups#Popups#MEDIA__IMPOSE_CD_ABNORMAL_STATUS";
const char* CGIAppViewController_MEDIA__IMPOSE_CD_READ_ERROR::_name = "MediaPopups#Popups#MEDIA__IMPOSE_CD_READ_ERROR";
const char* CGIAppViewController_MEDIA__IMPOSE_CD_READING::_name = "MediaPopups#Popups#MEDIA__IMPOSE_CD_READING";
const char* CGIAppViewController_MEDIA__IMPOSE_CD_EJECTING::_name = "MediaPopups#Popups#MEDIA__IMPOSE_CD_EJECTING";
const char* CGIAppViewController_MEDIA__IMPOSE_CD_LOADING::_name = "MediaPopups#Popups#MEDIA__IMPOSE_CD_LOADING";
const char* CGIAppViewController_MEDIA__IMPOSE_CD_OVERHEATED::_name = "MediaPopups#Popups#MEDIA__IMPOSE_CD_OVERHEATED";
const char* CGIAppViewController_MEDIA__IMPOSE_CD_DISC_ERROR::_name = "MediaPopups#Popups#MEDIA__IMPOSE_CD_DISC_ERROR";
const char* CGIAppViewController_MEDIA__NOVIDEOFILE_TOASTPOPUP::_name = "MediaPopups#Popups#MEDIA__NOVIDEOFILE_TOASTPOPUP";
const char* CGIAppViewController_MEDIA__NOPHOTOFILE_TOASTPOPUP::_name = "MediaPopups#Popups#MEDIA__NOPHOTOFILE_TOASTPOPUP";

//SpiPopups
const char* CGIAppViewController_INFO_POPUP_CARPLAY_ADVISORY_SYSIND::_name   = "SpiPopups#Global_Popups#INFO_POPUP_CARPLAY_ADVISORY_SYSIND";
const char* CGIAppViewController_INFO_POPUP_CARPLAY_DISCLAIMER_SYSIND::_name = "SpiPopups#Global_Popups#INFO_POPUP_CARPLAY_DISCLAIMER_SYSIND";
const char* CGIAppViewController_INFO_POPUP_CARPLAY_REMEMBER_DISCLAIMER_SYSIND::_name = "SpiPopups#Global_Popups#INFO_POPUP_CARPLAY_REMEMBER_DISCLAIMER_SYSIND";
const char* CGIAppViewController_INFO__POPUP_CARPLAY_START_FAIL::_name       = "SpiPopups#Global_Popups#INFO__POPUP_CARPLAY_START_FAIL";
const char* CGIAppViewController_INFO__POPUP_CARPLAY_STARTING_SYSIND::_name  = "SpiPopups#Global_Popups#INFO__POPUP_CARPLAY_STARTING_SYSIND";
const char* CGIAppViewController_INFO__POPUP_CARPLAY_STARTED_SYSIND::_name 	 =  "SpiPopups#Global_Popups#INFO__POPUP_CARPLAY_STARTED_SYSIND";
const char* CGIAppViewController_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND::_name = "SpiPopups#Global_Popups#INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND";
const char* CGIAppViewController_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND::_name = "SpiPopups#Global_Popups#INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND";
const char* CGIAppViewController_INFO__POPUP_ANDROID_START_FAIL::_name = "SpiPopups#Global_Popups#INFO__POPUP_ANDROID_START_FAIL";
const char* CGIAppViewController_INFO__POPUP_ANDROID_STARTING_SYSIND::_name = "SpiPopups#Global_Popups#INFO__POPUP_ANDROID_STARTING_SYSIND";
const char* CGIAppViewController_INFO__POPUP_ANDROID_STARTED_SYSIND::_name = "SpiPopups#Global_Popups#INFO__POPUP_ANDROID_STARTED_SYSIND";
const char* CGIAppViewController_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND::_name = "SpiPopups#Global_Popups#INFO__POPUP_ANDROID_DISCLAIMER_SYSIND";
const char* CGIAppViewController_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND::_name = "SpiPopups#Global_Popups#INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND";
const char* CGIAppViewController_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND::_name = "SpiPopups#Global_Popups#INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND";
const char* CGIAppViewController_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND::_name = "SpiPopups#Global_Popups#INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND";
const char* CGIAppViewController_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND::_name = "SpiPopups#Global_Popups#INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND";
const char* CGIAppViewController_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND::_name = "SpiPopups#Global_Popups#INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND";
const char* CGIAppViewController_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND::_name = "SpiPopups#Global_Popups#INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND";
const char* CGIAppViewController_INFO__POPUP_CARPLAY_ADVISORY_SYSIND::_name = "SpiPopups#Global_Popups#INFO__POPUP_CARPLAY_ADVISORY_SYSIND";
const char* CGIAppViewController_SPI_IMPOSE_CARPLAY_CALL_INFO::_name = "SpiPopups#Global_Popups#SPI_IMPOSE_CARPLAY_CALL_INFO";
//Gadgets
const char* CGIAppViewController_MEDIA_CURRENT_SONG_SMALL_GADGET::_name = "MediaGadgets#Gadgets#MEDIA_CURRENT_SONG_SMALL_GADGET";
const char* CGIAppViewController_MEDIA_CURRENT_SONG_LARGE_GADGET::_name = "MediaGadgets#Gadgets#MEDIA_CURRENT_SONG_LARGE_GADGET";
