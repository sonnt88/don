/**
 * If in this class no special handling is defined, the implementation of the 
 * base class IAppViewSettings will be used by ScreenBrokerClient (as part of 
 * libapphmi_fw_appbase-asf_a.a).
 * If this application needs special handling, it can be added to this class.
 * 
 * The base class IAppViewSettings provides:
 * - the popup presentation arguments 
 * - the different request IDs to show/hide a surface
 * - the different request IDs to show/hide a popup dependent on popup 
 *   presentation arguments)
 * which are needed by ScreenBorkerClient to communicate with ScreenBroker.
*/
#include "gui_std_if.h"

#include "AppViewSettings.h"

