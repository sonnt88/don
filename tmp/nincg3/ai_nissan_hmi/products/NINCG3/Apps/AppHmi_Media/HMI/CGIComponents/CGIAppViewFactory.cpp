#include "gui_std_if.h"

#include "CGIAppViewFactory.h"
#include "AppHmi_MediaMessages.h"
// the views
#include "CGIAppViewController_Media.h"
/*-------------------------------------*/
/**
 * RenderTargetInvalidatingViewScene is derived from ViewSceneBase.
 * This class is used to invalidate the scene
 *.
 *
 */
namespace Courier {
template<class ViewSceneBase> class RenderTargetInvalidatingViewScene : public ViewSceneBase
{
   public:
      RenderTargetInvalidatingViewScene(Courier::Bool managed = false) : ViewSceneBase(managed) { }
      virtual ~RenderTargetInvalidatingViewScene() { }

      virtual void Invalidate()
      {
         for (Courier::Int32 idx = 0; idx < ViewSceneBase::GetCameraPtrVector().Size(); idx++)
         {
            ViewSceneBase::GetViewHandler()->Invalidate(ViewSceneBase::GetCameraPtrVector()[idx]->GetRenderTarget());
         }
      }
};


}

SCENE_MAPPING_BEGIN(aScenes)
SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__CD_CDMP3_MAIN),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__AUX__USB_MAIN),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__AUX__IPOD_MAIN),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA_AUX__BROWSER),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA_AUX__METADATA_BROWSE),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA_AUX__LINEIN_MAIN),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA_AUX__USB_FOLDER_BROWSE),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__AUX__BTAUDIO_MAIN),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__CARPLAY_APP),                            //Spi scene
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SETTINGS__CARPLAY),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_CARPLAY_TUTORIAL),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__DTM_SYSTEM_DRIVES),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__LOADING_MAIN),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__INFORMATION__SETTINGS),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__AUX__USB_PHOTO_BROWSER),

                       //PopUps
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA_AUX__POPUP_USB_CHECK_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA_AUX__USB_POPUP_QUICKSEARCH),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA_AUX__POPUP_ABCSEARCH_KEYBOARD),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__POPUP_USB_NO_AUDIO_FILE),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__POPUP_AUX_USB_ERROR_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA_POPUP_IPOD_ERROR_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA_AUX__POPUP_LOADING_FILE),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__POPUP_SOURCE_INITIALISING),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__NOSOURCE_INTERACTIVECENTERPOPUP),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__FILEERROR_INTERACTIVECENTERPOPUP),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__SETPROFILEFAILED_INTERACTIVECENTERPOPUP),

                       //impose
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__IMPOSE_AUDIO_OPERATION),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__IMPOSE_CD_ABNORMAL_STATUS),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__IMPOSE_CD_READ_ERROR),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__IMPOSE_CD_READING),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__IMPOSE_CD_EJECTING),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__IMPOSE_CD_LOADING),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__IMPOSE_CD_OVERHEATED),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__IMPOSE_CD_DISC_ERROR),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__NOVIDEOFILE_TOASTPOPUP),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA__NOPHOTOFILE_TOASTPOPUP),

                       //Global popup
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_GLOBAL_WAIT_SCENE),

                       //SPI_POPUP_SCOPE1
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO_POPUP_CARPLAY_DISCLAIMER_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO_POPUP_CARPLAY_REMEMBER_DISCLAIMER_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO_POPUP_CARPLAY_ADVISORY_SYSIND),

                       //SPI_POPUP_SCOPE2
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__POPUP_CARPLAY_START_FAIL),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__POPUP_CARPLAY_STARTING_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__POPUP_CARPLAY_STARTED_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__POPUP_ANDROID_START_FAIL),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__POPUP_ANDROID_STARTING_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__POPUP_ANDROID_STARTED_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_INFO__POPUP_CARPLAY_ADVISORY_SYSIND),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SPI_IMPOSE_CARPLAY_CALL_INFO),
                       //Gadgets
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA_CURRENT_SONG_SMALL_GADGET),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MEDIA_CURRENT_SONG_LARGE_GADGET),

                       SCENE_MAPPING_END()

                       CGIAppViewFactory::CGIAppViewFactory(): CGIAppViewFactoryBase(TABSIZE(aScenes), aScenes)
{
}


/**
 *  This method is used to create a new scene when it is called.
 * @param [in]= sViewName, type = Char*,<View name is passed as an input parameter>
 * @param [out] = View*, type = Courier::View, <the created view returned to calling method>
 *.@param [in, out] = none
 * @return = Courier::View*
 * @throws <NA>
 *
 * pre
 * post
 * bug = none
 */
Courier::View* CGIAppViewFactory::Create(const Courier::Char* sViewName)
{
   return SceneMapping::createView(sViewName, aScenes, TABSIZE(aScenes), GetViewHandler());
}


/**
 *  This method is used to destroy the scene when it is about to leave the state.
 * @param [in]= pView, type = Char*,<View name is passed as an input parameter>
 * @param [out] = void
 *.@param [in, out] = none
 * @return = none
 * @throws <NA>
 *
 * pre
 * post
 * bug = none
 */
void CGIAppViewFactory::Destroy(Courier::View* pView)
{
   SceneMapping::destroyView(aScenes, TABSIZE(aScenes), pView);
}


/**
 *  This method is used to create the view controller for the scene.
 * @param [in]= sViewName, type = Char*,<View name is passed as an input parameter>
 * @param [out] = Courier::ViewController
 *.@param [in, out] = none
 * @return = Courier::ViewController*
 * @throws <NA>
 *
 * pre
 * post
 * bug = none
 */
Courier::ViewController* CGIAppViewControllerFactory::Create(const Courier::Char* sViewName)
{
   return SceneMapping::createViewController(sViewName, aScenes, TABSIZE(aScenes));
}


/**
 *  This method is used to destroy the view controller .
 * @param [in]= viewController, type = Courier::ViewController*,<Pointer to view controller>
 * @param [out] = void
 *.@param [in, out] = none
 * @return = none
 * @throws <NA>
 *
 * pre
 * post
 * bug = none
 */
void CGIAppViewControllerFactory::Destroy(Courier::ViewController* viewController)
{
   COURIER_UNUSED(viewController);
}


/****************************EOF***********************/
