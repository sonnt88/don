/*
 * Id:        AppHmi_MediaStateMachineData.cpp
 *
 * Function:  VS System Data Source File.
 *
 * Generated: Thu May 12 14:47:35 2016
 *
 * Coder 7, 3, 2, 2426
 * 
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include "AppHmi_MediaStateMachineSEMLibB.h"


#include "AppHmi_MediaStateMachineData.h"


#include <stdarg.h>


/*
 * VS System Internal Variable Initializing Function.
 */
void AppHmi_MediaStateMachine::SEM_InitInternalVariables (void)
{
  IsMetadataPlayingList = 0;
  avrcpType = 0;
  currentListId = 4032ul;
  currentSwitchID = 0;
  getActiveSrc = 0ul;
  getIsRootFolder = 0;
  isTempContextActive = 0;
  screen_Type = 1;
  IN_SwitchId = 0;
  In_SelfswitchId = 0;
  IsSpiPhoneSourceActive = 0;
  IsTempContext = 0;
}


/*
 * SEM Deduct Function.
 */
unsigned char AppHmi_MediaStateMachine::SEM_Deduct (SEM_EVENT_TYPE EventNo, ...)
{
  va_list ap;

  va_start(ap, EventNo);
  if (SEM.State == 0x00u /* STATE_SEM_NOT_INITIALIZED */)
  {
    return SES_NOT_INITIALIZED;
  }
  if (VS_NOF_EVENTS <= EventNo)
  {
    return (SES_RANGE_ERR);
  }
  switch (EventNo)
  {
  case 14:
    EventArgsVar.DB135.VS_BOOLVar[0] = (VS_BOOL) va_arg(ap, VS_INT);
    break;

  case 16:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 17:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 20:
    EventArgsVar.DB184.VS_UINT8Var[0] = (VS_UINT8) va_arg(ap, VS_INT);
    EventArgsVar.DB184.VS_UINT8Var[1] = (VS_UINT8) va_arg(ap, VS_INT);
    break;

  case 27:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    EventArgsVar.DB172.VS_INT8Var[1] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 32:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 33:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 34:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 35:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 36:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 37:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 38:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 39:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 40:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 41:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 42:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 43:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 44:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 45:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 46:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 47:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 48:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 49:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 50:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 51:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 52:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 53:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 54:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 55:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 56:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 57:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 58:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 59:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 60:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 61:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 62:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 63:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 64:
    EventArgsVar.DB184.VS_UINT8Var[0] = (VS_UINT8) va_arg(ap, VS_INT);
    break;

  case 65:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[2] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 66:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 67:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 68:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 69:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 70:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 71:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 72:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 73:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 74:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 75:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB80.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 76:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 77:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 78:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 79:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 80:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB80.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 81:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 82:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 83:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 84:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 85:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 86:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 87:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 88:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 89:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 90:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 91:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 92:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 93:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 94:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 95:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 96:
    EventArgsVar.DB96.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 97:
    EventArgsVar.DB96.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 98:
    EventArgsVar.DB96.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 99:
    EventArgsVar.DB96.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 100:
    EventArgsVar.DB96.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 101:
    EventArgsVar.DB96.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 102:
    EventArgsVar.DB96.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 103:
    EventArgsVar.DB96.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 104:
    EventArgsVar.DB96.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 105:
    EventArgsVar.DB96.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 106:
    EventArgsVar.DB96.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 107:
    EventArgsVar.DB96.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 108:
    EventArgsVar.DB96.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 109:
    EventArgsVar.DB96.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 110:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 111:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 112:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 119:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 120:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 121:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 122:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 123:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 124:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 125:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 126:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 127:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 128:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 129:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 130:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 131:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 132:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 133:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 134:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 135:
    EventArgsVar.DB135.VS_BOOLVar[0] = (VS_BOOL) va_arg(ap, VS_INT);
    break;

  case 136:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 138:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 140:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    EventArgsVar.DB172.VS_INT8Var[1] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 144:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 146:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 147:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 148:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 149:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 158:
    EventArgsVar.DB158.VS_UINT8Var[0] = (VS_UINT8) va_arg(ap, VS_INT);
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[2] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 160:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB80.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 161:
    EventArgsVar.DB135.VS_BOOLVar[0] = (VS_BOOL) va_arg(ap, VS_INT);
    break;

  case 162:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 163:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 164:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 165:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 166:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 167:
    EventArgsVar.DB158.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB158.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 168:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 169:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 170:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 171:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 172:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    EventArgsVar.DB172.VS_INT8Var[1] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 176:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 177:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 178:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 179:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 180:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 181:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 182:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 184:
    EventArgsVar.DB184.VS_UINT8Var[0] = (VS_UINT8) va_arg(ap, VS_INT);
    EventArgsVar.DB184.VS_UINT8Var[1] = (VS_UINT8) va_arg(ap, VS_INT);
    break;

  case 185:
    EventArgsVar.DB172.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 190:
    EventArgsVar.DB190.VS_INT32Var[0] = (VS_INT32) va_arg(ap, VS_INT32_VAARG);
    break;

  case 191:
    EventArgsVar.DB190.VS_INT32Var[0] = (VS_INT32) va_arg(ap, VS_INT32_VAARG);
    break;

  case 196:
    EventArgsVar.DB80.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  default:
    break;
  }
  SEM.EventNo = EventNo;
  SEM.DIt = 2;
  SEM.State = 0x02u; /* STATE_SEM_PREPARE */
  SEM.OriginalEventNo = EventNo;

  va_end(ap);
  return (SES_OKAY);
}


/*
 * Guard Expression Functions.
 */
VS_BOOL AppHmi_MediaStateMachine::VSGuard (SEM_GUARD_EXPRESSION_TYPE i)
{
  switch (i)
  {
  case 0:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_DOWN && dgSpiMediaActiveSource());
  case 1:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && dgSpiMediaActiveSource());
  case 2:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && dgSpiMediaActiveSource());
  case 3:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP && dgSpiMediaActiveSource());
  case 4:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == ANDROID_AUTO_ASK_ON_CONNECT_STATUS_ON);
  case 5:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == ANDROID_AUTO_ASK_ON_CONNECT_STATUS_OFF && EventArgsVar.DB172.VS_INT8Var[1] == Enum_AUTO_LAUNCH_CONNECT_ON);
  case 6:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == CARPLAY_ASK_ON_CONNECT_STATUS_ON);
  case 7:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == CARPLAY_ASK_ON_CONNECT_STATUS_OFF && EventArgsVar.DB172.VS_INT8Var[1] == Enum_AUTO_LAUNCH_CONNECT_ON);
  case 8:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_DOWN && (IsSpiPhoneSourceActive == 1 || dgSpiAlertActiveSource()));
  case 9:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && (IsSpiPhoneSourceActive == 1 || dgSpiAlertActiveSource()));
  case 10:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_DOWN && dgSpiSessionActive());
  case 11:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && dgSpiSessionActive());
  case 12:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && dgSpiSessionActive());
  case 13:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP && dgSpiSessionActive());
  case 14:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_DOWN && dgSpeechRecognitionActiveSource());
  case 15:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && dgSpeechRecognitionActiveSource());
  case 16:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && dgBlockClockScreenOnHKILLUMLongPress() == 1);
  case 17:
    return (VS_BOOL)(EventArgsVar.DB135.VS_BOOLVar[0] == true);
  case 18:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && screen_Type == Enum_OUTSIDE_BROWSE_SCREENS);
  case 19:
    return (VS_BOOL)(EventArgsVar.DB184.VS_UINT8Var[0] == Enum_SOURCE_AUX);
  case 20:
    return (VS_BOOL)(EventArgsVar.DB184.VS_UINT8Var[0] == Enum_SOURCE_CDMP3);
  case 21:
    return (VS_BOOL)(EventArgsVar.DB184.VS_UINT8Var[0] == Enum_SOURCE_CD);
  case 22:
    return (VS_BOOL)(EventArgsVar.DB184.VS_UINT8Var[0] == Enum_SOURCE_USB);
  case 23:
    return (VS_BOOL)(EventArgsVar.DB184.VS_UINT8Var[0] == Enum_SOURCE_IPOD);
  case 24:
    return (VS_BOOL)(EventArgsVar.DB184.VS_UINT8Var[0] == Enum_SOURCE_BT && EventArgsVar.DB184.VS_UINT8Var[1] == 1);
  case 25:
    return (VS_BOOL)(EventArgsVar.DB184.VS_UINT8Var[0] == Enum_SOURCE_BT && EventArgsVar.DB184.VS_UINT8Var[1] == 0);
  case 26:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
  case 27:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
  case 28:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
  case 29:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
  case 30:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
  case 31:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
  case 32:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION && (screen_Type == Enum_INSIDE_BROWSE_SCREENS || !dgActiveApplicationState()));
  case 33:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
  case 34:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
  case 35:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
  case 36:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
  case 37:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__IMPOSE_CD_OVERHEATED);
  case 38:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__IMPOSE_CD_DISC_ERROR);
  case 39:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
  case 40:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
  case 41:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
  case 42:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getActiveSrc != Enum_SOURCE_BT_NOT_CONNECTED && getActiveSrc != Enum_SOURCE_AUX && dgIsMediaActive() && !dgIsMuteActive());
  case 43:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && getActiveSrc != Enum_SOURCE_BT_NOT_CONNECTED && getActiveSrc != Enum_SOURCE_AUX && dgIsMediaActive() && !dgIsMuteActive());
  case 44:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP && getActiveSrc != Enum_SOURCE_BT_NOT_CONNECTED && getActiveSrc != Enum_SOURCE_AUX && dgIsMediaActive() && !dgIsMuteActive());
  case 45:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP);
  case 46:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__FILEERROR_INTERACTIVECENTERPOPUP);
  case 47:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__NOPHOTOFILE_TOASTPOPUP);
  case 48:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__NOSOURCE_INTERACTIVECENTERPOPUP);
  case 49:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__NOVIDEOFILE_TOASTPOPUP);
  case 50:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__SETPROFILEFAILED_INTERACTIVECENTERPOPUP);
  case 51:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__SETPROFILEFAILED_INTERACTIVECENTERPOPUP);
  case 52:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__NOPHOTOFILE_TOASTPOPUP);
  case 53:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__FILEERROR_INTERACTIVECENTERPOPUP);
  case 54:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__NOVIDEOFILE_TOASTPOPUP);
  case 55:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__NOSOURCE_INTERACTIVECENTERPOPUP);
  case 56:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_DISC_ERROR);
  case 57:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_OVERHEATED);
  case 58:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_ABCSEARCH_KEYBOARD);
  case 59:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] < 0);
  case 60:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] > 0);
  case 61:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
  case 62:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
  case 63:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
  case 64:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
  case 65:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
  case 66:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
  case 67:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
  case 68:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
  case 69:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
  case 70:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
  case 71:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
  case 72:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
  case 73:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
  case 74:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
  case 75:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
  case 76:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
  case 77:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
  case 78:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
  case 79:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
  case 80:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
  case 81:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_DOWN);
  case 82:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
  case 83:
    return (VS_BOOL)(IsTempContext != 1);
  case 84:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND);
  case 85:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND);
  case 86:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND);
  case 87:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND);
  case 88:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND);
  case 89:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND);
  case 90:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_START_FAIL);
  case 91:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_START_FAIL);
  case 92:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
  case 93:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTING_SYSIND);
  case 94:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTED_SYSIND);
  case 95:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTED_SYSIND);
  case 96:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND);
  case 97:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND);
  case 98:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_ADVISORY_SYSIND);
  case 99:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND);
  case 100:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB158.VS_UINT32Var[1] == Global_Scenes_WAIT_SCENE);
  case 101:
    return (VS_BOOL)(getActiveSrc == Enum_SOURCE_CDMP3);
  case 102:
    return (VS_BOOL)(IsMetadataPlayingList == false);
  case 103:
    return (VS_BOOL)(IsMetadataPlayingList == true);
  case 104:
    return (VS_BOOL)(currentListId != LIST_ID_BROWSER_MAIN);
  case 105:
    return (VS_BOOL)(currentListId == LIST_ID_BROWSER_MAIN);
  case 106:
    return (VS_BOOL)(avrcpType == Enum_HIGHER);
  case 107:
    return (VS_BOOL)(avrcpType == Enum_LOWER);
  case 108:
    return (VS_BOOL)(EventArgsVar.DB80.VS_UINT32Var[0] == Media_Scenes_MEDIA__INFORMATION__SETTINGS);
  case 109:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && screen_Type == Enum_INSIDE_BROWSE_SCREENS);
  case 110:
    return (VS_BOOL)(getActiveSrc == Enum_SOURCE_IPOD);
  case 111:
    return (VS_BOOL)(getActiveSrc == Enum_SOURCE_BT);
  case 112:
    return (VS_BOOL)(getActiveSrc == Enum_SOURCE_USB);
  case 113:
    return (VS_BOOL)(isTempContextActive == false);
  case 114:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getIsRootFolder == true && getActiveSrc == Enum_SOURCE_USB);
  case 115:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && getActiveSrc != Enum_SOURCE_BT);
  case 116:
    return (VS_BOOL)(!EventArgsVar.DB135.VS_BOOLVar[0] && getActiveSrc == Enum_SOURCE_BT || getActiveSrc == Enum_SOURCE_CDMP3 || getActiveSrc == Enum_SOURCE_USB);
  case 117:
    return (VS_BOOL)(getIsRootFolder == true && getActiveSrc == Enum_SOURCE_USB);
  case 118:
    return (VS_BOOL)(getIsRootFolder == true && getActiveSrc == Enum_SOURCE_BT);
  case 119:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getIsRootFolder == true && getActiveSrc == Enum_SOURCE_BT);
  case 120:
    return (VS_BOOL)(EventArgsVar.DB135.VS_BOOLVar[0] && getActiveSrc == Enum_SOURCE_BT);
  case 121:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getIsRootFolder == false);
  case 122:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && getActiveSrc == Enum_SOURCE_BT);
  case 123:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getIsRootFolder == true && getActiveSrc == Enum_SOURCE_CDMP3);
  case 124:
    return (VS_BOOL)(getIsRootFolder == false);
  case 125:
    return (VS_BOOL)(getActiveSrc == Enum_SOURCE_CDMP3 && getIsRootFolder == true);
  case 126:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == LIST_ROW_VALUE);
  case 127:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_LIST_ITEM_USB_BROWSE_FOLDER_BROWSE && getActiveSrc == Enum_SOURCE_USB);
  case 128:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_LIST_ITEM_USB_BROWSE_PLAYLIST && getActiveSrc == Enum_SOURCE_USB || EventArgsVar.DB158.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_PLAYLIST && getActiveSrc == Enum_SOURCE_IPOD);
  case 129:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_LIST_ITEM_USB_BROWSE_ARTIST && getActiveSrc == Enum_SOURCE_USB || EventArgsVar.DB158.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_ARTIST && getActiveSrc == Enum_SOURCE_IPOD);
  case 130:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_LIST_ITEM_USB_BROWSE_ALBUM && getActiveSrc == Enum_SOURCE_USB || EventArgsVar.DB158.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_ALBUM && getActiveSrc == Enum_SOURCE_IPOD);
  case 131:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_LIST_ITEM_USB_BROWSE_SONG && getActiveSrc == Enum_SOURCE_USB || EventArgsVar.DB158.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_SONG && getActiveSrc == Enum_SOURCE_IPOD);
  case 132:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_LIST_ITEM_USB_BROWSE_GENRE && getActiveSrc == Enum_SOURCE_USB || EventArgsVar.DB158.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_GENRE && getActiveSrc == Enum_SOURCE_IPOD);
  case 133:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_LIST_ITEM_USB_BROWSE_COMPOSER && getActiveSrc == Enum_SOURCE_USB || EventArgsVar.DB158.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_COMPOSER && getActiveSrc == Enum_SOURCE_IPOD);
  case 134:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_AUDIOBOOK && getActiveSrc == Enum_SOURCE_IPOD);
  case 135:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_PODCAST && getActiveSrc == Enum_SOURCE_IPOD);
  case 136:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getActiveSrc == Enum_SOURCE_USB);
  case 137:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getActiveSrc == Enum_SOURCE_IPOD);
  case 138:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && getActiveSrc == Enum_SOURCE_USB);
  case 139:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[2] == LIST_ID_BROWSER_SONG || EventArgsVar.DB158.VS_UINT32Var[2] == LIST_ID_BROWSER_ALBUM_SONG || EventArgsVar.DB158.VS_UINT32Var[2] == LIST_ID_BROWSER_ARTIST_ALBUM_SONG || EventArgsVar.DB158.VS_UINT32Var[2] == LIST_ID_BROWSER_BOOKTITLE_CHAPTER || EventArgsVar.DB158.VS_UINT32Var[2] == LIST_ID_BROWSER_COMPOSER_ALBUM_SONG || EventArgsVar.DB158.VS_UINT32Var[2] == LIST_ID_BROWSER_GENRE_ARTIST_ALBUM_SONG || EventArgsVar.DB158.VS_UINT32Var[2] == LIST_ID_BROWSER_PLAYLIST_SONG || EventArgsVar.DB158.VS_UINT32Var[2] == LIST_ID_BROWSER_PODCAST_EPISODE || EventArgsVar.DB158.VS_UINT32Var[2] == LIST_ID_BROWSER_GENRE_ARTIST_SONG || EventArgsVar.DB158.VS_UINT32Var[2] == LIST_ID_BROWSER_GENRE_ALBUM_SONG || EventArgsVar.DB158.VS_UINT32Var[2] == LIST_ID_BROWSER_GENRE_SONG || EventArgsVar.DB158.VS_UINT32Var[2] == LIST_ID_BROWSER_ARTIST_SONG || EventArgsVar.DB158.VS_UINT32Var[2] == LIST_ID_BROWSER_COMPOSER_SONG);
  case 140:
    return (VS_BOOL)(EventArgsVar.DB158.VS_UINT32Var[2] != LIST_ID_BROWSER_SONG && EventArgsVar.DB158.VS_UINT32Var[2] != LIST_ID_BROWSER_ALBUM_SONG && EventArgsVar.DB158.VS_UINT32Var[2] != LIST_ID_BROWSER_ARTIST_ALBUM_SONG && EventArgsVar.DB158.VS_UINT32Var[2] != LIST_ID_BROWSER_BOOKTITLE_CHAPTER && EventArgsVar.DB158.VS_UINT32Var[2] != LIST_ID_BROWSER_COMPOSER_ALBUM_SONG && EventArgsVar.DB158.VS_UINT32Var[2] != LIST_ID_BROWSER_GENRE_ARTIST_ALBUM_SONG && EventArgsVar.DB158.VS_UINT32Var[2] != LIST_ID_BROWSER_PLAYLIST_SONG && EventArgsVar.DB158.VS_UINT32Var[2] != LIST_ID_BROWSER_PODCAST_EPISODE && EventArgsVar.DB158.VS_UINT32Var[2] != LIST_ID_BROWSER_GENRE_ARTIST_SONG && EventArgsVar.DB158.VS_UINT32Var[2] != LIST_ID_BROWSER_GENRE_ALBUM_SONG && EventArgsVar.DB158.VS_UINT32Var[2] != LIST_ID_BROWSER_GENRE_SONG && EventArgsVar.DB158.VS_UINT32Var[2] != LIST_ID_BROWSER_ARTIST_SONG && EventArgsVar.DB158.VS_UINT32Var[2] != LIST_ID_BROWSER_COMPOSER_SONG);
  case 141:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && getActiveSrc == Enum_SOURCE_IPOD);
  case 142:
    return (VS_BOOL)(EventArgsVar.DB172.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getActiveSrc == Enum_SOURCE_BT);
  case 143:
    return (VS_BOOL)(EventArgsVar.DB80.VS_UINT32Var[0] == LIST_ID_COVER_PLAYER_IMAGE);
  }
  return (VS_BOOL)(EventArgsVar.DB96.VS_UINT32Var[0] == Media_Scenes_MEDIA_VIDEO_PLAYER_MAIN);
}


/*
 * Action Expressions Wrapper Function.
 */
VS_VOID AppHmi_MediaStateMachine::VSAction (SEM_ACTION_EXPRESSION_TYPE i)
{
  switch (i)
  {
  case 0:
    Notify_Init_Finished();
    break;
  case 1:
    acPerform_ABCSearchBtnPressUpdMsg();
    break;
  case 2:
    acPerform_AndroidDontRemindMeReqMsg();
    break;
  case 5:
    acPerform_BTBrowseTimerStopReqMsg();
    break;
  case 9:
    acPerform_CurrentPlayingListReqMsg();
    break;
  case 10:
    acPerform_DisclaimerButtonNo();
    break;
  case 11:
    acPerform_DontRemindMeReqMsg();
    break;
  case 12:
    acPerform_EncoderNegativeRotationUpdMsg();
    break;
  case 13:
    acPerform_EncoderPositiveRotationUpdMsg();
    break;
  case 14:
    acPerform_EncoderPressUpdMsg();
    break;
  case 15:
    acPerform_ExitBrowse();
    break;
  case 16:
    acPerform_FastForwardStartReqMsg();
    break;
  case 17:
    acPerform_FastForwardStopReqMsg();
    break;
  case 18:
    acPerform_FastRewindStartReqMsg();
    break;
  case 19:
    acPerform_FastRewindStopReqMsg();
    break;
  case 20:
    acPerform_FitScreenPhotoPlayerReqMsg();
    break;
  case 21:
    acPerform_FolderUpBtnPress();
    break;
  case 22:
    acPerform_FullScreenPhotoPlayerReqMsg();
    break;
  case 23:
    acPerform_HK_EJECT_MsgPost();
    break;
  case 24:
    acPerform_HMISubStateON();
    break;
  case 25:
    acPerform_InitialiseBTBrowseMsg();
    break;
  case 26:
    acPerform_InitialiseFolderBrowser();
    break;
  case 27:
    acPerform_InitialisePictureBrowserReqMsg();
    break;
  case 28:
    acPerform_InitialisePlayPhotoPlayerReqMsg();
    break;
  case 29:
    acPerform_InitializeMediaParametersUpdMsg();
    break;
  case 30:
    acPerform_LaunchAndroidAuto();
    break;
  case 31:
    acPerform_LaunchCarPlay();
    break;
  case 32:
    acPerform_MetaDataBrowserBackBtnPress();
    break;
  case 33:
    acPerform_OnOffAnimationEffectPhotoPlayerReqMsg();
    break;
  case 34:
    acPerform_OptionButtonClosePressedMsg();
    break;
  case 35:
    acPerform_OptionButtonPressedMsg();
    break;
  case 36:
    acPerform_PausePhotoPlayerReqMsg();
    break;
  case 37:
    acPerform_PauseReqMsg();
    break;
  case 38:
    acPerform_PlayAllSongsReqMsg();
    break;
  case 40:
    acPerform_PlayPauseReqMsg();
    break;
  case 41:
    acPerform_PlayReqMsg();
    break;
  case 42:
    acPerform_QuickSearchBtnPressUpdMsg();
    break;
  case 43:
    acPerform_RandomReqMsg();
    break;
  case 44:
    acPerform_RepeatReqMsg();
    break;
  case 45:
    acPerform_ResumePlayPhotoPlayerReqMsg();
    break;
  case 48:
    acPerform_SeekNextReqMsg();
    break;
  case 49:
    acPerform_SeekPrevReqMsg();
    break;
  case 50:
    acPerform_SendDipoPlayCommand();
    break;
  case 52:
    acPerform_SettingTimePhotoPlayerSlideShowReqMsg();
    break;
  case 53:
    acPerform_SourceToggleReqMsg();
    break;
  case 57:
    acPerform_TAReqMsg();
    break;
  case 58:
    acPerform_ToggleAlbumArtReqMsg();
    break;
  case 61:
    acPerform_UserInactivityTimerStopMsg();
    break;
  case 62:
    acPerform_onAndroidDisclaimerBtnNoReqMsg();
    break;
  case 63:
    acPerform_onHK_AUX_CDEventMsg();
    break;
  case 78:
    gacDisplayFooterLineReq();
    break;
  case 79:
    gacHideFooterLineReq();
    break;
  case 83:
    gacScrollListDown();
    break;
  case 84:
    gacScrollListUp();
    break;
  case 85:
    gacScrollPageDown();
    break;
  case 86:
    gacScrollPageUp();
    break;
  case 88:
    gacWaitAnimationStartReq();
    break;
  case 89:
    gacWaitAnimationStopReq();
    break;
  case 90:
    IsSpiPhoneSourceActive = EventArgsVar.DB172.VS_INT8Var[0];
    break;
  case 91:
    IsTempContext = EventArgsVar.DB135.VS_BOOLVar[0];
    break;
  case 92:
    screen_Type = Enum_OUTSIDE_BROWSE_SCREENS;
    break;
  case 93:
    currentSwitchID = EventArgsVar.DB80.VS_UINT32Var[0];
    break;
  case 94:
    getActiveSrc = Enum_SOURCE_AUX;
    break;
  case 95:
    getActiveSrc = Enum_SOURCE_CDMP3;
    break;
  case 96:
    getActiveSrc = Enum_SOURCE_CD;
    break;
  case 97:
    getActiveSrc = Enum_SOURCE_USB;
    break;
  case 98:
    getActiveSrc = Enum_SOURCE_IPOD;
    break;
  case 99:
    getActiveSrc = Enum_SOURCE_BT;
    break;
  case 100:
    getActiveSrc = Enum_SOURCE_BT_NOT_CONNECTED;
    break;
  case 101:
    currentListId = LIST_ID_BROWSER_MAIN;
    break;
  case 102:
    avrcpType = EventArgsVar.DB184.VS_UINT8Var[0];
    break;
  case 103:
    isTempContextActive = EventArgsVar.DB135.VS_BOOLVar[0];
    break;
  case 104:
    IN_SwitchId = 0;
    break;
  case 105:
    getIsRootFolder = EventArgsVar.DB135.VS_BOOLVar[0];
    break;
  case 106:
    screen_Type = Enum_INSIDE_BROWSE_SCREENS;
    break;
  case 107:
    getIsRootFolder = EventArgsVar.DB135.VS_BOOLVar[0];
    break;
  case 108:
    currentSwitchID = 0;
    break;
  case 109:
    getIsRootFolder = EventArgsVar.DB135.VS_BOOLVar[0];
    break;
  case 110:
    getIsRootFolder = EventArgsVar.DB135.VS_BOOLVar[0];
    break;
  case 111:
    IsMetadataPlayingList = true;
    break;
  case 112:
    currentListId = EventArgsVar.DB80.VS_UINT32Var[0];
    break;
  case 113:
    IsMetadataPlayingList = false;
    break;
  case 114:
    getIsRootFolder = EventArgsVar.DB135.VS_BOOLVar[0];
    break;
  case 115:
    currentListId = LIST_ID_BROWSER_PLAYLIST;
    break;
  case 116:
    currentListId = LIST_ID_BROWSER_ARTIST;
    break;
  case 117:
    currentListId = LIST_ID_BROWSER_ALBUM;
    break;
  case 118:
    currentListId = LIST_ID_BROWSER_SONG;
    break;
  case 119:
    currentListId = LIST_ID_BROWSER_GENRE;
    break;
  case 120:
    currentListId = LIST_ID_BROWSER_COMPOSER;
    break;
  case 121:
    currentListId = LIST_ID_BROWSER_AUDIOBOOK;
    break;
  case 122:
    currentListId = LIST_ID_BROWSER_PODCAST;
    break;
  case 123:
    currentListId = EventArgsVar.DB80.VS_UINT32Var[0];
    break;
  case 124:
    getIsRootFolder = EventArgsVar.DB135.VS_BOOLVar[0];
    break;
  case 125:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__SETPROFILEFAILED_INTERACTIVECENTERPOPUP);
    break;
  case 126:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__NOPHOTOFILE_TOASTPOPUP);
    break;
  case 127:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__FILEERROR_INTERACTIVECENTERPOPUP);
    break;
  case 128:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__NOVIDEOFILE_TOASTPOPUP);
    break;
  case 129:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__NOSOURCE_INTERACTIVECENTERPOPUP);
    break;
  case 130:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_DISC_ERROR);
    break;
  case 131:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_OVERHEATED);
    break;
  case 132:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_AUX__POPUP_ABCSEARCH_KEYBOARD);
    break;
  case 133:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
    break;
  case 134:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
    break;
  case 135:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
    break;
  case 136:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
    break;
  case 137:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
    break;
  case 138:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
    break;
  case 139:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 140:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 141:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 142:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 143:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 144:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 145:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 146:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 147:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 148:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 149:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 150:
    acPerform_MediaSpiStateActiveUpdMsg(false);
    break;
  case 151:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 152:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND);
    break;
  case 153:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND);
    break;
  case 154:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND);
    break;
  case 155:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND);
    break;
  case 156:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND);
    break;
  case 157:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_START_FAIL);
    break;
  case 158:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_START_FAIL);
    break;
  case 159:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 160:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTING_SYSIND);
    break;
  case 161:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTED_SYSIND);
    break;
  case 162:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTED_SYSIND);
    break;
  case 163:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND);
    break;
  case 164:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND);
    break;
  case 165:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 166:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND);
    break;
  case 167:
    gacViewDestroyReq(Global_Scenes_WAIT_SCENE);
    break;
  case 168:
    gacSetApplicationMode(Enum_APP_MODE_SOURCE);
    break;
  case 169:
    gacViewHideReq(Spi_Spi_Scene_INFO_CARPLAY_APP);
    break;
  case 170:
    gacViewDestroyReq(Spi_Spi_Scene_INFO_CARPLAY_APP);
    break;
  case 171:
    gacHideStatusLineReq(Enum_HEADER_TYPE_SPI);
    break;
  case 172:
    gacViewHideReq(Spi_Spi_Scene_SETTINGS__SPI);
    break;
  case 173:
    gacViewDestroyReq(Spi_Spi_Scene_SETTINGS__SPI);
    break;
  case 174:
    gacViewHideReq(Spi_Spi_Scene_SETTINGS__CARPLAY);
    break;
  case 175:
    gacViewDestroyReq(Spi_Spi_Scene_SETTINGS__CARPLAY);
    break;
  case 176:
    gacViewHideReq(Spi_Spi_Scene_SETTINGS__ANDROID);
    break;
  case 177:
    gacViewDestroyReq(Spi_Spi_Scene_SETTINGS__ANDROID);
    break;
  case 178:
    gacViewHideReq(Spi_Spi_Scene_TUTORIAL_ANDROID_AUTO);
    break;
  case 179:
    gacViewDestroyReq(Spi_Spi_Scene_TUTORIAL_ANDROID_AUTO);
    break;
  case 180:
    gacViewHideReq(Spi_Spi_Scene_TUTORIAL_CARPLAY);
    break;
  case 181:
    gacViewDestroyReq(Spi_Spi_Scene_TUTORIAL_CARPLAY);
    break;
  case 182:
    gacViewHideReq(Spi_Spi_Scene_INFO_APPS_SPI);
    break;
  case 183:
    gacViewDestroyReq(Spi_Spi_Scene_INFO_APPS_SPI);
    break;
  case 184:
    gacSetApplicationMode(Enum_APP_MODE_UTILITY);
    break;
  case 185:
    gacContextSwitchDoneRes(EventArgsVar.DB158.VS_UINT32Var[1]);
    break;
  case 186:
    gacViewCreateReq(Spi_Spi_Scene_INFO_CARPLAY_APP);
    break;
  case 187:
    gacViewShowReq(Spi_Spi_Scene_INFO_CARPLAY_APP);
    break;
  case 188:
    acPerform_MediaSpiStateActiveUpdMsg(true);
    break;
  case 189:
    gacDisplayStatusLineReq(Enum_HEADER_TYPE_SPI);
    break;
  case 190:
    gacViewCreateReq(Spi_Spi_Scene_SETTINGS__SPI);
    break;
  case 191:
    gacViewShowReq(Spi_Spi_Scene_SETTINGS__SPI);
    break;
  case 192:
    gacViewCreateReq(Spi_Spi_Scene_SETTINGS__CARPLAY);
    break;
  case 193:
    gacViewShowReq(Spi_Spi_Scene_SETTINGS__CARPLAY);
    break;
  case 194:
    gacViewCreateReq(Spi_Spi_Scene_SETTINGS__ANDROID);
    break;
  case 195:
    gacViewShowReq(Spi_Spi_Scene_SETTINGS__ANDROID);
    break;
  case 196:
    gacViewCreateReq(Spi_Spi_Scene_TUTORIAL_CARPLAY);
    break;
  case 197:
    gacViewShowReq(Spi_Spi_Scene_TUTORIAL_CARPLAY);
    break;
  case 198:
    gacViewCreateReq(Spi_Spi_Scene_TUTORIAL_ANDROID_AUTO);
    break;
  case 199:
    gacViewShowReq(Spi_Spi_Scene_TUTORIAL_ANDROID_AUTO);
    break;
  case 200:
    gacContextSwitchDoneRes(EventArgsVar.DB158.VS_UINT32Var[1]);
    break;
  case 201:
    gacContextSwitchDoneRes(EventArgsVar.DB158.VS_UINT32Var[1]);
    break;
  case 202:
    gacViewCreateReq(Spi_Spi_Scene_INFO_APPS_SPI);
    break;
  case 203:
    gacViewShowReq(Spi_Spi_Scene_INFO_APPS_SPI);
    break;
  case 204:
    acPerform_LaunchCarplayWithApp(Enum_DiPOAppType__DIPO_MOBILEPHONE);
    break;
  case 205:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_HK_NEXT);
    break;
  case 206:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_HK_NEXT);
    break;
  case 207:
    acPerform_InterruptToMeterMsg(false, Enum_NEXT_SONG);
    break;
  case 208:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_HK_PREVIOUS);
    break;
  case 209:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_HK_PREVIOUS);
    break;
  case 210:
    acPerform_InterruptToMeterMsg(false, Enum_PREV_SONG);
    break;
  case 211:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_SWC_NEXT);
    break;
  case 212:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_SWC_NEXT);
    break;
  case 213:
    acPerform_InterruptToMeterMsg(true, Enum_NEXT_SONG);
    break;
  case 214:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_SWC_PREV);
    break;
  case 215:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_SWC_PREV);
    break;
  case 216:
    acPerform_InterruptToMeterMsg(true, Enum_PREV_SONG);
    break;
  case 217:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_HK_NEXT);
    break;
  case 218:
    acPostBeep(Enum_hmibase_BEEPTYPE_ROGER);
    break;
  case 219:
    acPerform_InterruptToMeterMsg(false, Enum_FF_SONG);
    break;
  case 220:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_HK_NEXT);
    break;
  case 221:
    acPerform_InterruptToMeterMsg(false, Enum_FF_RW_STOP);
    break;
  case 222:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_HK_PREVIOUS);
    break;
  case 223:
    acPerform_InterruptToMeterMsg(false, Enum_RW_SONG);
    break;
  case 224:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_HK_PREVIOUS);
    break;
  case 225:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_SWC_NEXT);
    break;
  case 226:
    acPerform_InterruptToMeterMsg(true, Enum_FF_SONG);
    break;
  case 227:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_SWC_NEXT);
    break;
  case 228:
    acPerform_InterruptToMeterMsg(true, Enum_FF_RW_STOP);
    break;
  case 229:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_SWC_PREV);
    break;
  case 230:
    acPerform_InterruptToMeterMsg(true, Enum_RW_SONG);
    break;
  case 231:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_SWC_PREV);
    break;
  case 232:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND);
    break;
  case 233:
    gacViewHideReq(Spi_Spi_Scene_INFO__ANDROID_APP);
    break;
  case 234:
    gacViewDestroyReq(Spi_Spi_Scene_INFO__ANDROID_APP);
    break;
  case 235:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 236:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 237:
    acPerform_LaunchCarplayWithApp(Enum_DiPOAppType__DIPO_NOWPLAYING);
    break;
  case 238:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND);
    break;
  case 239:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND);
    break;
  case 240:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_START_FAIL);
    break;
  case 241:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_START_FAIL);
    break;
  case 242:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 243:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTING_SYSIND);
    break;
  case 244:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTED_SYSIND);
    break;
  case 245:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTED_SYSIND);
    break;
  case 246:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND);
    break;
  case 247:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND);
    break;
  case 248:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 249:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND);
    break;
  case 250:
    acPerform_LaunchAndroidAutowithAppReqMsg(Enum_DiPOAppType__DIPO_MOBILEPHONE);
    break;
  case 251:
    acPerform_LaunchAndroidAutowithAppReqMsg(Enum_DiPOAppType__DIPO_MUSIC);
    break;
  case 252:
    acPerform_onAppStateUpdMsg(Enum_hmibase_IN_BACKGROUND);
    break;
  case 253:
    acPerform_onAppStateUpdMsg(Enum_hmibase_IN_FOREGROUND);
    break;
  case 254:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_SWC_PTT);
    break;
  case 255:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_SWC_PTT);
    break;
  case 256:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_SWC_PHONE);
    break;
  case 257:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_SWC_PHONE);
    break;
  case 258:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTING_SYSIND);
    break;
  case 259:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 260:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_HK_PHONE);
    break;
  case 261:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_HK_PHONE);
    break;
  case 262:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_SWC_PTT);
    break;
  case 263:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_SWC_PTT);
    break;
  case 264:
    acPerform_LaunchCarplayWithApp(Enum_DiPOAppType__DIPO_SIRI_BUTTONDOWN);
    break;
  case 265:
    acPerform_LaunchCarplayWithApp(Enum_DiPOAppType__DIPO_SIRI_BUTTONUP);
    break;
  case 266:
    acPostBeep(Enum_hmibase_BEEPTYPE_WARN);
    break;
  case 267:
    gacHideStatusLineReq(Enum_HEADER_TYPE_AUDIO_OTHER_TOP_SCREEN);
    break;
  case 268:
    gacViewHideReq(Media_Scenes_MEDIA__CD_CDMP3_MAIN);
    break;
  case 269:
    gacViewDestroyReq(Media_Scenes_MEDIA__CD_CDMP3_MAIN);
    break;
  case 270:
    gacViewHideReq(Media_Scenes_MEDIA__AUX__USB_MAIN);
    break;
  case 271:
    gacViewDestroyReq(Media_Scenes_MEDIA__AUX__USB_MAIN);
    break;
  case 272:
    gacViewHideReq(Media_Scenes_MEDIA__AUX__IPOD_MAIN);
    break;
  case 273:
    gacViewDestroyReq(Media_Scenes_MEDIA__AUX__IPOD_MAIN);
    break;
  case 274:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__LINEIN_MAIN);
    break;
  case 275:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__LINEIN_MAIN);
    break;
  case 276:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT);
    break;
  case 277:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT);
    break;
  case 278:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 279:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE);
    break;
  case 280:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE);
    break;
  case 281:
    gacPopupSBCloseReq(Global_Scenes_WAIT_SCENE);
    break;
  case 282:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__BROWSER);
    break;
  case 283:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__BROWSER);
    break;
  case 284:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_AUX__POPUP_ABCSEARCH_KEYBOARD);
    break;
  case 285:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__METADATA_BROWSE);
    break;
  case 286:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__METADATA_BROWSE);
    break;
  case 287:
    gacHideStatusLineReq(Enum_HEADER_TYPE_AUDIO_BT_SUB_SCREEN);
    break;
  case 288:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__BTAUDIO_BROWSER);
    break;
  case 289:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__BTAUDIO_BROWSER);
    break;
  case 290:
    gacHideStatusLineReq(Enum_HEADER_TYPE_AUDIO_OTHER_SUB_SCREEN);
    break;
  case 291:
    gacHideStatusLineReq(Enum_HEADER_TYPE_AUDIO_BT_TOP_SCREEN);
    break;
  case 292:
    gacViewHideReq(Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN);
    break;
  case 293:
    gacViewDestroyReq(Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN);
    break;
  case 294:
    gacViewHideReq(Media_Scenes_MEDIA__DTM_SYSTEM_DRIVES);
    break;
  case 295:
    gacViewDestroyReq(Media_Scenes_MEDIA__DTM_SYSTEM_DRIVES);
    break;
  case 296:
    gacViewHideReq(Media_Scenes_MEDIA__INFORMATION__SETTINGS);
    break;
  case 297:
    gacViewDestroyReq(Media_Scenes_MEDIA__INFORMATION__SETTINGS);
    break;
  case 298:
    gacViewHideReq(Media_Scenes_MEDIA_AUX_PHOTO_PLAYER_SLIDESHOW);
    break;
  case 299:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX_PHOTO_PLAYER_SLIDESHOW);
    break;
  case 300:
    gacViewHideReq(Media_Scenes_MEDIA_AUX_PHOTO_PLAYER_FULLSCREEN);
    break;
  case 301:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX_PHOTO_PLAYER_FULLSCREEN);
    break;
  case 302:
    gacViewHideReq(Media_Scenes_MEDIA_AUX_PHOTO_PLAYER_COVER);
    break;
  case 303:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX_PHOTO_PLAYER_COVER);
    break;
  case 304:
    gacViewHideReq(Media_Scenes_MEDIA__AUX__USB_PHOTO_BROWSER);
    break;
  case 305:
    gacViewDestroyReq(Media_Scenes_MEDIA__AUX__USB_PHOTO_BROWSER);
    break;
  case 306:
    gacViewHideReq(Media_Scenes_MEDIA_VIDEO_PLAYER_MAIN);
    break;
  case 307:
    gacViewDestroyReq(Media_Scenes_MEDIA_VIDEO_PLAYER_MAIN);
    break;
  case 308:
    gacViewDestroyReq(Media_Scenes_MEDIA_VIDEO_PLAYER_FULLSCREEN);
    break;
  case 309:
    gacViewHideReq(Media_Scenes_MEDIA_VIDEO_PLAYER_FULLSCREEN);
    break;
  case 310:
    acPerform_ScreenNameUpdMsg(Media_Scenes_MEDIA__AUX__USB_MAIN);
    break;
  case 311:
    gacDisplayStatusLineReq(Enum_HEADER_TYPE_AUDIO_OTHER_TOP_SCREEN);
    break;
  case 312:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA__AUX__USB_MAIN);
    break;
  case 313:
    gacViewCreateReq(Media_Scenes_MEDIA__AUX__USB_MAIN);
    break;
  case 314:
    gacViewShowReq(Media_Scenes_MEDIA__AUX__USB_MAIN);
    break;
  case 315:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA__AUX__IPOD_MAIN);
    break;
  case 316:
    gacViewCreateReq(Media_Scenes_MEDIA__AUX__IPOD_MAIN);
    break;
  case 317:
    gacViewShowReq(Media_Scenes_MEDIA__AUX__IPOD_MAIN);
    break;
  case 318:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT);
    break;
  case 319:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT);
    break;
  case 320:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT);
    break;
  case 321:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__LINEIN_MAIN);
    break;
  case 322:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__LINEIN_MAIN);
    break;
  case 323:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__LINEIN_MAIN);
    break;
  case 324:
    gacDisplayStatusLineReq(Enum_HEADER_TYPE_AUDIO_BT_TOP_SCREEN);
    break;
  case 325:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN);
    break;
  case 326:
    gacViewCreateReq(Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN);
    break;
  case 327:
    gacViewShowReq(Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN);
    break;
  case 328:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA__CD_CDMP3_MAIN);
    break;
  case 329:
    gacViewCreateReq(Media_Scenes_MEDIA__CD_CDMP3_MAIN);
    break;
  case 330:
    gacViewShowReq(Media_Scenes_MEDIA__CD_CDMP3_MAIN);
    break;
  case 331:
    gacViewCreateReq(Media_Scenes_MEDIA__DTM_SYSTEM_DRIVES);
    break;
  case 332:
    gacViewShowReq(Media_Scenes_MEDIA__DTM_SYSTEM_DRIVES);
    break;
  case 333:
    gacViewCreateReq(Media_Scenes_MEDIA__INFORMATION__SETTINGS);
    break;
  case 334:
    gacViewShowReq(Media_Scenes_MEDIA__INFORMATION__SETTINGS);
    break;
  case 335:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX_PHOTO_PLAYER_SLIDESHOW);
    break;
  case 336:
    gacViewShowReq(Media_Scenes_MEDIA_AUX_PHOTO_PLAYER_SLIDESHOW);
    break;
  case 337:
    gacViewCreateReq(Media_Scenes_MEDIA_VIDEO_PLAYER_MAIN);
    break;
  case 338:
    gacViewShowReq(Media_Scenes_MEDIA_VIDEO_PLAYER_MAIN);
    break;
  case 339:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 340:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 341:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
    break;
  case 342:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
    break;
  case 343:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
    break;
  case 344:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
    break;
  case 345:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
    break;
  case 346:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 347:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
    break;
  case 348:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
    break;
  case 349:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
    break;
  case 350:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
    break;
  case 351:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 352:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 353:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 354:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 355:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 356:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_OVERHEATED);
    break;
  case 357:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_DISC_ERROR);
    break;
  case 358:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 359:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 360:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 361:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_DISC_ERROR);
    break;
  case 362:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_OVERHEATED);
    break;
  case 363:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 364:
    gacContextSwitchOutReq(Enum_MASTER_CONTEXT_AUDIOSOURCE, 0, Enum_APPID_APPHMI_MASTER);
    break;
  case 365:
    acPerform_OpenDropdown(LIST_ID_POPOUT_LIST);
    break;
  case 366:
    acPerform_CloseDropdown(LIST_ID_POPOUT_LIST);
    break;
  case 367:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__FILEERROR_INTERACTIVECENTERPOPUP);
    break;
  case 368:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__FILEERROR_INTERACTIVECENTERPOPUP);
    break;
  case 369:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__NOPHOTOFILE_TOASTPOPUP);
    break;
  case 370:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__NOPHOTOFILE_TOASTPOPUP);
    break;
  case 371:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__NOSOURCE_INTERACTIVECENTERPOPUP);
    break;
  case 372:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__NOSOURCE_INTERACTIVECENTERPOPUP);
    break;
  case 373:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__NOVIDEOFILE_TOASTPOPUP);
    break;
  case 374:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__NOVIDEOFILE_TOASTPOPUP);
    break;
  case 375:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__SETPROFILEFAILED_INTERACTIVECENTERPOPUP);
    break;
  case 376:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__SETPROFILEFAILED_INTERACTIVECENTERPOPUP);
    break;
  case 377:
    gacViewShowReq(MediaPopups_Popups_MEDIA__SETPROFILEFAILED_INTERACTIVECENTERPOPUP);
    break;
  case 378:
    gacViewHideReq(MediaPopups_Popups_MEDIA__SETPROFILEFAILED_INTERACTIVECENTERPOPUP);
    break;
  case 379:
    gacViewClearReq(MediaPopups_Popups_MEDIA__SETPROFILEFAILED_INTERACTIVECENTERPOPUP);
    break;
  case 380:
    gacViewShowReq(MediaPopups_Popups_MEDIA__NOPHOTOFILE_TOASTPOPUP);
    break;
  case 381:
    gacViewHideReq(MediaPopups_Popups_MEDIA__NOPHOTOFILE_TOASTPOPUP);
    break;
  case 382:
    gacViewClearReq(MediaPopups_Popups_MEDIA__NOPHOTOFILE_TOASTPOPUP);
    break;
  case 383:
    gacViewShowReq(MediaPopups_Popups_MEDIA__FILEERROR_INTERACTIVECENTERPOPUP);
    break;
  case 384:
    gacViewHideReq(MediaPopups_Popups_MEDIA__FILEERROR_INTERACTIVECENTERPOPUP);
    break;
  case 385:
    gacViewClearReq(MediaPopups_Popups_MEDIA__FILEERROR_INTERACTIVECENTERPOPUP);
    break;
  case 386:
    gacViewShowReq(MediaPopups_Popups_MEDIA__NOVIDEOFILE_TOASTPOPUP);
    break;
  case 387:
    gacViewHideReq(MediaPopups_Popups_MEDIA__NOVIDEOFILE_TOASTPOPUP);
    break;
  case 388:
    gacViewClearReq(MediaPopups_Popups_MEDIA__NOVIDEOFILE_TOASTPOPUP);
    break;
  case 389:
    gacViewShowReq(MediaPopups_Popups_MEDIA__NOSOURCE_INTERACTIVECENTERPOPUP);
    break;
  case 390:
    gacViewHideReq(MediaPopups_Popups_MEDIA__NOSOURCE_INTERACTIVECENTERPOPUP);
    break;
  case 391:
    gacViewClearReq(MediaPopups_Popups_MEDIA__NOSOURCE_INTERACTIVECENTERPOPUP);
    break;
  case 392:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_DISC_ERROR, 6000);
    break;
  case 393:
    gacViewShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_DISC_ERROR);
    break;
  case 394:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_DISC_ERROR);
    break;
  case 395:
    gacViewHideReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_DISC_ERROR);
    break;
  case 396:
    gacViewClearReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_DISC_ERROR);
    break;
  case 397:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_OVERHEATED, 6000);
    break;
  case 398:
    gacViewShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_OVERHEATED);
    break;
  case 399:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_OVERHEATED);
    break;
  case 400:
    gacViewHideReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_OVERHEATED);
    break;
  case 401:
    gacViewClearReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_OVERHEATED);
    break;
  case 402:
    gacViewShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_ABCSEARCH_KEYBOARD);
    break;
  case 403:
    gacViewHideReq(MediaPopups_Popups_MEDIA_AUX__POPUP_ABCSEARCH_KEYBOARD);
    break;
  case 404:
    gacViewClearReq(MediaPopups_Popups_MEDIA_AUX__POPUP_ABCSEARCH_KEYBOARD);
    break;
  case 405:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING, 6000);
    break;
  case 406:
    gacViewShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
    break;
  case 407:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
    break;
  case 408:
    gacViewHideReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
    break;
  case 409:
    gacViewClearReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
    break;
  case 410:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING, 6000);
    break;
  case 411:
    gacViewShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
    break;
  case 412:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
    break;
  case 413:
    gacViewHideReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
    break;
  case 414:
    gacViewClearReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
    break;
  case 415:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_READING, 6000);
    break;
  case 416:
    gacViewShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
    break;
  case 417:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
    break;
  case 418:
    gacViewHideReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
    break;
  case 419:
    gacViewClearReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
    break;
  case 420:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
    break;
  case 421:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR, 6000);
    break;
  case 422:
    gacViewShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
    break;
  case 423:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
    break;
  case 424:
    gacViewHideReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
    break;
  case 425:
    gacViewClearReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
    break;
  case 426:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS, 6000);
    break;
  case 427:
    gacViewShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
    break;
  case 428:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
    break;
  case 429:
    gacViewHideReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
    break;
  case 430:
    gacViewClearReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
    break;
  case 431:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION, 3000);
    break;
  case 432:
    gacViewShowReq(MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
    break;
  case 433:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
    break;
  case 434:
    gacViewHideReq(MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
    break;
  case 435:
    gacViewClearReq(MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
    break;
  case 436:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
    break;
  case 437:
    gacViewShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 438:
    gacViewHideReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 439:
    gacViewClearReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 440:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND, 6000);
    break;
  case 441:
    gacViewShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 442:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 443:
    gacViewHideReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 444:
    gacViewClearReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 445:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND, 6000);
    break;
  case 446:
    gacViewShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 447:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 448:
    gacViewHideReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 449:
    gacViewClearReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 450:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 451:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND, 6000);
    break;
  case 452:
    gacViewShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 453:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 454:
    gacViewHideReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 455:
    gacViewClearReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 456:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 457:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND, 6000);
    break;
  case 458:
    gacViewShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 459:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 460:
    gacViewHideReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 461:
    gacViewClearReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 462:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 463:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND, 6000);
    break;
  case 464:
    gacViewShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 465:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 466:
    gacViewHideReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 467:
    gacViewClearReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 468:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 469:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND, 6000);
    break;
  case 470:
    gacViewShowReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 471:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 472:
    gacViewHideReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 473:
    gacViewClearReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 474:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 475:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND, 6000);
    break;
  case 476:
    gacViewShowReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 477:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 478:
    gacViewHideReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 479:
    gacViewClearReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 480:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 481:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE, 6000);
    break;
  case 482:
    gacViewShowReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 483:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 484:
    gacViewHideReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 485:
    gacViewClearReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 486:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 487:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH, 30000);
    break;
  case 488:
    gacViewShowReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 489:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 490:
    gacViewHideReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 491:
    gacViewClearReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 492:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND, 6000);
    break;
  case 493:
    gacViewShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 494:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 495:
    gacViewHideReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 496:
    gacViewClearReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 497:
    acPerform_onHK_BackUpdMsg(Enum_enPress);
    break;
  case 498:
    acPerform_onHK_BackUpdMsg(Enum_enRelease);
    break;
  case 499:
    acPerform_EncoderRotatonUpdMsg(EventArgsVar.DB172.VS_INT8Var[0]);
    break;
  case 500:
    acPerform_EncoderPressedUpdMsg(Enum_enPress);
    break;
  case 501:
    acPerform_EncoderPressedUpdMsg(Enum_enRelease);
    break;
  case 502:
    gacContextSwitchBackRes(IN_SwitchId);
    break;
  case 503:
    gacContextSwitchCompleteRes(IN_SwitchId);
    break;
  case 504:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND);
    break;
  case 505:
    gacPopupCreateAndSBShowReq(Global_Scenes_WAIT_SCENE);
    break;
  case 506:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND);
    break;
  case 507:
    acPostBeep(Enum_hmibase_BEEPTYPE_CLICK);
    break;
  case 508:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 509:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 510:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 511:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND);
    break;
  case 512:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND);
    break;
  case 513:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND);
    break;
  case 514:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND);
    break;
  case 515:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND);
    break;
  case 516:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND);
    break;
  case 517:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND);
    break;
  case 518:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND);
    break;
  case 519:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND);
    break;
  case 520:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND);
    break;
  case 521:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND);
    break;
  case 522:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND);
    break;
  case 523:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND);
    break;
  case 524:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND);
    break;
  case 525:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND);
    break;
  case 526:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND);
    break;
  case 527:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND);
    break;
  case 528:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND);
    break;
  case 529:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND);
    break;
  case 530:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND);
    break;
  case 531:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_START_FAIL);
    break;
  case 532:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_START_FAIL);
    break;
  case 533:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_START_FAIL);
    break;
  case 534:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_START_FAIL);
    break;
  case 535:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_START_FAIL);
    break;
  case 536:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_START_FAIL);
    break;
  case 537:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_START_FAIL);
    break;
  case 538:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_START_FAIL);
    break;
  case 539:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 540:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 541:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 542:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTING_SYSIND);
    break;
  case 543:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTING_SYSIND);
    break;
  case 544:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTING_SYSIND);
    break;
  case 545:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTED_SYSIND);
    break;
  case 546:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTED_SYSIND);
    break;
  case 547:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTED_SYSIND);
    break;
  case 548:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTED_SYSIND);
    break;
  case 549:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTED_SYSIND);
    break;
  case 550:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTED_SYSIND);
    break;
  case 551:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTED_SYSIND);
    break;
  case 552:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTED_SYSIND);
    break;
  case 553:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND);
    break;
  case 554:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND);
    break;
  case 555:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND);
    break;
  case 556:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND);
    break;
  case 557:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND);
    break;
  case 558:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND);
    break;
  case 559:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND);
    break;
  case 560:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND);
    break;
  case 561:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 562:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 563:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 564:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 565:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND);
    break;
  case 566:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND);
    break;
  case 567:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND);
    break;
  case 568:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND);
    break;
  case 569:
    gacViewShowReq(Global_Scenes_WAIT_SCENE);
    break;
  case 570:
    gacRegisterForCloseOnTouchSession(Global_Scenes_WAIT_SCENE);
    break;
  case 571:
    gacDeregisterForCloseOnTouchSessionReq(Global_Scenes_WAIT_SCENE);
    break;
  case 572:
    gacViewHideReq(Global_Scenes_WAIT_SCENE);
    break;
  case 573:
    gacViewClearReq(Global_Scenes_WAIT_SCENE);
    break;
  case 574:
    gacDisplayStatusLineReq(Enum_HEADER_TYPE_AUDIO_OTHER_SUB_SCREEN);
    break;
  case 575:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE);
    break;
  case 576:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE);
    break;
  case 577:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE);
    break;
  case 578:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__METADATA_BROWSE);
    break;
  case 579:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__METADATA_BROWSE);
    break;
  case 580:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__METADATA_BROWSE);
    break;
  case 581:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__BROWSER);
    break;
  case 582:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__BROWSER);
    break;
  case 583:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__BROWSER);
    break;
  case 584:
    acPerform_InitialisePhotoPlayerCoverMsg(0);
    break;
  case 585:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX_PHOTO_PLAYER_COVER);
    break;
  case 586:
    gacViewShowReq(Media_Scenes_MEDIA_AUX_PHOTO_PLAYER_COVER);
    break;
  case 587:
    gacViewCreateReq(Media_Scenes_MEDIA__AUX__USB_PHOTO_BROWSER);
    break;
  case 588:
    gacViewShowReq(Media_Scenes_MEDIA__AUX__USB_PHOTO_BROWSER);
    break;
  case 589:
    acPerform_EncoderTrackChangeUpMsg(Enum_ENCODER_ROTATION_POSITIVE, EventArgsVar.DB172.VS_INT8Var[0]);
    break;
  case 590:
    acPerform_EncoderTrackChangeUpMsg(Enum_ENCODER_ROTATION_NEGATIVE, EventArgsVar.DB172.VS_INT8Var[0]);
    break;
  case 591:
    acPerform_ChangeVolumeReqMsg(Enum_LOW_VOLUME);
    break;
  case 592:
    acPerform_ChangeVolumeReqMsg(Enum_HIGH_VOLUME);
    break;
  case 593:
    acPerform_ChangeVolumeReqMsg(Enum_MID_VOLUME);
    break;
  case 594:
    gacContextSwitchOutReq(Enum_PHONE_CONTEXT_CONNECTION_MANAGER, Enum_MEDIA_CONTEXT_BT_AUDIO_DEFAULT, Enum_APPID_APPHMI_PHONE);
    break;
  case 595:
    acPerform_MediaInfoSettingsBtnPressUpdMsg(Enum_VIDEO_INFORMATION);
    break;
  case 596:
    acPerform_MediaInfoSettingsBtnPressUpdMsg(Enum_VIDEO_PLAYER_SETTINGS);
    break;
  case 597:
    gacDisplayStatusLineReq(Enum_HEADER_TYPE_AUDIO_BT_SUB_SCREEN);
    break;
  case 598:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__BTAUDIO_BROWSER);
    break;
  case 599:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__BTAUDIO_BROWSER);
    break;
  case 600:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__BTAUDIO_BROWSER);
    break;
  case 601:
    gacContextSwitchOutReq(Enum_PHONE_CONTEXT_CONNECTION_MANAGER, Enum_MEDIA_CONTEXT_HOMESCREEN_BT_AUDIO, Enum_APPID_APPHMI_PHONE);
    break;
  case 602:
    acPerform_EncoderTrackChangeUpMsg(Enum_ENCODER_ROTATION_POSITIVE, EventArgsVar.DB172.VS_INT8Var[0]);
    break;
  case 603:
    acPerform_EncoderTrackChangeUpMsg(Enum_ENCODER_ROTATION_NEGATIVE, EventArgsVar.DB172.VS_INT8Var[0]);
    break;
  case 604:
    gacContextSwitchBackRes(currentSwitchID);
    break;
  case 605:
    gacContextSwitchCompleteRes(currentSwitchID);
    break;
  case 606:
    acPerform_VideoModeBtnPressUpdMsg(Enum_VIDEO_INFORMATION);
    break;
  case 607:
    acPerform_MediaInfoSettingsBtnPressUpdMsg(Enum_PHOTO_INFORMATION);
    break;
  case 608:
    acPerform_MediaInfoSettingsBtnPressUpdMsg(Enum_PHOTO_PLAYER_SETTINGS);
    break;
  case 609:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 610:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_ABCSEARCH_KEYBOARD);
    break;
  case 611:
    acPerform_FolderBrowserBtnPress(EventArgsVar.DB158.VS_UINT32Var[0]);
    break;
  case 612:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB158.VS_UINT32Var[0]);
    break;
  case 613:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB158.VS_UINT32Var[0]);
    break;
  case 614:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB158.VS_UINT32Var[0]);
    break;
  case 615:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB158.VS_UINT32Var[0]);
    break;
  case 616:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB158.VS_UINT32Var[0]);
    break;
  case 617:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB158.VS_UINT32Var[0]);
    break;
  case 618:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB158.VS_UINT32Var[0]);
    break;
  case 619:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB158.VS_UINT32Var[0]);
    break;
  case 620:
    acPerform_MetaDataBrowserBtnPress(PLAY_SONG_IN_LIST, EventArgsVar.DB158.VS_UINT32Var[1]);
    break;
  case 621:
    acPerform_MetaDataBrowserBtnPress(EventArgsVar.DB158.VS_UINT32Var[0], EventArgsVar.DB158.VS_UINT32Var[1]);
    break;
  case 622:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 623:
    gacContextSwitchOutReq(Enum_PHONE_CONTEXT_CONNECTION_MANAGER, Enum_MEDIA_CONTEXT_BT_AUDIO_BROWSE, Enum_APPID_APPHMI_PHONE);
    break;
  case 624:
    acPerform_FolderBrowserBtnPress(EventArgsVar.DB158.VS_UINT32Var[0]);
    break;
  case 625:
    acPerform_ButtonPhotoPlayerCoverItemPressUpMsg(LIST_ID_COVER_PLAYER_IMAGE, EventArgsVar.DB80.VS_UINT32Var[0]);
    break;
  case 626:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX_PHOTO_PLAYER_FULLSCREEN);
    break;
  case 627:
    gacViewShowReq(Media_Scenes_MEDIA_AUX_PHOTO_PLAYER_FULLSCREEN);
    break;
  case 628:
    acPerform_ButtonPhotoPlayerCoverItemPressUpMsg(LIST_ID_COVER_PLAYER_IMAGE, EventArgsVar.DB80.VS_UINT32Var[0]);
    break;
  case 629:
    acPerform_InitialisePhotoPlayerCoverMsg(EventArgsVar.DB80.VS_UINT32Var[0]);
    break;
  case 630:
    gacViewCreateReq(Media_Scenes_MEDIA_VIDEO_PLAYER_FULLSCREEN);
    break;
  case 631:
    gacViewShowReq(Media_Scenes_MEDIA_VIDEO_PLAYER_FULLSCREEN);
    break;

  default:
    break;
  }
}
