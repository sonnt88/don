/*
 * Id:        AppHmi_MediaStateMachineData.cpp
 *
 * Function:  VS System Data Source File.
 *
 * Generated: Tue Apr 26 23:56:42 2016
 *
 * Coder 7, 3, 2, 2426
 * 
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include "AppHmi_MediaStateMachineSEMLibB.h"


#include "AppHmi_MediaStateMachineData.h"


#include <stdarg.h>


/*
 * VS System Internal Variable Initializing Function.
 */
void AppHmi_MediaStateMachine::SEM_InitInternalVariables (void)
{
  currentListId = 4032ul;
  getActiveSrc = 0ul;
  getIsRootFolder = 0;
  isTempContextActive = 0;
  screen_Type = 1;
  activeApplicationID = 13;
  Cs_SelfContextId = 0;
  IN_SwitchId = 0;
  IsSpiPhoneSourceActive = 0;
  TempContextStatus = 0;
}


/*
 * SEM Deduct Function.
 */
unsigned char AppHmi_MediaStateMachine::SEM_Deduct (SEM_EVENT_TYPE EventNo, ...)
{
  va_list ap;

  va_start(ap, EventNo);
  if (SEM.State == 0x00u /* STATE_SEM_NOT_INITIALIZED */)
  {
    return SES_NOT_INITIALIZED;
  }
  if (VS_NOF_EVENTS <= EventNo)
  {
    return (SES_RANGE_ERR);
  }
  switch (EventNo)
  {
  case 13:
    EventArgsVar.DB71.VS_BOOLVar[0] = (VS_BOOL) va_arg(ap, VS_INT);
    break;

  case 14:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 15:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 17:
    EventArgsVar.DB17.VS_UINT8Var[0] = (VS_UINT8) va_arg(ap, VS_INT);
    EventArgsVar.DB17.VS_UINT8Var[1] = (VS_UINT8) va_arg(ap, VS_INT);
    break;

  case 21:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 22:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 23:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 24:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 25:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB64.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 26:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 27:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 28:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 29:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 30:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 31:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 32:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 33:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 34:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 35:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 36:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB64.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 37:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB64.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 38:
    EventArgsVar.DB94.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB94.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB94.VS_UINT32Var[2] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 39:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 40:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 41:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 42:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 43:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 44:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 45:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 46:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 47:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 48:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 49:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 50:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 51:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 52:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 53:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 54:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 55:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 56:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 57:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 58:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 59:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 61:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 62:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB64.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 63:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB64.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 64:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB64.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 65:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB64.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 66:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB64.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 67:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB64.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 68:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB64.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 69:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 70:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 71:
    EventArgsVar.DB71.VS_BOOLVar[0] = (VS_BOOL) va_arg(ap, VS_INT);
    break;

  case 72:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 73:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 74:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    EventArgsVar.DB74.VS_INT8Var[1] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 77:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 78:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 79:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 80:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 81:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 90:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 91:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 92:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 93:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 94:
    EventArgsVar.DB94.VS_UINT8Var[0] = (VS_UINT8) va_arg(ap, VS_INT);
    EventArgsVar.DB94.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB94.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB94.VS_UINT32Var[2] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 95:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB64.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 96:
    EventArgsVar.DB96.VS_BOOLVar[0] = (VS_BOOL) va_arg(ap, VS_INT);
    break;

  case 97:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB64.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 98:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB64.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 99:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 100:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 101:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB64.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 102:
    EventArgsVar.DB64.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB64.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 103:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 104:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 105:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 106:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    EventArgsVar.DB74.VS_INT8Var[1] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 109:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 110:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 111:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 112:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 113:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 114:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 115:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 117:
    EventArgsVar.DB117.VS_UINT8Var[0] = (VS_UINT8) va_arg(ap, VS_INT);
    EventArgsVar.DB117.VS_UINT8Var[1] = (VS_UINT8) va_arg(ap, VS_INT);
    break;

  case 118:
    EventArgsVar.DB74.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 122:
    EventArgsVar.DB123.VS_INT32Var[0] = (VS_INT32) va_arg(ap, VS_INT32_VAARG);
    break;

  case 123:
    EventArgsVar.DB123.VS_INT32Var[0] = (VS_INT32) va_arg(ap, VS_INT32_VAARG);
    break;

  default:
    break;
  }
  SEM.EventNo = EventNo;
  SEM.DIt = 2;
  SEM.State = 0x02u; /* STATE_SEM_PREPARE */

  va_end(ap);
  return (SES_OKAY);
}


/*
 * Guard Expression Functions.
 */
VS_BOOL AppHmi_MediaStateMachine::VSGuard (SEM_GUARD_EXPRESSION_TYPE i)
{
  switch (i)
  {
  case 0:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_CARPLAY_TUTORIAL);
  case 1:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_DOWN && dgSpiMediaActiveSource());
  case 2:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && dgSpiMediaActiveSource());
  case 3:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && dgSpiMediaActiveSource());
  case 4:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP && dgSpiMediaActiveSource());
  case 5:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == CARPLAY_ASK_ON_CONNECT_STATUS_ON);
  case 6:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_DOWN && dgSpiSessionActive());
  case 7:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && dgSpiSessionActive());
  case 8:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && dgSpiSessionActive());
  case 9:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP && dgSpiSessionActive());
  case 10:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_DOWN && dgSpeechRecognitionActiveSource());
  case 11:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && dgSpeechRecognitionActiveSource());
  case 12:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && dgBlockClockScreenOnHKILLUMLongPress() == 1);
  case 13:
    return (VS_BOOL)(EventArgsVar.DB96.VS_BOOLVar[0] == true);
  case 14:
    return (VS_BOOL)((EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP || EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP) && screen_Type == Enum_OUTSIDE_BROWSE_SCREENS);
  case 15:
    return (VS_BOOL)(EventArgsVar.DB17.VS_UINT8Var[0] == Enum_SOURCE_AUX);
  case 16:
    return (VS_BOOL)(EventArgsVar.DB117.VS_UINT8Var[0] == Enum_SOURCE_AUX);
  case 17:
    return (VS_BOOL)(EventArgsVar.DB17.VS_UINT8Var[0] == Enum_SOURCE_USB);
  case 18:
    return (VS_BOOL)(EventArgsVar.DB117.VS_UINT8Var[0] == Enum_SOURCE_USB);
  case 19:
    return (VS_BOOL)(EventArgsVar.DB17.VS_UINT8Var[0] == Enum_SOURCE_IPOD);
  case 20:
    return (VS_BOOL)(EventArgsVar.DB117.VS_UINT8Var[0] == Enum_SOURCE_IPOD);
  case 21:
    return (VS_BOOL)(EventArgsVar.DB17.VS_UINT8Var[0] == Enum_SOURCE_BT && EventArgsVar.DB17.VS_UINT8Var[1] == 1);
  case 22:
    return (VS_BOOL)(EventArgsVar.DB117.VS_UINT8Var[0] == Enum_SOURCE_BT && EventArgsVar.DB117.VS_UINT8Var[1] == 1);
  case 23:
    return (VS_BOOL)(EventArgsVar.DB17.VS_UINT8Var[0] == Enum_SOURCE_BT && EventArgsVar.DB17.VS_UINT8Var[1] == 0);
  case 24:
    return (VS_BOOL)(EventArgsVar.DB117.VS_UINT8Var[0] == Enum_SOURCE_BT && EventArgsVar.DB117.VS_UINT8Var[1] == 0);
  case 25:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getActiveSrc != Enum_SOURCE_BT_NOT_CONNECTED && getActiveSrc != Enum_SOURCE_AUX && dgIsMediaActive());
  case 26:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && getActiveSrc != Enum_SOURCE_BT_NOT_CONNECTED && getActiveSrc != Enum_SOURCE_AUX && dgIsMediaActive());
  case 27:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP && getActiveSrc != Enum_SOURCE_BT_NOT_CONNECTED && getActiveSrc != Enum_SOURCE_AUX && dgIsMediaActive());
  case 28:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
  case 29:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
  case 30:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
  case 31:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
  case 32:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
  case 33:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
  case 34:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
  case 35:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
  case 36:
    return (VS_BOOL)(isTempContextActive == false);
  case 37:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
  case 38:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
  case 39:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__POPUP_SOURCE_INITIALISING);
  case 40:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
  case 41:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
  case 42:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
  case 43:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
  case 44:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
  case 45:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
  case 46:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
  case 47:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
  case 48:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
  case 49:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
  case 50:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] < 0);
  case 51:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
  case 52:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] > 0);
  case 53:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP);
  case 54:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_DOWN);
  case 55:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
  case 56:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1);
  case 57:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP);
  case 58:
    return (VS_BOOL)((EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP || EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP) && dgIsMediaActive());
  case 59:
    return (VS_BOOL)((EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP || EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP) && dgSpiMediaActiveSource());
  case 60:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP || EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP);
  case 61:
    return (VS_BOOL)(TempContextStatus != 1);
  case 62:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_REMEMBER_DISCLAIMER_SYSIND);
  case 63:
    return (VS_BOOL)(dgSetDisclaimerState() == 1);
  case 64:
    return (VS_BOOL)(dgSetDisclaimerState() == 0);
  case 65:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && dgSetDisclaimerState() == 0);
  case 66:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && dgSetDisclaimerState() == 1);
  case 67:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
  case 68:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_ADVISORY_SYSIND);
  case 69:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_DISCLAIMER_SYSIND);
  case 70:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB64.VS_UINT32Var[1] == Global_Scenes_WAIT_SCENE);
  case 71:
    return (VS_BOOL)(currentListId != LIST_ID_BROWSER_MAIN);
  case 72:
    return (VS_BOOL)(currentListId == LIST_ID_BROWSER_MAIN);
  case 73:
    return (VS_BOOL)((EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP || EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP) && screen_Type == Enum_INSDIE_BROWSE_SCREENS);
  case 74:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getIsRootFolder == true);
  case 75:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getIsRootFolder == false);
  case 76:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == LIST_ROW_VALUE);
  case 77:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_USB_BROWSE_FOLDER_BROWSE && getActiveSrc == Enum_SOURCE_USB);
  case 78:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_USB_BROWSE_PLAYLIST && getActiveSrc == Enum_SOURCE_USB || EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_PLAYLIST && getActiveSrc == Enum_SOURCE_IPOD);
  case 79:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_USB_BROWSE_ARTIST && getActiveSrc == Enum_SOURCE_USB || EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_ARTIST && getActiveSrc == Enum_SOURCE_IPOD);
  case 80:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_USB_BROWSE_ALBUM && getActiveSrc == Enum_SOURCE_USB || EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_ALBUM && getActiveSrc == Enum_SOURCE_IPOD);
  case 81:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_USB_BROWSE_SONG && getActiveSrc == Enum_SOURCE_USB || EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_SONG && getActiveSrc == Enum_SOURCE_IPOD);
  case 82:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_USB_BROWSE_GENRE && getActiveSrc == Enum_SOURCE_USB || EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_GENRE && getActiveSrc == Enum_SOURCE_IPOD);
  case 83:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_USB_BROWSE_COMPOSER && getActiveSrc == Enum_SOURCE_USB || EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_COMPOSER && getActiveSrc == Enum_SOURCE_IPOD);
  case 84:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_AUDIOBOOK && getActiveSrc == Enum_SOURCE_IPOD);
  case 85:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_PODCAST && getActiveSrc == Enum_SOURCE_IPOD);
  case 86:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getActiveSrc == Enum_SOURCE_USB);
  case 87:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getActiveSrc == Enum_SOURCE_IPOD);
  case 88:
    return (VS_BOOL)(EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_USB_BROWSE_UPDATE_MUSIC_LIBRARY && getActiveSrc == Enum_SOURCE_USB || EventArgsVar.DB64.VS_UINT32Var[0] == Enum_LIST_ITEM_IPOD_BROWSE_UPDATE_MUSIC_LIBRARY && getActiveSrc == Enum_SOURCE_IPOD);
  case 89:
    return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && getActiveSrc == Enum_SOURCE_USB);
  case 90:
    return (VS_BOOL)(EventArgsVar.DB94.VS_UINT32Var[2] == LIST_ID_BROWSER_SONG || EventArgsVar.DB94.VS_UINT32Var[2] == LIST_ID_BROWSER_ALBUM_SONG || EventArgsVar.DB94.VS_UINT32Var[2] == LIST_ID_BROWSER_ARTIST_ALBUM_SONG || EventArgsVar.DB94.VS_UINT32Var[2] == LIST_ID_BROWSER_BOOKTITLE_CHAPTER || EventArgsVar.DB94.VS_UINT32Var[2] == LIST_ID_BROWSER_COMPOSER_ALBUM_SONG || EventArgsVar.DB94.VS_UINT32Var[2] == LIST_ID_BROWSER_GENRE_ARTIST_ALBUM_SONG || EventArgsVar.DB94.VS_UINT32Var[2] == LIST_ID_BROWSER_PLAYLIST_SONG || EventArgsVar.DB94.VS_UINT32Var[2] == LIST_ID_BROWSER_PODCAST_EPISODE || EventArgsVar.DB94.VS_UINT32Var[2] == LIST_ID_BROWSER_GENRE_ARTIST_SONG || EventArgsVar.DB94.VS_UINT32Var[2] == LIST_ID_BROWSER_GENRE_ALBUM_SONG || EventArgsVar.DB94.VS_UINT32Var[2] == LIST_ID_BROWSER_GENRE_SONG || EventArgsVar.DB94.VS_UINT32Var[2] == LIST_ID_BROWSER_ARTIST_SONG || EventArgsVar.DB94.VS_UINT32Var[2] == LIST_ID_BROWSER_COMPOSER_SONG);
  case 91:
    return (VS_BOOL)(EventArgsVar.DB94.VS_UINT32Var[2] != LIST_ID_BROWSER_SONG && EventArgsVar.DB94.VS_UINT32Var[2] != LIST_ID_BROWSER_ALBUM_SONG && EventArgsVar.DB94.VS_UINT32Var[2] != LIST_ID_BROWSER_ARTIST_ALBUM_SONG && EventArgsVar.DB94.VS_UINT32Var[2] != LIST_ID_BROWSER_BOOKTITLE_CHAPTER && EventArgsVar.DB94.VS_UINT32Var[2] != LIST_ID_BROWSER_COMPOSER_ALBUM_SONG && EventArgsVar.DB94.VS_UINT32Var[2] != LIST_ID_BROWSER_GENRE_ARTIST_ALBUM_SONG && EventArgsVar.DB94.VS_UINT32Var[2] != LIST_ID_BROWSER_PLAYLIST_SONG && EventArgsVar.DB94.VS_UINT32Var[2] != LIST_ID_BROWSER_PODCAST_EPISODE && EventArgsVar.DB94.VS_UINT32Var[2] != LIST_ID_BROWSER_GENRE_ARTIST_SONG && EventArgsVar.DB94.VS_UINT32Var[2] != LIST_ID_BROWSER_GENRE_ALBUM_SONG && EventArgsVar.DB94.VS_UINT32Var[2] != LIST_ID_BROWSER_GENRE_SONG && EventArgsVar.DB94.VS_UINT32Var[2] != LIST_ID_BROWSER_ARTIST_SONG && EventArgsVar.DB94.VS_UINT32Var[2] != LIST_ID_BROWSER_COMPOSER_SONG);
  }
  return (VS_BOOL)(EventArgsVar.DB74.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && getActiveSrc == Enum_SOURCE_IPOD);
}


/*
 * Action Expressions Wrapper Function.
 */
VS_VOID AppHmi_MediaStateMachine::VSAction (SEM_ACTION_EXPRESSION_TYPE i)
{
  switch (i)
  {
  case 0:
    Notify_Init_Finished();
    break;
  case 1:
    acDisclaimerActiveMsg();
    break;
  case 2:
    acDisclaimerInactiveMsg();
    break;
  case 3:
    acPerform_AutoLaunchConnectOffUpdMsg();
    break;
  case 4:
    acPerform_AutoLaunchConnectOnUpdMsg();
    break;
  case 6:
    acPerform_ClearContext();
    break;
  case 7:
    acPerform_DisclaimerButtonNo();
    break;
  case 8:
    acPerform_EncoderNegativeRotationUpdMsg();
    break;
  case 9:
    acPerform_EncoderPositiveRotationUpdMsg();
    break;
  case 10:
    acPerform_EncoderPressUpdMsg();
    break;
  case 11:
    acPerform_ExitBrowse();
    break;
  case 12:
    acPerform_FastForwardStartReqMsg();
    break;
  case 13:
    acPerform_FastForwardStopReqMsg();
    break;
  case 14:
    acPerform_FastRewindStartReqMsg();
    break;
  case 15:
    acPerform_FastRewindStopReqMsg();
    break;
  case 16:
    acPerform_FolderUpBtnPress();
    break;
  case 17:
    acPerform_HMISubStateON();
    break;
  case 18:
    acPerform_InitialiseFolderBrowser();
    break;
  case 19:
    acPerform_InitializeMediaParametersUpdMsg();
    break;
  case 20:
    acPerform_LaunchCarPlay();
    break;
  case 21:
    acPerform_MetaDataBrowserBackBtnPress();
    break;
  case 22:
    acPerform_PauseReqMsg();
    break;
  case 25:
    acPerform_PlayReqMsg();
    break;
  case 26:
    acPerform_QuickSearchBtnPressUpdMsg();
    break;
  case 27:
    acPerform_RandomReqMsg();
    break;
  case 28:
    acPerform_RepeatReqMsg();
    break;
  case 30:
    acPerform_SeekNextReqMsg();
    break;
  case 31:
    acPerform_SeekPrevReqMsg();
    break;
  case 32:
    acPerform_SendDipoPlayCommand();
    break;
  case 33:
    acPerform_SourceToggleReqMsg();
    break;
  case 36:
    acPerform_TAReqMsg();
    break;
  case 38:
    acPerform_onHK_AUX_CDEventMsg();
    break;
  case 39:
    acRememberUsrChoiceOnNoCarPlayMsg();
    break;
  case 55:
    gacScrollListDown();
    break;
  case 56:
    gacScrollListUp();
    break;
  case 57:
    gacScrollPageDown();
    break;
  case 58:
    gacScrollPageUp();
    break;
  case 60:
    gacWaitAnimationStartReq();
    break;
  case 61:
    gacWaitAnimationStopReq();
    break;
  case 62:
    IN_SwitchId = EventArgsVar.DB64.VS_UINT32Var[1];
    break;
  case 63:
    Cs_SelfContextId = EventArgsVar.DB64.VS_UINT32Var[1];
    break;
  case 64:
    IsSpiPhoneSourceActive = EventArgsVar.DB74.VS_INT8Var[0];
    break;
  case 65:
    TempContextStatus = EventArgsVar.DB71.VS_BOOLVar[0];
    break;
  case 66:
    screen_Type = Enum_OUTSIDE_BROWSE_SCREENS;
    break;
  case 67:
    getActiveSrc = Enum_SOURCE_AUX;
    break;
  case 68:
    getActiveSrc = Enum_SOURCE_USB;
    break;
  case 69:
    getActiveSrc = Enum_SOURCE_IPOD;
    break;
  case 70:
    getActiveSrc = Enum_SOURCE_BT;
    break;
  case 71:
    getActiveSrc = Enum_SOURCE_BT_NOT_CONNECTED;
    break;
  case 72:
    currentListId = LIST_ID_BROWSER_MAIN;
    break;
  case 73:
    isTempContextActive = EventArgsVar.DB71.VS_BOOLVar[0];
    break;
  case 74:
    IN_SwitchId = 0;
    break;
  case 75:
    screen_Type = Enum_INSDIE_BROWSE_SCREENS;
    break;
  case 76:
    getIsRootFolder = EventArgsVar.DB71.VS_BOOLVar[0];
    break;
  case 77:
    currentListId = EventArgsVar.DB64.VS_UINT32Var[0];
    break;
  case 78:
    getIsRootFolder = EventArgsVar.DB71.VS_BOOLVar[0];
    break;
  case 79:
    currentListId = LIST_ID_BROWSER_PLAYLIST;
    break;
  case 80:
    currentListId = LIST_ID_BROWSER_ARTIST;
    break;
  case 81:
    currentListId = LIST_ID_BROWSER_ALBUM;
    break;
  case 82:
    currentListId = LIST_ID_BROWSER_SONG;
    break;
  case 83:
    currentListId = LIST_ID_BROWSER_GENRE;
    break;
  case 84:
    currentListId = LIST_ID_BROWSER_COMPOSER;
    break;
  case 85:
    currentListId = LIST_ID_BROWSER_AUDIOBOOK;
    break;
  case 86:
    currentListId = LIST_ID_BROWSER_PODCAST;
    break;
  case 87:
    currentListId = EventArgsVar.DB64.VS_UINT32Var[0];
    break;
  case 88:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__LINEIN_MAIN);
    break;
  case 89:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__LINEIN_MAIN);
    break;
  case 90:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__LINEIN_MAIN);
    break;
  case 91:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 92:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 93:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 94:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 95:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 96:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 97:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 98:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 99:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 100:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 101:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 102:
    acPerform_MediaSpiStateActiveUpdMsg(false);
    break;
  case 103:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_REMEMBER_DISCLAIMER_SYSIND);
    break;
  case 104:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 105:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 106:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 107:
    gacViewDestroyReq(Global_Scenes_WAIT_SCENE);
    break;
  case 108:
    gacSetApplicationMode(Enum_APP_MODE_SOURCE);
    break;
  case 109:
    gacViewHideReq(Spi_Spi_Scene_INFO_CARPLAY_APP);
    break;
  case 110:
    gacViewDestroyReq(Spi_Spi_Scene_INFO_CARPLAY_APP);
    break;
  case 111:
    gacViewHideReq(Spi_Spi_Scene_SETTINGS__CARPLAY);
    break;
  case 112:
    gacViewDestroyReq(Spi_Spi_Scene_SETTINGS__CARPLAY);
    break;
  case 113:
    gacViewHideReq(Spi_Spi_Scene_CARPLAY_TUTORIAL);
    break;
  case 114:
    gacViewDestroyReq(Spi_Spi_Scene_CARPLAY_TUTORIAL);
    break;
  case 115:
    acPerform_ScreenHeaderReq(Spi_Spi_Scene_SETTINGS__CARPLAY);
    break;
  case 116:
    gacViewCreateReq(Spi_Spi_Scene_SETTINGS__CARPLAY);
    break;
  case 117:
    gacViewShowReq(Spi_Spi_Scene_SETTINGS__CARPLAY);
    break;
  case 118:
    acPerform_MediaSpiStateActiveUpdMsg(true);
    break;
  case 119:
    gacSetApplicationMode(Enum_APP_MODE_UTILITY);
    break;
  case 120:
    acPerform_ScreenHeaderReq(Spi_Spi_Scene_CARPLAY_TUTORIAL);
    break;
  case 121:
    gacViewCreateReq(Spi_Spi_Scene_CARPLAY_TUTORIAL);
    break;
  case 122:
    gacViewShowReq(Spi_Spi_Scene_CARPLAY_TUTORIAL);
    break;
  case 123:
    gacContextSwitchDoneRes(EventArgsVar.DB64.VS_UINT32Var[1]);
    break;
  case 124:
    gacContextSwitchDoneRes(EventArgsVar.DB64.VS_UINT32Var[1]);
    break;
  case 125:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 126:
    gacViewCreateReq(Spi_Spi_Scene_INFO_CARPLAY_APP);
    break;
  case 127:
    gacViewShowReq(Spi_Spi_Scene_INFO_CARPLAY_APP);
    break;
  case 128:
    gacContextSwitchCompleteRes(IN_SwitchId);
    break;
  case 129:
    gacContextSwitchCompleteRes(EventArgsVar.DB64.VS_UINT32Var[1]);
    break;
  case 130:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_HK_NEXT);
    break;
  case 131:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_HK_NEXT);
    break;
  case 132:
    acPerform_InterruptToMeterMsg(false, Enum_NEXT_SONG);
    break;
  case 133:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_HK_PREVIOUS);
    break;
  case 134:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_HK_PREVIOUS);
    break;
  case 135:
    acPerform_InterruptToMeterMsg(false, Enum_PREV_SONG);
    break;
  case 136:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_HK_NEXT);
    break;
  case 137:
    acPostBeep(Enum_hmibase_BEEPTYPE_ROGER);
    break;
  case 138:
    acPerform_InterruptToMeterMsg(false, Enum_FF_SONG);
    break;
  case 139:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_HK_NEXT);
    break;
  case 140:
    acPerform_InterruptToMeterMsg(false, Enum_FF_RW_STOP);
    break;
  case 141:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_HK_PREVIOUS);
    break;
  case 142:
    acPerform_InterruptToMeterMsg(false, Enum_RW_SONG);
    break;
  case 143:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_HK_PREVIOUS);
    break;
  case 144:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_SWC_NEXT);
    break;
  case 145:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_SWC_NEXT);
    break;
  case 146:
    acPerform_InterruptToMeterMsg(true, Enum_NEXT_SONG);
    break;
  case 147:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_SWC_PREV);
    break;
  case 148:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_SWC_PREV);
    break;
  case 149:
    acPerform_InterruptToMeterMsg(true, Enum_PREV_SONG);
    break;
  case 150:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_SWC_NEXT);
    break;
  case 151:
    acPerform_InterruptToMeterMsg(true, Enum_FF_SONG);
    break;
  case 152:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_SWC_NEXT);
    break;
  case 153:
    acPerform_InterruptToMeterMsg(true, Enum_FF_RW_STOP);
    break;
  case 154:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_SWC_PREV);
    break;
  case 155:
    acPerform_InterruptToMeterMsg(true, Enum_RW_SONG);
    break;
  case 156:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_SWC_PREV);
    break;
  case 157:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 158:
    gacContextSwitchCompleteRes(EventArgsVar.DB64.VS_UINT32Var[1]);
    break;
  case 159:
    acPerform_LaunchCarplayWithApp(Enum_DiPOAppType__DIPO_NOWPLAYING);
    break;
  case 160:
    acPerform_onAppStateUpdMsg(Enum_hmibase_IN_BACKGROUND);
    break;
  case 161:
    acPerform_onAppStateUpdMsg(Enum_hmibase_IN_FOREGROUND);
    break;
  case 162:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_SWC_PHONE);
    break;
  case 163:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_SWC_PHONE);
    break;
  case 164:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_SWC_PHONE);
    break;
  case 165:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_SWC_PHONE);
    break;
  case 166:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_SWC_PTT);
    break;
  case 167:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_SWC_PTT);
    break;
  case 168:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_SWC_PTT);
    break;
  case 169:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_SWC_PTT);
    break;
  case 170:
    acPerform_LaunchCarplayWithApp(Enum_DiPOAppType__DIPO_SIRI_BUTTONDOWN);
    break;
  case 171:
    acPerform_LaunchCarplayWithApp(Enum_DiPOAppType__DIPO_SIRI_BUTTONUP);
    break;
  case 172:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_REMEMBER_DISCLAIMER_SYSIND);
    break;
  case 173:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_HK_PHONE);
    break;
  case 174:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_HK_PHONE);
    break;
  case 175:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_HK_PHONE);
    break;
  case 176:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_HK_PHONE);
    break;
  case 177:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 178:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 179:
    acPostBeep(Enum_hmibase_BEEPTYPE_WARN);
    break;
  case 180:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 181:
    gacViewHideReq(Media_Scenes_MEDIA__CD_CDMP3_MAIN);
    break;
  case 182:
    gacViewDestroyReq(Media_Scenes_MEDIA__CD_CDMP3_MAIN);
    break;
  case 183:
    gacViewHideReq(Media_Scenes_MEDIA__AUX__USB_MAIN);
    break;
  case 184:
    gacViewDestroyReq(Media_Scenes_MEDIA__AUX__USB_MAIN);
    break;
  case 185:
    gacViewHideReq(Media_Scenes_MEDIA__AUX__IPOD_MAIN);
    break;
  case 186:
    gacViewDestroyReq(Media_Scenes_MEDIA__AUX__IPOD_MAIN);
    break;
  case 187:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__LINEIN_MAIN);
    break;
  case 188:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__LINEIN_MAIN);
    break;
  case 189:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT);
    break;
  case 190:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT);
    break;
  case 191:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 192:
    gacPopupSBCloseReq(Global_Scenes_WAIT_SCENE);
    break;
  case 193:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE);
    break;
  case 194:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE);
    break;
  case 195:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__BROWSER);
    break;
  case 196:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__BROWSER);
    break;
  case 197:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__METADATA_BROWSE);
    break;
  case 198:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__METADATA_BROWSE);
    break;
  case 199:
    gacViewHideReq(Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN);
    break;
  case 200:
    gacViewDestroyReq(Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN);
    break;
  case 201:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__POPUP_SOURCE_INITIALISING);
    break;
  case 202:
    gacViewHideReq(Media_Scenes_MEDIA__LOADING_MAIN);
    break;
  case 203:
    gacViewDestroyReq(Media_Scenes_MEDIA__LOADING_MAIN);
    break;
  case 204:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA__AUX__USB_MAIN);
    break;
  case 205:
    gacViewCreateReq(Media_Scenes_MEDIA__AUX__USB_MAIN);
    break;
  case 206:
    gacViewShowReq(Media_Scenes_MEDIA__AUX__USB_MAIN);
    break;
  case 207:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA__AUX__IPOD_MAIN);
    break;
  case 208:
    gacViewCreateReq(Media_Scenes_MEDIA__AUX__IPOD_MAIN);
    break;
  case 209:
    gacViewShowReq(Media_Scenes_MEDIA__AUX__IPOD_MAIN);
    break;
  case 210:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT);
    break;
  case 211:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT);
    break;
  case 212:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT);
    break;
  case 213:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN);
    break;
  case 214:
    gacViewCreateReq(Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN);
    break;
  case 215:
    gacViewShowReq(Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN);
    break;
  case 216:
    gacViewCreateReq(Media_Scenes_MEDIA__LOADING_MAIN);
    break;
  case 217:
    gacViewShowReq(Media_Scenes_MEDIA__LOADING_MAIN);
    break;
  case 218:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA__LOADING_MAIN);
    break;
  case 219:
    gacContextSwitchDoneRes(1);
    break;
  case 220:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__POPUP_SOURCE_INITIALISING);
    break;
  case 221:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 222:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 223:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 224:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 225:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 226:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 227:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 228:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 229:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 230:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 231:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 232:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 233:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 234:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 235:
    gacViewShowReq(MediaPopups_Popups_MEDIA__POPUP_SOURCE_INITIALISING);
    break;
  case 236:
    gacViewHideReq(MediaPopups_Popups_MEDIA__POPUP_SOURCE_INITIALISING);
    break;
  case 237:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__POPUP_SOURCE_INITIALISING);
    break;
  case 238:
    gacViewShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 239:
    gacViewHideReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 240:
    gacViewClearReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 241:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND, 6000);
    break;
  case 242:
    gacViewShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 243:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 244:
    gacViewHideReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 245:
    gacViewClearReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 246:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND, 3000);
    break;
  case 247:
    gacViewShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 248:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 249:
    gacViewHideReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 250:
    gacViewClearReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 251:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 252:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND, 6000);
    break;
  case 253:
    gacViewShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 254:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 255:
    gacViewHideReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 256:
    gacViewClearReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 257:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 258:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND, 6000);
    break;
  case 259:
    gacViewShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 260:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 261:
    gacViewHideReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 262:
    gacViewClearReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 263:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 264:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND, 6000);
    break;
  case 265:
    gacViewShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 266:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 267:
    gacViewHideReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 268:
    gacViewClearReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 269:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND, 6000);
    break;
  case 270:
    gacViewShowReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 271:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 272:
    gacViewHideReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 273:
    gacViewClearReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 274:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 275:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND, 6000);
    break;
  case 276:
    gacViewShowReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 277:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 278:
    gacViewHideReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 279:
    gacViewClearReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 280:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 281:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE, 6000);
    break;
  case 282:
    gacViewShowReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 283:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 284:
    gacViewHideReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 285:
    gacViewClearReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 286:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 287:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH, 30000);
    break;
  case 288:
    gacViewShowReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 289:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 290:
    gacViewHideReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 291:
    gacViewClearReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 292:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND, 6000);
    break;
  case 293:
    gacViewShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 294:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 295:
    gacViewHideReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 296:
    gacViewClearReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 297:
    acPerform_onHK_BackUpdMsg(Enum_enPress);
    break;
  case 298:
    acPerform_onHK_BackUpdMsg(Enum_enRelease);
    break;
  case 299:
    acPerform_onHK_BackUpdMsg(Enum_enLongPress);
    break;
  case 300:
    acPerform_onHK_BackUpdMsg(Enum_enLongPressRelease);
    break;
  case 301:
    acPerform_EncoderRotatonUpdMsg(EventArgsVar.DB74.VS_INT8Var[0]);
    break;
  case 302:
    acPerform_EncoderPressedUpdMsg(Enum_enPress);
    break;
  case 303:
    acPerform_EncoderPressedUpdMsg(Enum_enRelease);
    break;
  case 304:
    acPerform_EncoderPressedUpdMsg(Enum_enLongPress);
    break;
  case 305:
    acPerform_EncoderPressedUpdMsg(Enum_enLongPressRelease);
    break;
  case 306:
    gacContextSwitchBackRes(IN_SwitchId);
    break;
  case 307:
    gacPopupCreateAndSBShowReq(Global_Scenes_WAIT_SCENE);
    break;
  case 308:
    gacViewShowReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_REMEMBER_DISCLAIMER_SYSIND);
    break;
  case 309:
    gacViewHideReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_REMEMBER_DISCLAIMER_SYSIND);
    break;
  case 310:
    gacViewClearReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_REMEMBER_DISCLAIMER_SYSIND);
    break;
  case 311:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 312:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 313:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 314:
    gacViewShowReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 315:
    gacViewHideReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 316:
    gacViewClearReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 317:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 318:
    gacViewShowReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 319:
    gacViewHideReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 320:
    gacViewClearReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 321:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO_POPUP_CARPLAY_REMEMBER_DISCLAIMER_SYSIND);
    break;
  case 322:
    gacViewShowReq(Global_Scenes_WAIT_SCENE);
    break;
  case 323:
    gacRegisterForCloseOnTouchSession(Global_Scenes_WAIT_SCENE);
    break;
  case 324:
    gacDeregisterForCloseOnTouchSessionReq(Global_Scenes_WAIT_SCENE);
    break;
  case 325:
    gacViewHideReq(Global_Scenes_WAIT_SCENE);
    break;
  case 326:
    gacViewClearReq(Global_Scenes_WAIT_SCENE);
    break;
  case 327:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__METADATA_BROWSE);
    break;
  case 328:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__METADATA_BROWSE);
    break;
  case 329:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__METADATA_BROWSE);
    break;
  case 330:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__BROWSER);
    break;
  case 331:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__BROWSER);
    break;
  case 332:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__BROWSER);
    break;
  case 333:
    gacContextSwitchOutReq(Enum_PHONE_CONTEXT_BT_SETTINGS_FROM_MEDIA, Enum_DEFAULT_CONTEXT_BACK, Enum_APPID_APPHMI_PHONE);
    break;
  case 334:
    acPostBeep(Enum_hmibase_BEEPTYPE_CLICK);
    break;
  case 335:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 336:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE);
    break;
  case 337:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE);
    break;
  case 338:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE);
    break;
  case 339:
    acPerform_FolderBrowserBtnPress(EventArgsVar.DB64.VS_UINT32Var[0]);
    break;
  case 340:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB64.VS_UINT32Var[0]);
    break;
  case 341:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB64.VS_UINT32Var[0]);
    break;
  case 342:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB64.VS_UINT32Var[0]);
    break;
  case 343:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB64.VS_UINT32Var[0]);
    break;
  case 344:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB64.VS_UINT32Var[0]);
    break;
  case 345:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB64.VS_UINT32Var[0]);
    break;
  case 346:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB64.VS_UINT32Var[0]);
    break;
  case 347:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB64.VS_UINT32Var[0]);
    break;
  case 348:
    acPerform_MetaDataBrowserBtnPress(PLAY_SONG_IN_LIST, EventArgsVar.DB94.VS_UINT32Var[1]);
    break;
  case 349:
    acPerform_MetaDataBrowserBtnPress(EventArgsVar.DB94.VS_UINT32Var[0], EventArgsVar.DB94.VS_UINT32Var[1]);
    break;

  default:
    break;
  }
}
