/*
 * Id:        AppHmi_MediaStateMachineSEMLibB.cpp
 *
 * Function:  Contains all API functions.
 *
 * Generated: Thu May 12 16:27:29 2016
 *
 * Coder 7, 3, 2, 2426
 * 
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include "AppHmi_MediaStateMachineSEMLibB.h"


/*
 * Conditional Compilation Definitions for the API only.
 */
#define SEM_RDHW_TYPE_1                    0
#define SEM_RDHW_TYPE_2                    0
#define SEM_RDHW_TYPE_3                    1
#define SEM_RDHW_WIDTH_16_BIT              0
#define SEM_RDHW_WIDTH_24_BIT              0
#define SEM_RDHW_WIDTH_32_BIT              1
#define SEM_RDHW_WIDTH_48_BIT              0
#define SEM_RDHW_WIDTH_64_BIT              0
#define SEM_RD_WIDTH_8_BIT                 0
#define SEM_RD_WIDTH_16_BIT                1
#define SEM_RD_WIDTH_32_BIT                0
#define SEM_RDFM_NUMBER                    5
#define SEM_EVENT_GROUP_INDEX              1
#define SEM_EVENT_GROUP_TABLE_INDEX        0
#define SEM_SIGNAL_QUEUE_ERROR_IF_FULL     1
#define SEM_SIGNAL_QUEUE_NO_ERROR_IF_FULL  0
#define SEM_RMN_ACTIONS                    15
#define SEM_RMN_GUARDS                     1
#define SEM_RMN_NEGATIVE_STATE_SYNCS       2
#define SEM_RMN_NEXT_STATES                15
#define SEM_RMN_POSITIVE_STATE_SYNCS       2
#define SEM_RMN_SIGNALS                    1
#define SEM_SIGNAL                         1
#define SEM_EXPL                           0
#define SEM_EXPL_ABS                       0
#define SEM_FORCE_STATE                    0
#define SEM_GET_OUTPUT_ALL                 0
#define SEM_INIT_ALL                       1
#define SEM_MACHINE                        0
#define SEM_NAME                           0
#define SEM_NAME_ABS                       1
#define SEM_NEXT_STATE_CHG                 1
#define SEM_SIGNAL_QUEUE_INFO              0
#define SEM_STATE                          1
#define SEM_STATE_ALL                      1
#define SEM_INIT_EXTERNAL_VARIABLES        0
#define SEM_INIT_INTERNAL_VARIABLES        1
#define VS_ACTION_EXPLS                    0
#define VS_ACTION_FUNCTION_NAMES           1
#define VS_EVENT_EXPLS                     0
#define VS_EVENT_NAMES                     1
#define VS_STATE_EXPLS                     0
#define VS_STATE_NAMES                     1


#ifdef VS_RUNTIME_INFO
AppHmi_MediaStateMachineVSRunTimeInfo volatile const VS_RUNTIME_INFO_EXTKW AppHmi_MediaStateMachinevsRunTimeInfo = 
{
  VS_SIGNATURE_VERSION,
  VS_SIGNATURE_CONTENT
};
#endif


const SEM_STATE_TYPE AppHmi_MediaStateMachine::StateUndefined = ((SEM_STATE_TYPE)-1L);

const SEM_EVENT_TYPE AppHmi_MediaStateMachine::EventUndefined = ((SEM_EVENT_TYPE)-1L);

const SEM_EVENT_TYPE AppHmi_MediaStateMachine::EventGroupUndefined = 0xFFu;

const SEM_EVENT_TYPE AppHmi_MediaStateMachine::EventTerminationId = ((SEM_EVENT_TYPE)-1L);

const SEM_ACTION_EXPRESSION_TYPE AppHmi_MediaStateMachine::ActionExpressionTerminationId = ((SEM_ACTION_EXPRESSION_TYPE)-1L);


#if (SEM_SIGNAL)
void AppHmi_MediaStateMachine::SEM_InitSignalQueue (void)
{
  SEM.SPut = 0u;
  SEM.SGet = 0u;
  SEM.SUsed = 0u;
}


unsigned char AppHmi_MediaStateMachine::SEM_SignalQueuePut (SEM_EVENT_TYPE SignalNo)
{
  unsigned char result;

  if (SEM.SUsed == 5u)
  {
    result = SES_SIGNAL_QUEUE_FULL;
  }
  else
  {
    SEM.SUsed++;
    SEM.SQueue[SEM.SPut] = SignalNo;
    if (++SEM.SPut == 5u)
    {
      SEM.SPut = 0u;
    }
    result = SES_OKAY;
  }
  return (result);
}


SEM_EVENT_TYPE AppHmi_MediaStateMachine::SEM_SignalQueueGet (void)
{
  SEM_EVENT_TYPE SignalNo = EventUndefined;

  if (SEM.SUsed)
  {
    SEM.SUsed--;
    SignalNo = SEM.SQueue[SEM.SGet];
    if (++SEM.SGet == 5u)
    {
      SEM.SGet = 0u;
    }
  }
  return (SignalNo);
}


#if (SEM_SIGNAL_QUEUE_INFO == 1)
void AppHmi_MediaStateMachine::SEM_SignalQueueInfo (SEM_SIGNAL_QUEUE_TYPE *NofSignals)
{
  *NofSignals = SEM.SUsed;
}
#endif
#endif


#if (SEM_INIT_ALL)
void AppHmi_MediaStateMachine::SEM_InitAll (void)
{
  SEM_Init();
#if (SEM_INIT_EXTERNAL_VARIABLES)
  SEM_InitExternalVariables();
#endif
#if (SEM_INIT_INTERNAL_VARIABLES)
  SEM_InitInternalVariables();
#endif
#if (SEM_SIGNAL)
  SEM_InitSignalQueue();
#endif
}
#endif


void AppHmi_MediaStateMachine::SEM_Init (void)
{
#ifdef VS_RUNTIME_INFO
  *AppHmi_MediaStateMachinevsRunTimeInfo.pSignatureVersion;
#endif
#if (VS_NOF_STATE_MACHINES != 0u)
  {
    SEM_STATE_MACHINE_TYPE i;
    for (i = 0u; i < VS_NOF_STATE_MACHINES; i++)
    {
      SEM.WSV[i] = StateUndefined;
      SEM.CSV[i] = StateUndefined;
    }
  }
#if (SEM_NEXT_STATE_CHG == 1)
  SEM.Chg = 0;
#endif
#endif
  SEM.State = STATE_SEM_INITIALIZED;
}


void AppHmi_MediaStateMachine::DeductChangeState (VS_VOID)
{
  SEM_STATE_MACHINE_TYPE i;
  for (i = 0u; i < VS_NOF_STATE_MACHINES; i++)
  {
    if (SEM.WSV[i] != StateUndefined)
    {
#if (SEM_NEXT_STATE_CHG == 1)
      if (SEM.CSV[i] != SEM.WSV[i])
      {
        SEM.Chg = 1;
      }
#endif
      SEM.CSV[i] = SEM.WSV[i];
      SEM.WSV[i] = StateUndefined;
    }
  }
}


unsigned char AppHmi_MediaStateMachine::SEM_GetOutput (SEM_ACTION_EXPRESSION_TYPE *ActionNo)
{
  for(;;)
  {
    switch (SEM.State)
    {
    case STATE_SEM_PREPARE:
      switch (SEM.DIt)
      {
      case kDIInitialTriggerLess:
        SEM.AnyReactions = 0;
        BufferVariables(1/*VS_TRIGGERLESS_TRIGGER*/);
        SEM.DIt = kDIInitialTriggerLessLoop;
        break;

      case kDIInitialTriggerLessLoop:
        if (SEM.AnyReactions == 1)
        {
          DeductChangeState();
          SEM.AnyReactions = 0;
          BufferVariables(1/*VS_TRIGGERLESS_TRIGGER*/);
        }
        else
        {
          SEM.EventNo = SEM.InitialEventNo;
          BufferVariables(SEM.EventNo);
          DeductChangeState();
          SEM.DIt = kDIFirstEventGroup;
        }
        break;

      case kDIOriginalEvent:
        SEM.DIt = kDIFirstEventGroup;
        break;

      case kDIFirstEventGroup:
        SEM.EventNo = VS.AppHmi_MediaStateMachineEventGroupIndex[SEM.EventNo];
        if (SEM.EventNo == EventGroupUndefined)
        {
          SEM.DIt = kDITriggerLessLoop;
          SEM.AnyReactions = 0;
          SEM.EventNo = 1; /*VS_TRIGGERLESS_TRIGGER*/
          BufferVariables(1);
          DeductChangeState();
        }
        else
        {
          SEM.DIt = kDIRemainingEventGroup;
          SEM.EventNo += VS_NOF_EVENTS;
        }
        break;

      case kDIRemainingEventGroup:
        SEM.DIt = kDITriggerLessLoop;
        SEM.AnyReactions = 0;
        SEM.EventNo = 1; /*VS_TRIGGERLESS_TRIGGER*/
        BufferVariables(1);
        DeductChangeState();
        break;

      case kDITriggerLessLoop:
        if (SEM.AnyReactions == 1)
        {
          DeductChangeState();
          SEM.AnyReactions = 0;
          BufferVariables(1/*VS_TRIGGERLESS_TRIGGER*/);
        }
        else
        {
          SEM.EventNo = SEM_SignalQueueGet();
          if (SEM.EventNo == EventUndefined)
          {
            SEM.State = STATE_SEM_OKAY;
            return (SES_OKAY);
          }
          SEM.DIt = kDITreatingSignal;
          BufferVariables(SEM.EventNo);
          DeductChangeState();
        }
        break;

      case kDITakeSignalOut:
        SEM.EventNo = SEM_SignalQueueGet();
        if (SEM.EventNo == EventUndefined)
        {
          SEM.State = STATE_SEM_OKAY;
          return (SES_OKAY);
        }
        SEM.DIt = kDITreatingSignal;
        BufferVariables(SEM.EventNo);
        DeductChangeState();
        break;

      case kDITreatingSignal:
        SEM.DIt = kDITriggerLessLoop;
        SEM.AnyReactions = 0;
        SEM.EventNo = 1; /*VS_TRIGGERLESS_TRIGGER*/
        BufferVariables(1);
        DeductChangeState();
        break;

      }

      SEM.iFirstR = VS.AppHmi_MediaStateMachineRuleTableIndex[SEM.EventNo];
      SEM.iLastR = VS.AppHmi_MediaStateMachineRuleTableIndex[SEM.EventNo + 1];
      SEM.State = STATE_SEM_CONSULT;
      /* fall through */

    case STATE_SEM_CONSULT:
      while (SEM.iFirstR < SEM.iLastR)
      {
        SEM_INTERNAL_TYPE i;
        VS_UINT8 nNo;
        VS_UINT8 nPos;
        VS_UINT8 nNxt;
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        VS_UINT8 nNeg;
#endif
#if (SEM_RMN_SIGNALS)
        VS_UINT8 nSignal;
#endif
#if (SEM_RMN_GUARDS)
        VS_UINT8 nGuard;
#endif
        SEM_RULE_INDEX_TYPE iRI;

        iRI = VS.AppHmi_MediaStateMachineRuleIndex[SEM.iFirstR++];
#if (SEM_RD_WIDTH_8_BIT && SEM_RDHW_TYPE_1 && SEM_RDHW_WIDTH_16_BIT)
        i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
        nNxt = (unsigned char)(i & 0x0Fu);
        SEM.nAction = (unsigned char)(i >> 4u);
        i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
        nPos = (unsigned char)(i & 0x0Fu);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)(i >> 4u);
#endif
#endif
#if (SEM_RD_WIDTH_8_BIT && SEM_RDHW_TYPE_2 && SEM_RDHW_WIDTH_24_BIT)
        i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
        nPos = (unsigned char)(i & 0x0Fu);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)(i >> 4u);
#endif
        i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
#if (SEM_RMN_GUARDS)
        nGuard = (unsigned char)(i & 0x0Fu);
#endif
        nNxt = (unsigned char)(i >> 4u);
        i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
        SEM.nAction = (unsigned char)(i & 0x0Fu);
#if (SEM_RMN_SIGNALS)
        nSignal = (unsigned char)(i >> 4u);
#endif
#endif
#if (SEM_RD_WIDTH_8_BIT && SEM_RDHW_TYPE_1 && SEM_RDHW_WIDTH_32_BIT)
        SEM.nAction = (unsigned char)VS.AppHmi_MediaStateMachineRuleData[iRI++];
        nNxt = (unsigned char)VS.AppHmi_MediaStateMachineRuleData[iRI++];
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)VS.AppHmi_MediaStateMachineRuleData[iRI++];
#else
        iRI++;
#endif
        nPos = (unsigned char)VS.AppHmi_MediaStateMachineRuleData[iRI++];
#endif
#if (SEM_RD_WIDTH_8_BIT && SEM_RDHW_TYPE_2 && SEM_RDHW_WIDTH_48_BIT)
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)VS.AppHmi_MediaStateMachineRuleData[iRI++];
#else
        iRI++;
#endif
        nPos = (unsigned char)VS.AppHmi_MediaStateMachineRuleData[iRI++];
        nNxt = (unsigned char)VS.AppHmi_MediaStateMachineRuleData[iRI++];
#if (SEM_RMN_GUARDS)
        nGuard = (unsigned char)VS.AppHmi_MediaStateMachineRuleData[iRI];
#endif
        iRI++;
#if (SEM_RMN_SIGNALS)
        nSignal = (unsigned char)VS.AppHmi_MediaStateMachineRuleData[iRI];
#endif
        iRI++;
        SEM.nAction = (unsigned char)VS.AppHmi_MediaStateMachineRuleData[iRI++];
#endif
#if (SEM_RD_WIDTH_16_BIT && SEM_RDHW_TYPE_1 && SEM_RDHW_WIDTH_16_BIT)
        i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
        nPos = (unsigned char)(i & 0x0Fu);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)((i >> 4u) & 0x0Fu);
#endif
        nNxt = (unsigned char)((i >> 8u) & 0x0Fu);
        SEM.nAction = (unsigned char)((i >> 12u) & 0x0Fu);
#endif
#if (SEM_RD_WIDTH_16_BIT && SEM_RDHW_TYPE_3 && SEM_RDHW_WIDTH_32_BIT)
        i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
        nPos = (unsigned char)(i & 0x0Fu);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)((i >> 4u) & 0x0Fu);
#endif
#if (SEM_RMN_GUARDS)
        nGuard = (unsigned char)((i >> 8u) & 0x0Fu);
#endif
        nNxt = (unsigned char)(i >> 12u);
        i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
        SEM.nAction = (unsigned char)(i & 0x0Fu);
#if (SEM_RMN_SIGNALS)
        nSignal = (unsigned char)((i >> 4u) & 0x0Fu);
#endif
#endif
#if (SEM_RD_WIDTH_16_BIT && SEM_RDHW_TYPE_1 && SEM_RDHW_WIDTH_32_BIT)
        i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
        nNxt = (unsigned char)(i & 0x0FFu);
        SEM.nAction = (unsigned char)(i >> 8u);
        i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
        nPos = (unsigned char)(i & 0x0FFu);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)(i >> 8u);
#endif
#endif
#if (SEM_RD_WIDTH_16_BIT && SEM_RDHW_TYPE_2 && SEM_RDHW_WIDTH_48_BIT)
        i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
        nPos = (unsigned char)(i & 0x0FFu);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)(i >> 8u);
#endif
        i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
#if (SEM_RMN_GUARDS)
        nGuard = (unsigned char)(i & 0x0FFu);
#endif
        nNxt = (unsigned char)(i >> 8u);
        i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
        SEM.nAction = (unsigned char)(i & 0x0FFu);
#if (SEM_RMN_SIGNALS)
        nSignal = (unsigned char)(i >> 8u);
#endif
#endif
#if (SEM_RD_WIDTH_32_BIT && SEM_RDHW_TYPE_1 && SEM_RDHW_WIDTH_32_BIT)
        i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
        nPos = (unsigned char)(i & 0X0FFu);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)((i >> 8u) & 0X0FFu);
#endif
        nNxt = (unsigned char)((i >> 16u) & 0x0FFu);
        SEM.nAction = (unsigned char)((i >> 24u) & 0x0FFu);
#endif
#if (SEM_RD_WIDTH_32_BIT && SEM_RDHW_TYPE_3 && SEM_RDHW_WIDTH_64_BIT)
        i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
        nPos = (unsigned char)(i & 0x0FFu);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)((i >> 8u) & 0x0FFu);
#endif
#if (SEM_RMN_GUARDS)
        nGuard = (unsigned char)((i >> 16u) & 0x0FFu);
#endif
        nNxt = (unsigned char)(i >> 24u);
        i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
        SEM.nAction = (unsigned char)(i & 0x0FFu);
#if (SEM_RMN_SIGNALS)
        nSignal = (unsigned char)((i >> 8u) & 0x0FFu);
#endif
#endif
#if (VS_NOF_STATE_MACHINES != 0u)
        for (nNo = 0 ; nNo < nPos; nNo++)
        {
          SEM_STATE_TYPE sa;
          sa = (SEM_STATE_TYPE) VS.AppHmi_MediaStateMachineRuleData[iRI++];
          if (sa != SEM.CSV[VS.AppHmi_MediaStateMachineStateMachineIndex[sa]])
          {
            goto NextRule;
          }
        }
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        for (nNo = 0; nNo < nNeg; nNo++)
        {
          SEM_STATE_TYPE sa;
          i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
          sa = SEM.CSV[VS.AppHmi_MediaStateMachineStateMachineIndex[i]];
          if ((sa == StateUndefined) || (sa == (SEM_STATE_TYPE) i))
          {
            goto NextRule;
          }
        }
#endif
#endif
#if (SEM_RMN_GUARDS)
        if (nGuard)
        {
          for (nNo = 0; nNo < nGuard; nNo++)
          {
            if (VSGuard(VS.AppHmi_MediaStateMachineRuleData[iRI++]) == VS_FALSE)
            {
              goto NextRule;
            }
          }
        }
#endif

#if (VS_NOF_STATE_MACHINES != 0u)
        SEM.AnyReactions = 1;
        for (nNo = 0; nNo < nNxt; nNo++)
        {
          SEM_STATE_TYPE sa;
          sa = (SEM_STATE_TYPE) VS.AppHmi_MediaStateMachineRuleData[iRI++];
          i = VS.AppHmi_MediaStateMachineStateMachineIndex[sa];
          if (SEM.WSV[i] == StateUndefined)
          {
            SEM.WSV[i] = sa;
          }
          else if (SEM.WSV[i] != sa)
          {
            SEM._iRI = iRI;
            return (SES_CONTRADICTION);
          }
        }
#endif
#if (SEM_RMN_SIGNALS)
        if (nSignal)
        {
          SEM.AnyReactions = 1;
          for (nNo = 0; nNo < nSignal; nNo++)
          {
            i = VS.AppHmi_MediaStateMachineRuleData[iRI++];
#if (SEM_SIGNAL_QUEUE_ERROR_IF_FULL)
            if (SEM_SignalQueuePut((SEM_EVENT_TYPE)i) == SES_SIGNAL_QUEUE_FULL)
            {
              SEM._iRI = iRI;
              return (SES_SIGNAL_QUEUE_FULL);
            }
#endif
#if (SEM_SIGNAL_QUEUE_NO_ERROR_IF_FULL)
            SEM_SignalQueuePut((SEM_EVENT_TYPE)i);
#endif
          }
        }
#endif
        if (SEM.nAction)
        {
          SEM.AnyReactions = 1;
          *ActionNo = (SEM_ACTION_EXPRESSION_TYPE)VS.AppHmi_MediaStateMachineRuleData[iRI];
          if (SEM.nAction > 1)
          {
            iRI++;
            SEM._iRI = iRI;
            SEM.nAction--;
            SEM.State = STATE_SEM_OUTPUT;
          }
          return (SES_FOUND);
        }
NextRule:
        ;
      }
      SEM.State = STATE_SEM_PREPARE;
      break;

    case STATE_SEM_OUTPUT:
      if (SEM.nAction)
      {
        *ActionNo = (SEM_ACTION_EXPRESSION_TYPE) VS.AppHmi_MediaStateMachineRuleData[SEM._iRI++];
        SEM.nAction--;
        return (SES_FOUND);
      }
      SEM.State = STATE_SEM_CONSULT;
      break;

    case STATE_SEM_OKAY:
      return (SES_OKAY);

    default:
      return (SES_EMPTY);
    }
  }
}


#if (SEM_GET_OUTPUT_ALL == 1)
#if (SEM_SIGNAL)
#error SEM_GetOutputAll cannot be used when the Project contains signals.
#endif
unsigned char AppHmi_MediaStateMachine::SEM_GetOutputAll (SEM_ACTION_EXPRESSION_TYPE *ActionVector,
  SEM_ACTION_EXPRESSION_TYPE MaxSize)
{
  SEM_ACTION_EXPRESSION_TYPE i;
  unsigned char CC;

  for (i = 0; ((CC = SEM_GetOutput(&ActionVector[i])) == SES_FOUND) && i < MaxSize - 1; i++);

  if (CC == SES_OKAY)
  {
    ActionVector[i] = ActionExpressionTerminationId;
    return (SES_OKAY);
  }
  if (CC == SES_FOUND)
  {
    return (SES_BUFFER_OVERFLOW);
  }
  return (CC);
}
#endif


#if (SEM_NEXT_STATE_CHG == 1)
unsigned char AppHmi_MediaStateMachine::SEM_NextStateChg (void)
#else
unsigned char AppHmi_MediaStateMachine::SEM_NextState (void)
#endif
{
  unsigned char CC;
  SEM_ACTION_EXPRESSION_TYPE nAction;

  if (SEM.State != STATE_SEM_OKAY)
  {
    while ((CC = SEM_GetOutput (&nAction)) == SES_FOUND)
    {
      ;
    }
    if (CC != SES_OKAY)
    {
      return(CC);
    }
  }
  DeductChangeState();
  SEM.State = STATE_SEM_INITIALIZED;

#if (SEM_NEXT_STATE_CHG == 1)
  if (SEM.Chg)
  {
    SEM.Chg = 0;
    return (SES_FOUND);
  }
  return (SES_OKAY);
#else
  return (SES_OKAY);
#endif
}


#if (SEM_NAME == 1)
unsigned char AppHmi_MediaStateMachine::SEM_Name (unsigned char IdentType,
  SEM_EXPLANATION_TYPE IdentNo, char *Text, unsigned short MaxSize) const
{
  char c, *s;
  unsigned short i;

  if (!MaxSize)
  {
    return (SES_TEXT_TOO_LONG);
  }
  switch (IdentType)
  {
#if (VS_EVENT_NAMES != 0)
  case EVENT_TYPE:
    if (VS_NOF_EVENTS <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    s = (char *)VS.AppHmi_MediaStateMachineEventNames + VS.AppHmi_MediaStateMachineEventNamesIndex[IdentNo];
    break;
#endif
#if ((VS_STATE_NAMES != 0) && (VS_NOF_STATES != 0u))
  case STATE_TYPE:
    if (VS_NOF_STATES <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    s = (char *)VS.AppHmi_MediaStateMachineStateNames + VS.AppHmi_MediaStateMachineStateNamesIndex[IdentNo];
    break;
#endif
#if ((VS_ACTION_FUNCTION_NAMES != 0) && (VS_NOF_ACTION_FUNCTIONS != 0u))
  case ACTION_TYPE:
    if (VS_NOF_ACTION_FUNCTIONS <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    s = (char *)VS.AppHmi_MediaStateMachineActionNames + VS.AppHmi_MediaStateMachineActionNamesIndex[IdentNo];
    break;
#endif
  default:
    return (SES_TYPE_ERR);
  }
  for (i = 0; i < MaxSize; i++)
  {
    c = *s++;
    *Text++ = c;
    if (c == '\0')
    {
      return (SES_OKAY);
    }
  }
  Text--;
  *Text = '\0';
  return (SES_TEXT_TOO_LONG);
}
#endif


#if (SEM_NAME_ABS == 1)
unsigned char AppHmi_MediaStateMachine::SEM_NameAbs (unsigned char IdentType,
  SEM_EXPLANATION_TYPE IdentNo, char const **Text) const
{
  switch (IdentType)
  {
#if (VS_EVENT_NAMES != 0)
  case EVENT_TYPE:
    if (VS_NOF_EVENTS <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    *Text = (char const *)&VS.AppHmi_MediaStateMachineEventNames + VS.AppHmi_MediaStateMachineEventNamesIndex[IdentNo];
    break;
#endif
#if ((VS_STATE_NAMES != 0) && (VS_NOF_STATES != 0u))
  case STATE_TYPE:
    if (VS_NOF_STATES <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    *Text = (char const *)&VS.AppHmi_MediaStateMachineStateNames + VS.AppHmi_MediaStateMachineStateNamesIndex[IdentNo];
    break;
#endif
#if ((VS_ACTION_FUNCTION_NAMES != 0) && (VS_NOF_ACTION_FUNCTIONS != 0u))
  case ACTION_TYPE:
    if (VS_NOF_ACTION_FUNCTIONS <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    *Text = (char const *)&VS.AppHmi_MediaStateMachineActionNames + VS.AppHmi_MediaStateMachineActionNamesIndex[IdentNo];
    break;
#endif
  default:
    return (SES_TYPE_ERR);
  }
  return (SES_OKAY);
}
#endif


#if (SEM_EXPL == 1)
unsigned char AppHmi_MediaStateMachine::SEM_Expl (unsigned char IdentType, SEM_EXPLANATION_TYPE IdentNo,
  char *Text, unsigned short MaxSize) const
{
  char c, *s;
  unsigned short i;

  if (!MaxSize)
  {
    return (SES_TEXT_TOO_LONG);
  }
  switch (IdentType)
  {
#if (VS_EVENT_EXPLS != 0)
  case EVENT_TYPE:
    if (VS_NOF_EVENTS <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    s = (char *)VS.AppHmi_MediaStateMachineEventExpls + VS.AppHmi_MediaStateMachineEventExplsIndex[IdentNo];
    break;
#endif
#if ((VS_STATE_EXPLS != 0) && (VS_NOF_STATES != 0u))
  case STATE_TYPE:
    if (VS_NOF_STATES <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    s = (char *)VS.AppHmi_MediaStateMachineStateExpls + VS.AppHmi_MediaStateMachineStateExplsIndex[IdentNo];
    break;
#endif
#if ((VS_ACTION_EXPLS != 0) && (VS_NOF_ACTION_FUNCTIONS != 0u))
  case ACTION_TYPE:
    if (VS_NOF_ACTION_FUNCTIONS <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    s = (char *)VS.AppHmi_MediaStateMachineActionExpls + VS.AppHmi_MediaStateMachineActionExplsIndex[IdentNo];
    break;
#endif
  default:
    return (SES_TYPE_ERR);
  }

  for (i = 0; i < MaxSize; i++)
  {
    c = *s++;
    *Text++ = c;
    if (c == '\0')
    {
      return (SES_OKAY);
    }
  }
  Text--;
  *Text = '\0';
  return (SES_TEXT_TOO_LONG);
}
#endif


#if (SEM_EXPL_ABS == 1)
unsigned char AppHmi_MediaStateMachine::SEM_ExplAbs (unsigned char IdentType,
  SEM_EXPLANATION_TYPE IdentNo, char const **Text) const
{
  switch (IdentType)
  {
#if (VS_EVENT_EXPLS != 0)
  case EVENT_TYPE:
    if (VS_NOF_EVENTS <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    *Text = (char const *)&VS.AppHmi_MediaStateMachineEventExpls + VS.AppHmi_MediaStateMachineEventExplsIndex[IdentNo];
    break;
#endif
#if ((VS_STATE_EXPLS != 0) && (VS_NOF_STATES != 0u))
  case STATE_TYPE:
    if (VS_NOF_STATES <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    *Text = (char const *)&VS.AppHmi_MediaStateMachineStateExpls + VS.AppHmi_MediaStateMachineStateExplsIndex[IdentNo];
    break;
#endif
#if ((VS_ACTION_EXPLS != 0) && (VS_NOF_ACTION_FUNCTIONS != 0u))
  case ACTION_TYPE:
    if (VS_NOF_ACTION_FUNCTIONS <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    *Text = (char const *)&VS.AppHmi_MediaStateMachineActionExpls + VS.AppHmi_MediaStateMachineActionExplsIndex[IdentNo];
    break;
#endif
  default:
    return (SES_TYPE_ERR);
  }
  return (SES_OKAY);
}
#endif


#if (SEM_STATE == 1)
unsigned char AppHmi_MediaStateMachine::SEM_State (SEM_STATE_MACHINE_TYPE StateMachineNo,
  SEM_STATE_TYPE *StateNo) const
{
  if (VS_NOF_STATE_MACHINES <= StateMachineNo)
  {
    return (SES_RANGE_ERR);
  }
#if (VS_NOF_STATE_MACHINES != 0u)
  *StateNo = SEM.CSV[StateMachineNo];
#else
  *StateNo = 0;
#endif
  return (SES_FOUND);
}
#endif


#if (SEM_STATE_ALL == 1)
unsigned char AppHmi_MediaStateMachine::SEM_StateAll (SEM_STATE_TYPE *StateVector,
  SEM_STATE_MACHINE_TYPE MaxSize) const
{
  SEM_STATE_MACHINE_TYPE i;

  if (VS_NOF_STATE_MACHINES > MaxSize)
  {
    return (SES_BUFFER_OVERFLOW);
  }

#if (VS_NOF_STATE_MACHINES != 0u)
  for (i = 0u; i < VS_NOF_STATE_MACHINES; i++)
  {
    StateVector[i] = SEM.CSV[i];
  }
#else
  for (i = 0u; i < VS_NOF_STATE_MACHINES; i++)
  {
    StateVector[i] = 0;
  }
#endif
  return (SES_FOUND);
}
#endif


#if (SEM_MACHINE == 1)
unsigned char AppHmi_MediaStateMachine::SEM_Machine (SEM_STATE_TYPE StateNo,
  SEM_STATE_MACHINE_TYPE *StateMachineNo) const
{
  if (VS_NOF_STATES <= StateNo)
  {
    return (SES_RANGE_ERR);
  }
#if (VS_NOF_STATE_MACHINES != 0u)
  *StateMachineNo = VS.AppHmi_MediaStateMachineStateMachineIndex[StateNo];
#else
  *StateMachineNo = 0;
#endif
  return (SES_FOUND);
}
#endif


#if (SEM_FORCE_STATE == 1)
unsigned char AppHmi_MediaStateMachine::SEM_ForceState (SEM_STATE_TYPE StateNo)
{
  if (VS_NOF_STATES <= StateNo)
  {
    return (SES_RANGE_ERR);
  }
#if (VS_NOF_STATE_MACHINES != 0u)
  SEM.CSV[VS.AppHmi_MediaStateMachineStateMachineIndex[StateNo]] = StateNo;
#endif
  return (SES_OKAY);
}
#endif
