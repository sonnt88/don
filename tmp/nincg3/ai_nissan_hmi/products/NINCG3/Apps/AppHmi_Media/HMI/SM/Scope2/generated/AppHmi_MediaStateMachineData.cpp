/*
 * Id:        AppHmi_MediaStateMachineData.cpp
 *
 * Function:  VS System Data Source File.
 *
 * Generated: Thu May 12 16:27:29 2016
 *
 * Coder 7, 3, 2, 2426
 * 
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include "AppHmi_MediaStateMachineSEMLibB.h"


#include "AppHmi_MediaStateMachineData.h"


#include <stdarg.h>


/*
 * VS System Internal Variable Initializing Function.
 */
void AppHmi_MediaStateMachine::SEM_InitInternalVariables (void)
{
  avrcpType = 0;
  currentListId = 4032ul;
  currentSwitchID = 0;
  getActiveSrc = 0ul;
  getIsRootFolder = 0;
  isTempContextActive = 0;
  screen_Type = 1;
  IN_SwitchId = 0;
  In_SelfswitchId = 0;
  IsSpiPhoneSourceActive = 0;
  IsTempContext = 0;
  SetSpiUtilityHistory = 0;
}


void AppHmi_MediaStateMachine::BufferVariables (SEM_EVENT_TYPE EventNo)
{
  if (EventNo >= 180)
  {
    EventNo -= 1;
  }
  switch (EventNo)
  {
  case 1:
    VSDBVar.DB1.VS_INTVar[0] = SetSpiUtilityHistory;
    break;

  default:
    break;
  }
}


/*
 * SEM Deduct Function.
 */
unsigned char AppHmi_MediaStateMachine::SEM_Deduct (SEM_EVENT_TYPE EventNo, ...)
{
  va_list ap;

  va_start(ap, EventNo);
  if (SEM.State == 0x00u /* STATE_SEM_NOT_INITIALIZED */)
  {
    return SES_NOT_INITIALIZED;
  }
  if (VS_NOF_EVENTS <= EventNo)
  {
    return (SES_RANGE_ERR);
  }
  switch (EventNo)
  {
  case 15:
    EventArgsVar.DB142.VS_BOOLVar[0] = (VS_BOOL) va_arg(ap, VS_INT);
    break;

  case 17:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 18:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 20:
    EventArgsVar.DB165.VS_UINT8Var[0] = (VS_UINT8) va_arg(ap, VS_INT);
    EventArgsVar.DB165.VS_UINT8Var[1] = (VS_UINT8) va_arg(ap, VS_INT);
    break;

  case 27:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    EventArgsVar.DB153.VS_INT8Var[1] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 32:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 33:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 34:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 35:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 36:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 37:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 38:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 39:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 40:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 41:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 42:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 43:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 44:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 45:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 46:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 47:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 48:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 49:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 50:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 51:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 52:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 53:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 54:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 55:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 56:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 57:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 58:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 59:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 60:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 61:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 62:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 63:
    EventArgsVar.DB165.VS_UINT8Var[0] = (VS_UINT8) va_arg(ap, VS_INT);
    break;

  case 64:
    EventArgsVar.DB139.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB139.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB139.VS_UINT32Var[2] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 65:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 66:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 67:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 68:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 69:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 70:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 71:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 72:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 73:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 74:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 75:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 76:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 77:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 78:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 79:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 80:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 81:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 82:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 83:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 84:
    EventArgsVar.DB143.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 85:
    EventArgsVar.DB89.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 86:
    EventArgsVar.DB89.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 87:
    EventArgsVar.DB89.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 88:
    EventArgsVar.DB89.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 89:
    EventArgsVar.DB89.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 90:
    EventArgsVar.DB143.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 91:
    EventArgsVar.DB143.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 92:
    EventArgsVar.DB143.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 93:
    EventArgsVar.DB143.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 94:
    EventArgsVar.DB143.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 101:
    EventArgsVar.DB143.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 102:
    EventArgsVar.DB143.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB143.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 103:
    EventArgsVar.DB143.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB143.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 104:
    EventArgsVar.DB143.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB143.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 105:
    EventArgsVar.DB143.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB143.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 106:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 107:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 108:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 109:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 110:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 111:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 112:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 113:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 114:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 115:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 116:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 117:
    EventArgsVar.DB142.VS_BOOLVar[0] = (VS_BOOL) va_arg(ap, VS_INT);
    break;

  case 118:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 120:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 122:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    EventArgsVar.DB153.VS_INT8Var[1] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 126:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 127:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 128:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 129:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 130:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 139:
    EventArgsVar.DB139.VS_UINT8Var[0] = (VS_UINT8) va_arg(ap, VS_INT);
    EventArgsVar.DB139.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB139.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB139.VS_UINT32Var[2] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 141:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 142:
    EventArgsVar.DB142.VS_BOOLVar[0] = (VS_BOOL) va_arg(ap, VS_INT);
    break;

  case 143:
    EventArgsVar.DB143.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB143.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 144:
    EventArgsVar.DB143.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB143.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 145:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 146:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 147:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 148:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB60.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 149:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 150:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 151:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 152:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 153:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    EventArgsVar.DB153.VS_INT8Var[1] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 157:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 158:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 159:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 160:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 161:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 162:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 163:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 165:
    EventArgsVar.DB165.VS_UINT8Var[0] = (VS_UINT8) va_arg(ap, VS_INT);
    EventArgsVar.DB165.VS_UINT8Var[1] = (VS_UINT8) va_arg(ap, VS_INT);
    break;

  case 166:
    EventArgsVar.DB153.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 171:
    EventArgsVar.DB172.VS_INT32Var[0] = (VS_INT32) va_arg(ap, VS_INT32_VAARG);
    break;

  case 172:
    EventArgsVar.DB172.VS_INT32Var[0] = (VS_INT32) va_arg(ap, VS_INT32_VAARG);
    break;

  case 177:
    EventArgsVar.DB60.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  default:
    break;
  }
  if (EventNo == SE_RESET)
  {
    SEM.EventNo = SE_RESET;
    SEM.DIt = 2;
  }
  else
  {
    SEM.EventNo = VS_TRIGGERLESS_TRIGGER;
    SEM.DIt = 0;
  }
  SEM.State = 0x02u; /* STATE_SEM_PREPARE */
  SEM.InitialEventNo = EventNo;
  SEM.OriginalEventNo = EventNo;

  va_end(ap);
  return (SES_OKAY);
}


/*
 * Guard Expression Functions.
 */
VS_BOOL AppHmi_MediaStateMachine::VSGuard (SEM_GUARD_EXPRESSION_TYPE i)
{
  switch (i)
  {
  case 0:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == ANDROID_AUTO_ASK_ON_CONNECT_STATUS_ON);
  case 1:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == CARPLAY_ASK_ON_CONNECT_STATUS_ON);
  case 2:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_DOWN && dgSpiMediaActiveSource());
  case 3:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && dgSpiMediaActiveSource());
  case 4:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && dgSpiMediaActiveSource());
  case 5:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP && dgSpiMediaActiveSource());
  case 6:
    return (VS_BOOL)(dgSetDIPOActiveState());
  case 7:
    return (VS_BOOL)(!dgSetDIPOActiveState());
  case 8:
    return (VS_BOOL)(dgSetAAPActiveState());
  case 9:
    return (VS_BOOL)(!dgSetAAPActiveState());
  case 10:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_DOWN && dgSpiSessionActive() && !dgIsAndroidAutoCallStatus());
  case 11:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && dgSpiSessionActive() && !dgIsAndroidAutoCallStatus());
  case 12:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && dgSpiSessionActive() && !dgIsAndroidAutoCallStatus());
  case 13:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP && dgSpiSessionActive() && !dgIsAndroidAutoCallStatus());
  case 14:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_DOWN && dgSpiSessionActive());
  case 15:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && dgSpiSessionActive());
  case 16:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && dgSpiSessionActive());
  case 17:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP && dgSpiSessionActive());
  case 18:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && dgBlockClockScreenOnHKILLUMLongPress() == 1);
  case 19:
    return (VS_BOOL)(EventArgsVar.DB142.VS_BOOLVar[0] == true);
  case 20:
    return (VS_BOOL)((EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP || EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP) && screen_Type == Enum_OUTSIDE_BROWSE_SCREENS);
  case 21:
    return (VS_BOOL)(EventArgsVar.DB165.VS_UINT8Var[0] == Enum_SOURCE_AUX);
  case 22:
    return (VS_BOOL)(EventArgsVar.DB165.VS_UINT8Var[0] == Enum_SOURCE_CDMP3);
  case 23:
    return (VS_BOOL)(EventArgsVar.DB165.VS_UINT8Var[0] == Enum_SOURCE_CD);
  case 24:
    return (VS_BOOL)(EventArgsVar.DB165.VS_UINT8Var[0] == Enum_SOURCE_USB);
  case 25:
    return (VS_BOOL)(EventArgsVar.DB165.VS_UINT8Var[0] == Enum_SOURCE_IPOD);
  case 26:
    return (VS_BOOL)(EventArgsVar.DB165.VS_UINT8Var[0] == Enum_SOURCE_BT && EventArgsVar.DB165.VS_UINT8Var[1] == 1);
  case 27:
    return (VS_BOOL)(EventArgsVar.DB165.VS_UINT8Var[0] == Enum_SOURCE_BT && EventArgsVar.DB165.VS_UINT8Var[1] == 0);
  case 28:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
  case 29:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
  case 30:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
  case 31:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
  case 32:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
  case 33:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
  case 34:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION && (screen_Type == Enum_INSIDE_BROWSE_SCREENS || !dgActiveApplicationState()));
  case 35:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
  case 36:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
  case 37:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
  case 38:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
  case 39:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
  case 40:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
  case 41:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
  case 42:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getActiveSrc != Enum_SOURCE_BT_NOT_CONNECTED && getActiveSrc != Enum_SOURCE_AUX && dgIsMediaActive() && !dgIsMuteActive());
  case 43:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && getActiveSrc != Enum_SOURCE_BT_NOT_CONNECTED && getActiveSrc != Enum_SOURCE_AUX && dgIsMediaActive() && !dgIsMuteActive());
  case 44:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP && getActiveSrc != Enum_SOURCE_BT_NOT_CONNECTED && getActiveSrc != Enum_SOURCE_AUX && dgIsMediaActive() && !dgIsMuteActive());
  case 45:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP);
  case 46:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
  case 47:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__POPUP_SOURCE_INITIALISING);
  case 48:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__POPUP_SOURCE_INITIALISING);
  case 49:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
  case 50:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
  case 51:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
  case 52:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
  case 53:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
  case 54:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
  case 55:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
  case 56:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
  case 57:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
  case 58:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
  case 59:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
  case 60:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
  case 61:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
  case 62:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
  case 63:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
  case 64:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
  case 65:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
  case 66:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
  case 67:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
  case 68:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
  case 69:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
  case 70:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
  case 71:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
  case 72:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
  case 73:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
  case 74:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
  case 75:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
  case 76:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
  case 77:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
  case 78:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
  case 79:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
  case 80:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
  case 81:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
  case 82:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
  case 83:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] < 0);
  case 84:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
  case 85:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] > 0);
  case 86:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_DOWN);
  case 87:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
  case 88:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
  case 89:
    return (VS_BOOL)(IsTempContext != 1);
  case 90:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1);
  case 91:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP);
  case 92:
    return (VS_BOOL)(VSDBVar.DB1.VS_INTVar[0] != 0);
  case 93:
    return (VS_BOOL)(VSDBVar.DB1.VS_INTVar[0] == 0);
  case 94:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND);
  case 95:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND);
  case 96:
    return (VS_BOOL)(dgIsRemindBtnEnabled());
  case 97:
    return (VS_BOOL)(!dgIsRemindBtnEnabled());
  case 98:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP || EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP);
  case 99:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND);
  case 100:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND);
  case 101:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND);
  case 102:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND);
  case 103:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND);
  case 104:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND);
  case 105:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND);
  case 106:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND);
  case 107:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND);
  case 108:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND);
  case 109:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_START_FAIL);
  case 110:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_START_FAIL);
  case 111:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_START_FAIL);
  case 112:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_START_FAIL);
  case 113:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
  case 114:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
  case 115:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTING_SYSIND);
  case 116:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTING_SYSIND);
  case 117:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTED_SYSIND);
  case 118:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTED_SYSIND);
  case 119:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTED_SYSIND);
  case 120:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTED_SYSIND);
  case 121:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND);
  case 122:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND);
  case 123:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND);
  case 124:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND);
  case 125:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_ADVISORY_SYSIND);
  case 126:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_ADVISORY_SYSIND);
  case 127:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND);
  case 128:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND);
  case 129:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == SpiPopups_Global_Popups_SPI_IMPOSE_CARPLAY_CALL_INFO);
  case 130:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_TOP_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == SpiPopups_Global_Popups_SPI_IMPOSE_CARPLAY_CALL_INFO);
  case 131:
    return (VS_BOOL)(EventArgsVar.DB143.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB143.VS_UINT32Var[1] == Global_Scenes_WAIT_SCENE);
  case 132:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_MEDIA && EventArgsVar.DB60.VS_UINT32Var[1] == Global_Scenes_WAIT_SCENE);
  case 133:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_MEDIA_GADGET_CURRENT_SONG_WIDGET_L);
  case 134:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == Enum_MEDIA_GADGET_CURRENT_SONG_WIDGET_S);
  case 135:
    return (VS_BOOL)(getActiveSrc == Enum_SOURCE_CDMP3);
  case 136:
    return (VS_BOOL)(currentListId != LIST_ID_BROWSER_MAIN);
  case 137:
    return (VS_BOOL)(currentListId == LIST_ID_BROWSER_MAIN);
  case 138:
    return (VS_BOOL)(avrcpType == Enum_HIGHER);
  case 139:
    return (VS_BOOL)(avrcpType == Enum_LOWER);
  case 140:
    return (VS_BOOL)((EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP || EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONGUP) && screen_Type == Enum_INSIDE_BROWSE_SCREENS);
  case 141:
    return (VS_BOOL)(getActiveSrc == Enum_SOURCE_IPOD);
  case 142:
    return (VS_BOOL)(getActiveSrc == Enum_SOURCE_BT);
  case 143:
    return (VS_BOOL)(getActiveSrc == Enum_SOURCE_USB);
  case 144:
    return (VS_BOOL)(isTempContextActive == false);
  case 145:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getIsRootFolder == true && getActiveSrc == Enum_SOURCE_USB);
  case 146:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && getActiveSrc != Enum_SOURCE_BT);
  case 147:
    return (VS_BOOL)(!EventArgsVar.DB142.VS_BOOLVar[0] && getActiveSrc == Enum_SOURCE_BT || getActiveSrc == Enum_SOURCE_CDMP3 || getActiveSrc == Enum_SOURCE_USB);
  case 148:
    return (VS_BOOL)(getIsRootFolder == true && getActiveSrc == Enum_SOURCE_USB);
  case 149:
    return (VS_BOOL)(getIsRootFolder == true && getActiveSrc == Enum_SOURCE_BT);
  case 150:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getIsRootFolder == true && getActiveSrc == Enum_SOURCE_BT);
  case 151:
    return (VS_BOOL)(EventArgsVar.DB142.VS_BOOLVar[0] && getActiveSrc == Enum_SOURCE_BT);
  case 152:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getIsRootFolder == false);
  case 153:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && getActiveSrc == Enum_SOURCE_BT);
  case 154:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getIsRootFolder == true && getActiveSrc == Enum_SOURCE_CDMP3);
  case 155:
    return (VS_BOOL)(getIsRootFolder == false);
  case 156:
    return (VS_BOOL)(getActiveSrc == Enum_SOURCE_CDMP3 && getIsRootFolder == true);
  case 157:
    return (VS_BOOL)(EventArgsVar.DB60.VS_UINT32Var[0] == LIST_ROW_VALUE);
  case 158:
    return (VS_BOOL)((EventArgsVar.DB60.VS_UINT32Var[0] != LIST_ITEM_USB_BROWSE_UPDATE_MUSIC_LIBRARY && getActiveSrc == Enum_SOURCE_USB || EventArgsVar.DB60.VS_UINT32Var[0] != LIST_ITEM_IPOD_BROWSE_UPDATE_MUSIC_LIBRARY && getActiveSrc == Enum_SOURCE_IPOD) && EventArgsVar.DB60.VS_UINT32Var[0] != LIST_ROW_VALUE);
  case 159:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getActiveSrc == Enum_SOURCE_USB);
  case 160:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getActiveSrc == Enum_SOURCE_IPOD);
  case 161:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && getActiveSrc == Enum_SOURCE_USB);
  case 162:
    return (VS_BOOL)((EventArgsVar.DB139.VS_UINT32Var[2] == LIST_ID_BROWSER_SONG || EventArgsVar.DB139.VS_UINT32Var[2] == LIST_ID_BROWSER_ALBUM_SONG || EventArgsVar.DB139.VS_UINT32Var[2] == LIST_ID_BROWSER_ARTIST_ALBUM_SONG || EventArgsVar.DB139.VS_UINT32Var[2] == LIST_ID_BROWSER_BOOKTITLE_CHAPTER || EventArgsVar.DB139.VS_UINT32Var[2] == LIST_ID_BROWSER_COMPOSER_ALBUM_SONG || EventArgsVar.DB139.VS_UINT32Var[2] == LIST_ID_BROWSER_GENRE_ARTIST_ALBUM_SONG || EventArgsVar.DB139.VS_UINT32Var[2] == LIST_ID_BROWSER_PLAYLIST_SONG || EventArgsVar.DB139.VS_UINT32Var[2] == LIST_ID_BROWSER_PODCAST_EPISODE || EventArgsVar.DB139.VS_UINT32Var[2] == LIST_ID_BROWSER_GENRE_ARTIST_SONG || EventArgsVar.DB139.VS_UINT32Var[2] == LIST_ID_BROWSER_GENRE_ALBUM_SONG || EventArgsVar.DB139.VS_UINT32Var[2] == LIST_ID_BROWSER_GENRE_SONG || EventArgsVar.DB139.VS_UINT32Var[2] == LIST_ID_BROWSER_ARTIST_SONG || EventArgsVar.DB139.VS_UINT32Var[2] == LIST_ID_BROWSER_COMPOSER_SONG) && EventArgsVar.DB139.VS_UINT32Var[0] != LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST && EventArgsVar.DB139.VS_UINT32Var[0] != LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST);
  case 163:
    return (VS_BOOL)(EventArgsVar.DB139.VS_UINT32Var[2] != LIST_ID_BROWSER_SONG && EventArgsVar.DB139.VS_UINT32Var[2] != LIST_ID_BROWSER_ALBUM_SONG && EventArgsVar.DB139.VS_UINT32Var[2] != LIST_ID_BROWSER_ARTIST_ALBUM_SONG && EventArgsVar.DB139.VS_UINT32Var[2] != LIST_ID_BROWSER_BOOKTITLE_CHAPTER && EventArgsVar.DB139.VS_UINT32Var[2] != LIST_ID_BROWSER_COMPOSER_ALBUM_SONG && EventArgsVar.DB139.VS_UINT32Var[2] != LIST_ID_BROWSER_GENRE_ARTIST_ALBUM_SONG && EventArgsVar.DB139.VS_UINT32Var[2] != LIST_ID_BROWSER_PLAYLIST_SONG && EventArgsVar.DB139.VS_UINT32Var[2] != LIST_ID_BROWSER_PODCAST_EPISODE && EventArgsVar.DB139.VS_UINT32Var[2] != LIST_ID_BROWSER_GENRE_ARTIST_SONG && EventArgsVar.DB139.VS_UINT32Var[2] != LIST_ID_BROWSER_GENRE_ALBUM_SONG && EventArgsVar.DB139.VS_UINT32Var[2] != LIST_ID_BROWSER_GENRE_SONG && EventArgsVar.DB139.VS_UINT32Var[2] != LIST_ID_BROWSER_ARTIST_SONG && EventArgsVar.DB139.VS_UINT32Var[2] != LIST_ID_BROWSER_COMPOSER_SONG && EventArgsVar.DB139.VS_UINT32Var[0] != LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST && EventArgsVar.DB139.VS_UINT32Var[0] != LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST);
  case 164:
    return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_LONG1 && getActiveSrc == Enum_SOURCE_IPOD);
  }
  return (VS_BOOL)(EventArgsVar.DB153.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP && getActiveSrc == Enum_SOURCE_BT);
}


/*
 * Action Expressions Wrapper Function.
 */
VS_VOID AppHmi_MediaStateMachine::VSAction (SEM_ACTION_EXPRESSION_TYPE i)
{
  switch (i)
  {
  case 0:
    Notify_Init_Finished();
    break;
  case 1:
    acPerform_AndroidDontRemindMeReqMsg();
    break;
  case 4:
    acPerform_BTBrowseTimerStopReqMsg();
    break;
  case 8:
    acPerform_CurrentPlayingListReqMsg();
    break;
  case 9:
    acPerform_DisclaimerButtonNo();
    break;
  case 10:
    acPerform_DontRemindMeReqMsg();
    break;
  case 11:
    acPerform_EncoderNegativeRotationUpdMsg();
    break;
  case 12:
    acPerform_EncoderPositiveRotationUpdMsg();
    break;
  case 13:
    acPerform_EncoderPressUpdMsg();
    break;
  case 14:
    acPerform_ExitBrowse();
    break;
  case 15:
    acPerform_FastForwardStartReqMsg();
    break;
  case 16:
    acPerform_FastForwardStopReqMsg();
    break;
  case 17:
    acPerform_FastRewindStartReqMsg();
    break;
  case 18:
    acPerform_FastRewindStopReqMsg();
    break;
  case 19:
    acPerform_FolderUpBtnPress();
    break;
  case 20:
    acPerform_HK_EJECT_MsgPost();
    break;
  case 21:
    acPerform_HMISubStateON();
    break;
  case 22:
    acPerform_InitialiseBTBrowseMsg();
    break;
  case 23:
    acPerform_InitialiseFolderBrowser();
    break;
  case 24:
    acPerform_InitializeMediaParametersUpdMsg();
    break;
  case 25:
    acPerform_LaunchAndroidAuto();
    break;
  case 26:
    acPerform_LaunchCarPlay();
    break;
  case 27:
    acPerform_MediaAudioGadget_MsgPost();
    break;
  case 28:
    acPerform_MetaDataBrowserBackBtnPress();
    break;
  case 29:
    acPerform_PauseReqMsg();
    break;
  case 31:
    acPerform_PlayPauseReqMsg();
    break;
  case 32:
    acPerform_PlayReqMsg();
    break;
  case 33:
    acPerform_QuickSearchBtnPressUpdMsg();
    break;
  case 34:
    acPerform_RandomReqMsg();
    break;
  case 35:
    acPerform_RepeatReqMsg();
    break;
  case 37:
    acPerform_SeekNextReqMsg();
    break;
  case 38:
    acPerform_SeekPrevReqMsg();
    break;
  case 39:
    acPerform_SendDipoPlayCommand();
    break;
  case 40:
    acPerform_SourceToggleReqMsg();
    break;
  case 43:
    acPerform_TAReqMsg();
    break;
  case 44:
    acPerform_ToggleAlbumArtReqMsg();
    break;
  case 45:
    acPerform_UpdateAPPSSettingsOnNOReqMsg();
    break;
  case 46:
    acPerform_UpdateAPPSSettingsOnYesReqMsg();
    break;
  case 47:
    acPerform_UserInactivityTimerStopMsg();
    break;
  case 48:
    acPerform_VerticalKeyLanguage();
    break;
  case 49:
    acPerform_onAndroidDisclaimerBtnNoReqMsg();
    break;
  case 51:
    acVKBSearchListDownMsg();
    break;
  case 52:
    acVKBSearchListUpMsg();
    break;
  case 67:
    gacDisplayFooterLineReq();
    break;
  case 68:
    gacHideFooterLineReq();
    break;
  case 72:
    gacScrollListDown();
    break;
  case 73:
    gacScrollListUp();
    break;
  case 74:
    gacScrollPageDown();
    break;
  case 75:
    gacScrollPageUp();
    break;
  case 76:
    gacTBTRetriggerAcousticOutput();
    break;
  case 77:
    gacWaitAnimationStartReq();
    break;
  case 78:
    gacWaitAnimationStopReq();
    break;
  case 79:
    SetSpiUtilityHistory = 0;
    break;
  case 80:
    In_SelfswitchId = EventArgsVar.DB60.VS_UINT32Var[1];
    break;
  case 81:
    IN_SwitchId = EventArgsVar.DB60.VS_UINT32Var[1];
    break;
  case 82:
    IN_SwitchId = EventArgsVar.DB60.VS_UINT32Var[1];
    break;
  case 83:
    IsSpiPhoneSourceActive = EventArgsVar.DB153.VS_INT8Var[0];
    break;
  case 84:
    IsTempContext = EventArgsVar.DB142.VS_BOOLVar[0];
    break;
  case 85:
    screen_Type = Enum_OUTSIDE_BROWSE_SCREENS;
    break;
  case 86:
    currentSwitchID = EventArgsVar.DB60.VS_UINT32Var[0];
    break;
  case 87:
    screen_Type = Enum_INSIDE_BROWSE_SCREENS;
    break;
  case 88:
    getActiveSrc = Enum_SOURCE_AUX;
    break;
  case 89:
    getActiveSrc = Enum_SOURCE_CDMP3;
    break;
  case 90:
    getActiveSrc = Enum_SOURCE_CD;
    break;
  case 91:
    getActiveSrc = Enum_SOURCE_USB;
    break;
  case 92:
    getActiveSrc = Enum_SOURCE_IPOD;
    break;
  case 93:
    getActiveSrc = Enum_SOURCE_BT;
    break;
  case 94:
    getActiveSrc = Enum_SOURCE_BT_NOT_CONNECTED;
    break;
  case 95:
    currentListId = LIST_ID_BROWSER_MAIN;
    break;
  case 96:
    avrcpType = EventArgsVar.DB165.VS_UINT8Var[0];
    break;
  case 97:
    isTempContextActive = EventArgsVar.DB142.VS_BOOLVar[0];
    break;
  case 98:
    In_SelfswitchId = EventArgsVar.DB60.VS_UINT32Var[1];
    break;
  case 99:
    SetSpiUtilityHistory = 1;
    break;
  case 100:
    getIsRootFolder = EventArgsVar.DB142.VS_BOOLVar[0];
    break;
  case 101:
    getIsRootFolder = EventArgsVar.DB142.VS_BOOLVar[0];
    break;
  case 102:
    currentSwitchID = 0;
    break;
  case 103:
    IN_SwitchId = 0;
    break;
  case 104:
    getIsRootFolder = EventArgsVar.DB142.VS_BOOLVar[0];
    break;
  case 105:
    getIsRootFolder = EventArgsVar.DB142.VS_BOOLVar[0];
    break;
  case 106:
    currentListId = EventArgsVar.DB60.VS_UINT32Var[0];
    break;
  case 107:
    getIsRootFolder = EventArgsVar.DB142.VS_BOOLVar[0];
    break;
  case 108:
    currentListId = EventArgsVar.DB60.VS_UINT32Var[0];
    break;
  case 109:
    currentListId = EventArgsVar.DB60.VS_UINT32Var[0];
    break;
  case 110:
    getIsRootFolder = EventArgsVar.DB142.VS_BOOLVar[0];
    break;
  case 111:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__POPUP_SOURCE_INITIALISING);
    break;
  case 112:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
    break;
  case 113:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
    break;
  case 114:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
    break;
  case 115:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
    break;
  case 116:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
    break;
  case 117:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
    break;
  case 118:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 119:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 120:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 121:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 122:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 123:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 124:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 125:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 126:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 127:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 128:
    gacViewDestroyReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 129:
    acPerform_SetAppModeOnCheck(Enum_APP_MODE_UTILITY);
    break;
  case 130:
    acPerform_MediaSpiStateActiveUpdMsg(false);
    break;
  case 131:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 132:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND);
    break;
  case 133:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND);
    break;
  case 134:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND);
    break;
  case 135:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND);
    break;
  case 136:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND);
    break;
  case 137:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_START_FAIL);
    break;
  case 138:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_START_FAIL);
    break;
  case 139:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 140:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTING_SYSIND);
    break;
  case 141:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTED_SYSIND);
    break;
  case 142:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTED_SYSIND);
    break;
  case 143:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND);
    break;
  case 144:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND);
    break;
  case 145:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 146:
    gacViewDestroyReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND);
    break;
  case 147:
    gacViewDestroyReq(SpiPopups_Global_Popups_SPI_IMPOSE_CARPLAY_CALL_INFO);
    break;
  case 148:
    gacViewDestroyReq(Global_Scenes_WAIT_SCENE);
    break;
  case 149:
    gacViewHideReq(Spi_Spi_Scene_INFO_CARPLAY_APP);
    break;
  case 150:
    gacViewDestroyReq(Spi_Spi_Scene_INFO_CARPLAY_APP);
    break;
  case 151:
    gacHideStatusLineReq(Enum_HEADER_TYPE_SPI);
    break;
  case 152:
    gacSetApplicationMode(Enum_APP_MODE_SOURCE);
    break;
  case 153:
    gacViewHideReq(Spi_Spi_Scene_TUTORIAL_CARPLAY);
    break;
  case 154:
    gacViewDestroyReq(Spi_Spi_Scene_TUTORIAL_CARPLAY);
    break;
  case 155:
    gacViewHideReq(Spi_Spi_Scene_SETTINGS__CARPLAY);
    break;
  case 156:
    gacViewDestroyReq(Spi_Spi_Scene_SETTINGS__CARPLAY);
    break;
  case 157:
    gacViewHideReq(Spi_Spi_Scene_INFO_APPS_SPI);
    break;
  case 158:
    gacViewDestroyReq(Spi_Spi_Scene_INFO_APPS_SPI);
    break;
  case 159:
    gacViewHideReq(Spi_Spi_Scene_TUTORIAL_ANDROID_AUTO);
    break;
  case 160:
    gacViewDestroyReq(Spi_Spi_Scene_TUTORIAL_ANDROID_AUTO);
    break;
  case 161:
    gacViewHideReq(Spi_Spi_Scene_SETTINGS__ANDROID);
    break;
  case 162:
    gacViewDestroyReq(Spi_Spi_Scene_SETTINGS__ANDROID);
    break;
  case 163:
    gacViewHideReq(Spi_Spi_Scene_SETTINGS__SPI);
    break;
  case 164:
    gacViewDestroyReq(Spi_Spi_Scene_SETTINGS__SPI);
    break;
  case 165:
    acPerform_SetAppModeAndContext(Enum_APP_MODE_APPS, In_SelfswitchId);
    break;
  case 166:
    gacViewCreateReq(Spi_Spi_Scene_INFO_CARPLAY_APP);
    break;
  case 167:
    gacViewShowReq(Spi_Spi_Scene_INFO_CARPLAY_APP);
    break;
  case 168:
    acPerform_MediaSpiStateActiveUpdMsg(true);
    break;
  case 169:
    gacDisplayStatusLineReq(Enum_HEADER_TYPE_SPI);
    break;
  case 170:
    gacViewCreateReq(Spi_Spi_Scene_SETTINGS__SPI);
    break;
  case 171:
    gacViewShowReq(Spi_Spi_Scene_SETTINGS__SPI);
    break;
  case 172:
    gacViewCreateReq(Spi_Spi_Scene_SETTINGS__CARPLAY);
    break;
  case 173:
    gacViewShowReq(Spi_Spi_Scene_SETTINGS__CARPLAY);
    break;
  case 174:
    gacViewCreateReq(Spi_Spi_Scene_SETTINGS__ANDROID);
    break;
  case 175:
    gacViewShowReq(Spi_Spi_Scene_SETTINGS__ANDROID);
    break;
  case 176:
    gacViewCreateReq(Spi_Spi_Scene_TUTORIAL_CARPLAY);
    break;
  case 177:
    gacViewShowReq(Spi_Spi_Scene_TUTORIAL_CARPLAY);
    break;
  case 178:
    gacViewCreateReq(Spi_Spi_Scene_TUTORIAL_ANDROID_AUTO);
    break;
  case 179:
    gacViewShowReq(Spi_Spi_Scene_TUTORIAL_ANDROID_AUTO);
    break;
  case 180:
    acPerform_SetAppModeAndContext(Enum_APP_MODE_UTILITY, EventArgsVar.DB60.VS_UINT32Var[1]);
    break;
  case 181:
    acPerform_SetAppModeAndContext(Enum_APP_MODE_UTILITY, EventArgsVar.DB60.VS_UINT32Var[1]);
    break;
  case 182:
    gacViewCreateReq(Spi_Spi_Scene_INFO_APPS_SPI);
    break;
  case 183:
    gacViewShowReq(Spi_Spi_Scene_INFO_APPS_SPI);
    break;
  case 184:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND);
    break;
  case 185:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 186:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND);
    break;
  case 187:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 188:
    acPerform_LaunchCarplayWithApp(Enum_DiPOAppType__DIPO_NOWPLAYING);
    break;
  case 189:
    acPerform_LaunchAndroidAutowithAppReqMsg(Enum_DiPOAppType__DIPO_MUSIC);
    break;
  case 190:
    acPerform_LaunchSPIWithApp(Enum_DiPOAppType__DIPO_MOBILEPHONE);
    break;
  case 191:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_HK_NEXT);
    break;
  case 192:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_HK_NEXT);
    break;
  case 193:
    acPerform_InterruptToMeterMsg(false, Enum_NEXT_SONG);
    break;
  case 194:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_HK_PREVIOUS);
    break;
  case 195:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_HK_PREVIOUS);
    break;
  case 196:
    acPerform_InterruptToMeterMsg(false, Enum_PREV_SONG);
    break;
  case 197:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_SWC_NEXT);
    break;
  case 198:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_SWC_NEXT);
    break;
  case 199:
    acPerform_InterruptToMeterMsg(true, Enum_NEXT_SONG);
    break;
  case 200:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_SWC_PREV);
    break;
  case 201:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_SWC_PREV);
    break;
  case 202:
    acPerform_InterruptToMeterMsg(true, Enum_PREV_SONG);
    break;
  case 203:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_HK_NEXT);
    break;
  case 204:
    acPostBeep(Enum_hmibase_BEEPTYPE_ROGER);
    break;
  case 205:
    acPerform_InterruptToMeterMsg(false, Enum_FF_SONG);
    break;
  case 206:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_HK_NEXT);
    break;
  case 207:
    acPerform_InterruptToMeterMsg(false, Enum_FF_RW_STOP);
    break;
  case 208:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_HK_PREVIOUS);
    break;
  case 209:
    acPerform_InterruptToMeterMsg(false, Enum_RW_SONG);
    break;
  case 210:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_HK_PREVIOUS);
    break;
  case 211:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_SWC_NEXT);
    break;
  case 212:
    acPerform_InterruptToMeterMsg(true, Enum_FF_SONG);
    break;
  case 213:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_SWC_NEXT);
    break;
  case 214:
    acPerform_InterruptToMeterMsg(true, Enum_FF_RW_STOP);
    break;
  case 215:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_SWC_PREV);
    break;
  case 216:
    acPerform_InterruptToMeterMsg(true, Enum_RW_SONG);
    break;
  case 217:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_SWC_PREV);
    break;
  case 218:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND);
    break;
  case 219:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND);
    break;
  case 220:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND);
    break;
  case 221:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND);
    break;
  case 222:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_START_FAIL);
    break;
  case 223:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_START_FAIL);
    break;
  case 224:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 225:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTING_SYSIND);
    break;
  case 226:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTED_SYSIND);
    break;
  case 227:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTED_SYSIND);
    break;
  case 228:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 229:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND);
    break;
  case 230:
    acPerform_onAppStateUpdMsg(Enum_hmibase_IN_BACKGROUND);
    break;
  case 231:
    acPerform_onAppStateUpdMsg(Enum_hmibase_IN_FOREGROUND);
    break;
  case 232:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_SWC_PHONE);
    break;
  case 233:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_SWC_PHONE);
    break;
  case 234:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_SWC_PHONE);
    break;
  case 235:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_SWC_PHONE);
    break;
  case 236:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTING_SYSIND);
    break;
  case 237:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 238:
    acPerform_KeyEventReqMsg(Enum_enPress, Enum_HARDKEYCODE_SWC_PTT);
    break;
  case 239:
    acPerform_KeyEventReqMsg(Enum_enRelease, Enum_HARDKEYCODE_SWC_PTT);
    break;
  case 240:
    acPerform_KeyEventReqMsg(Enum_enLongPress, Enum_HARDKEYCODE_SWC_PTT);
    break;
  case 241:
    acPerform_KeyEventReqMsg(Enum_enLongPressRelease, Enum_HARDKEYCODE_SWC_PTT);
    break;
  case 242:
    acPostBeep(Enum_hmibase_BEEPTYPE_ERROR);
    break;
  case 243:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_SPI_IMPOSE_CARPLAY_CALL_INFO);
    break;
  case 244:
    gacHideStatusLineReq(Enum_HEADER_TYPE_AUDIO_OTHER_TOP_SCREEN);
    break;
  case 245:
    gacViewHideReq(Media_Scenes_MEDIA__CD_CDMP3_MAIN);
    break;
  case 246:
    gacViewDestroyReq(Media_Scenes_MEDIA__CD_CDMP3_MAIN);
    break;
  case 247:
    gacViewHideReq(Media_Scenes_MEDIA__AUX__USB_MAIN);
    break;
  case 248:
    gacViewDestroyReq(Media_Scenes_MEDIA__AUX__USB_MAIN);
    break;
  case 249:
    gacViewHideReq(Media_Scenes_MEDIA__AUX__IPOD_MAIN);
    break;
  case 250:
    gacViewDestroyReq(Media_Scenes_MEDIA__AUX__IPOD_MAIN);
    break;
  case 251:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__LINEIN_MAIN);
    break;
  case 252:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__LINEIN_MAIN);
    break;
  case 253:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT);
    break;
  case 254:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT);
    break;
  case 255:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 256:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE);
    break;
  case 257:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE);
    break;
  case 258:
    gacPopupSBCloseReq(Global_Scenes_WAIT_SCENE);
    break;
  case 259:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__BROWSER);
    break;
  case 260:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__BROWSER);
    break;
  case 261:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__METADATA_BROWSE);
    break;
  case 262:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__METADATA_BROWSE);
    break;
  case 263:
    gacHideStatusLineReq(Enum_HEADER_TYPE_AUDIO_BT_SUB_SCREEN);
    break;
  case 264:
    gacViewHideReq(Media_Scenes_MEDIA_AUX__BTAUDIO_BROWSER);
    break;
  case 265:
    gacViewDestroyReq(Media_Scenes_MEDIA_AUX__BTAUDIO_BROWSER);
    break;
  case 266:
    gacSetApplicationMode(Enum_APP_MODE_MEDIA_SOURCE);
    break;
  case 267:
    gacHideStatusLineReq(Enum_HEADER_TYPE_AUDIO_OTHER_SUB_SCREEN);
    break;
  case 268:
    gacHideStatusLineReq(Enum_HEADER_TYPE_AUDIO_BT_TOP_SCREEN);
    break;
  case 269:
    gacViewHideReq(Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN);
    break;
  case 270:
    gacViewDestroyReq(Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN);
    break;
  case 271:
    gacViewHideReq(Media_Scenes_MEDIA__DTM_SYSTEM_DRIVES);
    break;
  case 272:
    gacViewDestroyReq(Media_Scenes_MEDIA__DTM_SYSTEM_DRIVES);
    break;
  case 273:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__POPUP_SOURCE_INITIALISING);
    break;
  case 274:
    gacViewHideReq(Media_Scenes_MEDIA__LOADING_MAIN);
    break;
  case 275:
    gacViewDestroyReq(Media_Scenes_MEDIA__LOADING_MAIN);
    break;
  case 276:
    gacDisplayStatusLineReq(Enum_HEADER_TYPE_AUDIO_OTHER_TOP_SCREEN);
    break;
  case 277:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA__AUX__USB_MAIN);
    break;
  case 278:
    gacViewCreateReq(Media_Scenes_MEDIA__AUX__USB_MAIN);
    break;
  case 279:
    gacViewShowReq(Media_Scenes_MEDIA__AUX__USB_MAIN);
    break;
  case 280:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA__AUX__IPOD_MAIN);
    break;
  case 281:
    gacViewCreateReq(Media_Scenes_MEDIA__AUX__IPOD_MAIN);
    break;
  case 282:
    gacViewShowReq(Media_Scenes_MEDIA__AUX__IPOD_MAIN);
    break;
  case 283:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT);
    break;
  case 284:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT);
    break;
  case 285:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT);
    break;
  case 286:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__LINEIN_MAIN);
    break;
  case 287:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__LINEIN_MAIN);
    break;
  case 288:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__LINEIN_MAIN);
    break;
  case 289:
    gacDisplayStatusLineReq(Enum_HEADER_TYPE_AUDIO_BT_TOP_SCREEN);
    break;
  case 290:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN);
    break;
  case 291:
    gacViewCreateReq(Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN);
    break;
  case 292:
    gacViewShowReq(Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN);
    break;
  case 293:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA__CD_CDMP3_MAIN);
    break;
  case 294:
    gacViewCreateReq(Media_Scenes_MEDIA__CD_CDMP3_MAIN);
    break;
  case 295:
    gacViewShowReq(Media_Scenes_MEDIA__CD_CDMP3_MAIN);
    break;
  case 296:
    gacViewCreateReq(Media_Scenes_MEDIA__DTM_SYSTEM_DRIVES);
    break;
  case 297:
    gacViewShowReq(Media_Scenes_MEDIA__DTM_SYSTEM_DRIVES);
    break;
  case 298:
    gacSetApplicationMode(Enum_APP_MODE_UTILITY);
    break;
  case 299:
    gacViewCreateReq(Media_Scenes_MEDIA__LOADING_MAIN);
    break;
  case 300:
    gacViewShowReq(Media_Scenes_MEDIA__LOADING_MAIN);
    break;
  case 301:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA__LOADING_MAIN);
    break;
  case 302:
    gacContextSwitchDoneRes(1);
    break;
  case 303:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__POPUP_SOURCE_INITIALISING);
    break;
  case 304:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 305:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 306:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
    break;
  case 307:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
    break;
  case 308:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
    break;
  case 309:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
    break;
  case 310:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
    break;
  case 311:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 312:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
    break;
  case 313:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
    break;
  case 314:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
    break;
  case 315:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
    break;
  case 316:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 317:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 318:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 319:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 320:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 321:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 322:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 323:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 324:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 325:
    gacContextSwitchOutReq(Enum_MASTER_CONTEXT_AUDIOSOURCE, 0, Enum_APPID_APPHMI_MASTER);
    break;
  case 326:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 327:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 328:
    gacViewShowReq(MediaPopups_Popups_MEDIA__POPUP_SOURCE_INITIALISING);
    break;
  case 329:
    gacViewHideReq(MediaPopups_Popups_MEDIA__POPUP_SOURCE_INITIALISING);
    break;
  case 330:
    gacViewClearReq(MediaPopups_Popups_MEDIA__POPUP_SOURCE_INITIALISING);
    break;
  case 331:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING, 6000);
    break;
  case 332:
    gacViewShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
    break;
  case 333:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
    break;
  case 334:
    gacViewHideReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
    break;
  case 335:
    gacViewClearReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING);
    break;
  case 336:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING, 6000);
    break;
  case 337:
    gacViewShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
    break;
  case 338:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
    break;
  case 339:
    gacViewHideReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
    break;
  case 340:
    gacViewClearReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING);
    break;
  case 341:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_READING, 6000);
    break;
  case 342:
    gacViewShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
    break;
  case 343:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
    break;
  case 344:
    gacViewHideReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
    break;
  case 345:
    gacViewClearReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
    break;
  case 346:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READING);
    break;
  case 347:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR, 6000);
    break;
  case 348:
    gacViewShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
    break;
  case 349:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
    break;
  case 350:
    gacViewHideReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
    break;
  case 351:
    gacViewClearReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR);
    break;
  case 352:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS, 6000);
    break;
  case 353:
    gacViewShowReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
    break;
  case 354:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
    break;
  case 355:
    gacViewHideReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
    break;
  case 356:
    gacViewClearReq(MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS);
    break;
  case 357:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION, 3000);
    break;
  case 358:
    gacViewShowReq(MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
    break;
  case 359:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
    break;
  case 360:
    gacViewHideReq(MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
    break;
  case 361:
    gacViewClearReq(MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
    break;
  case 362:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION);
    break;
  case 363:
    gacViewShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 364:
    gacViewHideReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 365:
    gacViewClearReq(MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE);
    break;
  case 366:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND, 6000);
    break;
  case 367:
    gacViewShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 368:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 369:
    gacViewHideReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 370:
    gacViewClearReq(MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND);
    break;
  case 371:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND, 6000);
    break;
  case 372:
    gacViewShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 373:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 374:
    gacViewHideReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 375:
    gacViewClearReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 376:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND);
    break;
  case 377:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND, 6000);
    break;
  case 378:
    gacViewShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 379:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 380:
    gacViewHideReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 381:
    gacViewClearReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 382:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND);
    break;
  case 383:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND, 6000);
    break;
  case 384:
    gacViewShowReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 385:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 386:
    gacViewHideReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 387:
    gacViewClearReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 388:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND);
    break;
  case 389:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND, 6000);
    break;
  case 390:
    gacViewShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 391:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 392:
    gacViewHideReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 393:
    gacViewClearReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND);
    break;
  case 394:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND, 6000);
    break;
  case 395:
    gacViewShowReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 396:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 397:
    gacViewHideReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 398:
    gacViewClearReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 399:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND);
    break;
  case 400:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND, 6000);
    break;
  case 401:
    gacViewShowReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 402:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 403:
    gacViewHideReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 404:
    gacViewClearReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 405:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND);
    break;
  case 406:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE, 6000);
    break;
  case 407:
    gacViewShowReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 408:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 409:
    gacViewHideReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 410:
    gacViewClearReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 411:
    gacPopupSBCloseReq(MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE);
    break;
  case 412:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH, 30000);
    break;
  case 413:
    gacViewShowReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 414:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 415:
    gacViewHideReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 416:
    gacViewClearReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 417:
    acStartAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND, 6000);
    break;
  case 418:
    gacViewShowReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 419:
    acStopAppPopupTimer(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 420:
    gacViewHideReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 421:
    gacViewClearReq(MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND);
    break;
  case 422:
    gacContextSwitchCompleteRes(In_SelfswitchId);
    break;
  case 423:
    acPerform_onHK_BackUpdMsg(Enum_enPress);
    break;
  case 424:
    acPerform_onHK_BackUpdMsg(Enum_enRelease);
    break;
  case 425:
    acPerform_onHK_BackUpdMsg(Enum_enLongPress);
    break;
  case 426:
    acPerform_onHK_BackUpdMsg(Enum_enLongPressRelease);
    break;
  case 427:
    acPerform_EncoderRotatonUpdMsg(EventArgsVar.DB153.VS_INT8Var[0]);
    break;
  case 428:
    acPerform_EncoderPressedUpdMsg(Enum_enPress);
    break;
  case 429:
    acPerform_EncoderPressedUpdMsg(Enum_enRelease);
    break;
  case 430:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 431:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 432:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_DISCLAIMER_SYSIND);
    break;
  case 433:
    acPostBeep(Enum_hmibase_BEEPTYPE_CLICK);
    break;
  case 434:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND);
    break;
  case 435:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND);
    break;
  case 436:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_DISCLAIMER_SYSIND);
    break;
  case 437:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND);
    break;
  case 438:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND);
    break;
  case 439:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND);
    break;
  case 440:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND);
    break;
  case 441:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND);
    break;
  case 442:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND);
    break;
  case 443:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND);
    break;
  case 444:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND);
    break;
  case 445:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND);
    break;
  case 446:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND);
    break;
  case 447:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND);
    break;
  case 448:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND);
    break;
  case 449:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND);
    break;
  case 450:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND);
    break;
  case 451:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND);
    break;
  case 452:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND);
    break;
  case 453:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_START_FAIL);
    break;
  case 454:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_START_FAIL);
    break;
  case 455:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_START_FAIL);
    break;
  case 456:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_START_FAIL);
    break;
  case 457:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_START_FAIL);
    break;
  case 458:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_START_FAIL);
    break;
  case 459:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_START_FAIL);
    break;
  case 460:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_START_FAIL);
    break;
  case 461:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 462:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 463:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTING_SYSIND);
    break;
  case 464:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTING_SYSIND);
    break;
  case 465:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTING_SYSIND);
    break;
  case 466:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTING_SYSIND);
    break;
  case 467:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTED_SYSIND);
    break;
  case 468:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTED_SYSIND);
    break;
  case 469:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTED_SYSIND);
    break;
  case 470:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_STARTED_SYSIND);
    break;
  case 471:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTED_SYSIND);
    break;
  case 472:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTED_SYSIND);
    break;
  case 473:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTED_SYSIND);
    break;
  case 474:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_STARTED_SYSIND);
    break;
  case 475:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND);
    break;
  case 476:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND);
    break;
  case 477:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND);
    break;
  case 478:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND);
    break;
  case 479:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND);
    break;
  case 480:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND);
    break;
  case 481:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND);
    break;
  case 482:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND);
    break;
  case 483:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 484:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 485:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 486:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_ADVISORY_SYSIND);
    break;
  case 487:
    gacViewShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND);
    break;
  case 488:
    gacViewHideReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND);
    break;
  case 489:
    gacViewClearReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND);
    break;
  case 490:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND);
    break;
  case 491:
    gacViewShowReq(SpiPopups_Global_Popups_SPI_IMPOSE_CARPLAY_CALL_INFO);
    break;
  case 492:
    gacViewHideReq(SpiPopups_Global_Popups_SPI_IMPOSE_CARPLAY_CALL_INFO);
    break;
  case 493:
    gacViewClearReq(SpiPopups_Global_Popups_SPI_IMPOSE_CARPLAY_CALL_INFO);
    break;
  case 494:
    gacPopupSBCloseReq(SpiPopups_Global_Popups_SPI_IMPOSE_CARPLAY_CALL_INFO);
    break;
  case 495:
    gacViewShowReq(Global_Scenes_WAIT_SCENE);
    break;
  case 496:
    gacRegisterForCloseOnTouchSession(Global_Scenes_WAIT_SCENE);
    break;
  case 497:
    gacDeregisterForCloseOnTouchSessionReq(Global_Scenes_WAIT_SCENE);
    break;
  case 498:
    gacViewHideReq(Global_Scenes_WAIT_SCENE);
    break;
  case 499:
    gacViewClearReq(Global_Scenes_WAIT_SCENE);
    break;
  case 500:
    gacViewCreateReq(MediaGadgets_Gadgets_MEDIA_CURRENT_SONG_LARGE_GADGET);
    break;
  case 501:
    gacViewShowReq(MediaGadgets_Gadgets_MEDIA_CURRENT_SONG_LARGE_GADGET);
    break;
  case 502:
    gacViewCreateReq(MediaGadgets_Gadgets_MEDIA_CURRENT_SONG_SMALL_GADGET);
    break;
  case 503:
    gacViewShowReq(MediaGadgets_Gadgets_MEDIA_CURRENT_SONG_SMALL_GADGET);
    break;
  case 504:
    gacDisplayStatusLineReq(Enum_HEADER_TYPE_AUDIO_OTHER_SUB_SCREEN);
    break;
  case 505:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE);
    break;
  case 506:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE);
    break;
  case 507:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE);
    break;
  case 508:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__METADATA_BROWSE);
    break;
  case 509:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__METADATA_BROWSE);
    break;
  case 510:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__METADATA_BROWSE);
    break;
  case 511:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__BROWSER);
    break;
  case 512:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__BROWSER);
    break;
  case 513:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__BROWSER);
    break;
  case 514:
    acPerform_EncoderTrackChangeUpMsg(Enum_ENCODER_ROTATION_POSITIVE, EventArgsVar.DB153.VS_INT8Var[0]);
    break;
  case 515:
    acPerform_EncoderTrackChangeUpMsg(Enum_ENCODER_ROTATION_NEGATIVE, EventArgsVar.DB153.VS_INT8Var[0]);
    break;
  case 516:
    acPerform_ChangeVolumeReqMsg(Enum_LOW_VOLUME);
    break;
  case 517:
    acPerform_ChangeVolumeReqMsg(Enum_HIGH_VOLUME);
    break;
  case 518:
    acPerform_ChangeVolumeReqMsg(Enum_MID_VOLUME);
    break;
  case 519:
    gacContextSwitchOutReq(Enum_PHONE_CONTEXT_CONNECTION_MANAGER, Enum_MEDIA_CONTEXT_BT_AUDIO_DEFAULT, Enum_APPID_APPHMI_PHONE);
    break;
  case 520:
    gacDisplayStatusLineReq(Enum_HEADER_TYPE_AUDIO_BT_SUB_SCREEN);
    break;
  case 521:
    acPerform_ScreenHeaderReq(Media_Scenes_MEDIA_AUX__BTAUDIO_BROWSER);
    break;
  case 522:
    gacViewCreateReq(Media_Scenes_MEDIA_AUX__BTAUDIO_BROWSER);
    break;
  case 523:
    gacViewShowReq(Media_Scenes_MEDIA_AUX__BTAUDIO_BROWSER);
    break;
  case 524:
    gacContextSwitchOutReq(Enum_PHONE_CONTEXT_CONNECTION_MANAGER, Enum_MEDIA_CONTEXT_HOMESCREEN_BT_AUDIO, Enum_APPID_APPHMI_PHONE);
    break;
  case 525:
    acPerform_EncoderTrackChangeUpMsg(Enum_ENCODER_ROTATION_POSITIVE, EventArgsVar.DB153.VS_INT8Var[0]);
    break;
  case 526:
    acPerform_EncoderTrackChangeUpMsg(Enum_ENCODER_ROTATION_NEGATIVE, EventArgsVar.DB153.VS_INT8Var[0]);
    break;
  case 527:
    gacContextSwitchBackRes(currentSwitchID);
    break;
  case 528:
    gacContextSwitchCompleteRes(currentSwitchID);
    break;
  case 529:
    gacContextSwitchCompleteRes(IN_SwitchId);
    break;
  case 530:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND);
    break;
  case 531:
    gacPopupCreateAndSBShowReq(Global_Scenes_WAIT_SCENE);
    break;
  case 532:
    gacContextSwitchBackRes(IN_SwitchId);
    break;
  case 533:
    gacPopupCreateAndSBShowReq(SpiPopups_Global_Popups_INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND);
    break;
  case 534:
    gacPopupCreateAndSBShowReq(MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH);
    break;
  case 535:
    acPerform_FolderBrowserBtnPress(EventArgsVar.DB60.VS_UINT32Var[0]);
    break;
  case 536:
    acPerform_BrowseBtnPressUpdMsg(EventArgsVar.DB60.VS_UINT32Var[0]);
    break;
  case 537:
    acPerform_MetaDataBrowserBtnPress(PLAY_SONG_IN_LIST, EventArgsVar.DB139.VS_UINT32Var[1]);
    break;
  case 538:
    acPerform_MetaDataBrowserBtnPress(EventArgsVar.DB139.VS_UINT32Var[0], EventArgsVar.DB139.VS_UINT32Var[1]);
    break;
  case 539:
    gacContextSwitchOutReq(Enum_PHONE_CONTEXT_CONNECTION_MANAGER, Enum_MEDIA_CONTEXT_BT_AUDIO_BROWSE, Enum_APPID_APPHMI_PHONE);
    break;
  case 540:
    acPerform_FolderBrowserBtnPress(EventArgsVar.DB60.VS_UINT32Var[0]);
    break;

  default:
    break;
  }
}
