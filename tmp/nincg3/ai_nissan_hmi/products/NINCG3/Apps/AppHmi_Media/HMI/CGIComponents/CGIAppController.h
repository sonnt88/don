#ifndef CGIAPPCONTROLLER_H
#define CGIAPPCONTROLLER_H

#include "Common/CGIAppController/CGIAppControllerProject.h"
#include "AppHmi_MediaMessages_Common.h"
#include "AppHmi_MediaMessages.h"
#include "AppHmi_SpiMessages_Common.h"
#include "Courier/Visualization/VisualizationMsgs.h"

class CGIAppController : public CGIAppControllerProject
{
   courier_messages:
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_CTRL_COMP)
      ON_COURIER_MESSAGE(SourceChangeUpdMsg)
      ON_COURIER_MESSAGE(SpiDeviceConnectedUpdMsg)
      ON_COURIER_MESSAGE(SpiPhoneAppStateUpdMsg)
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
      ON_COURIER_MESSAGE(AuxKeyRegisterUpdMsg)
#endif
#if defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2)|| defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R)
      ON_COURIER_MESSAGE(Courier::ViewPlaceholderResMsg)
      ON_COURIER_MESSAGE(RenderingCompleteMsg)
#endif


      COURIER_MSG_MAP_END_DELEGATE(CGIAppControllerProject)

   public:

      using CGIAppControllerProject::onCourierMessage;

      CGIAppController(hmibase::services::hmiappctrl::ProxyHandler& proxyHandler);
      virtual ~CGIAppController();

      bool onCourierMessage(const SourceChangeUpdMsg& oMsg);
      bool onCourierMessage(const SpiDeviceConnectedUpdMsg& oMsg);
      bool onCourierMessage(const SpiPhoneAppStateUpdMsg& oMsg);
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
      bool onCourierMessage(const AuxKeyRegisterUpdMsg& oMsg);
#endif

#if defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2)|| defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R)
      bool onCourierMessage(const Courier::ViewPlaceholderResMsg& oMsg);
      bool onCourierMessage(const RenderingCompleteMsg& msg);
#endif

   private:
      int _sourceID;
      bool _retVal;
      bool _mSpiRetVal;
      bool _mspiLockRetVal;
      bool _isPlaceholdercreated;
};


#endif // CGIAPPCONTROLLER_H
