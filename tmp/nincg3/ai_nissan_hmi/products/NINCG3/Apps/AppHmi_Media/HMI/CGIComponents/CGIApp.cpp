#include "gui_std_if.h"

#include "CGIApp.h"

CGIApp::CGIApp(const char* assetFile, hmibase::services::hmiappctrl::ProxyHandler& proxyHandler)
   : CgiApplication<AppHmi_MediaStateMachineImpl>(assetFile, proxyHandler)
{
}


CGIApp::CGIApp(const std::vector<std::string>& assetFiles, hmibase::services::hmiappctrl::ProxyHandler& proxyHandler)
   : CgiApplication<AppHmi_MediaStateMachineImpl>(assetFiles, proxyHandler)
{
}


CGIApp::~CGIApp()
{
}
