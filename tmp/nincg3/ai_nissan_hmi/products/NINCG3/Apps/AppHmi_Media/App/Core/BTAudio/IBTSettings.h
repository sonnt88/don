/**
 *  @file   IDeviceConnection.h
 *  @author ECV - A-IVI Media
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */


#ifndef IBTSETTINGS_H_
#define IBTSETTINGS_H_
#include "MOST_BTSet_FIProxy.h"

namespace App {
namespace Core {


class IBTSettings
{
   public:
      virtual ~IBTSettings() {};
      virtual bool getDeviceConnectionStatus() = 0;
      virtual void trigger_BTDefaultScreen() = 0;
      virtual void updateTrackInfoInBTMain() = 0;
};


}  // namespace Core
}  // namespace App

#endif /* IBTSETTINGS_H_ */
