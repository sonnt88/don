/**
 *  @file   SpiSettingListHandler.cpp
 *  @author ECV - rhk5kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#include "hall_std_if.h"
#include "SpiSettingListHandler.h"
#include "Spi_defines.h"
#include "Core/LanguageDefines.h"
#include "Core/Utils/KdsInfo.h"
#ifdef DP_DATAPOOL_ID
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_hmi_04_if.h"
#endif

#include "App/datapool/SpiMediaDataPoolConfig.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS         TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::SpiSettingListHandler::

#include "trcGenProj/Header/SpiSettingListHandler.cpp.trc.h"
#endif
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
static const char* const DATA_CONTEXT_LIST_ID_SETTINGS_CARPLAY_AUTO_LAUNCH = "AutoLaunchConnectList";
static const char* const DATA_CONTEXT_LIST_ID_SETTINGS_CARPLAY_TUTORIAL = "CarPlayTutorialList";
static const char* const DATA_CONTEXT_LIST_ID_SETTINGS_CARPLAY_ASK_ON_CONNECT = "AskOnConnectList";

#elif defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2)
static const char* const DATA_CONTEXT_LIST_ID_SPI_MAIN_SETTINGS_CARPLAY = "CarPlaySettingsList";
static const char* const DATA_CONTEXT_LIST_ID_SPI_MAIN_SETTINGS_ANDROID = "AndroidSettingsList";
static const char* const DATA_CONTEXT_LIST_ID_CARPLAY_AUTO_CONNECTION_SETTINGS = "CarPlayAutoConnection";
static const char* const DATA_CONTEXT_LIST_ID_CARPLAY_HELP_SETTINGS = "CarPlayHelp";
static const char* const DATA_CONTEXT_LIST_ID_CARPLAY_ASK_ON_CONNECT_SETTINGS = "CarPlayAskOnConnect";
static const char* const DATA_CONTEXT_LIST_ID_ANDROID_AUTO_CONNECTION_SETTINGS = "AndroidAutoConnectionStatus";
static const char* const DATA_CONTEXT_LIST_ID_ANDROID_AUTO_HELP_SETTINGS = "AndroidAutoHelp";
static const char* const DATA_CONTEXT_LIST_ID_ANDROID_AUTO_ASK_ON_CONNECT_SETTINGS = "AndroidAutoAskOnConnect";
static const char* const DATA_CONTEXT_LIST_ID_INFO_APPS_CARPLAY = "CarPlayInfoApps";
static const char* const DATA_CONTEXT_LIST_ID_INFO_APPS_ANDROID_AUTO = "AndroidAutoInfoApps";

#endif

namespace App {
namespace Core {

static SpiSettingListHandler* pstaticSpiSettingListHandler;
/**
 * @Constructor of class SpiSettingListHandler
 */
SpiSettingListHandler::SpiSettingListHandler(SpiConnectionHandling* paraSpiConnectionHandling , ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& spiListMidwServiceProxy , SpiConnectionSubject* paraSpiConnectionNotify)
   : _mpSpiConnectionHandling(paraSpiConnectionHandling), _spiListMidwServiceProxy(spiListMidwServiceProxy), _mSpiConnectionNotifyer(paraSpiConnectionNotify)
{
   ETG_I_REGISTER_FILE();
   ETG_TRACE_USR4(("SpiSettingListHandler: Constructor"));
   StartupSync::getInstance().registerPropertyRegistrationIF(this);
   pstaticSpiSettingListHandler = this;
   _mDpHandler = SpiMediaDataPoolConfig::getInstance();
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_SETTINGS_CARPLAY_MAIN, this);
   eDeviceCategory = DeviceCategory__DEV_TYPE_UNKNOWN;
   _mUpdateTriggerSettDevType = DeviceType__DEVICE_TYPE_UNKNOWN;
   _mSpiSettingStatus = DIPO_AAP_NO_UPDATE;
   _mIsDeviceselectinReq = false;
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_SETTINGS_SPI_MAIN, this);
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_SETTINGS_CARPLAY, this);
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_SETTINGS_ANDROID_AUTO_MAIN, this);
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_SPI_INFO_APPS, this);

#endif
   _mSpiConnectionNotifyer->registerObserver(this);
}


/**
 * @Destructor of class SpiSettingListHandler
 */
SpiSettingListHandler::~SpiSettingListHandler()
{
   ETG_I_REGISTER_FILE();
   ETG_TRACE_USR4(("SpiSettingListHandler: Destructor"));
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_SETTINGS_CARPLAY_MAIN);
   _mDpHandler = NULL;
   _mpSpiConnectionHandling = NULL;
   if (NULL != _mSpiConnectionNotifyer)
   {
      _mSpiConnectionNotifyer = NULL;
   }
   eDeviceCategory = DeviceCategory__DEV_TYPE_UNKNOWN;
   _spiListMidwServiceProxy.reset();
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_SETTINGS_SPI_MAIN);
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_SETTINGS_CARPLAY);
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_SETTINGS_ANDROID_AUTO_MAIN);
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_SPI_INFO_APPS);

#endif
}


/*
 * This function performs the upreg of the required properties.
 * @param[in]  - proxy: Pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - stateChange: Contains the current Service State
 * @param[out] - None
 * @return     - Void
 */
void SpiSettingListHandler::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& /*proxy */, const asf::core::ServiceStateChange& /*stateChange*/)
{
}


/*
 * This function performs the Relupreg of the required properties.
 * @param[in]  - proxy: Pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - stateChange: Contains the current Service State
 * @param[out] - None
 * @return     - Void
 */
void SpiSettingListHandler::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& /*proxy */, const asf::core::ServiceStateChange& /*stateChange*/)
{
}


void SpiSettingListHandler::onSetDeviceUsagePreferenceError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetDeviceUsagePreferenceError >&/* error*/)
{
}


/*
 * This function is called when method result for SetDeviceUsagePreference received
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - result: Pointer to SetDeviceUsagePreference
 * @param[out] - None
 * @return     - Void
 */
void SpiSettingListHandler::onSetDeviceUsagePreferenceResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetDeviceUsagePreferenceResult >& result)
{
   if (proxy == _spiListMidwServiceProxy)
   {
      ETG_TRACE_USR4(("SpiConnectionHandling: onSetDeviceUsagePreferenceResult Received with response code = %d Error code = %d settingupdatestatus = %d ", result->getResponseCode(), result->getErrorCode(), ETG_CENUM(enSpiSettingUpdateStatus, _mSpiSettingStatus)));
      if (::midw_smartphoneint_fi_types::T_e8_ResponseCode__SUCCESS == result->getResponseCode())
      {
         if (DIPO_AAP_NO_UPDATE != _mSpiSettingStatus)
         {
            updateHMISettingOnUserAction(_mSpiSettingStatus);
         }
      }
      else if (::midw_smartphoneint_fi_types::T_e8_ResponseCode__FAILURE == result->getResponseCode())
      {
         if (DIPO_AAP_NO_UPDATE != _mSpiSettingStatus)
         {
            _mSpiSettingStatus = DIPO_AAP_NO_UPDATE;
            if (DeviceType__APPLE_DEVICE == _mUpdateTriggerSettDevType)
            {
               callUpdateCarPlaySettinglist();
            }
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
            else if (DeviceType__ANDROID_DEVICE == _mUpdateTriggerSettDevType)
            {
               callUpdateAndriodAutoSettinglist();
            }
#endif
         }
      }
   }
}


/**
 * setSpiSettingPreference - helper function to call method start sendSetDeviceUsagepreference
 * @param[in] :setting
 * @return void
 */
void SpiSettingListHandler::setSpiSettingPreference(enDeviceCategory deviceCategory, ::midw_smartphoneint_fi_types::T_e8_EnabledInfo enableInfo)
{
   ETG_TRACE_USR4(("spn setSpiSettingPreference sendSetDeviceUsagePreferenceStart deviceUsage = %d current datapool setting = %d", enableInfo, _mDpHandler->getDpSpiAutoLaunch()));
   _spiListMidwServiceProxy->sendSetDeviceUsagePreferenceStart(*this, ALL_DEVICE_HANDLE, (::midw_smartphoneint_fi_types::T_e8_DeviceCategory)deviceCategory, enableInfo);
}


/**
 * This function is responsible for creation of lists corresponding to
 * the list IDs that it receives
 * @param[in] -  listID : unique ID for lists
 * @param[out]-  tSharedPtrDataProvider : Shared pointer for the created list
 */

tSharedPtrDataProvider SpiSettingListHandler::getListDataProvider(const ListDataInfo& listDataInfo)
{
   //To update Initial List conditions
   switch (listDataInfo.listId)
   {
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
      case LIST_ID_SETTINGS_CARPLAY_MAIN:
      {
         ETG_TRACE_USR4(("SpiSettingListHandler:List creation request for CarPlay Settings Screen"));
         //return fillDataForSpiSettingsList();										//scope1 settings list
         return fillDataForCarPlayMainSettingsList();
         break;
      }
#elif defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2)
      case LIST_ID_SETTINGS_SPI_MAIN:
      {
         ETG_TRACE_USR4(("SpiSettingListHandler:List creation request for SPI main Settings Screen"));
         return fillDataForSpiMainSettingsList();									//scope2 main settings list
         break;
      }
      case LIST_ID_SETTINGS_CARPLAY:
      {
         ETG_TRACE_USR4(("SpiSettingListHandler:List creation request for CarPlay main Settings Screen scope2"));
         return fillDataForCarPlayMainSettingsList();
         //return updateSpiCarPlayMainSettingsList();  							//scope2 CarPlay main settings list
         break;
      }
      case LIST_ID_SETTINGS_ANDROID_AUTO_MAIN:
      {
         ETG_TRACE_USR4(("SpiSettingListHandler:List creation request for AndroidAuto main Settings Screen scope2"));
         return fillDataForAndroidAutoMainSettingsList();   				 //scope2 AndroidAuto main settings list
         break;
      }
      case LIST_ID_SPI_INFO_APPS:
      {
         ETG_TRACE_USR4(("SpiSettingListHandler:List creation request for INFO APPS screen scope2"));
         return fillDataForInfoAppsList();   				 //scope2 INFO APPS SPI list
         break;
      }


#endif
      default:
      {
         //invalid listId
         return tSharedPtrDataProvider();
      }
   }
}


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
/**
 * This function is responsible for checking initial conditions to fill in lists and triggered
 * from getListDataProvider
 * This checks the Current Datapool Status of AutoLaunchConnect to fill in lists.
 * @param[in]  - input parameter void
 * @param[out] - tSharedPtrDataProvider : Shared pointer for the created list
 */

tSharedPtrDataProvider SpiSettingListHandler::fillDataForSpiMainSettingsList()
{
   ETG_TRACE_USR1(("SpiSettingListHandler:fillDataForSpiMainSettingsList() is called"));

   return updateSpiSettingsList();
}


#endif

/**
 * This function is responsible for checking initial conditions to fill in lists and triggered
 * from getListDataProvider
 * This checks the Current Datapool Status of AutoLaunchConnect to fill in lists.
 * @param[in]  - input parameter void
 * @param[out] - tSharedPtrDataProvider : Shared pointer for the created list
 */

//! To Update according to Datapool status
tSharedPtrDataProvider SpiSettingListHandler::fillDataForCarPlayMainSettingsList()
{
   ETG_TRACE_USR1(("SpiSettingListHandler:fillDataForCarPlayMainSettingsList() is called"));
   uint32 u32CarPlayAutoLaunchDpStatus = CARPLAY_AUTOLAUNCHSTATUS_NOT_DEFINED ;
   uint32 u32AskOnConnectDpStatus = CARPLAY_ASK_ON_CONNECT_NOT_DEFINED ;
   if (_mDpHandler != NULL)
   {
      u32CarPlayAutoLaunchDpStatus = _mDpHandler->getDpSpiCarPlayAutoLaunch();
      u32AskOnConnectDpStatus = _mDpHandler->getDpSpiCarPlayAskOnConnect();
   }
   ETG_TRACE_USR4(("SpiSettingListHandler:Initial Datapool Status for CarPlay AutoLaunchStatus: %d, AskOnConnect: %d", u32CarPlayAutoLaunchDpStatus, u32AskOnConnectDpStatus));
   return updateSpiCarPlayMainSettingsList(u32CarPlayAutoLaunchDpStatus, u32AskOnConnectDpStatus);
}


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2

/**
 * This function is responsible for checking initial conditions to fill in lists and triggered
 * from getListDataProvider
 * This checks the Current Datapool Status of AutoLaunchConnect to fill in lists.
 * @param[in]  - input parameter void
 * @param[out] - tSharedPtrDataProvider : Shared pointer for the created list
 */
tSharedPtrDataProvider SpiSettingListHandler::fillDataForAndroidAutoMainSettingsList()
{
   ETG_TRACE_USR1(("SpiSettingListHandler:fillDataForAndroidAutoMainSettingsList() is called"));
   uint32 u32AndroidAutoLaunchDpStatus = ANDROID_AUTO_AUTOLAUNCHSTATUS_NOT_DEFINED ;
   uint32 u32AndroidAskOnConnectDpStatus = ANDROID_AUTO_ASK_ON_CONNECT_NOT_DEFINED ;

   if (_mDpHandler != NULL)
   {
      u32AndroidAutoLaunchDpStatus = _mDpHandler->getDpAndroidAutoLaunch();
      u32AndroidAskOnConnectDpStatus = _mDpHandler->getDpAndroidAutoAskOnConnect();
   }
   ETG_TRACE_USR4(("SpiSettingListHandler:Initial Datapool Status for AndroidAuto AutoLaunchStatus: %d, AskOnConnect: %d", u32AndroidAutoLaunchDpStatus, u32AndroidAskOnConnectDpStatus));
   return updateAndroidAutoSettingsList(u32AndroidAutoLaunchDpStatus, u32AndroidAskOnConnectDpStatus);
}


tSharedPtrDataProvider SpiSettingListHandler::fillDataForInfoAppsList()
{
   ETG_TRACE_USR1(("SpiSettingListHandler:fillDataForInfoAppsList() is called"));
   return updateInfoAppsSpiList();
}


/**
 * This function is responsible for Creation of Spi main scope2 list and triggered
 * from fillDataForSpiMainSettingsList
 * @param[in]  - input parameter void
 * @param[out] - tSharedPtrDataProvider : Shared pointer for the created list
 */
tSharedPtrDataProvider SpiSettingListHandler::updateSpiSettingsList() const
{
   ETG_TRACE_USR1(("SpiSettingListHandler:updateSpiMainSettingsList() is called"));
   ListDataProviderBuilder listBuilder(LIST_ID_SETTINGS_SPI_MAIN, DATA_CONTEXT_LIST_ID_SPI_MAIN_SETTINGS_CARPLAY);
   if (true == KdsInfo::isCarPlayEnabled())
   {
      listBuilder.AddItem(
         (int)LIST_ITEM_CARPLAY_SETTINGS,
         0UL,
         DATA_CONTEXT_LIST_ID_SPI_MAIN_SETTINGS_CARPLAY)
      .AddData(true);
   }

   if (true == KdsInfo::isAndroidAutoEnabled())
   {
      listBuilder.AddItem(
         (int)LIST_ITEM_ANDROID_AUTO_SETTINGS,
         0UL,
         DATA_CONTEXT_LIST_ID_SPI_MAIN_SETTINGS_ANDROID)
      .AddData(true);
   }

   tSharedPtrDataProvider dataProvider = listBuilder.CreateDataProvider();
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));
   return dataProvider;
}


#endif

/**
 * This message is sent by the list widget when it requires new data.
 * This happens when the list is displayed or scrolled.
 * @param[in] - oMsg gives the list ID of the list that needs to be created
 */
bool SpiSettingListHandler::onCourierMessage(const ListDateProviderReqMsg& oMsg)
{
   ETG_TRACE_USR4(("spn onCourierMessage: ListDateProviderReqMsg listId=%d", oMsg.GetListId()));
   if (oMsg.GetListId() >= LIST_ID_SETTINGS_CARPLAY_MAIN)
   {
      ListDataInfo listDataInfo;
      listDataInfo.listId = oMsg.GetListId();
      listDataInfo.startIndex =  oMsg.GetStartIndex();
      listDataInfo.windowSize = oMsg.GetWindowElementSize();
      return ListRegistry::s_getInstance().updateList(listDataInfo);  //TODO : Confirm if this should be included after list data is received
   }
   return false;
}


/**
 * This function is responsible for Creation of Carplay scope2 lists and triggered
 * from updateSpiCarPlayMainSettingsList
 * @param[in]  - input parameter void
 * @param[out] - tSharedPtrDataProvider : Shared pointer for the created list
 */
tSharedPtrDataProvider SpiSettingListHandler::updateSpiCarPlayMainSettingsList(uint32 u32CpAutoLaunchListStatus, uint32 u32CpAskOnConnectListStatus) const
{
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
   ListDataProviderBuilder listBuilder(LIST_ID_SETTINGS_CARPLAY_MAIN, DATA_CONTEXT_LIST_ID_SETTINGS_CARPLAY_ASK_ON_CONNECT);
   ETG_TRACE_USR1(("SpiSettingList:updateSpiCarPlayMainSettingsList() is called CpAutoLaunchListStatus : %d, CpAskOnConnectListStatus: %d", u32CpAutoLaunchListStatus, u32CpAskOnConnectListStatus));

   switch (u32CpAutoLaunchListStatus)
   {
      case CARPLAY_AUTOLAUNCHSTATUS_ON:
      {
         listBuilder.AddItem(
            (int)LIST_ITEM_AUTOLAUNCH_STATUS,
            0UL,
            DATA_CONTEXT_LIST_ID_SETTINGS_CARPLAY_AUTO_LAUNCH)
         .AddData(Candera::String(SETTINGS_CARPLAY_AutomaticLaunchOnConnect))
         .AddData(Candera::String(SETTINGS_CARPLAY_ListButtonChoice_ON));
         break;
      }
      case CARPLAY_AUTOLAUNCHSTATUS_OFF:
      {
         listBuilder.AddItem(
            (int)LIST_ITEM_AUTOLAUNCH_STATUS,
            0UL,
            DATA_CONTEXT_LIST_ID_SETTINGS_CARPLAY_AUTO_LAUNCH)
         .AddData(Candera::String(SETTINGS_CARPLAY_AutomaticLaunchOnConnect))
         .AddData(Candera::String(SETTINGS_CARPLAY_ListButtonChoice_OFF));
         break;
      }
      default:
         break;
   }

   listBuilder.AddItem(
      (int)LIST_ITEM_CARPLAY_TUTORIAL,
      0UL,
      DATA_CONTEXT_LIST_ID_SETTINGS_CARPLAY_TUTORIAL)
   .AddData(Candera::String(SETTINGS_CARPLAY_Button_CarPlayTutorial));

   switch (u32CpAskOnConnectListStatus)
   {
      case CARPLAY_ASK_ON_CONNECT_ON:
      {
         listBuilder.AddItem(
            (int)LIST_ITEM_CARPLAY_ASK_ON_CONNECT,
            0UL,
            DATA_CONTEXT_LIST_ID_SETTINGS_CARPLAY_ASK_ON_CONNECT)
         .AddData(Candera::String(SETTINGS_CARPLAY_ListButtonChoice_AskOnConnect))
         .AddData(Candera::String(SETTINGS_CARPLAY_ListButtonChoice_ON));
         break;
      }
      case CARPLAY_ASK_ON_CONNECT_OFF:
      {
         listBuilder.AddItem(
            (int)LIST_ITEM_CARPLAY_ASK_ON_CONNECT,
            0UL,
            DATA_CONTEXT_LIST_ID_SETTINGS_CARPLAY_ASK_ON_CONNECT)
         .AddData(Candera::String(SETTINGS_CARPLAY_ListButtonChoice_AskOnConnect))
         .AddData(Candera::String(SETTINGS_CARPLAY_ListButtonChoice_OFF));
         break;
      }
      default:
         break;
   }

#elif defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2)

   ListDataProviderBuilder listBuilder(LIST_ID_SETTINGS_CARPLAY, DATA_CONTEXT_LIST_ID_CARPLAY_AUTO_CONNECTION_SETTINGS);
   ETG_TRACE_USR1(("SpiSettingList:updateSpiCarPlayMainSettingsList() is called CpAutoLaunchListStatus : %d, CpAskOnConnectListStatus: %d", u32CpAutoLaunchListStatus, u32CpAskOnConnectListStatus));
   switch (u32CpAutoLaunchListStatus)
   {
      case CARPLAY_AUTOLAUNCHSTATUS_ON:
      {
         listBuilder.AddItem(
            (int) LIST_ITEM_CARPLAY_AUTOMATIC_CONNECTION_STATUS,
            0UL,
            DATA_CONTEXT_LIST_ID_CARPLAY_AUTO_CONNECTION_SETTINGS)
         .AddData(true)
         .AddData(false);
         break;
      }
      case CARPLAY_AUTOLAUNCHSTATUS_OFF:
      {
         listBuilder.AddItem(
            (int) LIST_ITEM_CARPLAY_AUTOMATIC_CONNECTION_STATUS,
            0UL,
            DATA_CONTEXT_LIST_ID_CARPLAY_AUTO_CONNECTION_SETTINGS)
         .AddData(true)
         .AddData(true);
         break;
      }
      default:
         break;
   }

   listBuilder.AddItem(
      (int)LIST_ITEM_CARPLAY_HELP,
      0UL,
      DATA_CONTEXT_LIST_ID_CARPLAY_HELP_SETTINGS)
   .AddData(true);

   switch (u32CpAskOnConnectListStatus)
   {
      case CARPLAY_ASK_ON_CONNECT_ON:
      {
         listBuilder.AddItem(
            (int)LIST_ITEM_CARPLAY_ASK_ON_CONNECT_STATUS,
            0UL,
            DATA_CONTEXT_LIST_ID_CARPLAY_ASK_ON_CONNECT_SETTINGS)
         .AddData(true)
         .AddData(false);
         break;
      }
      case CARPLAY_ASK_ON_CONNECT_OFF:
      {
         listBuilder.AddItem(
            (int)LIST_ITEM_CARPLAY_ASK_ON_CONNECT_STATUS,
            0UL,
            DATA_CONTEXT_LIST_ID_CARPLAY_ASK_ON_CONNECT_SETTINGS)
         .AddData(true)
         .AddData(true);
         break;
      }

      default:
         break;
   }
#endif
   tSharedPtrDataProvider dataProvider = listBuilder.CreateDataProvider();
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));
   return dataProvider;
}


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
/**
 * This function is responsible for Creation of Android Auto scope2 lists and triggered
 * from fillDataForAndroidAutoMainSettingsList
 * @param[in]  - input parameter void
 * @param[out] - tSharedPtrDataProvider : Shared pointer for the created list
 */

tSharedPtrDataProvider SpiSettingListHandler::updateAndroidAutoSettingsList(uint32 u32AutoConnectionStatus, uint32 u32AskOnConnectStatus)const
{
   ListDataProviderBuilder listBuilder(LIST_ID_SETTINGS_ANDROID_AUTO_MAIN, DATA_CONTEXT_LIST_ID_ANDROID_AUTO_CONNECTION_SETTINGS);
   ETG_TRACE_USR1(("SpiSettingList:updateAndroidAutoSettingsList() is called AutoLaunchListStatus : %d, AskOnConnectListStatus: %d", u32AutoConnectionStatus, u32AskOnConnectStatus));

   switch (u32AutoConnectionStatus)
   {
      case ANDROID_AUTO_AUTOLAUNCHSTATUS_ON:
      {
         listBuilder.AddItem(
            (int)LIST_ITEM_ANDROID_AUTO_AUTOMATIC_CONNECTION_STATUS,
            0UL,
            DATA_CONTEXT_LIST_ID_ANDROID_AUTO_CONNECTION_SETTINGS)
         .AddData(true)
         .AddData(false);
         break;
      }
      case ANDROID_AUTO_AUTOLAUNCHSTATUS_OFF:
      {
         listBuilder.AddItem(
            (int)LIST_ITEM_ANDROID_AUTO_AUTOMATIC_CONNECTION_STATUS,
            0UL,
            DATA_CONTEXT_LIST_ID_ANDROID_AUTO_CONNECTION_SETTINGS)
         .AddData(true)
         .AddData(true);
         break;
      }
      default:
         break;
   }

   listBuilder.AddItem(
      (int)LIST_ITEM_ANDROID_AUTO_HELP,
      0UL,
      DATA_CONTEXT_LIST_ID_ANDROID_AUTO_HELP_SETTINGS)
   .AddData(true);

   switch (u32AskOnConnectStatus)
   {
      case ANDROID_AUTO_ASK_ON_CONNECT_ON:
      {
         listBuilder.AddItem(
            (int)LIST_ITEM_ANDROID_AUTO_ASK_ON_CONNECT_STATUS,
            0UL,
            DATA_CONTEXT_LIST_ID_ANDROID_AUTO_ASK_ON_CONNECT_SETTINGS)
         .AddData(true)
         .AddData(false);
         break;
      }
      case ANDROID_AUTO_ASK_ON_CONNECT_OFF:
      {
         listBuilder.AddItem(
            (int)LIST_ITEM_ANDROID_AUTO_ASK_ON_CONNECT_STATUS,
            0UL,
            DATA_CONTEXT_LIST_ID_ANDROID_AUTO_ASK_ON_CONNECT_SETTINGS)
         .AddData(true)
         .AddData(true);
         break;
      }
      default:
         break;
   }

   tSharedPtrDataProvider dataProvider = listBuilder.CreateDataProvider();
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));
   return dataProvider;
}


/**
 * This function is responsible for Creation of INFO APPS  scope2 lists and triggered
 * from fillDataForInfoAppsList
 * @param[in]  - eDeviceCategory
 * @param[out] - tSharedPtrDataProvider : Shared pointer for the created list
 */
tSharedPtrDataProvider SpiSettingListHandler::updateInfoAppsSpiList() const
{
   ListDataProviderBuilder listBuilder(LIST_ID_SPI_INFO_APPS, DATA_CONTEXT_LIST_ID_INFO_APPS_CARPLAY);
   ETG_TRACE_USR1(("SpiSettingList:updateInfoAppsSpiList() is called"));
   if (true == KdsInfo::isCarPlayEnabled())
   {
      listBuilder.AddItem(
         (int)LIST_ITEM_INFO_APPS_CARPLAY,
         0UL,
         DATA_CONTEXT_LIST_ID_INFO_APPS_CARPLAY).AddData(true);
   }

   if (true == KdsInfo::isAndroidAutoEnabled())
   {
      listBuilder.AddItem(
         (int)LIST_ITEM_INFO_APPS_ANDROID_AUTO,
         0UL,
         DATA_CONTEXT_LIST_ID_INFO_APPS_ANDROID_AUTO).AddData(true);
   }

   tSharedPtrDataProvider dataProvider = listBuilder.CreateDataProvider();
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));
   return dataProvider;
}


#endif

/**
 * This message is sent by the Widget when a button is pressed/released.
 * This message is used here when user presses on list's elements.
 * @param[in] - oMsg gives the reaction of the Button that needs to be created
 * @param[out] - bool
 */

bool SpiSettingListHandler::onCourierMessage(const ButtonReactionMsg& oMsg)
{
   //Based on the Sender parameter of the ButtonReactionMsg we can determine if the button is part of list item inside a FlexListWidget2D.
   //For those buttons we can obtain ListId, HdlRow, HdlCol and Flags which were associated to that ListItem.
   ETG_TRACE_USR4(("Posting on ButtonReactionMsg in SPiListhandler"));
   ListProviderEventInfo info;
   if (oMsg.GetEnReaction() == enRelease)
   {
      //List ID LIST_ID_SETTINGS_CARPLAY_MAIN is applicable for Scope1 settings
      if (ListProviderEventInfo::GetItemIdentifierInfo(oMsg.GetSender(), info) && (info.getListId() == LIST_ID_SETTINGS_CARPLAY_MAIN))
      {
         return updateListIDSettingsCarPlay(info, oMsg);
         //return updateListIDSettingsCarPlayMain(info, oMsg);
      }
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      //All below mentioned List ID are applicable for Scope2 settings
      if (ListProviderEventInfo::GetItemIdentifierInfo(oMsg.GetSender(), info) && (info.getListId() == LIST_ID_SETTINGS_SPI_MAIN))
      {
         return updateListIDSettingsSpiMain(info, oMsg);
      }
      //!To update according to datapool
      if (ListProviderEventInfo::GetItemIdentifierInfo(oMsg.GetSender(), info) && (info.getListId() == LIST_ID_SETTINGS_CARPLAY))
      {
         return updateListIDSettingsCarPlay(info, oMsg);
      }
      if (ListProviderEventInfo::GetItemIdentifierInfo(oMsg.GetSender(), info) && (info.getListId() == LIST_ID_SETTINGS_ANDROID_AUTO_MAIN))
      {
         return updateListIDSettingsAndroidAutoMain(info, oMsg);
      }
      if (ListProviderEventInfo::GetItemIdentifierInfo(oMsg.GetSender(), info) && (info.getListId() == LIST_ID_SPI_INFO_APPS))
      {
         return updateListIDSpiInfoApps(info, oMsg);
      }
#endif
      return false;
   }
   else
   {
      return true;
   }
}


/**
 * This function is responsible for handling the List ButtonReactionMsgs and triggered
 * from ButtonReactionMsgs() on matching ListId and rowId
 * @param[in]  - input parameter void
 * @param[out] - void
 */

void SpiSettingListHandler::vHandleListItemPressActions(const unsigned int listId, const unsigned int hdlRow)
{
   ETG_TRACE_USR4(("SpiSettingListHandler:vHandleListItemPressActions called"));
   //To Handle Toggling AutoLaunch Case

   switch (listId)
   {
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
      case LIST_ID_SETTINGS_CARPLAY_MAIN:
      {
         if (hdlRow == LIST_ITEM_AUTOLAUNCH_STATUS)
         {
            //onToggleAutoLaunchStatus();
            startWaitAnimation();
            onToggleCarPlayAutoLaunchStatus();
         }
         if (hdlRow == LIST_ITEM_CARPLAY_ASK_ON_CONNECT)
         {
            startWaitAnimation();
            onToggleCarPlayAskOnConnectStatus();
         }
         break;
      }

#elif defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2)
      case LIST_ID_SETTINGS_CARPLAY:
      {
         if (hdlRow == LIST_ITEM_CARPLAY_ASK_ON_CONNECT_STATUS)
         {
            startWaitAnimation();
            onToggleCarPlayAskOnConnectStatus();
         }
         if (hdlRow == LIST_ITEM_CARPLAY_AUTOMATIC_CONNECTION_STATUS)
         {
            startWaitAnimation();
            onToggleCarPlayAutoLaunchStatus();
         }
      }
      break;
      case LIST_ID_SETTINGS_ANDROID_AUTO_MAIN:
      {
         if (hdlRow == LIST_ITEM_ANDROID_AUTO_ASK_ON_CONNECT_STATUS)
         {
            startWaitAnimation();
            onToggleAndroidAutoAskOnConnectStatus();
         }
         if (hdlRow == LIST_ITEM_ANDROID_AUTO_AUTOMATIC_CONNECTION_STATUS)
         {
            startWaitAnimation();
            onToggleAndroidAutoLaunchStatus();
         }
      }
      break;
#endif

      default:
         break;
   }
}


/**
 * This function is responsible for toggling the CArPlay AskOnConnect current Status and updating the Lists and datapool status.
 * This is triggered from vHandleListItemPressActions() on matching ListId and rowId
 * @param[in]  - input parameter void
 * @param[out] - tSharedPtrDataProvider
 */

void SpiSettingListHandler::onToggleCarPlayAskOnConnectStatus()
{
   if (_mDpHandler != NULL)
   {
      ETG_TRACE_USR4(("SpiSettingListHandler:received onToggleCarPlayAskOnConnectStatus() AskOnConnectStatus = %d AutoLaunchConnectStatus = %d", _mDpHandler->getDpSpiCarPlayAskOnConnect(), _mDpHandler->getDpSpiCarPlayAutoLaunch()));
      uint32 u32AskOnConnectCurr = _mDpHandler->getDpSpiCarPlayAskOnConnect();
      _mUpdateTriggerSettDevType = DeviceType__APPLE_DEVICE;
      //!Toggle AskOnConnect status
      switch (u32AskOnConnectCurr)
      {
         case CARPLAY_ASK_ON_CONNECT_ON:
         {
            _mSpiSettingStatus = DIPO_ASK_ON_CONNECT_OFF;
            if (CARPLAY_AUTOLAUNCHSTATUS_OFF == _mDpHandler->getDpSpiCarPlayAutoLaunch())
            {
               setSpiSettingPreference(DeviceCategory__DEV_TYPE_DIPO, T_e8_EnabledInfo__USAGE_DISABLED);
            }
            else
            {
               setSpiSettingPreference(DeviceCategory__DEV_TYPE_DIPO, T_e8_EnabledInfo__USAGE_ENABLED);
            }
            break;
         }
         case CARPLAY_ASK_ON_CONNECT_OFF:
         {
            _mSpiSettingStatus = DIPO_ASK_ON_CONNECT_ON;
            setSpiSettingPreference(DeviceCategory__DEV_TYPE_DIPO, T_e8_EnabledInfo__USAGE_CONF_REQD);
            break;
         }
         default:
            break;
      }
   }
}


/**
 * This function is responsible for toggling the CArPlay AutoLaunch current Status and updating the Lists and datapool status.
 * This is triggered from vHandleListItemPressActions() on matching ListId and rowId
 * @param[in]  - input parameter void
 * @param[out] - tSharedPtrDataProvider
 */

void SpiSettingListHandler::onToggleCarPlayAutoLaunchStatus()
{
   if (_mDpHandler != NULL)
   {
      ETG_TRACE_USR4(("SpiSettingListHandler:received onToggleCarPlayAutoLaunchStatus() AskOnConnectStatus = %d AutoLaunchStatus = %d ", _mDpHandler->getDpSpiCarPlayAskOnConnect(), _mDpHandler->getDpSpiCarPlayAutoLaunch()));
      uint32 u32AutoLaunchCurr = _mDpHandler->getDpSpiCarPlayAutoLaunch();
      _mUpdateTriggerSettDevType = DeviceType__APPLE_DEVICE;
      //!Toggle AutoLaunch status

      switch (u32AutoLaunchCurr)
      {
         case CARPLAY_AUTOLAUNCHSTATUS_ON:
         {
            if (CARPLAY_ASK_ON_CONNECT_OFF == _mDpHandler->getDpSpiCarPlayAskOnConnect())
            {
               _mSpiSettingStatus = DIPO_AUTOLAUNCHSTATUS_OFF;
               setSpiSettingPreference(DeviceCategory__DEV_TYPE_DIPO, T_e8_EnabledInfo__USAGE_DISABLED);
            }
            else
            {
               _mDpHandler->setDpSpiCarPlayAutoLaunch(CARPLAY_AUTOLAUNCHSTATUS_OFF);
               callUpdateCarPlaySettinglist();
            }
            break;
         }
         case CARPLAY_AUTOLAUNCHSTATUS_OFF:
         {
            if (CARPLAY_ASK_ON_CONNECT_OFF == _mDpHandler->getDpSpiCarPlayAskOnConnect())
            {
               _mSpiSettingStatus = DIPO_AUTOLAUNCHSTATUS_ON;
               setSpiSettingPreference(DeviceCategory__DEV_TYPE_DIPO, T_e8_EnabledInfo__USAGE_ENABLED);
            }
            else
            {
               _mDpHandler->setDpSpiCarPlayAutoLaunch(CARPLAY_AUTOLAUNCHSTATUS_ON);
               callUpdateCarPlaySettinglist();
            }
            break;
         }
         default:
            break;
      }
   }
}


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
/**
 * This function is responsible for toggling the AndroidAuto AskOnConnect current Status and updating the Lists and datapool status.
 * This is triggered from vHandleListItemPressActions() on matching ListId and rowId
 * @param[in]  - input parameter void
 * @param[out] - tSharedPtrDataProvider
 */

void SpiSettingListHandler::onToggleAndroidAutoAskOnConnectStatus()
{
   if (_mDpHandler != NULL)
   {
      ETG_TRACE_USR4(("SpiSettingListHandler:received onToggleAndroidAutoAskOnConnectStatus() AndroidAutoAskOnConnect = %d AndroidAutoAutoLaunch =%d ", _mDpHandler->getDpAndroidAutoAskOnConnect(), _mDpHandler->getDpAndroidAutoLaunch()));
      uint32 u32AskOnConnectCurr = _mDpHandler->getDpAndroidAutoAskOnConnect();
      _mUpdateTriggerSettDevType = DeviceType__ANDROID_DEVICE;
      //!Toggle AskOnConnect status

      switch (u32AskOnConnectCurr)
      {
         case ANDROID_AUTO_ASK_ON_CONNECT_ON:
         {
            _mSpiSettingStatus = AAP_ASK_ON_CONNECT_OFF;
            if (ANDROID_AUTO_AUTOLAUNCHSTATUS_OFF == _mDpHandler->getDpAndroidAutoLaunch())
            {
               setSpiSettingPreference(DeviceCategory__DEV_TYPE_ANDROIDAUTO, T_e8_EnabledInfo__USAGE_DISABLED);
            }
            else
            {
               setSpiSettingPreference(DeviceCategory__DEV_TYPE_DIPO, T_e8_EnabledInfo__USAGE_ENABLED);
            }
            break;
         }
         case ANDROID_AUTO_ASK_ON_CONNECT_OFF:
         {
            _mSpiSettingStatus = AAP_ASK_ON_CONNECT_ON;
            setSpiSettingPreference(DeviceCategory__DEV_TYPE_ANDROIDAUTO, T_e8_EnabledInfo__USAGE_CONF_REQD);
            break;
         }
         default:
            break;
      }
   }
}


/**
 * This function is responsible for toggling the AndroidAuto AutoLaunch current Status and updating the Lists and datapool status.
 * This is triggered from vHandleListItemPressActions() on matching ListId and rowId
 * @param[in]  - input parameter void
 * @param[out] - tSharedPtrDataProvider
 */

void SpiSettingListHandler::onToggleAndroidAutoLaunchStatus()
{
   if (_mDpHandler != NULL)
   {
      ETG_TRACE_USR4(("SpiSettingListHandler:received onToggleAndroidAutoAskOnConnectStatus() AndroidAutoAskOnConnect = %d AndroidAutoAutoLaunch =%d ", _mDpHandler->getDpAndroidAutoAskOnConnect(), _mDpHandler->getDpAndroidAutoLaunch()));
      uint32 u32AutoLaunchCurr = _mDpHandler->getDpAndroidAutoLaunch();
      _mUpdateTriggerSettDevType = DeviceType__ANDROID_DEVICE;
      //!Toggle AutoLaunch status
      switch (u32AutoLaunchCurr)
      {
         case ANDROID_AUTO_AUTOLAUNCHSTATUS_ON:
         {
            if (ANDROID_AUTO_ASK_ON_CONNECT_OFF == _mDpHandler->getDpAndroidAutoAskOnConnect())
            {
               _mSpiSettingStatus = AAP_AUTO_AUTOLAUNCHSTATUS_OFF;
               setSpiSettingPreference(DeviceCategory__DEV_TYPE_ANDROIDAUTO, T_e8_EnabledInfo__USAGE_DISABLED);
            }
            else
            {
               _mDpHandler->setDpAndroidAutoLaunch(ANDROID_AUTO_AUTOLAUNCHSTATUS_OFF);
               callUpdateAndriodAutoSettinglist();
            }
            break;
         }
         case ANDROID_AUTO_AUTOLAUNCHSTATUS_OFF:
         {
            if (ANDROID_AUTO_ASK_ON_CONNECT_OFF == _mDpHandler->getDpAndroidAutoAskOnConnect())
            {
               _mSpiSettingStatus = AAP_AUTO_AUTOLAUNCHSTATUS_ON;
               setSpiSettingPreference(DeviceCategory__DEV_TYPE_ANDROIDAUTO, T_e8_EnabledInfo__USAGE_ENABLED);
            }
            else
            {
               _mDpHandler->setDpAndroidAutoLaunch(ANDROID_AUTO_AUTOLAUNCHSTATUS_ON);
               callUpdateAndriodAutoSettinglist();
            }
            break;
         }
         default:
            break;
      }
   }
}


/**
 * This message is sent by the Widget when a button is pressed/released.
 * This message is used here when user presses on list's elements.
 * @param[in] - oMsg gives the reaction of the Button that needs to be created and ListProviderEventInfo
 * @param[out] - bool
 */
bool SpiSettingListHandler::updateListIDSettingsSpiMain(ListProviderEventInfo& info, const ButtonReactionMsg& oMsg)
{
   ETG_TRACE_USR4(("Posting ButtonReactionMsg for SPI SettingListhandler"));
   unsigned int listId      = info.getListId();     // the list id for generic access
   unsigned int hdlRow      = info.getHdlRow();     // normaly the index
   unsigned int hdlCol      = info.getHdlCol();     // if more than 1 active element in one list row, e.g. Button in a button line

   ETG_TRACE_USR4(("Spi Settings:ButtonReactionMsg ListId : %d, HdlRow : %d, hdlCol : %d", listId, hdlRow, hdlCol));
   switch (info.getHdlRow())
   {
      case LIST_ITEM_CARPLAY_SETTINGS:
      {
         ETG_TRACE_USR4(("SpiSettings:list id : %d ,row : %d", info.getListId(), info.getHdlRow()));
         POST_MSG((COURIER_MESSAGE_NEW(ButtonListItemUpdMsg)(listId, hdlRow, hdlCol, oMsg.GetEnReaction())));
         return true;
         break;
      }
      case LIST_ITEM_ANDROID_AUTO_SETTINGS:
      {
         ETG_TRACE_USR4(("SpiSettings:list id : %d ,row : %d", info.getListId(), info.getHdlRow()));
         POST_MSG((COURIER_MESSAGE_NEW(ButtonListItemUpdMsg)(listId, hdlRow, hdlCol, oMsg.GetEnReaction())));
         return true;
         break;
      }
      default:
         break;
   }
   return false;
}


#endif

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
/**
 * This message is received on press of Don't ask again button on disclaimer Popup.
 * This function is updating the list element and datapool status.
 * @param[in]  - input parameter void
 * @param[out] - bool
 */

bool SpiSettingListHandler::onCourierMessage(const AutoLaunchConnectOnUpdateMsg& /*msg*/)
{
   ETG_TRACE_USR4(("GUI->HALL: onCourierMessage: AutoLaunchConnectOnUpdateMsg Received"));
   uint32 u32CpAutoLaunchListStatus = CARPLAY_AUTOLAUNCHSTATUS_NOT_DEFINED;
   uint32 u32CpAskOnConnectListStatus = CARPLAY_ASK_ON_CONNECT_NOT_DEFINED;
   if (_mDpHandler != NULL)
   {
      if (_mDpHandler->getDpSpiCarPlayAutoLaunch() == CARPLAY_AUTOLAUNCHSTATUS_OFF)
      {
         _mDpHandler->setDpSpiCarPlayAutoLaunch(CARPLAY_AUTOLAUNCHSTATUS_ON);
      }
      _mDpHandler->setDpSpiCarPlayAskOnConnect(CARPLAY_ASK_ON_CONNECT_OFF);
      u32CpAutoLaunchListStatus = _mDpHandler->getDpSpiCarPlayAutoLaunch();
      u32CpAskOnConnectListStatus = _mDpHandler->getDpSpiCarPlayAskOnConnect();
   }
   updateSpiCarPlayMainSettingsList(u32CpAutoLaunchListStatus, u32CpAskOnConnectListStatus);
   ETG_TRACE_USR1(("SpiSettingListHandler::onCourierMessage() is called CpAutoLaunchListStatus : %d, CpAskOnConnectListStatus: %d", u32CpAutoLaunchListStatus, u32CpAskOnConnectListStatus));

   return true;
}


/**
 * This message is received on press of Yes button on Remember user settings Popup
 * This function is updating the list element and datapool status.
 * @param[in]  - input parameter void
 * @param[out] - bool
 */
bool SpiSettingListHandler::onCourierMessage(const RememberUsrChoiceOnNoCarPlayMsg& /*msg*/)
{
   ETG_TRACE_USR4(("GUI->HALL: onCourierMessage: RememberUsrChoiceOnCarPlayLaunchMsg Received"));
   uint32 u32CpAutoLaunchListStatus = CARPLAY_AUTOLAUNCHSTATUS_NOT_DEFINED;
   uint32 u32CpAskOnConnectListStatus = CARPLAY_ASK_ON_CONNECT_NOT_DEFINED;
   if (_mDpHandler != NULL)
   {
      if (_mDpHandler->getDpSpiCarPlayAutoLaunch() == CARPLAY_AUTOLAUNCHSTATUS_ON)
      {
         _mDpHandler->setDpSpiCarPlayAutoLaunch(CARPLAY_AUTOLAUNCHSTATUS_OFF);
      }
      _mDpHandler->setDpSpiCarPlayAskOnConnect(CARPLAY_ASK_ON_CONNECT_OFF);
      u32CpAutoLaunchListStatus = _mDpHandler->getDpSpiCarPlayAutoLaunch();
      u32CpAskOnConnectListStatus = _mDpHandler->getDpSpiCarPlayAskOnConnect();
   }
   updateSpiCarPlayMainSettingsList(u32CpAutoLaunchListStatus, u32CpAskOnConnectListStatus);
   ETG_TRACE_USR1(("SpiSettingListHandler::onCourierMessage() is called CpAutoLaunchListStatus : %d, CpAskOnConnectListStatus: %d", u32CpAutoLaunchListStatus, u32CpAskOnConnectListStatus));
   return true;
}


#endif
/**
 * This message is sent by the Widget when a button is pressed/released.
 * This message is used here when user presses on list's elements.
 * @param[in] - oMsg gives the reaction of the Button that needs to be created and ListProviderEventInfo
 * @param[out] - bool
 */
bool SpiSettingListHandler::updateListIDSettingsCarPlay(ListProviderEventInfo& info, const ButtonReactionMsg& oMsg)
{
   ETG_TRACE_USR4(("Posting ButtonReactionMsg for SPI SettingListhandler"));
   unsigned int listId      = info.getListId();     // the list id for generic access
   unsigned int hdlRow      = info.getHdlRow();     // normaly the index
   unsigned int hdlCol      = info.getHdlCol();     // if more than 1 active element in one list row, e.g. Button in a button line

   ETG_TRACE_USR4(("Spi Settings:ButtonReactionMsg ListId : %d, HdlRow : %d, hdlCol : %d", listId, hdlRow, hdlCol));
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
   switch (info.getHdlRow())
   {
      case LIST_ITEM_AUTOLAUNCH_STATUS:
      {
         //!Update CarPlayAutoLaunch Status
         ETG_TRACE_USR4(("SpiSettings:list id : %d ,row : %d", info.getListId(), info.getHdlRow()));
         vHandleListItemPressActions(listId, hdlRow);
         return true;
         break;
      }
      case LIST_ITEM_CARPLAY_TUTORIAL:
      {
         ETG_TRACE_USR4(("SpiSettings:list id : %d ,row : %d", info.getListId(), info.getHdlRow()));
         POST_MSG((COURIER_MESSAGE_NEW(ButtonListItemUpdMsg)(listId, hdlRow, hdlCol, oMsg.GetEnReaction())));
         return true;
         break;
      }
      case LIST_ITEM_CARPLAY_ASK_ON_CONNECT:
      {
         //!Update CarPlayAskOnCopnnect Status
         ETG_TRACE_USR4(("SpiSettings:list id : %d ,row : %d", info.getListId(), info.getHdlRow()));
         vHandleListItemPressActions(listId, hdlRow);
         return true;
         break;
      }
      default:
         break;
   }

#elif defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2)
   switch (info.getHdlRow())
   {
      case LIST_ITEM_CARPLAY_AUTOMATIC_CONNECTION_STATUS:
      {
         //!Update CarPlayAutoLaunch Status
         ETG_TRACE_USR4(("SpiSettings:list id : %d ,row : %d", info.getListId(), info.getHdlRow()));
         vHandleListItemPressActions(listId, hdlRow);
         return true;
         break;
      }
      case LIST_ITEM_CARPLAY_HELP:
      {
         ETG_TRACE_USR4(("SpiSettings:list id : %d ,row : %d", info.getListId(), info.getHdlRow()));
         POST_MSG((COURIER_MESSAGE_NEW(ButtonListItemUpdMsg)(listId, hdlRow, hdlCol, oMsg.GetEnReaction())));
         return true;
         break;
      }
      case LIST_ITEM_CARPLAY_ASK_ON_CONNECT_STATUS:
      {
         //!Update CarPlayAskOnCopnnect Status
         ETG_TRACE_USR4(("SpiSettings:list id : %d ,row : %d", info.getListId(), info.getHdlRow()));
         vHandleListItemPressActions(listId, hdlRow);
         return true;
         break;
      }
      default:
         break;
   }
#endif
   return false;
}


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
/**
 * This message is sent by the Widget when a button is pressed/released.
 * This message is used here when user presses on list's elements.
 * @param[in] - oMsg gives the reaction of the Button that needs to be created and ListProviderEventInfo
 * @param[out] - bool
 */
bool SpiSettingListHandler::updateListIDSettingsAndroidAutoMain(ListProviderEventInfo& info, const ButtonReactionMsg& oMsg)
{
   ETG_TRACE_USR4(("Posting ButtonReactionMsg for AndroidAuto SettingListhandler"));
   unsigned int listId      = info.getListId();     // the list id for generic access
   unsigned int hdlRow      = info.getHdlRow();     // normaly the index
   unsigned int hdlCol      = info.getHdlCol();     // if more than 1 active element in one list row, e.g. Button in a button line

   ETG_TRACE_USR4(("Spi Settings:ButtonReactionMsg ListId : %d, HdlRow : %d, hdlCol : %d", listId, hdlRow, hdlCol));

   switch (info.getHdlRow())
   {
      case LIST_ITEM_ANDROID_AUTO_AUTOMATIC_CONNECTION_STATUS:
      {
         //!Update Android AutoLaunch Status
         ETG_TRACE_USR4(("SpiSettings:list id : %d ,row : %d", info.getListId(), info.getHdlRow()));
         vHandleListItemPressActions(listId, hdlRow);
         return true;
      }
      break;
      case LIST_ITEM_ANDROID_AUTO_HELP:
      {
         ETG_TRACE_USR4(("SpiSettings:list id : %d ,row : %d", info.getListId(), info.getHdlRow()));
         POST_MSG((COURIER_MESSAGE_NEW(ButtonListItemUpdMsg)(listId, hdlRow, hdlCol, oMsg.GetEnReaction())));
         return true;
      }
      break;
      case LIST_ITEM_ANDROID_AUTO_ASK_ON_CONNECT_STATUS:
      {
         //!Update CarPlayAskOnCopnnect Status
         ETG_TRACE_USR4(("SpiSettings:list id : %d ,row : %d", info.getListId(), info.getHdlRow()));
         vHandleListItemPressActions(listId, hdlRow);
         return true;
      }
      break;
      default:
         break;
   }

   return false;
}


/**
 * This message is sent by the Widget when a button is pressed/released.
 * This message is used here when user presses on list's elements.
 * @param[in] - oMsg gives the reaction of the Button that needs to be created and ListProviderEventInfo
 * @param[out] - bool
 */
bool SpiSettingListHandler::updateListIDSpiInfoApps(ListProviderEventInfo& info, const ButtonReactionMsg& oMsg)
{
   ETG_TRACE_USR4(("Posting ButtonReactionMsg for SPI SettingListhandler"));
   unsigned int listId      = info.getListId();     // the list id for generic access
   unsigned int hdlRow      = info.getHdlRow();     // normaly the index
   unsigned int hdlCol      = info.getHdlCol();     // if more than 1 active element in one list row, e.g. Button in a button line

   ETG_TRACE_USR4(("Spi Settings:ButtonReactionMsg ListId : %d, HdlRow : %d, hdlCol : %d", listId, hdlRow, hdlCol));

   switch (info.getHdlRow())
   {
      case LIST_ITEM_INFO_APPS_ANDROID_AUTO:
      {
         ETG_TRACE_USR4(("SpiSettings:list id : %d ,row : %d", info.getListId(), info.getHdlRow()));
         POST_MSG((COURIER_MESSAGE_NEW(ButtonListItemUpdMsg)(listId, hdlRow, hdlCol, oMsg.GetEnReaction())));
         return true;
      }
      break;
      case LIST_ITEM_INFO_APPS_CARPLAY:
      {
         ETG_TRACE_USR4(("SpiSettings:list id : %d ,row : %d", info.getListId(), info.getHdlRow()));
         POST_MSG((COURIER_MESSAGE_NEW(ButtonListItemUpdMsg)(listId, hdlRow, hdlCol, oMsg.GetEnReaction())));
         return true;
      }
      break;
      default:
         break;
   }

   return false;
}


/**
 * This CourierMessage will be received onButtonYes when don't remind button is enabled
 * This is to update the Apps Settings.
 * @param[in] - None
 * @param[out] - None
 */

bool SpiSettingListHandler::onCourierMessage(const UpdateAPPSSettingsOnYesReqMsg& /*oMsg*/)
{
   ETG_TRACE_USR4(("GUI->HALL: onCourierMessage::UpdateAPPSSettingsOnYesReqMsg Received"));
   if (::midw_smartphoneint_fi_types::T_e8_DeviceType__APPLE_DEVICE	== _mpSpiConnectionHandling->getConnDeviceType())
   {
      if (_mDpHandler != NULL)
      {
         if (_mDpHandler->getDpSpiCarPlayAutoLaunch()	== CARPLAY_AUTOLAUNCHSTATUS_OFF)
         {
            _mDpHandler->setDpSpiCarPlayAutoLaunch(CARPLAY_AUTOLAUNCHSTATUS_ON);
         }
         _mDpHandler->setDpSpiCarPlayAskOnConnect(CARPLAY_ASK_ON_CONNECT_OFF);
      }
      setSpiSettingPreference(DeviceCategory__DEV_TYPE_DIPO, T_e8_EnabledInfo__USAGE_ENABLED);
      fillDataForCarPlayMainSettingsList();
   }
   else if (::midw_smartphoneint_fi_types::T_e8_DeviceType__ANDROID_DEVICE == _mpSpiConnectionHandling->getConnDeviceType())
   {
      if (_mDpHandler != NULL)
      {
         if (_mDpHandler->getDpAndroidAutoLaunch() == ANDROID_AUTO_AUTOLAUNCHSTATUS_OFF)
         {
            _mDpHandler->setDpAndroidAutoLaunch(ANDROID_AUTO_AUTOLAUNCHSTATUS_ON);
         }
         _mDpHandler->setDpAndroidAutoAskOnConnect(ANDROID_AUTO_ASK_ON_CONNECT_OFF);
      }
      setSpiSettingPreference(DeviceCategory__DEV_TYPE_ANDROIDAUTO, T_e8_EnabledInfo__USAGE_ENABLED);
      fillDataForAndroidAutoMainSettingsList();
   }
   return true;
}


/**
 * This CourierMessage will be received onButtonNo when don't remind button is enabled
 * This is to update the Apps Settings.
 * @param[in] - None
 * @param[out] - None
 */

bool SpiSettingListHandler::onCourierMessage(const UpdateAPPSSettingsOnNOReqMsg& /*oMsg*/)
{
   ETG_TRACE_USR4(("GUI->HALL: onCourierMessage::UpdateAPPSSettingsOnNOReqMsg Received"));
   if (::midw_smartphoneint_fi_types::T_e8_DeviceType__APPLE_DEVICE == _mpSpiConnectionHandling->getConnDeviceType())
   {
      if (_mDpHandler != NULL)
      {
         if (_mDpHandler->getDpSpiCarPlayAutoLaunch() == CARPLAY_AUTOLAUNCHSTATUS_ON)
         {
            _mDpHandler->setDpSpiCarPlayAutoLaunch(CARPLAY_AUTOLAUNCHSTATUS_OFF);
         }
         _mDpHandler->setDpSpiCarPlayAskOnConnect(CARPLAY_ASK_ON_CONNECT_OFF);
      }
      fillDataForCarPlayMainSettingsList();
      setSpiSettingPreference(DeviceCategory__DEV_TYPE_DIPO, T_e8_EnabledInfo__USAGE_DISABLED);
   }
   else if (::midw_smartphoneint_fi_types::T_e8_DeviceType__ANDROID_DEVICE == _mpSpiConnectionHandling->getConnDeviceType())
   {
      if (_mDpHandler != NULL)
      {
         if (_mDpHandler->getDpAndroidAutoLaunch() == ANDROID_AUTO_AUTOLAUNCHSTATUS_ON)
         {
            _mDpHandler->setDpAndroidAutoLaunch(ANDROID_AUTO_AUTOLAUNCHSTATUS_OFF);
         }
         _mDpHandler->setDpAndroidAutoAskOnConnect(ANDROID_AUTO_ASK_ON_CONNECT_OFF);
      }
      setSpiSettingPreference(DeviceCategory__DEV_TYPE_ANDROIDAUTO, T_e8_EnabledInfo__USAGE_DISABLED);
      fillDataForAndroidAutoMainSettingsList();
   }
   return true;
}


#endif


/**
 * updateSpiSettinglistOnRestoreFactotySetting : This function is called
 * on press of restore factory setting to update the list .
 * @param[in] - None
 * @param[out] - None
 */
void SpiSettingListHandler::updateSpiSettinglistOnRestoreFactotySetting()
{
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
   if (_mDpHandler != NULL)
   {
      updateSpiCarPlayMainSettingsList(
         _mDpHandler->getDpSpiCarPlayAutoLaunch(),
         _mDpHandler->getDpSpiCarPlayAskOnConnect());
      ETG_TRACE_USR4(("SpiSettingslist OnRestoreFactotySetting Scope-1 Autolaunch status : %d", _mDpHandler->getDpSpiCarPlayAutoLaunch()));
   }
#elif defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2)
   if (_mDpHandler != NULL)
   {
      updateSpiCarPlayMainSettingsList(_mDpHandler->getDpSpiCarPlayAutoLaunch(), _mDpHandler->getDpSpiCarPlayAskOnConnect());
      ETG_TRACE_USR4(("SpiSettingslist OnRestoreFactotySetting Scope-2 DIPO Autolaunch status: %d AskOnConnect status : %d", _mDpHandler->getDpSpiCarPlayAutoLaunch(), _mDpHandler->getDpSpiCarPlayAskOnConnect()));
      updateAndroidAutoSettingsList(_mDpHandler->getDpAndroidAutoLaunch(), _mDpHandler->getDpAndroidAutoAskOnConnect());
      ETG_TRACE_USR4(("SpiSettingslist OnRestoreFactotySetting Scope-2 AAP Autolaunch status: %d AskOnConnect status : %d", _mDpHandler->getDpAndroidAutoLaunch(), _mDpHandler->getDpAndroidAutoAskOnConnect()));
   }
#endif
}


/**
 * updateHMISettingOnUserAction : This function is called
 * to change the HMI datapool and performs action based on
 * setting and device connection status.
 * @param[in] - Settingsupdate Status
 * @param[out] - None
 */
void SpiSettingListHandler::updateHMISettingOnUserAction(enSpiSettingUpdateStatus settingUpdateStatus)
{
   ETG_TRACE_USR4(("SpiSettingListHandler: updateHMISettingOnUserAction settingUpdateStatus = %d , _mUpdateTriggerSettDevType = %d " , ETG_CENUM(enSpiSettingUpdateStatus, settingUpdateStatus) , ETG_CENUM(enDeviceType, _mUpdateTriggerSettDevType)));
   if (_mDpHandler != NULL)
   {
      switch (settingUpdateStatus)
      {
         case DIPO_ASK_ON_CONNECT_OFF :
         {
            _mDpHandler->setDpSpiCarPlayAskOnConnect(CARPLAY_ASK_ON_CONNECT_OFF);
            break;
         }
         case DIPO_ASK_ON_CONNECT_ON :
         {
            _mDpHandler->setDpSpiCarPlayAskOnConnect(CARPLAY_ASK_ON_CONNECT_ON);
            break;
         }
         case DIPO_AUTOLAUNCHSTATUS_OFF :
         {
            _mDpHandler->setDpSpiCarPlayAutoLaunch(CARPLAY_AUTOLAUNCHSTATUS_OFF);
            break;
         }
         case DIPO_AUTOLAUNCHSTATUS_ON :
         {
            _mDpHandler->setDpSpiCarPlayAutoLaunch(CARPLAY_AUTOLAUNCHSTATUS_ON);
            break;
         }
         case AAP_ASK_ON_CONNECT_OFF :
         {
            _mDpHandler->setDpAndroidAutoAskOnConnect(ANDROID_AUTO_ASK_ON_CONNECT_OFF);
            break;
         }
         case AAP_ASK_ON_CONNECT_ON :
         {
            _mDpHandler->setDpAndroidAutoAskOnConnect(ANDROID_AUTO_ASK_ON_CONNECT_ON);
            break;
         }
         case AAP_AUTO_AUTOLAUNCHSTATUS_OFF :
         {
            _mDpHandler->setDpAndroidAutoLaunch(ANDROID_AUTO_AUTOLAUNCHSTATUS_OFF);
            break;
         }
         case AAP_AUTO_AUTOLAUNCHSTATUS_ON :
         {
            _mDpHandler->setDpAndroidAutoLaunch(ANDROID_AUTO_AUTOLAUNCHSTATUS_ON);
            break;
         }
         case DIPO_AAP_NO_UPDATE :
         default :
            break;
      }
      if (DeviceType__APPLE_DEVICE == _mUpdateTriggerSettDevType)
      {
         _mSpiSettingStatus = DIPO_AAP_NO_UPDATE;
         if (true == _mpSpiConnectionHandling->isDipoDeviceConnected())
         {
            _mIsDeviceselectinReq = (settingUpdateStatus == DIPO_AUTOLAUNCHSTATUS_ON) ? true : false ;
            callUpdateCarPlaySettinglist();
            if (true == _mDpHandler->getDpSpiCarPlayAskOnConnect())
            {
               //Irrespective of autolaunch show disclaimer popup, once setting is made from OFF to ON
               _mpSpiConnectionHandling->setSettingTrigDisc(true);
               _mpSpiConnectionHandling->vSpiDeviceConnectionPopUp(convConnDeviceCategoryToType(_mpSpiConnectionHandling->getActiveDeviceCategory()));
            }
            else if (true == _mDpHandler->getDpSpiCarPlayAutoLaunch() && (true ==  _mIsDeviceselectinReq))
            {
               _mpSpiConnectionHandling->sendSelectDevice(_mpSpiConnectionHandling->getActiveDeviceHandle(), _mpSpiConnectionHandling->getdeviceConnectionType(), DeviceConnectionReq__DEV_CONNECT, EnabledInfo__USAGE_ENABLED);
            }
         }
         else
         {
            callUpdateCarPlaySettinglist();
         }
      }
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      else if (DeviceType__ANDROID_DEVICE == _mUpdateTriggerSettDevType)
      {
         _mSpiSettingStatus = DIPO_AAP_NO_UPDATE;
         if (true == _mpSpiConnectionHandling->isAndroidDeviceConnected())
         {
            _mIsDeviceselectinReq = (settingUpdateStatus == AAP_AUTO_AUTOLAUNCHSTATUS_ON) ? true : false ;
            callUpdateAndriodAutoSettinglist();
            if (true == _mDpHandler->getDpAndroidAutoAskOnConnect())
            {
               _mpSpiConnectionHandling->setSettingTrigDisc(true);
               _mpSpiConnectionHandling->vSpiDeviceConnectionPopUp(convConnDeviceCategoryToType(_mpSpiConnectionHandling->getActiveDeviceCategory()));
            }
            else if (true == _mDpHandler->getDpAndroidAutoLaunch() && (true ==  _mIsDeviceselectinReq))
            {
               _mpSpiConnectionHandling->sendSelectDevice(_mpSpiConnectionHandling->getActiveDeviceHandle(), _mpSpiConnectionHandling->getdeviceConnectionType(), DeviceConnectionReq__DEV_CONNECT, EnabledInfo__USAGE_ENABLED);
            }
         }
         else
         {
            callUpdateAndriodAutoSettinglist();
         }
      }
#endif
   }
}


/**
 * callUpdateCarPlaySettinglist : This function is called
 * to update the CarPlaysettinglist .
 * @param[in] - None
 * @param[out] - None
 */
void SpiSettingListHandler::callUpdateCarPlaySettinglist()
{
   if (_mDpHandler != NULL)
   {
      updateSpiCarPlayMainSettingsList(_mDpHandler->getDpSpiCarPlayAutoLaunch(), _mDpHandler->getDpSpiCarPlayAskOnConnect());
   }
   _mUpdateTriggerSettDevType = DeviceType__DEVICE_TYPE_UNKNOWN;
   if (false == _mIsDeviceselectinReq)
   {
      stopWaitAnimation();
   }
}


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
/**
 * callUpdateAndriodAutoSettinglist : This function is called
 * to update the callUpdateAndriodAutoSettinglist .
 * @param[in] - None
 * @param[out] - None
 */
void SpiSettingListHandler::callUpdateAndriodAutoSettinglist()
{
   if (_mDpHandler != NULL)
   {
      updateAndroidAutoSettingsList(_mDpHandler->getDpAndroidAutoLaunch(), _mDpHandler->getDpAndroidAutoAskOnConnect());
   }
   _mUpdateTriggerSettDevType = DeviceType__DEVICE_TYPE_UNKNOWN;
   if (false == _mIsDeviceselectinReq)
   {
      stopWaitAnimation();
   }
}


#endif

/**
 * handleSelectDeviceResult : This function is called
 * when selectdevice result received .
 * @param[in] - deviceselectionstatus
 * @param[out] - None
 */
void SpiSettingListHandler::handleSelectDeviceResult(::midw_smartphoneint_fi_types::T_e8_ResponseCode deviceSelectionStatus)
{
   ETG_TRACE_USR4(("SpiSettingListHandler: handleSelectDeviceResult = %d " , deviceSelectionStatus));
   if (true == _mIsDeviceselectinReq)
   {
      _mIsDeviceselectinReq = false;
      stopWaitAnimation();
   }
}


/**
 * convConnDeviceCategoryToType : This function is called
 * to convert device category to device type .
 * @param[in] - devicecategory
 * @param[out] - devicetype
 */
::midw_smartphoneint_fi_types::T_e8_DeviceType SpiSettingListHandler::convConnDeviceCategoryToType(::midw_smartphoneint_fi_types::T_e8_DeviceCategory deviceCategory)
{
   ETG_TRACE_USR4(("SpiSettingListHandler: convConnDeviceCategoryToType called DeviceCategory = %d ", deviceCategory));
   ::midw_smartphoneint_fi_types::T_e8_DeviceType connDeviceType = T_e8_DeviceType__DEVICE_TYPE_UNKNOWN ;
   switch (deviceCategory)
   {
      case DeviceCategory__DEV_TYPE_DIPO :
      {
         connDeviceType = T_e8_DeviceType__APPLE_DEVICE;
         break;
      }
      case DeviceCategory__DEV_TYPE_ANDROIDAUTO :
      {
         connDeviceType = T_e8_DeviceType__ANDROID_DEVICE;
         break;
      }
      default:
         break;
   }
   return connDeviceType;
}


/**
 * startWaitAnimation : This function is called
 * to start wait animation .
 * @param[in] - None
 * @param[out] - None
 */
void SpiSettingListHandler::startWaitAnimation()
{
   POST_MSG((COURIER_MESSAGE_NEW(SpiWaitAnimationReqMsg)(ANIMATION_START)));
}


/**
 * stopWaitAnimation : This function is called
 * to stop wait animation .
 * @param[in] - None
 * @param[out] - None
 */
void SpiSettingListHandler::stopWaitAnimation()
{
   POST_MSG((COURIER_MESSAGE_NEW(SpiWaitAnimationReqMsg)(ANIMATION_STOP)));
}


/*
 * This function is implemented to trigger courier message through TTfis command for settings screen
 * @param[in]  - u8ListSettings to sent the courier message
 *  description for u8ListSettings:
 *  value 1- sent the courier message 1 to trigger settings screen
 *  value 2- sent the courier message 2 to trigger CarPlay Tutorial screen screen
 * @return     - Void
 */

ETG_I_CMD_DEFINE((traceCmd_CarPlaySettingsList, "CarPlaySettingsList %u", uint8)) // Trace class declaration
void SpiSettingListHandler::traceCmd_CarPlaySettingsList(uint8 u8ListSettings)
{
   ETG_TRACE_USR4(("traceCmd_CarPlaySettingsList ListSettings=%d", u8ListSettings));

   if (u8ListSettings == 1)
   {
      if (pstaticSpiSettingListHandler != NULL)
      {
         pstaticSpiSettingListHandler->triggerCourierMsg(1);
      }
   }
}


/*
 * This function is implemented to trigger courier message through TTfis command for settings screen
 * @param[in]  - u8triggerId to sent the courier message
 *  description for u8ListSettings:
 *  value 1- Go to Settings screen
 *  value 2- Go to Carplay tutorial screen
 * @return     - Void
 */

void SpiSettingListHandler::triggerCourierMsg(uint8 u8triggerId)
{
   switch (u8triggerId)
   {
      case 1:
      {
         POST_MSG((COURIER_MESSAGE_NEW(SettingsCarPlayReqMsg)()));
         ETG_TRACE_USR4(("HALL->GUI:traceCmd_CarPlaySettingsList ListSettings=%d", u8triggerId));
         break;
      }

      default:
         break;
   }
}


} //end of namespace Core
} //end of namespace App
