/**
 *  @file   IDeviceConnection.h
 *  @author ECV - vma6cob
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */


#ifndef IDEVICECONNECTION_H_
#define IDEVICECONNECTION_H_

class IDeviceConnectionOserver;
namespace App {
namespace Core {

class IDeviceConnection
{
   public:
      virtual void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/) = 0;
      virtual void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/) = 0;
      virtual std::string getActiveDeviceName(uint32 sHeaderIndex) = 0;
      virtual bool getConnectedDeviceSize(std::string&) = 0;
      virtual void registerObserver(IDeviceConnectionOserver*) = 0;
      virtual void deregisterObserver(IDeviceConnectionOserver*) = 0;
      virtual uint32 getNewlyInsertedDeviceTag() = 0;
      virtual void resetNewlyInsertedDeviceTag() = 0;
      virtual void setSpiActiveStatus(bool) = 0;
      virtual void setActiveMediaDevice(uint32 deviceTag) = 0;
};


}  // namespace Core
}  // namespace App

#endif /* IDEVICECONNECTION_H_ */
