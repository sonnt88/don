/**
 *  @file   SpiMetaData.h
 *  @author ECV - ndi6kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */
#include "hall_std_if.h"
#include "SpiMetaData.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS         TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::SpiMetaData::
#include "trcGenProj/Header/SpiMetaData.cpp.trc.h"
#endif
#define ARRAY_SIZE         15
namespace App {
namespace Core {

static SpiMetaData* pStaticSpiMetaData;
/*
 * Description : Destructor of the class SpiMetaData
 */
SpiMetaData::~SpiMetaData()
{
   ETG_TRACE_USR4(("SpiMetaData: Destructor"));
   _spiMetaDataProxy.reset();
}


/*
 * Description : Constructor of the class SpiMetaData
 */

SpiMetaData::SpiMetaData(::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& spiMetaDataProxy):
   _spiMetaDataProxy(spiMetaDataProxy)
{
   ETG_TRACE_USR4(("SpiMetaData: Constructor"));
   StartupSync::getInstance().registerPropertyRegistrationIF(this);
}


/*
 * This function performs the upreg of the required properties.
 * @param[in]  - proxy: Pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - stateChange: Contains the current Service State
 * @param[out] - None
 * @return     - Void
 */
void SpiMetaData::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_spiMetaDataProxy && (_spiMetaDataProxy == proxy))
   {
      _spiMetaDataProxy->sendApplicationPhoneDataUpReg(*this);
   }
}


/*
 * This function performs the Relupreg of the required properties.
 * @param[in]  - proxy: Pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - stateChange: Contains the current Service State
 * @param[out] - None
 * @return     - Void
 */
void SpiMetaData::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_spiMetaDataProxy && (_spiMetaDataProxy == proxy))
   {
      _spiMetaDataProxy->sendApplicationPhoneDataRelUpRegAll();
   }
}


/**
 * onApplicationPhoneDataError - called if ApplicationPhoneDataError property is updated with error
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void SpiMetaData::onApplicationPhoneDataError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::ApplicationPhoneDataError >& /*error*/)
{
   ETG_TRACE_USR4(("SpiMetaData: onApplicationPhoneDataError received"));
}


/*
 * This function is called when the property update for AApplicationPhoneDataStatus status
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - status: Pointer to the ApplicationPhoneDataStatus
 * @param[out] - None
 * @return     - Void
 */
void SpiMetaData::onApplicationPhoneDataStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::ApplicationPhoneDataStatus >& status)
{
   ::std::vector< ::midw_smartphoneint_fi_types::T_ApplicationPhoneCallMetadata >ApplicationPhoneDataStatus = status->getPhoneCallMetadata();
   ::std::vector< ::midw_smartphoneint_fi_types::T_ApplicationPhoneCallMetadata >::const_iterator itr = ApplicationPhoneDataStatus.begin();
   ::midw_smartphoneint_fi_types::T_e8_PhoneCallDirection ePhoneCallDirection;
   ::midw_smartphoneint_fi_types::T_e8_PhoneCallState ePhoneCallState;
   for (int8 i8Index = 0 ; itr != ApplicationPhoneDataStatus.end(); itr++, i8Index++)
   {
      ETG_TRACE_USR4(("onApplicationPhoneDataStatus received Index = %d", i8Index));
      _mStrPhoneNumber = itr->getPhoneNumber();
      _mStrDisplayName = itr->getDisplayName();
      updateCallDuration(itr->getCallDuration());
      ePhoneCallDirection = itr->getCallDirection();
      ePhoneCallState = itr->getCallState();
      updateCallDirection(ePhoneCallDirection);
      updateCallState(ePhoneCallState);
      ETG_TRACE_USR4(("onApplicationPhoneDataStatus  : PHONE_NUMBER = %s", _mStrPhoneNumber.c_str()));
      ETG_TRACE_USR4(("onApplicationPhoneDataStatus  : DISPLAY_NAME = %s", _mStrDisplayName.c_str()));
   }
   (*_updatePhoneMetadata).mCallInfo = _mStrCallDirection.c_str();
   if (_mStrPhoneNumber.compare(_mStrDisplayName) != 0)
   {
      (*_updatePhoneMetadata).mCallerInfo = _mStrDisplayName.c_str();
   }
   else
   {
      (*_updatePhoneMetadata).mCallerInfo = _mStrPhoneNumber.c_str();
   }

   _updatePhoneMetadata.MarkItemModified(ItemKey::DIPOPhoneMetaData::CallInfoItem);
   _updatePhoneMetadata.MarkItemModified(ItemKey::DIPOPhoneMetaData::CallerInfoItem);

   if (_updatePhoneMetadata.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_updatePhoneMetadata : Updation failed..!!!"));
   }
}


/*
 * This is a helper function to update Call duration from function onApplicationPhoneDataStatus
 * @param[in]  - uint32 Call duration
 * @param[out] - None
 * @return     - Void
 */
void SpiMetaData::updateCallDuration(uint32 i32CallDuration)
{
   _mCallDuration = i32CallDuration;
   char callduration[ARRAY_SIZE] = "\0";
   snprintf(callduration, sizeof callduration, "%d",  _mCallDuration);
   std::string calldurationstr = callduration;

   ETG_TRACE_USR4(("onApplicationPhoneDataStatus  : CALL_DURATION = %d", _mCallDuration)); /* Trying to print as int */
   ETG_TRACE_USR4(("onApplicationPhoneDataStatus  : CALL_DURATION = %s", calldurationstr.c_str())); /* Trying to convert to string and print */
}


/*
 * This is a helper function to update Call Direction operation from function onApplicationPhoneDataStatus
 * @param[in]  - PhoneCallDirection
 * @param[out] - None
 * @return     - Void
 */
void SpiMetaData::updateCallDirection(::midw_smartphoneint_fi_types::T_e8_PhoneCallDirection ePhoneCallDirection)
{
   ETG_TRACE_USR4(("updateCallDirection called..ePhoneCallDirection = %d", ePhoneCallDirection));
   switch (ePhoneCallDirection)
   {
      case ::midw_smartphoneint_fi_types::T_e8_PhoneCallDirection__INCOMING :
      {
         _mStrCallDirection = "Incoming"; /* Setting it as string because may have to display as string in popup */
         break;
      }
      case ::midw_smartphoneint_fi_types::T_e8_PhoneCallDirection__OUTGOING :
      {
         _mStrCallDirection = "Outgoing";
         break;
      }
      default :
         break;
   }

   ETG_TRACE_USR4(("updateCallDirection : CALL_DIRECTION = %s", _mStrCallDirection.c_str()));
}


/*
 * This is a helper function to update Call Status operation from function onApplicationPhoneDataStatus
 * @param[in]  - PhoneCallStatus
 * @param[out] - None
 * @return     - Void
 */
void SpiMetaData::updateCallState(::midw_smartphoneint_fi_types::T_e8_PhoneCallState ePhoneCallState)
{
   ETG_TRACE_USR4(("updateCallState called..ePhoneCallState = %d", ePhoneCallState));
   switch (ePhoneCallState)
   {
      case  ::midw_smartphoneint_fi_types::T_e8_PhoneCallState__RINGING :
      {
         _mStrCallState = "RINGING" ;  /* Setting it as string because may have to display as string in popup */
         break;
      }
      case  ::midw_smartphoneint_fi_types::T_e8_PhoneCallState__ACTIVE :
      {
         _mStrCallState = "ACTIVE" ;
         break;
      }
      case ::midw_smartphoneint_fi_types::T_e8_PhoneCallState__HELD :
      {
         _mStrCallState = "HELD" ;
         break;
      }
      default :
         break;
   }

   ETG_TRACE_USR4(("updateCallState  : CALL_STATE = %s", _mStrCallState.c_str()));
}


}/* namespace Core */
} /* namespace App */
