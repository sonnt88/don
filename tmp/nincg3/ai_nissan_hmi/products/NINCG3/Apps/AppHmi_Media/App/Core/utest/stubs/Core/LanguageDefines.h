#ifndef LANGUAGEDEFINES_H_
#define LANGUAGEDEFINES_H_

#define LANGUAGE_TEXT(text_id, text) text	//TODO Once same string ID are available for Scope2, below macro could be used
#define LANGUAGE_STRING(text_id, text) text
#define LANGUAGE_ID(text_id, text) text
#define LANGUAGE_ID_TYPE std::string
#define LANGUAGE_ID_STRING(text) text

namespace Candera
{
typedef char TChar;

class String
{
   public:
      String():
         m_text("")
      {

      }
      String(const TChar* string) :
         m_text("")
      {
         m_text = const_cast<TChar*>(string);
      }

      ~String() {}

      String& operator=(const String& string)
      {
         m_text = string.GetCString();
         return *this;
      }
      String& operator=(const TChar* text)
      {
         m_text = const_cast<TChar*>(text);
         return *this;
      }

      TChar* GetCString() const
      {
         return m_text;
      }

      bool operator==(const String& other) const
      {
         return (m_text == other.m_text);
      }
      bool operator!=(const String& other) const
      {
         return !(*this == other);
      }

   private:
      TChar* m_text;

};
}

#endif
