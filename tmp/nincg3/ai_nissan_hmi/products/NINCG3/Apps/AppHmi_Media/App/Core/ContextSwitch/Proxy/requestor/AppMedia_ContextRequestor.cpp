///////////////////////////////////////////////////////////
//  AppMedia_ContextRequestor.cpp
//  Implementation of the Class AppMedia_ContextRequestor
//  Created on:      13-Jul-2015 11:44:47 AM
//  Original author: pad1cob
///////////////////////////////////////////////////////////


#include "hall_std_if.h"
#include "MediaDatabinding.h"
#include "AppMedia_ContextRequestor.h"
#include "clContextProviderInterface.h"
#include "clContextRequestorInterface.h"
#include "ProjectBaseTypes.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"
#include <algorithm>
#include "AppHmi_MediaMessages.h"
#include "ProjectBaseMsgs.h"

AppMedia_ContextRequestor::AppMedia_ContextRequestor()
{
}


AppMedia_ContextRequestor::~AppMedia_ContextRequestor()
{
}


bool AppMedia_ContextRequestor::onCourierMessage(const ContextSwitchOutReqMsg& msg)
{
   vRequestContext(msg.GetSourceContextId(), msg.GetTargetContextId());
   return true;
}


void AppMedia_ContextRequestor::vOnRequestResponse(ContextState /*enState*/)
{
}


void AppMedia_ContextRequestor::vOnNewAvailableContexts()
{
   bool carplayConnectionStatus = false;
   if (bIsContextAvailable(MEDIA_SPI_CONTEXT_CARPLAY_DEVICE_CONNECTED))
   {
      carplayConnectionStatus = true;
   }
   if (carplayConnectionStatus)
   {
      (*_carPlayConnectionStatus).mIsCarplayActive = false;
   }
   else
   {
      (*_carPlayConnectionStatus).mIsCarplayActive = true;
   }
   _carPlayConnectionStatus.MarkAllItemsModified();
   _carPlayConnectionStatus.SendUpdate(true);
}


void AppMedia_ContextRequestor::vOnNewActiveContext(uint32 targetContextId, ContextSwitchTypes::ContextState enContextState, ContextSwitchTypes::ContextType contextType)
{
   if (contextType == ContextType__TEMPORARY)
   {
      POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchTypeTempActiveCheckMsg)(true)));
   }
   else
   {
      POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchTypeTempActiveCheckMsg)(false)));
   }
   clContextRequestor::vOnNewActiveContext(targetContextId, enContextState, contextType);
}


/**
 * This message is sent by the SM on HK_AUX press in SPI utility scene to clear context.
 * @param[in] - msg
 * @return    -bool
 */

bool AppMedia_ContextRequestor::onCourierMessage(const onClearContextOnHkMsg& /*oMsg*/)
{
   clContextRequestor::vClearContexts();
   return true;
}
