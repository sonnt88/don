/*
 * @file       : Spi_defines.h
 * @author     : ECV3-rhk5kor
 * @copyright  : (c) 2015 Robert Bosch Car Multimedia GmbH
 * @addtogroup : SPI/Media
 */
#ifndef SPI_DEFINES_H
#define SPI_DEFINES_H

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R
#define VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
#endif

#define UNDEFINE_VALUE 0
#define UNDEFINE_KEYEVENT 255
#define INVALID_CONNECTION 255
#define DUMMY_COURIER 1
#define ALL_DEVICE_HANDLE 0xFFFFFFFF
#define DEFAULT_APP_HANDLE 0
#define FEATURE_NOT_RESTRICTED 00000000
#define DUMMY_SWITCH_ID 1
#define SPEED_INVALID_VALUE 65535
/************************enAutoLaunchDatapoolStatus*************************************
 * 0 e8AUTOLAUNCH_STATUS_ASK
      Set datapool AutoLaunch Status ASK and show disclaimer popup when Carplay connected.
 * 1 e8AUTOLAUNCH_STATUS_ON
     Set datapool AutoLaunch Status ON and Launch Carplay without Popup.
 * 2 e8AUTOLAUNCH_STATUS_OFF
      Set datapool AutoLaunch Status OFF and don't launch Carplay when connected.
 ***************************************************************************************/

enum enAutoLaunchDatapoolStatus
{
   AUTOLAUNCH_STATUS_ASK = 0,
   AUTOLAUNCH_STATUS_ON = 1,
   AUTOLAUNCH_STATUS_OFF = 2,
   AUTOLAUNCH_STATUS_NOT_DEFINED = 3
};


enum enDisplayContext
{
   DISPLAY_CONTEXT_UNKNOWN = 0u,
   DISPLAY_CONTEXT_SPEECH_REC = 1u,
   DISPLAY_CONTEXT_PHONE = 2u,
   DISPLAY_CONTEXT_NAVIGATION = 3u,
   DISPLAY_CONTEXT_MEDIA = 4u,
   DISPLAY_CONTEXT_INFORMATIONAL = 5u,
   DISPLAY_CONTEXT_EMERGENCY = 6u,
   DISPLAY_CONTEXT_SPEAKING = 7u,
   DISPLAY_CONTEXT_TBT_NAVIGATION = 8u
};


enum enDisplayFlag
{
   DISPLAY_FLAG_FALSE = 0,
   DISPLAY_FLAG_TRUE = 1
};


enum enAudioContext
{
   AudioContext__SPI_AUDIO_MAIN = 0u,

   AudioContext__SPI_AUDIO_INTERNET_APP = 1u,

   AudioContext__SPI_AUDIO_SPEECH_REC = 2u,

   AudioContext__SPI_AUDIO_ADVISOR_PHONE = 3u,

   AudioContext__SPI_AUDIO_EMER_PHONE = 4u,

   AudioContext__SPI_AUDIO_PHONE = 5u,

   AudioContext__SPI_AUDIO_INCOM_TONE = 6u,

   AudioContext__SPI_AUDIO_TRAFFIC = 7u,

   AudioContext__SPI_AUDIO_EMERGENCY_MSG = 8u,

   AudioContext__SPI_AUDIO_SYNC_MSG = 9u
};


enum enAudioFlag
{
   AUDIO_FLAG_FALSE = 0,
   AUDIO_FLAG_TRUE = 1
};


enum enCarPlayAskOnConnectStatus
{
   CARPLAY_ASK_ON_CONNECT_OFF = 0,
   CARPLAY_ASK_ON_CONNECT_ON = 1,
   CARPLAY_ASK_ON_CONNECT_NOT_DEFINED = 2
};


enum enCarPlayAutoLaunchStatus
{
   CARPLAY_AUTOLAUNCHSTATUS_OFF = 0,
   CARPLAY_AUTOLAUNCHSTATUS_ON = 1,
   CARPLAY_AUTOLAUNCHSTATUS_NOT_DEFINED = 2
};


enum enAndroidAskOnConnectStatus
{
   ANDROID_AUTO_ASK_ON_CONNECT_OFF = 0,
   ANDROID_AUTO_ASK_ON_CONNECT_ON = 1,
   ANDROID_AUTO_ASK_ON_CONNECT_NOT_DEFINED = 2
};


enum enAndroidAutoLaunchStatus
{
   ANDROID_AUTO_AUTOLAUNCHSTATUS_OFF = 0,
   ANDROID_AUTO_AUTOLAUNCHSTATUS_ON = 1,
   ANDROID_AUTO_AUTOLAUNCHSTATUS_NOT_DEFINED = 2
};


enum enKeyCode
{
   KeyCode__DEV_PHONE_CALL = 12u,

   KeyCode__DEV_PHONE_END = 13u,

   KeyCode__DEV_SOFT_LEFT = 14u,

   KeyCode__DEV_SOFT_MIDDLE = 15u,

   KeyCode__DEV_SOFT_RIGHT = 16u,

   KeyCode__DEV_APPLICATION = 17u,

   KeyCode__DEV_OK = 18u,

   KeyCode__DEV_DELETE = 19u,

   KeyCode__DEV_ZOOM_IN = 20u,

   KeyCode__DEV_ZOOM_OUT = 21u,

   KeyCode__DEV_CLEAR = 22u,

   KeyCode__DEV_FORWARD = 23u,

   KeyCode__DEV_BACKWARD = 24u,

   KeyCode__DEV_HOME = 25u,

   KeyCode__DEV_SEARCH = 26u,

   KeyCode__DEV_MENU = 27u,

   KeyCode__DEV_PTT = 28u,

   KeyCode__MULTIMEDIA_PLAY = 29u,

   KeyCode__MULTIMEDIA_PAUSE = 30u,

   KeyCode__MULTIMEDIA_STOP = 31u,

   KeyCode__MULTIMEDIA_FORWARD = 32u,

   KeyCode__MULTIMEDIA_REWIND = 33u,

   KeyCode__MULTIMEDIA_NEXT = 34u,

   KeyCode__MULTIMEDIA_PREVIOUS = 35u,

   KeyCode__MULTIMEDIA_MUTE = 36u,

   KeyCode__MULTIMEDIA_UNMUTE = 37u,

   KeyCode__MULTIMEDIA_PHOTO = 38u,

   KeyCode__DEV_PHONE_FLASH = 39u
};


enum enKeyMode
{
   KeyMode__KEY_RELEASE = 0u,
   KeyMode__KEY_PRESS = 1u
};


enum ensetConfiguration
{
   Disable = 0,
   Enable = 1
};


enum enVehicle_Configuration
{
   Vehicle_Configuration__PARK_MODE = 0u,

   Vehicle_Configuration__DRIVE_MODE = 1u,

   Vehicle_Configuration__DAY_MODE = 2u,

   Vehicle_Configuration__NIGHT_MODE = 3u,

   Vehicle_Configuration__RIGHT_HAND_DRIVE = 4u,

   Vehicle_Configuration__LEFT_HAND_DRIVE = 5u
};


enum enDeviceCategory
{
   DeviceCategory__DEV_TYPE_UNKNOWN = 0u,

   DeviceCategory__DEV_TYPE_DIPO = 1u,

   DeviceCategory__DEV_TYPE_MIRRORLINK = 2u,

   DeviceCategory__DEV_TYPE_ANDROIDAUTO = 3u
};


enum enDeviceConnectionType
{
   DeviceConnectionType__UNKNOWN_CONNECTION = 0u,

   DeviceConnectionType__USB_CONNECTED = 1u,

   DeviceConnectionType__WIFI_CONNECTED = 2u
};


enum enDeviceConnectionReq
{
   DeviceConnectionReq__DEV_CONNECT = 0u,

   DeviceConnectionReq__DEV_DISCONNECT = 1u
};


enum enEnabledInfo
{
   EnabledInfo__USAGE_DISABLED = 0u,

   EnabledInfo__USAGE_ENABLED = 1u
};


enum enSteeringWheelPosition
{
   LHD = 0,
   RHD = 1
};


enum enSpeechAppState
{
   SpeechAppState__SPI_APP_STATE_SPEECH_UNKNOWN = 0u,

   SpeechAppState__SPI_APP_STATE_SPEECH_END = 1u,

   SpeechAppState__SPI_APP_STATE_SPEECH_SPEAKING = 2u,

   SpeechAppState__SPI_APP_STATE_SPEECH_RECOGNISING = 3u
};


enum enPhoneAppState
{
   PhoneAppState__SPI_APP_STATE_PHONE_UNKNOWN = 0u,

   PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE = 1u,

   PhoneAppState__SPI_APP_STATE_PHONE_NOTACTIVE = 2u
};


enum enNavigationAppState
{
   NavigationAppState__SPI_APP_STATE_NAV_UNKNOWN = 0u,

   NavigationAppState__SPI_APP_STATE_NAV_ACTIVE = 1u,

   NavigationAppState__SPI_APP_STATE_NAV_NOTACTIVE = 2u
};


enum enErrorPopUp
{
   INFO__POPUP_CARPLAY_START_FAIL = 0u,
   INFO__POPUP_ANDROID_START_FAIL = 1u,
   INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND = 2u,
   INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND = 3u,
   INFO__POPUP_CARPLAY_STARTING_SYSIND = 4u,
   INFO__POPUP_CARPLAY_STARTED_SYSIND = 5u,
   INFO__POPUP_ANDROID_STARTING_SYSIND = 6u,
   INFO__POPUP_ANDROID_STARTED_SYSIND = 7u,
   INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND = 8u,
   INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND = 9u,
   INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND = 10u,
   INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND = 11u,
   INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND = 12u,
   INFO__POPUP_CARPLAY_ADVISORY_SYSIND = 13u
};


enum enRestrictionsCarPlay
{
   DIPO_SoftKeyboard = 0x01, //00000001
   DIPO_SoftPhoneKeyPad = 0x02, //00000010
   DIPO_NonMusicLists = 0x04, //00000100
   DIPO_MusicLists = 0x08, //00001000
   DIPO_JapanMaps = 0x10 //00010000
};


enum enRestrictionsAndroidAuto
{
   AAP_VideoPlayback = 0x01, //00000001
   AAP_TextInput  = 0x02, //00000010
   AAP_VoiceInput = 0x04, //00000100
   AAP_SetUpAndConfiguration = 0x08, //00001000
   AAP_MessageLength = 0x10 //00010000
};


enum enDeviceType
{
   DeviceType__DEVICE_TYPE_UNKNOWN = 0u,
   DeviceType__ANDROID_DEVICE = 1u,
   DeviceType__APPLE_DEVICE = 2u
};


enum enUserAuthorizationStatus
{
   UserAuthorizationStatus__USER_AUTH_UNKNOWN = 0u,
   UserAuthorizationStatus__USER_AUTH_UNAUTHORIZED = 1u,
   UserAuthorizationStatus__USER_AUTH_AUTHORIZED = 2u
};


enum enSpiSettingUpdateStatus
{
   DIPO_AAP_NO_UPDATE = 0,
   DIPO_ASK_ON_CONNECT_OFF = 1,
   DIPO_ASK_ON_CONNECT_ON = 2,
   DIPO_AUTOLAUNCHSTATUS_OFF = 3,
   DIPO_AUTOLAUNCHSTATUS_ON = 4,
   AAP_ASK_ON_CONNECT_OFF = 5,
   AAP_ASK_ON_CONNECT_ON = 6,
   AAP_AUTO_AUTOLAUNCHSTATUS_OFF = 7,
   AAP_AUTO_AUTOLAUNCHSTATUS_ON = 8
};


enum enVehicle_HandBrakeState
{
   HAND_BRAKE_INFO_UNAVAILABLE = 0,
   HAND_BRAKE_NOT_ENGAGED = 1,
   HAND_BRAKE_ENGAGED  = 2
};


#endif
