/**
*  @file   BrowseFactory.h
*  @author ECV - vma6cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef BROWSEFACTORY_H_
#define BROWSEFACTORY_H_

#include "IBrowse.h"


namespace App
{
namespace Core
{

class BrowseFactory
{
   public:
      BrowseFactory();
      ~BrowseFactory();
      static IBrowse* getBrowser(uint32 listId);
};

}
}


#endif /* BROWSEFACTORY_H_ */
