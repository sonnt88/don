///////////////////////////////////////////////////////////
//  AppMedia_ContextRequestor.h
//  Implementation of the Class AppMedia_ContextRequestor
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(AppMedia_ContextRequestor_h)
#define AppMedia_ContextRequestor_h

/**
 * interface class for context proxies to provide contexts to requestors
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 5:44:42 PM
 */
//#include "hall_std_if.h"
#include "hmi_trace_if.h"
#include "ProjectBaseMsgs.h"
#include "ProjectBaseTypes.h"
#include "clContextRequestor.h"
#include "clContext.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"
#include "asf/core/Types.h"
#include "asf/core/Proxy.h"
#include "AppHmi_MediaMessages.h"
#include "CourierTunnelService/CourierMessageReceiverStub.h"
#include "AppHmi_SpiMessages.h"


using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;


class AppMedia_ContextRequestor : public clContextRequestor
{
   public:
      AppMedia_ContextRequestor();
      virtual ~AppMedia_ContextRequestor();

      void vOnNewActiveContext(uint32 targetContextId, ContextSwitchTypes::ContextState enContextState, ContextSwitchTypes::ContextType contextType);

   protected:
      bool onCourierMessage(const ContextSwitchOutReqMsg& msg);
      bool onCourierMessage(const onClearContextOnHkMsg& msg);

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      ON_COURIER_MESSAGE(ContextSwitchOutReqMsg)
      ON_COURIER_MESSAGE(onClearContextOnHkMsg)
      COURIER_MSG_MAP_END()

      DataBindingItem<CarplayContextAvailabilityDataBindingSource> _carPlayConnectionStatus;

      virtual void vOnRequestResponse(ContextState /*enState*/);
      virtual void vOnNewAvailableContexts();
};


#endif // !defined(EA_FAF598AF_B4F9_4bb1_91CE_ABBA6F93217B__INCLUDED_)
