/**
* @file Playmode.h
* @author RBEI/ECV-AIVI_MediaTeam
* @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
* @addtogroup AppHMI_Media
*/

#ifndef PLAYMODE_H_
#define PLAYMODE_H_

#include "MPlay_fi_types.h"
#include "Core/LanguageDefines.h"
#include "Core/MediaDefines.h"

namespace App
{
namespace Core
{
enum PlayingSelection
{
   FOLDER_BASED_SELECTION,
   METADATA_BASED_SELECTION,
   UNKNOWN_SELECTION
};

class PlayMode
{
   public:
      PlayMode();
      virtual ~PlayMode();
      bool updateRepeatState(::MPlay_fi_types::T_e8_MPlayRepeat , Candera::String& , ::MPlay_fi_types::T_e8_MPlayListType, Candera::String&);
      bool updateRandomState(::MPlay_fi_types::T_e8_MPlayMode , Candera::String& , ::MPlay_fi_types::T_e8_MPlayListType, Candera::String&);
      void updateRandomRepeatState(::MPlay_fi_types::T_e8_MPlayMode randomStatus, ::MPlay_fi_types::T_e8_MPlayRepeat repeatStatus, PlayingSelection currentPlayingSelection);

      ::MPlay_fi_types::T_e8_MPlayRepeat getNextRepeatState(PlayingSelection currentPlayingSelection, ::MPlay_fi_types::T_e8_MPlayRepeat repeatStatus);
      ::MPlay_fi_types::T_e8_MPlayMode getNextRandomState(::MPlay_fi_types::T_e8_MPlayMode randomStatus);

      bool getRamdonState()
      {
         return _isRandomActive;
      }
      bool getRepeatState()
      {
         return _isRepeatActive;
      }
      Candera::String& getRepeatText()
      {
         return _oRepeat;
      }
      Candera::String& getRadomText()
      {
         return _oRandom;
      }
      Candera::String& getRadomRepeatText()
      {
         return _oRandomRepeat;
      }

   protected:
      bool _isRepeatActive ;
      bool _isRandomActive ;
      Candera::String _oRandomRepeat;
      Candera::String _oRandom;
      Candera::String _oRepeat;
};

} /* namespace Core */
} /* namespace App */
#endif /* PLAYMODE_H_ */
