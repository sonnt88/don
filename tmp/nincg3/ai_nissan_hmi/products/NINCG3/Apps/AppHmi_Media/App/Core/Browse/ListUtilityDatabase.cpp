/**
*  @file   ListUtilityDatabase.cpp
*  @author ECV - vma6cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#include "hall_std_if.h"
#include "ListUtilityDatabase.h"

/**
* @Constructor
*/
ListUtilityDatabase::ListUtilityDatabase():
   m_u32CurrentStartIndex(0)
   , m_u32CurrentBufferSize(0)
   , m_u32ListHandle(0)
   , m_u32ListSize(0)
   , m_u8ActiveItemIndex(0)
   //, m_sFirstItem("")
{
   _listItemTag.clear();
}

/**
* @Destructor
*/
ListUtilityDatabase::~ListUtilityDatabase()
{
}

/**
* vSetCurrentStartIndex - function to store the start index of list
* @param[in] StartIndex
*/
void ListUtilityDatabase::vSetCurrentStartIndex(uint32 StartIndex)
{
   m_u32CurrentStartIndex = StartIndex;
}

/**
* u32GetCurrentStartIndex - function to get the start index of list
* @param[in] StartIndex
*/
uint32 ListUtilityDatabase::u32GetCurrentStartIndex() const
{
   return m_u32CurrentStartIndex;
}

/**
* vSetCurrentBufferSize - function to set the widow size of list
* @param[in] StartIndex
*/
void ListUtilityDatabase::vSetCurrentBufferSize(uint32 BufferSize)
{
   m_u32CurrentBufferSize = BufferSize;
}

/**
* u32GetCurrentBufferSize - function to get the widow size of list
* @param[in] StartIndex
*/
uint32 ListUtilityDatabase::u32GetCurrentBufferSize() const
{
   return m_u32CurrentBufferSize;
}

/**
* vSetListHandle - function to set list handle
* @param[in] StartIndex
*/
void ListUtilityDatabase::vSetListHandle(uint32 u32ListHandle)
{
   m_u32ListHandle = u32ListHandle;
}

/**
* u32GetListHandle - function to get the list handle
* @param[in] StartIndex
*/
uint32 ListUtilityDatabase::u32GetListHandle() const
{
   return m_u32ListHandle;
}

/**
* vSetListSize - function to set the current list size
* @param[in] StartIndex
*/
void ListUtilityDatabase::vSetListSize(uint32 u32ListSize)
{
   m_u32ListSize = u32ListSize;

}

/**
* u32GetListSize - function to get the current list size
* @param[in] StartIndex
*/
uint32 ListUtilityDatabase::u32GetListSize() const
{
   return m_u32ListSize;
}

/**
* vSetActiveItemIndex - function to set the selected item's index in the list
* @param[in] StartIndex
*/
void ListUtilityDatabase::vSetActiveItemIndex(uint32 u32ActiveItemIndex)
{
   m_u8ActiveItemIndex = u32ActiveItemIndex;
}

/**
* u32GetActiveItemIndex - function to get the selected item's index in the list
* @param[in] StartIndex
*/
uint32 ListUtilityDatabase::u32GetActiveItemIndex() const
{
   return m_u8ActiveItemIndex;
}

/**
* u32GetActiveItemTag - function to get the active item's tag in the list
* @param[in] StartIndex
*/
uint32 ListUtilityDatabase::u32GetActiveItemTag()
{

   if (m_u8ActiveItemIndex < _listItemTag.size())	//vector size check is needed to avoid resets
   {
      return _listItemTag[m_u8ActiveItemIndex];
   }
   else
   {
      return 0;
   }
}

/**
* u32GetTagbyIndex - function to get the item's tag based on index
* @param[in] StartIndex
*/
uint32 ListUtilityDatabase::u32GetTagbyIndex(uint32 u32Index)
{

   if (u32Index < _listItemTag.size())	//vector size check is needed to avoid resets
   {
      return _listItemTag[u32Index];
   }
   else
   {
      return 0;
   }
}

/**
* vStoreIndexToTagData - function to store the tag of all elements in list
* @param[in] StartIndex
*/
void ListUtilityDatabase::vStoreIndexToTagData(std::vector<uint32> & v)
{
   _listItemTag = v;
}

/**
* vSetFirstElement - function to set first element
* @param[in] StartIndex
*/
void ListUtilityDatabase::vSetFirstElement(std::vector<std::string> firstelement)
{
   m_sFirstItem = firstelement;
}

/**
* getFirstElementText - function to get first element
* @param[in] selectedIndex
*/
std::string ListUtilityDatabase::getFirstElementText(uint32 selectedIndex) const
{
   if (selectedIndex < m_sFirstItem.size())
   {
      return m_sFirstItem[selectedIndex];
   }
   else
   {
      return (m_sFirstItem.size() == 0) ? "" : m_sFirstItem[0];
   }
}

/**
* getListTextByIndex - function to get list text for selected index
* @param[in] selectedIndex
*/
Candera::String ListUtilityDatabase::getListTextByIndex(uint32 selectedIndex) const
{
   if (selectedIndex < m_sFirstItem.size())
   {
      return Candera::String(m_sFirstItem[selectedIndex].c_str());
   }
   else
   {
      return LANGUAGE_STRING(BROWSE_All, "All");
   }
}
