/**
*  @file   KdsInfo.h
*  @author ECV - vma6cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef KDSINFO_H_
#define KDSINFO_H_

#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
#include "CourierTunnelService/CourierMessageReceiverStub.h"
#endif

namespace App {
namespace Core {

class KdsInfo
{
   public:
      KdsInfo();
      virtual ~KdsInfo();
      static void readRegionFromKDS();
      static bool isJapanRegion();
      static bool isEuropeonRegion();
      static bool isTurkeyRegion();
      static bool isRussianRegion();
      static bool isMexicoRegion();
      static bool isGccRegion();
      static bool isAsrRegion();
      static bool isNewZealandRegion();
      static bool isSouthAfricanRegion();
      static bool isCarPlayEnabled();
      static bool isAndroidAutoEnabled();


#ifdef VARIANT_S_FTR_ENABLE_UNITTEST
      static void setCurrentRegion(uint8 currentRegion);
#endif

#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      COURIER_MSG_MAP_END()
#endif

   private:
      static uint8 _currentRegion;
};


} // end of namespace Core
} // end of namespace App


#endif /* KDSINFO_H_ */
