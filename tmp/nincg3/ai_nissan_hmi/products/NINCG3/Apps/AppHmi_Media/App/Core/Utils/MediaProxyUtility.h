/**
*  @file   MediaProxyUtility.h
*  @author ECV - kng3kor
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef MEDIAPROXYUTILITY_H_
#define MEDIAPROXYUTILITY_H_
#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
#include "mplay_MediaPlayer_FIProxy.h"
#endif

namespace App {
namespace Core {
class MediaProxyUtility
{
   public:
      virtual ~MediaProxyUtility();
#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
      static void createInstance(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy);
#endif
      static MediaProxyUtility& getInstance();
      //MediaProxyUtility(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy);

#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
      uint32 GetDeviceType(uint32 deviceTag);
      uint32 getHallDeviceType(uint32 deviceTag);
      uint32 getActiveDeviceTag();
#endif
      uint32 getActiveDeviceType();
#ifdef VARIANT_S_FTR_ENABLE_UNITTEST
      void setDummyActiveDeviceType(uint32 dummyActiveDeviceType);
#endif
      uint32 getHallActiveDeviceType();
#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
      std::string getViewName(uint32 deviceTag);
      bool isAnyBTDeviceConnected();
      bool isAnyBTDeviceDisConnectedOverUSB();
      bool isIndexingDone(uint32 activeDeviceTag);
      void getCurrentDeviceIndexingValues(uint8& indexingState, uint8& indexingPercent);
      uint32 GetConnectedDeviceTag(uint32 deviceType);
      bool getBrowsertype();
#endif

   private:
      static MediaProxyUtility* _theInstance;
#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
      MediaProxyUtility(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy);

      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& _mediaPlayerProxy;
#else
      MediaProxyUtility();
      uint32 _dummyActiveDeviceType;
#endif
};


}
}


#endif /* MEDIAPROXYUTILITY_H_ */
