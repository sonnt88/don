/**
 *  @file   BTSetting.cpp
 *  @author ECV - kng3kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */
#include "hall_std_if.h"
#include "BTSetting.h"
#include "MediaDatabinding.h"
#include "Core/Utils/MediaUtils.h"

#define UTFUTIL_S_IMPORT_INTERFACE_GENERIC
#include "utf_if.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS         TR_CLASS_APPHMI_MEDIA_HALL_BTAUDIO
#define ETG_I_TRACE_CHANNEL             TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX          "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX               App::Core::BTSetting::

#include "trcGenProj/Header/BTSetting.cpp.trc.h"
#endif

using namespace ::App::Core;

namespace App {
namespace Core {
BTSetting* BTSetting::_btSettinsInstance = NULL;

/**
 * @Destructor
 */
BTSetting::~BTSetting()
{
   ETG_TRACE_USR4(("BTSetting: BTSetting distructor"));
   _btSettinsInstance = NULL;
   _btSettingProxy.reset();
   _telephoneProxy.reset();
}


BTSetting::BTSetting()
   : _btSettingProxy(::MOST_BTSet_FI::MOST_BTSet_FIProxy::createProxy("btSetFiPort", *this)),
     _telephoneProxy(::MOST_Tel_FI::MOST_Tel_FIProxy::createProxy("btTelFiPort", *this))
     , _btDeviceName("")
     , _connectedDeviceHandle(0xFF)
     , _DeviceAudioStreamingSupportType(most_BTSet_fi_types::T_e8_BTSetAudioStreamingSupportType__e8AVRCP_NO_METADATA)
     , _isDeviceConnected(false)
{
   StartupSync::getInstance().registerPropertyRegistrationIF(this);
   _btSettinsInstance = this;
   ETG_I_REGISTER_FILE();
}


/**
 * deregisterProperties - Trigger property deregistration to BT properties,  called from MediaHall class
 * @param[in] proxy
 * @parm[in] stateChange - state change service for corrosponding  proxy
 * @return void
 */
void BTSetting::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_btSettingProxy && _btSettingProxy == proxy)
   {
      _btSettingProxy->sendDeviceListExtendedRelUpRegAll();
      _btSettingProxy->sendBluetoothAudioSourceRelUpRegAll();
   }
   else if (_telephoneProxy && _telephoneProxy == proxy)
   {
      _telephoneProxy->sendCallStatusNoticeRelUpRegAll();
   }
}


/**
 * registerProperties - Trigger property registration to BT properties,  called from MediaHall class
 * @param[in] proxy
 * @parm[in] stateChange - state change service for corrosponding  proxy
 * @return void
 */
void BTSetting::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_btSettingProxy && _btSettingProxy == proxy)
   {
      ETG_TRACE_USR4(("registerProperties from btsetting"));
      _btSettingProxy->sendDeviceListExtendedUpReg(*this);
      _btSettingProxy->sendBluetoothAudioSourceUpReg(*this);
   }
   else if (_telephoneProxy && _telephoneProxy == proxy)
   {
      _telephoneProxy->sendCallStatusNoticeUpReg(*this);
   }
}


/**
 * onDeviceListExtendedError - called if Device list  property is updated with error from BT
 *
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void BTSetting::onDeviceListExtendedError(const ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy >& /*proxy*/
      , const ::boost::shared_ptr< ::MOST_BTSet_FI::DeviceListExtendedError>& /*error*/)
{
   ETG_TRACE_USR4(("BTSetting::onDeviceListExtendedError"));
}


/**
 * onDeviceListExtendedStatus - received status update for Device list property
 *
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */
void BTSetting::onDeviceListExtendedStatus(const ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy >& /*proxy*/
      , const ::boost::shared_ptr< ::MOST_BTSet_FI::DeviceListExtendedStatus >& status)
{
   ETG_TRACE_USR4(("onDeviceListExtendedStatus called"));
   if (status->getU8NumPairedDevices() != 0)
   {
      ::most_BTSet_fi_types_Extended::T_BTSetDeviceListExtendedResult deviceList = status->getODeviceListExtendedResult();
      onDeviceListExtendedStatusResult(deviceList);
   }
   else
   {
      ETG_TRACE_USR4(("No BT device is paired"));
   }
   MediaDatabinding::getInstance().updateBTDeviceConnectedStatus(_isDeviceConnected);
}


/**
 * onDeviceListExtendedStatusResult - called when devicelistextendedstatus  update for Device list property received.
 *
 * @param[in] deviceList
 * @return void
 */
void BTSetting::onDeviceListExtendedStatusResult(::most_BTSet_fi_types_Extended::T_BTSetDeviceListExtendedResult& deviceList)
{
   ETG_TRACE_USR4(("onDeviceListExtendedStatusResult"));
   uint32 currentActiveSource = MediaDatabinding::getInstance().GetCurrentAudioSource();
   ::most_BTSet_fi_types_Extended::T_BTSetDeviceListExtendedResult::iterator itr = deviceList.begin();
   _btDeviceName.clear();
   _connectedDeviceHandle = 0xFF;
   _isDeviceConnected = false;
   for (; itr != deviceList.end(); ++itr)
   {
      if (itr->getBDeviceConnectedStatus()  == true && itr->getODeviceProfileStatus().getBAVP() == true)
      {
         _isDeviceConnected = true;
         _btDeviceName = itr->getSDeviceName();
         MediaDatabinding::getInstance().updateBTDeviceInfo(_btDeviceName.c_str());
         _connectedDeviceHandle = itr->getU8DeviceHandle();
         _btSettingProxy->sendGetDeviceInfoExtendedStart(*this, _connectedDeviceHandle);
         _btSettingProxy->sendBluetoothAudioSourceGet(*this);
         if (_connectedDeviceHandle != _prevDeviceHandle)
         {
            if (currentActiveSource == SRC_PHONE_BTAUDIO)
            {
               MediaDatabinding::getInstance().clearMediaMetaDataInfo();
            }
         }
         ETG_TRACE_USR4(("Connected BT device Handle = %d  connected BT Device name %s", _connectedDeviceHandle, _btDeviceName.c_str()));
      }
   }
}


/**
 * onCallStatusNoticeError - called if CallStatusNotice  property is updated with error from Phone
 *
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void BTSetting::onCallStatusNoticeError(const ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy >& /*proxy*/,
                                        const ::boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeError >& /*error*/)
{
   ETG_TRACE_USR4(("onCallStatusNoticeError called"));
}


/**
 * onCallStatusNoticeStatus - received status update for CallStatusNotice property
 *
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */
void BTSetting::onCallStatusNoticeStatus(const ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeStatus >& status)
{
   ETG_TRACE_USR4(("onCallStatusNoticeStatus called"));
   most_Tel_fi_types::T_TelCallStatusNoticeStream _callStatus = status->getOCallStatusNoticeStream();
   most_Tel_fi_types::T_TelCallStatusNoticeStream::iterator itr = _callStatus.begin();
   bool isHandsetActive = false;
   bool isMetadataVisible = false;
   for (; itr != _callStatus.end(); ++itr)
   {
      uint8 deviceHandle = itr->getU8DeviceHandle();
      bool bIsVehicleAudio = itr->getBUsingVehicleAudio();
      uint8 callStatus = itr->getE8CallStatus();
      if (deviceHandle == _connectedDeviceHandle)
      {
         ETG_TRACE_USR4(("onCallStatusNoticeStatus called:deviceHandle : %d bIsVehicleAudio : %d callStatus : %d ",
                         deviceHandle, bIsVehicleAudio, callStatus));
         ETG_TRACE_USR4(("onCallStatusNoticeStatus : deviceHandle : %d _connectedDeviceHandle : %d", deviceHandle,
                         _connectedDeviceHandle));
         if ((callStatus != ::most_Tel_fi_types::T_e8_TelCallStatus__e8IDLE) && (bIsVehicleAudio == false))
         {
            isHandsetActive = true;
            isMetadataVisible = false;
            MediaDatabinding::getInstance().updateBTScreenOnCallHandsetModeActive(isHandsetActive, isMetadataVisible);
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
            MediaDatabinding::getInstance().updateTrackNoInfoInBT(isMetadataVisible);
#endif
            ETG_TRACE_USR4(("Updated data to data bindings"));
            break;
         }
      }
   }
   if (_isDeviceConnected)
   {
      if (!isHandsetActive)
      {
         isHandsetActive = false;
         isMetadataVisible = true;
         updateTrackInfoInBTMain();
         MediaDatabinding::getInstance().updateBTScreenOnCallHandsetModeActive(isHandsetActive,
               isMetadataVisible);
      }
   }
}


/*
 *
 * tracecmd_ShowBTDefaultScreen message - called when user fires APPHMI_MEDIA ShowBTDefaultScreen through TTFIs.
 *
 * Description the function calls a non static tracecmd_ShowBTDefaultScreen to display the Default screen when BT is not connected.
 * This trace is only for testing purpose for context switching.later it will be removed.
 * @param[in] none
 * @return void
 */
ETG_I_CMD_DEFINE((tracecmd_ShowBTDefaultScreen, "ShowBTDefaultScreen"))

void BTSetting::tracecmd_ShowBTDefaultScreen()
{
   if (_btSettinsInstance)
   {
      ETG_TRACE_USR4(("BTSetting::tracecmd_ShowBTDefaultScreen"));
      _btSettinsInstance->trigger_BTDefaultScreen();
   }
}


/**
 * getDeviceConnectionStatus function - called to get the BT device connection status.
 * @param[in] none
 * @return bool.
 */
bool BTSetting::getDeviceConnectionStatus()
{
   ETG_TRACE_USR4(("BTSetting::getDeviceConnectionStatus"));
   if (_isDeviceConnected == false)
   {
      ETG_TRACE_USR4(("BTSetting::getDeviceConnectionStatus: device is not connected"));
   }
   return _isDeviceConnected;
}


/**
 * trigger_BTDefaultScreen function - called to post courier meesage .
 * @param[in] none
 * @return void.
 */
void BTSetting::trigger_BTDefaultScreen()
{
   if (_isDeviceConnected == false)
   {
      ETG_TRACE_USR4(("BTSetting::getDeviceConnectionStatus: device is not connected"));
      POST_MSG((COURIER_MESSAGE_NEW(ActiveSourceUpdateMsg)(1, SOURCE_BT, 0)));
   }
}


/**
 * onBluetoothAudioSourceError - called if Bluetooth Audio Source property is updated with error from BT
 *
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */

void BTSetting::onBluetoothAudioSourceError(const ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< ::MOST_BTSet_FI::BluetoothAudioSourceError >& /*error*/)
{
   ETG_TRACE_USR4(("BTSetting::onBluetoothAudioSourceError"));
}


/**
 * onBluetoothAudioSourceStatus - received status update for Bluetooth Audio Source property
 *
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */
void BTSetting::onBluetoothAudioSourceStatus(const ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< ::MOST_BTSet_FI::BluetoothAudioSourceStatus >& status)
{
   ETG_TRACE_USR4(("BTSetting::onBluetoothAudioSourceStatus"));
   bool isNextTrackSupportAvailable;
   bool isPrevTrackSupportAvailable;
   bool isFastForwardSupportAvailable;
   bool isFastRewindSupportAvailable;
   bool isRepeatSupportAvailable;
   bool isShuffleSupportAvailable;
   bool isPlayTimeSupportAvailable;
   bool isMetaDataSupportAvailable;
   if (_btSettingProxy && (status->getU8DeviceHandle() == _connectedDeviceHandle))
   {
      isNextTrackSupportAvailable = status->getODeviceControlSupport().getBNextTrackSupport();
      isFastForwardSupportAvailable = status->getODeviceControlSupport().getBFastForwardSupport();
      MediaDatabinding::getInstance().updateBTFFNextTrackSupportStatus(isFastForwardSupportAvailable, isNextTrackSupportAvailable);

      isPrevTrackSupportAvailable = status->getODeviceControlSupport().getBPreviousTrackSupport();
      isFastRewindSupportAvailable = status->getODeviceControlSupport().getBRewindSupport();
      MediaDatabinding::getInstance().updateBTFRPrevTrackSupportStatus(isFastRewindSupportAvailable, isPrevTrackSupportAvailable);

      isRepeatSupportAvailable = status->getODeviceControlSupport().getBRepeatSupport();
      isShuffleSupportAvailable = status->getODeviceControlSupport().getBShuffleSupport();
      MediaDatabinding::getInstance().updateBTRepeatRandomStatus(isRepeatSupportAvailable, isShuffleSupportAvailable);

      isPlayTimeSupportAvailable = status->getODeviceControlSupport().getBPlaytimeSupport();
      MediaDatabinding::getInstance().updateBTPlayTimeSupportStatus(isPlayTimeSupportAvailable);

      isMetaDataSupportAvailable = status->getODeviceControlSupport().getBMetadataSupport();
      MediaDatabinding::getInstance().updateBTMetadataSupportStatus(isMetaDataSupportAvailable);

#ifdef BT_BROWSE_FUNCTION_ENABLE
      bool isFolderBrowseSupportAvailable = status->getODeviceControlSupport().getBFolderBrowseSupport();
      ETG_TRACE_USR4(("BFolderBrowseSupport = %d  ", isFolderBrowseSupportAvailable));
      MediaDatabinding::getInstance().updateBTFolderBrowseSupportStatus(isFolderBrowseSupportAvailable);
#endif
      ETG_TRACE_USR4(("BPlaySupport = %d BPauseSupport = %d ", status->getODeviceControlSupport().getBPlaySupport(),
                      status->getODeviceControlSupport().getBPauseSupport()));
      ETG_TRACE_USR4(("BNextTrackSupport = %d BPreviousTrackSupport = %d ", isNextTrackSupportAvailable
                      , isPrevTrackSupportAvailable));
      ETG_TRACE_USR4(("BFastForwardSupport = %d BRewindSupport = %d ", isFastForwardSupportAvailable
                      , isFastRewindSupportAvailable));
      ETG_TRACE_USR4(("BPlayTimeSupport = %d  ", isPlayTimeSupportAvailable));
      ETG_TRACE_USR4(("BMetadataSupport = %d  ", isMetaDataSupportAvailable));
   }
}


/**
 * onGetDeviceInfoExtendedError - called if DeviceInfo property is updated with error from BT
 *
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void BTSetting::onGetDeviceInfoExtendedError(const ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< ::MOST_BTSet_FI::GetDeviceInfoExtendedError >& /*error*/)
{
   ETG_TRACE_USR4(("BTSetting::onGetDeviceInfoExtendedError"));
}


/**
 * onGetDeviceInfoExtendedResult - called if sendGetDeviceInfoExtendedStart method_result from BT was successful.
 *
 * @param[in] proxy
 * @parm[in] result
 * @return void
 */
void BTSetting::onGetDeviceInfoExtendedResult(const ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< ::MOST_BTSet_FI::GetDeviceInfoExtendedResult >& result)
{
   ETG_TRACE_USR4(("BTSetting::onGetDeviceInfoExtendedResult"));
   if (result->getU8DeviceHandle() == _connectedDeviceHandle)
   {
      _prevDeviceHandle = result->getU8DeviceHandle(); // to store currently connected devicehandle.
      _DeviceAudioStreamingSupportType = result->getOBluetoothFeatureSupport().getE8AudioStreamingSupportType();
      bool isMenuAvailable = false;
      ETG_TRACE_USR4(("Connected BT AudioStreaming support Type :%d", _DeviceAudioStreamingSupportType));
      switch (_DeviceAudioStreamingSupportType)
      {
         case most_BTSet_fi_types::T_e8_BTSetAudioStreamingSupportType__e8AVRCP_NO_METADATA:
         {
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
            POST_MSG((COURIER_MESSAGE_NEW(AVRCPVersionUpdateMsg)(1, LOWER)));
            MediaDatabinding::getInstance().updateTrackNoInfoInBT(isMenuAvailable);
#endif
            MediaDatabinding::getInstance().updateBTMenuSupportStatus(isMenuAvailable);
            break;
         }
         case most_BTSet_fi_types::T_e8_BTSetAudioStreamingSupportType__e8AVRCP_METADATA:
         {
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
            POST_MSG((COURIER_MESSAGE_NEW(AVRCPVersionUpdateMsg)(1, LOWER)));
            MediaDatabinding::getInstance().updateTrackNoInfoInBT(isMenuAvailable);
#endif
            MediaDatabinding::getInstance().updateBTMenuSupportStatus(isMenuAvailable);
            break;
         }
         case most_BTSet_fi_types::T_e8_BTSetAudioStreamingSupportType__e8AVRCP_BROWSING:
         case most_BTSet_fi_types::T_e8_BTSetAudioStreamingSupportType__e8AVRCP_MYMEDIA:
         {
            isMenuAvailable = true;
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
            POST_MSG((COURIER_MESSAGE_NEW(AVRCPVersionUpdateMsg)(1, HIGHER)));
            MediaDatabinding::getInstance().updateTrackNoInfoInBT(isMenuAvailable);
#endif
            MediaDatabinding::getInstance().updateBTMenuSupportStatus(isMenuAvailable);
            break;
         }
         default:
         {
            ETG_TRACE_USR4(("Connected BT device  AVRCP version unknown"));
            break;
         }
      }
   }
}


/**
 * updateTrackInfoInBTMain function - called to get the AVRCP version of currently connected phone .
 * @param[in] none
 * @return void.
 */
void BTSetting::updateTrackInfoInBTMain()
{
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
   if (_DeviceAudioStreamingSupportType == most_BTSet_fi_types::T_e8_BTSetAudioStreamingSupportType__e8AVRCP_BROWSING ||
         _DeviceAudioStreamingSupportType == most_BTSet_fi_types::T_e8_BTSetAudioStreamingSupportType__e8AVRCP_MYMEDIA)
   {
      MediaDatabinding::getInstance().updateTrackNoInfoInBT(true);
   }
   else
   {
      MediaDatabinding::getInstance().updateTrackNoInfoInBT(false);
   }
#endif
}


}//App
}//Core
