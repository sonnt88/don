/**
* @file MetaData.h
* @author RBEI/ECV-AIVI_MediaTeam
* @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
* @addtogroup AppHMI_Media
*/

#ifndef MPLAY_FI_TYPES_H_
#define MPLAY_FI_TYPES_H_

#include <string>

namespace MPlay_fi_types {

enum T_e8_MPlayCategoryType
{
   /**
    * If the meaning of "e8CTY_NONE" isn't clear, then there should be a
    * description here.
    */
   T_e8_MPlayCategoryType__e8CTY_NONE = 0u,
   /**
    * If the meaning of "e8CTY_GENRE" isn't clear, then there should be a
    * description here.
    */
   T_e8_MPlayCategoryType__e8CTY_GENRE = 1u,
   /**
    * If the meaning of "e8CTY_ARTIST" isn't clear, then there should be a
    * description here.
    */
   T_e8_MPlayCategoryType__e8CTY_ARTIST = 2u,
   /**
    * If the meaning of "e8CTY_ALBUM" isn't clear, then there should be a
    * description here.
    */
   T_e8_MPlayCategoryType__e8CTY_ALBUM = 3u,
   /**
    * If the meaning of "e8CTY_SONG" isn't clear, then there should be a
    * description here.
    */
   T_e8_MPlayCategoryType__e8CTY_SONG = 4u,
   /**
    * If the meaning of "e8CTY_COMPOSER" isn't clear, then there should be a
    * description here.
    */
   T_e8_MPlayCategoryType__e8CTY_COMPOSER = 5u,
   /**
    * If the meaning of "e8CTY_AUTHOR" isn't clear, then there should be a
    * description here.
    */
   T_e8_MPlayCategoryType__e8CTY_AUTHOR = 6u,
   /**
    * If the meaning of "e8CTY_TITLE" isn't clear, then there should be a
    * description here.
    */
   T_e8_MPlayCategoryType__e8CTY_TITLE = 7u,
   /**
    * If the meaning of "e8CTY_CHAPTER" isn't clear, then there should be a
    * description here.
    */
   T_e8_MPlayCategoryType__e8CTY_CHAPTER = 8u,
   /**
    * If the meaning of "e8CTY_NAME" isn't clear, then there should be a
    * description here.
    */
   T_e8_MPlayCategoryType__e8CTY_NAME = 9u,
   /**
    * If the meaning of "e8CTY_EPISODE" isn't clear, then there should be a
    * description here.
    */
   T_e8_MPlayCategoryType__e8CTY_EPISODE = 10u,
   /**
    * If the meaning of "e8CTY_PLAYLIST" isn't clear, then there should be a
    * description here.
    */
   T_e8_MPlayCategoryType__e8CTY_PLAYLIST = 11u,
   /**
    * If the meaning of "e8CTY_PLAYLIST_INTERNAL" isn't clear, then there
    * should be a description here.
    */
   T_e8_MPlayCategoryType__e8CTY_PLAYLIST_INTERNAL = 12u,
   /**
    * If the meaning of "e8CTY_IMAGE" isn't clear, then there should be a
    * description here.
    */
   T_e8_MPlayCategoryType__e8CTY_IMAGE = 13u
};


enum T_e8_MPlayRepeat
{
   /**
    * If the meaning of "e8RPT_NONE" isn't clear, then there should be a description here.
    */
   T_e8_MPlayRepeat__e8RPT_NONE = 0u,
   /**
    * If the meaning of "e8RPT_ONE" isn't clear, then there should be a description here.
    */
   T_e8_MPlayRepeat__e8RPT_ONE = 1u,
   /**
    * If the meaning of "e8RPT_LIST" isn't clear, then there should be a description here.
    */
   T_e8_MPlayRepeat__e8RPT_LIST = 2u,
   /**
    * If the meaning of "e8RPT_LIST_WITH_SUBLISTS" isn't clear, then there should be a description here.
    */
   T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS = 3u,
   /**
    * If the meaning of "e8RPT_ALL" isn't clear, then there should be a description here.
    */
   T_e8_MPlayRepeat__e8RPT_ALL = 4u
};


enum T_e8_MPlayMode
{
   /**
    * If the meaning of "e8PBM_NORMAL" isn't clear, then there should be a description here.
    */
   T_e8_MPlayMode__e8PBM_NORMAL = 0u,
   /**
    * If the meaning of "e8PBM_RANDOM" isn't clear, then there should be a description here.
    */
   T_e8_MPlayMode__e8PBM_RANDOM = 1u
};


/**
 * Defines the type of list requested from the MediaPlayer.
 */
enum T_e8_MPlayListType
{
   /**
    * If the meaning of "e8LTY_GENRE" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_GENRE = 0u,
   /**
    * If the meaning of "e8LTY_ARTIST" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_ARTIST = 1u,
   /**
    * If the meaning of "e8LTY_ALBUM" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_ALBUM = 2u,
   /**
    * If the meaning of "e8LTY_SONG" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_SONG = 3u,
   /**
    * If the meaning of "e8LTY_GENRE_ARTIST" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_GENRE_ARTIST = 4u,
   /**
    * If the meaning of "e8LTY_GENRE_ARTIST_ALBUM" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_GENRE_ARTIST_ALBUM = 5u,
   /**
    * If the meaning of "e8LTY_GENRE_ARTIST_ALBUM_SONG" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_GENRE_ARTIST_ALBUM_SONG = 6u,
   /**
    * If the meaning of "e8LTY_GENRE_ARTIST_SONG" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_GENRE_ARTIST_SONG = 7u,
   /**
    * If the meaning of "e8LTY_GENRE_ALBUM" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_GENRE_ALBUM = 8u,
   /**
    * If the meaning of "e8LTY_GENRE_ALBUM_SONG" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_GENRE_ALBUM_SONG = 9u,
   /**
    * If the meaning of "e8LTY_GENRE_SONG" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_GENRE_SONG = 10u,
   /**
    * If the meaning of "e8LTY_ARTIST_ALBUM" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_ARTIST_ALBUM = 11u,
   /**
    * If the meaning of "e8LTY_ARTIST_ALBUM_SONG" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_ARTIST_ALBUM_SONG = 12u,
   /**
    * If the meaning of "e8LTY_ARTIST_SONG" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_ARTIST_SONG = 13u,
   /**
    * If the meaning of "e8LTY_ALBUM_SONG" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_ALBUM_SONG = 14u,
   /**
    * If the meaning of "e8LTY_PODCAST" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_PODCAST = 15u,
   /**
    * If the meaning of "e8LTY_PODCAST_EPISODE" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_PODCAST_EPISODE = 16u,
   /**
    * If the meaning of "e8LTY_AUDIOBOOK" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_AUDIOBOOK = 17u,
   /**
    * If the meaning of "e8LTY_BOOKTITLE_CHAPTER" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_BOOKTITLE_CHAPTER = 18u,
   /**
    * If the meaning of "e8LTY_AUTHOR" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_AUTHOR = 19u,
   /**
    * If the meaning of "e8LTY_AUTHOR_BOOKTITLE" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_AUTHOR_BOOKTITLE = 20u,
   /**
    * If the meaning of "e8LTY_AUTHOR_BOOKTITLE_CHAPTER" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_AUTHOR_BOOKTITLE_CHAPTER = 21u,
   /**
    * If the meaning of "e8LTY_COMPOSER" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_COMPOSER = 22u,
   /**
    * If the meaning of "e8LTY_COMPOSER_ALBUM" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_COMPOSER_ALBUM = 23u,
   /**
    * If the meaning of "e8LTY_COMPOSER_ALBUM_SONG" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_COMPOSER_ALBUM_SONG = 24u,
   /**
    * If the meaning of "e8LTY_COMPOSER_SONG" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_COMPOSER_SONG = 25u,
   /**
    * If the meaning of "e8LTY_VIDEO" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_VIDEO = 26u,
   /**
    * If the meaning of "e8LTY_VIDEO_EPISODE" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_VIDEO_EPISODE = 27u,
   /**
    * If the meaning of "e8LTY_PLAYLIST" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_PLAYLIST = 28u,
   /**
    * If the meaning of "e8LTY_PLAYLIST_SONG" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_PLAYLIST_SONG = 29u,
   /**
    * If the meaning of "e8LTY_CURRENT_SELECTION" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_CURRENT_SELECTION = 30u,
   /**
    * If the meaning of "e8LTY_IMAGE" isn't clear, then there should be a description here.
    */
   T_e8_MPlayListType__e8LTY_IMAGE = 31u
};


/**
 * Type of device on which the media object is stored.
 */
enum T_e8_MPlayDeviceType
{
   /**
    * If the meaning of "e8DTY_UNKNOWN" isn't clear, then there should be a description here.
    */
   T_e8_MPlayDeviceType__e8DTY_UNKNOWN = 0u,
   /**
    * If the meaning of "e8DTY_USB" isn't clear, then there should be a description here.
    */
   T_e8_MPlayDeviceType__e8DTY_USB = 1u,
   /**
    * If the meaning of "e8DTY_IPOD" isn't clear, then there should be a description here.
    */
   T_e8_MPlayDeviceType__e8DTY_IPOD = 2u,
   /**
    * If the meaning of "e8DTY_SD" isn't clear, then there should be a description here.
    */
   T_e8_MPlayDeviceType__e8DTY_SD = 3u,
   /**
    * If the meaning of "e8DTY_BLUETOOTH" isn't clear, then there should be a description here.
    */
   T_e8_MPlayDeviceType__e8DTY_BLUETOOTH = 4u,
   /**
    * If the meaning of "e8DTY_IPHONE" isn't clear, then there should be a description here.
    */
   T_e8_MPlayDeviceType__e8DTY_IPHONE = 5u,
   /**
    * If the meaning of "e8DTY_MTP" isn't clear, then there should be a description here.
    */
   T_e8_MPlayDeviceType__e8DTY_MTP = 6u,
   /**
    * If the meaning of "e8DTY_CDROM" isn't clear, then there should be a description here.
    */
   T_e8_MPlayDeviceType__e8DTY_CDROM = 7u,
   /**
    * If the meaning of "e8DTY_FLASH" isn't clear, then there should be a description here.
    */
   T_e8_MPlayDeviceType__e8DTY_FLASH = 8u,
   /**
    * If the meaning of "e8DTY_CDDA" isn't clear, then there should be a description here.
    */
   T_e8_MPlayDeviceType__e8DTY_CDDA = 9u,
   /**
    * If the meaning of "e8DTY_UNSUPPORTED" isn't clear, then there should be a description here.
    */
   T_e8_MPlayDeviceType__e8DTY_UNSUPPORTED = 10u
};


class T_MPlayMediaObject
{
   public:
      T_MPlayMediaObject();
      virtual ~T_MPlayMediaObject();

      std::string getSMetaDataField1() const;
      std::string getSMetaDataField2() const;
      std::string getSMetaDataField3() const;
      std::string getSMetaDataField4() const;

      T_e8_MPlayCategoryType getE8CategoryType() const;
      T_e8_MPlayDeviceType getE8DeviceType() const;
      unsigned int getU32Tag() const;

      void setSMetaDataField1(const std::string& data);
      void setSMetaDataField2(const std::string& data);
      void setSMetaDataField3(const std::string& data);
      void setSMetaDataField4(const std::string& data);

      void setE8CategoryType(const T_e8_MPlayCategoryType& category);

   private:
      std::string                  _metaData[4];
      T_e8_MPlayCategoryType       _category;
      T_e8_MPlayDeviceType _devicetype;
      unsigned int _tag;
};


} /* namespace MPlay_fi_types */
#endif /* MPLAY_FI_TYPES_H_ */
