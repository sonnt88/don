/**
 *  @file   <DeviceStatusList_Test.h>
 *  @author <ECV> - <pul1hc>
 *  @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
 *  @addtogroup <AppHmi_media>
 *  @{
 */

#ifndef DEVICESTATUSLIST_TEST_H_
#define DEVICESTATUSLIST_TEST_H_

#include "gtest/gtest.h"
#include "Core/Utils/MediaUtils.h"
#include "Core/DeviceStatus/DeviceStatusList.h"
#include "Core/Utils/MediaProxyUtility.h"

using namespace std::tr1;
using namespace App::Core;

class DeviceStatusList_Test__updateUSBInfo_withValidParam: public testing::TestWithParam<tuple<uint32, std::string> >
{
   protected:
      uint32 _dummyActiveDeviceType;
      std::string _strExpectedUSBInfo;
      DeviceStatusList _deviceStatusList;

   protected:
      virtual void SetUp()
      {
         _dummyActiveDeviceType = get<0>(GetParam());
         _strExpectedUSBInfo = get<1>(GetParam());
         _deviceStatusList.setUSBInfo("Unknown Medium");
         MediaProxyUtility::getInstance().setDummyActiveDeviceType(_dummyActiveDeviceType);
      }

      virtual void TearDown()
      {
      }
};


class DeviceStatusList_Test__updateUSBInfo_withInvalidParam: public testing::TestWithParam<tuple<uint32, std::string> >
{
   protected:
      uint32 _dummyActiveDeviceType;
      std::string _strExpectedUSBInfo;
      DeviceStatusList _deviceStatusList;

   protected:
      virtual void SetUp()
      {
         _dummyActiveDeviceType = get<0>(GetParam());
         _strExpectedUSBInfo = get<1>(GetParam());
         _deviceStatusList.setUSBInfo("Unknown Medium");
         MediaProxyUtility::getInstance().setDummyActiveDeviceType(_dummyActiveDeviceType);
      }
      virtual void TearDown()
      {
      }
};


class DeviceStatusList_Test__updateCDInfo_withValidParam: public testing::TestWithParam<tuple<uint32, std::string> >
{
   protected:
      uint32 _dummyActiveDeviceType;
      std::string _strExpectedCDInfo;
      DeviceStatusList _deviceStatusList;

   protected:
      virtual void SetUp()
      {
         _dummyActiveDeviceType = get<0>(GetParam());
         _strExpectedCDInfo = get<1>(GetParam());
         _deviceStatusList.setCDInfo("No Medium");
         MediaProxyUtility::getInstance().setDummyActiveDeviceType(_dummyActiveDeviceType);
      }

      virtual void TearDown()
      {
      }
};


class DeviceStatusList_Test__updateCDInfo_withInvalidParam: public testing::TestWithParam<tuple<uint32, std::string> >
{
   protected:
      uint32 _dummyActiveDeviceType;
      std::string _strExpectedCDInfo;
      DeviceStatusList _deviceStatusList;

   protected:
      virtual void SetUp()
      {
         _dummyActiveDeviceType = get<0>(GetParam());
         _strExpectedCDInfo = get<1>(GetParam());
         _deviceStatusList.setCDInfo("No Medium");
         MediaProxyUtility::getInstance().setDummyActiveDeviceType(_dummyActiveDeviceType);
      }
      virtual void TearDown()
      {
      }
};


class DeviceStatusList_Test__getServiceSystemStatusItemValue_withValidParam: public testing::TestWithParam <
   tuple<uint8, uint32, std::string> >
{
   protected:
      uint8 _itemIndex;
      uint32 _dummyActiveDeviceType;
      std::string _cStrExpectedItemValue;
      DeviceStatusList _deviceStatusList;

   protected:
      virtual void SetUp()
      {
         _itemIndex = get<0>(GetParam());
         _dummyActiveDeviceType = get<1>(GetParam());
         _cStrExpectedItemValue = get<2>(GetParam());
         _deviceStatusList.setCDStatus("650MB");
         _deviceStatusList.setCDInfo("Medium CDMp3");
         _deviceStatusList.setUSBStatus("16384");
         _deviceStatusList.setUSBInfo("Mass Storage");
         MediaProxyUtility::getInstance().setDummyActiveDeviceType(_dummyActiveDeviceType);
      }

      virtual void TearDown()
      {
      }
};


class DeviceStatusList_Test__getServiceSystemStatusItemValue_withInvalidParam: public testing::TestWithParam <
   tuple<uint8, uint32, std::string> >
{
   protected:
      uint8 _itemIndex;
      uint32 _dummyActiveDeviceType;
      std::string _cStrExpectedItemValue;
      DeviceStatusList _deviceStatusList;

   protected:
      virtual void SetUp()
      {
         _itemIndex = get<0>(GetParam());
         _dummyActiveDeviceType = get<1>(GetParam());
         _cStrExpectedItemValue = get<2>(GetParam());
         _deviceStatusList.setCDStatus("4096MB");
         _deviceStatusList.setCDInfo("Medium CDDA");
         _deviceStatusList.setUSBStatus("8192");
         _deviceStatusList.setUSBInfo("iPod");
         MediaProxyUtility::getInstance().setDummyActiveDeviceType(_dummyActiveDeviceType);
      }

      virtual void TearDown()
      {
      }
};


#endif /* DEVICESTATUSLIST_TEST_H_ */
