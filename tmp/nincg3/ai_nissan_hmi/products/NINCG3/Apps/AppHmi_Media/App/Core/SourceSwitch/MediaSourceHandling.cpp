/**
 *  @file   MediaSourceHandling.cpp
 *  @author ECV - kng3kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */
#include "hall_std_if.h"
#include "Core/SourceSwitch/MediaSourceHandling.h"
#include "Core/PlayScreen/IPlayScreen.h"
#include "Core/Utils/MediaProxyUtility.h"
#include "AppHmi_MasterBase/AudioInterface/AudioDefines.h"
#include "mplay_MediaPlayer_FIProxy.h"
#include "Core/BTAudio/IBTSettings.h"
#include "Core/DeviceConnection/IDeviceConnection.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL_SOURCE_SWITCH
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::MediaSourceHandling::
#include "trcGenProj/Header/MediaSourceHandling.cpp.trc.h"
#endif


#define AUX_TYPE                     (20u)
#define SA_SINKID_SOURCE_ONE  (1u)
#define EMPTY_HEADER_TEXT 1u

namespace App {
namespace Core {


using namespace std;
using namespace MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange;
using namespace MASTERAUDIOSERVICE_INTERFACE::SoundProperties;
using namespace org::genivi::audiomanager::CommandInterface;
#define SUB_SOURCEID_NOTFOUND 0xFF


MediaSourceHandling* MediaSourceHandling::_mediaSourceHandling = NULL;

/**
 * @Destructor
 */
MediaSourceHandling::~MediaSourceHandling()
{
   _mediaPlayerProxy.reset();
   _audioSourceChangeProxy.reset();
   _soundPropertiesProxy.reset();
   _commandIFProxy.reset();
   _mediaSourceHandling = NULL;
   if (NULL != _hmiDataHandler)
   {
      delete _hmiDataHandler;
   }
   _hmiDataHandler = NULL;
}


/**
 * @Constructor
 */
MediaSourceHandling::MediaSourceHandling(IBTSettings* _pBTSettings, IDeviceConnection* pDeviceConnection, IPlayScreen* pPlayScreen,
      ::boost::shared_ptr < AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy >& pCommandInterfaceProxy, ContextSwitchHomeScreen* pHomeScreen
      , ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy, HmiDataHandlerExt* hmiDataHandler)
   : _audioSourceChangeProxy(AudioSourceChangeProxy::createProxy("audioSourceChangePort", *this))
   , _soundPropertiesProxy(SoundPropertiesProxy::createProxy("soundPropertiesPort", *this))
   , _commandIFProxy(pCommandInterfaceProxy)
   , _iBTSettings(*_pBTSettings)
   , _iDeviceConnection(*pDeviceConnection)
   , _iPlayScreen(*pPlayScreen)
   , _currentActiveMediaSource(SOURCE_NOT_DEF)
   , _currentActiveMediaSUBSourceID(0)
   , _currentAudioSource(SRC_MEDIA_NO_SRC)
   , _extendedUSBSupport(true)
   , _DeviceHomeScreen(*pHomeScreen)
   , _mediaPlayerProxy(pMediaPlayerProxy)
   , sinkid(0)
   , _SrcChgContextid(0)
   , _SrcChgSwitchid(0)
   , _prevContextid(0)
   , _prevSwitchid(0)
   , _ContextSwitchWaitingView("")
   , _isSpiAppStateActive(false)
   ,  _hmiDataHandler(hmiDataHandler)
{
   StartupSync::getInstance().registerPropertyRegistrationIF(this);
   ETG_I_REGISTER_FILE();
   _mediaSourceHandling = this;
   _viewname = "";
}


/**
 * registerProperties - Trigger property registration to hmi_master,  called from MediaHall class
 * @param[in] proxy
 * @parm[in] stateChange - state change service for corrosponding  proxy
 * @return void
 */
void MediaSourceHandling::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_audioSourceChangeProxy && _audioSourceChangeProxy == proxy)
   {
      ETG_TRACE_USR4(("MEDIAAPP : AudioSource Change Available"));
      _audioSourceChangeProxy->sendActiveSourceRegister(*this);
      _audioSourceChangeProxy->sendSourceListChangedRegister(*this);
      _audioSourceChangeProxy->sendGetSourceListRequest(*this, 2);
      _audioSourceChangeProxy->sendActiveSourceListRegister(*this);

      //workaround to get current active source on start of systeam currently first update is missing
      if (!_mediaPlayerProxy->hasMediaPlayerDeviceConnections())
      {
         _iDeviceConnection.registerObserver(this);
      }
      _audioSourceChangeProxy->sendActiveSourceGet(*this);
   }
   else if (_commandIFProxy && _commandIFProxy == proxy)
   {
      _commandIFProxy->sendMainSinkSoundPropertyChangedRegister(*this);
      _commandIFProxy->sendGetListMainSinkSoundPropertiesRequest(*this, SA_SINKID_SOURCE_ONE);
   }
   else if (_soundPropertiesProxy && _soundPropertiesProxy == proxy)
   {
      _soundPropertiesProxy->sendVolumeRegister(*this);
      _soundPropertiesProxy->sendVolumeGet(*this);
      _soundPropertiesProxy->sendMuteStateRegister(*this);
      _soundPropertiesProxy->sendMuteStateGet(*this);
   }
}


/**
 * deregisterProperties - Trigger property deregistration to hmi_master,  called from MediaHall class
 * @param[in] proxy
 * @parm[in] stateChange - state change service for corrosponding  proxy
 * @return void
 */
void MediaSourceHandling::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_audioSourceChangeProxy && _audioSourceChangeProxy == proxy)
   {
      _audioSourceChangeProxy->sendDeregisterAll();
   }
   else if (_commandIFProxy && _commandIFProxy == proxy)
   {
      _commandIFProxy->sendMainSinkSoundPropertyChangedDeregisterAll();
   }
   else if (_soundPropertiesProxy && _soundPropertiesProxy == proxy)
   {
      _soundPropertiesProxy->sendVolumeDeregisterAll();
      _soundPropertiesProxy->sendMuteStateDeregisterAll();
   }
}


/**
 * onMainSinkSoundPropertyChangedError - Error handling for sendGetListMainSinkSoundPropertiesRequest
 * @param[in] proxy
 * @param[in] error
 * @param[out] none
 * @return void
 */
void MediaSourceHandling::onMainSinkSoundPropertyChangedError(const ::boost::shared_ptr<CommandInterfaceProxy >& /*proxy*/,
      const ::boost::shared_ptr<MainSinkSoundPropertyChangedError >& /*error*/)
{
   ETG_TRACE_USR4(("MediaSourceHandling:onMainSinkSoundPropertyChangedError is called"));
}


/**
 * onMainSinkSoundPropertyChangedSignal - Status/Signal handling for MainSinkSoundPropertyChanged
 * @param[in] proxy
 * @param[in] error
 * @param[out] none
 * @return void
 */
void MediaSourceHandling::onMainSinkSoundPropertyChangedSignal(const ::boost::shared_ptr<CommandInterfaceProxy >& /*proxy*/,
      const ::boost::shared_ptr<MainSinkSoundPropertyChangedSignal >& signal)
{
   ETG_TRACE_USR4(("MediaSourceHandling:onMainSinkSoundPropertyChangedSignal is called"));
   //MediaDatabinding::getInstance().updateVolumeAvailability(signal->getSoundProperty().getElem2());			//This is used to update the volume availablity in screen and for playback operations

   sinkid = signal->getSinkID();
   //Check received property is Aux or not.
   //If it is Aux , then update the current Aux level.
   if ((signal->getSoundProperty().getElem1()) == AUX_TYPE)
   {
      if ((signal->getSoundProperty().getElem2() >= 1) && (signal->getSoundProperty().getElem2() <= 3))
      {
         ETG_TRACE_USR4(("onMainSinkSoundPropertyChangedSignal:%d", signal->getSoundProperty().getElem2()));
         MediaDatabinding::getInstance().updateAuxLevel(signal->getSoundProperty().getElem2());

         //TODO: cal the binding function with Para of signal->getSoundProperty().getElem2()
      }
      else
      {
         ETG_TRACE_USR4(("MediaSourceHandling:Info:Invalid Aux Level"));
      }
   }
}


/**
 * onGetListMainSinkSoundPropertiesError - Error handling for GetListMainSinkSoundProperties
 * @param[in] proxy
 * @param[in] error
 * @param[out] none
 * @return void
 */
void MediaSourceHandling::onGetListMainSinkSoundPropertiesError(const ::boost::shared_ptr<AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy >& /*proxy*/,
      const ::boost::shared_ptr<AUDIOMANAGER_COMMANDINTERFACE::GetListMainSinkSoundPropertiesError >& /*error*/)
{
   ETG_TRACE_USR4(("MediaSourceHandling:onGetListMainSinkSoundPropertiesError is called"));
}


/**
 * onGetListMainSinkSoundPropertiesResponse - Response handling for GetListMainSinkSoundProperties
 * @param[in] proxy
 * @param[in] error
 * @param[out] none
 * @return void
 */
void MediaSourceHandling::onGetListMainSinkSoundPropertiesResponse(const ::boost::shared_ptr<AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy >& /*proxy*/,
      const ::boost::shared_ptr<AUDIOMANAGER_COMMANDINTERFACE::GetListMainSinkSoundPropertiesResponse >& response)
{
   ETG_TRACE_USR4(("AudioServiceClient:onGetListMainSinkSoundPropertiesResponse is called"));
   vector< GetListMainSinkSoundPropertiesResponseListSoundPropertiesStruct > _audioPropertyVector = response->getListSoundProperties();
   vector< GetListMainSinkSoundPropertiesResponseListSoundPropertiesStruct >::iterator Itr = _audioPropertyVector.begin();

   for (; Itr != _audioPropertyVector.end(); ++Itr)
   {
      ETG_TRACE_USR4((" itr value %d", Itr->getElem2()));
      if (AUX_TYPE == Itr->getElem1())
      {
         ETG_TRACE_USR4((" AUX_TYPE found:%d", Itr->getElem1()));
         //MediaDatabinding::getInstance().updateAuxLevel(Itr->getElem2());
         MediaDatabinding::getInstance().updateAuxLevel(Itr->getElem2());
      }
   }
}


/**
 * onSetMainSinkSoundPropertyError - Error handling for SetMainSinkSoundProperty
 * @param[in] proxy
 * @param[in] error
 * @param[out] none
 * @return void
 */
void MediaSourceHandling::onSetMainSinkSoundPropertyError(const ::boost::shared_ptr< CommandInterfaceProxy >& /*proxy*/,
      const ::boost::shared_ptr< SetMainSinkSoundPropertyError >& /*error*/)
{
   ETG_TRACE_ERR(("MediaSourceHandling:onSetMainSinkSoundPropertyError is called"));
}


/**
 * onSetMainSinkSoundPropertyResponse - response for SetMainSinkSoundProperty
 * @param[in] proxy
 * @param[in] error
 * @param[out] none
 * @return void
 */
void MediaSourceHandling::onSetMainSinkSoundPropertyResponse(const ::boost::shared_ptr< AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy >& /*proxy*/,
      const ::boost::shared_ptr< AUDIOMANAGER_COMMANDINTERFACE::SetMainSinkSoundPropertyResponse >& /*response*/)
{
   ETG_TRACE_ERR(("MediaSourceHandling:onSetMainSinkSoundPropertyResponse is called"));
}


/**
 * onActivateSourceError - Notification from HMI master when source requested for activation is unsuccessful
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void MediaSourceHandling::onActivateSourceError(const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/, const ::boost::shared_ptr< ActivateSourceError >& /*error*/)
{
   ETG_TRACE_USR4(("onActivateSourceError received"));
}


/**
 * onActivateSourceResponse - Notification from HMI master when source requested for activation is successful
 * @param[in] proxy
 * @parm[in] response
 * @return void
 */
void MediaSourceHandling::onActivateSourceResponse(const ::boost::shared_ptr< AudioSourceChangeProxy >& proxy, const ::boost::shared_ptr< ActivateSourceResponse >& /*response*/)
{
   if (_audioSourceChangeProxy == proxy)
   {
      ETG_TRACE_USR4(("onActivateSourceResponse received"));
   }
}


/**
 * onDeactivateSourceError - Notification from HMI master when request for source deactivation is not successful
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void MediaSourceHandling::onDeactivateSourceError(const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/, const ::boost::shared_ptr< DeactivateSourceError >& /*error*/)
{
   ETG_TRACE_USR4(("onDeactivateSourceError received"));
}


/**
 * onDeactivateSourceResponse - Notification from HMI master when request for source deactivation is successful
 * @param[in] proxy
 * @parm[in] response
 * @return void
 */
void MediaSourceHandling::onDeactivateSourceResponse(const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/, const ::boost::shared_ptr< DeactivateSourceResponse >& /*response*/)
{
   ETG_TRACE_USR4(("onDeactivateSourceResponse received"));
}


/**
 * onSourceListChangedError - Error notification from HMI master when sourceListChange error happens
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void MediaSourceHandling::onSourceListChangedError(const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/, const ::boost::shared_ptr< SourceListChangedError >& /*error*/)
{
   ETG_TRACE_USR4(("onSourceListChangedError received"));
}


/**
 * onSourceListChangedSignal - Notification from HMI master when SourceListChange is obtained
 * @param[in] proxy
 * @parm[in] signal
 * @return void
 */
void MediaSourceHandling::onSourceListChangedSignal(const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/, const ::boost::shared_ptr< SourceListChangedSignal >& /*signal*/)
{
   ETG_TRACE_USR4(("onSourceListChangedSignal received"));

   if (_audioSourceChangeProxy)
   {
      ETG_TRACE_USR4(("sendGetSourceListRequest for media"));
      //Request for the changed source list
      _audioSourceChangeProxy->sendGetSourceListRequest(*this, GROUP_MEDIA); //TODO:need to check group media id
   }
}


/**
 * onGetSourceListError - Notification from HMI master when SourceListChange request is unsuccessful
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void MediaSourceHandling::onGetSourceListError(const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/, const ::boost::shared_ptr< GetSourceListError >& /*error*/)
{
   ETG_TRACE_USR4(("onGetSourceListError received"));
}


/**
 * onGetSourceListResponse - Notification from HMI master when request for source list is successful
 * @param[in] proxy
 * @parm[in] signal
 * @return void
 */
void MediaSourceHandling::onGetSourceListResponse(const ::boost::shared_ptr< AudioSourceChangeProxy >& proxy, const ::boost::shared_ptr< GetSourceListResponse >& response)
{
   ETG_TRACE_USR4(("onGetSourceListResponse received"));
   if (_audioSourceChangeProxy == proxy)
   {
      const ::std::vector< sourceDetails >& SourceList = response->getSources();
      ::std::vector< sourceDetails >::const_iterator it = SourceList.begin();

      ETG_TRACE_USR4(("onGetSourceListResponse Sourcelistsize:%d", SourceList.size()));

      //When a New list update arrives, reset the availability of all sources - Set to FALSE
      _sourcelist.clear();

      while (it != SourceList.end())
      {
         // Update the list info using UpdateSourceList
         updateSourceList(it->getSrcId(), it->getSubSrcId(), it->getAvailability(), it->getAvailabilityReason());

         if ((it->getSrcId() == SRC_MEDIA_PLAYER) && (MediaProxyUtility::getInstance().getHallDeviceType(it->getSubSrcId()) == SOURCE_BT)
               && (_currentAudioSource == SRC_PHONE_BTAUDIO) && (it->getAvailability() == 1))
         {
            requestSourceActivation(it->getSrcId(), it->getSubSrcId());
         }
         it++;
      } // End of while loop
   }
}


/**
 * vUpdateSourceList - private helper function to update source list change
 * @param[in] SrcID, SubSrcID - source id and sub source id
 * @parm[in] Availability, AvailabilityReason source is available or not and reason for availability
 * @return void
 */
void MediaSourceHandling::updateSourceList(int32 SrcID, int32 SubSrcID, int32 Availability, int32 AvailabilityReason)
{
   ETG_TRACE_USR4(("MediaSourceHandling::vUpdateSourceList SrcID:%d SubSrcID:%d ", SrcID, SubSrcID));

   // int32 i32SourceType = i32GetHallSourceType(SrcID, SubSrcID);

   SourceDetailInfo oSourceInfo;

   //Set all necessary info for the source in the internal vector
   //oSourceInfo.i32SourceType = i32SourceType;
   oSourceInfo.availability = Availability;
   oSourceInfo.availabilityReason = AvailabilityReason;
   oSourceInfo.sourceID = SrcID;
   oSourceInfo.subSourceID = SubSrcID;
   if (SubSrcID > 0) // If the Device ID is valid only then determine the type of the Device, 0 is MyMedia and -1 is invalid device tag
   {
      oSourceInfo.sourceType = MediaProxyUtility::getInstance().GetDeviceType(SubSrcID);
   }

   ETG_TRACE_USR4(("SourceList: oSourceInfo.i32SourceType: %d", oSourceInfo.sourceType));
   ETG_TRACE_USR4(("oSourceInfo.i32Availability: %d ", oSourceInfo.availability));
   ETG_TRACE_USR4(("oSourceInfo.i32AvailabilityReason: %d ", oSourceInfo.availabilityReason));

   _sourcelist.push_back(oSourceInfo);
   MediaDatabinding::getInstance().updateConnectedDevice(_sourcelist);
}


/**
 * vRequestSourceActivation - helper function tused by Media classes to request a source activation
 * @param[in] SrcID, SubSrcID - source id and sub source id
 * @return void
 */
void MediaSourceHandling::requestSourceActivation(int32 SrcID, int32 SubSrcID)
{
   ETG_TRACE_USR4(("vRequestSourceActivation SrcID %d SubSrcID %d", SrcID, SubSrcID));
   sourceData sourceInfo(SrcID, SubSrcID, 0);
   _audioSourceChangeProxy->sendActivateSourceRequest(*this, sourceInfo);
}


/**
 * onActiveSourceError - Notification from HMI master when  active source property update is unsuccessful
 * @param[in] proxy, error
 * @return void
 */
void MediaSourceHandling::onActiveSourceError(const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/, const ::boost::shared_ptr< ActiveSourceError >& /*error*/)
{
}


/**
 * onActiveSourceUpdate - Notification from HMI master when  active source property update is successful
 * @param[in] proxy, update
 * @return void
 */
void MediaSourceHandling::onActiveSourceUpdate(const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/, const ::boost::shared_ptr< ActiveSourceUpdate >& update)
{
   int32 sourceId = update->getActiveSource().getSrcId();
   int32 subSourceId = update->getActiveSource().getSubSrcId();
   ETG_TRACE_USR4(("onActiveSourceUpdate SrcID %d SubSrcID %d", sourceId, subSourceId));
   bool isBTConnected = isBTDeviceconnected();

   POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE)));
   _iPlayScreen.updateLoadingFilePopupStatus(false);
   MediaDatabinding::getInstance().updateAudioSource(sourceId);
   POST_MSG((COURIER_MESSAGE_NEW(SourceChangeUpdMsg)(sourceId)));

   int32 prevAudioSrc = _currentAudioSource;
   int32 prevActiveMediaSrc = _currentActiveMediaSource;
   int32 prevActiveSubSrcId = _currentActiveMediaSUBSourceID;
   ETG_TRACE_USR4(("prevAudioSrc %d prevActiveMediaSrc %d prevActiveSubSrcId %d", prevAudioSrc, prevActiveMediaSrc, prevActiveSubSrcId));

   updateCurrentActiveMediaSource(sourceId, subSourceId);
   if (isMediaSource(sourceId))
   {
      if ((prevActiveMediaSrc != _currentActiveMediaSource) || (prevActiveSubSrcId != _currentActiveMediaSUBSourceID))
      {
         setActiveMediaDevice(_currentActiveMediaSUBSourceID);
         clearAndRequestNewScreenData(sourceId);
         int32 btSourceAvailibility = (isBTConnected == true) ? 1 : 0;
         POST_MSG((COURIER_MESSAGE_NEW(ActiveSourceUpdateMsg)(1, _currentActiveMediaSource, btSourceAvailibility)));
         transitToMainScreen(_currentActiveMediaSource, btSourceAvailibility);
         POST_MSG((COURIER_MESSAGE_NEW(InitializeMediaParametersUpdMsg)(1)));
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
         reqMediaContextOnUtilityActive(subSourceId);
#endif
      }

      MediaDatabinding::getInstance().updateIsMediaActive(true);			//This data binding update is used for dataguard condition in media SM
      MediaDatabinding::getInstance().updateCurrentMediaSource(_currentActiveMediaSource);
      MediaDatabinding::getInstance().updateActiveDeviceTag(_currentActiveMediaSUBSourceID);    //This update function is used only for TML
      deactivateLoadingPopup();
   }
   else
   {
      MediaDatabinding::getInstance().updateIsMediaActive(false);			//This data binding update is used for dataguard condition in media SM
   }
   _currentAudioSource = sourceId;
   _hmiDataHandler->getCurrentAudioSourceUpdate(_currentAudioSource);
}


/**
 * isBTDeviceconnected - To check if BT device connected,
 * @param[in] none
 * @return bool
 */
bool MediaSourceHandling::isBTDeviceconnected()
{
   bool result = false;
   bool isBTConnected = MediaProxyUtility::getInstance().isAnyBTDeviceConnected();

   if (!isBTConnected)
   {
      bool isBTConnectedOverUSB = MediaProxyUtility::getInstance().isAnyBTDeviceDisConnectedOverUSB();
      if (isBTConnectedOverUSB)
      {
         ETG_TRACE_USR4(("onActiveSourceUpdate isBTConnectedOverUSB %d", isBTConnectedOverUSB));
         bool isMetadataVisible = false;
         MediaDatabinding::getInstance().updateBTDisconnectionStatusOverUSB(isBTConnectedOverUSB,
               isMetadataVisible);
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
         MediaDatabinding::getInstance().updateTrackNoInfoInBT(isMetadataVisible);
#endif
         result = true;
      }
      else
      {
         ETG_TRACE_USR4(("onActiveSourceUpdate isBTConnectedOverUSB %d ", isBTConnectedOverUSB));
         bool isMetadataVisible = true;
         _iBTSettings.updateTrackInfoInBTMain();
         MediaDatabinding::getInstance().updateBTDisconnectionStatusOverUSB(isBTConnectedOverUSB,
               isMetadataVisible);
      }
   }
   else
   {
      result = true;
   }
   return result;
}


void MediaSourceHandling::setActiveMediaDevice(uint32 deviceTag)
{
   if (_mediaPlayerProxy->hasActiveMediaDevice() && deviceTag > 0)
   {
      bool isVolumeAvailable = MediaDatabinding::getInstance().getVolumeAvailablity();
      if (_mediaPlayerProxy->getActiveMediaDevice().getU8DeviceTag() != deviceTag && !isVolumeAvailable)
      {
         _iDeviceConnection.setActiveMediaDevice(deviceTag);
      }
   }
}


/**
 * clearScreenData - Private Helper Function to clear screen data
 * @param[in] currentSource
 * @return void
 */
void MediaSourceHandling::clearAndRequestNewScreenData(int32 currentSource)
{
   int32 previousSource = _currentAudioSource;
   if (previousSource != SRC_MEDIA_AUX)
   {
      MediaDatabinding::getInstance().clearPlayModeStatus();
      MediaDatabinding::getInstance().clearMediaMetaDataInfo();
      MediaDatabinding::getInstance().updateAlbumArtDefault(true);
      MediaDatabinding::getInstance().clearFolderorCurrentPlayingListBtnText(); // to avoid showing the previous device list btn text
   }

   if (currentSource != SRC_MEDIA_AUX)
   {
      _iPlayScreen.setPlayModeUpdatePending(true);
      _iPlayScreen.requestNowPlayingGet();
   }
}


/**
 * isMediaSource - Private Helper Function return true if current source is media source
 * @param[in] source
 * @return void
 */
bool MediaSourceHandling::isMediaSource(int32 source)
{
   if ((SRC_MEDIA_AUX <= source) && (source < SRC_SPI_ENTERTAIN))
   {
      return true;
   }
   return false;
}


void MediaSourceHandling::updateCurrentActiveMediaSource(int32 sourceId, int32 subSourceId)
{
   if ((sourceId >= SRC_MEDIA_AUX) && (sourceId <= SRC_SPI_ENTERTAIN))
   {
      _currentActiveMediaSource = getMediaSourceType(sourceId, subSourceId);
      updateSubSorceId(sourceId, subSourceId);
   }
}


/**
 * isTemporarySource - Private Helper Function return true if current source is temp source due to that current source is paused
 * @param[in] source
 * @return void
 */
bool MediaSourceHandling::isTemporarySource(int32 source)
{
   ETG_TRACE_USR4(("isTemporarySource :%d", source));
   bool isTempSource = false;
   switch (source)
   {
      case SRC_TUNER_TA_FM:
      case SRC_TUNER_TA_DAB:
      case SRC_PHONE:
      case SRC_PHONE_OUTBAND_RING:
      case SRC_PHONE_INBAND_RING:
      {
         isTempSource = true;
         break;
      }
      default:
      {
         break;
      }
   }

   return isTempSource;
}


void MediaSourceHandling::updateSubSorceId(int32 sourceId, int32 subSourceId)
{
   if (sourceId == SRC_PHONE_BTAUDIO && MediaProxyUtility::getInstance().isAnyBTDeviceConnected())
   {
      _currentActiveMediaSUBSourceID = MediaProxyUtility::getInstance().GetConnectedDeviceTag(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH);
   }
   else
   {
      _currentActiveMediaSUBSourceID = subSourceId;
   }
}


void MediaSourceHandling::updateWaitingContextView()
{
   switch (_SrcChgContextid)
   {
      case MEDIA_MASTER_SRC_MEDIA_PLAYER:
         _ContextSwitchWaitingView = MediaProxyUtility::getInstance().getViewName(_currentActiveMediaSUBSourceID);
         break;

      case MEDIA_MASTER_SRC_PHONE_BTAUDIO:
         _ContextSwitchWaitingView = (MediaProxyUtility::getInstance().isAnyBTDeviceConnected()) ? "Media#Scenes#MEDIA__AUX__BTAUDIO_MAIN" : "Media#Scenes#MEDIA_AUX__BTAUDIO_MAIN_DEFAULT";
         break;

      default:
         break;
   }
}


/**
 * GetMediaSourceType - to Deactivate media loading popup as coonected source started playing
 * @param[in] None
 * @return void
 */
void MediaSourceHandling::deactivateLoadingPopup()
{
   switch (_currentActiveMediaSource)
   {
      case SOURCE_USB:
      case SOURCE_IPOD:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND)));
         break;
      }
      case SOURCE_CDMP3:
      case SOURCE_CD:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING)));
         break;
      }
      default:
      {
         ETG_TRACE_USR4(("unknown source"));
         break;
      }
   }
}


/**
 * GetMediaSourceType - to decide the serivce type
 * @param[in] proxy, update
 * @return void
 */

int32 MediaSourceHandling::getMediaSourceType(int32 SrcID, int32 SubSrcID)
{
   ETG_TRACE_USR4(("MediaSourceHandling::i32GetHallSourceType SrcID:%d SubSrcID:%d ", SrcID, SubSrcID));

   int32 sourceType = SOURCE_NOT_DEF;

   switch (SrcID)
   {
      case SRC_MEDIA_PLAYER:
      {
         //If source is a Mediaplayer source, then obtain the subsource Device ID
         ETG_TRACE_USR4((" MediaPlayer Source"));

         if (SubSrcID > 0) // If the Device ID is valid only then determine the type of the Device, 0 is MyMedia and -1 is invalid device tag
         {
            sourceType = MediaProxyUtility::getInstance().getHallDeviceType(SubSrcID);
         }

         break;
      }

      case SRC_MEDIA_AUX:
      {
         // Set AUX source as available in toggle sequence list
         ETG_TRACE_USR4((" Aux source"));
         sourceType = SOURCE_AUX;
         break;
      }
      case SRC_PHONE_BTAUDIO:
      {
         ETG_TRACE_USR4((" BT source"));
         sourceType = SOURCE_BT;
         break;
      }
      case SRC_MEDIA_CDDA:
      {
         ETG_TRACE_USR4((" CDDA source"));
         sourceType = SOURCE_CD;
         break;
      }
      case SRC_SPI_ENTERTAIN:
      {
         ETG_TRACE_USR4((" SPI source"));
         sourceType = SOURCE_SPI;
         break;
      }

      default: //For all other sources other than Media, no handling done here
      {
         ETG_TRACE_USR4((" The current active source does not belong to Media , so no handling in Media"));
         break;
      }
   } //End of Switch case
   return sourceType;
}


/**
 * GetMediaCurrentActiveSource - to return current active media source
 * @param[in] null
 * @return void
 */
int32 MediaSourceHandling::getMediaCurrentActiveSource()
{
   return _currentActiveMediaSource;
}


int32 MediaSourceHandling::getCurrentAudioSource()
{
   return _currentAudioSource;
}


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
bool MediaSourceHandling::onCourierMessage(const ChangeVolumeReqMsg& msg)
{
   uint8 volumeLevel = msg.GetVolumeLevels();
   //Send the Value to Audio service for set
   SetMainSinkSoundPropertyRequestSoundPropertyStruct stSetMainSourceSoundProperty = SetMainSinkSoundPropertyRequestSoundPropertyStruct(AUX_TYPE, (volumeLevel + 1));
   //SinkId is used to identify the Target Id(Like Rear or Front).
   //Currently Front Target only available so SinkId is one
   _commandIFProxy->sendSetMainSinkSoundPropertyRequest(*this, SA_SINKID_SOURCE_ONE, stSetMainSourceSoundProperty);
   return true;
}


#endif

/**
 * SourceToggleReqMsg - This Msg is received from GUI on HK_AUX press.
 * @param[in] msg
 * @return bool
 */


bool MediaSourceHandling::onCourierMessage(const SourceToggleReqMsg& /*msg*/)
{
#ifdef	VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
   int8 nextMediaSource = _currentActiveMediaSource ;
   std::vector<SourceDetailInfo>::const_iterator itr;
   bool isToggleSourceActivation = false;
   bool isBtSouceAvailable = false;
   while (isToggleSourceActivation != true)
   {
      nextMediaSource = getNextPossibleSource(nextMediaSource);
      if ((SOURCE_SPI == nextMediaSource) && (true == MediaDatabinding::getInstance().getSpiDIPOSourceAvailability()))
      {
         ETG_TRACE_USR4((" clMediaSourceHandling:: Activate SPI_ENTERTAINMENT"));
         POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MUSIC, 0 , APPID_APPHMI_MEDIA)));
         isToggleSourceActivation = true;
         ETG_TRACE_USR4((" SPI_triggered, break loop"));
         break;
      }

      if (nextMediaSource == SOURCE_BT)
      {
         isBtSouceAvailable = isAnyBTSourceAvailable();
      }
      for (itr = _sourcelist.begin() ; itr != _sourcelist.end() && isToggleSourceActivation == false; ++itr)
      {
         if (isBtSouceAvailable && (itr->sourceID == SRC_PHONE_BTAUDIO))
         {
            itr++;
         }
         int32 localSource = getMediaSourceType(itr->sourceID, itr->subSourceID);

         ETG_TRACE_USR4((" clMediaSourceHandling::SourceToggleReqMsg:i8NextMediaSource:%d", nextMediaSource));
         ETG_TRACE_USR4((" clMediaSourceHandling::SourceToggleReqMsg:localSource:%d", localSource));
         if (localSource == nextMediaSource && itr->availability == 1)  //TODO: check Availability reason ex:No files found
         {
            requestSourceActivation(itr->sourceID, itr->subSourceID);
            isToggleSourceActivation = true;
         }
      }
   }
   return isToggleSourceActivation;


#endif


#if defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2)|| defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R)

   int8 nextMediaSource = _currentActiveMediaSource ;
   std::vector<SourceDetailInfo>::const_iterator itr;
   bool isToggleSourceActivation = false;
   bool isBtSouceAvailable = false;

   while (isToggleSourceActivation != true)
   {
      if (_currentActiveMediaSource == SOURCE_USB && _extendedUSBSupport == 1)
      {
         nextMediaSource = SOURCE_USB;
         _extendedUSBSupport++;
      }
      else
      {
         nextMediaSource = getNextPossibleSource(nextMediaSource);
      }
      if (SOURCE_SPI == nextMediaSource)
      {
         ETG_TRACE_USR4((" clMediaSourceHandling:: Activate SPI_ENTERTAINMENT"));
         if (true == MediaDatabinding::getInstance().getSpiDIPOSourceAvailability())
         {
            POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MUSIC, 0 , APPID_APPHMI_MEDIA)));
            isToggleSourceActivation = true;
            ETG_TRACE_USR4((" SPI_triggered, break loop after activating CarPlay"));
            break;
         }
         else if (true == MediaDatabinding::getInstance().getSpiAAPSourceAvailability())
         {
            POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(MEDIA_SPI_CONTEXT_ANDROID_AUTO_MUSIC_VIDEO, 0 , APPID_APPHMI_MEDIA)));
            isToggleSourceActivation = true;
            ETG_TRACE_USR4((" SPI_triggered, break loop after Activating AAP"));
            break;
         }
      }

      if (nextMediaSource == SOURCE_BT)
      {
         isBtSouceAvailable = isAnyBTSourceAvailable();
      }
      for (itr = _sourcelist.begin() ; itr != _sourcelist.end() && isToggleSourceActivation == false; ++itr)
      {
         if (isBtSouceAvailable && (itr->sourceID == SRC_PHONE_BTAUDIO))
         {
            itr++;
         }
         int32 localSource = getMediaSourceType(itr->sourceID, itr->subSourceID);
         int32 localsubSource = itr->subSourceID;
         ETG_TRACE_USR4((" clMediaSourceHandling::SourceToggleReqMsg:i8NextMediaSource:%d", nextMediaSource));
         ETG_TRACE_USR4((" clMediaSourceHandling::SourceToggleReqMsg:localSource:%d", localSource));
         if (SOURCE_AUX != _currentActiveMediaSource || nextMediaSource != SOURCE_AUX)
         {
            bool bIsSubSourceIdChange = true;
            if (itr->sourceID == SRC_MEDIA_PLAYER)
            {
               bIsSubSourceIdChange = (localsubSource != _currentActiveMediaSUBSourceID) ? true : false;
            }
            if (localSource == nextMediaSource && itr->availability == 1 && bIsSubSourceIdChange)  //TODO: check Availability reason ex:No files found
            {
               requestSourceActivation(itr->sourceID, itr->subSourceID);
               isToggleSourceActivation = true;
               if (localSource != SOURCE_USB)
               {
                  _extendedUSBSupport = 0;
               }
               else
               {
                  _extendedUSBSupport++;
               }
            }
         }
         else
         {
            isToggleSourceActivation = true;
         }
      }
   }
   return isToggleSourceActivation;
#endif
   return true;
}


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
/**
 * onHK_Eject_PressMsg - This Msg is received from GUI on HK_EJECT press.
 * @param[in] msg
 * @return bool
 */
bool MediaSourceHandling::onCourierMessage(const onHK_Eject_PressMsg& /*oMsg*/)
{
   //call the eject fi
   ETG_TRACE_USR4(("onHK_Eject_PressMsg::ejectCD"));
   if (_mediaPlayerProxy)
   {
      _mediaPlayerProxy->sendEjectOpticalDiscStart(*this, ::mplay_shared_fi_types::T_e8_EjectParmType__e8CMD_EJECT);
   }
   return true;
}


/**
 * onMediaAudioGadget_PressMsg - This Msg is received from GUI on Home screen Gadget.
 * @param[in] msg
 * @return bool
 */
bool MediaSourceHandling::onCourierMessage(const onMediaAudioGadget_PressMsg& /*oMsg*/)
{
   ETG_TRACE_USR4(("onMediaAudioGadget_PressMsg Acive source %d - %d ", _currentAudioSource, _currentActiveMediaSUBSourceID));
   //requestSourceActivation(MediaId , _currentActiveMediaSUBSourceID);
   switch (_currentAudioSource)
   {
      case SRC_MEDIA_CDDA:
         POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(MEDIA_MASTER_SRC_MEDIA_CDDA, 0, APPID_APPHMI_MEDIA)));
         break;

      case SRC_MEDIA_PLAYER:
         POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(MEDIA_MASTER_SRC_MEDIA_PLAYER, 0, APPID_APPHMI_MEDIA)));
         break;

      case SRC_PHONE_BTAUDIO:
         POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(MEDIA_MASTER_SRC_PHONE_BTAUDIO, 0, APPID_APPHMI_MEDIA)));
         break;

      case SRC_MEDIA_AUX:
         POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(MEDIA_MASTER_SRC_MEDIA_AUX, 0, APPID_APPHMI_MEDIA)));
         break;
      case SRC_SPI_ENTERTAIN:
      {
         if (true == MediaDatabinding::getInstance().getSpiDIPOSourceAvailability())
         {
            POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MUSIC, 0 , APPID_APPHMI_MEDIA)));
         }
         else if (true == MediaDatabinding::getInstance().getSpiAAPSourceAvailability())
         {
            POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(MEDIA_SPI_CONTEXT_ANDROID_AUTO_MUSIC_VIDEO, 0 , APPID_APPHMI_MEDIA)));
         }
         break;
      }
      default:
         break;
   }
   return true;
}


#endif

/*
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
bool MediaSourceHandling::onCourierMessage(const SourceToggleReqMsg& msg)
{
   int8 nextMediaSource = _currentActiveMediaSource ;
   std::vector<SourceDetailInfo>::const_iterator itr;
   bool isToggleSourceActivation = false;
   bool isBtSouceAvailable = false;
   while (isToggleSourceActivation != true)
   {
      nextMediaSource = getNextPossibleSource(nextMediaSource);
      if ((SOURCE_SPI == nextMediaSource) && (true == MediaDatabinding::getInstance().getSpiDIPOSourceAvailability()))
      {
         ETG_TRACE_USR4((" clMediaSourceHandling:: Activate SPI_ENTERTAINMENT"));
         POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MUSIC, 0 , APPID_APPHMI_MEDIA)));
         isToggleSourceActivation = true;
         ETG_TRACE_USR4((" SPI_triggered, break loop"));
         break;
      }

      if (nextMediaSource == SOURCE_BT)
      {
         isBtSouceAvailable = isAnyBTSourceAvailable();
      }
      for (itr = _sourcelist.begin() ; itr != _sourcelist.end() && isToggleSourceActivation == false; ++itr)
      {
         if (isBtSouceAvailable && (itr->sourceID == SRC_PHONE_BTAUDIO))
         {
            itr++;
         }
         int32 localSource = getMediaSourceType(itr->sourceID, itr->subSourceID);

         ETG_TRACE_USR4((" clMediaSourceHandling::SourceToggleReqMsg:i8NextMediaSource:%d", nextMediaSource));
         ETG_TRACE_USR4((" clMediaSourceHandling::SourceToggleReqMsg:localSource:%d", localSource));
         if (localSource == nextMediaSource && itr->availability == 1)  //TODO: check Availability reason ex:No files found
         {
            requestSourceActivation(itr->sourceID, itr->subSourceID);
            isToggleSourceActivation = true;
         }
      }
   }
   return isToggleSourceActivation;
}


#endif
*/


bool MediaSourceHandling::isAnyBTSourceAvailable()
{
   std::vector<SourceDetailInfo>::const_iterator itr;
   bool isBtSouceAvailable = false;
   for (itr = _sourcelist.begin() ; itr != _sourcelist.end(); ++itr)
   {
      if (itr->sourceID == SRC_MEDIA_PLAYER)
      {
         int32 mediaHallSourceType = getMediaSourceType(itr->sourceID, itr->subSourceID);
         if (mediaHallSourceType == SOURCE_BT)
         {
            isBtSouceAvailable = true;
            break;
         }
      }
   }
   return isBtSouceAvailable;
}


/**
 * getNextPossibleSource - This is an helper fucntion to decide next possilbe source as per the requirement.
 * @param[in] SourceId
 * @return SourceId
 */
int32 MediaSourceHandling::getNextPossibleSource(int32 SourceId)
{
   int32 nextSourceID = SOURCE_NOT_DEF;
   if (SourceId == SOURCE_AUX)
   {
      nextSourceID = SOURCE_USB;
   }
   else if (SourceId == SOURCE_USB)
   {
      nextSourceID = SOURCE_IPOD;
   }
   else if (SourceId == SOURCE_IPOD)
   {
      nextSourceID = SOURCE_BT;
   }
   else if (SourceId == SOURCE_BT)
   {
      nextSourceID = SOURCE_CD;
   }
   else if (SourceId == SOURCE_CD)
   {
      nextSourceID = SOURCE_CDMP3;
   }
   else if (SourceId == SOURCE_CDMP3)
   {
      nextSourceID = SOURCE_SPI;
   }
   else if (SourceId == SOURCE_SPI)
   {
      nextSourceID = SOURCE_AUX;
   }
   else
   {
      nextSourceID = SOURCE_AUX;
   }
   return nextSourceID;
}


/**
 * ExitBrowseOnHK_AUXPressMsg - Message from StateMachine when HK_AUX is pressed in browse screen or when there is a need to go to media main screen
 *
 * @param[in] ExitBrowseOnHK_AUXPressMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaSourceHandling::onCourierMessage(const ExitBrowseOnHK_AUXPressMsg& /*oMsg*/)
{
   bool isBTConnected = isBTDeviceconnected();
   POST_MSG((COURIER_MESSAGE_NEW(ActiveSourceUpdateMsg)(1, _currentActiveMediaSource, isBTConnected)));
   transitToMainScreen(_currentActiveMediaSource, isBTConnected);
   ETG_TRACE_USR4(("clMediaSourceHandling::_currentActiveMediaSource posted with value: %d", _currentActiveMediaSource));

   return true;
}


/**
 * ContextSwitchInReqMsg - GetContextId from home screen and source switch
 *
 * @param[in] ContextSwitchInReqMsg message
 * @parm[out] none
 * @return true - message consumed
 */

bool MediaSourceHandling::onCourierMessage(const ContextSwitchInReqMsg& msg)
{
   bool isConsumed = false;

   ETG_TRACE_USR4(("clMediaSourceHandling::ContextSwitchInReqMsg: %d", msg.GetContextId()));
   if (msg.GetContextId() == MEDIA_MASTER_SRC_MEDIA_AUX || msg.GetContextId() == MEDIA_MASTER_SRC_MEDIA_CDDA)
   {
      POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(msg.GetSwitchId(), CONTEXT_TRANSITION_DONE)));
      POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(msg.GetSwitchId(), CONTEXT_TRANSITION_COMPLETE)));
      if (_isSpiAppStateActive)
      {
         POST_MSG((COURIER_MESSAGE_NEW(RequestSpiInactiveOnMediaSourceMsg)()));
      }
      isConsumed = true;
   }
   else if (msg.GetContextId() == MEDIA_MASTER_SRC_MEDIA_PLAYER || msg.GetContextId() == MEDIA_MASTER_SRC_PHONE_BTAUDIO)
   {
      _prevContextid = _SrcChgContextid;
      _prevSwitchid = _SrcChgSwitchid;
      _SrcChgContextid = msg.GetContextId();
      _SrcChgSwitchid = msg.GetSwitchId();
      sendViewChange();
      if (_isSpiAppStateActive)
      {
         POST_MSG((COURIER_MESSAGE_NEW(RequestSpiInactiveOnMediaSourceMsg)()));
      }
      isConsumed = true;
   }

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
   {
      int32 lDevID = -1 ;
      int32 lSubSrcID = -1;
      if (MEDIA_CONTEXT_HOMESCREEN_AUX == msg.GetContextId())
      {
         lSubSrcID = getSubSourceID(SRC_MEDIA_AUX);
         if (SUB_SOURCEID_NOTFOUND != lSubSrcID)
         {
            ETG_TRACE_USR4(("ContextSwitchInReqMsg:SRC_MEDIA_AUX"));
            requestSourceActivation(SRC_MEDIA_AUX, lSubSrcID);
            POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(msg.GetSwitchId(), CONTEXT_TRANSITION_DONE)));
            POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(msg.GetSwitchId(), CONTEXT_TRANSITION_COMPLETE)));
            isConsumed = true;
         }
      }
      else if (MEDIA_CONTEXT_HOMESCREEN_BT_AUDIO == msg.GetContextId())
      {
         //for BT sub source ID is -1 (BT paired / default BT screen)
         requestSourceActivation(SRC_PHONE_BTAUDIO, BT_SUBSOURCE_ID);
         POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(msg.GetSwitchId(), CONTEXT_TRANSITION_DONE)));
         POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(msg.GetSwitchId(), CONTEXT_TRANSITION_COMPLETE)));
         isConsumed = true;
      }
      else if (true == _DeviceHomeScreen.getHomeScreenSourceInfo(msg.GetContextId(), lDevID, lSubSrcID))
      {
         requestSourceActivation(getSourceId(lDevID), lSubSrcID);
         isConsumed = true;
         POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(msg.GetSwitchId(), CONTEXT_TRANSITION_DONE)));
         POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(msg.GetSwitchId(), CONTEXT_TRANSITION_COMPLETE)));
      }
      else
      {
         ETG_TRACE_USR1(("Could not able to find ContextId %d", msg.GetContextId()));
      }
   }
#endif

   return isConsumed;
}


void MediaSourceHandling::sendViewChange()
{
   ETG_TRACE_USR1(("sendViewChange %d :%d %s", _SrcChgContextid, _currentActiveMediaSUBSourceID, _viewname.c_str()));
   switch (_SrcChgContextid)
   {
      case MEDIA_MASTER_SRC_MEDIA_PLAYER:
      {
         if (isMediaPlayerSourceActive() && (_currentAudioSource == SRC_MEDIA_PLAYER))
         {
            if (_viewname == MediaProxyUtility::getInstance().getViewName(_currentActiveMediaSUBSourceID))
            {
               POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(_SrcChgSwitchid, CONTEXT_TRANSITION_DONE)));
               if (_prevContextid != _SrcChgContextid)
               {
                  POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(_prevSwitchid, CONTEXT_TRANSITION_COMPLETE)));
               }
               _ContextSwitchWaitingView = "";
            }
            else
            {
               _ContextSwitchWaitingView = MediaProxyUtility::getInstance().getViewName(_currentActiveMediaSUBSourceID);
            }
         }
         else
         {
            _ContextSwitchWaitingView = "none";
         }
         break;
      }
      case MEDIA_MASTER_SRC_PHONE_BTAUDIO:
      {
         std::string nextView = (MediaProxyUtility::getInstance().isAnyBTDeviceConnected()) ? "Media#Scenes#MEDIA__AUX__BTAUDIO_MAIN" : "Media#Scenes#MEDIA_AUX__BTAUDIO_MAIN_DEFAULT";
         _ContextSwitchWaitingView = nextView;
         if (SRC_PHONE_BTAUDIO == MediaDatabinding::getInstance().GetCurrentAudioSource())
         {
            if (_viewname == nextView)
            {
               POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(_SrcChgSwitchid, CONTEXT_TRANSITION_DONE)));
               if (_prevContextid != _SrcChgContextid)
               {
                  POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(_prevSwitchid, CONTEXT_TRANSITION_COMPLETE)));
               }
               _ContextSwitchWaitingView = "";
            }
         }
         break;
      }
      default:
         break;
   }
}


bool MediaSourceHandling::isMediaPlayerSourceActive()
{
   switch (_currentActiveMediaSource)
   {
      case SOURCE_IPOD:
      case SOURCE_USB:
      case SOURCE_CDMP3:
      {
         return true;
      }
      default:
      {
         return false;
      }
   }
}


bool MediaSourceHandling::onCourierMessage(const ActiveRenderedView& msg)
{
   ETG_TRACE_USR1(("ActiveRenderedView %d - %s", msg.GetSurfaceId(), msg.GetViewName().GetCString()));
   if (msg.GetSurfaceId() == 104)
   {
      _viewname = msg.GetViewName().GetCString();
      if (_ContextSwitchWaitingView == _viewname)
      {
         sendViewChange();
      }
   }
   return false;
}


/**
 * getSourceId - Get Source ID for master
 *
 * @param[in] lDevID device id message
 * @parm[out] none
 * @return Source ID for master
 */
int32 MediaSourceHandling::getSourceId(int32 lDevID)
{
   int32 source = -1;

   switch (lDevID)
   {
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH:
      {
         source = SRC_PHONE_BTAUDIO;
         break;
      }
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPOD:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPHONE:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_USB:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDROM:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_MTP:
      {
         source = SRC_MEDIA_PLAYER;
         break;
      }
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA:
      {
         source = SRC_MEDIA_CDDA;
         break;
      }
      default:
      {
         break;
      }
   }
   return source;
}


/**
 * getSubSourceID - Get Sub source ID for master
 *
 * @param[in] lDevID device id message
 * @parm[out] none
 * @return Dub Source ID for master
 */
int32  MediaSourceHandling::getSubSourceID(int32 SourceID)
{
   std::vector<SourceDetailInfo>::const_iterator itr;
   for (itr = _sourcelist.begin() ; itr != _sourcelist.end() ; ++itr)
   {
      if (itr->sourceID == SourceID)
      {
         return itr->subSourceID;
      }
   }
   return SUB_SOURCEID_NOTFOUND;
}


/**
 * onEjectOpticalDiscError - Error for Eject .
 * @param[in] msg
 * @return bool
 */

void MediaSourceHandling::onEjectOpticalDiscError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::EjectOpticalDiscError >& /*error*/)
{
   ETG_TRACE_USR4(("onEjectOpticalDiscError::ejectCD "));
}


/**
 * onEjectOpticalDiscError - Status for Eject .
 * @param[in] msg
 * @return bool
 */

void MediaSourceHandling::onEjectOpticalDiscResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::EjectOpticalDiscResult >& result)
{
   ETG_TRACE_USR4(("onEjectOpticalDiscResult::ejectCD %d", result->getServiceStatus()));
}


void MediaSourceHandling::deviceConnectionStatus()
{
   if (_currentAudioSource == SRC_MEDIA_PLAYER)
   {
      _audioSourceChangeProxy->sendActiveSourceGet(*this);
   }
   _iDeviceConnection.deregisterObserver(this);
}


void MediaSourceHandling::onSetVolumeError(const ::boost::shared_ptr< AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy >& /*proxy*/,
      const ::boost::shared_ptr< AUDIOMANAGER_COMMANDINTERFACE::SetVolumeError >& /*error*/)
{
   ETG_TRACE_USR4(("onSetVolumeError"));
}


void MediaSourceHandling::onSetVolumeResponse(const ::boost::shared_ptr< AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy >&/*proxy*/,
      const ::boost::shared_ptr< AUDIOMANAGER_COMMANDINTERFACE::SetVolumeResponse >& /*response*/)
{
   ETG_TRACE_USR4(("onSetVolumeResponse"));
}


bool MediaSourceHandling::onCourierMessage(const PlayFromVolumeZeroStateMsg& /*oMsg*/)
{
   _commandIFProxy->sendSetVolumeRequest(*this, sinkid, ABSOLUTE_VALUE_MODE, DEFAULT_VOLUME_ONE);
   return true;
}


void MediaSourceHandling::onVolumeError(const ::boost::shared_ptr<MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy >& /*proxy*/, const ::boost::shared_ptr<MASTERAUDIOSERVICE_INTERFACE::SoundProperties:: VolumeError >& /*error*/)
{
   ETG_TRACE_USR4(("onVolumeError"));
}


void MediaSourceHandling::onVolumeUpdate(const ::boost::shared_ptr<MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy >& /*proxy*/, const ::boost::shared_ptr<MASTERAUDIOSERVICE_INTERFACE::SoundProperties:: VolumeUpdate >& update)
{
   ETG_TRACE_USR4(("onVolumeUpdate =%d", update->getVolume()));
#ifdef VOLUME_ZERO_SUPPORT
   MediaDatabinding::getInstance().updateVolumeAvailability(update->getVolume());			//This is used to update the volume availablity in screen and for playback operations
#endif
}


/*
 * APPHMI_MEDIA_CDEject message - Ttfis Command to Eject CDDA/CDMP3
 *
 * Description : Ttfis Command to Eject CDDA/CDMP3
 * @param[in] none
 * @return void
 */

ETG_I_CMD_DEFINE((tracecmd_CDEject, "CDEject"))
void MediaSourceHandling::tracecmd_CDEject()
{
   if (_mediaSourceHandling)
   {
      _mediaSourceHandling->ttfisCmdCDEject();
   }
}


void MediaSourceHandling::ttfisCmdCDEject()
{
   //call the eject fi
   ETG_TRACE_USR4(("ttfisCmdCDEject::ejectCD"));
   if (_mediaPlayerProxy)
   {
      _mediaPlayerProxy->sendEjectOpticalDiscStart(*this, ::mplay_shared_fi_types::T_e8_EjectParmType__e8CMD_EJECT);
   }
}


bool MediaSourceHandling::onCourierMessage(const onMediaSpiStateActiveUpdMsg& msg)
{
   ETG_TRACE_USR4(("MediaSourceHandling::tonMediaSpiStateActiveUpdMsg"));
   bool isBTConnected = isBTDeviceconnected();
   bool isConsumed = false;
   _isSpiAppStateActive = msg.GetIsSpiStateActive();
   _iDeviceConnection.setSpiActiveStatus(_isSpiAppStateActive);
   if (!_isSpiAppStateActive)
   {
      POST_MSG((COURIER_MESSAGE_NEW(ScreenHeaderReq)(EMPTY_HEADER_TEXT)));
      transitToMainScreen(_currentActiveMediaSource, isBTConnected);
      isConsumed = true;
   }
   return isConsumed;
}


void MediaSourceHandling::transitToMainScreen(uint8, bool)
{
   bool isBTConnected = isBTDeviceconnected();
   if (!_isSpiAppStateActive)
   {
      POST_MSG((COURIER_MESSAGE_NEW(ActiveSourceMainScreenUpdMsg)(1, _currentActiveMediaSource, isBTConnected)));
   }
}


void MediaSourceHandling::reqMediaContextOnUtilityActive(int32 subSourceId)
{
   if ((subSourceId == _iDeviceConnection.getNewlyInsertedDeviceTag()) && (getAvailibilityReson(subSourceId) == 1) &&
         (_hmiDataHandler->getCurrentVisibleAppMode() == APP_MODE_UTILITY))
   {
      POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(MEDIA_MASTER_SRC_MEDIA_PLAYER, 0 , APPID_APPHMI_MEDIA)));
      _iDeviceConnection.resetNewlyInsertedDeviceTag();
   }
}


int32 MediaSourceHandling::getAvailibilityReson(int32 subSourceId)
{
   int32 availabilityReason = 0;
   std::vector<SourceDetailInfo>::const_iterator itr;
   for (itr = _sourcelist.begin() ; itr != _sourcelist.end() ; ++itr)
   {
      if (itr->sourceID == SRC_MEDIA_PLAYER && itr->subSourceID == subSourceId && itr->availability == 1)
      {
         availabilityReason = itr->availabilityReason;
      }
   }
   return availabilityReason;
}


void MediaSourceHandling::onActiveSourceListError(const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/, const ::boost::shared_ptr< ActiveSourceListError >& /*error*/)
{
   ETG_TRACE_USR4(("MediaSourceHandling::onActiveSourceListError received"));
}


void MediaSourceHandling::onActiveSourceListSignal(const ::boost::shared_ptr< AudioSourceChangeProxy >& /*proxy*/, const ::boost::shared_ptr< ActiveSourceListSignal >& signal)
{
   std::vector< sourceData > osrcData = signal->getActiveSources();
   ETG_TRACE_USR4(("MediaSourceHandling::onActiveSourceListSignal audio source list size- %d", osrcData.size()));
   std::vector< sourceData >::iterator itr = osrcData.begin();
   bool isAnyMediaSourceInStack = false;

   for (; itr != osrcData.end(); itr++)
   {
      ETG_TRACE_USR4(("MediaSourceHandling::onActiveSourceListSignal %d", itr->getSrcId()))
      if (isMediaSource(itr->getSrcId()))
      {
         isAnyMediaSourceInStack = true;
         break;
      }
   }
   if ((!isAnyMediaSourceInStack) && (!osrcData.empty()))							// the empty use case is also checked because the source list will be empty during intermittent state
   {
      ETG_TRACE_USR4(("Reset media source id as no source available in audio stack"));
      _currentActiveMediaSUBSourceID = 0;
   }
}


/**
 * onMuteStateError - This status is received from HMI_Master if there are any errors in muteState.
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */

void MediaSourceHandling::onMuteStateError(const ::boost::shared_ptr< SoundPropertiesProxy >& /*proxy*/, const ::boost::shared_ptr< MuteStateError >& /*error*/)
{
   ETG_TRACE_USR4((" MediaSourceHandling::onMuteStateError"));
}


/**
 * onMuteStateUpdate - This status is received from HMI_Master if there is any change in property mutestate.
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */

void MediaSourceHandling::onMuteStateUpdate(const ::boost::shared_ptr< SoundPropertiesProxy >& /*proxy*/, const ::boost::shared_ptr< MuteStateUpdate >& update)
{
   ETG_TRACE_USR4((" MediaSourceHandling::onMuteStateUpdate"));
   if (update->hasMuteState())
   {
      ETG_TRACE_USR4((" Master Mute State received %d", update->getMuteState()));
      MediaDatabinding::getInstance().updateMuteStatus(update->getMuteState());
   }
}


} // namespace Core
} // namespace App
