///////////////////////////////////////////////////////////
//  AppMedia_ContextProvider.h
//  Implementation of the Class AppMedia_ContextProvider
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(AppMedia_ContextProvider_h)
#define AppMedia_ContextProvider_h

/**
 * interface class for context proxies to provide contexts to requestors
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 5:44:42 PM
 */

#include "hmi_trace_if.h"
#include "clContextProvider.h"
#include "ProjectBaseMsgs.h"
#include "ProjectBaseTypes.h"
#include "Project_Trace.h"
#include "AppHmi_MediaMessages.h"
#include "asf/core/Proxy.h"

#define SPI_CONTEXT_ONRVC_FIX_NCG3D_7788
#ifndef SPI_CONTEXT_ONRVC_FIX_NCG3D_7788
#define SPI_CONTEXT_ONRVC_FIX_NCG3D_7788
#endif

static const clContext aSupportedContexts[] =
{
#define SUPPORTED_CONTEXTS(context, contextType, priority, surfaceId,  viewName, sourceId)                  { context, contextType, priority, surfaceId,  viewName, sourceId },
#include "contexts.dat"
#undef SUPPORTED_CONTEXTS
};


using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;


class AppMedia_ContextProvider : public clContextProvider
{
   public:
      void vSendContextTable()
      {
         ::std::vector<clContext> availableContexts;
         const int mapSize = sizeof aSupportedContexts / sizeof(clContext);
         for (int u32Index = 0; u32Index < mapSize; u32Index++)
         {
            availableContexts.push_back(aSupportedContexts[u32Index]);
         }
         storeAvailableContexts(availableContexts);
#ifdef SPI_CONTEXT_ONRVC_FIX_NCG3D_7788
         activeContextId = 0;
#endif
      }

      AppMedia_ContextProvider();
      virtual ~AppMedia_ContextProvider();

      virtual bool bOnNewContext(uint32 sourceContextId, uint32 targetContextId);
      virtual void vOnBackRequestResponse(ContextState /*enState*/);
      virtual void vClearContexts();
      virtual void vOnNewActiveContext(uint32 contextId, ContextState enState);
#ifdef SPI_CONTEXT_ONRVC_FIX_NCG3D_7788
      void vSetSpiRvcTemporaryContext(bool);
      bool bGetSetSpiRvcTemporaryContext();
#endif

      bool onCourierMessage(const ContextSwitchInResMsg& msg);
      bool onCourierMessage(const ContextSwitchInReqMsg& msg);
      bool onCourierMessage(const ActiveRenderedView&);

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      ON_COURIER_MESSAGE(ContextSwitchInResMsg)
      ON_COURIER_MESSAGE(ActiveRenderedView)
      ON_COURIER_MESSAGE(ContextSwitchInReqMsg)
      COURIER_MSG_MAP_END()
   private:
      uint32 activeContextId;
#ifdef SPI_CONTEXT_ONRVC_FIX_NCG3D_7788
      bool _mbSpiRvcTempContextRequest;
#endif
};


#endif // !defined(EA_FAF598AF_B4F9_4bb1_91CE_ABBA6F93217B__INCLUDED_)
