/**
*  @file   ListUtilityDatabase.h
*  @author ECV - vma6cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef LIST_UTILITY_DATABASE_H
#define LIST_UTILITY_DATABASE_H

#include "Core/LanguageDefines.h"

class ListUtilityDatabase
{
   public:
      ListUtilityDatabase();
      ~ListUtilityDatabase();
      void vSetCurrentStartIndex(uint32 StartIndex);
      uint32 u32GetCurrentStartIndex() const;
      void vSetCurrentBufferSize(uint32 BufferSize);
      uint32 u32GetCurrentBufferSize() const;
      void vSetListHandle(uint32 listHanlde);
      uint32 u32GetListHandle() const;
      void vSetListSize(uint32 listHanlde);
      uint32 u32GetListSize() const;
      void vSetActiveItemIndex(uint32 u32Index);
      uint32 u32GetActiveItemIndex() const;
      uint32 u32GetTagbyIndex(uint32 Index);
      uint32 u32GetActiveItemTag();
      void vStoreIndexToTagData(std::vector<uint32> & v);
      void vSetFirstElement(std::vector<std::string>  firstelement);
      std::string getFirstElementText(uint32) const;
      Candera::String getListTextByIndex(uint32) const;

   private:
      uint32 m_u32CurrentStartIndex;
      uint32 m_u32CurrentBufferSize;
      uint32 m_u32ListHandle;
      uint32 m_u32ListSize;
      uint8 m_u8ActiveItemIndex;
      std::vector<std::string>  m_sFirstItem;
      std::vector<uint32> _listItemTag;
};


#endif // LIST_UTILITY_DATABASE_H
