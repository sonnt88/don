/*
 * SpiKeyHandling.cpp
 * Author: rhk5kor
 *  Created on: Jun 25, 2015
 *  @addtogroup AppHmi_media
 */

#include "hall_std_if.h"
#include "SpiKeyHandler.h"
#include "AppHmi_MasterBase/AudioInterface/AudioDefines.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS         TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::SpiKeyHandler::
#include "trcGenProj/Header/SpiKeyHandler.cpp.trc.h"
#endif


namespace App {
namespace Core {

using namespace ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange;


SpiKeyHandler* SpiKeyHandler::pSpiKeyHandler = NULL;

/**
 * Description     : Constructor of class SpiKeyHandler
 */

SpiKeyHandler::SpiKeyHandler(SpiConnectionHandling* _poSpiConnectionHandling , ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& spiMidwServiceProxy)
   :  _spiConnectionHandler(_poSpiConnectionHandling)
   , _spiMidwServiceProxy(spiMidwServiceProxy)
{
   ETG_TRACE_USR4(("SpiKeyHandler: Constructor"));
   StartupSync::getInstance().registerPropertyRegistrationIF(this);
   pSpiKeyHandler = this;
   _bSpeechAppStateActive = false;
   _bPhoneAppStateActive = false;
   _bSiriButtonUpRequired = false;
}


/**
 * Description     : Destructor of class SpiKeyHandler
 */

SpiKeyHandler::~SpiKeyHandler()
{
   ETG_TRACE_USR4(("SpiKeyHandler: Destructor"));
   _spiMidwServiceProxy.reset();
   _spiConnectionHandler = NULL;
   _bSpeechAppStateActive = false;
   _bPhoneAppStateActive = false;
   _bSiriButtonUpRequired = false;
}


SpiKeyHandler* SpiKeyHandler::getInstance()
{
   if (pSpiKeyHandler != NULL)
   {
      return pSpiKeyHandler;
   }
   else
   {
      return NULL;
   }
}


/*
 * This function performs the upreg of the required properties.
 * @param[in]  - proxy: Pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - stateChange: Contains the current Service State
 * @param[out] - None
 * @return     - Void
 */

void SpiKeyHandler::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _spiMidwServiceProxy)
   {
      ETG_TRACE_USR4(("SpiKey: registerProperties for HMI-Master Properties "));
   }
}


/*
 * This function performs the Relupreg of the required properties.
 * @param[in]  - proxy: Pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - stateChange: Contains the current Service State
 * @param[out] - None
 * @return     - Void
 */

void SpiKeyHandler::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _spiMidwServiceProxy)
   {
      ETG_TRACE_USR4(("SpiKey: DeregisterProperties for HMI-Master Properties "));
   }
}


void SpiKeyHandler::notifyDiPOAppStatusInfoStatus(const uint32 SpeechAppState, const uint32 PhoneAppState)
{
   ETG_TRACE_USR1(("SpiKey: onDiPOAppStatusInfoStatus UPdate"));

   if (PhoneAppState == ::midw_smartphoneint_fi_types::T_e8_PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE)
   {
      _bPhoneAppStateActive = true;
      POST_MSG((COURIER_MESSAGE_NEW(SpiPhoneAppStateUpdMsg)(true)));
   }
   else if (PhoneAppState == ::midw_smartphoneint_fi_types::T_e8_PhoneAppState__SPI_APP_STATE_PHONE_NOTACTIVE
            || PhoneAppState == ::midw_smartphoneint_fi_types:: T_e8_PhoneAppState__SPI_APP_STATE_PHONE_UNKNOWN)
   {
      _bPhoneAppStateActive = false;
      POST_MSG((COURIER_MESSAGE_NEW(SpiPhoneAppStateUpdMsg)(false)));
   }

   switch (SpeechAppState)
   {
      case ::midw_smartphoneint_fi_types::T_e8_SpeechAppState__SPI_APP_STATE_SPEECH_SPEAKING:
      case ::midw_smartphoneint_fi_types::T_e8_SpeechAppState__SPI_APP_STATE_SPEECH_RECOGNISING:
         _bSpeechAppStateActive = true;
         break;
      case ::midw_smartphoneint_fi_types::T_e8_SpeechAppState__SPI_APP_STATE_SPEECH_END:
      case ::midw_smartphoneint_fi_types::T_e8_SpeechAppState__SPI_APP_STATE_SPEECH_UNKNOWN:
         _bSpeechAppStateActive = false;
         break;

      default:
         _bSpeechAppStateActive = false;
         break;
   }
   vUpdateSpiSpeechRecognitionDgStatus(_bSpeechAppStateActive);
   vUpdateSpiPhoneAppDgStatus(_bPhoneAppStateActive);
}


/**
 * This message is sent triggered from SM on the press of hard key events.
 * @param[in] - oMsg
 * @return    -bool
 */

bool SpiKeyHandler::onCourierMessage(const OnHk_KeyEventReqMsg& oMsg)
{
   ETG_TRACE_USR4(("SpiKey:GUI->HALL onCourierMessage OnHk_KeyEventReqMsg received: KeyCode = %d KeyMode %d", oMsg.GetKeyCode(), oMsg.GetKeyMode()));
   uint32 u32KeyCode = oMsg.GetKeyCode();
   uint32 u32KeyMode = oMsg.GetKeyMode();
   uint32 u32MappedKeyMode = u32GetMappedKeyMode(u32KeyMode);

   //! is Long Press/Release
   bool bLongPressRelease = ((u32KeyMode == Enum_enLongPress) || (u32KeyMode == Enum_enLongPressRelease));
   ETG_TRACE_USR4(("SpiKey:GUI->HALL onCourierMessage OnHk_KeyEventUpdMsg bLongPressRelease %d", bLongPressRelease));

   switch (u32KeyCode)
   {
      case  Enum_HARDKEYCODE_HK_NEXT:      // HK_Next
      case  Enum_HARDKEYCODE_SWC_NEXT:     //SWC_Next
      {
         if (bLongPressRelease)
         {
            u32KeyCode = ::midw_smartphoneint_fi_types::T_e32_KeyCode__MULTIMEDIA_FORWARD;
         }
         else
         {
            u32KeyCode = ::midw_smartphoneint_fi_types::T_e32_KeyCode__MULTIMEDIA_NEXT;
         }
         //!Send KeyEvent to MW
         _spiConnectionHandler->vSendKeyEvent(u32MappedKeyMode, u32KeyCode);
      }
      break;

      case Enum_HARDKEYCODE_HK_PREVIOUS: // Previous
      case Enum_HARDKEYCODE_SWC_PREV:
      {
         if (bLongPressRelease)
         {
            u32KeyCode = ::midw_smartphoneint_fi_types::T_e32_KeyCode__MULTIMEDIA_REWIND;
         }
         else
         {
            u32KeyCode = ::midw_smartphoneint_fi_types::T_e32_KeyCode__MULTIMEDIA_PREVIOUS;
         }
         //!Send KeyEvent to MW
         _spiConnectionHandler->vSendKeyEvent(u32MappedKeyMode, u32KeyCode);
      }
      break;

      case Enum_HARDKEYCODE_HK_PHONE:
      case Enum_HARDKEYCODE_SWC_PHONE:
      {
         vHandlePhoneAppRequest(u32KeyCode , u32KeyMode);
      }
      break;

      case Enum_HARDKEYCODE_SWC_PTT:
      {
         if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_DIPO == _spiConnectionHandler->getActiveDeviceCategory())
         {
            vDIPOSpeechAppStateRequestHandler(u32KeyMode);
         }
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
         if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO == _spiConnectionHandler->getActiveDeviceCategory())
         {
            vAndroidAutoSpeechAppStateRequestHandler(u32KeyMode);
         }
#endif
         vHandlePhoneAppRequest(u32KeyCode , u32KeyMode);
      }
      break;

      default:
         break;
   }

   return true;
}


/*
 * This function parse the keyevent received from SM since press/long press is same for device.
 * @param[in]  - u32KeyMode: KeyEvent received from SM
 * @param[out] - u32MappedKeyMode
 * @return     - uint32
 */

uint32 SpiKeyHandler::u32GetMappedKeyMode(uint32 u32KeyMode)
{
   uint32 u32MappedKeyMode = UNDEFINE_KEYEVENT;

   switch (u32KeyMode)
   {
      case Enum_enPress:
      case Enum_enLongPress:
         u32MappedKeyMode = ::midw_smartphoneint_fi_types::T_e8_KeyMode__KEY_PRESS;
         break;
      case Enum_enRelease:
      case Enum_enLongPressRelease:
         u32MappedKeyMode = ::midw_smartphoneint_fi_types::T_e8_KeyMode__KEY_RELEASE;
         break;
      default:
         break;
   }
   return u32MappedKeyMode;
}


/**
 * This message is sent triggered from SM on the press/release of HK_Back.
 * @param[in] - oMsg
 * @return    -bool
 */

bool SpiKeyHandler::onCourierMessage(const onHK_BackUpdMsg& oMsg)
{
   ETG_TRACE_USR4(("SpiKey:GUI->HALL onCourierMessage onHK_BackReqMsg received"));
   uint32 u32KeyState = Enum_enPress;
   uint32 u32KeyCode = T_e32_KeyCode__DEV_BACKWARD;
   switch (oMsg.GetKeyState())
   {
      case Enum_enPress:
      case Enum_enLongPress:
         u32KeyState = ::midw_smartphoneint_fi_types::T_e8_KeyMode__KEY_PRESS;
         break;
      case Enum_enRelease:
      case Enum_enLongPressRelease:
         u32KeyState = ::midw_smartphoneint_fi_types::T_e8_KeyMode__KEY_RELEASE;
         break;
      default:
         break;
   }
   _spiConnectionHandler->vSendKeyEvent(u32KeyState , u32KeyCode);
   return true;
}


/**
 * This message is sent triggered from SM on the Rotation of Main Encoder.
 * @param[in] - oMsg
 * @return    -bool
 */

bool SpiKeyHandler::onCourierMessage(const onEncoderRotatonUpdMsg& oMsg)
{
   int i8EncCount = 0;
   ETG_TRACE_USR4(("SpiKey:GUI->HALL onCourierMessage onEncoderRotatonUpdMsg received"));
   i8EncCount = oMsg.GetEncCount();
   _spiConnectionHandler->vSendRotaryEncoderEvent(i8EncCount);
   return true;
}


/**
 * This message is sent triggered from SM on the press/release of HK_SELECT/Main encoder.
 * @param[in] - oMsg
 * @return    -bool
 */

bool SpiKeyHandler::onCourierMessage(const onEncoderPressedUpdMsg& oMsg)
{
   ETG_TRACE_USR4(("SpiKey:GUI->HALL onCourierMessage onEncoderPressedUpdMsg received"));
   uint32 u32EncPressedState = Enum_enPress;
   uint32 u32EncKeyCode = T_e32_KeyCode__DEV_MENU;
   switch (oMsg.GetEncPressedState())
   {
      case Enum_enPress:
      case Enum_enLongPress:
         u32EncPressedState = ::midw_smartphoneint_fi_types::T_e8_KeyMode__KEY_PRESS;
         break;
      case Enum_enRelease:
      case Enum_enLongPressRelease:
         u32EncPressedState = ::midw_smartphoneint_fi_types::T_e8_KeyMode__KEY_RELEASE;
         break;
      default:
         break;
   }
   _spiConnectionHandler->vSendKeyEvent(u32EncPressedState, u32EncKeyCode);
   return true;
}


/**
 * This message is sent triggered from SM for context switch from Master for SWC_SRC key for Sending Play command to AID.
 * @param[in] - oMsg
 * @return    -bool
 */

bool SpiKeyHandler::onCourierMessage(const onSendDipoPlayCommandUpdMsg& /*oMsg*/)
{
   ETG_TRACE_USR4(("SpiKey:GUI->HALL onCourierMessage onSendDipoPlayCommandUpdMsg received"));
   //!Send play keyCode Press event to AID
   uint32 u32KeyMode = T_e8_KeyMode__KEY_PRESS;
   uint32 u32KeyCode = T_e32_KeyCode__MULTIMEDIA_PLAY;
   _spiConnectionHandler->vSendKeyEvent(u32KeyMode, u32KeyCode);
   //! start 100ms timer to send play keyCode release event
   QSTimerExpiredMsg* pMsg = COURIER_MESSAGE_NEW(QSTimerExpiredMsg)();
   if (pMsg)
   {
      bool bRet = _qsTimer.setTimeout(0, 100, pMsg);
      _qsTimer.start();
      ETG_TRACE_USR4(("_qsTimer.setTimeout : %d", bRet));
   }
   return true;
}


/**
 * onCourierMessage - Message triggered when QStimer of 100ms expires to send Play command release key
 *
 * @param[in] QSTimerExpiredMsg
 * @return true - message consumed
 */

bool SpiKeyHandler::onCourierMessage(const QSTimerExpiredMsg& /*oMsg*/)
{
   ETG_TRACE_USR4(("QSTimerExpiredMsg is Received"));
   if (_qsTimer.getStatus() != 0)
   {
      _qsTimer.stop();
   }

   //!Send play keyCode release event to AID
   uint32 u32KeyMode = T_e8_KeyMode__KEY_RELEASE;
   uint32 u32KeyCode = T_e32_KeyCode__MULTIMEDIA_PLAY;
   _spiConnectionHandler->vSendKeyEvent(u32KeyMode, u32KeyCode);
   return true;
}


/*
 * vHandlePhoneAppRequest: Handler to Handle PhoneApp request
 * @param[in]  - u32KeyCode u32KeyMode: KeyEvent received from SM
 * @param[out] - none
 * @return     - void
 */

void SpiKeyHandler::vHandlePhoneAppRequest(uint32 u32KeyCode, uint32 u32KeyMode)
{
   //! Handling for Phone use cases
   ETG_TRACE_USR4(("vHandlePhoneAppRequest: _bPhoneAppStateActive: %d", _bPhoneAppStateActive));
   ETG_TRACE_USR4(("vHandlePhoneAppRequest: KeyCode = %d KeyMode %d", u32KeyCode, u32KeyMode));

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
   uint32 u32MappedKeyMode = u32GetMappedKeyMode(u32KeyMode);
   if (true == bGetPhoneAppStatus())
   {
      if ((u32KeyCode == Enum_HARDKEYCODE_HK_PHONE) || (u32KeyCode == Enum_HARDKEYCODE_SWC_PTT))
      {
         _spiConnectionHandler->vSendKeyEvent(u32MappedKeyMode, T_e32_KeyCode__DEV_PHONE_CALL);
      }
      else if (u32KeyCode == Enum_HARDKEYCODE_SWC_PHONE)
      {
         _spiConnectionHandler->vSendKeyEvent(u32MappedKeyMode, T_e32_KeyCode__DEV_PHONE_END);
      }
   }
   if ((u32KeyCode == Enum_HARDKEYCODE_HK_PHONE) && ((u32KeyMode == Enum_enRelease) || (u32KeyMode == Enum_enLongPressRelease)))
   {
      _spiConnectionHandler->vLaunchApp(_spiConnectionHandler->getActiveDeviceHandle(), enDeviceCategory(_spiConnectionHandler->getActiveDeviceCategory()), DiPOAppType__DIPO_MOBILEPHONE);
   }
#else
   uint32 u32MappedKeyMode = u32GetMappedKeyMode(u32KeyMode);

   if ((true == bGetPhoneAppStatus()) && (u32KeyCode == Enum_HARDKEYCODE_SWC_PHONE))
   {
      if ((u32KeyMode == Enum_enLongPress) || (u32KeyMode == Enum_enLongPressRelease))
      {
         ETG_TRACE_USR4(("vHandlePhoneAppRequest: T_e32_KeyCode__DEV_PHONE_END"));
         _spiConnectionHandler->vSendKeyEvent(u32MappedKeyMode, T_e32_KeyCode__DEV_PHONE_END);
      }
      else
      {
         ETG_TRACE_USR4(("vHandlePhoneAppRequest: T_e32_KeyCode__DEV_PHONE_CALL"));
         _spiConnectionHandler->vSendKeyEvent(u32MappedKeyMode, T_e32_KeyCode__DEV_PHONE_CALL);
      }
   }
   else
   {
      if (u32KeyMode == Enum_enRelease)
      {
         _spiConnectionHandler->vLaunchApp(_spiConnectionHandler->getActiveDeviceHandle(), enDeviceCategory(_spiConnectionHandler->getActiveDeviceCategory()), DiPOAppType__DIPO_MOBILEPHONE);
      }
      else if (u32KeyMode == Enum_enLongPressRelease)
      {
         POST_MSG((COURIER_MESSAGE_NEW(PlayBeepReqMsg)(hmibase::BEEPTYPE_ERROR)));
      }
   }
#endif
}


/*
 * vDIPOSpeechAppStateRequestHandler: Handler to Request SIRI State on PTT key events
 * @param[in]  - u32KeyEvent: KeyEvent received from SM
 * @param[out] - none
 * @return     - void
 */

void SpiKeyHandler::vDIPOSpeechAppStateRequestHandler(uint32 u32KeyEvent)
{
   enDiPOAppType eDiPoAppType = DiPOAppType__DIPO_NOT_USED;
   ETG_TRACE_USR4(("vDIPOSpeechAppStateRequestHandler: _bSpeechAppStateActive:  _bPhoneAppStateActive: %d %d", _bSpeechAppStateActive, _bPhoneAppStateActive));

   switch (u32KeyEvent)
   {
      case Enum_enPress: // short press
      {
         if (!_bSpeechAppStateActive && !_bPhoneAppStateActive)
         {
            eDiPoAppType = DiPOAppType__DIPO_SIRI_PREWARN;

            //! Start 800 ms to send SIRI Button Down
            vStartTimerOnPttPress();
         }
         else if (_bSpeechAppStateActive)
         {
            eDiPoAppType = DiPOAppType__DIPO_SIRI_BUTTONDOWN;
         }
         else
         {
            //Nothing to do
         }
      }
      break;
      case Enum_enRelease:
      {
         //! Stop the PTT timer started if key released
         vStopPttTimer();
         if (true == _bSpeechAppStateActive)
         {
            eDiPoAppType = DiPOAppType__DIPO_SIRI_BUTTONUP;
            vSiriButtonUpRequired(false);
         }
         //! If key released before long press(1500ms) and Button Down is sent after 800ms
         else if ((false == _bSpeechAppStateActive) && (true == bIsSiriButtonUpRequired()))
         {
            eDiPoAppType = DiPOAppType__DIPO_SIRI_BUTTONUP;  //Applicable only for DIPO
            vSiriButtonUpRequired(false);
         }
         //! for PTT Short release provide context to SDS
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
         else if (!(_bSpeechAppStateActive || _bPhoneAppStateActive || PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE == _spiConnectionHandler->getBtCallStatus()))
         {
            ETG_TRACE_USR4(("SpiKeyHandler:ContextSwitchOutReqMsg posted to SDS with ContextId: %d", SDS_CONTEXT_ROOT));
            POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(SDS_CONTEXT_ROOT, MEDIA_CONTEXT_START , SDS_CONTEXT_START)));
         }
#endif
         else
         {
            //Nothing to do
         }
      }
      break;

      case Enum_enLongPressRelease:
      {
         if (!_bPhoneAppStateActive)
         {
            eDiPoAppType = DiPOAppType__DIPO_SIRI_BUTTONUP;
            vSiriButtonUpRequired(false);
         }
      }
      break;

      default:
         break;
   }
   vSendLaunchAppRequestHandler(eDiPoAppType);
}


/*
 * vAndroidAutoSpeechAppStateRequestHandler: Handler to Request SIRI State on PTT key events
 * @param[in]  - u32KeyEvent: KeyEvent received from SM
 * @param[out] - none
 * @return     - void
 */

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
void SpiKeyHandler::vAndroidAutoSpeechAppStateRequestHandler(uint32 u32KeyEvent)
{
   enDiPOAppType eDiPoAppType = DiPOAppType__DIPO_NOT_USED;
   ETG_TRACE_USR4(("vAndroidAutoSpeechAppStateRequestHandler: _bSpeechAppStateActive:  _bPhoneAppStateActive: %d %d", _bSpeechAppStateActive, _bPhoneAppStateActive));

   switch (u32KeyEvent)
   {
      case Enum_enPress:
      {
         if ((!_bSpeechAppStateActive && !_bPhoneAppStateActive) || (_bSpeechAppStateActive))
         {
            eDiPoAppType = DiPOAppType__DIPO_SIRI_BUTTONDOWN;
         }
      }
      break;
      case Enum_enRelease:
      {
         if (true == _bSpeechAppStateActive)
         {
            eDiPoAppType = DiPOAppType__DIPO_SIRI_BUTTONUP;
         }
         //! for PTT Short release provide context to SDS
         else if (!(_bSpeechAppStateActive || _bPhoneAppStateActive || PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE == _spiConnectionHandler->getBtCallStatus()))
         {
            ETG_TRACE_USR4(("SpiKeyHandler:ContextSwitchOutReqMsg posted to SDS with ContextId: %d", SDS_CONTEXT_ROOT));
            POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(SDS_CONTEXT_ROOT, MEDIA_CONTEXT_START , SDS_CONTEXT_START)));
         }
         else
         {
            //Nothing to do
         }
      }
      break;

      case Enum_enLongPressRelease:
      {
         if (!_bPhoneAppStateActive)
         {
            eDiPoAppType = DiPOAppType__DIPO_SIRI_BUTTONUP;
         }
      }
      break;

      default:
         break;
   }
   vSendLaunchAppRequestHandler(eDiPoAppType);
}


#endif

/*
 * vSendLaunchAppRequestHandler: Handler to Request LaunchApp
 * @param[in]  - enDiPOAppType eDiPoAppType
 * @param[out] - none
 * @return     - void
 */

void SpiKeyHandler::vSendLaunchAppRequestHandler(enDiPOAppType eDiPoAppType)
{
   if (_spiConnectionHandler && eDiPoAppType != DiPOAppType__DIPO_NOT_USED)
   {
      ETG_TRACE_USR4(("vSendLaunchAppRequestHandler %u", ETG_CENUM(enDiPOAppType, eDiPoAppType)));

      _spiConnectionHandler->vLaunchApp(
         _spiConnectionHandler->getActiveDeviceHandle(),
         static_cast<enDeviceCategory>(_spiConnectionHandler->getActiveDeviceCategory()),
         eDiPoAppType);
   }
}


/*
 * vStartTimerOnPttPress: Handler to Start 800ms timer on PTT short press
 * @param[in]  - None
 * @param[out] - none
 * @return     - void
 */

void SpiKeyHandler::vStartTimerOnPttPress()
{
   ETG_TRACE_USR4(("vStartTimerOnPttPress: 800ms timer is started on PTT short press"));
   //! start 800ms timer to send SIRI Button_Down event if SIRI is inactive
   onPTTShortTimerExpMsg* pttShortTimer = COURIER_MESSAGE_NEW(onPTTShortTimerExpMsg)();
   if (pttShortTimer)
   {
      bool bRet = _timerOnPttShortPress.setTimeout(0, 800, pttShortTimer);
      _timerOnPttShortPress.start();
      ETG_TRACE_USR4(("_timerOnPttShortPress.setTimeout : %d", bRet));
   }
}


/*
 * vStartTimerOnPttPress: Handler to Stop 800ms timer started on PTT press
 * @param[in]  - None
 * @param[out] - none
 * @return     - void
 */

void SpiKeyHandler::vStopPttTimer()
{
   ETG_TRACE_USR4(("vStopPttTimer: stop the PTT timer"));
   if (_timerOnPttShortPress.getStatus() != 0)
   {
      _timerOnPttShortPress.stop();
   }
}


/**
 * onCourierMessage - Message triggered when onPTTShortTimer of 800ms expires
 *
 * @param[in] onPTTShortTimerExpMsg
 * @return true - message consumed
 */

bool SpiKeyHandler::onCourierMessage(const onPTTShortTimerExpMsg& /*oMsg*/)
{
   enDiPOAppType eDiPoAppType = DiPOAppType__DIPO_NOT_USED;
   ETG_TRACE_USR4(("onPTTShortTimerExpMsg is Received"));

   vStopPttTimer();
   //! send SIRI Button Down event after 800ms
   if (!_bSpeechAppStateActive && !_bPhoneAppStateActive)
   {
      eDiPoAppType = DiPOAppType__DIPO_SIRI_BUTTONDOWN;
   }
   vSendLaunchAppRequestHandler(eDiPoAppType);
   vSiriButtonUpRequired(true);
   return true;
}


/*
 * vSiriButtonUpRequired: Handler to Set Siri Button Up request if PTT was released before long press and after 800ms
 * @param[in]  - Bool
 * @param[out] - none
 * @return     - void
 */

void SpiKeyHandler::vSiriButtonUpRequired(bool bSiriButtonUpRequest)
{
   _bSiriButtonUpRequired = bSiriButtonUpRequest;
   ETG_TRACE_USR4(("vSiriButtonUpRequired: _bSiriButtonUpRequired: %d", _bSiriButtonUpRequired));
}


/*
 * bIsSiriButtonUpRequired: Handler to Get Siri Button Up request status
 * @param[in]  - None
 * @param[out] - none
 * @return     - Bool
 */

bool SpiKeyHandler::bIsSiriButtonUpRequired()
{
   ETG_TRACE_USR4(("bIsSiriButtonUpRequired: _bSiriButtonUpRequired: %d", _bSiriButtonUpRequired));
   return _bSiriButtonUpRequired;
}


/*
 * bGetPhoneAppStatus: Handler to get PhoneApp status
 * @param[in]  - None
 * @param[out] - none
 * @return     - Bool
 */

bool SpiKeyHandler::bGetPhoneAppStatus()
{
   return _bPhoneAppStateActive;
}


/*
 * This is a helper function to set Spi Device Speech Recognition Source status via Dataguard in SM
 * @param[in]  - Bool
 * @param[out] - None
 * @return     - Void
 */

void SpiKeyHandler::vUpdateSpiSpeechRecognitionDgStatus(bool bSpeechAppStatus)
{
   ETG_TRACE_USR4(("vUpdateSpiSpeechRecognitionDgStatus called"));
   (*_SpeechRecognitionState).mIsSpeechRecognitionActiveSource = bSpeechAppStatus;
   _SpeechRecognitionState.MarkAllItemsModified();

   if (_SpeechRecognitionState.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_SpeechRecognitionState : Updation failed..!!!"));
   }
}


/*
 * This is a helper function to set Spi Device Phone Source status via Dataguard in SM
 * @param[in]  - Bool
 * @param[out] - None
 * @return     - Void
 */

void SpiKeyHandler::vUpdateSpiPhoneAppDgStatus(bool bPhoneAppStatus)
{
   ETG_TRACE_USR4(("vUpdateSpiPhoneAppDgStatus called"));
   (*_SpiPhoneAppStatus).mIsSpiPhoneAppStatus = bPhoneAppStatus;
   _SpiPhoneAppStatus.MarkAllItemsModified();

   if (_SpiPhoneAppStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_SpiPhoneAppStatus : Updation failed..!!!"));
   }
}


} // end App
} //end Core
