/**
*  @file   IListFacade.h
*  @author ECV - vma6cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef ILISTFACADE_H_
#define ILISTFACADE_H_

#include "mplay_MediaPlayer_FIProxy.h"
#include "Core/MediaDefines.h"
#include <CanderaWidget/String/String.h>
namespace App {
namespace Core {
static const unsigned int MEDIA_BASE_LIST_ID = 4000;
/*File type to display in folder list*/
enum FolderListType
{
   FTS_FOLDER = 0u,
   FTS_AUDIO = 1u,
   FTS_VIDEO = 2u,
   FTS_PLAYLIST = 3u,
   FTS_IMAGE = 4u,
   FTS_ALL = 5u,
   FTS_FOLDER_UP = 6u,
   FTS_INVALID = 7u
};


struct RequestedListInfo
{
   uint32 _listType;
   uint32 _startIndex;
   uint32 _activeItemIndex;
   uint32 _bufferSize;
   uint32 _deviceTag;
};


struct ListResultInfo
{
   ::MPlay_fi_types::T_MPlayMediaObjects _oMediaObjects;
   uint32 _totalListSize;
   uint32 _startIndex;
   uint32 _listType;
   uint8 _metaDataTag;
};


/*Structure declaration for folder browsing*/
struct FolderListInfo
{
   std::string _folderPath;
   ::MPlay_fi_types::T_MPlayFileList _fileListItems;
   uint32 _totalListSize;
   uint32 _startIndex;
   uint32 _listType;
   uint32 _listHandle;
};


struct FileInfo
{
   enum  FolderListType _fileType;
   ::MPlay_fi_types::T_e8_MPlayPlayableStatus _playableStatus;
   std::string _fileName;
   bool _isPlaying;
};


#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
struct QuickSearchStringResult
{
   uint32 _position;
   ::MPlay_fi_types::T_e8_QuickSearchResult _quickSearchResult;
};


#endif
enum StateMachineState
{
   IDLE,
   WAITING_PLAYERINDEX_METHODRESULT,
   WAITING_PLAYERINDEXLISTSLICE_METHODRESULT,
   LIST_REQUEST_PROCESS_DONE
};


#if defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
typedef struct
{
   uint32 u32ListHandle;
   std::string sSearchKeyboardLetter;
   bool bLetterAvailable;
   uint32 u32LetterStartIndex;
   uint32 u32LetterEndIndex;
   uint32 countIndex;
} trSearchKeyboard_ListItem;
#endif

class IListFacade
{
   public:
      virtual ~IListFacade() {}
      virtual void requestListData(const RequestedListInfo& requestedListInfo) = 0;
      virtual void requestPlayItem(uint32 selectedIndex) = 0;
      virtual void removeListFromMap() = 0;
      virtual void releaseListHandle(uint32 listHandle) = 0;
      virtual void requestPlayOrSubFolderList(uint32 index) = 0;
      virtual void initStatemachine() = 0;
      virtual void requestChildIndexList(uint32 nextListId, uint32 activeDeviceTag) = 0;
      virtual void requestParentIndexList(uint32 prevListId) = 0;
      virtual void requestParentFolderList(const RequestedListInfo& requestedListInfo) = 0;
      virtual void initialiseListValuesForFolderBrowser(uint32) = 0;
      virtual void requestCurrentPlayingList(uint32 currentPlayingListHandle) = 0;

      //set functions
      virtual void setActiveIndex(uint32 activeIndex) = 0;
      virtual void setCurrentFolderPath(std::string) = 0;
      virtual void setCurrentStateMachineState(uint8) = 0;
      virtual void setCurrentPlaylistPath(std::string) = 0;
      virtual void setFolderRequestType(enum FolderListType folderListRequestType) = 0;
      virtual void setFocussedIndexAndNextListId(int32, uint32) = 0;
      virtual void updateFolderListParameters(uint32, uint32) = 0;

      //get functions
      virtual bool isRootFolder() = 0;
      virtual std::string getCurrentFolderPath() const = 0;
      virtual std::string getFirstStringElement(uint32) = 0;
      virtual uint32 getCurrentStartIndex(uint32 listid) = 0;
      virtual Candera::String getTextBySelectedIndex(uint32) = 0;
      virtual bool isListWaitingToComplete(void) = 0;

      virtual void registerProperties() = 0;
      virtual void deregisterProperties() = 0;
#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
      virtual void vRequestQSMediaplayerFilelist(std::string SearchString, uint32 startIndex) = 0;
#endif

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R
      virtual void requestPlayAllsongs() = 0;
#endif
#ifdef ABCSEARCH_SUPPORT
      virtual std::map<std::string, trSearchKeyboard_ListItem> getABCSearchKeyboardListItemMap() = 0;
#endif
};


}
}


#endif /* ILISTFACADE_H_ */
