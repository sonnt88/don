/**
 *  @file   SpiConnectionHandling.h
 *  @author ECV - alc7kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#ifndef _SPI_CONNECTION_H_
#define _SPI_CONNECTION_H_

#include "midw_smartphoneint_fiProxy.h"
#include "AppHmi_MediaStateMachine.h"
#include "AppHmi_SpiMessages.h"
#include "bosch/cm/ai/hmi/masteraudioservice/AudioSourceChangeProxy.h"
#include "bosch/cm/ai/hmi/masteraudioservice/SoundPropertiesProxy.h"
#include "org/genivi/audiomanager/CommandInterfaceProxy.h"
#include "App/Core/ContextSwitch/Proxy/provider/AppMedia_ContextProvider.h"
#include "AppHmi_MasterBase/AudioInterface/AudioDefines.h"
#include "Core/Spi/SpiUtils.h"
#include "Spi_defines.h"
#include  "CgiExtensions/DataBindingItem.hpp"


#include <ilm_client.h>
#include <ilm_control.h>

#ifndef CONTEXT_SWITCH_NS
#define CONTEXT_SWITCH_NS bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes
#endif

#define SPI_TEMPORARYCONTEXT_ONRVC_NCG3D_7788
#ifndef SPI_TEMPORARYCONTEXT_ONRVC_NCG3D_7788
#define SPI_TEMPORARYCONTEXT_ONRVC_NCG3D_7788
#endif


//forward declaration of class SpiMediaDataPoolConfig
class SpiMediaDataPoolConfig;
#define MASTERAUDIOSERVICE_INTERFACE ::bosch::cm::ai::hmi::masteraudioservice
#define AUDIOMANAGER_COMMANDINTERFACE org::genivi::audiomanager::CommandInterface

namespace App {
namespace Core {
class SpiConnectionSubject;
class SpiConnectionHandling:
   public ::midw_smartphoneint_fi::DeviceStatusInfoCallbackIF
   , public ::midw_smartphoneint_fi::GetDeviceInfoListCallbackIF
   , public ::midw_smartphoneint_fi::SelectDeviceCallbackIF
   , public ::midw_smartphoneint_fi::LaunchAppCallbackIF
   , public ::midw_smartphoneint_fi::SendTouchEventCallbackIF
   , public ::midw_smartphoneint_fi::SessionStatusInfoCallbackIF
   , public ::midw_smartphoneint_fi::DeviceDisplayContextCallbackIF
   , public ::midw_smartphoneint_fi::DeviceAudioContextCallbackIF
   , public ::midw_smartphoneint_fi::GetDeviceUsagePreferenceCallbackIF
   , public ::midw_smartphoneint_fi::BluetoothDeviceStatusCallbackIF
   , public ::midw_smartphoneint_fi::AccessoryAudioContextCallbackIF
   , public::midw_smartphoneint_fi:: AccessoryDisplayContextCallbackIF
   , public::midw_smartphoneint_fi::InvokeBluetoothDeviceActionCallbackIF
   , public::midw_smartphoneint_fi::SetFeatureRestrictionsCallbackIF
   , public ::midw_smartphoneint_fi::SendKeyEventCallbackIF
   , public::midw_smartphoneint_fi::SetVehicleConfigurationCallbackIF
   , public::midw_smartphoneint_fi::RotaryControllerEventCallbackIF
   , public::midw_smartphoneint_fi::AccessoryAppStateCallbackIF
   , public ::midw_smartphoneint_fi::ApplicationMediaMetaDataCallbackIF
   , public ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::ActiveSourceListCallbackIF
   , public MASTERAUDIOSERVICE_INTERFACE::SoundProperties::VolumeCallbackIF
   , public ::midw_smartphoneint_fi::ProjectionDeviceAuthorizationCallbackIF
   , public::midw_smartphoneint_fi::SetVehicleMovementStateCallbackIF
   , public MASTERAUDIOSERVICE_INTERFACE::SoundProperties::MuteStateCallbackIF

{
   public:
      virtual ~SpiConnectionHandling();
      SpiConnectionHandling(::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& pSpiConnectionProxy, AppMedia_ContextProvider* para_ContextProvider , ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::AudioSourceChangeProxy >& audioSourceChangeSPIProxy, ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy >& soundPropertiesProxy , SpiConnectionSubject* paraSpiConnectionNotify);

      void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);
      void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);

      virtual void onDeviceStatusInfoError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceStatusInfoError >& error);
      virtual void onDeviceStatusInfoStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceStatusInfoStatus >& status);

      virtual void onGetDeviceInfoListError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetDeviceInfoListError >& error);
      virtual void onGetDeviceInfoListResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetDeviceInfoListResult >& result);

      virtual void onSelectDeviceError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SelectDeviceError >& error);
      virtual void onSelectDeviceResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SelectDeviceResult >& result) ;

      virtual void onLaunchAppError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::LaunchAppError >& error);
      virtual void onLaunchAppResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::LaunchAppResult >& result);

      virtual void onSendTouchEventError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SendTouchEventError >& error);
      virtual void onSendTouchEventResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SendTouchEventResult >& result);

      virtual void onSessionStatusInfoStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SessionStatusInfoStatus >& status);
      virtual void onSessionStatusInfoError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SessionStatusInfoError >& error);

      virtual void onDeviceDisplayContextStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceDisplayContextStatus >& status);
      virtual void onDeviceDisplayContextError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceDisplayContextError >& error);

      virtual void onDeviceAudioContextStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceAudioContextStatus >& status);
      virtual void onDeviceAudioContextError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceAudioContextError >& error);

      virtual void onGetDeviceUsagePreferenceError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetDeviceUsagePreferenceError >& error);
      virtual void onGetDeviceUsagePreferenceResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetDeviceUsagePreferenceResult >& result);

      virtual void onBluetoothDeviceStatusStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::BluetoothDeviceStatusStatus >& status);
      virtual void onBluetoothDeviceStatusError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::BluetoothDeviceStatusError >& error) ;

      virtual void onAccessoryAudioContextError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::AccessoryAudioContextError >& error);
      virtual void onAccessoryAudioContextResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::AccessoryAudioContextResult >& result);

      virtual void onAccessoryDisplayContextError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::AccessoryDisplayContextError >& error);
      virtual void onAccessoryDisplayContextResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::AccessoryDisplayContextResult >& result);

      virtual void onInvokeBluetoothDeviceActionError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::InvokeBluetoothDeviceActionError >& error);
      virtual void onInvokeBluetoothDeviceActionResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::InvokeBluetoothDeviceActionResult >& result);

      virtual void onSetFeatureRestrictionsError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetFeatureRestrictionsError >& error);
      virtual void onSetFeatureRestrictionsResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetFeatureRestrictionsResult >& result);

      virtual void onSendKeyEventError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SendKeyEventError >& error);
      virtual void onSendKeyEventResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SendKeyEventResult >& result);

      virtual void onSetVehicleConfigurationError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetVehicleConfigurationError >& error);
      virtual void onSetVehicleConfigurationResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetVehicleConfigurationResult >& result);

      virtual void onRotaryControllerEventError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::RotaryControllerEventError >& error);
      virtual void onRotaryControllerEventResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::RotaryControllerEventResult >& result);

      virtual void onAccessoryAppStateError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::AccessoryAppStateError >& error);
      virtual void onAccessoryAppStateResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::AccessoryAppStateResult >& result);

      virtual void onApplicationMediaMetaDataError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::ApplicationMediaMetaDataError >& error);
      virtual void onApplicationMediaMetaDataStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::ApplicationMediaMetaDataStatus >& status);

      virtual void onProjectionDeviceAuthorizationError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::ProjectionDeviceAuthorizationError >& error);
      virtual void onProjectionDeviceAuthorizationStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::ProjectionDeviceAuthorizationStatus >& status);

      virtual void onSetVehicleMovementStateError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetVehicleMovementStateError >& /*error*/);
      virtual void onSetVehicleMovementStateResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >&/*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetVehicleMovementStateResult >& result);

      virtual void onActiveSourceListError(const ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::AudioSourceChangeProxy >& proxy,
                                           const ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::ActiveSourceListError >& error);
      virtual void onActiveSourceListSignal(const ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::AudioSourceChangeProxy >& proxy,
                                            const ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::ActiveSourceListSignal >& signal);

      void onVolumeError(const ::boost::shared_ptr<MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy >& proxy,
                         const ::boost::shared_ptr<MASTERAUDIOSERVICE_INTERFACE::SoundProperties::VolumeError >& error);
      void onVolumeUpdate(const ::boost::shared_ptr<MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy >& proxy,
                          const ::boost::shared_ptr<MASTERAUDIOSERVICE_INTERFACE::SoundProperties:: VolumeUpdate >& update);
      virtual void onMuteStateError(const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy >& /*proxy*/,
                                    const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::SoundProperties::MuteStateError >& /*error*/);
      virtual void onMuteStateUpdate(const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy >& /*proxy*/,
                                     const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::SoundProperties::MuteStateUpdate >& update);

      static void TraceCmd_SpiConnectionPopUpTrigger(uint8 u8TriggerStatus = 0);
      static void TraceCmd_SetAccessoryDisplayContext(enDisplayFlag displayflag, enDisplayContext displayContext);
      static void TraceCmd_SetAccessoryAudioContext(enAudioFlag audioflag, enAudioContext audioContext);
      static void TraceCmd_setSpiSetting(enDeviceCategory eDeviceCategory, enAutoLaunchDatapoolStatus eSetting);
      static void TraceCmd_SpiLaunchApp(enDeviceCategory eDeviceCategory, enDiPOAppType eDipoAppType);
      static void traceCmd_Spi_TriggerPTTShortPress();
      static void traceCmd_Spi_TriggerPTTShortRelease();
      static void traceCmd_Spi_TriggerPTTLongRelease();
      static void traceCmd_Spi_TriggerPTTLongPress();
      static void traceCmd_Spi_TriggerTELShortPress();
      static void traceCmd_Spi_TriggerTELShortRelease();
      static void traceCmd_Spi_TriggerTELLongPress();
      static void traceCmd_Spi_TriggerSWCNextShortPress();
      static void traceCmd_Spi_TriggerSWCNextRelease();
      static void TraceCmd_SetVehicleConfiguration(enVehicle_Configuration eVehicleConfig , ensetConfiguration eSetconfig);
      static void TraceCmd_SpiSetAccessoryAppState(enSpeechAppState eSpeechAppState , enPhoneAppState ePhoneAppState, enNavigationAppState eNavigationAppState);
      static void traceCmd_Spi_SendPlayCommand();
      static void TraceCmd_SpiConnErrorPopUps(enErrorPopUp ePopUpName);
      static void TraceCmd_SPIPhoneCallMetaData();
      void sendErrorPopUpTrigger(enErrorPopUp ePopUpName);
      void SendPlaycommandTrigger();
      void onTriggerSPIPhoneCallMetaData();

      void triggerCourierMsg(int msgid);
      void sendAvailableContextList(::midw_smartphoneint_fi_types::T_e8_DeviceCategory eDeviceCategory);
      void removeUnavailableContext(::midw_smartphoneint_fi_types::T_e8_DeviceCategory eDeviceCategory);
      void sendSelectDevice(uint32 u32devicehandle, int8 i8DeviceConnectionType , enDeviceConnectionReq eDeviceConnectionRequest, enEnabledInfo eEnableInfo);
      void vSetAccessoryAudioContext(bool bAudioflag, enAudioContext audioContext);
      void vLaunchApp(uint32 u32Devicehandle, enDeviceCategory eDeviceCategory, enDiPOAppType eDipoAppType);
      void setAccessoryDisplayContext(bool bDisplayflag, enDisplayContext displayContext);
      void setSpiSetting(enDeviceCategory eDeviceCategory, enAutoLaunchDatapoolStatus eSetting);
      void vUpdateCurrentAudioSource();
      int8 getdeviceConnectionType();
      int32 getActiveDeviceHandle();
      int32 getCurrentActiveAudioSource();

      void setVehicleConfiguration(enVehicle_Configuration eVehicleConfig , bool bSetconfig);
      void vSendKeyEvent(uint32, uint32);
      void vSetDeviceHandle(::midw_smartphoneint_fi_types::T_e8_DeviceCategory, uint32);
      bool onCourierMessage(const LaunchCarPlayReqMsg& /*msg*/);
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
      bool onCourierMessage(const DisclaimerActiveMsg& /*msg*/);
      bool onCourierMessage(const DisclaimerInactiveMsg& /*msg*/);
#endif
      bool onCourierMessage(const OnDisclaimerNoUpdMsg& /*msg*/);
      bool onCourierMessage(const OnLaunchCarplaywithAppReqMsg& /*msg*/);
      bool onCourierMessage(const onHK_AUX_CDEventMsg& /*msg*/);
      bool onCourierMessage(const onAppStateUpdMsg& state);
      bool onCourierMessage(const onHMISubStateONUpdateMsg& /*msg*/);


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      bool onCourierMessage(const DontRemindButtonReqMsg& /*msg*/);
      bool onCourierMessage(const AndroidDontRemindButtonReqMsg& /*msg*/);
      bool onCourierMessage(const LaunchAndroidAutoReqMsg& /*msg*/);
      bool onCourierMessage(const onAndroidDisclaimerNoUpdMsg&  /*msg*/);
      bool onCourierMessage(const OnLaunchAndroidAutowithAppReqMsg&  /*msg*/);
      bool onCourierMessage(const OnLaunchSPIWithAppReqMsg&  /*msg*/);
      void vCheckAndLaunchAAPPhone();
      bool isNativeBTSourceActive();
#endif

      bool vIsAudioSourceChange(tU32 u32SourceId = 0);
      bool isAccessoryAudioContext(tU32 u32SourceId = 0);
      bool isDipoDeviceConnected();
      bool isAndroidDeviceConnected();

      void onTriggerPTTShortPress();
      void onTriggerPTTShortRelease();
      void onTriggerPTTLongPress();
      void onTriggerPTTLongRelease();
      void onTriggerTELShortPress();
      void onTriggerTELShortRelease();
      void onTriggerTELLongPress();
      void onTriggerSWCNextShortPress();
      void onTriggerSWCNextRelease();
      void setRVCStatus(bool bRVCStatus);
      bool getRVCStatus();
      void getDeviceUsagePreference(uint32 u32DeviceHandle, ::midw_smartphoneint_fi_types::T_e8_DeviceCategory eDeviceCategory);
      void setDeviceConnectionType(int8 deviceConnectionType);
      void setAccessoryAppState(enSpeechAppState, enPhoneAppState, enNavigationAppState);
      void vSendRotaryEncoderEvent(int8);
      void setBtCallStatus(enPhoneAppState eBtCallStatus);
      void vOnBTCallStatusUpdate(enPhoneAppState eBTCallstatus);
      void setAccSpeechAppStatus(enSpeechAppState);
      void setAccNavAppStatus(enNavigationAppState);
      enPhoneAppState getBtCallStatus();
      enSpeechAppState getAccSpeechAppStatus();
      enNavigationAppState getAccNavAppStatus();
      void vSpiPhoneSourceStatus();
      void vSpiAlertSourceStatus();
      ::midw_smartphoneint_fi_types::T_e8_DeviceCategory getActiveDeviceCategory();
      void vUpdateSpiDeviceSessionStatus(bool, ::midw_smartphoneint_fi_types::T_e8_DeviceCategory eDeviceCategory);
      void vUpdateSpiAlertSourceStatus(bool);
      void vSpiDeviceConnectionPopUp(::midw_smartphoneint_fi_types::T_e8_DeviceType);
      ::midw_smartphoneint_fi_types::T_e8_DeviceType getConnDeviceType();
      bool getIsDeviceAuthorizationRequ();
      void setSettingTrigDisc(bool);
      bool getSettingTrigDisc();
      void vSpiMediaSourceStatus();
      void vUpdateSpiMediaSourceStatus(bool);
      void setCurrentHMIState(uint8 u8CurrentHMIState);
      uint8 getCurrentHMIState();
      void vSetSpiVideoActive(bool);
      bool bGetIsSpiVideoActive();
      void setVehicleMovementState(::midw_smartphoneint_fi_types::T_e8_ParkBrake , ::midw_smartphoneint_fi_types::T_e8_VehicleState);
#ifdef SPI_TEMPORARYCONTEXT_ONRVC_NCG3D_7788
      void vSetTemporaryContextOnRVC(bool);
      bool bGetTemporaryContextStatusOnRVC();
      void vUpdateAppModeStatusOnRVCOff();
#endif
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      ON_COURIER_MESSAGE(LaunchCarPlayReqMsg)
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
      ON_COURIER_MESSAGE(DisclaimerActiveMsg)
      ON_COURIER_MESSAGE(DisclaimerInactiveMsg)
#endif
      ON_COURIER_MESSAGE(OnDisclaimerNoUpdMsg)
      ON_COURIER_MESSAGE(OnLaunchCarplaywithAppReqMsg)
      ON_COURIER_MESSAGE(onHK_AUX_CDEventMsg)
      ON_COURIER_MESSAGE(onHMISubStateONUpdateMsg)
      //to know the FG AAP
      ON_COURIER_MESSAGE(onAppStateUpdMsg)
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      ON_COURIER_MESSAGE(LaunchAndroidAutoReqMsg)
      ON_COURIER_MESSAGE(DontRemindButtonReqMsg)
      ON_COURIER_MESSAGE(AndroidDontRemindButtonReqMsg)
      ON_COURIER_MESSAGE(onAndroidDisclaimerNoUpdMsg)
      ON_COURIER_MESSAGE(OnLaunchAndroidAutowithAppReqMsg)
      ON_COURIER_MESSAGE(OnLaunchSPIWithAppReqMsg)
#endif

      COURIER_MSG_MAP_END();

      Courier::DataItemContainer<CarplayiconStatusDataBindingSource> _IsCarplayiconStatus;
      DataBindingItem<SpiAlertActiveSourceDataBindingSource> _SpiAlertSourceState;
      DataBindingItem<SpiSessionUpdateDataBindingSource> _SpiSessionStatus;
      DataBindingItem<SpiMediaActiveSourceDataBindingSource> _SpiMediaSourceStatus;
      Courier::DataItemContainer<HeaderTextDataBindingSource> _headerText;
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
      DataBindingItem<SpiDisclaimerActionDataBindingSource> _SpiDisclaimerActionState;
#endif
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      Courier::DataItemContainer<DontRemindButtonStatusDataBindingSource> _IsDontRemindBtnStatus;
      Courier::DataItemContainer<AndroidAutoDontRemindButtonStatusDataBindingSource> _IsAndroidDontRemindBtnStatus;
      Courier::DataItemContainer<DIPOActiveStatusDataBindingSource> _isDIPOSessionActive;
      Courier::DataItemContainer<AndroidAutoActiveStatusDataBindingSource> _isAAPSessionActive;
      Courier::DataItemContainer<DisclaimerRemindBtnStatusDataBindingSource> _isDisclaimerRemindBtnStatus;

#endif

   private:
      ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& _spiMidwServiceProxy;
      ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::AudioSourceChangeProxy >& _audioSourceChangeSPIProxy;
      ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy >& _soundPropertiesProxy;
      ::std::vector< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::sourceData >SourceList;
      void updateDeviceStatus(int8);
      void updateDeviceConnectionType(int8);
      void vHandleSessionActiveStatus(::midw_smartphoneint_fi_types::T_e8_DeviceCategory, uint32);
      void vHandleSessionInActiveStatus(::midw_smartphoneint_fi_types::T_e8_DeviceCategory);
      void vDestroySpiTouchSurface();
      void vCreateSpiTouchSurface();
      void infoStartUpParm();
      void infoSteeringWheelPosition();
      void setFeatureRestrictionsDIPO();
      void setFeatureRestrictionsAAP();
      void vHandleContextResponse();
      bool isSourceAvailableInActiveList(uint32 checksource);
      void vUpdateSMSourceInfo();
      void vUpdateAudioAccessoryContext();
      void setIsDeviceAuthorizationRequ(bool);
      void setConnDeviceType(::midw_smartphoneint_fi_types::T_e8_DeviceType);
      void setUserSelection(bool);
      bool getUserSelection();
      void vFindAccessoryAudioContext(bool& audioflag, enAudioContext& eAudioContext);
      bool bIsAudioUnBorrowRequired();
      void checkBTCallNShowDiscPopUp();
      void checkBTCallNSendSelectDevice();
      void setIsDeviceSelectionRequ(bool);
      bool getIsDeviceSelectionRequ();
      void checkAndSelectDeviceifReq();
      void checkAndShowDisclaimerPopUpifRequ();
      void vUpdateCarplayIconStatus(bool);
      void setHeaderTextData(const Candera::String&);
      void mapBTChangeInfo(::midw_smartphoneint_fi_types::T_e8_BTChangeInfo);
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      void vTriggerDIPOSessionUpdate(bool);
      void vTriggerAAPSessionUpdate(bool);
      void updateDontRemindBtnStatus();
#endif
      //Added for BT-Phone Call handling
      int8 _mi8DeviceConnectionType;
      enPhoneAppState _meBtCallStatus;
      enSpeechAppState _meAccessorySpeechAppState;
      enNavigationAppState _meAccessoryNavAppState;
      uint32 _mu32DIPODeviceHandle;
      uint32 _mu32AAPDeviceHandle;

      //uint32 _mu32CurrentAudioSource; //skn6kor
      int32 _mCurrentActiveAudioSource;
      uint32 _mcurrentsource;
      enAudioContext _meCurrentAudioContext;

      bool _bMuteState;
      //instance created for dtatpool handler
      SpiMediaDataPoolConfig* _mDpHandler;
      AppMedia_ContextProvider* mptr_ContextProvider;
      SpiConnectionSubject* _mSpiConnectionNotifyer;

      bool bIlmInit;
      bool _mbRVCStatus;
      bool _mbDisplayFlagStatus;
      bool _mbCarPlayConnStatus;
      bool _mbAAPConnStatus;
      bool _mSpiVideoActive;
      ::std::string _mStrAlbumName;
      ::std::string _mStrArtistName;
      ::std::string _mStrTitleName;
      ::std::string _mAppDeviceName;
      uint32 _mTrackNumber;
      uint32 _mTotalTrackCount;
      bool _mbSessionStatus;
      bool _mUserSelection;
      bool _mbDeviceSelectionReq;
      bool _mbIsSettTrigDisc;
      uint8 _mu8CurrentHMIState;
      uint32 _mu32BTDeviceHandle;
      uint32 _mu32PrjDeviceHandle;
      ::midw_smartphoneint_fi_types::T_e8_BTChangeInfo _meBTChangeInfo;
      bool _mbIsBtDeactivationReq;
      ::midw_smartphoneint_fi_types::T_e8_DeviceCategory _meActiveDeviceCategory;
      ::midw_smartphoneint_fi_types::T_e8_DeviceType _meConnDeviceType;
      bool _mbAuthorizationStatus;
      ::midw_smartphoneint_fi_types::T_e8_ParkBrake _meHandBrakeState;
      ::midw_smartphoneint_fi_types::T_e8_VehicleState _meVehicleState;
#ifdef SPI_TEMPORARYCONTEXT_ONRVC_NCG3D_7788
      bool _mbSpiRvcTemporaryContext;
#endif
};


}// namespace AppLogic
} // namespace App

#endif // _SPI_CONNECTION_H_
