/*
 * TraceCommand.cpp
 *
 *  Created on: May 28, 2015
 *      Author: sla7cob
 */
#include "hall_std_if.h"
#include "Core/Utils/TraceCommand.h"
#include "MediaDatabinding.h"

#include "Common/Focus/RnaiviFocus.h"
#include "Common/Focus/Controller/ListRotaryController.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::TraceCommand::
#include "trcGenProj/Header/TraceCommand.cpp.trc.h"
#endif

namespace App {
namespace Core {

TraceCommand::TraceCommand()
{
   ETG_I_REGISTER_FILE();
}


TraceCommand::~TraceCommand()
{
   // TODO Auto-generated destructor stub
}


/*
 *
 * TtraceCmd_ShowTrackInfo message - called when user fires APPHMI_MEDIA ShowTrackInfo through TTFIs.
 *
 * Description the function calls a non static showTrackInfo to display the track info
 * @param[in] none
 * @return void
 */
ETG_I_CMD_DEFINE((traceCmd_ShowTrackInfo, "ShowTrackInfo"))

void TraceCommand::traceCmd_ShowTrackInfo()
{
   MediaDatabinding::getInstance().showTrackInfo();
}


/*
 *
 * tracecmd_ShowPlayModeStatus message - called when user fires APPHMI_MEDIA_ShowPlayModeStatus through TTFIs.
 *
 * Description the function calls a non static showPlayModeStatus to display the play mode status
 * @param[in] none
 * @return void
 */
ETG_I_CMD_DEFINE((tracecmd_ShowPlayModeStatus, "ShowPlayModeStatus"))

void TraceCommand::tracecmd_ShowPlayModeStatus()
{
   MediaDatabinding::getInstance().showPlayModeStatus();
}


/*
 * tracecmd_ShowCurrentListTypeOfMedtadataBrowse message - called when user fires APPHMI_MEDIA_ShowMetadataBrowseListType through TTFIs.
 *
 * Description the function calls a non static showCurrentListTypeOfMetadataBrowse to display the current list type of metadata browse.
 * @param[in] none
 * @return void
 */
ETG_I_CMD_DEFINE((tracecmd_ShowCurrentListTypeOfMedtadataBrowse, "ShowMetadataBrowseListType"))

void TraceCommand::tracecmd_ShowCurrentListTypeOfMedtadataBrowse()
{
   MediaDatabinding::getInstance().showCurrentListTypeOfMetadataBrowse();
}


/*
 * tracecmd_ShowCurrentFolderPath message - called when user fires APPHMI_MEDIA_ShowCurrentFolderPath through TTFIs.
 *
 * Description the function prints the current folder path .
 * @param[in] none
 * @return void
 */
ETG_I_CMD_DEFINE((tracecmd_ShowCurrentFolderPath, "ShowCurrentFolderPath"))

void TraceCommand::tracecmd_ShowCurrentFolderPath()
{
   ETG_TRACE_USR4(("Current folder path   : %s" , MediaDatabinding::getInstance().getCurrentFolderPath().c_str()));
   ETG_TRACE_USR4(("Size of Current folder path   : %d" , MediaDatabinding::getInstance().getCurrentFolderPath().length()));
}


/*
 * tracecmd_ShowIndexingInfo message - called when user fires APPHMI_MEDIA_ShowIndexingInfo through TTFIs.
 *
 * Description the function calls a non static showIndexingInfo to display the indexing info of the current device.
 * @param[in] none
 * @return void
 */
ETG_I_CMD_DEFINE((tracecmd_ShowIndexingInfo, "ShowIndexingInfo"))

void TraceCommand::tracecmd_ShowIndexingInfo()
{
   MediaDatabinding::getInstance().showIndexingInfo();
}


/*
 * tracecmd_ShowActiveMediaDevice message - called when user fires APPHMI_MEDIA_ShowActiveMediaDevice through TTFIs.
 *
 * Description the function calls a non static showIndexingInfo to display the current active media device.
 * @param[in] none
 * @return void
 */
ETG_I_CMD_DEFINE((tracecmd_ShowActiveMediaDevice, "ShowActiveMediaDevice"))

void TraceCommand::tracecmd_ShowActiveMediaDevice()
{
   ETG_TRACE_USR4(("Current Active Media device  : %d" , MediaDatabinding::getInstance().getCurrentActiveMediaDevice()));
}


ETG_I_CMD_DEFINE((tracecmd_ShowConnectedDevices, "ShowConnectedDevices"))

void TraceCommand::tracecmd_ShowConnectedDevices()
{
   MediaDatabinding::getInstance().showConnectedDevices();
}


ETG_I_CMD_DEFINE((tracecmd_ShowAudioSource, "ShowAudioSource"))

void TraceCommand::tracecmd_ShowAudioSource()
{
   MediaDatabinding::getInstance().showAudioSource();
}


ETG_I_CMD_DEFINE((tracecmd_ShowBTDeviceName, "ShowBTDeviceName"))

void TraceCommand::tracecmd_ShowBTDeviceName()
{
   MediaDatabinding::getInstance().showBTDeviceName();
}


ETG_I_CMD_DEFINE((tracecmd_ShowActiveDeviceTag, "ShowActiveDeviceTag"))

void TraceCommand::tracecmd_ShowActiveDeviceTag()
{
   MediaDatabinding::getInstance().showActiveDeviceTag();
}


ETG_I_CMD_DEFINE((tracecmd_ShowTotalNumberOfFilesAndFolders, "ShowTotalNumberOfFilesAndFolders"))

void TraceCommand::tracecmd_ShowTotalNumberOfFilesAndFolders()
{
   MediaDatabinding::getInstance().showTotalNumberOfFilesAndFolders();
}


ETG_I_CMD_DEFINE((tracecmd_ShowActivePlayingItemTag, "ShowActivePlayingItemTag"))

void TraceCommand::tracecmd_ShowActivePlayingItemTag()
{
   MediaDatabinding::getInstance().showActivePlayingItemTag();
}


ETG_I_CMD_DEFINE((traceCmd_SetRtEncDirection, "SetRtEncDirection %d", uint8))   // Trace class declaration
void TraceCommand::traceCmd_SetRtEncDirection(uint8 direction)
{
   /*FocusManagerConfigurator* configurator = dynamic_cast<FocusManagerConfigurator*>(RnaiviFocus::getConfigurator());
   if (configurator)
   {
      if (direction)
      {
         ETG_TRACE_USR4(("List rotation direction forward"));
         if (configurator->getReversibleListController())
         {
            configurator->getReversibleListController()->setDirection(true);
         }
      }
      else
      {
         ETG_TRACE_USR4(("List rotation direction reverse"));
         if (configurator->getReversibleListController())
         {
            configurator->getReversibleListController()->setDirection(false);
         }
      }
   }*/

   (COURIER_MESSAGE_NEW(SetScrollDirectionMsg)(direction))->Post();
}


ETG_I_CMD_DEFINE((tracecmd_ShowAlbumArtInfo, "ShowAlbumArtInfo"))

void TraceCommand::tracecmd_ShowAlbumArtInfo()
{
   MediaDatabinding::getInstance().showAlbumArtInfo();
}


ETG_I_CMD_DEFINE((tracecmd_ShowBTDeviceConnectedStatus, "ShowBTDeviceConnectedStatus"))

void TraceCommand::tracecmd_ShowBTDeviceConnectedStatus()
{
   MediaDatabinding::getInstance().showBTDeviceConnectedStatus();
}


}//end of namespace Core
}//end of namespace App
