/**
 *  @file   MediaPlayScreenHandling.cpp
 *  @author ECV - kng3kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#include "hall_std_if.h"
#include "Core/PlayScreen/MediaPlayScreenHandling.h"
#include "Core/Utils/MediaUtils.h"
#include "Core/Utils/KdsInfo.h"
#include "Core/Utils/MediaProxyUtility.h"

#define UTFUTIL_S_IMPORT_INTERFACE_GENERIC
#include "utf_if.h"

using namespace MPlay_fi_types;

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS         TR_CLASS_APPHMI_MEDIA_HALL_PLAY_SCREEN
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::MediaPlayScreenHandling::
#include "trcGenProj/Header/MediaPlayScreenHandling.cpp.trc.h"
#endif

#define MEDIAPLAYER_PLAY_TIME_STRING_SIZE 40
#define WAIT_TIME_SECONDS 2000
#define NEXT_PREVIOUS_REQUEST_TIMER 1000


namespace App {
namespace Core {

MediaPlayScreenHandling* MediaPlayScreenHandling::_playScreenHandlingInstance = NULL;


/**
 * @Destructor
 */
MediaPlayScreenHandling::~MediaPlayScreenHandling()
{
   _playScreenHandlingInstance = NULL;
   _mediaPlayerProxy.reset();
   _tunerMasterFiProxy.reset();
#ifdef TBT_NOTIFICATION_SUPPORT
   if (_mPtrNavigationServiceHandler != NULL)
   {
      _mPtrNavigationServiceHandler->vDeRegisterforTBTNotification(this);
      _mPtrNavigationServiceHandler = NULL;
   }
#endif
#ifdef PHOTOPLAYER_SUPPORT
   _u8SlideshowState = ::MPlay_fi_types::T_e8_MPlayState__e8PBS_STOPPED;
   _u32SlideshowTimeStatus = 0;
#endif
}


/**
 * @Constructor
 */
MediaPlayScreenHandling::MediaPlayScreenHandling(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy,
      ::boost::shared_ptr< ::MIDW_TUNERMASTER_FI::MIDW_TUNERMASTER_FIProxy >& pTunerMasterProxy)

   : _mediaPlayerProxy(pMediaPlayerProxy)
   , _tunerMasterFiProxy(pTunerMasterProxy)
   , _isNextPrevActionCompleted(TRUE)
   , _nextPrevActionCounter(0)
   , _errorfiletimer(*this, WAIT_TIME_SECONDS)
   , _nextPrevBufferTimer(*this, NEXT_PREVIOUS_REQUEST_TIMER)
   , _isPlayModeUpdatePending(false)
   , _currentPlayingSelection(UNKNOWN_SELECTION)
   , _isActualAlbumArtUpdated(false)
   , _statusOfSongPlayingFromFolderBrowse(false)
#ifdef PHOTOPLAYER_SUPPORT
   , _u8SlideshowState(::MPlay_fi_types::T_e8_MPlayState__e8PBS_STOPPED)
   , _u32SlideshowTimeStatus(0)
#endif
{
   _strAlbumArt.clear();
   _currentImageSize = 0;
   _TAStatus = false;
   _isAlbumArtEnable = true;
   _isAlbumArtFetched = false;
   _playScreenHandlingInstance = this;
   playPauseStatus = ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_PLAY;
   _ImageData.clear();
   ETG_I_REGISTER_FILE();
#ifdef TBT_NOTIFICATION_SUPPORT
   _mPtrNavigationServiceHandler = NavigationServiceHandler::GetInstance();
   if (_mPtrNavigationServiceHandler != NULL)
   {
      _mPtrNavigationServiceHandler->vRegisterforTBTNotification(this);
   }
#endif
}


/**
 * registerProperties - Trigger property registration to mediaplayer,  called from MediaHall class
 * @param[in] proxy
 * @parm[in] stateChange - state change service for corrosponding  proxy
 * @return void
 */
void MediaPlayScreenHandling::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_mediaPlayerProxy && _mediaPlayerProxy == proxy)
   {
      _mediaPlayerProxy->sendPlaytimeUpReg(*this);
      _mediaPlayerProxy->sendCurrentFolderPathUpReg(*this);
      _mediaPlayerProxy->sendNowPlayingUpReg(*this);
      _mediaPlayerProxy->sendPlaybackModeUpReg(*this);
      _mediaPlayerProxy->sendRepeatModeUpReg(*this);
      _mediaPlayerProxy->sendPlaybackStateUpReg(*this);
#ifdef PHOTOPLAYER_SUPPORT
      _mediaPlayerProxy->sendSlideshowStateUpReg(*this);
      _mediaPlayerProxy->sendSlideshowTimeUpReg(*this);
      _mediaPlayerProxy->sendNowShowingUpReg(*this);
#endif
   }
   else if (_tunerMasterFiProxy && _tunerMasterFiProxy == proxy)
   {
      ETG_TRACE_USR4(("sendFID_TUNMSTR_G_ANNO_BUTTON_STATUSUpReg"));
      _tunerMasterFiProxy->sendFID_TUNMSTR_G_ANNO_BUTTON_STATUSUpReg(*this);
   }
}


/**
 * deregisterProperties - Trigger property deregistration to mediaplayer, called from MediaHall class
 * @param[in] proxy
 * @parm[in] stateChange - state change service for corrsponding proxy
 * @return void
 */
void MediaPlayScreenHandling::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_mediaPlayerProxy && _mediaPlayerProxy == proxy)
   {
      _mediaPlayerProxy->sendPlaytimeRelUpRegAll();
      _mediaPlayerProxy->sendCurrentFolderPathRelUpRegAll();
      _mediaPlayerProxy->sendNowPlayingRelUpRegAll();
      _mediaPlayerProxy->sendPlaybackModeRelUpRegAll();
      _mediaPlayerProxy->sendRepeatModeRelUpRegAll();
      _mediaPlayerProxy->sendPlaybackStateRelUpRegAll();
#ifdef PHOTOPLAYER_SUPPORT
      _mediaPlayerProxy->sendSlideshowStateRelUpRegAll();
      _mediaPlayerProxy->sendSlideshowTimeRelUpRegAll();
      _mediaPlayerProxy->sendNowShowingRelUpRegAll();
#endif
   }
   else if (_tunerMasterFiProxy && _tunerMasterFiProxy == proxy)
   {
      _tunerMasterFiProxy->sendFID_TUNMSTR_G_ANNO_BUTTON_STATUSRelUpRegAll();
   }
}


/**
 * onPlaytimeError - called if playtime  property is updated with error from media player
 *
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void MediaPlayScreenHandling::onPlaytimeError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::PlaytimeError >& /*error*/)
{
   ETG_TRACE_USR4(("onPlaytimeError received"));
}


/**
 * onNowPlayingError - called if nowplaying  property is updated with error from media player
 *
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void MediaPlayScreenHandling::onNowPlayingError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::NowPlayingError >& /*error*/)
{
   ETG_TRACE_USR4(("onNowPlayingError received"));
}


/**
 * onPlaybackStateStatus - received status update for playback state property
 *
 * Description : This method will be invoked if the phone device AVRCP version is One_THREE or less ;For other version it will be handled in BTSetting class.
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */
void MediaPlayScreenHandling::onPlaybackStateStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::PlaybackStateStatus >& status)
{
   ETG_TRACE_USR4(("onPlaybackStateStatus received = %d ", status->getE8State()));

   bool isPlayActive = false;
   bool isPauseActive = false;

   switch (status->getE8State())
   {
      case::MPlay_fi_types::T_e8_MPlayState__e8PBS_PLAYING:
      case::MPlay_fi_types::T_e8_MPlayState__e8PBS_FFWD:
      case::MPlay_fi_types::T_e8_MPlayState__e8PBS_FREV:
      {
         isPlayActive = true;
         playPauseStatus = ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_PLAY;
         break;
      }
      case::MPlay_fi_types::T_e8_MPlayState__e8PBS_PAUSED:
      case::MPlay_fi_types::T_e8_MPlayState__e8PBS_STOPPED:
      {
         isPauseActive = true;
         playPauseStatus = ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_PAUSE;
         break;
      }
      default:
         ETG_TRACE_USR4(("Default case"));
         break;
   }
   MediaDatabinding::getInstance().updateBTIconState(isPlayActive, isPauseActive);
}


/**
 * requestNowPlayingGet - helper function to trigger get now playing status if source is change
 *
 * Description :
 * @param[in]
 * @parm[in]
 * @return void
 */
void MediaPlayScreenHandling::requestNowPlayingGet()
{
   _strAlbumArt.clear();
   _isActualAlbumArtUpdated = false;
   _mediaPlayerProxy->sendNowPlayingGet(*this);
   _mediaPlayerProxy->sendCurrentFolderPathGet(*this);
   if (!MediaDatabinding::getInstance().getVolumeAvailablity())
   {
      _mediaPlayerProxy->sendPlaytimeGet(*this);
   }
}


/**
 * onPlaybackStateError - called if Playback state  property is updated with error from media player
 *
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void MediaPlayScreenHandling::onPlaybackStateError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::PlaybackStateError >& /*error*/)
{
   ETG_TRACE_USR4(("onPlaybackStateError received"));
}


/**
 * onPlaytimeStatus - received status update for play time property
 *
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */
void MediaPlayScreenHandling::onPlaytimeStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::PlaytimeStatus >& status)
{
   ETG_TRACE_USR4(("onPlaytimeStatus received: "));
   ETG_TRACE_USR4(("ElapsedTime :%d TotalTime:%d", status->getU32ElapsedPlaytime(), status->getU32TotalPlaytime()));

   if (status->getU32TotalPlaytime() > 0)
   {
      unsigned int uiElapsedTimeInSec = status->getU32ElapsedPlaytime();

      unsigned int uiTotalTimeInSec = status->getU32TotalPlaytime();

      char aElapseTime [MEDIAPLAYER_PLAY_TIME_STRING_SIZE];
      char aRemainingTime [MEDIAPLAYER_PLAY_TIME_STRING_SIZE];
      char aTotalTime [MEDIAPLAYER_PLAY_TIME_STRING_SIZE];

      // Convert total time in hour, minute and seconds
      unsigned int uiTotalHour = 0U;
      unsigned int uiTotalMinutes = 0U;
      MediaUtils::vConvertSecToHourMinSec(uiTotalHour, uiTotalMinutes, uiTotalTimeInSec);
      MediaUtils::vFormatTime(aTotalTime, uiTotalHour, uiTotalMinutes, uiTotalTimeInSec);
      std::string oStrTotalTime = oGetMonospaceDigit(aTotalTime, MEDIAPLAYER_PLAY_TIME_STRING_SIZE);

      // Convert elapsed time in hour, minute and seconds
      unsigned int uiElapsedHour = 0U;
      unsigned int uiElapsedMinutes = 0U;
      MediaUtils::vConvertSecToHourMinSec(uiElapsedHour, uiElapsedMinutes, uiElapsedTimeInSec);
      MediaUtils::vFormatTime(aElapseTime, uiElapsedHour, uiElapsedMinutes, uiElapsedTimeInSec);
      std::string oStrElapseTime = oGetMonospaceDigit(aElapseTime, MEDIAPLAYER_PLAY_TIME_STRING_SIZE);

      // Convert Remaining time in hour, minute and seconds
      unsigned int uiRemainingHour = 0U;
      unsigned int uiRemainingMinutes = 0U;
      unsigned int uiRemainingSec = uiTotalTimeInSec - status->getU32ElapsedPlaytime();
      MediaUtils::vConvertSecToHourMinSec(uiRemainingHour, uiRemainingMinutes, uiRemainingSec);
      MediaUtils::vFormatTime(aRemainingTime, uiRemainingHour, uiRemainingMinutes, uiRemainingSec);
      std::string oStrRemaingTime("-");
      oStrRemaingTime.append(oGetMonospaceDigit(aRemainingTime, MEDIAPLAYER_PLAY_TIME_STRING_SIZE));
      MediaDatabinding::getInstance().updatePlaytimeInfo(aElapseTime, aTotalTime, status->getU32ElapsedPlaytime(), status->getU32TotalPlaytime());
#ifdef HMI_CDDA_SUPPORT
      /*CDDA metadata info for next track if playtime update is completed
      (This is workaround )*/
      if (_metaData.getDeviceType() == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA)
      {
         if (status->getU32ElapsedPlaytime() >= status->getU32TotalPlaytime())
         {
            ETG_TRACE_USR4(("CDDA - meatadata info for next track : %d --- %d ", status->getU32ElapsedPlaytime(), status->getU32TotalPlaytime()));
            if (_mediaPlayerProxy)
            {
               if (!_playMode.getRepeatState())
               {
                  _mediaPlayerProxy->sendRequestPlaybackActionStart(*this, ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_NEXT, 1);
               }
               else
               {
                  ETG_TRACE_USR4(("REPEAT"));
                  _mediaPlayerProxy->sendRequestPlaybackActionStart(*this, ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_PREV, 1);
               }
            }
         }
      }
#endif
   }
}//end function


/**
 * oGetMonospaceDigit - private helper function to get  time string in Mono space digit
 *
 * @param[in,out] pDestBuff
 * @parm[in] u32DestBufferSize
 * @return void
 */
std::string MediaPlayScreenHandling::oGetMonospaceDigit(char* pDestBuff, uint32 u32DestBufferSize)
{
   std::string oStr;
   cUtf8String strUtf8_String;
   tU8 Utf8StringBuffer[MEDIAPLAYER_PLAY_TIME_STRING_SIZE];
   strUtf8_String.vInit(Utf8StringBuffer, sizeof Utf8StringBuffer);
   if (NULL != pDestBuff)
   {
      for (uint32 u32Index = 0; u32Index < u32DestBufferSize; u32Index++)
      {
         if ((pDestBuff[u32Index] >= 0x30) && (pDestBuff[u32Index] <= 0x3A))
         {
            strUtf8_String.vAppendChar(pDestBuff[u32Index] + 0xF800);
         }
         else
         {
            strUtf8_String.vAppendChar(pDestBuff[u32Index]);
         }
      }
   }
   oStr.assign(reinterpret_cast<char*>(const_cast<tU8*>(strUtf8_String.pubGetCStr())));
   return oStr;
}


/**
 * onNowPlayingStatus - Receives info about the currently playing track and updates the same on playscreen
 *
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */
void MediaPlayScreenHandling::onNowPlayingStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::NowPlayingStatus >& status)
{
   ETG_TRACE_USR4(("uiListHandle :%d e8NowPlayingState:%d", status->getU32ListHandle(), status->getE8NowPlayingState()));

   ETG_TRACE_USR4(("vNowPlaying BitRate:%d, SampleRate:%d, audioChannelFormat:%d", status->getOMediaObject().getU32BitRate(), status->getOMediaObject().getU32SampleRate(), status->getOMediaObject().getE8AudioChannelFormat()));
   ETG_TRACE_USR4(("vNowPlaying F1:%s", status->getOMediaObject().getSMetaDataField1().c_str()));
   ETG_TRACE_USR4(("vNowPlaying F2:%s", status->getOMediaObject().getSMetaDataField2().c_str()));
   ETG_TRACE_USR4(("vNowPlaying F3:%s", status->getOMediaObject().getSMetaDataField3().c_str()));
   ETG_TRACE_USR4(("vNowPlaying F4:%s", status->getOMediaObject().getSMetaDataField4().c_str()));
   ETG_TRACE_USR4(("category F4:%d", status->getOMediaObject().getE8CategoryType()));
   ETG_TRACE_USR4(("ItemTag:%d", status->getU32Tag()));

   if ((_mediaPlayerProxy->hasNowPlaying()) &&
         (_mediaPlayerProxy->getNowPlaying().getU32Tag() != status->getU32Tag()))
   {
      _isActualAlbumArtUpdated = false;
   }

   triggerPendingNextPrevPlayBackAction();
   if (status->getOMediaObject().getU8DeviceTag() == MediaDatabinding::getInstance().getActiveDeviceTag())
   {
      MediaDatabinding::getInstance().updateActivePlayingItemTag(status->getU32Tag());
      _metaData.setMediaObject(status->getOMediaObject());
      updateMetaDataField(status->getOMediaObject());
      _mediaPlayerProxy->sendRequestListInformationStart(*this, status->getU32ListHandle());
      vUpdateAlbumArt(status->getOMediaObject().getSAlbumArt(), status->getE8NowPlayingState());
      handlePlayableStateOfCurrentTrack(status->getOMediaObject().getE8PlayableStatus());

      if ((_mediaPlayerProxy->hasNowPlaying()) &&
            (_mediaPlayerProxy->getNowPlaying().getOMediaObject().getU8DeviceTag() != status->getOMediaObject().getU8DeviceTag()))
      {
         _currentPlayingSelection = UNKNOWN_SELECTION;
      }
   }
   else
   {
      MediaDatabinding::getInstance().updateActivePlayingItemTag(0XFFFFFFFF);
      ETG_TRACE_USR4(("Wrong Now playing status CurrentSrcDevTag = %d NowPlayingDevTag = %d ", MediaDatabinding::getInstance().getActiveDeviceTag(),
                      status->getOMediaObject().getU8DeviceTag()));
      _currentPlayingSelection = UNKNOWN_SELECTION;
   }
}//end function


void MediaPlayScreenHandling::getSongPlayFromFolderBrowseStatus(bool status)
{
   _statusOfSongPlayingFromFolderBrowse = status;
}


/**
* updateMetaDataField - send ID3 update info to media screen
*
* @param[in] T_MPlayMediaObject song details
* @parm[out] none
* @return none
*/
void MediaPlayScreenHandling::updateMetaDataField(const T_MPlayMediaObject& oMediaObject)
{
   switch (oMediaObject.getE8CategoryType())
   {
      case  T_e8_MPlayCategoryType__e8CTY_TITLE:
      case T_e8_MPlayCategoryType__e8CTY_CHAPTER:
         MediaDatabinding::getInstance().updateMediaMetadataInfo(_metaData.getChapterTitle(), _metaData.getBookTitle(), _metaData.getArtistName());
         break;
      case T_e8_MPlayCategoryType__e8CTY_EPISODE:
      case T_e8_MPlayCategoryType__e8CTY_NAME:
         MediaDatabinding::getInstance().updateMediaMetadataInfo(_metaData.getArtistName(), _metaData.getPodcastEpisode(), _metaData.getPodcastName());
         break;
      default:
         break;
   }
}


#ifdef TBT_NOTIFICATION_SUPPORT
void MediaPlayScreenHandling::onNextManeuverDetailsUpdate(const bool& isRGActive, const TBTManeuverInfoData& TBTManeuverInfo)
{
   MediaDatabinding::getInstance().updateTBTInfo(isRGActive, TBTManeuverInfo);
}


#endif


/**
* handlePlayableStateOfCurrentTrack - start 2 sec timer if current track is not playble and on expire of timer loading file popup need to trigger to user
* untill playable file selected
* @param[in] T_e8_MPlayPlayableStatus
* @parm[out] none
* @return none
*/

void MediaPlayScreenHandling::handlePlayableStateOfCurrentTrack(T_e8_MPlayPlayableStatus playableStatus)
{
   if (playableStatus != ::MPlay_fi_types::T_e8_MPlayPlayableStatus__e8FP_PLAYABLE)
   {
      if (!_errorfiletimer.isActive() && isLoadingFilePopupActive == false)
      {
         _errorfiletimer.start();
      }
   }
   else
   {
      ETG_TRACE_USR4(("MediaPlayScreenHandling : Remove MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE popup"));
      _errorfiletimer.stop();
      //remove popup
      POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND)));
      POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE)));
      updateLoadingFilePopupStatus(false);
   }
}


void MediaPlayScreenHandling::onExpired(asf::core::Timer& timer, boost::shared_ptr<asf::core::TimerPayload> data)
{
   ETG_TRACE_USR4(("MediaPlayScreenHandling : Received Timer expired\n"));
   if (timer.getAct()  == _errorfiletimer.getAct())
   {
      if (data->getReason() == asf::core::TimerPayload_Reason__Completed && MediaDatabinding::getInstance().getCurrentActiveMediaDevice() == SOURCE_USB && MediaDatabinding::getInstance().GetCurrentAudioSource() == SRC_MEDIA_PLAYER)
      {
         // display popup
         ETG_TRACE_USR4(("MediaPlayScreenHandling : MediaDatabinding::getInstance().getCurrentActiveMediaDevice() %d", MediaDatabinding::getInstance().getCurrentActiveMediaDevice()));
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND)));
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE)));
         updateLoadingFilePopupStatus(true);
      }
   }
   else if ((timer.getAct()  == _nextPrevBufferTimer.getAct()) && (data->getReason() == asf::core::TimerPayload_Reason__Completed))
   {
      triggerPendingNextPrevPlayBackAction();
   }
}


void MediaPlayScreenHandling::updateLoadingFilePopupStatus(bool status)
{
   isLoadingFilePopupActive = status;
}


/**
 * vUpdateAlbumArt - private helper function to fetch album art
 *
 * @param[in] oStr Alubm art path
 * @parm[in, out] None
 * @return void
 */
void MediaPlayScreenHandling::vUpdateAlbumArt(const std::string& oStr, T_e8_MPlayNowPlayingState nowPlayingState)
{
   ETG_TRACE_USR4(("AlbumArt Path: %s", oStr.c_str()));

   if ((nowPlayingState == T_e8_MPlayNowPlayingState__e8NP_NEW_TRACK || nowPlayingState == T_e8_MPlayNowPlayingState__e8NP_LIST_START) && !_isActualAlbumArtUpdated)
   {
      if ((oStr != _strAlbumArt) && (!oStr.empty()))
      {
         _strAlbumArt = oStr;
         _mediaPlayerProxy->sendGetMediaObjectAlbumArtInfoStart(*this, _strAlbumArt, ALBUMARTWIDTH, ALBUMARTHEIGHT);
      }
      else if (oStr.empty())
      {
         _strAlbumArt.clear();
         _isAlbumArtFetched = false;
         drawAlbumArt();
      }
   }
} //end function

/**
 * onGetMediaObjectAlbumArtInfoResult
 * @param[in] proxy
 * @return void
 */

void MediaPlayScreenHandling::onGetMediaObjectAlbumArtInfoResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::GetMediaObjectAlbumArtInfoResult >& result)
{
   ETG_TRACE_USR4(("onGetMediaObjectAlbumArtInfoResult"));

   if (result->getU32PhotoSize() > 0)
   {
      _currentImageSize = result->getU32PhotoSize();
      ETG_TRACE_USR4(("m_u32CurrentImageSize = %d,SMIMEImageSubtype = %s", _currentImageSize, result->getSMIMEImageSubtype().c_str()));
      _mediaPlayerProxy->sendGetMediaObjectAlbumArtStart(*this, _strAlbumArt, ALBUMARTWIDTH, ALBUMARTHEIGHT);
   }
}


/**
 * onGetMediaObjectAlbumArtInfoError
 * @param[in] proxy
 * @return void
 */
void MediaPlayScreenHandling::onGetMediaObjectAlbumArtInfoError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::GetMediaObjectAlbumArtInfoError >& /*error*/)
{
   ETG_TRACE_USR4(("onGetMediaObjectAlbumArtInfoError"));
   _isAlbumArtFetched = false;
   drawAlbumArt();
   //MediaDatabinding::getInstance().updateAlbumArtDefault();
}


/**
 * onGetMediaObjectAlbumArtResult
 * @param[in] proxy
 * @return void
 */
void MediaPlayScreenHandling::onGetMediaObjectAlbumArtResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::GetMediaObjectAlbumArtResult >& result)
{
   ETG_TRACE_USR4(("onGetMediaObjectAlbumArtResult"));

   _ImageData = result->getOImageData();
   // MediaDatabinding::getInstance().updateAlbumArt(result->getOImageData(), _currentImageSize);
   _isAlbumArtFetched = true;
   _isActualAlbumArtUpdated = true;
   drawAlbumArt();
}


/**
 * onGetMediaObjectAlbumArtError
 * @param[in] proxy
 * @return void
 */
void MediaPlayScreenHandling::onGetMediaObjectAlbumArtError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::GetMediaObjectAlbumArtError >& /*error*/)
{
   ETG_TRACE_USR4(("onGetMediaObjectAlbumArtError"));
   _isAlbumArtFetched = false;
   drawAlbumArt();
   //MediaDatabinding::getInstance().updateAlbumArtDefault();
}


/**
 * onRequestPlaybackActionResult - called if RequestPlaybackAction method_result from media player was successful.
 *
 * @param[in] proxy
 * @parm[in] result
 * @return void
 */
void MediaPlayScreenHandling::onRequestPlaybackActionResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestPlaybackActionResult >& /*result*/)
{
   ETG_TRACE_USR4(("onRequestPlaybackActionResult received"));
}


/**
 * onRequestPlaybackActionError - called if RequestPlaybackAction method_result from media player has errors.
 *
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void MediaPlayScreenHandling::onRequestPlaybackActionError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestPlaybackActionError >& /*error*/)
{
   ETG_TRACE_USR4(("onRequestPlaybackActionError received"));
}


void MediaPlayScreenHandling::setPlayModeUpdatePending(bool isPlayModeUpdatePending)
{
   ETG_TRACE_USR4(("setPlayModeUpdatePending %d", isPlayModeUpdatePending))
   _currentPlayingSelection = UNKNOWN_SELECTION;
   _isPlayModeUpdatePending = isPlayModeUpdatePending;
}


/**
 * onPlaybackRepeatModeError - called if onPlaybackRepeatMode method_result from media player has errors.
 *
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */
void MediaPlayScreenHandling::onPlaybackModeError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::PlaybackModeError >& /*error*/)
{
   ETG_TRACE_USR4(("onPlaybackModeError received"));
}


/**
 * onPlaybackModeStatus - called if onPlaybackRepeatMode method_result from media player has result.
 *
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */
void MediaPlayScreenHandling::onPlaybackModeStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::PlaybackModeStatus >& status)
{
   ETG_TRACE_USR4(("onRandomModeStatus:RandomMode %d", status->getE8Mode()));

   if ((!_isPlayModeUpdatePending) && _mediaPlayerProxy->hasRepeatMode())
   {
      ETG_TRACE_USR4(("RandomStatus = %d RepeatStatus = %d _currentPlayingSelection %d", status->getE8Mode(), _mediaPlayerProxy->getRepeatMode().getE8Repeat(), _currentPlayingSelection));
      _playMode.updateRandomRepeatState(status->getE8Mode(), _mediaPlayerProxy->getRepeatMode().getE8Repeat(), _currentPlayingSelection);

      MediaDatabinding::getInstance().updateRepeatRandomStatus(_playMode.getRadomRepeatText(), _playMode.getRamdonState(), _playMode.getRepeatState(),
            _playMode.getRadomText(), _playMode.getRepeatText());
   }
}


/**
 * onRepeatModeError - called if onRepeatModeError method_result from media player has errors.
 *
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */
void MediaPlayScreenHandling::onRepeatModeError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::RepeatModeError >& /*error*/)
{
   ETG_TRACE_USR4(("onRepeatModeError received"));
}


/**
 * onRepeatModeStatus - called if onRepeatModeStatus method_result from media player has result.
 *
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */
void MediaPlayScreenHandling::onRepeatModeStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::RepeatModeStatus >& status)
{
   ETG_TRACE_USR4(("onRepeatModeStatus:RepeatMode %d", status->getE8Repeat()));

   if ((!_isPlayModeUpdatePending) && _mediaPlayerProxy->hasPlaybackMode())
   {
      ETG_TRACE_USR4(("RandomStatus = %d RepeatStatus = %d _currentPlayingSelection %d", _mediaPlayerProxy->getPlaybackMode().getE8Mode(), status->getE8Repeat(), _currentPlayingSelection));
      _playMode.updateRandomRepeatState(_mediaPlayerProxy->getPlaybackMode().getE8Mode(), status->getE8Repeat(), _currentPlayingSelection);
      MediaDatabinding::getInstance().updateRepeatRandomStatus(_playMode.getRadomRepeatText(), _playMode.getRamdonState(), _playMode.getRepeatState(),
            _playMode.getRadomText(), _playMode.getRepeatText());
   }
}


/**
 * onRequestListInformationError - called if RequestListInformation method_result from media player has errors
 *
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void MediaPlayScreenHandling::onRequestListInformationError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestListInformationError >& /*error*/)
{
   ETG_TRACE_USR4(("onRequestListInformationError received. \n"));
}


/**
 * onRequestListInformationResult - called if RequestListInformation method_result from media player has status.
 *
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */
void MediaPlayScreenHandling::onRequestListInformationResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestListInformationResult >& result)
{
   ::MPlay_fi_types::T_e8_MPlayListType currentListHandleType = result->getE8ListType();

   if (_statusOfSongPlayingFromFolderBrowse)
   {
      if (_mediaPlayerProxy->getNowPlaying().getOMediaObject().getE8PlayableStatus() == T_e8_MPlayPlayableStatus__e8FP_DRM_PROTECTED && currentListHandleType == ::MPlay_fi_types::T_e8_MPlayListType__e8LTY_CURRENT_SELECTION)
      {
         ETG_TRACE_USR4(("Playable status %d DRM Protected popup Inserted", _mediaPlayerProxy->getNowPlaying().getOMediaObject().getE8PlayableStatus()));
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION)));
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND)));
      }
      _statusOfSongPlayingFromFolderBrowse = false;
   }
   if (_metaData.getDeviceType() == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA)
   {
      MediaDatabinding::getInstance().updateTrackNumber(_mediaPlayerProxy->getNowPlaying().getU32Tag() - 1, result->getU32ListSize());
   }
   else
   {
      uint32 _currentTrackNo = (_mediaPlayerProxy->hasNowPlaying()) ? (_mediaPlayerProxy->getNowPlaying().getU32Position()) : 0;
      MediaDatabinding::getInstance().updateTrackNumber(_currentTrackNo, result->getU32ListSize());
   }
   ETG_TRACE_USR4(("onRequestListInformationResult %d", currentListHandleType));
}


/**
 * onCurrentFolderPathError - called if CurrentFolderPath method_result from media player has errors
 *
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void MediaPlayScreenHandling::onCurrentFolderPathError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< mplay_MediaPlayer_FI::CurrentFolderPathError >& /*error*/)
{
   ETG_TRACE_USR4(("onCurrentFolderPathError"));
}


/**
 * onCurrentFolderPathStatus - Receives status updates for the property CurrentFolderPath.
 *
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */
void MediaPlayScreenHandling::onCurrentFolderPathStatus(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< mplay_MediaPlayer_FI::CurrentFolderPathStatus >& status)
{
   std::string strFolderPath = status->getSCurrentFolderPath();
   uint32 listhandle = status->getU32ListHandle();
   ETG_TRACE_USR4(("listhandle = %d onCurrentFolderPathStatus : %s", listhandle, strFolderPath.c_str()));
   std::string Artist = _metaData.getArtistName();
   std::string Album = _metaData.getAlbumName();
   std::string Title = _metaData.getSongTitle();
#ifdef BT_METADATA_DEFAULT_VALUES_SUPPORT
   /*BT metadata info is filled with Song Title, Album Name and Track Name for unavailable case*/
   if (_metaData.getDeviceType() == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH)
   {
      if (Title == "")
      {
         Title = "Audio track";
      }
      if (Artist == "" &&  !Album.empty())
      {
         Artist = "Unknown artist";
      }
      else if (Album == "" &&  !Artist.empty())
      {
         Album = "Unknown album";
      }
   }
   else
   {
      if (_metaData.getAlbumName() == "" && listhandle != 0)
      {
         std::string sFileName;
         MediaUtils::bExtractFileName(strFolderPath, sFileName);
         Album = sFileName;
      }
   }
#else
   if (_metaData.getAlbumName() == "" && listhandle != 0)
   {
      std::string sFileName;
      MediaUtils::bExtractFileName(strFolderPath, sFileName);
      Album = sFileName;
   }
#endif
   /*CDDA metadata info filled with track no for CD text not availabe*/
   if (_metaData.getDeviceType() == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA && Title == "")
   {
      std::string iStr = "Track ";
      std::string trackName = "";
      char indexInStr[MEDIA_HEADER_STRING_SIZE] = "";

      MediaUtils::convertToString(indexInStr, _metaData.getTagValue());
      trackName.assign(iStr + indexInStr);
      Artist.assign("");
      Album.assign("");
      Title.assign(trackName);
      ETG_TRACE_USR4(("trackName = %s", trackName.c_str()))
   }

   if ((_mediaPlayerProxy->hasNowPlaying()) && (_mediaPlayerProxy->getNowPlaying().getOMediaObject().getU8DeviceTag() == MediaDatabinding::getInstance().getActiveDeviceTag()))
   {
      if (!isPodcastAndAudiobookPlaying())
      {
         MediaDatabinding::getInstance().updateMediaMetadataInfo(Artist, Album, Title);
      }
      updatePlayModeIfPending(status->getU32ListHandle());
   }
}


/**
 * bUpdateCategoryType - Function to check the categorytype.
 *
 * @param[in] none
 * @parm[in] none
 * @return bool
 */
bool MediaPlayScreenHandling::isPodcastAndAudiobookPlaying()
{
   ::MPlay_fi_types::T_e8_MPlayCategoryType categoryType = _metaData.getCategoryType();
   if ((categoryType == T_e8_MPlayCategoryType__e8CTY_TITLE) ||
         (categoryType == T_e8_MPlayCategoryType__e8CTY_CHAPTER) ||
         (categoryType == T_e8_MPlayCategoryType__e8CTY_EPISODE) ||
         (categoryType == T_e8_MPlayCategoryType__e8CTY_NAME))
   {
      return true;
   }
   else
   {
      return false;
   }
}


void MediaPlayScreenHandling::updatePlayModeIfPending(uint32 listHandle)
{
   PlayingSelection currentPlayingSelection = (listHandle != 0) ? FOLDER_BASED_SELECTION : METADATA_BASED_SELECTION;

   if (currentPlayingSelection != _currentPlayingSelection
         && (_currentPlayingSelection != UNKNOWN_SELECTION) && (_mediaPlayerProxy->hasRepeatMode()))
   {
      if (currentPlayingSelection == METADATA_BASED_SELECTION)
      {
         if (_mediaPlayerProxy->getRepeatMode().getE8Repeat() == ::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_ALL)
         {
            _isPlayModeUpdatePending = false;
            _mediaPlayerProxy->sendRepeatModeSet(*this, ::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_LIST);
         }
         else if (_mediaPlayerProxy->getRepeatMode().getE8Repeat() == ::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS)
         {
            _isPlayModeUpdatePending = true;
         }
      }
      else
      {
         if (_mediaPlayerProxy->getRepeatMode().getE8Repeat() == ::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_LIST)
         {
            _isPlayModeUpdatePending = false;
            _mediaPlayerProxy->sendRepeatModeSet(*this, ::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_ALL);
         }
         else if (_mediaPlayerProxy->getRepeatMode().getE8Repeat() == ::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS)
         {
            _isPlayModeUpdatePending = true;
         }
      }
   }
   _currentPlayingSelection = currentPlayingSelection;

   if (_isPlayModeUpdatePending && _mediaPlayerProxy->hasRepeatMode()
         && _mediaPlayerProxy->hasPlaybackMode())
   {
      ETG_TRACE_USR4(("RandomStatus = %d RepeatStatus = %d _currentPlayingSelection %d", _mediaPlayerProxy->getPlaybackMode().getE8Mode(), _mediaPlayerProxy->getRepeatMode().getE8Repeat(), _currentPlayingSelection));
      _playMode.updateRandomRepeatState(_mediaPlayerProxy->getPlaybackMode().getE8Mode(), _mediaPlayerProxy->getRepeatMode().getE8Repeat(), _currentPlayingSelection);
      MediaDatabinding::getInstance().updateRepeatRandomStatus(_playMode.getRadomRepeatText(), _playMode.getRamdonState(), _playMode.getRepeatState(),
            _playMode.getRadomText(), _playMode.getRepeatText());
   }
   _isPlayModeUpdatePending = false;
}


#ifdef PHOTOPLAYER_SUPPORT
/**
 * ResumePlayPhotoPlayerReqMsg Courier message - called when user wants to resume photo slideshow after pausing.
 *
 * @param[in] msg
 * @return true - message consumed
 */
bool MediaPlayScreenHandling::onCourierMessage(const ResumePlayPhotoPlayerReqMsg& /*msg*/)
{
   ETG_TRACE_USR4(("onCourierMessage - ResumePlayPhotoPlayerReqMsg - _u8SlideshowState: %d, _u32SlideshowTimeStatus: %d", _u8SlideshowState, _u32SlideshowTimeStatus));
   switch (_u8SlideshowState)
   {
      case ::MPlay_fi_types::T_e8_MPlayState__e8PBS_PAUSED:
      {
         _mediaPlayerProxy->sendRequestSlideshowActionStart(*this, ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_PLAY, 1);
         break;
      }
      case ::MPlay_fi_types::T_e8_MPlayState__e8PBS_STOPPED:
      {
         POST_MSG((COURIER_MESSAGE_NEW(InitialisePlayPhotoPlayerReqMsg)()));
         break;
      }
      default:
         break;
   }

   return true;
}


/**
 * PausePhotoPlayerReqMsg Courier message - called when user wants to pause photo slideshow.
 *
 * @param[in] msg
 * @return true - message consumed
 */
bool MediaPlayScreenHandling::onCourierMessage(const PausePhotoPlayerReqMsg& /*msg*/)
{
   ETG_TRACE_USR4(("onCourierMessage - PausePhotoPlayerReqMsg - _u8SlideshowState: %d, _u32SlideshowTimeStatus: %d", _u8SlideshowState, _u32SlideshowTimeStatus));
   _mediaPlayerProxy->sendRequestSlideshowActionStart(*this, ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_PAUSE, 1);

   return true;
}


/**
 * PrevPhotoPlayerReqMsg Courier message - called when user wants to jump to previous photo slideshow.
 *
 * @param[in] msg
 * @return true - message consumed
 */
bool MediaPlayScreenHandling::onCourierMessage(const PrevPhotoPlayerReqMsg& msg)
{
   uint8 jumpSteps = msg.GetSteps();
   ETG_TRACE_USR4(("onCourierMessage - PrevPhotoPlayerReqMsg - _u8SlideshowState: %d, _u32SlideshowTimeStatus: %d, jumpSteps: %d", _u8SlideshowState, _u32SlideshowTimeStatus, jumpSteps));
   _mediaPlayerProxy->sendRequestSlideshowActionStart(*this, ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_PREV, jumpSteps);

   return true;
}


/**
 * NextPhotoPlayerReqMsg Courier message - called when user wants to jump to next photo slideshow.
 *
 * @param[in] msg
 * @return true - message consumed
 */
bool MediaPlayScreenHandling::onCourierMessage(const NextPhotoPlayerReqMsg& msg)
{
   uint8 jumpSteps = msg.GetSteps();
   ETG_TRACE_USR4(("onCourierMessage - NextPhotoPlayerReqMsg - _u8SlideshowState: %d, _u32SlideshowTimeStatus: %d, jumpSteps = %d", _u8SlideshowState, _u32SlideshowTimeStatus, jumpSteps));
   _mediaPlayerProxy->sendRequestSlideshowActionStart(*this, ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_NEXT, jumpSteps);

   return true;
}


/**
 * StopPhotoPlayerReqMsg Courier message - called when user wants to stop photo slideshow.
 *
 * @param[in] msg
 * @return true - message consumed
 */
bool MediaPlayScreenHandling::onCourierMessage(const StopPhotoPlayerReqMsg& /*msg*/)
{
   ETG_TRACE_USR4(("onCourierMessage - StopPhotoPlayerReqMsg - _u8SlideshowState: %d, _u32SlideshowTimeStatus: %d", _u8SlideshowState, _u32SlideshowTimeStatus));
   _mediaPlayerProxy->sendRequestSlideshowActionStart(*this, ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_STOP, 1);

   return true;
}


/**
 * FitScreenPhotoPlayerReqMsg Courier message - called when user wants to resume from fullscreen to photo player cover.
 *
 * @param[in] msg
 * @return false - message passed
 */
bool MediaPlayScreenHandling::onCourierMessage(const FitScreenPhotoPlayerReqMsg& /*msg*/)
{
   ETG_TRACE_USR4(("onCourierMessage - FitScreenPhotoPlayerReqMsg - _u8SlideshowState: %d, _u32SlideshowTimeStatus: %d", _u8SlideshowState, _u32SlideshowTimeStatus));
   switch (_u8SlideshowState)
   {
      case ::MPlay_fi_types::T_e8_MPlayState__e8PBS_PLAYING:
      case ::MPlay_fi_types::T_e8_MPlayState__e8PBS_PAUSED:
      {
         _mediaPlayerProxy->sendRequestSlideshowActionStart(*this, ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_STOP, 1);
         break;
      }

      default:
         break;
   }

   return false;
}


/**
 * SettingPhotoPlayerReqMsg Courier message - called when user wants to set photo playing option.
 *
 * @param[in] msg
 * @return true - message consumed
 */
bool MediaPlayScreenHandling::onCourierMessage(const SettingPhotoPlayerReqMsg& /*msg*/)
{
   ETG_TRACE_USR4(("onCourierMessage - SettingPhotoPlayerReqMsg"));
   return true;
}


#endif

/**
 * SeekNextReqMsg Courier message - called when user wants to activate next track or presses HK_NEXT.
 *
 * @param[in] msg
 * @return bool
 */
bool MediaPlayScreenHandling::onCourierMessage(const SeekNextReqMsg& /*msg*/)
{
   if (MediaDatabinding::getInstance().getVolumeAvailablity())
   {
      if (bUpdateNextPrevCounter(T_e8_MPlayAction__e8PBA_NEXT))
      {
         ++_nextPrevActionCounter;
         ETG_TRACE_USR4(("MEDIA HALL: SeekNextReqMsg nextprevious counter increased %d. \n", _nextPrevActionCounter));
      }
   }
   else
   {
      ETG_TRACE_USR4(("Volume is zero or in mute state and next action cannot be performed"));
   }
   return true;
}


/**
 * SeekPrevReqMsg Courier message - called when user wants to activate previous track or presses HK_PREVIOUS.
 *
 * @param[in] msg
 * @return bool
 */
bool MediaPlayScreenHandling::onCourierMessage(const SeekPrevReqMsg& /*msg*/)
{
   if (MediaDatabinding::getInstance().getVolumeAvailablity())
   {
      if (bUpdateNextPrevCounter(T_e8_MPlayAction__e8PBA_PREV))
      {
         --_nextPrevActionCounter;
         ETG_TRACE_USR4(("MEDIA HALL: SeekPrevReqMsg nextprevious counter decreased %d. \n", _nextPrevActionCounter));
      }
   }
   else
   {
      ETG_TRACE_USR4(("Volume is zero or in mute state and previous action cannot be performed"));
   }
   return true;
}


/**
 * FastForwardStartReqMsg Courier message - called when user does a long press of HK_NEXT.
 *
 * @param[in] msg
 * @return bool
 */
bool MediaPlayScreenHandling::onCourierMessage(const FastForwardStartReqMsg& /*msg*/)
{
   if (MediaDatabinding::getInstance().getVolumeAvailablity())
   {
      _mediaPlayerProxy->sendRequestPlaybackActionStart(*this, ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_FFWD_START, 1);
   }
   else
   {
      ETG_TRACE_USR4(("Volume is zero or in mute state and fast forward action cannot be performed"));
   }
   return true;
}


/**
 * FastForwardStopReqMsg Courier message - called when user release the long press of HK_NEXT.
 *
 * @param[in] msg
 * @return bool
 */
bool MediaPlayScreenHandling::onCourierMessage(const FastForwardStopReqMsg& /*msg*/)
{
   if (MediaDatabinding::getInstance().getVolumeAvailablity())
   {
      _mediaPlayerProxy->sendRequestPlaybackActionStart(*this, ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_FFWD_STOP, 1);
   }
   else
   {
      ETG_TRACE_USR4(("Volume is zero or in mute state and fast forward action cannot be performed"));
   }
   return true;
}


/**
 * FastRewindStartReqMsg Courier message - called when user does a long press of HK_PREVIOUS.
 *
 * @param[in] msg
 * @return bool
 */
bool MediaPlayScreenHandling::onCourierMessage(const FastRewindStartReqMsg& /*msg*/)
{
   if (MediaDatabinding::getInstance().getVolumeAvailablity())
   {
      _mediaPlayerProxy->sendRequestPlaybackActionStart(*this, ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_FREV_START, 1);
   }
   else
   {
      ETG_TRACE_USR4(("Volume is zero or in mute state and fast rewind action cannot be performed"));
   }
   return true;
}


/**
 * FastRewindStopReqMsg Courier message - called when user release the long press of HK_PREVIOUS.
 *
 * @param[in] msg
 * @return bool
 */
bool MediaPlayScreenHandling::onCourierMessage(const FastRewindStopReqMsg& /*msg*/)
{
   if (MediaDatabinding::getInstance().getVolumeAvailablity())
   {
      _mediaPlayerProxy->sendRequestPlaybackActionStart(*this, ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_FREV_STOP, 1);
   }
   else
   {
      ETG_TRACE_USR4(("Volume is zero or in mute state and fast rewind action cannot be performed"));
   }
   return true;
}


/**
 * PlayPauseReqMsg Courier message - called when user does a  press of SK_PlayPause in Audio Main Screen.
 *
 * @param[in] msg
 * @return bool
 */
bool MediaPlayScreenHandling::onCourierMessage(const PlayPauseReqMsg&)
{
   if (playPauseStatus == ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_PLAY)
   {
      POST_MSG((COURIER_MESSAGE_NEW(PauseReqMsg)()));
   }
   else
   {
      POST_MSG((COURIER_MESSAGE_NEW(PlayReqMsg)()));
   }
   return true;
}


/**
 * PlayReqMsg Courier message - called when user does a  press of SK_Play in BT Audio Main Screen.
 *
 * @param[in] msg
 * @return bool
 */
bool MediaPlayScreenHandling::onCourierMessage(const PlayReqMsg&)
{
   if (MediaDatabinding::getInstance().getVolumeAvailablity())
   {
      _mediaPlayerProxy->sendRequestPlaybackActionStart(*this, ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_PLAY, 1);
   }
   else
   {
      ETG_TRACE_USR4(("Volume is zero or in mute state and play will happen with volume 1"));
      POST_MSG((COURIER_MESSAGE_NEW(PlayFromVolumeZeroStateMsg)()));
   }
   return true;
}


/**
 * PauseReqMsg Courier message - called when user does a  press of SK_Pause in BT Audio Main Screen.
 *
 * @param[in] msg
 * @return bool
 */
bool MediaPlayScreenHandling::onCourierMessage(const PauseReqMsg&)
{
   _mediaPlayerProxy->sendRequestPlaybackActionStart(*this, ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_PAUSE, 1);
   return true;
}


/**
 * RepeatReqMsg Courier message - called when user does a press Repeat.
 *
 * @param[in] msg
 * @return bool
 */
bool MediaPlayScreenHandling::onCourierMessage(const RepeatReqMsg& /*msg*/)
{
   ETG_TRACE_USR4(("MEDIA HALL : RepeatReqMsg courier message received and _currentPlayingSelection: %d", _currentPlayingSelection));

   ::MPlay_fi_types::T_e8_MPlayRepeat repeatStatus = _playMode.getNextRepeatState(_currentPlayingSelection, _mediaPlayerProxy->getRepeatMode().getE8Repeat());

   ETG_TRACE_USR4(("RepeatReqMsg requesting repeatStatus: %d", repeatStatus));

   _mediaPlayerProxy->sendRepeatModeSet(*this, repeatStatus);

   return true;
}


/**
 * RandomReqMsg Courier message - called when user does a press Random/Shuffle.
 *
 * @param[in] msg
 * @return bool
 */
bool MediaPlayScreenHandling::onCourierMessage(const RandomReqMsg& /*msg*/)
{
   ETG_TRACE_USR4(("MEDIA HALL : RandomReqMsg courier message received and _currentPlayingSelection: %d", _currentPlayingSelection));

   ::MPlay_fi_types::T_e8_MPlayMode randomStatus = _playMode.getNextRandomState(_mediaPlayerProxy->getPlaybackMode().getE8Mode());

   ETG_TRACE_USR4(("RandomReqMsg requesting randomStatus: %d", randomStatus));

   _mediaPlayerProxy->sendPlaybackModeSet(*this, randomStatus);

   return true;
}


/**
 * bUpdateNextPrevCounter function - called to check whether _nextPrevActionCounter needs to be incremented/decremented.
 *
 * Description : The function will send a methodstart to skip the song based on::MPlay_fi_types::T_e8_MPlayAction enum value .
 * @param[in] enum::MPlay_fi_types::T_e8_MPlayAction
 * @return bool
 * 		  TRUE  -if next previous action is not completed.
 * 		  FALSE -if next and previous action is completed.
 */
bool MediaPlayScreenHandling::bUpdateNextPrevCounter(T_e8_MPlayAction nPlaybackId)
{
   ETG_TRACE_USR4(("MEDIA HALL : incrementNextPreviousCounter message received. %d \n", _isNextPrevActionCompleted));
   bool bIsRequestNeedToBuffer = true;
   if (_isNextPrevActionCompleted)
   {
      if (_nextPrevActionCounter == 0)
      {
         ++_nextPrevActionCounter;
      }

      _mediaPlayerProxy->sendRequestPlaybackActionStart(*this, nPlaybackId, _nextPrevActionCounter);
      _nextPrevBufferTimer.start();

      _isNextPrevActionCompleted = FALSE;
      _nextPrevActionCounter = 0;
      bIsRequestNeedToBuffer = false;
   }
   return bIsRequestNeedToBuffer;
}


/**
 * triggerPendingNextPrevPlayBackAction function - called when an update has been completed during fast/repeated press of HK_PREVIOUS or HK_NEXT.
 *
 * Description : The function will send a methodstart to skip the songs to _nextPrevActionCounter steps based on _nextPrevActionCounter value.
 * @param[in] none
 * @return void
 */
void MediaPlayScreenHandling::triggerPendingNextPrevPlayBackAction()
{
   ETG_TRACE_USR4(("MediaPlayScreenHandling::triggerPendingNextPrevPlayBackAction _isNextPrevActionCompleted =%d and _nextPrevActionCounter =%d ", _isNextPrevActionCompleted, _nextPrevActionCounter));

   if (!_isNextPrevActionCompleted)
   {
      _nextPrevBufferTimer.stop();
      if (_nextPrevActionCounter > 0)
      {
         _mediaPlayerProxy->sendRequestPlaybackActionStart(*this, ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_NEXT, _nextPrevActionCounter);
         ETG_TRACE_USR4(("Number of Next track requested %d", _nextPrevActionCounter));
         _nextPrevBufferTimer.start();
      }
      else if (_nextPrevActionCounter < 0)
      {
         _nextPrevActionCounter *= (-1);
         _mediaPlayerProxy->sendRequestPlaybackActionStart(*this, ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_PREV, _nextPrevActionCounter);
         ETG_TRACE_USR4(("Number of Previous track requested %d", _nextPrevActionCounter));
         _nextPrevBufferTimer.start();
      }
      else
      {
         _isNextPrevActionCompleted = TRUE;
         _mediaPlayerProxy->sendNowPlayingGet(*this); // Fix for NCG3D-11912
      }
      _nextPrevActionCounter = 0;
   }
}


ETG_I_CMD_DEFINE((TraceCmd_ActivateNextPrevious, "ActivateNextPrevious %d", ETG_I_CENUM(T_e8_MPlayAction)))   // Trace class declaration

/**
 * TraceCmd_ActivateNextPrevious message - called when user fires APPHMI_MEDIA ActivateNextPrevious through TTFIs.
 * This message will post courier message based on the value of nPlaybackId.
 * @param[in] nPlaybackId
 * description for nPlaybackId:
 *    value 3- Previous
 *    value 4- Next
 *    value 5- Start Fast Rewind
 *    value 6- Stop Fast Rewind
 *    value 7- Start Fast Forward
 *    value 8- Stop Fast Rewind
 * @return void
 */
void MediaPlayScreenHandling::TraceCmd_ActivateNextPrevious(T_e8_MPlayAction nPlaybackId)
{
   if (_playScreenHandlingInstance)
   {
      _playScreenHandlingInstance->postTtfisSkipMessage(nPlaybackId);
   }
}


/**
 * postTtfisSkipCommond private helper function to post courier message for ttfis trigger for playback action.
 * This message will post courier message based on the value of nPlaybackId.
 * @param[in] nPlaybackId
 * description for nPlaybackId:
 *    value 3- Previous
 *    value 4- Next
 *    value 5- Start Fast Rewind
 *    value 6- Stop Fast Rewind
 *    value 7- Start Fast Forward
 *    value 8- Stop Fast Rewind
 * @return void
 */
void MediaPlayScreenHandling::postTtfisSkipMessage(T_e8_MPlayAction playbackId)
{
   switch (playbackId)
   {
      case ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_NEXT:
      {
         POST_MSG((COURIER_MESSAGE_NEW(SeekNextReqMsg)()));
         break;
      }
      case ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_PREV:
      {
         POST_MSG((COURIER_MESSAGE_NEW(SeekPrevReqMsg)()));
         break;
      }
      case ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_FFWD_START:
      {
         POST_MSG((COURIER_MESSAGE_NEW(FastForwardStartReqMsg)()));
         break;
      }
      case ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_FFWD_STOP:
      {
         POST_MSG((COURIER_MESSAGE_NEW(FastForwardStopReqMsg)()));
         break;
      }
      case ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_FREV_START:
      {
         POST_MSG((COURIER_MESSAGE_NEW(FastRewindStartReqMsg)()));
         break;
      }
      case ::MPlay_fi_types::T_e8_MPlayAction__e8PBA_FREV_STOP:
      {
         POST_MSG((COURIER_MESSAGE_NEW(FastRewindStopReqMsg)()));
         break;
      }

      default :
      {
         ETG_TRACE_USR4(("unknown parameter passed from TTFIS"));
         break;
      }
   }

   ETG_TRACE_USR4(("TraceCmd_ActivateNextPrevious %d requested", ETG_CENUM(T_e8_MPlayAction, playbackId)));
}


/**
 * TAReqMsg Courier message - called when user Presses the TA Button.
 *
 * @param[in] msg
 * @return bool
 */
bool MediaPlayScreenHandling::onCourierMessage(const TAReqMsg& /*msg*/)
{
   ::midw_tunermaster_fi_types::T_b8_SourceAnno source;
   ::midw_tunermaster_fi_types::T_b32_AnnoType anno;

   _TAStatus = !_TAStatus;

   anno.setTUNMSTR_ANNOTYPE_TA(_TAStatus);
   source.setTUNMSTR_ANNOSRC_FM(true);
   source.setTUNMSTR_ANNOSRC_DAB(true);

   _tunerMasterFiProxy->sendFID_TUNMSTR_S_ACTIVATE_ANNOUNCEMENTStart(*this, anno, source);

   return true;
}


/**
 * Description     : Virtual function implemented to get update of FID_TUNMSTR_S_ACTIVATE_ANNOUNCEMENT Error
 *
 * @param[in]      : proxy: the client side representation of the CCA Functional Interface 'MIDW_TUNERMASTER_FIProxy'
 * @param[in]      : error : The error message of 'FID_TUNMSTR_S_ACTIVATE_ANNOUNCEMENT'
 * @return         : void
 */
void MediaPlayScreenHandling::onFID_TUNMSTR_S_ACTIVATE_ANNOUNCEMENTError(const ::boost::shared_ptr< ::MIDW_TUNERMASTER_FI::MIDW_TUNERMASTER_FIProxy >& proxy, const boost::shared_ptr< ::MIDW_TUNERMASTER_FI::FID_TUNMSTR_S_ACTIVATE_ANNOUNCEMENTError >& error)
{
   (void)proxy;
   (void)error;
   ETG_TRACE_USR4(("onFID_TUNMSTR_S_ACTIVATE_ANNOUNCEMENTError"));
}


/**
 * Description     : Virtual function implemented to get update of FID_TUNMSTR_S_ACTIVATE_ANNOUNCEMENT Result
 *
 * @param[in]      : proxy: the client side representation of the CCA Functional Interface 'MIDW_TUNERMASTER_FIProxy'
 * @param[in]      : result : The Result message of the property "FID_TUNMSTR_S_ACTIVATE_ANNOUNCEMENT"
 * @return         : void
 */
void MediaPlayScreenHandling::onFID_TUNMSTR_S_ACTIVATE_ANNOUNCEMENTResult(const ::boost::shared_ptr< ::MIDW_TUNERMASTER_FI::MIDW_TUNERMASTER_FIProxy >& proxy, const boost::shared_ptr< ::MIDW_TUNERMASTER_FI::FID_TUNMSTR_S_ACTIVATE_ANNOUNCEMENTResult >& result)
{
   (void)proxy;
   (void)result;
   ETG_TRACE_USR4(("onFID_TUNMSTR_S_ACTIVATE_ANNOUNCEMENTResult"));
}


/**
 * Description     : Virtual function implemented to get update of FID_TUNMSTR_G_ANNO_BUTTON_STATUS Error
 *
 * @param[in]      : proxy: the client side representation of the CCA Functional Interface 'MIDW_TUNERMASTER_FIProxy'
 * @param[in]      : error : The error message of 'FID_TUNMSTR_G_ANNO_BUTTON_STATUS'
 * @return         : void
 */
void MediaPlayScreenHandling::onFID_TUNMSTR_G_ANNO_BUTTON_STATUSError(const ::boost::shared_ptr< ::MIDW_TUNERMASTER_FI::MIDW_TUNERMASTER_FIProxy >& proxy, const boost::shared_ptr< ::MIDW_TUNERMASTER_FI::FID_TUNMSTR_G_ANNO_BUTTON_STATUSError >& error)
{
   (void)proxy;
   (void)error;
   ETG_TRACE_USR4(("onFID_TUNMSTR_G_ANNO_BUTTON_STATUSError"));
}


/**
 * Description     : Virtual function implemented to get update of FID_TUNMSTR_G_ANNO_BUTTON_STATUS Status
 *
 * @param[in]      : proxy: the client side representation of the CCA Functional Interface 'MIDW_TUNERMASTER_FIProxy'
 * @param[in]      : status : The Result message of the property "FID_TUNMSTR_G_ANNO_BUTTON_STATUS"
 * @return         : void
 */
void MediaPlayScreenHandling::onFID_TUNMSTR_G_ANNO_BUTTON_STATUSStatus(const ::boost::shared_ptr< ::MIDW_TUNERMASTER_FI::MIDW_TUNERMASTER_FIProxy >& proxy, const boost::shared_ptr< ::MIDW_TUNERMASTER_FI::FID_TUNMSTR_G_ANNO_BUTTON_STATUSStatus >& status)
{
   (void)proxy;//To solve lint errors

   ::midw_tunermaster_fi_types::T_b8_SourceAnno source;
   ::midw_tunermaster_fi_types::T_b32_AnnoType anno;

   if (status->getSource() == 0)
   {
      _TAStatus  = status->getB32AnnouncementType().getTUNMSTR_ANNOTYPE_TA();

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      if (!KdsInfo::isEuropeonRegion())
      {
         _TAStatus = false;
      }
#endif
   }
   MediaDatabinding::getInstance().updateTAStatusInfo(_TAStatus);

   ETG_TRACE_USR4(("onFID_TUNMSTR_G_ANNO_BUTTON_STATUSStatus, m_TAStatus = %d", _TAStatus));
}


#ifdef ALBUM_ART_TOGGLE_SUPPORT
/**
 * AlbumArtToggleReqMsg Courier message - called when user Presses the Album Artwork Button.
 *
 * @param[in] msg
 * @return bool
 */
bool MediaPlayScreenHandling::onCourierMessage(const AlbumArtToggleReqMsg& /*msg*/)
{
   _isAlbumArtEnable = _isAlbumArtEnable ? false : true;
   MediaDatabinding::getInstance().updateAlbumArtTogglestatus(_isAlbumArtEnable);
   if ((_currentImageSize != 0) && (!_strAlbumArt.empty()) && (_isAlbumArtFetched))
   {
      MediaDatabinding::getInstance().updateAlbumArtVisibleStatus(_isAlbumArtEnable);
   }
   else
   {
      MediaDatabinding::getInstance().updateDefaultAlbumArtVisibleStatus(_isAlbumArtEnable);
   }
   return true;
}


#endif

void MediaPlayScreenHandling::drawAlbumArt()
{
   if ((_currentImageSize != 0) && (!_strAlbumArt.empty()) && (_isAlbumArtFetched))
   {
      MediaDatabinding::getInstance().updateAlbumArt(_ImageData, _currentImageSize, _isAlbumArtEnable);
   }
   else
   {
      MediaDatabinding::getInstance().updateAlbumArtDefault(_isAlbumArtEnable);
   }
}


ETG_I_CMD_DEFINE((TraceCmd_ActivatePopup, "ActivateMediaPopup %d", int))

void MediaPlayScreenHandling::TraceCmd_ActivatePopup(int popupId)
{
   if (_playScreenHandlingInstance)
   {
      _playScreenHandlingInstance->postActivatePopup(popupId);
   }
}


void MediaPlayScreenHandling::postActivatePopup(int popupId)
{
   switch (popupId)
   {
      case 1:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE)));
         break;
      }
      case 2:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND)));
         break;
      }
      case 3:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND)));
         break;
      }
      case 4:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND)));
         break;
      }
      case 5:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1,  MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH)));
         break;
      }
      case 6:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND)));
         break;
      }
      case 7:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND)));
         break;
      }
      case 8:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND)));
         break;
      }
      case 9:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION)));
         break;
      }
      case 10:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS)));
         break;
      }
      case 11:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING)));
         break;
      }
      case 12:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING)));
         break;
      }
      case 13:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_READING)));
         break;
      }
      case 14:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR)));
         break;
      }
      case 15:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND)));
         break;
      }
      case 16:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND)));
         break;
      }
      case 17:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE)));
         break;
      }
      case 18:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_OVERHEATED)));
         break;
      }
      case 19:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_DISC_ERROR)));
         break;
      }
      default:
         break;
   }
}


ETG_I_CMD_DEFINE((TraceCmd_DeActivatePopup, "DeActivateMediaPopup %d", int))

void MediaPlayScreenHandling::TraceCmd_DeActivatePopup(int popupId)
{
   if (_playScreenHandlingInstance)
   {
      _playScreenHandlingInstance->postDeActivatePopup(popupId);
   }
}


void MediaPlayScreenHandling::postDeActivatePopup(int popupId)
{
   switch (popupId)
   {
      case 1:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE)));
         break;
      }
      case 2:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_UNSUPPORTED_SYSIND)));
         break;
      }
      case 3:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND)));
         break;
      }
      case 4:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND)));
         break;
      }
      case 5:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1,  MediaPopups_Popups_MEDIA_AUX__USB_POPUP_QUICKSEARCH)));
         break;
      }
      case 6:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND)));
         break;
      }
      case 7:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND)));
         break;
      }
      case 8:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_POPUP_IPOD_NOTSUPPORTED_SYSIND)));
         break;
      }
      case 9:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION)));
         break;
      }
      case 10:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS)));
         break;
      }
      case 11:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING)));
         break;
      }
      case 12:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING)));
         break;
      }
      case 13:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_READING)));
         break;
      }
      case 14:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR)));
         break;
      }
      case 15:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND)));
         break;
      }
      case 16:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__POPUP_AUX_USB_FORMAT_ERROR_SYSIND)));
         break;
      }
      case 17:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE)));
         break;
      }
      case 18:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_OVERHEATED)));
         break;
      }
      case 19:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_DISC_ERROR)));
         break;
      }
      default:
         break;
   }
}


#ifdef ENCODER_ROTATION_TRACK_CHANGE_SUPPORT
/**
 * EncoderRotatoryMsg Courier message - called when user rotates the Encoder.
 *
 * @param[in] msg
 * @return bool
 */
bool MediaPlayScreenHandling::onCourierMessage(const EncoderTrackChangeUpMsg& msg)
{
   uint8 steps = msg.GetSteps();
   if (isEncoderAvailableRegion())
   {
      if (steps > 0)
      {
         _nextPrevActionCounter = steps;
         POST_MSG((COURIER_MESSAGE_NEW(SeekNextReqMsg)()));
      }
      else
      {
         _nextPrevActionCounter = steps * -1;
         POST_MSG((COURIER_MESSAGE_NEW(SeekPrevReqMsg)()));
      }
   }
   return true;
}


/**
 * isEncoderAvailable function - checks the regions for the availability of Encoder functionality.
  * @param[in] none
 * @return bool
 */
bool MediaPlayScreenHandling::isEncoderAvailableRegion()
{
   return ((!KdsInfo::isAsrRegion()) || (!KdsInfo::isEuropeonRegion()) || (!KdsInfo::isGccRegion())
           || (!KdsInfo::isMexicoRegion()) || (!KdsInfo::isNewZealandRegion()) || (!KdsInfo::isRussianRegion())
           || (!KdsInfo::isSouthAfricanRegion()) || (!KdsInfo::isTurkeyRegion())) ? true : false;
}


#endif

#ifdef SEEK_SUPPORT
//ETG_I_CMD_DEFINE((TraceCmd_SeekTo, "SeekTo %d", int))

void MediaPlayScreenHandling::TraceCmd_SeekTo(unsigned int seektime)
{
   if (_playScreenHandlingInstance)
   {
      _playScreenHandlingInstance->postSeekmessage(seektime);
   }
}


void MediaPlayScreenHandling::postSeekmessage(unsigned int seektime)
{
   _mediaPlayerProxy->sendSeekToStart(*this, seektime * 1000, ::MPlay_fi_types::T_e8_MPlayPlaypointFormat__e8PPF_ABSOLUTE);
}


void MediaPlayScreenHandling::onSeekToError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< mplay_MediaPlayer_FI::SeekToError >& error)
{
   (void)proxy;
   (void)error;
   ETG_TRACE_USR4(("Received the seek error"));
}


void MediaPlayScreenHandling::onSeekToResult(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< mplay_MediaPlayer_FI::SeekToResult >& result)
{
   (void)proxy;
   (void)result;
   ETG_TRACE_USR4(("Received the seek result"));
}


bool MediaPlayScreenHandling::onCourierBindingItemChange_MediaMetaDataItem(Courier::Request& request)
{
   _metadataProgressBar.SetValue(request.ItemKey(), request.GetItemValue());
   unsigned int seektime = static_cast<unsigned int>((*_metadataProgressBar).mProgressbar_ElapseTime);
   ETG_TRACE_USR4(("data binding update received %d", seektime));
   _mediaPlayerProxy->sendSeekToStart(*this, seektime * 1000, ::MPlay_fi_types::T_e8_MPlayPlaypointFormat__e8PPF_ABSOLUTE);
   return true;
}


#endif

#ifdef PHOTOPLAYER_SUPPORT
/**
 * onRequestSlideshowActionError - function called when mediaplayer gets an error during the method call of RequestSlideshowAction.
 *
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void MediaPlayScreenHandling::onRequestSlideshowActionError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestSlideshowActionError >& /*error*/)
{
   ETG_TRACE_USR4(("onRequestSlideshowActionError"));
}


/**
 * onRequestSlideshowActionResult - function called when mediaplayer gets an result during the method call of RequestSlideshowAction.
 *
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void MediaPlayScreenHandling::onRequestSlideshowActionResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestSlideshowActionResult >& /*result*/)
{
   ETG_TRACE_USR4(("onRequestSlideshowActionResult"));
}


/**
 * onSlideshowStateError - function called when mediaplayer gets an error during the property SlideshowState updated.
 *
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void MediaPlayScreenHandling::onSlideshowStateError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< mplay_MediaPlayer_FI::SlideshowStateError >& /*error*/)
{
   _u8SlideshowState = ::MPlay_fi_types::T_e8_MPlayState__e8PBS_STOPPED;
   ETG_TRACE_USR4(("onSlideshowStateError"));
}


/**
 * onSlideshowStateStatus - function called when mediaplayer gets an status during the property SlideshowState updated.
 *
 * @param[in] proxy
 * @param[in] status
 * @return void
 */
void MediaPlayScreenHandling::onSlideshowStateStatus(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< mplay_MediaPlayer_FI::SlideshowStateStatus >& status)
{
   _u8SlideshowState = status->getE8State();
   ETG_TRACE_USR4(("onSlideshowStateStatus %d", _u8SlideshowState));
   switch (_u8SlideshowState)
   {
      case ::MPlay_fi_types::T_e8_MPlayState__e8PBS_STOPPED:
      {
         MediaDatabinding::getInstance().updatePhotoPlayerInfoPlayPauseActiveStatus(false, true);
         break;
      }
      case ::MPlay_fi_types::T_e8_MPlayState__e8PBS_PAUSED:
      {
         MediaDatabinding::getInstance().updatePhotoPlayerInfoPlayPauseActiveStatus(false, true);
         break;
      }
      case ::MPlay_fi_types::T_e8_MPlayState__e8PBS_PLAYING:
      {
         MediaDatabinding::getInstance().updatePhotoPlayerInfoPlayPauseActiveStatus(true, false);
         break;
      }

      default:
         break;
   }
}


/**
 * onSlideshowTimeError - function called when mediaplayer gets an error during the property SlideshowTime updated.
 *
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void MediaPlayScreenHandling::onSlideshowTimeError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< mplay_MediaPlayer_FI::SlideshowTimeError >& /*error*/)
{
   _u32SlideshowTimeStatus = 0;
   ETG_TRACE_USR4(("MediaPlayScreenHandling::onSlideshowTimeError"));
}


/**
 * onSlideshowTimeStatus - function called when mediaplayer gets an status during the property SlideshowTime updated.
 *
 * @param[in] proxy
 * @param[in] status
 * @return void
 */
void MediaPlayScreenHandling::onSlideshowTimeStatus(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< mplay_MediaPlayer_FI::SlideshowTimeStatus >& status)
{
   _u32SlideshowTimeStatus = status->getU32Time();
   ETG_TRACE_USR4(("onSlideshowTimeStatus = %d", _u32SlideshowTimeStatus));
}


/**
 * onNowShowingError - function called when mediaplayer gets an error during the property NowShowing updated.
 *
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void MediaPlayScreenHandling::onNowShowingError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< mplay_MediaPlayer_FI::NowShowingError >& /*error*/)
{
   _u8SlideshowState = ::MPlay_fi_types::T_e8_MPlayState__e8PBS_STOPPED;
   ETG_TRACE_USR4(("onNowShowingError"));
}


/**
 * onNowShowingStatus - function called when mediaplayer gets an status during the property NowShowing updated.
 *
 * @param[in] proxy
 * @param[in] status
 * @return void
 */
void MediaPlayScreenHandling::onNowShowingStatus(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< mplay_MediaPlayer_FI::NowShowingStatus >& status)
{
   ::MPlay_fi_types::T_MPlayMediaObject mediaObject =  status->getOMediaObject();
   uint32 Listhandel = status->getU32ListHandle();
   ::MPlay_fi_types::T_e8_MPlayNowPlayingState NowPlayingState = status->getE8NowPlayingState() ;
   uint32 position = status->getU32Position();
   uint32 Tag = status->getU32Tag();
   ::std::string nextfile =  status->getSNextFile();
   ::std::string filename = mediaObject.getSFilename();
   ::std::string MetaDataField1 = mediaObject.getSMetaDataField1();
   ::std::string MetaDataField2 = mediaObject.getSMetaDataField2();
   ::std::string MetaDataField3 = mediaObject.getSMetaDataField3();
   ::std::string MetaDataField4 = mediaObject.getSMetaDataField4();
   uint32 MetaDataTag1 = mediaObject.getU32MetaDataTag1();
   uint32 MetaDataTag2 = mediaObject.getU32MetaDataTag2();
   uint32 MetaDataTag3 = mediaObject.getU32MetaDataTag3();
   uint32 MetaDataTag4 = mediaObject.getU32MetaDataTag4();


//   ::std::string filePath = mediaObject.getSAlbumArt();
   std::string filePath = "/media/AAM_dev_sda1" + filename;

   ETG_TRACE_USR4(("onNowShowingStatus - _u8SlideshowState = %d, _u32SlideshowTimeStatus = %d", _u8SlideshowState, _u32SlideshowTimeStatus));
   ETG_TRACE_USR4(("onNowShowingStatus - Listhandel = %d, NowPlayingState = %d, position = %d, Tag = %d", Listhandel, NowPlayingState, position, Tag));
   ETG_TRACE_USR4(("onNowShowingStatus - File name = %s", filename.c_str()));
   ETG_TRACE_USR4(("onNowShowingStatus - File sAlbumArt = %s", mediaObject.getSAlbumArt().c_str()));
   ETG_TRACE_USR4(("onNowShowingStatus - Next name = %s", nextfile.c_str()));
   ETG_TRACE_USR4(("onNowShowingStatus - mediaObject.getSMetaDataField1() = %s", MetaDataField1.c_str()));
   ETG_TRACE_USR4(("onNowShowingStatus - mediaObject.getSMetaDataField2() = %s", MetaDataField2.c_str()));
   ETG_TRACE_USR4(("onNowShowingStatus - mediaObject.getSMetaDataField3() = %s", MetaDataField3.c_str()));
   ETG_TRACE_USR4(("onNowShowingStatus - mediaObject.getSMetaDataField4() = %s", MetaDataField4.c_str()));
   ETG_TRACE_USR4(("onNowShowingStatus - MetaDataTag1 = %d, MetaDataTag2 = %d, MetaDataTag3 = %d, MetaDataTag4 = %d", MetaDataTag1, MetaDataTag2, MetaDataTag3, MetaDataTag4));

   MediaDatabinding::getInstance().updatePhotoPlayerInfoPhotoContent(filePath, true);

   POST_MSG((COURIER_MESSAGE_NEW(SlideShowPositionPhotoPlayerChangedUpdMsg)(LIST_ID_COVER_PLAYER_IMAGE, position)));
}


#endif
}//end of namespace Core
}//end of namespace App
