/**
* @file MetaData.cpp
* @author RBEI/ECV-AIVI_MediaTeam
* @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
* @addtogroup AppHMI_Media
*/

#include "Core/PlayScreen/MetaData.h"
using namespace MPlay_fi_types;
namespace App {
namespace Core {

#define NUMBER_OF_CATOGERIES 14

enum MetaDataInfo
{
   GENRE = 0,
   ARTIST,
   ALBUM,
   SONG,
   COMPOSER,
   BOOK_TITLE,
   CHAPTER,
   PODCAST_NAME,
   PODCAST_EPISODE,
   PLAYLIST_NAME,
   INFO_UNDEFINED
};


MetaDataFields fields[NUMBER_OF_CATOGERIES][INFO_UNDEFINED] =
   //Genre, Artist,  Album,   Song,    Composer, B.Title, Chapter, P.Name, PEpisode, Playlist Name
{
   {UNKNOWN, FIELD1, FIELD2, FIELD3, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN},                       //T_e8_MPlayCategoryType__e8CTY_NONE = 0u,
   {FIELD1,  UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN},                       //T_e8_MPlayCategoryType__e8CTY_GENRE = 1u,
   {UNKNOWN, FIELD2,  UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN},                       //T_e8_MPlayCategoryType__e8CTY_ARTIST = 2u,
   {FIELD1,  FIELD2,  FIELD3,  UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN},                       //T_e8_MPlayCategoryType__e8CTY_ALBUM = 3u,
   {FIELD1,  FIELD2,  FIELD3,  FIELD4,  UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN},                       //T_e8_MPlayCategoryType__e8CTY_SONG = 4u,
   {UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, FIELD1,  UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN},                       //T_e8_MPlayCategoryType__e8CTY_COMPOSER = 5u,
   {UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN},                       //T_e8_MPlayCategoryType__e8CTY_AUTHOR = 6u,
   {UNKNOWN, FIELD1, UNKNOWN, UNKNOWN, UNKNOWN, FIELD2,  FIELD3, UNKNOWN, UNKNOWN, UNKNOWN},                         //T_e8_MPlayCategoryType__e8CTY_TITLE = 7u,
   {UNKNOWN, FIELD1, UNKNOWN, UNKNOWN, UNKNOWN, FIELD2,  FIELD3,  UNKNOWN, UNKNOWN, UNKNOWN},                        //T_e8_MPlayCategoryType__e8CTY_CHAPTER = 8u,
   {UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, FIELD1,  UNKNOWN, UNKNOWN},                       //T_e8_MPlayCategoryType__e8CTY_NAME = 9u,
   {UNKNOWN, FIELD3, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, FIELD2,  FIELD1,  UNKNOWN},                       //T_e8_MPlayCategoryType__e8CTY_EPISODE = 10u,
   {UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, FIELD1},                        //T_e8_MPlayCategoryType__e8CTY_PLAYLIST = 11u,
   {UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN},                       //T_e8_MPlayCategoryType__e8CTY_PLAYLIST_INTERNAL = 12u,
   {UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN},                       //T_e8_MPlayCategoryType__e8CTY_IMAGE = 13u
};


/**
* MetaData - Constructor, initializes class members
*
* @param[in] T_MPlayMediaObject song details
* @parm[out] none
* @return none
*/
MetaData::MetaData()
{
   _categoryType = T_e8_MPlayCategoryType__e8CTY_NONE;
   for (int field = 0; field < FIELDS_UNDEFINED; ++field)
   {
      _metaData[field] = "";
   }
   _unknownString = "";
   _tag = 0;
   _deviceType = MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_UNKNOWN;
}


/**
* MetaData - Parameter Constructor, initializes class members
*
* @param[in] T_MPlayMediaObject song details
* @parm[out] none
* @return none
*/
MetaData::MetaData(const T_MPlayMediaObject& oMediaObject)
{
   setMediaObject(oMediaObject);
}


/**
* ~MetaData - destructor
*
* @param[in] none
* @parm[out] none
* @return none
*/
MetaData::~MetaData()
{
}


/**
* setMediaObject - The setter function
*
* @param[in] T_MPlayMediaObject song details
* @parm[out] none
* @return none
*/
void MetaData::setMediaObject(const T_MPlayMediaObject& oMediaObject)
{
   _categoryType = oMediaObject.getE8CategoryType();
   _deviceType = oMediaObject.getE8DeviceType();
   _tag = oMediaObject.getU32Tag();

   if (_categoryType < T_e8_MPlayCategoryType__e8CTY_NONE || _categoryType > NUMBER_OF_CATOGERIES)
   {
      _categoryType = T_e8_MPlayCategoryType__e8CTY_NONE;
   }
   _metaData[FIELD1] = oMediaObject.getSMetaDataField1();
   _metaData[FIELD2] = oMediaObject.getSMetaDataField2();
   _metaData[FIELD3] = oMediaObject.getSMetaDataField3();
   _metaData[FIELD4] = oMediaObject.getSMetaDataField4();
   _metaData[UNKNOWN] = _unknownString;
}


/**
* setUnknownString - sets the string which needs to be returned on unknown configuration, could be updated based on language
*
* @param[in] string - language dependent Unknown strings
* @parm[out] none
* @return none
*/
void MetaData::setUnknownString(const std::string string)
{
   _unknownString = string;
   _metaData[UNKNOWN] = _unknownString;
}


/**
* getComposerName - Provide ComposerName
*
* @param[in] none
* @parm[out] none
* @return std::string ComposerName
*/
std::string MetaData::getComposerName()
{
   return _metaData[fields[_categoryType][COMPOSER]];
}


/**
* getBookTitle - Provide BookTitle
*
* @param[in] none
* @parm[out] none
* @return std::string BookTitle
*/
std::string MetaData::getBookTitle()
{
   return _metaData[fields[_categoryType][BOOK_TITLE]];
}


/**
* getChapterTitle - Provide ChapterTitle
*
* @param[in] none
* @parm[out] none
* @return std::string ChapterTitle
*/
std::string MetaData::getChapterTitle()
{
   return _metaData[fields[_categoryType][CHAPTER]];
}


/**
* getPosdcastEpisode - Provide PosdcastEpisode
*
* @param[in] none
* @parm[out] none
* @return std::string PosdcastEpisode
*/
std::string MetaData::getPodcastName()
{
   return _metaData[fields[_categoryType][PODCAST_NAME]];
}


/**
* getPosdcastEpisode - Provide PosdcastEpisode
*
* @param[in] none
* @parm[out] none
* @return std::string PosdcastEpisode
*/
std::string MetaData::getPodcastEpisode()
{
   return _metaData[fields[_categoryType][PODCAST_EPISODE]];
}


/**
* getPlayListName - Provide PlayListName
*
* @param[in] none
* @parm[out] none
* @return std::string PlayListName
*/
std::string MetaData::getPlayListName()
{
   return _metaData[fields[_categoryType][PLAYLIST_NAME]];
}


/**
* getGener - Provides Gener
*
* @param[in] none
* @parm[out] none
* @return std::string Gener
*/
std::string MetaData::getGenre()
{
   return _metaData[fields[_categoryType][GENRE]];
}


/**
* getArtistName - Provide ArtistName
*
* @param[in] none
* @parm[out] none
* @return std::string ArtistName
*/
std::string MetaData::getArtistName()
{
   return _metaData[fields[_categoryType][ARTIST]];
}


/**
* getAlbumName - Provide AlbumName
*
* @param[in] none
* @parm[out] none
* @return std::string AlbumName
*/
std::string MetaData::getAlbumName()
{
   return _metaData[fields[_categoryType][ALBUM]];
}


/**
* getSongTitle - Provide SongTitle
*
* @param[in] none
* @parm[out] none
* @return std::string SongTitle
*/
std::string MetaData::getSongTitle()
{
   return _metaData[fields[_categoryType][SONG]];
}


/**
* getDeviceType - Provide device type
*
* @param[in] none
* @parm[out] none
* @return uint32 DeviceType
*/

unsigned int MetaData::getDeviceType()
{
   return _deviceType;
}


/**
* getCategoryType - Provide Category type
*
* @param[in] none
* @parm[out] none
* @return uint32 CategoryType
*/
::MPlay_fi_types::T_e8_MPlayCategoryType MetaData::getCategoryType()
{
   return _categoryType;
}


/**
* getTagValue - Provide tag
*
* @param[in] none
* @parm[out] none
* @return uint32 tag
*/
unsigned int MetaData::getTagValue()
{
   return _tag;
}


} /* namespace Core */
} /* namespace App */
