/*
 * HmiDataHandlerExt.h
 *
 *  Created on: Nov 18, 2015
 *  Author: A-IVI Media Team
 */

#ifndef HMIDATAHANDLEREXT_H_
#define HMIDATAHANDLEREXT_H_
#define NS_HMI_DATA_SERVICE ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData


#include "Common/HmiDataHandler/HmiDataHandler.h"
#include "bosch/cm/ai/nissan/hmi/hmidataservice/HmiDataProxy.h"
#include "AppHmi_MediaMessages.h"
#include "hmi_trace_if.h"
#include "AppBase/ServiceAvailableIF.h"

class HmiDataHandlerExt: public HmiDataCommonHandler::ISwipeDirectionStatusClientPropertyCB,
   public HmiDataCommonHandler::HmiDataHandler,
   public NS_HMI_DATA_SERVICE::SetApplicationModeCallbackIF,
   public NS_HMI_DATA_SERVICE::VisibleApplicationModeCallbackIF,
   public StartupSync::PropertyRegistrationIF,
   public hmibase::ServiceAvailableIF
{
   public:
      HmiDataHandlerExt();
      virtual ~HmiDataHandlerExt();
      virtual void onSetApplicationModeError(const ::boost::shared_ptr< NS_HMI_DATA_SERVICE::HmiDataProxy >& /*proxy*/, const ::boost::shared_ptr< NS_HMI_DATA_SERVICE::SetApplicationModeError >& /*error*/);
      virtual void onSetApplicationModeResponse(const ::boost::shared_ptr< NS_HMI_DATA_SERVICE::HmiDataProxy >& /*proxy*/, const ::boost::shared_ptr< NS_HMI_DATA_SERVICE::SetApplicationModeResponse >& /*response*/);
      virtual void handleSwipeDirectionPropertyUpdate(const uint8 /*swipeDirectionType*/);
      virtual void onVisibleApplicationModeError(const ::boost::shared_ptr< NS_HMI_DATA_SERVICE::HmiDataProxy >& proxy, const ::boost::shared_ptr< NS_HMI_DATA_SERVICE::VisibleApplicationModeError >& error);
      virtual void onVisibleApplicationModeUpdate(const ::boost::shared_ptr< NS_HMI_DATA_SERVICE::HmiDataProxy >& proxy, const ::boost::shared_ptr< NS_HMI_DATA_SERVICE::VisibleApplicationModeUpdate >& update);
      void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);
      void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);

      uint8 getCurrentVisibleAppMode();
      void getCurrentAudioSourceUpdate(int32);
   protected:
      bool onCourierMessage(const SetApplicationModeReqMsg& oMsg);

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_VIEW_COMP)
      ON_COURIER_MESSAGE(SetApplicationModeReqMsg)
      COURIER_MSG_MAP_END();

      /** Instance of HmiDataHandler */
      HmiDataCommonHandler::HmiDataHandler* _hmiDataHandler;
      int _mSrcId;
      int _mAppModeId;
};


#undef NS_HMI_DATA_SERVICE
#endif /* HMIDATAHANDLEREXT_H_ */
