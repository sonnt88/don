/**
 *  @file   MediaDatabindingCommon.cpp
 *  @author ECV - RN_A_IVI
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */
#include "hall_std_if.h"
#include "MediaDatabindingCommon.h"
#include "Core/Utils/MediaProxyUtility.h"
#include <CgiExtensions/ImageLoader.h>

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_APPHMI_MEDIA_HALL_DATA_BINDING
#include "trcGenProj/Header/MediaDatabindingCommon.cpp.trc.h"

#endif


namespace App {
namespace Core {

static const unsigned int MEDIA_BASE_LIST_ID = 4000;
static const float LIST_SIZE_WIDTH = 800.0f;
/**
 * @Destructor
 */
MediaDatabindingCommon::~MediaDatabindingCommon()
{
   ETG_TRACE_USR4(("clMedia_Databindinghandler Destructor"));
}


/**
 * @Constructor
 */
MediaDatabindingCommon::MediaDatabindingCommon()
   : _currentSourceType(SOURCE_NOT_DEF)
   , _indexState(1)
   , _indexingPercent(0)
   , _currentFolderPath("/")
   , _deviceTag(0)
   , _audioSource(SRC_INVALID)
   , _activeListType(0)
   , _numOfFolders(0)
   , _numberOfFiles(0)
   , _activeplayingitemtag(0)
   , _coverImage()
   , _sizeOfAlbumArtImage(0)
   , _btdeviceConnectedStatus(false)
   , _isVolumeAvailable(true)
   , _bIsSpiDIPOActive(false)
   , _bIsSpiAAPActive(false)
   , _currentTrackInfo("")

{
   ETG_TRACE_USR4(("clMedia_Databindinghandler Constructor"));
   ETG_I_REGISTER_FILE();
   _activeApplicationID = APPID_APPHMI_UNKNOWN;

   //_coverImage = ImageWidgetBackEndInterface::pCreate(); 	// Creating Image Shared Object
}


/**
 * UpdateMediaMetadataInfo - Function to update meta data to GUI
 * @param[in] ArtistName, AlbumName, TitleName
 * @parm[out] None
 * @return void
 */
void MediaDatabindingCommon::updateMediaMetadataInfo(std::string strArtist, std::string strAlbum, std::string strTitle)
{
   ETG_TRACE_USR4(("Artist name:%s", strArtist.c_str()));
   ETG_TRACE_USR4(("Album Name: %s ", strAlbum.c_str()));
   ETG_TRACE_USR4(("Song Name:%s", strTitle.c_str()));

   (*_mediaMetaData).mArtistName = strArtist.c_str();
   (*_mediaMetaData).mAlbumName = strAlbum.c_str();
   (*_mediaMetaData).mSongTitle = strTitle.c_str();

   _mediaMetaData.MarkItemModified(ItemKey::MediaMetaData::ArtistNameItem);
   _mediaMetaData.MarkItemModified(ItemKey::MediaMetaData::AlbumNameItem);
   _mediaMetaData.MarkItemModified(ItemKey::MediaMetaData::SongTitleItem);

   if (_mediaMetaData.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("m_MediaMetaData : Updation failed..!!!"));
   }
}


/**
 * UpdateBTDeviceInfo - Function to update BT device name  to GUI
 * @param[in] string value,
 * @parm[out] None
 * @return void
 */
void MediaDatabindingCommon::updateBTDeviceInfo(std::string strDeviceName)
{
   ETG_TRACE_USR4(("Device Name:%s", strDeviceName.c_str()));

   (*_btDeviceData).mBTDeviceName = strDeviceName.c_str();

   _btDeviceData.MarkItemModified(ItemKey::BTAudioDeviceData::BTDeviceNameItem);
   if (_btDeviceData.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("UpdateBTDeviceInfo : Updation failed..!!!"));
   }
}


/**
 * UpdateBTIconState - Function to update Play,Pause Icon active status to GUI in BLUETOOTH MAIN screen.
 * @param[in] bool,bool
 * @parm[out] None
 * @return void
 */
void MediaDatabindingCommon::updateBTIconState(bool bisPlayActive, bool bisPauseActive)
{
   ETG_TRACE_USR4(("Icon status :%d , %d", bisPlayActive , bisPauseActive));
   (*_btDeviceData).misPlayActive = bisPlayActive;
   (*_btDeviceData).misPauseActive = bisPauseActive;
   _btDeviceData.MarkItemModified(ItemKey::BTAudioDeviceData::isPlayActiveItem);
   _btDeviceData.MarkItemModified(ItemKey::BTAudioDeviceData::isPauseActiveItem);
   ETG_TRACE_USR4(("item modified"));
   if (_btDeviceData.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("UpdateBTIconState : Updation failed..!!!"));
   }
}


/**
 * UpdatePlaytimeInfo - Function to update iPod meta data to GUI
 * @param[in] Elaspetime, TotalTime
 * @parm[out] None
 * @return void
 */
void MediaDatabindingCommon::updatePlaytimeInfo(std::string ElapseTime, std::string TotalTime, uint32 Progressbar_elapsetime, uint32 Progressbar_totaltime)
{
   ETG_TRACE_USR4(("UpdatePlaytimeInfo:Elaspetime %s", ElapseTime.c_str()));
   _totalPlayTime = TotalTime;													//_totalPlayTime is used for TML scripts

   (*_mediaMetaData).mElapseTime = ElapseTime.c_str();
   (*_mediaMetaData).mTotalTime  = TotalTime.c_str();
   (*_mediaMetaData).mProgressbar_ElapseTime = Progressbar_elapsetime;
   (*_mediaMetaData).mProgressbar_TotalTime = Progressbar_totaltime;
   _mediaMetaData.MarkAllItemsModified();

   if (_mediaMetaData.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("UpdatePlaytimeInfo failed..!!!"));
   }
}


/**
 * UpdateRepeatRandomStatus - Function to update Playmode status
 * @param[in] string playmode
 * @parm[out] None
 * @return void
 */
void MediaDatabindingCommon::updateRepeatRandomStatus(Candera::String& sRepeatRandomMode, bool sRandomActiveMode, bool sRepeatActiveMode, Candera::String& sRandomText, Candera::String& sRepeatText)
{
   ETG_TRACE_USR4(("UpdateRandomStatus:RepeatRandomMode %s", sRepeatRandomMode.GetCString()));
   (*_randomRepeatStatus).mRandomRepeatText = sRepeatRandomMode;
   (*_randomRepeatStatus).mRandomActiveMode = sRandomActiveMode;
   (*_randomRepeatStatus).mRepeatActiveMode = sRepeatActiveMode;
   (*_randomRepeatStatus).mRepeatText = sRepeatText;
   (*_randomRepeatStatus).mRandomText = sRandomText;
   _randomRepeatStatus.MarkAllItemsModified();
   if (_randomRepeatStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("UpdateRepeatRandomStatus failed..!!!"));
   }
}


/**
 * clearPlayModeStatus - clear play mode status
 * @param[in] none
 * @parm[out] None
 * @return void
 */
void MediaDatabindingCommon::clearPlayModeStatus()
{
   ETG_TRACE_USR4(("clearPlayModeStatus called"));
   (*_randomRepeatStatus).mRandomRepeatText = "";
   (*_randomRepeatStatus).mRandomActiveMode = false;
   (*_randomRepeatStatus).mRepeatActiveMode = false;
   (*_randomRepeatStatus).mRepeatText = "";
   (*_randomRepeatStatus).mRandomText = "";
   _randomRepeatStatus.MarkAllItemsModified();
   if (_randomRepeatStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("UpdateRepeatRandomStatus failed..!!!"));
   }
}


/**
 *  updateAlbumArt - Function to set source image pointer and update Cover Art
 *  @param [in] vImageData - image data
 *  @param [in] u32ImageSize - Image Size
 *  @return void
 */
void MediaDatabindingCommon::updateAlbumArt(const ::std::vector< uint8 > u8ImageData, uint32 u32ImageDataSize , bool _isAlbumArtEnable)
{
   ETG_TRACE_USR4(("clMedia_Databindinghandler::UpdateAlbumArt"));

   // Preparing Message data
   //ETG_TRACE_USR4(("UpdateAlbumArt : mEnableStatus %d", (*_albumArtData).mEnableStatus));
   updateAlbumArtDataBinding(u8ImageData.data(), u32ImageDataSize , _isAlbumArtEnable);
}


/**
 *  updateAlbumArtDefault - Function to update default album art
 *  @return void
 */
void MediaDatabindingCommon::updateAlbumArtDefault(bool _isAlbumArtEnable)
{
   ETG_TRACE_USR4(("clMedia_Databindinghandler::UpdateAlbumArtDefault"));
   // (*_albumArtData).mAlbumArtImage = ImageLoader::getAssetBitmapImage(DEFAULTALBUMARTIMAGE);
   (*_albumArtData).mAlbumArtVisibility = false;
   (*_albumArtData).mDefaultAlbumArtVisibility = _isAlbumArtEnable;
   _albumArtData.MarkItemModified(ItemKey::AlbumArtData::AlbumArtVisibilityItem);
   _albumArtData.MarkItemModified(ItemKey::AlbumArtData::DefaultAlbumArtVisibilityItem);
   if (_albumArtData.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("UpdateAlbumArt : Updation failed..!!!"));
   }
}


/**
 *  updateAlbumArtDataBinding - Function to update Album Art
 *  @param [in] pImagePtr - Image data
 *  @param [in] u32ImageSize - Image Size
 *  @return void
 */
void MediaDatabindingCommon::updateAlbumArtDataBinding(const void* data, uint32 size , bool _isAlbumArtEnable)
{
   Candera::Bitmap* bmp = ImageLoader::loadBitmapData(data, size);
   _sizeOfAlbumArtImage = size;
   ETG_TRACE_USR4(("clMedia_Databindinghandler::updateAlbumArtDataBinding"));
   if (bmp != NULL)
   {
      (*_albumArtData).mDefaultAlbumArtVisibility = false;
      (*_albumArtData).mAlbumArtVisibility = _isAlbumArtEnable;
      (*_albumArtData).mAlbumArtImage = ImageLoader::createImage(bmp);
      _albumArtData.MarkItemModified(ItemKey::AlbumArtData::DefaultAlbumArtVisibilityItem);
      _albumArtData.MarkItemModified(ItemKey::AlbumArtData::AlbumArtVisibilityItem);
      _albumArtData.MarkItemModified(ItemKey::AlbumArtData::AlbumArtImageItem);
   }
   if (_albumArtData.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("UpdateAlbumArt : Updation failed..!!!"));
   }
}


/**
 *  updateBrowseListStartIndex - Function to update start index to vertical list
 *  @param [in] startIdx
 *  @return void
 */
void MediaDatabindingCommon::updateBrowseListStartIndex(uint32 startIdx)
{
   ETG_TRACE_USR4(("MediaDatabinding::UpdateBrowseListStartIndex:%d", startIdx));

   (*_usbBrowseListScreen).mStartIndex = startIdx;
   _usbBrowseListScreen.MarkItemModified(ItemKey::USBBrowseListScreen::StartIndexItem);

   if (_usbBrowseListScreen.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("UpdateUSBBrowseListScreen : Updation failed..!!!"));
   }
}


/**
 *  updateScrollBarSwitchIndex - Function to update the switch widget's index to display scroll bar with/without scroll bar
 *  @param [in] index
 *  @return void
 */
void MediaDatabindingCommon::updateScrollBarSwitchIndex(uint32 sIndex)
{
   ETG_TRACE_USR4(("MediaDatabinding::UpdateBrowseListStartIndex:%d", sIndex));

   (*_usbBrowseListScreen).mSwitchBtwCompositeIndex = sIndex;
   _usbBrowseListScreen.MarkItemModified(ItemKey::USBBrowseListScreen::SwitchBtwCompositeIndexItem);

   if (_usbBrowseListScreen.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("UpdateUSBBrowseListScreen : Updation failed..!!!"));
   }
}


/**
 * clearMediaMetaDataInfo message - called when user wants skip a track in USB.
 *
 * @Description :This method will clear the USB metadata info with empty string and reset the elapsed time to 0:00.
 * @param[in] no param
 * @return void
 */
void MediaDatabindingCommon::clearMediaMetaDataInfo()
{
   (*_mediaMetaData).mArtistName = "";
   (*_mediaMetaData).mAlbumName = "";
   (*_mediaMetaData).mSongTitle = "";
   (*_mediaMetaData).mElapseTime = "";
   ETG_TRACE_USR4(("clearMediaMetaDataInfo : called..!!!"));
   _mediaMetaData.MarkAllItemsModified();

   if (_mediaMetaData.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("clearMediaMetaDataInfo : Updation failed..!!!"));
   }
}//end of clearMediaMetaDataInfo function


/**
 * updateCurrentMediaSource
 *
 * @Description :This method will get call when there is a update in active source
 * @param[in] int
 * @return void
 */
void MediaDatabindingCommon::updateCurrentMediaSource(uint32 CurrentMediaSrc)
{
   _currentSourceType = CurrentMediaSrc;
   updateGadgetScreenButtonsVisibilty(_currentSourceType);
}


/**
 * updateActiveApplicationStatus.
 * @Description :This method will get call when there is a update in active Application
 * @param[in] int
 * @return void
 */
void MediaDatabindingCommon::updateActiveApplicationStatus(bool AppStatus)
{
   (*_ActiveApplicationState).mIsActiveApplication = AppStatus;
   _ActiveApplicationState.MarkAllItemsModified();

   if (_ActiveApplicationState.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_ActiveApplicationState : Updation failed..!!!"));
   }
}


/**
 * getActiveApplicationID.
 * @Description :This method to return whether media is active Application
 * @param[in] no param
 * @return int
 */
uint32 MediaDatabindingCommon::getActiveApplicationStatus()
{
   return (*_ActiveApplicationState).mIsActiveApplication ;
}


#ifdef QUICKSEARCH_SUPPORT
/**
 * UpdateQuickSearchInfo message - called when Character needs to be updated for Quicksearch popup.
 *
 * @Description :This method will update the character whichever it gets as an input in Quciksearch popup text except for 0 for which it will update as 0-9
 * @param[in] String which needs to be updated
 * @return void
 */
void MediaDatabindingCommon::updateQuickSearchInfo(std::string CurrentString)
{
   std::string str("0");

   if (!str.compare(CurrentString.c_str()))
   {
      (*_quickSearch).mCurrentString = "0-9";
      ETG_TRACE_USR4(("UpdateQuickSearchInfo : if str is 0 mCurrentString = %s", (*_quickSearch).mCurrentString.GetCString()));
   }
   else
   {
      (*_quickSearch).mCurrentString = CurrentString.c_str();
      ETG_TRACE_USR4(("UpdateQuickSearchInfo : if str is not 0 CurrentString.c_str() = %s", (*_quickSearch).mCurrentString.GetCString()));
   }

   ETG_TRACE_USR4(("UpdateQuickSearchInfo : CurrentString.c_str() = %s", CurrentString.c_str()));

   _quickSearch.MarkAllItemsModified();

   if (_quickSearch.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("UpdateQuickSearchInfo : Updation failed..!!!"));
   }
}


#endif

void MediaDatabindingCommon::updateAuxScreenInfo(std::string strInfo)
{
   (*_defaultAuxInfo).mAuxInfo = strInfo.c_str();
   _defaultAuxInfo.MarkItemModified(ItemKey::MediaAuxScreenInfo::AuxInfoItem);
   if (_defaultAuxInfo.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateAuxScreenInfo : Updation failed..!!!"));
   }
}


/**
 * UpdateTAStatusInfo message - called when TAStatus is changed to active/inactive
 *
 * @Description :This method will update the TAstatus info in the TA Button.
 * @param[in] Bool which decides the TA button as in Active or InActive state.
 * @return void
 */
void MediaDatabindingCommon::updateTAStatusInfo(bool Status)
{
   (*_taStatus).mIsActive = Status;

   _taStatus.MarkAllItemsModified();

   if (_taStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("UpdateTAStatusInfo : Updation failed..!!!"));
   }
}


void MediaDatabindingCommon::updateConnectedDevice(::std::vector< SourceDetailInfo > DeviceInfo)
{
   _connectedDevice = DeviceInfo;
}


void MediaDatabindingCommon::updateActiveDeviceTag(uint32 DeviceTag)
{
   _deviceTag = DeviceTag;
}


void MediaDatabindingCommon::updateAudioSource(uint32 SourceType)
{
   _audioSource = SourceType;
}


void MediaDatabindingCommon::updateActiveListType(uint32 ListType)
{
   _activeListType = ListType;
}


void MediaDatabindingCommon::updateTotalNumberOfFilesAndFolders(uint32 TotalNumFiles, uint32 TotalNumFolders)
{
   _numOfFolders = TotalNumFolders;
   _numberOfFiles = TotalNumFiles;
}


void MediaDatabindingCommon::updateActivePlayingItemTag(uint32 Tag)
{
   _activeplayingitemtag = Tag;
}


/**
 * updateIndexingInfo message - called when indexing status is getting update.
 *
 * @Description :This method will update the indexing info in the status line.
 * @param[in] uint8 indexState - which tells the index state.
 * @param[in] uint8 indexingPercent - which tells the percentage of indexing for the current device.
 * @return void
 */
void MediaDatabindingCommon::updateIndexingInfo(uint8 indexState, uint8 indexingPercentage)
{
   //TODO update the indexing info once the status line implementation is done.remove the private data member also.
   _indexState = indexState;
   _indexingPercent = indexingPercentage;
}


/**
 * updateFolderPath message - called when indexing status is getting update.
 *
 * @Description :This method will update the current folder path of the current song
 * @param[in] string indexState - which tell the current folder path.
 * @return void
 */
void MediaDatabindingCommon::updateFolderPath(std::string currentFolderPath)
{
   /*TODO update the current folder info .
    *remove this once the status line implementation is done.remove the private data member also.*/

   _currentFolderPath = currentFolderPath;
}


/*
 * showTrackInfo function prints the track info and this is used only for TML scripts
 *
 * @param[in] none
 * @return void
 */
void MediaDatabindingCommon::showTrackInfo()
{
   ETG_TRACE_USR4(("Artist  Name: %s" , (*_mediaMetaData).mArtistName.GetCString()));
   ETG_TRACE_USR4(("Size of Artist  Name: %d" , strlen((*_mediaMetaData).mArtistName.GetCString())));
   ETG_TRACE_USR4(("Album   Name: %s" , (*_mediaMetaData).mAlbumName.GetCString()));
   ETG_TRACE_USR4(("Size of Album   Name: %d" , strlen((*_mediaMetaData).mAlbumName.GetCString())));
   ETG_TRACE_USR4(("Song    Name: %s" , (*_mediaMetaData).mSongTitle.GetCString()));
   ETG_TRACE_USR4(("Size of Song    Name: %d" , strlen((*_mediaMetaData).mSongTitle.GetCString())));
   ETG_TRACE_USR4(("Elapsed Time: %s" , (*_mediaMetaData).mElapseTime.GetCString()));
   ETG_TRACE_USR4(("Total   Time: %s" , _totalPlayTime.c_str()));
   ETG_TRACE_USR4(("Current playing track : %s" , _currentTrackInfo.c_str()));
}


/*
 * showPlayModeStatus function prints the play mode info and this is used only for TML scripts
 *
 * @param[in] none
 * @description :This function will check if there is a null string then display it is as Normal mode else display the repeat/random mode.
 * @return void
 */
void MediaDatabindingCommon::showPlayModeStatus()
{
   if ((*_randomRepeatStatus).mRandomRepeatText.IsEmpty())
   {
      ETG_TRACE_USR4(("RandomRepeat Text: Normal mode"));				//This case is added for TML testing.
   }
   else
   {
      ETG_TRACE_USR4(("RandomRepeat Text: %s" , (*_randomRepeatStatus).mRandomRepeatText.GetCString()));
   }
}


/*
 * showCurrentListTypeOfMetadataBrowse function prints the current list type in metadata browse info and this is used only for TML scripts
 *
 * @param[in] none
 * @return void
 */
void MediaDatabindingCommon::showCurrentListTypeOfMetadataBrowse()
{
   uint32 metaDataListType = _activeListType % MEDIA_BASE_LIST_ID;
   ETG_TRACE_USR4(("List type in Metadata browse: %d" , metaDataListType));
}


/*
 * showIndexingInfo function prints the current list type in metadata browse info and this is used only for TML scripts
 *
 * @param[in] none
 * @return void
 */
void MediaDatabindingCommon::showIndexingInfo()
{
   MediaProxyUtility::getInstance().getCurrentDeviceIndexingValues(_indexState, _indexingPercent);
   ETG_TRACE_USR4(("Index state      : %d" , _indexState));
   ETG_TRACE_USR4(("Index percentage : %d" , _indexingPercent));
}


/*
 * getCurrentActiveMediaDevice function prints the current active media device and this is used only for TML scripts
 *
 * @param[in] none
 * @return void
 */
uint32 MediaDatabindingCommon::getCurrentActiveMediaDevice()
{
   return _currentSourceType;
}


/*
 * getCurrentFolderPath function prints the current folder path in folder browse .
 *
 * @param[in] none
 * @return void
 */
std::string MediaDatabindingCommon::getCurrentFolderPath()
{
   /* TODO update the current selected folder path info .
    * This can be removed once the status line implementation is done.
   remove the private data member also. */

   return _currentFolderPath;
}


void MediaDatabindingCommon::updateBTDeviceConnectedStatus(bool status)
{
   _btdeviceConnectedStatus = status;
}


void MediaDatabindingCommon::showConnectedDevices()
{
   ::std::vector< SourceDetailInfo >::iterator itr = _connectedDevice.begin();
   uint32 NoofConnectedDevice = 1;
   while (itr != _connectedDevice.end())
   {
      if (itr->subSourceID == -1)
      {
         switch (itr->sourceID)
         {
            case SRC_PHONE_BTAUDIO:
            case SRC_MEDIA_CDDA:
            case SRC_MEDIA_AUX:
               ETG_TRACE_USR4(("Connected Device %d   : %d \n", NoofConnectedDevice++, itr->sourceID));
               break;
            default:
            {
               break;
            }
         }//end of switch
      }
      else if (itr->subSourceID > 0 && itr->sourceID == SRC_PHONE_BTAUDIO)
      {
         ETG_TRACE_USR4(("Connected Device %d   : %d \n", NoofConnectedDevice++, itr->sourceID));
      }
      else if (itr->subSourceID > 0)
      {
         ETG_TRACE_USR4(("Connected Device %d   : %d \n", NoofConnectedDevice++, itr->sourceType));
      }
      itr++;
   }
}


void MediaDatabindingCommon::showBTDeviceConnectedStatus()
{
   ETG_TRACE_USR4(("BT Device Connected Status: %d" , _btdeviceConnectedStatus));
}


void MediaDatabindingCommon::showAudioSource()
{
   ETG_TRACE_USR4(("Active Audio Source: %d" , _audioSource));
}


uint32 MediaDatabindingCommon::GetCurrentAudioSource()
{
   return _audioSource;
}


void MediaDatabindingCommon::showBTDeviceName()
{
   ETG_TRACE_USR4(("BT Device Name:  %s" , (*_btDeviceData).mBTDeviceName.GetCString()));
}


void MediaDatabindingCommon::showActiveDeviceTag()
{
   ETG_TRACE_USR4(("Device Tag: %d" , _deviceTag));
}


void MediaDatabindingCommon::showTotalNumberOfFilesAndFolders()
{
   ETG_TRACE_USR4(("Number of Folders: %d" , _numOfFolders));
   ETG_TRACE_USR4(("Number of Files: %d" , _numberOfFiles));
}


void MediaDatabindingCommon::showActivePlayingItemTag()
{
   ETG_TRACE_USR4(("Playing Song Tag: %d" , _activeplayingitemtag));
}


// end of functions that supports only for TML

uint32 MediaDatabindingCommon::getActiveDeviceTag()
{
   return _deviceTag;
}


void MediaDatabindingCommon::showAlbumArtInfo()
{
   ETG_TRACE_USR4(("Status of AlbumArt Default Image: %d" , (*_albumArtData).mDefaultAlbumArtVisibility));
   ETG_TRACE_USR4(("Status of AlbumArt Image: %d" , (*_albumArtData).mAlbumArtVisibility));
   ETG_TRACE_USR4(("Size of the Image: %d" , _sizeOfAlbumArtImage));
}


bool MediaDatabindingCommon::getVolumeAvailablity()
{
#ifdef VOLUME_ZERO_SUPPORT
   return _isVolumeAvailable;
#else
   return true;
#endif
}


/*
 * updateIsMediaActive function is used to update the data binding item IsMediaActive of the binding source MediaSourceAvailabilityDataBindingSource
 *	This function will be called whenever media source is active or inactive
 * @param[in] bool
 * @return void
 */
void MediaDatabindingCommon::updateIsMediaActive(bool activeState)
{
   ETG_TRACE_USR4(("updateIsMediaActive =%d", activeState));
   (*_MediaApplicationActiveState).mIsMediaActive = activeState;
   _MediaApplicationActiveState.MarkAllItemsModified();
   bool checkDBupdate = _MediaApplicationActiveState.SendUpdate(true);
   if (checkDBupdate == false)
   {
      ETG_TRACE_USR4(("IsMediaActive : Updation failed..!!!"));
   }
}


/**
 * setSpiDIPOSourceAvailability
 * @param[in] :Status :True Available
 * 					False Not Available
 * @parm[out] None
 * @return void
 */
void MediaDatabindingCommon::setSpiDIPOSourceAvailability(bool status)
{
   _bIsSpiDIPOActive = status;
}


/**
 * getSpiDIPOSourceAvailability
 * @param[in] :None
 * @parm[out] :SpiSourceStatus
 * @return bool
 */
bool MediaDatabindingCommon::getSpiDIPOSourceAvailability()
{
   return _bIsSpiDIPOActive;
}


/**
 * setSpiAAPSourceAvailability
 * @param[in] :Status :True Available
 * 					False Not Available
 * @parm[out] None
 * @return void
 */
void MediaDatabindingCommon::setSpiAAPSourceAvailability(bool status)
{
   _bIsSpiAAPActive = status;
}


/**
 * getSpiAAPSourceAvailability
 * @param[in] :None
 * @parm[out] :SpiSourceStatus
 * @return bool
 */
bool MediaDatabindingCommon::getSpiAAPSourceAvailability()
{
   return _bIsSpiAAPActive;
}


#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
/**
 * updateQuickSearchAvailabeStatus
 * @param[in] :Satus
 * @parm[out] :None
 * @return Void
 */

void MediaDatabindingCommon::updateQuickSearchAvailabeStatus(bool status)
{
   ETG_TRACE_USR4(("updateQuickSearchAvailabeStatus:%d", status));

   (*_quickSearchAvailabe).mIsQuickSearchActive = status;
   _quickSearchAvailabe.MarkAllItemsModified();
   if (false == _quickSearchAvailabe.SendUpdate(true))
   {
      ETG_TRACE_USR4(("QuickSearchAvailabeStatus : Updation failed..!!!"));
   }
}


#endif

void MediaDatabindingCommon::updateListSizeInMetaDataBrowse(float playAllSongsButtonSize, float verticalListSize)
{
   ETG_TRACE_USR4(("updateListSizeInMetaDataBrowse:%f %f", playAllSongsButtonSize, verticalListSize));
   (*_browseListSize).mPlayAllSongsListButtonSize.SetX(static_cast<float>(LIST_SIZE_WIDTH));
   (*_browseListSize).mPlayAllSongsListButtonSize.SetY(playAllSongsButtonSize);
   (*_browseListSize).mVerticalListSize.SetX(static_cast<float>(LIST_SIZE_WIDTH));
   (*_browseListSize).mVerticalListSize.SetY(verticalListSize);
   if (_browseListSize.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateListSizeInMetaDataBrowse : Updation failed..!!!"));
   }
}


void MediaDatabindingCommon::updatePlayAllSongButtonVisibility(bool isPlayAllSongButtonVisible)
{
   (*_browseListSize).mIsPlayallsongButtonActive = isPlayAllSongButtonVisible;
   if (_browseListSize.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateListSizeInMetaDataBrowse : Updation failed..!!!"));
   }
}


/**
 * updateMuteStatus
 * @param[in] :muteState
 * @parm[out] :None
 * @return Void
 */
void MediaDatabindingCommon::updateMuteStatus(bool muteState)
{
   (*_muteStatus).mIsMuteActive = muteState;
   _muteStatus.MarkItemModified(ItemKey::UpdateMuteStatus::IsMuteActiveItem);
   if (_muteStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("onMuteStateUpdate : Updation failed..!!!"));
   }
}


#ifdef ABCSEARCH_SUPPORT
/**
 * updateABCSearchButtonVisibleStatus
 * @param[in] :value
 * @parm[out] :None
 * @return bool
 */
bool MediaDatabindingCommon::updateABCSearchButtonVisibleStatus(bool value)
{
   ETG_TRACE_USR4(("updateABCSearchButtonVisibleStatus %d", value));
   (*_ABCSearchButtonInfo).mVisibled = value;

   _ABCSearchButtonInfo.MarkItemModified(ItemKey::ABCSearchButtonInfo::VisibledItem);
   if (_ABCSearchButtonInfo.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateABCSearchButtonVisibleStatus : Updating failed..!!!"));
      return false ;
   }
   return true ;
}


/**
 * updateABCSearchButtonEnableStatus
 * @param[in] :value
 * @parm[out] :None
 * @return bool
 */
bool MediaDatabindingCommon::updateABCSearchButtonEnableStatus(bool value)
{
   ETG_TRACE_USR4(("updateABCSearchButtonEnableStatus %d", value));
   (*_ABCSearchButtonInfo).mEnabled = value;

   _ABCSearchButtonInfo.MarkItemModified(ItemKey::ABCSearchButtonInfo::EnabledItem);
   if (_ABCSearchButtonInfo.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateABCSearchButtonEnableStatus : Updating failed..!!!"));
      return false ;
   }
   return true ;
}


/**
 * updateABCKeyboardValidChars
 * @param[in] :validChars
 * @parm[out] :None
 * @return bool
 */
bool MediaDatabindingCommon::updateABCKeyboardValidChars(std::string validChars)
{
   ETG_TRACE_USR4(("updateABCKeyboardSValidChars %s", validChars.c_str()));
   (*_ABCKeyboardInfo).mValidChars = validChars.c_str();

   _ABCKeyboardInfo.MarkItemModified(ItemKey::ABCKeyboardInfo::ValidCharsItem);
   if (_ABCKeyboardInfo.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateABCKeyboardSValidChars : Updating failed..!!!"));
      return false ;
   }
   return true ;
}


#endif
}//end of namespace Core
}//end of namespace App
