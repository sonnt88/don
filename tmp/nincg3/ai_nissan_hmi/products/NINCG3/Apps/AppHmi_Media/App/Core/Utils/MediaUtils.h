/**
*  @file   MediaUtils.h
*  @author ECV - bvi1cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/


#ifndef MEDIAPLAYER_UTILS_H_
#define MEDIAPLAYER_UTILS_H_

#define MEDIA_HEADER_STRING_SIZE 40
#define MEDIA_MIN_INDEXING_PERCENTAGE 0
#include "asf/core/Types.h"
#include "MPlay_fi_types.h"
#include "AppHmi_MediaTypes_Common.h"

namespace App {
namespace Core {

class MediaUtils
{
   public:
      static void vConvertSecToHourMinSec(unsigned int& uiHour, unsigned int& uiMinute, unsigned int& uiSecond);
      static void vFormatTime(char* TimeString, uint32 u32TimeInHour, uint32 u32TimeInMinutes, uint32 u32TimeInSec);
      static bool bCheckStringLengthNCopy(const std::string& strFullMetaData, std::string& strTrim, const uint32 u32Length);
      static bool bExtractFileName(const std::string& strPath, std::string& strName);
      static void AppendSelectedFolderInCurrFolderPath(std::string& strPath, const std::string& strFolderName);
      static void goOneFolderUp(std::string& strFolderPath, std::string& strnewFolderPath);
      static void convertToString(char* buffer, const uint32 intToStr);
      static void checkTheFolderPath(const std::string& strFolderPath, std::string& strNewFolderPath);
      static int32 ConvertHallDeviceType(uint32 deviceType);
      static std::string FooterTextForBrowser(uint32, uint32);
};


} // end of namespace Core
} // end of namespace App


#endif /* MEDIAPLAYER_UTILS_H_ */
