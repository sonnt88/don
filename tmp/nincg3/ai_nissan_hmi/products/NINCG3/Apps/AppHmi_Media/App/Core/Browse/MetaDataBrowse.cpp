/**
 *  @file   MetaDataBrowse.cpp
 *  @author ECV - vma6cob
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#include "hall_std_if.h"
#include "MetaDataBrowse.h"
#include "Core/Utils/MediaUtils.h"
#include "Core/LanguageDefines.h"
#include "CgiExtensions/ImageLoader.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL_BROWSE
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::MetaDataBrowse::
#include "trcGenProj/Header/MetaDataBrowse.cpp.trc.h"
#endif

using namespace MPlay_fi_types;

namespace App {
namespace Core {

static const char* const DATA_CONTEXT_LIST_CONTENT_BROWSER_ITEM    = "BrowserListText";
static const char* const DATA_CONTEXT_LIST_CONTENT_EMPTY_BROWSER_ITEM    = "BrowserListText_Empty";
static const char* const DATA_CONTEXT_LIST_BUTTON_BROWSER_ITEM    = "Browser";
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R
static const char* const DATA_CONTEXT_LIST_BUTTON_BROWSER_ITEM_EVEN = "Browse_Even";
#endif

struct staticBrowserInfo
{
   uint32 listId;
   LANGUAGE_ID_TYPE name;
};


static staticBrowserInfo ListIdUSBBrowser[] =
{
#if defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE1)|| defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R)
   {LIST_ID_BROWSER_FOLDER_BROWSE, LANGUAGE_ID(USB__BROWSE_Button_FolderBrowse, "FOLDER BROWSE")},
#endif
   {LIST_ID_BROWSER_PLAYLIST, LANGUAGE_ID(USB_BROWSE__PlayList_HeadLine, "PLAYLIST")},
   {LIST_ID_BROWSER_ARTIST, LANGUAGE_ID(USB_BROWSE__Artists_HeadLine   , "ARTIST")},
   {LIST_ID_BROWSER_ALBUM, LANGUAGE_ID(USB_BROWSE__Album_HeadLine , "ALBUM")},
   {LIST_ID_BROWSER_SONG, LANGUAGE_ID(USB_BROWSE__Songs_HeadLine , "SONG")},
   {LIST_ID_BROWSER_GENRE, LANGUAGE_ID(USB_BROWSE__Genres_HeadLine, "GENRE")},
   {LIST_ID_BROWSER_COMPOSER, LANGUAGE_ID(USB_BROWSE__Composer_HeadLine , "COMPOSER")}
};


static staticBrowserInfo ListIdIpodBrowser[] =
{
   {LIST_ID_BROWSER_PLAYLIST, LANGUAGE_ID(IPOD_MENU__PlayList_HeadLine , "PlayList")},
   {LIST_ID_BROWSER_ARTIST, LANGUAGE_ID(IPOD_MENU__Artists_HeadLine , "ARTIST")},
   {LIST_ID_BROWSER_ALBUM, LANGUAGE_ID(IPOD_MENU__Album_HeadLine  , "ALBUM")},
   {LIST_ID_BROWSER_SONG, LANGUAGE_ID(IPOD_MENU__Songs_HeadLine, "SONG")},
   {LIST_ID_BROWSER_GENRE, LANGUAGE_ID(IPOD_MENU__Genres_HeadLine, "GENRE")},
   {LIST_ID_BROWSER_COMPOSER, LANGUAGE_ID(IPOD_MENU__Composer_HeadLine, "COMPOSER")},
   {LIST_ID_BROWSER_AUDIOBOOK, LANGUAGE_ID(IPOD_MENU__AudiobookList_HeadLine, "AUDIOBOOK")},
   {LIST_ID_BROWSER_PODCAST, LANGUAGE_ID(IPOD_MENU__Podcast_HeadLine, "PODCAST")}
};


static uint32 ListSelectionmap[][2] =
{
   /*List ID when 'Item' is Selected*/         /*List ID When 'All' is Selected*/
   {LIST_ID_BROWSER_GENRE_ARTIST,               LIST_ID_BROWSER_ARTIST},      /* LTY_GENRE */
   {LIST_ID_BROWSER_ARTIST_ALBUM,               LIST_ID_BROWSER_ALBUM},       /* LTY_ARTIST */
   {LIST_ID_BROWSER_ALBUM_SONG,                 LIST_ID_BROWSER_SONG},        /* LTY_ALBUM */
   {PLAY_SONG_IN_LIST,                          PLAY_SONG_IN_LIST},           /* LTY_SONG */
   {LIST_ID_BROWSER_GENRE_ARTIST_ALBUM,         LIST_ID_BROWSER_GENRE_ALBUM}, /* LTY_GENRE_ARTIST */
   {LIST_ID_BROWSER_GENRE_ARTIST_ALBUM_SONG,    LIST_ID_BROWSER_GENRE_ARTIST_SONG}, /* LTY_GENRE_ARTIST_ALBUM */
   {PLAY_SONG_IN_LIST,                          PLAY_SONG_IN_LIST},           /* LTY_GENRE_ARTIST_ALBUM_SONG */
   {PLAY_SONG_IN_LIST,                          PLAY_SONG_IN_LIST},           /* LTY_GENRE_ARTIST_SONG */
   {LIST_ID_BROWSER_GENRE_ALBUM_SONG,           LIST_ID_BROWSER_GENRE_SONG},  /* LTY_GENRE_ALBUM */
   {PLAY_SONG_IN_LIST,                          PLAY_SONG_IN_LIST},           /* LTY_GENRE_ALBUM_SONG */
   {PLAY_SONG_IN_LIST,                          PLAY_SONG_IN_LIST},           /* LTY_GENRE_SONG */
   {LIST_ID_BROWSER_ARTIST_ALBUM_SONG,          LIST_ID_BROWSER_ARTIST_SONG}, /* LTY_ARTIST_ALBUM */
   {PLAY_SONG_IN_LIST,                          PLAY_SONG_IN_LIST},           /* LTY_ARTIST_ALBUM_SONG */
   {PLAY_SONG_IN_LIST,                          PLAY_SONG_IN_LIST},           /* LTY_ARTIST_SONG */
   {PLAY_SONG_IN_LIST,                          PLAY_SONG_IN_LIST},           /* LTY_ALBUM_SONG */
   {LIST_ID_BROWSER_PODCAST_EPISODE,            PLAY_SONG_IN_LIST},           /* LTY_PODCAST */
   {PLAY_SONG_IN_LIST,                          PLAY_SONG_IN_LIST},           /* LTY_PODCAST_EPISODE */
   {LIST_ID_BROWSER_BOOKTITLE_CHAPTER,          PLAY_SONG_IN_LIST},           /* LTY_AUDIOBOOK */
   {PLAY_SONG_IN_LIST,                          PLAY_SONG_IN_LIST},           /* LTY_BOOKTITLE_CHAPTER */
   {INVALID_VALUE,                              INVALID_VALUE},               /* LTY_AUTHOR */
   {INVALID_VALUE,                              INVALID_VALUE},               /* LTY_AUTHOR_BOOKTITLE */
   {INVALID_VALUE,                              INVALID_VALUE},               /* LTY_AUTHOR_BOOKTITLE_CHAPTER */
   {LIST_ID_BROWSER_COMPOSER_ALBUM,             LIST_ID_BROWSER_ALBUM},       /* LTY_COMPOSER */
   {LIST_ID_BROWSER_COMPOSER_ALBUM_SONG,        LIST_ID_BROWSER_COMPOSER_SONG},/* LTY_COMPOSER_ALBUM */
   {PLAY_SONG_IN_LIST,                          PLAY_SONG_IN_LIST},           /* LTY_COMPOSER_ALBUM_SONG */
   {PLAY_SONG_IN_LIST,                          PLAY_SONG_IN_LIST},           /* LTY_COMPOSER_SONG */
   {INVALID_VALUE,                              INVALID_VALUE},               /* LTY_VIDEO */
   {INVALID_VALUE,                              INVALID_VALUE},               /* LTY_VIDEO_EPISODE */
   {LIST_ID_BROWSER_PLAYLIST_SONG,              PLAY_SONG_IN_LIST},           /* LTY_PLAYLIST */
   {PLAY_SONG_IN_LIST,                          PLAY_SONG_IN_LIST},           /* LTY_PLAYLIST_SONG */
   {PLAY_SONG_IN_LIST,                          PLAY_SONG_IN_LIST}            /* LTY_CURRENT_SELECTION */
};


MetaDataBrowse::MetaDataBrowse() :
   _isAllset(false),
   _nextListId(0),
   prevListWindowStartIndex(0),
   _isBackTraversing(false)
{
   ETG_I_REGISTER_FILE();

   PlayableStatus.clear();
   _listBrowsingHistory.clear();
   ListHistoryInfo historyInfo;
   historyInfo.listId = LIST_ID_BROWSER_MAIN;
   historyInfo.windowStartIndex = 0;
   _listBrowsingHistory.push_back(historyInfo);
   _headerHistory.clear();
#ifdef METADATA_THUMBNAIL_ALBUMART_SUPPORT
   ptrThumbNail = NULL;
#endif
}


MetaDataBrowse::~MetaDataBrowse()
{
#ifdef METADATA_THUMBNAIL_ALBUMART_SUPPORT
   if (ptrThumbNail)
   {
      COURIER_SAFE_DELETE(ptrThumbNail);
   }
#endif
}


/**
 *  MetaDataBrowse::getDataProvider - Gets the ListDataProvider
 *  @param [in] requestedlistInfo - structure containing the requested list information
 *  @param [in] activeDeviceType - current active device type
 *  @return tSharedPtrDataProvider
 */
tSharedPtrDataProvider MetaDataBrowse::getDataProvider(RequestedListInfo& requestedlistInfo, uint32 /*activeDeviceType*/)
{
   ETG_TRACE_USR4(("MetaDataBrowse::getDataProvider:%d", _nextListId));
   requestedlistInfo._listType = _nextListId;
   if (_isAllset && requestedlistInfo._startIndex > 0)
   {
      requestedlistInfo._startIndex -= 1; // The start index to be requested from mediaplayer is one less than what the GUI requests
   }
   requestListData(requestedlistInfo);

   MediaDatabinding::getInstance().updateActiveListType(_nextListId);
   return tSharedPtrDataProvider();
}


/**
 *  MetaDataBrowse::populateNextListOrPlayItem - Function to populate the next list or play based on the selected item
 *  @param [in] currentListId - current list Id from which the user selects an Item
 *  @param [in] selectedItem - selected Item
 *  @param [in] activeDeviceType - current active device type
 *  @return none
 */
void MetaDataBrowse::populateNextListOrPlayItem(uint32 currentListId, uint32 selectedRow, uint32 activeDeviceType, uint32 activeDeviceTag)
{
   ETG_TRACE_USR4(("MetaDataBrowse::populateNextListOrPlayItem"));
   _nextListId = getNextListId(currentListId, selectedRow, activeDeviceType);
   uint32 activeItemIndex = getActiveItemIndex(selectedRow);
   _activeDeviceType = activeDeviceType;
   HideThumbnailImageInMetadataBrowse(_nextListId);

   if (_nextListId == PLAY_SONG_IN_LIST)
   {
      playSelectedItem(activeItemIndex);
      _nextListId = currentListId;
   }
   else
   {
      ListHistoryInfo historyInfo;
      historyInfo.listId = currentListId;
      historyInfo.windowStartIndex = _windowStartIndex;
      updateBrowsingHistory(historyInfo);
      bool isPlayAllSongsActive = true;
      if ((_nextListId == LIST_ID_BROWSER_PODCAST) || (_nextListId == LIST_ID_BROWSER_PODCAST_EPISODE) || (_nextListId == LIST_ID_BROWSER_AUDIOBOOK))
      {
         isPlayAllSongsActive = false;
         MediaDatabinding::getInstance().updateListSizeInMetaDataBrowse(0.0f, 414.0f);
         MediaDatabinding::getInstance().updatePlayAllSongButtonVisibility(isPlayAllSongsActive);
      }
      else
      {
         MediaDatabinding::getInstance().updateListSizeInMetaDataBrowse(69.0f, 345.0f);
         MediaDatabinding::getInstance().updatePlayAllSongButtonVisibility(isPlayAllSongsActive);
      }
      populateNextList(currentListId, activeItemIndex, activeDeviceTag);
   }
   ETG_TRACE_USR4(("next List is created id %d", _nextListId));
}


/**
 *  MetaDataBrowse::UpdateThumbnailAlbumArtInMetadataBrowse - Function to Hide the display of thumbnailalbumart in metadatabrowse except albumlist
 *  @param [in] None
 *  @return none
 */
void MetaDataBrowse::HideThumbnailImageInMetadataBrowse(uint32 listId)
{
   if ((listId != LIST_ID_BROWSER_ALBUM) && (listId != LIST_ID_BROWSER_GENRE_ARTIST_ALBUM) && (listId != LIST_ID_BROWSER_GENRE_ALBUM)
         && (listId != LIST_ID_BROWSER_ARTIST_ALBUM) && (listId != LIST_ID_BROWSER_COMPOSER_ALBUM))
   {
      ETG_TRACE_USR4((" MetaDataBrowse::UpdateThumbnailAlbumArtInMetadataBrowse()"));
#ifdef METADATA_THUMBNAIL_ALBUMART_SUPPORT
      MediaDatabinding::getInstance().updateThumbnailAlbumArt(false, false);
#endif
   }
}


/**
 *  MetaDataBrowse::playSelectedItem - Function to play the selected item
 *  @param [in] selectedIndex - selected Item
 *  @return none
 */
void MetaDataBrowse::playSelectedItem(uint32 selectedIndex)
{
   ETG_TRACE_USR4(("PlayableStatus %d", PlayableStatus[selectedIndex]));
   if (PlayableStatus[selectedIndex] == T_e8_MPlayPlayableStatus__e8FP_DRM_PROTECTED)
   {
      POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND)));
   }
   else
   {
      _isBackTraversing = true;  // flag is set to scroll to the previously browsed position when user again enters into browse screen.
      prevListWindowStartIndex = _windowStartIndex;
      switchtoPlayScreen();
      if (_iListFacade)
      {
         _iListFacade->requestPlayItem(selectedIndex);
      }
   }
}


/**
 *  MetaDataBrowse::getHeaderText - Function to get the header text
 *  @param [in] activeItemIndex - active Item
 *  @return headerText
 */
Candera::String MetaDataBrowse::getHeaderText(uint32 activeItemIndex)
{
   Candera::String headerText;
   if (_activeDeviceType == SOURCE_USB)
   {
      headerText = LANGUAGE_ID_STRING(ListIdUSBBrowser[activeItemIndex].name);
   }
   else
   {
      headerText = LANGUAGE_ID_STRING(ListIdIpodBrowser[activeItemIndex].name);
   }

   return headerText;
}


/**
 *  MetaDataBrowse::populateNextList - Function to populate the next(child) list
 *  @param [in] currentListId - current(parent) list id
 *  @param [in] activeItemIndex - selected row based on which the next list will be populated
 *  @return none
 */
void MetaDataBrowse::populateNextList(uint32 currentListId, uint32 activeItemIndex, uint32 activeDeviceTag)
{
   Candera::String headerText;
   if (_iListFacade)
   {
      initListStateMachine();
      _iListFacade->setActiveIndex(activeItemIndex);
      headerText = getHeaderTextBySelectedIndex(activeItemIndex, currentListId);
   }
   if (currentListId == LIST_ID_BROWSER_MAIN)
   {
      headerText = getHeaderText(activeItemIndex);
      MediaDatabinding::getInstance().updateScrollBarSwitchIndex(getNodeIndexQuickSearch(_nextListId));
   }

   POST_MSG((COURIER_MESSAGE_NEW(BrowserHeaderUpdMsg)(headerText)));
   updateHeaderTextToHistory(headerText);
   if (_iListFacade)
   {
      _iListFacade->requestChildIndexList(_nextListId, activeDeviceTag);
   }

   ETG_TRACE_USR4(("headerText:%s", headerText.GetCString()));
}


/**
 *  MetaDataBrowse::getHeaderTextBySelectedIndex - Function to get the header text based on the list Id
 *  @param [in] activeItemIndex - selected row based on which the next list will be populated
 *  @param [in] currentListId -current list id
 *  @return Candera::String
 */
Candera::String MetaDataBrowse::getHeaderTextBySelectedIndex(uint32 activeItemIndex, uint32 currentListId)
{
   Candera::String browseHeaderText;
   if (activeItemIndex < _sListItemText.size())
   {
      return _sListItemText[activeItemIndex].c_str();
   }
   else
   {
#ifdef SHOW_BROWSETEXT_IN_HEADER_SUPPORT
      switch (currentListId)
      {
         case LIST_ID_BROWSER_GENRE:
         {
            browseHeaderText = LANGUAGE_STRING(MEDIA_BROWSE__All_Artists, "All Artists");
            break;
         }
         case LIST_ID_BROWSER_ARTIST:
         case LIST_ID_BROWSER_GENRE_ARTIST:
         case LIST_ID_BROWSER_COMPOSER:
         {
            browseHeaderText = LANGUAGE_STRING(MEDIA_BROWSE__All_Albums, "All Albums");
            break;
         }
         case LIST_ID_BROWSER_ALBUM:
         case LIST_ID_BROWSER_GENRE_ARTIST_ALBUM:
         case LIST_ID_BROWSER_GENRE_ALBUM:
         case LIST_ID_BROWSER_ARTIST_ALBUM:
         case LIST_ID_BROWSER_COMPOSER_ALBUM:
         {
            browseHeaderText = LANGUAGE_STRING(MEDIA_BROWSE__All_Songs, "All Songs");
            break;
         }
         default:
         {
            ETG_TRACE_USR4(("IBrowse::getListTextForAll default case"));
            break;
         }
      }
#elif VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
      (void) currentListId;
      browseHeaderText = LANGUAGE_STRING(BROWSE_All, "All");
#endif

      return browseHeaderText;
   }
}


/**
 *  FolderBrowse::populatePreviousList - Function to populate the previous (parent) list when user pressed Back key
 *  @return none
 */
void MetaDataBrowse::populatePreviousList(uint32 /*deviceTag*/)
{
   prevListWindowStartIndex = getPrevListWindowStartIndex();
   uint32 prevListId = getPrevListId();
   // uint32 Index = getNodeIndexQuickSearch(prevListId);
   ETG_TRACE_USR4(("prev List id:%d", prevListId));
   HideThumbnailImageInMetadataBrowse(prevListId);
   if (_iListFacade)
   {
      _iListFacade->removeListFromMap();
   }

   if (prevListId == LIST_ID_BROWSER_MAIN)
   {
      //      MediaDatabinding::getInstance().updateScrollBarSwitchIndex(Index);
      POST_MSG((COURIER_MESSAGE_NEW(ButtonListItemUpdMsg)(LIST_ID_BROWSER_MAIN, LIST_ROW_VALUE, 0, enRelease)));
      _isAllset = false;
   }
   else
   {
      Candera::String headerText = getCurrentListHeaderText();
      POST_MSG((COURIER_MESSAGE_NEW(BrowserHeaderUpdMsg)(headerText)));
      _nextListId = prevListId;
      _isBackTraversing = true;
      POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_START)));
      _iListFacade->requestParentIndexList(prevListId);
      ETG_TRACE_USR4(("prev List is created id %d", prevListId));
   }
}


/**
 *  MetaDataBrowse::getNextListId - Function to get the next(Child) list id based on the current list id
 *  @param [in] currentListId
 *  @param [in] selectedRow
 *  @param [in] activeDeviceType
 *  @return - next list id
 */
uint32 MetaDataBrowse::getNextListId(uint32 currentListId, uint32 selectedRow, uint32 activeDeviceType)
{
   uint32 nextListId = 0;
   switch (currentListId)
   {
      case LIST_ID_BROWSER_MAIN:
      {
         if (activeDeviceType == SOURCE_USB)
         {
            nextListId = ListIdUSBBrowser[selectedRow].listId;
         }
         else if (activeDeviceType == SOURCE_IPOD)
         {
            nextListId = ListIdIpodBrowser[selectedRow].listId;
         }
         break;
      }
      case PLAY_SONG_IN_LIST:
         nextListId = PLAY_SONG_IN_LIST;
         break;
      default:
      {
         uint32 isAllSelected = (_isAllset && (selectedRow == 0)) ? 1 : 0 ;
         nextListId = ListSelectionmap[currentListId % MEDIA_BASE_LIST_ID][isAllSelected];
         break;
      }
   }
   ETG_TRACE_USR4(("MetaDataBrowse::getNextListId:%d", nextListId));
   return nextListId;
}


void MetaDataBrowse::populateCurrentPlayingList(uint32 currentPlayingListHandle)
{
   if (_iListFacade)
   {
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      Candera::String currentListHeader = LANGUAGE_STRING(IPOD__MENU_Button_CurrentList, "Current List");
      POST_MSG((COURIER_MESSAGE_NEW(BrowserHeaderUpdMsg)(currentListHeader)));
#endif
      _iListFacade->setCurrentStateMachineState(LIST_REQUEST_PROCESS_DONE);
      _iListFacade->requestCurrentPlayingList(currentPlayingListHandle);
   }
}


/**
 * getPrevListId - Get meta data previous list id from from  history, and remove current list id from vector
 * @param[in] none
 * @parm[out] none
 * @return uint32 - previous list id
 */
uint32 MetaDataBrowse::getPrevListId()
{
   ListHistoryInfo historyInfo = _listBrowsingHistory.back();
   if (historyInfo.listId != LIST_ID_BROWSER_MAIN)
   {
      _listBrowsingHistory.pop_back();
   }
   return historyInfo.listId;
}


/**
 * getPrevListIdStartIndex - Get meta data previous list id's start index from  history
 * @param[in] none
 * @parm[out] none
 * @return uint32 - previous start index
 */
uint32 MetaDataBrowse::getPrevListWindowStartIndex()
{
   ListHistoryInfo historyInfo = _listBrowsingHistory.back();
   return historyInfo.windowStartIndex;
}


/**
 * updateListData - Populates the list with the data in ListResultInfo
 *
 * @param[in] ListResultInfo
 * @parm[out] none
 * @return void
 */
void MetaDataBrowse::updateListData(void)
{
   setAllFlag();

   ETG_TRACE_USR4(("updateListData u32ListType=%d , u32StartIndex=%d,u32, TotalListSize=%d",
                   _listResultInfo._listType,  _listResultInfo._startIndex, _listResultInfo._totalListSize));

#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
   if (_listResultInfo._totalListSize <= FLEX_LIST_MAX_QUICK_SEARCH)
   {
      MediaDatabinding::getInstance().updateQuickSearchAvailabeStatus(false);
   }
   else
   {
      MediaDatabinding::getInstance().updateQuickSearchAvailabeStatus(getNodeIndexQuickSearch(_nextListId));
   }
#endif

   tSharedPtrDataProvider dataProvider = fillListItems();

   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));

   scrollListToWindow(prevListWindowStartIndex);
}


/**
 * scrollListToWindow - Private helper function to scroll the list to particular window
 * @param[in] uint32 - windowStartIndex
 * @parm[out] none
 * @return void
 */
void MetaDataBrowse::scrollListToWindow(uint32 windowStartIndex)
{
   if (_isBackTraversing)
   {
      ETG_TRACE_USR4(("updateListData _windowStartIndex=%d", windowStartIndex));

      POST_MSG((COURIER_MESSAGE_NEW(ListChangeMsg)(LIST_ID_BROWSER_METADATA_BROWSE, ListChangeSet, windowStartIndex)));
      _isBackTraversing = false;
   }
}


/**
 * setAllFlag - Private helper function to set flag if "All" is available
 * @param[in] none
 * @parm[out] none
 * @return void
 */
void MetaDataBrowse::setAllFlag()
{
   bool isAllFieldRequired = IBrowse::isAllAvailable(_listResultInfo._oMediaObjects[0].getE8CategoryType());
   _isAllset = (isAllFieldRequired) ? true : false;
}


/**
 * getMetaDataFieldText - Private helper function to get window size for the browser
 * @param[in] none
 * @parm[out] none
 * @return uint32 - windowSize
 */
uint32 MetaDataBrowse::getWidnowSize(void)
{
   return (_isAllset && (_listResultInfo._startIndex == 0)) ? (_listResultInfo._oMediaObjects.size() + 1)
          : (_listResultInfo._oMediaObjects.size());
}


/**
 * getMetaDataFieldText - Private helper function to get total list size
 * @param[in] none
 * @parm[out] none
 * @return uint32 - totalListSize
 */
uint32 MetaDataBrowse::gettotalListSize(void)
{
   return (_isAllset) ? (_listResultInfo._totalListSize + 1) : (_listResultInfo._totalListSize);
}


/**
 * fillListItems - Private helper function to fill browser list items
 * @param[in] none
 * @parm[out] none
 * @return tSharedPtrDataProvider
 */
tSharedPtrDataProvider MetaDataBrowse::fillListItems()
{
   ETG_TRACE_USR4(("MetaDataBrowse::fillListItems()"));
   Candera::UInt32 guiListIndex = 0;
   uint32 windowsize = getWidnowSize();
   // In lists with "All", the start index returned from media player is one less than what GUI expects. So we add 1.
   _listResultInfo._startIndex = (_isAllset && _listResultInfo._startIndex > 0) ?
                                 (_listResultInfo._startIndex + 1) : (_listResultInfo._startIndex);

   tSharedPtrDataProvider dataProvider = ListDataProvider::newBackEndInterface(LIST_ID_BROWSER_METADATA_BROWSE,
                                         _listResultInfo._startIndex, windowsize, gettotalListSize());
   tSharedPtrDataItemVector rowItems;
   PlayableStatus.clear();

   if (_isAllset && (_listResultInfo._startIndex == 0))
   {
      rowItems.push_back(DataItem::newIdentifierItem(LIST_ID_BROWSER_METADATA_BROWSE, guiListIndex,
                         _listResultInfo._listType + MEDIA_BASE_LIST_ID, 0));
      rowItems.push_back(DataItem::newDataItem(getListTextForAll(_nextListId),
                         guiListIndex, DATA_CONTEXT_LIST_CONTENT_BROWSER_ITEM));

      (*dataProvider)[guiListIndex] = DataItem::newDataItem(rowItems, guiListIndex, getlistItemTemplateForMetaDataBrowse(guiListIndex));
   }

   for (Candera::UInt32 listSliceIndex = 0; guiListIndex < windowsize; ++listSliceIndex)
   {
      ETG_TRACE_USR4(("MetaDataBrowse::fillListItems() inside for loop"));
      rowItems.clear();
      rowItems.push_back(DataItem::newIdentifierItem(LIST_ID_BROWSER_METADATA_BROWSE, guiListIndex,
                         _listResultInfo._listType + MEDIA_BASE_LIST_ID, 0));
      PlayableStatus.push_back(_listResultInfo._oMediaObjects[listSliceIndex].getE8PlayableStatus());
      rowItems.push_back(DataItem::newDataItem(getMetaDataFieldText(listSliceIndex).c_str(), guiListIndex,
                         DATA_CONTEXT_LIST_CONTENT_BROWSER_ITEM));
#ifdef METADATA_THUMBNAIL_ALBUMART_SUPPORT

      if ((_nextListId == LIST_ID_BROWSER_ALBUM) || (_nextListId == LIST_ID_BROWSER_GENRE_ARTIST_ALBUM) || (_nextListId == LIST_ID_BROWSER_GENRE_ALBUM)
            || (_nextListId == LIST_ID_BROWSER_ARTIST_ALBUM) || (_nextListId == LIST_ID_BROWSER_COMPOSER_ALBUM))
      {
         ETG_TRACE_USR4(("list items are album"));
         ::MPlay_fi_types::T_MPlayImageData info = _listResultInfo._oMediaObjects[listSliceIndex].getOThumbnailData();
         ThumbNailAlbumArtData Data;
         Data = ThumbNailAlbumArtData();
         Candera::Bitmap* bmp = ImageLoader::loadBitmapData(info.data(), info.size());
         if (bmp != NULL && info.size() != 0)
         {
            ETG_TRACE_USR4(("image is available"));
            Data.mThumbnail_AlbumArt = ImageLoader::createImage(bmp);
            Data.misDefaultThumbnailAvailable = false;
            Data.misThumbNailAlbumArtAvailable = true;
         }
         else
         {
            ETG_TRACE_USR4(("image is not available"));
            Data.misDefaultThumbnailAvailable = true;
            Data.misThumbNailAlbumArtAvailable = false;
         }
         ptrThumbNail = COURIER_NEW(Courier::DataBindingUpdater<ThumbNailAlbumArtDataBindingSource>)(Data);
         rowItems.push_back(DataItem::newDataItem(ptrThumbNail, guiListIndex));
      }
#endif

      (*dataProvider)[guiListIndex] = DataItem::newDataItem(rowItems, guiListIndex, getlistItemTemplateForMetaDataBrowse(guiListIndex));
   }
   return dataProvider;
}


/**
 * getlistItemTemplateForMetaDataBrowse - function to get list template.
 * @param[in] uint32 index
 * @parm[out] const char
 * @return const char - return listItemTemplate
 */
const char* MetaDataBrowse::getlistItemTemplateForMetaDataBrowse(uint32& index)
{
   const char* listItemTemplate;

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R
   if (index % 2)
   {
      listItemTemplate = DATA_CONTEXT_LIST_BUTTON_BROWSER_ITEM;
   }
   else
   {
      listItemTemplate = DATA_CONTEXT_LIST_BUTTON_BROWSER_ITEM_EVEN;
   }

#else
   listItemTemplate = DATA_CONTEXT_LIST_BUTTON_BROWSER_ITEM;
#endif
   ++index;
   return listItemTemplate;
}


/**
 * getListTextForAll - Private helper function to fill the text for "All" list item
 * @param[in] currentListId
 * @parm[out] none
 * @return Candera::String
 */
Candera::String MetaDataBrowse::getListTextForAll(uint32 currentListId)
{
   ETG_TRACE_USR4(("IBrowse::getListTextForAll"));
   Candera::String listAllText;
#ifdef METADATA_BROWSE_ALL_SUPPORT
   switch (currentListId)
   {
      case LIST_ID_BROWSER_GENRE:
      {
         listAllText = LANGUAGE_STRING(MEDIA_BROWSE__All_Artists, "All Artists");
         break;
      }
      case LIST_ID_BROWSER_ARTIST:
      case LIST_ID_BROWSER_GENRE_ARTIST:
      case LIST_ID_BROWSER_COMPOSER:
      {
         listAllText = LANGUAGE_STRING(MEDIA_BROWSE__All_Albums, "All Albums");
         break;
      }
      case LIST_ID_BROWSER_ALBUM:
      case LIST_ID_BROWSER_GENRE_ARTIST_ALBUM:
      case LIST_ID_BROWSER_GENRE_ALBUM:
      case LIST_ID_BROWSER_ARTIST_ALBUM:
      case LIST_ID_BROWSER_COMPOSER_ALBUM:
      {
         listAllText = LANGUAGE_STRING(MEDIA_BROWSE__All_Songs, "All Songs");
         break;
      }
      default:
      {
         ETG_TRACE_USR4(("IBrowse::getListTextForAll default case"));
         break;
      }
   }
#elif VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
   (void) currentListId;
   listAllText = LANGUAGE_STRING(BROWSE_All, "All");
#endif

   return listAllText;
}


/**
 * getMetaDataFieldText - Private helper function to get metadata text from oMediaObject
 * @param[in] uint32 listSliceIndex
 * @parm[out] none
 * @return std::string - metaDataFieldText
 */
std::string MetaDataBrowse::getMetaDataFieldText(uint32 listSliceIndex)
{
   std::string metaDataFieldText;
   metaDataFieldText.clear();
   switch (_listResultInfo._oMediaObjects[listSliceIndex].getE8CategoryType())
   {
      case T_e8_MPlayCategoryType__e8CTY_PLAYLIST:
      case T_e8_MPlayCategoryType__e8CTY_GENRE:
      case T_e8_MPlayCategoryType__e8CTY_COMPOSER:
      case T_e8_MPlayCategoryType__e8CTY_NAME:
      {
         metaDataFieldText = _listResultInfo._oMediaObjects[listSliceIndex].getSMetaDataField1();
         break;
      }
      case T_e8_MPlayCategoryType__e8CTY_ARTIST:
      case T_e8_MPlayCategoryType__e8CTY_TITLE:
      case T_e8_MPlayCategoryType__e8CTY_EPISODE:
      {
         metaDataFieldText = _listResultInfo._oMediaObjects[listSliceIndex].getSMetaDataField2();
         break;
      }
      case T_e8_MPlayCategoryType__e8CTY_ALBUM:
      case T_e8_MPlayCategoryType__e8CTY_CHAPTER:
      {
         metaDataFieldText = _listResultInfo._oMediaObjects[listSliceIndex].getSMetaDataField3();
         break;
      }
      case T_e8_MPlayCategoryType__e8CTY_SONG:
      {
         metaDataFieldText = _listResultInfo._oMediaObjects[listSliceIndex].getSMetaDataField4();
         break;
      }
   }
   return metaDataFieldText;
}


/**
 * isAllAvailable - Private helper function to get if All field is required for current category
 * @param[in] uint32 categoryType
 * @parm[out] none
 * @return bool - true if all is required otherwise false
 */


/**
 * getActiveItemIndex - Get meta data list index for selected row from current window
 * @param[in] uint32 selectedIndex - selected row from current window
 * @parm[out] none
 * @return uint32 -
 */
int32 MetaDataBrowse::getActiveItemIndex(int32 selectedIndex)
{
   uint32 activeItemIndex = FLEX_LIST_MAX_BUFFER_SIZE + 1;

   if (_isAllset && (_listResultInfo._startIndex == 0))
   {
      if (selectedIndex == 0)
      {
         activeItemIndex = FLEX_LIST_MAX_BUFFER_SIZE + 1;
      }
      else
      {
         activeItemIndex = selectedIndex - 1;
      }
   }
   else
   {
      activeItemIndex = selectedIndex;
   }
   ETG_TRACE_USR4(("activeItemIdx:%d", activeItemIndex));
   return activeItemIndex;
}


#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
uint32 MetaDataBrowse::getActiveQSIndex(uint32 absoluteIndex)
{
   if (_isAllset)
   {
      return  absoluteIndex += 1;
   }
   else
   {
      return absoluteIndex;
   }
}


#endif

/** switchtoPlayScreen - private function to switch main screen and close current browser
 * @param[in] none
 * @parm[out] none
 * @return void -
 */
void MetaDataBrowse::switchtoPlayScreen()
{
   ETG_TRACE_USR4(("switchtoPlayScreen %d", _activeDeviceType));
   if (_activeDeviceType == SOURCE_USB)
   {
      POST_MSG((COURIER_MESSAGE_NEW(SwitchtoUSBMsg)(1)));
   }
   else if (_activeDeviceType == SOURCE_IPOD)
   {
      POST_MSG((COURIER_MESSAGE_NEW(SwitchtoIPODMsg)(1)));
   }
}


/**
 * updateBrowsingHistory - Function to add list to history when new list is selected

 * @param[in] listId
 * @parm[out] none
 * @return none
 */
void MetaDataBrowse::updateBrowsingHistory(ListHistoryInfo& historyInfo)
{
   if (historyInfo.listId != PLAY_SONG_IN_LIST)
   {
      _listBrowsingHistory.push_back(historyInfo);
   }
}


/**
 * updateHeaderTextToHistory - Function to add header text of the current list to history

 * @param[in] header text
 * @parm[out] none
 * @return none
 */
void MetaDataBrowse::updateHeaderTextToHistory(Candera::String headerText)
{
   _headerHistory.push_back(headerText);
}


/**
 * getCurrentListHeaderText - Function to retrieve header text for current list from history

 * @param[in] none
 * @parm[out] header text
 * @return none
 */
Candera::String MetaDataBrowse::getCurrentListHeaderText()
{
   Candera::String headerText;
   _headerHistory.pop_back();
   if (_headerHistory.size())
   {
      headerText = _headerHistory.back();
   }
   return headerText;
}


/**
 * updateEmptyListData - Populates the list in a different template when the list is empty
 * @param[in] Current List Type
 * @parm[out] none
 * @return void
 */
void MetaDataBrowse::updateEmptyListData(uint32 /*_currentListType*/)
{
   ETG_TRACE_USR4(("Empty List Data"));
   ListDataProviderBuilder listBuilder(LIST_ID_BROWSER_METADATA_BROWSE, DATA_CONTEXT_LIST_CONTENT_EMPTY_BROWSER_ITEM);
   listBuilder.AddItem(0).AddData(LANGUAGE_STRING(METADATA_BROWSE_EmptyList, "The List is Empty"));
   tSharedPtrDataProvider dataProvider = listBuilder.CreateDataProvider();
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));
}


/**
 *  MetaDataBrowse::initializeListParameters - initializes the list parameters
 *  @return Return_Description
 */
void MetaDataBrowse::initializeListParameters()
{
   _isAllset = false;
   _nextListId = 0;
   prevListWindowStartIndex = 0;
   _isBackTraversing = false;

   PlayableStatus.clear();
   _listBrowsingHistory.clear();
   ListHistoryInfo historyInfo;
   historyInfo.listId = LIST_ID_BROWSER_MAIN;
   historyInfo.windowStartIndex = 0;
   _listBrowsingHistory.push_back(historyInfo);
   _headerHistory.clear();
}


/**
 *  MetaDataBrowse::setWindowStartIndex - sets the window start index for meta data browse
 * @param[in] windowStartIndex
 *  @return Return_Description
 */
void MetaDataBrowse::setWindowStartIndex(uint32 windowStartindex)
{
   _windowStartIndex = windowStartindex;
}


/**
 * setMetadataBrowseListId - sets the metadata browse list id after list information is obtained
 * @param[in] uint32 listId - list id obtained from service
 * @parm[out] none
 * @return void -
 */
void MetaDataBrowse::setMetadataBrowseListId(uint32 listId)
{
   _nextListId = listId;
}


void MetaDataBrowse::setFocussedIndex(int32 focussedIndex)
{
   ETG_TRACE_USR4(("MetaDataBrowse::setFocussedIndex:%d", focussedIndex));
   _focussedIndex = focussedIndex;
   _iListFacade->setFocussedIndexAndNextListId(_focussedIndex, getNextListId(_nextListId, _focussedIndex, _activeDeviceType));
}


void MetaDataBrowse::setFooterDetails(uint32 focussedIndex)
{
   uint32 currentStartIndex = _listResultInfo._startIndex;
   uint32 currentFolderListSize = _listResultInfo._totalListSize;
   bool isAllAvail = isAllAvailable(_listResultInfo._oMediaObjects[0].getE8CategoryType());

   uint32 absoluteIndex =  focussedIndex + currentStartIndex;
   currentFolderListSize = isAllAvail ? currentFolderListSize + 1 : currentFolderListSize;
   std::string footerText = MediaUtils::FooterTextForBrowser(absoluteIndex, currentFolderListSize);
   MediaDatabinding::getInstance().updateFooterInFolderBrowse(footerText.c_str());
   ETG_TRACE_USR4(("FolderNoUpdateMsg and %s ", footerText.c_str()));
}


}
}
