/**
 *  @file   MediaLanguageHandling.cpp
 *  @author ECV - put5cob
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#include "hall_std_if.h"
#include "Core/Utils/Language/LanguageHandling.h"
#include "MediaDatabinding.h"
#include "Core/Utils/MediaUtils.h"
#include "App/datapool/MediaDataPoolConfig.h"
#include "AppHmi_MediaStateMachine.h"

#define UTFUTIL_S_IMPORT_INTERFACE_GENERIC
#include "utf_if.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS         TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::MediaLanguageHandling::
#include "trcGenProj/Header/LanguageHandling.cpp.trc.h"
#endif
using namespace ::vehicle_main_fi_types;
using namespace ::MPlay_fi_types;

namespace App
{
namespace Core
{

/**
 * @Destructor
 */
MediaLanguageHandling::~MediaLanguageHandling()
{

   if (_commonLanguage) // De register from language common
   {
      _commonLanguage->vUnRegisterforUpdate(this);
   }
   _commonLanguage = NULL;
   _mediaPlayerProxy.reset();
}

/**
@ constroctor

*/
MediaLanguageHandling::MediaLanguageHandling(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy, ::VehicleDataCommonHandler::VehicleDataHandler* commonLanguage)

   : _mediaPlayerProxy(pMediaPlayerProxy), _commonLanguage(commonLanguage)
{

   ETG_TRACE_USR4(("MediaLanguageHandling::in constructor "));
   initializeSystemLanguage();
   if (_commonLanguage) //  register to language common
   {
      ETG_TRACE_USR4(("MediaLanguageHandling::inside Reg constructor"));
      _commonLanguage->vRegisterforUpdate(this);
   }
   setMPlayLanguage(MediaDataPoolConfig::getInstance()->getLanguage());
   ETG_I_REGISTER_FILE();
}

/**
* onLanguageError
*
* @param[in] proxy
* @parm[in] Error
* @return void
*/
void MediaLanguageHandling::onLanguageError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< mplay_MediaPlayer_FI::LanguageError >& /*error*/)
{
   ETG_TRACE_USR4(("MediaLanguageHandling::onLanguageError"));
}

/**
* onLanguageStatus
*
* @param[in] proxy
* @parm[in] status
* @return void
*/
void MediaLanguageHandling::onLanguageStatus(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy>& /*proxy*/,
      const ::boost::shared_ptr< mplay_MediaPlayer_FI::LanguageStatus>& /*status*/)
{
   ETG_TRACE_USR4(("MediaLanguageHandling::onLanguageStatus"));
}

/**
* readLanguageOnStartUp
* language interface to get current language
* @param[in] No
* @return uint8
*/

uint8 MediaLanguageHandling::readLanguageOnStartUp()
{
   uint8 language = 0;

   language = MediaDataPoolConfig::getInstance()->getLanguage();
   //COURIER_MESSAGE_NEW(Courier::SetCultureReqMsg)(Courier::ItemId("en_GB"))->Post();
   ETG_TRACE_USR4(("MediaLanguageHandling::readLanguageOnStartUp %d", language));
   return language;
}

/**
* writeLanguageintoDP
* language interface to get current language
* @param[in] languageIndex
* @return void
*/
//This function has to be removed since the common interface (updateLanguageTable) is provided - pmy5cob
void MediaLanguageHandling::writeLanguageintoDP(uint8 /*languageIndex*/)
{
	/*
	ETG_TRACE_USR4(("MediaLanguageHandling::writeLanguageintoDP %d", languageIndex));

	/*Set media player FI with current language from vaicle data
	MediaDataPoolConfig::getInstance()->setLanguage(languageIndex);
	setMPlayLanguage(languageIndex);
	POST_MSG((COURIER_MESSAGE_NEW(LanguageChangeUpdMsg)()));*/
}

/**
 * updateLanguageTable
 * language interface to get current language
 * @param[in] languageSourceTable
 * @return void
 */

void MediaLanguageHandling::updateLanguageTable(std::vector<T_Language_SourceTable> languageSourceTable)
{
	uint8 languageIndex = static_cast<uint8>(languageSourceTable[T_e8_Language_SourceId__System].getEnLanguage());
	ETG_TRACE_USR4(("MediaLanguageHandling::writeLanguageintoDP %d", languageIndex));

	/*Set media player FI with current language from vaicle data*/
	MediaDataPoolConfig::getInstance()->setLanguage(languageIndex);
	setMPlayLanguage(languageIndex);
	POST_MSG((COURIER_MESSAGE_NEW(LanguageChangeUpdMsg)()));
}

/**
* vInitializeSystemLanguage
* Set Mplay property
* @param[in]
* @return void
*/

void MediaLanguageHandling::initializeSystemLanguage()
{
   ETG_TRACE_USR4(("MediaLanguageHandling::vInitializeSystemLanguage"));

   _languageISOCodeMap[T_e8_Language_Code__Unknown] = 	T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH	;
   _languageISOCodeMap[T_e8_Language_Code__Albanese] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH	;
   _languageISOCodeMap[T_e8_Language_Code__Arabic]  =  T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_ARABIC  		;
   _languageISOCodeMap[T_e8_Language_Code__Bulgarian]	= T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_BULGARIAN	;
   _languageISOCodeMap[T_e8_Language_Code__Chinese_Cantonese_Simplified_Chinese_character]	= T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_STANDARD_CHINESE	   ;
   _languageISOCodeMap[T_e8_Language_Code__Chinese_Cantonese_Traditional_Chinese_character]  = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_TRADITIONAL_CHINESE ;
   _languageISOCodeMap[T_e8_Language_Code__Chinese_Mandarin_Simplified_Chinese_character]  = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_STANDARD_CHINESE      ;
   _languageISOCodeMap[T_e8_Language_Code__Croatian] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_CROATIAN  ;
   _languageISOCodeMap[T_e8_Language_Code__Czech] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_CZECH        ;
   _languageISOCodeMap[T_e8_Language_Code__Danish] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_DANISH  	;
   _languageISOCodeMap[T_e8_Language_Code__Dutch]  = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_DUTCH       ;
   _languageISOCodeMap[T_e8_Language_Code__English_Australian]	= T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH;
   _languageISOCodeMap[T_e8_Language_Code__English_Canadian] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH	;
   _languageISOCodeMap[T_e8_Language_Code__English_India]	= T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH	;
   _languageISOCodeMap[T_e8_Language_Code__English_UK]  = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_UK_ENGLISH	;
   _languageISOCodeMap[T_e8_Language_Code__English_US]  = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH    ;
   _languageISOCodeMap[T_e8_Language_Code__English_US_for_JPN]	= T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH	;
   _languageISOCodeMap[T_e8_Language_Code__English_US_for_PRC]	= T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH	;
   _languageISOCodeMap[T_e8_Language_Code__Estonian] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_ESTONIAN		;
   _languageISOCodeMap[T_e8_Language_Code__Finnish]    = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_FINNISH       	;
   _languageISOCodeMap[T_e8_Language_Code__Flemish] = 	T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH	;
   _languageISOCodeMap[T_e8_Language_Code__French]  = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_FRENCH		;
   _languageISOCodeMap[T_e8_Language_Code__French_Canadian]  = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_FRENCH   ;
   _languageISOCodeMap[T_e8_Language_Code__German] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_GERMAN  		;
   _languageISOCodeMap[T_e8_Language_Code__Greek] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_GREEK   		;
   _languageISOCodeMap[T_e8_Language_Code__Hebrew] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH		;
   _languageISOCodeMap[T_e8_Language_Code__Hindi]	= T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH		;
   _languageISOCodeMap[T_e8_Language_Code__Hungarian]  = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_HUNGARIAN	;
   _languageISOCodeMap[T_e8_Language_Code__Indonesian]	= T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH	;
   _languageISOCodeMap[T_e8_Language_Code__Italian] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_ITALIAN		;
   _languageISOCodeMap[T_e8_Language_Code__Japanese] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_JAPANESE       	;
   _languageISOCodeMap[T_e8_Language_Code__Korean] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_KOREAN 		;
   _languageISOCodeMap[T_e8_Language_Code__Latvian] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_LATVIAN		;
   _languageISOCodeMap[T_e8_Language_Code__Lithuanian]	= T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_LITHUANIAN	;
   _languageISOCodeMap[T_e8_Language_Code__Malay]	= T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH;	;
   _languageISOCodeMap[T_e8_Language_Code__Norwegian] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NORWEGIAN     	;
   _languageISOCodeMap[T_e8_Language_Code__Persian_Farsi_Iranian] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH	;
   _languageISOCodeMap[T_e8_Language_Code__Philippines] = 	T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH	;
   _languageISOCodeMap[T_e8_Language_Code__Polish]  = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_POLISH       	;
   _languageISOCodeMap[T_e8_Language_Code__Portuguese] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_PORTUGUESE     ;
   _languageISOCodeMap[T_e8_Language_Code__Portuguese_Brazilian] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_BRAZILIAN_PORTUGUESE ;
   _languageISOCodeMap[T_e8_Language_Code__Romanian] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_ROMANIAN   	;
   _languageISOCodeMap[T_e8_Language_Code__Russian] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_RUSSIAN  		;
   _languageISOCodeMap[T_e8_Language_Code__Serbian] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_SERBIAN         	;
   _languageISOCodeMap[T_e8_Language_Code__Slovakian] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH  	;
   _languageISOCodeMap[T_e8_Language_Code__Slovenian] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_SLOVENIAN 	;
   _languageISOCodeMap[T_e8_Language_Code__Spanish] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_SPANISH		;
   _languageISOCodeMap[T_e8_Language_Code__Spanish_Latin_American]	= T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_SPANISH;
   _languageISOCodeMap[T_e8_Language_Code__Spanish_Mexican] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_SPANISH   ;
   _languageISOCodeMap[T_e8_Language_Code__Swedish] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_SWEDISH      	;
   _languageISOCodeMap[T_e8_Language_Code__Taiwanese] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH	;
   _languageISOCodeMap[T_e8_Language_Code__Thai]  = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_THAI     		;
   _languageISOCodeMap[T_e8_Language_Code__Turkish] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_TURKISH    	;
   _languageISOCodeMap[T_e8_Language_Code__Ukrainian] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_UKRAINIAN	;
   _languageISOCodeMap[T_e8_Language_Code__Vietnamese]	= T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH	;
   _languageISOCodeMap[T_e8_Language_Code__UnSupported] = T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH	;
   _languageISOCodeMap[T_e8_Language_Code__Unused]	= T_e8_MPlayLanguage__e8LANGUAGE_SELECTION_NA_ENGLISH		;

}

/**
* vSetLanguageProperty
* Set Mplay property
* @param[in] languageIndex
* @return void
*/

void MediaLanguageHandling::setMPlayLanguage(uint8 languageIndex)
{
   ETG_TRACE_USR4(("MediaLanguageHandling::vSetLanguageProperty %d", languageIndex));
   std::map< T_e8_Language_Code, T_e8_MPlayLanguage>::iterator itr = _languageISOCodeMap.find((T_e8_Language_Code)languageIndex);

   if (_mediaPlayerProxy && itr != _languageISOCodeMap.end())
   {
      ETG_TRACE_USR4(("sendLanguageSet %d", itr->second));
      _mediaPlayerProxy->sendLanguageSet(*this, itr->second);
   }
}

}//end of namespace Core
}//end of namespace App



