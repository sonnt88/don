/**
*  @file   MediaMetaDataInfo.h
*  @author ECV - pkm8cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef MEDIAIMETADATAINFO_H_
#define MEDIAIMETADATAINFO_H_

#include "AppHmi_MediaConstants.h"
#include "AppHmi_MediaMessages.h"
#include "CourierTunnelService/CourierMessageReceiverStub.h"
#include "MPlay_fi_types.h"
#include "mplay_MediaPlayer_FIProxy.h"

namespace App {
namespace Core {

enum videoDataFields
{
   TITLE = 0,
   TYPE,
   DATE,
   TIME,
   SIZE,
   RESOLUTION,
   PATH,
   FIELDS_UNKNOWN,
};


class MediaMetaDataInfo
{
   public:
      ~MediaMetaDataInfo();
      static MediaMetaDataInfo* getInstance();
      static void createInstance(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy);
      tSharedPtrDataProvider getMediaMetaDataInfoListDataProvider(const ::MPlay_fi_types::T_MPlayMediaObject& oMediaObject, uint8 infoSettings);

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_MAP_DELEGATE_END()

   private:
      void initialiseMediaMetaData();
      void setVideoMetaData(const ::MPlay_fi_types::T_MPlayMediaObject& oMediaObject);
      void setMediaFileFormat(const ::MPlay_fi_types::T_e8_MPlayFileFormat _fileFormat);
      void setPhotoMetaData(const ::MPlay_fi_types::T_MPlayMediaObject& oMediaObject);
      const char* getlistItemTemplateForMediaMetaDataInfo(uint32& index);
      Candera::String getMediaMetaDataInfoItemValue(uint8 ItemIndex);
      std::string _mediaMetaData[FIELDS_UNKNOWN];
      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& _mediaPlayerProxy;
      static MediaMetaDataInfo* _theInstance;
      MediaMetaDataInfo(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy);
};


}
}


#endif /* MEDIAIMETADATAINFO_H_ */
