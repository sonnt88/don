/*
 * AppHmi_MediaTypes_Common.h
 *
 *  Created on: Dec 7, 2015
 *      Author: asd8cob
 */

#ifndef APPHMI_MEDIATYPES_COMMON_H_
#define APPHMI_MEDIATYPES_COMMON_H_


// =========================================================================
enum enMediaSrc {
    SOURCE_CD = 0,
    SOURCE_AUX,
    SOURCE_BT,
    SOURCE_IPOD,
    SOURCE_USB,
    SOURCE_CDMP3,
    SOURCE_SPI,
    SOURCE_NOT_DEF
};


#endif /* APPHMI_MEDIATYPES_COMMON_H_ */
