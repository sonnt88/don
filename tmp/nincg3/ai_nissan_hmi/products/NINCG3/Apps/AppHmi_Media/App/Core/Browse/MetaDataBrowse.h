/**
*  @file   MetaDataBrowse.h
*  @author ECV - vma6cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef METADATABROWSE_H_
#define METADATABROWSE_H_

#include "IBrowse.h"


namespace App {
namespace Core {

struct ListHistoryInfo
{
   uint32 listId;
   uint32 windowStartIndex;
};


class MetaDataBrowse : public IBrowse
{
   public:
      MetaDataBrowse();
      ~MetaDataBrowse();
      tSharedPtrDataProvider getDataProvider(RequestedListInfo& requestedlistInfo, uint32 activeDeviceType);
      void populateNextListOrPlayItem(uint32 currentListId, uint32 selectedItem, uint32 activeDeviceType, uint32 activeDeviceTag);
      void populatePreviousList(uint32 deviceTag);
      int32 getActiveItemIndex(int32 selectedIndex);

      void updateListData(void);
      void updateEmptyListData(uint32 _currentListType);
      void initializeListParameters();
      void setWindowStartIndex(uint32);
      void populateCurrentPlayingList(uint32 /*currentPlayingListHandle*/);
      void setMetadataBrowseListId(uint32);
      void setFocussedIndex(int32);
      uint32 getNextListId(uint32 currentListId, uint32 selectedRow, uint32 activeDeviceType);
      void setFooterDetails(uint32);
#ifdef VERTICAL_KEYBOARD_SEARCH_SUPPORT
      void updateSearchKeyBoardListInfo(trSearchKeyboard_ListItem /*item*/) {}
      bool updateVerticalListToGuiList(const ButtonListItemUpdMsg& /*oMsg*/)
      {
         return true;
      }
      void SetActiveVerticalKeyLanguague() {}
      void ShowVerticalKeyInfo(bool /*pDefault*/) {}
#endif
#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
      uint32 getActiveQSIndex(uint32 absoluteIndex);
#endif

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_MAP_DELEGATE_END()

   private:
      uint32 getPrevListId();
      uint32 getPrevListWindowStartIndex();
      const char* getlistItemTemplateForMetaDataBrowse(uint32& index);

      void switchtoPlayScreen();
      void updateBrowsingHistory(ListHistoryInfo& historyInfo);
      void playSelectedItem(uint32 selectedIndex);
      void populateNextList(uint32 currentListId, uint32 activeItemIndex, uint32 activeDeviceTag);
      Candera::String getCurrentListHeaderText();
      void updateHeaderTextToHistory(Candera::String headerText);
      void HideThumbnailImageInMetadataBrowse(uint32);
      Candera::String getHeaderText(uint32 activeItemIndex);
      std::string getMetaDataFieldText(uint32 listSliceIndex);
      tSharedPtrDataProvider fillListItems();
      void setAllFlag();
      uint32 getWidnowSize(void);
      uint32 gettotalListSize(void);
      void scrollListToWindow(uint32 windowStartIndex);
      Candera::String getListTextForAll(uint32 currentListId);
      Candera::String getHeaderTextBySelectedIndex(uint32, uint32);
      bool _isAllset;
      std::vector<uint32> PlayableStatus;
      std::vector<ListHistoryInfo> _listBrowsingHistory;
      std::vector<Candera::String> _headerHistory;
      uint32 _nextListId;
      bool _isBackTraversing;
      uint32 prevListWindowStartIndex;
#ifdef METADATA_THUMBNAIL_ALBUMART_SUPPORT
      Courier::DataBindingUpdater<ThumbNailAlbumArtDataBindingSource>* ptrThumbNail;
#endif
};


}
}


#endif /* METADATABROWSE_H_ */
