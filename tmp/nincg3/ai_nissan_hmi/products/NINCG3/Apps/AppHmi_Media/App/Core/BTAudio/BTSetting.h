/**
 *  @file   BTSetting.h
 *  @author ECV - kng3kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#ifndef BTSETTING_H_
#define BTSETTING_H_

#include "IBTSettings.h"
#include "MOST_Tel_FIProxy.h"
#include "AppBase/ServiceAvailableIF.h"
#include "most_Tel_fi_types.h"

namespace App {
namespace Core {

class BTSetting
   : public IBTSettings
   , public hmibase::ServiceAvailableIF
   , public StartupSync::PropertyRegistrationIF
   , public ::MOST_BTSet_FI::DeviceListExtendedCallbackIF
   , public ::MOST_BTSet_FI::BluetoothAudioSourceCallbackIF
   , public ::MOST_BTSet_FI::GetDeviceInfoExtendedCallbackIF
   , public ::MOST_Tel_FI::CallStatusNoticeCallbackIF


{
   public:

      BTSetting();
      virtual ~BTSetting();

      virtual void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                                      const asf::core::ServiceStateChange& stateChange);
      virtual void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                                        const asf::core::ServiceStateChange& stateChange);

      virtual void onDeviceListExtendedError(const ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy >& /*proxy*/,
                                             const ::boost::shared_ptr< ::MOST_BTSet_FI::DeviceListExtendedError >& /*error*/);
      virtual void onDeviceListExtendedStatus(const ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy >& /*proxy*/,
                                              const ::boost::shared_ptr< ::MOST_BTSet_FI::DeviceListExtendedStatus >& status);

      virtual void onBluetoothAudioSourceError(const ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< ::MOST_BTSet_FI::BluetoothAudioSourceError >& /*error*/);
      virtual void onBluetoothAudioSourceStatus(const ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< ::MOST_BTSet_FI::BluetoothAudioSourceStatus >& status);

      virtual void onGetDeviceInfoExtendedError(const ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< ::MOST_BTSet_FI::GetDeviceInfoExtendedError >& /*error*/);
      virtual void onGetDeviceInfoExtendedResult(const ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< ::MOST_BTSet_FI::GetDeviceInfoExtendedResult >& result);

      virtual void onCallStatusNoticeError(const ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy >& /*proxy*/,
                                           const ::boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeError >& /*error*/);
      virtual void onCallStatusNoticeStatus(const ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy >& /*proxy*/,
                                            const ::boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeStatus >& status);

      virtual bool getDeviceConnectionStatus();
      virtual void trigger_BTDefaultScreen();

      static void tracecmd_ShowBTDefaultScreen();
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_MAP_DELEGATE_END()

   private:

      virtual void updateTrackInfoInBTMain();
      void onDeviceListExtendedStatusResult(::most_BTSet_fi_types_Extended::T_BTSetDeviceListExtendedResult&);

      ::boost::shared_ptr< ::MOST_BTSet_FI::MOST_BTSet_FIProxy > _btSettingProxy;
      ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > _telephoneProxy;
      std::string _btDeviceName;
      uint8 _connectedDeviceHandle;
      uint8 _prevDeviceHandle;
      most_BTSet_fi_types::T_e8_BTSetAudioStreamingSupportType _DeviceAudioStreamingSupportType;
      bool _isDeviceConnected;
      static BTSetting* _btSettinsInstance;
};


}
}


#endif /* BTSETTING_H_ */
