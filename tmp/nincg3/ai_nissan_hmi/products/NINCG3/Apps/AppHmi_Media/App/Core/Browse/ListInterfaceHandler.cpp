/**
 *  @file   ListInterfaceHandler.cpp
 *  @author ECV - chs4cob
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */


#include "hall_std_if.h"
#include "ListInterfaceHandler.h"
#include "Core/Utils/MediaUtils.h"
//#include "Core/Utils/Browse/BrowseUtils.h"
#include "BrowseFactory.h"
#include "Core/Utils/MediaProxyUtility.h"
#include "Core/PlayScreen/IPlayScreen.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS         TR_CLASS_APPHMI_MEDIA_HALL_BROWSE
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::ListInterfaceHandler::
#include "trcGenProj/Header/ListInterfaceHandler.cpp.trc.h"
#endif

namespace App {
namespace Core {
using namespace ::mplay_MediaPlayer_FI;
using namespace ::MPlay_fi_types;
static const unsigned int MODIFIED_LIST_ID_FOLDER_BROWSE = (LIST_ID_BROWSER_FOLDER_BROWSE % MEDIA_BASE_LIST_ID);
static const int FILTER_NOT_REQUIRED = -1;

int filterListMap[][3] =
{
   /*0*/  FILTER_NOT_REQUIRED,               FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*1*/  FILTER_NOT_REQUIRED,               FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*2*/  FILTER_NOT_REQUIRED,               FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*3*/  FILTER_NOT_REQUIRED,               FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*4*/  T_e8_MPlayListType__e8LTY_GENRE,   FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*5*/  T_e8_MPlayListType__e8LTY_GENRE,   T_e8_MPlayListType__e8LTY_GENRE_ARTIST, FILTER_NOT_REQUIRED,
   /*6*/  T_e8_MPlayListType__e8LTY_GENRE,   T_e8_MPlayListType__e8LTY_GENRE_ARTIST, T_e8_MPlayListType__e8LTY_GENRE_ARTIST_ALBUM,
   /*7*/  T_e8_MPlayListType__e8LTY_GENRE,   T_e8_MPlayListType__e8LTY_GENRE_ARTIST, FILTER_NOT_REQUIRED,
   /*8*/  T_e8_MPlayListType__e8LTY_GENRE,   FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*9*/  T_e8_MPlayListType__e8LTY_GENRE,   FILTER_NOT_REQUIRED,                    T_e8_MPlayListType__e8LTY_GENRE_ALBUM,
   /*10*/  T_e8_MPlayListType__e8LTY_GENRE,   FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*11*/  FILTER_NOT_REQUIRED,               T_e8_MPlayListType__e8LTY_ARTIST,       FILTER_NOT_REQUIRED,
   /*12*/  FILTER_NOT_REQUIRED,               T_e8_MPlayListType__e8LTY_ARTIST,       T_e8_MPlayListType__e8LTY_ARTIST_ALBUM,
   /*13*/  FILTER_NOT_REQUIRED,               T_e8_MPlayListType__e8LTY_ARTIST,       FILTER_NOT_REQUIRED,
   /*14*/  FILTER_NOT_REQUIRED,               FILTER_NOT_REQUIRED,                    T_e8_MPlayListType__e8LTY_ALBUM,
   /*15*/  FILTER_NOT_REQUIRED,               FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*16*/  T_e8_MPlayListType__e8LTY_PODCAST, FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*17*/  FILTER_NOT_REQUIRED,               FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*18*/  FILTER_NOT_REQUIRED,               T_e8_MPlayListType__e8LTY_AUDIOBOOK,    FILTER_NOT_REQUIRED,
   /*19*/  FILTER_NOT_REQUIRED,               FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*20*/  FILTER_NOT_REQUIRED,               FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*21*/  FILTER_NOT_REQUIRED,               FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*22*/  FILTER_NOT_REQUIRED,               FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*23*/  T_e8_MPlayListType__e8LTY_COMPOSER, FILTER_NOT_REQUIRED,                   FILTER_NOT_REQUIRED,
   /*24*/  T_e8_MPlayListType__e8LTY_COMPOSER, T_e8_MPlayListType__e8LTY_COMPOSER_ALBUM, FILTER_NOT_REQUIRED,
   /*25*/  T_e8_MPlayListType__e8LTY_COMPOSER, FILTER_NOT_REQUIRED,                   FILTER_NOT_REQUIRED,
   /*26*/  FILTER_NOT_REQUIRED,               FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*27*/  FILTER_NOT_REQUIRED,               FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*28*/  FILTER_NOT_REQUIRED,               FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
   /*29*/  T_e8_MPlayListType__e8LTY_PLAYLIST, FILTER_NOT_REQUIRED,                   FILTER_NOT_REQUIRED,
   /*30*/  FILTER_NOT_REQUIRED,               FILTER_NOT_REQUIRED,                    FILTER_NOT_REQUIRED,
};


/**
 * @Constructor
 */
ListInterfaceHandler::ListInterfaceHandler(::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& mediaPlayerProxy,
      BrowserSpeedLock* browserSpeedLock, IPlayScreen* pPlayScreen)
   : _mediaPlayerProxy(mediaPlayerProxy)
   , _iPlayScreen(*pPlayScreen)
   , _currentFolderPath("/")
   , _currentPlaylist("")
   , _currentListType(0)
   , _deviceTag(MEDIA_VIRTUAL_DEVICE_TAG)
   , _currentStateMachineState(IDLE)
   , _folderRequestType(FTS_FOLDER)
   , _isWaitingForListCompletion(false)
   , _browserSpeedLock(browserSpeedLock)
   , _isNewList(false)
   , _isBackTraversing(false)
//   , _btCurrentPlayingListStatus(false)
{
   _folderListMap.clear();
   _listMap.clear();
   _currListHandel = 0;
   _currLanguage = 0xFF;
}


/**
 * @Destructor
 */
ListInterfaceHandler::~ListInterfaceHandler()
{
   _mediaPlayerProxy.reset();
   _currentListType = 0;
   _deviceTag = 0;
   _currentStateMachineState = IDLE;
   _folderListMap.clear();
   _listMap.clear();
   _browserSpeedLock = NULL;
   //   _btCurrentPlayingListStatus = false;
}


/**
 * registerProperties - Trigger property registration to mediaplayer
 * @param[in] None
 * @return void
 */
void ListInterfaceHandler::registerProperties()
{
   _mediaPlayerProxy->sendMediaPlayerListChangeUpReg(*this);
}


/**
 * deregisterProperties - Trigger property deregistration to mediaplayer
 * @param[in] proxy
  * @return void
 */
void ListInterfaceHandler::deregisterProperties()
{
   _mediaPlayerProxy->sendMediaPlayerListChangeRelUpRegAll();
}


/**
 * PlayitemRequest - Function to play the selected song
 *
 * @param[in] u32Index - Selected Row number from list
 * @parm[out] none
 * @return void
 */
void ListInterfaceHandler::requestPlayItem(uint32 selectedIndex)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::vPlayitemRequest(u16Index = %d)", selectedIndex));
   if (_listMap.count(_currentListType))
   {
      uint32 listHandle = _listMap[_currentListType]->u32GetListHandle();
      uint32 tag = _listMap[_currentListType]->u32GetTagbyIndex(selectedIndex);
      if (_mediaPlayerProxy)
      {
         _mediaPlayerProxy->sendPlayItemFromListByTagStart(*this, listHandle, tag);
      }
   }
   else
   {
      ETG_TRACE_USR4(("list type %d not available in map", _currentListType));
   }
}


/**
 * onPlayItemFromListByTagResult - function called when mediaplayer gets a result during the method call of PlayItemFromListByTagStart.
 *
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void ListInterfaceHandler::onPlayItemFromListByTagResult(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& proxy, const boost::shared_ptr< PlayItemFromListByTagResult >& result)
{
   (void)proxy;
   (void)result;
   ETG_TRACE_USR4(("ListInterfaceHandler::onPlayItemFromListByTagResult"));
}


/**
 * onPlayItemFromListByTagError - function called when mediaplayer gets a result during the method call of PlayItemFromListByTagStart.
 *
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void ListInterfaceHandler::onPlayItemFromListByTagError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& proxy, const boost::shared_ptr< PlayItemFromListByTagError >& error)
{
   (void)proxy;
   (void)error;
   ETG_TRACE_USR4(("ListInterfaceHandler::onPlayItemFromListByTagError"));
}


/**
 * setActiveIndex - Function to set the selected index in a list
 *
 * @param[in] _activeIndex - Selected Row number from list
 * @parm[out] none
 * @return void
 */
void ListInterfaceHandler::setActiveIndex(uint32 activeIndex)
{
   if (_listMap.count(_currentListType))
   {
      _listMap[_currentListType]->vSetActiveItemIndex(activeIndex);
   }
}


/**
 * getTextBySelectedIndex - Function to get the selected text
 *
 * @param[in] selectedIndex - Selected Row from list
 * @parm[out] none
 * @return void
 */
Candera::String ListInterfaceHandler::getTextBySelectedIndex(uint32 selectedIndex)
{
   Candera::String selectedText;
   if (_listMap.count(_currentListType))
   {
      selectedText = _listMap[_currentListType]->getListTextByIndex(selectedIndex);
   }
   return selectedText;
}


bool ListInterfaceHandler::isListWaitingToComplete(void)
{
   return _isWaitingForListCompletion;
}


void ListInterfaceHandler::requestChildIndexList(uint32 nextListId, uint32 activeDeviceTag)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::requestChildIndexList"));

   _currentListType = nextListId % MEDIA_BASE_LIST_ID;
   _isWaitingForListCompletion = true;

   createInstanceIfNull(_currentListType);
   _deviceTag = activeDeviceTag;

   if (_listMap.count(_currentListType))
   {
      _listMap[_currentListType]->vSetCurrentStartIndex(FLEX_LIST_START_INDEX);
      _listMap[_currentListType]->vSetCurrentBufferSize(FLEX_LIST_MAX_BUFFER_SIZE);
   }
   initStatemachine();
   triggerStatemachine();
}


void ListInterfaceHandler::requestParentIndexList(uint32 prevListId)
{
   _currentListType = prevListId % MEDIA_BASE_LIST_ID;

   _isWaitingForListCompletion = true;
   _isBackTraversing = true;

   if (_listMap.count(_currentListType))
   {
      _listMap[_currentListType]->vSetCurrentStartIndex(FLEX_LIST_START_INDEX);
      _listMap[_currentListType]->vSetCurrentBufferSize(FLEX_LIST_MAX_BUFFER_SIZE);

      IBrowse* ibrowse = BrowseFactory::getBrowser(_currentListType + MEDIA_BASE_LIST_ID);
      _browserSpeedLock->setFocussedIndex(-1);
      uint32 nextListId = ibrowse->getNextListId(_currentListType + MEDIA_BASE_LIST_ID, 0, 0);
      _browserSpeedLock->setNextListId(nextListId);
      uint32 tag1 = 0;
      uint32 tag2 = 0;
      uint32 tag3 = 0;
      getFiltersToRequestListByIndex(nextListId % MEDIA_BASE_LIST_ID, 0, tag1, tag2, tag3);
      _browserSpeedLock->setFilterTag(tag1, tag2, tag3);
   }
   initStatemachine();
   triggerStatemachine();
}


void ListInterfaceHandler::requestCurrentPlayingList(uint32 currentPlayingListHandle)
{
   ETG_TRACE_USR4(("ListInterfaceHandler:requestCurrentPlayingList:%d", currentPlayingListHandle));
   _mediaPlayerProxy->sendRequestListInformationStart(*this, currentPlayingListHandle);
}


/**
 * requestListData - Function to populate the list
 * @param[in] const RequestedListInfo& -
 * @parm[out] none
 * @return void
 */
void ListInterfaceHandler::requestListData(const RequestedListInfo& requestedListInfo)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::vRequestListData(u8ListType = %d, u32Index = %d) waitinglist status %d",
                   requestedListInfo._listType, requestedListInfo._startIndex, _isWaitingForListCompletion));

   _currentListType = requestedListInfo._listType % MEDIA_BASE_LIST_ID;
   createInstanceIfNull(_currentListType);
   _deviceTag = requestedListInfo._deviceTag;

   if (!_isWaitingForListCompletion)
   {
      if (_listMap.count(_currentListType))
      {
         _listMap[_currentListType]->vSetCurrentStartIndex(requestedListInfo._startIndex);
         _listMap[_currentListType]->vSetCurrentBufferSize(requestedListInfo._bufferSize);
      }
      initStatemachine();
      triggerStatemachine();
   }
   else
   {
      uint32 listsize = _listMap[_currentListType]->u32GetListSize();
      IBrowse* ibrowse = BrowseFactory::getBrowser(_currentListType + MEDIA_BASE_LIST_ID);

      _isWaitingForListCompletion = false;
      if (listsize > 0)
      {
         updateIndexListSliceToGuiList();
      }
      else
      {
         POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
         MediaDatabinding::getInstance().updateListSizeInMetaDataBrowse(0.0f, 414.0f);
         MediaDatabinding::getInstance().updatePlayAllSongButtonVisibility(false);
         ibrowse->updateEmptyListData(_currentListType);
#ifdef BROWSER_FOOTER_DEFAULT_VALUES_SUPPORT
         updateBrowserFooterWithDefaultValues();
#endif
#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
         MediaDatabinding::getInstance().updateQuickSearchAvailabeStatus(false);
#endif
      }
   }
}


/**
 * ReleaseListHandle - Function to set the release the list handle for the list
 * @param[in] uint32 - listhandle
 * @parm[out] none
 * @return void
 */
void ListInterfaceHandler::releaseListHandle(uint32 listHandle)
{
   if (listHandle != 0 && (_mediaPlayerProxy->getNowPlaying().getU32ListHandle() != listHandle))
   {
      _mediaPlayerProxy->sendReleaseMediaPlayerListStart(*this, listHandle);
   }
}


/**
 * removeListFromMap - Function to remove the previous list from map
 * @param[in] none
 * @parm[out] none
 * @return void
 */
void ListInterfaceHandler::removeListFromMap()
{
   std::map <uint32, ListUtilityDatabase*>::iterator iter = _listMap.find(_currentListType);
   if (iter != _listMap.end())
   {
      uint32 listHandle = (iter->second)->u32GetListHandle();
      releaseListHandle(listHandle);
      delete(iter->second);
      _listMap.erase(iter);
   }
}


void ListInterfaceHandler::onRequestListInformationError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< RequestListInformationError >& /*error*/)
{
}


void ListInterfaceHandler::onRequestListInformationResult(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< RequestListInformationResult >& result)
{
   ETG_TRACE_USR4(("RequestListInformation_ListTyp:%d", result->getE8ListType()));
   ETG_TRACE_USR4(("RequestListInformation_DevTag:%d", result->getU8DeviceTag()));
   ETG_TRACE_USR4(("RequestListInformation_ListSize:%d", result->getU32ListSize()));

   //::MPlay_fi_types::T_e8_MPlayDeviceType currentDeviceType = _mediaPlayerProxy->getNowPlaying().getOMediaObject().getE8DeviceType();
   IBrowse* ibrowse = NULL;

   //if (currentDeviceType != ::MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH)
   //{
   _currentListType = result->getE8ListType();
   ibrowse = BrowseFactory::getBrowser(LIST_ID_BROWSER_METADATA_BROWSE);
   ibrowse->setMetadataBrowseListId(_currentListType);
   //}
   //else
   //{
   //   _currentListType = MODIFIED_LIST_ID_FOLDER_BROWSE;
   //   ibrowse = BrowseFactory::getBrowser(LIST_ID_BROWSER_FOLDER_BROWSE);
   //}
   _deviceTag = result->getU8DeviceTag();
   createInstanceIfNull(_currentListType);

   if (_listMap.count(_currentListType))
   {
      _listMap[_currentListType]->vSetCurrentStartIndex(FLEX_LIST_START_INDEX);
      _listMap[_currentListType]->vSetCurrentBufferSize(FLEX_LIST_MAX_BUFFER_SIZE);
      _listMap[_currentListType]->vSetListSize(result->getU32ListSize());
      _listMap[_currentListType]->vSetListHandle(_mediaPlayerProxy->getNowPlaying().getU32ListHandle());
   }
   _isWaitingForListCompletion = true;
   initStatemachine();
   triggerStatemachine();
}


/**
 * onReleaseMediaPlayerListError - function called when mediaplayer gets a result during the method call of ReleaseMediaPlayerListStart.
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void ListInterfaceHandler::onReleaseMediaPlayerListError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ReleaseMediaPlayerListError >& /*error*/)
{
}


/**
 * onCreateMediaPlayerIndexedListResult - function called when mediaplayer gets a result during the method call of ReleaseMediaPlayerListStart.
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void ListInterfaceHandler::onReleaseMediaPlayerListResult(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ReleaseMediaPlayerListResult >& /*result*/)
{
   ETG_TRACE_USR4(("List handle is successfully released"));
}


/**
 *  insertNewUtilityDatabaseToListMap - inserts the listUtilityDatabase for a 'listType' to the '_listMap'
 *  @param [in] listType
 *  @param [in] listUtilityDatabase
 *  @return void
 */
void ListInterfaceHandler::insertNewUtilityDatabaseToListMap(uint32 listType, ListUtilityDatabase* listUtilityDatabase)
{
   if (listUtilityDatabase)
   {
      _listMap.insert(std::pair<uint32, ListUtilityDatabase* >(listType, listUtilityDatabase));
   }
   else
   {
      ETG_TRACE_USR4(("instance creation failed for list id %d", listType));
   }
}


/**
 * vCreateInstanceIfNull - Creats a new database for list type
 * @param[in] u8ListType
 * @parm[out] none
 * @return void
 */
void ListInterfaceHandler::createInstanceIfNull(uint32 listType)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::vCreateInstanceIfNull(%d)", listType));
   std::map<uint32, ListUtilityDatabase*>::iterator Iter_DB = _listMap.find(listType);
   if (Iter_DB == _listMap.end())
   {
      ListUtilityDatabase* listUtilityDatabase = new ListUtilityDatabase();
      insertNewUtilityDatabaseToListMap(listType, listUtilityDatabase);
   }
   else
   {
      ETG_TRACE_USR4(("vCreateInstanceIfNull() --> ListUtilityDatabase is already created"));
   }
}


/**
 * vIntStatemachine - Initializes the process of fetching list
 * @param[in] none
 * @parm[out] none
 * @return void
 */
void ListInterfaceHandler::initStatemachine()
{
   ETG_TRACE_USR4(("vIntStatemachine _currentStateMachineState %d", _currentStateMachineState));

   if (_currentStateMachineState == LIST_REQUEST_PROCESS_DONE)
   {
      _currentStateMachineState = WAITING_PLAYERINDEX_METHODRESULT;
   }
   else
   {
      _currentStateMachineState = IDLE;
   }
}


/**
 * setCurrentStateMachineState - This is a setter function for the local variable _currentStateMachineState
 * @param[in] uint8 state
 * @parm[out] none
 * @return void
 */
void ListInterfaceHandler::setCurrentStateMachineState(uint8 state)
{
   _currentStateMachineState = state;
}


/**
 * vTriggerStatemachine - Fetches the sliced list
 * @param[in] none
 * @parm[out] none
 * @return void
 */
void ListInterfaceHandler::triggerStatemachine()
{
   ETG_TRACE_USR4(("ListInterfaceHandler::vTriggerStatemachine(+) m_currentStateMachineState = %d and listtype %d ", _currentStateMachineState, _currentListType));
   switch (_currentStateMachineState)
   {
      case IDLE:
      {
#ifdef HMI_CDDA_SUPPORT
         if (MediaProxyUtility::getInstance().GetDeviceType(_deviceTag) == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA)
         {
            ETG_TRACE_USR4(("List Created for CDDA"));
            requestMediaPlayerCDDAList();
         }
         else
         {
            requestMediaPlayerList();
         }
#else
         requestMediaPlayerList();
#endif
         _currentStateMachineState = WAITING_PLAYERINDEX_METHODRESULT;
         break;
      }

      case WAITING_PLAYERINDEX_METHODRESULT:
      {
         requestMediaPlayerSliceList();
         _currentStateMachineState = WAITING_PLAYERINDEXLISTSLICE_METHODRESULT;
         break;
      }

      case WAITING_PLAYERINDEXLISTSLICE_METHODRESULT:
      {
         _currentStateMachineState = LIST_REQUEST_PROCESS_DONE;
         break;
      }

      default:
      {
         break;
      }
   }
}


/**
 * requestMediaPlayerList - Request mediaplayer list based on the list type request from the user
 *
 * @param[in] none
 * @parm[out] none
 * @return void
 */
void ListInterfaceHandler::requestMediaPlayerList()
{
   if (_currentListType == MODIFIED_LIST_ID_FOLDER_BROWSE)
   {
      switch (_folderRequestType)
      {
         case FTS_FOLDER:
         case FTS_FOLDER_UP:
         {
            requestMediaPlayerFileList(_currentFolderPath);
            break;
         }
         case FTS_PLAYLIST:
         {
            requestMediaPlayerPlaylist(_currentPlaylist);
            break;
         }

         default:
         {
            break;
         }
      }//end of switch block
   }//end of if block, it is applicable only for folder browser requests
   else
   {
      requestMediaPlayerIndexedList(_currentListType);
   }
}


/**
 * requestMediaPlayerSliceList - Requests sliced mediaplayer list based on the list type request from the user.
 *
 * @param[in] none
 * @parm[out] none
 * @return void
 */
void ListInterfaceHandler::requestMediaPlayerSliceList()
{
   if (_listMap.count(_currentListType))
   {
      uint32 startIndex = _listMap[_currentListType]->u32GetCurrentStartIndex();
      uint32 listHandle = _listMap[_currentListType]->u32GetListHandle();
      uint32 windowSize = _listMap[_currentListType]->u32GetCurrentBufferSize();
      uint32 listSize = _listMap[_currentListType]->u32GetListSize();

      _browserSpeedLock->setCurrentListType(_currentListType + MEDIA_BASE_LIST_ID);
      _browserSpeedLock->setCurrentListHandle(listHandle);
      ETG_TRACE_USR4(("list size and start index %d and %d ", listSize, startIndex));
      if ((startIndex + windowSize) > listSize)
      {
         ETG_TRACE_USR4(("ListInterfaceHandler::Current Buffer Exceed Total List Size"));
         windowSize = listSize - startIndex;
         _listMap[_currentListType]->vSetCurrentBufferSize(windowSize);
      }

      if (_currentListType == MODIFIED_LIST_ID_FOLDER_BROWSE)
      {
         switch (_folderRequestType)
         {
            case FTS_FOLDER :
            case FTS_FOLDER_UP:
            {
               requestMediaPlayerFileListSlice(listHandle, startIndex, windowSize);
               break;
            }
            case FTS_PLAYLIST:
            {
               requestMediaPlayerPlaylistSlice(listHandle, startIndex, windowSize);
               break;
            }

            default:
            {
               break;
            }
         }//end of switch block
      }
      else // meta data list slice requested
      {
         requestMediaPlayerIndexedListSlice(listHandle, startIndex, windowSize);
      }
   }
   else
   {
      ETG_TRACE_USR4(("Not in the list map"));
   }
}


uint32 ListInterfaceHandler::getCurrentStartIndex(uint32 listid)
{
   if (_listMap.count(listid))
   {
      return _listMap[listid]->u32GetCurrentStartIndex();
   }
   return 0;
}


/**
 * getCurrentFolderPath - The function will return the current folder path.
 * @param[in] none
 * @return std::string - current folder path
 */
std::string ListInterfaceHandler::getCurrentFolderPath()  const
{
   return _currentFolderPath;
}


/**
 * setCurrentFolderPath - The function will set the current folder path with new folder path.
 * @param[in] std::string new folder path
 * @return void
 */
void ListInterfaceHandler::setCurrentFolderPath(std::string setDesiredFolderPath)
{
   _currentFolderPath = setDesiredFolderPath;
}


/**
 * setCurrentPlaylistPath - The function will set the current playlist path with new folder path. This is applicable only for folder browsing
 * @param[in] std::string new folder path
 * @return void
 */
void ListInterfaceHandler::setCurrentPlaylistPath(std::string setDesiredPlaylistPath)
{
   _currentPlaylist = setDesiredPlaylistPath;
}


/**
 * isRootFolder - return true if current folder browser is in root folder
 * @param[in] none
 * @return bool - true if currently root folder otherwise false
 */
bool ListInterfaceHandler::isRootFolder()
{
   int32 currentFolderLevel = std::count(_currentFolderPath.begin(), _currentFolderPath.end(), '/');
   bool isCurrentFolderBrowserInRoot = false;

   if ((currentFolderLevel == 1) && (_currentFolderPath.compare("/") == 0))
   {
      isCurrentFolderBrowserInRoot = true;
   }
   ETG_TRACE_USR4(("isCurrentFolderBrowserInRoot = %d ", isCurrentFolderBrowserInRoot));
   return isCurrentFolderBrowserInRoot;
}


/**
 * vRequestMediaPlayerFileList - Invokes a method start to media player
 *
 * @param[in] strPath
 * @parm[out] none
 * @return void
 */
void ListInterfaceHandler::requestMediaPlayerFileList(std::string strPath)
{
   uint32 activeDevicetype = MediaProxyUtility::getInstance().getActiveDeviceType();
   ETG_TRACE_USR4(("ListInterfaceHandler::vRequestMediaPlayerFileList(path:%s)", strPath.c_str()));
   ETG_TRACE_USR4(("ListInterfaceHandler::vRequestMediaPlayerFileList (%d _deviceTag) (%d _deviceType)", _deviceTag, activeDevicetype));

   if (_mediaPlayerProxy)
   {
      if (activeDevicetype == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_MTP && (!strPath.empty()))					// For MT devices the / at the end is extra so we have to remove the last character
      {
         // Ex:"/Internal memory" for MTP and /Internal memory/ for USB devices
         strPath = strPath.substr(0, strPath.size() - 1);
         ETG_TRACE_USR4(("Truncated folder path request for MTP only %s", strPath.c_str()));
      }
      _mediaPlayerProxy->sendCreateMediaPlayerFileListStart(*this, strPath, _deviceTag, (::MPlay_fi_types::T_e8_MPlayFileTypeSelection)FTS_ALL);
      POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_START)));

      _browserSpeedLock->setFocussedIndex(0);  // when new list is entered reset the focussed index
      _isNewList = true;
   }
}


/**
 * RequestMediaPlayerPlaylist - Invokes a method start to media player for playlist retrieval
 *
 * @param[in] strPlaylistName
 * @parm[out] none
 * @return void
 */
void ListInterfaceHandler::requestMediaPlayerPlaylist(const std::string strPlaylistName)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::RequestMediaPlayerPlaylist(path:%s) ", strPlaylistName.c_str()));
   if (_mediaPlayerProxy)
   {
      _mediaPlayerProxy->sendCreateMediaPlayerPlaylistListStart(*this, strPlaylistName);
      POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_START)));
   }
}


/**
 * getFiltersToRequestIndexedList - Function to get the filters for each list type
 * @param[in] ListType
 * @parm[out] none
 * @return void
 */
void ListInterfaceHandler::getFiltersToRequestIndexedList(uint32 listType, uint32& tag1, uint32& tag2, uint32& tag3)
{
   int filterListType1 = filterListMap[listType][0];
   int filterListType2 = filterListMap[listType][1];
   int filterListType3 = filterListMap[listType][2];

   if ((filterListType1 != FILTER_NOT_REQUIRED) && (_listMap.count(filterListType1)))
   {
      tag1 = _listMap[filterListType1]->u32GetActiveItemTag();
   }

   if ((filterListType2 != FILTER_NOT_REQUIRED) && (_listMap.count(filterListType2)))
   {
      tag2 = _listMap[filterListType2]->u32GetActiveItemTag();
   }

   if ((filterListType3 != FILTER_NOT_REQUIRED) && (_listMap.count(filterListType3)))
   {
      tag3 = _listMap[filterListType3]->u32GetActiveItemTag();
   }
}


/**
 * getFiltersToRequestIndexedList - Function to get the filters for each list type
 * @param[in] ListType
 * @parm[out] none
 * @return void
 */
void ListInterfaceHandler::getFiltersToRequestListByIndex(uint32 listType, uint32 index, uint32& tag1, uint32& tag2, uint32& tag3)
{
   int filterListType1 = filterListMap[listType][0];
   int filterListType2 = filterListMap[listType][1];
   int filterListType3 = filterListMap[listType][2];

   if ((filterListType1 != FILTER_NOT_REQUIRED) && (_listMap.count(filterListType1)))
   {
      tag1 = _listMap[filterListType1]->u32GetTagbyIndex(index);
   }

   if ((filterListType2 != FILTER_NOT_REQUIRED) && (_listMap.count(filterListType2)))
   {
      tag2 = _listMap[filterListType2]->u32GetTagbyIndex(index);
   }

   if ((filterListType3 != FILTER_NOT_REQUIRED) && (_listMap.count(filterListType3)))
   {
      tag3 = _listMap[filterListType3]->u32GetTagbyIndex(index);
   }
}


/**
 * requestMediaPlayerIndexedList - Invokes a method start to get indexed list
 *
 * @param[in] ListType
 * @parm[out] none
 * @return void
 */
void ListInterfaceHandler::requestMediaPlayerIndexedList(uint32 listType)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::RequestMediaPlayerIndexedList(ListType =%d )", listType));
   uint32 filterTag1 = 0;
   uint32 filterTag2 = 0;
   uint32 filterTag3 = 0;

   getFiltersToRequestIndexedList(listType, filterTag1, filterTag2, filterTag3);

   ETG_TRACE_USR4(("vRequestMediaPlayerIndexedList->Tag1 = %d,Tag2 = %d,Tag3 %d,ListType = %d m_u32DeviceTag = %d)", filterTag1, filterTag2, filterTag3, listType, _deviceTag));
   _mediaPlayerProxy->sendCreateMediaPlayerIndexedListStart(*this, (::MPlay_fi_types::T_e8_MPlayListType)listType, filterTag1, filterTag2, filterTag3, _deviceTag);
   POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_START)));

   _browserSpeedLock->setFocussedIndex(-1);  // when new list is entered reset the focussed index
   _browserSpeedLock->setDeviceTag(_deviceTag);
   _isNewList = true;
}


/**
 * onCreateMediaPlayerIndexedListResult - function called when mediaplayer gets a result during the method call of createMediaPlayerIndexedListStart.
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void ListInterfaceHandler::onCreateMediaPlayerIndexedListResult(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& proxy, const boost::shared_ptr< CreateMediaPlayerIndexedListResult >& result)
{
   (void)proxy;

   ETG_TRACE_USR4(("onCreateMediaPlayerIndexedListResult"));

   if (_listMap.count(_currentListType))
   {
      _listMap[_currentListType]->vSetListHandle(result->getU32ListHandle());
      _listMap[_currentListType]->vSetListSize(result->getU32ListSize());
   }
   else
   {
      ETG_TRACE_USR4(("ERROR:List is not added in m_ListMap"));
   }
   triggerStateMachineIfListNotEmpty(result->getU32ListSize());
}


/**
 * updateListSlice - private helper function to populate list based on the list size
 * @param[in] listsize [uint32]
 * @return void
 */
void ListInterfaceHandler::triggerStateMachineIfListNotEmpty(uint32 listsize)
{
   ETG_TRACE_USR4(("triggerStateMachineIfListNotEmpty %d ", _currentListType));

   if (listsize > 0)
   {
      triggerStatemachine();
   }
   else
   {
      POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
      if (_currentListType == MODIFIED_LIST_ID_FOLDER_BROWSE)
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivateFolderBrowsingMsg)(1, isRootFolder())));
      }
      else
      {
         POST_MSG((COURIER_MESSAGE_NEW(ListPopulateReqMsg)(1, _currentListType)));
      }
   }
}


/**
 * onCreateMediaPlayerIndexedListError - function called when mediaplayer gets an error during the method call of createMediaPlayerIndexedListStart.
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void ListInterfaceHandler::onCreateMediaPlayerIndexedListError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& proxy, const boost::shared_ptr< CreateMediaPlayerIndexedListError >& error)
{
   (void)proxy;
   (void)error;
   ETG_TRACE_USR4(("onCreateMediaPlayerIndexedListError"));

   if (_listMap.count(_currentListType))
   {
      _listMap[_currentListType]->vSetListSize(0);
   }

   POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
   POST_MSG((COURIER_MESSAGE_NEW(ListPopulateReqMsg)(1, _currentListType)));
}


/**
 * requestMediaPlayerIndexedListSlice - function to get indexed list slice
 *
 * @param[in] listHandle
 * @param[in] windowStart
 * @param[in] windowSize
 * @return void
 */
void ListInterfaceHandler::requestMediaPlayerIndexedListSlice(uint32 listHandle, uint32 windowStart, uint32 windowSize)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::RequestMediaPlayerIndexedListSlice:u32ListHandle =%d, u32WindowStart=%d, u16WindowSize=%d", listHandle, windowStart, windowSize));
#if defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
   updateSearchkeyboardinfo(listHandle);
#endif

   if (_mediaPlayerProxy)
   {
      ETG_TRACE_USR4(("ListInterfaceHandler::sendRequestMediaPlayerIndexedListSliceStart"));
      _mediaPlayerProxy->sendRequestMediaPlayerIndexedListSliceStart(*this, listHandle, windowStart, windowSize);
   }
}


/**
 * onRequestMediaPlayerIndexedListSliceResult - function called when mediaplayer gets a result during the method call of RequestMediaPlayerIndexedListSliceStart.
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void ListInterfaceHandler::onRequestMediaPlayerIndexedListSliceResult(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& proxy, const boost::shared_ptr< RequestMediaPlayerIndexedListSliceResult >& result)
{
   (void)proxy;
   ETG_TRACE_USR4(("onRequestMediaPlayerIndexedListSliceResult listSize = %d", result->getOMediaObjects().size()));
   IBrowse* ibrowse = BrowseFactory::getBrowser(_currentListType + MEDIA_BASE_LIST_ID);
   _oMediaObjects = result->getOMediaObjects();

   if (_listMap.count(_currentListType) && !_isWaitingForListCompletion)
   {
      updateMetadataListSliceToGuiList(_oMediaObjects);
   }
   else
   {
#ifdef BROWSER_FOOTER_DEFAULT_VALUES_SUPPORT
      updateBrowserFooterWithDefaultValues();
#endif
      MediaDatabinding::getInstance().updateScrollBarSwitchIndex(ibrowse->getNodeIndexQuickSearch(_currentListType + MEDIA_BASE_LIST_ID));
      POST_MSG((COURIER_MESSAGE_NEW(ListPopulateReqMsg)(1, _currentListType)));
   }
}


void ListInterfaceHandler::updateIndexListSliceToGuiList()
{
   if (_currentListType == MODIFIED_LIST_ID_FOLDER_BROWSE)
   {
      updateFileListSliceToGuiList(_oFileListMediaObjects);
   }
   else
   {
      updateMetadataListSliceToGuiList(_oMediaObjects);
   }
}


void ListInterfaceHandler::updateMetadataListSliceToGuiList(const ::MPlay_fi_types::T_MPlayMediaObjects& mediaobject)
{
   IBrowse* ibrowse = BrowseFactory::getBrowser(_currentListType + MEDIA_BASE_LIST_ID);
   ListResultInfo _ListResultInfo;
   _ListResultInfo._oMediaObjects = mediaobject;
   _ListResultInfo._listType = _currentListType;
   _ListResultInfo._startIndex = _listMap[_currentListType]->u32GetCurrentStartIndex();
   _ListResultInfo._totalListSize = _listMap[_currentListType]->u32GetListSize();
   updateIndexInMapDB(_ListResultInfo);
   POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
   ibrowse->updateMetaDataListResult(_ListResultInfo);
   ibrowse->updateListData();
   if (_isNewList)
   {
      _browserSpeedLock->setFirstItemTag(_listMap[_currentListType]->u32GetTagbyIndex(0));
      uint32 nextListId = ibrowse->getNextListId(_currentListType + MEDIA_BASE_LIST_ID, 0, 0);
      _browserSpeedLock->setNextListId(nextListId);
      uint32 tag1 = 0;
      uint32 tag2 = 0;
      uint32 tag3 = 0;
      getFiltersToRequestIndexedList(nextListId % MEDIA_BASE_LIST_ID, tag1, tag2, tag3);
      _browserSpeedLock->setFilterTag(tag1, tag2, tag3);
   }
   if (_isNewList || _isBackTraversing)
   {
      _browserSpeedLock->startUserInactiveTimeout();
      _isNewList = false;
      _isBackTraversing = false;
   }

   triggerStatemachine();
}


void ListInterfaceHandler::updateFileListSliceToGuiList(const ::MPlay_fi_types::T_MPlayFileList& fileObject)
{
   ETG_TRACE_USR4(("updateFileListSliceToGuiList"));
   IBrowse* ibrowse = BrowseFactory::getBrowser(_currentListType + MEDIA_BASE_LIST_ID);
   FolderListInfo _FolderListItemInfo;
   _FolderListItemInfo._fileListItems = fileObject;
   _FolderListItemInfo._listType = _currentListType;
   _FolderListItemInfo._totalListSize = _listMap[_currentListType]->u32GetListSize();
   _FolderListItemInfo._startIndex = _listMap[_currentListType]->u32GetCurrentStartIndex();
   _FolderListItemInfo._folderPath = _currentFolderPath;
   _FolderListItemInfo._listHandle = _listMap[_currentListType]->u32GetListHandle();
   updateFolderListDataInMap(_FolderListItemInfo);
   POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
   ibrowse->updateFolderListResult(_FolderListItemInfo);

#ifdef HMI_CDDA_SUPPORT

   if (MediaProxyUtility::getInstance().GetDeviceType(_deviceTag) == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA)
   {
      ibrowse->updateListDataCDDA();
   }
   else
   {
      ibrowse->updateListData();
   }
#else
   ibrowse->updateListData();
#endif
   if (_isNewList || _isBackTraversing)
   {
      _browserSpeedLock->startUserInactiveTimeout();
      _isNewList = false;
      _isBackTraversing = false;
   }
   triggerStatemachine();
}


/**
 * onRequestMediaPlayerIndexedListSliceError - function called when mediaplayer gets an error during the method call of createMediaPlayerIndexedListStart.
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void ListInterfaceHandler::onRequestMediaPlayerIndexedListSliceError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& proxy, const boost::shared_ptr< RequestMediaPlayerIndexedListSliceError >& error)
{
   (void)proxy;
   (void)error;
   ETG_TRACE_USR4(("onRequestMediaPlayerIndexedListSliceError"));
   POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
}


void ListInterfaceHandler::updateIndexInMapDB(const ListResultInfo& listResultInfo)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::updateIndexInMapDB()"));

   std::vector<uint32> filterTag;
   std::vector<std::string> FirstElement;
   filterTag.resize(listResultInfo._oMediaObjects.size());
   uint32 filesize = listResultInfo._oMediaObjects.size();

   for (uint32 listItemsIndex = 0; listItemsIndex < filesize; listItemsIndex++)
   {
      ETG_TRACE_USR4(("DataTag[%d] Category = %d", listItemsIndex, listResultInfo._oMediaObjects[listItemsIndex].getE8CategoryType()));
      switch (listResultInfo._oMediaObjects[listItemsIndex].getE8CategoryType())
      {
         case T_e8_MPlayCategoryType__e8CTY_PLAYLIST:
         case T_e8_MPlayCategoryType__e8CTY_GENRE:
         case T_e8_MPlayCategoryType__e8CTY_COMPOSER:
         case T_e8_MPlayCategoryType__e8CTY_NAME:
         {
            filterTag[listItemsIndex] = listResultInfo._oMediaObjects[listItemsIndex].getU32MetaDataTag1();
            FirstElement.push_back(listResultInfo._oMediaObjects[listItemsIndex].getSMetaDataField1().c_str());
            break;
         }

         case T_e8_MPlayCategoryType__e8CTY_ARTIST:
         case T_e8_MPlayCategoryType__e8CTY_TITLE:
         case T_e8_MPlayCategoryType__e8CTY_EPISODE:
         {
            filterTag[listItemsIndex] = listResultInfo._oMediaObjects[listItemsIndex].getU32MetaDataTag2();
            FirstElement.push_back(listResultInfo._oMediaObjects[listItemsIndex].getSMetaDataField2().c_str());
            break;
         }

         case T_e8_MPlayCategoryType__e8CTY_ALBUM:
         case T_e8_MPlayCategoryType__e8CTY_CHAPTER:
         {
            filterTag[listItemsIndex] = listResultInfo._oMediaObjects[listItemsIndex].getU32MetaDataTag3();
            FirstElement.push_back(listResultInfo._oMediaObjects[listItemsIndex].getSMetaDataField3().c_str());
            break;
         }

         case T_e8_MPlayCategoryType__e8CTY_SONG:
         {
            filterTag[listItemsIndex] = listResultInfo._oMediaObjects[listItemsIndex].getU32MetaDataTag4();
            FirstElement.push_back(listResultInfo._oMediaObjects[listItemsIndex].getSMetaDataField4().c_str());
            break;
         }

         default:
         {
            break;
         }
      }
      ETG_TRACE_USR4(("DataTag[%d] u32Tag = %d", listItemsIndex, listResultInfo._oMediaObjects[listItemsIndex].getU32Tag()));
   }

   if (_currentListType != MODIFIED_LIST_ID_FOLDER_BROWSE)
   {
      createInstanceIfNull(_currentListType);
      if (_listMap.count(_currentListType))
      {
         IBrowse* ibrowse = BrowseFactory::getBrowser(_currentListType + MEDIA_BASE_LIST_ID);
         ETG_TRACE_USR4((" storing the tag info for type:%d", _currentListType));
         _listMap[_currentListType]->vStoreIndexToTagData(filterTag);
         _listMap[_currentListType]->vSetFirstElement(FirstElement);
         ibrowse->setListItemText(FirstElement);
      }
   }
}


/**
 * onCreateMediaPlayerFileListError - function called when mediaplayer gets an error during the method call of CreateMediaPlayerFileList.
 *
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void ListInterfaceHandler::onCreateMediaPlayerFileListError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< CreateMediaPlayerFileListError >& /*error*/)
{
   ETG_TRACE_USR4(("onCreateMediaPlayerFileListError"));

   if (_listMap.count(_currentListType))
   {
      _listMap[_currentListType]->vSetListSize(0);
   }

   POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
   POST_MSG((COURIER_MESSAGE_NEW(ActivateFolderBrowsingMsg)(1, isRootFolder())));
}


/**
 * onCreateMediaPlayerFileListResult - function called when mediaplayer gets result during the method call of CreateMediaPlayerFileList.
 *
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void ListInterfaceHandler::onCreateMediaPlayerFileListResult(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< CreateMediaPlayerFileListResult >& result)
{
   ETG_TRACE_USR4(("onCreateMediaPlayerFileListResult list handle= %d & list size =%d", result->getU32ListHandle(), result->getU32ListSize()));
   ETG_TRACE_USR4(("onCreateMediaPlayerFileListResult noOfFolder= %d & noOfFiles =%d", result->getU32TotalNumFolders(), result->getU32TotalNumFiles()));
   if (_listMap.count(_currentListType))
   {
      _listMap[_currentListType]->vSetListHandle(result->getU32ListHandle());
      _listMap[_currentListType]->vSetListSize(result->getU32ListSize());
      _listMap[_currentListType]->vSetCurrentStartIndex(FLEX_LIST_START_INDEX);
      _listMap[_currentListType]->vSetCurrentBufferSize(FLEX_LIST_MAX_BUFFER_SIZE);
   }

   MediaDatabinding::getInstance().updateTotalNumberOfFilesAndFolders(result->getU32TotalNumFiles(), result->getU32TotalNumFolders());
   triggerStateMachineIfListNotEmpty(result->getU32ListSize());

#ifdef VERTICAL_KEYBOARD_SEARCH_SUPPORT
   /*Default list for vertical key , because list is not available for folder browse*/
   IBrowse* ibrowse = NULL;
   ibrowse = BrowseFactory::getBrowser(LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST);
   ibrowse->SetActiveVerticalKeyLanguague();
#endif
}


/**
 * onCreateMediaPlayerPlaylistListError - function called when mediaplayer gets an error during the method call of CreateMediaPlayerPlaylistList.
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void ListInterfaceHandler::onCreateMediaPlayerPlaylistListError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< CreateMediaPlayerPlaylistListError >& /*error*/)
{
   ETG_TRACE_USR4(("onCreateMediaPlayerPlaylistListError"));

   if (_listMap.count(_currentListType))
   {
      _listMap[_currentListType]->vSetListSize(0);
   }

   POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
   POST_MSG((COURIER_MESSAGE_NEW(ActivateFolderBrowsingMsg)(1, isRootFolder())));
}


/**
 * onCreateMediaPlayerPlaylistListResult - function called when mediaplayer gets result during the method call of CreateMediaPlayerPlaylistList.
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void ListInterfaceHandler::onCreateMediaPlayerPlaylistListResult(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< CreateMediaPlayerPlaylistListResult >& result)
{
   ETG_TRACE_USR4(("onCreateMediaPlayerPlaylistListResult list handle= %d & list size =%d", result->getU32ListHandle(), result->getU32ListSize()));
#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT)
   MediaDatabinding::getInstance().updateQuickSearchAvailabeStatus(false);
#elif defined(ABCSEARCH_SUPPORT)
   MediaDatabinding::getInstance().updateQuickSearchAvailabeStatus(true);
#endif
   if (_listMap.count(_currentListType))
   {
      _listMap[_currentListType]->vSetListHandle(result->getU32ListHandle());
      _listMap[_currentListType]->vSetListSize(result->getU32ListSize());
      _listMap[_currentListType]->vSetCurrentStartIndex(FLEX_LIST_START_INDEX);
      _listMap[_currentListType]->vSetCurrentBufferSize(FLEX_LIST_MAX_BUFFER_SIZE);
   }
   triggerStateMachineIfListNotEmpty(result->getU32ListSize());
}


/**
 * requestMediaPlayerFileListSlice - private function called to get a sliced file list from mediaplayer.
 * Description - This function sends a method start sendRequestMediaPlayerFileListSliceStart to mediaplayer
 * @param[in] listHandle
 * @param[in] windowStart
 * @param[in] windowSize
 * @return void
 */
void ListInterfaceHandler::requestMediaPlayerFileListSlice(uint32 listHandle, uint32 windowStart, uint32 windowSize)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::RequestMediaPlayerFileListSlice:u32ListHandle =%d, u32WindowStart=%d, u16WindowSize=%d", listHandle, windowStart, windowSize));
   if (_mediaPlayerProxy)
   {
      _mediaPlayerProxy->sendRequestMediaPlayerFileListSliceStart(*this, listHandle, windowStart, windowSize);
   }
}


/**
 * requestMediaPlayerPlaylistSlice - private function called to get a sliced playlist list from mediaplayer.
 * Description - This function sends a method start sendRequestMediaPlayerFileListSliceStart to mediaplayer
 * @param[in] listHandle
 * @param[in] windowStart
 * @param[in] windowSize
 * @return void
 */
void ListInterfaceHandler::requestMediaPlayerPlaylistSlice(uint32 listHandle, uint32 windowStart, uint32 windowSize)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::RequestMediaPlayerPlaylistSlice:u32ListHandle =%d, u32WindowStart=%d, u16WindowSize=%d", listHandle, windowStart, windowSize));
   if (_mediaPlayerProxy)
   {
      _mediaPlayerProxy->sendRequestMediaPlayerPlaylistListSliceStart(*this, listHandle, windowStart, windowSize);
   }
}


/**
 * onRequestMediaPlayerFileListSliceError - function called when mediaplayer gets an error during the method call of RequestMediaPlayerFileListSlice.
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void ListInterfaceHandler::onRequestMediaPlayerFileListSliceError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< RequestMediaPlayerFileListSliceError >& /*error*/)
{
   ETG_TRACE_USR4(("onRequestMediaPlayerFileListSliceError"));
   POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
}


/**
 * onRequestMediaPlayerFileListSliceResult - function called when mediaplayer gets result during the method call of RequestMediaPlayerFileListSlice.
 *
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void ListInterfaceHandler::onRequestMediaPlayerFileListSliceResult(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< RequestMediaPlayerFileListSliceResult >& result)
{
   ETG_TRACE_USR4(("onRequestMediaPlayerFileListSliceResult with file list size %d %d", result->getOFileList().size(), _isWaitingForListCompletion));
   _oFileListMediaObjects = result->getOFileList();
   if (_listMap.count(_currentListType) && !_isWaitingForListCompletion)
   {
      updateFileListSliceToGuiList(_oFileListMediaObjects);
   }
   else
   {
#ifdef BROWSER_FOOTER_DEFAULT_VALUES_SUPPORT
      updateBrowserFooterWithDefaultValues();
#endif
      POST_MSG((COURIER_MESSAGE_NEW(ActivateFolderBrowsingMsg)(1, isRootFolder())));
   }
}


/**
 * onRequestMediaPlayerPlaylistListSliceError - function called when mediaplayer gets an error during the method call of RequestMediaPlayerPlaylistListSlice.
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void ListInterfaceHandler::onRequestMediaPlayerPlaylistListSliceError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< RequestMediaPlayerPlaylistListSliceError >& /*error*/)
{
   ETG_TRACE_USR4(("onRequestMediaPlayerPlaylistListSliceError"));
   POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
}


/**
 * onRequestMediaPlayerPlaylistListSliceResult - function called when mediaplayer gets result during the method call of RequestMediaPlayerPlaylistListSlice.
 *
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void ListInterfaceHandler::onRequestMediaPlayerPlaylistListSliceResult(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< RequestMediaPlayerPlaylistListSliceResult >& result)
{
   ETG_TRACE_USR4(("onRequestMediaPlayerFileListSliceResult with file list size %d ", result->getOFileList().size()));
   _oFileListMediaObjects = result->getOFileList();
   if (_listMap.count(_currentListType) && !_isWaitingForListCompletion)
   {
      updateFileListSliceToGuiList(_oFileListMediaObjects);
   }
   else
   {
#ifdef BROWSER_FOOTER_DEFAULT_VALUES_SUPPORT
      updateBrowserFooterWithDefaultValues();
#endif
      POST_MSG((COURIER_MESSAGE_NEW(ActivateFolderBrowsingMsg)(1, isRootFolder())));
   }
}


void ListInterfaceHandler::requestParentFolderList(const RequestedListInfo& requestedListInfo)
{
   _isWaitingForListCompletion = true;
   _isBackTraversing = true;
   ETG_TRACE_USR4(("requestParentFolderList stidx:%d, buffsize:%d", requestedListInfo._startIndex, requestedListInfo._bufferSize));
   if (_listMap.count(_currentListType))
   {
      _listMap[_currentListType]->vSetCurrentStartIndex(requestedListInfo._startIndex);
      _listMap[_currentListType]->vSetCurrentBufferSize(requestedListInfo._bufferSize);
      _browserSpeedLock->setFocussedIndex(0);
   }
   initStatemachine();
   triggerStatemachine();
}


/**
 * initialiseListValuesForFolderBrowser - function called whenever user enter folder browser for the first time.
 *
 * This function assign default values needed for folder browsing
 * @param[in] none
 * @return void
 */
void ListInterfaceHandler::initialiseListValuesForFolderBrowser(uint32 deviceTag)
{
   _isWaitingForListCompletion = true;
   _deviceTag = deviceTag;
   _currentListType = MODIFIED_LIST_ID_FOLDER_BROWSE;
   createInstanceIfNull(MODIFIED_LIST_ID_FOLDER_BROWSE);
   if (_listMap.count(_currentListType))
   {
      _listMap[_currentListType]->vSetCurrentStartIndex(FLEX_LIST_START_INDEX);
      _listMap[_currentListType]->vSetCurrentBufferSize(FLEX_LIST_MAX_BUFFER_SIZE);
   }
   initStatemachine();
   triggerStatemachine();
}


/**
 * updateFolderListDataInMap - private function is used to store the file structure in a internal map
 * @param[in] T_structFolderListItem
 * @return void
 */
void ListInterfaceHandler::updateFolderListDataInMap(const FolderListInfo& structFolderListItemInfo)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::vUpdateFolderListDataInMap is called"));

   _folderListMap.clear();
   FileInfo fileItemsInfo;
   std::vector<std::string> FirstElement;
   uint32 listSize = structFolderListItemInfo._fileListItems.size();
   Candera::UInt32 listItemsIndex = 0;

#ifdef FOLDERUP_VISIBILITY_FOR_BROWSER
   if ((!isRootFolder()) && (structFolderListItemInfo._startIndex == 0))
   {
      fileItemsInfo._fileName = "FOLDER UP";
      fileItemsInfo._fileType = FTS_FOLDER_UP;
      fileItemsInfo._playableStatus = T_e8_MPlayPlayableStatus__e8FP_PLAYABLE;
      fileItemsInfo._isPlaying = false;
      _folderListMap.insert(std::pair<uint32, FileInfo>(listItemsIndex, fileItemsInfo));
      ++listItemsIndex;
      ++listSize;
   }
#endif

   for (uint32 indexFromFolderList = 0; listItemsIndex < listSize; ++listItemsIndex, ++indexFromFolderList)
   {
      fileItemsInfo._fileName = structFolderListItemInfo._fileListItems[indexFromFolderList].getSFilename().c_str();
      FirstElement.push_back(structFolderListItemInfo._fileListItems[indexFromFolderList].getSFilename().c_str());
      fileItemsInfo._fileType = GetFileType(structFolderListItemInfo._fileListItems[indexFromFolderList].getE8FileType());
      fileItemsInfo._playableStatus = structFolderListItemInfo._fileListItems[indexFromFolderList].getE8PlayableStatus();
      fileItemsInfo._isPlaying = structFolderListItemInfo._fileListItems[indexFromFolderList].getBIsPlaying();
      _folderListMap.insert(std::pair<uint32, FileInfo>(listItemsIndex, fileItemsInfo));

      ETG_TRACE_USR4(("listItemsIndex = %d", listItemsIndex));
      ETG_TRACE_USR4(("file name %s", fileItemsInfo._fileName.c_str()));
      ETG_TRACE_USR4(("_playableStatus =%d _isPlaying =%d", fileItemsInfo._playableStatus, fileItemsInfo._isPlaying));
   }

   //createInstanceIfNull(_currentListType);
   // if (_listMap.count(_currentListType) == MODIFIED_LIST_ID_FOLDER_BROWSE)
   // {
   ETG_TRACE_USR4((" storing the first element for folder list type:%d", _currentListType));
   _listMap[_currentListType]->vSetFirstElement(FirstElement);
   //}
}


enum FolderListType ListInterfaceHandler::GetFileType(T_e8_MPlayFileType filetype)
{
   switch (filetype)
   {
      case T_e8_MPlayFileType__e8FT_FOLDER:
      {
         return FTS_FOLDER;
      }

      case T_e8_MPlayFileType__e8FT_AUDIO :
      {
         return FTS_AUDIO;
      }

      case T_e8_MPlayFileType__e8FT_VIDEO:
      {
         return FTS_VIDEO;
      }

      case T_e8_MPlayFileType__e8FT_PLAYLIST:
      {
         return FTS_PLAYLIST;
      }

      case T_e8_MPlayFileType__e8FT_IMAGE:
      {
         return FTS_IMAGE;
      }

      default:
      {
         break;
      }
   }
   return FTS_INVALID;
}


/**
 * setFolderRequestType - function to set the folder request type when traversing through folder browser
 *
 * @param[in] enum FolderListType
 * @return void
 */
void ListInterfaceHandler::setFolderRequestType(enum FolderListType folderListRequestType)
{
   _folderRequestType = folderListRequestType;
}


void ListInterfaceHandler::setFocussedIndexAndNextListId(int32 focussedIndex, uint32 nextListId)
{
   if (_listMap.count(_currentListType))
   {
      IBrowse* ibrowse = BrowseFactory::getBrowser(_currentListType + MEDIA_BASE_LIST_ID);
      focussedIndex = ibrowse->getActiveItemIndex(focussedIndex);
      _browserSpeedLock->setFocussedIndex(focussedIndex);

      if (_currentListType != MODIFIED_LIST_ID_FOLDER_BROWSE)
      {
         _browserSpeedLock->setNextListId(nextListId);
         _browserSpeedLock->setSelectedListItemTag(_listMap[_currentListType]->u32GetTagbyIndex(focussedIndex));

         uint32 tag1 = 0;
         uint32 tag2 = 0;
         uint32 tag3 = 0;
         getFiltersToRequestListByIndex(nextListId % MEDIA_BASE_LIST_ID, focussedIndex, tag1, tag2, tag3);
         _browserSpeedLock->setFilterTag(tag1, tag2, tag3);
      }
      _browserSpeedLock->startUserInactiveTimeout();
   }
}


/**
 * PlayItemFromFolderList - private function is called when user selects a row in Folder list browser.
 *
 * Description: This fuction sends play request to mediaplayer if the selected item (index) is an audio file
 * @param[in] index
 * @return void
 */
void ListInterfaceHandler::requestPlayOrSubFolderList(uint32 selectedIndex)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::PlayItemFromFolderList %d with (selectedIndex = %d)", _currentListType, selectedIndex));
   if (_listMap.count(_currentListType) && _mediaPlayerProxy)
   {
      _isWaitingForListCompletion = true;
      uint32 listHandle = _listMap[_currentListType]->u32GetListHandle();
      ETG_TRACE_USR4(("PlayItemFromFolderList -> list handle %d (%d)", listHandle, _folderListMap[selectedIndex]._fileType));
      switch (_folderListMap[selectedIndex]._fileType)
      {
         case FTS_AUDIO:
         {
            ETG_TRACE_USR4(("requestPlayOrSubFolderList -> selectedIndex,Playable status %d %d", selectedIndex, _folderListMap[selectedIndex]._playableStatus));
            if (_folderListMap[selectedIndex]._playableStatus == ::MPlay_fi_types::T_e8_MPlayPlayableStatus__e8FP_DRM_PROTECTED)
            {
               POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_FILE_DRM_PROTECTED_SYSIND)));
            }
            else
            {
#ifdef FOLDERUP_VISIBILITY_FOR_BROWSER
               // the selected index value is decremented by 1 to send the correct start index for the list handle if it is not root folder and when folder up is not visible
               selectedIndex = (!(isRootFolder()) && _listMap[_currentListType]->u32GetCurrentStartIndex() == 0) ? selectedIndex - 1 : selectedIndex;							//Fix for NCG3D-5377;
#endif

               /* the selected index is added with the start index of the window to get the absolute index position wrt to the list handle else it will play the relative index value of the list handle
                * Eg if there are 100 songs in a folder and window size is limited to 25 and user wants to select the last song ie 100
                * here the current start index of the window is 75 and selected index is 25
                * on selection HMI should inform midw to play 100 th song so we add current start index and selected index to get the absolute position
                */

               uint32 absolute_index = selectedIndex + _listMap[_currentListType]->u32GetCurrentStartIndex();
               ETG_TRACE_USR4(("requestPlayOrSubFolderList -> absolute index %d", absolute_index));
               // send a courier message ActivateSongInFolderBrowsingMsg to controller inorder to go to Media main screen.

               _mediaPlayerProxy->sendPlayItemFromListStart(*this, listHandle, absolute_index, 0);
               POST_MSG((COURIER_MESSAGE_NEW(ActivateSongInFolderBrowsingMsg)()));
            }
            break;
         }
#ifdef SUPPORT_VIDEO_PLAYER
         case FTS_VIDEO:
         {
            ETG_TRACE_USR4(("VIDEO PLAYER ENABLE"));
#ifdef FOLDERUP_VISIBILITY_FOR_BROWSER
            // the selected index value is decremented by 1 to send the correct start index for the list handle if it is not root folder and when folder up is not visible
            selectedIndex = (!(isRootFolder()) && _listMap[_currentListType]->u32GetCurrentStartIndex() == 0) ? selectedIndex - 1 : selectedIndex;							//Fix for NCG3D-5377;
#endif

            /* the selected index is added with the start index of the window to get the absolute index position wrt to the list handle else it will play the relative index value of the list handle
             * Eg if there are 100 songs in a folder and window size is limited to 25 and user wants to select the last song ie 100
             * here the current start index of the window is 75 and selected index is 25
             * on selection HMI should inform midw to play 100 th song so we add current start index and selected index to get the absolute position
             */

            uint32 absolute_index = selectedIndex + _listMap[_currentListType]->u32GetCurrentStartIndex();
            ETG_TRACE_USR4(("requestPlayOrSubFolderList -> absolute index %d", absolute_index));
            // send a courier message ActivateSongInFolderBrowsingMsg to controller inorder to go to Media main screen.

            _mediaPlayerProxy->sendPlayItemFromListStart(*this, listHandle, absolute_index, 0);
            POST_MSG((COURIER_MESSAGE_NEW(ActivateVideoInBrowsingMsg)()));
            break;
         }
#endif
         case FTS_FOLDER:
         {
            MediaUtils::AppendSelectedFolderInCurrFolderPath(_currentFolderPath, _folderListMap[selectedIndex]._fileName);
            ETG_TRACE_USR4(("Selected folder path %s", _currentFolderPath.c_str()));
            MediaDatabinding::getInstance().updateFolderPath(_currentFolderPath);
            _folderRequestType = FTS_FOLDER;
            setCurrentStateMachineState(IDLE);
            triggerStatemachine();
            break;
         }
         case FTS_PLAYLIST:
         {
            _currentPlaylist = _currentFolderPath + _folderListMap[selectedIndex]._fileName;
            MediaUtils::AppendSelectedFolderInCurrFolderPath(_currentFolderPath, _folderListMap[selectedIndex]._fileName);
            // ETG_TRACE_USR4(("Selected folder path %s", _currentFolderPath.c_str()));
            MediaDatabinding::getInstance().updateFolderPath(_currentFolderPath);
            _folderRequestType = FTS_PLAYLIST;
            setCurrentStateMachineState(IDLE);
            triggerStatemachine();
            break;
         }
         case FTS_FOLDER_UP:
         {
            // _folderRequestType is reset to "folder_type" because on press of folder up from anywhere it leads only to a folder not to playlists
            _folderRequestType = FTS_FOLDER_UP;
            POST_MSG((COURIER_MESSAGE_NEW(FolderUpBtnPressReqMsg)()));
            break;
         }

         default:
         {
            break;
         }
      } //end of switch block
   }
   else
   {
      ETG_TRACE_USR4(("ListInterfaceHandler::m_u8CurrentListType is not present in corresponding Maps"));
   }
}


void ListInterfaceHandler::updateFolderListParameters(uint32 listHandle, uint32 listSize)
{
   createInstanceIfNull(MODIFIED_LIST_ID_FOLDER_BROWSE);
   if (_listMap.count(MODIFIED_LIST_ID_FOLDER_BROWSE))
   {
      _listMap[MODIFIED_LIST_ID_FOLDER_BROWSE]->vSetListHandle(listHandle);
      _listMap[MODIFIED_LIST_ID_FOLDER_BROWSE]->vSetListSize(listSize);
   }
}


/**
 * onPlayItemFromListError - function called when mediaplayer gets an error during the method call of PlayItemFromList.
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void ListInterfaceHandler::onPlayItemFromListError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< PlayItemFromListError >& /*error*/)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::onPlayItemFromListError"));
}


/**
 * onPlayItemFromListResult - function called when mediaplayer gets result during the method call of PlayItemFromList.
 *
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void ListInterfaceHandler::onPlayItemFromListResult(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< PlayItemFromListResult >& /*result*/)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::onPlayItemFromListResult"));

   _iPlayScreen.getSongPlayFromFolderBrowseStatus(true);

   //#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
   //   ::MPlay_fi_types::T_e8_MPlayDeviceType currentDeviceType = _mediaPlayerProxy->getNowPlaying().getOMediaObject().getE8DeviceType();
   //   if (::MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH == currentDeviceType)
   //   {
   //      _btCurrentPlayingListStatus = true;
   //      MediaDatabinding::getInstance().updateCurrentPlayingListBtnStatus(_btCurrentPlayingListStatus);
   //   }
   //#endif
}


/**
 * onMediaPlayerListChangeError - function called whenever mediaplayer error while updating the list during indexing
 *
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void ListInterfaceHandler::onMediaPlayerListChangeError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< MediaPlayerListChangeError >& /*error*/)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::onMediaPlayerListChangeError"));
}


/**
 * onMediaPlayerListChangeStatus - function called whenever mediaplayer updates the list during indexing
 *
 * @param[in] proxy
 * @param[in] status
 * @return void
 */
void ListInterfaceHandler::onMediaPlayerListChangeStatus(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< MediaPlayerListChangeStatus >& status)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::onMediaPlayerListChangeStatus listhandle = %d listsize = %d", status->getU32ListHandle(), status->getU32ListSize()));
   uint32 listHandle = 0xFFFFFFFF;
   uint32 listSize = 0;
   //TODO check to update the list size of all the items in the browsing history.
   IBrowse* ibrowse = BrowseFactory::getBrowser(_currentListType + MEDIA_BASE_LIST_ID);
   ibrowse->updateFolderHistoryDuringIndexing(status->getU32ListHandle(), status->getU32ListSize());

   if (_listMap.count(_currentListType))
   {
      listHandle = _listMap[_currentListType]->u32GetListHandle();
      listSize = _listMap[_currentListType]->u32GetListSize();
   }
   ETG_TRACE_USR4(("current listhandle = %d currentlistsize = %d ", listHandle, listSize));

   if (listHandle == status->getU32ListHandle())
   {
      if ((status->getE8Change() == T_e8_MPlayChange__e8LCH_CONTENT_CHANGED) &&
            (status->getU32ListSize() != listSize) && (!MediaProxyUtility::getInstance().isIndexingDone(_deviceTag)))
      {
         _listMap[_currentListType]->vSetListSize(status->getU32ListSize());
         _currentStateMachineState = WAITING_PLAYERINDEX_METHODRESULT;
         triggerStatemachine();
      }
   }
}


/**
 * sGetFirstStringElement - function called to get the first element of the corresponding list.
 *
 * @param[in] no param
 * @return String
 */

std::string ListInterfaceHandler::getFirstStringElement(uint32 startIndex)
{
   std::string m_firststringelement;

   if (_listMap.count(_currentListType))
   {
      m_firststringelement = _listMap[_currentListType]->getFirstElementText(startIndex);
   }

   /*     ETG_TRACE_USR4(("ListInterfaceHandler::vRequestQSMediaplayerFilelist"));
          if (_mediaPlayerProxy)
          {
             ETG_TRACE_USR4(("ListInterfaceHandler::sendRequestMediaPlayerIndexedListSliceStart"));
             _mediaPlayerProxy->sendSearchKeyboardMediaPlayerListStart(*this,m_ListMap[m_u8CurrentListType]->u32GetListHandle());
          }*/
   ETG_TRACE_USR4(("Result: sGetFirstStringElement   = %s \n", m_firststringelement.c_str()));
   return m_firststringelement;
}


#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
void ListInterfaceHandler::vRequestQSMediaplayerFilelist(std::string SearchString, uint32 startIndex)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::vRequestQSMediaplayerFilelist"));
   MPlay_fi_types::T_SearchString stringinfo;
   stringinfo.setSText(SearchString);
   stringinfo.setU8Encoding(0);

   if (_mediaPlayerProxy)
   {
      ETG_TRACE_USR4(("ListInterfaceHandler::vRequestQSMediaplayerFilelist"));
      _mediaPlayerProxy->sendQuicksearchStart(*this, _listMap[_currentListType]->u32GetListHandle(), startIndex, stringinfo);
   }
}


void ListInterfaceHandler::onQuicksearchError(const ::boost::shared_ptr<mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr<mplay_MediaPlayer_FI::QuicksearchError >& /*error*/)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::onQuicksearchError"));
}


void ListInterfaceHandler::onQuicksearchResult(const ::boost::shared_ptr<mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr<mplay_MediaPlayer_FI::QuicksearchResult >& result)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::onQuicksearchResult"));

   uint32 activelistid = (_currentListType == MODIFIED_LIST_ID_FOLDER_BROWSE) ? LIST_ID_BROWSER_FOLDER_BROWSE : LIST_ID_BROWSER_METADATA_BROWSE ;

   IBrowse* ibrowse = BrowseFactory::getBrowser(activelistid);

   QuickSearchStringResult _quickSearchStringInfo;
   _quickSearchStringInfo._position = result->getU32Position();
   _quickSearchStringInfo._quickSearchResult = result->getOSearchResult();

   //uint32 windowSize = _listMap[_currentListType]->u32GetCurrentBufferSize();

   ETG_TRACE_USR4(("ListInterfaceHandler::_quickSearchStringInfo._position %d", _quickSearchStringInfo._position));
   ETG_TRACE_USR4(("ListInterfaceHandler::_quickSearchStringInfo._quickSearchResult %d", _quickSearchStringInfo._quickSearchResult));

   if (_quickSearchStringInfo._quickSearchResult == T_e8_QuickSearchResult__e8FOUND || _quickSearchStringInfo._quickSearchResult == T_e8_QuickSearchResult__e8FOLLOWING)
   {
      ETG_TRACE_USR4(("ListInterfaceHandler::T_e8_QuickSearchResult__e8FOUND"));
      //if((_quickSearchStringInfo._position-windowSize)>5)
      //{
      POST_MSG((COURIER_MESSAGE_NEW(ListChangeMsg)(activelistid, ListChangeSet, ibrowse->getActiveQSIndex(_quickSearchStringInfo._position))));
      //}
      //else
      //{
      //    MediaDatabinding::getInstance().updateBrowseListStartIndex(_quickSearchStringInfo._position-5);
      //}
   }
   else if (_quickSearchStringInfo._quickSearchResult == T_e8_QuickSearchResult__e8ENDOFLIST)
   {
      ETG_TRACE_USR4(("ListInterfaceHandler::T_e8_QuickSearchResult__e8ENDOFLIST"));
      //MediaDatabinding::getInstance().updateBrowseListStartIndex((_quickSearchStringInfo._position-5));
      POST_MSG((COURIER_MESSAGE_NEW(ListChangeMsg)(activelistid, ListChangeSet, ibrowse->getActiveQSIndex(_quickSearchStringInfo._position) - 5)));
   }
}


#endif

//#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
//
//bool ListInterfaceHandler::getBTCurrentPlayingListStatus()
//{
//   return _btCurrentPlayingListStatus;
//}
//
//#endif
#ifdef ABCSEARCH_SUPPORT
std::map<std::string, trSearchKeyboard_ListItem> ListInterfaceHandler::getABCSearchKeyboardListItemMap()
{
   return _mABCSearchKeyboardListItemMap;
}


#endif


#ifdef BROWSER_FOOTER_DEFAULT_VALUES_SUPPORT
void ListInterfaceHandler::updateBrowserFooterWithDefaultValues()
{
   if (_listMap.count(_currentListType))
   {
      uint32 listSize = _listMap[_currentListType]->u32GetListSize();
      if (listSize > 0)
      {
         if (_currentListType == MODIFIED_LIST_ID_FOLDER_BROWSE)
         {
#ifdef FOLDERUP_VISIBILITY_FOR_BROWSER
            listSize = isRootFolder() ? listSize : listSize + 1;
#endif
         }
         else
         {
            listSize = IBrowse::isAllAvailable(_oMediaObjects[0].getE8CategoryType()) ? listSize + 1 : listSize;
         }
      }
      std::string footerText = MediaUtils::FooterTextForBrowser(0, listSize);
      MediaDatabinding::getInstance().updateFooterInFolderBrowse(footerText.c_str());
   }
}


#endif
/*
void ListInterfaceHandler::onSearchKeyboardMediaPlayerListResult(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& proxy, const boost::shared_ptr< SearchKeyboardMediaPlayerListResult >& result)
{
    (void)proxy;
    ETG_TRACE_USR4(("onSearchKeyboardMediaPlayerListResult"));

    m_SearchKeyboardListItem.u32ListHandle = result->getU32ListHandle();
    m_SearchKeyboardListItem.sSearchKeyboardLetter = result->getOSearchKeyboardList().data()->kSSearchKeyboardLetter;
    m_SearchKeyboardListItem.bLetterAvailable = result->getOSearchKeyboardList().data()->kBLetterAvailable;
    m_SearchKeyboardListItem.u32LetterStartIndex = result->getOSearchKeyboardList().data()->kU32LetterStartIndex;
    m_SearchKeyboardListItem.u32LetterEndIndex = result->getOSearchKeyboardList().data()->kU32LetterEndIndex;

       ETG_TRACE_USR4(("Result: u32ListHandle   = %d \n", m_SearchKeyboardListItem.u32ListHandle));
       ETG_TRACE_USR4(("Result: sSearchKeyboardLetter     = %s \n", m_SearchKeyboardListItem.sSearchKeyboardLetter));
       ETG_TRACE_USR4(("Result: bLetterAvailable     = %d \n", m_SearchKeyboardListItem.bLetterAvailable));
       ETG_TRACE_USR4(("Result: u32LetterStartIndex     = %d \n", m_SearchKeyboardListItem.u32LetterStartIndex));
       ETG_TRACE_USR4(("Result: u32LetterEndIndex     = %d \n", m_SearchKeyboardListItem.u32LetterEndIndex));
}


void ListInterfaceHandler::onSearchKeyboardMediaPlayerListError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& proxy, const boost::shared_ptr< SearchKeyboardMediaPlayerListError >& error)
{
       (void)proxy;
       (void)error;
       ETG_TRACE_USR4(("onSearchKeyboardMediaPlayerListError"));
}*/
#ifdef HMI_CDDA_SUPPORT
/**
 * onCreateMediaPlayerCDListError - CDDA error list
 *
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void ListInterfaceHandler::onCreateMediaPlayerCDListError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerCDListError >& /*error*/)
{
   ETG_TRACE_USR4(("onCreateMediaPlayerCDListError for CD "));

   if (_listMap.count(_currentListType))
   {
      _listMap[_currentListType]->vSetListSize(0);
   }

   POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
   POST_MSG((COURIER_MESSAGE_NEW(ActivateFolderBrowsingMsg)(1, isRootFolder())));
}


/**
 * onCreateMediaPlayerCDListResult - CDDA  list reasult
 *
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void ListInterfaceHandler::onCreateMediaPlayerCDListResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerCDListResult >& result)
{
   ETG_TRACE_USR4(("onCreateMediaPlayerCDListResult list handle= %d & list size =%d", result->getU32ListHandle(), result->getU32ListSize()));
   ETG_TRACE_USR4(("onCreateMediaPlayerCDListResult noOfFolder= %d & noOfFiles =%d", result->getU32TotalNumFolders(), result->getU32TotalNumFiles()));
   uint32 u32size = result->getU32ListSize();
   uint32 u32ListHandel = result->hasU32ListHandle();
   uint32 u32NumberFile = result->getU32TotalNumFiles();
   ETG_TRACE_USR4((" Size :- %d :: ListHandel =%d :: file number = %d ", u32size, u32ListHandel, u32NumberFile));
   if (_listMap.count(_currentListType))
   {
      _listMap[_currentListType]->vSetListHandle(result->getU32ListHandle());
      _listMap[_currentListType]->vSetListSize(result->getU32ListSize());
      _listMap[_currentListType]->vSetCurrentStartIndex(FLEX_LIST_START_INDEX);
      _listMap[_currentListType]->vSetCurrentBufferSize(FLEX_LIST_MAX_BUFFER_SIZE);
   }
   MediaDatabinding::getInstance().updateTotalNumberOfFilesAndFolders(result->getU32TotalNumFiles(), result->getU32TotalNumFolders());
   triggerStateMachineIfListNotEmpty(result->getU32ListSize());
}


/**
 * requestMediaPlayerCDDAList - CDDA  request list
 *
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void ListInterfaceHandler::requestMediaPlayerCDDAList()
{
   ETG_TRACE_USR4(("ListInterfaceHandler::requestMediaPlayerCDDAList (%d _deviceTag) ", _deviceTag));
   if (_mediaPlayerProxy)
   {
      _mediaPlayerProxy->sendCreateMediaPlayerCDListStart(*this, "/", _deviceTag, ::MPlay_fi_types::T_e8_MPlayFileTypeSelection__e8FTS_ALL);
      POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_START)));
   }
}


#endif

#if defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
/**
 * onSearchKeyboardMediaPlayerListError - Vertical keyboard
 *
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void ListInterfaceHandler::onSearchKeyboardMediaPlayerListError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const boost::shared_ptr< mplay_MediaPlayer_FI::SearchKeyboardMediaPlayerListError >& error)
{
   (void)proxy;
   (void)error;
   ETG_TRACE_USR4(("onSearchKeyboardMediaPlayerListError"));
}


/**
 * onSearchKeyboardMediaPlayerListResult - Vertical keyboard
 *
 * @param[in] proxy
 * @param[in] result
 * @return void
 */

void ListInterfaceHandler::onSearchKeyboardMediaPlayerListResult(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const boost::shared_ptr< mplay_MediaPlayer_FI::SearchKeyboardMediaPlayerListResult >& result)
{
   (void)proxy;
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
   (void)result;
#else
#ifdef VERTICAL_KEYBOARD_SEARCH_SUPPORT
   uint32 verticalListype = LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST % MEDIA_BASE_LIST_ID;
   createInstanceIfNull(verticalListype);
   if (_listMap.count(verticalListype))
   {
      _listMap[verticalListype]->vSetListHandle(result->getU32ListHandle());
      _listMap[verticalListype]->vSetListSize(result->getOSearchKeyboardList().size());
      _listMap[verticalListype]->vSetCurrentStartIndex(FLEX_LIST_START_INDEX);
      _listMap[verticalListype]->vSetCurrentBufferSize(FLEX_LIST_MAX_BUFFER_SIZE);
   }
   IBrowse* ibrowse = NULL;
   ibrowse = BrowseFactory::getBrowser(LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST);
   ibrowse->SetActiveVerticalKeyLanguague();
#endif
#ifdef ABCSEARCH_SUPPORT
   _mABCSearchKeyboardListItemMap.clear();
#endif

   ETG_TRACE_USR4(("onSearchKeyboardMediaPlayerListResult %d", result->getU32ListHandle()));

   ::MPlay_fi_types::T_MPlaySearchKeyboardList lkeyList = result->getOSearchKeyboardList();

   ::MPlay_fi_types::T_MPlaySearchKeyboardList::iterator itr = lkeyList.begin();

   ETG_TRACE_USR4(("Result: lkeyList.size()   = %d \n", lkeyList.size()));
   for (; itr != lkeyList.end(); itr++)
   {
      m_SearchKeyboardListItem.u32ListHandle = result->getU32ListHandle();
      m_SearchKeyboardListItem.sSearchKeyboardLetter = itr->getSSearchKeyboardLetter();
      m_SearchKeyboardListItem.bLetterAvailable = itr->getBLetterAvailable();
      m_SearchKeyboardListItem.u32LetterStartIndex = itr->getU32LetterStartIndex();
      m_SearchKeyboardListItem.u32LetterEndIndex = itr->getU32LetterEndIndex();

      ETG_TRACE_USR4(("Result: sSearchKeyboardLetter     = %s \n", m_SearchKeyboardListItem.sSearchKeyboardLetter.c_str()));
      ETG_TRACE_USR4(("Result: bLetterAvailable     = %d \n", m_SearchKeyboardListItem.bLetterAvailable));
      ETG_TRACE_USR4(("Result: u32LetterStartIndex     = %d \n", m_SearchKeyboardListItem.u32LetterStartIndex));
      ETG_TRACE_USR4(("Result: u32LetterEndIndex     = %d \n", m_SearchKeyboardListItem.u32LetterEndIndex));
#ifdef VERTICAL_KEYBOARD_SEARCH_SUPPORT
      ibrowse->updateSearchKeyBoardListInfo(m_SearchKeyboardListItem);
#endif
#ifdef ABCSEARCH_SUPPORT
      if (m_SearchKeyboardListItem.bLetterAvailable)
      {
         _mABCSearchKeyboardListItemMap.insert(std::pair<std::string, trSearchKeyboard_ListItem>(m_SearchKeyboardListItem.sSearchKeyboardLetter, m_SearchKeyboardListItem));
      }
#endif
   }

#endif
}


/**
 * updateSearchkeyboardinfo - Vertical keyboard
 *
 * @param[in] listHandel
 * @return void
 */
void ListInterfaceHandler::updateSearchkeyboardinfo(uint32 listHandel)
{
   ETG_TRACE_USR4(("ListInterfaceHandler::updateSearchkeyboardinfo"));
   uint8 activeLanguage;

   activeLanguage = MediaDataPoolConfig::getInstance()->getLanguage();

   if (_mediaPlayerProxy)
   {
      if (_currListHandel != listHandel || _currLanguage != activeLanguage)
      {
         _mediaPlayerProxy->sendSearchKeyboardMediaPlayerListStart(*this, listHandel);;
      }
      _currListHandel = listHandel;
      _currLanguage = activeLanguage;
   }
}


#endif

/**
 * requestPlayAllsongs - Folder Browse/MetadatBrowse

 * @return void
 */
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R
void ListInterfaceHandler::requestPlayAllsongs()
{
   if ((_listMap.count(_currentListType)) && (_currentListType == MODIFIED_LIST_ID_FOLDER_BROWSE))
   {
      _mediaPlayerProxy->sendPlayItemFromListStart(*this, _listMap[_currentListType]->u32GetListHandle(), 0, 0);	      //start index is given as 0 instead of -1 to play the first playable object in the list
      ETG_TRACE_USR4(("Listhandle :%d", _listMap[_currentListType]->u32GetListHandle()));
   }
   else
   {
      bool isAllplayingPossible = _browserSpeedLock->requestPlayAllSongsForMetadataBrowse();
      ETG_TRACE_USR4(("ListInterfaceHandler::requestPlayAllSongsForMetadataBrowse currlist %d : %d ", _currentListType, isAllplayingPossible));
      if (!isAllplayingPossible)
      {
         IBrowse* ibrowse = BrowseFactory::getBrowser(_currentListType + MEDIA_BASE_LIST_ID);
         uint32 nextListId = ibrowse->getNextListId(_currentListType + MEDIA_BASE_LIST_ID, 0, 0);
         uint32 tag1 = 0;
         uint32 tag2 = 0;
         uint32 tag3 = 0;
         getFiltersToRequestIndexedList(nextListId % MEDIA_BASE_LIST_ID, tag1, tag2, tag3);
         if (_browserSpeedLock)
         {
            _browserSpeedLock->createMediaPlayerIndexedListStart(nextListId, tag1, tag2, tag3);
         }
         else
         {
            ETG_TRACE_USR4(("ListInterfaceHandler::speed lock is null"));
         }
      }
   }
}


#endif
}//end of namespace Core
}//end of namespace App
