/**
 *  @file   SpiRestoreFactorySetting.cpp
 *  @author ECV - alc7kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#include "hall_std_if.h"
#include "SpiRestoreFactorySetting.h"
#ifdef DP_DATAPOOL_ID
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_hmi_04_if.h"
#endif

#include "App/datapool/SpiMediaDataPoolConfig.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS         TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::SpiRestoreFactorySetting::

#include "trcGenProj/Header/SpiRestoreFactorySetting.cpp.trc.h"
#endif
namespace App {
namespace Core {


/**
 * RestoreFactorySettings Class constructor
 */
SpiRestoreFactorySetting::SpiRestoreFactorySetting(SpiSettingListHandler* spiSettingListHandler): _mpSpiSettingListHandler(spiSettingListHandler)
{
   ETG_I_REGISTER_FILE();
   ETG_TRACE_USR4(("SpiRestoreFactorySettings() Constructor"));
   _mdefSetServiceBase = DefSetServiceBase::GetInstance();
   _mDpHandler = SpiMediaDataPoolConfig::getInstance();
   if (_mdefSetServiceBase != NULL)
   {
      _mdefSetServiceBase->vRegisterforUpdate(this);
   }
   StartupSync::getInstance().registerPropertyRegistrationIF(this);
}


/**
 * RestoreFactorySettings Class destructor
 */
SpiRestoreFactorySetting:: ~SpiRestoreFactorySetting()
{
   ETG_TRACE_USR4(("~RestoreFactorySettings() Destructor"));
   ETG_I_UNREGISTER_FILE();
   if (_mdefSetServiceBase != NULL)
   {
      _mdefSetServiceBase->vUnRegisterforUpdate(this);
      //donot delete this instance as its not created by us.
      _mdefSetServiceBase = NULL;
   }
   _mDpHandler = NULL;
   _mpSpiSettingListHandler = NULL;
}


/**
* registerProperties - register Properties
* @param[in] None
* @parm[out] None
* @return void
*/
void SpiRestoreFactorySetting::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& /*proxy*/, const asf::core::ServiceStateChange& /*stateChange*/)
{
   ETG_TRACE_USR4(("RestoreFactorySettings registerProperties"));
}


/**
* registerProperties - deregister Properties
* @param[in] None
* @parm[out] None
* @return void
*/
void SpiRestoreFactorySetting::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& /*proxy*/, const asf::core::ServiceStateChange& /*stateChange*/)
{
   ETG_TRACE_USR4(("RestoreFactorySettings registerProperties"));
}


/**
 * onAvailable -
 * @param[in] proxy
 * @param[in] stateChange
 */
void SpiRestoreFactorySetting::onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
      const asf::core::ServiceStateChange& stateChange)
{
   ETG_TRACE_USR4(("RestoreFactorySettings:: onAvailable"));
   StartupSync::getInstance().onAvailable(proxy, stateChange);
}


/**
 * onUnavailable -
 * @param[in] proxy
 * @param[in] stateChange
 */
void SpiRestoreFactorySetting::onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
      const asf::core::ServiceStateChange& stateChange)
{
   ETG_TRACE_USR4(("RestoreFactorySettings:: onUnavailable"));
   StartupSync::getInstance().onUnavailable(proxy, stateChange);
}


/**
 * reqPrepareResponse: To know the response of the respective APP when Restore factory setting is pressed.
 * @param[in]: None
 * @param[in] :None
 */
void SpiRestoreFactorySetting::reqPrepareResponse()
{
   ETG_TRACE_USR4(("SpiRestoreFactorySettings:: reqPrepareResponse"));
   if (_mdefSetServiceBase)
   {
      _mdefSetServiceBase->sendPrepareResponse(0, this);
   }
}


/**
 * reqExecuteResponse: To Execute the response when restore factory setting has been executed..
 * @param[in]: None
 * @param[in] :None
 */
void SpiRestoreFactorySetting::reqExecuteResponse()
{
   ETG_TRACE_USR4(("RestoreFactorySettings:: reqExecuteResponse"));
   resetHmiDataPool();
   if (_mdefSetServiceBase)
   {
      _mdefSetServiceBase->sendExecuteResponse(0, this);
   }
}


/**
 * reqExecuteResponse: To Finalize the response when restore factory setting has been executed.
 * @param[in]: None
 * @param[in] :None
 */
void SpiRestoreFactorySetting:: reqFinalizeResponse()
{
   ETG_TRACE_USR4(("RestoreFactorySettings:: reqFinalizeResponse"))
   _mpSpiSettingListHandler->updateSpiSettinglistOnRestoreFactotySetting();
   if (_mdefSetServiceBase)
   {
      _mdefSetServiceBase->sendFinalizeResponse(0, this);
   }
}


/**
 * reqExecuteResponse: To reset hmi data pool to default value..
 * @param[in]: None
 * @param[in] :None
 */
void SpiRestoreFactorySetting::resetHmiDataPool()
{
   if (_mDpHandler)
   {
      ETG_TRACE_USR4(("RestoreFactorySettings:: resetHmiDataPool to default value"));
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
      _mDpHandler->setDpSpiAutoLaunch(AUTOLAUNCH_STATUS_ASK);
#elif defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2)
      _mDpHandler->setDpSpiCarPlayAskOnConnect(CARPLAY_ASK_ON_CONNECT_ON);
      _mDpHandler->setDpSpiCarPlayAutoLaunch(CARPLAY_AUTOLAUNCHSTATUS_OFF);
      _mDpHandler->setDpAndroidAutoAskOnConnect(ANDROID_AUTO_ASK_ON_CONNECT_ON);
      _mDpHandler->setDpAndroidAutoLaunch(ANDROID_AUTO_AUTOLAUNCHSTATUS_OFF);
#endif
   }
}


} //end of namespace Core
} //end of namespace App
