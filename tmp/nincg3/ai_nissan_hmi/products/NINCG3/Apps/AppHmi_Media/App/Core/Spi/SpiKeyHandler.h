/*
 * SpiKeyHandling.h
 * Author: rhk5kor
 *  Created on: Jun 25, 2015
 *  @addtogroup AppHmi_media
 */

#ifndef SPIKEYHANDLER_H_
#define SPIKEYHANDLER_H_
#include "AppHmi_MediaStateMachine.h"
#include "AppBase/ServiceAvailableIF.h"
#include "midw_smartphoneint_fiProxy.h"
#include "SpiConnectionHandling.h"
#include "Core/Spi/SpiUtils.h"
#include "../../ai_hmi_cgi/components/Framework/AppUtils/Timer.h"
#include  "CgiExtensions/DataBindingItem.hpp"


namespace App {
namespace Core {

//class MediaHall; //forward declaration
class SpiConnectionHandling;

class SpiKeyHandler
   : public hmibase::ServiceAvailableIF
   , public StartupSync::PropertyRegistrationIF

{
   public:

      SpiKeyHandler* getInstance();

      virtual ~SpiKeyHandler();

      SpiKeyHandler(SpiConnectionHandling* poSpiConnectionHandling , ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& spiMidwServiceProxy);

      void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);
      void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);

      void notifyDiPOAppStatusInfoStatus(const uint32 SpeechAppState, const uint32 PhoneAppState);
      void vStartTimerOnPttPress();
      void vStopPttTimer();

      bool onCourierMessage(const OnHk_KeyEventReqMsg& /*msg*/);
      bool onCourierMessage(const onHK_BackUpdMsg& /*msg*/);
      bool onCourierMessage(const onEncoderRotatonUpdMsg& /*msg*/);
      bool onCourierMessage(const onEncoderPressedUpdMsg& /*msg*/);
      bool onCourierMessage(const onSendDipoPlayCommandUpdMsg& /*msg*/);
      //bool onCourierMessage(const RejectCallUpdMsg& /*msg*/);
      virtual bool onCourierMessage(const QSTimerExpiredMsg& oMsg);
      bool onCourierMessage(const onPTTShortTimerExpMsg& oMsg);

      void vHandlePhoneAppRequest(uint32 , uint32);
      uint32 u32GetMappedKeyMode(uint32 u32KeyMode);
      void vSiriButtonUpRequired(bool);
      bool bIsSiriButtonUpRequired();
      bool bGetPhoneAppStatus();
      void vUpdateSpiSpeechRecognitionDgStatus(bool);
      void vUpdateSpiPhoneAppDgStatus(bool);

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      ON_COURIER_MESSAGE(OnHk_KeyEventReqMsg)
      ON_COURIER_MESSAGE(onHK_BackUpdMsg)
      ON_COURIER_MESSAGE(onEncoderRotatonUpdMsg)
      ON_COURIER_MESSAGE(onEncoderPressedUpdMsg)
      ON_COURIER_MESSAGE(QSTimerExpiredMsg)
      ON_COURIER_MESSAGE(onSendDipoPlayCommandUpdMsg)
      ON_COURIER_MESSAGE(onPTTShortTimerExpMsg)
      //ON_COURIER_MESSAGE(RejectCallUpdMsg)
      COURIER_MSG_MAP_END();
      DataBindingItem<SpeechRecognitionActiveSourceDataBindingSource> _SpeechRecognitionState;
      DataBindingItem<SpiPhoneAppStatusDataBindingSource> _SpiPhoneAppStatus;
   private:

      void vDIPOSpeechAppStateRequestHandler(uint32);
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      void vAndroidAutoSpeechAppStateRequestHandler(uint32);
#endif
      void vSendLaunchAppRequestHandler(enDiPOAppType eDiPoAppType);

      bool _bSpeechAppStateActive;
      bool _bPhoneAppStateActive;
      bool _bSiriButtonUpRequired;

      static SpiKeyHandler* pSpiKeyHandler;
      SpiConnectionHandling* _spiConnectionHandler;
      ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& _spiMidwServiceProxy;
      Util::Timer _qsTimer;
      Util::Timer _timerOnPttShortPress;
};


}// end App
}//end core


#endif /* SPIKEYHANDLER_H_ */
