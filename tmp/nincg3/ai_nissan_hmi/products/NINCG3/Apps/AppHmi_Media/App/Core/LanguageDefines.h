/*
 * LanguageDefines.h
 *
 *  Created on: Jul 31, 2015
 *      Author: sla7cob
 */
#ifndef LANGUAGEDEFINES_H_
#define LANGUAGEDEFINES_H_

#include "HmiTranslation_TextIds.h"
#include <CanderaWidget/String/String.h>	//To include Candera::String

const unsigned int TextId_Invalid  = 0x0;
const unsigned int TextId_Waiting  = 0x0;

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R
#define LANGUAGE_STRING(text_id, text) text
#define LANGUAGE_ID(text_id, text) text
#define LANGUAGE_ID_TYPE Candera::String
#define LANGUAGE_ID_STRING(text) text
#else
#define LANGUAGE_STRING(text_id, text) (TextId_Invalid==text_id?Candera::String(text):Candera::String(text_id))
#define LANGUAGE_ID(text_id, text) text_id
#define LANGUAGE_ID_TYPE uint32
#define LANGUAGE_ID_STRING(text_id) Candera::String(text_id)
#endif

//Language ID mapping - this is done since the text ids are going to be diffrent for scope1 and scope2
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
#define BROWSE_All                         TextId_MEDIA__METADATA_BROWSE_All
#define LINEIN__MAIN_Headline              TextId_MEDIA_LINEIN__MAIN_Headline
#define CDMP3__MAIN_Headline               TextId_MEDIA_CD_CDMP3__MAIN_Headline
#define USB__MAIN_Headline                 TextId_MEDIA_USB__MAIN_Headline
#define IPOD__MAIN_Headline                TextId_MEDIA_IPOD__MAIN_Headline
#define BTAUDIO__MAIN_Headline             TextId_MEDIA_BTAUDIO__MAIN_Headline
#define USB__BROWSE_Headline               TextId_MEDIA_USB__BROWSE_Headline
#define USB__MAIN_Headline_USBIndexing     TextId_MEDIA_USB__MAIN_Headline_USBIndexing
#define IPOD__MAIN_Headline_IpodIndexing   TextId_MEDIA_IPOD__MAIN_Headline_IpodIndexing
#define MEDIA__MAIN_Headline_Indexing	   TextId_MEDIA__MAIN_Headline_Indexing
#define USB__FOLDER_BROWSE_Button_FolderUp TextId_MEDIA_CD_MP3_USB__FOLDER_BROWSE_Button_FolderUp
#define METADATA_BROWSE_EmptyList          TextId_MEDIA__METADATA_BROWSE_EmptyList
#define USB__BROWSE_Button_FolderBrowse    TextId_MEDIA_USB__BROWSE_Button_FolderBrowse
#define USB__BROWSE_Button_PlayList        TextId_MEDIA_USB__BROWSE_Button_PlayList
#define USB__BROWSE_Button_Artist          TextId_MEDIA_USB__BROWSE_Button_Artist
#define USB__BROWSE_Button_Album           TextId_MEDIA_USB__BROWSE_Button_Album
#define USB__BROWSE_Button_Song            TextId_MEDIA_USB__BROWSE_Button_Song
#define USB__BROWSE_Button_Genre           TextId_MEDIA_USB__BROWSE_Button_Genre
#define USB__BROWSE_Button_Composer        TextId_MEDIA_USB__BROWSE_Button_Composer
#define IPOD__MENU_Button_PlayList         TextId_MEDIA_IPOD__MENU_Button_PlayList
#define IPOD__MENU_Button_Artist           TextId_MEDIA_IPOD__MENU_Button_Artist
#define IPOD__MENU_Button_Album            TextId_MEDIA_IPOD__MENU_Button_Album
#define IPOD__MENU_Button_Song             TextId_MEDIA_IPOD__MENU_Button_Song
#define IPOD__MENU_Button_Genre            TextId_MEDIA_IPOD__MENU_Button_Genre
#define IPOD__MENU_Button_Composer         TextId_MEDIA_IPOD__MENU_Button_Composer
#define IPOD__MENU_Button_Audiobook        TextId_MEDIA_IPOD__MENU_Button_Audiobook
#define IPOD__MENU_Button_Podcast          TextId_MEDIA_IPOD__MENU_Button_Podcast
#define IPOD__MENU_Button_UpdateMusicLibrary TextId_MEDIA_IPOD__MENU_Button_UpdateMusicLibrary
#define USB__MAIN_FooterButton_RepeatTrack   TextId_MEDIA_USB__MAIN_FooterButton_RepeatTrack
#define USB__MAIN_FooterButton_RepeatFolder  TextId_MEDIA_USB__MAIN_FooterButton_RepeatFolder
#define USB__MAIN_FooterButton_Random        TextId_MEDIA_USB__MAIN_FooterButton_Random
#define USB__MAIN_FooterButton_RandomFolder  TextId_MEDIA_USB__MAIN_FooterButton_RandomFolder
#define USB__MAIN_FooterButton_RandomAll     TextId_MEDIA_USB__MAIN_FooterButton_RandomAll
#define SETTINGS_CARPLAY_AutomaticLaunchOnConnect       TextId_SYSTEM__SETTINGS_CARPLAY_ListButton_AutomaticLaunchOnConnect
#define SETTINGS_CARPLAY_ListButtonChoice_ON	TextId_SYSTEM__SETTINGS_CARPLAY_ListButtonChoice_ON
#define SETTINGS_CARPLAY_ListButtonChoice_OFF   TextId_SYSTEM__SETTINGS_CARPLAY_ListButtonChoice_OFF
#define SETTINGS_CARPLAY_ListButtonChoice_AskOnConnect   TextId_SYSTEM__SETTINGS_CARPLAY_ListButtonChoice_AskOnConnect
#define SETTINGS_CARPLAY_Button_CarPlayTutorial  TextId_SYSTEM__SETTINGS_CARPLAY_Button_CarPlayTutorial
#define SETTINGS_CARPLAY_Headline			TextId_SYSTEM__SETTINGS_CARPLAY_Headline
#define SETTINGS_CARPLAY_TUTORIAL_Headline  TextId_SYSTEM__SETTINGS_CARPLAY_TUTORIAL_Headline
#define MEDIA_LINEIN__MAIN_Message          TextId_MEDIA_LINEIN__MAIN_Message
#define USB_BROWSE__PlayList_HeadLine      TextId_MEDIA_USB__BROWSE_Button_PlayList
#define USB_BROWSE__Artists_HeadLine      TextId_MEDIA_USB__BROWSE_Button_Artist
#define USB_BROWSE__Album_HeadLine        TextId_MEDIA_USB__BROWSE_Button_Album
#define USB_BROWSE__Songs_HeadLine        TextId_MEDIA_USB__BROWSE_Button_Song
#define USB_BROWSE__Genres_HeadLine       TextId_MEDIA_USB__BROWSE_Button_Genre
#define USB_BROWSE__Composer_HeadLine     TextId_MEDIA_USB__BROWSE_Button_Composer
#define IPOD_MENU__PlayList_HeadLine       TextId_MEDIA_IPOD__MENU_Button_PlayList
#define IPOD_MENU__Artists_HeadLine       TextId_MEDIA_IPOD__MENU_Button_Artist
#define IPOD_MENU__Album_HeadLine         TextId_MEDIA_IPOD__MENU_Button_Album
#define IPOD_MENU__Songs_HeadLine         TextId_MEDIA_IPOD__MENU_Button_Song
#define IPOD_MENU__Genres_HeadLine        TextId_MEDIA_IPOD__MENU_Button_Genre
#define IPOD_MENU__Composer_HeadLine      TextId_MEDIA_IPOD__MENU_Button_Composer
#define IPOD_MENU__Podcast_HeadLine       TextId_MEDIA_IPOD__MENU_Button_Podcast
#define IPOD_MENU__AudiobookList_HeadLine TextId_MEDIA_IPOD__MENU_Button_Audiobook
#define BT_AUDIO_BTMENU                   TextId_Invalid//Since this text is not applicable for scope1,a default id is given.
#define BT_AUDIO_LOWER_AVRCP_MAIN_BUTTON_CONNECT TextId_Invalid//Since this text is not applicable for scope1,a default id is given.

#elif defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2)

#define LINEIN__MAIN_Headline               TextId_IT_00027
#define CDMP3__MAIN_Headline                TextId_IT_00037
#define CDMP3__MAIN_Menu                    TextId_IT_00181
#define USB__MAIN_Headline                  TextId_IT_00044
#define USB1__MAIN_Headline                 TextId_IT_00050
#define USB2__MAIN_Headline                 TextId_IT_00052
#define IPOD__MAIN_Headline                 TextId_IT_00055
#define IPOD1__MAIN_Headline                TextId_IT_00058
#define IPOD2__MAIN_Headline                TextId_IT_00061
#define BTAUDIO__MAIN_Headline              TextId_IT_00031
#define USB__BROWSE_Headline                TextId_IT_00381
#define USB__MAIN_Headline_USBIndexing      TextId_Invalid//Given some default value for the texts which are not applicable for scope2.
#define IPOD__MAIN_Headline_IpodIndexing    TextId_Invalid//Given some default value for the texts which are not applicable for scope2.
#define USB__FOLDER_BROWSE_Button_FolderUp  TextId_Waiting//Given some default value for the texts.will be removed once the text_id is available.
#define MEDIA__MAIN_Headline_Indexing	    TextId_Invalid//Given some default value for the texts which are not applicable for scope2.
#define METADATA_BROWSE_EmptyList           TextId_IT_00364
#define USB__BROWSE_Button_FolderList       TextId_IT_00132
#define USB__BROWSE_Button_PlayList         TextId_IT_00342
#define USB__BROWSE_Button_Artist           TextId_IT_00344
#define USB__BROWSE_Button_All_Artist 		  TextId_IT_00346
#define USB__BROWSE_Button_All_Songs        TextId_IT_00354
#define USB__BROWSE_Button_All_Albums       TextId_IT_00350
#define USB__BROWSE_Button_Album            TextId_IT_00125
#define USB__BROWSE_Button_Song             TextId_IT_00126
#define USB__BROWSE_Button_Genre            TextId_IT_00358
#define USB__BROWSE_Button_Composer         TextId_IT_00360
#define IPOD__MENU_Button_CurrentList       TextId_IT_00133
#define IPOD__MENU_Button_PlayList          TextId_IT_00342
#define IPOD__MENU_Button_Artist            TextId_IT_00344
#define IPOD__MENU_Button_All_Artist 		  TextId_IT_00346
#define IPOD_MENU_Button_All_Songs          TextId_IT_00354
#define IPOD_MENU_Button_All_Albums         TextId_IT_00350
#define IPOD__MENU_Button_Album             TextId_IT_00348
#define IPOD__MENU_Button_Song              TextId_IT_00352
#define IPOD__MENU_Button_Genre             TextId_IT_00358
#define IPOD__MENU_Button_Composer          TextId_IT_00360
#define IPOD__MENU_Button_Audiobook         TextId_IT_00362
#define IPOD__MENU_Button_Podcast           TextId_IT_00356
#define USB__MAIN_FooterButton_RepeatTrack  TextId_Waiting//Given some default value for the texts.will be removed once the text_id is available.
#define USB__MAIN_FooterButton_RepeatFolder TextId_Waiting//Given some default value for the texts.will be removed once the text_id is available.
#define USB__MAIN_FooterButton_Random       TextId_Waiting//Given some default value for the texts.will be removed once the text_id is available.
#define USB__MAIN_FooterButton_RandomFolder TextId_Waiting//Given some default value for the texts.will be removed once the text_id is available.
#define USB__MAIN_FooterButton_RandomAll    TextId_Waiting//Given some default value for the texts.will be removed once the text_id is available.
#define MEDIA_LINEIN__MAIN_Message          TextId_Invalid//Given some default value for the texts which are not applicable for scope2.
#define MEDIA_BROWSE__All_Artists           TextId_IT_00346
#define MEDIA_BROWSE__All_Songs             TextId_IT_00354
#define MEDIA_BROWSE__All_Albums            TextId_IT_00350
#define MEDIA_BROWSE__All_Albums_Headline   TextId_IT_00351
#define MEDIA_BROWSE__All_Artists_Headline  TextId_IT_00347
#define MEDIA_BROWSE__All_Songs_Headline    TextId_IT_00355
#define USB_BROWSE__PlayList_HeadLine       TextId_IT_00343
#define USB_BROWSE__Artists_HeadLine        TextId_IT_00345
#define USB_BROWSE__Album_HeadLine          TextId_IT_00349
#define USB_BROWSE__Songs_HeadLine          TextId_IT_00353
#define USB_BROWSE__Genres_HeadLine         TextId_IT_00359
#define USB_BROWSE__Composer_HeadLine       TextId_IT_00361
#define IPOD_MENU__PlayList_HeadLine        TextId_IT_00343
#define IPOD_MENU__Artists_HeadLine         TextId_IT_00345
#define IPOD_MENU__Album_HeadLine           TextId_IT_00349
#define IPOD_MENU__Songs_HeadLine           TextId_IT_00353
#define IPOD_MENU__Genres_HeadLine          TextId_IT_00359
#define IPOD_MENU__Composer_HeadLine        TextId_IT_00361
#define IPOD_MENU__Podcast_HeadLine         TextId_IT_00357
#define IPOD_MENU__AudiobookList_HeadLine   TextId_IT_00363
#define MEDIA_BROWSE__NowPlaying            TextId_IT_00129
#define IPOD__MENU_Button_UpdateMusicLibrary TextId_Waiting//Given some default value for the texts.will be removed once the text_id is available.
#define BT_AUDIO_BTMENU                      TextId_IT_00142
#define BROWSE_All                           TextId_Invalid//Given some default value for the texts which are not applicable for scope2.
#define BT_AUDIO_LOWER_AVRCP_MAIN_BUTTON_CONNECT TextId_New_Phone_0001


#endif


#endif //LANGUAGEDEFINES_H_
