/**
 *  @file   AccessoryAppStateHandler.h
 *  @author ECV - alc7kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#include "hall_std_if.h"
#include "AccessoryAppStateHandler.h"
#include "SpiKeyHandler.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS         TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::AccessoryAppStateHandler::
#include "trcGenProj/Header/AccessoryAppStateHandler.cpp.trc.h"
#endif
namespace App {
namespace Core {
/*AccessoryAppStateHandler destructor*/

AccessoryAppStateHandler::~AccessoryAppStateHandler()
{
   ETG_TRACE_USR4(("Spi AccessoryAppStateHandler destructor"));
   _meSpeechAppState = SpeechAppState__SPI_APP_STATE_SPEECH_UNKNOWN;
   _mePhoneAppState = PhoneAppState__SPI_APP_STATE_PHONE_UNKNOWN;
   _meNavigationAppState = NavigationAppState__SPI_APP_STATE_NAV_UNKNOWN;
   _meDIPOPhoneAppState = PhoneAppState__SPI_APP_STATE_PHONE_UNKNOWN;
   _spiMidwServiceProxy.reset();
   _spiNavigationServiceProxy.reset();
   _spiSdshmiProxy.reset();
   _spitelephoneProxy.reset();
   _mpSpiConnectionHandling = NULL;
   _mbBlockScreenSaver = false;
   _mInterfaceScreenSaver = NULL;
   _mpoSpiKeyHandler = NULL;
   _mbSpeechActiveStatus = false;
}


/*AccessoryAppStateHandler constructor*/
AccessoryAppStateHandler::AccessoryAppStateHandler(SpiConnectionHandling* paraSpiConnectionHandling, SpiKeyHandler* poKeyHandler, ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& spiMidwServiceProxy):
   _mpSpiConnectionHandling(paraSpiConnectionHandling)
   , _spiMidwServiceProxy(spiMidwServiceProxy)
   , _mpoSpiKeyHandler(poKeyHandler)
   , _spitelephoneProxy(::MOST_Tel_FI::MOST_Tel_FIProxy::createProxy("btTelFiPort", *this))
   , _spiNavigationServiceProxy(org::bosch::cm::navigation::NavigationService::NavigationServiceProxy::createProxy("NavigationPort", *this))
   , _spiSdshmiProxy(::sds2hmi_sds_fi::Sds2hmi_sds_fiProxy::createProxy("sds2hmiFiPort", *this))
{
   ETG_TRACE_USR4(("Spi AccessoryAppStateHandler constructor"));
   StartupSync::getInstance().registerPropertyRegistrationIF(this);
   _meSpeechAppState = SpeechAppState__SPI_APP_STATE_SPEECH_UNKNOWN;
   _mePhoneAppState = PhoneAppState__SPI_APP_STATE_PHONE_UNKNOWN;
   _meNavigationAppState = NavigationAppState__SPI_APP_STATE_NAV_UNKNOWN;
   _meDIPOPhoneAppState = PhoneAppState__SPI_APP_STATE_PHONE_UNKNOWN;
   _mInterfaceScreenSaver = UserInterfaceScreenSaver::getInstance();
   _mbBlockScreenSaver = false;
   _mbSpeechActiveStatus = false;
}


/* This function performs the upreg of the required properties.
 * @param[in]  - proxy: Pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - stateChange: Contains the current Service State
 * @param[out] - None
 * @return     - Void
 */

void AccessoryAppStateHandler::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_spiMidwServiceProxy && (proxy == _spiMidwServiceProxy))
   {
      _spiMidwServiceProxy->sendDiPOAppStatusInfoUpReg(*this);;
   }
   if (_spiNavigationServiceProxy && (proxy == _spiNavigationServiceProxy))
   {
      _spiNavigationServiceProxy->sendNavStatusRegister(*this);
   }
   if (_spiSdshmiProxy && (proxy == _spiSdshmiProxy))
   {
      _spiSdshmiProxy->sendSDS_StatusUpReg(*this);
   }
   if (_spitelephoneProxy && (proxy == _spitelephoneProxy))
   {
      _spitelephoneProxy->sendCallStatusNoticeUpReg(*this);
   }
}


/* This function performs the Relupreg of the required properties.
 * @param[in]  - proxy: Pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - stateChange: Contains the current Service State
 * @param[out] - None
 * @return     - Void
 */
void AccessoryAppStateHandler::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_spiMidwServiceProxy && (proxy == _spiMidwServiceProxy))
   {
      _spiMidwServiceProxy->sendDiPOAppStatusInfoRelUpRegAll();
   }
   if (_spiNavigationServiceProxy && (proxy == _spiNavigationServiceProxy))
   {
      _spiNavigationServiceProxy->sendNavStatusDeregisterAll();
   }
   if (_spiSdshmiProxy && (proxy == _spiSdshmiProxy))
   {
      _spiSdshmiProxy->sendSDS_StatusRelUpRegAll();
   }
   if (_spitelephoneProxy && (proxy == _spitelephoneProxy))
   {
      _spitelephoneProxy->sendCallStatusNoticeRelUpRegAll();
   }
}


/**
 * onDiPOAppStatusInfoError - called if DipoAppStatus property is updated with error
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void AccessoryAppStateHandler::onDiPOAppStatusInfoError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >&/*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DiPOAppStatusInfoError >&/*error*/)
{
   ETG_TRACE_USR4(("AccessoryAppStateHandler: onDiPOAppStatusInfoError Received ..."));
}


/**
 * Thois is  a helper function to update datagaurd to block/allow clock screen saver on Longpress of ILLUM key in case of  NAVI/PHONE/SIRI
 * @param[in] void
 * @return void
 */
void AccessoryAppStateHandler::vUpdateBlockClockScreenSaverState()
{
   (*_BlockClockScreenOnHKILLUMLongPressState).mIsBlockClockScreenOnHKILLUMLongPress = _mbBlockScreenSaver;
   _BlockClockScreenOnHKILLUMLongPressState.MarkItemModified(ItemKey::BlockClockScreenOnHKILLUMLongPress::IsBlockClockScreenOnHKILLUMLongPressItem);
   _BlockClockScreenOnHKILLUMLongPressState.SendUpdate();
}


/**
 * Thois is  a helper function to block/allow clock screen saver in case of  NAVI/PHONE/SIRI
 * @param[in] void
 * @return void
 */
void AccessoryAppStateHandler::vHandleClockscreenSaver(enSpeechAppState eSpeechAppState, enPhoneAppState  ePhoneAppState, enNavigationAppState eNavigationAppState)
{
   ETG_TRACE_USR4(("AccessoryAppStateHandler: vHandleClockscreenSaver Called ..."));
   if ((eSpeechAppState == SpeechAppState__SPI_APP_STATE_SPEECH_SPEAKING) || (eSpeechAppState == SpeechAppState__SPI_APP_STATE_SPEECH_RECOGNISING)
         || (ePhoneAppState == PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE) || (eNavigationAppState == NavigationAppState__SPI_APP_STATE_NAV_ACTIVE))
   {
      ETG_TRACE_USR4(("AccessoryAppStateHandler: vHandleClockscreenSaver  vBlockScreenSaver called..."));
      _mbBlockScreenSaver = true;

      if (_mInterfaceScreenSaver != NULL)
      {
         _mInterfaceScreenSaver->vBlockScreenSaver();  //Case: Media already in foreground, block screensaver here
      }
   }
   else
   {
      ETG_TRACE_USR4(("AccessoryAppStateHandler: vHandleClockscreenSaver   vAllowScreenSaver called..."));
      _mbBlockScreenSaver = false;

      if (_mInterfaceScreenSaver != NULL)
      {
         _mInterfaceScreenSaver->vAllowScreenSaver(); //Case: Media already in foreground, allow screensaver here
      }
   }
   vUpdateBlockClockScreenSaverState();
}


/**
 * This function is called when application state is changed to Foreground and we need to block clock screen saver
 * @param[in] void
 * @return void
 */
bool AccessoryAppStateHandler::bIsBlockClockscreenSaver()
{
   ETG_TRACE_USR4(("AccessoryAppStateHandler: bIsBlockClockscreenSaver Called ..."));
   return _mbBlockScreenSaver;
}


/**
 * SetAppModeAndContext - To Set Application Mode and do a context switch on entry action
 * @param[in] oMsg
 * @param[out] None
 * @return void
 */
bool AccessoryAppStateHandler::onCourierMessage(const SetAppModeAndContext& oMsg)
{
   ETG_TRACE_USR4(("AccessoryAppStateHandler:GUI->HALL onCourierMessage SetAppModeAndContext received: AppMode = %d SwitchId %d", oMsg.GetAppId(), oMsg.GetSwitchId()));
   if ((_mePhoneAppState != PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE) && (_meDIPOPhoneAppState != PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE) && (_meSpeechAppState != SpeechAppState__SPI_APP_STATE_SPEECH_SPEAKING) && (_meSpeechAppState != SpeechAppState__SPI_APP_STATE_SPEECH_RECOGNISING))
   {
      ETG_TRACE_USR4(("AccessoryAppStateHandler:GUI->HALL onCourierMessage SetAppModeAndContext setting: AppMode = %d", oMsg.GetAppId()));
      POST_MSG((COURIER_MESSAGE_NEW(SetApplicationModeReqMsg)(oMsg.GetAppId())));
   }
   POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(oMsg.GetSwitchId(), CONTEXT_TRANSITION_DONE)));
   return true;
}


/**
 * SetAppModeOnCheck - To Set Application Mode On Check of call active status when on entry action to UTILITY screens
 * @param[in] oMsg
 * @param[out] None
 * @return void
 */
bool AccessoryAppStateHandler::onCourierMessage(const SetAppModeOnCheck& oMsg)
{
   ETG_TRACE_USR4(("AccessoryAppStateHandler:GUI->HALL onCourierMessage SetAppModeOnCheck received: AppMode = %d", oMsg.GetAppId()));
   if ((_mePhoneAppState != PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE) && (_meDIPOPhoneAppState != PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE) && (_meSpeechAppState != SpeechAppState__SPI_APP_STATE_SPEECH_SPEAKING) && (_meSpeechAppState != SpeechAppState__SPI_APP_STATE_SPEECH_RECOGNISING))
   {
      ETG_TRACE_USR4(("AccessoryAppStateHandler:GUI->HALL onCourierMessage SetAppModeOnCheck setting: AppMode = %d", oMsg.GetAppId()));
      POST_MSG((COURIER_MESSAGE_NEW(SetApplicationModeReqMsg)(oMsg.GetAppId())));
   }
   return true;
}


/*
 * This function is called when the property update for DiPOAppStatusInfo Received
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - status: Pointer to the DiPOAppStatusInfo
 * @param[out] - None
 * @return     - Void
 */
void AccessoryAppStateHandler::onDiPOAppStatusInfoStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DiPOAppStatusInfoStatus >& status)
{
   ETG_TRACE_USR4(("AccessoryAppStateHandler: onDiPOAppStatusInfoStatus Received ..."));
   if (proxy == _spiMidwServiceProxy)
   {
      if (_mpoSpiKeyHandler)
      {
         _mpoSpiKeyHandler->notifyDiPOAppStatusInfoStatus(status->getAppStateSpeech(), status->getAppStatePhone());
      }
      if (_meDIPOPhoneAppState != enPhoneAppState(status->getAppStatePhone()))
      {
         _meDIPOPhoneAppState = enPhoneAppState(status->getAppStatePhone());
         vUpdateApplicationModeOnCall();
      }
      if (_meSpeechAppState != enSpeechAppState(status->getAppStateSpeech()))
      {
         _meSpeechAppState = enSpeechAppState(status->getAppStateSpeech());
         vUpdateApplicationModeonSpeech();
      }

      enNavigationAppState eNavigationAppState = enNavigationAppState(status->getAppStateNavigation());
      ETG_TRACE_USR4(("AccessoryAppStateHandler: onDiPOAppStatusInfoStatus eSpeechAppState = %d ePhoneAppState = %d eNavigationAppState = %d", ETG_CENUM(enSpeechAppState, _meSpeechAppState), ETG_CENUM(enPhoneAppState, _meDIPOPhoneAppState), ETG_CENUM(enNavigationAppState, eNavigationAppState)));
      vHandleClockscreenSaver(_meSpeechAppState, _meDIPOPhoneAppState, eNavigationAppState);
      switch (status->getAppStateNavigation())
      {
         case ::midw_smartphoneint_fi_types::T_e8_NavigationAppState__SPI_APP_STATE_NAV_ACTIVE :
         {
            if (NavigationAppState__SPI_APP_STATE_NAV_ACTIVE == _mpSpiConnectionHandling->getAccNavAppStatus())
            {
               //_spiNavigationServiceProxy->sendStopGuidanceRequest(*this);
               _spiNavigationServiceProxy->sendCancelRouteGuidanceRequest(*this);
            }
            break;
         }
         case ::midw_smartphoneint_fi_types::T_e8_NavigationAppState__SPI_APP_STATE_NAV_NOTACTIVE:
         {
            //Todo@send root guidance start
            break;
         }
         default:
         {
            break;
         }
      }
   }
}


/**
 * onStopGuidanceError - called if StopGuidance method result  is updated with error
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void AccessoryAppStateHandler:: onStopGuidanceError(const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& /*proxy*/, const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::StopGuidanceError >& /*error*/)
{
   ETG_TRACE_USR4(("AccessoryAppStateHandler: onStopGuidanceError Received "));
}


/**
 * onStopGuidanceResponse - called if StopGuidance method result  is updated with response
 * @param[in] proxy
 * @parm[in] response
 * @return void
 */
void AccessoryAppStateHandler:: onStopGuidanceResponse(const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy, const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::StopGuidanceResponse >& /*response*/)
{
   if (proxy == _spiNavigationServiceProxy)
   {
      ETG_TRACE_USR4(("AccessoryAppStateHandler: onStopGuidanceResponse Received "));
   }
}


/**
 * onCancelRouteGuidanceError - called if CancelRouteGuidance method result  is updated with error
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void AccessoryAppStateHandler:: onCancelRouteGuidanceError(const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& /*proxy*/, const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::CancelRouteGuidanceError >& /*error*/)
{
   ETG_TRACE_USR4(("AccessoryAppStateHandler: onCancelRouteGuidanceError Received "));
}


/**
 * onCancelRouteGuidanceResponse - called if CancelRouteGuidance method result  is updated with response
 * @param[in] proxy
 * @parm[in] response
 * @return void
 */
void AccessoryAppStateHandler:: onCancelRouteGuidanceResponse(const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy, const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::CancelRouteGuidanceResponse >& /*response*/)
{
   if (proxy == _spiNavigationServiceProxy)
   {
      ETG_TRACE_USR4(("AccessoryAppStateHandler: onCancelRouteGuidanceResponse Received "));
   }
}


/**
 * onNavStatusError - called if NavStatus  property  is updated with error
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void AccessoryAppStateHandler:: onNavStatusError(const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& /*proxy*/, const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::NavStatusError >& /*error*/)
{
   ETG_TRACE_USR4(("AccessoryAppStateHandler: onNavStatusError Received "));
}


/**
 * onNavStatusError - called if NavStatus property  is updated with update
 * @param[in] proxy
 * @parm[in] update
 * @return void
 */
void AccessoryAppStateHandler:: onNavStatusUpdate(const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy, const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::NavStatusUpdate >& update)
{
   ETG_TRACE_USR4(("AccessoryAppStateHandler: onNavStatusUpdate Received "));
   if (proxy == _spiNavigationServiceProxy)
   {
      ETG_TRACE_USR4(("AccessoryAppStateHandler: onNavStatusUpdate NavState = %d ", update->getNavStatus()));
      switch (update->getNavStatus())
      {
         case ::org::bosch::cm::navigation::NavigationService::NavStatus__NAVSTATUS_UNKNOWN:
         {
            _meNavigationAppState = NavigationAppState__SPI_APP_STATE_NAV_UNKNOWN;
            break;
         }
         case ::org::bosch::cm::navigation::NavigationService::NavStatus__NAVSTATUS_IDLE:
         case ::org::bosch::cm::navigation::NavigationService::NavStatus__NAVSTATUS_GUIDANCE_ACTIVE_OFFBOARD:
         case ::org::bosch::cm::navigation::NavigationService::NavStatus__NAVSTATUS_BUSY://todo : Recheck this mapping
         {
            _meNavigationAppState = NavigationAppState__SPI_APP_STATE_NAV_NOTACTIVE;
            break;
         }
         case ::org::bosch::cm::navigation::NavigationService::NavStatus__NAVSTATUS_GUIDANCE_ACTIVE:
         case ::org::bosch::cm::navigation::NavigationService::NavStatus__NAVSTATUS_CALCULATING_ROUTE:
         {
            _meNavigationAppState = NavigationAppState__SPI_APP_STATE_NAV_ACTIVE;
            break;
         }
         default :
         {
            break;
         }
      }
      _mpSpiConnectionHandling->setAccNavAppStatus(_meNavigationAppState);
      ETG_TRACE_USR4(("AccessoryAppStateHandler: onNavStatusUpdate call setAccessoryAppState eSpeechAppState = %d ePhoneAppState = %d eNavigationAppState = %d", ETG_CENUM(enSpeechAppState, _meSpeechAppState), ETG_CENUM(enPhoneAppState, _mePhoneAppState), ETG_CENUM(enNavigationAppState, _meNavigationAppState)));
      _mpSpiConnectionHandling->setAccessoryAppState(_meSpeechAppState, _mePhoneAppState, _meNavigationAppState);
   }
}


/**
 * onSDS_StatusError - called if SDS_Status property  is updated with error
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void AccessoryAppStateHandler::onSDS_StatusError(const ::boost::shared_ptr< ::sds2hmi_sds_fi::Sds2hmi_sds_fiProxy >&/*proxy*/, const ::boost::shared_ptr< ::sds2hmi_sds_fi::SDS_StatusError >&/* error*/)
{
   ETG_TRACE_USR4(("onSDS_StatusError received"));
}


/**
 * onSDS_StatusStatus - called if SDS_Status property  is updated with status
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */
void AccessoryAppStateHandler::onSDS_StatusStatus(const ::boost::shared_ptr< ::sds2hmi_sds_fi::Sds2hmi_sds_fiProxy >& proxy, const ::boost::shared_ptr< ::sds2hmi_sds_fi::SDS_StatusStatus >& status)
{
   ETG_TRACE_USR4(("onSDS_StatusStatus received"));
   if (proxy == _spiSdshmiProxy)
   {
      switch (status->getState())
      {
         case ::sds2hmi_sds_fi_types::T_e8_SDS_Status__UNKNOWN :
         {
            _meSpeechAppState = SpeechAppState__SPI_APP_STATE_SPEECH_UNKNOWN;
            break;
         }
         case ::sds2hmi_sds_fi_types::T_e8_SDS_Status__UNAVAILABLE :
         case ::sds2hmi_sds_fi_types::T_e8_SDS_Status__LOADING:
         case ::sds2hmi_sds_fi_types::T_e8_SDS_Status__IDLE:
         case ::sds2hmi_sds_fi_types::T_e8_SDS_Status__ERROR:
         case ::sds2hmi_sds_fi_types::T_e8_SDS_Status__DATA_ERROR:
         case ::sds2hmi_sds_fi_types::T_e8_SDS_Status__IDLE_TTS_ONLY:
         {
            _meSpeechAppState = SpeechAppState__SPI_APP_STATE_SPEECH_END;
            break;
         }
         case ::sds2hmi_sds_fi_types::T_e8_SDS_Status__LISTENING:
         case ::sds2hmi_sds_fi_types::T_e8_SDS_Status__PROCESSING:
         {
            _meSpeechAppState = SpeechAppState__SPI_APP_STATE_SPEECH_RECOGNISING;
            break;
         }
         case ::sds2hmi_sds_fi_types::T_e8_SDS_Status__ACTIVE:
         case ::sds2hmi_sds_fi_types::T_e8_SDS_Status__PAUSE:
         {
            _meSpeechAppState = SpeechAppState__SPI_APP_STATE_SPEECH_SPEAKING;
            break;
         }
         default :
         {
            break;
         }
      }
      _mpSpiConnectionHandling->setAccSpeechAppStatus(_meSpeechAppState);
      ETG_TRACE_USR4(("AccessoryAppStateHandler: onSDS_StatusStatus call setAccessoryAppState eSpeechAppState = %d ePhoneAppState = %d eNavigationAppState = %d", ETG_CENUM(enSpeechAppState, _meSpeechAppState), ETG_CENUM(enPhoneAppState, _mePhoneAppState), ETG_CENUM(enNavigationAppState, _meNavigationAppState)));
      _mpSpiConnectionHandling->setAccessoryAppState(_meSpeechAppState, _mePhoneAppState, _meNavigationAppState);
   }
}


/**
 * onCallStatusNoticeError - called if CallStatusNotice  property is updated with error from Phone
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void AccessoryAppStateHandler::onCallStatusNoticeError(const ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeError >& /*error*/)
{
   ETG_TRACE_USR4(("onCallStatusNoticeError received"));
}


/**
 * onCallStatusNoticeStatus - received status update for CallStatusNotice property
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */
void AccessoryAppStateHandler::onCallStatusNoticeStatus(const ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy >& proxy , const ::boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeStatus >& status)
{
   ETG_TRACE_USR4(("onCallStatusNoticeStatus received"));
   if (proxy == _spitelephoneProxy)
   {
      enPhoneAppState enNewCallStatus = evaluateBTTelCallStatus(status);
      ETG_TRACE_USR4(("AccessoryAppStateHandler: onCallStatusNoticeStatus  ePhoneAppState = %d  NewCallState = %d",  ETG_CENUM(enPhoneAppState, _mePhoneAppState), ETG_CENUM(enPhoneAppState, enNewCallStatus)));
      if (isCallSatatusChange(enNewCallStatus))
      {
         _mpSpiConnectionHandling->setAccessoryAppState(_meSpeechAppState, _mePhoneAppState, _meNavigationAppState);
         _mpSpiConnectionHandling->vOnBTCallStatusUpdate(_mePhoneAppState);
         bool bIsCallActive = (_mePhoneAppState == PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE) ? true : false ;
         vUpdateAapCallDgStatus(bIsCallActive);
         vUpdateApplicationModeOnCall();
      }
   }
}


/**
 * evaluateBTTelCallStatus : This is a helper function to evaluate the BT call status according to both call instances
 * @param[in] :status
 * @parm[in]:void
 * @return enPhoneAppState
 */
enPhoneAppState AccessoryAppStateHandler:: evaluateBTTelCallStatus(const boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeStatus >& status)
{
   enPhoneAppState eCallInstance = PhoneAppState__SPI_APP_STATE_PHONE_NOTACTIVE;
   uint8 u8CallState1 = status->getOCallStatusNoticeStream()[0].getE8CallStatus();
   uint8 u8CallState2 = status->getOCallStatusNoticeStream()[1].getE8CallStatus();
   enPhoneAppState eCallInstance1 = evaluateCallInstance1(u8CallState1);
   enPhoneAppState eCallInstance2 = evaluateCallInstance2(u8CallState2);
   ETG_TRACE_USR4(("AccessoryAppStateHandler: evaluateBTTelCallStatus called eCallInstance1 = %d eCallInstance2 = %d", ETG_CENUM(enPhoneAppState, eCallInstance1), ETG_CENUM(enPhoneAppState, eCallInstance1)));
   if ((PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE == eCallInstance1) || (PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE == eCallInstance2))
   {
      eCallInstance = PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE;
   }
   return eCallInstance;
}


/**
 * evaluateCallInstance1 : This is a helper function to evaluate the BT call status for call instance1
 * @param[in] :u8CallState1
 * @parm[in]:void
 * @return enPhoneAppState
 */
enPhoneAppState AccessoryAppStateHandler::evaluateCallInstance1(uint8 u8CallState1)
{
   ETG_TRACE_USR4(("AccessoryAppStateHandler: evaluateCallInstance1 called %d " , u8CallState1));
   enPhoneAppState eCallInstance1 = PhoneAppState__SPI_APP_STATE_PHONE_UNKNOWN;
   switch (u8CallState1)
   {
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8DIALING:
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8ACTIVE:
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8ON_HOLD:
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8DISCONNECTING:
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8BUSY:
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8CONFERENCE:
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8IN_VOICEMAIL:
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8RINGTONE:
      {
         eCallInstance1 = PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE;
         break;
      }
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8IDLE:
      default:
      {
         eCallInstance1 = PhoneAppState__SPI_APP_STATE_PHONE_NOTACTIVE;
         break;
      }
   }
   return eCallInstance1;
}


/**
 * evaluateCallInstance2 : This is a helper function to evaluate the BT call status for call instance2
 * @param[in] :u8CallState1
 * @parm[in]:void
 * @return enPhoneAppState
 */
enPhoneAppState AccessoryAppStateHandler::evaluateCallInstance2(uint8 u8CallState2)
{
   ETG_TRACE_USR4(("AccessoryAppStateHandler: evaluateCallInstance2 called %d", u8CallState2));
   enPhoneAppState eCallInstance2 = PhoneAppState__SPI_APP_STATE_PHONE_UNKNOWN;
   switch (u8CallState2)
   {
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8DIALING:
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8ACTIVE:
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8ON_HOLD:
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8DISCONNECTING:
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8BUSY:
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8CONFERENCE:
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8IN_VOICEMAIL:
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8RINGTONE:
      {
         eCallInstance2 = PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE;
         break;
      }
      case ::most_Tel_fi_types::T_e8_TelCallStatus__e8IDLE:
      default:
      {
         eCallInstance2 = PhoneAppState__SPI_APP_STATE_PHONE_NOTACTIVE;
         break;
      }
   }
   return eCallInstance2;
}


/**
 * IscallSatatusChange : This is a helper function to evaluate call status
 * @param[in] :enPhoneAppState
  * @return: bool
 */
bool AccessoryAppStateHandler::isCallSatatusChange(enPhoneAppState newcallstatus)
{
   bool callStateChange = false;
   if (_mePhoneAppState != newcallstatus)
   {
      _mePhoneAppState = newcallstatus;
      callStateChange = true;
   }
   return callStateChange;
}


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
/**
 * This is a helper function to set Application mode when call is active
 * @param[in]: None
 * @return: None
 */
void AccessoryAppStateHandler::vUpdateApplicationModeOnCall()
{
   ETG_TRACE_USR1(("AccessoryAppStateHandler:  vUpdateApplicationModeOnCall Entered"));

   if ((_mePhoneAppState == PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE) || (_meDIPOPhoneAppState == PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE))
   {
      ETG_TRACE_USR1(("AccessoryAppStateHandler: vUpdateApplicationModeOnCall setting APPS_INCOMING_CALL"));
      POST_MSG((COURIER_MESSAGE_NEW(SetApplicationModeReqMsg)(APP_MODE_APPS_INCOMING_CALL)));
   }
   else if (_mpSpiConnectionHandling->bGetIsSpiVideoActive())
   {
      ETG_TRACE_USR1(("AccessoryAppStateHandler: vUpdateApplicationModeOnCall setting APPS"));
      POST_MSG((COURIER_MESSAGE_NEW(SetApplicationModeReqMsg)(APP_MODE_APPS)));
   }
   else
   {
      ETG_TRACE_USR1(("AccessoryAppStateHandler: vUpdateApplicationModeOnCall setting utility"));
      POST_MSG((COURIER_MESSAGE_NEW(SetApplicationModeReqMsg)(APP_MODE_MEDIA_UTILITY)));
   }
}


/**
 * This is a helper function to set Application mode when speech recognisition is active
 * @param[in]: None
 * @return: None
 */
void AccessoryAppStateHandler::vUpdateApplicationModeonSpeech()
{
   ETG_TRACE_USR1(("AccessoryAppStateHandler: vUpdateApplicationModeOnSpeech Entered"));

   if (((_mePhoneAppState != PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE) && (_meDIPOPhoneAppState != PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE)) && ((_meSpeechAppState == SpeechAppState__SPI_APP_STATE_SPEECH_SPEAKING) || (_meSpeechAppState == SpeechAppState__SPI_APP_STATE_SPEECH_RECOGNISING)) && (false == _mbSpeechActiveStatus))
   {
      _mbSpeechActiveStatus = true;
      ETG_TRACE_USR1(("AccessoryAppStateHandler: vUpdateApplicationModeOnSpeech setting SPCXVR"));
      POST_MSG((COURIER_MESSAGE_NEW(SetApplicationModeReqMsg)(APP_MODE_SPCXVR)));
   }
   else if ((_mpSpiConnectionHandling->bGetIsSpiVideoActive()) && (_meSpeechAppState == SpeechAppState__SPI_APP_STATE_SPEECH_END) && (true == _mbSpeechActiveStatus))
   {
      _mbSpeechActiveStatus = false;
      ETG_TRACE_USR1(("AccessoryAppStateHandler: vUpdateApplicationModeOnSpeech setting APPS"));
      POST_MSG((COURIER_MESSAGE_NEW(SetApplicationModeReqMsg)(APP_MODE_APPS)));
   }
   else if ((_meSpeechAppState == SpeechAppState__SPI_APP_STATE_SPEECH_END) && (true == _mbSpeechActiveStatus))
   {
      _mbSpeechActiveStatus = false;
      ETG_TRACE_USR1(("AccessoryAppStateHandler: vUpdateApplicationModeOnSpeech setting utility"));
      POST_MSG((COURIER_MESSAGE_NEW(SetApplicationModeReqMsg)(APP_MODE_MEDIA_UTILITY)));
   }
   else
   {
      //Nothing to do
   }
}


/**
 * vUpdateCallDgStatus : This is a helper function to set Dataguard of AapCall status in SM
 * @param[in]  - Bool
 * @param[out] - None
 * @return     - Void
 */
void AccessoryAppStateHandler::vUpdateAapCallDgStatus(bool bAapCallStatus)
{
   ETG_TRACE_USR4(("vUpdateAapCallDgStatus called"));
   (*_AndroidAutoCallStatus).misAndroidAutoCallStatus = bAapCallStatus;
   _AndroidAutoCallStatus.MarkAllItemsModified();

   if (_AndroidAutoCallStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_AndroidAutoCallStatus : Updation failed..!!!"));
   }
}


#endif
} //namespace Core
} //namespace app
