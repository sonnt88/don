/****************************************************************************
 * Copyright (C) Robert Bosch Car Multimedia GmbH, 2013
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ***************************************************************************/
#ifndef MediaGui_h
#define MediaGui_h

#include "AppBase/GuiComponentBase.h"
#include "HMIAppCtrl/Proxy/ProxyHandler.h"
#include "ProjectBaseMsgs.h"
#include "ProjectBaseTypes.h"
#include "AppHmi_MediaMessages.h"
#include "CgiExtensions/CourierMessageMapper.h"
#include "CourierTunnelService/CourierMessageReceiverStub.h"
#include "hmi_trace_if.h"

// datapool for persistent data
//#define DP_S_IMPORT_INTERFACE_FI
//#include "dp_hmi_01_if.h"
//class UserInterfaceControl;

namespace App {
namespace Core {


class MediaGui :
   public GuiComponentBase
{
   public:
      MediaGui();
      virtual ~MediaGui();

      virtual void preRun();
      virtual void postRun();

      static void TraceCmd_NotProcessedMsg(const unsigned char* pcu8Data);

   protected:
      virtual unsigned int getDefaultTraceClass();
      virtual void setupCgiInstance();

      hmibase::services::hmiappctrl::ProxyHandler _hmiAppCtrlProxyHandler;

   private:

      void PersistentValuesRead();
      void PersistentValuesWrite();
      //HmiAppMedia::PersMemVars _mvar;

      //UserInterfaceControl* _mUserInterfaceCntrl;
};


} // namespace Core
} // namespace App


#endif
