/**
 *  @file   <MediaHall.h>
 *  @author <ECV> - <kng3kor>
 *  @copyright (c) <2014> Robert Bosch Car Multimedia GmbH
 *  @addtogroup <AppHmi_media>
 *  @{
 */

#ifndef MediaHall_h
#define MediaHall_h

#include "CourierTunnelService/CourierMessageReceiverStub.h"
#include "AppBase/HallComponentBase.h"

#include "MediaDatabinding.h"
#include "Core/DeviceConnection/MediaDeviceConnectionHandling.h"
#include "Core/PlayScreen/MediaPlayScreenHandling.h"
#include "Core/SourceSwitch/MediaSourceHandling.h"
#include "Core/Browse/MediaBrowseHandling.h"
#include "Core/Utils/Language/LanguageHandling.h"
#include "App/Core/Spi/SpiConnectionHandling.h"
#include "App/Core/Spi/SpiConnectionObserver.h"
#include "App/Core/Spi/VehicleDataHandler.h"
#include "App/Core/Utils/TraceCommand.h"
#include "App/Core/Spi/SpiSettingListHandler.h"
#include "App/Core/Spi/SpiKeyHandler.h"
#include "App/Core/Spi/SpiMetaData.h"
#include "App/Core/BTAudio/BTSetting.h"
#include "App/Core/Spi/AccessoryAppStateHandler.h"
#include "App/Core/ContextSwitch/ContextSwitchHomeScreen.h"
#include "App/Core/ContextSwitch/Proxy/requestor/AppMedia_ContextRequestor.h"
#include "App/Core/ContextSwitch/Proxy/provider/AppMedia_ContextProvider.h"
#include "App/Core/Spi/SpiRestoreFactorySetting.h"
#include "App/Core/HmiDataHandlerExt/HmiDataHandlerExt.h"
#include "App/Core/DeviceStatus/DeviceStatusList.h"
#include "App/Core/MediaRestoreFactorySeeting/MediaRestoreFactorySetting.h"
#ifdef MEDIAPLAYER_SETTINGS_CHANGE_SUPPORT
#include "App/Core/MediaInfoSettings/MediaInfoSettingsHandling.h"
#endif
#ifdef PHOTOPLAYER_SUPPORT
#include "App/Core/PhotoPlayer/PhotoPlayerHandler.h"
#endif
#ifdef POPOUTLIST_SUPPORT
#include "App/Core/PopOutList/PopOutList.h"
#endif

#include "AppHmi_MediaMessages.h"
#include "AppHmi_MasterBase/AudioInterface/AudioDefines.h"

#include "App/Core/DataCollector/DataCollector.h"
#include "Common/PopupTimer/PopupTimer.h"
#include "Common/UserInterfaceCtrl/IUserInterfaceObserver.h"

#include "ProjectBaseMsgs.h"
#include "ProjectBaseTypes.h"
#include "AppHmi_MediaMessages.h"
#include "CgiExtensions/CourierMessageMapper.h"
#include "CourierTunnelService/CourierMessageReceiverStub.h"
#include "Common/HmiInfo/Proxy/HmiInfoProxy.h"
#include "hmi_trace_if.h"


class UserInterfaceControl;
class UserInterfaceScreenSaver;


#define AUDIOMANAGER_COMMANDINTERFACE org::genivi::audiomanager::CommandInterface
#define MASTERAUDIOSERVICE_INTERFACE ::bosch::cm::ai::hmi::masteraudioservice
namespace App {
namespace Core {

class MediaHall
   : public HallComponentBase ,  public IUserInterfaceObserver
{
   public:
      MediaHall();
      virtual ~MediaHall();

      // TimerCallbackIF
      //  virtual void onExpired(asf::core::Timer& timer, boost::shared_ptr<asf::core::TimerPayload> data);

      virtual void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange);
      virtual void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange);

      bool onCourierMessage(const UserInterfaceRegisterReq& msg);
      bool onCourierMessage(const UserInterfaceDeRegisterReq& msg);

      virtual void lockStateApplicationUpdate(int applicationId, bool lockState);
      bool onCourierMessage(const ApplicationStateUpdMsg& msg);

   protected:

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      ON_COURIER_MESSAGE(ApplicationStateUpdMsg)
      ON_COURIER_MESSAGE(UserInterfaceRegisterReq)
      ON_COURIER_MESSAGE(UserInterfaceDeRegisterReq)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_DELEGATE_TO_OBJ(_mediaDatabindingHandler)
      COURIER_MSG_DELEGATE_TO_OBJ(_mediaBrowseHandling)
#ifdef PHOTOPLAYER_SUPPORT
      COURIER_MSG_DELEGATE_TO_OBJ(_photoPlayerHandler)
#endif
      COURIER_MSG_DELEGATE_TO_OBJ(_mediaSourceHandling)
      COURIER_MSG_DELEGATE_TO_OBJ(_mediaPlayScreenHandling)
#ifdef MEDIAPLAYER_SETTINGS_CHANGE_SUPPORT
      COURIER_MSG_DELEGATE_TO_OBJ(_mediaInfoSettingsHandling)
#endif
#ifdef POPOUTLIST_SUPPORT
      COURIER_MSG_DELEGATE_TO_OBJ(_popOutList)
#endif
      COURIER_MSG_DELEGATE_TO_OBJ(_spiConnectionHandler)
      COURIER_MSG_DELEGATE_TO_OBJ(_spiSettingListHandler)
      COURIER_MSG_DELEGATE_TO_OBJ(_poContextProvider)
      COURIER_MSG_DELEGATE_TO_OBJ(_poContextRequestor)
      COURIER_MSG_DELEGATE_TO_OBJ(_dataCollector)
      COURIER_MSG_DELEGATE_TO_OBJ(_spiKeyHandler)
      COURIER_MSG_DELEGATE_TO_OBJ(_commonLanguage)
      COURIER_MSG_DELEGATE_TO_OBJ(_spiVehicleDataHandler)
      COURIER_MSG_DELEGATE_TO_OBJ(_spiAccessoryAppStateHandler)
      COURIER_MSG_DELEGATE_TO_OBJ(_spiRestoreFactorySetting)
      COURIER_MSG_DELEGATE_TO_OBJ(_hmiDataHandler)
      COURIER_MSG_DELEGATE_TO_OBJ(_deviceStatus)
      COURIER_MSG_DELEGATE_TO_OBJ(_pHmiInfoProxy)
      COURIER_MSG_DELEGATE_TO_CLASS(HallComponentBase)
      COURIER_MSG_DELEGATE_TO_REF(_mPopupTimer)
      COURIER_MSG_MAP_DELEGATE_END()

   private:
      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy > _MediaPlayerProxy;
      ::boost::shared_ptr< ::MIDW_TUNERMASTER_FI::MIDW_TUNERMASTER_FIProxy > _tunermasterFiProxy;
      ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy > _spiMidwServiceProxy;
      ::boost::shared_ptr < AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy > _commandIFProxy;
      ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy > _vehicleinfoProxy;
      ::boost::shared_ptr< ::dimming_main_fi::Dimming_main_fiProxy > _spiDimmingFiProxy;
      ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::AudioSourceChangeProxy > _audioSourceChangeSPIProxy;
      ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy > _soundPropertiesProxy;

      AppMedia_ContextProvider* _poContextProvider;
      AppMedia_ContextRequestor* _poContextRequestor;
      SpiSettingListHandler* _spiSettingListHandler;
      IDeviceConnection* _iDeviceConnection;
      MediaPlayScreenHandling* _mediaPlayScreenHandling;
      IBTSettings* _iBTSettings;
      MediaSourceHandling* _mediaSourceHandling;
      MediaDatabinding* _mediaDatabindingHandler;
      SpiConnectionHandling* _spiConnectionHandler;
      MediaBrowseHandling*  _mediaBrowseHandling;
#ifdef PHOTOPLAYER_SUPPORT
      PhotoPlayerHandler* _photoPlayerHandler;
#endif
      SpiKeyHandler* _spiKeyHandler;
      SpiMetaData* _spiMetaData;
      VehicleDataHandler* _spiVehicleDataHandler;
      TraceCommand* _traceCommand;
      DataCollector* _dataCollector;
      MediaLanguageHandling* _systemLanguage;
      AccessoryAppStateHandler* _spiAccessoryAppStateHandler;
      ::VehicleDataCommonHandler::VehicleDataHandler* _commonLanguage;
      ContextSwitchHomeScreen* _DeviceHomeScreen;
      SpiRestoreFactorySetting* _spiRestoreFactorySetting;
      PopupTimerHandler::PopupTimer _mPopupTimer;
      HmiDataHandlerExt* _hmiDataHandler;
      DeviceStatusList* _deviceStatus;
      UserInterfaceControl* _mUserInterfaceCntrl;
      SpiConnectionSubject* _spiConnectionNotifyer;
      UserInterfaceScreenSaver* _mUserInterfaceScreenSaver;
      MediaRestoreFactorySetting* _mediaRestoreFactorySetting;
      IHmiInfoProxy* _pHmiInfoProxy;
#ifdef MEDIAPLAYER_SETTINGS_CHANGE_SUPPORT
      MediaInfoSettingsHandling*  _mediaInfoSettingsHandling;
#endif
#ifdef POPOUTLIST_SUPPORT
      PopOutList* _popOutList;
#endif
      DECLARE_CLASS_LOGGER();
};


} // namespace Core
} // namespace App


#endif
