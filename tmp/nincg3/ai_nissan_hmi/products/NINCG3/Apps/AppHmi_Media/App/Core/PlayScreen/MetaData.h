/**
* @file MetaData.h
* @author RBEI/ECV-AIVI_MediaTeam
* @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
* @addtogroup AppHMI_Media
*/


#ifndef METADATA_H_
#define METADATA_H_

#include "MPlay_fi_types.h"

namespace App {
namespace Core {

enum MetaDataFields
{
   FIELD1 = 0,
   FIELD2,
   FIELD3,
   FIELD4,
   UNKNOWN,
   FIELDS_UNDEFINED
};


/**
* Provide getters to extract meta data
*/
class MetaData
{
   public:
      MetaData();
      MetaData(const ::MPlay_fi_types::T_MPlayMediaObject& oMediaObject);
      virtual ~MetaData();

      void setMediaObject(const ::MPlay_fi_types::T_MPlayMediaObject& oMediaObject);
      void setUnknownString(const std::string string);

      std::string getComposerName();
      std::string getBookTitle();
      std::string getChapterTitle();

      std::string getPodcastName();
      std::string getPodcastEpisode();
      std::string getPlayListName();

      std::string getGenre();
      std::string getArtistName();
      std::string getAlbumName();
      std::string getSongTitle();

      unsigned int getDeviceType();
      unsigned int getTagValue();
      ::MPlay_fi_types::T_e8_MPlayCategoryType getCategoryType();
   private:
      std::string _metaData[FIELDS_UNDEFINED];
      std::string _unknownString;
      ::MPlay_fi_types::T_e8_MPlayCategoryType _categoryType;
      ::MPlay_fi_types::T_e8_MPlayDeviceType _deviceType;
      unsigned int _tag;
};


} /* namespace Core */
} /* namespace App */
#endif /* METADATA_H_ */
