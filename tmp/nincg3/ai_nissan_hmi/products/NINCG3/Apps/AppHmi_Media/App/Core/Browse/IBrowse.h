/**
*  @file   IBrowse.h
*  @author ECV - vma6cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef IBROWSE_H_
#define IBROWSE_H_

#include "asf/core/Types.h"
#include "Widgets/2D/FlexList/ListDataProvider.h"
#include "IListFacade.h"
#include "MediaDatabinding.h"
#include "Common/VerticalQuickSearch/VerticalQuickSearch.h"
namespace App {
namespace Core {

enum BrowserType
{
   STATIC_BROWSE,
   METADATA_BROWSE,
   FOLDER_BROWSE
};


#define FLEX_LIST_START_INDEX        0
#define FLEX_LIST_MAX_BUFFER_SIZE   26
#define FLEX_LIST_MAX_QUICK_SEARCH   6

class IBrowse
{
   public:
      IBrowse();
      virtual ~IBrowse();
      virtual tSharedPtrDataProvider getDataProvider(RequestedListInfo& requestedlistInfo, uint32 activeDeviceType) = 0;
      virtual void populateNextListOrPlayItem(uint32 currentListId, uint32 selectedItem, uint32 activeDeviceType, uint32 activeDeviceTag) = 0;
      virtual void populatePreviousList(uint32 deviceTag) = 0;
      virtual void updateFolderBrowsingHistory(const FolderListInfo&) { }
      virtual void clearFolderBrowsingHistory(uint32 /*currentListHandle*/, std::string) {}
      virtual void updateFolderHistoryDuringIndexing(uint32 /*currentListHandle*/, uint32 /*listsize*/) {}
      virtual void checkBrowsingHistory() {}
      virtual int32 getActiveItemIndex(int32 /*selectedIndex*/)
      {
         return 0;
      }
      virtual void populateCurrentPlayingList(uint32 /*currentPlayingListHandle*/) { }
      virtual void setMetadataBrowseListId(uint32) { }
      virtual void setWindowStartIndex(uint32) = 0;
      virtual void initializeListParameters() = 0;
      virtual void updateEmptyListData(uint32) = 0;
      virtual void updateListData() = 0;
      virtual void updateListDataCDDA();
      virtual void setFocussedIndex(int32) = 0;
      static bool isAllAvailable(uint32 categoryType);
      virtual uint32 getNextListId(uint32 /*currentListId*/, uint32 /*selectedRow*/, uint32 /*activeDeviceType*/)
      {
         return 0;
      }
#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
      virtual uint32 getActiveQSIndex(uint32 /*absoluteIndex*/)
      {
         return 0;
      }
#endif
      void setListFacade(IListFacade* iListFacade);
      void updateMetaDataListResult(const ListResultInfo& listResultInfo);
      void updateFolderListResult(const FolderListInfo& folderListResult);
      void setIndexingDoneStatus(bool isIndexingDone);
      void setListItemText(std::vector<std::string>  listItem);
      uint32 getNodeIndexQuickSearch(uint32 Listtype) const;
      virtual bool checkFolderUpVisibility()
      {
         return false;
      }
      virtual void setFooterDetails(uint32) = 0;
#ifdef VERTICAL_KEYBOARD_SEARCH_SUPPORT
      virtual void updateSearchKeyBoardListInfo(trSearchKeyboard_ListItem item) = 0;
      virtual bool updateVerticalListToGuiList(const ButtonListItemUpdMsg& oMsg) = 0;
      virtual void SetActiveVerticalKeyLanguague() = 0;
      virtual void ShowVerticalKeyInfo(bool pDefault) = 0;
#endif
   protected:
      void initListStateMachine();
      void requestListData(const RequestedListInfo& requestedlistInfo);

      uint32 _activeDeviceType;
      uint32 _windowStartIndex;
      IListFacade* _iListFacade;
      ListResultInfo _listResultInfo;
      FolderListInfo _folderListInfo;
      bool _isIndexingDone;
      uint32 _focussedIndex;
      std::vector<std::string>  _sListItemText;
};


}
}


#endif /* IBROWSE_H_ */
