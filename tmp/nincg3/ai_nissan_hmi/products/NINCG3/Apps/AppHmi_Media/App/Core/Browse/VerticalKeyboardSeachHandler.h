/**
*  @file   VerticalKeyboardSearchHandler.h
*  @author ECV - mth4cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef VERTICALKEYBOARDSEACHHANDLER_H_
#define VERTICALKEYBOARDSEACHHANDLER_H_

#include "IBrowse.h"
#include "Common/VerticalQuickSearch/VerticalQuickSearch.h"
#include "App/datapool/MediaDataPoolConfig.h"
#ifdef VERTICAL_KEYBOARD_SEARCH_SUPPORT
#define MAIN_LISTSIZE 6
#define MAX_CHAR_FOR123 "~"

namespace App {
namespace Core {

class VerticalKeyboardSearch: public IBrowse
{
   public:
      VerticalKeyboardSearch();
      virtual ~VerticalKeyboardSearch();
      tSharedPtrDataProvider getDataProvider(RequestedListInfo& requestedlistInfo, uint32 activeDeviceType);
      void populateNextListOrPlayItem(uint32, uint32, uint32, uint32) {}
      void populatePreviousList(uint32) {}
      void updateListData() {}
      void initializeListParameters() {}
      void updateEmptyListData(uint32) {}
      void setWindowStartIndex(uint32) {}
      void setFocussedIndex(int32) {}
      void setFooterDetails(uint32) {}
      void updateSearchKeyBoardListInfo(trSearchKeyboard_ListItem item);

      bool updateVerticalListToGuiList(const ButtonListItemUpdMsg& oMsg);
      void SetActiveVerticalKeyLanguague();
      void ShowVerticalKeyInfo(bool pDefault);

      // Fill for default language
      tSharedPtrDataProvider fillMainVerticalList();
      tSharedPtrDataProvider fillExpandedVerticalList();

      // Fill for Selected language
      tSharedPtrDataProvider fillLanguageMainVerticalList();
      tSharedPtrDataProvider fillLanguageExpandedVerticalList();

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_MAP_DELEGATE_END()

   private :

      uint32 getStartIndexMainList(unsigned int pHdlRow);
      void searchIndexforExtList(unsigned int pHdlRow);
      void printVirtualKeyListInfo(std::map<std::string, trSearchKeyboard_ListItem> pStr);
      void printMainListInfo(std::map<uint8, std::string> pStr);

      //Update Default List info as LATIN Language
      void updateMainListInfo(std::vector <std::string> pStr);
      void updateExtListinfo(const std::vector<std::string> ListHandler);

      //Update Language List info as Other Language
      void updateOtherLangMainListInfo(std::vector <std::string> pStr);
      void updateOtherlangExtListinfo(const std::vector<std::string> ListHandler);

      void updateSystemLanguageInfo();
      void updateOtherLanguageInfo(uint8 pLanguage);

      // Map for Default language Latin
      std::map<std::string, trSearchKeyboard_ListItem> _mDefaultExtList;
      std::map<uint8, std::string> _mDefaultMainList;

      // Map for Other language
      std::map<std::string, trSearchKeyboard_ListItem> _mLanguageExtList;
      std::map<uint8, std::string> _mLanguageMainList;

      // Index value update
      void updateListCount(std::map<std::string, trSearchKeyboard_ListItem> &pStr);

      //flag for selected language
      bool _systemLanguage; // false = Latin true = other than Latin
      bool _switchSelectedlanguage; // false = default language switch from VK
};


}
}


#endif

#endif /* VERTICALKEYBOARDSEACHHANDLER_H_ */
