/**
 * @file <SpiConnectionObserver.cpp>
 * @author <ECV1> <A-IVI>
 * @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
 * @addtogroup <AppHmi_Media>
 */

#include "hall_std_if.h"
#include "SpiConnectionObserver.h"

namespace App {
namespace Core {

/**
 * Description     : Destructor of class SpiConnectionObserver
 */
SpiConnectionObserver::~SpiConnectionObserver()
{
}


/**
 * Description     : Constructor of class SpiConnectionObserver
 */
SpiConnectionObserver::SpiConnectionObserver()
{
}


/**
 * Description     : Destructor of class SpiConnectionSubject
 */
SpiConnectionSubject::~SpiConnectionSubject()
{
   if (!(_observers.empty()))
   {
      _observers.clear();
   }
}


/**
 * Description     : Constructor of class SpiConnectionSubject
 */
SpiConnectionSubject::SpiConnectionSubject()
{
}


/*
 * This function is used to call the function notifyObservers
 * @param[in]  - status
 * @param[out] - None
 * @return     - void
 */
void SpiConnectionSubject::handleSelectDeviceResult(::midw_smartphoneint_fi_types::T_e8_ResponseCode deviceSelectionResult)
{
   notifyObservers(deviceSelectionResult);
}


/*
 * This function is used to register the observers for data connection Notification status
 * @param[in]  - poObserver
 * @param[out] - None
 * @return     - void
 */
void SpiConnectionSubject::registerObserver(SpiConnectionObserver* poObserver)
{
   if (poObserver != NULL)
   {
      _observers.push_back(poObserver);
   }
}


/*
 * This function is used notify the observers of the data connection notification status
 * @param[in]  - status
 * @param[out] - None
 * @return     - void
 */
void SpiConnectionSubject::notifyObservers(::midw_smartphoneint_fi_types::T_e8_ResponseCode deviceSelectionResult)
{
   std::vector<SpiConnectionObserver*>::iterator iter = _observers.begin();
   while (iter != _observers.end())
   {
      if (*iter != NULL)
      {
         (*iter)->handleSelectDeviceResult(deviceSelectionResult);
      }
      iter++;
   }
}


}
}
