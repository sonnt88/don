/**
*  @file   KdsInfo.cpp
*  @author ECV - vma6cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#include "hall_std_if.h"
#include "KdsInfo.h"
#ifdef DP_DATAPOOL_ID
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_hmi_04_if.h"
#endif
#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
#include "dp_tclKdsCustomer.h"
#endif

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::KdsInfo::
#include "trcGenProj/Header/KdsInfo.cpp.trc.h"
#endif

namespace App {
namespace Core {

const unsigned int JPN_REGION = 0x19;
const unsigned int OTH_EUR_REGION = 0x06;
const unsigned int INVALID_REGION = 0xFF;
const unsigned int TKY_REGION = 0x04;
const unsigned int RUS_REGION = 0x05;
const unsigned int MEX_REGION = 0x02;
const unsigned int GCC_REGION = 0x0A;
const unsigned int ASR_REGION = 0x0C;
const unsigned int NZE_REGION = 0x0C;
const unsigned int SAF_REGION = 0x10;
uint8 KdsInfo::_currentRegion = INVALID_REGION;


KdsInfo::KdsInfo()
{
   ETG_I_REGISTER_FILE();
}


KdsInfo::~KdsInfo()
{
}


#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
/**
 * readRegionFromKDS - To read current language region from KDS
 * @param[in] none
 * @param[out] none
 * @return none
 */
void KdsInfo::readRegionFromKDS()
{
   ETG_TRACE_USR4(("KdsInfo:readRegionFromKDS is called"));

   if (_currentRegion == INVALID_REGION)
   {
      dp_tclKdsVehicleInformation languageRegion;
      uint8 configRegion = 0;

      if (DP_S32_NO_ERR == DP_s32GetConfigItem("VehicleInformation", "DestinationRegion1", &configRegion, 1))
      {
         ETG_TRACE_USR4(("KdsInfo:Current Region read from KDS =%d", configRegion)); //for testing
         _currentRegion = configRegion;
      }
   }
}


/**
 * readMyCarPlayConnectivity - To read currently available Connection status from KDS
 * @param[in] none
 * @param[out] none
 * @return none
 */

bool KdsInfo::isCarPlayEnabled()
{
   dp_tclKdsSystemConfiguration1 MyCarPlayStatus;
   uint8 MyCarPlayFeatureStatus = 0;
   uint8 kdsDataStatus = 0;
   kdsDataStatus = MyCarPlayStatus.u8GetCarPlay(MyCarPlayFeatureStatus);
   ETG_TRACE_USR4(("readMyCarPlayConnectivityFromKDS() MyCarPlayFeatureStatus = %d", MyCarPlayFeatureStatus));
   return (bool)MyCarPlayFeatureStatus;
}


/**
 * readMyAndroidAutoConnectivity - To read currently available Connection status from KDS
 * @param[in] none
 * @param[out] none
 * @return none
 */

bool KdsInfo::isAndroidAutoEnabled()
{
   dp_tclKdsSystemConfiguration1 MyAndroidAutoStatus;
   uint8 MyAndroidAutoFeatureStatus = 0;
   uint8 kdsDataStatus = 0;
   kdsDataStatus = MyAndroidAutoStatus.u8GetAndroidAuto(MyAndroidAutoFeatureStatus);
   ETG_TRACE_USR4(("readMyAndroidAutoConnectivityFromKDS() MyAndroidAutoFeatureStatus = %d", MyAndroidAutoFeatureStatus));
   return (bool)MyAndroidAutoFeatureStatus;
}


#else
/**
 * readRegionFromKDS - Stub function read current language region from KDS for unit test
 * @param[in] none
 * @param[out] none
 * @return none
 */
void KdsInfo::readRegionFromKDS()
{
}


/**
 * setCurrentRegion - Stub function set value for current language region variable
 * @param[in] currentRegion - current current language region setting to member variable
 * @param[out] none
 * @return none
 */
void KdsInfo::setCurrentRegion(uint8 currentRegion)
{
   _currentRegion = currentRegion;
}


/**
 * readMyCarPlayConnectivity - To read currently available Connection status from KDS
 * @param[in] none
 * @param[out] none
 * @return none
 */

bool KdsInfo::isCarPlayEnabled()
{
}


/**
 * readMyAndroidAutoConnectivity - To read currently available Connection status from KDS
 * @param[in] none
 * @param[out] none
 * @return none
 */

bool KdsInfo::isAndroidAutoEnabled()
{
}


#endif

/**
 * isJapanRegion - Check kds region is Japan and update corresponding variable for List Button Availability
 * @param[in] None
 * @param[out] None
 * @return bool
 */
bool KdsInfo::isJapanRegion()
{
   readRegionFromKDS();
   ETG_TRACE_USR4(("KdsInfo:isJapanRegion is called :%d", _currentRegion));
   return (_currentRegion == JPN_REGION) ? true : false;
}


/**
 * isEuropeonRegion - Check if kds region is Europeon
 * @param[in] None
 * @param[out] None
 * @return bool
 */
bool KdsInfo::isEuropeonRegion()
{
   readRegionFromKDS();
   ETG_TRACE_USR4(("KdsInfo:isEuropeonRegion is called :%d", _currentRegion));
   return (_currentRegion == OTH_EUR_REGION) ? true : false;
}


/**
 * isTurkeyRegion - Check if kds region is Turkey
 * @param[in] None
 * @param[out] None
 * @return bool
 */

bool KdsInfo::isTurkeyRegion()
{
   readRegionFromKDS();
   ETG_TRACE_USR4(("KdsInfo:isTurkeyRegion is called :%d", _currentRegion));
   return (_currentRegion == TKY_REGION) ? true : false;
}


/**
 * isRussianRegion - Check if kds region is Russian
 * @param[in] None
 * @param[out] None
 * @return bool
 */
bool KdsInfo::isRussianRegion()
{
   readRegionFromKDS();
   ETG_TRACE_USR4(("KdsInfo:isRussianRegion is called :%d", _currentRegion));
   return (_currentRegion == RUS_REGION) ? true : false;
}


/**
 * isMexicoRegion - Check if kds region is Mexico
 * @param[in] None
 * @param[out] None
 * @return bool
 */
bool KdsInfo::isMexicoRegion()
{
   readRegionFromKDS();
   ETG_TRACE_USR4(("KdsInfo:isMexicoRegion is called :%d", _currentRegion));
   return (_currentRegion == MEX_REGION) ? true : false;
}


/**
 * isGccRegion - Check if kds region is GCC
 * @param[in] None
 * @param[out] None
 * @return bool
 */
bool KdsInfo::isGccRegion()
{
   readRegionFromKDS();
   ETG_TRACE_USR4(("KdsInfo:isGccRegion is called :%d", _currentRegion));
   return (_currentRegion == GCC_REGION) ? true : false;
}


/**
 * isAsrRegion - Check if kds region is ASR
 * @param[in] None
 * @param[out] None
 * @return bool
 */
bool KdsInfo::isAsrRegion()
{
   readRegionFromKDS();
   ETG_TRACE_USR4(("KdsInfo:isAsrRegion is called :%d", _currentRegion));
   return (_currentRegion == ASR_REGION) ? true : false;
}


/**
 * isNewZealandRegion - Check if kds region is New Zealand
 * @param[in] None
 * @param[out] None
 * @return bool
 */
bool KdsInfo::isNewZealandRegion()
{
   readRegionFromKDS();
   ETG_TRACE_USR4(("KdsInfo:isNewZealandRegion is called :%d", _currentRegion));
   return (_currentRegion == NZE_REGION) ? true : false;
}


/**
 * isSouthAfricanRegion - Check if kds region is South Africa
 * @param[in] None
 * @param[out] None
 * @return bool
 */
bool KdsInfo::isSouthAfricanRegion()
{
   readRegionFromKDS();
   ETG_TRACE_USR4(("KdsInfo:isSouthAfricanRegion is called :%d", _currentRegion));
   return (_currentRegion == SAF_REGION) ? true : false;
}


}
}
