/*
 * MediaRestoreFactorySetting.cpp
 *
 *  Created on: Mar 17, 2016
 *      Author: sla7cob
 */
#include "hall_std_if.h"
#include "Core/MediaRestoreFactorySeeting/MediaRestoreFactorySetting.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS         TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::MediaRestoreFactorySetting::

#include "trcGenProj/Header/MediaRestoreFactorySetting.cpp.trc.h"
#endif
namespace App {
namespace Core {

/**
 * RestoreFactorySettings Class constructor
 */
MediaRestoreFactorySetting::MediaRestoreFactorySetting(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& mediaPlayerProxy):
   _mediaPlayerProxy(mediaPlayerProxy)
{
   ETG_I_REGISTER_FILE();
   _mdefSetServiceBase = DefSetServiceBase::GetInstance();
   if (_mdefSetServiceBase != NULL)
   {
      _mdefSetServiceBase->vRegisterforUpdate(this);
   }
}


/**
 * RestoreFactorySettings Class destructor
 */
MediaRestoreFactorySetting::~MediaRestoreFactorySetting()
{
   if (_mdefSetServiceBase != NULL)
   {
      _mdefSetServiceBase->vUnRegisterforUpdate(this);
      //donot delete this instance as its not created by us.
      _mdefSetServiceBase = NULL;
   }
}


/**
 * onClearMediaPlayerDataError
 * @param[in] proxy
 * @return void
 */
void MediaRestoreFactorySetting::onClearMediaPlayerDataError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::ClearMediaPlayerDataError >& /*error*/)
{
   ETG_TRACE_USR4(("MediaRestoreFactorySetting::onClearMediaPlayerDataError"));
}


/**
 * onClearMediaPlayerDataResult
 * @param[in] proxy
 * @return void
 */
void MediaRestoreFactorySetting::onClearMediaPlayerDataResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::ClearMediaPlayerDataResult >& /*result*/)
{
   ETG_TRACE_USR4(("MediaRestoreFactorySetting::onClearMediaPlayerDataResult"));
}


/**
 * reqPrepareResponse: To know the response of the respective APP when Restore factory setting is pressed.
 * @param[in]: None
 * @param[in] :None
 */
void MediaRestoreFactorySetting::reqPrepareResponse()
{
   ETG_TRACE_USR4(("MediaRestoreFactorySetting:: reqPrepareResponse"));
   if (_mdefSetServiceBase)
   {
      _mdefSetServiceBase->sendPrepareResponse(0, this);
   }
}


/**
 * reqExecuteResponse: To Execute the response when restore factory setting has been executed..
 * @param[in]: None
 * @param[in] :None
 */
void MediaRestoreFactorySetting::reqExecuteResponse()
{
   ETG_TRACE_USR4(("MediaRestoreFactorySetting:: reqExecuteResponse"));
   if (_mdefSetServiceBase)
   {
      _mediaPlayerProxy->sendClearMediaPlayerDataStart(*this);
      _mdefSetServiceBase->sendExecuteResponse(0, this);
   }
}


/**
 * reqExecuteResponse: To Finalize the response when restore factory setting has been executed.
 * @param[in]: None
 * @param[in] :None
 */
void MediaRestoreFactorySetting:: reqFinalizeResponse()
{
   ETG_TRACE_USR4(("MediaRestoreFactorySetting:: reqFinalizeResponse"));
   if (_mdefSetServiceBase)
   {
      _mdefSetServiceBase->sendFinalizeResponse(0, this);
   }
}


}
}
