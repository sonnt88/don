/**
*  @file   <KdsInfo_Test.cpp>
*  @author <ECV> - <pul1hc>
*  @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
*  @addtogroup <AppHmi_media>
*  @{
*/
#include "KdsInfo_Test.h"

uint8 JPN_REGION = 0x19;
uint8 JPN_REGION_BOUNDARY_ABOVE = 0x20;
uint8 JPN_REGION_BOUNDARY_BELOW = 0x18;

uint8 OTH_EUR_REGION = 0x06;
uint8 OTH_EUR_REGION_BOUNDARY_ABOVE = 0x07;
uint8 OTH_EUR_REGION_BOUNDARY_BELOW = 0x05;

uint8 INVALID_REGION = 0xFF;
uint8 INVALID_REGION_BOUNDARY_ABOVE = 0x00;
uint8 INVALID_REGION_BOUNDARY_BELOW = 0xFE;

int8 SIGNED_INT_ABNORMAL = -0x01;


INSTANTIATE_TEST_CASE_P(
   KdsInfo_isJapanRegion,
   KdsInfo_Test__isJapanRegion_True,
   ::testing::Values
   (
      make_tuple(JPN_REGION)
   )
);

TEST_P(KdsInfo_Test__isJapanRegion_True, isJapanRegion_JPN_REGION_True)
{
   bool actualJapanRegion;
   actualJapanRegion = KdsInfo::isJapanRegion();
   EXPECT_TRUE(actualJapanRegion);
}


INSTANTIATE_TEST_CASE_P(
   KdsInfo_isJapanRegion,
   KdsInfo_Test__isJapanRegion_False,
   ::testing::Values
   (
      make_tuple(JPN_REGION_BOUNDARY_ABOVE),
      make_tuple(JPN_REGION_BOUNDARY_BELOW),
      make_tuple(OTH_EUR_REGION),
      make_tuple(OTH_EUR_REGION_BOUNDARY_ABOVE),
      make_tuple(OTH_EUR_REGION_BOUNDARY_BELOW),
      make_tuple(INVALID_REGION),
      make_tuple(INVALID_REGION_BOUNDARY_ABOVE),
      make_tuple(INVALID_REGION_BOUNDARY_BELOW),
      make_tuple(SIGNED_INT_ABNORMAL)
   )
);

TEST_P(KdsInfo_Test__isJapanRegion_False, isJapanRegion_JPN_REGION_False)
{
   bool actualJapanRegion;
   actualJapanRegion = KdsInfo::isJapanRegion();
   EXPECT_FALSE(actualJapanRegion);
}


INSTANTIATE_TEST_CASE_P(
   KdsInfo_isEuropeonRegion,
   KdsInfo_Test__isEuropeonRegion_True,
   ::testing::Values
   (
      make_tuple(OTH_EUR_REGION)
   )
);

TEST_P(KdsInfo_Test__isEuropeonRegion_True, isEuropeonRegion_OTH_EUR_REGION_True)
{
   bool actualEuropeonRegion;
   actualEuropeonRegion = KdsInfo::isEuropeonRegion();
   EXPECT_TRUE(actualEuropeonRegion);
}


INSTANTIATE_TEST_CASE_P(
   KdsInfo_isEuropeonRegion,
   KdsInfo_Test__isEuropeonRegion_False,
   ::testing::Values
   (
      make_tuple(OTH_EUR_REGION_BOUNDARY_ABOVE),
      make_tuple(OTH_EUR_REGION_BOUNDARY_BELOW),
      make_tuple(JPN_REGION),
      make_tuple(JPN_REGION_BOUNDARY_ABOVE),
      make_tuple(JPN_REGION_BOUNDARY_BELOW),
      make_tuple(INVALID_REGION),
      make_tuple(INVALID_REGION_BOUNDARY_ABOVE),
      make_tuple(INVALID_REGION_BOUNDARY_BELOW),
      make_tuple(SIGNED_INT_ABNORMAL)
   )
);

TEST_P(KdsInfo_Test__isEuropeonRegion_False, isEuropeonRegion_OTH_EUR_REGION_False)
{
   bool actualEuropeonRegion;
   actualEuropeonRegion = KdsInfo::isEuropeonRegion();
   EXPECT_FALSE(actualEuropeonRegion);
}
