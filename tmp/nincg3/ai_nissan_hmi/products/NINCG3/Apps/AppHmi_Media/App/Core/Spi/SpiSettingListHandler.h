/**
 *  @file   SpiSettingListHandler.h
 *  @author ECV - rhk5kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */


#ifndef _SPISETTING_LISTHANDLER_H_
#define _SPISETTING_LISTHANDLER_H_

#include "AppHmi_MediaStateMachine.h"
#include "AppBase/ServiceAvailableIF.h"
#include "AppHmi_SpiMessages.h"
#include "Common/ListHandler/ListRegistry.h"
#include "CgiExtensions/ListDataProviderDistributor.h"
#include "Core/Spi/SpiConnectionHandling.h"
#include "SpiConnectionObserver.h"

//forward declaration of class SpiMediaDataPoolConfig
class SpiMediaDataPoolConfig;

namespace App {
namespace Core {
class SpiConnectionHandling;
class SpiConnectionSubject;
class SpiSettingListHandler : public ListImplementation,
   public hmibase::ServiceAvailableIF,
   public StartupSync::PropertyRegistrationIF,
   public ::midw_smartphoneint_fi::SetDeviceUsagePreferenceCallbackIF,
   public SpiConnectionObserver
{
   public:
      virtual ~SpiSettingListHandler();
      static SpiSettingListHandler* getInstance();
      SpiSettingListHandler(SpiConnectionHandling* paraSpiConnectionHandling , ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& spiListMidwServiceProxy , SpiConnectionSubject* paraSpiConnectionNotify);

      virtual bool onCourierMessage(const ButtonReactionMsg& oMsg);
      virtual bool onCourierMessage(const ListDateProviderReqMsg& oMsg);
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
      virtual bool onCourierMessage(const AutoLaunchConnectOnUpdateMsg& oMsg/*msg*/);
      virtual bool onCourierMessage(const RememberUsrChoiceOnNoCarPlayMsg& oMsg/*msg*/);
#endif
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      virtual bool onCourierMessage(const UpdateAPPSSettingsOnYesReqMsg& oMsg/*msg*/);
      virtual bool onCourierMessage(const UpdateAPPSSettingsOnNOReqMsg& oMsg/*msg*/);
#endif

      tSharedPtrDataProvider getSpiMainListDataProvider() const;
      tSharedPtrDataProvider updateSpiSettingsList() const;
      tSharedPtrDataProvider updateSpiCarPlayMainSettingsList(uint32 /*u32CarPlayAutoLaunch*/, uint32 /*u32AskOnConnectStatus*/) const;
      tSharedPtrDataProvider updateSpiSettingsList(uint32 /*u32AutoConnectionStatus*/, uint32 /*u32AskOnConnectStatus*/) const;
      tSharedPtrDataProvider updateAndroidAutoSettingsList(uint32 /*u32AutoConnectionStatus*/, uint32 /*u32AskOnConnectStatus*/) const;
      tSharedPtrDataProvider updateInfoAppsSpiList() const;
      tSharedPtrDataProvider fillDataForSpiMainSettingsList();
      tSharedPtrDataProvider fillDataForCarPlayMainSettingsList();
      tSharedPtrDataProvider fillDataForAndroidAutoMainSettingsList();
      tSharedPtrDataProvider fillDataForInfoAppsList();

      void onToggleCarPlayAskOnConnectStatus();
      void onToggleCarPlayAutoLaunchStatus();
      void onToggleAndroidAutoAskOnConnectStatus();
      void onToggleAndroidAutoLaunchStatus();

      void updateHMISettingOnUserAction(enSpiSettingUpdateStatus);

      virtual tSharedPtrDataProvider getListDataProvider(const ListDataInfo& listDataInfo);
      void vHandleListItemPressActions(const unsigned int listId, const unsigned int hdlRow);

      virtual void onSetDeviceUsagePreferenceError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetDeviceUsagePreferenceError >& error);
      virtual void onSetDeviceUsagePreferenceResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetDeviceUsagePreferenceResult >& result);

      void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);
      void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);
      void handleSelectDeviceResult(::midw_smartphoneint_fi_types::T_e8_ResponseCode);

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
      ON_COURIER_MESSAGE(AutoLaunchConnectOnUpdateMsg)
      ON_COURIER_MESSAGE(RememberUsrChoiceOnNoCarPlayMsg)
#endif
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      ON_COURIER_MESSAGE(UpdateAPPSSettingsOnYesReqMsg)
      ON_COURIER_MESSAGE(UpdateAPPSSettingsOnNOReqMsg)
#endif
      COURIER_MSG_MAP_DELEGATE_DEF_BEGIN()
      COURIER_MSG_DELEGATE_TO_CLASS(ListImplementation)
      COURIER_MSG_MAP_DELEGATE_DEF_END()
      COURIER_MSG_MAP_DELEGATE_END()
      static void traceCmd_CarPlaySettingsList(uint8 u8ListSettings);
      void triggerCourierMsg(uint8 u8triggerId);
      void updateSpiSettinglistOnRestoreFactotySetting();

   private:
      SpiMediaDataPoolConfig* _mDpHandler;
      SpiConnectionHandling* _mpSpiConnectionHandling;
      SpiConnectionSubject* _mSpiConnectionNotifyer;
      enDeviceCategory eDeviceCategory;
      ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& _spiListMidwServiceProxy;
      enSpiSettingUpdateStatus _mSpiSettingStatus;
      enDeviceType _mUpdateTriggerSettDevType;
      bool _mIsDeviceselectinReq;
      bool updateListIDSettingsSpiMain(ListProviderEventInfo&, const ButtonReactionMsg&);
      bool updateListIDSettingsCarPlay(ListProviderEventInfo&, const ButtonReactionMsg&);
      bool updateListIDSettingsAndroidAutoMain(ListProviderEventInfo&, const ButtonReactionMsg&);
      bool updateListIDSpiInfoApps(ListProviderEventInfo&, const ButtonReactionMsg&);
      void setSpiSettingPreference(enDeviceCategory , ::midw_smartphoneint_fi_types::T_e8_EnabledInfo);
      ::midw_smartphoneint_fi_types::T_e8_DeviceType convConnDeviceCategoryToType(::midw_smartphoneint_fi_types::T_e8_DeviceCategory);
      void callUpdateCarPlaySettinglist();
      void callUpdateAndriodAutoSettinglist();
      void startWaitAnimation();
      void stopWaitAnimation();
};


} //namespace Core
} //namespace app

#endif  /* _SPISETTING_LISTHANDLER_H_ */
