/**
 *  @file   ContextSwitchHomeScreen.cpp
 *  @Author:put5cob (RBEI/ECV3)
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */


#include "hall_std_if.h"
#include "ContextSwitchHomeScreen.h"
#include <algorithm>

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS         TR_CLASS_APPHMI_MEDIA_HALL_UTILS
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::ContextSwitchHomeScreen::

#include "trcGenProj/Header/ContextSwitchHomeScreen.cpp.trc.h"
#endif

using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;

namespace App {
namespace Core {

/**
 * ContextSwitchHomeScreen constroctor
 * @param[in] None
 * @param[out] None
 * @return void
 */
ContextSwitchHomeScreen::ContextSwitchHomeScreen(AppMedia_ContextProvider* poContextProvider)
   : _poContextProvider(poContextProvider)
{
   _mDeviceInfo.clear();
   if (_poContextProvider)
   {
      _poContextProvider->vAddAvailableContext(MEDIA_CONTEXT_HOMESCREEN_AUX);
      _poContextProvider->vAddAvailableContext(MEDIA_CONTEXT_HOMESCREEN_BT_AUDIO);
   }
   setDefaultConnection();
}


/**
 * ContextSwitchHomeScreen Destroctor
 * @param[in] None
 * @param[out] None
 * @return void
 */
ContextSwitchHomeScreen::~ContextSwitchHomeScreen()
{
   _mDeviceInfo.clear();
   _poContextProvider = NULL;
}


/**
 * setDeviceInfoHomeScreen - Handle device connection
 * @param[in] deviceInfo
 * @param[out] None
 * @return void
 */

void ContextSwitchHomeScreen::setDeviceInfoHomeScreen(::MPlay_fi_types::T_MPlayDeviceInfo deviceInfo)
{
   ETG_TRACE_USR4(("ContextSwitchHomeScreen::setDeviceInfoHomeScreen..........."));
   ::MPlay_fi_types::T_MPlayDeviceInfo::iterator itr = deviceInfo.begin();
   TDeviceInfo ldeviceinfo;

   for (; itr != deviceInfo.end(); itr++)
   {
      bool bIsDeviceConnected = itr->getBDeviceConnected();
      ldeviceinfo.DeviceName = itr->getSDeviceName();
      ldeviceinfo.DeviceTag = itr->getU8DeviceTag();
      ldeviceinfo.DeviceType = itr->getE8DeviceType();

      ETG_TRACE_USR4(("IsDeviceConnected = %d  NAME = %s" , bIsDeviceConnected,  ldeviceinfo.DeviceName.c_str()));
      ETG_TRACE_USR4(("i8DeviceTag= %d ", ldeviceinfo.DeviceTag));
      ETG_TRACE_USR4(("i8DeviceType= %d ", ldeviceinfo.DeviceType));

      //if connected device is disconnected
      if (bIsDeviceConnected == FALSE)
      {
         UpdateDeviceDisconnection(ldeviceinfo);
      }
      else if ((bIsDeviceConnected == TRUE))
      {
         /*Update not available Id to home screen*/
         UpdateDeviceConnection(ldeviceinfo);
      }
   }
   printAvailDevice();
}


/**
 * UpdateDeviceDisconnection - update list while device remove from port
 * @param[in] deviceInfo
 * @param[out] None
 * @return void
 */

void ContextSwitchHomeScreen::UpdateDeviceDisconnection(TDeviceInfo Deviceinfo)
{
   ETG_TRACE_USR4(("ContextSwitchHomeScreen::UpdateDeviceDisconnection"));
   std::map<Courier::UInt32, TDeviceInfo>::iterator it;

   for (it = _mDeviceInfo.begin(); it != _mDeviceInfo.end();)
   {
      if ((it->second.DeviceType == Deviceinfo.DeviceType) && (it->second.DeviceTag == Deviceinfo.DeviceTag))
      {
         std::map<Courier::UInt32, TDeviceInfo>::iterator temp_it = it;
         it++;

         ETG_TRACE_USR4(("Device Removed %d , type = %d  , tag = %d ",  temp_it->first, temp_it->second.DeviceType, temp_it->second.DeviceTag));
         if (_poContextProvider)
         {
            //Do not sent remove notification due to BT default screen
            if (MEDIA_CONTEXT_HOMESCREEN_BT_AUDIO != temp_it->first)
            {
               _poContextProvider->vRemoveContext(temp_it->first);
            }
         }
         _mDeviceInfo.erase(temp_it);
      }
      else
      {
         it++;
      }
   }
   printAvailDevice();
}


/**
 * UpdateDeviceConnection - update list while device insert into port
 * @param[in] deviceInfo
 * @param[out] None
 * @return void
 */
void ContextSwitchHomeScreen::UpdateDeviceConnection(TDeviceInfo Deviceinfo)
{
   ETG_TRACE_USR4(("ContextSwitchHomeScreen::UpdateDeviceConnection"));
   std::map<Courier::UInt32, TDeviceInfo>::iterator it;
   enum enContextId lCurrentID = MEDIA_CONTEXT_END;

   switch (Deviceinfo.DeviceType)
   {
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPOD:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPHONE:
      {
         /*IPOD2 will insert if IPOD1 is not available*/
         if ((it = _mDeviceInfo.find(MEDIA_CONTEXT_HOMESCREEN_IPOD1)) != _mDeviceInfo.end())
         {
            /*Insert if device is not Available */
            if (false == isDeviceAvailabe(Deviceinfo))
            {
               lCurrentID = MEDIA_CONTEXT_HOMESCREEN_IPOD2;
               _mDeviceInfo.insert(std::pair<Courier::UInt32, TDeviceInfo>(MEDIA_CONTEXT_HOMESCREEN_IPOD2, Deviceinfo));
               ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: MEDIA_CONTEXT_HOMESCREEN_IPOD2"));
            }
         }
         else
         {
            if (false == isDeviceAvailabe(Deviceinfo)) /*Insert if device is not Available */
            {
               lCurrentID = MEDIA_CONTEXT_HOMESCREEN_IPOD1;
               _mDeviceInfo.insert(std::pair<Courier::UInt32, TDeviceInfo>(MEDIA_CONTEXT_HOMESCREEN_IPOD1, Deviceinfo));
               ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: MEDIA_CONTEXT_HOMESCREEN_IPOD1"));
            }
         }
         break;
      }
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_USB:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_MTP:
      {
         /*USB2 will insert if USB1 is not available*/
         if ((it = _mDeviceInfo.find(MEDIA_CONTEXT_HOMESCREEN_USB1)) != _mDeviceInfo.end())
         {
            if (false == isDeviceAvailabe(Deviceinfo))
            {
               lCurrentID = MEDIA_CONTEXT_HOMESCREEN_USB2;
               _mDeviceInfo.insert(std::pair<Courier::UInt32, TDeviceInfo>(MEDIA_CONTEXT_HOMESCREEN_USB2, Deviceinfo));
               ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: MEDIA_CONTEXT_HOMESCREEN_USB2"));
            }
         }
         else
         {
            if (false == isDeviceAvailabe(Deviceinfo)) /*Insert if device is not Available */
            {
               lCurrentID = MEDIA_CONTEXT_HOMESCREEN_USB1;
               _mDeviceInfo.insert(std::pair<Courier::UInt32, TDeviceInfo>(MEDIA_CONTEXT_HOMESCREEN_USB1, Deviceinfo));
               ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: MEDIA_CONTEXT_HOMESCREEN_USB1"));
            }
         }
         break;
      }
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH:
      {
         lCurrentID = MEDIA_CONTEXT_HOMESCREEN_BT_AUDIO;
         _mDeviceInfo.insert(std::pair<Courier::UInt32, TDeviceInfo>(MEDIA_CONTEXT_HOMESCREEN_BT_AUDIO, Deviceinfo));
         ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: MEDIA_CONTEXT_HOMESCREEN_BT_AUDIO"));

         break;
      }
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDROM:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA:
      {
         lCurrentID = MEDIA_CONTEXT_HOMESCREEN_CD;
         _mDeviceInfo.insert(std::pair<Courier::UInt32, TDeviceInfo>(MEDIA_CONTEXT_HOMESCREEN_CD, Deviceinfo));
         ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: MEDIA_CONTEXT_HOMESCREEN_CD"));
         break;
      }

      default:
      {
         ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: Default"));
         break;
      }
   }
   if (_poContextProvider && lCurrentID != MEDIA_CONTEXT_END)
   {
      _poContextProvider->vAddAvailableContext(lCurrentID);
   }
}


/**
 * printAvailDevice - Print Available list info
 * @param[in] None
 * @param[out] None
 * @return void
 */
void ContextSwitchHomeScreen::printAvailDevice()
{
   std::map<Courier::UInt32, TDeviceInfo>::iterator it;

   ETG_TRACE_USR4(("ContextSwitchHomeScreen::printAvailDevice"));
   for (it = _mDeviceInfo.begin(); it != _mDeviceInfo.end(); ++it)
   {
      ETG_TRACE_USR4(("Context ID -- %d ",  it->first));
      ETG_TRACE_USR4(("Device Name -- %s ", it->second.DeviceName.c_str()));
      ETG_TRACE_USR4(("Device Tag -- %d ", it->second.DeviceTag));
      ETG_TRACE_USR4(("Device Type -- %d ", it->second.DeviceType));
   }
}


/**
 * printAvailDevice - Check device is Available in list
 * @param[in] deviceInfo
 * @param[out] None
 * @return void
 */
bool ContextSwitchHomeScreen::isDeviceAvailabe(TDeviceInfo Deviceinfo)
{
   std::map<Courier::UInt32, TDeviceInfo>::iterator it;

   ETG_TRACE_USR4(("ContextSwitchHomeScreen::isDeviceAvailabe"));
   for (it = _mDeviceInfo.begin(); it != _mDeviceInfo.end(); it++)
   {
      if (it->second.DeviceType == Deviceinfo.DeviceType && it->second.DeviceTag == Deviceinfo.DeviceTag)
      {
         return true;
      }
   }
   return false;
}


/**
 * getHomeScreenSourceInfo - Provide source id for contextId
 * @param[in] ContextId
 * @param[out] SrcID , SubSrcID
 * @return void
 */
bool ContextSwitchHomeScreen::getHomeScreenSourceInfo(Courier::UInt32 ContextId, int32& SrcID, int32& SubSrcID)
{
   ETG_TRACE_USR4(("ContextSwitchHomeScreen: getHomeScreenSourceInfo %d", ContextId));
   bool isFound = false;
   std::map<Courier::UInt32, TDeviceInfo>::iterator it;

   if ((it = _mDeviceInfo.find(ContextId)) != _mDeviceInfo.end())
   {
      SrcID = it->second.DeviceType;
      SubSrcID = it->second.DeviceTag;
      isFound = true;
   }

   return isFound;
}


/**
 * getHomeScreenSourceInfo - Provide source id for contextId
 * @param[in] ContextId
 * @param[out] SrcID , SubSrcID
 * @return void
 */
void ContextSwitchHomeScreen::setDefaultConnection()
{
   if (_poContextProvider)
   {
      _poContextProvider->vRemoveContext(MEDIA_CONTEXT_HOMESCREEN_CD);
      _poContextProvider->vRemoveContext(MEDIA_CONTEXT_HOMESCREEN_USB1);
      _poContextProvider->vRemoveContext(MEDIA_CONTEXT_HOMESCREEN_USB2);
      _poContextProvider->vRemoveContext(MEDIA_CONTEXT_HOMESCREEN_IPOD1);
      _poContextProvider->vRemoveContext(MEDIA_CONTEXT_HOMESCREEN_IPOD2);
   }
}


}
}
