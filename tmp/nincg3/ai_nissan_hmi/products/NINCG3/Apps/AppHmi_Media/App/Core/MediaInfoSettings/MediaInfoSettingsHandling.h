/**
*  @file   MediaInfoSettingsHandling.h
*  @author ECV - pkm8cob
*  @copyright (c) 2016 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef MEDIAINFOSETTINGSEHANDLING_H
#define MEDIAINFOSETTINGSEHANDLING_H

#include "AppHmi_MediaConstants.h"
#include "AppHmi_MediaMessages.h"
#include "AppHmi_MediaTypes.h"
#include "CgiExtensions/ListDataProviderDistributor.h"
#include "CourierTunnelService/CourierMessageReceiverStub.h"
#include "Common/ListHandler/ListRegistry.h"
#include "mplay_MediaPlayer_FIProxy.h"
#include "MediaDatabinding.h"
#include "Core/MediaInfoSettings/MediaSettings.h"
#include "Core/MediaInfoSettings/MediaMetaDataInfo.h"
#include "MPlay_fi_types.h"
#include "Core/PhotoPlayer/PhotoPlayerHandler.h"

namespace App {
namespace Core {

class MediaInfoSettingsHandling :
   public ListImplementation
{
   public:
      MediaInfoSettingsHandling(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy, PhotoPlayerHandler* pPhotoPlayerHandler);
      tSharedPtrDataProvider getListDataProvider(const ListDataInfo& _ListInfo);
      virtual ~MediaInfoSettingsHandling();
      virtual bool onCourierMessage(const ListDateProviderReqMsg& oMsg);
      virtual bool onCourierMessage(const MediaInfoSettingsBtnPressUpdMsg& oMsg);
      bool onCourierMessage(const ButtonReactionMsg& oMsg);

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      ON_COURIER_MESSAGE(MediaInfoSettingsBtnPressUpdMsg)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_DELEGATE_TO_CLASS(ListImplementation)
      COURIER_MSG_MAP_DELEGATE_DEF_END()
      COURIER_MSG_DELEGATE_TO_OBJ(_mediaSettings)
      COURIER_MSG_DELEGATE_TO_OBJ(_mediaMetaDataInfo)
      COURIER_MSG_MAP_DELEGATE_END()

   private:
      void updateListData(ListDataInfo& listDataInfo, uint32_t listId, uint32_t startIndex, uint32_t windowSize);
      ListDataInfo _listDataInfo;
      MediaSettings* _mediaSettings;
      MediaMetaDataInfo* _mediaMetaDataInfo;
      PhotoPlayerHandler* _photoPlayerHandler;
      uint8 _InfoSettings;
      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& _mediaPlayerProxy;
};


}//end of namespace Core
}//end of namespace App


#endif /* MEDIAINFOSETTINGSEHANDLING_H */
