/**
 *  @file   MediaInfoSettingsHandling.cpp
 *  @author ECV - pkm8cob
 *  @copyright (c) 2016 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#include "hall_std_if.h"
#include "MediaInfoSettingsHandling.h"
#include "Core/Utils/MediaProxyUtility.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL_MEDIA_INFOSETTINGS
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::MediaInfoSettingsHandling::
#include "trcGenProj/Header/MediaInfoSettingsHandling.cpp.trc.h"
#endif
using namespace MPlay_fi_types;

namespace App {
namespace Core {
/**
 * @Constructor
 */
MediaInfoSettingsHandling::MediaInfoSettingsHandling(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& mediaPlayerProxy, PhotoPlayerHandler* pPhotoPlayerHandler)
   : _mediaPlayerProxy(mediaPlayerProxy),
     _photoPlayerHandler(pPhotoPlayerHandler),
     _InfoSettings(Enum_UNKNOWN_INFO_SETTINGS)
{
   ETG_I_REGISTER_FILE();

   ETG_TRACE_USR4(("MediaInfoSettingsHandling Constructor"));
   MediaSettings::createInstance(_mediaPlayerProxy);
   MediaMetaDataInfo::createInstance(_mediaPlayerProxy);
   _mediaSettings = MediaSettings::getInstance();
   _mediaMetaDataInfo = MediaMetaDataInfo::getInstance();

   ListRegistry::s_getInstance().addListImplementation(LIST_ID_INFORMATION_SETTINGS, this);
}


/**
 * @Destructor
 */
MediaInfoSettingsHandling::~MediaInfoSettingsHandling()
{
   ETG_TRACE_USR4(("MediaInfoSettingsHandling Destructor"));

   _mediaPlayerProxy.reset();
   if (_mediaSettings)
   {
      delete _mediaSettings;
      _mediaSettings = NULL;
   }
   if (_mediaMetaDataInfo)
   {
      delete _mediaMetaDataInfo;
      _mediaMetaDataInfo = NULL;
   }
   if (_photoPlayerHandler)
   {
      _photoPlayerHandler = NULL;
   }
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_INFORMATION_SETTINGS);
}


/**
 * getListDataProvider - Gets the ListDataProvider from the corresponding listdataprovider functions
 * @param[in] listId
 * @parm[out] none
 * @return tSharedPtrDataProvider
 */
tSharedPtrDataProvider MediaInfoSettingsHandling::getListDataProvider(const ListDataInfo& listInfo)
{
   ETG_TRACE_USR4(("MediaInfoSettingsHandling:getListDataProvider is called %d ", listInfo.listId));
   switch (_InfoSettings)
   {
      case VIDEO_PLAYER_SETTINGS:
#ifdef PHOTOPLAYER_SUPPORT
      case PHOTO_PLAYER_SETTINGS:
#endif
      {
         if (_mediaSettings)
         {
            return _mediaSettings->getMediaSettingsListDataProvider(_InfoSettings);
         }
         break;
      }
      case VIDEO_INFORMATION:
      {
         if ((_mediaPlayerProxy->hasNowPlaying()))
         {
            const ::MPlay_fi_types::T_MPlayMediaObject& oMediaObject = _mediaPlayerProxy->getNowPlaying().getOMediaObject();
            if (oMediaObject.getU8DeviceTag() == MediaDatabinding::getInstance().getActiveDeviceTag() &&
                  oMediaObject.getE8MediaType() == T_e8_MPlayMediaType__e8MTY_VIDEO)
            {
               if (_mediaMetaDataInfo)
               {
                  return _mediaMetaDataInfo->getMediaMetaDataInfoListDataProvider(oMediaObject, _InfoSettings);
               }
            }
         }
         break;
      }
      case PHOTO_INFORMATION:
      {
         const ::MPlay_fi_types::T_MPlayMediaObject oMediaObject = _photoPlayerHandler->getCurrentImage();
         if (_mediaMetaDataInfo)
         {
            return _mediaMetaDataInfo->getMediaMetaDataInfoListDataProvider(oMediaObject, _InfoSettings);
         }
         break;
      }
      default:
      {
         break;
      }
   }
   ETG_TRACE_USR4(("MediaInfoSettingsHandling:Default : ERROR:Invalid List"));
   return tSharedPtrDataProvider();
}


/**
 * updateListData - Update the List Data.
 * @param[in] listId
 * @param[in] startIndex
 * @param[in] windowSize
 * @parm[out] listDataInfo
 * @return None
 */
void MediaInfoSettingsHandling::updateListData(ListDataInfo& listDataInfo, uint32_t listId, uint32_t startIndex, uint32_t windowSize)
{
   ETG_TRACE_USR4(("MediaInfoSettingsHandling updateListData"));
   listDataInfo.listId = listId;
   listDataInfo.startIndex = startIndex;
   listDataInfo.windowSize = windowSize;
}


/**
 * onCourierMessage -

 * @param[in] ButtonReactionMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaInfoSettingsHandling::onCourierMessage(const ButtonReactionMsg& oMsg)
{
   ETG_TRACE_USR4(("MediaInfoSettingsHandling ButtonReactionMsg"));
   ListProviderEventInfo info;
   if (ListProviderEventInfo::GetItemIdentifierInfo(oMsg.GetSender(), info))
   {
      ETG_TRACE_USR4(("MediaInfoSettingsHandling ButtonReactionMsg %d ", info.getListId()));
   }

   return false;
}


/**
 * onCourierMessage -

 * @param[in] ListDateProviderReqMsg
 * @parm[out] bool
 */
bool MediaInfoSettingsHandling::onCourierMessage(const ListDateProviderReqMsg& oMsg)
{
   ETG_TRACE_USR4(("MediaInfoSettingsHandling ListDateProviderReqMsg %d ", oMsg.GetListId()));
   if (oMsg.GetListId() == LIST_ID_INFORMATION_SETTINGS)
   {
      updateListData(_listDataInfo, oMsg.GetListId(), oMsg.GetStartIndex(), oMsg.GetWindowElementSize());
      return ListRegistry::s_getInstance().updateList(_listDataInfo);
   }
   return false;
}


/**
 * onCourierMessage -

 * @param[in] MediaInfoSettingsBtnPressUpdMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaInfoSettingsHandling::onCourierMessage(const MediaInfoSettingsBtnPressUpdMsg& msg)
{
   _InfoSettings = msg.GetInfoSettings();
   ETG_TRACE_USR4(("MediaInfoSettingsHandling MediaInfoSettingsBtnPressUpdMsg  %d ", _InfoSettings));
   POST_MSG((COURIER_MESSAGE_NEW(MediaInfoSettingsMsg)(1)));

   return true;
}


}//end of namespace Core
}//end of namespace App
