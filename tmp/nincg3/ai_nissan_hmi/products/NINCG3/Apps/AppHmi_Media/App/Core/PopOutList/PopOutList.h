/******************************************************************
*FILE: PopOutList.h
*SW-COMPONENT: APPHMI_MEDIA
*DESCRIPTION: Serves as an example for copyright comments
*COPYRIGHT: © 2016 RBEI
*
*The reproduction, distribution and utilization of this file as
*well as the communication of its contents to others without express
*authorization is prohibited. Offenders will be held liable for the
*payment of damages. All rights reserved in the event of the grant
*of a patent, utility model or design.
******************************************************************/

#ifndef POPOUTLIST_H_
#define POPOUTLIST_H_

//#include "AppHmi_MediaConstants.h"
//#include "AppHmi_MediaMessages.h"
//#include "AppHmi_MediaTypes.h"
#include "CgiExtensions/ListDataProviderDistributor.h"
#include "CourierTunnelService/CourierMessageReceiverStub.h"
#include "Common/ListHandler/ListRegistry.h"
#include "mplay_MediaPlayer_FIProxy.h"
#include "MediaDatabinding.h"
#include "MPlay_fi_types.h"

namespace App {
namespace Core {

class PopOutList :
   public ListImplementation
{
   public:
      PopOutList(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy);
      tSharedPtrDataProvider getListDataProvider(const ListDataInfo& _ListInfo);
      virtual ~PopOutList();
      bool onCourierMessage(const ScreenNameUpdMsg& oMsg);
      bool onCourierMessage(const OptionButtonPressedMsg& oMsg);
      bool onCourierMessage(const OptionButtonClosePressedMsg& oMsg);

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      ON_COURIER_MESSAGE(ScreenNameUpdMsg)
      ON_COURIER_MESSAGE(OptionButtonPressedMsg)
      ON_COURIER_MESSAGE(OptionButtonClosePressedMsg)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_DELEGATE_TO_CLASS(ListImplementation)
      COURIER_MSG_MAP_DELEGATE_DEF_END()
      COURIER_MSG_MAP_DELEGATE_END()

   private:

      ListDataInfo _listDataInfo;
      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& _mediaPlayerProxy;
      std::map<uint32, std::vector<std::string> > _maplistData;
      std::vector<std::string> _currentPopoutList;
      uint32 _sceneID;
      uint32 _optionbtnvisibleInfo;

      void updateListData(ListDataInfo& listDataInfo, uint32_t listId, uint32_t startIndex, uint32_t windowSize);
      void updatePopOutListDetailsForSceneID();
      const char* getlistItemTemplateForPopOutList(uint32& index);
};


}//end of namespace Core
}//end of namespace App


#endif /* POPOUTLIST_H_ */
