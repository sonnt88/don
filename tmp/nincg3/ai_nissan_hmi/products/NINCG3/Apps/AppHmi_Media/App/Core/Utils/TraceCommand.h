/*
 * TraceCommand.h
 *
 *  Created on: May 28, 2015
 *      Author: sla7cob
 */

#ifndef TRACECOMMAND_H_
#define TRACECOMMAND_H_


namespace App {
namespace Core {

class TraceCommand
{
   public:
      TraceCommand();
      virtual ~TraceCommand();
      //functions used to support TML
      static void traceCmd_ShowTrackInfo();
      static void tracecmd_ShowPlayModeStatus();
      static void tracecmd_ShowCurrentListTypeOfMedtadataBrowse();
      static void tracecmd_ShowCurrentFolderPath();
      static void tracecmd_ShowIndexingInfo();
      static void tracecmd_ShowActiveMediaDevice();
      static void tracecmd_ShowConnectedDevices();
      static void tracecmd_ShowAudioSource();
      static void tracecmd_ShowBTDeviceName();
      static void tracecmd_ShowActiveDeviceTag();
      static void tracecmd_ShowTotalNumberOfFilesAndFolders();
      static void tracecmd_ShowActivePlayingItemTag();
      static void tracecmd_ShowAlbumArtInfo();
      static void tracecmd_ShowBTDeviceConnectedStatus();
      //end of functions used to support TML

      static void traceCmd_SetRtEncDirection(uint8 direction);
};


}//end of namespace Core
}//end of namespace App
#endif /* TRACECOMMAND_H_ */
