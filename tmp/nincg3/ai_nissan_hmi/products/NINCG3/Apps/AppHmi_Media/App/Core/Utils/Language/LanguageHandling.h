/**
*  @file   MediaLanguageHandling.h
*  @author ECV - put5cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef _MEDIA_LANGUAGE_HANDLING_H_
#define _MEDIA_LANGUAGE_HANDLING_H_


#include "AppHmi_MediaStateMachine.h"
#include "mplay_MediaPlayer_FIProxy.h"
#include "App/datapool/MediaDataPoolConfig.h"
#include "VehicleDataHandler.h"



//forward declaration of class MediaDataPoolConfig
class MediaDataPoolConfig;

namespace App
{
namespace Core
{
class MediaLanguageHandling
   : public ::mplay_MediaPlayer_FI::LanguageCallbackIF
   , public ::VehicleDataCommonHandler::iLanguageUpdate
{
   public:
      virtual ~MediaLanguageHandling();
      MediaLanguageHandling(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy, ::VehicleDataCommonHandler::VehicleDataHandler*);

      virtual void onLanguageError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
                                   const ::boost::shared_ptr< mplay_MediaPlayer_FI::LanguageError >& /*error*/);
      virtual void onLanguageStatus(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy>& /*proxy*/,
                                    const ::boost::shared_ptr< mplay_MediaPlayer_FI::LanguageStatus>& /*status*/);

      /* Info from Language handler */
      virtual uint8 readLanguageOnStartUp();
      virtual void writeLanguageintoDP(uint8 languageIndex);
      void updateLanguageTable(std::vector<vehicle_main_fi_types::T_Language_SourceTable> languageSourceTable);

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_MAP_DELEGATE_END()

   private:
      /*Set language property to Mplay FI*/
      void setMPlayLanguage(uint8 languageIndex);
      void initializeSystemLanguage();

      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& _mediaPlayerProxy;
      ::VehicleDataCommonHandler::VehicleDataHandler* _commonLanguage;
      std::map< ::vehicle_main_fi_types::T_e8_Language_Code, ::MPlay_fi_types::T_e8_MPlayLanguage> _languageISOCodeMap;
};

}
}
#endif // _MEDIA_LANGUAGE_HANDLING_H_
