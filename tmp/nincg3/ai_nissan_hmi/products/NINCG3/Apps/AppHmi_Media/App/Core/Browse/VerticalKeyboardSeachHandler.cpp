/**
 *  @file   VerticalKeyboardSearchHandler.cpp
 *  @author ECV - mth4cob
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#include "hall_std_if.h"
#include "Core/Browse/VerticalKeyboardSeachHandler.h"
#include "Core/Utils/MediaProxyUtility.h"
#include "vehicle_main_fi_typesConst.h"
#ifdef VERTICAL_KEYBOARD_SEARCH_SUPPORT
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL_BROWSE
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::VerticalKeyboardSeach::
#include "trcGenProj/Header/VerticalKeyboardSeachHandler.cpp.trc.h"
#endif


static const char* const DATA_CONTEXT_LIST_BUTTON_VERTICAL_SEARCH_MAIN_LIST_ITEM    = "MainIndexBarText";
static const char* const DATA_CONTEXT_LIST_BUTTON_VERTICAL_SEARCH_EXPANDED_LIST_ITEM    = "FilteredIndexBarText";

namespace App {
namespace Core {
using namespace ::vehicle_main_fi_types;

VerticalKeyboardSearch::VerticalKeyboardSearch()
{
   _mDefaultExtList.clear();
   _mDefaultMainList.clear();
   _mLanguageExtList.clear();
   _mLanguageMainList.clear();
   _switchSelectedlanguage = true;
   _systemLanguage = false;
   ETG_I_REGISTER_FILE();
}


VerticalKeyboardSearch::~VerticalKeyboardSearch()
{
   _mDefaultExtList.clear();
   _mDefaultMainList.clear();
   _mLanguageExtList.clear();
   _mLanguageMainList.clear();
}


/**
 *  VerticalKeyboardSeach::getDataProvider - Gets the ListDataProvider
 *  @param [in] requestedlistInfo - structure containing the requested list information
 *  @param [in] activeDeviceType - current active device type
 *  @return tSharedPtrDataProvider
 */
tSharedPtrDataProvider VerticalKeyboardSearch::getDataProvider(RequestedListInfo& requestedlistInfo, uint32 activeDeviceType)
{
   (void)activeDeviceType;
   tSharedPtrDataProvider dataPtr;
   ETG_TRACE_USR4(("VerticalKeyboardSeach::	getDataProvider._listType %d",	requestedlistInfo._listType));

   if (_switchSelectedlanguage == true) // Fill for default list if language switch is Latin
   {
      if (requestedlistInfo._listType == LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST)
      {
         dataPtr = VerticalKeyboardSearch::fillMainVerticalList();
      }
      else if (requestedlistInfo._listType == LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST)
      {
         dataPtr = VerticalKeyboardSearch::fillExpandedVerticalList();
      }
   }
   else // Fill for default list if language switch is other language
   {
      if (requestedlistInfo._listType == LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST)
      {
         dataPtr = VerticalKeyboardSearch::fillLanguageMainVerticalList();
      }
      else if (requestedlistInfo._listType == LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST)
      {
         dataPtr = VerticalKeyboardSearch::fillLanguageExpandedVerticalList();
      }
   }
   return dataPtr;
}


/**
 *  VerticalKeyboardSeach::ButtonListItemUpdMsg - on press of list item
 *  @param [in] omsg
 *  @return tSharedPtrDataProvider
 */

bool VerticalKeyboardSearch::updateVerticalListToGuiList(const ButtonListItemUpdMsg& oMsg)
{
   ETG_TRACE_USR4(("VerticalKeyboardSeach::ButtonListItemUpdMsg %d", oMsg.GetListId()));
   unsigned int listId      = oMsg.GetListId();     // the list id for generic access
   unsigned int hdlRow      = oMsg.GetHdl();     // normaly the index
   //unsigned int hdlCol		 = oMsg.GetSubHdl();
   bool status = true;
   RequestedListInfo requestedListInfo;

   uint32 swtichindex = 0;
   uint32 activeDeviceType = MediaDatabinding::getInstance().getCurrentActiveMediaDevice();
   requestedListInfo._deviceTag = MediaProxyUtility::getInstance().getActiveDeviceTag();

   if (listId == LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST)
   {
      requestedListInfo._listType =  LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST;
      requestedListInfo._bufferSize = 5;
      swtichindex = 1;
      requestedListInfo._startIndex = getStartIndexMainList(hdlRow);
   }
   else if (listId == LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST)
   {
      requestedListInfo._listType =  LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST;
      requestedListInfo._bufferSize = 6;
      requestedListInfo._startIndex = 0;
      swtichindex = 0;
   }
   else
   {
      requestedListInfo._listType =  LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST;
      requestedListInfo._bufferSize = 5;
      swtichindex = 0;
      requestedListInfo._startIndex = 0;
   }

   ETG_TRACE_USR4(("VerticalKeyboardSeach::	requestedListInfo._listType %d",	requestedListInfo._listType));
   ETG_TRACE_USR4(("VerticalKeyboardSeach::	requestedListInfo._startIndex %d",	requestedListInfo._startIndex));
   ETG_TRACE_USR4(("VerticalKeyboardSeach::	requestedListInfo.swtichindex %d",	swtichindex));
   ETG_TRACE_USR4(("VerticalKeyboardSeach::	requestedListInfo._bufferSize %d",	requestedListInfo._bufferSize));

   if (listId == LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST || listId == LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST)
   {
      VerticalKeyboardSearch::getDataProvider(requestedListInfo, activeDeviceType);
   }

   if (listId == LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST)
   {
      searchIndexforExtList(hdlRow);
   }

   status = MediaDatabinding::getInstance().updateVerticalSearchListIndex((requestedListInfo._startIndex), swtichindex);

   return status;
}


/**
 *  VerticalKeyboardSeach::fillMainVerticalList - to fill Main VerticalList
 *  @param [in] none
 *  @return tSharedPtrDataProvider
 */
tSharedPtrDataProvider VerticalKeyboardSearch::fillMainVerticalList()
{
   ETG_TRACE_USR4(("VerticalKeyboardSeach::fillMainVerticalList"));

   printMainListInfo(_mDefaultMainList);
   ListDataProviderBuilder listBuilder(LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST, DATA_CONTEXT_LIST_BUTTON_VERTICAL_SEARCH_MAIN_LIST_ITEM);;
   std::map<uint8, std::string>::iterator itr;

   for (itr = _mDefaultMainList.begin(); itr != _mDefaultMainList.end(); itr++)
   {
      listBuilder.AddItem(itr->first, 0, DATA_CONTEXT_LIST_BUTTON_VERTICAL_SEARCH_MAIN_LIST_ITEM).AddData(itr->second.c_str());
   }
   tSharedPtrDataProvider dataProvider = listBuilder.CreateDataProvider();
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));

   return tSharedPtrDataProvider();
}


/**
 *  VerticalKeyboardSeach::fillLanguageMainVerticalList - to fill Main VerticalList
 *  @param [in] none
 *  @return tSharedPtrDataProvider
 */
tSharedPtrDataProvider VerticalKeyboardSearch::fillLanguageMainVerticalList()
{
   printMainListInfo(_mLanguageMainList);
   ETG_TRACE_USR4(("VerticalKeyboardSeach::fillLanguageMainVerticalList"));
   ListDataProviderBuilder listBuilder(LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST, DATA_CONTEXT_LIST_BUTTON_VERTICAL_SEARCH_MAIN_LIST_ITEM);;
   std::map<uint8, std::string>::iterator itr;

   for (itr = _mLanguageMainList.begin(); itr != _mLanguageMainList.end(); itr++)
   {
      listBuilder.AddItem(itr->first, 0, DATA_CONTEXT_LIST_BUTTON_VERTICAL_SEARCH_MAIN_LIST_ITEM).AddData(itr->second.c_str());
   }
   tSharedPtrDataProvider dataProvider = listBuilder.CreateDataProvider();
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));

   return tSharedPtrDataProvider();
}


/**
 *  VerticalKeyboardSeach::fillExpandedVerticalList - to fill Expanded VerticalList
 *  @param [in] none
 *  @return tSharedPtrDataProvider
 */
tSharedPtrDataProvider VerticalKeyboardSearch::fillExpandedVerticalList()
{
   ListDataProviderBuilder listBuilder(LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST, DATA_CONTEXT_LIST_BUTTON_VERTICAL_SEARCH_EXPANDED_LIST_ITEM);
   //uint32 itemIdx = 0;
   Candera::UInt32 guiListIndex = 0;
   uint32 listSize = _mDefaultExtList.size();
   uint32 windowSize = _mDefaultExtList.size();

   ETG_TRACE_USR4(("VerticalKeyboardSeach::fillExpandedVerticalList"));

   tSharedPtrDataProvider dataProvider = ListDataProvider::newBackEndInterface(LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST, 0,
                                         windowSize, listSize);  //MEDIA_BASE_LIST_ID is added to u32ListType to get the LIST id value

   std::map<std::string, trSearchKeyboard_ListItem>::iterator itr;
   for (itr = _mDefaultExtList.begin(); itr != _mDefaultExtList.end() && guiListIndex < windowSize; itr++)
   {
      listBuilder.AddItem(guiListIndex, 0, DATA_CONTEXT_LIST_BUTTON_VERTICAL_SEARCH_EXPANDED_LIST_ITEM).AddData(itr->second.sSearchKeyboardLetter.c_str()).AddData(itr->second.bLetterAvailable);
      guiListIndex++;
   }

   dataProvider = listBuilder.CreateDataProvider();
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));
   return tSharedPtrDataProvider();
}


/**
 *  VerticalKeyboardSeach::fillExpandedVerticalList - to fill Expanded VerticalList
 *  @param [in] none
 *  @return tSharedPtrDataProvider
 */
tSharedPtrDataProvider VerticalKeyboardSearch::fillLanguageExpandedVerticalList()
{
   ListDataProviderBuilder listBuilder(LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST, DATA_CONTEXT_LIST_BUTTON_VERTICAL_SEARCH_EXPANDED_LIST_ITEM);
   Candera::UInt32 guiListIndex = 0;
   uint32 listSize = _mLanguageExtList.size();
   uint32 windowSize = _mLanguageExtList.size();

   ETG_TRACE_USR4(("VerticalKeyboardSeach::fillLanguageExpandedVerticalList"));

   tSharedPtrDataProvider dataProvider = ListDataProvider::newBackEndInterface(LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST, 0,
                                         windowSize, listSize);  //MEDIA_BASE_LIST_ID is added to u32ListType to get the LIST id value

   std::map<std::string, trSearchKeyboard_ListItem>::iterator itr;
   for (itr = _mLanguageExtList.begin(); itr != _mLanguageExtList.end() && guiListIndex < windowSize; itr++)
   {
      listBuilder.AddItem(guiListIndex, 0, DATA_CONTEXT_LIST_BUTTON_VERTICAL_SEARCH_EXPANDED_LIST_ITEM).AddData(itr->first.c_str()).AddData(itr->second.bLetterAvailable);
      guiListIndex++;
   }

   dataProvider = listBuilder.CreateDataProvider();
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));
   return tSharedPtrDataProvider();
}


/**
 * Merge extended List from Mediaplayer and List from Language Handler
 * VerticalKeyboardSeach::updateSearchKeyBoardListInfo - Service List
 *  @param [in] none
 *  @return Void
 */
void VerticalKeyboardSearch::updateSearchKeyBoardListInfo(trSearchKeyboard_ListItem item)
{
   std::map<std::string, trSearchKeyboard_ListItem>::iterator itr;
   ETG_TRACE_USR4(("VerticalKeyboardSeach::updateSearchKeyBoardListInfo"));

   if ((itr = _mDefaultExtList.find(item.sSearchKeyboardLetter)) != _mDefaultExtList.end())
   {
      itr->second.bLetterAvailable = item.bLetterAvailable;
      itr->second.u32LetterStartIndex = item.u32LetterStartIndex;
      itr->second.u32LetterEndIndex = item.u32LetterEndIndex;
      itr->second.sSearchKeyboardLetter = item.sSearchKeyboardLetter;
      itr->second.u32ListHandle = item.u32ListHandle;
   }
   /*Special Handling for string "123"*/
   if (item.sSearchKeyboardLetter == "#")
   {
      if ((itr = _mDefaultExtList.find(MAX_CHAR_FOR123)) != _mDefaultExtList.end())
      {
         itr->second.bLetterAvailable = item.bLetterAvailable;
         itr->second.u32LetterStartIndex = item.u32LetterStartIndex;
         itr->second.u32LetterEndIndex = item.u32LetterEndIndex;
         itr->second.sSearchKeyboardLetter = "123";
         itr->second.u32ListHandle = item.u32ListHandle;
      }
   }
   /*Update list count for default list*/
   updateListCount(_mDefaultExtList);

   /*Extended list has to fill if system language is not default*/
   if (_systemLanguage == true)
   {
      if ((itr = _mLanguageExtList.find(item.sSearchKeyboardLetter)) != _mLanguageExtList.end())
      {
         itr->second.bLetterAvailable = item.bLetterAvailable;
         itr->second.u32LetterStartIndex = item.u32LetterStartIndex;
         itr->second.u32LetterEndIndex = item.u32LetterEndIndex;
         itr->second.sSearchKeyboardLetter = item.sSearchKeyboardLetter;
         itr->second.u32ListHandle = item.u32ListHandle;
      }
      /*Update list count for Language list*/
      updateListCount(_mLanguageExtList);
   }
}


/**
 *  Insert Extended list from language handler
 *  VerticalKeyboardSeach::updateDefaultListhandelinfo - Deafult list
 *  @param [in] none
 *  @return Void
 */
void VerticalKeyboardSearch::updateExtListinfo(const std::vector<std::string> ListHandler)
{
   ETG_TRACE_USR4(("VerticalKeyboardSeach::updateDefaultListhandelinfo"));
   trSearchKeyboard_ListItem lDefault;
   _mDefaultExtList.clear();
   uint32 lindex = 0;
   for (lindex = 0 ; lindex < ListHandler.size(); lindex++)
   {
      if (std::string::npos != ListHandler[lindex].find_first_not_of("0123456789"))
      {
         lDefault.bLetterAvailable = false;
         lDefault.u32LetterStartIndex = 0;
         lDefault.u32LetterEndIndex = 0;
         lDefault.sSearchKeyboardLetter = ListHandler[lindex];
         lDefault.countIndex = lindex;
         _mDefaultExtList.insert(std::pair<std::string, trSearchKeyboard_ListItem>(ListHandler[lindex], lDefault));
      }
   }
   /*Special Handling for "123" string*/
   lDefault.bLetterAvailable = false;
   lDefault.u32LetterStartIndex = 0;
   lDefault.u32LetterEndIndex = 0;
   lDefault.sSearchKeyboardLetter = "123";
   lDefault.countIndex = _mDefaultExtList.size();
   _mDefaultExtList.insert(std::pair<std::string, trSearchKeyboard_ListItem>(MAX_CHAR_FOR123, lDefault));
}


/**
 *  Insert other language Extended list from language handler
 *  VerticalKeyboardSeach::updateLanguageDefaultExtListinfo - Deafult list
 *  @param [in] none
 *  @return Void
 */
void VerticalKeyboardSearch::updateOtherlangExtListinfo(const std::vector<std::string> ListHandler)
{
   ETG_TRACE_USR4(("VerticalKeyboardSeach::updateDefaultListhandelinfo"));
   trSearchKeyboard_ListItem lDefault;
   _mLanguageExtList.clear();
   uint32 lindex = 0;
   for (lindex = 0 ; lindex < ListHandler.size(); lindex++)
   {
      lDefault.bLetterAvailable = false;
      lDefault.u32LetterStartIndex = 0;
      lDefault.u32LetterEndIndex = 0;
      lDefault.sSearchKeyboardLetter = ListHandler[lindex];
      lDefault.countIndex = lindex;
      _mLanguageExtList.insert(std::pair<std::string, trSearchKeyboard_ListItem>(ListHandler[lindex], lDefault));
   }
   printVirtualKeyListInfo(_mLanguageExtList);
}


/**
 *  Print To test
 *  VerticalKeyboardSeach::print
 *  @param [in] none
 *  @return Void//static const char ListHandelstr[MAIN_LISTSIZE] []= {"A", "F", "K", "P", "U", "Z"};
 */
void VerticalKeyboardSearch::printVirtualKeyListInfo(std::map<std::string, trSearchKeyboard_ListItem> pStr)
{
   std::map<std::string, trSearchKeyboard_ListItem>::iterator itr;

   ETG_TRACE_USR4(("printVirtualKeyListInfo"));
   for (itr = pStr.begin(); itr != pStr.end(); itr++)
   {
      ETG_TRACE_USR4(("bLetterAvailable = %d", itr->second.bLetterAvailable));
      ETG_TRACE_USR4(("u32LetterStartIndex = %d", itr->second.u32LetterStartIndex));
      ETG_TRACE_USR4(("u32LetterEndIndex = %d", itr->second.u32LetterEndIndex));
      ETG_TRACE_USR4(("sSearchKeyboardLetter = %s", itr->second.sSearchKeyboardLetter.c_str()));
      ETG_TRACE_USR4(("u32ListHandle = %d", itr->second.u32ListHandle));
      ETG_TRACE_USR4(("u32ListCount = %d", itr->second.countIndex));
   }
}


/**
 *  Print To test
 *  printMainListInfo::print
 *  @param [in] none
 *  @return Void//static const char ListHandelstr[MAIN_LISTSIZE] []= {"A", "F", "K", "P", "U", "Z"};
 */
void VerticalKeyboardSearch::printMainListInfo(std::map<uint8, std::string> pStr)
{
   std::map<uint8, std::string>::iterator itr;

   ETG_TRACE_USR4(("printMainListInfo"));
   for (itr = pStr.begin(); itr != pStr.end(); itr++)
   {
      ETG_TRACE_USR4(("bLetterAvailable = %s", itr->second.c_str()));
   }
}


/**
 * Update Main List
 * updateMainListInfo:: Update main list info from language handler
 *  @param [in] none
 *  @return Void
 */
void VerticalKeyboardSearch::updateMainListInfo(std::vector <std::string> pStr)
{
   _mDefaultMainList.clear();
   uint8 lindex = 0;

   for (lindex = 0; lindex < pStr.size() ; lindex++)
   {
      _mDefaultMainList.insert(std::pair<uint8, std::string>(lindex, pStr[lindex]));
   }
   printMainListInfo(_mDefaultMainList);
}


/**
 * Update Main List
 * updateLanguageMainListInfo:: Update main list info from other language handler
 *  @param [in] none
 *  @return Void
 */
void VerticalKeyboardSearch::updateOtherLangMainListInfo(std::vector <std::string> pStr)
{
   _mLanguageMainList.clear();

   for (uint8 lindex = 0; lindex < pStr.size() ; lindex++)
   {
      _mLanguageMainList.insert(std::pair<uint8, std::string>(lindex, pStr[lindex]));
   }
   printMainListInfo(_mLanguageMainList);
}


/**
 *  return position of character from extended list
 *  getStartIndexMainList::
 *  @param [in] pHdlRow
 *  @return start index
 */
uint32 VerticalKeyboardSearch::getStartIndexMainList(unsigned int pHdlRow)
{
   std::map<std::string, trSearchKeyboard_ListItem>::iterator e_itr;
   std::map<uint8, std::string>::iterator m_itr;
   uint32 position = 0;

   if (_switchSelectedlanguage == true) // Fill for default list if language switch is Latin
   {
      m_itr = _mDefaultMainList.find(pHdlRow); //find character from main list
      if (m_itr != _mDefaultMainList.end())
      {
         e_itr = _mDefaultExtList.find(m_itr->second.c_str()); //search character from ext list
         if (e_itr != _mDefaultExtList.end())
         {
            position = e_itr->second.countIndex;
            if (position < 3)
            {
               position = 0;
            }
            else if (position > ((_mDefaultExtList.size() - 3)))
            {
               position = position - 3;
            }
            else
            {
               position = position - 2;   //get position of selected character
            }
         }
      }
   }
   else // Fill for Language list if language switch other than Latin
   {
      m_itr = _mLanguageMainList.find(pHdlRow); //find character from main list
      if (m_itr != _mLanguageMainList.end())
      {
         e_itr = _mLanguageExtList.find(m_itr->second.c_str()); //search character from ext list
         if (e_itr != _mLanguageExtList.end())
         {
            position = e_itr->second.countIndex;
            if (position < 3)
            {
               position = 0;
            }
            else if (position > ((_mLanguageExtList.size() - 3)))
            {
               position = position - 3;
            }
            else
            {
               position = position - 2;   //get position of selected character
            }
         }
      }
   }
   ETG_TRACE_USR4(("VerticalKeyboardSeach::position %d", position));
   return position;
}


/**
 *  Quick search for selected character from extended list
 *  searchIndexforExtList::
 *  @param [in] pHdlRow
 *  @return start Quick Search
 */
void VerticalKeyboardSearch::searchIndexforExtList(unsigned int pHdlRow)
{
   if (_switchSelectedlanguage == true) // Fill for default list if language switch is Latin
   {
      std::map<std::string, trSearchKeyboard_ListItem>::iterator e_itr = _mDefaultExtList.begin();
      if (pHdlRow < _mDefaultExtList.size())
      {
         std::advance(e_itr, pHdlRow); // find out map info from input index
         ETG_TRACE_USR4(("main bLetterAvailable = %d", e_itr->second.bLetterAvailable));
         ETG_TRACE_USR4(("u32LetterStartIndex = %d", e_itr->second.u32LetterStartIndex));
         ETG_TRACE_USR4(("sSearchKeyboardLetter = %s", e_itr->second.sSearchKeyboardLetter.c_str()));
         ETG_TRACE_USR4(("u32ListCount = %d", e_itr->second.countIndex));

         if (_iListFacade)
         {
            // Quick search from selected character
            if (e_itr->first != MAX_CHAR_FOR123)
            {
               _iListFacade->vRequestQSMediaplayerFilelist(e_itr->first.c_str(), e_itr->second.u32LetterStartIndex);
            }
            else // search "#" for "123" from media player
            {
               _iListFacade->vRequestQSMediaplayerFilelist("#", e_itr->second.u32LetterStartIndex);
            }
         }
      }
   }
   else // Fill for Language list if language switch other than Latin
   {
      std::map<std::string, trSearchKeyboard_ListItem>::iterator e_itr = _mLanguageExtList.begin();
      if (pHdlRow < _mLanguageExtList.size())
      {
         std::advance(e_itr, pHdlRow); // find out map info from input index
         ETG_TRACE_USR4(("Language bLetterAvailable = %d", e_itr->second.bLetterAvailable));
         ETG_TRACE_USR4(("u32LetterStartIndex = %d", e_itr->second.u32LetterStartIndex));
         ETG_TRACE_USR4(("sSearchKeyboardLetter = %s", e_itr->second.sSearchKeyboardLetter.c_str()));
         ETG_TRACE_USR4(("u32ListCount = %d", e_itr->second.countIndex));

         if (_iListFacade)
         {
            // Quick search from selected character
            _iListFacade->vRequestQSMediaplayerFilelist(e_itr->first.c_str(), e_itr->second.u32LetterStartIndex);
         }
      }
   }
}


/**
 *  populate character from default language list
 *  handler
 *  SetActiveVerticalKeyLanguague::
 *  @param [in] activeLanguage
 *  @return start Quick Search
 */
void VerticalKeyboardSearch::SetActiveVerticalKeyLanguague()
{
   uint8 activeLanguage;

   activeLanguage = MediaDataPoolConfig::getInstance()->getLanguage();
   ETG_TRACE_USR4(("SetActiveVerticalKeyLanguague %d", activeLanguage));

   switch (activeLanguage)
   {
      case T_e8_Language_Code__English_US:
      case T_e8_Language_Code__English_UK:
      case T_e8_Language_Code__English_Australian:
      case T_e8_Language_Code__English_India:
      case T_e8_Language_Code__English_Canadian:
      case T_e8_Language_Code__French:
      case T_e8_Language_Code__German:
      case T_e8_Language_Code__Spanish:
      case T_e8_Language_Code__Spanish_Latin_American:
      case T_e8_Language_Code__Spanish_Mexican:
      case T_e8_Language_Code__French_Canadian:
      {
         updateSystemLanguageInfo();
         MediaDatabinding::getInstance().updateVerticalSearchLanguageSwitch(false);
      }
      break;
      case T_e8_Language_Code__Russian:
      case T_e8_Language_Code__Arabic:
      case T_e8_Language_Code__Japanese:
      {
         //Update default language info
         updateSystemLanguageInfo();

         //Update selected language info
         updateOtherLanguageInfo(activeLanguage);
         _systemLanguage = true;
         MediaDatabinding::getInstance().updateVerticalSearchLanguageSwitch(true);
         break;
      }
      default:
      {
         updateSystemLanguageInfo();
         MediaDatabinding::getInstance().updateVerticalSearchLanguageSwitch(false);
         break;
      }
   }
}


/**
 *  updateLanguageInfo
 *  Set other language info
 *
 *  @param [in] void
 *  @return void
 */
void VerticalKeyboardSearch::updateSystemLanguageInfo()
{
   VerticalQuickSearch overticalQSTest(VerticalQuickSearch::enTableLatin);
   overticalQSTest.bUpdateQsLanguageAndCurrentGrpTable(VerticalQuickSearch::QUICK_SEARCH_LATIN, VerticalQuickSearch::enTableLatin);
   std::vector <std::string> oindexBarList = overticalQSTest.getChrListIndexBar(VerticalQuickSearch::LATIN_GROUP);
   updateMainListInfo(oindexBarList);
   std::vector <std::string> oindexBarList1 = overticalQSTest.getChrListExtendedIndexBar(VerticalQuickSearch::LATIN_GROUP);
   updateExtListinfo(oindexBarList1);
   printMainListInfo(_mDefaultMainList);
}


/**
 *  updateOtherLanguageInfo
 *  Set Russian language info
 *
 *  @param [in] void
 *  @return void
 */
void VerticalKeyboardSearch::updateOtherLanguageInfo(uint8 pLanguage)
{
   std::vector <std::string> lmainList;
   std::vector <std::string> lExtList;

   lmainList.clear();
   lExtList.clear();

   switch (pLanguage)
   {
      case T_e8_Language_Code__Russian:
      {
         ETG_TRACE_USR4(("object created updateRussianLanguageInfo"));
         VerticalQuickSearch overticalQSTest(VerticalQuickSearch::enTableRussain);
         overticalQSTest.bUpdateQsLanguageAndCurrentGrpTable(VerticalQuickSearch::QUICK_SEARCH_RUSSIN, VerticalQuickSearch::enTableRussain);
         lmainList = overticalQSTest.getChrListIndexBar(VerticalQuickSearch::LANGUAGE_GROUP);
         lExtList = overticalQSTest.getChrListExtendedIndexBar(VerticalQuickSearch::LANGUAGE_GROUP);
      }
      break;
      case T_e8_Language_Code__Arabic:
      {
         ETG_TRACE_USR4(("object created Arabic"));
         VerticalQuickSearch overticalQSTest(VerticalQuickSearch::enTableArabic);
         overticalQSTest.bUpdateQsLanguageAndCurrentGrpTable(VerticalQuickSearch::QUICK_SEARCH_ARABIC, VerticalQuickSearch::enTableArabic);
         lmainList = overticalQSTest.getChrListIndexBar(VerticalQuickSearch::LANGUAGE_GROUP);
         lExtList = overticalQSTest.getChrListExtendedIndexBar(VerticalQuickSearch::LANGUAGE_GROUP);
      }
      break;
      case T_e8_Language_Code__Japanese:
      {
         ETG_TRACE_USR4(("object created Japanese"));
         VerticalQuickSearch overticalQSTest(VerticalQuickSearch::enTableJapanese);
         overticalQSTest.bUpdateQsLanguageAndCurrentGrpTable(VerticalQuickSearch::QUICK_SEARCH_JAPANESE, VerticalQuickSearch::enTableJapanese);
         lmainList = overticalQSTest.getChrListIndexBar(VerticalQuickSearch::LANGUAGE_GROUP);
         lExtList = overticalQSTest.getChrListExtendedIndexBar(VerticalQuickSearch::LANGUAGE_GROUP);
      }
      break;
      default:
      {
         break;
      }
   }
   updateOtherLangMainListInfo(lmainList);
   updateOtherlangExtListinfo(lExtList);
}


/**
 *  ShowVerticalKeyInfo
 *  Show list info as per language switch
 *  @param [in] pDefault
 *  @return void
 */

void VerticalKeyboardSearch::ShowVerticalKeyInfo(bool pDefault)
{
   ETG_TRACE_USR4(("ShowVerticalKeyInfo %d", pDefault));

   _switchSelectedlanguage = pDefault;
   if (pDefault == true)
   {
      fillMainVerticalList();
   }
   else
   {
      fillLanguageMainVerticalList();
   }
}


/**
 *  updateListCount
 *  Fill count value with updated list info
 *  @param [in] pStr
 *  @return void
 */
void VerticalKeyboardSearch::updateListCount(std::map<std::string, trSearchKeyboard_ListItem> &pStr)
{
   std::map<std::string, trSearchKeyboard_ListItem>::iterator itr = pStr.begin();

   for (uint32 lindex = 0 ; lindex < pStr.size(); lindex++)
   {
      itr->second.countIndex = lindex;
      itr++;
   }
}


}
}


#endif
