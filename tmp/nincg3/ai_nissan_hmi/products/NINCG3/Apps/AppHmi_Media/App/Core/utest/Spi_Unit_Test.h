/**
*  @file   <Spi_Unit_Test.h>
*  @author <ECV> - <bvi1cob>
*  @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
*  @addtogroup <Spi>
*  @{
*/

#ifndef SPIPLAYER_UTILS_TEST_H_
#define SPIPLAYER_UTILS_TEST_H_

#include "gtest/gtest.h"
#include "Core/Spi/SpiUtils.h"

typedef std::tr1::tuple<int8, bool> ValidDeviceStatus;

ValidDeviceStatus deviceStatus[] =
{
   std::tr1::make_tuple(0, true),
   std::tr1::make_tuple(1, true),
   std::tr1::make_tuple(2, true),
   std::tr1::make_tuple(3, true)
};

class ValidDeviceStatus_Test : public testing::TestWithParam <ValidDeviceStatus>
{
   protected:
      int8 _validDevice;
      bool _validDeviceResult;
      ::App::Core::SpiUtils* _spiUtil;
   protected:
      virtual void SetUp()
      {
         _spiUtil = new ::App::Core::SpiUtils;
      }
      virtual void TearDown()
      {
         delete _spiUtil;
      }
};

#define _validDevice std::tr1::get<0>
#define _validDeviceResult std::tr1::get<1>



typedef std::tr1::tuple<int8, bool> InValidDeviceStatus;

InValidDeviceStatus inValidDevice[] =
{
   std::tr1::make_tuple(6, false),
   std::tr1::make_tuple('a', false)
};

class InValidDeviceStatus_Test : public testing::TestWithParam <InValidDeviceStatus>
{
   protected:
      int8 _inValidDevice;
      bool _inValidDeviceResult;
      ::App::Core::SpiUtils* _spiUtil;
   protected:
      virtual void SetUp()
      {
         _spiUtil = new ::App::Core::SpiUtils;
      }
      virtual void TearDown()
      {
         delete _spiUtil;
      }
};

#define _inValidDevice std::tr1::get<0>
#define _inValidDeviceResult std::tr1::get<1>


typedef std::tr1::tuple<int8, bool> ValidConnectionType;

ValidConnectionType ValidConnection[] =
{
   std::tr1::make_tuple(0, true),
   std::tr1::make_tuple(1, true),
   std::tr1::make_tuple(2, true)
};

class ValidDeviceConnectionType_Test : public testing::TestWithParam <ValidConnectionType>
{
   protected:
      int8 _ValidConn;
      bool _ValidConnectionResult;
      ::App::Core::SpiUtils* _spiUtil;
   protected:
      virtual void SetUp()
      {
         _spiUtil = new ::App::Core::SpiUtils;
      }
      virtual void TearDown()
      {
         delete _spiUtil;
      }
};

#define _ValidConn std::tr1::get<0>
#define _ValidConnectionResult std::tr1::get<1>


typedef std::tr1::tuple<int8, bool> InValidConnectionType;

InValidConnectionType InValidConnection[] =
{
   std::tr1::make_tuple(6, false),
   std::tr1::make_tuple('a', false)
};

class InValidDeviceConnectionType_Test : public testing::TestWithParam <InValidConnectionType>
{
   protected:
      int8 _InValidConn;
      bool _InValidConnectionResult;
      ::App::Core::SpiUtils* _spiUtil;
   protected:
      virtual void SetUp()
      {
         _spiUtil = new ::App::Core::SpiUtils;
      }
      virtual void TearDown()
      {
         delete _spiUtil;
      }
};

#define _InValidConn std::tr1::get<0>
#define _InValidConnectionResult std::tr1::get<1>

typedef std::tr1::tuple<int8, int8, bool> ValidUSBConnection;

ValidUSBConnection USBConnStatus[] =
{
   std::tr1::make_tuple(1, 1, true)
};

class ValidUSBConnection_Test : public testing::TestWithParam <ValidUSBConnection>
{
   protected:
      int8 _validDeviceStatus;
      int8 _validConnectionType;
      bool _validUSBConnResult;
      ::App::Core::SpiUtils* _spiUtil;
   protected:
      virtual void SetUp()
      {
         _spiUtil = new ::App::Core::SpiUtils;
      }
      virtual void TearDown()
      {
         delete _spiUtil;
      }
};

#define _validDeviceStatus std::tr1::get<0>
#define _validConnectionType std::tr1::get<1>
#define _validUSBConnResult std::tr1::get<2>

typedef std::tr1::tuple<int8, int8, bool> inValidUSBConnection;

inValidUSBConnection inValidUSBConnStatus[] =
{
   std::tr1::make_tuple(0, 0, false),
   std::tr1::make_tuple(0, 1, false),
   std::tr1::make_tuple(0, 2, false),
   std::tr1::make_tuple(1, 0, false),
   std::tr1::make_tuple(1, 2, false),
   std::tr1::make_tuple(2, 0, false),
   std::tr1::make_tuple(2, 1, false),
   std::tr1::make_tuple(2, 2, false),
   std::tr1::make_tuple(3, 0, false),
   std::tr1::make_tuple(3, 1, false),
   std::tr1::make_tuple(3, 2, false)
};

class InValidUSBConnection_Test : public testing::TestWithParam <inValidUSBConnection>
{
   protected:
      int8 _inValidDeviceStatus;
      int8 _inValidConnectionType;
      bool _inValidUSBConnResult;
      ::App::Core::SpiUtils* _spiUtil;
   protected:
      virtual void SetUp()
      {
         _spiUtil = new ::App::Core::SpiUtils;
      }
      virtual void TearDown()
      {
         delete _spiUtil;
      }
};

#define _inValidDeviceStatus std::tr1::get<0>
#define _inValidConnectionType std::tr1::get<1>
#define _inValidUSBConnResult std::tr1::get<2>



typedef std::tr1::tuple<int8, bool> ValidDipoConnection;

ValidDipoConnection DIPOConnStatus[] =
{
   std::tr1::make_tuple(1, true)
};

class ValidDIPOConnection_Test : public testing::TestWithParam <ValidDipoConnection>
{
   protected:
      int8 _validConnectionStatus;
      bool _validDIPOConnResult;
      ::App::Core::SpiUtils* _spiUtil;
   protected:
      virtual void SetUp()
      {
         _spiUtil = new ::App::Core::SpiUtils;
      }
      virtual void TearDown()
      {
         delete _spiUtil;
      }
};

#define _validConnectionStatus std::tr1::get<0>
#define _validDIPOConnResult std::tr1::get<1>

typedef std::tr1::tuple<int8, bool> InValidDipoConnection;

InValidDipoConnection InValidDIPOConnStatus[] =
{
   std::tr1::make_tuple(0, false),
   std::tr1::make_tuple(2, false),
};

class InValidDIPOConnection_Test : public testing::TestWithParam <InValidDipoConnection>
{
   protected:
      int8 _inValidConnectionStatus;
      bool _inValidDIPOConnResult;
      ::App::Core::SpiUtils* _spiUtil;
   protected:
      virtual void SetUp()
      {
         _spiUtil = new ::App::Core::SpiUtils;
      }
      virtual void TearDown()
      {
         delete _spiUtil;
      }
};

#define _inValidConnectionStatus std::tr1::get<0>
#define _inValidDIPOConnResult std::tr1::get<1>


#endif //SPIPLAYER_UTILS_TEST_H_

