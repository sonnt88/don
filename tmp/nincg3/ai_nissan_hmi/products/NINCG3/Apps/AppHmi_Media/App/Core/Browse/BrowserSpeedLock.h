/**
*  @file   BrowserSpeedLock.h
*  @author ECV - vma6cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef BROWSER_SPEED_LOCK_H_
#define BROWSER_SPEED_LOCK_H_

#include "mplay_MediaPlayer_FIProxy.h"
#include "CourierTunnelService/CourierMessageReceiverStub.h"
#include "AppHmi_MediaStateMachine.h"
#include "Common/VehicleDataHandler/VehicleDataHandler.h"
namespace App {
namespace Core {
class BrowserSpeedLock :
   public ::mplay_MediaPlayer_FI::PlayItemFromListCallbackIF
   , public ::mplay_MediaPlayer_FI::PlayItemFromListByTagCallbackIF
   , public ::mplay_MediaPlayer_FI::CreateMediaPlayerIndexedListCallbackIF
   , public ::mplay_MediaPlayer_FI::RequestMediaPlayerIndexedListSliceCallbackIF
   , public ::VehicleDataCommonHandler::ISpeedLockState
{
   public:
      BrowserSpeedLock(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy);

      ~BrowserSpeedLock();

      void setCurrentListHandle(uint32 listHandle);
      void setFocussedIndex(int32 focussedIndex);
      void setCurrentListType(uint32 listType);
      void setSelectedListItemTag(uint32 itemTag);
      void setNextListId(uint32);
      void startUserInactiveTimeout();
      void setFilterTag(uint32, uint32, uint32);
      void setFirstItemTag(uint32);
      void setDeviceTag(uint32);
      bool requestPlayAllSongsForMetadataBrowse();//Only For PlayAllSongs Butoon in MetadataBrowse in Scoe2.1
      void createMediaPlayerIndexedListStart(uint32, uint32, uint32, uint32);
      virtual void currentSpeedLockState(bool);

      virtual void onPlayItemFromListError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
                                           const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::PlayItemFromListError >& /*error*/);
      virtual void onPlayItemFromListResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
                                            const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::PlayItemFromListResult >& /*result*/);
      virtual void onPlayItemFromListByTagError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::PlayItemFromListByTagError >& /*error*/);
      virtual void onPlayItemFromListByTagResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::PlayItemFromListByTagResult >& /*result*/);
      virtual void onCreateMediaPlayerIndexedListError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerIndexedListError >& /*error*/) { }
      virtual void onCreateMediaPlayerIndexedListResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerIndexedListResult >& /*result*/);
      virtual void onRequestMediaPlayerIndexedListSliceError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestMediaPlayerIndexedListSliceError >& /*error*/) { }
      virtual void onRequestMediaPlayerIndexedListSliceResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestMediaPlayerIndexedListSliceResult >& result);

      bool onCourierMessage(const UserInactivityTimeoutInBrowseMsg&);
      bool onCourierMessage(const UserInactivityTimerStopMsg&);

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      ON_COURIER_MESSAGE(UserInactivityTimeoutInBrowseMsg)
      ON_COURIER_MESSAGE(UserInactivityTimerStopMsg)
      COURIER_MSG_MAP_END()

   private:
      void startTimer();
      void stopTimer();
      void setUserInactiveTimeout();
      bool isListTypeSong(uint32);
      void readRegionFromKDS();
      bool isJapanRegion() const;
      void setVehicleSpeed(uint16);
      void setVehicleThresholdSpeed(uint16);

      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& _mediaPlayerProxy;
      static BrowserSpeedLock* _browserSpeedLock;
      Util::Timer _userOperationTimer;
      uint32 _deviceTag;
      uint32 _listHandle;
      uint32 _nextListHandle;
      int32 _focussedIndex;
      uint32 _currentListType;
      uint32 _nextListType;
      uint32 _listItemTag;
      uint32 _firstListItemTag;
      uint32 _filterTag1;
      uint32 _filterTag2;
      uint32 _filterTag3;
      bool _speedLockState;
};


}
}


#endif
