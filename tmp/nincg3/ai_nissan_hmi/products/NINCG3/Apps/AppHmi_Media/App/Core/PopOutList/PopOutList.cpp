/******************************************************************
*FILE: PopOutList.cpp
*SW-COMPONENT: APPHMI_MEDIA
*DESCRIPTION: Serves as an example for copyright comments
*COPYRIGHT: © 2016 RBEI
*
*The reproduction, distribution and utilization of this file as
*well as the communication of its contents to others without express
*authorization is prohibited. Offenders will be held liable for the
*payment of damages. All rights reserved in the event of the grant
*of a patent, utility model or design.
******************************************************************/


#include "hall_std_if.h"
#include "PopOutList.h"
#include "Core/Utils/MediaProxyUtility.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL_MEDIA_POPOUTLIST
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::PopOutList::
#include "trcGenProj/Header/PopOutList.cpp.trc.h"
#endif
using namespace MPlay_fi_types;


namespace App {
namespace Core {

static const char* const MEDIA_POPOUT_LIST_ITEM_EVEN  = "PopOutList_Even";
static const char* const MEDIA_POPOUT_LIST_ITEM_ODD  = "PopOutList_Odd";


/**
 * @Constructor
 */
PopOutList::PopOutList(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& mediaPlayerProxy)
   : _mediaPlayerProxy(mediaPlayerProxy)
   , _optionbtnvisibleInfo(0)
{
   ETG_I_REGISTER_FILE();
   ETG_TRACE_USR4(("PopOutList Constructor"));
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_POPOUT_LIST, this);
   _maplistData.clear();
   updatePopOutListDetailsForSceneID();
}


/**
 * @Destructor
 */
PopOutList::~PopOutList()
{
   ETG_TRACE_USR4(("PopOutList Destructor"));

   _mediaPlayerProxy.reset();

   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_POPOUT_LIST);
}


void PopOutList:: updatePopOutListDetailsForSceneID()
{
   ETG_TRACE_USR4(("updatePopOutListDetailsForSceneID Received"));

   std::vector<std::string> _popOutlistDataForUSBIPOD;
   _popOutlistDataForUSBIPOD.push_back("Ambiance");
   _popOutlistDataForUSBIPOD.push_back("Sound Settings");
   _popOutlistDataForUSBIPOD.push_back("User manual");

   _maplistData[Media_Scenes_MEDIA__AUX__USB_MAIN] = _popOutlistDataForUSBIPOD;
   _maplistData[Media_Scenes_MEDIA__AUX__IPOD_MAIN] = _popOutlistDataForUSBIPOD;
   _maplistData[Media_Scenes_MEDIA_AUX__LINEIN_MAIN] = _popOutlistDataForUSBIPOD;

   std::vector<std::string> _popOutlistDataForBT;
   _popOutlistDataForBT.push_back("Bluetooth audio source");
   _popOutlistDataForBT.push_back("Ambiance");
   _popOutlistDataForBT.push_back("Sound Settings");
   _popOutlistDataForBT.push_back("User manual");

   _maplistData[Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN] = _popOutlistDataForBT;

   std::vector<std::string> _popOutlistDataForBrowse;
   _popOutlistDataForBrowse.push_back("Ambiance");
   _popOutlistDataForBrowse.push_back("Sound Settings");

   _maplistData[Media_Scenes_MEDIA_AUX__METADATA_BROWSE] = _popOutlistDataForBrowse;
   _maplistData[Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE] = _popOutlistDataForBrowse;
   _maplistData[Media_Scenes_MEDIA_AUX__BROWSER] = _popOutlistDataForBrowse;

   std::vector<std::string> _popOutlistDataForSoundSetting;
   _popOutlistDataForSoundSetting.push_back("Reset");

   std::vector<std::string> _popOutlistDataForVideoPlayer;
   _popOutlistDataForVideoPlayer.push_back("Information");
   _popOutlistDataForVideoPlayer.push_back("Settings");

   _maplistData[Media_Scenes_MEDIA_VIDEO_PLAYER_MAIN] = _popOutlistDataForVideoPlayer;

   std::vector<std::string> _popOutlistDataForPhotoPlayer;
   _popOutlistDataForPhotoPlayer.push_back("Information");
   _popOutlistDataForPhotoPlayer.push_back("Set as Profile");
   _popOutlistDataForPhotoPlayer.push_back("Settings");

   std::vector<std::string> _popOutlistDataForScreenRecoderBrowse1;
   _popOutlistDataForScreenRecoderBrowse1.push_back("Settings");
   _popOutlistDataForScreenRecoderBrowse1.push_back("Delete All");

   std::vector<std::string> _popOutlistDataForScreenRecoderBrowse2;
   _popOutlistDataForScreenRecoderBrowse2.push_back("Folder Info");
   _popOutlistDataForScreenRecoderBrowse2.push_back("Manage Protection File");
   _popOutlistDataForScreenRecoderBrowse2.push_back("Delete");
   _popOutlistDataForScreenRecoderBrowse2.push_back("Delete All");

   std::vector<std::string> _popOutlistDataForScreenRecoderProtectionFile;
   _popOutlistDataForScreenRecoderProtectionFile.push_back("Delete");

   std::vector<std::string> _popOutlistDataForScreenRecoderDelete;
   _popOutlistDataForScreenRecoderDelete.push_back("Manage Protection File");

   std::vector<std::string> _popOutlistDataForScreenRecoderPlayer;
   _popOutlistDataForScreenRecoderPlayer.push_back("File Info");
   _popOutlistDataForScreenRecoderPlayer.push_back("Protect/UnProtect");
   _popOutlistDataForScreenRecoderPlayer.push_back("Delete");

   std::vector<std::string> _popOutlistDataForDMBPlayer;
   _popOutlistDataForDMBPlayer.push_back("Metropolitan Channel Search");
   _popOutlistDataForDMBPlayer.push_back("Country Channel Search");

   std::vector<std::string> _popOutlistDataForDMBPlayerDelete;
   _popOutlistDataForDMBPlayerDelete.push_back("Delete");
}


tSharedPtrDataProvider PopOutList::getListDataProvider(const ListDataInfo& listInfo)
{
   ETG_TRACE_USR4(("PopOutList:getListDataProvider is called %d ", listInfo.listId));

   if (listInfo.listId == LIST_ID_POPOUT_LIST)
   {
      ListDataProviderBuilder listBuilder(LIST_ID_POPOUT_LIST, MEDIA_POPOUT_LIST_ITEM_ODD);

      uint32 itemIdx = 0;
      std::vector<std::string>::iterator itr = _currentPopoutList.begin();
      for (; itr != _currentPopoutList.end(); itr++)
      {
         ETG_TRACE_USR4(("PopOutList:getListDataProvider popout: %s", (*itr).c_str()));
         listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForPopOutList(itemIdx)).AddData((*itr).c_str()).AddData(true);
      }

      return listBuilder.CreateDataProvider();
   }
   return tSharedPtrDataProvider();
}


const char* PopOutList::getlistItemTemplateForPopOutList(uint32& index)
{
   const char* listItemTemplate;

   if (index % 2)
   {
      listItemTemplate = MEDIA_POPOUT_LIST_ITEM_EVEN;
   }
   else
   {
      listItemTemplate = MEDIA_POPOUT_LIST_ITEM_ODD;
   }
   ++index;
   return listItemTemplate;
}


bool PopOutList::onCourierMessage(const ScreenNameUpdMsg& oMsg)
{
   _sceneID = oMsg.GetSceneName();
   _currentPopoutList.clear();
   _currentPopoutList = _maplistData[_sceneID];
   return true;
}


bool PopOutList::onCourierMessage(const OptionButtonPressedMsg& /*oMsg*/)
{
   ETG_TRACE_USR4(("PopOutList:OptionButtonPressedMsg is received"));
   _optionbtnvisibleInfo = 1;

   MediaDatabinding::getInstance().updateOptionButtonVisiblity(_optionbtnvisibleInfo);
   return true;
}


bool PopOutList::onCourierMessage(const OptionButtonClosePressedMsg& /*oMsg*/)
{
   ETG_TRACE_USR4(("PopOutList:OptionButtonClosePressedMsg is received"));
   _optionbtnvisibleInfo = 0;

   MediaDatabinding::getInstance().updateOptionButtonVisiblity(_optionbtnvisibleInfo);
   return true;
}


}//end of namespace Core
}//end of namespace App
