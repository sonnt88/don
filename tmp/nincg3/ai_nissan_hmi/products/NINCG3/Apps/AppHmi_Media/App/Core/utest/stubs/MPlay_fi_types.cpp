/**
* @file MetaData.h
* @author RBEI/ECV-AIVI_MediaTeam
* @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
* @addtogroup AppHMI_Media
*/

#include "Core/utest/stubs/MPlay_fi_types.h"

namespace MPlay_fi_types {

T_MPlayMediaObject::T_MPlayMediaObject()
{
   for (int field = 0; field < 4; ++field)
   {
      _metaData[field] = "";
   }
   _category = T_e8_MPlayCategoryType__e8CTY_NONE;
}


T_MPlayMediaObject::~T_MPlayMediaObject()
{
}


std::string T_MPlayMediaObject::getSMetaDataField1() const
{
   return _metaData[0];
}


std::string T_MPlayMediaObject::getSMetaDataField2() const
{
   return _metaData[1];
}


std::string T_MPlayMediaObject::getSMetaDataField3() const
{
   return _metaData[2];
}


std::string T_MPlayMediaObject::getSMetaDataField4() const
{
   return _metaData[3];
}


T_e8_MPlayCategoryType T_MPlayMediaObject::getE8CategoryType() const
{
   return _category;
}


T_e8_MPlayDeviceType T_MPlayMediaObject::getE8DeviceType() const
{
   return _devicetype;
}


unsigned int T_MPlayMediaObject::getU32Tag() const
{
   return _tag;
}


void T_MPlayMediaObject::setSMetaDataField1(const std::string& data)
{
   _metaData[0] = data;
}


void T_MPlayMediaObject::setSMetaDataField2(const std::string& data)
{
   _metaData[1] = data;
}


void T_MPlayMediaObject::setSMetaDataField3(const std::string& data)
{
   _metaData[2] = data;
}


void T_MPlayMediaObject::setSMetaDataField4(const std::string& data)
{
   _metaData[3] = data;
}


void T_MPlayMediaObject::setE8CategoryType(const T_e8_MPlayCategoryType& category)
{
   _category = category;
}


} /* namespace MPlay_fi_types */
