/**
* @file midw_smartphoneint_fi_types.h
* @author RBEI/ECV-AIVI_MediaTeam
* @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
* @addtogroup AppHMI_Media
*/

#ifndef MIDW_SMARTPHONEINT_FI_TYPES_
#define MIDW_SMARTPHONEINT_FI_TYPES_

#include <string>
namespace midw_smartphoneint_fi_types
{

/**
 * This enumeration identifies result of an operation
 */
enum T_e8_DeviceStatusInfo
{

   /**
    * If the meaning of "NOT_KNOWN" isn't clear, then there should be a
    * description here.
    */
   T_e8_DeviceStatusInfo__NOT_KNOWN = 0u,
   /**
    * If the meaning of "DEVICE_ADDED" isn't clear, then there should be a
    * description here.
    */
   T_e8_DeviceStatusInfo__DEVICE_ADDED = 1u,
   /**
    * If the meaning of "DEVICE_REMOVED" isn't clear, then there should be a
    * description here.
    */
   T_e8_DeviceStatusInfo__DEVICE_REMOVED = 2u,
   /**
    * If the meaning of "DEVICE_CHANGED" isn't clear, then there should be a
    * description here.
    */
   T_e8_DeviceStatusInfo__DEVICE_CHANGED = 3u

};

/**
 * This enumeration identifies the device connection types
 */
enum T_e8_DeviceConnectionType
{

   /**
    * If the meaning of "UNKNOWN_CONNECTION" isn't clear, then there should
    * be a description here.
    */
   T_e8_DeviceConnectionType__UNKNOWN_CONNECTION = 0u,
   /**
    * If the meaning of "USB_CONNECTED" isn't clear, then there should be a
    * description here.
    */
   T_e8_DeviceConnectionType__USB_CONNECTED = 1u,
   /**
    * If the meaning of "WIFI_CONNECTED" isn't clear, then there should be a
    * description here.
    */
   T_e8_DeviceConnectionType__WIFI_CONNECTED = 2u

};

enum T_e8_DeviceCategory
{

   /**
    * If the meaning of "DEV_TYPE_UNKNOWN" isn't clear, then there should be a description here.
    */
   T_e8_DeviceCategory__DEV_TYPE_UNKNOWN = 0u,
   /**
    * If the meaning of "DEV_TYPE_DIPO" isn't clear, then there should be a description here.
    */
   T_e8_DeviceCategory__DEV_TYPE_DIPO = 1u,
   /**
    * If the meaning of "DEV_TYPE_MIRRORLINK" isn't clear, then there should be a description here.
    */
   T_e8_DeviceCategory__DEV_TYPE_MIRRORLINK = 2u,
   /**
    * If the meaning of "DEV_TYPE_ANDROIDAUTO" isn't clear, then there should be a description here.
    */
   T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO = 3u

};

enum T_e8_DeviceConnectionStatus
{

   /**
    * If the meaning of "DEV_NOT_CONNECTED" isn't clear, then there should be a description here.
    */
   T_e8_DeviceConnectionStatus__DEV_NOT_CONNECTED = 0u,
   /**
    * If the meaning of "DEV_CONNECTED" isn't clear, then there should be a description here.
    */
   T_e8_DeviceConnectionStatus__DEV_CONNECTED = 1u

};

}

#endif //MIDW_SMARTPHONEINT_FI_TYPES_
