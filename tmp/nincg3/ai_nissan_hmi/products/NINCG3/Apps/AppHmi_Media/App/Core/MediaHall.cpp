/**
 *  @file   MediaHall.cpp
 *  @author ECV - kng3kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */
#include "hall_std_if.h"
#include "Core/MediaHall.h"
#include "Core/Utils/MediaProxyUtility.h"
#include "proxy/clContextProxy.h"
#include "Common/UserInterfaceCtrl/UserInterfaceControl.h"
#include "Common/UserInterfaceCtrl/UserInterfaceScreenSaver.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::MediaHall::
#include "trcGenProj/Header/MediaHall.cpp.trc.h"
#endif

using namespace ::App::Core;
using namespace ::CourierTunnelService::CourierMessageReceiver;
using namespace org::genivi::audiomanager::CommandInterface;


namespace App {
namespace Core {


DEFINE_CLASS_LOGGER_AND_LEVEL("App/Core", MediaHall, Info);
using namespace ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange;
using namespace MASTERAUDIOSERVICE_INTERFACE::SoundProperties;
/**
 * @Destructor
 */
MediaHall::~MediaHall()
{
   _MediaPlayerProxy.reset();
   _tunermasterFiProxy.reset();
   _commandIFProxy.reset();
   _vehicleinfoProxy.reset();
   _spiMidwServiceProxy.reset();
   _spiDimmingFiProxy.reset();
   _audioSourceChangeSPIProxy.reset();
   if (_mUserInterfaceCntrl != NULL)
   {
      _mUserInterfaceCntrl->deregisterLockApplicationUpdate(this);
      delete _mUserInterfaceCntrl;
      _mUserInterfaceCntrl = NULL;
   }
   _mUserInterfaceScreenSaver = NULL;
   if (NULL != _deviceStatus)
   {
      delete _deviceStatus;
   }
   _deviceStatus = NULL;
   if (NULL != _hmiDataHandler)
   {
      delete _hmiDataHandler;
   }
   _hmiDataHandler = NULL;
   delete _spiRestoreFactorySetting;
   delete _spiVehicleDataHandler;
   delete _spiAccessoryAppStateHandler;
   delete _mediaBrowseHandling;
#ifdef PHOTOPLAYER_SUPPORT
   delete _photoPlayerHandler;
#endif
   delete _mediaSourceHandling;
   delete _mediaPlayScreenHandling;
   delete _iDeviceConnection;
   delete _spiConnectionHandler;
   delete _traceCommand;
   delete _spiSettingListHandler;
   delete _iBTSettings;
   delete _dataCollector;
   delete _spiKeyHandler;
   delete _spiMetaData;
   delete _DeviceHomeScreen;
   delete _poContextProvider;
   delete _poContextRequestor;
   delete _spiConnectionNotifyer;
   delete _mediaRestoreFactorySetting;
#ifdef MEDIAPLAYER_SETTINGS_CHANGE_SUPPORT
   if (NULL != _mediaInfoSettingsHandling)
   {
      delete _mediaInfoSettingsHandling;
      _mediaInfoSettingsHandling = NULL;
   }
#endif
   _mediaDatabindingHandler = NULL;
   _mediaBrowseHandling = NULL;
#ifdef PHOTOPLAYER_SUPPORT
   _photoPlayerHandler = NULL;
#endif
   _mediaSourceHandling = NULL;
   _mediaPlayScreenHandling = NULL;
   _iDeviceConnection = NULL;
   _spiConnectionHandler = NULL;
   _commonLanguage = NULL;
   _systemLanguage = NULL;

   //this line is added for HmiInfo Service
   //hmiInfoproxy deletion should be done from MASTER only
   _pHmiInfoProxy = NULL;

#ifdef POPOUTLIST_SUPPORT
   if (NULL != _popOutList)
   {
      delete _popOutList;
      _popOutList = NULL;
   }
#endif
}


/**
 * @Constructor
 */
MediaHall::MediaHall()
   : HallComponentBase("", "App.Core.AppHmi_Media", "/org/genivi/NodeStateManager/LifeCycleConsumer/AppHmi_Media"),
     _audioSourceChangeSPIProxy(AudioSourceChangeProxy::createProxy("audioSourceChangePort", *this)),
     _MediaPlayerProxy(::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy::createProxy("mediaPlayerFiPort", *this)),
     _tunermasterFiProxy(::MIDW_TUNERMASTER_FI::MIDW_TUNERMASTER_FIProxy::createProxy("tunermasterFiPort", *this)),
     _spiMidwServiceProxy(::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy::createProxy("midwspiPort", *this)),
     _commandIFProxy(::CommandInterfaceProxy::createProxy("commandInterfacePort", *this)),
     _vehicleinfoProxy(::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy::createProxy("vehicleMainFiPort", *this)),
     _spiDimmingFiProxy(::dimming_main_fi::Dimming_main_fiProxy ::createProxy("vdDimFiPort", *this)),
     _soundPropertiesProxy(SoundPropertiesProxy::createProxy("soundPropertiesPort", *this))

{
   ETG_I_REGISTER_FILE();
   ETG_TRACE_USR4(("Media Hall Creation \n"));
   StartupSync::getInstance().registerPropertyRegistrationIF(this);

   DefSetServiceBase::s_Intialize("AppHmiMediaServicePort");//port name needs to be same as port name given in cmc file
   MediaProxyUtility::createInstance(_MediaPlayerProxy);

   _poContextProvider   = new AppMedia_ContextProvider();
   _poContextRequestor  = new AppMedia_ContextRequestor();
   clContextProxy::instance()->vSetApplication(APPID_APPHMI_MEDIA);
   clContextProxy::instance()->vSetProviderImpl(_poContextProvider);
   clContextProxy::instance()->vSetRequestorImpl(_poContextRequestor);

   _mediaDatabindingHandler = &(MediaDatabinding::getInstance());
   _DeviceHomeScreen = new ContextSwitchHomeScreen(_poContextProvider);
   _iBTSettings = new BTSetting();
   _iDeviceConnection = new MediaDeviceConnectionHandling(_MediaPlayerProxy, _DeviceHomeScreen, _iBTSettings);
   _mediaPlayScreenHandling = new MediaPlayScreenHandling(_MediaPlayerProxy, _tunermasterFiProxy);
   _mediaBrowseHandling = new MediaBrowseHandling(_MediaPlayerProxy, _mediaPlayScreenHandling);
#ifdef PHOTOPLAYER_SUPPORT
   _photoPlayerHandler = new PhotoPlayerHandler(_MediaPlayerProxy, _mediaPlayScreenHandling);
#endif
   _spiConnectionNotifyer = new SpiConnectionSubject();
   _spiConnectionHandler = new SpiConnectionHandling(_spiMidwServiceProxy, _poContextProvider, _audioSourceChangeSPIProxy, _soundPropertiesProxy , _spiConnectionNotifyer);
   _spiSettingListHandler = new SpiSettingListHandler(_spiConnectionHandler, _spiMidwServiceProxy , _spiConnectionNotifyer);
   _traceCommand = new TraceCommand();
   _dataCollector = new DataCollector(_iDeviceConnection, _spiConnectionHandler);
   _spiKeyHandler = new SpiKeyHandler(_spiConnectionHandler, _spiMidwServiceProxy);
   _spiMetaData = new SpiMetaData(_spiMidwServiceProxy);
   _spiAccessoryAppStateHandler = new AccessoryAppStateHandler(_spiConnectionHandler, _spiKeyHandler, _spiMidwServiceProxy);
   _commonLanguage =  ::VehicleDataCommonHandler::VehicleDataHandler::GetInstance();
   _systemLanguage = new MediaLanguageHandling(_MediaPlayerProxy, _commonLanguage);
   _spiVehicleDataHandler = new VehicleDataHandler(_vehicleinfoProxy, _spiDimmingFiProxy, _spiConnectionHandler, _commonLanguage, _spiMidwServiceProxy);
   _spiRestoreFactorySetting = new SpiRestoreFactorySetting(_spiSettingListHandler);
   _hmiDataHandler = new HmiDataHandlerExt();
   _mediaSourceHandling = new MediaSourceHandling(_iBTSettings, _iDeviceConnection, _mediaPlayScreenHandling, _commandIFProxy, _DeviceHomeScreen, _MediaPlayerProxy, _hmiDataHandler);
   _deviceStatus = new DeviceStatusList(_MediaPlayerProxy , _iDeviceConnection);
   _mediaRestoreFactorySetting = new MediaRestoreFactorySetting(_MediaPlayerProxy);
#ifdef MEDIAPLAYER_SETTINGS_CHANGE_SUPPORT
   _mediaInfoSettingsHandling = new MediaInfoSettingsHandling(_MediaPlayerProxy, _photoPlayerHandler);
#endif
#ifdef POPOUTLIST_SUPPORT
   _popOutList = new PopOutList(_MediaPlayerProxy);
#endif
   _mUserInterfaceScreenSaver = UserInterfaceScreenSaver::getInstance();
   _mUserInterfaceCntrl = new UserInterfaceControl(_mUserInterfaceScreenSaver);
   _mUserInterfaceScreenSaver->vSetScreenSaverRequestor(_mUserInterfaceCntrl);
   if (_mUserInterfaceCntrl != NULL)
   {
      _mUserInterfaceCntrl->setApplicationId(APPID_APPHMI_MEDIA);
      _mUserInterfaceCntrl->registerLockApplicationUpdate(this);
   }
   _pHmiInfoProxy = HmiInfoProxy::GetInstance();
}


//void MediaHall::onExpired(asf::core::Timer& /*timer*/, boost::shared_ptr<asf::core::TimerPayload> /*data*/)
//{
//   ETG_TRACE_USR4(("MEDIAAPP : Received Timer expired\n"));
//   if (timer.getAct()  == Manualtune_timer.getAct())
//   {
//      if (data->getReason() == asf::core::TimerPayload_Reason__Completed)
//      {
//      }
//   }
//   else
//   {
//   }
//}

/**
 * registerProperties - trigger for property registration, this function is called once mw service is running and media gui thread is UP(guistartupfinished is
 *                          received from media state machine)
 * @param[in] proxy
 * @parm[in] stateChange
 * @return void
 */
void MediaHall::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange)
{
   ETG_TRACE_USR4(("MediaHall: registerProperties"));
   if (_iDeviceConnection != NULL && _mediaPlayScreenHandling != NULL && _mediaBrowseHandling != NULL)
   {
      _iDeviceConnection->registerProperties(proxy, stateChange);
      _mediaPlayScreenHandling->registerProperties(proxy, stateChange);
      _mediaBrowseHandling->registerProperties(proxy, stateChange);
   }
#ifdef PHOTOPLAYER_SUPPORT
   if (_photoPlayerHandler != NULL)
   {
      _photoPlayerHandler->registerProperties(proxy, stateChange);
   }
#endif
   if (_spiConnectionHandler != NULL)
   {
      _spiConnectionHandler->registerProperties(proxy, stateChange);
   }
}


bool MediaHall::onCourierMessage(const UserInterfaceRegisterReq& msg)
{
   if (_mUserInterfaceCntrl != NULL)
   {
      _mUserInterfaceCntrl->hardKeyRegistrationReq(msg.GetKeyCode(), msg.GetPriority());
   }
   return true;
}


bool MediaHall::onCourierMessage(const UserInterfaceDeRegisterReq& msg)
{
   if (_mUserInterfaceCntrl != NULL)
   {
      _mUserInterfaceCntrl->hardKeyDeregistrationReq(msg.GetKeyCode());
   }
   return true;
}


void MediaHall::lockStateApplicationUpdate(int applicationId, bool lockState)
{
   ETG_TRACE_USR1(("MediaHall :lockStateApplicationUpdate lockState=%d,applicationId=%d", lockState, applicationId));
   POST_MSG((COURIER_MESSAGE_NEW(UserInterfaceLockStateAppRes)(applicationId, lockState)));

   ETG_TRACE_USR4(("GUI->HALL: SpiLockApplicationUpdMsg Posted"));

   POST_MSG((COURIER_MESSAGE_NEW(SpiLockApplicationUpdMsg)(applicationId, lockState)));
}


/**
 * deregisterProperties - trigger for property deregistration
 * @param[in] proxy
 * @parm[in] stateChange
 * @return void
 */
void MediaHall::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange)
{
   ETG_TRACE_USR4(("MediaHall: deregisterProperties"));
   if (_iDeviceConnection != NULL && _mediaPlayScreenHandling != NULL && _mediaBrowseHandling != NULL)
   {
      _iDeviceConnection->deregisterProperties(proxy, stateChange);
      _mediaPlayScreenHandling->deregisterProperties(proxy, stateChange);
      _mediaBrowseHandling->deregisterProperties(proxy, stateChange);
   }
#ifdef PHOTOPLAYER_SUPPORT
   if (_photoPlayerHandler != NULL)
   {
      _photoPlayerHandler->deregisterProperties(proxy, stateChange);
   }
#endif
   if (_spiConnectionHandler != NULL)
   {
      _spiConnectionHandler->deregisterProperties(proxy, stateChange);
   }
}


bool MediaHall::onCourierMessage(const ApplicationStateUpdMsg& msg)
{
   ETG_TRACE_USR4(("MediaHall::onCourierMessage ApplicationStateUpdMsg Called ..."));
   if ((_mUserInterfaceScreenSaver != NULL) && (_spiAccessoryAppStateHandler != NULL))
   {
      _mUserInterfaceScreenSaver->vSetApplicationState((msg.GetState() == hmibase::IN_FOREGROUND ? true : false));
      if ((msg.GetState() == hmibase::IN_FOREGROUND) && (_spiAccessoryAppStateHandler->bIsBlockClockscreenSaver() == false))
      {
         _mUserInterfaceScreenSaver->vAllowScreenSaver();
      }
      else
      {
         _mUserInterfaceScreenSaver->vBlockScreenSaver();
      }
   }
   if (msg.GetState() == hmibase::IN_FOREGROUND)
   {
      ETG_TRACE_USR4(("clMediaSourceHandling::Media is ActiveApplication"));
      MediaDatabinding::getInstance().updateActiveApplicationStatus(true);
   }
   else
   {
      ETG_TRACE_USR4(("clMediaSourceHandling::Media is not ActiveApplication"));
      MediaDatabinding::getInstance().updateActiveApplicationStatus(false);
   }
   return false;
}


} // namespace Core
} // namespace App
