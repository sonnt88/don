///////////////////////////////////////////////////////////
//  AppMedia_ContextProvider.cpp
//  Implementation of the Class AppMedia_ContextProvider
//  Created on:      13-Jul-2015 11:44:47 AM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#include "hall_std_if.h"
#include "AppMedia_ContextProvider.h"
#include "clContextProviderInterface.h"
#include "clContextRequestorInterface.h"
#include "ProjectBaseMsgs.h"
#include "ProjectBaseTypes.h"

#include "MediaDatabinding.h"

using namespace App::Core ;
AppMedia_ContextProvider::AppMedia_ContextProvider()
{
   vSendContextTable();
#ifdef SPI_CONTEXT_ONRVC_FIX_NCG3D_7788
   _mbSpiRvcTempContextRequest = false;
#endif
}


AppMedia_ContextProvider::~AppMedia_ContextProvider()
{
#ifdef SPI_CONTEXT_ONRVC_FIX_NCG3D_7788
   _mbSpiRvcTempContextRequest = false;
#endif
}


bool AppMedia_ContextProvider::onCourierMessage(const ContextSwitchInResMsg& msg)
{
   ContextState enState =  ContextState__INVALID;
   if (msg.GetResponseType() == CONTEXT_TRANSITION_BACK)
   {
      if (activeContextId != 0)
      {
         if ((activeContextId == getFrontContext()) || (getFrontContext() == 0))
         {
            vSwitchBack();
         }
      }
      return true;
   }
   switch (msg.GetResponseType())
   {
      case CONTEXT_TRANSITION_DONE:
         enState = ContextState__ACCEPTED;
         break;
      case CONTEXT_TRANSITION_COMPLETE:
         enState = ContextState__COMPLETED;
         POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchStateUpdateMsg)(1, CONTEXT_SWITCH_CLOSED)));
         break;
      case CONTEXT_TRANSITION_FAILED:
         enState = ContextState__REJECTED;
         break;
      default:
         break;
   }
   if (msg.GetSwitchId())
   {
      vOnNewContextRequestState(0, enState);
   }
   return true;
}


void AppMedia_ContextProvider::vOnNewActiveContext(uint32 contextId, ContextState enState)
{
   if (enState == ContextState__ACCEPTED)
   {
      activeContextId = contextId;
   }
   clContextProvider::vOnNewActiveContext(contextId, enState);
}


bool AppMedia_ContextProvider::bOnNewContext(uint32 sourceContextId, uint32 targetContextId)
{
   POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInReqMsg)(targetContextId, sourceContextId, 1)));
   return true;
}


void AppMedia_ContextProvider::vOnBackRequestResponse(ContextState /*enState*/)
{
}


void AppMedia_ContextProvider::vClearContexts()
{
   activeContextId = 0;
}


bool AppMedia_ContextProvider::onCourierMessage(const ActiveRenderedView& msg)
{
   vOnActiveRenderedView(msg.GetViewName().GetCString(), msg.GetSurfaceId());
   return false;
}


bool AppMedia_ContextProvider::onCourierMessage(const ContextSwitchInReqMsg& msg)
{
   bool isConsumed = false;
   switch (msg.GetContextId())
   {
      case MEDIA_CONTEXT_SYSTEM_INFO_MAIN :
      case MEDIA_CONTEXT_HOME_MENU_MAIN:
      {
         if (SRC_SPI_ENTERTAIN == MediaDatabinding::getInstance().GetCurrentAudioSource())
         {
            if (true == MediaDatabinding::getInstance().getSpiDIPOSourceAvailability())
            {
               POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(msg.GetSwitchId(), CONTEXT_TRANSITION_COMPLETE)));
               POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MUSIC, 0 , APPID_APPHMI_MEDIA)));
            }
            else if (true == MediaDatabinding::getInstance().getSpiAAPSourceAvailability())
            {
               POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(msg.GetSwitchId(), CONTEXT_TRANSITION_COMPLETE)));
               POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(MEDIA_SPI_CONTEXT_ANDROID_AUTO_MUSIC_VIDEO, 0 , APPID_APPHMI_MEDIA)));
            }
         }
         else
         {
            POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(msg.GetSwitchId(), CONTEXT_TRANSITION_DONE)));
            POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(msg.GetSwitchId(), CONTEXT_TRANSITION_COMPLETE)));
            POST_MSG((COURIER_MESSAGE_NEW(ExitBrowseOnHK_AUXPressMsg)()));
         }
         isConsumed = true;
      }
      break;
      case MEDIA_CONEXT_TESTMODE_DEVICE_STATUS:
      {
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
         POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(msg.GetSwitchId(), CONTEXT_TRANSITION_DONE)));
         POST_MSG((COURIER_MESSAGE_NEW(TestModeContextSwitchInReqMsg)(msg.GetSwitchId())));
         isConsumed = true;
#endif
      }
      break;
#ifdef SPI_CONTEXT_ONRVC_FIX_NCG3D_7788
      case MEDIA_SPI_CONTEXT_RVC_ON:
      {
         vSetSpiRvcTemporaryContext(true);
         POST_MSG((COURIER_MESSAGE_NEW(SetApplicationModeReqMsg)(APP_MODE_UTILITY)));
         POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(msg.GetSwitchId(), CONTEXT_TRANSITION_DONE)));
      }
      break;
      case MEDIA_SPI_CONTEXT_CARPLAY_VIDEO_ACTIVE:
      {
         if (true == bGetSetSpiRvcTemporaryContext())
         {
            POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(msg.GetSwitchId(), CONTEXT_TRANSITION_COMPLETE)));
            vSetSpiRvcTemporaryContext(false);
         }
      }
      break;
#endif
      default:
      {
         isConsumed = false;
         break;
      }
   }
   return isConsumed;
}


#ifdef SPI_CONTEXT_ONRVC_FIX_NCG3D_7788
void AppMedia_ContextProvider::vSetSpiRvcTemporaryContext(bool bSpiRvcTempContextStatus)
{
   _mbSpiRvcTempContextRequest = bSpiRvcTempContextStatus;
}


bool AppMedia_ContextProvider::bGetSetSpiRvcTemporaryContext()
{
   return _mbSpiRvcTempContextRequest;
}


#endif
