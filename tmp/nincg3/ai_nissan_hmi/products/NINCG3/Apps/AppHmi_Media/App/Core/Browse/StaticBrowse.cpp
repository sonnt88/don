/**
*  @file   StaticBrowse.cpp
*  @author ECV - vma6cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#include "hall_std_if.h"
#include "StaticBrowse.h"
#include "Core/LanguageDefines.h"
#include "Core/Utils/MediaProxyUtility.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL_BROWSE
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::StaticBrowse::
#include "trcGenProj/Header/StaticBrowse.cpp.trc.h"
#endif

namespace App {
namespace Core {

static const char* const DATA_CONTEXT_LIST_BUTTON_BROWSER_ITEM    = "Browser";
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R
static const char* const DATA_CONTEXT_LIST_BUTTON_BROWSER_ITEM_EVEN = "Browse_Even";
#endif

StaticBrowse::StaticBrowse()
{
   ETG_I_REGISTER_FILE();
}


/**
 *  StaticBrowse::getDataProvider - Gets the ListDataProvider
 *  @param [in] requestedlistInfo - structure containing the requested list information
 *  @param [in] activeDeviceType - current active device type
 *  @return tSharedPtrDataProvider
 */
tSharedPtrDataProvider StaticBrowse::getDataProvider(RequestedListInfo& requestedlistInfo, uint32 activeDeviceType)
{
   ETG_TRACE_USR4(("StaticBrowse::getDataProvider"));

   _activeDeviceType = activeDeviceType;
   initListStateMachine();
   return getBrowseListDataProvider(requestedlistInfo._deviceTag, activeDeviceType);
}


/**
 *  StaticBrowse::getBrowseListDataProvider - populates the static browse list screen
 *  @param [in] activeDeviceTag
 *  @param [in] activeDeviceType
 *  @return tSharedPtrDataProvider
 */
tSharedPtrDataProvider StaticBrowse::getBrowseListDataProvider(uint32 /*activeDeviceTag*/, uint32 activeDeviceType)
{
   ETG_TRACE_USR4(("StaticBrowse::getBrowseListDataProvider"));
   ListDataProviderBuilder listBuilder(LIST_ID_BROWSER_MAIN, DATA_CONTEXT_LIST_BUTTON_BROWSER_ITEM);
   bool indexingStatus = getIndexingStatusToEnableList();
   if (activeDeviceType == SOURCE_USB)
   {
      uint32 itemIdx = 0;

#if defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE1)|| defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R)
      bool playingBrowserType = MediaProxyUtility::getInstance().getBrowsertype();
      listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForStaticBrowse(itemIdx)).AddData(LANGUAGE_STRING(USB__BROWSE_Button_FolderBrowse, "FOLDER BROWSE")).AddData(true).AddData(playingBrowserType);
#endif
      listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForStaticBrowse(itemIdx)).AddData(LANGUAGE_STRING(USB__BROWSE_Button_PlayList, "PLAYLIST")).AddData(indexingStatus).AddData(false);
      listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForStaticBrowse(itemIdx)).AddData(LANGUAGE_STRING(USB__BROWSE_Button_Artist, "ARTIST")).AddData(indexingStatus).AddData(false);
      listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForStaticBrowse(itemIdx)).AddData(LANGUAGE_STRING(USB__BROWSE_Button_Album, "ALBUM")).AddData(indexingStatus).AddData(false);
      listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForStaticBrowse(itemIdx)).AddData(LANGUAGE_STRING(USB__BROWSE_Button_Song, "SONG")).AddData(indexingStatus).AddData(false);
      listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForStaticBrowse(itemIdx)).AddData(LANGUAGE_STRING(USB__BROWSE_Button_Genre, "GENRE")).AddData(indexingStatus).AddData(false);
      listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForStaticBrowse(itemIdx)).AddData(LANGUAGE_STRING(USB__BROWSE_Button_Composer, "COMPOSER")).AddData(indexingStatus).AddData(false);
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      MediaDatabinding::getInstance().updateFolderOrCurrentPlayingListBtnText(LANGUAGE_STRING(USB__BROWSE_Button_FolderList, "Folder List")); //Applicable only for scope2, empty function is called in scope1.
      //Since there is no Text id available for Folder list in Scope2 it is given as unavailable later it will be modified.
#endif
   }
   else if (activeDeviceType == SOURCE_IPOD)
   {
      uint32 itemIdx = 0;
      listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForStaticBrowse(itemIdx)).AddData(LANGUAGE_STRING(IPOD__MENU_Button_PlayList, "PlayList")).AddData(true);
      listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForStaticBrowse(itemIdx)).AddData(LANGUAGE_STRING(IPOD__MENU_Button_Artist, "ARTIST")).AddData(true);
      listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForStaticBrowse(itemIdx)).AddData(LANGUAGE_STRING(IPOD__MENU_Button_Album, "ALBUM")).AddData(true);
      listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForStaticBrowse(itemIdx)).AddData(LANGUAGE_STRING(IPOD__MENU_Button_Song, "SONG")).AddData(true);
      listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForStaticBrowse(itemIdx)).AddData(LANGUAGE_STRING(IPOD__MENU_Button_Genre, "GENRE")).AddData(true);
      listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForStaticBrowse(itemIdx)).AddData(LANGUAGE_STRING(IPOD__MENU_Button_Composer, "COMPOSER")).AddData(true);
      listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForStaticBrowse(itemIdx)).AddData(LANGUAGE_STRING(IPOD__MENU_Button_Audiobook, "AUDIOBOOK")).AddData(true);
      listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForStaticBrowse(itemIdx)).AddData(LANGUAGE_STRING(IPOD__MENU_Button_Podcast, "PODCAST")).AddData(true);
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      MediaDatabinding::getInstance().updateFolderOrCurrentPlayingListBtnText(LANGUAGE_STRING(IPOD__MENU_Button_CurrentList, "Current List")); //Applicable only for scope2, empty function is called in scope1
#endif
   }
   tSharedPtrDataProvider dataProvider = listBuilder.CreateDataProvider();
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));
   POST_MSG((COURIER_MESSAGE_NEW(ListChangeMsg)(LIST_ID_BROWSER_MAIN, ListChangeSet, _windowStartIndex)));
   _windowStartIndex = 0;  // reset start index to display list from beginning on new entry from main screen
   return tSharedPtrDataProvider();
}


/**
 * getlistItemTemplateForStaticBrowse - function to get list template.
 * @param[in] uint32 index
 * @parm[out] const char
 * @return const char - return listItemTemplate
 */
const char* StaticBrowse::getlistItemTemplateForStaticBrowse(uint32& index)
{
   const char* listItemTemplate;

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R
   if (index % 2)
   {
      listItemTemplate = DATA_CONTEXT_LIST_BUTTON_BROWSER_ITEM_EVEN;
   }
   else
   {
      listItemTemplate = DATA_CONTEXT_LIST_BUTTON_BROWSER_ITEM;
   }

#else
   listItemTemplate = DATA_CONTEXT_LIST_BUTTON_BROWSER_ITEM;
#endif
   ++index;
   return listItemTemplate;
}


/**
 *  StaticBrowse::getIndexingStatusToEnableList - returns true value in case of scope2.1
 *  @param[in] none
 *  @return bool
 */
bool StaticBrowse::getIndexingStatusToEnableList()
{
#ifndef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R
   return _isIndexingDone;
#else
   return true;
#endif
}


/**
 *  StaticBrowse::setWindowStartIndex - sets the window start index for static browse
 *  @param[in] windowStartIndex
 *  @return none
 */
void StaticBrowse::setWindowStartIndex(uint32 windowStartindex)
{
   _windowStartIndex = windowStartindex;
}


void StaticBrowse::setFocussedIndex(int32 focussedIndex)
{
   ETG_TRACE_USR4(("StaticBrowse::setFocussedIndex:%d", focussedIndex));
   _focussedIndex = focussedIndex;
}


}
}
