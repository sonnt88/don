/**
 *  @file   BrowserSpeedLock.cpp
 *  @author ECV - vma6cob
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#include "hall_std_if.h"
#include "BrowserSpeedLock.h"
#include "Core/Utils/KdsInfo.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL_SPEEDLOCK
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::BrowserSpeedLock::
#include "trcGenProj/Header/BrowserSpeedLock.cpp.trc.h"
#endif

using namespace ::MPlay_fi_types;

namespace App {
namespace Core {

BrowserSpeedLock* BrowserSpeedLock::_browserSpeedLock = NULL;

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
const unsigned int USER_INACTIVE_TIMEOUT_IN_BROWSE = 1000;  //time in ms
#endif
BrowserSpeedLock::BrowserSpeedLock(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy):
   _mediaPlayerProxy(pMediaPlayerProxy),
   _deviceTag(0),
   _focussedIndex(-1),
   _listHandle(0),
   _nextListHandle(0),
   _currentListType(0),
   _nextListType(0),
   _listItemTag(0),
   _firstListItemTag(0),
   _filterTag1(0),
   _filterTag2(0),
   _filterTag3(0),
   _speedLockState(false)


{
   ETG_I_REGISTER_FILE();
   _browserSpeedLock = this;

   VehicleDataCommonHandler::VehicleDataHandler::GetInstance()->vRegisterforSpeedLock(this);
}


BrowserSpeedLock::~BrowserSpeedLock()
{
   VehicleDataCommonHandler::VehicleDataHandler::GetInstance()->vUnRegisterforSpeedLock(this);
   _mediaPlayerProxy.reset();
   _browserSpeedLock = NULL;
}


/**
 * startTimer - Function to start the timer
 * @return none
 */
void BrowserSpeedLock::startTimer()
{
   ETG_TRACE_USR4(("BrowserSpeedLock timerStarted"));
   _userOperationTimer.start();
}


/**
 * stopTimer - Function to stop the timer
 * @return none
 */
void BrowserSpeedLock::stopTimer()
{
   ETG_TRACE_USR4(("BrowserSpeedLock timerStopped"));
   if (_userOperationTimer.getStatus() != Util::Timer::Stopped)
   {
      _userOperationTimer.stop();
   }
}


/**
 * currentSpeedLockState - overridden function from ISpeedLockState - gives current speedlock state
 * @param[in] - lockState - bool
 * @return none
 */
void BrowserSpeedLock::currentSpeedLockState(bool lockState)
{
   ETG_TRACE_USR4(("BrowserSpeedLock currentSpeedLockState:%d %d", lockState, _speedLockState));
   if (lockState != _speedLockState) // not to start timer for repeated "true" states
   {
      _speedLockState = lockState;
      (_speedLockState) ? startUserInactiveTimeout() : stopTimer();
   }
}


/**
 * startUserInactiveTimeout - Function to start the timer to calcuate the user inactivity
 * @return none
 */
void BrowserSpeedLock::startUserInactiveTimeout()
{
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
   if (KdsInfo::isJapanRegion() && _speedLockState)
   {
      ETG_TRACE_USR4(("BrowserSpeedLock startUserInactiveTimeout"));
      UserInactivityTimeoutInBrowseMsg* pNotifyMsg = COURIER_MESSAGE_NEW(UserInactivityTimeoutInBrowseMsg)();
      _userOperationTimer.setTimeout(0, USER_INACTIVE_TIMEOUT_IN_BROWSE, pNotifyMsg);

      startTimer();
   }
#endif
}


/**
 * UserInactivityTimeoutInBrowseMsg - message posted when the timeout has happened
 * @return true - message consumed
 */
bool BrowserSpeedLock::onCourierMessage(const UserInactivityTimeoutInBrowseMsg&)
{
   ETG_TRACE_USR4(("BrowserSpeedLock UserInactivityTimeoutInBrowseMsg"));
   stopTimer();

   if (_currentListType == LIST_ID_BROWSER_FOLDER_BROWSE)
   {
      ETG_TRACE_USR4(("BrowserSpeedLock : folderbrowse:%d", _focussedIndex));
      _mediaPlayerProxy->sendPlayItemFromListStart(*this, _listHandle, _focussedIndex, 0);
   }
   else
   {
      if (isListTypeSong(_currentListType))
      {
         uint32 itemTag = (_focussedIndex >= 0) ? _listItemTag : _firstListItemTag;
         ETG_TRACE_USR4(("BrowserSpeedLock metadata:%d", itemTag));
         _mediaPlayerProxy->sendPlayItemFromListByTagStart(*this, _listHandle, itemTag);
      }
      else if (isListTypeSong(_nextListType))
      {
         _mediaPlayerProxy->sendCreateMediaPlayerIndexedListStart(*this, (::MPlay_fi_types::T_e8_MPlayListType)(_nextListType % 4000),
               _filterTag1, _filterTag2, _filterTag3, _deviceTag);
      }
      else
      {
         ETG_TRACE_USR4(("next list type not song"));
      }
   }
   return true;
}


/**
 * requestPlayAllSongsForMetadataBrowse - function is called when PlayAllSongs msg is posted.
 * @return bool
 */
bool BrowserSpeedLock::requestPlayAllSongsForMetadataBrowse()
{
   ETG_TRACE_USR4(("BrowserSpeedLock _nextListType:%d and _currentListType:%d", _nextListType, _currentListType));
   bool playPossible = false;
   if (isListTypeSong(_currentListType))
   {
      _mediaPlayerProxy->sendPlayItemFromListByTagStart(*this, _listHandle, _firstListItemTag);
      playPossible = true;
   }
   else if (isListTypeSong(_nextListType))
   {
      createMediaPlayerIndexedListStart(_nextListType, _filterTag1, _filterTag2, _filterTag3);
      playPossible = true;
   }
   else
   {
      playPossible = false;
   }
   return playPossible;
}


/**
 * createMediaPlayerIndexedListStart - function is called when nextlisttype is not song .
 * @return none
 */
void BrowserSpeedLock::createMediaPlayerIndexedListStart(uint32 listType, uint32 filter1, uint32 filter2, uint32 filter3)
{
   ETG_TRACE_USR4(("BrowserSpeedLock::createMediaPlayerIndexedListStart %d %d %d %d", listType, filter1, filter2, filter3));
   _mediaPlayerProxy->sendCreateMediaPlayerIndexedListStart(*this, (::MPlay_fi_types::T_e8_MPlayListType)(listType % 4000),
         filter1, filter2, filter3, _deviceTag);
}


/**
 * onCreateMediaPlayerIndexedListResult - function called when mediaplayer gets a result
 *                                        during the method call of CreateMediaPlayerIndexedListStart
 * @return none
 */
void BrowserSpeedLock::onCreateMediaPlayerIndexedListResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerIndexedListResult >& result)
{
   _nextListHandle = result->getU32ListHandle();
   uint32 listSize = result->getU32ListSize();
   ETG_TRACE_USR4(("BrowserSpeedLock onCreateMediaPlayerIndexedListResult size:%d", listSize));
   if (listSize)
   {
      _mediaPlayerProxy->sendRequestMediaPlayerIndexedListSliceStart(*this, _nextListHandle, 0, 1);
   }
}


/**
 * onRequestMediaPlayerIndexedListSliceResult - function called when mediaplayer gets a result
 *                                              during the method call of RequestMediaPlayerIndexedListSliceStart
 * @return none
 */
void BrowserSpeedLock::onRequestMediaPlayerIndexedListSliceResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestMediaPlayerIndexedListSliceResult >& result)
{
   ETG_TRACE_USR4(("BrowserSpeedLock onRequestMediaPlayerIndexedListSliceResult%d", result->getOMediaObjects()[0].getE8CategoryType()));
   uint32 tag = 0;

   switch (result->getOMediaObjects()[0].getE8CategoryType())
   {
      case T_e8_MPlayCategoryType__e8CTY_PLAYLIST:
      case T_e8_MPlayCategoryType__e8CTY_GENRE:
      case T_e8_MPlayCategoryType__e8CTY_COMPOSER:
      case T_e8_MPlayCategoryType__e8CTY_NAME:
      {
         tag = result->getOMediaObjects()[0].getU32MetaDataTag1();
         break;
      }

      case T_e8_MPlayCategoryType__e8CTY_ARTIST:
      case T_e8_MPlayCategoryType__e8CTY_TITLE:
      case T_e8_MPlayCategoryType__e8CTY_EPISODE:
      {
         tag = result->getOMediaObjects()[0].getU32MetaDataTag2();
         break;
      }

      case T_e8_MPlayCategoryType__e8CTY_ALBUM:
      case T_e8_MPlayCategoryType__e8CTY_CHAPTER:
      {
         tag = result->getOMediaObjects()[0].getU32MetaDataTag3();
         break;
      }

      case T_e8_MPlayCategoryType__e8CTY_SONG:
      {
         tag = result->getOMediaObjects()[0].getU32MetaDataTag4();
         break;
      }

      default:
      {
         break;
      }
   }

   _mediaPlayerProxy->sendPlayItemFromListByTagStart(*this, _nextListHandle, tag);
}


/**
 * UserInactivityTimerStopMsg - message posted from SM to stop timer when state is exited
 * @return true - message consumed
 */
bool BrowserSpeedLock::onCourierMessage(const UserInactivityTimerStopMsg&)
{
   stopTimer();

   return true;
}


/**
 * setDeviceTag - function to set device tag
 * @return none
 */
void BrowserSpeedLock::setDeviceTag(uint32 deviceTag)
{
   _deviceTag = deviceTag;
}


/**
 * setCurrentListHandle - function to set current list handle
 * @return none
 */
void BrowserSpeedLock::setCurrentListHandle(uint32 listHandle)
{
   ETG_TRACE_USR4(("BrowserSpeedLock listHandle:%d", listHandle));
   _listHandle = listHandle;
}


/**
 * setFocussedIndex - function to set focussed item index
 * @return none
 */
void BrowserSpeedLock::setFocussedIndex(int32 focussedIndex)
{
   ETG_TRACE_USR4(("BrowserSpeedLock focussedIndex:%d", focussedIndex));
   _focussedIndex = focussedIndex;
}


/**
 * setFocussedIndex - function to set current list type
 * @return none
 */
void BrowserSpeedLock::setCurrentListType(uint32 listType)
{
   ETG_TRACE_USR4(("BrowserSpeedLock listType:%d", listType));
   _currentListType = listType;
}


/**
 * setSelectedListItemTag - function to set tag of the focussed item
 * @return none
 */
void BrowserSpeedLock::setSelectedListItemTag(uint32 itemTag)
{
   ETG_TRACE_USR4(("BrowserSpeedLock itemTag:%d", itemTag));
   _listItemTag = itemTag;
}


/**
 * setNextListId - function to set next list type
 * @return none
 */
void BrowserSpeedLock::setNextListId(uint32 nextListId)
{
   ETG_TRACE_USR4(("BrowserSpeedLock setNextListId:%d", nextListId));
   _nextListType = nextListId;
}


/**
 * setFirstItemTag - function to set tag of first list item
 * @return none
 */
void BrowserSpeedLock::setFirstItemTag(uint32 tag)
{
   ETG_TRACE_USR4(("BrowserSpeedLock setFirstItemTag:%d", tag));
   _firstListItemTag = tag;
}


/**
 * setFilterTag - function to set filter tags to fetch next list
 * @return none
 */
void BrowserSpeedLock::setFilterTag(uint32 tag1, uint32 tag2, uint32 tag3)
{
   ETG_TRACE_USR4(("BrowserSpeedLock setFilterTag:%d %d %d", tag1, tag2, tag3));
   _filterTag1 = tag1;
   _filterTag2 = tag2;
   _filterTag3 = tag3;
}


/**
 * isListTypeSong - function to check if given list type is Song
 * @param[in] listType - The list type which needs to be checked
 * @return bool - true if given list type is song
 */
bool BrowserSpeedLock::isListTypeSong(uint32 listType)
{
   bool isCurrentListTypeSong;
   switch (listType)
   {
      case LIST_ID_BROWSER_PLAYLIST:
      case LIST_ID_BROWSER_PLAYLIST_SONG:
      case LIST_ID_BROWSER_ALBUM_SONG:
      case LIST_ID_BROWSER_GENRE_ALBUM_SONG:
      case LIST_ID_BROWSER_GENRE_ARTIST_ALBUM_SONG:
      case LIST_ID_BROWSER_GENRE_ARTIST_SONG:
      case LIST_ID_BROWSER_GENRE_SONG:
      case LIST_ID_BROWSER_ARTIST_ALBUM_SONG:
      case LIST_ID_BROWSER_ARTIST_SONG:
      case LIST_ID_BROWSER_COMPOSER_ALBUM_SONG:
      case LIST_ID_BROWSER_COMPOSER_SONG:
      case LIST_ID_BROWSER_BOOKTITLE_CHAPTER:
      case LIST_ID_BROWSER_PODCAST_EPISODE:
      case LIST_ID_BROWSER_SONG:
         isCurrentListTypeSong = true;
         break;
      default:
         isCurrentListTypeSong = false;
         break;
   }
   return isCurrentListTypeSong;
}


/**
 * onPlayItemFromListError - function called when mediaplayer gets a error when PlayItemFromListStart is called
 * @return none
 */
void BrowserSpeedLock::onPlayItemFromListError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::PlayItemFromListError >& /*error*/)
{
   ETG_TRACE_USR4(("BrowserSpeedLock onPlayItemFromListError"));
}


/**
 * onPlayItemFromListResult - function called when mediaplayer gets a result when PlayItemFromListStart is called
 * @return none
 */
void BrowserSpeedLock::onPlayItemFromListResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::PlayItemFromListResult >& /*result*/)
{
   ETG_TRACE_USR4(("BrowserSpeedLock onPlayItemFromListResult"));
}


/**
 * onPlayItemFromListByTagError - function called when mediaplayer gets a error when PlayItemFromListByTagStart is called
 * @return none
 */
void BrowserSpeedLock::onPlayItemFromListByTagError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::PlayItemFromListByTagError >& /*error*/)
{
   ETG_TRACE_USR4(("BrowserSpeedLock onPlayItemFromListByTagError"));
}


/**
 * onPlayItemFromListByTagResult - function called when mediaplayer gets a result when PlayItemFromListByTagStart is called
 * @return none
 */
void BrowserSpeedLock::onPlayItemFromListByTagResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::PlayItemFromListByTagResult >& /*result*/)
{
   ETG_TRACE_USR4(("BrowserSpeedLock onPlayItemFromListByTagResult"));
}


}
}
