/**
 *  @file   BrowseFactory.cpp
 *  @author ECV - vma6cob
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#include "hall_std_if.h"
#include "BrowseFactory.h"
#include "FolderBrowse.h"
#include "StaticBrowse.h"
#include "MetaDataBrowse.h"
#include "VerticalKeyboardSeachHandler.h"

namespace App {
namespace Core {

BrowseFactory::BrowseFactory()
{
}


BrowseFactory::~BrowseFactory()
{
}


/**
 *  BrowseFactory::getBrowser - Function to return the IBrowse pointer based on the listId
 *  @param [in] listId
 *  @return IBrowser
 */
IBrowse* BrowseFactory::getBrowser(uint32 listId)
{
   IBrowse* browse = 0;
   switch (listId)
   {
      case LIST_ID_BROWSER_MAIN:
         static StaticBrowse staticBrowse;
         browse = &staticBrowse;
         break;
      case LIST_ID_BROWSER_FOLDER_BROWSE:
      case LIST_ID_BROWSER_IMAGE:
         static FolderBrowse folderbrowse;
         browse = &folderbrowse;
         break;
#ifdef  VERTICAL_KEYBOARD_SEARCH_SUPPORT
         /*Can not able to create object for vertical list for scope1
          * because language handler is not available for scope1*/
      case LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST:
      case LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST:
         static VerticalKeyboardSearch verticalKeyboardSearch;
         browse = &verticalKeyboardSearch;
         break;
#endif
      default:
         static MetaDataBrowse metaDataBrowse;
         browse = &metaDataBrowse;
         break;
   }
   assert(browse != NULL);
   return browse;
}


}
}
