/**
*  @file   ListInterfaceHandler.h
*  @author ECV - chs4cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef LISTINTERFACEHANDLER_HEADER
#define LISTINTERFACEHANDLER_HEADER


#include "mplay_fi_if.h"
#include "mplay_MediaPlayer_FIProxy.h"
#include "ListUtilityDatabase.h"
#include "IListFacade.h"
#include "Core/MediaDefines.h"
#include "BrowserSpeedLock.h"
#include "App/datapool/MediaDataPoolConfig.h"
#include "Core/PlayScreen/IPlayScreen.h"
/*typedef struct
{
   uint32 u32ListHandle;
   std::string sSearchKeyboardLetter;
   bool bLetterAvailable;
   uint32 u32LetterStartIndex;
   uint32 u32LetterEndIndex;
} trSearchKeyboard_ListItem;*/


namespace App {
namespace Core {

class MediaBrowseHandling;
class ListInterfaceHandler
   : public IListFacade
   , public ::mplay_MediaPlayer_FI::CreateMediaPlayerIndexedListCallbackIF
   , public ::mplay_MediaPlayer_FI::RequestMediaPlayerIndexedListSliceCallbackIF
   , public ::mplay_MediaPlayer_FI::PlayItemFromListByTagCallbackIF
   , public ::mplay_MediaPlayer_FI::CreateMediaPlayerFileListCallbackIF
   , public ::mplay_MediaPlayer_FI::CreateMediaPlayerPlaylistListCallbackIF
   , public ::mplay_MediaPlayer_FI::RequestMediaPlayerFileListSliceCallbackIF
   , public ::mplay_MediaPlayer_FI::RequestMediaPlayerPlaylistListSliceCallbackIF
   , public ::mplay_MediaPlayer_FI::PlayItemFromListCallbackIF
   , public ::mplay_MediaPlayer_FI::ReleaseMediaPlayerListCallbackIF
   , public ::mplay_MediaPlayer_FI::RequestListInformationCallbackIF
   , public ::mplay_MediaPlayer_FI::MediaPlayerListChangeCallbackIF
#ifdef HMI_CDDA_SUPPORT
   , public  ::mplay_MediaPlayer_FI::CreateMediaPlayerCDListCallbackIF
#endif
#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
   , public ::mplay_MediaPlayer_FI::QuicksearchCallbackIF
#endif
#if defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
   , public ::mplay_MediaPlayer_FI::SearchKeyboardMediaPlayerListCallbackIF
#endif
{
   public:
      ListInterfaceHandler(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy,
                           BrowserSpeedLock* _browserSpeedLock, IPlayScreen* pPlayScreen);
      ~ListInterfaceHandler();
      void requestListData(const RequestedListInfo& requestedListInfo);
      void requestPlayItem(uint32 selectedIndex);
      void setActiveIndex(uint32 activeIndex);
      Candera::String getTextBySelectedIndex(uint32);
      void requestMediaPlayerIndexedList(uint32 ListType);
      void requestChildIndexList(uint32 nextListId, uint32 activeDeviceTag);
      virtual void registerProperties();
      virtual void deregisterProperties();

      virtual void onCreateMediaPlayerIndexedListError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerIndexedListError >& error);
      virtual void onCreateMediaPlayerIndexedListResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerIndexedListResult >& result);

      virtual void onRequestMediaPlayerIndexedListSliceError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestMediaPlayerIndexedListSliceError >& error);
      virtual void onRequestMediaPlayerIndexedListSliceResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestMediaPlayerIndexedListSliceResult >& result);

      virtual void onPlayItemFromListByTagError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::PlayItemFromListByTagError >& error);
      virtual void onPlayItemFromListByTagResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::PlayItemFromListByTagResult >& result);

      virtual void onReleaseMediaPlayerListError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::ReleaseMediaPlayerListError >& /*error*/);
      virtual void onReleaseMediaPlayerListResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
            const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::ReleaseMediaPlayerListResult >& result);

      // Folder based browsing methods
      virtual void onCreateMediaPlayerFileListError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerFileListError >& /*error*/);
      virtual void onCreateMediaPlayerFileListResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerFileListResult >& result);

      virtual void onCreateMediaPlayerPlaylistListError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerPlaylistListError >& /*error*/);
      virtual void onCreateMediaPlayerPlaylistListResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerPlaylistListResult >& result);

      virtual void onRequestMediaPlayerFileListSliceError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestMediaPlayerFileListSliceError >& /*error*/);
      virtual void onRequestMediaPlayerFileListSliceResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestMediaPlayerFileListSliceResult >& result);

      virtual void onRequestMediaPlayerPlaylistListSliceError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestMediaPlayerPlaylistListSliceError >& /*error*/);
      virtual void onRequestMediaPlayerPlaylistListSliceResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestMediaPlayerPlaylistListSliceResult >& result);

      virtual void onPlayItemFromListError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
                                           const boost::shared_ptr< ::mplay_MediaPlayer_FI::PlayItemFromListError >& /*error*/);
      virtual void onPlayItemFromListResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
                                            const boost::shared_ptr< ::mplay_MediaPlayer_FI::PlayItemFromListResult >& result);

      virtual void onRequestListInformationError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
            const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestListInformationError >& error);

      virtual void onRequestListInformationResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
            const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestListInformationResult >& result);

#ifdef HMI_CDDA_SUPPORT
      virtual void onCreateMediaPlayerCDListError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerCDListError >& /*error*/);
      virtual void onCreateMediaPlayerCDListResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerCDListResult >& result);
#endif

#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
      //QuickSearch
      virtual void onQuicksearchError(const ::boost::shared_ptr<mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
                                      const ::boost::shared_ptr<mplay_MediaPlayer_FI::QuicksearchError >& /*error*/);

      virtual void onQuicksearchResult(const ::boost::shared_ptr<mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
                                       const ::boost::shared_ptr<mplay_MediaPlayer_FI::QuicksearchResult >& result);
#endif
      virtual void onMediaPlayerListChangeError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< mplay_MediaPlayer_FI::MediaPlayerListChangeError >& error);
      virtual void onMediaPlayerListChangeStatus(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< mplay_MediaPlayer_FI::MediaPlayerListChangeStatus >& status);
#if defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
      //Vertical Keyboard Search
      virtual void onSearchKeyboardMediaPlayerListError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< mplay_MediaPlayer_FI::SearchKeyboardMediaPlayerListError >& /*error*/);

      virtual void onSearchKeyboardMediaPlayerListResult(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< mplay_MediaPlayer_FI::SearchKeyboardMediaPlayerListResult >& result);
#endif
      void requestMediaPlayerFileList(std::string strPath);
      void requestMediaPlayerPlaylist(const std::string strPlaylistName);
      std::string getCurrentFolderPath() const;
      void removeListFromMap();
      bool isRootFolder();
      void setCurrentStateMachineState(uint8);
      void setCurrentFolderPath(std::string);
      void setCurrentPlaylistPath(std::string);
      void decrementCurrentFolderLevel();
      void updateFolderListParameters(uint32, uint32);
      enum FolderListType GetFileType(::MPlay_fi_types::T_e8_MPlayFileType);
      void setFocussedIndexAndNextListId(int32, uint32);


//    virtual void onSearchKeyboardMediaPlayerListError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& proxy, const boost::shared_ptr< SearchKeyboardMediaPlayerListError >& error);
//    virtual void onSearchKeyboardMediaPlayerListResult(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& proxy, const boost::shared_ptr< SearchKeyboardMediaPlayerListResult >& result);

      void requestPlayOrSubFolderList(uint32 index);
      void initStatemachine();
      void requestCurrentPlayingList(uint32 currentPlayingListHandle);

      uint32 getCategoryType(uint32 MListType) const;
      std::string getFirstStringElement(uint32);
      void Reindexing(uint32 ActiveDeviceTag);
      void releaseListHandle(uint32 listHandle);
      uint32 getCurrentStartIndex(uint32 listid);
      void setFolderRequestType(enum FolderListType folderListRequestType);
      bool isListWaitingToComplete(void);
      void initialiseListValuesForFolderBrowser(uint32);
      void getFiltersToRequestIndexedList(uint32, uint32&, uint32&, uint32&);
#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
      void vRequestQSMediaplayerFilelist(std::string SearchString, uint32 startIndex);
#endif
//#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
//      bool getBTCurrentPlayingListStatus();
//#endif
#ifdef ABCSEARCH_SUPPORT
      std::map<std::string, trSearchKeyboard_ListItem> getABCSearchKeyboardListItemMap();
#endif
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_MAP_DELEGATE_END()

   private:
      void triggerStatemachine();
      void requestMediaPlayerList();
#ifdef HMI_CDDA_SUPPORT
      void requestMediaPlayerCDDAList();
#endif
      void requestMediaPlayerSliceList();
      void createInstanceIfNull(uint32 listType);
      void updateIndexInMapDB(const ListResultInfo& listResultInfo);
      void getFiltersToRequestListByIndex(uint32, uint32, uint32&, uint32&, uint32&);
      /*For folder browsing*/
      void requestMediaPlayerIndexedListSlice(uint32 listHandle, uint32 windowStart, uint32 windowSize);
      void requestMediaPlayerFileListSlice(uint32 listHandle, uint32 windowStart, uint32 windowSize);
      void requestMediaPlayerPlaylistSlice(uint32 listHandle, uint32 windowStart, uint32 windowSize);
      void updateFolderListDataInMap(const FolderListInfo& structFolderListItemInfo);
      void triggerStateMachineIfListNotEmpty(uint32 listsize);
      void insertNewUtilityDatabaseToListMap(uint32 listType, ListUtilityDatabase* listUtilityDatabase);
      void updateIndexListSliceToGuiList();
      void updateMetadataListSliceToGuiList(const ::MPlay_fi_types::T_MPlayMediaObjects& mediaobject);
      void updateFileListSliceToGuiList(const ::MPlay_fi_types::T_MPlayFileList& fileObject);
      void requestParentIndexList(uint32 prevListId);
      void requestParentFolderList(const RequestedListInfo& requestedListInfo);
      bool shouldSongRestart(uint32 tag);
#if defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
      void updateSearchkeyboardinfo(uint32 listHandel);
#endif
#ifdef BROWSER_FOOTER_DEFAULT_VALUES_SUPPORT
      void updateBrowserFooterWithDefaultValues();
#endif

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R
      void requestPlayAllsongs();
#endif
      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy > _mediaPlayerProxy;
      BrowserSpeedLock* _browserSpeedLock;
      IPlayScreen& _iPlayScreen;

      std::string _currentFolderPath;
      std::string _currentPlaylist;
      uint32 _currentListType;
      uint32 _deviceTag;
      uint8 _currentStateMachineState;
      enum FolderListType _folderRequestType;
      std::map<uint32, ListUtilityDatabase* > _listMap;
      std::map<uint32, FileInfo > _folderListMap;
//    trSearchKeyboard_ListItem m_SearchKeyboardListItem;
      bool _isWaitingForListCompletion;
      ::MPlay_fi_types::T_MPlayMediaObjects _oMediaObjects;
      ::MPlay_fi_types::T_MPlayFileList _oFileListMediaObjects;
      bool _isNewList;
      bool _isBackTraversing;
#if defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
      trSearchKeyboard_ListItem m_SearchKeyboardListItem;
#endif
//#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
//      bool _btCurrentPlayingListStatus;
//#endif
      uint32 _currListHandel;
      uint32 _currLanguage;

#ifdef ABCSEARCH_SUPPORT
      std::map<std::string, trSearchKeyboard_ListItem> _mABCSearchKeyboardListItemMap;
#endif
};


}//end of namespace Core
}//end of namespace App
#endif //LISTINTERFACEHANDLER_HEADER
