/**
 *  @file   MediaScope1Databinding.cpp
 *  @author ECV - IVI-MediaTeam
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */
#include "hall_std_if.h"
#include "MediaDatabinding.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::MediaDatabinding::
#include "trcGenProj/Header/MediaDatabinding.cpp.trc.h"
#endif

namespace App {
namespace Core {

MediaDatabinding* MediaDatabinding::_theInstance = 0;
MediaDatabinding::~MediaDatabinding()
{
   ETG_TRACE_USR4(("clMedia_Databindinghandler Destructor"));
   removeInstance();
}


MediaDatabinding::MediaDatabinding()
{
   // TODO Auto-generated constructor stub
}


/**
 *  MediaDatabinding::getInstance - function to get singleton instance
 *  @return Singleton Instance
 */
MediaDatabinding& MediaDatabinding::getInstance()
{
   if (_theInstance == 0)
   {
      _theInstance = new MediaDatabinding();
   }

   assert(_theInstance);
   return *_theInstance;
}


/**
 *  MediaDatabinding::removeInstance - function to delete the singleton instance
 *  @return Singleton Instance
 */
void MediaDatabinding::removeInstance()
{
   if (_theInstance)
   {
      delete _theInstance;
      _theInstance = 0;
   }
}


/**
 * updateTrackNumber  - called to update the track no details
 * @param[in] int,int
 * @return void
 *  This function is used to print the track number info in scope1 which is used only for TML .
 */
void MediaDatabinding::updateTrackNumber(uint32 currentTrackNo, uint32 totalTrackNo)
{
   char currentTrack[ARRAY_SIZE] = "\0";
   char totalTrack[ARRAY_SIZE] = "\0";
   currentTrackNo = currentTrackNo + 1;// 1 is added since mediaplayer count starts from 0
   snprintf(currentTrack, sizeof currentTrack, "%d", currentTrackNo);
   snprintf(totalTrack, sizeof totalTrack, "%d", totalTrackNo);
   std::string currentPlayingTrack = currentTrack;
   std::string totalAvailableTracks = totalTrack;
   std::string trackinfo = currentPlayingTrack + "/" +  totalAvailableTracks;
   _currentTrackInfo = trackinfo;

   ETG_TRACE_USR4(("Current playing trackNumber is : %s", trackinfo.c_str()));
}


/**
 * UpdateMediaMetadataInfo - Function to update meta data to GUI
 * @param[in] ArtistName, AlbumName, TitleName
 * @parm[out] None
 * @return void
 */
void MediaDatabinding::updateMediaMetadataInfo(std::string strArtist, std::string strAlbum, std::string strTitle)
{
   MediaDatabindingCommon::updateMediaMetadataInfo(strArtist, strAlbum, strTitle);
}


/**
 * updateVolumeAvailability - This is a property update function and it will be called by sound whenever there is a change in Volume
 *
 * @param[in] volumelevel
 * @return void
 */
void MediaDatabinding::updateVolumeAvailability(int16 volumeLevel)
{
   ETG_TRACE_USR4(("updateVolumeAvailability %d", volumeLevel));
   _isVolumeAvailable = (volumeLevel == 0) ? false : true;
   (*_volumeInfo).mIsVolumeAvailable = _isVolumeAvailable;
   _volumeInfo.MarkAllItemsModified();
   if (_volumeInfo.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("onVolumeUpdate : Updation failed..!!!"));
   }
}


/**
 * updateBTDisconnectionStatusOverUSB
 * @param[in] :isBTDisconnectedOverUSB,isMetadataVisible
 * @parm[out] :None
 * @return Void
 */
void MediaDatabinding::updateBTDisconnectionStatusOverUSB(bool isBTDisconnectedOverUSB, bool isMetadataVisible)
{
   (*_BTDisconnectionUpdateOverUSB).mDefaultTextUpdate = isBTDisconnectedOverUSB;
   (*_btHandsetModeActiveStatus).misMetadataVisible = isMetadataVisible;
   _BTDisconnectionUpdateOverUSB.MarkItemModified(ItemKey::BTScreenUpdateWhenConnectedWithUSB::DefaultTextUpdateItem);
   _btHandsetModeActiveStatus.MarkItemModified(ItemKey::BTHandSetModeActiveStatus::isMetadataVisibleItem);

   _BTDisconnectionUpdateOverUSB.SendUpdate(true);
   _btHandsetModeActiveStatus.SendUpdate(true);
}


void MediaDatabindingCommon::updateBTScreenOnCallHandsetModeActive(bool bisHandsetActive, bool bisMetadataVisible)
{
   ETG_TRACE_USR4(("updateBTHandsetModeActiveStatus :%d , %d", bisHandsetActive , bisMetadataVisible));
   (*_btHandsetModeActiveStatus).misHandsetModeActive = bisHandsetActive;
   (*_btHandsetModeActiveStatus).misMetadataVisible = bisMetadataVisible;
   _btHandsetModeActiveStatus.MarkItemModified(ItemKey::BTHandSetModeActiveStatus::isHandsetModeActiveItem);
   _btHandsetModeActiveStatus.MarkItemModified(ItemKey::BTHandSetModeActiveStatus::isMetadataVisibleItem);
   ETG_TRACE_USR4(("item modified"));
   if (_btHandsetModeActiveStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("UpdateBTIconState : Updation failed..!!!"));
   }
}


// VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
}


}
