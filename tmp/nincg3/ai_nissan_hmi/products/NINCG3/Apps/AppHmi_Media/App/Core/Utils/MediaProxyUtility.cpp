/**
*  @file   MediaProxyUtility.cpp
*  @author ECV - kng3kor
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#include "hall_std_if.h"
#include "MediaProxyUtility.h"
#include "AppHmi_MediaTypes_Common.h"
#include "Core/MediaDefines.h"
#include "MediaUtils.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_APPHMI_MEDIA_HALL
#include "trcGenProj/Header/MediaProxyUtility.cpp.trc.h"
#endif

const char* SCREEN_MEDIA__CD_CDMP3_MAIN = "Media#Scenes#MEDIA__CD_CDMP3_MAIN";
const char* SCREEN__MEDIA__AUX__USB_MAIN = "Media#Scenes#MEDIA__AUX__USB_MAIN";
const char* SCREEN__MEDIA__AUX__IPOD_MAIN = "Media#Scenes#MEDIA__AUX__IPOD_MAIN";
const char* SCREEN__MEDIA_AUX__BTAUDIO_MAIN_DEFAULT = "Media#Scenes#MEDIA_AUX__BTAUDIO_MAIN_DEFAULT";
const char* SCREEN__MEDIA__AUX__BTAUDIO_MAIN = "Media#Scenes#MEDIA__AUX__BTAUDIO_MAIN";

namespace App {
namespace Core {

MediaProxyUtility* MediaProxyUtility::_theInstance = NULL;

/**
 * @Destructor
 */
MediaProxyUtility::~MediaProxyUtility()
{
}


#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
/**
 * @Constructor
 */
MediaProxyUtility::MediaProxyUtility(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy)
   : _mediaPlayerProxy(pMediaPlayerProxy)
{
}


void MediaProxyUtility::createInstance(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy)
{
   if (_theInstance == NULL)
   {
      _theInstance = new MediaProxyUtility(pMediaPlayerProxy);
   }
}


/**
 *  MediaProxyUtility::getInstance - function to get singleton instance
 *  @return Singleton Instance
 */
MediaProxyUtility& MediaProxyUtility::getInstance()
{
   assert(_theInstance);
   return *_theInstance;
}


#else
/**
 * @Constructor
 */
MediaProxyUtility::MediaProxyUtility(): _dummyActiveDeviceType(1000u)
{
}


/**
 *  MediaProxyUtility::getInstance - function to get singleton instance
 *  @return Singleton Instance
 */
MediaProxyUtility& MediaProxyUtility::getInstance()
{
   if (NULL == _theInstance)
   {
      _theInstance = new MediaProxyUtility();
   }
   return *_theInstance;
}


#endif


#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
/**
 * GetDeviceType - to get currently playing device type
 *
 * @param[in] deviceTag
 * @parm[out]
 * @return uint32 media player device type
 */
uint32 MediaProxyUtility::GetDeviceType(uint32 deviceTag)
{
   uint32 deviceType = MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_UNKNOWN;
   if (_mediaPlayerProxy->hasMediaPlayerDeviceConnections())
   {
      ::MPlay_fi_types::T_MPlayDeviceInfo deviceInfo = _mediaPlayerProxy->getMediaPlayerDeviceConnections().getODeviceInfo();
      ::MPlay_fi_types::T_MPlayDeviceInfo::iterator itr = deviceInfo.begin();
      for (; itr != deviceInfo.end(); itr++)
      {
         if (itr->getU8DeviceTag() == deviceTag)
         {
            deviceType = itr->getE8DeviceType();
            break;
         }
      }
   }
   else
   {
      ETG_TRACE_USR4(("Device ConnectionHandling Status not received."));
   }
   return deviceType;
}


uint32 MediaProxyUtility::getHallDeviceType(uint32 deviceTag)
{
   return MediaUtils::ConvertHallDeviceType(GetDeviceType(deviceTag));
}


/**
 * getActiveDeviceTag - get currently playing device tag
 *
 * @param[in] none
 * @parm[out]
 * @return uint32
 */
uint32 MediaProxyUtility::getActiveDeviceTag()
{
   uint32 u32ActiveDeviceTag = 0;

   if (_mediaPlayerProxy->hasMediaPlayerDeviceConnections())
   {
      ::MPlay_fi_types::T_MPlayDeviceInfo deviceInfo = _mediaPlayerProxy->getMediaPlayerDeviceConnections().getODeviceInfo();
      ::MPlay_fi_types::T_MPlayDeviceInfo::iterator itr = deviceInfo.begin();
      for (; itr != deviceInfo.end(); itr++)
      {
         if (itr->getBDeviceActiveSource())
         {
            u32ActiveDeviceTag = itr->getU8DeviceTag();
            break;
         }
      }
   }
   else
   {
      ETG_TRACE_USR4(("Device ConnectionHandling Status not received."));
   }
   ETG_TRACE_USR4((" active device tag=%d", u32ActiveDeviceTag));

   return (u32ActiveDeviceTag);
}


#endif


#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
/**
 * getActiveDeviceType - get currently playing device type
 *
 * @param[in] none
 * @parm[out]
 * @return uint32
 */
uint32 MediaProxyUtility::getActiveDeviceType()
{
   uint32 u32ActiveDeviceTag = getActiveDeviceTag();
   return GetDeviceType(u32ActiveDeviceTag);
}


#else
/**
 * getActiveDeviceType - get dummy value of currently playing device type for unit testing
 *
 * @param[in] none
 * @parm[out]
 * @return uint32
 */
uint32 MediaProxyUtility::getActiveDeviceType()
{
   return _dummyActiveDeviceType;
}


/**
 * setDummyActiveDeviceType - set dummy value of currently playing device type for unit testing
 *
 * @param[in] none
 * @parm[out]
 * @return uint32
 */
void MediaProxyUtility::setDummyActiveDeviceType(uint32 dummyActiveDeviceType)
{
   _dummyActiveDeviceType = dummyActiveDeviceType;
}


#endif


/**
 * ConvertHallDeviceType - covert media player device type to media HALL type
 *
 * @param[in] deviceType
 * @parm[out]
 * @return uint32 media hall device type
 */


uint32 MediaProxyUtility::getHallActiveDeviceType()
{
   return MediaUtils::ConvertHallDeviceType(getActiveDeviceType());
}


#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
/**
 * bIsIndexingDone - returns true if the Device Indexing is completed otherwise returns false.
 * @param[in] None
 * @parm[out] None
 * @return bool value
 */
bool MediaProxyUtility::isIndexingDone(uint32 activeDeviceTag)
{
//   MPlay_fi_types::T_MPlayIndexingStateResult indexingState;
   bool isIndexingCompleted = false;

   if (_mediaPlayerProxy->hasIndexingState())
   {
      uint8 deviceIndexState = MPlay_fi_types::T_e8_MPlayDeviceIndexedState__e8IDS_NOT_STARTED;
      uint8 deviceIndexPercent = 0;
      getCurrentDeviceIndexingValues(deviceIndexState, deviceIndexPercent);

      if (deviceIndexState == MPlay_fi_types::T_e8_MPlayDeviceIndexedState__e8IDS_COMPLETE && activeDeviceTag != MEDIA_VIRTUAL_DEVICE_TAG)
      {
         ETG_TRACE_USR4(("bIsIndexingDone::Indexed status complete returning true"));
         isIndexingCompleted = true;
      }
   }
   else
   {
      ETG_TRACE_USR4(("IndexingState Status not received."));
   }
   return isIndexingCompleted;
}


void MediaProxyUtility::getCurrentDeviceIndexingValues(uint8& indexingState, uint8& indexingPercent)
{
   MPlay_fi_types::T_MPlayIndexingStateResult indexingStateResult;
   uint32 activeDeviceTag = getActiveDeviceTag();

   if (_mediaPlayerProxy->hasIndexingState())
   {
      indexingStateResult = _mediaPlayerProxy->getIndexingState().getOIndexingStateResult();
      MPlay_fi_types::T_MPlayIndexingStateResult::iterator itr = indexingStateResult.begin();

      while (itr != indexingStateResult.end())
      {
         if (itr->getU8DeviceTag() == activeDeviceTag)
         {
            indexingState = itr->getE8DeviceIndexedState();
            indexingPercent = itr->getU8IndexingPercentComplete();
            itr = indexingStateResult.end();
            ETG_TRACE_USR4(("bIsIndexingDone:deviceIndexState = %d", indexingState));
         }
         else
         {
            itr++;
         }
      }//end of the loop
   }
   else
   {
      indexingState = 0;
      indexingPercent = 0;
   }
}


bool MediaProxyUtility::isAnyBTDeviceConnected()
{
   bool isBTConnected = FALSE;

   if (_mediaPlayerProxy->hasMediaPlayerDeviceConnections())
   {
      ::MPlay_fi_types::T_MPlayDeviceInfo deviceInfo = _mediaPlayerProxy->getMediaPlayerDeviceConnections().getODeviceInfo();
      ::MPlay_fi_types::T_MPlayDeviceInfo::iterator itr = deviceInfo.begin();
      for (; itr != deviceInfo.end(); itr++)
      {
         if (itr->getBDeviceConnected() && (itr->getE8DeviceType() == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH))
         {
            isBTConnected = TRUE;
            break;
         }
      }
   }
   else
   {
      ETG_TRACE_USR4(("Device ConnectionHandling Status not received."));
   }
   ETG_TRACE_USR4((" isBTConnected = %d", isBTConnected));

   return (isBTConnected);
}


bool MediaProxyUtility::isAnyBTDeviceDisConnectedOverUSB()
{
   bool isBTConnectedWithUSB = FALSE;
   if (_mediaPlayerProxy->hasMediaPlayerDeviceConnections())
   {
      ::MPlay_fi_types::T_MPlayDeviceInfo deviceInfo = _mediaPlayerProxy->getMediaPlayerDeviceConnections().getODeviceInfo();
      ::MPlay_fi_types::T_MPlayDeviceInfo::iterator itr = deviceInfo.begin();
      for (; itr != deviceInfo.end(); itr++)
      {
         if ((itr->getE8DeviceType() == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH) && (itr->getE8DisconnectReason() == MPlay_fi_types::T_e8_MPlayDisconnectReason__e8DR_CONNECTED_OVER_USB))
         {
            isBTConnectedWithUSB = TRUE;
            break;
         }
      }
   }
   return isBTConnectedWithUSB;
}


/**
 * GetConnectedDeviceTag - to get first connected device tag for device type
 *
 * @param[in] deviceType
 * @parm[out]
 * @return uint32 media player device type
 */
uint32 MediaProxyUtility::GetConnectedDeviceTag(uint32 deviceType)
{
   uint32 deviceTag = 0;
   if (_mediaPlayerProxy->hasMediaPlayerDeviceConnections())
   {
      ::MPlay_fi_types::T_MPlayDeviceInfo deviceInfo = _mediaPlayerProxy->getMediaPlayerDeviceConnections().getODeviceInfo();
      ::MPlay_fi_types::T_MPlayDeviceInfo::iterator itr = deviceInfo.begin();
      for (; itr != deviceInfo.end(); itr++)
      {
         if ((itr->getE8DeviceType() == deviceType) && (itr->getBDeviceConnected() == true))
         {
            deviceTag = itr->getU8DeviceTag();
            ETG_TRACE_USR4(("Connected device tag = %d", deviceTag));
            break;
         }
      }
   }
   else
   {
      ETG_TRACE_USR4(("Device ConnectionHandling Status not received."));
   }
   return deviceTag;
}


/**
 * getBrowsertype - get the browser type
 * @return bool CurrentPlayingListHandle
 */
bool MediaProxyUtility::getBrowsertype()
{
   bool CurrentPlayingListHandle = (_mediaPlayerProxy->getCurrentFolderPath().getU32ListHandle() == 0) ? false : true ;
   return CurrentPlayingListHandle;
}


/**
 * getViewName - get the browser type
 * @return bool CurrentPlayingListHandle
 */

std::string MediaProxyUtility::getViewName(uint32 deviceTag)
{
   uint32 deviceType = GetDeviceType(deviceTag);
   switch (deviceType)
   {
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_USB:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_MTP:
         return SCREEN__MEDIA__AUX__USB_MAIN;

      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDROM:
         return SCREEN_MEDIA__CD_CDMP3_MAIN;

      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPOD:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPHONE:
         return SCREEN__MEDIA__AUX__IPOD_MAIN;

      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH:
         return SCREEN__MEDIA__AUX__BTAUDIO_MAIN ;

      default:
         return "none";
   }
}


#endif
}


}
