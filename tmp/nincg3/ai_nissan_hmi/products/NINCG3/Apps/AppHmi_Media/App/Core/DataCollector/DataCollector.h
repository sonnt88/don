/*
 * @file       : DataCollector.h
 * @author     : CM-PJCB
 * @copyright  : (c) 2015 Robert Bosch Car Multimedia GmbH
 * @addtogroup : Media
 */
#ifndef DATACOLLECTOR_H
#define DATACOLLECTOR_H

#include "datacollector_main_fiProxy.h"
#include "AppHmi_MediaStateMachine.h"
#include "DataCollector_defines.h"
#include "IDataCollector.h"
#include "MediaDatabinding.h"
#include "Core/Utils/MediaUtils.h"
#include "datacollector_main_fi_typesConst.h"
#include "StatusBarHandler.h"
#include "midw_smartphoneint_fiProxy.h"
#include "Core/Spi/SpiConnectionHandling.h"


namespace App {
namespace Core {
class IDeviceConnection;
class SpiConnectionHandling; //forward declaration

class DataCollector :
   public IDataCollector,
   public IStatusBarHandler

{
   public:

      DataCollector(IDeviceConnection* _pdeviceConnection, SpiConnectionHandling* paraSpiConnectionHandling);
      virtual ~DataCollector();

      virtual void setHeaderIconData(enHeaderIcon headerIconData);
      virtual void setHeaderTextData(const Candera::String& headerTextData);
      virtual void vInitializeStatusLineDataDefault();

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      ON_COURIER_MESSAGE(ScreenHeaderReq)
      ON_COURIER_MESSAGE(IndexingInfoUpdMsg)
      ON_COURIER_MESSAGE(BrowserHeaderUpdMsg)
      ON_COURIER_MESSAGE(InterruptToMeterMsg)
      ON_COURIER_MESSAGE(LanguageChangeUpdMsg)

      COURIER_MSG_MAP_DELEGATE_DEF_BEGIN()
      COURIER_MSG_DELEGATE_TO_OBJ(_statusBarHandler)
      COURIER_MSG_MAP_DELEGATE_DEF_END()
      COURIER_MSG_MAP_DELEGATE_END()

   protected:

      bool onCourierMessage(const ScreenHeaderReq& msg);
      bool onCourierMessage(const IndexingInfoUpdMsg& msg);
      bool onCourierMessage(const BrowserHeaderUpdMsg& msg);
      bool onCourierMessage(const InterruptToMeterMsg& msg);
      bool onCourierMessage(const LanguageChangeUpdMsg& /*msg*/);
      void updateIndexingOnActiveScreen(int8, int8);
      void updateAuxInfo(void) const;
      void vMapSpiSourcetype();
   private:

      virtual void updatePhoneSignalStrength(uint8 SignalStrength);
      virtual void updateHeaderPhoneBatteryInfo(uint8 BatteryInfo);
      virtual void updateHeaderPhoneBTConnectInfo(uint8 BTConnectInfo);
      virtual void updateHeaderTAInfo(uint8 TAInfo);
      virtual void updateHeaderTimeInfo(std::string TimeInfo);
      virtual void updateMeterConnectionInfo(bool MeterConnectionStatus);
      virtual void updateStatusLineDefaultData();

      void setHeaderIconVisibility(bool isVisible);
      void setHeaderTextVisibility(bool isVisible);
      void setHeaderAntennaIconVisibility(bool isVisible);
      void setHeaderBatteryIconVisibility(bool isVisible);
      void setHeaderTAIconVisibility(bool isVisible);
      void setHeaderBTIconVisibility(bool isVisible);
      void setHeaderSMSIconVisibility(bool isVisible);
      void setHeaderSMSAutoIconVisibility(bool isVisible);
      void setHeaderGraceNoteVisibility(bool isVisible);
      void setHeaderTCUIconVisibility(bool isVisible);
      void setHeaderIconDynDataVisibility(bool isVisible);

      Candera::String getHeaderText(uint32 sHeaderIndex) const;
      enHeaderIcon getHeaderIcon(uint32 sHeaderIndex) const;
      void updateHeaderBasedOnActiveMedia(std::string);

      std::map<uint32, ::datacollector_main_fi_types::T_e8_datacollector_Audio_Source> m_Source_MAP;
      std::map<uint32, ::datacollector_main_fi_types::T_e8_datacollector_Interrupt_Type> m_Action_MAP;

      IDeviceConnection& _ideviceConnection;
      SpiConnectionHandling* _mpSpiConnectionHandling;

      //DataBinding
      Courier::DataItemContainer<HeaderIconDataBindingSource> _headerIcon;
      Courier::DataItemContainer<HeaderTextDataBindingSource> _headerText;
      Courier::DataItemContainer<HeaderTimeDataBindingSource> _headerTime;
      Courier::DataItemContainer<HeaderAntennaIconDataBindingSource> _headerAntennaIcon;
      Courier::DataItemContainer<HeaderBatteryIconDataBindingSource> _headerBatteryIcon;
      Courier::DataItemContainer<HeaderTAiconDataBindingSource> _headerTAicon;
      Courier::DataItemContainer<HeaderBTiconDataBindingSource> _headerBTicon;
      Courier::DataItemContainer<HeaderSMSiconDataBindingSource> _headerSMSicon;
      Courier::DataItemContainer<HeaderSMSautoIconDataBindingSource> _headerSMSautoIcon;
      Courier::DataItemContainer<HeaderGraceNoteIconDataBindingSource> _headerGraceNote;
      Courier::DataItemContainer<HeaderTCUiconDataBindingSource> _headerTCUicon;
      Courier::DataItemContainer<HeaderIconDynDataBindingSource> _headerIconDynData;

      StatusBarCommonHandler::StatusBarHandler* _statusBarHandler;

      uint32 _currentActiveScreen;
      Candera::String _metadataBrowserHeader;
      std::string _sHeaderText_USB;
      std::string _sHeaderText_IPOD;
      int8 _indexPercent;
      int8 _indexState;

      DECLARE_CLASS_LOGGER();
};


} // namespace Core
} // namespace App


#endif
