/**
*  @file   <Spi_Unit_Test.cpp>
*  @author <ECV> - <bvi1cob>
*  @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
*  @addtogroup <AppHmi_media>
*  @{
*/

#include "Spi_Unit_Test.h"


INSTANTIATE_TEST_CASE_P(ValidDeviceInfoTest, ValidDeviceStatus_Test, testing::ValuesIn(deviceStatus));


TEST_P(ValidDeviceStatus_Test, ValidDeviceStatus_ReturnsOK)
{
   EXPECT_EQ(_validDeviceResult(GetParam()), _spiUtil->updateDeviceStatusValidity(_validDevice(GetParam())));
}


INSTANTIATE_TEST_CASE_P(InvalidDeviceInfoTest, InValidDeviceStatus_Test, testing::ValuesIn(inValidDevice));


TEST_P(InValidDeviceStatus_Test, InValidDeviceStatus_ReturnsOK)
{
   EXPECT_EQ(_inValidDeviceResult(GetParam()), _spiUtil->updateDeviceStatusValidity(_inValidDevice(GetParam())));
}

INSTANTIATE_TEST_CASE_P(ValidConnectionTypeInfoTest, ValidDeviceConnectionType_Test, testing::ValuesIn(ValidConnection));


TEST_P(ValidDeviceConnectionType_Test, ValidConnectionType_ReturnsOK)
{
   EXPECT_EQ(_ValidConnectionResult(GetParam()), _spiUtil->deviceConnectiontypeValidity(_ValidConn(GetParam())));
}


INSTANTIATE_TEST_CASE_P(InValidConnectionTypeInfoTest, InValidDeviceConnectionType_Test, testing::ValuesIn(InValidConnection));


TEST_P(InValidDeviceConnectionType_Test, InValidConnectionType_ReturnsOK)
{
   EXPECT_EQ(_InValidConnectionResult(GetParam()), _spiUtil->deviceConnectiontypeValidity(_InValidConn(GetParam())));
}


INSTANTIATE_TEST_CASE_P(ValidUSBConnectionInfoTest, ValidUSBConnection_Test, testing::ValuesIn(USBConnStatus));


TEST_P(ValidUSBConnection_Test, ValidUSBConnection_ReturnsOK)
{
   EXPECT_EQ(_validUSBConnResult(GetParam()), _spiUtil->checkUSBConnectedDeviceValidity(_validDeviceStatus(GetParam()), _validConnectionType(GetParam())));
}

INSTANTIATE_TEST_CASE_P(InValidUSBConnectionInfoTest, InValidUSBConnection_Test, testing::ValuesIn(inValidUSBConnStatus));


TEST_P(InValidUSBConnection_Test, InValidUSBConnection_ReturnsOK)
{
   EXPECT_EQ(_inValidUSBConnResult(GetParam()), _spiUtil->checkUSBConnectedDeviceValidity(_inValidDeviceStatus(GetParam()), _inValidConnectionType(GetParam())));
}

INSTANTIATE_TEST_CASE_P(ValidDIPOConnectionInfoTest, ValidDIPOConnection_Test, testing::ValuesIn(DIPOConnStatus));


TEST_P(ValidDIPOConnection_Test, ValidDIPOConnection_ReturnsOK)
{
   EXPECT_EQ(_validDIPOConnResult(GetParam()), _spiUtil->dipoConnectionValidity(_validConnectionStatus(GetParam())));
}

INSTANTIATE_TEST_CASE_P(InValidDIPOConnectionInfoTest, InValidDIPOConnection_Test, testing::ValuesIn(InValidDIPOConnStatus));


TEST_P(InValidDIPOConnection_Test, InValidDIPOConnection_ReturnsOK)
{
   EXPECT_EQ(_inValidDIPOConnResult(GetParam()), _spiUtil->dipoConnectionValidity(_inValidConnectionStatus(GetParam())));
}








