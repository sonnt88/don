/**
 *  @file   MediaScope2_1_RDatabinding.cpp
 *  @author ECV - IVI-MediaTeam
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#ifndef MEDIADATABINDING_SCOPE2_H_
#define MEDIADATABINDING_SCOPE2_H_

#include "Core/DataModel/MediaDatabindingCommon.h"
namespace App {
namespace Core {
class MediaDatabinding: public MediaDatabindingCommon
{
   public:
      MediaDatabinding();
      virtual ~MediaDatabinding();

      void updateAuxLevel(int16 AuxLevel);
      void updateFolderOrCurrentPlayingListBtnText(Candera::String buttonText);
      void updateBTMenuSupportStatus(Courier::Bool);
      void updateBTRepeatRandomStatus(Courier::Bool, Courier::Bool);
      void updateBTFFNextTrackSupportStatus(Courier::Bool, Courier::Bool);
      void updateBTFRPrevTrackSupportStatus(Courier::Bool, Courier::Bool);
      void updateBTPlayTimeSupportStatus(Courier::Bool);
      void updateBTFolderBrowseSupportStatus(Courier::Bool);
      void updateAlbumArtTogglestatus(Courier::Bool);
      void updateDefaultAlbumArtVisibleStatus(Courier::Bool);
      void updateMediaMetadataInfo(std::string, std::string, std::string);
      void updateVideoPlayerViewModeSettings(Courier::Bool);
      void updateVolumeAvailability(int16);
      static MediaDatabinding& getInstance();
      void updateCurrentPlayingListBtnStatus(bool);
      void updateAlbumArtVisibleStatus(bool);
      void updateTrackNumber(uint32, uint32);
      void updateFooterInFolderBrowse(std::string);
      void updateTBTInfo(const bool& isRGActive, const TBTManeuverInfoData& TBTManeuverInfo);
      void SetApplicationModeValue(uint8);
      bool updateVerticalSearchListIndex(uint32, uint32);
      bool updateVerticalSearchLanguageSwitch(bool value);
      void updateThumbnailAlbumArt(Courier::Bool, Courier::Bool);
#ifdef PHOTOPLAYER_SUPPORT
      bool updatePhotoPlayerInfoPhotoName(std::string);
      bool updatePhotoPlayerInfoPhotoContent(const ::std::vector< uint8 > u8PhotoContent, uint32 u32PhotoContentSize , bool);
      bool updatePhotoPlayerInfoPhotoContent(std::string strPhotoFilePath, bool);
      bool updatePhotoPlayerInfoPlayPauseActiveStatus(bool, bool);
#endif
#ifdef POPOUTLIST_SUPPORT
      void updateOptionButtonVisiblity(uint32);
#endif
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_VIEW_COMP)
      COURIER_DUMMY_CASE(0)
      COURIER_MSG_MAP_END()

   private:
      static void removeInstance();

      DataBindingItem<BTMetadataAvailabilityDataBindingSource> _btMetaDataVisibility;
      DataBindingItem<BTButtonFunctionalityStatusDataBindingSource> _btButtonSupportStatus;
      DataBindingItem<VolumeButtonStatusDataBindingSource> _volumeButtonStatus;
      DataBindingItem<BrowseScreenButtonsDataBindingSource> _browseScreenButton;
      DataBindingItem<TrackNumberDetailsDataBindingSource> _trackNumberDetails;
      DataBindingItem<TBTManeuverInfoDataBindingSource> _mTBTManeuverInfo;
      DataBindingItem<ThumbNailAlbumArtDataBindingSource> _mThumbNail;
      DataBindingItem<VideoPlayerSettingsDataBindingSource> _mVideoPlayerSettings;
#ifdef PHOTOPLAYER_SUPPORT
      DataBindingItem<PhotoPlayerInfoDataBindingSource> _photoPlayerInfo;
#endif
#ifdef POPOUTLIST_SUPPORT
      DataBindingItem<Zone3ParametersDataBindingSource> _zone3Parameters;
#endif

      static MediaDatabinding* _theInstance;
      bool _isNextTrackSupportAvailForBT;
      bool _isPreviousTrackSupportAvailForBT;
      uint8 _modeValue;
};


}
}


#endif /* MEDIADATABINDING_SCOPE2_H_ */
