/**
 *  @file   SpiConnectionHandling.cpp
 *  @author ECV - alc7kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#include "hall_std_if.h"

#include "SpiConnectionHandling.h"
#include "SpiConnectionObserver.h"
#ifdef DP_DATAPOOL_ID
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_hmi_04_if.h"
#endif
#include "App/datapool/SpiMediaDataPoolConfig.h"
#include "bosch/cm/ai/nissan/hmi/contextswitchservice/ContextSwitchTypesConst.h"
#include "MediaDatabinding.h"

#include "ProjectBaseTypes.h"

#include "AppHmi_MasterBase/AudioInterface/AudioDefines.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS         TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::SpiConnectionHandling::


#include "trcGenProj/Header/SpiConnectionHandling.cpp.trc.h"
#endif

using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;

namespace App {
namespace Core {

using namespace ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange;
using namespace MASTERAUDIOSERVICE_INTERFACE::SoundProperties;
using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;
/**
 * static pointer to class SpiConnectionHandling
 * Implemented for testing
 */
static SpiConnectionHandling* pstaticSpiConnectionHandling;


/**
 * Description     : Destructor of class SpiConnectionHandling
 */
SpiConnectionHandling::~SpiConnectionHandling()
{
   ETG_TRACE_USR4(("SpiConnectionHandling: Destructor"));
   _spiMidwServiceProxy.reset();
   _audioSourceChangeSPIProxy.reset();
   _mDpHandler = NULL;
   mptr_ContextProvider = NULL;
   _mSpiConnectionNotifyer = NULL;
   _mcurrentsource = 255;
   _mbRVCStatus = false;
   _mbDisplayFlagStatus = false;
   _mu32DIPODeviceHandle = UNDEFINE_VALUE;
   _mu32AAPDeviceHandle = UNDEFINE_VALUE;
   _mi8DeviceConnectionType = INVALID_CONNECTION;
   _mbCarPlayConnStatus = false;
   _mbAAPConnStatus = false;
   _mbSessionStatus = false;
   _meActiveDeviceCategory = ::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_UNKNOWN;
   _meBtCallStatus = PhoneAppState__SPI_APP_STATE_PHONE_UNKNOWN;
   _meAccessorySpeechAppState = SpeechAppState__SPI_APP_STATE_SPEECH_UNKNOWN;
   _meAccessoryNavAppState = NavigationAppState__SPI_APP_STATE_NAV_UNKNOWN;
   _mSpiVideoActive = false;
   _mu8CurrentHMIState = HMI_MODE_OFF;
   _meHandBrakeState = T_e8_ParkBrake__PARK_BRAKE_NOT_ENGAGED;
   _meVehicleState = T_e8_VehicleState__PARK_MODE;
#ifdef SPI_TEMPORARYCONTEXT_ONRVC_NCG3D_7788
   _mbSpiRvcTemporaryContext = false;
#endif
}


/**
 * Description     : Constructor of class SpiConnectionHandling
 */
SpiConnectionHandling::SpiConnectionHandling(::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& pSpiConnectionProxy, AppMedia_ContextProvider* para_ContextProvider , ::boost::shared_ptr< ::bosch::cm::ai::hmi::masteraudioservice::AudioSourceChange::AudioSourceChangeProxy >& audioSourceChangeSPIProxy, ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy >& soundPropertiesProxy , SpiConnectionSubject* paraSpiConnectionNotify)
   : _spiMidwServiceProxy(pSpiConnectionProxy)
   , mptr_ContextProvider(para_ContextProvider)
   , _mCurrentActiveAudioSource(SRC_MEDIA_NO_SRC)
   , _meCurrentAudioContext(AudioContext__SPI_AUDIO_MAIN)
   , _audioSourceChangeSPIProxy(audioSourceChangeSPIProxy)
   , _soundPropertiesProxy(soundPropertiesProxy)
   , _mSpiConnectionNotifyer(paraSpiConnectionNotify)
{
   ETG_TRACE_USR4(("SpiConnectionHandling: Constructor"));
   _mu32DIPODeviceHandle = UNDEFINE_VALUE;
   bIlmInit = false;
   _mcurrentsource = 255;
   ETG_I_REGISTER_FILE();
   _mDpHandler = SpiMediaDataPoolConfig::getInstance();
   pstaticSpiConnectionHandling = this;
   _mbRVCStatus = false;
   _mbDisplayFlagStatus = false;
   _mu32AAPDeviceHandle = UNDEFINE_VALUE;
   _mi8DeviceConnectionType = INVALID_CONNECTION;
   _mbCarPlayConnStatus = false;
   _mbAAPConnStatus = false;
   _mbSessionStatus = false;
   _meActiveDeviceCategory = ::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_UNKNOWN;
   _meBtCallStatus = PhoneAppState__SPI_APP_STATE_PHONE_UNKNOWN;
   _meAccessorySpeechAppState = SpeechAppState__SPI_APP_STATE_SPEECH_UNKNOWN;
   _meAccessoryNavAppState = NavigationAppState__SPI_APP_STATE_NAV_UNKNOWN;
   _mSpiVideoActive = false;
   _mbIsSettTrigDisc = false;
   _mbAuthorizationStatus = false;
   _bMuteState = false;
   _mu8CurrentHMIState = HMI_MODE_OFF;
   _mUserSelection = false;
   _mbDeviceSelectionReq = false;
   _meHandBrakeState = T_e8_ParkBrake__PARK_BRAKE_NOT_ENGAGED;
   _meVehicleState = T_e8_VehicleState__PARK_MODE;
   _mu32BTDeviceHandle = 255;
   _mu32PrjDeviceHandle = 255;
   _meBTChangeInfo = T_e8_BTChangeInfo__NO_CHANGE;
   _mbIsBtDeactivationReq = false;
#ifdef SPI_TEMPORARYCONTEXT_ONRVC_NCG3D_7788
   _mbSpiRvcTemporaryContext = false;
#endif
   if (ILM_FAILED == ilm_init())
   {
      ETG_TRACE_USR4((" tclSmartPhoneIntegrationAppLogic::tclSmartPhoneIntegrationAppLogic() - ilm_init failed "));
      bIlmInit = FALSE;
   }
   else
   {
      bIlmInit = TRUE;
   }

   //! Make Carplay icon Invisible on initial
   (*_IsCarplayiconStatus).misCarplayiconVisible = false;
   _IsCarplayiconStatus.SendUpdate(true);

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
   (*_IsDontRemindBtnStatus).misDontRemindButtonEnable = false;
   _IsDontRemindBtnStatus.SendUpdate(true);
   _IsDontRemindBtnStatus.MarkItemModified(ItemKey::DontRemindButtonStatus::isDontRemindButtonEnableItem);

   (*_IsAndroidDontRemindBtnStatus).misAndroidAutoDontRemindButtonEnable = false;
   _IsAndroidDontRemindBtnStatus.SendUpdate(true);
   _IsAndroidDontRemindBtnStatus.MarkItemModified(ItemKey::AndroidAutoDontRemindButtonStatus::isAndroidAutoDontRemindButtonEnableItem);

   (*_isDIPOSessionActive).misDIPOActive = false;
   _isDIPOSessionActive.SendUpdate(true);
   _isDIPOSessionActive.MarkItemModified(ItemKey::DIPOActiveStatus::isDIPOActiveItem);

   (*_isAAPSessionActive).misAndroidAutoActive = false;
   _isAAPSessionActive.SendUpdate(true);
   _isAAPSessionActive.MarkItemModified(ItemKey::AndroidAutoActiveStatus::isAndroidAutoActiveItem);

   (*_isDisclaimerRemindBtnStatus).misRemindBtnEnabled = false;
   _isDisclaimerRemindBtnStatus.SendUpdate(true);
   _isDisclaimerRemindBtnStatus.MarkItemModified(ItemKey::DisclaimerRemindBtnStatus::isRemindBtnEnabledItem);
#endif
}


/*
 * This function performs the upreg of the required properties.
 * @param[in]  - proxy: Pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - stateChange: Contains the current Service State
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_spiMidwServiceProxy && (_spiMidwServiceProxy == proxy))
   {
      _spiMidwServiceProxy->sendDeviceStatusInfoUpReg(*this);
      _spiMidwServiceProxy->sendSessionStatusInfoUpReg(*this);
      _spiMidwServiceProxy->sendDeviceDisplayContextUpReg(*this);
      _spiMidwServiceProxy->sendDeviceAudioContextUpReg(*this);
      _spiMidwServiceProxy->sendBluetoothDeviceStatusUpReg(*this);
      _spiMidwServiceProxy->sendApplicationMediaMetaDataUpReg(*this);
      _spiMidwServiceProxy->sendProjectionDeviceAuthorizationUpReg(*this);
   }
   else if (_audioSourceChangeSPIProxy && (_audioSourceChangeSPIProxy == proxy))
   {
      _audioSourceChangeSPIProxy->sendActiveSourceListRegister(*this);
   }
   else if (_soundPropertiesProxy && _soundPropertiesProxy == proxy)
   {
      _soundPropertiesProxy->sendVolumeRegister(*this);
      _soundPropertiesProxy->sendMuteStateRegister(*this);
   }
}


/*
 * This function performs the Relupreg of the required properties.
 * @param[in]  - proxy: Pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - stateChange: Contains the current Service State
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_spiMidwServiceProxy && (_spiMidwServiceProxy == proxy))
   {
      _spiMidwServiceProxy->sendDeviceStatusInfoRelUpRegAll();
      _spiMidwServiceProxy->sendSessionStatusInfoRelUpRegAll();
      _spiMidwServiceProxy->sendDeviceDisplayContextRelUpRegAll();
      _spiMidwServiceProxy->sendDeviceAudioContextRelUpRegAll();
      _spiMidwServiceProxy->sendBluetoothDeviceStatusRelUpRegAll();
      _spiMidwServiceProxy->sendApplicationMediaMetaDataRelUpRegAll();
      _spiMidwServiceProxy->sendProjectionDeviceAuthorizationRelUpRegAll();
   }
   else if (_audioSourceChangeSPIProxy && (_audioSourceChangeSPIProxy == proxy))
   {
      _audioSourceChangeSPIProxy->sendDeregisterAll();
   }
   else if (_soundPropertiesProxy && _soundPropertiesProxy == proxy)
   {
      _soundPropertiesProxy->sendVolumeDeregisterAll();
      _soundPropertiesProxy->sendMuteStateDeregisterAll();
   }
}


/*
 * This function is called when the property update for ProjectionDeviceAuthorization receive with error
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error : Pointer to the DeviceAuthorizationError
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onProjectionDeviceAuthorizationError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::ProjectionDeviceAuthorizationError >& /*error*/)
{
}


/*
 * This function is called when the property update for ProjectionDeviceAuthorization receive
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - status : Pointer to the DeviceAuthorizationStatus
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onProjectionDeviceAuthorizationStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::ProjectionDeviceAuthorizationStatus >& status)
{
   if (proxy == _spiMidwServiceProxy)
   {
      ETG_TRACE_USR4(("SpiConnectionHandling: onProjectionDeviceAuthorizationStatus Received : NumerofDevice = %d ", status->getNumDevices()));
      if ((false == status->getNumDevices()) && (false == getUserSelection()))
      {
         setSettingTrigDisc(false);
         if (::midw_smartphoneint_fi_types::T_e8_DeviceType__APPLE_DEVICE == getConnDeviceType())
         {
            ETG_TRACE_USR4(("HALL->GUI: DipoDeviceDisConnectedUpdMsg Posted "));
            POST_MSG((COURIER_MESSAGE_NEW(DipoDeviceDisConnectedUpdMsg)()));
         }
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
         if (::midw_smartphoneint_fi_types::T_e8_DeviceType__ANDROID_DEVICE == getConnDeviceType())
         {
            //! To sync with device category
            ETG_TRACE_USR4(("HALL->GUI: AndroidDeviceDisConnectedUpdMsg Posted"));
            POST_MSG((COURIER_MESSAGE_NEW(AndroidDeviceDisConnectedUpdMsg)()));
         }
         updateDontRemindBtnStatus();
#endif
      }
      ::std::vector< ::midw_smartphoneint_fi_types::T_DeviceAuthInfo >DeviceAuthInfo = status->getDeviceAuthInfoList();
      ::std::vector< ::midw_smartphoneint_fi_types::T_DeviceAuthInfo >::const_iterator itr = DeviceAuthInfo.begin();
      for (int8 i8Index = 0 ; itr != DeviceAuthInfo.end(); itr++, i8Index++)
      {
         ETG_TRACE_USR4(("onProjectionDeviceAuthorizationStatus DeviceHandle : %d DeviceType : %d DeviceAuthorizationStatus : %d " , itr->getDeviceHandle(), ETG_CENUM(enDeviceType, itr->getDeviceType()), ETG_CENUM(enUserAuthorizationStatus, itr->getUserAuthorizationStatus())));
         setConnDeviceType(itr->getDeviceType());
         if (T_e8_UserAuthorizationStatus__USER_AUTH_UNAUTHORIZED == itr->getUserAuthorizationStatus())
         {
            switch (itr->getDeviceType())
            {
               case ::midw_smartphoneint_fi_types::T_e8_DeviceType__APPLE_DEVICE :
               {
                  _mu32DIPODeviceHandle = itr->getDeviceHandle();
                  checkBTCallNShowDiscPopUp();
                  break;
               }
               case ::midw_smartphoneint_fi_types::T_e8_DeviceType__ANDROID_DEVICE :
               {
                  _mu32AAPDeviceHandle = itr->getDeviceHandle();
                  checkBTCallNShowDiscPopUp();
                  break;
               }
               default :
                  break;
            }
         }
      }
   }
}


/*
 * This function is called when the property update for DeviceStatusInfo fails
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to the DeviceStatusInfoError
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onDeviceStatusInfoError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceStatusInfoError >& /*error*/)
{
   ETG_TRACE_USR4(("SpiConnectionHandling: onDeviceStatusInfoError Received"));
}


/*
 * Virtual function implemented to get an update of DeviceStatusInfo
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - status: Pointer to the DeviceStatusInfoStatus
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onDeviceStatusInfoStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceStatusInfoStatus >& status)
{
   ETG_TRACE_USR4(("Entered onDeviceStatusInfoStatus.."));
   if (proxy == _spiMidwServiceProxy)
   {
      updateDeviceStatus(status->getDeviceStatus());
      updateDeviceConnectionType(status->getDeviceConnectionType());
      setDeviceConnectionType(status->getDeviceConnectionType());
      ETG_TRACE_USR4(("onDeviceStatusInfoStatus: DeviceConnectionStatus=%d,DeviceConnectionType=%d", status->getDeviceStatus(), status->getDeviceConnectionType()));
      bool USBconnectionValidity = SpiUtils::checkUSBConnectedDeviceValidity(status->getDeviceStatus(), status->getDeviceConnectionType());
      if (true == USBconnectionValidity)
      {
         ETG_TRACE_USR4(("onDeviceStatusInfoStatus: sendGetDeviceInfoListStart Post"));
         _spiMidwServiceProxy->sendGetDeviceInfoListStart(*this);
      }
   }
}


/**
 * UpdateDeviceStatus - helper function to handle device status
 * @param[in] -i8DeviceStatus
 * @parm[out] -None
 * @return    -Void
 */
void SpiConnectionHandling::updateDeviceStatus(int8 i8DeviceStatus)
{
   ETG_TRACE_USR4(("updateDeviceStatus: u8DeviceStatus=%d _meActiveDeviceCategory = %d", i8DeviceStatus , _meActiveDeviceCategory));
   bool deviceInfoStatus =  SpiUtils::updateDeviceStatusValidity(i8DeviceStatus);
   if (true == deviceInfoStatus)
   {
      switch (i8DeviceStatus)
      {
         case ::midw_smartphoneint_fi_types::T_e8_DeviceStatusInfo__NOT_KNOWN:
         case ::midw_smartphoneint_fi_types::T_e8_DeviceStatusInfo__DEVICE_CHANGED:
            break;
         case ::midw_smartphoneint_fi_types::T_e8_DeviceStatusInfo__DEVICE_REMOVED:
         {
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
            if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO == getActiveDeviceCategory())
            {
               if (true == _mbSessionStatus)
               {
                  ETG_TRACE_USR4(("HALL->GUI: AndroidAutoNoUSBPopUpReqMsg Posted "));
                  POST_MSG((COURIER_MESSAGE_NEW(AndroidAutoNoUSBPopUpReqMsg)()));
               }
               ETG_TRACE_USR4(("HALL->GUI: AAPStartingPopUpClosingReqMsg Posted "));
               POST_MSG((COURIER_MESSAGE_NEW(AAPStartingPopUpClosingReqMsg)()));
            }
            else if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_DIPO == getActiveDeviceCategory())
            {
               if (true == _mbSessionStatus)
               {
                  ETG_TRACE_USR4(("HALL->GUI: CarplayNoUSBPopUpReqMsg Posted "));
                  POST_MSG((COURIER_MESSAGE_NEW(CarplayNoUSBPopUpReqMsg)()));
               }
               ETG_TRACE_USR4(("HALL->GUI: CarplayStartingPopUpClosingReqMsg Posted "));
               POST_MSG((COURIER_MESSAGE_NEW(CarplayStartingPopUpClosingReqMsg)()));
            }
            ETG_TRACE_USR4(("HALL->GUI: SpiVideoExitUpdMsg Posted"));
            POST_MSG((COURIER_MESSAGE_NEW(SpiVideoExitUpdMsg)()));
            updateDontRemindBtnStatus();
#endif
            vHandleContextResponse();
#ifdef SPI_TEMPORARYCONTEXT_ONRVC_NCG3D_7788
            vUpdateAppModeStatusOnRVCOff();
#endif
            _mbCarPlayConnStatus = false;
            _mbAAPConnStatus = false;
            _meActiveDeviceCategory = ::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_UNKNOWN;
            //! Make Carplay icon Invisible on when device removed
            vUpdateCarplayIconStatus(false);
            _mbDeviceSelectionReq = false;
            setUserSelection(false);
            setConnDeviceType(::midw_smartphoneint_fi_types::T_e8_DeviceType__DEVICE_TYPE_UNKNOWN);
            setIsDeviceAuthorizationRequ(false);
            //! Added to remove Carplay icon as soon as Device is removed.
            //@need to check why removeUnavailableContext() takes time to update.
            /*if(NULL != mptr_ContextProvider)
                        {
                        mptr_ContextProvider->vRemoveContext(MEDIA_SPI_CONTEXT_CARPLAY_DEVICE_CONNECTED);
                        }*/
            break;
         }
         case ::midw_smartphoneint_fi_types::T_e8_DeviceStatusInfo__DEVICE_ADDED:
         {
            ETG_TRACE_USR4(("New Device Added"));
            infoStartUpParm();
            break;
         }
         default:
            break;
      }
   }
}


/**
 * updateDeviceConnectionType - helper function to handle the device conenctions
 * @param[in] -i8DeviceConnectionType
 * @parm[out] -None
 * @return    -Void
 */
void SpiConnectionHandling::updateDeviceConnectionType(int8 i8DeviceConnectionType)
{
   ETG_TRACE_USR4(("updateDeviceConnectionType= %d", i8DeviceConnectionType));
   bool _validConnectionType = SpiUtils::deviceConnectiontypeValidity(i8DeviceConnectionType);
   if (true == _validConnectionType)
   {
      switch (i8DeviceConnectionType)
      {
         case ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionType__UNKNOWN_CONNECTION:
         case ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionType__WIFI_CONNECTED:
         case ::midw_smartphoneint_fi_types::T_e8_DeviceConnectionType__USB_CONNECTED:
            break;
         default:
            break;
      }
   }
}


/*
 * This function is called when the method call for GetDeviceInfoList fails
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to the GetDeviceInfoListError
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onGetDeviceInfoListError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetDeviceInfoListError >& /*error*/)
{
   ETG_TRACE_USR4(("onGetDeviceInfoListError: Received"));
}


/*
 * Virtual function implemented to get the result of DeviceInfoList
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - result: Pointer to the GetDeviceInfoListResult
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onGetDeviceInfoListResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetDeviceInfoListResult >& result)
{
   ETG_TRACE_USR4(("onGetDeviceInfoListResult: Received"));
   if (proxy == _spiMidwServiceProxy)
   {
      ::std::vector< ::midw_smartphoneint_fi_types::T_DeviceDetails >DeviceInfoList = result->getDeviceInfoList();
      ::std::vector< ::midw_smartphoneint_fi_types::T_DeviceDetails >::const_iterator itr = DeviceInfoList.begin();
      ETG_TRACE_USR4(("onGetDeviceInfoListResult: Number of devices =%d ", result->getNumDevices()));
      for (int8 i8Index = 0 ; itr != DeviceInfoList.end(); itr++, i8Index++)
      {
         ETG_TRACE_USR4(("onGetDeviceInfoListResult: Index = %d DeviceCategory = %d DeviceConnectionType = %d DeviceConnectionStatus = %d SelectDeviceAuto = %d",
                         i8Index, itr->getEnDeviceCategory(), itr->getEnDeviceConnectionType(), itr->getEnDeviceConnectionStatus(), itr->getBSelectedDevice()));
         setDeviceConnectionType(itr->getEnDeviceConnectionType());
         ETG_TRACE_USR4(("onGetDeviceInfoListResult : BTDeviceAddress : %s ", itr->getSzBTAddress().c_str()));
         ETG_TRACE_USR4(("onGetDeviceInfoListResult : DeviceName : %s ", itr->getSzDeviceName().c_str()));
         ETG_TRACE_USR4(("onGetDeviceInfoListResult : DeviceModelName: %s ", itr->getSzDeviceModelName().c_str()));
         ETG_TRACE_USR4(("onGetDeviceInfoListResult : DeviceManufacturerName %s ", itr->getSzDeviceManufacturerName().c_str()));
         bool dipoConnection = SpiUtils::dipoConnectionValidity(itr->getEnDeviceConnectionStatus());
         if ((true == dipoConnection))
         {
            _meActiveDeviceCategory = itr->getEnDeviceCategory();
            vSetDeviceHandle(itr->getEnDeviceCategory(), itr->getU32DeviceHandle());
            ETG_TRACE_USR4(("onGetDeviceInfoListResult: DeviceCategory = %d DeviceHandle = %d DeviceConnectionStatus = %d,u32AllDevicehandle = %d ", itr->getEnDeviceCategory(), itr->getU32DeviceHandle(), itr->getEnDeviceConnectionStatus() , ALL_DEVICE_HANDLE));
            getDeviceUsagePreference(ALL_DEVICE_HANDLE, itr->getEnDeviceCategory());
            switch (getActiveDeviceCategory())
            {
               case ::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_DIPO :
               {
                  if ((true == _mDpHandler->getDpSpiCarPlayAutoLaunch() && (false == _mDpHandler->getDpSpiCarPlayAskOnConnect())) || (true == getUserSelection()))
                  {
                     setUserSelection(false);
                     sendSelectDevice(getActiveDeviceHandle(), getdeviceConnectionType(), DeviceConnectionReq__DEV_CONNECT, EnabledInfo__USAGE_ENABLED);
                  }
                  break;
               }
               case ::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO :
               {
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
                  if ((true == _mDpHandler->getDpAndroidAutoLaunch() && (false == _mDpHandler->getDpAndroidAutoAskOnConnect())) || (true == getUserSelection()))
                  {
                     setUserSelection(false);
                     sendSelectDevice(getActiveDeviceHandle(), getdeviceConnectionType(), DeviceConnectionReq__DEV_CONNECT, EnabledInfo__USAGE_ENABLED);
                  }
#endif
                  break;
               }
               default:
                  break;
            }
         }
      }
   }
}


/*
 * This function is called when the method call for SelectDevice fails
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to the SelectDeviceError
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onSelectDeviceError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SelectDeviceError >& /*error*/)
{
   ETG_TRACE_USR4(("onSelectDeviceError: Received.. "));
}


/*
 * Virtual function implemented to get the result of SelectDevice
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - result: Pointer to the GetDeviceInfoListResult
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onSelectDeviceResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SelectDeviceResult >& result)
{
   ETG_TRACE_USR4(("onSelectDeviceResult: Received.. "));
   if (proxy == _spiMidwServiceProxy)
   {
      ETG_TRACE_USR4(("onSelectDeviceResult: DeviceHandle = %d,Deviceresponsecode = %d", result->getDeviceHandle(), result->getResponseCode()));
      if (::midw_smartphoneint_fi_types::T_e8_ResponseCode__SUCCESS == result->getResponseCode())
      {
      }
      else if (::midw_smartphoneint_fi_types::T_e8_ResponseCode__FAILURE == result->getResponseCode())
      {
         ETG_TRACE_USR4(("onSelectDeviceResult: T_e8_ResponseCode__FAILURE Received.. ErrorType = %d", result->getErrorCode()));
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
         if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_DIPO == getActiveDeviceCategory())
         {
            POST_MSG((COURIER_MESSAGE_NEW(CarPlayAuthFailurePopUpReqMsg)()));
         }
         else if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO == getActiveDeviceCategory())
         {
            POST_MSG((COURIER_MESSAGE_NEW(AndroidAuthFailurePopUpReqMsg)()));
         }
#elif VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
         if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_DIPO == getActiveDeviceCategory())
         {
            POST_MSG((COURIER_MESSAGE_NEW(CarplayAdvisoryPopUpReqMsg)()));
         }
#endif
      }
      _mSpiConnectionNotifyer->handleSelectDeviceResult(result->getResponseCode());
   }
}


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
/**
 * This message is sent by the Gui when user wants to launch carplay
 * @param[in] - msg
 * @return    -bool
 */
bool SpiConnectionHandling::onCourierMessage(const DisclaimerActiveMsg& /*msg*/)
{
   (*_SpiDisclaimerActionState).mIsSpiDisclaimerActionTrue = 1;
   _SpiDisclaimerActionState.MarkItemModified(ItemKey::SpiDisclaimerAction::IsSpiDisclaimerActionTrueItem);
   _SpiDisclaimerActionState.SendUpdate();
   return true;
}


/**
 * This message is sent by the Gui when user does not want to launch carplay
 * @param[in] - msg
 * @return    -bool
 */
bool SpiConnectionHandling::onCourierMessage(const DisclaimerInactiveMsg& /*msg*/)
{
   (*_SpiDisclaimerActionState).mIsSpiDisclaimerActionTrue = 0;
   _SpiDisclaimerActionState.MarkItemModified(ItemKey::SpiDisclaimerAction::IsSpiDisclaimerActionTrueItem);
   _SpiDisclaimerActionState.SendUpdate();
   return true;
}


#endif
/**
 * This message is sent by the Gui when user want to Launch CarPlay.
 * @param[in] - msg
 * @return    -bool
 */
bool SpiConnectionHandling::onCourierMessage(const LaunchCarPlayReqMsg& /*msg*/)
{
   ETG_TRACE_USR4(("GUI->HALL: onCourierMessage: LaunchCarPlayReqMsg Received"));
   //For showing popup onDevice Info
   if (true == getSettingTrigDisc())
   {
      setSettingTrigDisc(false);
      if (false == _mbSessionStatus)
      {
         sendSelectDevice(getActiveDeviceHandle(), getdeviceConnectionType(), DeviceConnectionReq__DEV_CONNECT, EnabledInfo__USAGE_ENABLED);
      }
   }
   else
   {
      if (false == _mbSessionStatus)
      {
         ETG_TRACE_USR4(("onCourierMessage : ProjectionDeviceAuthorization called device handle = %d ", _mu32DIPODeviceHandle));
         setUserSelection(true);
         _spiMidwServiceProxy->sendProjectionDeviceAuthorizationSet(*this, _mu32DIPODeviceHandle, T_e8_UserAuthorizationStatus__USER_AUTH_AUTHORIZED);
      }
      else
      {
         ETG_TRACE_USR4(("onCourierMessage : vLaunchApp called  _mu32DIPODeviceHandle=%d eDeviceCategory =%d ", getActiveDeviceHandle(), getActiveDeviceCategory()));
         vLaunchApp(getActiveDeviceHandle(), (enDeviceCategory)getActiveDeviceCategory(), DiPOAppType__DIPO_NO_URL);
      }
   }
   return true;
}


/**
 * This message is sent by the Gui when user Press No.
 * @param[in] - msg
 * @return    -bool
 */
bool SpiConnectionHandling::onCourierMessage(const OnDisclaimerNoUpdMsg& /*msg*/)
{
   ETG_TRACE_USR4(("GUI->HALL: onCourierMessage: OnDisclaimerNoUpdMsg Received"));
   if (true == getSettingTrigDisc())
   {
      setSettingTrigDisc(false);
      sendSelectDevice(getActiveDeviceHandle(), getdeviceConnectionType(), DeviceConnectionReq__DEV_DISCONNECT, EnabledInfo__USAGE_DISABLED);
   }
   else
   {
      setUserSelection(false);
      _spiMidwServiceProxy->sendProjectionDeviceAuthorizationSet(*this, _mu32DIPODeviceHandle, T_e8_UserAuthorizationStatus__USER_AUTH_UNAUTHORIZED);
   }
   return true;
}


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
/**
 * This message is sent by the Gui when user want to Launch Android auto.
 * @param[in] - msg
 * @return    -bool
 */
bool SpiConnectionHandling::onCourierMessage(const LaunchAndroidAutoReqMsg& /*msg*/)
{
   ETG_TRACE_USR4(("GUI->HALL: onCourierMessage: LaunchAndroidAutoReqMsg Received"));

   if (true == getSettingTrigDisc())
   {
      setSettingTrigDisc(false);
      if (false == _mbSessionStatus)
      {
         sendSelectDevice(getActiveDeviceHandle(), getdeviceConnectionType(), DeviceConnectionReq__DEV_CONNECT, EnabledInfo__USAGE_ENABLED);
      }
   }
   else
   {
      if (false == _mbSessionStatus)
      {
         ETG_TRACE_USR4(("onCourierMessage : ProjectiondeviceAuthorization called device handle = %d ", _mu32AAPDeviceHandle));
         setUserSelection(true);
         _spiMidwServiceProxy->sendProjectionDeviceAuthorizationSet(*this, _mu32AAPDeviceHandle, T_e8_UserAuthorizationStatus__USER_AUTH_AUTHORIZED);
      }
      else
      {
         ETG_TRACE_USR4(("onCourierMessage : vLaunchApp called  _mu32DIPODeviceHandle=%d eDeviceCategory =%d ", getActiveDeviceHandle(), getActiveDeviceCategory()));
         vLaunchApp(getActiveDeviceHandle(), (enDeviceCategory)getActiveDeviceCategory(), DiPOAppType__DIPO_NO_URL);
      }
   }
   return true;
}


/**
 * This message is sent by the Gui when user don`t want to Launch Android auto.
 * @param[in] - msg
 * @return    -bool
 */
bool SpiConnectionHandling::onCourierMessage(const onAndroidDisclaimerNoUpdMsg&  /*msg*/)
{
   ETG_TRACE_USR4(("GUI->HALL: onCourierMessage: onAndroidDisclaimerNoUpdMsg Received"));
   if (true == getSettingTrigDisc())
   {
      setSettingTrigDisc(false);
      sendSelectDevice(getActiveDeviceHandle(), getdeviceConnectionType(), DeviceConnectionReq__DEV_DISCONNECT, EnabledInfo__USAGE_DISABLED);
   }
   else
   {
      setUserSelection(false);
      _spiMidwServiceProxy->sendProjectionDeviceAuthorizationSet(*this, _mu32AAPDeviceHandle, T_e8_UserAuthorizationStatus__USER_AUTH_UNAUTHORIZED);
   }
   return true;
}


#endif

/*
 * This function is called when the method call for LaunchApp fails
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to the LaunchAppError
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onLaunchAppError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::LaunchAppError >& /*error*/)
{
   ETG_TRACE_USR4(("onLaunchAppError: Received.."));
}


/*
 * Virtual function implemented to get the result of LaunchApp
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - result: Pointer to the LaunchAppResult
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onLaunchAppResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::LaunchAppResult >& result)
{
   ETG_TRACE_USR4(("MIDW->HALL onLaunchAppResult: Received..: eResponseCode=%d,eErrorType=%d,u32DeviceHandle=%d", result->getResponseCode(), result->getErrorCode(), result->getDeviceHandle()));

   if (::midw_smartphoneint_fi_types::T_e8_ResponseCode__FAILURE == result->getResponseCode())
   {
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_DIPO == getActiveDeviceCategory())
      {
         POST_MSG((COURIER_MESSAGE_NEW(CarplayStartFailPopUpReqMsg)()));
      }
      else if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO == getActiveDeviceCategory())
      {
         POST_MSG((COURIER_MESSAGE_NEW(AndroidAutoStartFailPopUpReqMsg)()));
      }
#elif VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
      if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_DIPO == getActiveDeviceCategory())
      {
         POST_MSG((COURIER_MESSAGE_NEW(CarplayAdvisoryPopUpReqMsg)()));
      }
#endif
   }
}


/*
 * This function is called when the method call for SendTouchEvent fails
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to the SendTouchEventError
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onSendTouchEventError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SendTouchEventError >& /*error*/)
{
   ETG_TRACE_USR4(("onSendTouchEventError: Received.."));
}


/*
 * Virtual function implemented to get the result of SendTouchEvent
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - result: Pointer to the SendTouchEventResult
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onSendTouchEventResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SendTouchEventResult >& /*result*/)
{
   ETG_TRACE_USR4(("onSendTouchEventResult..Received"));
}


/*
 * Virtual function implemented to get the property update of onSessionStatusInfoStatus
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - status: Pointer to the SessionStatusInfoStatus
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onSessionStatusInfoStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SessionStatusInfoStatus >& status)
{
   ETG_TRACE_USR4(("MIDW->HALL onSessionStatusInfoStatus..Received"));
   if (proxy == _spiMidwServiceProxy)
   {
      ::midw_smartphoneint_fi_types::T_e8_SessionStatus eSessionStatus = status->getSessionStatus();
      ETG_TRACE_USR4(("MIDW->HALL onSessionStatusInfoStatus eSessionStatus=%d  ", eSessionStatus));
      ::midw_smartphoneint_fi_types::T_e8_DeviceCategory eDeviceCategory = T_e8_DeviceCategory__DEV_TYPE_UNKNOWN;
      switch (eSessionStatus)
      {
         case ::midw_smartphoneint_fi_types::T_e8_SessionStatus__SESSION_ACTIVE :
         {
            ETG_TRACE_USR4(("eSessionStatus SESSION_ACTIVE"));
            eDeviceCategory = status->getDeviceCategory();
            vHandleSessionActiveStatus(eDeviceCategory, status->getDeviceHandle());
            break;
         }
         case ::midw_smartphoneint_fi_types::T_e8_SessionStatus__SESSION_INACTIVE :
         {
            ETG_TRACE_USR4(("eSessionStatus SESSION_INACTIVE"));
            vHandleContextResponse();
            eDeviceCategory = status->getDeviceCategory();
            vHandleSessionInActiveStatus(eDeviceCategory);
            break;
         }
         case ::midw_smartphoneint_fi_types::T_e8_SessionStatus__SESSION_ERROR:
         {
            ETG_TRACE_USR4(("MIDW->HALL onSessionStatusInfoStatus Session Error Received"));
            vHandleContextResponse();
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
            if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_DIPO == getActiveDeviceCategory())
            {
               POST_MSG((COURIER_MESSAGE_NEW(CarplayStartFailPopUpReqMsg)()));
            }
            else if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO == getActiveDeviceCategory())
            {
               POST_MSG((COURIER_MESSAGE_NEW(AndroidAutoStartFailPopUpReqMsg)()));
            }
#elif VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
            if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_DIPO == getActiveDeviceCategory())
            {
               POST_MSG((COURIER_MESSAGE_NEW(CarplayAdvisoryPopUpReqMsg)()));
               //! Make Carplay icon Invisible on session error
               vUpdateCarplayIconStatus(false);
            }
#endif
            break;
         }
         default:
            break;
      }//end Switch
   }
}


/*
 * This function is called when the property update for onSessionStatusInfo fails
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to the SessionStatusInfo
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onSessionStatusInfoError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >&/* proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SessionStatusInfoError >& /*error*/)
{
   ETG_TRACE_USR4(("onSessionStatusInfoError: Received.."));
}


/*
 * Virtual function implemented to get the property update of onDeviceDisplayContextStatus
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - status: Pointer to the DeviceDisplayContextStatus
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onDeviceDisplayContextStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceDisplayContextStatus >& status)
{
   if (proxy == _spiMidwServiceProxy)
   {
      ETG_TRACE_USR4(("MIDW->HALL onDeviceDisplayContextStatus..Received : bDisplayFlag=%d, bhasDisplayFlag=%d" , status->getDisplayFlag(), status->hasDisplayFlag()));
      if (TRUE == status->getDisplayFlag())
      {
         vSetSpiVideoActive(true);
         //!If RVC is active set display flag, to update accessory display context.
         if (_mbDisplayFlagStatus == false)           //Check to avoid multiple context switch requests
         {
            _mbDisplayFlagStatus = true;
            vCreateSpiTouchSurface();
            ETG_TRACE_USR4(("SpiConnectionHandling: onDeviceDisplayContextStatus _mbDisplayFlagStatus :%d DisplayContext = %d", _mbDisplayFlagStatus, status->getDisplayContext()));
            //!Do a Self Context to launch
            POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(MEDIA_SPI_CONTEXT_CARPLAY_VIDEO_ACTIVE, 0 , APPID_APPHMI_MEDIA)));
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
            switch (getActiveDeviceCategory())
            {
               case T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO:
               {
                  POST_MSG((COURIER_MESSAGE_NEW(AAPStartingPopUpClosingReqMsg)()));
                  break;
               }
               case T_e8_DeviceCategory__DEV_TYPE_DIPO:
               {
                  POST_MSG((COURIER_MESSAGE_NEW(CarplayStartingPopUpClosingReqMsg)()));
                  break;
               }
               default:
                  break;
            }
#endif
            ETG_TRACE_USR4(("HALL->GUI ContextSwitchOutReqMsg Posted TargetId:%d, TargetContextId:%d, SourceContextId:%d", \
                            MEDIA_SPI_CONTEXT_CARPLAY_VIDEO_ACTIVE, MEDIA_CONTEXT_START, APPID_APPHMI_MEDIA));
         }
      }
      else if (FALSE == status->getDisplayFlag())
      {
         if (_mbDisplayFlagStatus == true)
         {
            _mbDisplayFlagStatus = false;
            vDestroySpiTouchSurface();
            vHandleContextResponse();
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
            ETG_TRACE_USR4(("HALL->GUI: SpiVideoExitUpdMsg Posted"));
            POST_MSG((COURIER_MESSAGE_NEW(SpiVideoExitUpdMsg)()));
#endif
         }
      }
   }
}


/*
 * This function is called when the property update for DeviceDisplayContex fails
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to the SessionStatusInfo
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onDeviceDisplayContextError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceDisplayContextError >& /*error*/)
{
   ETG_TRACE_USR4(("MIDW->HALL onDeviceDisplayContextError..Received"));
}


/*
 * Virtual function implemented to get the property update of onDeviceAudioContextStatus
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - status: Pointer to the DeviceDisplayContextStatus
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onDeviceAudioContextStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceAudioContextStatus >& status)
{
   if (proxy == _spiMidwServiceProxy)
   {
      ETG_TRACE_USR4(("MIDW->HALL onDeviceAudioContextStatus..Received : bAudioFlag=%d audiocontext=%d", status->getAudioFlag(),  status->getAudioContext()));
   }
}


/*
 * This function is called when the property update for onDeviceAudioContextError fails
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to the SessionStatusInfo
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onDeviceAudioContextError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DeviceAudioContextError >& /*error*/)
{
   ETG_TRACE_USR4(("spns MIDW->HALL onDeviceAudioContextError..Received"));
}


/*
 * This function is called when the method call for GetDeviceUsagePreference fails
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to the GetDeviceUsagePreferenceError
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onGetDeviceUsagePreferenceError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetDeviceUsagePreferenceError >& /*error*/)
{
   ETG_TRACE_USR4(("spns MIDW->HALL onGetDeviceUsagePreferenceError..Received"));
}


/*
 * This function is called to Get the result of method call for GetDeviceUsagePreference
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - result: Pointer to the GetDeviceUsagePreferenceresult
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onGetDeviceUsagePreferenceResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::GetDeviceUsagePreferenceResult >& result)
{
   ETG_TRACE_USR4(("spns MIDW->HALL onGetDeviceUsagePreferenceResult...Received"));
   if (proxy == _spiMidwServiceProxy)
   {
      ::midw_smartphoneint_fi_types::T_e8_EnabledInfo deviceEnableInfo = result->getEnabledInfo();
      _meActiveDeviceCategory = result->getDeviceCategory();
      ETG_TRACE_USR4(("spns MIDW->HALL onGetDeviceUsagePreferenceResult deviceEnableInfo = %d Device Category = %d DeviceConnectionType = %d", deviceEnableInfo, result->getDeviceCategory(), getdeviceConnectionType()));
   }
}


/**
 * sendAvailableContextList()- Handle to Add/Update the SPI context List when available
 * This function Updates Context Availability when the Carplay Session is active.
 * @param[in] None
 * @param[out] None
 * @return void
 */

void SpiConnectionHandling::sendAvailableContextList(::midw_smartphoneint_fi_types::T_e8_DeviceCategory eDeviceCategory)
{
   ETG_TRACE_USR4(("sendAvailableContextList: Received...eDeviceCategory =%d", eDeviceCategory));
   if (NULL != mptr_ContextProvider)
   {
      switch (eDeviceCategory)
      {
         case T_e8_DeviceCategory__DEV_TYPE_DIPO :
         {
            mptr_ContextProvider->vAddAvailableContext(MEDIA_SPI_CONTEXT_CARPLAY_DEVICE_CONNECTED, TEMPORARY);
            mptr_ContextProvider->vAddAvailableContext(MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MOBILEPHONE);
            mptr_ContextProvider->vAddAvailableContext(MEDIA_SPI_CONTEXT_CARPLAY_DIPO_SIRI_BUTTONUP);
            mptr_ContextProvider->vAddAvailableContext(MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MUSIC);
            mptr_ContextProvider->vAddAvailableContext(MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MUSIC_NO_VIDEO);
            ETG_TRACE_USR4(("spnc SpiConnectionHandling:CarPlay SESSION_ACTIVE updated AvailableContextList added %d %d %d %d %d Successfully", \
                            MEDIA_SPI_CONTEXT_CARPLAY_DEVICE_CONNECTED, MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MOBILEPHONE, MEDIA_SPI_CONTEXT_CARPLAY_DIPO_SIRI_BUTTONUP, MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MUSIC, MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MUSIC_NO_VIDEO));
            //! Make Carplay icon visible on context availabale
            vUpdateCarplayIconStatus(true);
            MediaDatabinding::getInstance().setSpiDIPOSourceAvailability(true); //To update the SPI availability to Media Source Handling
            break;
         }
         case T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO :
         {
            ETG_TRACE_USR4(("sendAvailableContextList: DeviceCategory__DEV_TYPE_ANDROIDAUTO"));
            mptr_ContextProvider->vAddAvailableContext(MEDIA_SPI_CONTEXT_ANDROID_AUTO_MUSIC_VIDEO);
            mptr_ContextProvider->vAddAvailableContext(MEDIA_SPI_CONTEXT_ANDROID_AUTO_MUSIC_NO_VIDEO);
            mptr_ContextProvider->vAddAvailableContext(MEDIA_SPI_CONTEXT_ANDROID_AUTO_MOBILEPHONE);
            mptr_ContextProvider->vAddAvailableContext(MEDIA_SPI_CONTEXT_ANDROID_DEVICE_CONNECTED);
            ETG_TRACE_USR4(("SpiConnectionHandling:ANDROIDAUTO SESSION_ACTIVE updated AvailableContextList added %d %d %d %d Successfully", \
                            MEDIA_SPI_CONTEXT_ANDROID_AUTO_MUSIC_VIDEO, MEDIA_SPI_CONTEXT_ANDROID_AUTO_MUSIC_NO_VIDEO, MEDIA_SPI_CONTEXT_ANDROID_AUTO_MOBILEPHONE, MEDIA_SPI_CONTEXT_ANDROID_DEVICE_CONNECTED));
            MediaDatabinding::getInstance().setSpiAAPSourceAvailability(true); //To update the SPI availability to Media Source Handling
            break;
         }
         default :
         {
            break;
         }
      }
   }
   else
   {
      ETG_TRACE_USR4((" mptr_ContextProvider is NULL"));
   }
}


/**
 * removeUnavailableContext() - Handle to Remove/Update the SPI context List when unavailable
 * This function Updates Context Availability when the Carplay Session is Inactive.
 * @param[in] None
 * @param[out] None
 * @return void
 */

void SpiConnectionHandling::removeUnavailableContext(::midw_smartphoneint_fi_types::T_e8_DeviceCategory eDeviceCategory)
{
   ETG_TRACE_USR4(("removeUnavailableContext: Received.."));
   if (NULL != mptr_ContextProvider)
   {
      switch (eDeviceCategory)
      {
         case T_e8_DeviceCategory__DEV_TYPE_DIPO :
         {
            mptr_ContextProvider->vRemoveContext(MEDIA_SPI_CONTEXT_CARPLAY_DEVICE_CONNECTED);
            mptr_ContextProvider->vRemoveContext(MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MOBILEPHONE);
            mptr_ContextProvider->vRemoveContext(MEDIA_SPI_CONTEXT_CARPLAY_DIPO_SIRI_BUTTONUP);
            mptr_ContextProvider->vRemoveContext(MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MUSIC);
            mptr_ContextProvider->vRemoveContext(MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MUSIC_NO_VIDEO);
            ETG_TRACE_USR4(("SpiConnectionHandling:CarPlay SESSION_INACTIVE updated AvailableContextList. Removed %d %d %d %d %d Successfully", \
                            MEDIA_SPI_CONTEXT_CARPLAY_DEVICE_CONNECTED, MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MOBILEPHONE, MEDIA_SPI_CONTEXT_CARPLAY_DIPO_SIRI_BUTTONUP, MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MUSIC, MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MUSIC_NO_VIDEO));
            MediaDatabinding::getInstance().setSpiDIPOSourceAvailability(false); //To update the SPI availability to Media Source Handling
            break;
         }
         case T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO :
         {
            ETG_TRACE_USR4(("RemoveAvailableContextList: DeviceCategory__DEV_TYPE_ANDROIDAUTO"));
            mptr_ContextProvider->vRemoveContext(MEDIA_SPI_CONTEXT_ANDROID_AUTO_MUSIC_VIDEO);
            mptr_ContextProvider->vRemoveContext(MEDIA_SPI_CONTEXT_ANDROID_AUTO_MUSIC_NO_VIDEO);
            mptr_ContextProvider->vRemoveContext(MEDIA_SPI_CONTEXT_ANDROID_AUTO_MOBILEPHONE);
            mptr_ContextProvider->vRemoveContext(MEDIA_SPI_CONTEXT_ANDROID_DEVICE_CONNECTED);
            ETG_TRACE_USR4(("SpiConnectionHandling:ANDROIDAUTO SESSION_INACTIVE updated AvailableContextList Removed %d %d %d %d Successfully", \
                            MEDIA_SPI_CONTEXT_ANDROID_AUTO_MUSIC_VIDEO, MEDIA_SPI_CONTEXT_ANDROID_AUTO_MUSIC_NO_VIDEO, MEDIA_SPI_CONTEXT_ANDROID_AUTO_MOBILEPHONE, MEDIA_SPI_CONTEXT_ANDROID_DEVICE_CONNECTED));
            MediaDatabinding::getInstance().setSpiAAPSourceAvailability(false); //To update the SPI availability to Media Source Handling
            break;
         }
         default :
         {
            break;
         }
      }
   }
   else
   {
      ETG_TRACE_USR4((" mptr_ContextProvider is NULL"));
   }
}


/**
 * sendSelectDevice - helper function to send the sendSelectDevice device to AID.
 * @param[in] : u32devicehandle
 * @param[in] : i8DeviceConnectionType
 * @param[in] : eDeviceConnectionRequest
 * @param[in] : enEnabledInfo eEnableInfo
 * @return void
 */
void SpiConnectionHandling::sendSelectDevice(uint32 u32devicehandle, int8 i8DeviceConnectionType , enDeviceConnectionReq eDeviceConnectionRequest, enEnabledInfo eEnableInfo)
{
   ETG_TRACE_USR4(("hall->midw sendSelectDevice POST sendSelectDeviceStart u32devicehandle=%d i8DeviceConnectionType =%d eDeviceConnectionRequest = %d eEnableInfo =%d", u32devicehandle, i8DeviceConnectionType, eDeviceConnectionRequest, eEnableInfo));
   _spiMidwServiceProxy->sendSelectDeviceStart(*this , u32devicehandle, (::midw_smartphoneint_fi_types::T_e8_DeviceConnectionType)i8DeviceConnectionType, (::midw_smartphoneint_fi_types::T_e8_DeviceConnectionReq)eDeviceConnectionRequest, (::midw_smartphoneint_fi_types::T_e8_EnabledInfo)eEnableInfo, ::midw_smartphoneint_fi_types::T_e8_EnabledInfo__USAGE_DISABLED, getActiveDeviceCategory());
}


/**
 * onActiveSourceUpdate - Notification from HMI master when  active source List  update is Unsuccessful
 * @param[in] proxy, update
 * @return void
 */
void SpiConnectionHandling::onActiveSourceListError(const ::boost::shared_ptr<AudioSourceChangeProxy>& /*proxy*/ , const ::boost::shared_ptr<ActiveSourceListError>& /*error*/)
{
   ETG_TRACE_USR4(("spn onActiveSourceListError Received"));
}


/**
 * updateIfRequiredSourceIsActive - updates if the audio source is present in ActiveSourceList or not
 * @param[in] uint32
 * @return bool
 */
bool SpiConnectionHandling::isSourceAvailableInActiveList(uint32 checksource)
{
   ETG_TRACE_USR4(("isSourceAvailableInActiveList  Source :%d", checksource));
   bool isSourceActive = false;
   int count = 0;

   ::std::vector<sourceData>::const_iterator iter = SourceList.begin();
   for (; iter != SourceList.end(); iter++)
   {
      ETG_TRACE_USR4(("isSourceAvailableInActiveList count:%d source:%d", count, iter->getSrcId()));
      count++;

      if (checksource == iter->getSrcId())
      {
         isSourceActive = true;
         ETG_TRACE_USR4(("isSourceAvailableInActiveList  isSourceActive:%u", isSourceActive));
         break;
      }
   }
   ETG_TRACE_USR4(("isSourceAvailableInActiveList isSourceActive:%u", isSourceActive));
   return isSourceActive;
}


/* isNativeBTSourceActive - helper function to check if Native BT phone is active in sourcelist
* @param[in] void
* @return bool
*/
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
bool SpiConnectionHandling::isNativeBTSourceActive()
{
   ::std::vector<sourceData>::const_iterator iter = SourceList.begin();
   for (; iter != SourceList.end(); iter++)
   {
      switch (iter->getSrcId())
      {
         case SRC_PHONE :
         case SRC_PHONE_OUTBAND_RING:
         case SRC_PHONE_INBAND_RING:
         {
            ETG_TRACE_USR4(("isNativeBTSourceActive Src: %d", iter->getSrcId()));
            return true;
         }
         default:
            break;
      }
   }
   return false;
}


#endif

/**
 * vUpdateSMSourceInfo - updates SM gaurd expression based on current active source
 * @param[in] void
 * @return void
 */
void SpiConnectionHandling::vUpdateSMSourceInfo()
{
   vSpiPhoneSourceStatus();
   vSpiAlertSourceStatus();
   vSpiMediaSourceStatus();
}


/**
 * vFindAccessoryAudioContext - helper function to set the audio flag and context based on current active audio source
 * @param[in] bool and eAudioContext
 * @return void
 */
void SpiConnectionHandling::vFindAccessoryAudioContext(bool& audioflag, enAudioContext& eAudioContext)
{
   ETG_TRACE_USR4(("vFindAccessoryAudioContext audioflag :%u eAudioContext:%u", audioflag, eAudioContext));

   switch (_mCurrentActiveAudioSource)
   {
      case SRC_SPI_ENTERTAIN:
      case SRC_SPI_PHONE:
      case SRC_SPI_SPEECHRECOGNITION:
      case SRC_SPI_ALERT:
      {
         audioflag = false;
         eAudioContext = AudioContext__SPI_AUDIO_MAIN;
         break;
      }
      case SRC_TUNER_TA_FM:
      case SRC_TUNER_TA_DAB:
      {
         audioflag = true;
         eAudioContext = AudioContext__SPI_AUDIO_TRAFFIC;
         break;
      }
      case SRC_PHONE_INBAND_RING:
      case SRC_PHONE:
      case SRC_PHONE_OUTBAND_RING:
      {
         audioflag = true;
         eAudioContext = AudioContext__SPI_AUDIO_PHONE;
         break;
      }
      case SRC_TCU_ECALL:
      {
         audioflag = true;
         eAudioContext = AudioContext__SPI_AUDIO_EMER_PHONE;
         break;
      }
      default:
      {
         ETG_TRACE_USR4(("set default audio context"));
         break;
      }
   }
}


/**
 * vUpdateAudioAccessoryContext - function to check if source is changed and update the audio context
 * @param[in] proxy, update
 * @return void
 */
void SpiConnectionHandling::vUpdateAudioAccessoryContext()
{
   bool audioflag = TRUE;
   enAudioContext eAudioContext = AudioContext__SPI_AUDIO_MAIN;
   bool sourcechange = vIsAudioSourceChange(_mCurrentActiveAudioSource);
   ETG_TRACE_USR4(("vUpdateAudioAccessoryContext _mCurrentActiveAudioSource :%d", _mCurrentActiveAudioSource));
   if (TRUE == sourcechange)
   {
      if (TRUE == bIsAudioUnBorrowRequired())
      {
         //unborrow previous audio source
         vSetAccessoryAudioContext(FALSE, _meCurrentAudioContext);
      }
      //now send the actual audio context
      vFindAccessoryAudioContext(audioflag, eAudioContext);
      _meCurrentAudioContext = eAudioContext;
      ETG_TRACE_USR4(("new  :%d", _meCurrentAudioContext));
      vSetAccessoryAudioContext(audioflag, eAudioContext);
   }
}


/**
 * bIsAudioUnBorrowRequired - Helper function to check if previous audio context was  any of the borrow source
 * @param[in] enAudioContext
 * @return bool
 */
bool SpiConnectionHandling::bIsAudioUnBorrowRequired()
{
   ETG_TRACE_USR4(("old _meCurrentAudioContext :%d", _meCurrentAudioContext));
   switch (_meCurrentAudioContext)
   {
      case AudioContext__SPI_AUDIO_SPEECH_REC:
      case AudioContext__SPI_AUDIO_ADVISOR_PHONE:
      case AudioContext__SPI_AUDIO_EMER_PHONE:
      case AudioContext__SPI_AUDIO_PHONE:
      case AudioContext__SPI_AUDIO_INCOM_TONE:
      case AudioContext__SPI_AUDIO_TRAFFIC:
      case AudioContext__SPI_AUDIO_EMERGENCY_MSG:
      case AudioContext__SPI_AUDIO_SYNC_MSG:
      {
         return TRUE;
      }
      default:
         return FALSE;
   }
}


/* * onActiveSourceListSignal - Notification from HMI master when  active source List  update(For MIx source too)
 * @param[in] proxy, update
 * @return void
 */
void SpiConnectionHandling::onActiveSourceListSignal(const ::boost::shared_ptr< AudioSourceChangeProxy >& proxy, const ::boost::shared_ptr< ActiveSourceListSignal >& sourceListSignal)
{
   ETG_TRACE_USR4(("spn onActiveSourceListSignal Received from HMI-MASTER"));
   if (proxy == _audioSourceChangeSPIProxy)
   {
      SourceList = sourceListSignal->getActiveSources();
      vUpdateCurrentAudioSource();
      vUpdateSMSourceInfo();
      vUpdateAudioAccessoryContext();
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      vCheckAndLaunchAAPPhone();
#endif
   }
}


/*
 * This function is called when the property update for SendKeyEvent fails
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to the SendKeyEvent
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onSendKeyEventError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SendKeyEventError >&/*error*/)
{
   if (proxy == _spiMidwServiceProxy)
   {
      ETG_TRACE_USR4(("SpiKeyHandler:onSendKeyEventError Received"));
   }
}


/*
 * This function is called to the result of SendKeyEvent.
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to the SendKeyEvent
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onSendKeyEventResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SendKeyEventResult >& result)
{
   if (proxy == _spiMidwServiceProxy)
   {
      ETG_TRACE_USR4(("SpiKeyHandler:onSendKeyEventResult Received ResponseCode=%d, ErrorCode = %d", result->getResponseCode(), result->getErrorCode()));
   }
}


/**
 * OnActiveApplicationUpdate - CONTEXT SWITCH OBSERVER to get the APPID from HMI maste
 * @param[in] applicationId
 * @param[out] None
 * @return void
 */
bool SpiConnectionHandling::onCourierMessage(const onAppStateUpdMsg& state)
{
   ETG_TRACE_USR4(("SpiConnectionHandling: onAppStateUpdMsg = %d,_mCurrentActiveAudioSource=%d",  state.GetAppState(), _mCurrentActiveAudioSource));
   if (hmibase::IN_BACKGROUND == state.GetAppState())
   {
      ETG_TRACE_USR4(("spn sendAccessoryDisplayContextStart IS post ..."));
      bool displayflag = TRUE;
      enDisplayContext eDisplayContext = DISPLAY_CONTEXT_UNKNOWN;
      if (HMI_MODE_OFF == getCurrentHMIState())
      {
         eDisplayContext = DISPLAY_CONTEXT_EMERGENCY;
         vSetAccessoryAudioContext(true, AudioContext__SPI_AUDIO_SYNC_MSG);
      }
      setAccessoryDisplayContext(displayflag, eDisplayContext);
   }
   return true;
}


bool SpiConnectionHandling::onCourierMessage(const onHMISubStateONUpdateMsg& /*msg*/)
{
   setAccessoryDisplayContext(false, DISPLAY_CONTEXT_EMERGENCY);
   vSetAccessoryAudioContext(false, AudioContext__SPI_AUDIO_SYNC_MSG);
   return true;
}


/**
 * vIsAudioSourceChange - helper function to check if AudioSource changed
 * @param[in] -u32SourceId
 * @parm[out] -None
 * @return    -bool
 */
bool SpiConnectionHandling::vIsAudioSourceChange(tU32 u32SourceId)
{
   bool bIsAudioSourceChange = false;

   if ((u32SourceId != _mcurrentsource) && (u32SourceId != SRC_SPI_INFO))
   {
      _mcurrentsource = u32SourceId;
      ETG_TRACE_USR4(("vIsAudioSourceChange spn Source change spn onActiveSourceUpdate _mcurrentsource =%d ", _mcurrentsource));
      bIsAudioSourceChange = true;
   }
   return bIsAudioSourceChange;
}


/**
 *isAccessoryAudioContext - helper function to check the AccessoryContext
 * @param[in] -u32SourceId
 * @parm[out] -None
 * @return    -bool
 */
bool SpiConnectionHandling::isAccessoryAudioContext(tU32 u32SourceId)
{
   if ((u32SourceId >= SRC_SPI_ENTERTAIN) && (u32SourceId <= SRC_SPI_ALERT))
   {
      ETG_TRACE_USR4(("spn isAccessoryAudioContext: sourceId is SPI"));
      return FALSE;
   }
   else
   {
      return TRUE;
   }
}


/**
 *vSetAccessoryAudioContext - helper function to Set the AccessoryAudioContext
 * @param[in] -u32SourceId
 * @parm[out] -None
 * @return    -void
 */
void SpiConnectionHandling::vSetAccessoryAudioContext(bool bAudioflag, enAudioContext eAudioContext)
{
   ETG_TRACE_USR4(("spn sendAccessoryAudioContextStart IS post ...audioflag = %d audioContext = %d", bAudioflag, eAudioContext));
   _spiMidwServiceProxy->sendAccessoryAudioContextStart(*this, getActiveDeviceHandle() , bAudioflag, (::midw_smartphoneint_fi_types::T_e8_AudioContext)eAudioContext);
}


/**
 * vUpdateCurrentAudioSource - helper function To return current Audio source.
 * @param[in] none
 * @return void
 */

void SpiConnectionHandling::vUpdateCurrentAudioSource()
{
   ::std::vector<sourceData>::const_iterator iter = SourceList.begin(); // stack top is list bottom
   while (iter != SourceList.end())
   {
      _mCurrentActiveAudioSource = iter->getSrcId();
      iter++;
   }
   ETG_TRACE_USR4(("vUpdateCurrentAudioSource:Current audio source=%d", _mCurrentActiveAudioSource));
}


/**
 * SetDeviceHandle - helper function To Set Device Handle.
 * @param[in] : eDeviceCategory
 * @param[in] : u32DeviceHandle
 * @return : void
 */
void SpiConnectionHandling::vSetDeviceHandle(::midw_smartphoneint_fi_types::T_e8_DeviceCategory eDeviceCategory, uint32 u32DeviceHandle)
{
   ETG_TRACE_USR4(("vSetDeviceHandle Enter :: eDeviceCategory=%d u32DeviceHandle= %d", eDeviceCategory, u32DeviceHandle));
   if (T_e8_DeviceCategory__DEV_TYPE_DIPO == eDeviceCategory)
   {
      _mu32DIPODeviceHandle = u32DeviceHandle;
      _mbCarPlayConnStatus = true;
   }
   else if (T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO == eDeviceCategory)
   {
      _mu32AAPDeviceHandle = u32DeviceHandle;
      _mbAAPConnStatus = true;
   }
   else
   {
   }
}


bool SpiConnectionHandling::isDipoDeviceConnected()
{
   return _mbCarPlayConnStatus;
}


bool SpiConnectionHandling::isAndroidDeviceConnected()
{
   return _mbAAPConnStatus;
}


/**
 * getActiveDeviceHandle - helper function To return Active session Device Handle.
 * @param[in] none
 * @return int32
 */

int32 SpiConnectionHandling::getActiveDeviceHandle()
{
   ETG_TRACE_USR4(("getActiveDeviceHandle: _meActiveDeviceCategory=%d _mu32AAPDeviceHandle = %d _mu32DIPODeviceHandle = %d", _meActiveDeviceCategory , _mu32AAPDeviceHandle , _mu32DIPODeviceHandle));
   switch (_meActiveDeviceCategory)
   {
      case ::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_DIPO :
      {
         return _mu32DIPODeviceHandle;
         break;
      }
      case ::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO :
      {
         return _mu32AAPDeviceHandle;
         break;
      }
      default:
      {
         return 0;
         break;
      }
   }
}


/**
 * OnLaunchCarplaywithAppReqMsg - this message is sent by GUI for Launching Carplay with different App/context.
 * @param[in] oMsg
 * @return bool
 */

bool SpiConnectionHandling::onCourierMessage(const OnLaunchCarplaywithAppReqMsg& oMsg /*msg*/)
{
   ETG_TRACE_USR4(("GUI->HALL: onCourierMessage OnLaunchCarplaywithAppReqMsg Received with LaunchAppType: %d", oMsg.GetLaunchAppType()));
   enDeviceCategory eDeviceCategory = DeviceCategory__DEV_TYPE_DIPO;
   vLaunchApp(getActiveDeviceHandle(), eDeviceCategory, (enDiPOAppType)oMsg.GetLaunchAppType());
   return true;
}


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
/**
 * OnLaunchAndroidAutowithAppReqMsg - this message is sent by GUI for Launching AAP with different App/context.
 * @param[in] oMsg
 * @return bool
 */

bool SpiConnectionHandling::onCourierMessage(const OnLaunchAndroidAutowithAppReqMsg& oMsg /*msg*/)
{
   ETG_TRACE_USR4(("GUI->HALL: onCourierMessage OnLaunchAndroidAutowithAppReqMsg Received with LaunchAppType: %d", oMsg.GetLaunchAppType()));
   enDeviceCategory eDeviceCategory = DeviceCategory__DEV_TYPE_ANDROIDAUTO;
   vLaunchApp(getActiveDeviceHandle(), eDeviceCategory, (enDiPOAppType)oMsg.GetLaunchAppType());
   return true;
}


bool SpiConnectionHandling::onCourierMessage(const OnLaunchSPIWithAppReqMsg&  oMsg/*msg*/)
{
   ETG_TRACE_USR4(("GUI->HALL: onCourierMessage OnLaunchSPIWithAppReqMsg Received with LaunchAppType: %d eDeviceCategory =%d", oMsg.GetLaunchAppType(), getActiveDeviceCategory()));
   vLaunchApp(getActiveDeviceHandle(), (enDeviceCategory)getActiveDeviceCategory(), (enDiPOAppType)oMsg.GetLaunchAppType());
   return true;
}


#endif

/**
 * vLaunchApp - helper function To send method start Launch APP.
 * @param[in] edipoAppType
 * @return int32
 */
void SpiConnectionHandling::vLaunchApp(uint32 u32Devicehandle, enDeviceCategory eDeviceCategory, enDiPOAppType eDipoAppType)
{
   if (getCurrentHMIState() != HMI_MODE_OFF)//NOT_OFF
   {
      ETG_TRACE_USR4(("SPI vLaunchApp:sendLaunchAppStart post :u32Devicehandle = %d eDipoAppType = %d  eDeviceCategory =%d", u32Devicehandle, ETG_CENUM(enDiPOAppType, eDipoAppType), eDeviceCategory));
      _spiMidwServiceProxy->sendLaunchAppStart(*this, u32Devicehandle, (::midw_smartphoneint_fi_types::T_e8_DeviceCategory)eDeviceCategory, DEFAULT_APP_HANDLE , (::midw_smartphoneint_fi_types::T_e8_DiPOAppType)eDipoAppType, "", ::midw_smartphoneint_fi_types::T_e8_EcnrSetting__ECNR_NOCHANGE);
   }
   else
   {
      ETG_TRACE_USR4(("vLaunchApp: CurrentHMIState is not ON"));
   }
}


/**
 * sendAccessoryDisplayContextMethodStart- helper function to call the method start through Ttfis
 * @param[in] -displayflag
 * @param[in] -displayContext
 * @parm[out] -None
 * @return    -Void
 */
void SpiConnectionHandling::setAccessoryDisplayContext(bool displayflag, enDisplayContext displayContext)
{
   //! if RVC is not active
   if (false == getRVCStatus())
   {
      ETG_TRACE_USR4(("hall->midw SendAccessoryDisplayContextMethodStart displayflag = %d displayContext = %d", displayflag, displayContext));
      _spiMidwServiceProxy->sendAccessoryDisplayContextStart(*this, getActiveDeviceHandle(), displayflag, (::midw_smartphoneint_fi_types::T_e8_DisplayContext)displayContext);
   }
   else
   {
      ETG_TRACE_USR4(("setAccessoryDisplayContext: RVC is active"));
   }
}


/**
 * onCourierMessage - This message is received when HK_AUX/CD is pressed.to update accesssory display context to carplay.
 * @param[in] -
 * @parm[out] -None
 * @return    -bool
 */

bool SpiConnectionHandling::onCourierMessage(const onHK_AUX_CDEventMsg& /*msg*/)
{
   //! don't send Context Back request when on Carplay and HK_AUX press
   vSetSpiVideoActive(false);
   POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(DUMMY_SWITCH_ID, CONTEXT_TRANSITION_COMPLETE)));
   ETG_TRACE_USR4(("GUI->HALL:SpiConnectionHandling: onCourierMessage onHK_AUX_CDEventMsg, send DisplayContext to MIDW"));
   bool displayflag = TRUE;
   enDisplayContext eDisplayContext = DISPLAY_CONTEXT_UNKNOWN;
   setAccessoryDisplayContext(displayflag, eDisplayContext);
   vSetAccessoryAudioContext(true, AudioContext__SPI_AUDIO_MAIN);
   return true;
}


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2

/**
 * This CourierMessage will be triggered on toggle of DontRemindButton in carPlay DisclaimerPopup
 * @param[in] - None
 * @param[out] - None
 */
bool SpiConnectionHandling::onCourierMessage(const DontRemindButtonReqMsg& /*msg*/)
{
   (*_IsDontRemindBtnStatus).misDontRemindButtonEnable = (*_IsDontRemindBtnStatus).misDontRemindButtonEnable ? false : true;
   _IsDontRemindBtnStatus.SendUpdate(true);
   _IsDontRemindBtnStatus.MarkItemModified(ItemKey::DontRemindButtonStatus::isDontRemindButtonEnableItem);

   //Below Lines of code is to update the Datagaurd variable for StateMachine
   (*_isDisclaimerRemindBtnStatus).misRemindBtnEnabled = (*_IsDontRemindBtnStatus).misDontRemindButtonEnable;
   _isDisclaimerRemindBtnStatus.SendUpdate(true);
   _isDisclaimerRemindBtnStatus.MarkItemModified(ItemKey::DisclaimerRemindBtnStatus::isRemindBtnEnabledItem);
   return true;
}


/**
 * This CourierMessage will be triggered on toggle of DontRemindButton in Android DisclaimerPopup
 * @param[in] - None
 * @param[out] - None
 */

bool SpiConnectionHandling::onCourierMessage(const AndroidDontRemindButtonReqMsg& /*msg*/)
{
   (*_IsAndroidDontRemindBtnStatus).misAndroidAutoDontRemindButtonEnable = (*_IsAndroidDontRemindBtnStatus).misAndroidAutoDontRemindButtonEnable ? false : true;
   _IsAndroidDontRemindBtnStatus.SendUpdate(true);
   _IsAndroidDontRemindBtnStatus.MarkItemModified(ItemKey::AndroidAutoDontRemindButtonStatus::isAndroidAutoDontRemindButtonEnableItem);

   //Below Lines of code is to update the Datagaurd variable for StateMachine
   (*_isDisclaimerRemindBtnStatus).misRemindBtnEnabled = (*_IsAndroidDontRemindBtnStatus).misAndroidAutoDontRemindButtonEnable;
   _isDisclaimerRemindBtnStatus.SendUpdate(true);
   _isDisclaimerRemindBtnStatus.MarkItemModified(ItemKey::DisclaimerRemindBtnStatus::isRemindBtnEnabledItem);
   return true;
}


/**
 * updateDontRemindBtnStatus:: To reset the Don't remindButton value to False.
 * @param[in] - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::updateDontRemindBtnStatus()
{
   //Below DataBinding to Update the DataGaurd in StateMachine
   if (::midw_smartphoneint_fi_types::T_e8_DeviceType__APPLE_DEVICE == getConnDeviceType())
   {
      (*_IsDontRemindBtnStatus).misDontRemindButtonEnable = false;
      _IsDontRemindBtnStatus.SendUpdate(true);
      _IsDontRemindBtnStatus.MarkItemModified(ItemKey::DontRemindButtonStatus::isDontRemindButtonEnableItem);
   }
   else if (::midw_smartphoneint_fi_types::T_e8_DeviceType__ANDROID_DEVICE == getConnDeviceType())
   {
      (*_IsAndroidDontRemindBtnStatus).misAndroidAutoDontRemindButtonEnable = false;
      _IsAndroidDontRemindBtnStatus.SendUpdate(true);
      _IsAndroidDontRemindBtnStatus.MarkItemModified(ItemKey::AndroidAutoDontRemindButtonStatus::isAndroidAutoDontRemindButtonEnableItem);
   }
   (*_isDisclaimerRemindBtnStatus).misRemindBtnEnabled = false;
   _isDisclaimerRemindBtnStatus.SendUpdate(true);
   _isDisclaimerRemindBtnStatus.MarkItemModified(ItemKey::DisclaimerRemindBtnStatus::isRemindBtnEnabledItem);
}


#endif

void SpiConnectionHandling::onBluetoothDeviceStatusStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::BluetoothDeviceStatusStatus >& status)
{
   ETG_TRACE_USR4(("SpiConnectionHandling: onBluetoothDeviceStatusStatus Received"));
   if (proxy == _spiMidwServiceProxy)
   {
      _mu32BTDeviceHandle = status->getBluetoothDeviceHandle();
      _mu32PrjDeviceHandle = status->getProjectionDeviceHandle();
      mapBTChangeInfo(status->getBTChangeInfo());
      ETG_TRACE_USR4(("onBluetoothDeviceStatusStatus _mu32BTDeviceHandle=%d,_mu32PrjDeviceHandle=%d", status->getBluetoothDeviceHandle(), status->getProjectionDeviceHandle()));
      ETG_TRACE_USR4(("onBluetoothDeviceStatusStatus _mbBTCallAcitveStatus=%d,_meBTChangeInfo=%d, SameDevice=%d", status->getCallActiveStatus(), status->getBTChangeInfo(), status->getSameDevice()));
      if ((status->getSameDevice()) || (PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE != getBtCallStatus()))
      {
         _spiMidwServiceProxy->sendInvokeBluetoothDeviceActionStart(*this, status->getBluetoothDeviceHandle(), status->getProjectionDeviceHandle(), _meBTChangeInfo);
      }
      else
      {
         _mbIsBtDeactivationReq = true;
      }
   }
}


void SpiConnectionHandling::onBluetoothDeviceStatusError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::BluetoothDeviceStatusError >& /*error*/)
{
   ETG_TRACE_USR4(("SpiConnectionHandling: onBluetoothDeviceStatusError Received"));
}


void SpiConnectionHandling:: setVehicleConfiguration(enVehicle_Configuration eVehicleConfig , bool bSetconfig)
{
   ETG_TRACE_USR4(("sendsetVehicleConfigurationStart eVehicleConfig=%d,bSetconfig=%d ", eVehicleConfig, bSetconfig));
   _spiMidwServiceProxy->sendSetVehicleConfigurationStart(*this, (::midw_smartphoneint_fi_types::T_e8_Vehicle_Configuration)eVehicleConfig, bSetconfig);
}


/*
 * This function is a function to send SetVehicle Movement method start to middleware
 * @param[in]  - parkbreakinfo
 * @param[in] - vehiclestate
 * @return     - Void
 */
void SpiConnectionHandling::setVehicleMovementState(::midw_smartphoneint_fi_types::T_e8_ParkBrake parkbreakinfo , ::midw_smartphoneint_fi_types::T_e8_VehicleState vehiclestate)
{
   ETG_TRACE_USR4(("HALL->MIDW :sendSetVehicleMovementStateStart ParkBrakeInfo = %d VehicleState = %d " , ETG_CENUM(enVehicle_HandBrakeState, parkbreakinfo), ETG_CENUM(enVehicle_Configuration,  vehiclestate)));
   if (_spiMidwServiceProxy)
   {
      if (_meHandBrakeState != parkbreakinfo)
      {
         _meHandBrakeState = parkbreakinfo;
      }

      if (_meVehicleState != vehiclestate)
      {
         _meVehicleState = vehiclestate;
      }

      _spiMidwServiceProxy->sendSetVehicleMovementStateStart(*this, parkbreakinfo, ::midw_smartphoneint_fi_types::T_e8_GearState__GEAR_PARK, vehiclestate);
   }
}


/*
 * This function is called when method result for SetVehicleMovementState received with error
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to SetVehicleMovementState
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onSetVehicleMovementStateError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetVehicleMovementStateError >& /*error*/)
{
   ETG_TRACE_USR4(("Spi VehicleDataHandler onSetVehicleMovementStateError Received ..."));
}


/*
 * This function is called when method result for SetVehicleMovementState received
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - result: Pointer to SetVehicleMovementState
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onSetVehicleMovementStateResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >&/*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetVehicleMovementStateResult >& result)
{
   ETG_TRACE_USR4(("Spi VehicleDataHandler :: onSetVehicleMovementStateResult Received response code = %d error code = %d " , result->getResponseCode(), result->getErrorCode()));
}


/**
 * setRVCStatus - helper function to set RVC status
 * @param[in] -bRVCStatus
 * @parm[out] -None
 * @return    -Void
 */

void SpiConnectionHandling::setRVCStatus(bool bRVCStatus)
{
   if (true == bRVCStatus)
   {
      _mbRVCStatus = true;
   }
   else
   {
      _mbRVCStatus = false;
   }
}


/**
 * getRVCStatus - helper function to get RVC status
 * @param[in] -none
 * @parm[out] -None
 * @return    -bool
 */

bool SpiConnectionHandling::getRVCStatus()
{
   return _mbRVCStatus;
}


/**
 * getDeviceUsagePreference - helper function to Check device Usage preference
 * @param[in] -u32DeviceHandle
 * @parm[in] -eDeviceCategory
 * @return    -void
 */
void SpiConnectionHandling::getDeviceUsagePreference(uint32 u32DeviceHandle, ::midw_smartphoneint_fi_types::T_e8_DeviceCategory eDeviceCategory)
{
   ETG_TRACE_USR4(("getDeviceUsagePreference :: sendGetDeviceUsagePreferenceStart POST u32DeviceHandle = %d,eDeviceCategory = %u", u32DeviceHandle, eDeviceCategory));
   _spiMidwServiceProxy->sendGetDeviceUsagePreferenceStart(*this, u32DeviceHandle, eDeviceCategory);
}


/**
 * setDeviceConnectionType - helper function to setDeviceConnectionType
 * @param[in] -deviceConnectionType
 * @return    -void
 */
void SpiConnectionHandling::setDeviceConnectionType(int8 deviceConnectionType)
{
   _mi8DeviceConnectionType = deviceConnectionType;
}


/**
 * setDeviceConnectionType - helper function to getdeviceConnectionType
 * @param[in] -deviceConnectionType
 * @return   _mi8DeviceConnectionType
 */
int8 SpiConnectionHandling:: getdeviceConnectionType()
{
   return _mi8DeviceConnectionType;
}


/*
 * This function is a helper function to send KeyEvent method start to MW for HK_NEXT/PREV.
 * @param[in]  - uint32 u32MapCode,u32MapMode.
 * @param[out] - None
 * @return     - Void
 */

void SpiConnectionHandling::vSendKeyEvent(uint32 u32KeyMode, uint32 u32KeyCode)
{
   ETG_TRACE_USR4(("vSendKeyEvent is called with  KeyCode = %d KeyMode = %d",  u32KeyCode, u32KeyMode));
   bool sendKeyEvent = true;
   switch (u32KeyCode)
   {
      case ::midw_smartphoneint_fi_types::T_e32_KeyCode__MULTIMEDIA_NEXT:
      case ::midw_smartphoneint_fi_types::T_e32_KeyCode__MULTIMEDIA_PREVIOUS :
      {
         //! Send Next-Prev only for SPI_ENTERTAIN
         if (false == isSourceAvailableInActiveList(SRC_SPI_ENTERTAIN))
         {
            sendKeyEvent = false;
         }
         break;
      }
      default :
         break;
   }
   if (true == sendKeyEvent)
   {
      ETG_TRACE_USR4(("SpiKey:Key events mapped from HALL->MIDW: KeyMode = %d KeyCode = %d", u32KeyMode, u32KeyCode));
      _spiMidwServiceProxy->sendSendKeyEventStart(*this, getActiveDeviceHandle(), (midw_smartphoneint_fi_types::T_e8_KeyMode) u32KeyMode, (::midw_smartphoneint_fi_types::T_e32_KeyCode) u32KeyCode);
   }
}


/*
 * This function is a helper function to send RotaryEncoderEvent method start to MW for Main Encoder.
 * @param[in]  - int8 i8EncoderCount.
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::vSendRotaryEncoderEvent(int8 i8EncoderCount)
{
   ETG_TRACE_USR4(("SpiKey:vSendRotaryEncoderEvent called  EncCount = %d", i8EncoderCount));
   _spiMidwServiceProxy->sendRotaryControllerEventStart(*this, getActiveDeviceHandle(), i8EncoderCount);
}


/*
 * This function is a helper function to setStartUpParam.
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::infoSteeringWheelPosition()
{
   ETG_TRACE_USR4(("SpiConnectionHandling: setStartUpParam Enter ........."));
   uint8 steeringWheelPosition = UNDEFINE_VALUE;
   enVehicle_Configuration eVehicleConfig = Vehicle_Configuration__LEFT_HAND_DRIVE;
   bool bSetconfig = true;
   if (DP_S32_NO_ERR == DP_s32GetConfigItem("VehicleInformation", "SteeringPosition", &steeringWheelPosition, 1))
   {
      ETG_TRACE_USR4(("SpiConnectionHandling::steeringWheelPosition:%d", steeringWheelPosition));
   }
   if (LHD == steeringWheelPosition)
   {
      eVehicleConfig = Vehicle_Configuration__LEFT_HAND_DRIVE;
   }
   else if (RHD == steeringWheelPosition)
   {
      eVehicleConfig = Vehicle_Configuration__RIGHT_HAND_DRIVE;
   }
   ETG_TRACE_USR4(("SpiConnectionHandling: setVehicleConfiguration  called VehicleConfig = %d setconfig = %d", ETG_CENUM(enVehicle_Configuration, eVehicleConfig), bSetconfig));
   setVehicleConfiguration(eVehicleConfig, bSetconfig);
}


/*
 * This function is a helper function to call the method sendAccessoryAppStateStart
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::setAccessoryAppState(enSpeechAppState eSpeechAppState , enPhoneAppState ePhoneAppState, enNavigationAppState eNavigationAppState)
{
   ETG_TRACE_USR4(("SpiConnectionHandling: sendAccessoryAppStateStart  called eSpeechAppState = %d ePhoneAppState = %d eNavigationAppState = %d", ETG_CENUM(enSpeechAppState, eSpeechAppState), ETG_CENUM(enPhoneAppState, ePhoneAppState), ETG_CENUM(enNavigationAppState, eNavigationAppState)));
   _spiMidwServiceProxy->sendAccessoryAppStateStart(*this, (::midw_smartphoneint_fi_types::T_e8_SpeechAppState)eSpeechAppState, (::midw_smartphoneint_fi_types::T_e8_PhoneAppState)ePhoneAppState, (::midw_smartphoneint_fi_types::T_e8_NavigationAppState) eNavigationAppState);
}


/*
 * This function is called to check BT call status before showing the popup
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::checkBTCallNShowDiscPopUp()
{
   ETG_TRACE_USR4(("SpiConnectionHandling: checkBTCallNShowDiscPopUp  BTCallstatus = %d ", ETG_CENUM(enPhoneAppState, getBtCallStatus())));
   if (PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE == getBtCallStatus())
   {
      setIsDeviceAuthorizationRequ(true);
   }
   else
   {
      vSpiDeviceConnectionPopUp(getConnDeviceType());
      setIsDeviceAuthorizationRequ(false);
   }
}


/*
 * This function is called to check BT call status before sendingSelectdevice
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::checkBTCallNSendSelectDevice()
{
   if ((PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE != getBtCallStatus()))
   {
      sendSelectDevice(getActiveDeviceHandle(), getdeviceConnectionType(), DeviceConnectionReq__DEV_CONNECT, EnabledInfo__USAGE_ENABLED);
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_DIPO == getActiveDeviceCategory())
      {
         POST_MSG((COURIER_MESSAGE_NEW(CarplayStartingPopUpReqMsg)()));
      }
      else if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO == getActiveDeviceCategory())
      {
         POST_MSG((COURIER_MESSAGE_NEW(AndroidAutoStartingPopUpReqMsg)()));
      }
#endif
   }
   else
   {
      setIsDeviceSelectionRequ(true);
   }
}


/*
 * This function is called to check and select Device when BT call is Not Active
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::checkAndSelectDeviceifReq()
{
   if (true == getIsDeviceSelectionRequ())
   {
      sendSelectDevice(getActiveDeviceHandle(), getdeviceConnectionType(), DeviceConnectionReq__DEV_CONNECT, EnabledInfo__USAGE_ENABLED);
      setIsDeviceSelectionRequ(false);
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_DIPO == getActiveDeviceCategory())
      {
         POST_MSG((COURIER_MESSAGE_NEW(CarplayStartingPopUpReqMsg)()));
      }
      else if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO == getActiveDeviceCategory())
      {
         POST_MSG((COURIER_MESSAGE_NEW(AndroidAutoStartingPopUpReqMsg)()));
      }
#endif
   }
}


/*
 * This function is called to check and showpopup when BT call is Not Active
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::checkAndShowDisclaimerPopUpifRequ()
{
   if (true == getIsDeviceAuthorizationRequ())
   {
      vSpiDeviceConnectionPopUp(getConnDeviceType());
      setIsDeviceAuthorizationRequ(false);
   }
}


/*
 * This function receives the notifications from appstatehandler fro BT-Phone call status
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::vOnBTCallStatusUpdate(enPhoneAppState eBTCallstatus)
{
   ETG_TRACE_USR4(("SpiConnectionHandling: vOnBTCallStatusUpdate  eBTCallstatus = %d ", ETG_CENUM(enPhoneAppState, eBTCallstatus)));
   setBtCallStatus(eBTCallstatus);
   if (PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE != eBTCallstatus)
   {
      checkAndSelectDeviceifReq();
      checkAndShowDisclaimerPopUpifRequ();
      if (_mbIsBtDeactivationReq)
      {
         _spiMidwServiceProxy->sendInvokeBluetoothDeviceActionStart(*this, _mu32BTDeviceHandle, _mu32PrjDeviceHandle, _meBTChangeInfo);
         _mbIsBtDeactivationReq = false;
      }
   }
}


/*
 * This is a helper function to perform CheckAndLaunchAAPPhone when phone source becomes avialble in activeaudiosourcelist
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
void SpiConnectionHandling::vCheckAndLaunchAAPPhone()
{
   if (isNativeBTSourceActive() && (PhoneAppState__SPI_APP_STATE_PHONE_ACTIVE == getBtCallStatus()) && _mbSessionStatus && (T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO == _meActiveDeviceCategory))
   {
      ETG_TRACE_USR4(("SpiConnectionHandling: vHandleLaunchAApfromAPP   called..."));
      vLaunchApp(getActiveDeviceHandle(), (enDeviceCategory)getActiveDeviceCategory(), DiPOAppType__DIPO_MOBILEPHONE);
   }
}


#endif

/*
 * This is a helper function to perform SESSIONACTIVE operation from function onSessionStatusInfoStatus
 * @param[in]  - DeviceCatergory and DeviceHandle
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::vHandleSessionActiveStatus(::midw_smartphoneint_fi_types::T_e8_DeviceCategory eDeviceCategory, uint32 u32DeviceHandle)
{
   if (false == _mbSessionStatus)
   {
      vSetDeviceHandle(eDeviceCategory, u32DeviceHandle);
      ETG_TRACE_USR4(("sendAvailableContextList: Called..eDeviceCategory =%d", eDeviceCategory));
      sendAvailableContextList(eDeviceCategory);
      _mbSessionStatus = true ;
      _meActiveDeviceCategory = eDeviceCategory;
      ETG_TRACE_USR4(("onCourierMessage LaunchAndroidAutoReqMsg : vLaunchApp called  DeviceHandle =%d _meActiveDeviceCategory =%d ", getActiveDeviceHandle(), _meActiveDeviceCategory));
      vLaunchApp(getActiveDeviceHandle(), (enDeviceCategory)eDeviceCategory, DiPOAppType__DIPO_NO_URL);
      vUpdateSpiDeviceSessionStatus(true, eDeviceCategory);
   }
}


/*
 * This is a helper function to perform SESSIONACTIVE operation from function onSessionStatusInfoStatus
 * @param[in]  - DeviceCatergory and DeviceHandle
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::vHandleSessionInActiveStatus(::midw_smartphoneint_fi_types::T_e8_DeviceCategory eDeviceCategory)
{
   _mbSessionStatus = false;
   ETG_TRACE_USR4(("removeUnavailableContext: Called..eDeviceCategory = %d", eDeviceCategory));
   removeUnavailableContext(eDeviceCategory);
   vUpdateSpiDeviceSessionStatus(false, eDeviceCategory);
   ETG_TRACE_USR4(("onSessionStatusInfoStatus:eDeviceCategory =%d", eDeviceCategory));
   if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_DIPO == eDeviceCategory)
   {
      //! Make Carplay icon Invisible on session inactive
      vUpdateCarplayIconStatus(false);
   }
}


/*
 * This function is called when the property update for ApplicationMediaMetaData fails
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to the ApplicationMediaMetaData
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onApplicationMediaMetaDataError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::ApplicationMediaMetaDataError >& /*error*/)
{
}


/*
 * This function is called when the property update for ApplicationMediaMetaData status
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - status: Pointer to the ApplicationMediaMetaData
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onApplicationMediaMetaDataStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::ApplicationMediaMetaDataStatus >& status)
{
   ::midw_smartphoneint_fi_types::T_ApplicationMediaMetaData  ApplicationMediaMetaData = status->getApplicationMediaMetaData();
   _mStrAlbumName  = ApplicationMediaMetaData.getAlbum();
   _mStrArtistName    = ApplicationMediaMetaData.getArtist();
   _mStrTitleName     = ApplicationMediaMetaData.getTitle();
   _mTrackNumber    = ApplicationMediaMetaData.getTrackNumber();
   _mTotalTrackCount = ApplicationMediaMetaData.getAlbumTrackCount();
   _mAppDeviceName = ApplicationMediaMetaData.getAppName();
   if (isSourceAvailableInActiveList(SRC_SPI_ENTERTAIN))
   {
      ETG_TRACE_USR4(("Currently Source is SRC_SPI_ENTERTAIN"));
      MediaDatabinding::getInstance().updateMediaMetadataInfo(_mStrArtistName, _mStrArtistName, _mStrTitleName);
      MediaDatabinding::getInstance().updateTrackNumber(_mTrackNumber, _mTotalTrackCount);
      setHeaderTextData(_mAppDeviceName.c_str());
   }
   ETG_TRACE_USR4(("onApplicationMediaMetaDataStatus  : ALBUM_NAME 					=		%s", _mStrAlbumName.c_str()));
   ETG_TRACE_USR4(("onApplicationMediaMetaDataStatus  : ARTIST_NAME 					= 		%s", _mStrArtistName.c_str()));
   ETG_TRACE_USR4(("onApplicationMediaMetaDataStatus  : TITLE_NAME 						= 		%s", _mStrTitleName.c_str()));
   ETG_TRACE_USR4(("onApplicationMediaMetaDataStatus  : CURRENT_TRACK_NUMBER = 	%d", _mTrackNumber));
   ETG_TRACE_USR4(("onApplicationMediaMetaDataStatus  : TOTAL_TRACK_COUNT 		= 		%d", _mTotalTrackCount));
   ETG_TRACE_USR4(("onApplicationMediaMetaDataStatus  : APP_DEVICE_NAME 				= 		%s", _mAppDeviceName.c_str()));
}


/*
 * This is a helper function to Destroy the Spi touch surface when Video rendering stop
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::vDestroySpiTouchSurface()
{
   ETG_TRACE_USR4((" SpiConnectionHandling::vDestroySpiSurface Enter"));
   ilm_surfaceSetVisibility(80, 0);
   ilm_commitChanges();
}


/*
 * This is a helper function to Create the Spi touch surface when Video rendering start
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: vCreateSpiTouchSurface()
{
   ETG_TRACE_USR4((" SpiConnectionHandling::vCreateSpiSurface Enter"));
   ilm_surfaceSetVisibility(80, 1);
   ilm_commitChanges();
}


/*
 * This function is called when method result for AccessoryAudioContext returns error
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to AccessoryAudioContext
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onAccessoryAudioContextError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::AccessoryAudioContextError >& /*error*/)
{
   ETG_TRACE_USR4(("SpiConnectionHandling: onAccessoryAudioContextError Received"));
}


/*
 * This function is called when method result for AccessoryAudioContext received
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - result: Pointer to AccessoryAudioContext
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onAccessoryAudioContextResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >&/*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::AccessoryAudioContextResult >& /*result*/)
{
   ETG_TRACE_USR4(("SpiConnectionHandling: onAccessoryAudioContextResult Received"));
}


/*
 * This function is called when method result for AccessoryDisplayContext received with error
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to AccessoryDisplayContext
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onAccessoryDisplayContextError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::AccessoryDisplayContextError >&/* error*/)
{
}


/*
 * This function is called when method result for AccessoryDisplayContext received
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - result: Pointer to AccessoryDisplayContext
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onAccessoryDisplayContextResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::AccessoryDisplayContextResult >& /*result*/)
{
}


/*
 * This function is called when method result for InvokeBluetoothDeviceAction received with error
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to InvokeBluetoothDeviceAction
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onInvokeBluetoothDeviceActionError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::InvokeBluetoothDeviceActionError >& /*error*/)
{
}


/*
 * This function is called when method result for InvokeBluetoothDeviceAction received
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - result: Pointer to InvokeBluetoothDeviceAction
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onInvokeBluetoothDeviceActionResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::InvokeBluetoothDeviceActionResult >& result)
{
   ETG_TRACE_USR4(("SpiConnectionHandling: onInvokeBluetoothDeviceActionResult Response code = %d Error code = %d ", result->getResponseCode(), result->getErrorCode()));
}


/*
 * This function is called when method result for SetVehicleConfiguration received with error
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to SetVehicleConfiguration
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onSetVehicleConfigurationError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetVehicleConfigurationError >& /*error*/)
{
}


/*
 * This function is called when method result for SetVehicleConfiguration received
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - result: Pointer to SetVehicleConfiguration
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onSetVehicleConfigurationResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetVehicleConfigurationResult >& /*result*/)
{
}


/*
 * This function is called when method result for SetFeatureRestrictions received with error
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to SetFeatureRestrictions
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onSetFeatureRestrictionsError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetFeatureRestrictionsError >& /*error*/)
{
}


/*
 * This function is called when method result for SetFeatureRestrictions received with error
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - result: Pointer to SetFeatureRestrictions
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onSetFeatureRestrictionsResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::SetFeatureRestrictionsResult >& /*result*/)
{
}


/*
 * This function is called when method result for RotaryControllerEvent received with error
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to SetFeatureRestrictions
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onRotaryControllerEventError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::RotaryControllerEventError >& /*error*/)
{
}


/*
 * This function is called when method result for RotaryControllerEvent received
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - result: Pointer to RotaryControllerEvent
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onRotaryControllerEventResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::RotaryControllerEventResult >& /*result*/)
{
}


/*
 * This function is called when method result for AccessoryAppState received with error
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error: Pointer to AccessoryAppState
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onAccessoryAppStateError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::AccessoryAppStateError >& /*error*/)
{
}


/*
 * This function is called when method result for AccessoryAppState received
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - result: Pointer to AccessoryAppState
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling:: onAccessoryAppStateResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::AccessoryAppStateResult >& /*result*/)
{
}


/*
 * This function is a helper function to send setFeatureRestrictions for CarPlay to middleware on startup
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::setFeatureRestrictionsDIPO()
{
   uint8 u8DIPODriveModeRestriction = (DIPO_SoftKeyboard | DIPO_NonMusicLists);
   ETG_TRACE_USR4(("HALL->MIDW :SetFeatureRestrictionsDipo DriveModeRestriction = %d", u8DIPODriveModeRestriction));
   ::midw_smartphoneint_fi_types::T_FeatureRestriction parkModeRestrictionInfo ;
   parkModeRestrictionInfo.setFeatureLockout(FEATURE_NOT_RESTRICTED);
   ::midw_smartphoneint_fi_types::T_FeatureRestriction driveModeRestrictionInfo ;
   driveModeRestrictionInfo.setFeatureLockout(u8DIPODriveModeRestriction);
   _spiMidwServiceProxy->sendSetFeatureRestrictionsStart(*this, ::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_DIPO, parkModeRestrictionInfo, driveModeRestrictionInfo);
}


/*
 * This function is a helper function to send setFeatureRestrictions for AndroidAuto  to middleware on startup
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::setFeatureRestrictionsAAP()
{
   uint8 u8AAPDriveModeRestriction = (AAP_VideoPlayback | AAP_TextInput | AAP_SetUpAndConfiguration);
   ETG_TRACE_USR4(("HALL->MIDW :SetFeatureRestrictionsAAP DriveModeRestriction = %d ", u8AAPDriveModeRestriction));
   ::midw_smartphoneint_fi_types::T_FeatureRestriction parkModeRestrictionInfo ;
   parkModeRestrictionInfo.setFeatureLockout(FEATURE_NOT_RESTRICTED);
   ::midw_smartphoneint_fi_types::T_FeatureRestriction driveModeRestrictionInfo ;
   driveModeRestrictionInfo.setFeatureLockout(u8AAPDriveModeRestriction);
   _spiMidwServiceProxy->sendSetFeatureRestrictionsStart(*this, ::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO, parkModeRestrictionInfo, driveModeRestrictionInfo);
}


/*
 * This is a helper function to read the Spi Current source as Phone and post msg to SM
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::vSpiPhoneSourceStatus()
{
   ETG_TRACE_USR4(("vSpiPhoneSourceStatus called"));
   if (isSourceAvailableInActiveList(SRC_SPI_PHONE))
   {
      ETG_TRACE_USR4(("vSpiPhoneSourceStatus:SPI Current active source is Phone"));
      POST_MSG((COURIER_MESSAGE_NEW(SpiPhoneSourceUpdMsg)(true)));
   }
   else
   {
      ETG_TRACE_USR4(("vSpiPhoneSourceStatus:SPI Current active source is not Phone"));
      POST_MSG((COURIER_MESSAGE_NEW(SpiPhoneSourceUpdMsg)(false)));
   }
}


/*
 * This is a helper function to sent Accessory info to AID during system statup
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::infoStartUpParm()
{
   infoSteeringWheelPosition();
   setFeatureRestrictionsDIPO();
   setFeatureRestrictionsAAP();
   setAccessoryDisplayContext(true, DISPLAY_CONTEXT_UNKNOWN);
   setAccessoryAppState(getAccSpeechAppStatus(), getBtCallStatus(), getAccNavAppStatus());
   setVehicleMovementState(_meHandBrakeState, _meVehicleState);
}


/*
 * This is a helper function to perform SESSIONACTIVE operation from function onSessionStatusInfoStatus
 * @param[in]  - DeviceCatergory and DeviceHandle
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::vSpiDeviceConnectionPopUp(::midw_smartphoneint_fi_types::T_e8_DeviceType deviceType)
{
   ETG_TRACE_USR4(("HALL->GUI vSpiDeviceConnectionPopUp Trigger DeviceType = %d ", ETG_CENUM(enDeviceType, deviceType)));
   switch (deviceType)
   {
      case ::midw_smartphoneint_fi_types::T_e8_DeviceType__APPLE_DEVICE :
      {
         uint32 autolaunch = _mDpHandler->getDpSpiCarPlayAutoLaunch();
         uint32 askOnConnect = _mDpHandler->getDpSpiCarPlayAskOnConnect();
         POST_MSG((COURIER_MESSAGE_NEW(DipoDeviceConnectedUpdMsg)(DUMMY_COURIER, askOnConnect, autolaunch)));
         break;
      }
      case ::midw_smartphoneint_fi_types::T_e8_DeviceType__ANDROID_DEVICE:
      {
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
         //! To be sync with the datapool status
         uint32 askOnConnAndroid = _mDpHandler->getDpAndroidAutoAskOnConnect();
         uint32 autoLaunchAndroid = _mDpHandler->getDpAndroidAutoLaunch();
         POST_MSG((COURIER_MESSAGE_NEW(AndroidDeviceConnectedUpdMsg)(DUMMY_COURIER, askOnConnAndroid, autoLaunchAndroid)));
#endif
         break;
      }
      default:
         break;
   }
}


/*
 * This is a helper function to getcurrentlyAciveDeviceCategory
 * @param[in]  -
 * @param[out] - None
 * @return     - Void
 */
::midw_smartphoneint_fi_types::T_e8_DeviceCategory SpiConnectionHandling::getActiveDeviceCategory()
{
   return _meActiveDeviceCategory;
}


/*
 * This is a helper function to set the BtCallStatus
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::setBtCallStatus(enPhoneAppState eBtCallStatus)
{
   _meBtCallStatus = eBtCallStatus;
}


/*
 * This is a helper function to return BtCallStatus
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
enPhoneAppState SpiConnectionHandling::getBtCallStatus()
{
   return _meBtCallStatus;
}


/*
 * This is a helper function to set the setAccSpeechAppStatus
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::setAccSpeechAppStatus(enSpeechAppState accSpeechAppStatus)
{
   _meAccessorySpeechAppState = accSpeechAppStatus;
}


/*
 * This is a helper function to set the setAccNavAppStatus
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::setAccNavAppStatus(enNavigationAppState accNavAppStatus)
{
   _meAccessoryNavAppState = accNavAppStatus;
}


/*
 * This is a helper function to return AccSpeechAppStatus
 * @param[in]  - None
 * @param[out] - enSpeechAppState
 * @return     - Void
 */
enSpeechAppState SpiConnectionHandling::getAccSpeechAppStatus()
{
   return _meAccessorySpeechAppState;
}


/*
 * This is a helper function to return AccNavAppStatus
 * @param[in]  - None
 * @param[out] - enNavigationAppState
 * @return     - Void
 */
enNavigationAppState SpiConnectionHandling::getAccNavAppStatus()
{
   return _meAccessoryNavAppState;
}


#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
/*
 * This is a helper function to return DIPO Device Session Status
 * @param[in]  - bool
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::vTriggerDIPOSessionUpdate(bool bDeviceStatus)
{
   ETG_TRACE_USR4(("vTriggerDIPOSessionUpdate: bDeviceSessionStatus: %d", bDeviceStatus));
   (*_isDIPOSessionActive).misDIPOActive = bDeviceStatus;
   _isDIPOSessionActive.SendUpdate(true);
   _isDIPOSessionActive.MarkItemModified(ItemKey::DIPOActiveStatus::isDIPOActiveItem);
   if (_isDIPOSessionActive.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_isDIPOSessionActive : Updation failed..!!!"));
   }
}


/*
 * This is a helper function to return AndroidAuto Device Session Status
 * @param[in]  - bool
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::vTriggerAAPSessionUpdate(bool bDeviceStatus)
{
   ETG_TRACE_USR4(("vTriggerAAPSessionUpdate: bDeviceSessionStatus: %d", bDeviceStatus));
   (*_isAAPSessionActive).misAndroidAutoActive = bDeviceStatus;
   _isAAPSessionActive.SendUpdate(true);
   _isAAPSessionActive.MarkItemModified(ItemKey::AndroidAutoActiveStatus::isAndroidAutoActiveItem);
   if (_isAAPSessionActive.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_isAAPSessionActive : Updation failed..!!!"));
   }
}


#endif

/*
 * This is a helper function to return Spi Device Session Status to register for the HK/SWC keys
 * @param[in]  - bool
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::vUpdateSpiDeviceSessionStatus(bool bDeviceSessionStatus, ::midw_smartphoneint_fi_types::T_e8_DeviceCategory eDeviceCategory)
{
   ETG_TRACE_USR4(("vUpdateSpiDeviceSessionStatus:SpiDeviceConnectedUpdMsg is updated with Session: %d device category : %d ", bDeviceSessionStatus , eDeviceCategory));
   POST_MSG((COURIER_MESSAGE_NEW(SpiDeviceConnectedUpdMsg)(bDeviceSessionStatus)));
   //!Update the Spi session status for dataguard in SM
   (*_SpiSessionStatus).mIsSpiSessionActive = bDeviceSessionStatus;
   _SpiSessionStatus.MarkAllItemsModified();
   if (_SpiSessionStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_SpiSessionStatusInfo : Updation failed..!!!"));
   }

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
   if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_DIPO == eDeviceCategory)
   {
      vTriggerDIPOSessionUpdate(bDeviceSessionStatus);
   }
   else if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO == eDeviceCategory)
   {
      vTriggerAAPSessionUpdate(bDeviceSessionStatus);
   }
#endif
}


/*
 * This is a helper function to return Spi Device Alert Source status
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */

void SpiConnectionHandling::vSpiAlertSourceStatus()
{
   ETG_TRACE_USR4(("vSpiAlertSourceStatus called"));
   if (isSourceAvailableInActiveList(SRC_SPI_ALERT))
   {
      ETG_TRACE_USR4(("vSpiAlertSourceStatus:SPI Alert source Status updated true"));
      vUpdateSpiAlertSourceStatus(true);
   }
   else
   {
      ETG_TRACE_USR4(("vSpiAlertSourceStatus:SPI Alert source Status updated false"));
      vUpdateSpiAlertSourceStatus(false);
   }
}


/*
 * This is a helper function to set Spi Device Alert Source status via Dataguard in SM
 * @param[in]  - Bool
 * @param[out] - None
 * @return     - Void
 */

void SpiConnectionHandling::vUpdateSpiAlertSourceStatus(bool bSpiAlertSourceStatus)
{
   ETG_TRACE_USR4(("vUpdateSpiAlertSourceStatus called"));
   (*_SpiAlertSourceState).mIsSpiAlertActiveSource = bSpiAlertSourceStatus;
   _SpiAlertSourceState.MarkAllItemsModified();

   if (_SpiAlertSourceState.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_SpiAlertSourceState : Updation failed..!!!"));
   }
}


void SpiConnectionHandling::setHeaderTextData(const Candera::String& headerTextData)
{
   (*_headerText).mText = headerTextData;
   _headerText.MarkItemModified(ItemKey::HeaderText::TextItem);
   _headerText.SendUpdate(true);
}


/*
 * This is a helper function to vHandleContextResponse
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::vHandleContextResponse()
{
   ETG_TRACE_USR4(("vHandleContextResponse _mSpiVideoActive=%d", _mSpiVideoActive));

   if (true == bGetIsSpiVideoActive())
   {
      POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(DUMMY_SWITCH_ID, CONTEXT_TRANSITION_BACK)));
      POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(DUMMY_SWITCH_ID, CONTEXT_TRANSITION_COMPLETE)));
      vSetSpiVideoActive(false);
#ifdef SPI_TEMPORARYCONTEXT_ONRVC_NCG3D_7788
      if ((true == bGetTemporaryContextStatusOnRVC()) && (false == bGetIsSpiVideoActive()))
      {
         ETG_TRACE_USR4(("Spi vHandleContextResponse::Temporary context requested in RVC"));
         POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchOutReqMsg)(MEDIA_SPI_CONTEXT_RVC_ON, 0 , APPID_APPHMI_MEDIA)));
         vSetTemporaryContextOnRVC(false);
      }
#endif
   }
}


/*
 * This is a helper function to return Spi Device Media/Entertainment Source status
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */

void SpiConnectionHandling::vSpiMediaSourceStatus()
{
   ETG_TRACE_USR4(("vSpiMediaSourceStatus called"));
   if (isSourceAvailableInActiveList(SRC_SPI_ENTERTAIN))
   {
      ETG_TRACE_USR4(("vSpiMediaSourceStatus:SPI Media source Status updated true"));
      vUpdateSpiMediaSourceStatus(true);
      MediaDatabinding::getInstance().updateMediaMetadataInfo(_mStrArtistName, _mStrArtistName, _mStrTitleName);
      MediaDatabinding::getInstance().updateTrackNumber(_mTrackNumber, _mTotalTrackCount);
      setHeaderTextData(_mAppDeviceName.c_str());
   }
   else
   {
      ETG_TRACE_USR4(("vSpiMediaSourceStatus:SPI Media source Status updated false"));
      vUpdateSpiMediaSourceStatus(false);
   }
}


/*
 * This is a helper function to set Spi Device Media/Entertainment Source status via Dataguard in SM
 * @param[in]  - Bool
 * @param[out] - None
 * @return     - Void
 */

void SpiConnectionHandling::vUpdateSpiMediaSourceStatus(bool bSpiMediaSourceStatus)
{
   ETG_TRACE_USR4(("vUpdateSpiMediaSourceStatus called"));
   (*_SpiMediaSourceStatus).mIsSpiMediaActiveSource = bSpiMediaSourceStatus;
   _SpiMediaSourceStatus.MarkAllItemsModified();

   if (_SpiMediaSourceStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("vUpdateSpiMediaSourceStatus : Updation failed..!!!"));
   }
}


/*
 * This is a helper function to onVolumeError
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onVolumeError(const ::boost::shared_ptr<MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy >& /*proxy*/, const ::boost::shared_ptr<MASTERAUDIOSERVICE_INTERFACE::SoundProperties:: VolumeError >& /*error*/)
{
   ETG_TRACE_USR4(("onVolumeError"));
}


/*
 * This is a helper function to onVolumeUpdate
 * @param[in]  - None
 * @param[out] - None
 * @return     - Void
 */
void SpiConnectionHandling::onVolumeUpdate(const ::boost::shared_ptr<MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy >& /*proxy*/, const ::boost::shared_ptr<MASTERAUDIOSERVICE_INTERFACE::SoundProperties:: VolumeUpdate >& update)
{
   ETG_TRACE_USR4(("onVolumeUpdate =%d", update->getVolume()));
   MediaDatabinding::getInstance().updateVolumeAvailability(update->getVolume());			//This is used to update the volume availablity in screen and for playback operations
   enAudioContext eAudioContext = AudioContext__SPI_AUDIO_SYNC_MSG;
   if (0 == update->getVolume())
   {
      if (FALSE == _bMuteState)
      {
         _bMuteState = TRUE;
         vSetAccessoryAudioContext(_bMuteState, eAudioContext);
      }
   }
   else
   {
      if (TRUE == _bMuteState)
      {
         _bMuteState = FALSE;
         vSetAccessoryAudioContext(_bMuteState, eAudioContext);
      }
   }
}


/**
 * onMuteStateError - This status is received from HMI_Master if there are any errors in muteState.
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */

void SpiConnectionHandling::onMuteStateError(const ::boost::shared_ptr< SoundPropertiesProxy >& /*proxy*/, const ::boost::shared_ptr< MuteStateError >& /*error*/)
{
   ETG_TRACE_USR4((" SpiConnectionHandling::onMuteStateError"));
}


/**
 * onMuteStateUpdate - This status is received from HMI_Master if there is any change in property mutestate.
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */

void SpiConnectionHandling::onMuteStateUpdate(const ::boost::shared_ptr< SoundPropertiesProxy >& /*proxy*/, const ::boost::shared_ptr< MuteStateUpdate >& update)
{
   ETG_TRACE_USR4((" SpiConnectionHandling::onMuteStateUpdate"));
   enAudioContext eAudioContext = AudioContext__SPI_AUDIO_SYNC_MSG;
   if (update->getMuteState())
   {
      if (FALSE == _bMuteState)
      {
         ETG_TRACE_USR4((" Audio-Mute State received"));
         _bMuteState = TRUE;
         vSetAccessoryAudioContext(_bMuteState, eAudioContext);
      }
   }
   else
   {
      ETG_TRACE_USR4((" Audio-UnMute State received"));
      _bMuteState = FALSE;
      vSetAccessoryAudioContext(_bMuteState, eAudioContext);
   }
}


void SpiConnectionHandling::setCurrentHMIState(uint8 u8CurrentHMIState)
{
   ETG_TRACE_USR4(("setCurrentHMIState u8CurrentHMIState =%d", u8CurrentHMIState));
   _mu8CurrentHMIState = u8CurrentHMIState ;
}


uint8 SpiConnectionHandling::getCurrentHMIState()
{
   ETG_TRACE_USR4(("getCurrentHMIState u8CurrentHMIState =%d", _mu8CurrentHMIState));
   return _mu8CurrentHMIState ;
}


void SpiConnectionHandling ::setIsDeviceAuthorizationRequ(bool IsDeviceAuthReq)
{
   _mbAuthorizationStatus = IsDeviceAuthReq;
}


bool SpiConnectionHandling ::getIsDeviceAuthorizationRequ()
{
   return _mbAuthorizationStatus;
}


void SpiConnectionHandling ::setIsDeviceSelectionRequ(bool isSelectDeviceReq)
{
   _mbDeviceSelectionReq = isSelectDeviceReq;
}


bool SpiConnectionHandling ::getIsDeviceSelectionRequ()
{
   return _mbDeviceSelectionReq;
}


void SpiConnectionHandling::setConnDeviceType(::midw_smartphoneint_fi_types::T_e8_DeviceType deviceType)
{
   _meConnDeviceType = deviceType;
}


::midw_smartphoneint_fi_types::T_e8_DeviceType SpiConnectionHandling::getConnDeviceType()
{
   return _meConnDeviceType;
}


void SpiConnectionHandling::setUserSelection(bool userSelection)
{
   _mUserSelection = userSelection;
}


bool SpiConnectionHandling::getUserSelection()
{
   return _mUserSelection;
}


void SpiConnectionHandling::setSettingTrigDisc(bool setTrigDisc)
{
   _mbIsSettTrigDisc = setTrigDisc;
}


bool SpiConnectionHandling::getSettingTrigDisc()
{
   return _mbIsSettTrigDisc;
}


/*
 * This is a helper function to set SpiVideo status when display becomes true .
 * @param[in]  - Bool
 * @param[out] - None
 * @return     - Void
 */

void SpiConnectionHandling::vSetSpiVideoActive(bool bSpiVideoStatus)
{
   _mSpiVideoActive = bSpiVideoStatus;
}


/*
 * This is a helper function to set Get Spi Video active status was true.
 * @param[in]  - void
 * @param[out] - None
 * @return     - Bool
 */

bool SpiConnectionHandling::bGetIsSpiVideoActive()
{
   return _mSpiVideoActive;
}


#ifdef SPI_TEMPORARYCONTEXT_ONRVC_NCG3D_7788
void SpiConnectionHandling::vSetTemporaryContextOnRVC(bool bRvcTemporaryContextStatus)
{
   ETG_TRACE_USR4((" vSetTemporaryContextOnRVC received : %d", bRvcTemporaryContextStatus));
   _mbSpiRvcTemporaryContext = bRvcTemporaryContextStatus;
}


bool SpiConnectionHandling::bGetTemporaryContextStatusOnRVC()
{
   ETG_TRACE_USR4((" bGetTemporaryContextStatusOnRVC : %d", _mbSpiRvcTemporaryContext));
   return _mbSpiRvcTemporaryContext;
}


void SpiConnectionHandling::vUpdateAppModeStatusOnRVCOff()
{
   if (NULL != mptr_ContextProvider)
   {
      if ((true == mptr_ContextProvider->bGetSetSpiRvcTemporaryContext()) && (false == bGetIsSpiVideoActive()))
      {
         POST_MSG((COURIER_MESSAGE_NEW(SetApplicationModeReqMsg)(APP_MODE_SOURCE)));
         POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchInResMsg)(DUMMY_SWITCH_ID, CONTEXT_TRANSITION_COMPLETE)));
      }
   }
}


#endif

int32 SpiConnectionHandling::getCurrentActiveAudioSource()
{
   return _mCurrentActiveAudioSource;
}


/*
 * This is a helper function to Update Carplay icon on media source scenes on device updates.
 * @param[in]  - bool
 * @param[out] - None
 * @return     - None
 */
void SpiConnectionHandling::vUpdateCarplayIconStatus(bool bCarplayIconVisibleStatus)
{
   (*_IsCarplayiconStatus).misCarplayiconVisible = bCarplayIconVisibleStatus;
   _IsCarplayiconStatus.SendUpdate(true);
}


void SpiConnectionHandling::mapBTChangeInfo(::midw_smartphoneint_fi_types::T_e8_BTChangeInfo btChangeInfo)
{
   switch (btChangeInfo)
   {
      case :: midw_smartphoneint_fi_types::T_e8_BTChangeInfo__SWITCH_DIPO_TO_BT:
      case ::	midw_smartphoneint_fi_types::T_e8_BTChangeInfo__NO_CHANGE:
      {
         _meBTChangeInfo = midw_smartphoneint_fi_types::T_e8_BTChangeInfo__NO_CHANGE;
         break;
      }
      default :
         _meBTChangeInfo = btChangeInfo;
         break;
   }
}


/////////////////////////////////////// TTFis TRACE COMMANDS //////////////////////////////////////////
ETG_I_CMD_DEFINE((TraceCmd_SpiConnectionPopUpTrigger, "SpiConnectionPopUpTrigger %u", uint8))  // Trace class declaration
/**
 * TraceCmd_SpiConnectionPopUpTrigger message - called when user fires APPHMI_MEDIA_SpiConnectionPopUpTrigger through TTFIs.
 * This message will post courier message based on the value of u8TriggerStatus.
 * @param[in] u8TriggerStatus
 * description for u8TriggerStatus:
 *    value 1- Display CarPlay PopUp
 *    value 0- Remove CarPlay PopUp
 * @return void
 */
void SpiConnectionHandling::TraceCmd_SpiConnectionPopUpTrigger(uint8 u8TriggerStatus)
{
   ETG_TRACE_USR4(("TraceCmd_SpiConnectionPopUpTrigger u8TriggerStatus=%d", u8TriggerStatus));

   if (1 == u8TriggerStatus)
   {
      if (pstaticSpiConnectionHandling != NULL)
      {
         pstaticSpiConnectionHandling->triggerCourierMsg(1);
      }
   }
   if (0 == u8TriggerStatus)
   {
      if (pstaticSpiConnectionHandling != NULL)
      {
         pstaticSpiConnectionHandling->triggerCourierMsg(2);
      }
   }

   if (2 == u8TriggerStatus)
   {
      if (pstaticSpiConnectionHandling != NULL)
      {
         ETG_TRACE_USR4(("TraceCmd_SpiConnectionPopUpTrigger: Post android "));
         pstaticSpiConnectionHandling->triggerCourierMsg(3);
      }
   }
}


/*
 * This function is implemented to trigger courier message through TTfis command
 * @param[in]  - msgid to sent the courier message
 * description for msgid:
 * value 1- sent the courier message 1
 * value 2- sent the courier message 2
 * @return     - Void
 */
void SpiConnectionHandling::triggerCourierMsg(int msgid)
{
   ETG_TRACE_USR4(("triggerCourierMsg msgid=%d", msgid));

   switch (msgid)
   {
      case 1:
      {
         ETG_TRACE_USR4(("Post courier message DipoDeviceConnectedUpdMsg "));
         POST_MSG((COURIER_MESSAGE_NEW(DipoDeviceConnectedUpdMsg)(DUMMY_COURIER, 1, 1)));
         break;
      }
      case 2:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DipoDeviceConnectedUpdMsg)(DUMMY_COURIER, 1, 0)));
         break;
      }
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      case 3:
      {
         ETG_TRACE_USR4(("Post courier message AndroidDeviceConnectedUpdMsg "));
         POST_MSG((COURIER_MESSAGE_NEW(AndroidDeviceConnectedUpdMsg)(DUMMY_COURIER, ANDROID_AUTO_ASK_ON_CONNECT_ON, ANDROID_AUTO_AUTOLAUNCHSTATUS_OFF)));

         break;
      }
#endif

      default:
         break;
   }
}


/**
 * Trace command to LaunchCarplay
 * @param[in]      : enDiPOAppType dipoAPPType
 * @return         : void
 */
ETG_I_CMD_DEFINE((TraceCmd_SpiLaunchApp, "SpiLaunchApp %d %d", ETG_I_CENUM(enDeviceCategory), ETG_I_CENUM(enDiPOAppType)))
void SpiConnectionHandling::TraceCmd_SpiLaunchApp(enDeviceCategory eDeviceCategory, enDiPOAppType eDipoAppType)
{
   ETG_TRACE_USR4(("TTFIS->HALL TraceCmd_SpiLaunchCarPlay=%d", eDipoAppType));
   pstaticSpiConnectionHandling->vLaunchApp(pstaticSpiConnectionHandling->getActiveDeviceHandle(), eDeviceCategory, eDipoAppType);
}


/**
 * SetAccessoryDisplayContext - call the method start through Ttfis APPHMI_MEDIA_SetAccessoryDisplayContext.
 * This message will post the method start sendAccessoryDisplayContext.
 * @param[in] :Displayflag
 * @param[in] :DisplayContext
 * @return void
 */
ETG_I_CMD_DEFINE((TraceCmd_SetAccessoryDisplayContext, "SetAccessoryDisplayContext %d %u", ETG_I_CENUM(enDisplayFlag), ETG_I_CENUM(enDisplayContext)))
void SpiConnectionHandling::TraceCmd_SetAccessoryDisplayContext(enDisplayFlag displayflag, enDisplayContext displayContext)
{
   bool bDisplayflag = FALSE;
   if (TRUE == displayflag)
   {
      bDisplayflag = TRUE;
   }
   else if (FALSE == displayflag)
   {
      bDisplayflag = FALSE;
   }
   else
   {
   }
   ETG_TRACE_USR4(("TTFIS->HALL sendAccessoryDisplayContextMethodStart displayflag =%d bDisplayflag =%d", displayflag, bDisplayflag));
   pstaticSpiConnectionHandling->setAccessoryDisplayContext(bDisplayflag, displayContext);
}


/**
 * SetAccessoryAudioContext - call the method start through Ttfis APPHMI_MEDIA_SetAccessoryAudioContext.
 * @param[in] :audioflag
 * @param[in] :audioContext
 * @return void
 */
ETG_I_CMD_DEFINE((TraceCmd_SetAccessoryAudioContext, "SetAccessoryAudioContext %d %u", ETG_I_CENUM(enAudioFlag), ETG_I_CENUM(enAudioContext)))
void SpiConnectionHandling::TraceCmd_SetAccessoryAudioContext(enAudioFlag audioflag, enAudioContext audioContext)
{
   bool bAudioflag = FALSE;
   if (TRUE == audioflag)
   {
      bAudioflag = TRUE;
   }
   else if (FALSE == audioflag)
   {
      bAudioflag = FALSE;
   }
   else
   {
   }
   ETG_TRACE_USR4(("TTFIS->HALL setAccessoryDisplayContext bAudioflag =%d audioContext =%d", bAudioflag, audioContext));
   pstaticSpiConnectionHandling->vSetAccessoryAudioContext(bAudioflag, audioContext);
}


void SpiConnectionHandling::onTriggerPTTShortPress()
{
   POST_MSG((COURIER_MESSAGE_NEW(HKStatusChangedUpdMsg)(::HARDKEYCODE_SWC_PTT, ::hmibase::HARDKEYSTATE_DOWN, 0)));
}


void SpiConnectionHandling::onTriggerPTTShortRelease()
{
   POST_MSG((COURIER_MESSAGE_NEW(HKStatusChangedUpdMsg)(::HARDKEYCODE_SWC_PTT, ::hmibase::HARDKEYSTATE_UP, 0)));
}


void SpiConnectionHandling::onTriggerPTTLongPress()
{
   POST_MSG((COURIER_MESSAGE_NEW(HKStatusChangedUpdMsg)(::HARDKEYCODE_SWC_PTT, ::hmibase::HARDKEYSTATE_LONG1, 0)));
}


void SpiConnectionHandling::onTriggerPTTLongRelease()
{
   POST_MSG((COURIER_MESSAGE_NEW(HKStatusChangedUpdMsg)(::HARDKEYCODE_SWC_PTT, ::hmibase::HARDKEYSTATE_LONGUP, 0)));
}


void SpiConnectionHandling::onTriggerTELShortPress()
{
   POST_MSG((COURIER_MESSAGE_NEW(HKStatusChangedUpdMsg)(::HARDKEYCODE_SWC_PHONE, ::hmibase::HARDKEYSTATE_DOWN, 0)));
}


void SpiConnectionHandling::onTriggerTELShortRelease()
{
   POST_MSG((COURIER_MESSAGE_NEW(HKStatusChangedUpdMsg)(::HARDKEYCODE_SWC_PHONE, ::hmibase::HARDKEYSTATE_UP, 0)));
}


void SpiConnectionHandling::onTriggerTELLongPress()
{
   POST_MSG((COURIER_MESSAGE_NEW(HKStatusChangedUpdMsg)(::HARDKEYCODE_SWC_PHONE, ::hmibase::HARDKEYSTATE_LONG1, 0)));
}


void SpiConnectionHandling::onTriggerSWCNextShortPress()
{
   POST_MSG((COURIER_MESSAGE_NEW(HKStatusChangedUpdMsg)(::HARDKEYCODE_SWC_NEXT, ::hmibase::HARDKEYSTATE_DOWN, 0)));
}


void SpiConnectionHandling::onTriggerSWCNextRelease()
{
   POST_MSG((COURIER_MESSAGE_NEW(HKStatusChangedUpdMsg)(::HARDKEYCODE_SWC_NEXT, ::hmibase::HARDKEYSTATE_UP, 0)));
}


ETG_I_CMD_DEFINE((traceCmd_Spi_TriggerPTTShortPress, "Spi_TriggerPTTShortPress"))
void SpiConnectionHandling::traceCmd_Spi_TriggerPTTShortPress()
{
   if (pstaticSpiConnectionHandling)
   {
      pstaticSpiConnectionHandling->onTriggerPTTShortPress();
   }
}


ETG_I_CMD_DEFINE((traceCmd_Spi_TriggerPTTShortRelease, "Spi_TriggerPTTShortRelease"))
void SpiConnectionHandling::traceCmd_Spi_TriggerPTTShortRelease()
{
   if (pstaticSpiConnectionHandling)
   {
      pstaticSpiConnectionHandling->onTriggerPTTShortRelease();
   }
}


ETG_I_CMD_DEFINE((traceCmd_Spi_TriggerPTTLongPress, "Spi_TriggerPTTLongPress"))
void SpiConnectionHandling::traceCmd_Spi_TriggerPTTLongPress()
{
   if (pstaticSpiConnectionHandling)
   {
      pstaticSpiConnectionHandling->onTriggerPTTLongPress();
   }
}


ETG_I_CMD_DEFINE((traceCmd_Spi_TriggerPTTLongRelease, "Spi_TriggerPTTLongRelease"))
void SpiConnectionHandling::traceCmd_Spi_TriggerPTTLongRelease()
{
   if (pstaticSpiConnectionHandling)
   {
      pstaticSpiConnectionHandling->onTriggerPTTLongRelease();
   }
}


ETG_I_CMD_DEFINE((traceCmd_Spi_TriggerTELShortPress, "Spi_TriggerTELShortPress"))
void SpiConnectionHandling::traceCmd_Spi_TriggerTELShortPress()
{
   if (pstaticSpiConnectionHandling)
   {
      pstaticSpiConnectionHandling->onTriggerTELShortPress();
   }
}


ETG_I_CMD_DEFINE((traceCmd_Spi_TriggerTELShortRelease, "Spi_TriggerTELShortRelease"))
void SpiConnectionHandling::traceCmd_Spi_TriggerTELShortRelease()
{
   if (pstaticSpiConnectionHandling)
   {
      pstaticSpiConnectionHandling->onTriggerTELShortRelease();
   }
}


ETG_I_CMD_DEFINE((traceCmd_Spi_TriggerSWCNextShortPress, "Spi_TriggerSWCNextShortPress"))
void SpiConnectionHandling::traceCmd_Spi_TriggerSWCNextShortPress()
{
   if (pstaticSpiConnectionHandling)
   {
      pstaticSpiConnectionHandling->onTriggerSWCNextShortPress();
   }
}


ETG_I_CMD_DEFINE((traceCmd_Spi_TriggerSWCNextRelease, "Spi_TriggerSWCNextRelease"))
void SpiConnectionHandling::traceCmd_Spi_TriggerSWCNextRelease()
{
   if (pstaticSpiConnectionHandling)
   {
      pstaticSpiConnectionHandling->onTriggerSWCNextRelease();
   }
}


ETG_I_CMD_DEFINE((TraceCmd_SetVehicleConfiguration, "SetVehicleConfiguration %u %d ", ETG_I_CENUM(enVehicle_Configuration), ETG_I_CENUM(ensetConfiguration)))
void SpiConnectionHandling::TraceCmd_SetVehicleConfiguration(enVehicle_Configuration eVehicleConfig , ensetConfiguration eSetconfig)
{
   bool bSetConfig = FALSE;
   if (TRUE == eSetconfig)
   {
      bSetConfig = TRUE;
   }
   else if (FALSE == eSetconfig)
   {
      bSetConfig = FALSE;
   }
   else
   {
   }

   ETG_TRACE_USR4(("TTFIS->HALL SetVehicleConfiguration eVehicleConfig = %d eSetconfig = %d", eVehicleConfig, eSetconfig));
   pstaticSpiConnectionHandling->setVehicleConfiguration(eVehicleConfig, bSetConfig);
}


ETG_I_CMD_DEFINE((TraceCmd_SpiSetAccessoryAppState, "SpiSetAccessoryAppState %u %u %u ", ETG_I_CENUM(enSpeechAppState), ETG_I_CENUM(enPhoneAppState), ETG_I_CENUM(enNavigationAppState)))
void SpiConnectionHandling::TraceCmd_SpiSetAccessoryAppState(enSpeechAppState eSpeechAppState , enPhoneAppState ePhoneAppState, enNavigationAppState eNavigationAppState)
{
   pstaticSpiConnectionHandling->setAccessoryAppState(eSpeechAppState, ePhoneAppState, eNavigationAppState);
}


/*
 * This function is implemented to trigger courier message through TTfis command for sending Play keycode command
 * @return     - Void
 */

ETG_I_CMD_DEFINE((traceCmd_Spi_SendPlayCommand, "Spi_SendPlayCommand")) // Trace class declaration
void SpiConnectionHandling::traceCmd_Spi_SendPlayCommand()
{
   ETG_TRACE_USR4(("traceCmd_Spi_SendPlayCommand is Posted"));

   if (pstaticSpiConnectionHandling != NULL)
   {
      pstaticSpiConnectionHandling->SendPlaycommandTrigger();
   }
}


/*
 * This function is implemented to trigger courier message through TTfis command for sending Play keyCode command
 * @return     - Void
 */

void SpiConnectionHandling::SendPlaycommandTrigger()
{
   POST_MSG((COURIER_MESSAGE_NEW(onSendDipoPlayCommandUpdMsg)()));
   ETG_TRACE_USR4(("HALL->GUI:traceCmd_Spi_SendPlayCommand :onSendDipoPlayCommandUpdMsg Posted"));
}


/*
 * This function is implemented to trigger courier message through TTfis command for Error PopUPs
 * @return     - Void
 */
ETG_I_CMD_DEFINE((TraceCmd_SpiConnErrorPopUps, "SpiConnErrorPopUps %u", ETG_I_CENUM(enErrorPopUp)))
void SpiConnectionHandling::TraceCmd_SpiConnErrorPopUps(enErrorPopUp ePopUpName)
{
   ETG_TRACE_USR4(("TraceCmd_SpiConnErrorPopUps is Posted"));
   if (pstaticSpiConnectionHandling != NULL)
   {
      pstaticSpiConnectionHandling->sendErrorPopUpTrigger(ePopUpName);
   }
}


/*
 * This function is helper function for TraceCmd_SpiConnErrorPopUps to trigger specific courier message
 * @return     - Void
 */
void SpiConnectionHandling::sendErrorPopUpTrigger(enErrorPopUp ePopUpName)
{
   ETG_TRACE_USR4(("SpiConnectionHandling::sendErrorPopUpTrigger %u", ePopUpName));
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
   switch (ePopUpName)
   {
      case INFO__POPUP_CARPLAY_START_FAIL:
      {
         POST_MSG((COURIER_MESSAGE_NEW(CarplayStartFailPopUpReqMsg)()));
         break;
      }
      case INFO__POPUP_ANDROID_START_FAIL:
      {
         POST_MSG((COURIER_MESSAGE_NEW(AndroidAutoStartFailPopUpReqMsg)()));
         break;
      }
      case INFO__POPUP_ANDROID_AUTH_FAILURE_SYSIND :
      {
         POST_MSG((COURIER_MESSAGE_NEW(AndroidAuthFailurePopUpReqMsg)()));
         break;
      }
      case INFO__POPUP_CARPLAY_AUTH_FAILURE_SYSIND:
      {
         POST_MSG((COURIER_MESSAGE_NEW(CarPlayAuthFailurePopUpReqMsg)()));
         break;
      }
      case INFO__POPUP_CARPLAY_STARTING_SYSIND:
      {
         POST_MSG((COURIER_MESSAGE_NEW(CarplayStartingPopUpReqMsg)()));
         break;
      }
      case INFO__POPUP_CARPLAY_STARTED_SYSIND:
      {
         POST_MSG((COURIER_MESSAGE_NEW(CarplayStartedPopUpReqMsg)()));
         break;
      }
      case INFO__POPUP_ANDROID_STARTING_SYSIND:
      {
         POST_MSG((COURIER_MESSAGE_NEW(AndroidAutoStartingPopUpReqMsg)()));
         break;
      }
      case INFO__POPUP_ANDROID_STARTED_SYSIND :
      {
         POST_MSG((COURIER_MESSAGE_NEW(AndroidAutoStartedPopUpReqMsg)()));
         break;
      }
      case INFO__POPUP_ANDROID_NO_USB_DEVICES_SYSIND:
      {
         POST_MSG((COURIER_MESSAGE_NEW(AndroidAutoNoUSBPopUpReqMsg)()));
         break;
      }
      case INFO__POPUP_CARPLAY_NO_USB_DEVICES_SYSIND:
      {
         POST_MSG((COURIER_MESSAGE_NEW(CarplayNoUSBPopUpReqMsg)()));
         break;
      }
      case INFO__POPUP_ANDROID_RECONNECT_USB_SYSIND:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ReconnectionUSBPopUpReqMsg)()));
         break;
      }
      case INFO__POPUP_CARPLAY_RECONNECT_USB_SYSIND:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ReconnectionUSBPopUpReqMsg)()));
         break;
      }
      case INFO__POPUP_ANDROIDAUTO_ADVISORY_SYSIND:
      {
         POST_MSG((COURIER_MESSAGE_NEW(AndroidAutoAdvisoryPopUpReqMsg)()));
         break;
      }
      case INFO__POPUP_CARPLAY_ADVISORY_SYSIND:
      {
         POST_MSG((COURIER_MESSAGE_NEW(CarplayAdvisoryPopUpReqMsg)()));
         break;
      }
      default:
         break;
   }
#endif
}


ETG_I_CMD_DEFINE((TraceCmd_SPIPhoneCallMetaData, "CarPlayPhoneCallMetaData"))
void SpiConnectionHandling::TraceCmd_SPIPhoneCallMetaData()
{
   ETG_TRACE_USR4(("TraceCmd_SPIPhoneCallMetaData is Posted"));
   if (pstaticSpiConnectionHandling != NULL)
   {
      pstaticSpiConnectionHandling->onTriggerSPIPhoneCallMetaData();
   }
}


void SpiConnectionHandling::onTriggerSPIPhoneCallMetaData()
{
   POST_MSG((COURIER_MESSAGE_NEW(SPICallInfoPopUpReqMsg)()));
   ETG_TRACE_USR4(("HALL->GUI:TraceCmd_SPIPhoneCallMetaData :SPICallInfoPopUpReqMsg Posted"));
}


}//end of namespace Core
}//end of namespace App
