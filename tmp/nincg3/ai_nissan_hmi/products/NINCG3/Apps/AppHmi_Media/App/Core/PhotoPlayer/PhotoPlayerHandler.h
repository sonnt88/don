/******************************************************************
*FILE: PhotoPlayerHandler.h
*SW-COMPONENT: AppHmi_media
*DESCRIPTION: Header file of handler for PhotoPlayer feature.
*             This handler monitor screens/image list for PhotoPlayer Cover and Full Screen
*COPYRIGHT: © 2016 Robert Bosch GmbH
*
*The reproduction, distribution and utilization of this file as
*well as the communication of its contents to others without express
*authorization is prohibited. Offenders will be held liable for the
*payment of damages. All rights reserved in the event of the grant
*of a patent, utility model or design.
******************************************************************/

#ifndef PHOTOPLAYERHANDLER_H
#define PHOTOPLAYERHANDLER_H

#include "CgiExtensions/ListDataProviderDistributor.h"
#include "Common/ListHandler/ListRegistry.h"
#include "mplay_MediaPlayer_FIProxy.h"
#include "AppHmi_MediaStateMachine.h"
#include "Core/Utils/MediaUtils.h"
#include "Core/PlayScreen/IPlayScreen.h"
#include "Core/Browse/IListFacade.h"

namespace App {
namespace Core {
class IPlayScreen;


static const unsigned int LIST_TYPE_PHOTO_PLAYER_IMAGE = 31u;
static const unsigned int FLEX_LIST_COVER_INIT_BUFFER_SIZE = 14u;
static const unsigned int FLEX_LIST_FULL_SCREEN_INIT_BUFFER_SIZE = 6u;
static const char* const  DATA_CONTEXT_PLAYER_COVER_LIST_ITEM = "Photo_Content";

struct WindowList
{
   uint32 _windowStartIndex;
   uint32 _windowSize;
};


struct RequestedList
{
   uint32 _startIndex;
   uint32 _bufferSize;
};


struct ResponsedList
{
   uint32 _listHandle;
   uint32 _startIndex;
   uint32 _totalListSize;
   ::MPlay_fi_types::T_MPlayMediaObjects _oMediaObjects;
};


struct SlideShowNextPrevSkip
{
   uint32 _nextPrevSkipState;
   uint8 _u8NextPrevSkipCount;
};


enum ResponsedListState
{
   RLS_IDLE = 1u,
   RLS_PROCESSING = 2u,
   RLS_DONE = 3u
};


enum PhotoPlayerScreen
{
   PPS_BROWSER = 0u,
   PPS_COVER = 1u,
   PPS_FULL_SCREEN = 2u,
   PPS_SLIDE_SHOW = 3u
};


enum SlideShowNextPrevSkipState
{
   SS_NoSkip = 0u,
   SS_InitSkip = 1u,
   SS_PrevSkip = 2u,
   SS_NextSkip = 3u
};


class PhotoPlayerHandler: public ListImplementation
   , public ::mplay_MediaPlayer_FI::MediaPlayerListChangeCallbackIF
   , public ::mplay_MediaPlayer_FI::CreateMediaPlayerIndexedListCallbackIF
   , public ::mplay_MediaPlayer_FI::RequestMediaPlayerIndexedListSliceCallbackIF
   , public ::mplay_MediaPlayer_FI::StartSlideshowCallbackIF

   , public ::mplay_MediaPlayer_FI::CreateMediaPlayerIndexedImageFolderListCallbackIF				//add feature switch
   , public ::mplay_MediaPlayer_FI::RequestMediaPlayerIndexedImageFolderListSliceCallbackIF


{
   public:
      PhotoPlayerHandler(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy, IPlayScreen* pPlayScreen);
      virtual ~PhotoPlayerHandler();

      tSharedPtrDataProvider getListDataProvider(const ListDataInfo& _ListInfo);

      void registerProperties(const ::boost::shared_ptr<asf::core::Proxy>& proxy, const asf::core::ServiceStateChange& /*stateChange*/);
      void deregisterProperties(const ::boost::shared_ptr<asf::core::Proxy>& proxy, const asf::core::ServiceStateChange& /*stateChange*/);

      virtual void onMediaPlayerListChangeError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerListChangeError >& error);
      virtual void onMediaPlayerListChangeStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerListChangeStatus >& status);
      virtual void onCreateMediaPlayerIndexedListError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerIndexedListError >& error);
      virtual void onCreateMediaPlayerIndexedListResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerIndexedListResult >& result);
      virtual void onRequestMediaPlayerIndexedListSliceError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestMediaPlayerIndexedListSliceError >& error);
      virtual void onRequestMediaPlayerIndexedListSliceResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestMediaPlayerIndexedListSliceResult >& result);
      virtual void onStartSlideshowError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::StartSlideshowError >& error);
      virtual void onStartSlideshowResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::StartSlideshowResult >& result);

      virtual void onCreateMediaPlayerIndexedImageFolderListError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
            const ::boost::shared_ptr<mplay_MediaPlayer_FI:: CreateMediaPlayerIndexedImageFolderListError >& error);
      virtual void onCreateMediaPlayerIndexedImageFolderListResult(const ::boost::shared_ptr<mplay_MediaPlayer_FI:: Mplay_MediaPlayer_FIProxy >& proxy,
            const ::boost::shared_ptr<mplay_MediaPlayer_FI:: CreateMediaPlayerIndexedImageFolderListResult >& result);

      virtual void onRequestMediaPlayerIndexedImageFolderListSliceError(const ::boost::shared_ptr<mplay_MediaPlayer_FI:: Mplay_MediaPlayer_FIProxy >& proxy,
            const ::boost::shared_ptr<mplay_MediaPlayer_FI:: RequestMediaPlayerIndexedImageFolderListSliceError >& error);
      virtual void onRequestMediaPlayerIndexedImageFolderListSliceResult(const ::boost::shared_ptr<mplay_MediaPlayer_FI:: Mplay_MediaPlayer_FIProxy >& proxy,
            const ::boost::shared_ptr< mplay_MediaPlayer_FI::RequestMediaPlayerIndexedImageFolderListSliceResult >& result);

      void populateCoverListOrShowFullScreenItem(uint32 selectedItem, uint32 activeDeviceType, uint32 activeDeviceTag);
      void playSlideShowImageList(uint32 selectedItem);
      ::MPlay_fi_types::T_MPlayMediaObject getCurrentImage();

      static void TraceCmd_EncoderRotation(enEncoder nRotationDirection);

      bool onCourierMessage(const InitialisePhotoPlayerCoverMsg& oMsg);
      bool onCourierMessage(const ButtonPhotoPlayerCoverItemPressUpMsg& /*oMsg*/);
      bool onCourierMessage(const RotatePhotoPlayerReqMsg& /*msg*/);
      bool onCourierMessage(const InitialisePlayPhotoPlayerReqMsg& /*msg*/);
      bool onCourierMessage(const FullScreenPhotoPlayerReqMsg& /*msg*/);
      bool onCourierMessage(const FitScreenPhotoPlayerReqMsg& /*msg*/);
      bool onCourierMessage(const SlideShowPositionPhotoPlayerChangedUpdMsg& msg);
      bool onCourierMessage(const InitialisePictureBrowserReqMsg& /*oMsg*/);

      bool onCourierMessage(const ButtonReactionMsg& oMsg);
      bool onCourierMessage(const FocusChangedUpdMsg& oMsg);
      virtual bool onCourierMessage(const ListDateProviderReqMsg& oMsg);
      virtual bool onCourierMessage(const ListChangedUpdMsg& oMsg);

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)

      ON_COURIER_MESSAGE(InitialisePhotoPlayerCoverMsg)
      ON_COURIER_MESSAGE(ButtonPhotoPlayerCoverItemPressUpMsg)
      ON_COURIER_MESSAGE(RotatePhotoPlayerReqMsg)
      ON_COURIER_MESSAGE(InitialisePlayPhotoPlayerReqMsg)
      ON_COURIER_MESSAGE(FullScreenPhotoPlayerReqMsg)
      ON_COURIER_MESSAGE(FitScreenPhotoPlayerReqMsg)
      ON_COURIER_MESSAGE(SlideShowPositionPhotoPlayerChangedUpdMsg)

      ON_COURIER_MESSAGE(ListChangedUpdMsg)
      ON_COURIER_MESSAGE(FocusChangedUpdMsg)

      ON_COURIER_MESSAGE(InitialisePictureBrowserReqMsg)

      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_DELEGATE_TO_CLASS(ListImplementation)
      COURIER_MSG_MAP_DELEGATE_END()

   private:
      void initializeListParameters();
      void setWindowStartIndex(uint32);
      void setFocussedIndex(int32);
      void requestListData(RequestedList requestedListParam);
      void scrollListToWindow(uint32 windowStartIndex);
      void updateListData(void);
      void updateEmptyListData(void);
      tSharedPtrDataProvider fillListItems();
      tSharedPtrDataProvider fillEmptyListItems();
      int32 getActiveItemIndex(int32 selectedIndex);
      void getImageList();

      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& _mediaPlayerProxy;
      IPlayScreen& _iPlayScreen;
      bool _isGettingListDataProvider;
      uint32 _responsedListState;
      uint32 _currentPhotoPlayerScreen;
      uint32 _windowStartIndex;
      uint32 _nextListWindowStartIndex;
      int32 _focussedIndex;
      bool _isNextShifting;
      uint32 _activeDeviceType;
      uint32 _activeDeviceTag;
      WindowList _currentWindowList;
      WindowList _currenEmptytWindowList;
      RequestedList _requestedList;
      ResponsedList _responsedList;
      SlideShowNextPrevSkip _slideShowNextPrevSkip;
      uint32 _slideShowPosition;
};


} //end of namespace Core
} //end of namespace App

#endif /* PHOTOPLAYERHANDLER_H_ */
