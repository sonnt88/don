/**
 * @file DataPoolConfig.h
 * @Author ECV1-alc7kor
 * @Copyright (c) 2015 Robert Bosch Car Multimedia Gmbh
 * @addtogroup AppHmi_Media_Spi
 */

#ifndef DATAPOOLCONFIG_H_
#define DATAPOOLCONFIG_H_

#include "asf/core/Types.h"

// Forward Declarations
class dp_tclAppHmi_MediaSpiAutoLaunch;
class dp_tclAppHmi_MediaCarPlayAskOnConnect;
class dp_tclAppHmi_MediaCarPlayAuoLaunchConnect;
class dp_tclAppHmi_MediaAndroidAutoLaunchStatus;
class dp_tclAppHmi_MediaAndroidAutoAskOnConnect;

/**
 * class SpiMediaDataPoolConfig
 * This class hold implementation related to Datapool elements. It provides interfaces to read/write DP elements
 */
class SpiMediaDataPoolConfig
{
   public:
      /**
       * Destructor of class DataPoolConfig
       * @return None
       */
      ~SpiMediaDataPoolConfig();

      /**
       * Singleton Class. Method to retrieve the instance of the class
       * @return Returns instance of the class
       */
      static SpiMediaDataPoolConfig* getInstance();

      /**
       * Function to only create an instance
       * @return None
       */
      static void createInstance();

      /**
       * Function to delete instance of the class
       * @return None
       */
      static void deleteInstance();

      /**
       * Setter function to write the Spi Auto Lunch Mode status Dp element
       * @param[in] autoLunchMode: Spi Auto Lunch Mode
       * @return None
       */
      void setDpSpiAutoLaunch(const uint32 autoLaunchMode);

      /**
       * Getter function to read the Spi Auto Lunch Mode status Dp element
       * @return Spi Auto Lunch Mode
       */
      uint32 getDpSpiAutoLaunch();

      /**
       * Setter function to write the  Spi CarPlay AskOnConnect status Dp element
       * @param[in] CarplayAskOnConnectStatus: Spi AskOnConnectStatus
       * @return None
       */

      void setDpSpiCarPlayAskOnConnect(const uint32 CarplayAskOnConnectStatus);

      /**
       * Getter function to read the Spi CarPlay AskOnConnect status Dp element
       * @return Spi CarPlay AskOnConnectStatus
       */
      uint32 getDpSpiCarPlayAskOnConnect();

      /**
       * Setter function to write the  Spi CarPlay AutoLaunch status Dp element
       * @param[in] CarplayAutoLaunchStatus: Spi CarplayAutoLaunchStatus
       * @return None
       */

      void setDpSpiCarPlayAutoLaunch(const uint32 CarplayAutoLaunchStatus);

      /**
       * Getter function to read the Spi CarplayAutoLaunchStatus status Dp element
       * @return Spi CarplayAutoLaunchStatus
       */
      uint32 getDpSpiCarPlayAutoLaunch();

      /**
       * Setter function to write the  Spi AndroidAuto LaunchStatus Dp element
       * @param[in] AndroidAutoLaunchStatus: Spi AndroidAutoLaunchStatus
       * @return None
       */

      void setDpAndroidAutoLaunch(const uint32 AndroidAutoLaunchStatus);

      /**
       * Getter function to read the Spi AndroidAuto LaunchStatus status Dp element
       * @return Spi AndroidAutoLaunchStatus
       */

      uint32 getDpAndroidAutoLaunch();
      /**
       * Setter function to write the  Spi CarPlay AutoLaunch status Dp element
       * @param[in] AndroidAutoAskOnConnectStatus: Spi AndroidAuto AskOnConnectStatus
       * @return None
       */

      void setDpAndroidAutoAskOnConnect(const uint32 AndroidAutoAskOnConnectStatus);

      /**
       * Getter function to read the Spi Android Auto AskOnConnectStatus status Dp element
       * @return Spi AndroidAutoAskOnConnectStatus
       */

      uint32 getDpAndroidAutoAskOnConnect();
   private:

      // Instance of the class
      static SpiMediaDataPoolConfig* _mDpSpiMedia;

      /**
       * Constructor of class DataPoolConfig
       * @return None
       */
      SpiMediaDataPoolConfig();

      //DP Elements
      dp_tclAppHmi_MediaSpiAutoLaunch _mDpSpiAutoLaunch;
      dp_tclAppHmi_MediaCarPlayAskOnConnect _mDpSpiCarPlayAskOnConnect;
      dp_tclAppHmi_MediaCarPlayAuoLaunchConnect _mDpSpiCarPlayAutoLaunch;
      dp_tclAppHmi_MediaAndroidAutoLaunchStatus _mDpAndroidAutoLaunchStatus;
      dp_tclAppHmi_MediaAndroidAutoAskOnConnect _mDpAndroidAutoAskOnConnect;

};


#endif /* DATAPOOLCONFIG_H_ */
