/**
 *  @file   MediaDeviceConnectionHandling.cpp
 *  @author ECV - kng3kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */


#include "hall_std_if.h"
#include "Core/DeviceConnection/MediaDeviceConnectionHandling.h"
#include "Core/LanguageDefines.h"
#include "Core/Utils/MediaUtils.h"
#include "Core/BTAudio/IBTSettings.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_APPHMI_MEDIA_HALL_DEVICE_CONNECTION
#include "trcGenProj/Header/MediaDeviceConnectionHandling.cpp.trc.h"
#endif

namespace App {
namespace Core {
using namespace ::MPlay_fi_types;
/**
 * @Destructor
 */
MediaDeviceConnectionHandling::~MediaDeviceConnectionHandling()
{
   _mediaPlayerProxy.reset();
}


/**
 * @Constructor
 */
MediaDeviceConnectionHandling::MediaDeviceConnectionHandling(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy
      , ContextSwitchHomeScreen* pHomeScreen, IBTSettings* _pBTSettings)
   : _mediaPlayerProxy(pMediaPlayerProxy), _DeviceHomeScreen(*pHomeScreen), _NewlyMediaPlayerSourceId(0), _iBTSettings(*_pBTSettings)
{
   ETG_TRACE_USR4(("MediaDeviceConnectionHandling created......."));
   _deviceConnectionObserver.clear();
   vInitialiseDeviceList();
   _spiActivestatus = false;
}


/**
 * vInitialiseDeviceList - Private helper function to resets the values in the device list struct
 * @param[in] None
 * @parm[out] None
 * @return void
 */
void MediaDeviceConnectionHandling::vInitialiseDeviceList()
{
   for (unsigned int uiIndex = 0; uiIndex < MPLAY_MAX_DEVICES; uiIndex++)
   {
      _oDeviceList[uiIndex].bIsActiveSource = FALSE;
      _oDeviceList[uiIndex].bIsConnected    = FALSE;
      _oDeviceList[uiIndex].sDeviceName    = '\0';
      _oDeviceList[uiIndex].uiDeviceTag     = 0;
      _oDeviceList[uiIndex].uiDeviceType    = MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_UNKNOWN;
      _oDeviceList[uiIndex].uiDeviceState   = MPlay_fi_types::T_e8_MPlayDeviceStatus__e8DS_NONE;
      _oDeviceList[uiIndex].uiNumberofFiles = 0xFFFFFFFF;
      _oDeviceList[uiIndex].indexedState = MPlay_fi_types::T_e8_MPlayDeviceIndexedState__e8IDS_NOT_STARTED;
      _oDeviceList[uiIndex].disconnectReason = T_e8_MPlayDisconnectReason__e8DR_REMOVED;
      _oDeviceList[uiIndex].deviceSize = 0;
   }
}


/**
 * registerProperties - Trigger property registration to mediaplayer,  called from MediaHall class
 * @param[in] proxy
 * @parm[in] stateChange - state change service for corrosponding  proxy
 * @return void
 */
void MediaDeviceConnectionHandling::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_mediaPlayerProxy && _mediaPlayerProxy == proxy)
   {
      ETG_TRACE_USR4(("Send Properties registration for mediaplayer......"));
      _mediaPlayerProxy->sendMediaPlayerDeviceConnectionsUpReg(*this);
      _mediaPlayerProxy->sendIndexingStateUpReg(*this);
      _mediaPlayerProxy->sendMediaPlayerOpticalDiscSlotStateUpReg(*this);
      _mediaPlayerProxy->sendActiveMediaDeviceUpReg(*this);
      _mediaPlayerProxy->sendMediaPlayerOpticalDiscCDInfoUpReg(*this);
   }
}


/**
 * deregisterProperties - Trigger property deregistration to mediaplayer, called from MediaHall class
 * @param[in] proxy
 * @parm[in] stateChange - state change service for corrsponding proxy
 * @return void
 */
void MediaDeviceConnectionHandling::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_mediaPlayerProxy && _mediaPlayerProxy == proxy)
   {
      _mediaPlayerProxy->sendMediaPlayerDeviceConnectionsRelUpRegAll();
      _mediaPlayerProxy->sendIndexingStateRelUpRegAll();
      _mediaPlayerProxy->sendMediaPlayerOpticalDiscSlotStateRelUpRegAll();
      _mediaPlayerProxy->sendActiveMediaDeviceRelUpRegAll();
      _mediaPlayerProxy->sendMediaPlayerOpticalDiscCDInfoRelUpRegAll();
   }
}


/**
 * vMediaPlayerDeviceConnections_Status - update info of all mediaplayer devices connection (e.g. connection, number of file, device type,
 * device tag, indexing state etc. )
 *
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */
void MediaDeviceConnectionHandling::onMediaPlayerDeviceConnectionsStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsStatus >& status)
{
   ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus received..........."));
   notifyDeviceConnectionObserver();

   ::MPlay_fi_types::T_MPlayDeviceInfo oDeviceInfo = status->getODeviceInfo();
   MPlay_fi_types::T_MPlayDeviceInfo::iterator itr = oDeviceInfo.begin();

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
   _DeviceHomeScreen.setDeviceInfoHomeScreen(oDeviceInfo);
#endif
   for (int8 u8Index = 0 ; itr != oDeviceInfo.end(); itr++, u8Index++)
   {
      bool bIsDeviceConnected = itr->getBDeviceConnected();
      std::string sDeviceName = itr->getSDeviceName();
      bool bIsActiveSource    = itr->getBDeviceActiveSource();
      int8 i8DeviceTag        = itr->getU8DeviceTag();
      int8 i8DeviceType       = itr->getE8DeviceType();
      int8 i8DeviceState      = itr->getE8DeviceState();
      int32 i32NumberofFiles  = itr->getU32NumberOfFiles();
      int8 i8DeviceIndexedState = itr->getE8DeviceIndexedState();
      int8 i8DeviceDisconnectReason = itr->getE8DisconnectReason();
      int8 i8AvailabilityReason = itr->getE8AvailabilityReason();
      uint32 u32DeviceSize = itr->getU32TotalSize();
      ETG_TRACE_USR4(("IsDeviceConnected = %d Index State= %d NAME = %s" , bIsDeviceConnected, i8DeviceIndexedState, sDeviceName.c_str()));
      ETG_TRACE_USR4(("IsActiveSource= %d ", bIsActiveSource));
      ETG_TRACE_USR4(("i8DeviceTag= %d ", i8DeviceTag));
      ETG_TRACE_USR4(("i8DeviceType= %d ", i8DeviceType));
      ETG_TRACE_USR4(("i8DeviceState= %d ", i8DeviceState));
      ETG_TRACE_USR4(("u32NumberofFiles= %d ", i32NumberofFiles));
      ETG_TRACE_USR4(("i8DeviceDisconnectReason= %d ", i8DeviceDisconnectReason));
      ETG_TRACE_USR4(("i8AvailabilityReason= %d ", i8AvailabilityReason));
      ETG_TRACE_USR4(("u32DeviceSize= %d ", u32DeviceSize));
      // to find which device status has changed. Compare with initial structure with new connected device info
      if (_oDeviceList[u8Index].bIsConnected != bIsDeviceConnected && i8DeviceTag != 0)
      {
         //Switch to the source thats connected if not already the active source
         _oDeviceList[u8Index].uiNumberofFiles = 0xFFFFFFFF; //Set initial number of files for a device

         //if connected device is disconnected
         if (bIsDeviceConnected == FALSE)
         {
            UpdateDeviceDisconnection(i8DeviceType, i8DeviceDisconnectReason);
            updateDeviceStatusOnDisconnection(i8DeviceTag);
         }
         else if ((bIsDeviceConnected == TRUE))
         {
            UpdateDeviceConnection(i8DeviceType, i8DeviceTag, i8AvailabilityReason);
            UpdateUnsupportedErrorStatus(i8DeviceState, i8DeviceType);
         }
      }//If connectionstatus has changed
      else if ((_oDeviceList[u8Index].disconnectReason != i8DeviceDisconnectReason) && (_oDeviceList[u8Index].uiDeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH))
      {
         UpdateDeviceDisconnection(i8DeviceType, i8DeviceDisconnectReason);
      }
      if ((i8DeviceTag != 0) && bIsDeviceConnected)
      {
         UpdateCommunicationErrorStatus(i8DeviceState, i8DeviceType);

         // Check for Number of files only after indexing is complete
         if (i8DeviceIndexedState == MPlay_fi_types::T_e8_MPlayDeviceIndexedState__e8IDS_COMPLETE && (i8DeviceIndexedState != _oDeviceList[u8Index].indexedState))
         {
            ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: i32NumberofFiles: %d", i32NumberofFiles));
            if (i32NumberofFiles > 0)
            {
               _oDeviceList[u8Index].uiNumberofFiles = i32NumberofFiles;
            }
            else
            {
               UpdateNoMediaFiles();
            }
         }
      }
#ifdef SHOW_CD_STATUS_OVERHEATED
      if (i8DeviceDisconnectReason == T_e8_MPlayDisconnectReason__e8DR_TEMPERATURE)
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_OVERHEATED)));
      }
#else
      if (i8DeviceDisconnectReason == T_e8_MPlayDisconnectReason__e8DR_TEMPERATURE)
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS)));
      }
#endif
      //Copy the device list to local struct
      _oDeviceList[u8Index].bIsActiveSource = bIsActiveSource;
      _oDeviceList[u8Index].bIsConnected    = bIsDeviceConnected;
      _oDeviceList[u8Index].sDeviceName     = sDeviceName;
      _oDeviceList[u8Index].uiDeviceTag     = i8DeviceTag;
      _oDeviceList[u8Index].uiDeviceType    = i8DeviceType;
      _oDeviceList[u8Index].uiDeviceState   = i8DeviceState;
      _oDeviceList[u8Index].indexedState = i8DeviceIndexedState;
      _oDeviceList[u8Index].disconnectReason = i8DeviceDisconnectReason;
      _oDeviceList[u8Index].deviceSize = u32DeviceSize;
      _oDeviceList[u8Index].i8AvailabilityReason = i8AvailabilityReason;
   }//end of for loop
}


/**
 * updateDeviceStatusOnDisconnection - called if device connected status is false
 *
 * @param[in] DeviceTag
 * @parm[in] void
 * @
 */
void MediaDeviceConnectionHandling::updateDeviceStatusOnDisconnection(int8 i8DeviceTag)
{
   ETG_TRACE_USR4(("updateDeviceStatusOnDisconnection:"));
   if (i8DeviceTag == _prevDeviceTag)
   {
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      POST_MSG((COURIER_MESSAGE_NEW(SourceUpdateToTestmodeMsg)(1)));
#endif
   }
}


/**
 * onIndexingStateStatus - updates the status of the indexing operation for a connected device. (e.g. Indexing percentage,
 * device tag, indexing state etc. )
 *
 * @param[in] proxy
 * @parm[in] status
 * @return void
 */
void MediaDeviceConnectionHandling::onIndexingStateStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::IndexingStateStatus >& status)
{
   ETG_TRACE_USR4(("onIndexingStateStatus:"));
   MPlay_fi_types::T_MPlayIndexingStateResult indexingState;
   uint8 u8CurrentDeviceTag = u32GetActiveDeviceTag();
   indexingState = status->getOIndexingStateResult();

   MPlay_fi_types::T_MPlayIndexingStateResult::iterator itr = indexingState.begin();
   for (; itr != indexingState.end(); itr++)
   {
      if (itr->getU8DeviceTag() == u8CurrentDeviceTag)
      {
         if (_deviceIndexState != itr->getE8DeviceIndexedState())
         {
            POST_MSG(COURIER_MESSAGE_NEW(IndexingCompleteUpdMsg)());
         }
         _deviceIndexState = itr->getE8DeviceIndexedState();
         uint8 indexPercentage = itr->getU8IndexingPercentComplete();
         ETG_TRACE_USR4(("onIndexingStateStatus:deviceIndexState = %d, indexPercentage= %d", _deviceIndexState, indexPercentage));
         MediaDatabinding::getInstance().updateIndexingInfo(_deviceIndexState, indexPercentage);
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
         POST_MSG((COURIER_MESSAGE_NEW(IndexingInfoUpdMsg)(indexPercentage, _deviceIndexState)));
#endif
         break;
      }
      else
      {
         ETG_TRACE_USR4(("Not a Current active device"));
      }
   }//end of loop
}


/**
 * onIndexingStateError - called if IndexingState property is updated with error from media player
 *
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void MediaDeviceConnectionHandling::onIndexingStateError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::IndexingStateError >& /*error*/)
{
   ETG_TRACE_USR4(("onIndexingStateError: Error"));
}


/**
 * UpdateDeviceConnection - called if device connection property is updated if device is disconnected
 *
 * @param[in] u8DeviceType
 * @parm[in] void
 * @
 */
void MediaDeviceConnectionHandling::UpdateDeviceDisconnection(int8 u8DeviceType, int8 u8DeviceDisconnectionReason) const
{
   switch (u8DeviceType)
   {
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPOD:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPHONE:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_USB:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_MTP:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND)));
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_LOADING_FILE)));
         ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: media device %d disconnected", u8DeviceType));
         break;
      }
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH:
      {
         ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: BT disconnected"));
         int32 btSourceAvailibility = 0;
         if (u8DeviceDisconnectionReason == MPlay_fi_types::T_e8_MPlayDisconnectReason__e8DR_CONNECTED_OVER_USB)
         {
            bool isMetadataVisible = false;
            bool isBTConnectedOverUSB = true;
            MediaDatabinding::getInstance().updateBTDisconnectionStatusOverUSB(isBTConnectedOverUSB,
                  isMetadataVisible);
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
            MediaDatabinding::getInstance().updateTrackNoInfoInBT(isMetadataVisible);
#endif
            btSourceAvailibility = 1;
         }
         else
         {
            bool isMetadataVisible = false;
            bool isBTConnectedOverUSB = false;
            MediaDatabinding::getInstance().updateBTDisconnectionStatusOverUSB(isBTConnectedOverUSB,
                  isMetadataVisible);
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
            MediaDatabinding::getInstance().updateTrackNoInfoInBT(isMetadataVisible);
#endif
            btSourceAvailibility = 0;
         }
         if (MediaDatabinding::getInstance().getCurrentActiveMediaDevice() == SOURCE_BT)
         {
            MediaDatabinding::getInstance().updateActiveDeviceTag(0xFFFFFFFF);
            POST_MSG((COURIER_MESSAGE_NEW(ActiveSourceUpdateMsg)(1, SOURCE_BT, btSourceAvailibility)));
            if (!_spiActivestatus)
            {
               POST_MSG((COURIER_MESSAGE_NEW(ActiveSourceMainScreenUpdMsg)(1, SOURCE_BT, btSourceAvailibility)));
            }
         }

         break;
      }
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDROM:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING)));
         ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: CDROM disconnected"));
         break;
      }
#ifdef HMI_CDDA_SUPPORT
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING)));
         ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: CDDA disconnected"));
         break;
      }
#endif
      default:
      {
         ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: DeviceType: %d disconnected", u8DeviceType));
         break;
      }
   }
}


/**
 * UpdateDeviceConnection - called if device connection property is updated if device is connected
 *
 * @param[in] u8DeviceType
 * @parm[in] void
 * @
 */
void MediaDeviceConnectionHandling::UpdateDeviceConnection(int8 u8DeviceType, uint8 deiveTag, int8 i8AvailabilityReason)
{
   switch (u8DeviceType)
   {
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPOD:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPHONE:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_USB:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_MTP:
      {
         ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: media device %d connected", u8DeviceType));
         if (i8AvailabilityReason != MPlay_fi_types::T_e8_MPlayAvailabilityReason__e8AR_SAME_MEDIA)
         {
            POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND)));
            _NewlyMediaPlayerSourceId = deiveTag;
         }
         break;
      }
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH:
      {
         bool isMetadataVisible = true;
         bool isBTConnectedOverUSB = false;
         _iBTSettings.updateTrackInfoInBTMain();
         MediaDatabinding::getInstance().updateBTDisconnectionStatusOverUSB(isBTConnectedOverUSB,
               isMetadataVisible);
         ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus BT connected"));
         int32 btSourceAvailibility = 1;
         if (MediaDatabinding::getInstance().getCurrentActiveMediaDevice() == SOURCE_BT)
         {
            _mediaPlayerProxy->sendActiveMediaDeviceSet(*this, deiveTag, true);
            MediaDatabinding::getInstance().updateActiveDeviceTag(deiveTag);
            POST_MSG((COURIER_MESSAGE_NEW(ActiveSourceUpdateMsg)(1, SOURCE_BT, btSourceAvailibility)));
            if (!_spiActivestatus)
            {
               POST_MSG((COURIER_MESSAGE_NEW(ActiveSourceMainScreenUpdMsg)(1, SOURCE_BT, btSourceAvailibility)));
            }
         }
         break;
      }
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDROM:
      {
         ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: CDROM connected"));
         if (i8AvailabilityReason != MPlay_fi_types::T_e8_MPlayAvailabilityReason__e8AR_SAME_MEDIA)
         {
            POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING)));
            _NewlyMediaPlayerSourceId = deiveTag;
         }
         break;
      }
#ifdef HMI_CDDA_SUPPORT
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA:
      {
         ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: CDDA connected"));
         if (i8AvailabilityReason != MPlay_fi_types::T_e8_MPlayAvailabilityReason__e8AR_SAME_MEDIA)
         {
            POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING)));
            _NewlyMediaPlayerSourceId = deiveTag;
         }
         break;
      }
#endif
      default:
      {
         ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: DeviceType: %d connected", u8DeviceType));
         break;
      }
   }
}


/**
 * UpdateDeviceConnection - called if device connection property is updated if device has no media files
 *
 * @param[in] void
 * @parm[in] void
 * @
 */

void MediaDeviceConnectionHandling::UpdateNoMediaFiles() const
{
   ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsStatus: UpdateNoMediaFiles"));
   POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND)));
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
   POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__POPUP_USB_NO_AUDIO_FILE)));
#endif
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
   POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND)));
#endif
}


/**
 * UpdateUnsupportedErrorStatus - called if device connection property updated with communication error
 *
 * @param[in] i8DeviceState ,i8DeviceType
 * @parm[in] void
 * @
 */
void MediaDeviceConnectionHandling::UpdateUnsupportedErrorStatus(int8 i8DeviceState , int8 i8DeviceType)
{
   ETG_TRACE_USR4(("UpdateUnsupportedErrorStatus :i8DeviceState :%d,i8DeviceType: %d", i8DeviceState, i8DeviceType));
   if (i8DeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPOD || i8DeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPHONE)
   {
      if (i8DeviceState == MPlay_fi_types::T_e8_MPlayDeviceStatus__e8DS_UNSUPPORTED || i8DeviceState == MPlay_fi_types::T_e8_MPlayDeviceStatus__e8DS_UNSUPPORTED_FILESYSTEM)
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND)));
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND)));
      }

      else if (i8DeviceState == MPlay_fi_types::T_e8_MPlayDeviceStatus__e8DS_UNSUPPORTED_PARTITION)
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND)));
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_POPUP_IPOD_ERROR_SYSIND)));
      }
   }
   else if ((i8DeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_USB) || (i8DeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_MTP))
   {
      if (i8DeviceState == MPlay_fi_types::T_e8_MPlayDeviceStatus__e8DS_UNSUPPORTED || i8DeviceState == MPlay_fi_types::T_e8_MPlayDeviceStatus__e8DS_UNSUPPORTED_FILESYSTEM)
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND)));
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND)));
      }
   }
#ifdef HMI_CDDA_SUPPORT
   else if (i8DeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDROM || i8DeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA)
#else
   else if (i8DeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDROM)
#endif
   {
      if (i8DeviceState == MPlay_fi_types::T_e8_MPlayDeviceStatus__e8DS_UNSUPPORTED || i8DeviceState == MPlay_fi_types::T_e8_MPlayDeviceStatus__e8DS_UNSUPPORTED_FILESYSTEM)
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING)));
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR)));
      }
   }

   else if (i8DeviceType == T_e8_MPlayDeviceType__e8DTY_UNSUPPORTED)
   {
      POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND)));
      POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND)));
   }
}


/**
 * UpdateCommunicationErrorStatus - called if device connection property is updated if device is unsupported
 *
 * @param[in] i8DeviceState ,i8DeviceType
 * @parm[in] void
 * @
 */
void MediaDeviceConnectionHandling::UpdateCommunicationErrorStatus(int8 i8DeviceState , int8 i8DeviceType)
{
   ETG_TRACE_USR4(("UpdateCommunicationErrorStatus :i8DeviceState :%d,i8DeviceType: %d", i8DeviceState, i8DeviceType));
   if (i8DeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPOD || i8DeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPHONE)
   {
      if (i8DeviceState == MPlay_fi_types::T_e8_MPlayDeviceStatus__e8DS_COMMUNICATION_ERROR)
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND)));
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_POPUP_IPOD_COM_ERROR_SYSIND)));
      }
   }
   else if ((i8DeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_USB) || (i8DeviceType == MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_MTP))
   {
      if (i8DeviceState == MPlay_fi_types::T_e8_MPlayDeviceStatus__e8DS_COMMUNICATION_ERROR)
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA_AUX__POPUP_USB_CHECK_SYSIND)));
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__POPUP_AUX_USB_ERROR_SYSIND)));
      }
   }
}


/**
 * getConnectedDeviceSize - called when new device is connected to update the devicesize.
 *
 * @param[in] deviceSize
 * @parm[out] bool
 * @
 */
bool MediaDeviceConnectionHandling::getConnectedDeviceSize(std::string& deviceSize)
{
   ETG_TRACE_USR4(("MediaDeviceConnectionHandling::getConnectedDeviceSize"));
   uint32 uiDeviceSize = 0;
   bool retValue = false;
   char buffer[MAX_BUFFER_SIZE];
   memset(buffer, 0, MAX_BUFFER_SIZE);
   for (uint8 u8Index = 0; u8Index < MPLAY_MAX_DEVICES; u8Index++)
   {
      if (_oDeviceList[u8Index].uiDeviceTag == u32GetActiveDeviceTag())
      {
         ETG_TRACE_USR4(("MediaDeviceConnectionHandling::u32GetActiveDeviceTag() is equal"));
         uiDeviceSize = _oDeviceList[u8Index].deviceSize / CONVERT_DEVICESIZE_KB_TO_MB;
         snprintf(buffer, MAX_BUFFER_SIZE - 1, "%d", uiDeviceSize);
         deviceSize = buffer;
         ETG_TRACE_USR4(("getConnectedDeviceSize = %d ", uiDeviceSize));
         retValue = true;
      }
   }
   return retValue;
}


/**
 * onMediaPlayerDeviceConnectionsError - called if device connection property is updated with error from media player
 *
 * @param[in] proxy
 * @parm[in] error
 * @return void
 */
void MediaDeviceConnectionHandling::onMediaPlayerDeviceConnectionsError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
      const boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsError >& /*error*/)
{
   ETG_TRACE_USR4(("onMediaPlayerDeviceConnectionsError received........"));
}


std::string MediaDeviceConnectionHandling::getActiveDeviceName(uint32 sHeaderIndex)
{
   std::string deviceName;
   deviceName.clear();
#ifdef SHOW_DEVICE_NAME_IN_HEADER_SUPPORT
   uint32 u32ActiveDeviceTag = MediaDatabinding::getInstance().getActiveDeviceTag();
   char deviceTag[MEDIA_HEADER_STRING_SIZE] = "";

   for (uint8 u8Index = 0; u8Index < MPLAY_MAX_DEVICES; u8Index++)
   {
      if (_oDeviceList[u8Index].uiDeviceTag == (int8)u32ActiveDeviceTag)
      {
         deviceName =  _oDeviceList[u8Index].sDeviceName;

         if (deviceName == "")
         {
            MediaUtils::convertToString(deviceTag, _oDeviceList[u8Index].uiDeviceTag);
            deviceName = static_cast<std::string>(getDeviceText(sHeaderIndex).GetCString()) + deviceTag;
         }
         break;
      }
   }
   if (deviceName == "") //For lower Avrcp version Device tag will not available from mediaplayer so The text will be displayed as BLUETOOTH AUDIO.
   {
      deviceName = getDeviceText(sHeaderIndex).GetCString();
   }

#else
   deviceName = getDeviceText(sHeaderIndex).GetCString();
#endif

   return deviceName;
}


Candera::String MediaDeviceConnectionHandling::getDeviceText(uint32 sHeaderIndex)
{
   Candera::String deviceName;
   switch (sHeaderIndex)
   {
      case  Media_Scenes_MEDIA__AUX__USB_MAIN :
      {
         deviceName = LANGUAGE_STRING(USB__MAIN_Headline, "USB");
         break;
      }
      case Media_Scenes_MEDIA__AUX__IPOD_MAIN:
      {
         deviceName = LANGUAGE_STRING(IPOD__MAIN_Headline, "IPOD");
         break;
      }
      case Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN:
      case Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT:
      case Media_Scenes_MEDIA_AUX__BTAUDIO_BROWSER:
      {
         deviceName =  LANGUAGE_STRING(BTAUDIO__MAIN_Headline , "BLUETOOTH AUDIO");
         break;
      }
      case Media_Scenes_MEDIA__CD_CDMP3_MAIN:
      {
         deviceName =  LANGUAGE_STRING(CDMP3__MAIN_Headline, "CD");
         break;
      }
      default :
      {
         deviceName = "";
      }
      break;
   }
   return deviceName;
}


/**
 * u32GetActiveDeviceTag() - returns current active device tag.
 * @param[in] none
 * @return uint32
 */

uint32 MediaDeviceConnectionHandling::u32GetActiveDeviceTag()
{
   uint32 u32ActiveDeviceTag = MEDIA_VIRTUAL_DEVICE_TAG;
   for (uint8 u8Index = 0; u8Index < MPLAY_MAX_DEVICES; u8Index++)
   {
      if (_oDeviceList[u8Index].bIsActiveSource)
      {
         u32ActiveDeviceTag =  _oDeviceList[u8Index].uiDeviceTag;
         _prevDeviceTag = u32ActiveDeviceTag;
         break;
      }
   }
   ETG_TRACE_USR4(("m_u32DeviceTag=%d", u32ActiveDeviceTag));
   return u32ActiveDeviceTag;
}


/**
 * onEjectOpticalDiscError - Disk slot info error .
 * @param[in] msg
 * @return bool
 */

void MediaDeviceConnectionHandling::onMediaPlayerOpticalDiscSlotStateError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerOpticalDiscSlotStateError >& /*error*/)
{
   ETG_TRACE_USR4(("onMediaPlayerOpticalDiscSlotStateError for CD "));
}


/**
 * onEjectOpticalDiscError - Disk slot info Status .
 * @param[in] msg
 * @return bool
 */

void MediaDeviceConnectionHandling::onMediaPlayerOpticalDiscSlotStateStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerOpticalDiscSlotStateStatus >& status)
{
   ETG_TRACE_USR4(("onMediaPlayerOpticalDiscSlotStateStatus for CD Slot %d ", status->getE8State()));
   ::mplay_shared_fi_types::T_e8_SlotStateOpticalDiscType deviceStatus = status->getE8State();

   switch (deviceStatus)
   {
      case ::mplay_shared_fi_types::T_e8_SlotStateOpticalDiscType__INSERTING:
      {
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING)));
         break;
      }
      case ::mplay_shared_fi_types::T_e8_SlotStateOpticalDiscType__INSERTED_CDERROR:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING)));
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR)));
         break;
      }
      case ::mplay_shared_fi_types::T_e8_SlotStateOpticalDiscType__EJECTING:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING)));
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR)));
#ifdef SHOW_CD_STATUS_OVERHEATED
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_OVERHEATED)));
#else
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS)));
#endif
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING)));
         break;
      }
      case ::mplay_shared_fi_types::T_e8_SlotStateOpticalDiscType__INSERTED_AUTOMATIC_CDAUDIO:
      case ::mplay_shared_fi_types::T_e8_SlotStateOpticalDiscType__INSERTED_AUTOMATIC_CDROM:
      case ::mplay_shared_fi_types::T_e8_SlotStateOpticalDiscType__INSERTED_AUTOMATIC_CDERROR:
      {
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_LOADING)));
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_EJECTING)));
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_READ_ERROR)));
#ifdef SHOW_CD_STATUS_OVERHEATED
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_OVERHEATED)));
#else
         POST_MSG((COURIER_MESSAGE_NEW(DeActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_CD_ABNORMAL_STATUS)));
#endif
         break;
      }
      default:
      {
         ETG_TRACE_USR4(("onMediaPlayerOpticalDiscSlotStateStatus::Default condition "));
         break;
      }
   }
}


void MediaDeviceConnectionHandling::registerObserver(IDeviceConnectionOserver* pObserver)
{
   if (pObserver != NULL)
   {
      std::vector<IDeviceConnectionOserver*>::iterator itr = ::std::find(_deviceConnectionObserver.begin(), _deviceConnectionObserver.end(), pObserver);
      if (itr == _deviceConnectionObserver.end())
      {
         _deviceConnectionObserver.push_back(pObserver);
      }
   }
}


void MediaDeviceConnectionHandling::deregisterObserver(IDeviceConnectionOserver* pObserver)
{
   std::vector<IDeviceConnectionOserver*>::iterator itr = _deviceConnectionObserver.begin();
   while (itr != _deviceConnectionObserver.end())
   {
      if (*itr == pObserver)
      {
         _deviceConnectionObserver.erase(itr);
         break;
      }
      itr++;
   }
}


void MediaDeviceConnectionHandling::notifyDeviceConnectionObserver()
{
   std::vector<IDeviceConnectionOserver*> deviceConnectionObserver = _deviceConnectionObserver;
   std::vector<IDeviceConnectionOserver*>::iterator itr = deviceConnectionObserver.begin();

   while (itr != deviceConnectionObserver.end())
   {
      if (*itr != NULL)
      {
         (*itr)->deviceConnectionStatus();
      }
      itr++;
   }
}


void MediaDeviceConnectionHandling::onActiveMediaDeviceError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::ActiveMediaDeviceError >& /*error*/)
{
   ETG_TRACE_USR4(("MediaDeviceConnectionHandling::onActiveMediaDeviceError received "));
}


void MediaDeviceConnectionHandling::onActiveMediaDeviceStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::ActiveMediaDeviceStatus >& status)
{
   ETG_TRACE_USR4(("MediaDeviceConnectionHandling::onActiveMediaDeviceStatus active device tag = %d isActive = %d", status->getU8DeviceTag(), status->getBDeviceActiveSource()));
}


uint32 MediaDeviceConnectionHandling::getNewlyInsertedDeviceTag()
{
   return _NewlyMediaPlayerSourceId;
}


void MediaDeviceConnectionHandling::resetNewlyInsertedDeviceTag()
{
   _NewlyMediaPlayerSourceId = 0;
}


void MediaDeviceConnectionHandling::setSpiActiveStatus(bool activeStatus)
{
   _spiActivestatus = activeStatus;
}


void MediaDeviceConnectionHandling::setActiveMediaDevice(uint32 deviceTag)
{
   _mediaPlayerProxy->sendActiveMediaDeviceSet(*this, deviceTag, true);
}


void MediaDeviceConnectionHandling::onMediaPlayerOpticalDiscCDInfoError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerOpticalDiscCDInfoError >& /*error*/)
{
   ETG_TRACE_USR4(("onMediaPlayerListChangeError"));
}


void MediaDeviceConnectionHandling::onMediaPlayerOpticalDiscCDInfoStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerOpticalDiscCDInfoStatus >& status)
{
   ETG_TRACE_USR4(("onMediaPlayerListChangeStatus "));
   ::mplay_shared_fi_types::T_e8_CDInfo l_DiskInfo = status->getCDInfo();

   ETG_TRACE_USR4((" Drive Version = %s ", l_DiskInfo.getDriveVersion().c_str()));
   ETG_TRACE_USR4((" Drive State = %d ", l_DiskInfo.getE8DeviceState()));
#ifdef SUPPORT_CD_INFO
   MediaDatabinding::getInstance().updateCDInfo(l_DiskInfo.getDriveVersion().c_str(), l_DiskInfo.getE8DeviceState());
#endif
}


} // namespace AppLogic
} // namespace App
