#include "PlayMode_Test.h"
using namespace ::MPlay_fi_types;


INSTANTIATE_TEST_CASE_P(
   PlayMode_getNextRepeatState,
   PlayMode_Test_getNextRepeatState,
   ::testing::Values
   (

      make_tuple(FOLDER_BASED_SELECTION , T_e8_MPlayRepeat__e8RPT_NONE, T_e8_MPlayRepeat__e8RPT_ALL),
      make_tuple(FOLDER_BASED_SELECTION , T_e8_MPlayRepeat__e8RPT_ONE, T_e8_MPlayRepeat__e8RPT_ALL),
      make_tuple(FOLDER_BASED_SELECTION , T_e8_MPlayRepeat__e8RPT_LIST, T_e8_MPlayRepeat__e8RPT_ONE),
      make_tuple(FOLDER_BASED_SELECTION , T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS, T_e8_MPlayRepeat__e8RPT_ONE),
      make_tuple(FOLDER_BASED_SELECTION, T_e8_MPlayRepeat__e8RPT_ALL, T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS),

      make_tuple(METADATA_BASED_SELECTION , T_e8_MPlayRepeat__e8RPT_NONE, T_e8_MPlayRepeat__e8RPT_ONE),
      make_tuple(METADATA_BASED_SELECTION , T_e8_MPlayRepeat__e8RPT_ONE, T_e8_MPlayRepeat__e8RPT_LIST),
      make_tuple(METADATA_BASED_SELECTION , T_e8_MPlayRepeat__e8RPT_LIST, T_e8_MPlayRepeat__e8RPT_ONE),
      make_tuple(METADATA_BASED_SELECTION , T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS, T_e8_MPlayRepeat__e8RPT_ONE),
      make_tuple(METADATA_BASED_SELECTION, T_e8_MPlayRepeat__e8RPT_ALL, T_e8_MPlayRepeat__e8RPT_ONE),

      make_tuple(UNKNOWN_SELECTION , T_e8_MPlayRepeat__e8RPT_NONE, T_e8_MPlayRepeat__e8RPT_ONE),
      make_tuple(UNKNOWN_SELECTION , T_e8_MPlayRepeat__e8RPT_ONE, T_e8_MPlayRepeat__e8RPT_LIST),
      make_tuple(UNKNOWN_SELECTION , T_e8_MPlayRepeat__e8RPT_LIST, T_e8_MPlayRepeat__e8RPT_ONE),
      make_tuple(UNKNOWN_SELECTION , T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS, T_e8_MPlayRepeat__e8RPT_ONE),
      make_tuple(UNKNOWN_SELECTION, T_e8_MPlayRepeat__e8RPT_ALL, T_e8_MPlayRepeat__e8RPT_ONE)

   )
);

TEST_P(PlayMode_Test_getNextRepeatState, getNextRepeatState_repeatStatus_valid)
{
   ::MPlay_fi_types::T_e8_MPlayRepeat actualRepeatStatus;
   actualRepeatStatus = _spyPlayModeObj.getNextRepeatState(_currentPlayingSelection, _repeatStatus);
   EXPECT_EQ(_expectedRepeatStatus, actualRepeatStatus);

}


INSTANTIATE_TEST_CASE_P(
   PlayMode_getNextRandomState,
   PlayMode_Test_getNextRandomState,
   ::testing::Values
   (
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayMode__e8PBM_RANDOM),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayMode__e8PBM_NORMAL)
   )
);

TEST_P(PlayMode_Test_getNextRandomState, getNextRandomState_randomstatus_valid)
{
   ::MPlay_fi_types::T_e8_MPlayMode actualRandomStatus;
   actualRandomStatus = _spyPlayModeObj.getNextRandomState(_randomStatus);
   EXPECT_EQ(_expectedRandomStatus, actualRandomStatus);

}

INSTANTIATE_TEST_CASE_P(
   PlayMode_updateRandomRepeatState,
   PlayMode_Test_updateRandomRepeatStateScope1,
   ::testing::Values
   (
      // Test Values;
      //	- First Param -- Play mode (Input)
      //	- Seconds Param -- Repeat mode (Input)
      //	- Third Param -- Browser Type (Input)
      //	- Fourth Param -- Repeat button status (Expected Output)
      //	- Fifth Param -- Random button status (Expected Output)
      //	- Sixth Param -- RepeatRandomText status (Expected Output)

      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_NONE, FOLDER_BASED_SELECTION, true, false, ""),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_NONE, METADATA_BASED_SELECTION, true, false, "Random All"),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_NONE, UNKNOWN_SELECTION, true, false, ""),

      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_ONE, FOLDER_BASED_SELECTION, true, true, "Repeat Track"),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_ONE, METADATA_BASED_SELECTION, true, true, "Repeat Track"),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_ONE, UNKNOWN_SELECTION, true, true, "Repeat Track"),

      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_LIST, FOLDER_BASED_SELECTION, true, false, ""),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_LIST, METADATA_BASED_SELECTION, true, false, "Random All"),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_LIST, UNKNOWN_SELECTION, true, false, ""),

      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS, FOLDER_BASED_SELECTION, true, true, "Random Folder"),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS, METADATA_BASED_SELECTION, true, false, "Random All"),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS, UNKNOWN_SELECTION, true, false, ""),


      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_ALL, FOLDER_BASED_SELECTION, true, false , "Random All"),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_ALL, METADATA_BASED_SELECTION, true, false, ""),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_ALL, UNKNOWN_SELECTION, true, false, ""),


      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_NONE, FOLDER_BASED_SELECTION, false, false, ""),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_NONE, METADATA_BASED_SELECTION, false, false, ""),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_NONE, UNKNOWN_SELECTION, false, false, ""),

      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_ONE, FOLDER_BASED_SELECTION, false, true, "Repeat Track"),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_ONE, METADATA_BASED_SELECTION, false, true, "Repeat Track"),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_ONE, UNKNOWN_SELECTION, false, true, "Repeat Track"),

      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_LIST, FOLDER_BASED_SELECTION, false, false, ""),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_LIST, METADATA_BASED_SELECTION, false, false, ""),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_LIST, UNKNOWN_SELECTION, false, false, ""),

      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS, FOLDER_BASED_SELECTION, false, true, "Repeat Folder"),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS, METADATA_BASED_SELECTION, false, false, ""),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS, UNKNOWN_SELECTION, false, false, ""),


      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_ALL, FOLDER_BASED_SELECTION, false, false, ""),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_ALL, METADATA_BASED_SELECTION, false, false, ""),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_ALL, UNKNOWN_SELECTION, false, false, "")

   )
);

TEST_P(PlayMode_Test_updateRandomRepeatStateScope1, updateRandomRepeatState_valid)
{
   bool isRepeatActive ;
   bool isRandomActive ;
   std::string _actualRandomRepeatText = "";

   _spyPlayModeObj.updateRandomRepeatState(_randomStatus, _repeatStatus , _currentPlayingSelection);

   isRepeatActive = _spyPlayModeObj.getRepeatState();
   isRandomActive = _spyPlayModeObj.getRamdonState();
   _actualRandomRepeatText = _spyPlayModeObj.getRadomRepeatText().GetCString();


   EXPECT_EQ(_expectedRepeatStatus, isRepeatActive);
   EXPECT_EQ(_expectedRandomStatus , isRandomActive);
   EXPECT_EQ(_expectedRepeatRandomText, _actualRandomRepeatText);

}



INSTANTIATE_TEST_CASE_P(
   PlayMode_updateRandomRepeatState,
   PlayMode_Test_updateRandomRepeatStateScope2,
   ::testing::Values
   (
      // Test Values;
      //	- First Param -- Play mode (Input)
      //	- Seconds Param -- Repeat mode (Input)
      //	- Third Param -- Browser Type (Input)
      //	- Fourth Param -- Repeat button status (Expected Output)
      //	- Fifth Param -- Random button status (Expected Output)
      //	- Sixth Param -- RandomText Status (Expected Output)
      //	- Seventh Param -- RepeatText Status (Expected Output)

      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_NONE, FOLDER_BASED_SELECTION, true, false, "Random ON" , ""),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_NONE, METADATA_BASED_SELECTION, true, false,  "Random ON" , ""),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_NONE, UNKNOWN_SELECTION, true, false, "Random ON" , ""),

      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_ONE, FOLDER_BASED_SELECTION, true, true,  "Random ON" , "Repeat Track"),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_ONE, METADATA_BASED_SELECTION, true, true,  "Random ON" , "Repeat Track"),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_ONE, UNKNOWN_SELECTION, true, true, "Random ON" , "Repeat Track"),

      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_LIST, FOLDER_BASED_SELECTION, true, false, "Random ON" , ""),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_LIST, METADATA_BASED_SELECTION, true, false, "Random ON" , ""),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_LIST, UNKNOWN_SELECTION, true, false,  "Random ON" , ""),

      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS, FOLDER_BASED_SELECTION, true, true,  "Random ON" , "Repeat Folder"),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS, METADATA_BASED_SELECTION, true, false, "Random ON" , ""),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS, UNKNOWN_SELECTION, true, false,  "Random ON" , ""),


      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_ALL, FOLDER_BASED_SELECTION, true, false , "Random ON" , ""),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_ALL, METADATA_BASED_SELECTION, true, false, "Random ON" , ""),
      make_tuple(T_e8_MPlayMode__e8PBM_RANDOM, T_e8_MPlayRepeat__e8RPT_ALL, UNKNOWN_SELECTION, true, false, "Random ON" , ""),


      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_NONE, FOLDER_BASED_SELECTION, false, false,  "" , ""),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_NONE, METADATA_BASED_SELECTION, false, false, "" , ""),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_NONE, UNKNOWN_SELECTION, false, false, "" , ""),

      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_ONE, FOLDER_BASED_SELECTION, false, true, "" , "Repeat Track"),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_ONE, METADATA_BASED_SELECTION, false, true,  "" , "Repeat Track"),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_ONE, UNKNOWN_SELECTION, false, true,  "" , "Repeat Track"),

      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_LIST, FOLDER_BASED_SELECTION, false, false, "" , ""),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_LIST, METADATA_BASED_SELECTION, false, false,  "" , ""),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_LIST, UNKNOWN_SELECTION, false, false,  "" , ""),

      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS, FOLDER_BASED_SELECTION, false, true, "" , "Repeat Folder"),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS, METADATA_BASED_SELECTION, false, false,  "" , ""),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS, UNKNOWN_SELECTION, false, false, "" , ""),


      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_ALL, FOLDER_BASED_SELECTION, false, false, "" , ""),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_ALL, METADATA_BASED_SELECTION, false, false, "" , ""),
      make_tuple(T_e8_MPlayMode__e8PBM_NORMAL, T_e8_MPlayRepeat__e8RPT_ALL, UNKNOWN_SELECTION, false, false, "" , "")

   )
);

TEST_P(PlayMode_Test_updateRandomRepeatStateScope2, updateRandomRepeatState_valid)
{
   bool isRepeatActive ;
   bool isRandomActive ;
   std::string _actualRepeatText = "";
   std::string _actualRandomText = "";


   _spyPlayModeObj.updateRandomRepeatState(_randomStatus, _repeatStatus , _currentPlayingSelection);

   isRepeatActive = _spyPlayModeObj.getRepeatState();
   isRandomActive = _spyPlayModeObj.getRamdonState();
   _actualRepeatText = _spyPlayModeObj.getRepeatText().GetCString();
   _actualRandomText = _spyPlayModeObj.getRadomText().GetCString();

   EXPECT_EQ(_expectedRepeatStatus, isRepeatActive);
   EXPECT_EQ(_expectedRandomStatus , isRandomActive);
   EXPECT_EQ(_expectedRandomText, _actualRandomText);
   EXPECT_EQ(_expectedRepeatText, _actualRepeatText);

}

