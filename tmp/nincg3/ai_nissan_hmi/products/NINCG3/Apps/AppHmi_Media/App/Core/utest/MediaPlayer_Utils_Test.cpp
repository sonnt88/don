/**
*  @file   <MediaPlayer_Utils_Test.cpp>
*  @author <ECV> - <bvi1cob>
*  @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
*  @addtogroup <AppHmi_media>
*  @{
*/

#include "MediaPlayer_Utils_Test.h"

//Initialize "MediaUtilsTest_ParamInvalidPath" with values
INSTANTIATE_TEST_CASE_P(
   MediaUtils_ParamInvalidPath,
   MediaUtilsTest_ExtractFileName_InValid,
   ::testing::Values
   (
      //Test Values where First Param is the Invalid FilePath
      // TODO: Test for Eastern European/East Asian Character set
      make_tuple(""),
      make_tuple("             "),
      make_tuple("[[[[[[[[["),
      make_tuple("invalidfilepath"),
      make_tuple("GNU- GNU's Not Unix"),
      make_tuple("Z:\ai_nissan_hmi\products"),
      make_tuple("/"),
      make_tuple("/////////"),
      make_tuple("/mnt/media/\//\/"),
      make_tuple("sender@registry-A.registry-1.organization-X")
   )
);

// using macro TEST_P (Parameterized tests)
// Test for method bExtractFileName's behavior when passed with invalid path
TEST_P(MediaUtilsTest_ExtractFileName_InValid, InvalidPath)
{
   std::string strFileName;
   MediaUtils utils;
   EXPECT_FALSE(utils.bExtractFileName(_strInvalidPath, strFileName));
}


//Initialize "MediaUtilsTest_ParamValidPath" with values
INSTANTIATE_TEST_CASE_P(
   MediaUtils_ParamValidPath,
   MediaUtilsTest_ExtractFileName_Valid,
   ::testing::Values
   (
      //Test Values where First Param is the Valid FilePath; Second Param is FileName expected to be extracted
      // TODO: Test for Eastern European/East Asian Character set
      make_tuple("~/samba/views/nincg3", "nincg3"),
      make_tuple("$HOME/songs/abc.mp3", "abc.mp3"),
      make_tuple("/opt/tooling/rootfs", "rootfs"),
      make_tuple("/mnt/media/wave.wav", "wave.wav"),
      make_tuple("root@172.17.0.11:/opt/bosch/base/bin", "bin"),
      make_tuple("/mnt/media/abc/", "abc"),
      make_tuple("/abc/", "abc"),
      make_tuple("abc/", "abc"),
      // Adding some URL's/URI's
      make_tuple("https://inside-docupedia.bosch.com/confluence/display/CMAIDomainSoftware/Unit-Testing", "Unit-Testing"),
      make_tuple("http://www.myu.edu/org/admin/people#andy", "people#andy")
   )
);

// Test for method bExtractFileName's behavior when passed with Valid path
TEST_P(MediaUtilsTest_ExtractFileName_Valid, ValidPath)
{
   std::string strFileName;
   MediaUtils utils;
   EXPECT_TRUE(utils.bExtractFileName(_strValidPath, strFileName));
   EXPECT_EQ(strFileName, _strExpectedFileName);
}


//Initialize "MediaUtilsTest_ParamLessOrEqualStringLengths" with values
INSTANTIATE_TEST_CASE_P(
   MediaUtils_ParamStringLengths,
   MediaUtilsTest_CheckStringLengthNCopy,
   ::testing::Values
   (
      //Test Values where First Param is the Full Metadata;
      //Second Param is Length beyond which the Metadata infor must be truncated
      //Third param is the expected value
      //Forth param says if trimmed(true) or not (false)
      make_tuple("", 0, "", false),                            //null
      make_tuple("999999999", 8, "99999999", true),           //lesser
      make_tuple("0000000000", 0, "", true),                  //lesser-to null
      make_tuple("1", 1, "1", false),                          //equal
      make_tuple("123456789", 15, "123456789", false)       //greater
   )
);

// Test for method bCheckStringLengthNCopy's behavior when passed with Metadata String whose length Equals the supplied length
TEST_P(MediaUtilsTest_CheckStringLengthNCopy, BoundaryClass)
{
   std::string strTrimmedData;

   EXPECT_EQ(_isTrimmed, _utils.bCheckStringLengthNCopy(_strMetaData, strTrimmedData, _uInputLength));
   EXPECT_EQ(strTrimmedData, _strTruncatedMetaData);
}


//Initialize "MediaUtilsTest_ParamValues" with values
INSTANTIATE_TEST_CASE_P(
   MediaUtils_ParamTest,
   MediaUtilsTest_ConvertSecToHourMinSec,
   ::testing::Values
   (
      // Test Values;
      //	- First Param -- Given Time in Seconds
      //	- Seconds Param -- Post Calculations - Expected Time in Hours
      //	- Third Param -- Post Calculations - Expected Time in Minutes
      //	- Fourth Param -- Post Calculations - Expected Time in Seconds

      make_tuple(0, 0, 0, 0),           //Null, at lower
      make_tuple(1, 0, 0, 1),           //above lower
      make_tuple(3661, 1, 1, 1),        //above lower
      make_tuple(75, 0, 1, 15),         //Valid
      make_tuple(86399, 23, 59, 59),    //below higher boundary
      make_tuple(86400, 24, 0, 0),      //at higher boundary

      //Random, above higher
      make_tuple(3333, 0, 55, 33),
      make_tuple(5000, 1, 23, 20),
      make_tuple(10000, 2, 46, 40),
      make_tuple(85000, 23, 36, 40),
      make_tuple(172800, 48, 0, 0),
      make_tuple(1000000, 277, 46, 40),

      //boundary checks
      make_tuple(59, 0, 0, 59),
      make_tuple(60, 0, 1, 0),
      make_tuple(61, 0, 1, 1),
      make_tuple(3599, 0, 59, 59),
      make_tuple(3600, 1, 0, 0),
      make_tuple(3601, 1, 0, 1)
   )
);

// Test for method vConvertSecToHourMinSec's calculations for different values of seconds
TEST_P(MediaUtilsTest_ConvertSecToHourMinSec, BoundaryClass)
{
   unsigned int uHour = 0u;
   unsigned int uMinutes = 0u;
   unsigned int uSeconds = _uGivenSec;

   _utilsObj.vConvertSecToHourMinSec(uHour, uMinutes, uSeconds);
   EXPECT_EQ(uHour, _uExpectHr);
   EXPECT_EQ(uMinutes, _uExpectMin);
   EXPECT_EQ(uSeconds, _uExpectSec);
}


//Initialize "MediaUtilsTest_FolderUp_Valid" with values
INSTANTIATE_TEST_CASE_P(
   MediaUtils_ParamFolderUp,
   MediaUtilsTest_FolderUp_Valid,
   ::testing::Values
   (
      //Test Values where First Param is the current folder path; Second Param is expected folder path after folder up

      make_tuple("~/samba/views/nincg3/", "~/samba/views/"),
      make_tuple("$HOME/songs/abc   /", "$HOME/songs/"),
      make_tuple("/", "/"),
      make_tuple("abc", "abc"),
      make_tuple("", ""),
      make_tuple("/mnt/media/wave.wav/", "/mnt/media/"),
      make_tuple("root@172.17.0.11:/opt/bosch/base/bin/", "root@172.17.0.11:/opt/bosch/base/")
   )
);

// Test for method goOneFolderUp behavior when folder path is passed
TEST_P(MediaUtilsTest_FolderUp_Valid, FolderUp)
{
   std::string _strNewFolderPath = "";
   _utilsObj.goOneFolderUp(_strFolderPath, _strNewFolderPath);
   EXPECT_EQ(_strNewFolderPath, _strExpectedNewFolderPath);
}


//Initialize "MediaPlayerUtilsTest_CheckFormatTime" with values
INSTANTIATE_TEST_CASE_P(
   MediaPlayerUtils_CheckFormatTime,
   MediaPlayerUtilsTest_CheckFormatTime,
   ::testing::Values
   (

      // Test Values;
      //	- First Param -- Given Time in Hours
      //	- Seconds Param -- Given Time in Minutes
      //	- Third Param -- Given Time in Seconds
      //	- Fourth Param -- Post Format - Formatted Time

      //Testcases only to check Format of Hours
      make_tuple(10U, 0U, 0U, "10:00:00"),
      make_tuple(24U, 0U, 0U, "24:00:00"),
      make_tuple(1000U, 0U, 0U, "1000:00:00"),
      //Testcases only to check Format of Minutes
      make_tuple(0U, 0U, 0U, "0:00"),
      make_tuple(0U, 10U, 0U, "10:00"),
      make_tuple(0U, 59U, 0U, "59:00"),
      make_tuple(0U, 9U, 0U, "9:00"),
      // invalid usecase and boundary check
      make_tuple(0U, 59U, 60U, ""),
      make_tuple(0U, 59U, 61U, ""),
      make_tuple(0U, 60U, 0U, ""),
      make_tuple(0U, 61U, 0U, ""),
      make_tuple(1000U, 60U, 75U, "")
   )
);

// Test for method vFormatTime's behavior when passed with Metadata String whose length is Lesser than or Equal to the supplied length
TEST_P(MediaPlayerUtilsTest_CheckFormatTime, bCheckTimeFormat_True)
{
   char strFormattedTime[40];
   unsigned int uHour = _uGivenHr;
   unsigned int uMinutes = _uGivenMin;
   unsigned int uSeconds = _uGivenSec;

   _utilsObj.vFormatTime(strFormattedTime, uHour, uMinutes, uSeconds);

   EXPECT_STREQ(_strExpectedTimeFormat.c_str(), strFormattedTime);
}


INSTANTIATE_TEST_CASE_P(
   MediaPlayerUtils_checkTheFolderPath,
   MediaUtilsTest_checkTheFolderPath_Valid,
   ::testing::Values
   (
      make_tuple("", ""),
      make_tuple("/mnt/media/wave.wav/", "/mnt/media/wave.wav/"),
      make_tuple("/mnt/media/wave.wav", "/mnt/media/wave.wav/")
   )
);

TEST_P(MediaUtilsTest_checkTheFolderPath_Valid, checkTheFolderPath_valid)
{
   std::string actualNewFolderPath = "";
   _utilsObj.checkTheFolderPath(_strFolderPath, actualNewFolderPath);
   EXPECT_EQ(_strExpectedNewFolderPath, actualNewFolderPath);
}


INSTANTIATE_TEST_CASE_P(
   MediaPlayerUtils_convertToString,
   MediaUtilsTest_convertToString_Valid,
   ::testing::Values
   (

      make_tuple(0U, "0"),
      make_tuple(50U, "50"),
      make_tuple(100U, "100")

   )
);

// Test for method vFormatTime's behavior when passed with Metadata String whose length is Lesser than or Equal to the supplied length
TEST_P(MediaUtilsTest_convertToString_Valid, convertToString_Valid)
{
   char strActual[40];
   unsigned int uValue = _uGivenIntValue;

   _utilsObj.convertToString(strActual, uValue);

   EXPECT_STREQ(_strExpected.c_str(), strActual);
}


INSTANTIATE_TEST_CASE_P(
   MediaUtils_ConvertHallDeviceType,
   MediaUtils_Test_ConvertHallDeviceType,
   ::testing::Values
   (

      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH , SOURCE_BT),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_USB , SOURCE_USB),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_MTP , SOURCE_USB),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPOD , SOURCE_IPOD),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPHONE , SOURCE_IPOD),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDROM , SOURCE_CDMP3),
      //make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA , SOURCE_CD),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_UNKNOWN , SOURCE_NOT_DEF)
   )
);

TEST_P(MediaUtils_Test_ConvertHallDeviceType, ConvertHallDeviceType_deviceType_valid)
{
   uint32 actualDeviceType;
   actualDeviceType = _mediaUtilsObj.ConvertHallDeviceType(_deviceType);
   EXPECT_EQ(_expecteddeviceType, actualDeviceType);
}


INSTANTIATE_TEST_CASE_P(
   MediaUtils_FooterTextForBrowser,
   MediaUtils_Test__FooterTextForBrowser_withValidParams,
   ::testing::Values
   (
      make_tuple(0, 0, "0/0"),
      make_tuple(0, 1, "0/1"),
      make_tuple(1, 0, "1/0"),
      make_tuple(1, 1, "1/1"),
      make_tuple(0, 3, "0/3"),
      make_tuple(0, 5, "0/5"),
      make_tuple(0, 7, "0/7"),
      make_tuple(0, 9, "0/9"),
      make_tuple(0, 11, "0/11"),
      make_tuple(1, 2, "1/2"),
      make_tuple(1, 4, "1/4"),
      make_tuple(1, 6, "1/6"),
      make_tuple(1, 8, "1/8"),
      make_tuple(1, 10, "1/10"),
      make_tuple(1, 12, "1/12"),
      make_tuple(6, 20, "6/20"),
      make_tuple(11, 31, "11/31"),
      make_tuple(33, 42, "33/42"),
      make_tuple(24, 55, "24/55"),
      make_tuple(59, 77, "59/77"),
      make_tuple(6, 5, "6/5"),
      make_tuple(6, 6, "6/6"),
      make_tuple(6, 7, "6/7"),
      make_tuple(7, 7, "7/7"),
      make_tuple(100, 100, "100/100"),
      make_tuple(0, 4294967294, "0/4294967294"),
      make_tuple(99, 4294967294, "99/4294967294"),
      make_tuple(999, 4294967294, "999/4294967294"),
      make_tuple(9999, 4294967294, "9999/4294967294"),
      make_tuple(0, 4294967295, "0/4294967295"),
      make_tuple(100, 4294967295, "100/4294967295"),
      make_tuple(1000, 4294967295, "1000/4294967295"),
      make_tuple(10000, 4294967295, "10000/4294967295")
   )
);

TEST_P(MediaUtils_Test__FooterTextForBrowser_withValidParams, FooterTextForBrowser_inSupportedRangeParams_valid)
{
   std::string strActualFocusedIndexWithListSize = MediaUtils::FooterTextForBrowser(_focussedIndex, _listSize);
   EXPECT_EQ(_strExpectedFocusedIndexWithListSize, strActualFocusedIndexWithListSize);
}


INSTANTIATE_TEST_CASE_P(
   MediaUtils_FooterTextForBrowser,
   MediaUtils_Test__FooterTextForBrowser_withInvalidParams,
   ::testing::Values
   (
      make_tuple(-1, -1, "-1/-1"),
      make_tuple(-1, 0, "-1/0"),
      make_tuple(-1, 1, "-1/1"),
      make_tuple(0, -1, "0/-1"),
      make_tuple(1, -1, "1/-1"),
      make_tuple(100, 4294967296, "100/4294967296"),
      make_tuple(1000, 4294967296, "1000/4294967296"),
      make_tuple(-1, 4294967296, "-1/4294967296"),
      make_tuple(0, 4294967296, "0/4294967296"),
      make_tuple(1, 4294967296, "1/4294967296"),
      make_tuple(10000, 4294967297, "10000/4294967297"),
      make_tuple(100000, 4294967297, "100000/4294967297"),
      make_tuple(-1, 4294967297, "-1/4294967297"),
      make_tuple(0, 4294967297, "0/4294967297"),
      make_tuple(1, 4294967297, "1/4294967297")
   )
);

TEST_P(MediaUtils_Test__FooterTextForBrowser_withInvalidParams, FooterTextForBrowser_outOfSupportedRangeParams_invalid)
{
   std::string strActualFocusedIndexWithListSize = MediaUtils::FooterTextForBrowser(_focussedIndex, _listSize);
   EXPECT_NE(_strExpectedFocusedIndexWithListSize, strActualFocusedIndexWithListSize);
}
