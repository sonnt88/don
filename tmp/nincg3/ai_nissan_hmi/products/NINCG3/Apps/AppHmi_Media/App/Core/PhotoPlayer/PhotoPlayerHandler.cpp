/******************************************************************
*FILE: PhotoPlayerHandler.cpp
*SW-COMPONENT: AppHmi_media
*DESCRIPTION: Source file of handler for PhotoPlayer feature.
*             This handler monitor screens/image list for PhotoPlayer Cover and Full Screen
*COPYRIGHT: © 2016 Robert Bosch GmbH
*
*The reproduction, distribution and utilization of this file as
*well as the communication of its contents to others without express
*authorization is prohibited. Offenders will be held liable for the
*payment of damages. All rights reserved in the event of the grant
*of a patent, utility model or design.
******************************************************************/

#include "hall_std_if.h"
#include "PhotoPlayerHandler.h"
#include "MediaDatabinding.h"
//#include "Core/SourceSwitch/MediaSourceHandling.h"
#include "Core/Utils/MediaProxyUtility.h"
//#include "App/datapool/MediaDataPoolConfig.h"
//#include "vehicle_main_fi_typesConst.h"
#include <stdio.h>
#include <CgiExtensions/ImageLoader.h>
#include <Widgets/generated/WidgetsGenericTypes.h>

//#define MIDW_COMMON_S_IMPORT_INTERFACE_UTF8_SORT
//#include "midw_common_if.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL_PHOTO_PLAYER
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::PhotoPlayerHandler::
#include "trcGenProj/Header/PhotoPlayerHandler.cpp.trc.h"
#endif

namespace App {
namespace Core {
//using namespace ::vehicle_main_fi_types;
using namespace ::mplay_MediaPlayer_FI;
using namespace ::MPlay_fi_types;

/**
 * @Constructor
 */
PhotoPlayerHandler::PhotoPlayerHandler(::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& mediaPlayerProxy, IPlayScreen* pPlayScreen)
   : _mediaPlayerProxy(mediaPlayerProxy)
   , _iPlayScreen(*pPlayScreen)
   , _isGettingListDataProvider(false)
   , _responsedListState(RLS_IDLE)
   , _currentPhotoPlayerScreen(PPS_BROWSER)
   , _windowStartIndex(0)
   , _nextListWindowStartIndex(0)
   , _focussedIndex(0)
   , _isNextShifting(false)
   , _activeDeviceType(0)
   , _activeDeviceTag(0)
   , _slideShowPosition(0)
{
   ETG_I_REGISTER_FILE();
   ETG_TRACE_USR4(("PhotoPlayerHandler Constructor"));

   _currentWindowList._windowStartIndex = 0;
   _currentWindowList._windowSize = 0;

   _currenEmptytWindowList._windowStartIndex = 0;
   _currenEmptytWindowList._windowSize = 0;

   _requestedList._startIndex = 0;
   _requestedList._bufferSize = 0;

   _responsedList._listHandle = 0;
   _responsedList._startIndex = 0;
   _responsedList._totalListSize = 0;
   _responsedList._oMediaObjects.clear();

   _slideShowNextPrevSkip._nextPrevSkipState = SS_NoSkip;
   _slideShowNextPrevSkip._u8NextPrevSkipCount = 1;

   ListRegistry::s_getInstance().addListImplementation(LIST_ID_COVER_PLAYER_IMAGE, this);
}


/**
 * @Destructor
 */
PhotoPlayerHandler::~PhotoPlayerHandler()
{
   ETG_TRACE_USR4(("PhotoPlayerHandler Destructor"));

   _mediaPlayerProxy.reset();
   _isGettingListDataProvider = false;
   _responsedListState = RLS_IDLE;
   _currentPhotoPlayerScreen = PPS_BROWSER;
   _windowStartIndex = 0;
   _nextListWindowStartIndex = 0;
   _focussedIndex = 0;
   _isNextShifting = false;
   _activeDeviceType = 0;
   _activeDeviceTag = 0;
   _slideShowPosition = 0;

   _currentWindowList._windowStartIndex = 0;
   _currentWindowList._windowSize = 0;

   _currenEmptytWindowList._windowStartIndex = 0;
   _currenEmptytWindowList._windowSize = 0;

   _requestedList._startIndex = 0;
   _requestedList._bufferSize = 0;

   _responsedList._listHandle = 0;
   _responsedList._startIndex = 0;
   _responsedList._totalListSize = 0;
   _responsedList._oMediaObjects.clear();

   _slideShowNextPrevSkip._nextPrevSkipState = SS_NoSkip;
   _slideShowNextPrevSkip._u8NextPrevSkipCount = 1;

   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_COVER_PLAYER_IMAGE);
}


/**
 * registerProperties - Trigger property registration to mediaplayer,  called from MediaHall class
 * @param[in] proxy
 * @parm[in] stateChange - state change service for corrosponding  proxy
 * @return void
 */
void PhotoPlayerHandler::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_mediaPlayerProxy && _mediaPlayerProxy == proxy)
   {
      _mediaPlayerProxy->sendMediaPlayerListChangeUpReg(*this);
   }
}


/**
 * deregisterProperties - Trigger property deregistration to mediaplayer, called from MediaHall class
 * @param[in] proxy
 * @parm[in] stateChange - state change service for corrsponding proxy
 * @return void
 */
void PhotoPlayerHandler::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_mediaPlayerProxy && _mediaPlayerProxy == proxy)
   {
      _mediaPlayerProxy->sendMediaPlayerListChangeRelUpRegAll();
   }
}


/**
 * onMediaPlayerListChangeError - function called whenever mediaplayer error while updating the list during indexing
 *
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void PhotoPlayerHandler::onMediaPlayerListChangeError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< MediaPlayerListChangeError >& /*error*/)
{
   ETG_TRACE_USR4(("onMediaPlayerListChangeError"));
}


/**
 * onMediaPlayerListChangeStatus - function called whenever mediaplayer updates the list during indexing
 *
 * @param[in] proxy
 * @param[in] status
 * @return void
 */
void PhotoPlayerHandler::onMediaPlayerListChangeStatus(const ::boost::shared_ptr<Mplay_MediaPlayer_FIProxy>& /*proxy*/,
      const ::boost::shared_ptr<MediaPlayerListChangeStatus>& status)
{
   ETG_TRACE_USR4(("onMediaPlayerListChangeStatus status->getU32ListHandle() = %d status->getU32ListSize() = %d", status->getU32ListHandle(), status->getU32ListSize()));

   if (_responsedList._listHandle == status->getU32ListHandle())
   {
      if ((status->getE8Change() == T_e8_MPlayChange__e8LCH_CONTENT_CHANGED)
            && (status->getU32ListSize() != _responsedList._totalListSize)
            && (!MediaProxyUtility::getInstance().isIndexingDone(_activeDeviceTag)))
      {
         _responsedList._totalListSize = status->getU32ListSize();
         if ((_responsedList._totalListSize > 0) && (_mediaPlayerProxy))
         {
            ETG_TRACE_USR4(("onMediaPlayerListChangeStatus - sendRequestMediaPlayerIndexedListSliceStart - _responsedList._listHandle: %d, _requestedList._startIndex: %d, _requestedList._bufferSize: %d", _responsedList._listHandle, _requestedList._startIndex, _requestedList._bufferSize));
            _mediaPlayerProxy->sendRequestMediaPlayerIndexedListSliceStart(*this, _responsedList._listHandle, _requestedList._startIndex, _requestedList._bufferSize);
         }
      }
   }
}


/**
 * onCreateMediaPlayerIndexedListError - function called when mediaplayer gets an error during the method call of createMediaPlayerIndexedListStart.
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void PhotoPlayerHandler::onCreateMediaPlayerIndexedListError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& proxy, const boost::shared_ptr< CreateMediaPlayerIndexedListError >& error)
{
   (void)proxy;
   (void)error;
   ETG_TRACE_USR4(("onCreateMediaPlayerIndexedListError"));

   _responsedList._startIndex = 0;
   _responsedList._totalListSize = 0;

   _responsedListState = RLS_IDLE;
   POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
   POST_MSG((COURIER_MESSAGE_NEW(ListPopulateReqMsg)(1, LIST_ID_COVER_PLAYER_IMAGE)));
}


/**
 * onCreateMediaPlayerIndexedListResult - function called when mediaplayer gets a result during the method call of createMediaPlayerIndexedListStart.
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void PhotoPlayerHandler::onCreateMediaPlayerIndexedListResult(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& proxy, const boost::shared_ptr< CreateMediaPlayerIndexedListResult >& result)
{
   (void)proxy;

   _responsedList._listHandle = result->getU32ListHandle();
   _responsedList._startIndex = _requestedList._startIndex;
   _responsedList._totalListSize = result->getU32ListSize();

   ETG_TRACE_USR4(("onCreateMediaPlayerIndexedListResult - _responsedList._listHandle: %d, _responsedList._totalListSize: %d", _responsedList._listHandle, _responsedList._totalListSize));

   if (_responsedList._totalListSize > 0)
   {
      if (_mediaPlayerProxy)
      {
         ETG_TRACE_USR4(("onCreateMediaPlayerIndexedListResult - sendRequestMediaPlayerIndexedListSliceStart - _responsedList._listHandle: %d, _requestedList._startIndex: %d, _requestedList._bufferSize: %d", _responsedList._listHandle, _requestedList._startIndex, _requestedList._bufferSize));
         _mediaPlayerProxy->sendRequestMediaPlayerIndexedListSliceStart(*this, _responsedList._listHandle, _requestedList._startIndex, _requestedList._bufferSize);
      }
   }
   else
   {
      _responsedListState = RLS_IDLE;
      POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
      POST_MSG((COURIER_MESSAGE_NEW(ListPopulateReqMsg)(1, LIST_ID_COVER_PLAYER_IMAGE)));
   }
}


/**
 * onRequestMediaPlayerIndexedListSliceError - function called when mediaplayer gets an error during the method call of createMediaPlayerIndexedListStart.
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void PhotoPlayerHandler::onRequestMediaPlayerIndexedListSliceError(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& proxy, const boost::shared_ptr< RequestMediaPlayerIndexedListSliceError >& error)
{
   (void)proxy;
   (void)error;
   ETG_TRACE_USR4(("onRequestMediaPlayerIndexedListSliceError"));

   _responsedListState = RLS_IDLE;
   POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
   POST_MSG((COURIER_MESSAGE_NEW(ListPopulateReqMsg)(1, LIST_ID_COVER_PLAYER_IMAGE)));
}


/**
 * onRequestMediaPlayerIndexedListSliceResult - function called when mediaplayer gets a result during the method call of RequestMediaPlayerIndexedListSliceStart.
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void PhotoPlayerHandler::onRequestMediaPlayerIndexedListSliceResult(const ::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& proxy, const boost::shared_ptr< RequestMediaPlayerIndexedListSliceResult >& result)
{
   (void)proxy;
   ETG_TRACE_USR4(("onRequestMediaPlayerIndexedListSliceResult result->getOMediaObjects().size() = %d, _responsedListState: %d, _isGettingListDataProvider: %d", result->getOMediaObjects().size(), _responsedListState, _isGettingListDataProvider));
   _responsedList._oMediaObjects = result->getOMediaObjects();

   switch (_responsedListState)
   {
      case RLS_IDLE:
      {
         if (_isGettingListDataProvider)
         {
            updateListData();
            _isGettingListDataProvider = false;
         }
         break;
      }
      case RLS_PROCESSING:
      {
         _responsedListState = RLS_DONE;
         if (_isGettingListDataProvider)
         {
            _isGettingListDataProvider = false;
            POST_MSG((COURIER_MESSAGE_NEW(ListPopulateReqMsg)(1, LIST_ID_COVER_PLAYER_IMAGE)));
         }
         break;
      }
      default:
         break;
   }
}


/**
 * onStartSlideshowError - function called when mediaplayer gets an error during the method call of StartSlideshow.
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void PhotoPlayerHandler::onStartSlideshowError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::StartSlideshowError >& /*error*/)
{
   ETG_TRACE_USR4(("onStartSlideshowError"));
   POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
}


/**
 * onStartSlideshowResult - function called when mediaplayer gets an result during the method call of StartSlideshow.
 *
 * @param[in] proxy
 * @param[in] result
 * @return void
 */
void PhotoPlayerHandler::onStartSlideshowResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::StartSlideshowResult >& /*result*/)
{
   ETG_TRACE_USR4(("onStartSlideshowResult"));
   POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
}


/**
 * getListDataProvider - Gets the ListDataProvider from the corresponding listdataprovider functions
 * @param[in] listId
 * @parm[out] none
 * @return tSharedPtrDataProvider
 */
tSharedPtrDataProvider PhotoPlayerHandler::getListDataProvider(const ListDataInfo& listInfo)
{
   _activeDeviceType = MediaDatabinding::getInstance().getCurrentActiveMediaDevice();
   _activeDeviceTag = MediaProxyUtility::getInstance().getActiveDeviceTag();

   ETG_TRACE_USR4(("getListDataProvider is called - listInfo.listId: %d, listInfo.startIndex: %d, listInfo.windowSize: %d", listInfo.listId, listInfo.startIndex, listInfo.windowSize));
   ETG_TRACE_USR4(("getListDataProvider is called - _requestedList._startIndex: %d, _requestedList._bufferSize: %d", _requestedList._startIndex, _requestedList._bufferSize));
   ETG_TRACE_USR4(("getListDataProvider is called - _currentWindowList._windowStartIndex: %d, _currentWindowList._windowSize: %d, _currenEmptytWindowList._windowStartIndex: %d, _currenEmptytWindowList._windowSize: %d", _currentWindowList._windowStartIndex, _currentWindowList._windowSize, _currenEmptytWindowList._windowStartIndex, _currenEmptytWindowList._windowSize));
   ETG_TRACE_USR4(("getListDataProvider is called - _isGettingListDataProvider: %d, _responsedListState: %d, _currentPhotoPlayerScreen: %d, _isNextShifting: %d, _activeDeviceType: %d, _activeDeviceTag: %d, _nextListWindowStartIndex: %d", _isGettingListDataProvider, _responsedListState, _currentPhotoPlayerScreen, _isNextShifting, _activeDeviceType, _activeDeviceTag, _nextListWindowStartIndex));

   if ((LIST_ID_COVER_PLAYER_IMAGE == listInfo.listId) && (!_isGettingListDataProvider))
   {
      _isGettingListDataProvider = true;

      switch (_responsedListState)
      {
         case RLS_IDLE:
         {
            if (PPS_SLIDE_SHOW != _currentPhotoPlayerScreen)
            {
               _currentWindowList._windowStartIndex = listInfo.startIndex;
               _currentWindowList._windowSize = listInfo.windowSize;

               _requestedList._startIndex = listInfo.startIndex;
               _requestedList._bufferSize = listInfo.windowSize;

               requestListData(_requestedList);
            }
            else
            {
               _currenEmptytWindowList._windowStartIndex = listInfo.startIndex;
               _currenEmptytWindowList._windowSize = listInfo.windowSize;
               updateEmptyListData();
               _isGettingListDataProvider = false;
               scrollListToWindow(_nextListWindowStartIndex);
            }

            break;
         }
         case RLS_PROCESSING:
         {
            break;
         }
         case RLS_DONE:
         {
            if (PPS_SLIDE_SHOW != _currentPhotoPlayerScreen)
            {
               if ((listInfo.startIndex == _currentWindowList._windowStartIndex) && (listInfo.windowSize == _currentWindowList._windowSize))
               {
                  updateListData();
                  _responsedListState = RLS_IDLE;
                  _isGettingListDataProvider = false;
                  //scrollListToWindow(_nextListWindowStartIndex);
               }
               else
               {
                  _currenEmptytWindowList._windowStartIndex = listInfo.startIndex;
                  _currenEmptytWindowList._windowSize = listInfo.windowSize;
                  updateEmptyListData();
                  _responsedListState = RLS_IDLE;
                  _isGettingListDataProvider = false;
                  if (_isNextShifting)
                  {
                     if ((_nextListWindowStartIndex < _currenEmptytWindowList._windowStartIndex) || ((_currenEmptytWindowList._windowStartIndex + _currenEmptytWindowList._windowSize - 1) < _nextListWindowStartIndex))
                     {
                        scrollListToWindow(_nextListWindowStartIndex);
                     }
                     else
                     {
                        POST_MSG((COURIER_MESSAGE_NEW(ListPopulateReqMsg)(1, LIST_ID_COVER_PLAYER_IMAGE)));
                     }
                  }
                  else
                  {
                     POST_MSG((COURIER_MESSAGE_NEW(ListPopulateReqMsg)(1, LIST_ID_COVER_PLAYER_IMAGE)));
                  }
               }
            }
            else
            {
               _currenEmptytWindowList._windowStartIndex = listInfo.startIndex;
               _currenEmptytWindowList._windowSize = listInfo.windowSize;
               updateEmptyListData();
               _responsedListState = RLS_IDLE;
               _isGettingListDataProvider = false;
               scrollListToWindow(_nextListWindowStartIndex);
            }

            break;
         }
         default:
         {
            break;
         }
      }
   }

   ETG_TRACE_USR4(("End getListDataProvider is called - listInfo.listId: %d, listInfo.startIndex: %d, listInfo.windowSize: %d", listInfo.listId, listInfo.startIndex, listInfo.windowSize));
   ETG_TRACE_USR4(("End getListDataProvider is called - _requestedList._startIndex: %d, _requestedList._bufferSize: %d", _requestedList._startIndex, _requestedList._bufferSize));
   ETG_TRACE_USR4(("End getListDataProvider is called - _currentWindowList._windowStartIndex: %d, _currentWindowList._windowSize: %d, _currenEmptytWindowList._windowStartIndex: %d, _currenEmptytWindowList._windowSize: %d", _currentWindowList._windowStartIndex, _currentWindowList._windowSize, _currenEmptytWindowList._windowStartIndex, _currenEmptytWindowList._windowSize));
   ETG_TRACE_USR4(("End getListDataProvider is called - _responsedListState: %d, _isNextShifting: %d, _activeDeviceType: %d, _activeDeviceTag: %d, _nextListWindowStartIndex: %d", _responsedListState, _isNextShifting, _activeDeviceType, _activeDeviceTag, _nextListWindowStartIndex));

   return tSharedPtrDataProvider();
}


/**
 *  PhotoPlayerHandler::populateCoverListOrShowFullScreenItem - Function to populate the photo player cover list or show full screen selected image item
 *  @param [in] selectedItem - selected Item
 *  @param [in] activeDeviceType - current active device type
 *  @param [in] activeDeviceTag - current active device tag
 *  @return none
 */
void PhotoPlayerHandler::populateCoverListOrShowFullScreenItem(uint32 selectedItem, uint32 activeDeviceType, uint32 activeDeviceTag)
{
   ETG_TRACE_USR4(("populateCoverListOrShowFullScreenItem - selectedItem: %d, activeDeviceType: %d, activeDeviceTag: %d, _currentPhotoPlayerScreen: %d", selectedItem, activeDeviceType, activeDeviceTag, _currentPhotoPlayerScreen));

   _activeDeviceType = activeDeviceType;
   _activeDeviceTag = activeDeviceTag;
   uint32 activeItemIndex = getActiveItemIndex(selectedItem);

   POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_START)));

   _isNextShifting = true;
   _nextListWindowStartIndex = activeItemIndex;

   _currentWindowList._windowStartIndex = (activeItemIndex > 0) ? (activeItemIndex - 1) : activeItemIndex;
   _requestedList._startIndex = _currentWindowList._windowStartIndex;
   _responsedListState = RLS_PROCESSING;

   switch (_currentPhotoPlayerScreen)
   {
      case PPS_BROWSER:
      {
         _currentWindowList._windowSize = FLEX_LIST_COVER_INIT_BUFFER_SIZE;
         _requestedList._bufferSize = FLEX_LIST_COVER_INIT_BUFFER_SIZE;
         requestListData(_requestedList);

         _currentPhotoPlayerScreen = PPS_COVER;

         break;
      }

      case PPS_COVER:
      {
         _currentWindowList._windowSize = FLEX_LIST_FULL_SCREEN_INIT_BUFFER_SIZE;
         _requestedList._bufferSize = FLEX_LIST_FULL_SCREEN_INIT_BUFFER_SIZE;
         requestListData(_requestedList);

         _currentPhotoPlayerScreen = PPS_FULL_SCREEN;

         break;
      }

      case PPS_FULL_SCREEN:
      case PPS_SLIDE_SHOW:
      {
         _currentWindowList._windowSize = FLEX_LIST_COVER_INIT_BUFFER_SIZE;
         _requestedList._bufferSize = FLEX_LIST_COVER_INIT_BUFFER_SIZE;
         requestListData(_requestedList);

         _currentPhotoPlayerScreen = PPS_COVER;

         break;
      }
      default:
         break;
   }
}


/**
 *  PhotoPlayerHandler::playSlideShowImageList - Function to play slide show with a image list at the selected item
 *  @param [in] selectedItem - selected Item
 *  @return none
 */
void PhotoPlayerHandler::playSlideShowImageList(uint32 selectedItem)
{
   _responsedListState = RLS_DONE;
   _isGettingListDataProvider = true;

   _isNextShifting = true;
   _nextListWindowStartIndex = selectedItem;

   ETG_TRACE_USR4(("playSlideShowImageList - _responsedList._listHandle: %d, _responsedList._totalListSize: %d, _responsedList._startIndex: %d, _responsedList._oMediaObjects.size(): %d", _responsedList._listHandle, _responsedList._totalListSize, _responsedList._startIndex, _responsedList._oMediaObjects.size()));
   ETG_TRACE_USR4(("playSlideShowImageList - _focussedIndex: %d, _windowStartIndex: %d, _nextListWindowStartIndex: %d, selectedItem: %d, _currentPhotoPlayerScreen: %d", _focussedIndex, _windowStartIndex, _nextListWindowStartIndex, selectedItem, _currentPhotoPlayerScreen));

   if ((_responsedList._listHandle > 0) && (_responsedList._oMediaObjects.size() > 0) && (_responsedList._totalListSize > 0) && (selectedItem < _responsedList._totalListSize))
   {
      ETG_TRACE_USR4(("playSlideShowImageList - _responsedList._listHandle: %d, _responsedList._oMediaObjects[%d].getU32Tag(): %d", _responsedList._listHandle, selectedItem, _responsedList._oMediaObjects[selectedItem].getU32Tag()));
      POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_START)));
      _mediaPlayerProxy->sendStartSlideshowStart(*this, _responsedList._listHandle, _responsedList._oMediaObjects[selectedItem].getU32Tag());
   }
   else
   {
      ETG_TRACE_USR4(("playSlideShowImageList - List handle invalid or image list is empty or selected index is incorrect"));
   }

   _currentWindowList._windowSize = FLEX_LIST_FULL_SCREEN_INIT_BUFFER_SIZE;
   _requestedList._bufferSize = FLEX_LIST_FULL_SCREEN_INIT_BUFFER_SIZE;

   _currentPhotoPlayerScreen = PPS_SLIDE_SHOW;
   _responsedListState = RLS_IDLE;
   _isGettingListDataProvider = false;
}


/**
 *  PhotoPlayerHandler::getCurrentImage - Get the current Image MetaData Information.
 *  @return Return_Description
 */
T_MPlayMediaObject PhotoPlayerHandler::getCurrentImage()
{
   T_MPlayMediaObject oMediaObject = T_MPlayMediaObject::getDefaultInstance();
   ETG_TRACE_USR4(("getCurrentImage is called"));

   switch (_currentPhotoPlayerScreen)
   {
      case PPS_COVER:
      {
         oMediaObject.setSFilename(_responsedList._oMediaObjects[_windowStartIndex + 1].getSFilename());
         oMediaObject.setE8FileFormat(_responsedList._oMediaObjects[_windowStartIndex + 1].getE8FileFormat());
         break;
      }
      case PPS_FULL_SCREEN:
      {
         oMediaObject.setSFilename(_responsedList._oMediaObjects[_windowStartIndex].getSFilename());
         oMediaObject.setE8FileFormat(_responsedList._oMediaObjects[_windowStartIndex].getE8FileFormat());
         break;
      }
      case PPS_SLIDE_SHOW:
      {
         oMediaObject = _mediaPlayerProxy->getNowShowing().getOMediaObject();
         break;
      }
      default:
         ETG_TRACE_USR4(("Invalid Image data"));
         break;
   }
   return oMediaObject;
}


/**
 *  PhotoPlayerHandler::initializeListParameters - initializes the PhotoPlayer Handler parameters
 *  @return Return_Description
 */
void PhotoPlayerHandler::initializeListParameters()
{
   ETG_TRACE_USR4(("initializeListParameters"));

   _currentPhotoPlayerScreen = PPS_BROWSER;
   _windowStartIndex = 0;
   _nextListWindowStartIndex = 0;
   _focussedIndex = 0;
   _isNextShifting = false;
   _activeDeviceType = 0;
   _activeDeviceTag = 0;
   _slideShowPosition = 0;

   _currentWindowList._windowStartIndex = 0;
   _currentWindowList._windowSize = 0;

   _currenEmptytWindowList._windowStartIndex = 0;
   _currenEmptytWindowList._windowSize = 0;

   _requestedList._startIndex = 0;
   _requestedList._bufferSize = 0;

   _responsedList._listHandle = 0;
   _responsedList._startIndex = 0;
   _responsedList._totalListSize = 0;
   _responsedList._oMediaObjects.clear();

   _slideShowNextPrevSkip._nextPrevSkipState = SS_NoSkip;
   _slideShowNextPrevSkip._u8NextPrevSkipCount = 1;
}


/**
 *  setWindowStartIndex - sets the window start index for photo player list
 * @param[in] windowStartIndex
 *  @return Return_Description
 */
void PhotoPlayerHandler::setWindowStartIndex(uint32 windowStartindex)
{
   ETG_TRACE_USR4(("setWindowStartIndex: %d", windowStartindex));
   _windowStartIndex = windowStartindex;
}


/**
 *  setFocussedIndex - sets the focused index for photo player list
 * @param[in] focussedIndex
 *  @return Return_Description
 */
void PhotoPlayerHandler::setFocussedIndex(int32 focussedIndex)
{
   ETG_TRACE_USR4(("setFocussedIndex: %d", focussedIndex));
   _focussedIndex = focussedIndex;
}


/**
 * initialisePhotoPlayerCover - function called whenever user enter PhotoPlayer Cover for the first time.
 *
 * @param[in] requestedListParam
 * @param[out] none
 * @return void
 */
void PhotoPlayerHandler::requestListData(RequestedList requestedListParam)
{
   ETG_TRACE_USR4(("requestListData - _activeDeviceTag: %d, requestedListParam._startIndex: %d, requestedListParam._bufferSize: %d", _activeDeviceTag, requestedListParam._startIndex, requestedListParam._bufferSize));

   _mediaPlayerProxy->sendCreateMediaPlayerIndexedListStart(*this, (::MPlay_fi_types::T_e8_MPlayListType)LIST_TYPE_PHOTO_PLAYER_IMAGE, 0, 0, 0, _activeDeviceTag);
}


/**
 * updateListData - Populates the list with the data in _responsedList
 *
 * @param[in] none
 * @parm[out] none
 * @return void
 */
void PhotoPlayerHandler::updateListData()
{
   ETG_TRACE_USR4(("updateListData _responsedList._listHandle: %d , _responsedList._startIndex: %d, _responsedList._totalListSize: %d, _responsedList._oMediaObjects.size(): %d", _responsedList._listHandle, _responsedList._startIndex, _responsedList._totalListSize, _responsedList._oMediaObjects.size()));

   tSharedPtrDataProvider dataProvider = fillListItems();
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));

   scrollListToWindow(_nextListWindowStartIndex);

   POST_MSG((COURIER_MESSAGE_NEW(WaitAnimationReqMsg)(ANIMATION_STOP)));
   ETG_TRACE_USR4(("updateListData End"));
}


/**
 * updateEmptyListData - Populates the list with empty data
 *
 * @param[in] none
 * @parm[out] none
 * @return void
 */
void PhotoPlayerHandler::updateEmptyListData()
{
   ETG_TRACE_USR4(("updateEmptyListData _responsedList._listHandle: %d , _responsedList._startIndexL %d, _responsedList._totalListSize: %d, _responsedList._oMediaObjects.size(): %d", _responsedList._listHandle, _responsedList._startIndex, _responsedList._totalListSize, _responsedList._oMediaObjects.size()));

   tSharedPtrDataProvider dataProvider = fillEmptyListItems();
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));
   ETG_TRACE_USR4(("updateEmptyListData End"));
}


/**
 * fillListItems - Private helper function to fill photo list items
 * @param[in] none
 * @parm[out] none
 * @return tSharedPtrDataProvider
 */
tSharedPtrDataProvider PhotoPlayerHandler::fillListItems()
{
   uint32 mediaObjectSize = _responsedList._oMediaObjects.size();

   ETG_TRACE_USR4(("fillListItems - _currentWindowList._windowStartIndex: %d, _currentWindowList._windowSize: %d, _currenEmptytWindowList._windowStartIndex: %d, _currenEmptytWindowList._windowSize: %d", _currentWindowList._windowStartIndex, _currentWindowList._windowSize, _currenEmptytWindowList._windowStartIndex, _currenEmptytWindowList._windowSize));
   ETG_TRACE_USR4(("fillListItems - _requestedList._startIndex: %d, _requestedList._bufferSize: %d, ", _requestedList._startIndex, _requestedList._bufferSize));
   ETG_TRACE_USR4(("fillListItems - _responsedList._listHandle: %d, _responsedList._startIndex: %d, _responsedList._totalListSize: %d, _responsedList._oMediaObjects.size(): %d", _responsedList._listHandle, _responsedList._startIndex, _responsedList._totalListSize, mediaObjectSize));

   ListDataProviderBuilder photolistBuilder(LIST_ID_COVER_PLAYER_IMAGE, DATA_CONTEXT_PLAYER_COVER_LIST_ITEM);

   Candera::UInt32 guiListIndex = _currentWindowList._windowStartIndex;
   Candera::UInt32 windowEndIndex = _currentWindowList._windowStartIndex + _currentWindowList._windowSize;
   uint32 objectsListIndex = 0;
   for (; (objectsListIndex < mediaObjectSize) && (guiListIndex < windowEndIndex); objectsListIndex++, guiListIndex++)
   {
      PhotoPlayerInfoData PhotoPlayerInfo = PhotoPlayerInfoData();
      std::string sFilenameImagePath = _responsedList._oMediaObjects[objectsListIndex].getSFilename();
      ETG_TRACE_USR4(("fillListItems guiListIndex[%d]/[%d], mediaObjects[%d].getSFilename() %s ", guiListIndex, windowEndIndex, objectsListIndex, sFilenameImagePath.c_str()));

      if (sFilenameImagePath.c_str() != NULL)
      {
         ETG_TRACE_USR4(("fillListItems Start ImageLoader::loadBitmapFile(%s) ", sFilenameImagePath.c_str()));
         Candera::Bitmap* bmp = ImageLoader::loadBitmapFile(sFilenameImagePath.c_str());
         ETG_TRACE_USR4(("fillListItems End ImageLoader::loadBitmapFile(%s) ", sFilenameImagePath.c_str()));
         if (bmp != NULL)
         {
            ETG_TRACE_USR4(("fillListItems Width: %d Height: %d Start ImageLoader::createImage(%s) ", bmp->GetWidth(), bmp->GetHeight(), bmp->GetName()));
            PhotoPlayerInfo.mPhotoContent = ImageLoader::createImage(bmp);
            ETG_TRACE_USR4(("fillListItems Width: %d Height: %d End ImageLoader::createImage(%s) ", bmp->GetWidth(), bmp->GetHeight(), bmp->GetName()));
            PhotoPlayerInfo.mPhotoVisible = true;
            ETG_TRACE_USR4(("fillListItems Start coverlistBuilder.AddItem(guiListIndex).AddDataBindingUpdater<PhotoPlayerInfoDataBindingSource>(PhotoPlayerInfo) "));
            photolistBuilder.AddItem(guiListIndex).AddDataBindingUpdater<PhotoPlayerInfoDataBindingSource>(PhotoPlayerInfo);
            ETG_TRACE_USR4(("fillListItems End coverlistBuilder.AddItem(guiListIndex).AddDataBindingUpdater<PhotoPlayerInfoDataBindingSource>(PhotoPlayerInfo) "));
         }
         else
         {
            ETG_TRACE_USR4(("fillListItems - Load bitmap from file name %s failed", sFilenameImagePath.c_str()));
         }
      }
      else
      {
         ETG_TRACE_USR4(("fillListItems - Image file name invalid"));
      }
   }

   return photolistBuilder.CreateDataProvider(_currentWindowList._windowStartIndex, _responsedList._totalListSize);
}


/**
 * fillListItems - Private helper function to fill empty photo list items
 * @param[in] none
 * @parm[out] none
 * @return tSharedPtrDataProvider
 */
tSharedPtrDataProvider PhotoPlayerHandler::fillEmptyListItems()
{
   uint32 mediaObjectSize = _responsedList._oMediaObjects.size();

   ETG_TRACE_USR4(("fillEmptyListItems - _currentWindowList._windowStartIndex: %d, _currentWindowList._windowSize: %d, _currenEmptytWindowList._windowStartIndex: %d, _currenEmptytWindowList._windowSize: %d", _currentWindowList._windowStartIndex, _currentWindowList._windowSize, _currenEmptytWindowList._windowStartIndex, _currenEmptytWindowList._windowSize));
   ETG_TRACE_USR4(("fillEmptyListItems - _requestedList._startIndex: %d, _requestedList._bufferSize: %d, ", _requestedList._startIndex, _requestedList._bufferSize));
   ETG_TRACE_USR4(("fillEmptyListItems - _responsedList._listHandle: %d, _responsedList._startIndex: %d, _responsedList._totalListSize: %d, _responsedList._oMediaObjects.size(): %d", _responsedList._listHandle, _responsedList._startIndex, _responsedList._totalListSize, mediaObjectSize));

   ListDataProviderBuilder emptyPhotolistBuilder(LIST_ID_COVER_PLAYER_IMAGE, DATA_CONTEXT_PLAYER_COVER_LIST_ITEM);

   Candera::UInt32 guiListIndex = _currenEmptytWindowList._windowStartIndex;
   Candera::UInt32 windowEndIndex = _currenEmptytWindowList._windowStartIndex + _currenEmptytWindowList._windowSize;
   for (; guiListIndex < windowEndIndex; guiListIndex++)
   {
      PhotoPlayerInfoData PhotoPlayerInfo = PhotoPlayerInfoData();
      ETG_TRACE_USR4(("fillEmptyListItems guiListIndex[%d]/[%d], _currentPhotoPlayerScreen: %d", guiListIndex, windowEndIndex, _currentPhotoPlayerScreen));

      switch (_currentPhotoPlayerScreen)
      {
         case PPS_COVER:
         case PPS_FULL_SCREEN:
         {
            Candera::Bitmap* bmp = NULL;
            PhotoPlayerInfo.mPhotoContent = ImageLoader::createImage(bmp);

            break;
         }
         case PPS_SLIDE_SHOW:
         {
            PhotoPlayerInfo.mTextContent = "...";
            break;
         }
         default:
            break;
      }

      PhotoPlayerInfo.mPhotoVisible = true;
      emptyPhotolistBuilder.AddItem(guiListIndex).AddDataBindingUpdater<PhotoPlayerInfoDataBindingSource>(PhotoPlayerInfo);
   }

   return emptyPhotolistBuilder.CreateDataProvider(_currenEmptytWindowList._windowStartIndex, _responsedList._totalListSize);
}


/**
 * scrollListToWindow - Private helper function to scroll the list to particular window
 * @param[in] uint32 - windowStartIndex
 * @parm[out] none
 * @return void
 */
void PhotoPlayerHandler::scrollListToWindow(uint32 windowStartIndex)
{
   if (_isNextShifting)
   {
      ETG_TRACE_USR4(("scrollListToWindow windowStartIndex: %d", windowStartIndex));

      POST_MSG((COURIER_MESSAGE_NEW(ListChangeMsg)(LIST_ID_COVER_PLAYER_IMAGE, ListChangeSet, windowStartIndex)));
      _isNextShifting = false;
   }
}


/**
 * getActiveItemIndex - Get photo cover/full screen list index for selected row from current window
 * @param[in] uint32 selectedIndex - selected row from current window
 * @parm[out] none
 * @return uint32 -
 */
int32 PhotoPlayerHandler::getActiveItemIndex(int32 selectedIndex)
{
   uint32 activeItemIndex = 0;
   switch (_currentPhotoPlayerScreen)
   {
      case PPS_BROWSER:
      {
         activeItemIndex = (selectedIndex > 0) ? (selectedIndex - 1) : selectedIndex;
         break;
      }
      case PPS_COVER:
      {
         activeItemIndex = selectedIndex;
         break;
      }
      case PPS_FULL_SCREEN:
      {
         activeItemIndex = (selectedIndex > 0) ? (selectedIndex - 1) : selectedIndex;
         break;
      }
      case PPS_SLIDE_SHOW:
      {
         activeItemIndex = (selectedIndex > 0) ? (selectedIndex - 1) : selectedIndex;
//         activeItemIndex = _nextListWindowStartIndex;
         break;
      }
      default:
         break;
   }

   ETG_TRACE_USR4(("getActiveItemIndex - activeItemIdx: %d, selectedIndex: %d", activeItemIndex, selectedIndex));

   return activeItemIndex;
}


/**
 * onCourierMessage - Message from Statemachine when starting photo player cover first times.
 *
 * @param[in] InitialisePhotoPlayerCoverMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool PhotoPlayerHandler::onCourierMessage(const InitialisePhotoPlayerCoverMsg& oMsg)
{
   initializeListParameters();
   setWindowStartIndex(oMsg.GetRow());

   uint32 activeDeviceType = MediaDatabinding::getInstance().getCurrentActiveMediaDevice();
   uint32 activeDeviceTag = MediaProxyUtility::getInstance().getActiveDeviceTag();

   ETG_TRACE_USR4(("onCourierMessage - InitialisePhotoPlayerCoverMsg activeDeviceType: %d, activeDeviceTag: %d, _windowStartIndex: %d", activeDeviceType, activeDeviceTag, _windowStartIndex));

   populateCoverListOrShowFullScreenItem(_windowStartIndex, activeDeviceType, activeDeviceTag);

   return true;
}


/**
 * onCourierMessage - Message from Statemachine when selecting a photo item on photo player cover list.
 *
 * @param[in] ButtonPhotoPlayerCoverItemPressUpMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool PhotoPlayerHandler::onCourierMessage(const ButtonPhotoPlayerCoverItemPressUpMsg& oMsg)
{
   uint32 ListId = oMsg.GetListId();
   uint32 selectedRow = oMsg.GetRow();
   ETG_TRACE_USR4(("onCourierMessage - ButtonPhotoPlayerCoverItemPressUpMsg - ListId: %d, selectedRow: %d", ListId, selectedRow));
   if (ListId == LIST_ID_COVER_PLAYER_IMAGE)
   {
      //setWindowStartIndex(_windowStartIndex);           // Save window start index
      uint32 activeDeviceType = MediaDatabinding::getInstance().getCurrentActiveMediaDevice();
      uint32 activeDeviceTag = MediaProxyUtility::getInstance().getActiveDeviceTag();
      populateCoverListOrShowFullScreenItem(selectedRow/*oMsg.GetRow()*/, activeDeviceType, activeDeviceTag);
   }
   return true ;
}


/**
 * RotatePhotoPlayerReqMsg Courier message - called when user wants to rotate photo content.
 *
 * @param[in] msg
 * @return true - message consumed
 */
bool PhotoPlayerHandler::onCourierMessage(const RotatePhotoPlayerReqMsg& /*msg*/)
{
   ETG_TRACE_USR4(("onCourierMessage - RotatePhotoPlayerReqMsg"));
   return true;
}


/**
 * InitialisePlayPhotoPlayerReqMsg Courier message - called when user wants to start photo slideshow first times.
 *
 * @param[in] msg
 * @return true - message consumed
 */
bool PhotoPlayerHandler::onCourierMessage(const InitialisePlayPhotoPlayerReqMsg& /*msg*/)
{
   ETG_TRACE_USR4(("onCourierMessage - InitialisePlayPhotoPlayerReqMsg - _focussedIndex: %d, _windowStartIndex: %d", _focussedIndex, _windowStartIndex));

   //setWindowStartIndex(_windowStartIndex);           // Save window start index
   playSlideShowImageList(_windowStartIndex);

   return true;
}


/**
 * FullScreenPhotoPlayerReqMsg Courier message - called when user wants to show photo content in fullscreen.
 *
 * @param[in] msg
 * @return true - message consumed
 */
bool PhotoPlayerHandler::onCourierMessage(const FullScreenPhotoPlayerReqMsg& /*msg*/)
{
   ETG_TRACE_USR4(("onCourierMessage - FullScreenPhotoPlayerReqMsg - _focussedIndex: %d, _windowStartIndex: %d", _focussedIndex, _windowStartIndex));

   //setWindowStartIndex(_windowStartIndex);           // Save window start index
   uint32 activeDeviceType = MediaDatabinding::getInstance().getCurrentActiveMediaDevice();
   uint32 activeDeviceTag = MediaProxyUtility::getInstance().getActiveDeviceTag();

   populateCoverListOrShowFullScreenItem(1 + _windowStartIndex/*oMsg.GetRow()*/, activeDeviceType, activeDeviceTag);

   return true;
}


/**
 * FitScreenPhotoPlayerReqMsg Courier message - called when user wants to go back to photo cover.
 *
 * @param[in] msg
 * @return true - message passed
 */
bool PhotoPlayerHandler::onCourierMessage(const FitScreenPhotoPlayerReqMsg& /*msg*/)
{
   ETG_TRACE_USR4(("onCourierMessage - FitScreenPhotoPlayerReqMsg - _focussedIndex: %d, _windowStartIndex: %d, _currentPhotoPlayerScreen: %d, _slideShowPosition: %d", _focussedIndex, _windowStartIndex, _currentPhotoPlayerScreen, _slideShowPosition));

   if (PPS_SLIDE_SHOW == _currentPhotoPlayerScreen)
   {
      setWindowStartIndex(_slideShowPosition);
   }
   uint32 activeDeviceType = MediaDatabinding::getInstance().getCurrentActiveMediaDevice();
   uint32 activeDeviceTag = MediaProxyUtility::getInstance().getActiveDeviceTag();

   populateCoverListOrShowFullScreenItem(_windowStartIndex/*oMsg.GetRow()*/, activeDeviceType, activeDeviceTag);

   return false ;
}


/**
 * SlideShowPositionPhotoPlayerChangedUpdMsg Courier message - called when slideshow notify position of nowshowing.
 *
 * @param[in] msg
 * @return true - message consumed
 */
bool PhotoPlayerHandler::onCourierMessage(const SlideShowPositionPhotoPlayerChangedUpdMsg& oMsg)
{
   bool isMessageProcessed = false;
   uint32 listId = oMsg.GetListId();

   if ((LIST_ID_COVER_PLAYER_IMAGE == listId) && (PPS_SLIDE_SHOW == _currentPhotoPlayerScreen))
   {
      _slideShowPosition = oMsg.GetPosition();
      isMessageProcessed = true;
   }

   ETG_TRACE_USR4(("onCourierMessage - SlideShowPositionPhotoPlayerChangedUpdMsg - oMsg.GetListId(): %d, oMsg.GetPosition(): %d, _windowStartIndex: %d, _slideShowPosition: %d, _currentPhotoPlayerScreen: %d", listId, oMsg.GetPosition(), _windowStartIndex, _slideShowPosition, _currentPhotoPlayerScreen));

   return isMessageProcessed;
}


ETG_I_CMD_DEFINE((TraceCmd_EncoderRotation, "EncoderRotation %d", ETG_I_CENUM(enEncoder)))
void PhotoPlayerHandler::TraceCmd_EncoderRotation(enEncoder /*nRotationDirection*/)
{
   /*   ETG_TRACE_USR4(("TraceCmd_EncoderRotation is called"));
      if (nRotationDirection == ENCODER_ROTATION_POSITIVE)
      {
         ETG_TRACE_USR4(("ENCODER_ROTATION_POSITIVE is called"));
         POST_MSG( COURIER_MESSAGE_NEW(EncoderPositiveRotationUpdMsg)());
      }
      if (nRotationDirection == ENCODER_ROTATION_NEGATIVE)
      {
         ETG_TRACE_USR4(("ENCODER_ROTATION_NEGATIVE is called"));
         POST_MSG( COURIER_MESSAGE_NEW(EncoderNegativeRotationUpdMsg)());
      }
    */
}


/**
 * onCourierMessage - This message is received from the flexlist widget when it swipe left/right or press on list item

 * @param[in] ButtonReactionMsg
 * @parm[out] none
 * @return true - message consumed (LIST_ID_COVER_PLAYER_IMAGE) or false - message ignored(Others)
 */
bool PhotoPlayerHandler::onCourierMessage(const ButtonReactionMsg& oMsg)
{
   bool isMessageProcessed = false;

   ListProviderEventInfo info;
   if (ListProviderEventInfo::GetItemIdentifierInfo(oMsg.GetSender(), info))
   {
      unsigned int listId = info.getListId();     // the list id for generic access
      if (LIST_ID_COVER_PLAYER_IMAGE == listId)
      {
         unsigned int hdlRow = info.getHdlRow();     // normally the index
         unsigned int hdlCol = info.getHdlCol();
         enReaction enReaction = oMsg.GetEnReaction();

         switch (_currentPhotoPlayerScreen)
         {
            case PPS_SLIDE_SHOW:
            {
               if (enPress == enReaction)
               {
                  _slideShowNextPrevSkip._nextPrevSkipState = SS_InitSkip;
               }

               break;
            }
            default:
               break;
         }

         //For each ButtonReactionMsg for buttons which are part of list items we post a ButtonListItemMsg with detailed info about the list item.
         POST_MSG((COURIER_MESSAGE_NEW(ButtonListItemUpdMsg)(listId, hdlRow, hdlCol, enReaction)));
         isMessageProcessed = true;
      }

      ETG_TRACE_USR4(("onCourierMessage - ButtonReactionMsg - info.getListId(): %d, info.getHdlRow(): %d, info.getHdlCol(): %d, oMsg.GetEnReaction(): %d", listId, info.getHdlRow(), info.getHdlCol(), oMsg.GetEnReaction()));
      ETG_TRACE_USR4(("onCourierMessage - ButtonReactionMsg - _currentPhotoPlayerScreen: %d, _windowStartIndex: %d, _slideShowNextPrevSkip._nextPrevSkipState: %d, _slideShowNextPrevSkip._u8NextPrevSkipCount: %d", _currentPhotoPlayerScreen, _windowStartIndex, _slideShowNextPrevSkip._nextPrevSkipState, _slideShowNextPrevSkip._u8NextPrevSkipCount));
   }

   return isMessageProcessed;
}


/**
 * onCourierMessage - This message is received from the flexlist widget when it requires new data when the list is displayed or scrolled
 * @param[in] ListDateProviderReqMsg& oMsg
 * @parm[out] bool
 */
bool PhotoPlayerHandler::onCourierMessage(const ListDateProviderReqMsg& oMsg)
{
   uint32 listId = oMsg.GetListId();
   ETG_TRACE_USR4(("onCourierMessage - ListDateProviderReqMsg - oMsg.GetListId(): %d", listId));

   if (LIST_ID_COVER_PLAYER_IMAGE == listId)
   {
      ListDataInfo listDataInfo;
      listDataInfo.listId = listId;
      listDataInfo.startIndex =  oMsg.GetStartIndex();
      listDataInfo.windowSize = oMsg.GetWindowElementSize();
      return ListRegistry::s_getInstance().updateList(listDataInfo);
   }

   return false;
}


/**
 * onCourierMessage - This message is received from the flexlist widget when list is scrolled or changed start index
 * @param[in] ListChangedUpdMsg& oMsg
 * @parm[out] bool
 */
bool PhotoPlayerHandler::onCourierMessage(const ListChangedUpdMsg& oMsg)
{
   bool isMessageProcessed = false;
   uint32 listId = oMsg.GetListId();
   uint32 startIndex = oMsg.GetStartIndex();

   if (LIST_ID_COVER_PLAYER_IMAGE == listId)
   {
      switch (_currentPhotoPlayerScreen)
      {
         case PPS_COVER:
         case PPS_FULL_SCREEN:
         {
            setWindowStartIndex(startIndex);
            break;
         }
         case PPS_SLIDE_SHOW:
         {
            if (SS_InitSkip == _slideShowNextPrevSkip._nextPrevSkipState)
            {
               if (startIndex > _windowStartIndex)
               {
                  _slideShowNextPrevSkip._nextPrevSkipState = SS_NextSkip;
                  _slideShowNextPrevSkip._u8NextPrevSkipCount = static_cast<uint8>(startIndex - _windowStartIndex);
                  POST_MSG((COURIER_MESSAGE_NEW(NextPhotoPlayerReqMsg)(_slideShowNextPrevSkip._u8NextPrevSkipCount)));
                  _slideShowNextPrevSkip._nextPrevSkipState = SS_NoSkip;
                  _slideShowNextPrevSkip._u8NextPrevSkipCount = 1;
               }
               else
               {
                  if (startIndex < _windowStartIndex)
                  {
                     _slideShowNextPrevSkip._nextPrevSkipState = SS_PrevSkip;
                     _slideShowNextPrevSkip._u8NextPrevSkipCount = static_cast<uint8>(_windowStartIndex - startIndex);
                     POST_MSG((COURIER_MESSAGE_NEW(PrevPhotoPlayerReqMsg)(_slideShowNextPrevSkip._u8NextPrevSkipCount)));
                     _slideShowNextPrevSkip._nextPrevSkipState = SS_NoSkip;
                     _slideShowNextPrevSkip._u8NextPrevSkipCount = 1;
                  }
                  else
                  {
                     ETG_TRACE_USR4(("onCourierMessage - SlideShow previous and next skip count is not changed - _windowStartIndex: %d, startIndex: %d", _windowStartIndex, startIndex));
                  }
               }

               setWindowStartIndex(startIndex);
            }

            break;
         }
         default:
            break;
      }

      isMessageProcessed = true;
   }

   ETG_TRACE_USR4(("onCourierMessage - ListChangedUpdMsg - oMsg.GetListId(): %d, _windowStartIndex: %d", listId, _windowStartIndex));

   return isMessageProcessed;
}


/**
 * onCourierMessage - This message is received from the flexlist widget when list is change focus item
 * @param[in] ListChangedUpdMsg& oMsg
 * @parm[out] bool
 */
bool PhotoPlayerHandler::onCourierMessage(const FocusChangedUpdMsg& oMsg)
{
   bool isMessageProcessed = false;
   ListProviderEventInfo info;
   if (ListProviderEventInfo::GetItemIdentifierInfo(oMsg.GetWidget(), info))
   {
      uint32 listId = info.getListId();
      if (LIST_ID_COVER_PLAYER_IMAGE == listId)
      {
         _focussedIndex = static_cast<int32>(info.getHdlRow());     // normaly the index
         isMessageProcessed = true;
      }
      ETG_TRACE_USR4(("onCourierMessage - FocusChangedUpdMsg - info.getListId(): %d, _focussedIndex: %d", listId, _focussedIndex));
   }
   return isMessageProcessed;
}


bool PhotoPlayerHandler::onCourierMessage(const InitialisePictureBrowserReqMsg& /*oMsg*/)
{
   getImageList();
   return true;
}


void PhotoPlayerHandler::getImageList()
{
   uint8 deviceTag = MediaProxyUtility::getInstance().getActiveDeviceTag();
   ETG_TRACE_USR4(("get image list with device tag %d", deviceTag));
   //_mediaPlayerProxy->sendCreateMediaPlayerIndexedImageFolderListStart(*this,T_e8_MPlayListType__e8LTY_IMAGE, deviceTag,"/");
   //_mediaPlayerProxy->sendCreateMediaPlayerIndexedImageFolderListStart(*this,T_e8_MPlayListType__e8LTY_IMAGE_FOLDER, deviceTag,"/");
   _mediaPlayerProxy->sendCreateMediaPlayerIndexedImageFolderListStart(*this, T_e8_MPlayListType__e8LTY_IMAGE_FOLDER_ITEM, deviceTag, "/");
}


void PhotoPlayerHandler::onCreateMediaPlayerIndexedImageFolderListResult(const ::boost::shared_ptr<mplay_MediaPlayer_FI:: Mplay_MediaPlayer_FIProxy >& proxy,
      const ::boost::shared_ptr< mplay_MediaPlayer_FI::CreateMediaPlayerIndexedImageFolderListResult >& result)
{
   (void)proxy;
   ETG_TRACE_USR4(("onCreateMediaPlayerIndexedImageFolderListResult"));
   uint32 handle = result->getU32ListHandle();
   uint32 size = result->getU32ListSize();
   ETG_TRACE_USR4(("handle:%d size:%d", handle, size));

   _mediaPlayerProxy->sendRequestMediaPlayerIndexedImageFolderListSliceStart(*this, handle, 0, 26);
}


void PhotoPlayerHandler::onRequestMediaPlayerIndexedImageFolderListSliceResult(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy,
      const ::boost::shared_ptr<mplay_MediaPlayer_FI:: RequestMediaPlayerIndexedImageFolderListSliceResult >& result)
{
   (void)proxy;
   Candera::UInt32 guiListIndex = 0;
   ::MPlay_fi_types::T_MPlayImageObjects imgObjects = result->getOImageObjects();
   ETG_TRACE_USR4(("onRequestMediaPlayerIndexedImageFolderListSliceResult :%d", imgObjects.size()));

   ::MPlay_fi_types::T_MPlayImageObjects::iterator itr = imgObjects.begin();

   ListDataProviderBuilder listBuilder(LIST_ID_BROWSER_IMAGE, "GridListButton_AlbumArt");

   ETG_TRACE_USR4(("updateCoverFlowList 1 "));
   for (Candera::UInt32 listSliceIndex = 0; itr != imgObjects.end(); ++itr, ++listSliceIndex, ++guiListIndex)
   {
      std::string temp = imgObjects[guiListIndex].getSMountPoint() + imgObjects[guiListIndex].getSImageFile();

      Candera::Bitmap* bmp = ImageLoader::loadBitmapFile(temp.c_str());

      AlbumArt_GridListData Data;
      Data.mGridList_AlbumArt = ImageLoader::createImage(bmp);
      listBuilder.AddItem(guiListIndex).AddDataBindingUpdater<AlbumArt_GridListDataBindingSource>(Data);

      //ETG_TRACE_USR4(("updateCoverFlowList "));

      ETG_TRACE_USR4(("updateCoverFlowList %s ", temp.c_str()));
   }

   tSharedPtrDataProvider dataProvider = listBuilder.CreateDataProvider();
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));
}


void PhotoPlayerHandler::onCreateMediaPlayerIndexedImageFolderListError(const ::boost::shared_ptr<mplay_MediaPlayer_FI:: Mplay_MediaPlayer_FIProxy >& proxy,
      const ::boost::shared_ptr< mplay_MediaPlayer_FI::CreateMediaPlayerIndexedImageFolderListError >& error)
{
   (void)proxy;
   (void)error;
   ETG_TRACE_USR4(("PhotoPlayerHandler::onCreateMediaPlayerIndexedImageFolderListError"));
}


void PhotoPlayerHandler::onRequestMediaPlayerIndexedImageFolderListSliceError(const ::boost::shared_ptr<mplay_MediaPlayer_FI:: Mplay_MediaPlayer_FIProxy >& proxy,
      const ::boost::shared_ptr< mplay_MediaPlayer_FI::RequestMediaPlayerIndexedImageFolderListSliceError >& error)
{
   (void)proxy;
   (void)error;
   ETG_TRACE_USR4(("PhotoPlayerHandler::onRequestMediaPlayerIndexedImageFolderListSliceError"));
}


}//end of namespace Core
}//end of namespace App
