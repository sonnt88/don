/**
 *  @file   MediaDatabinding.cpp
 *  @author ECV - IVI-MediaTeam
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#ifndef MEDIADATABINDING_SCOPE1_H_
#define MEDIADATABINDING_SCOPE1_H_

#include "Core/DataModel/MediaDatabindingCommon.h"
namespace App {
namespace Core {

class MediaDatabinding : public MediaDatabindingCommon
{
   public:
      MediaDatabinding();
      virtual ~MediaDatabinding();

      //Add only scope1 specific functions
      static MediaDatabinding& getInstance();
      void updateMediaMetadataInfo(std::string, std::string, std::string);
      void updateVolumeAvailability(int16);
      void updateTrackNumber(uint32, uint32);
      void updateBTDisconnectionStatusOverUSB(bool, bool);
      void updateBTScreenOnCallHandsetModeActive(Courier::Bool, Courier::Bool);
      virtual void updateBTMetadataSupportStatus(Courier::Bool);
   private:
      static void removeInstance();

      static MediaDatabinding* _theInstance;
};


}
}


#endif /* MEDIADATABINDING_SCOPE1_H_ */
