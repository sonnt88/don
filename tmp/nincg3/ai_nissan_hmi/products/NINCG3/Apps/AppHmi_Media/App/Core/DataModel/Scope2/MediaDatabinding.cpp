/**
 *  @file   MediaScope2Databinding.cpp
 *  @author ECV - IVI-MediaTeam
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */
#include "hall_std_if.h"
#include "MediaDatabinding.h"
#include "Core/LanguageDefines.h"
#include "ProjectBaseTypes.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::MediaDatabinding::
#include "trcGenProj/Header/MediaDatabinding.cpp.trc.h"
#endif


namespace App {
namespace Core {
MediaDatabinding* MediaDatabinding::_theInstance = 0;
MediaDatabinding::MediaDatabinding()
   : _isPreviousTrackSupportAvailForBT(false)
   , _isNextTrackSupportAvailForBT(false)
{
   _modeValue = APP_MODE_UNDEFINED;
}


MediaDatabinding::~MediaDatabinding()
{
   ETG_TRACE_USR4(("clMedia_Databindinghandler Destructor"));
   removeInstance();
}


/**
 *  MediaDatabindingCommon::getInstance - function to get singleton instance
 *  @return Singleton Instance
 */
MediaDatabinding& MediaDatabinding::getInstance()
{
   if (_theInstance == 0)
   {
      _theInstance = new MediaDatabinding();
   }

   assert(_theInstance);
   return *_theInstance;
}


/**
 *  MediaDatabinding::removeInstance - function to delete the singleton instance
 *  @return Singleton Instance
 */
void MediaDatabinding::removeInstance()
{
   if (_theInstance)
   {
      delete _theInstance;
      _theInstance = 0;
   }
}


/**
 * updateBTMenuSupportStatus - Function to update Metadata status in BT Main screen to GUI
 * @param[in] bool
 * @parm[out] None
 * @return void
 */

void MediaDatabinding::updateBTMenuSupportStatus(bool isMenuAvailable)
{
   ETG_TRACE_USR4(("updateBTButtonEnableState"));
   if (isMenuAvailable)
   {
      (*_btMetaDataVisibility).mButtonBrowseTextUpdate = LANGUAGE_STRING(BT_AUDIO_BTMENU , "BTA Menu");
   }
   else
   {
      (*_btMetaDataVisibility).mButtonBrowseTextUpdate = LANGUAGE_STRING(BT_AUDIO_LOWER_AVRCP_MAIN_BUTTON_CONNECT, "Connect");
   }
   ETG_TRACE_USR4(("item modified :%s" , (*_btMetaDataVisibility).mButtonBrowseTextUpdate.GetCString()));
   _btMetaDataVisibility.MarkAllItemsModified();
   if (_btMetaDataVisibility.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("UpdateBTIconState : Updation failed..!!!"));
   }
}


/**
 * updateTrackNoInfoInBT - Function to update current playing and total Track No in BT Main screen to GUI
 * @param[in] bool
 * @parm[out] None
 * @return void
 */
void MediaDatabinding::updateTrackNoInfoInBT(Courier::Bool isTrackInfoAvailable)
{
   ETG_TRACE_USR4(("updateTrackNoInfoInBT"));
   (*_btMetaDataVisibility).misMenuAvailable = isTrackInfoAvailable;
   _btMetaDataVisibility.MarkItemModified(ItemKey::BTMetadataAvailability::isMenuAvailableItem);
   _btMetaDataVisibility.SendUpdate(true);
}


/**
 * updateAuxLevel - Function to update Aux level status to GUI
 * @param[in] AuxLevel
 * @parm[out] None
 * @return void
 */
void MediaDatabinding::updateAuxLevel(int16 AuxLevel)
{
   ETG_TRACE_USR4(("updateVolumeIconStatus"));
   (*_volumeButtonStatus).mIsButtonLowActive = ((AuxLevel - 1) == 0) ? true : false;
   (*_volumeButtonStatus).mIsButtonMidActive = ((AuxLevel - 1) == 1) ? true : false;
   (*_volumeButtonStatus).mIsButtonHighActive = ((AuxLevel - 1) == 2) ? true : false;
   _volumeButtonStatus.MarkAllItemsModified();
   if (_volumeButtonStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateVolumeIconStatus : Updation failed..!!!"));
   }
}


/**
 * updateBTRepeatRandomStatus - Function to control Enable/Disable Functionality of the buttons in BT Main Screen..
 * @param[in] bool,bool
 * @parm[out] None
 * @return void
 */

void MediaDatabinding::updateBTRepeatRandomStatus(Courier::Bool isRepeatSupportAvailable, Courier::Bool isRandomSupportAvailable)
{
   ETG_TRACE_USR4(("updateBTRepeatRandomStatus"));
   (*_btButtonSupportStatus).misRepeatSupportAvailable = isRepeatSupportAvailable;
   (*_btButtonSupportStatus).misRandomSupportAvailable = isRandomSupportAvailable;
   (*_btButtonSupportStatus).misRepeatTextAvailable = isRepeatSupportAvailable;
   (*_btButtonSupportStatus).misRandomTextAvailable = isRandomSupportAvailable;

   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isRepeatSupportAvailableItem);
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isRandomSupportAvailableItem);
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isRepeatTextAvailableItem);
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isRandomTextAvailableItem);

   if (_btButtonSupportStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateBTRepeatRandomStatus : Updation failed..!!!"));
   }
}


/**
 * updateBTFFNextTrackSupportStatus - Function to control Enable/Disable Functionality of the buttons in BT Main Screen..
 * @param[in] bool,bool
 * @parm[out] None
 * @return void
 */

void MediaDatabinding::updateBTFFNextTrackSupportStatus(Courier::Bool isFastForwardSupportAvailable, Courier::Bool isNextTrackSupportAvailable)
{
   ETG_TRACE_USR4(("updateBTRepeatRandomStatus"));
   (*_btButtonSupportStatus).misFastForwardSupportAvailable = isFastForwardSupportAvailable;
   (*_btButtonSupportStatus).misNextTrackSupportAvailable = isNextTrackSupportAvailable;

   if (isFastForwardSupportAvailable == true || isNextTrackSupportAvailable == true)
   {
      _isNextTrackSupportAvailForBT = true;
   }
   else
   {
      _isNextTrackSupportAvailForBT = false;
   }
   (*_btButtonSupportStatus).misNextTrackSupportAvailable = _isNextTrackSupportAvailForBT;

   //_btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isFastForwardSupportAvailableItem);
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isNextTrackSupportAvailableItem);

   if (_btButtonSupportStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateBTFFNextTrackSupportStatus : Updation failed..!!!"));
   }
}


void MediaDatabinding::updateTBTInfo(const bool& isRGActive, const TBTManeuverInfoData& TBTManeuverInfo)
{
   (*_mTBTManeuverInfo).mManeuverSymbolIndex = TBTManeuverInfo.mManeuverSymbolIndex;
   (*_mTBTManeuverInfo).mDistanceToManeuver = TBTManeuverInfo.mDistanceToManeuver;
   (*_mTBTManeuverInfo).mShowManeuverInfo = TBTManeuverInfo.mShowManeuverInfo;
   (*_mTBTManeuverInfo).mRoundAboutExitNumber = TBTManeuverInfo.mRoundAboutExitNumber;
   (*_mTBTManeuverInfo).mSwitchAlbumArtOrTBT = isRGActive;
   _mTBTManeuverInfo.MarkAllItemsModified();
   if (_mTBTManeuverInfo.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateTBTInfo : Updation failed..!!!")) ;
   }
}


/**
 * updateBTFRPrevTrackSupportStatus - Function to control Enable/Disable Functionality of the buttons in BT Main Screen..
 * @param[in] bool,bool
 * @parm[out] None
 * @return void
 */

void MediaDatabinding::updateBTFRPrevTrackSupportStatus(Courier::Bool isFastRewindSupportAvailable, Courier::Bool isPreviousTrackSupportAvailable)
{
   ETG_TRACE_USR4(("updateBTFRPrevTrackSupportStatus"));
   (*_btButtonSupportStatus).misFastRewindSupportAvailable = isFastRewindSupportAvailable;
   (*_btButtonSupportStatus).misPreviousTrackSupportAvailable = isPreviousTrackSupportAvailable;

   if (isFastRewindSupportAvailable == true || isPreviousTrackSupportAvailable == true)
   {
      _isPreviousTrackSupportAvailForBT = true;
   }
   else
   {
      _isPreviousTrackSupportAvailForBT = false;
   }
   (*_btButtonSupportStatus).misPreviousTrackSupportAvailable = _isPreviousTrackSupportAvailForBT;
   //_btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isFastRewindSupportAvailableItem);
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isPreviousTrackSupportAvailableItem);

   if (_btButtonSupportStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateBTFRPrevTrackSupportStatus : Updation failed..!!!"));
   }
}


void MediaDatabinding::updateBTPlayTimeSupportStatus(Courier::Bool isPlayTimeSupportAvailable)
{
   ETG_TRACE_USR4(("updateBTPlayTimeSupportStatus %d", isPlayTimeSupportAvailable));
   (*_btButtonSupportStatus).misPlayTimeSupportAvailable = isPlayTimeSupportAvailable;
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isPlayTimeSupportAvailableItem);
   if (_btButtonSupportStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateBTPlayTimeSupportStatus : Updation failed..!!!"));
   }
}


void MediaDatabinding::updateFolderOrCurrentPlayingListBtnText(Candera::String buttonText)
{
   ETG_TRACE_USR4(("updateFolderOrCurrentPlayingListBtnText"));
   (*_browseScreenButton).mFolderorCurrentPlayingListBtnText = buttonText.GetCString();
   _browseScreenButton.MarkItemModified(ItemKey::BrowseScreenButtons::FolderorCurrentPlayingListBtnTextItem);

   if (_browseScreenButton.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateFolderOrCurrentPlayingListBtnText : Updation failed..!!!"));
   }
}


/**
 * updateAlbumArtTogglestatus - Function to update album art toggling status in  Main Screen..
 * @param[in] bool
 * @parm[out] None
 * @return void
 */
void MediaDatabinding::updateAlbumArtTogglestatus(bool toggleStatus)
{
   (*_browseScreenButton).mIsAlbumArtEnable = toggleStatus;

   _browseScreenButton.MarkItemModified(ItemKey::BrowseScreenButtons::IsAlbumArtEnableItem);

   if (_browseScreenButton.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateTogglestateInfo : Updation failed..!!!"));
   }
}


void MediaDatabinding::updateDefaultAlbumArtVisibleStatus(bool toggleStatus)
{
   (*_albumArtData).mDefaultAlbumArtVisibility = toggleStatus;
   _albumArtData.MarkItemModified(ItemKey::AlbumArtData::DefaultAlbumArtVisibilityItem);
   if (_albumArtData.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateTogglestateInfo : Updation failed..!!!"));
   }
}


void MediaDatabinding::updateAlbumArtVisibleStatus(bool bitmapStatus)
{
   (*_albumArtData).mAlbumArtVisibility = bitmapStatus;
   _albumArtData.MarkItemModified(ItemKey::AlbumArtData::AlbumArtVisibilityItem);
   if (_albumArtData.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateTogglestateInfo : Updation failed..!!!"));
   }
}


void MediaDatabinding::updateCurrentPlayingListBtnStatus(bool buttonStatus)
{
   (*_browseScreenButton).mCurrentPlayingListBtnEnableStatus = buttonStatus;
   _browseScreenButton.MarkItemModified(ItemKey::BrowseScreenButtons::CurrentPlayingListBtnEnableStatusItem);

   if (_browseScreenButton.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateTogglestateInfo : Updation failed..!!!"));
   }
}


/**
 * UpdateMediaMetadataInfo - Function to update meta data to GUI
 * @param[in] ArtistName, AlbumName, TitleName
 * @parm[out] None
 * @return void
 */
void MediaDatabinding::updateMediaMetadataInfo(std::string strArtist, std::string strAlbum, std::string strTitle)
{
   MediaDatabindingCommon::updateMediaMetadataInfo(strArtist, strAlbum, strTitle);
   if ((_audioSource > SRC_MEDIA_AUX && _audioSource <= SRC_PHONE_BTAUDIO))
   {
      ETG_TRACE_USR4(("audio source = %d mode value = %d", _audioSource, _modeValue));
      POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION)));
   }
}


/**
 * updateTrackNumber  - called to update the track no details
 * @param[in] int,int
 * @return void
 */
void MediaDatabinding::updateTrackNumber(uint32 currentTrackNo, uint32 totalTrackNo)
{
   char currentTrack[ARRAY_SIZE] = "\0";
   char totalTrack[ARRAY_SIZE] = "\0";
   currentTrackNo = currentTrackNo + 1;// 1 is added since mediaplayer count starts from 0
   snprintf(currentTrack, sizeof currentTrack, "%d", currentTrackNo);
   snprintf(totalTrack, sizeof totalTrack, "%d", totalTrackNo);
   std::string currentPlayingTrack = currentTrack;
   std::string totalAvailableTracks = totalTrack;
   std::string trackinfo = currentPlayingTrack + "/" +  totalAvailableTracks;
   _currentTrackInfo = trackinfo;
   (*_trackNumberDetails).mCurrentTrackNo = trackinfo.c_str();
   (*_trackNumberDetails).mCurrentPlayingTrackNo = currentTrack;
   _trackNumberDetails.MarkItemModified(ItemKey::TrackNumberDetails::CurrentTrackNoItem);
   _trackNumberDetails.MarkItemModified(ItemKey::TrackNumberDetails::CurrentPlayingTrackNoItem);
   ETG_TRACE_USR4(("Current playing trackNumber is : %s", trackinfo.c_str()));
   if (_trackNumberDetails.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_trackNumberDetails : Updation failed..!!!"));
   }
}


/**
 * updateVolumeAvailability - This is a property update function and it will be called by sound whenever there is a change in Volume
 *
 * @param[in] volumelevel
 * @return void
 */
void MediaDatabinding::updateVolumeAvailability(int16 volumeLevel)
{
   ETG_TRACE_USR4(("updateVolumeAvailability %d", volumeLevel));
#ifdef VOLUME_ZERO_SUPPORT
   _isVolumeAvailable = (volumeLevel == 0) ? false : true;
   (*_volumeInfo).mIsVolumeAvailable = _isVolumeAvailable;
   _volumeInfo.MarkAllItemsModified();
   if (_volumeInfo.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("onVolumeUpdate : Updation failed..!!!"));
   }
   if ((_isPreviousTrackSupportAvailForBT == true) && ((*_volumeInfo).mIsVolumeAvailable == true))
   {
      (*_btButtonSupportStatus).misPreviousTrackSupportAvailable = true;
   }
   else
   {
      (*_btButtonSupportStatus).misPreviousTrackSupportAvailable = false;
   }
   if ((_isNextTrackSupportAvailForBT == true) && ((*_volumeInfo).mIsVolumeAvailable == true))
   {
      (*_btButtonSupportStatus).misNextTrackSupportAvailable = true;
   }
   else
   {
      (*_btButtonSupportStatus).misNextTrackSupportAvailable = false;
   }
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isNextTrackSupportAvailableItem);
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isPreviousTrackSupportAvailableItem);
   if (_btButtonSupportStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("onVolumeUpdate : Updation failed..!!!"));
   }
#endif
}


void MediaDatabinding::updateFooterInFolderBrowse(std::string FooterText)
{
   (*_trackNumberDetails).mCurrentFocussedFolderNo = FooterText.c_str();
   _trackNumberDetails.MarkItemModified(ItemKey::TrackNumberDetails::CurrentFocussedFolderNoItem);
   if (_trackNumberDetails.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_trackNumberDetails : Updation failed..!!!"));
   }
}


void MediaDatabinding::SetApplicationModeValue(uint8 ApplicationModeValue)
{
   _modeValue = ApplicationModeValue;
}


/**
 * updateVerticalSearchListIndex - This is a property update function and it will be called by sound whenever there is a change in Volume
 *
 * @param[in] volumelevel
 * @return void
 */
bool MediaDatabinding::updateVerticalSearchListIndex(uint32 startIndex, uint32 switchIndex)
{
   ETG_TRACE_USR4(("startIndex %d,%d", startIndex, switchIndex));
   (*_VerticalSearchList).mExpandedListStartIndex = startIndex;
   (*_VerticalSearchList).mListSwitchIndex = switchIndex;

   _VerticalSearchList.MarkAllItemsModified();
   if (_VerticalSearchList.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_VerticalSearchList : Updation failed..!!!"));
      return false;
   }
   return true;
}


/**
 * updateVolumeAvailability - This is a property update function and it will be called by sound whenever there is a change in Volume
 *
 * @param[in] volumelevel
 * @return void
 */

bool MediaDatabinding::updateVerticalSearchLanguageSwitch(bool value)
{
   ETG_TRACE_USR4(("updateVerticalSearchLanguageSwitch %d", value));
   (*_VerticalSearchList).mLanguageSwitch = value;

   _VerticalSearchList.MarkItemModified(ItemKey::VerticalSearchList::LanguageSwitchItem);
   if (_VerticalSearchList.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_VerticalSearchList : Updation failed..!!!"));
      return false;
   }
   return true;
}


void MediaDatabinding::updateGadgetScreenButtonsVisibilty(uint32 currentActiveSource)
{
   ETG_TRACE_USR4(("current active source %d", currentActiveSource));
   bool controlButtonStatus = (currentActiveSource == SOURCE_AUX) ? false : true;
   (*_trackNumberDetails).mGadgetScreenButtonVisibility = controlButtonStatus;
   _trackNumberDetails.MarkItemModified(ItemKey::TrackNumberDetails::GadgetScreenButtonVisibilityItem);
   if (_trackNumberDetails.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_trackNumberDetails : Updation failed..!!!"));
   }
}


void MediaDatabinding::updateCDInfo(Candera::String driveVersion, uint8 driveState)
{
   ETG_TRACE_USR4(("updateCDInfo"));
   (*_CDInfo).mDriveVersion = driveVersion.GetCString();
   if (0 == driveState)
   {
      (*_CDInfo).mDriveVersion = "NotReady";
   }
   else
   {
      (*_CDInfo).mDriveVersion = "Ready";
   }
   _CDInfo.MarkItemModified(ItemKey::CDInfoItem);
   if (_CDInfo.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("CDinfo  : Updation failed..!!!"));
   }
}


void MediaDatabinding::clearFolderorCurrentPlayingListBtnText()
{
   (*_browseScreenButton).mFolderorCurrentPlayingListBtnText = "";
   _browseScreenButton.MarkItemModified(ItemKey::BrowseScreenButtons::FolderorCurrentPlayingListBtnTextItem);
   if (_browseScreenButton.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_browseScreenButton : Updation failed..!!!"));
   }
   /**
    * updateBTDisconnectionStatusOverUSB
    * @param[in] :isBTDisconnectedOverUSB,isMetadataVisible
    * @parm[out] :None
    * @return Void
    */
}


void MediaDatabinding::updateBTDisconnectionStatusOverUSB(bool isBTDisconnectedOverUSB, bool isMetadataVisible)
{
   ETG_TRACE_USR4(("updateBTDisconnectionStatusOverUSB BTDisconnectedOverUSB: %d ,isMetadataVisible:%d", isBTDisconnectedOverUSB, isMetadataVisible));
   (*_BTDisconnectionUpdateOverUSB).mDefaultTextUpdate = isBTDisconnectedOverUSB;
   (*_BTMetadataSupport).misMetaDataSupportAvailable = isMetadataVisible;
   (*_btHandsetModeActiveStatus).misMetadataVisible = isMetadataVisible;
   (*_btButtonSupportStatus).misPlayTimeSupportAvailable = isMetadataVisible;
   (*_btButtonSupportStatus).misRepeatTextAvailable = isMetadataVisible;
   (*_btButtonSupportStatus).misRandomTextAvailable = isMetadataVisible;
   _BTDisconnectionUpdateOverUSB.MarkItemModified(ItemKey::BTScreenUpdateWhenConnectedWithUSB::DefaultTextUpdateItem);
   _BTMetadataSupport.MarkItemModified(ItemKey::BTMetaDataFunctionalityStatus::isMetaDataSupportAvailableItem);
   _btHandsetModeActiveStatus.MarkItemModified(ItemKey::BTHandSetModeActiveStatus::isMetadataVisibleItem);
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isPlayTimeSupportAvailableItem);
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isRepeatTextAvailableItem);
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isRandomTextAvailableItem);

   _BTMetadataSupport.SendUpdate(true);
   _btButtonSupportStatus.SendUpdate(true);
   _btHandsetModeActiveStatus.SendUpdate(true);
   _btMetaDataVisibility.SendUpdate(true);
   if (_BTDisconnectionUpdateOverUSB.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateBTDisconnectionStatusOverUSB : Updation failed..!!!"));
   }
}


void MediaDatabinding::updateBTMetadataSupportStatus(Courier::Bool isMetadataSupportAvailable)
{
   ETG_TRACE_USR4(("updateBTMetadataSupportStatus :%d", isMetadataSupportAvailable));
   if (isMetadataSupportAvailable)
   {
      (*_BTMetadataSupport).misMetaDataSupportAvailable = isMetadataSupportAvailable;
      (*_BTMetadataSupport).misDefaultTextAvailable = false;
   }
   else
   {
      (*_BTMetadataSupport).misMetaDataSupportAvailable = isMetadataSupportAvailable;
      (*_BTMetadataSupport).misDefaultTextAvailable = true;
   }
   _BTMetadataSupport.MarkItemModified(ItemKey::BTMetaDataFunctionalityStatus::isMetaDataSupportAvailableItem);
   _BTMetadataSupport.SendUpdate(true);
}


void MediaDatabinding::updateBTScreenOnCallHandsetModeActive(bool bisHandsetActive, bool bisMetadataVisible)
{
   ETG_TRACE_USR4(("updateBTHandsetModeActiveStatus :%d , %d", bisHandsetActive , bisMetadataVisible));
   (*_btHandsetModeActiveStatus).misHandsetModeActive = bisHandsetActive;
   (*_btHandsetModeActiveStatus).misMetadataVisible = bisMetadataVisible;
   (*_BTMetadataSupport).misMetaDataSupportAvailable = bisMetadataVisible;
   (*_btButtonSupportStatus).misPlayTimeSupportAvailable = bisMetadataVisible;
   (*_btButtonSupportStatus).misRepeatTextAvailable = bisMetadataVisible;
   (*_btButtonSupportStatus).misRandomTextAvailable = bisMetadataVisible;
   _btHandsetModeActiveStatus.MarkItemModified(ItemKey::BTHandSetModeActiveStatus::isHandsetModeActiveItem);
   _btHandsetModeActiveStatus.MarkItemModified(ItemKey::BTHandSetModeActiveStatus::isMetadataVisibleItem);
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isPlayTimeSupportAvailableItem);
   _BTMetadataSupport.MarkItemModified(ItemKey::BTMetaDataFunctionalityStatus::isMetaDataSupportAvailableItem);
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isRepeatTextAvailableItem);
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isRandomTextAvailableItem);

   _BTMetadataSupport.SendUpdate(true);
   _btButtonSupportStatus.SendUpdate(true);
   _btMetaDataVisibility.SendUpdate(true);
   ETG_TRACE_USR4(("item modified"));
   if (_btHandsetModeActiveStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("UpdateBTIconState : Updation failed..!!!"));
   }
}


}
}
