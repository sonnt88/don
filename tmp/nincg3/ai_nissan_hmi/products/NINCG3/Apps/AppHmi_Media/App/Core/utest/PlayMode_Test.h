
#ifndef PLAYMODE_TEST_H_
#define PLAYMODE_TEST_H_

#include "gtest/gtest.h"
#include "Spy/SpyPlayMode.h"

using namespace std::tr1;
using namespace App::Core;


class PlayMode_Test_getNextRepeatState :
   public testing::TestWithParam < tuple < PlayingSelection , ::MPlay_fi_types::T_e8_MPlayRepeat , ::MPlay_fi_types::T_e8_MPlayRepeat> >
{
   protected:
      SpyPlayMode _spyPlayModeObj;
      PlayingSelection _currentPlayingSelection;
      ::MPlay_fi_types::T_e8_MPlayRepeat _repeatStatus;
      ::MPlay_fi_types::T_e8_MPlayRepeat _expectedRepeatStatus;

   protected:
      virtual void SetUp()
      {
         _currentPlayingSelection			= get <0> (GetParam());
         _repeatStatus						= get <1> (GetParam());
         _expectedRepeatStatus  			= get <2> (GetParam());
      }
      virtual void TearDown() {}
};


class PlayMode_Test_getNextRandomState :
   public testing::TestWithParam < tuple < ::MPlay_fi_types::T_e8_MPlayMode , ::MPlay_fi_types::T_e8_MPlayMode> >
{
   protected:
      SpyPlayMode _spyPlayModeObj;

      ::MPlay_fi_types::T_e8_MPlayMode _randomStatus;
      ::MPlay_fi_types::T_e8_MPlayMode _expectedRandomStatus;
   protected:
      virtual void SetUp()
      {
         _randomStatus				= get <0> (GetParam());
         _expectedRandomStatus   	= get <1> (GetParam());
      }
      virtual void TearDown() {}
};



class PlayMode_Test_updateRandomRepeatStateScope1 :
   public testing::TestWithParam < tuple < ::MPlay_fi_types::T_e8_MPlayMode , ::MPlay_fi_types::T_e8_MPlayRepeat , PlayingSelection , bool , bool, std::string > >
{
   protected:
      SpyPlayMode _spyPlayModeObj;

      PlayingSelection _currentPlayingSelection;
      ::MPlay_fi_types::T_e8_MPlayRepeat _repeatStatus;
      ::MPlay_fi_types::T_e8_MPlayMode _randomStatus;
      bool _expectedRepeatStatus;
      bool _expectedRandomStatus;
      std::string _expectedRepeatRandomText;

   protected:
      virtual void SetUp()
      {
         _randomStatus				= get <0> (GetParam());
         _repeatStatus				= get <1> (GetParam());
         _currentPlayingSelection  	= get <2> (GetParam());
         _expectedRandomStatus		= get <3> (GetParam());
         _expectedRepeatStatus		= get <4> (GetParam());
         _expectedRepeatRandomText	= get <5> (GetParam());
      }
      virtual void TearDown() {}
};
class PlayMode_Test_updateRandomRepeatStateScope2 :
   public testing::TestWithParam < tuple < ::MPlay_fi_types::T_e8_MPlayMode , ::MPlay_fi_types::T_e8_MPlayRepeat , PlayingSelection , bool, bool, std::string , std::string > >
{
   protected:
      SpyPlayMode _spyPlayModeObj;

      PlayingSelection _currentPlayingSelection;
      ::MPlay_fi_types::T_e8_MPlayRepeat _repeatStatus;
      ::MPlay_fi_types::T_e8_MPlayMode _randomStatus;
      bool _expectedRepeatStatus;
      bool _expectedRandomStatus;
      std::string _expectedRandomText;
      std::string _expectedRepeatText;

   protected:
      virtual void SetUp()
      {
         _randomStatus				= get <0> (GetParam());
         _repeatStatus				= get <1> (GetParam());
         _currentPlayingSelection  	= get <2> (GetParam());
         _expectedRandomStatus		= get <3> (GetParam());
         _expectedRepeatStatus		= get <4> (GetParam());
         _expectedRandomText			= get <5> (GetParam());
         _expectedRepeatText			= get <6> (GetParam());
      }
      virtual void TearDown() {}
};
#endif
