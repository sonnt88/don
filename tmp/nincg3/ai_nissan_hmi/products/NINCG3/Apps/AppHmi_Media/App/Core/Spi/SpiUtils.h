/**
*  @file   SpiUtils.h
*  @author ECV - bvi1cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/


#ifndef SPI_UTILS_H_
#define SPI_UTILS_H_

#include "asf/core/Types.h"
#include "midw_smartphoneint_fi_types.h"

using namespace ::midw_smartphoneint_fi_types;

namespace App
{
namespace Core
{

/*enum deviceStatusInfo
{
	   T_e8_DeviceStatusInfo__NOT_KNOWN = 0u,
	   T_e8_DeviceStatusInfo__DEVICE_ADDED = 1u,
	   T_e8_DeviceStatusInfo__DEVICE_REMOVED = 2u,
	   T_e8_DeviceStatusInfo__DEVICE_CHANGED = 3u
};


enum deviceConnectionTypeInfo
{
	   T_e8_DeviceConnectionType__UNKNOWN_CONNECTION = 0u,
	   T_e8_DeviceConnectionType__USB_CONNECTED = 1u,
	   T_e8_DeviceConnectionType__WIFI_CONNECTED = 2u
};*/

class SpiUtils
{
   public:
      static bool updateDeviceStatusValidity(int8 deviceStatus);
      static bool deviceConnectiontypeValidity(int8 deviceConnectionType);
      static bool checkUSBConnectedDeviceValidity(int8 deviceStatus, int8 deviceConnectionType);
      static bool dipoConnectionValidity(int8 deviceConnectionStatus);
};


} // end of namespace Core
} // end of namespace App


#endif /* SPI_UTILS_H_ */
