/**
*  @file   <KdsInfo_Test.h>
*  @author <ECV> - <pul1hc>
*  @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
*  @addtogroup <AppHmi_media>
*  @{
*/

#ifndef KDSINFO_TEST_H_
#define KDSINFO_TEST_H_

#include "gtest/gtest.h"
#include "stubs/hall_std_if.h"
#include "Core/Utils/KdsInfo.h"

using namespace std::tr1;
using namespace App::Core;

class KdsInfo_Test__isJapanRegion_True :
   public testing::TestWithParam < tuple < uint8 > >
{
   protected:
      uint8 __currentRegion;

   protected:
      virtual void SetUp()
      {
         __currentRegion = get <0> (GetParam());
         KdsInfo::setCurrentRegion(__currentRegion);
      }
      virtual void TearDown() {}
};


class KdsInfo_Test__isJapanRegion_False :
   public testing::TestWithParam < tuple < uint8 > >
{
   protected:
      uint8 __currentRegion;

   protected:
      virtual void SetUp()
      {
         __currentRegion = get <0> (GetParam());
         KdsInfo::setCurrentRegion(__currentRegion);
      }
      virtual void TearDown() {}
};


class KdsInfo_Test__isEuropeonRegion_True :
   public testing::TestWithParam < tuple < uint8 > >
{
   protected:
      uint8 __currentRegion;

   protected:
      virtual void SetUp()
      {
         __currentRegion = get <0> (GetParam());
         KdsInfo::setCurrentRegion(__currentRegion);
      }
      virtual void TearDown() {}
};


class KdsInfo_Test__isEuropeonRegion_False :
   public testing::TestWithParam < tuple < uint8 > >
{
   protected:
      uint8 __currentRegion;

   protected:
      virtual void SetUp()
      {
         __currentRegion = get <0> (GetParam());
         KdsInfo::setCurrentRegion(__currentRegion);
      }
      virtual void TearDown() {}
};


#endif /* KDSINFO_TEST_H_ */
