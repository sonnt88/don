/**
 *  @file   MediaDeviceConnectionHandling.h
 *  @author ECV - kng3kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */


#ifndef _MEDIA_DEVICE_CONNECTIONS_H_
#define _MEDIA_DEVICE_CONNECTIONS_H_

#include "MediaDatabinding.h"
#include "DeviceConnectionOserver.h"
#include "IDeviceConnection.h"
#include "Core/ContextSwitch/ContextSwitchHomeScreen.h"

#define MPLAY_MAX_DEVICES 11
#define CONVERT_DEVICESIZE_KB_TO_MB 1024
#define MAX_BUFFER_SIZE 64

namespace App {
namespace Core {
class IBTSettings;
class MediaDeviceConnectionHandling
   : public IDeviceConnection
   , public ::mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsCallbackIF
   , public ::mplay_MediaPlayer_FI::IndexingStateCallbackIF
   , public ::mplay_MediaPlayer_FI::MediaPlayerOpticalDiscSlotStateCallbackIF
   , public ::mplay_MediaPlayer_FI::ActiveMediaDeviceCallbackIF
   , public ::mplay_MediaPlayer_FI::MediaPlayerOpticalDiscCDInfoCallbackIF
{
   public:
      virtual ~MediaDeviceConnectionHandling();
      MediaDeviceConnectionHandling(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy,
                                    ContextSwitchHomeScreen* pHomeScreen, IBTSettings* _pBTSettings);
      void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                              const asf::core::ServiceStateChange& /*stateChange*/);
      void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                                const asf::core::ServiceStateChange& /*stateChange*/);

      virtual void onMediaPlayerDeviceConnectionsStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsStatus >& status);
      virtual void onMediaPlayerDeviceConnectionsError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerDeviceConnectionsError >& /*error*/);
      virtual void onIndexingStateError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
                                        const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::IndexingStateError >& /*error*/);
      virtual void onIndexingStateStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
                                         const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::IndexingStateStatus >& status);
      virtual void onMediaPlayerOpticalDiscSlotStateError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerOpticalDiscSlotStateError >& /*error*/);
      virtual void onMediaPlayerOpticalDiscSlotStateStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerOpticalDiscSlotStateStatus >& status);
      virtual void onActiveMediaDeviceError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
                                            const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::ActiveMediaDeviceError >& /*error*/);
      virtual void onActiveMediaDeviceStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
                                             const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::ActiveMediaDeviceStatus >& status);
      virtual void onMediaPlayerOpticalDiscCDInfoError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerOpticalDiscCDInfoError >& /*error*/);
      virtual void onMediaPlayerOpticalDiscCDInfoStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/,
            const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::MediaPlayerOpticalDiscCDInfoStatus >& status);

      virtual std::string getActiveDeviceName(uint32 sHeaderIndex);
      virtual void registerObserver(IDeviceConnectionOserver*);
      virtual void deregisterObserver(IDeviceConnectionOserver* pObserver);
      virtual uint32 getNewlyInsertedDeviceTag();
      virtual void resetNewlyInsertedDeviceTag();
      bool getConnectedDeviceSize(std::string&);

      struct trDevice
      {
         bool bIsActiveSource;
         bool bIsConnected;
         std::string sDeviceName;
         unsigned int uiDeviceTag;
         unsigned int uiDeviceType;
         unsigned int uiDeviceState;
         unsigned int uiNumberofFiles;
         uint8 indexedState;
         uint8 disconnectReason;
         uint32 deviceSize;
         int8 i8AvailabilityReason;
      };

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_MAP_DELEGATE_END()

   private:
      void vInitialiseDeviceList();
      void UpdateDeviceConnection(int8, uint8, int8) ;
      void UpdateDeviceDisconnection(int8, int8) const;
      void UpdateNoMediaFiles() const;
      void updateDeviceStatusOnDisconnection(int8);
      void UpdateCommunicationErrorStatus(int8, int8);
      void UpdateUnsupportedErrorStatus(int8 i8DeviceState , int8 i8DeviceType);
      void notifyDeviceConnectionObserver();
      Candera::String getDeviceText(uint32 sHeaderIndex);
      uint32 u32GetActiveDeviceTag();
      void setSpiActiveStatus(bool);
      virtual void setActiveMediaDevice(uint32 deviceTag);

      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& _mediaPlayerProxy;
      IBTSettings& _iBTSettings;
      trDevice _oDeviceList[MPLAY_MAX_DEVICES];
      MPlay_fi_types::T_e8_MPlayDeviceIndexedState _deviceIndexState;
      ContextSwitchHomeScreen& _DeviceHomeScreen;
      std::vector<IDeviceConnectionOserver*> _deviceConnectionObserver;
      uint32 _prevDeviceTag;
      uint32 _NewlyMediaPlayerSourceId;
      bool _spiActivestatus;
};


}// namespace AppLogic
} // namespace App

#endif // _MEDIA_DEVICE_CONNECTIONS_H_
