/**
 *  @file   SpiMetaData.h
 *  @author ECV - ndi6kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#ifndef _SPI_METADATA_H_
#define _SPI_METADATA_H_

#include "midw_smartphoneint_fiProxy.h"
#include "AppBase/ServiceAvailableIF.h"
#include "AppHmi_MediaStateMachine.h"

namespace App {
namespace Core {

class SpiMetaData :
   public hmibase::ServiceAvailableIF,
   public StartupSync::PropertyRegistrationIF,
   public ::midw_smartphoneint_fi::ApplicationPhoneDataCallbackIF

{
   public :
      virtual ~SpiMetaData();
      SpiMetaData(::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& spiMetaDataProxy);

      void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);
      void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);

      virtual void onApplicationPhoneDataError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::ApplicationPhoneDataError >& error);
      virtual void onApplicationPhoneDataStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::ApplicationPhoneDataStatus >& status);

      Courier::DataItemContainer<DIPOPhoneMetaDataDataBindingSource> _updatePhoneMetadata;
   private:
      ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& _spiMetaDataProxy;
      void updateCallDuration(uint32);
      void updateCallDirection(::midw_smartphoneint_fi_types::T_e8_PhoneCallDirection);
      void updateCallState(::midw_smartphoneint_fi_types::T_e8_PhoneCallState);

      ::std::string _mStrPhoneNumber;
      ::std::string _mStrDisplayName;
      ::std::string _mStrCallDirection;
      ::std::string _mStrCallState;
      uint32 _mCallDuration;
};


} /* namespace Core */
} /* namespace App */

#endif /* _SPI_METADATA_H_ */
