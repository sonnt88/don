/**
*  @file   MediaSettings.h
*  @author ECV - pkm8cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef MEDIASETTINGS_H_
#define MEDIASETTINGS_H_

#include "AppHmi_MediaConstants.h"
#include "AppHmi_MediaMessages.h"
#include "AppHmi_MediaTypes.h"
#include "CourierTunnelService/CourierMessageReceiverStub.h"
#include "MediaDatabinding.h"
#include "mplay_MediaPlayer_FIProxy.h"

namespace App {
namespace Core {

enum videoViewMode
{
   FULL = 0,
   FIT,
};


class MediaSettings
#ifdef PHOTOPLAYER_SUPPORT
   : public ::mplay_MediaPlayer_FI::SlideshowTimeCallbackIF
#endif
{
   public:
      ~MediaSettings();
      static MediaSettings* getInstance();
      static void createInstance(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy);
      tSharedPtrDataProvider getMediaSettingsListDataProvider(uint8 infoSettings);

#ifdef PHOTOPLAYER_SUPPORT
      virtual void onSlideshowTimeError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::SlideshowTimeError >& error);
      virtual void onSlideshowTimeStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::SlideshowTimeStatus >& status);
#endif

      void setViewMode(bool);
      bool getViewMode();
      void updateMediaPlayerSettings();
      virtual bool onCourierMessage(const VideoModeBtnPressUpdMsg& /*oMsg*/);
#ifdef PHOTOPLAYER_SUPPORT
      virtual bool onCourierMessage(const SettingTimePhotoPlayerSlideShowReqMsg& /*oMsg*/);
      virtual bool onCourierMessage(const OnOffAnimationEffectPhotoPlayerReqMsg& /*oMsg*/);
#endif

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      ON_COURIER_MESSAGE(VideoModeBtnPressUpdMsg)
#ifdef PHOTOPLAYER_SUPPORT
      ON_COURIER_MESSAGE(SettingTimePhotoPlayerSlideShowReqMsg)
      ON_COURIER_MESSAGE(OnOffAnimationEffectPhotoPlayerReqMsg)
#endif
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_MAP_DELEGATE_END()

   private:
      const char* getlistItemTemplateForMediaSettings(uint32& index, uint8 infoSettings);
      Candera::String getMediaSettingsDataInfoItemValue(uint8 index, uint8 infoSettings);
#ifdef PHOTOPLAYER_SUPPORT
      PhotoPlayerSettingsData getCurrentPhotoSettingsData();
      uint32 _u32SlideshowTimeStatus;
      bool _bAnimationEffect;
#endif
      bool _videoViewMode;
      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& _mediaPlayerProxy;
      static MediaSettings* _theInstance;
      MediaSettings(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy);
};


}
}


#endif /* MEDIASETTINGS_H_ */
