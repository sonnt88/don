/**
*  @file   MetaData_Test.cpp
*  @author RBEI/ECV-AIVI_MediaTeam
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#include "MetaData_Test.h"

using namespace ::MPlay_fi_types;
//Initialize "MediaUtilsTest_ParamValues" with values
INSTANTIATE_TEST_CASE_P(
   MetaData_ParamTest,
   MetaData_Extraction_Test,
   ::testing::Values
   (
      //INPUT
      //1 to 4 - MetaData Fields
      //5th    - CategoryType

      //Expected output
      //6 to 15- Gener, Artist,  Album,   Song,    Composer, B.Title, Chapter, P.Name, PEpisode, Playlist Name

      make_tuple("", "", "", "", T_e8_MPlayCategoryType__e8CTY_NONE, "", "", "", "", "", "", "", "", "", ""),
      make_tuple("Blues", "", "", "", T_e8_MPlayCategoryType__e8CTY_GENRE, "Blues", "", "", "", "", "", "", "", "", ""),
      make_tuple("", "Raja", "", "", T_e8_MPlayCategoryType__e8CTY_ARTIST, "", "Raja", "", "", "", "", "", "", "", ""),
      make_tuple("Blues", "Raja", "Stage", "", T_e8_MPlayCategoryType__e8CTY_ALBUM, "Blues", "Raja", "Stage", "", "", "", "", "", "", ""),
      make_tuple("Blues", "Raja", "Stage", "TheSong", T_e8_MPlayCategoryType__e8CTY_SONG, "Blues", "Raja", "Stage", "TheSong", "", "", "", "", "", ""),
      make_tuple("Composer", "", "", "", T_e8_MPlayCategoryType__e8CTY_COMPOSER, "", "", "", "", "Composer", "", "", "", "", ""),
      make_tuple("", "", "", "", T_e8_MPlayCategoryType__e8CTY_AUTHOR, "", "", "", "", "", "", "", "", "", ""),
      make_tuple("", "BookTitle", "", "", T_e8_MPlayCategoryType__e8CTY_TITLE, "", "", "", "", "", "BookTitle", "", "", "", ""),
      make_tuple("", "BookTitle", "Chapter", "", T_e8_MPlayCategoryType__e8CTY_CHAPTER, "", "", "", "", "", "BookTitle", "Chapter", "", "", ""),
      make_tuple("PodCastName", "", "", "", T_e8_MPlayCategoryType__e8CTY_NAME, "", "", "", "", "", "", "", "PodCastName", "", ""),
      make_tuple("PodCastName", "EPISODE", "", "", T_e8_MPlayCategoryType__e8CTY_EPISODE, "", "", "", "", "", "", "", "EPISODE", "PodCastName", ""),
      make_tuple("PLAYLIST", "", "", "", T_e8_MPlayCategoryType__e8CTY_PLAYLIST, "", "", "", "", "", "", "", "", "", "PLAYLIST"),
      make_tuple("", "", "", "", T_e8_MPlayCategoryType__e8CTY_PLAYLIST_INTERNAL, "", "", "", "", "", "", "", "", "", ""),
      make_tuple("", "", "", "", T_e8_MPlayCategoryType__e8CTY_IMAGE, "", "", "", "", "", "", "", "", "", "")
   )
);

// Test for method vConvertSecToHourMinSec's calculations for different values of seconds
TEST_P(MetaData_Extraction_Test, Valid)
{
   _metaData.setMediaObject(_mediaObject);

   EXPECT_EQ(_validGener, _metaData.getGenre()) << "Error at CategoryType: " << _mediaObject.getE8CategoryType();
   EXPECT_EQ(_validArtistName, _metaData.getArtistName()) << "Error at CategoryType: " << _mediaObject.getE8CategoryType();
   EXPECT_EQ(_validAlbumName, _metaData.getAlbumName()) << "Error at CategoryType: " << _mediaObject.getE8CategoryType();
   EXPECT_EQ(_validSongTitle, _metaData.getSongTitle()) << "Error at CategoryType: " << _mediaObject.getE8CategoryType();
   EXPECT_EQ(_validComposerName, _metaData.getComposerName()) << "Error at CategoryType: " << _mediaObject.getE8CategoryType();
   EXPECT_EQ(_validBookTitle, _metaData.getBookTitle()) << "Error at CategoryType: " << _mediaObject.getE8CategoryType();
   EXPECT_EQ(_validChapterTitle, _metaData.getChapterTitle()) << "Error at CategoryType: " << _mediaObject.getE8CategoryType();
   EXPECT_EQ(_validPodcastName, _metaData.getPodcastName()) << "Error at CategoryType: " << _mediaObject.getE8CategoryType();
   EXPECT_EQ(_validPosdcastEpisode, _metaData.getPodcastEpisode()) << "Error at CategoryType: " << _mediaObject.getE8CategoryType();
   EXPECT_EQ(_validPlayListName, _metaData.getPlayListName()) << "Error at CategoryType: " << _mediaObject.getE8CategoryType();

}
