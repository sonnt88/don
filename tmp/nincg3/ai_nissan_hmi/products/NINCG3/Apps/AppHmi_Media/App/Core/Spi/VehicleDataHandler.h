/**
 *  @file   Vehicledatahandle.cpp
 *  @author ECV - alc7kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#ifndef _VEHICLE_DATAHANDLER_H_
#define _VEHICLE_DATAHANDLER_H_

#include "AppHmi_MediaStateMachine.h"
#include "AppHmi_MediaStateMachine.h"
#include "AppHmi_SpiMessages.h"
#include "AppBase/ServiceAvailableIF.h"
#include "VEHICLE_MAIN_FIProxy.h"
#include "dimming_main_fiProxy.h"
#include "Spi_defines.h"
#include "Core/Spi/SpiConnectionHandling.h"
#include "../Apps/Common/VehicleDataHandler/VehicleDataHandler.h"
#include "bosch/cm/ai/nissan/hmi/hmidataservice/HmiDataProxy.h"


#define HMIDATASERVICE ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData

namespace App {
namespace Core {

class SpiConnectionHandling;
class VehicleDataHandler:
   public ::VEHICLE_MAIN_FI::SpeedLimitCallbackIF
   , public ::dimming_main_fi::DIM_INFO_DimmingModeCallbackIF
   , public hmibase::ServiceAvailableIF
   , public StartupSync::PropertyRegistrationIF
   , public HMIDATASERVICE::CurrentHMIStateCallbackIF
   , public ::VehicleDataCommonHandler::ISpeedLockState
   , public ::VehicleDataCommonHandler::IHandBrakeState
   , public ::VEHICLE_MAIN_FI::SpeedCallbackIF
   , public ::midw_smartphoneint_fi::VehicleMechanicalSpeedCallbackIF
   , public HMIDATASERVICE::VisibleApplicationModeCallbackIF
{
   public :
      virtual ~VehicleDataHandler();
      VehicleDataHandler(::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& pSpiVehicleinfoProxy, ::boost::shared_ptr< ::dimming_main_fi::Dimming_main_fiProxy >& pSpiDimmingfiproxy, SpiConnectionHandling* paraSpiConnectionHandling, ::VehicleDataCommonHandler::VehicleDataHandler* , ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& spiServiceProxy);

      virtual void onSpeedLimitError(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::SpeedLimitError >& error);
      virtual void onSpeedLimitStatus(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::SpeedLimitStatus >& status);

      virtual void onDIM_INFO_DimmingModeError(const ::boost::shared_ptr< ::dimming_main_fi::Dimming_main_fiProxy >& proxy, const ::boost::shared_ptr< ::dimming_main_fi:: DIM_INFO_DimmingModeError >& error);
      virtual void onDIM_INFO_DimmingModeStatus(const ::boost::shared_ptr< ::dimming_main_fi::Dimming_main_fiProxy >& proxy, const ::boost::shared_ptr< ::dimming_main_fi:: DIM_INFO_DimmingModeStatus >& status);

      void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);
      void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);

      void onCurrentHMIStateError(const ::boost::shared_ptr< HMIDATASERVICE::HmiDataProxy >& /*proxy*/, const ::boost::shared_ptr< HMIDATASERVICE::CurrentHMIStateError >& /*error*/);
      void onCurrentHMIStateUpdate(const ::boost::shared_ptr< HMIDATASERVICE::HmiDataProxy >& /*proxy*/, const ::boost::shared_ptr< HMIDATASERVICE::CurrentHMIStateUpdate >& update);

      virtual void onSpeedError(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::SpeedError >& error);
      virtual void onSpeedStatus(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::SpeedStatus >& status);

      virtual void onVehicleMechanicalSpeedError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::VehicleMechanicalSpeedError >& error);
      virtual void onVehicleMechanicalSpeedResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::VehicleMechanicalSpeedResult >& result);

      virtual void onVisibleApplicationModeError(const ::boost::shared_ptr< HMIDATASERVICE::HmiDataProxy >& /*proxy*/, const ::boost::shared_ptr< HMIDATASERVICE::VisibleApplicationModeError >& /*error*/);
      virtual void onVisibleApplicationModeUpdate(const ::boost::shared_ptr< HMIDATASERVICE::HmiDataProxy >& /*proxy*/, const ::boost::shared_ptr< HMIDATASERVICE::VisibleApplicationModeGet >& update);

      virtual void currentSpeedLockState(bool State);
      virtual void currentHandBrakeState(uint8 State);

      bool onCourierMessage(const SpiLockApplicationUpdMsg& appLockStateMsg); //skn6kor

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      ON_COURIER_MESSAGE(SpiLockApplicationUpdMsg)
      COURIER_MSG_MAP_END();

   private :
      void setDimmingModeStatus(const ::boost::shared_ptr< ::dimming_main_fi:: DIM_INFO_DimmingModeStatus >& status);

      SpiConnectionHandling* _mSpiConnectionHandling;
      ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& _spiVehicleinfoProxy;
      ::boost::shared_ptr< ::dimming_main_fi::Dimming_main_fiProxy > _spiDimmingFiProxy;
      ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::HmiDataProxy > _hmiDataProxy;
      ::VehicleDataCommonHandler::VehicleDataHandler* _commonVehicle;
      ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& _spiServiceProxy;
      bool _mbAccessoryDisplayContext;
      ::midw_smartphoneint_fi_types::T_e8_ParkBrake _mParkBrakeInfo;
      ::midw_smartphoneint_fi_types::T_e8_VehicleState _mVehicleState;
};


} //namespace Core
} //namespace app

#endif  /* _VEHICLE_DATAHANDLER_H_*/
