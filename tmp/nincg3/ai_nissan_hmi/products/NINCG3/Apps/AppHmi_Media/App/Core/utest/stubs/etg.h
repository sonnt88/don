/**
*  @file   <etg.h>
*  @author <ECV> - <pul1hc>
*  @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
*  @addtogroup <AppHmi_media>
*  @{
*/

#ifndef _ET_ETG_H
#define _ET_ETG_H

#ifdef __cplusplus
extern "C"
{
#endif


#define ETG_TRACE_USR4
   // macro to (un)register ETG_I- commands of a file
   // this must be called once per file at very first time (e.g. constructor)
#define ETG_I_REGISTER_FILE()


#ifdef __cplusplus
}


#endif

#endif
