/**
 *  @file   AccessoryAppStateHandler.h
 *  @author ECV - alc7kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#ifndef _ACCESSORY_APPSTATE_HANDLER_H_
#define _ACCESSORY_APPSTATE_HANDLER_H_

#include "AppBase/ServiceAvailableIF.h"
#include "AppHmi_MediaStateMachine.h"
#include "AppHmi_SpiMessages.h"
#include "org/bosch/cm/navigation/NavigationServiceProxy.h"
#include "Core/Spi/SpiConnectionHandling.h"
#include "sds2hmi_sds_fiProxy.h"
#include "MOST_Tel_FIProxy.h"
#include "Spi_defines.h"
#include "Common/UserInterfaceCtrl/UserInterfaceScreenSaver.h"

namespace App {
namespace Core {

class SpiConnectionHandling;
class SpiKeyHandler;

class AccessoryAppStateHandler:
   public hmibase::ServiceAvailableIF,
   public StartupSync::PropertyRegistrationIF,
   public ::midw_smartphoneint_fi::DiPOAppStatusInfoCallbackIF,
   public org::bosch::cm::navigation::NavigationService::NavStatusCallbackIF,
   public org::bosch::cm::navigation::NavigationService::StopGuidanceCallbackIF,
   public org::bosch::cm::navigation::NavigationService::CancelRouteGuidanceCallbackIF,
   public sds2hmi_sds_fi::SDS_StatusCallbackIF,
   public ::MOST_Tel_FI::CallStatusNoticeCallbackIF

{
   public :
      virtual ~ AccessoryAppStateHandler();
      AccessoryAppStateHandler(SpiConnectionHandling* paraSpiConnectionHandling, SpiKeyHandler* poKeyHandler, ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& spiMidwServiceProxy);

      void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);
      void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);

      virtual void onDiPOAppStatusInfoError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DiPOAppStatusInfoError >& error);
      virtual void onDiPOAppStatusInfoStatus(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::DiPOAppStatusInfoStatus >& status);

      virtual void onStopGuidanceError(const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy, const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::StopGuidanceError >& error);
      virtual void onStopGuidanceResponse(const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy, const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::StopGuidanceResponse >& response);

      virtual void onCancelRouteGuidanceError(const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy, const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::CancelRouteGuidanceError >& error);
      virtual void onCancelRouteGuidanceResponse(const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy, const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::CancelRouteGuidanceResponse >& response);

      virtual void onNavStatusError(const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy, const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::NavStatusError >& error);
      virtual void onNavStatusUpdate(const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::NavigationServiceProxy >& proxy, const ::boost::shared_ptr< ::org::bosch::cm::navigation::NavigationService::NavStatusUpdate >& update);

      virtual void onSDS_StatusError(const ::boost::shared_ptr< ::sds2hmi_sds_fi::Sds2hmi_sds_fiProxy >& proxy, const ::boost::shared_ptr< ::sds2hmi_sds_fi::SDS_StatusError >& error);
      virtual void onSDS_StatusStatus(const ::boost::shared_ptr< ::sds2hmi_sds_fi::Sds2hmi_sds_fiProxy >& proxy, const ::boost::shared_ptr< ::sds2hmi_sds_fi::SDS_StatusStatus >& status);

      virtual void onCallStatusNoticeError(const ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeError >& /*error*/);
      virtual void onCallStatusNoticeStatus(const ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeStatus >& status);
      bool bIsBlockClockscreenSaver();
      void vUpdateBlockClockScreenSaverState();
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      bool onCourierMessage(const SetAppModeAndContext& oMsg);
      bool onCourierMessage(const SetAppModeOnCheck& oMsg);
      void vUpdateApplicationModeOnCall();
      void vUpdateApplicationModeonSpeech();
      void vUpdateAapCallDgStatus(bool);
#endif
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_CASE_DUMMY_ENTRY()
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      ON_COURIER_MESSAGE(SetAppModeAndContext)
      ON_COURIER_MESSAGE(SetAppModeOnCheck)
#endif
      COURIER_MSG_MAP_END();
      DataBindingItem<BlockClockScreenOnHKILLUMLongPressDataBindingSource> _BlockClockScreenOnHKILLUMLongPressState;
      DataBindingItem<AndroidAutoCallStatusDataBindingSource> _AndroidAutoCallStatus;
   private :
      enPhoneAppState evaluateBTTelCallStatus(const boost::shared_ptr< ::MOST_Tel_FI::CallStatusNoticeStatus >& status);
      enPhoneAppState evaluateCallInstance1(uint8 u8NewCallState1);
      enPhoneAppState evaluateCallInstance2(uint8 u8NewCallState2);
      bool isCallSatatusChange(enPhoneAppState);
      void vHandleClockscreenSaver(enSpeechAppState, enPhoneAppState, enNavigationAppState);
      SpiConnectionHandling* _mpSpiConnectionHandling;
      SpiKeyHandler* _mpoSpiKeyHandler;
      ::boost::shared_ptr< org::bosch::cm::navigation::NavigationService::NavigationServiceProxy> _spiNavigationServiceProxy;
      ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy>& _spiMidwServiceProxy;
      ::boost::shared_ptr< ::sds2hmi_sds_fi::Sds2hmi_sds_fiProxy> _spiSdshmiProxy;
      ::boost::shared_ptr< ::MOST_Tel_FI::MOST_Tel_FIProxy > _spitelephoneProxy;
      enSpeechAppState _meSpeechAppState;
      enPhoneAppState  _mePhoneAppState ;
      enPhoneAppState  _meDIPOPhoneAppState;
      enNavigationAppState _meNavigationAppState;
      bool _mbBlockScreenSaver;
      bool _mbSpeechActiveStatus;
      UserInterfaceScreenSaver* _mInterfaceScreenSaver;
};


} //namespace Core
} //namespace app

#endif  /* _VEHICLE_DATAHANDLER_H_*/
