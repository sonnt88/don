/**
 *  @file   CoverFlowBrowseHandler.cpp
 *  @author ECV - mth4cob
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#include "hall_std_if.h"
#include "Core/CoverFlow/CoverFlowHandler.h"
#include "MediaDatabinding.h"
#include "CgiExtensions/ImageLoader.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL_BROWSE
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::CoverFlowHandler::
#include "trcGenProj/Header/CoverFlowHandler.cpp.trc.h"
#endif

static const char* const DATA_CONTEXT_COVER_FLOW_LIST_ITEM    = "BitmapNode";

using namespace ::MPlay_fi_types;
namespace App {
namespace Core {


CoverFlowHandler::CoverFlowHandler(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy):
   _mediaPlayerProxy(pMediaPlayerProxy)
{
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_BROWSER_COVER_FLOW, this);
   ETG_I_REGISTER_FILE();
}


CoverFlowHandler::~CoverFlowHandler()
{
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_BROWSER_COVER_FLOW);
}


/**
 * This message is sent by the list widget when it requires new data.
 * This happens when the list is displayed or scrolled.
 * @param[in] - oMsg gives the list ID of the list that needs to be created
 */
bool CoverFlowHandler::onCourierMessage(const ListDateProviderReqMsg& oMsg)
{
   ETG_TRACE_USR4((" CoverFlowHandler: ListDateProviderReqMsg listId=%d", oMsg.GetListId()));
   if (oMsg.GetListId() == LIST_ID_BROWSER_COVER_FLOW)
   {
      ListDataInfo listDataInfo;
      listDataInfo.listId = oMsg.GetListId();
      listDataInfo.startIndex =  oMsg.GetStartIndex();
      listDataInfo.windowSize = oMsg.GetWindowElementSize();
      return ListRegistry::s_getInstance().updateList(listDataInfo);  //TODO : Confirm if this should be included after list data is received
   }
   return false;
}


/**
 *  CoverFlowBrowseHandler::getListDataProvider - Gets the ListDataProvider
 *  @param [in] requestedlistInfo - structure containing the requested list information
 *  @param [in] activeDeviceType - current active device type
 *  @return tSharedPtrDataProvider
 */
tSharedPtrDataProvider CoverFlowHandler::getListDataProvider(const ListDataInfo& _ListInfo)
{
   uint8 _deviceTag = MediaDatabinding::getInstance().getActiveDeviceTag();

   ETG_TRACE_USR4(("CoverFlowBrowseHandler::getListDataProvider %d,%d", _ListInfo.listId, _deviceTag));

   if (_ListInfo.listId == LIST_ID_BROWSER_COVER_FLOW)
   {
      _mediaPlayerProxy->sendCreateMediaPlayerIndexedListStart(*this, (T_e8_MPlayListType__e8LTY_SONG), 0, 0, 0, _deviceTag);
   }

   return tSharedPtrDataProvider();
}


/**
 *  CoverFlowBrowseHandler::ButtonListItemUpdMsg - on press of list item
 *  @param [in] omsg
 *  @return tSharedPtrDataProvider
 */
bool CoverFlowHandler::onCourierMessage(const ButtonListItemUpdMsg& oMsg)
{
   ETG_TRACE_USR4(("CoverFlowBrowseHandler::ButtonListItemUpdMsg %d", oMsg.GetListId()));
   unsigned int listId      = oMsg.GetListId();     // the list id for generic access
   unsigned int hdlRow      = oMsg.GetHdl();     // normaly the index
   unsigned int hdlCol		 = oMsg.GetSubHdl();

   ETG_TRACE_USR4(("CoverFlowHandler ButtonListItemUpdMsg hdlRow:%d,hdlCol : %d", hdlRow, hdlCol));
   if (listId == LIST_ID_BROWSER_COVER_FLOW)
   {
   }

   return true;
}


bool CoverFlowHandler::onCourierMessage(const UpdateCoverFlowlistMsg& /*oMsg*/)
{
   return true;
}


void CoverFlowHandler::onCreateMediaPlayerIndexedListError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerIndexedListError >& /*error*/)
{
}


void CoverFlowHandler::onCreateMediaPlayerIndexedListResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerIndexedListResult >& result)
{
   _listHandle = result->getU32ListHandle();
   uint32 listSize = result->getU32ListSize();
   ETG_TRACE_USR4(("CoverFlowHandler onCreateMediaPlayerIndexedListResult size:%d,listhandle : %d", listSize, _listHandle));
   if (listSize)
   {
      _mediaPlayerProxy->sendRequestMediaPlayerIndexedListSliceStart(*this, _listHandle, 0, 20);
   }
}


void CoverFlowHandler::onRequestMediaPlayerIndexedListSliceError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestMediaPlayerIndexedListSliceError >& /*error*/)
{
}


void CoverFlowHandler::onRequestMediaPlayerIndexedListSliceResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestMediaPlayerIndexedListSliceResult >& result)
{
   ::MPlay_fi_types::T_MPlayMediaObjects _oMediaObjects;
   ETG_TRACE_USR4(("onRequestMediaPlayerIndexedListSliceResult listSize = %d", result->getOMediaObjects().size()));
   _oMediaObjects = result->getOMediaObjects();
   updateCoverFlowList(_oMediaObjects);
}


void CoverFlowHandler::updateCoverFlowList(const ::MPlay_fi_types::T_MPlayMediaObjects& _oMediaObjects)
{
   ListDataProviderBuilder listBuilder(LIST_ID_BROWSER_COVER_FLOW, DATA_CONTEXT_COVER_FLOW_LIST_ITEM);

   ETG_TRACE_USR4(("updateCoverFlowList 1 "));
   for (uint32 listItemsIndex = 0; listItemsIndex < _oMediaObjects.size(); listItemsIndex++)
   {
      Courier::DataBindingUpdater<CoverFlowDataDataBindingSource>* ptr = listBuilder.AddItem(listItemsIndex).AddDataBindingUpdater<CoverFlowDataDataBindingSource>();

      if (ptr != NULL)
      {
         CoverFlowDataData& item = ptr->GetValue();
         std::string  imagepath = _oMediaObjects[listItemsIndex].getSCoverArt();
         ETG_TRACE_USR4(("updateCoverFlowList %s ", imagepath.c_str()));

         if (imagepath.c_str() != NULL)
         {
            ETG_TRACE_USR4(("updateCoverFlowList %s ", imagepath.c_str()));
            Candera::Bitmap* bmp = ImageLoader::loadBitmapFile(imagepath.c_str());

            if (bmp != NULL)
            {
               ETG_TRACE_USR4(("cehck"));
               item.mCoverFlowImage = ImageLoader::createImage(bmp);
            }
         }
         /*else
         {
         	Candera::Bitmap* bmp = ImageLoader::loadBitmapFile("/Global/Imports/AIVIResources/CoverArt/CoverArt");
         	if(bmp != NULL)
         	{
         		ETG_TRACE_USR4(("check"));
         		item.mAlbumArtImage = ImageLoader::createImage(bmp);
         	}
         }*/
      }
      else
      {
         ETG_TRACE_ERR(("CoverFlowHandler::updateCoverFlowList mismatch"));
      }
   }

   tSharedPtrDataProvider dataProvider = listBuilder.CreateDataProvider();
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));
}


}
}
