/**
*  @file   MediaMetaDataInfo.h
*  @author ECV - pkm8cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/
#include "hall_std_if.h"
#include "Core/MediaInfoSettings/MediaMetaDataInfo.h"
#include "AppHmi_MediaTypes.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL_MEDIA_INFOSETTINGS
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::MediaMetaDataInfo::
#include "trcGenProj/Header/MediaMetaDataInfo.cpp.trc.h"
#endif
using namespace MPlay_fi_types;

namespace App {
namespace Core {

static const char* const MEDIA_SETTINGS_LIST_BUTTON_SETTINGS_ITEM  = "Settings";
static const char* const MEDIA_SETTINGS_LIST_BUTTON_SETTINGS_ITEM_ODD  = "Settings_Odd";


MediaMetaDataInfo* MediaMetaDataInfo::_theInstance = NULL;

/**
 * @Destructor
 */
MediaMetaDataInfo::~MediaMetaDataInfo()
{
   if (_theInstance)
   {
      delete _theInstance;
      _theInstance = NULL;
   }
}


/**
 * @Constructor
 */
MediaMetaDataInfo::MediaMetaDataInfo(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy)
   : _mediaPlayerProxy(pMediaPlayerProxy)
{
   initialiseMediaMetaData();
   ETG_I_REGISTER_FILE();
}


void MediaMetaDataInfo::createInstance(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy)
{
   if (_theInstance == NULL)
   {
      _theInstance = new MediaMetaDataInfo(pMediaPlayerProxy);
   }
}


/**
 *  MediaMetaDataInfo::getInstance - function to get singleton instance
 *  @return Singleton Instance
 */
MediaMetaDataInfo* MediaMetaDataInfo::getInstance()
{
   assert(_theInstance);
   return _theInstance;
}


/**
 * initialiseVideoMetaData - function to Initialise Video Data.
 * @param[in] None
 * @parm[out] None
 * @return None.
 */
void MediaMetaDataInfo::initialiseMediaMetaData()
{
   ETG_TRACE_USR4(("MediaMetaDataInfo::initialiseVideoMetaData is called"));
   _mediaMetaData[TITLE] = "Title :";
   _mediaMetaData[TYPE] = "Type :";
   _mediaMetaData[DATE] = "Date :";
   _mediaMetaData[TIME] = "Time :";
   _mediaMetaData[SIZE] = "Size :";
   _mediaMetaData[RESOLUTION] = "Resolution :";
   _mediaMetaData[PATH] = "Path :";
}


/**getMediaMetaDataInfoListDataProvider:
 * add list items to the List scene
 * @return  list data needed for respective scene
 */
tSharedPtrDataProvider MediaMetaDataInfo::getMediaMetaDataInfoListDataProvider(const ::T_MPlayMediaObject& oMediaObject, uint8 infoSettings)
{
   ETG_TRACE_USR4(("MediaMetaDataInfo::getMediaMetaDataInfoListDataProvider is called"));
   initialiseMediaMetaData();
   //initializes the provider with the list id and the default data context
   ListDataProviderBuilder listBuilder(LIST_ID_INFORMATION_SETTINGS, MEDIA_SETTINGS_LIST_BUTTON_SETTINGS_ITEM);
   switch (infoSettings)
   {
      case VIDEO_INFORMATION:
      {
         setVideoMetaData(oMediaObject);
         uint32 itemIdx = 0;
         listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForMediaMetaDataInfo(itemIdx)).AddData(getMediaMetaDataInfoItemValue(TITLE));
         listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForMediaMetaDataInfo(itemIdx)).AddData(getMediaMetaDataInfoItemValue(TYPE));
         listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForMediaMetaDataInfo(itemIdx)).AddData(getMediaMetaDataInfoItemValue(DATE));
         listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForMediaMetaDataInfo(itemIdx)).AddData(getMediaMetaDataInfoItemValue(TIME));
         listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForMediaMetaDataInfo(itemIdx)).AddData(getMediaMetaDataInfoItemValue(SIZE));
         listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForMediaMetaDataInfo(itemIdx)).AddData(getMediaMetaDataInfoItemValue(RESOLUTION));
         break;
      }
      case PHOTO_INFORMATION:
      {
         setPhotoMetaData(oMediaObject);
         uint32 itemIdx = 0;
         listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForMediaMetaDataInfo(itemIdx)).AddData(getMediaMetaDataInfoItemValue(TITLE));
         listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForMediaMetaDataInfo(itemIdx)).AddData(getMediaMetaDataInfoItemValue(TYPE));
         listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForMediaMetaDataInfo(itemIdx)).AddData(getMediaMetaDataInfoItemValue(DATE));
         listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForMediaMetaDataInfo(itemIdx)).AddData(getMediaMetaDataInfoItemValue(PATH));
         listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForMediaMetaDataInfo(itemIdx)).AddData(getMediaMetaDataInfoItemValue(SIZE));
         listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForMediaMetaDataInfo(itemIdx)).AddData(getMediaMetaDataInfoItemValue(RESOLUTION));
         break;
      }
      default:
         break;
   }
   return listBuilder.CreateDataProvider();
}


/**
 * setVideoMetaData - function to Set Video Data.
 * @param[in] T_MPlayMediaObject
 * @parm[out] None
 * @return None.
 */
void MediaMetaDataInfo::setVideoMetaData(const ::T_MPlayMediaObject& oMediaObject)
{
   ETG_TRACE_USR4(("MediaMetaDataInfo::setVideoMetaData is called"));
   setMediaFileFormat(oMediaObject.getE8FileFormat());
   _mediaMetaData[TITLE] = _mediaMetaData[TITLE] + oMediaObject.getSFilename();
}


/**
 * setPhotoMetaData - function to Set Photo Data.
 * @param[in] T_MPlayMediaObject
 * @parm[out] None
 * @return None.
 */
void MediaMetaDataInfo::setPhotoMetaData(const ::T_MPlayMediaObject& oMediaObject)
{
   ETG_TRACE_USR4(("MediaMetaDataInfo::setPhotoMetaData is called"));
   setMediaFileFormat(oMediaObject.getE8FileFormat());
   _mediaMetaData[TITLE] = _mediaMetaData[TITLE] + oMediaObject.getSFilename();
}


/**
 * setVideoFileFormat - function to Set Video File Format.
 * @param[in] T_e8_MPlayFileFormat
 * @parm[out] None
 * @return None.
 */
void MediaMetaDataInfo::setMediaFileFormat(const ::T_e8_MPlayFileFormat _fileFormat)
{
   ETG_TRACE_USR4(("MediaMetaDataInfo::setVideoFileFormat is called"));
   std::string fileFormat = "";
   switch (_fileFormat)
   {
      case T_e8_MPlayFileFormat__e8FFT_MP4:
         fileFormat = "mp4";
         break;
      case T_e8_MPlayFileFormat__e8FFT_3GP:
         fileFormat = "3gp";
         break;
      case T_e8_MPlayFileFormat__e8FFT_3G2:
         fileFormat = "3g2";
         break;
      case T_e8_MPlayFileFormat__e8FFT_MPEG:
         fileFormat = "mpeg";
         break;
      case T_e8_MPlayFileFormat__e8FFT_AVI:
         fileFormat = "avi";
         break;
      case T_e8_MPlayFileFormat__e8FFT_MOV:
         fileFormat = "mov";
         break;
      case T_e8_MPlayFileFormat__e8FFT_DIVX:
         fileFormat = "divx";
         break;
      case T_e8_MPlayFileFormat__e8FFT_WMV:
         fileFormat = "wmv";
         break;
      case T_e8_MPlayFileFormat__e8FFT_M4V:
         fileFormat = "m4v";
         break;
      case T_e8_MPlayFileFormat__e8FFT_FLV:
         fileFormat = "flv";
         break;
      case T_e8_MPlayFileFormat__e8FFT_M3U:
         fileFormat = "m3u";
         break;
      case T_e8_MPlayFileFormat__e8FFT_PLS:
         fileFormat = "pls";
         break;
      case T_e8_MPlayFileFormat__e8FFT_XSPF:
         fileFormat = "xspf";
         break;
      case T_e8_MPlayFileFormat__e8FFT_WPL:
         fileFormat = "wpl";
         break;
      case T_e8_MPlayFileFormat__e8FFT_B4S:
         fileFormat = "b4s";
         break;
      case T_e8_MPlayFileFormat__e8FFT_ASX:
         fileFormat = "asx";
         break;
      case T_e8_MPlayFileFormat__e8FFT_JPEG:
         fileFormat = "jpeg";
         break;
      case T_e8_MPlayFileFormat__e8FFT_GIF:
         fileFormat = "gif";
         break;
      case T_e8_MPlayFileFormat__e8FFT_BMP:
         fileFormat = "bmp";
         break;
      case T_e8_MPlayFileFormat__e8FFT_PNG:
         fileFormat = "png";
         break;
      case T_e8_MPlayFileFormat__e8FFT_TIF:
         fileFormat = "tif";
         break;
      default:
         ETG_TRACE_USR4(("Non-Supported File Format"));
         break;
   }
   _mediaMetaData[TYPE] = _mediaMetaData[TYPE] + fileFormat;
}


/**
 * getlistItemTemplateForMediaInfo - function to get list template.
 * @param[in] uint32 index
 * @parm[out] const char
 * @return const char - return listItemTemplate
 */
const char* MediaMetaDataInfo::getlistItemTemplateForMediaMetaDataInfo(uint32& index)
{
   const char* listItemTemplate;

   if (index % 2)
   {
      listItemTemplate = MEDIA_SETTINGS_LIST_BUTTON_SETTINGS_ITEM_ODD;
   }
   else
   {
      listItemTemplate = MEDIA_SETTINGS_LIST_BUTTON_SETTINGS_ITEM;
   }
   ++index;
   return listItemTemplate;
}


/**getMediaMetaDataInfoItemValue-
 * get the Information about Video
 * @return  string-name for item
 */
Candera::String MediaMetaDataInfo::getMediaMetaDataInfoItemValue(uint8 ItemIndex)
{
   std::string itemValue = "";
   switch (ItemIndex)
   {
      case TITLE:
      {
         itemValue = _mediaMetaData[TITLE];
         break;
      }
      case TYPE:
      {
         itemValue = _mediaMetaData[TYPE];
         break;
      }
      case DATE:
      {
         itemValue = _mediaMetaData[DATE];
         break;
      }
      case TIME:
      {
         itemValue = _mediaMetaData[TIME];
         break;
      }
      case SIZE:
      {
         itemValue = _mediaMetaData[SIZE];
         break;
      }
      case RESOLUTION:
      {
         itemValue = _mediaMetaData[RESOLUTION];
         break;
      }
      case PATH:
      {
         itemValue = _mediaMetaData[PATH];
         break;
      }
      default:
      {
         itemValue = "";
      }
      break;
   }
   ETG_TRACE_USR4(("MediaMetaDataInfo MetaData: %s", itemValue.c_str()));
   return itemValue.c_str();
}


}// namespace AppLogic
} // namespace App
