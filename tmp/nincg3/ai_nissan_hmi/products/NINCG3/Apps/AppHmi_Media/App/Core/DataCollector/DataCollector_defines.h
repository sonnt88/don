/**
 * @file DataCollector.h
 * @Author ECV
 * @Copyright (c) 2015 Robert Bosch Car Multimedia Gmbh
 * @addtogroup
 */

#ifndef DATACOLLECTOR_DEFINES_H_
#define DATACOLLECTOR_DEFINES_H_
namespace App
{
namespace Core
{
#define UNDEFINED 0xFF
enum enHeaderIcon
{
   NAVI_DEST          = 0,
   NAVI_DEST_01       = 1,
   NAVI_INFO          = 2,
   NAVI_SETTINGS      = 3,
   NAVI_TRAFFIC       = 4,
   SRC_AUX            = 5,
   SRC_CD             = 6,
   SRC_IPOD           = 7,
   SRC_PHONE          = 8,
   SRC_RADIO          = 9,
   SRC_USB            = 10,
   SRC_XM             = 11,
   VR                 = 12,
   SMS                = 13,
   SXM                = 14,
   CONNECTED_SERVICES = 15,
   SIRI               = 16,
   REPLAY             = 17,
   UNDEF          = 0xFF

};
}
}

#endif /* DATACOLLECTOR_DEFINES_H_ */
