/****************************************************************************
 * Copyright (C) Robert Bosch Car Multimedia GmbH, 2013
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ***************************************************************************/
#include "gui_std_if.h"

#include "MediaGui.h"
#include "HMI/CGIComponents/AppViewSettings.h"
#include "HMI/CGIComponents/CGIApp.h"
#include "AppUtils/Trace/GuiInfo.h"
#include "AppBase/IApplicationSettings.h"
#include "Common/AppBaseSettings/KeyMapping.h"
#include "Common/UserInterfaceCtrl/UserInterfaceScreenSaver.h"
#include "Common/AssetShaperUtils/AssetShaperUtil.h"

//////// TRACE IF ///////////////////////////////////////////////////
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_MAIN
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::MediaGui::
#include "trcGenProj/Header/MediaGui.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN

DEFAULT_APPSETTINGS(SURFACEID_MAIN_SURFACE_MEDIA, SURFACEID_NONE, SURFACEID_TOP_POPUP_SURFACE_MEDIA, SURFACEID_CENTER_POPUP_SURFACE_MEDIA)

using namespace ::hmi;

namespace App {
namespace Core {


MediaGui::MediaGui() : GuiComponentBase(hmi::apps::appHmi_Media, appSettings)
{
   ETG_I_REGISTER_FILE();
   ETG_I_REGISTER_CHN(TraceCmd_NotProcessedMsg);
   _hmiAppCtrlProxyHandler.setTraceId(TR_CLASS_APPHMI_MEDIA_APPCTRL_PROXY);
   SYNC_BLOCK_CONNECTION(765, MEDIA_GADGET_CURRENT_SONG_WIDGET_L);
   SYNC_BLOCK_CONNECTION(765, MEDIA_GADGET_CURRENT_SONG_WIDGET_S);

//   _mUserInterfaceCntrl = new UserInterfaceControl();
//   if (_mUserInterfaceCntrl != NULL)
//   {
//      _mUserInterfaceCntrl->setApplicationId(APPID_APPHMI_MEDIA);
//   }
//    _mUserInterfaceCntrl->registerLockApplicationUpdate(this);
   appSettings.setUserInputObserver(UserInterfaceScreenSaver::getInstance());
}


MediaGui::~MediaGui()
{
   hmibase::utils::trace::GuiInfo::setViewHandler(NULL);

//   if (_mUserInterfaceCntrl != NULL)
//   {
//      _mUserInterfaceCntrl->deregisterLockApplicationUpdate(this);
//      delete _mUserInterfaceCntrl;
//      _mUserInterfaceCntrl = NULL;
//   }
}


unsigned int MediaGui::getDefaultTraceClass()
{
   return TR_CLASS_APPHMI_MEDIA_MAIN;
}


void MediaGui::setupCgiInstance()
{
   std::string assetFileName = Rnaivi::AssetShaperUtil::getDefaultAssetFileName();
#if defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2)|| defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R)
   std::vector<std::string> assetFileNames = Rnaivi::AssetShaperUtil::getAssetFiles(assetFileName);
   setCgiApp(new CGIApp(assetFileNames, _hmiAppCtrlProxyHandler));
#else
   setCgiApp(new CGIApp(assetFileName.c_str(), _hmiAppCtrlProxyHandler));
#endif
}


void MediaGui::preRun()
{
//	DP_vCreateDatapool();
   PersistentValuesRead();
}


void MediaGui::postRun()
{
   PersistentValuesWrite();
}


void MediaGui::PersistentValuesRead()
{
//   dp_tclhmiAppMediaPersMemVarsMedia dp;
//   _mvar = dp.tGetData();
}


void MediaGui::PersistentValuesWrite()
{
//   dp_tclhmiAppMediaPersMemVarsMedia dp;
//   dp.s32SetData(_mvar);
}


/**
 *sink for not processed ttfis input messages
 */
void MediaGui::TraceCmd_NotProcessedMsg(const unsigned char* data)
{
   if (data)
   {
      //ETG_TRACE_FATAL(("TraceCmd_NotProcessedMsg() : 0x%*x", ETG_LIST_LEN(data[0]), ETG_LIST_PTR_T8(data)));
   }
}


} // namespace Core
} // namespace App
