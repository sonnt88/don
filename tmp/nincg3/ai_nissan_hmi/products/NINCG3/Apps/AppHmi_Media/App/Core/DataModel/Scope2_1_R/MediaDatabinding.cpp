/**
 *  @file   MediaScope2Databinding.cpp
 *  @author ECV - IVI-MediaTeam
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */
#include "hall_std_if.h"
#include "MediaDatabinding.h"
#include "Core/LanguageDefines.h"
#include "ProjectBaseTypes.h"
#ifdef PHOTOPLAYER_SUPPORT
#include <CgiExtensions/ImageLoader.h>
#endif

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::MediaDatabinding::
#include "trcGenProj/Header/MediaDatabinding.cpp.trc.h"
#endif


namespace App {
namespace Core {
MediaDatabinding* MediaDatabinding::_theInstance = 0;
MediaDatabinding::MediaDatabinding()
   : _isPreviousTrackSupportAvailForBT(false)
   , _isNextTrackSupportAvailForBT(false)
{
   _modeValue = APP_MODE_UNDEFINED;
}


MediaDatabinding::~MediaDatabinding()
{
   ETG_TRACE_USR4(("clMedia_Databindinghandler Destructor"));
   removeInstance();
}


/**
 *  MediaDatabindingCommon::getInstance - function to get singleton instance
 *  @return Singleton Instance
 */
MediaDatabinding& MediaDatabinding::getInstance()
{
   if (_theInstance == 0)
   {
      _theInstance = new MediaDatabinding();
   }

   assert(_theInstance);
   return *_theInstance;
}


/**
 *  MediaDatabinding::removeInstance - function to delete the singleton instance
 *  @return Singleton Instance
 */
void MediaDatabinding::removeInstance()
{
   if (_theInstance)
   {
      delete _theInstance;
      _theInstance = 0;
   }
}


/**
 * updateBTMenuSupportStatus - Function to update Metadata status in BT Main screen to GUI
 * @param[in] bool
 * @parm[out] None
 * @return void
 */

void MediaDatabinding::updateBTMenuSupportStatus(bool isMenuAvailable)
{
   ETG_TRACE_USR4(("updateBTButtonEnableState"));
   (*_btMetaDataVisibility).misMenuAvailable = isMenuAvailable;
   if (isMenuAvailable)
   {
      (*_btMetaDataVisibility).mButtonBrowseTextUpdate = LANGUAGE_STRING(BT_AUDIO_BTMENU , "BTA Menu");
   }
   else
   {
      (*_btMetaDataVisibility).mButtonBrowseTextUpdate = LANGUAGE_STRING(BT_AUDIO_LOWER_AVRCP_MAIN_BUTTON_CONNECT, "Connect");
   }
   ETG_TRACE_USR4(("item modified :%s" , (*_btMetaDataVisibility).mButtonBrowseTextUpdate.GetCString()));
   _btMetaDataVisibility.MarkAllItemsModified();
   if (_btMetaDataVisibility.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("UpdateBTIconState : Updation failed..!!!"));
   }
}


/**
 * updateAuxLevel - Function to update Aux level status to GUI
 * @param[in] AuxLevel
 * @parm[out] None
 * @return void
 */
void MediaDatabinding::updateAuxLevel(int16 AuxLevel)
{
   ETG_TRACE_USR4(("updateVolumeIconStatus"));
   (*_volumeButtonStatus).mIsButtonLowActive = ((AuxLevel - 1) == 0) ? true : false;
   (*_volumeButtonStatus).mIsButtonMidActive = ((AuxLevel - 1) == 1) ? true : false;
   (*_volumeButtonStatus).mIsButtonHighActive = ((AuxLevel - 1) == 2) ? true : false;
   _volumeButtonStatus.MarkAllItemsModified();
   if (_volumeButtonStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateVolumeIconStatus : Updation failed..!!!"));
   }
}


/**
 * updateBTRepeatRandomStatus - Function to control Enable/Disable Functionality of the buttons in BT Main Screen..
 * @param[in] bool,bool
 * @parm[out] None
 * @return void
 */

void MediaDatabinding::updateBTRepeatRandomStatus(Courier::Bool isRepeatSupportAvailable, Courier::Bool isRandomSupportAvailable)
{
   ETG_TRACE_USR4(("updateBTRepeatRandomStatus"));
   (*_btButtonSupportStatus).misRepeatSupportAvailable = isRepeatSupportAvailable;
   (*_btButtonSupportStatus).misRandomSupportAvailable = isRandomSupportAvailable;

   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isRepeatSupportAvailableItem);
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isRandomSupportAvailableItem);

   if (_btButtonSupportStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateBTRepeatRandomStatus : Updation failed..!!!"));
   }
}


/**
 * updateBTFFNextTrackSupportStatus - Function to control Enable/Disable Functionality of the buttons in BT Main Screen..
 * @param[in] bool,bool
 * @parm[out] None
 * @return void
 */

void MediaDatabinding::updateBTFFNextTrackSupportStatus(Courier::Bool isFastForwardSupportAvailable, Courier::Bool isNextTrackSupportAvailable)
{
   ETG_TRACE_USR4(("updateBTRepeatRandomStatus"));
   (*_btButtonSupportStatus).misFastForwardSupportAvailable = isFastForwardSupportAvailable;
   (*_btButtonSupportStatus).misNextTrackSupportAvailable = isNextTrackSupportAvailable;

   if (isFastForwardSupportAvailable == true || isNextTrackSupportAvailable == true)
   {
      _isNextTrackSupportAvailForBT = true;
   }
   else
   {
      _isNextTrackSupportAvailForBT = false;
   }
   (*_btButtonSupportStatus).misNextTrackSupportAvailable = _isNextTrackSupportAvailForBT;

   //_btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isFastForwardSupportAvailableItem);
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isNextTrackSupportAvailableItem);

   if (_btButtonSupportStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateBTFFNextTrackSupportStatus : Updation failed..!!!"));
   }
}


void MediaDatabinding::updateTBTInfo(const bool& isRGActive, const TBTManeuverInfoData& TBTManeuverInfo)
{
   (*_mTBTManeuverInfo).mManeuverSymbolIndex = TBTManeuverInfo.mManeuverSymbolIndex;
   (*_mTBTManeuverInfo).mDistanceToManeuver = TBTManeuverInfo.mDistanceToManeuver;
   (*_mTBTManeuverInfo).mShowManeuverInfo = TBTManeuverInfo.mShowManeuverInfo;
   (*_mTBTManeuverInfo).mRoundAboutExitNumber = TBTManeuverInfo.mRoundAboutExitNumber;
   (*_mTBTManeuverInfo).mSwitchAlbumArtOrTBT = isRGActive;
   _mTBTManeuverInfo.MarkAllItemsModified();
   if (_mTBTManeuverInfo.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateTBTInfo : Updation failed..!!!")) ;
   }
}


/**
 * updateBTFRPrevTrackSupportStatus - Function to control Enable/Disable Functionality of the buttons in BT Main Screen..
 * @param[in] bool,bool
 * @parm[out] None
 * @return void
 */

void MediaDatabinding::updateBTFRPrevTrackSupportStatus(Courier::Bool isFastRewindSupportAvailable, Courier::Bool isPreviousTrackSupportAvailable)
{
   ETG_TRACE_USR4(("updateBTFRPrevTrackSupportStatus"));
   (*_btButtonSupportStatus).misFastRewindSupportAvailable = isFastRewindSupportAvailable;
   (*_btButtonSupportStatus).misPreviousTrackSupportAvailable = isPreviousTrackSupportAvailable;

   if (isFastRewindSupportAvailable == true || isPreviousTrackSupportAvailable == true)
   {
      _isPreviousTrackSupportAvailForBT = true;
   }
   else
   {
      _isPreviousTrackSupportAvailForBT = false;
   }
   (*_btButtonSupportStatus).misPreviousTrackSupportAvailable = _isPreviousTrackSupportAvailForBT;
   //_btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isFastRewindSupportAvailableItem);
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isPreviousTrackSupportAvailableItem);

   if (_btButtonSupportStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateBTFRPrevTrackSupportStatus : Updation failed..!!!"));
   }
}


void MediaDatabinding::updateBTPlayTimeSupportStatus(Courier::Bool isPlayTimeSupportAvailable)
{
   ETG_TRACE_USR4(("updateBTPlayTimeSupportStatus %d", isPlayTimeSupportAvailable));
   (*_btButtonSupportStatus).misPlayTimeSupportAvailable = isPlayTimeSupportAvailable;
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isPlayTimeSupportAvailableItem);
   if (_btButtonSupportStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateBTPlayTimeSupportStatus : Updation failed..!!!"));
   }
}


void MediaDatabinding::updateFolderOrCurrentPlayingListBtnText(Candera::String buttonText)
{
   ETG_TRACE_USR4(("updateFolderOrCurrentPlayingListBtnText"));
   (*_browseScreenButton).mFolderorCurrentPlayingListBtnText = buttonText.GetCString();
   _browseScreenButton.MarkItemModified(ItemKey::BrowseScreenButtons::FolderorCurrentPlayingListBtnTextItem);

   if (_browseScreenButton.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateFolderOrCurrentPlayingListBtnText : Updation failed..!!!"));
   }
}


/**
 * updateBTBrowseSupportStatus - Function to control Enable/Disable Functionality of the buttons in BT Main Screen.
 * @param[in] bool
 * @return void
 */

void MediaDatabinding::updateBTFolderBrowseSupportStatus(Courier::Bool isFolderBrowseSupportAvailable)
{
   ETG_TRACE_USR4(("updateBTFolderBrowseSupportStatus %d", isFolderBrowseSupportAvailable));
   (*_btButtonSupportStatus).misFolderBrowseSupportAvailable = isFolderBrowseSupportAvailable;

   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isFolderBrowseSupportAvailableItem);

   if (_btButtonSupportStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateBTFolderBrowseSupportStatus : Updation failed..!!!"));
   }
}


/**
 * updateAlbumArtTogglestatus - Function to update album art toggling status in  Main Screen..
 * @param[in] bool
 * @parm[out] None
 * @return void
 */
void MediaDatabinding::updateAlbumArtTogglestatus(bool toggleStatus)
{
   (*_browseScreenButton).mIsAlbumArtEnable = toggleStatus;

   _browseScreenButton.MarkItemModified(ItemKey::BrowseScreenButtons::IsAlbumArtEnableItem);

   if (_browseScreenButton.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateTogglestateInfo : Updation failed..!!!"));
   }
}


void MediaDatabinding::updateDefaultAlbumArtVisibleStatus(bool toggleStatus)
{
   (*_albumArtData).mDefaultAlbumArtVisibility = toggleStatus;
   _albumArtData.MarkItemModified(ItemKey::AlbumArtData::DefaultAlbumArtVisibilityItem);
   if (_albumArtData.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateTogglestateInfo : Updation failed..!!!"));
   }
}


void MediaDatabinding::updateAlbumArtVisibleStatus(bool bitmapStatus)
{
   (*_albumArtData).mAlbumArtVisibility = bitmapStatus;
   _albumArtData.MarkItemModified(ItemKey::AlbumArtData::AlbumArtVisibilityItem);
   if (_albumArtData.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateTogglestateInfo : Updation failed..!!!"));
   }
}


void MediaDatabinding::updateCurrentPlayingListBtnStatus(bool buttonStatus)
{
   (*_browseScreenButton).mCurrentPlayingListBtnEnableStatus = buttonStatus;
   _browseScreenButton.MarkItemModified(ItemKey::BrowseScreenButtons::CurrentPlayingListBtnEnableStatusItem);

   if (_browseScreenButton.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateTogglestateInfo : Updation failed..!!!"));
   }
}


/**
 * UpdateMediaMetadataInfo - Function to update meta data to GUI
 * @param[in] ArtistName, AlbumName, TitleName
 * @parm[out] None
 * @return void
 */
void MediaDatabinding::updateMediaMetadataInfo(std::string strArtist, std::string strAlbum, std::string strTitle)
{
   MediaDatabindingCommon::updateMediaMetadataInfo(strArtist, strAlbum, strTitle);
   if ((_audioSource > SRC_MEDIA_AUX && _audioSource <= SRC_PHONE_BTAUDIO) && (_modeValue == APP_MODE_UTILITY))
   {
      ETG_TRACE_USR4(("audio source = %d mode value = %d", _audioSource, _modeValue));
      POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, MediaPopups_Popups_MEDIA__IMPOSE_AUDIO_OPERATION)));
   }
}


/**
 * updateTrackNumber  - called to update the track no details
 * @param[in] int,int
 * @return void
 */
void MediaDatabinding::updateTrackNumber(uint32 currentTrackNo, uint32 totalTrackNo)
{
   char currentTrack[ARRAY_SIZE] = "\0";
   char totalTrack[ARRAY_SIZE] = "\0";
   currentTrackNo = currentTrackNo + 1;// 1 is added since mediaplayer count starts from 0
   snprintf(currentTrack, sizeof currentTrack, "%d", currentTrackNo);
   snprintf(totalTrack, sizeof totalTrack, "%d", totalTrackNo);
   (*_trackNumberDetails).mCurrentTrackNo = currentTrack;
   (*_trackNumberDetails).mTotalTrackNo = totalTrack;
   _trackNumberDetails.MarkAllItemsModified();
   if (_trackNumberDetails.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_trackNumberDetails : Updation failed..!!!"));
   }
}


/**
 * updateVolumeAvailability - This is a property update function and it will be called by sound whenever there is a change in Volume
 *
 * @param[in] volumelevel
 * @return void
 */
void MediaDatabinding::updateVolumeAvailability(int16 volumeLevel)
{
   ETG_TRACE_USR4(("updateVolumeAvailability %d", volumeLevel));
   _isVolumeAvailable = (volumeLevel == 0) ? false : true;
   (*_volumeInfo).mIsVolumeAvailable = _isVolumeAvailable;
   _volumeInfo.MarkAllItemsModified();
   if (_volumeInfo.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("onVolumeUpdate : Updation failed..!!!"));
   }
   if ((_isPreviousTrackSupportAvailForBT == true) && ((*_volumeInfo).mIsVolumeAvailable == true))
   {
      (*_btButtonSupportStatus).misPreviousTrackSupportAvailable = true;
   }
   else
   {
      (*_btButtonSupportStatus).misPreviousTrackSupportAvailable = false;
   }
   if ((_isNextTrackSupportAvailForBT == true) && ((*_volumeInfo).mIsVolumeAvailable == true))
   {
      (*_btButtonSupportStatus).misNextTrackSupportAvailable = true;
   }
   else
   {
      (*_btButtonSupportStatus).misNextTrackSupportAvailable = false;
   }
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isNextTrackSupportAvailableItem);
   _btButtonSupportStatus.MarkItemModified(ItemKey::BTButtonFunctionalityStatus::isPreviousTrackSupportAvailableItem);
   if (_btButtonSupportStatus.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("onVolumeUpdate : Updation failed..!!!"));
   }
}


void MediaDatabinding::updateFooterInFolderBrowse(std::string FooterText)
{
   (*_trackNumberDetails).mCurrentFocussedFolderNo = FooterText.c_str();
   _trackNumberDetails.MarkItemModified(ItemKey::TrackNumberDetails::CurrentFocussedFolderNoItem);
   if (_trackNumberDetails.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("_trackNumberDetails : Updation failed..!!!"));
   }
}


void MediaDatabinding::SetApplicationModeValue(uint8 ApplicationModeValue)
{
   _modeValue = ApplicationModeValue;
}


#ifdef PHOTOPLAYER_SUPPORT
/**
 * updatePhotoPlayerInfoPhotoName
 * @param[in] :strPhotoName
 * @parm[out] :None
 * @return bool
 */
bool MediaDatabinding::updatePhotoPlayerInfoPhotoName(std::string strPhotoName)
{
   ETG_TRACE_USR4(("updatePhotoPlayerInfoPhotoName %s", strPhotoName.c_str()));
   (*_photoPlayerInfo).mPhotoName = strPhotoName.c_str();

   _photoPlayerInfo.MarkItemModified(ItemKey::PhotoPlayerInfo::PhotoNameItem);
   if (_photoPlayerInfo.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updatePhotoPlayerInfoPhotoName : Updating failed..!!!"));
      return false ;
   }
   return true ;
}


/**
 * updatePhotoPlayerInfoPhotoContent
 * @param[in] :u8ImageData, u32ImageDataSize, isPhotoContentVisible
 * @parm[out] :None
 * @return bool
 */
bool MediaDatabinding::updatePhotoPlayerInfoPhotoContent(const ::std::vector<uint8> u8PhotoContent, uint32 u32PhotoContentSize, bool isPhotoContentVisible)
{
   bool ret = false;
   ETG_TRACE_USR4(("updatePhotoPlayerInfoPhotoContent %d %d", u32PhotoContentSize, isPhotoContentVisible));
   Candera::Bitmap* bmp = ImageLoader::loadBitmapData(u8PhotoContent.data(), u32PhotoContentSize);
   if (bmp != NULL)
   {
      (*_photoPlayerInfo).mPhotoVisible = isPhotoContentVisible;
      (*_photoPlayerInfo).mPhotoContent = ImageLoader::createImage(bmp);

      _photoPlayerInfo.MarkItemModified(ItemKey::PhotoPlayerInfo::PhotoVisibleItem);
      _photoPlayerInfo.MarkItemModified(ItemKey::PhotoPlayerInfo::PhotoContentItem);
      if (_photoPlayerInfo.SendUpdate(true) == false)
      {
         ETG_TRACE_USR4(("updatePhotoPlayerInfoPhotoContent : Updating failed..!!!"));
         ret = false ;
      }
      else
      {
         ret = true ;
      }
   }
   return ret;
}


/**
 * updatePhotoPlayerInfoPhotoContent
 * @param[in] :strPhotoFilePath, isPhotoContentVisible
 * @parm[out] :None
 * @return bool
 */
bool MediaDatabinding::updatePhotoPlayerInfoPhotoContent(std::string strPhotoFilePath, bool isPhotoContentVisible)
{
   bool ret = false;
   ETG_TRACE_USR4(("updatePhotoPlayerInfoPhotoContent - Photo Content Visible %d, Photo File Path %s", isPhotoContentVisible, strPhotoFilePath.c_str()));
   Candera::Bitmap* bmp = ImageLoader::loadBitmapFile(strPhotoFilePath.c_str());
   if (bmp != NULL)
   {
      (*_photoPlayerInfo).mPhotoVisible = isPhotoContentVisible;
      (*_photoPlayerInfo).mPhotoContent = ImageLoader::createImage(bmp);

      _photoPlayerInfo.MarkItemModified(ItemKey::PhotoPlayerInfo::PhotoVisibleItem);
      _photoPlayerInfo.MarkItemModified(ItemKey::PhotoPlayerInfo::PhotoContentItem);
      if (_photoPlayerInfo.SendUpdate(true) == false)
      {
         ETG_TRACE_USR4(("updatePhotoPlayerInfoPhotoContent : Updating failed..!!!"));
         ret = false ;
      }
      else
      {
         ret = true ;
      }
   }
   return ret;
}


/**
 * updatePhotoPlayerInfoPlayPauseActiveStatus
 * @param[in] :isPlayPhotoActive, isPausePhotoActive
 * @parm[out] :None
 * @return bool
 */
bool MediaDatabinding::updatePhotoPlayerInfoPlayPauseActiveStatus(bool isPlayPhotoActive, bool isPausePhotoActive)
{
   ETG_TRACE_USR4(("updatePhotoPlayerInfoPlayPauseActiveStatus %d %d", isPlayPhotoActive, isPausePhotoActive));
   (*_photoPlayerInfo).misPlayPhotoActive = isPlayPhotoActive;
   (*_photoPlayerInfo).misPausePhotoActive = isPausePhotoActive;

   _photoPlayerInfo.MarkItemModified(ItemKey::PhotoPlayerInfo::isPlayPhotoActiveItem);
   _photoPlayerInfo.MarkItemModified(ItemKey::PhotoPlayerInfo::isPausePhotoActiveItem);
   if (_photoPlayerInfo.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updatePhotoPlayerInfoPlayPauseActiveStatus : Updating failed..!!!"));
      return false ;
   }
   return true ;
}


#endif


/**
 * updateThumbnailAlbumArt - This function is to control the visibility of ThumbnailAlbumart
 *
 * @param[in] bool : defaultThumbnail,isThumbnailAvailable
 * @return void
 */
void MediaDatabinding::updateThumbnailAlbumArt(Courier::Bool defaultThumbnail, Courier::Bool isThumbnailAvailable)
{
   ETG_TRACE_USR4(("updateThumbnailAlbumArt : called  default:%d Thumbnail:%d", defaultThumbnail, isThumbnailAvailable));
   (*_mThumbNail).misDefaultThumbnailAvailable = defaultThumbnail;
   (*_mThumbNail).misThumbNailAlbumArtAvailable = isThumbnailAvailable;
   _mThumbNail.MarkItemModified(ItemKey::ThumbNailAlbumArt::isDefaultThumbnailAvailableItem);
   _mThumbNail.MarkItemModified(ItemKey::ThumbNailAlbumArt::isThumbNailAlbumArtAvailableItem);
   if (_mThumbNail.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateThumbnailAlbumArt : Updation failed..!!!"));
   }
}


/**
 * updateVideoPlayerViewModeSettings - Function to Update Video Player View Mode Settings
 * @param[in] bool
 * @return void
 */

void MediaDatabinding::updateVideoPlayerViewModeSettings(Courier::Bool viewMode)
{
   ETG_TRACE_USR4(("updateVideoPlayerViewModeSettings %d", viewMode));
   (*_mVideoPlayerSettings).mViewMode = viewMode;

   _mVideoPlayerSettings.MarkItemModified(ItemKey::VideoPlayerSettings::ViewModeItem);

   if (_mVideoPlayerSettings.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateVideoPlayerViewModeSettings : Updation failed..!!!"));
   }
}


#ifdef POPOUTLIST_SUPPORT

void MediaDatabinding::updateOptionButtonVisiblity(uint32 visibilityStatus)
{
   ETG_TRACE_USR4(("updateOptionButtonVisiblity %d", visibilityStatus));

   (*_zone3Parameters).mOptionButtonVisiblity = visibilityStatus;

   _zone3Parameters.MarkItemModified(ItemKey::Zone3Parameters::OptionButtonVisiblityItem);

   if (_zone3Parameters.SendUpdate(true) == false)
   {
      ETG_TRACE_USR4(("updateOptionButtonVisiblity : Updation failed..!!!"));
   }
}


#endif
}


}
