/**
*  @file   MediaUtils.cpp
*  @author ECV - bvi1cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/
#include "MediaUtils.h"
#include <stdio.h>

namespace App {
namespace Core {

#define MEDIAPLAYER_UTILS__PLAY_TIME_STRING_SIZE 40

/**
* vConvertSecToHourMinSec - Helper function to converts the seconds into hours:minutes:seconds
*
* @param[in,out] uiHour, uiMinute, uiSecond
* @parm[in] void
* @return void
*/
void MediaUtils::vConvertSecToHourMinSec(unsigned int& uiHour, unsigned int& uiMinute, unsigned int& uiSecond)
{
   const unsigned int u32MinInHour = 60U;
   const unsigned int u32SecsInMin = 60U;
   uiHour = 0U;
   uiMinute = 0U;
   // convert the Second into hour, minute and seconds
   uiHour = uiSecond / (u32MinInHour * u32SecsInMin);
   uiSecond -= uiHour * u32MinInHour * u32SecsInMin;
   uiMinute = uiSecond / u32SecsInMin;
   uiSecond -= uiMinute * u32SecsInMin;
}


/**
* vFormatTime - Helper function to get  time string in required format
*
* @param[in,out] TimeString - assign time info in string format
* @parm[in] u32TimeInHour, u32TimeInMinutes, u32TimeInSec time info in hour minute and second
* @return void
*/
void MediaUtils::vFormatTime(char* TimeString, uint32 u32TimeInHour, uint32 u32TimeInMinutes, uint32 u32TimeInSec)
{
   if (TimeString == NULL)
   {
      return ;
   }
   if (u32TimeInMinutes  < 60 && u32TimeInSec < 60)
   {
      if (u32TimeInHour > 0) //Hourse in double digit
      {
         snprintf(TimeString, sizeof(char)*MEDIAPLAYER_UTILS__PLAY_TIME_STRING_SIZE, "%u:%02u:%02u", u32TimeInHour, u32TimeInMinutes, u32TimeInSec);
      }
      else //Hours in single digit
      {
         int MinDigits = (u32TimeInMinutes > 9) ? 2 : 1;
         snprintf(TimeString, sizeof(char)*MEDIAPLAYER_UTILS__PLAY_TIME_STRING_SIZE, "%0*u:%02u", MinDigits, u32TimeInMinutes, u32TimeInSec);
      }
   }
   else
   {
      TimeString[0] = 0;//Invalid Min and Sec values
   }
}


/**
* bCheckStringLengthNCopy - Helper function to Trim Meta Data Info to size u32Length
*
* @parm[in, out] strFullMetaData - FullMetaData information extracted from the respective field. Size may or may not be greater than u32Length.
* @parm[in, out] strTrim - FullMetaData information truncated to length u32Length
* @parm[in] u32Length - The maximum length of strFullMetaData that needs to be copied to strTrim
* @return true if strFullMetaData was trimmed and copied to strTrim
* @return false if there was no need to trim strFullMetaData before copying to strTrim
*/
bool MediaUtils::bCheckStringLengthNCopy(const std::string& strFullMetaData, std::string& strTrim, const uint32 u32Length)
{
   strTrim = strFullMetaData;
   if (strTrim.length() > u32Length)
   {
      strTrim.erase(strTrim.begin() + u32Length , strTrim.end());
   }
   return (strTrim.length() != strFullMetaData.length());
}


/**
* bExtractFileName - Helper function to extract FileName from a given FilePath
*
* @parm[in] strPath - Full file path of media item
* @parm[in, out] strName - FileName extracted from the given path
* @return true if extraction of FileName from FilePath is successful
* @return false if extraction of FileName from FilePath is failed
*/
bool MediaUtils::bExtractFileName(const std::string& strPath, std::string& strName)
{
   std::string folderPath = strPath;
   std::size_t pos = folderPath.find_last_of('/');
   strName.clear();
   if (pos != std::string::npos)
   {
      if (pos == folderPath.length() - 1)
      {
         folderPath.erase(strPath.length() - 1);
         if ((pos = folderPath.find_last_of('/')) == std::string::npos)
         {
            strName = folderPath;
         }
         else
         {
            strName = folderPath.substr(pos + 1);
         }
      }
      else
      {
         strName = folderPath.substr(pos + 1);
      }
   }
   return (strName.length() != 0U);
}


/**
* AppendSelectedFolderInCurrFolderPath - Helper function to append the selected folder name to the current folder path.
*
* @parm[in, out] strPath - Current folder path of media item
* @parm[in] strFolderName - Selected folder name .
* @return void
*/
void MediaUtils::AppendSelectedFolderInCurrFolderPath(std::string& strPath, const std::string& strFolderName)
{
   strPath = strPath + strFolderName + "/";
}


/**
* goOneFolderUp - Helper function that is used to go one folder up from the current folder .
*
* @parm[in] strFolderPath - Full folder path of media item
* @parm[in,out] strnewFolderPath - new folder path  .
* @return void
*/
void MediaUtils::goOneFolderUp(std::string& strFolderPath, std::string& strnewFolderPath)
{
   if (!strFolderPath.empty())
   {
      std::size_t first_pos = strFolderPath.find_first_of('/');
      std::size_t last_pos = strFolderPath.find_last_of('/');
      if (first_pos == last_pos)
      {
         strnewFolderPath = strFolderPath;
      }
      else
      {
         std::size_t pos = strFolderPath.find_last_of('/', last_pos - 1);
         strnewFolderPath = strFolderPath.substr(0, pos + 1);
      }
   }
   else
   {
      return ;
   }
}


/**
* convertToString - Helper function that is used to convert a integer to string
*
* @parm[in] intToStr - The integer to be converted .
* @parm[in,out] buffer - variable where the converted integer is stored .
* @return void
*/
void MediaUtils::convertToString(char* buffer, uint32 intToStr)
{
   snprintf(buffer, sizeof(char)*MEDIAPLAYER_UTILS__PLAY_TIME_STRING_SIZE, "%u", intToStr);
}


/**
* checkTheFolderPath - Helper function that is used to check the folder path for the folder based browsing
*					   This function will append '/' char if it '\' is not available at the end of the folderpath from middleware.
* @parm[in] strFolderPath - The folder path which needs to be checked.
* @parm[in,out] strNewFolderPath - The modified new folder path
* @return void
*/
void MediaUtils::checkTheFolderPath(const std::string& strFolderPath, std::string& strNewFolderPath)
{
   if (!strFolderPath.empty())
   {
      std::size_t totalSize = strFolderPath.length();
      if (strFolderPath.at(totalSize - 1) == '/')
      {
         strNewFolderPath = strFolderPath;
      }
      else
      {
         strNewFolderPath = strFolderPath + "/";
      }
   }
   else
   {
      return ;
   }
}


int32 MediaUtils::ConvertHallDeviceType(uint32 deviceType)
{
   int32 i32SourceType = SOURCE_NOT_DEF;
   switch (deviceType)
   {
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH:
      {
         i32SourceType = SOURCE_BT;
         break;
      }

      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_USB:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_MTP:
      {
         i32SourceType = SOURCE_USB;

         break;
      }

      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPOD:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPHONE:
      {
         i32SourceType = SOURCE_IPOD;
         break;
      }

      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDROM:
      {
         i32SourceType = SOURCE_CDMP3;
         break;
      }
#ifdef HMI_CDDA_SUPPORT
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA:
      {
         i32SourceType = SOURCE_CD;
         break;
      }
#endif
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_UNKNOWN:
      default:
      {
         i32SourceType = SOURCE_NOT_DEF;
         break;
      }
   }
   return i32SourceType;
}


std::string MediaUtils::FooterTextForBrowser(uint32 focussedIndex, uint32 listSize)
{
   char chForFocussedIndex[MEDIA_HEADER_STRING_SIZE] = "";
   char chForFolderListSize[MEDIA_HEADER_STRING_SIZE] = "";

   convertToString(chForFocussedIndex, focussedIndex);
   convertToString(chForFolderListSize, listSize);

   std::string strForFocussedIndex = std::string(chForFocussedIndex);
   std::string strForFolderListSize = std::string(chForFolderListSize);
   std::string strFocusedIndexWithListSize  = strForFocussedIndex + "/" + strForFolderListSize;
   return strFocusedIndexWithListSize;
}


} // end of namespace Core
} // end of namespace App
