/*
 * @file       : IDataCollector.h
 * @author     : ECV1
 * @copyright  : (c) 2015 Robert Bosch Car Multimedia GmbH
 * @addtogroup : Phone
 */

#ifndef IDATACOLLECTOR_H
#define IDATACOLLECTOR_H


namespace App
{
namespace Core
{
class IDataCollector
{
   public:
      virtual ~IDataCollector() {}
      virtual void setHeaderIconData(enHeaderIcon HeaderIconData) = 0;
      virtual void setHeaderTextData(const Candera::String& HeaderTextData) = 0;
      virtual void vInitializeStatusLineDataDefault() = 0;
};

} // namespace Core
} // namespace App


#endif
