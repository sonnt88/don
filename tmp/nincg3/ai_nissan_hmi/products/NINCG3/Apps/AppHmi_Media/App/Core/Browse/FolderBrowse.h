/**
*  @file   FolderBrowse.h
*  @author ECV - vma6cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef FOLDERBROWSE_H_
#define FOLDERBROWSE_H_

#include "IBrowse.h"

namespace App {
namespace Core {

class FolderBrowse : public IBrowse
{
   public:
      FolderBrowse();
      ~FolderBrowse() {}
      tSharedPtrDataProvider getDataProvider(RequestedListInfo& requestedlistInfo, uint32 activeDeviceType);
      void populateNextListOrPlayItem(uint32 currentListId, uint32 selectedItem, uint32 activeDeviceType, uint32 /*activeDeviceTag*/);
      void populatePreviousList(uint32 deviceTag);
      void updateListData();
      void updateEmptyListData(uint32 _currentListType);
      void initializeListParameters();
      void updateFolderBrowsingHistory(const FolderListInfo& folderListItemInfo);
      void clearFolderBrowsingHistory(uint32 currentListHandle, std::string);
      void updateFolderHistoryDuringIndexing(uint32 currentListHandle, uint32 listsize);
      void checkBrowsingHistory();
//#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
//      void populateCurrentPlayingList(uint32 /*currentPlayingListHandle*/);
//#endif
      void setWindowStartIndex(uint32);
      void setFocussedIndex(int32);
      bool checkFolderUpVisibility();
      int32 getActiveItemIndex(int32 /*selectedIndex*/);
      void updateListDataCDDA();
#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
      uint32 getActiveQSIndex(uint32 absoluteIndex);
#endif
      void setFooterDetails(uint32);
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_MAP_DELEGATE_END()
#ifdef VERTICAL_KEYBOARD_SEARCH_SUPPORT
      void updateSearchKeyBoardListInfo(trSearchKeyboard_ListItem /*item*/) {}
      bool updateVerticalListToGuiList(const ButtonListItemUpdMsg& /*oMsg*/)
      {
         return true;
      }
      void SetActiveVerticalKeyLanguague() {}
      void ShowVerticalKeyInfo(bool /*pDefault*/) {}
#endif

      const char* getlistItemTemplate(uint32 index);

   private:
      void requestListWithHistoryOrDefaultData(uint32 deviceTag);
      void populateListFromHistory(uint32 deviceTag);
      enum ::MPlay_fi_types::T_e8_MPlayFileType _folderType;
      std::vector<FolderListInfo> _folderListBrowsingHistory;
};


}
}


#endif /* FOLDERBROWSE_H_ */
