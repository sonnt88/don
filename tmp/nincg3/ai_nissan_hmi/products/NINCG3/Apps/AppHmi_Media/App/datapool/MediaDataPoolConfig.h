/**
 * @file MediaDataPoolConfig.h
 * @Author ECV1-PUT5COB
 * @Copyright (c) 2015 Robert Bosch Car Multimedia Gmbh
 * @addtogroup AppHmi_Media
 */

#ifndef MEDIA_DATAPOOLCONFIG_H_
#define MEDIA_DATAPOOLCONFIG_H_

#include "asf/core/Types.h"
#ifdef DP_DATAPOOL_ID
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_hmi_04_if.h"
#endif
// Forward Declarations
//class dp_tclAppHmi_MediaLanguageIndexMedia;

/**
 * class MediaDataPoolConfig
 * This class hold implementation related to Datapool elements. It provides interfaces to read/write DP elements
 */
class MediaDataPoolConfig
{
   public:
      /**
       * Destructor of class DataPoolConfig
       * @return None
       */
      ~MediaDataPoolConfig()
      {
         delete _DpMedia;
      }

      /**
       * Singleton Class. Method to retrieve the instance of the class
       * @return Returns instance of the class
       */
      static MediaDataPoolConfig* getInstance()
      {
         if (!_DpMedia)
         {
            _DpMedia = new MediaDataPoolConfig();
         }
         return _DpMedia;
      }

      /**
        * Setter function to write the Spi Auto Lunch Mode status Dp element
        * @param[in] LanguageIndexMedia: Language
        * @return None
        */
      void setLanguage(const uint8 LanguageIndexMedia);

      /**
       * Getter function to read the LanguageIndexMedia status Dp element
       * @return LanguageIndexMedia
       */
      uint8 getLanguage();
   private:

      // Instance of the class
      static MediaDataPoolConfig* _DpMedia;

      MediaDataPoolConfig();
      MediaDataPoolConfig(const MediaDataPoolConfig&);
      MediaDataPoolConfig& operator=(const MediaDataPoolConfig&);
      //DP Elements
      dp_tclAppHmi_MediaLanguageIndexMedia _DpLanguage;

};


#endif /* DATAPOOLCONFIG_H_ */
