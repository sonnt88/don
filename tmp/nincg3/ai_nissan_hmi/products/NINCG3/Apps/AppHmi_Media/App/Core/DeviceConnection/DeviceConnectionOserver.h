/*
 * DeviceConnectionOserver.h
 *
 *  Created on: Oct 12, 2015
 *      Author: kng3kor
 */

#ifndef DEVICECONNECTIONOSERVER_H_
#define DEVICECONNECTIONOSERVER_H_

namespace App
{
namespace Core
{


class IDeviceConnectionOserver
{
   public:
      virtual void deviceConnectionStatus() = 0;
};

}
}


#endif /* DEVICECONNECTIONOSERVER_H_ */
