/**
*  @file   <MediaPlayer_Utils_Test.h>
*  @author <ECV> - <bvi1cob>
*  @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
*  @addtogroup <AppHmi_media>
*  @{
*/

#ifndef MEDIAPLAYER_UTILS_TEST_H_
#define MEDIAPLAYER_UTILS_TEST_H_

#include "gtest/gtest.h"
#include "Core/Utils/MediaUtils.h"

using namespace std::tr1;
using namespace App::Core ;

// Provide invalid paths for testing method bExtractFileName
class MediaUtilsTest_ExtractFileName_InValid :
   public testing::TestWithParam < tuple < std::string > >
{
   protected:
      std::string _strInvalidPath;

   protected:
      virtual void SetUp()
      {
         _strInvalidPath = get <0> (GetParam());
      }
      virtual void TearDown() { }
};


// Provide Valid paths for testing method bExtractFileName
class MediaUtilsTest_ExtractFileName_Valid :
   public testing::TestWithParam < tuple < std::string, std::string > >
{
   protected:
      std::string _strValidPath;
      std::string _strExpectedFileName;

   protected:
      virtual void SetUp()
      {
         _strValidPath = get <0> (GetParam());
         _strExpectedFileName = get <1> (GetParam());
      }
      virtual void TearDown() { }
};


// Provide Strings of varied string lengths which are <= to the length of "_strMetaData" for testing bCheckStringLengthNCopy
class MediaUtilsTest_CheckStringLengthNCopy :
   public testing::TestWithParam < tuple < std::string, unsigned int, std::string, bool > >
{
   protected:
      std::string _strMetaData;
      std::string _strTruncatedMetaData;
      unsigned int _uInputLength;
      MediaUtils _utils;
      bool _isTrimmed;

   protected:
      virtual void SetUp()
      {
         _strMetaData = get <0> (GetParam());
         _uInputLength = get <1> (GetParam());
         _strTruncatedMetaData = get <2> (GetParam());
         _isTrimmed = get <3> (GetParam());
      }
      virtual void TearDown() { }
};


class MediaUtilsTest_ConvertSecToHourMinSec :
   public testing::TestWithParam < tuple < unsigned int, unsigned int, unsigned int, unsigned int > >
{
   protected:
      unsigned int _uGivenSec;
      unsigned int _uExpectHr;
      unsigned int _uExpectMin;
      unsigned int _uExpectSec;
      MediaUtils _utilsObj;

   protected:
      virtual void SetUp()
      {
         _uGivenSec = get <0> (GetParam());
         _uExpectHr = get <1> (GetParam());
         _uExpectMin = get <2> (GetParam());
         _uExpectSec = get <3> (GetParam());
      }

      virtual void TearDown() { }
};


// Provide Valid folder path for testing method goOneFolderUp
class MediaUtilsTest_FolderUp_Valid :
   public testing::TestWithParam < tuple < std::string, std::string > >
{
   protected:
      std::string _strFolderPath;
      std::string _strExpectedNewFolderPath;
      MediaUtils _utilsObj;
   protected:
      virtual void SetUp()
      {
         _strFolderPath = get <0> (GetParam());
         _strExpectedNewFolderPath = get <1> (GetParam());
      }
      virtual void TearDown() { }
};


class MediaPlayerUtilsTest_CheckFormatTime :
   public testing::TestWithParam < tuple < unsigned int, unsigned int, unsigned int , std::string> >
{
   protected:
      unsigned int _uGivenHr;
      unsigned int _uGivenMin;
      unsigned int _uGivenSec;
      std::string _strExpectedTimeFormat;
      MediaUtils _utilsObj;

   protected:
      virtual void SetUp()
      {
         _uGivenHr = get <0> (GetParam());
         _uGivenMin = get <1> (GetParam());
         _uGivenSec = get <2> (GetParam());
         _strExpectedTimeFormat = get <3> (GetParam());
      }
      virtual void TearDown() { }
};


class MediaUtilsTest_checkTheFolderPath_Valid :
   public testing::TestWithParam < tuple < std::string, std::string > >
{
   protected:
      std::string _strFolderPath;
      std::string _strExpectedNewFolderPath;
      MediaUtils _utilsObj;
   protected:
      virtual void SetUp()
      {
         _strFolderPath 		   = get <0> (GetParam());
         _strExpectedNewFolderPath = get <1> (GetParam());
      }
      virtual void TearDown() { }
};


class MediaUtilsTest_convertToString_Valid :
   public testing::TestWithParam < tuple < unsigned int, std::string > >
{
   protected:
      std::string _strExpected;
      unsigned int _uGivenIntValue;
      MediaUtils _utilsObj;
   protected:
      virtual void SetUp()
      {
         _uGivenIntValue 	  = get <0> (GetParam());
         _strExpected 	  = get <1> (GetParam());
      }
      virtual void TearDown() { }
};


class MediaUtils_Test_ConvertHallDeviceType :
   public testing::TestWithParam < tuple < ::MPlay_fi_types::T_e8_MPlayDeviceType, enMediaSrc> >
{
   protected:

      ::MPlay_fi_types::T_e8_MPlayDeviceType _deviceType;
      enMediaSrc _expecteddeviceType;
      MediaUtils _mediaUtilsObj;

   protected:
      virtual void SetUp()
      {
         _deviceType 				= get <0> (GetParam());
         _expecteddeviceType 		= get <1> (GetParam());
      }
      virtual void TearDown() {}
};


class MediaUtils_Test__FooterTextForBrowser_withValidParams: public testing::TestWithParam <
   tuple<uint32, uint32, std::string> >
{
   protected:
      uint32 _focussedIndex;
      uint32 _listSize;
      std::string _strExpectedFocusedIndexWithListSize;

   protected:
      virtual void SetUp()
      {
         _focussedIndex = get<0>(GetParam());
         _listSize = get<1>(GetParam());
         _strExpectedFocusedIndexWithListSize = get<2>(GetParam());
      }
      virtual void TearDown()
      {
      }
};


class MediaUtils_Test__FooterTextForBrowser_withInvalidParams: public testing::TestWithParam <
   tuple<uint32, uint32, std::string> >
{
   protected:
      uint32 _focussedIndex;
      uint32 _listSize;
      std::string _strExpectedFocusedIndexWithListSize;

   protected:
      virtual void SetUp()
      {
         _focussedIndex = get<0>(GetParam());
         _listSize = get<1>(GetParam());
         _strExpectedFocusedIndexWithListSize = get<2>(GetParam());
      }
      virtual void TearDown()
      {
      }
};


#endif //MEDIAPLAYER_UTILS_TEST_H_
