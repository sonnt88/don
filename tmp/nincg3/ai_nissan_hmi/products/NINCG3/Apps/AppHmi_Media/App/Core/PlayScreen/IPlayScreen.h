/**
 *  @file   IDeviceConnection.h
 *  @author ECV - kng3kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#ifndef IPLAYSCREEN_H_
#define IPLAYSCREEN_H_


namespace App {
namespace Core {
class IPlayScreen
{
   public:
      virtual void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/) = 0;
      virtual void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/) = 0;
      virtual void requestNowPlayingGet() = 0;
      virtual void setPlayModeUpdatePending(bool isPlayModeUpdatePending) = 0;
      virtual void updateLoadingFilePopupStatus(bool status) = 0;
      virtual void getSongPlayFromFolderBrowseStatus(bool status) = 0;
};


}
}


#endif /* IPLAYSCREEN_H_ */
