/**
*  @file   SpiUtils.cpp
*  @author ECV - bvi1cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/
#include "SpiUtils.h"
#include <stdio.h>

namespace App
{
namespace Core
{

bool SpiUtils::updateDeviceStatusValidity(int8 deviceStatus)
{
   if (deviceStatus == midw_smartphoneint_fi_types::T_e8_DeviceStatusInfo__NOT_KNOWN ||
         deviceStatus == midw_smartphoneint_fi_types::T_e8_DeviceStatusInfo__DEVICE_ADDED ||
         deviceStatus == midw_smartphoneint_fi_types::T_e8_DeviceStatusInfo__DEVICE_REMOVED ||
         deviceStatus == midw_smartphoneint_fi_types::T_e8_DeviceStatusInfo__DEVICE_CHANGED

      )
   {
      return true;
   }
   else
   {
      return false;
   }
}


bool SpiUtils::deviceConnectiontypeValidity(int8 deviceConnectionType)
{
   if (deviceConnectionType == midw_smartphoneint_fi_types::T_e8_DeviceConnectionType__UNKNOWN_CONNECTION ||
         deviceConnectionType == midw_smartphoneint_fi_types::T_e8_DeviceConnectionType__USB_CONNECTED ||
         deviceConnectionType == midw_smartphoneint_fi_types::T_e8_DeviceConnectionType__WIFI_CONNECTED)
   {
      return true;
   }
   else
   {
      return false;
   }
}


bool SpiUtils::checkUSBConnectedDeviceValidity(int8 deviceStatus, int8 deviceConnectionType)
{
   if (deviceStatus == midw_smartphoneint_fi_types::T_e8_DeviceStatusInfo__DEVICE_ADDED && deviceConnectionType == midw_smartphoneint_fi_types::T_e8_DeviceConnectionType__USB_CONNECTED)
   {
      return true;
   }
   else
   {
      return false;
   }
}

bool SpiUtils::dipoConnectionValidity(int8 deviceConnectionStatus)
{
   bool bconnectionStatus = false;
   if (deviceConnectionStatus == midw_smartphoneint_fi_types::T_e8_DeviceConnectionStatus__DEV_CONNECTED)
   {
      bconnectionStatus = true;
   }
   return bconnectionStatus;
}

} // end of namespace Core
} // end of namespace App

