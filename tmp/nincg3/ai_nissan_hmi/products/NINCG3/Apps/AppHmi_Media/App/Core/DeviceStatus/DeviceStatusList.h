/*
 * DeviceStatusList.h
 *
 *  Created on: Oct 23, 2015
 *      Author: sla7cob
 */

#ifndef DEVICESTATUSLIST_H_
#define DEVICESTATUSLIST_H_

#include <string>
#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
#include "AppHmi_MediaStateMachine.h"
#include "CourierTunnelService/CourierMessageReceiverStub.h"
#include "CgiExtensions/ListDataProviderDistributor.h"
#include "Common/ListHandler/ListRegistry.h"
#include "mplay_MediaPlayer_FIProxy.h"
#else
#include "Core/utest/stubs/Core/LanguageDefines.h"
#endif
static const char* const DEVICE_STATUS_LIST_BUTTON_SYSTEM_DRIVER_ITEM  = "BtnListData";
namespace App {
namespace Core {
enum ListIndex
{
   Item_01 = 1,
   Item_02 = 2,
   Item_03 = 3,
   Item_04 = 4
};


#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
class IDeviceConnection;
class DeviceStatusList : public ListImplementation
{
   public:
      DeviceStatusList(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy, IDeviceConnection* pDeviceConnection);
      virtual tSharedPtrDataProvider getListDataProvider(const ListDataInfo& listDataInfo);
#else
class DeviceStatusList
{
   public:
      DeviceStatusList();
#endif
      virtual ~DeviceStatusList();

      void setUSBStatus(std::string);
      void setUSBInfo(std::string);
#ifdef VARIANT_S_FTR_ENABLE_UNITTEST
      void setCDStatus(std::string);
      void setCDInfo(std::string);
#endif

      std::string updateUSBStatus();
      std::string updateUSBInfo();
      std::string updateCDStatus();
      std::string updateCDInfo();
#ifdef VARIANT_S_FTR_ENABLE_UNITTEST
      Candera::String getServiceSystemStatusItemValue(uint8);
#endif

#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
      virtual bool onCourierMessage(const ListDateProviderReqMsg& oMsg);

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_DELEGATE_TO_CLASS(ListImplementation)
      COURIER_MSG_MAP_DELEGATE_DEF_END()
      COURIER_MSG_MAP_DELEGATE_END()

      DECLARE_CLASS_LOGGER();
#endif
   private:
      std::string _usbStatus;
      std::string _usbInfo;
      std::string _cdStatus;
      std::string _cdInfo;
#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
      tSharedPtrDataProvider getDeviceStatusListDataProvider();
      Candera::String getServiceSystemStatusItemValue(uint8);
      IDeviceConnection& _iDeviceConnection;
      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& _mediaPlayerProxy;
#endif
};


}
}


#endif /* DEVICESTATUSLIST_H_ */
