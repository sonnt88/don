/**
 *  @file   MediaBrowseHandling.cpp
 *  @author ECV - chs4cob
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#include "hall_std_if.h"
#include "MediaBrowseHandling.h"
#include "MediaDatabinding.h"
#include "Core/SourceSwitch/MediaSourceHandling.h"
#include "Core/Utils/MediaProxyUtility.h"
#include "ListInterfaceHandler.h"
#include "App/datapool/MediaDataPoolConfig.h"
#include "Common/VerticalQuickSearch/VerticalQuickSearch.h"
#include "vehicle_main_fi_typesConst.h"
#include <stdio.h>

#define MIDW_COMMON_S_IMPORT_INTERFACE_UTF8_SORT
#include "midw_common_if.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL_BROWSE
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::MediaBrowseHandling::
#include "trcGenProj/Header/MediaBrowseHandling.cpp.trc.h"
#endif

#ifdef BT_BROWSE_SUPPORT
#define WAIT_TIME_SECONDS 120000
#endif

namespace App {
namespace Core {
using namespace ::vehicle_main_fi_types;
using namespace ::mplay_MediaPlayer_FI;
using namespace ::MPlay_fi_types;
/**
 * @Constructor
 */
MediaBrowseHandling::MediaBrowseHandling(::boost::shared_ptr< Mplay_MediaPlayer_FIProxy >& mediaPlayerProxy, IPlayScreen* pPlayScreen)
   : _mediaPlayerProxy(mediaPlayerProxy)
   , _iPlayScreen(*pPlayScreen)
#ifdef QUICKSEARCH_SUPPORT
   , _QuickSearch(NULL)
#endif
   , _isRootBrowserUp(false)
   , _iBrowse(NULL)
   , _currentMetaDataListType(0)
#ifdef QUICKSEARCH_SUPPORT
   , _quickSearchItemIndex(0)
#endif
   , _focussedIndex(0)
   , isMessageProcessed(false)
   , _currentListId(0)
#ifdef BT_BROWSE_SUPPORT
   , _btBrowseTimer(*this, WAIT_TIME_SECONDS)
#endif
{
   ETG_I_REGISTER_FILE();
   ETG_TRACE_USR4(("MediaBrowseHandling Constructor"));

   _browserSpeedLock = new BrowserSpeedLock(_mediaPlayerProxy);
   _iListFacade = new ListInterfaceHandler(_mediaPlayerProxy, _browserSpeedLock, &_iPlayScreen);
#ifdef QUICKSEARCH_SUPPORT
   _QuickSearchIPOD = new QuickSearch(enAppIpod);
   _QuickSearchNotIpod = new QuickSearch(enAppNotIpod);
#endif
   _currentString = "";
   _nextPrevCharacter = "";
   _defaultLanguage = true;
   /*   if (_QuickSearchIPOD && _QuickSearchNotIpod)
      {
         _QuickSearchIPOD->bUpdateQsLanguageAndCurrentGrpTable(QUICK_SEARCH_LATIN, tclUtf8StringGroup::enTableLatin);
         _QuickSearchNotIpod->bUpdateQsLanguageAndCurrentGrpTable(QUICK_SEARCH_LATIN, tclUtf8StringGroup::enTableLatin_ipod);
      }*/

   BrowseFactory browserFactory;
   _iBrowse = browserFactory.getBrowser(LIST_ID_BROWSER_MAIN);
   _iBrowse->setListFacade(_iListFacade);
   _iBrowse = browserFactory.getBrowser(LIST_ID_BROWSER_FOLDER_BROWSE);
   _iBrowse->setListFacade(_iListFacade);
   _iBrowse = browserFactory.getBrowser(LIST_ID_BROWSER_METADATA_BROWSE);
   _iBrowse->setListFacade(_iListFacade);
   _iBrowse = browserFactory.getBrowser(LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST);
   _iBrowse->setListFacade(_iListFacade);
   _iBrowse = browserFactory.getBrowser(LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST);
   _iBrowse->setListFacade(_iListFacade);
   _iBrowse = browserFactory.getBrowser(LIST_ID_BROWSER_IMAGE);
   _iBrowse->setListFacade(_iListFacade);

   ListRegistry::s_getInstance().addListImplementation(LIST_ID_BROWSER_MAIN, this);
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_BROWSER_FOLDER_BROWSE, this);
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_BROWSER_METADATA_BROWSE, this);
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST, this);
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST, this);
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_BROWSER_IMAGE, this);
}


/**
 * @Destructor
 */
MediaBrowseHandling::~MediaBrowseHandling()
{
   ETG_TRACE_USR4(("MediaBrowseHandling Destructor"));

   _mediaPlayerProxy.reset();

   if (_iListFacade)
   {
      delete _iListFacade;
      _iListFacade = NULL;
   }
#ifdef QUICKSEARCH_SUPPORT
   _QuickSearch = NULL;
   delete _QuickSearchIPOD;
   delete _QuickSearchNotIpod;

   _QuickSearchIPOD = NULL;
   _QuickSearchNotIpod = NULL;
#endif
   _iBrowse = NULL;

   if (_browserSpeedLock)
   {
      delete _browserSpeedLock;
      _browserSpeedLock = NULL;
   }

   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_BROWSER_MAIN);
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_BROWSER_FOLDER_BROWSE);
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_BROWSER_METADATA_BROWSE);
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST);
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST);
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_BROWSER_IMAGE);
}


/**
 * registerProperties - Trigger property registration to mediaplayer,  called from MediaHall class
 * @param[in] proxy
 * @parm[in] stateChange - state change service for corrosponding  proxy
 * @return void
 */
void MediaBrowseHandling::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_mediaPlayerProxy && _mediaPlayerProxy == proxy && _iListFacade)
   {
      _iListFacade->registerProperties();
   }
}


/**
 * deregisterProperties - Trigger property deregistration to mediaplayer, called from MediaHall class
 * @param[in] proxy
 * @parm[in] stateChange - state change service for corrsponding proxy
 * @return void
 */
void MediaBrowseHandling::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (_mediaPlayerProxy && _mediaPlayerProxy == proxy && _iListFacade)
   {
      _iListFacade->deregisterProperties();
   }
}


/**
 * getListDataProvider - Gets the ListDataProvider from the corresponding listdataprovider functions
 * @param[in] listId
 * @parm[out] none
 * @return tSharedPtrDataProvider
 */
tSharedPtrDataProvider MediaBrowseHandling::getListDataProvider(const ListDataInfo& listInfo)
{
   ETG_TRACE_USR4(("MediaBrowseHandling:getListDataProvider is called %d ", listInfo.listId));

   RequestedListInfo requestedListInfo;
   BrowseFactory browserFactory;

   uint32 activeDeviceType = MediaDatabinding::getInstance().getCurrentActiveMediaDevice();
   uint32 activeDeviceTag =  MediaProxyUtility::getInstance().getActiveDeviceTag();

   _currentStartIndex = listInfo.startIndex;
   _currentListId = listInfo.listId;

   _iBrowse = browserFactory.getBrowser(_currentListId);

   switch (_currentListId)
   {
      case LIST_ID_BROWSER_MAIN:
      {
         _windowStartIndex = 0;  // reset window start index on entry into root category list
         requestedListInfo._deviceTag = activeDeviceTag;
         bool isIndexCompleted = MediaProxyUtility::getInstance().isIndexingDone(activeDeviceTag);
         _iBrowse->setIndexingDoneStatus(isIndexCompleted);
         _isRootBrowserUp = true;
         break;
      }
      case LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST:
      case LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST:
      {
         requestedListInfo._listType = listInfo.listId;
         _windowStartIndex = 0;  // reset window start index on entry into root category list
         requestedListInfo._deviceTag = activeDeviceTag;
         bool isIndexCompleted = MediaProxyUtility::getInstance().isIndexingDone(activeDeviceTag);
         _iBrowse->setIndexingDoneStatus(isIndexCompleted);
         break;
      }
      default:
      {
         requestedListInfo._listType = listInfo.listId;
         requestedListInfo._deviceTag = activeDeviceTag;
         requestedListInfo._startIndex = listInfo.startIndex;
         requestedListInfo._bufferSize = listInfo.windowSize;
         _isRootBrowserUp = false;
         break;
      }
   }
   return _iBrowse->getDataProvider(requestedListInfo, activeDeviceType);
}


/**
 * isMediaListId - Function to check whether the given list id belongs to media

 * @param[in] listId
 * @parm[out] none
 * @return true - list Id belongs to media
 * @return false - list Id does not belong to media
 */
bool MediaBrowseHandling::isMediaListId(unsigned int listId)
{
   ETG_TRACE_USR4(("MediaBrowseHandling::isMediaListId:%d", listId));
#ifdef PHOTOPLAYER_SUPPORT
   if (listId >= LIST_ID_BROWSER_GENRE && listId <= LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST && listId != LIST_ID_COVER_PLAYER_IMAGE)
#else
   if (listId >= LIST_ID_BROWSER_GENRE && listId <= LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST)
#endif
   {
      return true;
   }
   return false;
}


/**
 * onCourierMessage -

 * @param[in] ButtonReactionMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaBrowseHandling::onCourierMessage(const ButtonReactionMsg& oMsg)
{
   ListProviderEventInfo info;
   if (ListProviderEventInfo::GetItemIdentifierInfo(oMsg.GetSender(), info) && (isMediaListId(info.getListId())))
   {
      unsigned int listId      = info.getListId();     // the list id for generic access
      unsigned int hdlRow      = info.getHdlRow();     // normaly the index

      _currentMetaDataListType = info.getHdlCol();

      //For each ButtonReactionMsg for buttons which are part of list items we post a ButtonListItemMsg with detailed info about the list item.
      POST_MSG((COURIER_MESSAGE_NEW(ButtonListItemUpdMsg)(listId, hdlRow, _currentMetaDataListType, oMsg.GetEnReaction())));
      return true;
   }

   return false;
}


/**
 * onCourierMessage - Message from Statemachine when a button is pressed from static list
 *                    Message contains the nextListId that should be populated
 * @param[in] BrowseBtnPressUpdMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaBrowseHandling::onCourierMessage(const BrowseBtnPressUpdMsg& oMsg)
{
   uint32 row = oMsg.GetRow();
   uint32 activeDeviceType = MediaDatabinding::getInstance().getCurrentActiveMediaDevice();
   uint32 activeDeviceTag = MediaProxyUtility::getInstance().getActiveDeviceTag();
   _iBrowse = BrowseFactory::getBrowser(LIST_ID_BROWSER_MAIN); // To maintain previously browsed window
   _iBrowse->setWindowStartIndex(_windowStartIndex);           // Save window start index

   _iBrowse = BrowseFactory::getBrowser(LIST_ID_BROWSER_METADATA_BROWSE);
   _iBrowse->populateNextListOrPlayItem(LIST_ID_BROWSER_MAIN, row, activeDeviceType, activeDeviceTag);
   _windowStartIndex = 0;
#ifdef QUICKSEARCH_SUPPORT
   _quickSearchItemIndex = 0;
#endif
   return true;
}


/**
 * onCourierMessage - Message from Statemachine when a button is pressed from any Metadata list
 *                    Message contains the child ListId and row num
 * @param[in] MetaDataBrowserBtnPressUpdMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaBrowseHandling::onCourierMessage(const MetaDataBrowserBtnPressUpdMsg& oMsg)
{
   _iBrowse = BrowseFactory::getBrowser(LIST_ID_BROWSER_METADATA_BROWSE);
   _iBrowse->setWindowStartIndex(_windowStartIndex);           // Save window start index
   uint32 activeDeviceType = MediaDatabinding::getInstance().getCurrentActiveMediaDevice();
   uint32 activeDeviceTag = MediaProxyUtility::getInstance().getActiveDeviceTag();
   _iBrowse->populateNextListOrPlayItem(_currentMetaDataListType, oMsg.GetRow(), activeDeviceType, activeDeviceTag);
#ifdef QUICKSEARCH_SUPPORT
   _quickSearchItemIndex = 0;
#endif
   _windowStartIndex = 0;
   return true;
}


/**
 * onCourierMessage - Message from Statemachine when back button is pressed from any Metadata list
 *                    Message contains the parent ListId
 * @param[in] MetaDataBrowserBtnPressUpdMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaBrowseHandling::onCourierMessage(const MetaDataBrowserBackBtnPressUpdMsg& /*oMsg*/)
{
   ETG_TRACE_USR4(("MetaDataBrowserBackBtnPressUpdMsg "));
#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
   MediaDatabinding::getInstance().updateQuickSearchAvailabeStatus(false);
#endif
   if (_iListFacade && !_iListFacade->isListWaitingToComplete())
   {
      _iBrowse = BrowseFactory::getBrowser(LIST_ID_BROWSER_METADATA_BROWSE);
      uint32 activeDeviceTag = MediaProxyUtility::getInstance().getActiveDeviceTag();
      _iBrowse->populatePreviousList(activeDeviceTag);
#ifdef QUICKSEARCH_SUPPORT
      _quickSearchItemIndex = 0;
#endif
      _windowStartIndex = 0;
   }
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
   MediaDatabinding::getInstance().updateVerticalSearchListIndex(0, 0); //fix for NCG3D-12128
#endif
   return true;
}


/**
 * onCourierMessage - Message from Device Connection Handling when Indexing is completed

 * @param[in] IndexingCompleteUpdMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaBrowseHandling::onCourierMessage(const IndexingCompleteUpdMsg& /*oMsg*/)
{
   uint32 activeDeviceType = MediaDatabinding::getInstance().getCurrentActiveMediaDevice();
   if (_isRootBrowserUp && activeDeviceType != SOURCE_IPOD)
   {
      POST_MSG((COURIER_MESSAGE_NEW(ButtonListItemUpdMsg)(LIST_ID_BROWSER_MAIN, LIST_ROW_VALUE, 0, enRelease)));
      _isRootBrowserUp = false;
   }
   return true;
}


/**
 * onCourierMessage - Message from Statemachine when a browse button is pressed MEDIA_BROWSE_MAIN screen.
 *
 * @param[in] InitialiseFolderBrowserReqMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaBrowseHandling::onCourierMessage(const InitialiseFolderBrowserReqMsg& /*oMsg*/)
{
   if (_iListFacade)
   {
      std::string path = _mediaPlayerProxy->getCurrentFolderPath().getSCurrentFolderPath();
      uint32 currentPlayingListHandle = _mediaPlayerProxy->getCurrentFolderPath().getU32ListHandle();
      ETG_TRACE_USR4(("Current Folder path is %s ", path.c_str()));
      ETG_TRACE_USR4(("Current playing list handle is %d", currentPlayingListHandle));
      ETG_TRACE_USR4(("nowplaying list handle is %d", _mediaPlayerProxy->getNowPlaying().getU32ListHandle()));		//This can be removed later
      populateFolderBrowser(currentPlayingListHandle, path);
      MediaDatabinding::getInstance().updateFolderPath(_iListFacade->getCurrentFolderPath());
      // the initialiseListValuesForFolderBrowser is called to set default values for folder browse
      _iListFacade->initialiseListValuesForFolderBrowser(MediaProxyUtility::getInstance().getActiveDeviceTag());
   }
   return true;
}


#ifdef BT_BROWSE_SUPPORT
/**
 * onCourierMessage - Message is posted from Statemachine when a BT browse button is pressed from BT main screen
 *					  This is applicable only for scope2 .
 *					  Special use case :For BT browse ,folder browse should always start from root folder not from current playing folder like USB .
 * @param[in] InitialiseBTBrowseMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaBrowseHandling::onCourierMessage(const InitialiseBTBrowseMsg& /*oMsg*/)
{
   //MediaDatabinding::getInstance().updateCurrentPlayingListBtnStatus(_iListFacade->getBTCurrentPlayingListStatus());
   MediaDatabinding::getInstance().updateCurrentPlayingListBtnStatus(false); // Temp solution because BT Middleware meet issue about current playing list
   if (_iListFacade)
   {
      _iBrowse = BrowseFactory::getBrowser(LIST_ID_BROWSER_FOLDER_BROWSE);
      _iBrowse->initializeListParameters();									//This function is called to clear the history and release all the list handle in history
      MediaDatabinding::getInstance().updateFolderPath(_iListFacade->getCurrentFolderPath());
      _iListFacade->setCurrentStateMachineState(IDLE);
      // the initialiseListValuesForFolderBrowser is called to set default values for folder browse
      _iListFacade->initialiseListValuesForFolderBrowser(MediaProxyUtility::getInstance().getActiveDeviceTag());
      handleBTBrowseIdleState();
   }
   return true;
}


#endif
/**
 * populateFolderBrowser - This private function decides whether to jump to current playing list handle or list available in the history
 *
 * @param[in] listHandle -the list handle received from the middleware with currentfolderpath property
 * @param[in] path 		 -the path received from the middleware with currentfolderpath property
 * @return none
 */

void MediaBrowseHandling::populateFolderBrowser(uint32 listHandle, std::string path)
{
   if (_iListFacade)
   {
      _iBrowse = BrowseFactory::getBrowser(LIST_ID_BROWSER_FOLDER_BROWSE);
      if (listHandle != 0)
      {
         std::string currentFolderPath = "/";
         if ((path == "") || (path.at(0) != '/'))
         {
            currentFolderPath = (path == "") ? currentFolderPath : (currentFolderPath + path);
         }
         else
         {
            currentFolderPath = path;
         }

         // The folder path is modified to avoid the inconsistency in the folder path from _mediaPlayerProxy->getCurrentFolderPath()
         std::string modifiedFolderPath;
         MediaUtils::checkTheFolderPath(currentFolderPath, modifiedFolderPath);

         // TODO the below 2 lines are commented because in list handle is not properly from mediaplayer
         //_iListFacade->setCurrentStateMachineState(LIST_REQUEST_PROCESS_DONE);
         //_iListFacade->updateFolderListParameters(listHandle, FLEX_LIST_MAX_BUFFER_SIZE);

         ETG_TRACE_USR4(("InitialiseFolderBrowserReqMsg is called %s", modifiedFolderPath.c_str()));
         _iBrowse->clearFolderBrowsingHistory(listHandle, modifiedFolderPath);
         _iListFacade->setCurrentFolderPath(modifiedFolderPath);
      }
      else
      {
         ETG_TRACE_USR4(("Song is playing from metadata player"));
         _iBrowse->checkBrowsingHistory();
      }
   }
}


/**
 * onCourierMessage - Message from Statemachine when a button is pressed from any Folder Browser list. Message contains the selected row number.
 * @param[in] FolderBrowserBtnPressUpdMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaBrowseHandling::onCourierMessage(const FolderBrowserBtnPressUpdMsg& oMsg)
{
   _iBrowse = BrowseFactory::getBrowser(LIST_ID_BROWSER_FOLDER_BROWSE);
   uint32 activeDeviceType = MediaDatabinding::getInstance().getCurrentActiveMediaDevice();
   uint32 activeDeviceTag = MediaProxyUtility::getInstance().getActiveDeviceTag();
   _iBrowse->populateNextListOrPlayItem(LIST_ID_BROWSER_FOLDER_BROWSE, oMsg.GetRow(), activeDeviceType, activeDeviceTag);
   _windowStartIndex = 0;

   if (_browserSpeedLock && _iListFacade)
   {
      _browserSpeedLock->setFocussedIndex(0);
   }

#ifdef BT_BROWSE_SUPPORT
   handleBTBrowseIdleState();
#endif
   return true;
}


/**
 * onCourierMessage - Message from Statemachine when a folder up soft key is pressed or HK_BACK is pressed from any Folder Browser list.
 * @param[in] FolderUpBtnPressReqMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaBrowseHandling::onCourierMessage(const FolderUpBtnPressReqMsg& /*oMsg*/)
{
   _iBrowse = BrowseFactory::getBrowser(LIST_ID_BROWSER_FOLDER_BROWSE);
   uint32 activeDeviceTag = MediaProxyUtility::getInstance().getActiveDeviceTag();
   _iBrowse->populatePreviousList(activeDeviceTag);
   _windowStartIndex = 0;
#ifdef BT_BROWSE_SUPPORT
   handleBTBrowseIdleState();
#endif

   return true;
}


ETG_I_CMD_DEFINE((TraceCmd_EncoderRotation, "EncoderRotation %d", ETG_I_CENUM(enEncoder)))
void MediaBrowseHandling::TraceCmd_EncoderRotation(enEncoder /*nRotationDirection*/)
{
   /*   ETG_TRACE_USR4(("TraceCmd_EncoderRotation is called"));
      if (nRotationDirection == ENCODER_ROTATION_POSITIVE)
      {
         ETG_TRACE_USR4(("ENCODER_ROTATION_POSITIVE is called"));
         POST_MSG( COURIER_MESSAGE_NEW(EncoderPositiveRotationUpdMsg)());
      }
      if (nRotationDirection == ENCODER_ROTATION_NEGATIVE)
      {
         ETG_TRACE_USR4(("ENCODER_ROTATION_NEGATIVE is called"));
         POST_MSG( COURIER_MESSAGE_NEW(EncoderNegativeRotationUpdMsg)());
      }
    */
}


#ifdef QUICKSEARCH_SUPPORT
/**
 * onCourierMessage - Message from Statemachine when a QuickSearch button is pressed from any browse list
 *
 * @param[in] QuickSerachBtnPressUpdMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaBrowseHandling::onCourierMessage(const QuickSerachBtnPressUpdMsg& /*oMsg*/)
{
   ETG_TRACE_USR4(("QuickSerachBtnPressUpdMsg is Received %d %d", _currentStartIndex, _quickSearchItemIndex));
   BrowseFactory browserFactory;
   IBrowse* ibrowse;

   uint32 activelistid = (_currentListId == LIST_ID_BROWSER_FOLDER_BROWSE) ? LIST_ID_BROWSER_FOLDER_BROWSE : LIST_ID_BROWSER_METADATA_BROWSE ;

   ibrowse = browserFactory.getBrowser(activelistid);
   std::string CurrentActiveString("");

   SetActiveQSLanguague();

   if (isMessageProcessed)
   {
      _quickSearchItemIndex = _focussedIndex;
   }
   else
   {
      _quickSearchItemIndex =  updateStartIndex(_windowStartIndex);
   }

   _quickSearchItemIndex = ibrowse->getActiveItemIndex(_quickSearchItemIndex);

   _currentString = _iListFacade->getFirstStringElement(_quickSearchItemIndex);

   if (_QuickSearch && _iListFacade)
   {
      CurrentActiveString = _QuickSearch->oGetQSCurrentCharacter(_currentString);
      _nextPrevCharacter = CurrentActiveString;

      //_listInterfaceHandler->vRequestQSMediaplayerFilelist(CurrentActiveString);
   }

   MediaDatabinding::getInstance().updateQuickSearchInfo(CurrentActiveString);
   return true;
}


/**
 * SetActiveQSLanguague - Function to set current active language for Quick Search
 *
 * @param[in] none
 * @parm[out] none
 */

void MediaBrowseHandling::SetActiveQSLanguague()
{
   uint8 activeLanguage;
   e_QUICK_SEARCH_LANG activeQSLanguage;
   tclUtf8StringGroup::TableType activeLanguageTableType;
   tclUtf8StringGroup::TableType activeLanguageTableTypeIpod;
   uint32 i32SourceType = MediaProxyUtility::getInstance().getHallActiveDeviceType();

   activeLanguage = MediaDataPoolConfig::getInstance()->getLanguage();

   switch (activeLanguage)
   {
      case T_e8_Language_Code__Russian:
         activeQSLanguage = QUICK_SEARCH_RUSSIN;
         activeLanguageTableType = tclUtf8StringGroup::enTableRussain;
         activeLanguageTableTypeIpod = tclUtf8StringGroup::enTableRussain_ipod;
         break;

      case T_e8_Language_Code__Chinese_Cantonese_Simplified_Chinese_character:
      case	T_e8_Language_Code__Chinese_Cantonese_Traditional_Chinese_character:
      case T_e8_Language_Code__Chinese_Mandarin_Simplified_Chinese_character:
         activeQSLanguage = QUICK_SEARCH_UNKNOWN;
         activeLanguageTableType = tclUtf8StringGroup::enTableChinese;
         activeLanguageTableTypeIpod = tclUtf8StringGroup::enTableChinese_ipod;
         break;

      case T_e8_Language_Code__Thai:
         activeQSLanguage = QUICK_SEARCH_THAI;
         activeLanguageTableType = tclUtf8StringGroup::enTableThai;
         activeLanguageTableTypeIpod = tclUtf8StringGroup::enTableThai_ipod;
         break;

      case T_e8_Language_Code__English_US:
      case T_e8_Language_Code__English_UK:
      case T_e8_Language_Code__English_Australian:
      case T_e8_Language_Code__English_India:
      case T_e8_Language_Code__English_Canadian:
      case T_e8_Language_Code__French:
      case T_e8_Language_Code__German:
      case T_e8_Language_Code__Spanish:
      case T_e8_Language_Code__Spanish_Latin_American:
      case T_e8_Language_Code__Spanish_Mexican:
         activeQSLanguage = QUICK_SEARCH_LATIN;
         activeLanguageTableType = tclUtf8StringGroup::enTableLatin;
         activeLanguageTableTypeIpod = tclUtf8StringGroup::enTableLatin_ipod;
         break;

      case T_e8_Language_Code__Greek:
         activeQSLanguage = QUICK_SEARCH_GREEK;
         activeLanguageTableType = tclUtf8StringGroup::enTableGreek;
         activeLanguageTableTypeIpod = tclUtf8StringGroup::enTableGreek_ipod;
         break;

      case T_e8_Language_Code__Arabic:
         activeQSLanguage = QUICK_SEARCH_ARABIC;
         activeLanguageTableType = tclUtf8StringGroup::enTableArabic;
         activeLanguageTableTypeIpod = tclUtf8StringGroup::enTableArabic_ipod;
         break;

      default:
         activeQSLanguage = QUICK_SEARCH_LATIN;
         activeLanguageTableType = tclUtf8StringGroup::enTableLatin;
         activeLanguageTableTypeIpod = tclUtf8StringGroup::enTableLatin_ipod;
         break;
   }

   if (_QuickSearchIPOD && _QuickSearchNotIpod)
   {
      _QuickSearchIPOD->bUpdateQsLanguageAndCurrentGrpTable(activeQSLanguage, activeLanguageTableTypeIpod);
      _QuickSearchNotIpod->bUpdateQsLanguageAndCurrentGrpTable(activeQSLanguage, activeLanguageTableType);
   }
   _QuickSearch = (i32SourceType == SOURCE_IPOD) ? _QuickSearchIPOD : _QuickSearchNotIpod;
}


/**
 * onCourierMessage - Message from Statemachine when encoder postive rotation is received when in QuickSearch Popup
 *
 * @param[in] EncoderPositiveRotationUpdMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaBrowseHandling::onCourierMessage(const EncoderPositiveRotationUpdMsg& /*oMsg*/)
{
   if (_QuickSearch)
   {
      _nextPrevCharacter = _QuickSearch->oGetQsNextCharacter();
   }
   MediaDatabinding::getInstance().updateQuickSearchInfo(_nextPrevCharacter);
   return true;
}


/**
 * onCourierMessage - Message from Statemachine when encoder Negative rotation is received when in QuickSearch Popup
 *
 * @param[in] EncoderNegativeRotationUpdMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaBrowseHandling::onCourierMessage(const EncoderNegativeRotationUpdMsg& /*oMsg*/)
{
   if (_QuickSearch)
   {
      _nextPrevCharacter = _QuickSearch->oGetQsPreviousCharacter();
   }
   MediaDatabinding::getInstance().updateQuickSearchInfo(_nextPrevCharacter);
   return true;
}


/**
 * onCourierMessage - Message from Statemachine when Encoder Press is received when in QuickSearch Popup
 *
 * @param[in] EncoderPressUpdMsg
 * @parm[out] none
 * @return true - message consumed
 */


bool MediaBrowseHandling::onCourierMessage(const EncoderPressUpdMsg& /*oMsg*/)
{
   if (_iListFacade)
   {
      std::string oSearchChar("");
      getSearchChar(oSearchChar);
      _iListFacade->vRequestQSMediaplayerFilelist(oSearchChar, _windowStartIndex);
   }

   return true;
}


void MediaBrowseHandling::getSearchChar(std::string& ostr)
{
   std::string oQSChar0_9("");
   char unicodeQS0_9Chr[4] = {0xef, 0xa1, 0xa0, 0x00};
   oQSChar0_9.assign(unicodeQS0_9Chr);
   if (oQSChar0_9 == _nextPrevCharacter)
   {
      ostr.assign("!"); // send char ! as media player sorting table first char is space followed by symbol and number
   }
   else
   {
      ostr = _nextPrevCharacter;
   }
   ETG_TRACE_USR4(("Quick search character %s", ostr.c_str()));
}


#endif

/**
 * onCourierMessage - Message when a device is disconnected
 *
 * @param[in] none
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaBrowseHandling::onCourierMessage(const InitializeMediaParametersUpdMsg& /*oMsg*/)
{
   IBrowse* ibrowse;

   ibrowse = BrowseFactory::getBrowser(LIST_ID_BROWSER_METADATA_BROWSE);
   ibrowse->initializeListParameters();

   ibrowse = BrowseFactory::getBrowser(LIST_ID_BROWSER_FOLDER_BROWSE);
   ibrowse->initializeListParameters();

   return false;
}


/**
 * onCourierMessage - This message is received from the flexlist widget when it requires new data when the
 *               list is displayed or scrolled
 * @param[in] ListDateProviderReqMsg& oMsg
 * @parm[out] bool
 */
bool MediaBrowseHandling::onCourierMessage(const ListDateProviderReqMsg& oMsg)
{
   if (isMediaListId(oMsg.GetListId()))
   {
      ListDataInfo listDataInfo;
      listDataInfo.listId = oMsg.GetListId();
      listDataInfo.startIndex =  oMsg.GetStartIndex();
      listDataInfo.windowSize = oMsg.GetWindowElementSize();
      return ListRegistry::s_getInstance().updateList(listDataInfo);
   }
   return false;
}


#ifdef BROWSEINFO_CURRENT_PLAYLIST
bool MediaBrowseHandling::onCourierMessage(const CurrentPlayingListReqMsg& /*oMsg*/)
{
   //uint32 activeDeviceType = MediaProxyUtility::getInstance().getHallActiveDeviceType();
   //if (activeDeviceType == SOURCE_BT)
   //{
   //   _iBrowse = BrowseFactory::getBrowser(LIST_ID_BROWSER_FOLDER_BROWSE );
   //}
   //else
   //{
   _iBrowse = BrowseFactory::getBrowser(LIST_ID_BROWSER_METADATA_BROWSE);
   //}
   uint32 currentPlayingListHandle = _mediaPlayerProxy->getNowPlaying().getU32ListHandle();
   _iBrowse->populateCurrentPlayingList(currentPlayingListHandle);
   return true;
}


#endif

bool MediaBrowseHandling::onCourierMessage(const ListChangedUpdMsg& oMsg)
{
   if (oMsg.GetListId() == LIST_ID_BROWSER_METADATA_BROWSE || oMsg.GetListId() == LIST_ID_BROWSER_MAIN)
   {
      _windowStartIndex = oMsg.GetStartIndex();
   }
#ifdef BT_BROWSE_SUPPORT
   handleBTBrowseIdleState();
#endif
   ETG_TRACE_USR4(("updateStartIndex is called %d listid:%d", _windowStartIndex, oMsg.GetListId()));
#ifdef PHOTOPLAYER_SUPPORT
   if (oMsg.GetListId() == LIST_ID_COVER_PLAYER_IMAGE)
   {
      return false;
   }
#endif
   return true;
}


uint32 MediaBrowseHandling::updateStartIndex(uint32 startIndex)
{
   ETG_TRACE_USR4(("updateStartIndex is called %d", startIndex));
   ETG_TRACE_USR4(("_currentStartIndex is called %d", _currentStartIndex));

   if (_currentStartIndex > 0)
   {
      startIndex -= _currentStartIndex;
   }

   return startIndex;
}


bool MediaBrowseHandling::onCourierMessage(const Courier::StartupMsg& /*oMsg*/)
{
   FocusChangedUpdMsg::Subscribe(Courier::ComponentType::Model, Courier::ComponentType::View);

   return false;  // to further process this message
}


bool MediaBrowseHandling::onCourierMessage(const FocusChangedUpdMsg& oMsg)
{
   isMessageProcessed = false;
   ListProviderEventInfo info;
   if (ListProviderEventInfo::GetItemIdentifierInfo(oMsg.GetWidget(), info))
   {
      if (isMediaListId(info.getListId()))
      {
         uint32 listId = info.getListId();
         _focussedIndex = static_cast<int32>(info.getHdlRow());     // normaly the index
#ifdef BROWSER_FOOTER_DEFAULT_VALUES_SUPPORT
         uint32 focusedindex = _focussedIndex + 1;					// The value 1 is added because the focus index starts from index 0
         updateFooterDetails(focusedindex, listId);							//This function is called to display the footer in folder browser
#endif
         IBrowse* ibrowse = BrowseFactory::getBrowser(listId);
         ibrowse->setFocussedIndex(_focussedIndex);
         isMessageProcessed = true;
      }
#ifdef BT_BROWSE_SUPPORT
      handleBTBrowseIdleState();
#endif
   }
   return isMessageProcessed;
}


#ifdef BT_BROWSE_SUPPORT
bool MediaBrowseHandling::onCourierMessage(const BTBrowseTimerStopReqMsg& /*oMsg*/)
{
   ETG_TRACE_USR4(("BTBrowseTimerStopReqMsg: Timer Stops"));
   _btBrowseTimer.stop();
   return true;
}


void MediaBrowseHandling::handleBTBrowseIdleState()
{
   uint32 activeDeviceType = MediaProxyUtility::getInstance().getHallActiveDeviceType();
   if (activeDeviceType == SOURCE_BT)
   {
      ETG_TRACE_USR4(("MediaBrowseHandling : handleBTBrowseIdeleState Timer Starts\n"));
      _btBrowseTimer.start();
   }
}


void MediaBrowseHandling::onExpired(asf::core::Timer& timer, boost::shared_ptr<asf::core::TimerPayload> data)
{
   ETG_TRACE_USR4(("MediaBrowseHandling : Received Timer expired\n"));
   if (timer.getAct()  == _btBrowseTimer.getAct())
   {
      if (data->getReason() == asf::core::TimerPayload_Reason__Completed)
      {
         ETG_TRACE_USR4(("MediaBrowseHandling : TimerPayload_Reason__Completed\n"));
         POST_MSG((COURIER_MESSAGE_NEW(GoToBTBrowseMainScreenReqMsg)(1)));
      }
   }
}


/**
 * updateFooterDetails - This function is called whenever there is change in the focus during folder browsing and during display of folder browse.
 * @param[in] uint32 focussedindex
 * @parm[out] void
 */
void MediaBrowseHandling::updateFooterDetails(uint32 focussedIndex, uint32 listId)
{
   if (_iListFacade)
   {
      IBrowse* ibrowse = BrowseFactory::getBrowser(listId);
      ibrowse->setFooterDetails(focussedIndex);
   }
}


#endif

#ifdef VERTICAL_KEYBOARD_SEARCH_SUPPORT
/**
 *  VerticalKeyboardSeach::ButtonListItemUpdMsg - on press of list item
 *  @param [in] omsg
 *  @return tSharedPtrDataProvider
 */

bool MediaBrowseHandling::onCourierMessage(const ButtonListItemUpdMsg& oMsg)
{
   ETG_TRACE_USR4(("VerticalKeyboardSeach::ButtonListItemUpdMsg list Id %d", oMsg.GetListId()));
   IBrowse* ibrowse = NULL;
   ibrowse = BrowseFactory::getBrowser(LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST);

   ibrowse->updateVerticalListToGuiList(oMsg);
   return true;
}


/**
 *  MediaBrowseHandling::ButtonListItemUpdMsg - on press of list item
 *  @param [in] omsg
 *  @return tSharedPtrDataProvider
 */

bool MediaBrowseHandling::onCourierMessage(const VerticalKeyLanguageSwitch& oMsg)
{
   (void)oMsg;
   ETG_TRACE_USR4(("VerticalKeyboardSeach::VerticalKeyLanguageSwitch "));

   IBrowse* ibrowse = NULL;
   ibrowse = BrowseFactory::getBrowser(LIST_ID_VERTICAL_KEYBOARD_MAIN_LIST);

   if (_defaultLanguage == false)
   {
      _defaultLanguage = true;
      ibrowse->ShowVerticalKeyInfo(_defaultLanguage);
   }
   else
   {
      _defaultLanguage = false;
      ibrowse->ShowVerticalKeyInfo(_defaultLanguage);
   }
   return true;
}


/**
 *  MediaBrowseHandling::ButtonListItemUpdMsg - on press of list item
 *  @param [in] omsg
 *  @return bool
 */

bool MediaBrowseHandling::onCourierMessage(const VKBSearchListUpMsg& oMsg)
{
   (void)oMsg;
   ETG_TRACE_USR4(("MediaBrowseHandling::VKBSearchListUpMsg courier msg update: Scoll up"));
   POST_MSG((COURIER_MESSAGE_NEW(ListChangeMsg)(LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST, ListChangeUp, 1)));
   return true;
}


/**
 *  MediaBrowseHandling::VKBSearchListDownMsg - on press of list item
 *  @param [in] omsg
 *  @return bool
 */

bool MediaBrowseHandling::onCourierMessage(const VKBSearchListDownMsg& oMsg)
{
   (void)oMsg;
   ETG_TRACE_USR4(("MediaBrowseHandling::VKBSearchListDownMsg courier msg update: Scoll up"));
   POST_MSG((COURIER_MESSAGE_NEW(ListChangeMsg)(LIST_ID_VERTICAL_KEYBOARD_EXPANDED_LIST, ListChangeDown, 1)));
   return true;
}


#endif


/**
 *  MediaBrowseHandling::PlayAllSongsReqMsg - on press of Playallsongs button in folder browse
  *  @return bool
 */
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R
bool MediaBrowseHandling::onCourierMessage(const PlayAllSongsReqMsg& /*oMsg*/)
{
   if (_iListFacade)
   {
      ETG_TRACE_USR4(("MediaBrowseHandling::PlayAllSongsReqMsg"));
      _iListFacade->requestPlayAllsongs();
   }
   return true ;
}


//bool MediaBrowseHandling::onCourierMessage(const InitialisePictureBrowserReqMsg& /*oMsg*/)
//{
//	if(_iListFacade)
//	{
//	_iListFacade->getImageList();
//	}
//	return true;
//}

#endif

#ifdef ABCSEARCH_SUPPORT
/**
 *  MediaBrowseHandling::SpellerKeyPressed - on press of speller item
 *  @param [in] omsg
 *  @return bool
 */
bool MediaBrowseHandling::onCourierMessage(const SpellerKeyPressed& oMsg)
{
   ETG_TRACE_USR4(("MediaBrowseHandling::onCourierMessage - oMsg.GetKeyChar() %s", oMsg.GetKeyChar().GetCString()));

   POST_MSG((COURIER_MESSAGE_NEW(ExitABCKeyboardLayoutReqMsg)()));
   if (_iListFacade)
   {
      std::string oSearchChar(oMsg.GetKeyChar().GetCString());
      std::map<std::string, trSearchKeyboard_ListItem> mABCkeyboardList = _iListFacade->getABCSearchKeyboardListItemMap();
      std::map<std::string, trSearchKeyboard_ListItem>::iterator itr;
      if ((itr = mABCkeyboardList.find(oSearchChar)) != mABCkeyboardList.end())
      {
         std::string oQSChar0_9("");
         char unicodeQS0_9Chr[4] = { 0xef, 0xa1, 0xa0, 0x00 };
         oQSChar0_9.assign(unicodeQS0_9Chr);
         if (oQSChar0_9 == oSearchChar)
         {
            oSearchChar.assign("!"); // send char ! as media player sorting table first char is space followed by symbol and number
         }
//      // search "#" for "123" from media player
//      if ("123" == oSearchChar)
//      {
//         oSearchChar.assign("#");
//      }
         _iListFacade->vRequestQSMediaplayerFilelist(oSearchChar, _windowStartIndex);
         ETG_TRACE_USR4(("MediaBrowseHandling::onCourierMessage oSearchChar :%s", oSearchChar.c_str()));
         ETG_TRACE_USR4(("_windowStartIndex :%d itr->second.u32LetterStartIndex :%d", _windowStartIndex, itr->second.u32LetterStartIndex));
      }
   }

   return true ;
}


/**
 *  MediaBrowseHandling::ABCSearchBtnPressUpdMsg - on press of ABC Search button
 *  @param [in] omsg
 *  @return bool
 */
bool MediaBrowseHandling::onCourierMessage(const ABCSearchBtnPressUpdMsg& /*oMsg*/)
{
   if (_iListFacade)
   {
      std::map<std::string, trSearchKeyboard_ListItem> mABCkeyboardList = _iListFacade->getABCSearchKeyboardListItemMap();
      std::string validChars = "";
      for (std::map<std::string, trSearchKeyboard_ListItem>::iterator it = mABCkeyboardList.begin(); it != mABCkeyboardList.end(); ++it)
      {
         validChars.append(it->first);
      }
      MediaDatabinding::getInstance().updateABCKeyboardValidChars(validChars);

      ETG_TRACE_USR4(("MediaBrowseHandling::onCourierMessage - ABCSearchBtnPressUpdMsg - validChars = %s", validChars.c_str()));
   }

   return true ;
}


#endif
}//end of namespace Core
}//end of namespace App
