/**
 *  @file   MediaSourceHandling.h
 *  @author ECV - kng3kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */
#ifndef MediaSourceHandling_h
#define MediaSourceHandling_h

#include "AppBase/ServiceAvailableIF.h"
#include "bosch/cm/ai/hmi/masteraudioservice/AudioSourceChangeProxy.h"
#include "bosch/cm/ai/hmi/masteraudioservice/SoundPropertiesProxy.h"
#include "org/genivi/audiomanager/CommandInterfaceProxy.h"
#include "Core/ContextSwitch/ContextSwitchHomeScreen.h"
#include "MediaDatabinding.h"
#include "Core/DeviceConnection/DeviceConnectionOserver.h"
#include "App/Core/HmiDataHandlerExt/HmiDataHandlerExt.h"
//#define rnaivi 1

//const used for volume
const uint16 ABSOLUTE_VALUE_MODE = 3;
const int16 DEFAULT_VOLUME_ONE = 1;
const int16 BT_SUBSOURCE_ID = -1;

#define AUDIOMANAGER_COMMANDINTERFACE org::genivi::audiomanager::CommandInterface
#define MASTERAUDIOSERVICE_INTERFACE ::bosch::cm::ai::hmi::masteraudioservice

namespace App {
namespace Core {

class IBTSettings;
class IPlayScreen;
class IDeviceConnection;

class MediaSourceHandling
   : public hmibase::ServiceAvailableIF
   , public StartupSync::PropertyRegistrationIF
   , public IDeviceConnectionOserver
   , public MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::ActivateSourceCallbackIF
   , public MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::ActiveSourceCallbackIF
   , public MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::DeactivateSourceCallbackIF
   , public MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::SourceListChangedCallbackIF
   , public MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::GetSourceListCallbackIF
   , public MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::ActiveSourceListCallbackIF
   , public AUDIOMANAGER_COMMANDINTERFACE::SetMainSinkSoundPropertyCallbackIF
   , public AUDIOMANAGER_COMMANDINTERFACE::MainSinkSoundPropertyChangedCallbackIF
   , public AUDIOMANAGER_COMMANDINTERFACE::SetVolumeCallbackIF
   , public AUDIOMANAGER_COMMANDINTERFACE::GetListMainSinkSoundPropertiesCallbackIF
   , public ::mplay_MediaPlayer_FI::EjectOpticalDiscCallbackIF
   , public MASTERAUDIOSERVICE_INTERFACE::SoundProperties::VolumeCallbackIF
   , public MASTERAUDIOSERVICE_INTERFACE::SoundProperties::MuteStateCallbackIF
{
   public:
      virtual ~MediaSourceHandling();
      MediaSourceHandling(IBTSettings* _pBTSettings, IDeviceConnection* pDeviceConnection, IPlayScreen* pPlayScreen,
                          ::boost::shared_ptr<AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy >& pCommandInterfaceProxy, ContextSwitchHomeScreen* pHomeScreen
                          , ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy, HmiDataHandlerExt* hmiDataHandler);
      virtual void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);
      virtual void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);

      virtual void onActivateSourceError(const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::AudioSourceChangeProxy >& proxy,
                                         const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::ActivateSourceError >& error);
      virtual void onActivateSourceResponse(const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::AudioSourceChangeProxy >& proxy,
                                            const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::ActivateSourceResponse >& /*response*/);

      virtual void onDeactivateSourceError(const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::AudioSourceChangeProxy >& /*proxy*/,
                                           const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::DeactivateSourceError >& /*error*/);
      virtual void onDeactivateSourceResponse(const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::AudioSourceChangeProxy >& /*proxy*/,
                                              const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::DeactivateSourceResponse >& /*response*/);

      virtual void onSourceListChangedError(const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::AudioSourceChangeProxy >& /*proxy*/,
                                            const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::SourceListChangedError >& /*error*/);
      virtual void onSourceListChangedSignal(const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::AudioSourceChangeProxy >& /*proxy*/,
                                             const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::SourceListChangedSignal >& /*signal*/);

      virtual void onGetSourceListError(const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::AudioSourceChangeProxy >& /*proxy*/,
                                        const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::GetSourceListError >& /*error*/);
      virtual void onGetSourceListResponse(const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::AudioSourceChangeProxy >& proxy,
                                           const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::GetSourceListResponse >& response);

      virtual void onActiveSourceError(const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::AudioSourceChangeProxy >& /*proxy*/,
                                       const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::ActiveSourceError >& /*error*/);
      virtual void onActiveSourceUpdate(const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::AudioSourceChangeProxy >& /*proxy*/,
                                        const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::ActiveSourceUpdate >& update);
      void onSetMainSinkSoundPropertyError(const ::boost::shared_ptr< AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy >& /*proxy*/,
                                           const ::boost::shared_ptr< AUDIOMANAGER_COMMANDINTERFACE::SetMainSinkSoundPropertyError >& /*error*/);
      void onSetMainSinkSoundPropertyResponse(const ::boost::shared_ptr< AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy >& /*proxy*/,
                                              const ::boost::shared_ptr< AUDIOMANAGER_COMMANDINTERFACE::SetMainSinkSoundPropertyResponse >& /*response*/);
      virtual void onMainSinkSoundPropertyChangedError(const ::boost::shared_ptr<AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy >& /*proxy*/,
            const ::boost::shared_ptr<AUDIOMANAGER_COMMANDINTERFACE::MainSinkSoundPropertyChangedError >& /*error*/);
      virtual void onMainSinkSoundPropertyChangedSignal(const ::boost::shared_ptr<AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy >& /*proxy*/,
            const ::boost::shared_ptr<AUDIOMANAGER_COMMANDINTERFACE::MainSinkSoundPropertyChangedSignal >& signal);
      virtual void onGetListMainSinkSoundPropertiesError(const ::boost::shared_ptr<AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy >& /*proxy*/,
            const ::boost::shared_ptr<AUDIOMANAGER_COMMANDINTERFACE::GetListMainSinkSoundPropertiesError >& /*error*/);

      virtual void onGetListMainSinkSoundPropertiesResponse(const ::boost::shared_ptr<AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy >& /*proxy*/,
            const ::boost::shared_ptr<AUDIOMANAGER_COMMANDINTERFACE::GetListMainSinkSoundPropertiesResponse >& response);
      virtual void onEjectOpticalDiscError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::EjectOpticalDiscError >& /*error*/);
      virtual void onEjectOpticalDiscResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::EjectOpticalDiscResult >& result);

      virtual void onSetVolumeError(const ::boost::shared_ptr< AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy >& /*proxy*/,
                                    const ::boost::shared_ptr< AUDIOMANAGER_COMMANDINTERFACE::SetVolumeError >& /*error*/);
      virtual void onSetVolumeResponse(const ::boost::shared_ptr< AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy >&/*proxy*/,
                                       const ::boost::shared_ptr< AUDIOMANAGER_COMMANDINTERFACE::SetVolumeResponse >& /*response*/);

      virtual void onVolumeError(const ::boost::shared_ptr<MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy >& /*proxy*/, const ::boost::shared_ptr<MASTERAUDIOSERVICE_INTERFACE::SoundProperties:: VolumeError >& /*error*/);

      virtual void onVolumeUpdate(const ::boost::shared_ptr<MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy >& /*proxy*/, const ::boost::shared_ptr<MASTERAUDIOSERVICE_INTERFACE::SoundProperties:: VolumeUpdate >& update);

      virtual void onMuteStateError(const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy >& /*proxy*/,
                                    const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::SoundProperties::MuteStateError >& /*error*/);
      virtual void onMuteStateUpdate(const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy >& /*proxy*/,
                                     const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::SoundProperties::MuteStateUpdate >& update);

      virtual void onActiveSourceListError(const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::AudioSourceChangeProxy >& proxy,
                                           const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::ActiveSourceListError >& error);
      virtual void onActiveSourceListSignal(const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::AudioSourceChangeProxy >& proxy,
                                            const ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::ActiveSourceListSignal >& signal);

      virtual void requestSourceActivation(int32 SrcID, int32 SubSrcID);
      void updateActiveMediaSource(int32 SrcId, int32 SubSrcId);
      void updateVolumeButtonStatus();
      int32 getMediaCurrentActiveSource();
      int32 getCurrentAudioSource();
      virtual void deviceConnectionStatus();
      static void tracecmd_CDEject();

      bool onCourierMessage(const SourceToggleReqMsg& /*oMsg*/);
      bool onCourierMessage(const ExitBrowseOnHK_AUXPressMsg& /*oMsg*/);
      bool onCourierMessage(const PlayFromVolumeZeroStateMsg& /*oMsg*/);
      bool onCourierMessage(const ContextSwitchInReqMsg& msg);
      bool onCourierMessage(const ActiveRenderedView& msg);
      bool onCourierMessage(const onMediaSpiStateActiveUpdMsg& msg);
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      bool onCourierMessage(const ChangeVolumeReqMsg& oMsg);
      bool onCourierMessage(const onHK_Eject_PressMsg& /*oMsg*/);
      bool onCourierMessage(const onMediaAudioGadget_PressMsg& /*oMsg*/);
#endif


      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      ON_COURIER_MESSAGE(SourceToggleReqMsg)
      ON_COURIER_MESSAGE(ExitBrowseOnHK_AUXPressMsg)
      ON_COURIER_MESSAGE(PlayFromVolumeZeroStateMsg)
      ON_COURIER_MESSAGE(ContextSwitchInReqMsg)
      ON_COURIER_MESSAGE(ActiveRenderedView)
      ON_COURIER_MESSAGE(onMediaSpiStateActiveUpdMsg)
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
      ON_COURIER_MESSAGE(ChangeVolumeReqMsg)
      ON_COURIER_MESSAGE(onHK_Eject_PressMsg)
      ON_COURIER_MESSAGE(onMediaAudioGadget_PressMsg)
#endif
      COURIER_MSG_MAP_END();

   private:
      void updateSourceList(int32 SrcID, int32 SubSrcID, int32 Availability, int32 AvailabilityReason);
      int32 getMediaSourceType(int32 SrcID, int32 SubSrcID);
      int32 getNextPossibleSource(int32);
      void deactivateLoadingPopup();
      void clearAndRequestNewScreenData(int32 currentSource);
      bool isMediaSource(int32 source);
      bool isTemporarySource(int32 source);
      bool isAnyBTSourceAvailable();
      void updateSubSorceId(int32 sourceId, int32 subSourceId);
      void ttfisCmdCDEject();
      int32  getSourceId(int32 lDevID);
      int32  getSubSourceID(int32 SourceID);
      void updateWaitingContextView();

      void sendViewChange();
      bool isMediaPlayerSourceActive();
      void transitToMainScreen(uint8, bool);
      void updateCurrentActiveMediaSource(int32, int32);
      void reqMediaContextOnUtilityActive(int32 subSourceId);
      int32 getAvailibilityReson(int32 subSourceId);
      void setActiveMediaDevice(uint32 deviceTag);
      bool isBTDeviceconnected();

      ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::AudioSourceChange::AudioSourceChangeProxy > _audioSourceChangeProxy;
      ::boost::shared_ptr< MASTERAUDIOSERVICE_INTERFACE::SoundProperties::SoundPropertiesProxy > _soundPropertiesProxy;
      ::boost::shared_ptr < AUDIOMANAGER_COMMANDINTERFACE::CommandInterfaceProxy > _commandIFProxy;
      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& _mediaPlayerProxy;

      IBTSettings& _iBTSettings;
      IDeviceConnection& _iDeviceConnection;
      IPlayScreen& _iPlayScreen;
      int32 _currentActiveMediaSource;
      int32 _currentActiveMediaSUBSourceID;
      int32 _currentAudioSource;
      int8 _extendedUSBSupport;
      uint16 sinkid;
      std::string _viewname;
      std::string _ContextSwitchWaitingView;
      int32 _SrcChgContextid;
      int32 _SrcChgSwitchid;
      int32 _prevContextid;
      int32 _prevSwitchid;
      ::std::vector< SourceDetailInfo > _sourcelist;
      ContextSwitchHomeScreen& _DeviceHomeScreen;
      static  MediaSourceHandling* _mediaSourceHandling;
      bool _isSpiAppStateActive;
      HmiDataHandlerExt* _hmiDataHandler;
};


} // namespace Core
} // namespace App
#endif
