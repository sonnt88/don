/**
 *  @file   SpiRestoreFactorySetting.h
 *  @author ECV - alc7kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */


#ifndef _SPIRESTORE_FACTORY_SETTING_H_
#define _SPIRESTORE_FACTORY_SETTING_H_


#include "Spi_defines.h"
#include "SpiSettingListHandler.h"
#include "AppBase/ServiceAvailableIF.h"
#include "Common/DefSetServiceBase/DefSetServiceBase.h"

//forward declaration of class SpiMediaDataPoolConfig
class SpiMediaDataPoolConfig;

namespace App
{
namespace Core
{

class SpiRestoreFactorySetting
   : public hmibase::ServiceAvailableIF,
  public StartupSync::PropertyRegistrationIF,
  public iDefSetServiceBase
{
   public :
      SpiRestoreFactorySetting(SpiSettingListHandler* spiSettingListHandler);
      virtual ~SpiRestoreFactorySetting();

      void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, \
                              const asf::core::ServiceStateChange& stateChange);
      void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, \
                                const asf::core::ServiceStateChange& stateChange);


      virtual void onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, \
                               const asf::core::ServiceStateChange& stateChange);

      virtual void onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, \
                                 const asf::core::ServiceStateChange& stateChange);

      virtual void reqPrepareResponse();
      virtual void reqExecuteResponse();
      virtual void reqFinalizeResponse();


      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_CASE_DUMMY_ENTRY()
      COURIER_MSG_MAP_END();
   private :
      void resetHmiDataPool();
      SpiSettingListHandler* _mpSpiSettingListHandler;
      DefSetServiceBase* _mdefSetServiceBase;
      SpiMediaDataPoolConfig* _mDpHandler;

};
} //namespace Core
} //namespace app

#endif  /* _SPISETTING_LISTHANDLER_H_ */
