
/**
 *  @file   CoverFlowBrowseHandler.h
 *  @author ECV - mth4cob
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#ifndef COVERFLOWHANDLER_H_
#define COVERFLOWHANDLER_H_


#include "mplay_MediaPlayer_FIProxy.h"
#include "Common/ListHandler/ListRegistry.h"
#include "AppHmi_MediaStateMachine.h"
#include "CgiExtensions/ListDataProviderDistributor.h"


namespace App {
namespace Core {

class CoverFlowHandler: public ListImplementation
   , public ::mplay_MediaPlayer_FI::RequestMediaPlayerIndexedListSliceCallbackIF
   , public ::mplay_MediaPlayer_FI::CreateMediaPlayerIndexedListCallbackIF
{
   public:
      CoverFlowHandler(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy);
      virtual ~CoverFlowHandler();
      tSharedPtrDataProvider getListDataProvider(const ListDataInfo& _ListInfo);

      virtual void onRequestMediaPlayerIndexedListSliceError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestMediaPlayerIndexedListSliceError >& /*error*/);
      virtual void onRequestMediaPlayerIndexedListSliceResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestMediaPlayerIndexedListSliceResult >& result);
      virtual void onCreateMediaPlayerIndexedListError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerIndexedListError >& error);
      virtual void onCreateMediaPlayerIndexedListResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::CreateMediaPlayerIndexedListResult >& /*result*/);

      virtual bool onCourierMessage(const ListDateProviderReqMsg& oMsg);
      //virtual bool onCourierMessage(const ListChangedUpdMsg& oMsg);
      bool onCourierMessage(const UpdateCoverFlowlistMsg& oMsg);
      bool onCourierMessage(const ButtonListItemUpdMsg& oMsg);

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      ON_COURIER_MESSAGE(ListDateProviderReqMsg)
      //ON_COURIER_MESSAGE(ListChangedUpdMsg)
      ON_COURIER_MESSAGE(UpdateCoverFlowlistMsg)
      ON_COURIER_MESSAGE(ButtonListItemUpdMsg)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_MAP_DELEGATE_END()

      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& _mediaPlayerProxy;
      uint32 _listHandle;

   private :
      void updateCoverFlowList(const ::MPlay_fi_types::T_MPlayMediaObjects&);
};


}
}


#endif /* COVERFLOWBROWSEHANDLER_H_ */
