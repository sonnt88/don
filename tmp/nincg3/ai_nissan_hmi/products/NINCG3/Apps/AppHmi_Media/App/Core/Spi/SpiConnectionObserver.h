/**
 * @file <SpiConnectionObserver.h>
 * @author <ECV1> <A-IVI>
 * @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
 * @addtogroup <AppHmi_Media>
 */

#ifndef SpiConnectionObserver_H
#define SpiConnectionObserver_H

#include "midw_smartphoneint_fiProxy.h"


namespace App {
namespace Core {

class SpiConnectionObserver
{
   public:
      virtual ~SpiConnectionObserver();
      SpiConnectionObserver();
      virtual void handleSelectDeviceResult(::midw_smartphoneint_fi_types::T_e8_ResponseCode) = 0;
};


class SpiConnectionSubject
{
   public:

      virtual ~SpiConnectionSubject();
      SpiConnectionSubject();
      void handleSelectDeviceResult(::midw_smartphoneint_fi_types::T_e8_ResponseCode);
      void registerObserver(SpiConnectionObserver* poObserver);

   private:
      void notifyObservers(::midw_smartphoneint_fi_types::T_e8_ResponseCode);
      std::vector<SpiConnectionObserver*> _observers;
};


}
}


#endif
