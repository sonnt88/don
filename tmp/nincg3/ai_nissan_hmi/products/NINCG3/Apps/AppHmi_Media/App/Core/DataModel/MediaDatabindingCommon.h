/**
 *  @file   MediaDatabinding.h
 *  @author ECV - RN_A_IVI
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#ifndef MEDIADATABINDINGCOMMON_H_
#define MEDIADATABINDINGCOMMON_H_

#include "AppHmi_MediaStateMachine.h"
#include "Core/MediaDefines.h"
//#include "Widgets/2D/Image/ImageWidget2DBackEndInterface.h"
#include "AppHmi_MasterBase/AudioInterface/AudioDefines.h"
#include  "CgiExtensions/DataBindingItem.hpp"
#define ARRAY_SIZE         15
namespace App {
namespace Core {

/**
 * <To update HMI properties>
 */

//To store availability which is obtained from MediaSourceList from HMI_Master
struct SourceDetailInfo
{
   int32 sourceType; //The type of source as Mediaplayer type
   int32 availability ; // if source is avilble from HMI master
   int32 availabilityReason; //If unavailable then the reason for non playability
   int32 sourceID;
   int32 subSourceID;   // Device ID for activation, in case of CDDA ID is 0
};


class MediaDatabindingCommon
{
   public:
      virtual ~MediaDatabindingCommon();

      void updatePlaytimeInfo(std::string, std::string, uint32, uint32);
      void updateAlbumArt(const ::std::vector< uint8 > u8ImageData, uint32 u32ImageDataSize , bool);
      void updateAlbumArtDefault(bool);
      void updateAlbumArtDataBinding(const void* data, uint32 size, bool);
      void setCurrentMediaSource(uint32 CurrMediaSrc);
      void clearMediaMetaDataInfo();
#ifdef QUICKSEARCH_SUPPORT
      void updateQuickSearchInfo(std::string);
#endif
      void updateRepeatRandomStatus(Candera::String& , bool, bool, Candera::String& , Candera::String&);
      void clearPlayModeStatus();
      void updateBrowseListStartIndex(uint32 startIdx);
      void updateScrollBarSwitchIndex(uint32 sIndex);
      void updateBTDeviceInfo(std::string);
      void updateTAStatusInfo(bool);
      void updateBTIconState(Courier::Bool, Courier::Bool);
      void updateIndexingInfo(uint8, uint8);
      void updateFolderPath(std::string);
      void updateCurrentMediaSource(uint32 CurrMediaSrc);
      void updateConnectedDevice(::std::vector< SourceDetailInfo > DeviceType);
      void updateActiveDeviceTag(uint32 DeviceTag);
      void updateAudioSource(uint32 SourceType);
      void updateActiveListType(uint32 ListType);
      void updateTotalNumberOfFilesAndFolders(uint32 TotalNumFiles, uint32 TotalNumFolders);
      void updateActivePlayingItemTag(uint32 Tag);
      void updateActiveApplicationStatus(Courier::Bool);
      void updateAuxScreenInfo(std::string);
      void updateBTDeviceConnectedStatus(bool);
#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
      void updateQuickSearchAvailabeStatus(bool);
#endif
#ifdef ABCSEARCH_SUPPORT
      // For ABC Search
      bool updateABCSearchButtonVisibleStatus(bool);
      bool updateABCSearchButtonEnableStatus(bool);
      bool updateABCKeyboardValidChars(std::string);
#endif
      uint32 getActiveApplicationStatus();
      uint32 GetCurrentAudioSource();
      uint32 getActiveDeviceTag();
      bool getVolumeAvailablity();
      //functions used to support TML
      void showTrackInfo();
      void showPlayModeStatus();
      void showCurrentListTypeOfMetadataBrowse();
      void showIndexingInfo();
      uint32 getCurrentActiveMediaDevice();
      std::string getCurrentFolderPath();
      void showConnectedDevices();
      void showAudioSource();
      void showBTDeviceName();
      void showActiveDeviceTag();
      void showTotalNumberOfFilesAndFolders();
      void showActivePlayingItemTag();
      void showAlbumArtInfo();
      void showBTDeviceConnectedStatus();
      void updateIsMediaActive(bool);
      void updateListSizeInMetaDataBrowse(float, float);
      void updatePlayAllSongButtonVisibility(bool);
      void updateMuteStatus(bool);
      //end of functions used to support TML

      //static MediaDatabindingCommon& getInstance();

      virtual void updateAuxLevel(int16) {}
      virtual void updateFolderOrCurrentPlayingListBtnText(Candera::String) {}
      virtual void updateBTMenuSupportStatus(Courier::Bool) {}
      virtual void updateBTRepeatRandomStatus(Courier::Bool, Courier::Bool) {}
      virtual void updateBTFFNextTrackSupportStatus(Courier::Bool, Courier::Bool) {}
      virtual void updateBTFRPrevTrackSupportStatus(Courier::Bool, Courier::Bool) {}
      virtual void updateBTPlayTimeSupportStatus(Courier::Bool) {}
      virtual void updateAlbumArtTogglestatus(Courier::Bool) {}
      virtual void updateMediaMetadataInfo(std::string, std::string, std::string);
      virtual void updateDefaultAlbumArtVisibleStatus(bool) {}
      virtual void updateCurrentPlayingListBtnStatus(bool) {}
      virtual void updateVolumeAvailability(int16) {}
      virtual void updateFooterInFolderBrowse(std::string) {}
      virtual void SetApplicationModeValue(uint8) {}
      virtual void updateTrackNumber(uint32, uint32) {}
      virtual void updateBTDisconnectionStatusOverUSB(bool, bool) {}
      virtual void updateBTMetadataSupportStatus(Courier::Bool) {}
      virtual void updateGadgetScreenButtonsVisibilty(uint32) {}
      virtual void clearFolderorCurrentPlayingListBtnText() {}
      virtual void updateBTScreenOnCallHandsetModeActive(Courier::Bool, Courier::Bool) {}
      //Functions to check SPI Availability
      void setSpiDIPOSourceAvailability(bool status);
      bool getSpiDIPOSourceAvailability();
      void setSpiAAPSourceAvailability(bool status);
      bool getSpiAAPSourceAvailability();
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_VIEW_COMP)
      COURIER_DUMMY_CASE(0)
      COURIER_MSG_MAP_END()

   protected:
      MediaDatabindingCommon();
      MediaDatabindingCommon(const MediaDatabindingCommon&);

      DataBindingItem<AlbumArtDataDataBindingSource> _albumArtData;
      DataBindingItem<MediaVolumeLevelInfoDataBindingSource> _volumeInfo;
      DataBindingItem<BTHandSetModeActiveStatusDataBindingSource>_btHandsetModeActiveStatus;
      DataBindingItem<BTScreenUpdateWhenConnectedWithUSBDataBindingSource> _BTDisconnectionUpdateOverUSB;
      DataBindingItem<BTMetaDataFunctionalityStatusDataBindingSource> _BTMetadataSupport;
      uint32 _audioSource;
      bool _isVolumeAvailable;
      std::string _currentTrackInfo;
      //static MediaDatabindingCommon* _theInstance;

   private:

      DataBindingItem<MediaMetaDataDataBindingSource> _mediaMetaData;
      DataBindingItem<BTAudioDeviceDataDataBindingSource>_btDeviceData;
      DataBindingItem<USBBrowseListScreenDataBindingSource> _usbBrowseListScreen;
      DataBindingItem<UpdateMuteStatusDataBindingSource> _muteStatus;
#ifdef QUICKSEARCH_SUPPORT
      DataBindingItem<QuickSearchDataBindingSource> _quickSearch;
#endif
      DataBindingItem<RandomRepeatStatusDataBindingSource> _randomRepeatStatus;
      DataBindingItem<TAStatusDataBindingSource> _taStatus;

      Candera::MemoryManagement::SharedPointer<Candera::Image2D> _coverImage;
      DataBindingItem<MediaAuxScreenInfoDataBindingSource> _defaultAuxInfo;
      DataBindingItem<ActiveApplicationDataBindingSource> _ActiveApplicationState;
      DataBindingItem<MediaSourceAvailabilityDataBindingSource> _MediaApplicationActiveState;
#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
      DataBindingItem<QuickSearchAvailabilityDataBindingSource> _quickSearchAvailabe;
#endif
      DataBindingItem<BrowseListSizeDataBindingSource> _browseListSize;
#ifdef ABCSEARCH_SUPPORT
      // For ABC Search
      DataBindingItem<ABCSearchButtonInfoDataBindingSource> _ABCSearchButtonInfo;
      DataBindingItem<ABCKeyboardInfoDataBindingSource> _ABCKeyboardInfo;
#endif

      std::string _totalPlayTime;
      uint32 _currentSourceType;
      // TODO 3 private variable can be removed once the common data binding is done for status line
      uint8 _indexState;
      uint8 _indexingPercent;
      std::string _currentFolderPath;
      ::std::vector< SourceDetailInfo > _connectedDevice;
      uint32 _deviceTag;
      uint32 _activeListType;
      uint32 _numOfFolders;
      uint32 _numberOfFiles;
      uint32 _activeplayingitemtag;
      uint32 _activeApplicationID;
      uint32 _sizeOfAlbumArtImage;

      bool   _btdeviceConnectedStatus;
      bool _bIsSpiDIPOActive;
      bool _bIsSpiAAPActive;
};


}
}


#endif /* MEDIADATABINDINGCOMMON_H_ */
