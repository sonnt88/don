/**
*  @file   MetaData_Test.h
*  @author RBEI/ECV-AIVI_MediaTeam
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef META_DATA_TEST_H_
#define META_DATA_TEST_H_

#include "gtest/gtest.h"
#include "Core/PlayScreen/MetaData.h"

using namespace std::tr1;
using namespace App::Core ;
/**
* Provide values for metadata testing method
*/
class MetaData_Extraction_Test :
   public testing::TestWithParam < tuple < std::string, std::string, std::string, std::string, ::MPlay_fi_types::T_e8_MPlayCategoryType,
   std::string, std::string, std::string, std::string, std::string, std::string, std::string, std::string, std::string, std::string >  >
{
   protected:

      MetaData _metaData;

   protected:
      virtual void SetUp()
      {
         _unknown = "";

         _mediaObject.setSMetaDataField1(get <0> (GetParam()));
         _mediaObject.setSMetaDataField2(get <1> (GetParam()));
         _mediaObject.setSMetaDataField3(get <2> (GetParam()));
         _mediaObject.setSMetaDataField4(get <3> (GetParam()));
         _mediaObject.setE8CategoryType(get <4> (GetParam()));


         _validGener = get <5> (GetParam());
         _validArtistName = get <6> (GetParam());
         _validAlbumName = get <7> (GetParam());
         _validSongTitle = get <8> (GetParam());
         _validComposerName = get <9> (GetParam());
         _validBookTitle = get <10> (GetParam());
         _validChapterTitle = get <11> (GetParam());
         _validPodcastName = get <12> (GetParam());
         _validPosdcastEpisode = get <13> (GetParam());
         _validPlayListName = get <14> (GetParam());

         _metaData.setUnknownString(_unknown);
      }

      virtual void TearDown() { }

      ::MPlay_fi_types::T_MPlayMediaObject _mediaObject;
      std::string _validComposerName;
      std::string _validBookTitle;
      std::string _validChapterTitle;
      std::string _validPodcastName;
      std::string _validPosdcastEpisode;
      std::string _validPlayListName;
      std::string _validGener;
      std::string _validArtistName;
      std::string _validAlbumName;
      std::string _validSongTitle;

      std::string _unknown;
};

#endif //MEDIAPLAYER_UTILS_TEST_H_

