/**
 * @file DataPoolConfig.cpp
 * @Author ECV1-alc7kor
 * @Copyright (c) 2015 Robert Bosch Car Multimedia Gmbh
 * @addtogroup AppHmi_Media_Spi
 */

#include "hall_std_if.h"

#ifdef DP_DATAPOOL_ID
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_hmi_04_if.h"
#endif

#include "SpiMediaDataPoolConfig.h"
#include "App/Core/Spi/Spi_defines.h" //@todo: remove once settings for scope2 is available.

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
//#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL
//#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
//#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
//#define ETG_I_FILE_PREFIX                 SpiMediaDataPoolConfig::

//#include "trcGenProj/Header/SpiConnectionHandling.cpp.trc.h"
#endif

// Instantiate the static class object
SpiMediaDataPoolConfig* SpiMediaDataPoolConfig::_mDpSpiMedia = NULL;


// Destructor
SpiMediaDataPoolConfig::~SpiMediaDataPoolConfig()
{
   delete _mDpSpiMedia;
   _mDpSpiMedia = NULL;
}


// Constructor
SpiMediaDataPoolConfig::SpiMediaDataPoolConfig()
{
   DP_vCreateDatapool();
}


// Singleton class, hence function to get instance
SpiMediaDataPoolConfig* SpiMediaDataPoolConfig::getInstance()
{
   if (_mDpSpiMedia == NULL)
   {
      createInstance();
   }

   return _mDpSpiMedia;
}


void SpiMediaDataPoolConfig::createInstance()
{
   _mDpSpiMedia = new SpiMediaDataPoolConfig();
}


void SpiMediaDataPoolConfig::deleteInstance()
{
   delete _mDpSpiMedia;
   _mDpSpiMedia = NULL;
}


// Auto Lunch Mode getter/setter
void SpiMediaDataPoolConfig::setDpSpiAutoLaunch(const uint32 autoLaunchMode)
{
   _mDpSpiAutoLaunch.vSetData(autoLaunchMode);
}


uint32 SpiMediaDataPoolConfig::getDpSpiAutoLaunch()
{
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
   return _mDpSpiAutoLaunch.tGetData();
#elif defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2)   //@todo: Once Settings are enabled in scope2, then remove this switch
   return AUTOLAUNCH_STATUS_ASK;
#else
   return _mDpSpiAutoLaunch.tGetData();
#endif
}


//!CarPlay AskOnConnect Setter/Getter handlers
void SpiMediaDataPoolConfig::setDpSpiCarPlayAskOnConnect(const uint32 CarplayAskOnConnectStatus)
{
   _mDpSpiCarPlayAskOnConnect.vSetData(CarplayAskOnConnectStatus);
}


uint32 SpiMediaDataPoolConfig::getDpSpiCarPlayAskOnConnect()
{
   return _mDpSpiCarPlayAskOnConnect.tGetData();
}


//!CarPlay AutoLaunch Setter/Getter handlers
void SpiMediaDataPoolConfig::setDpSpiCarPlayAutoLaunch(const uint32 CarplayAutoLaunchStatus)
{
   _mDpSpiCarPlayAutoLaunch.vSetData(CarplayAutoLaunchStatus);
}


uint32 SpiMediaDataPoolConfig::getDpSpiCarPlayAutoLaunch()
{
   return _mDpSpiCarPlayAutoLaunch.tGetData();
}


//!Android Auto Automatic Launch Setter/Getter handlers
void SpiMediaDataPoolConfig::setDpAndroidAutoLaunch(const uint32 AndroidAutoLaunchStatus)
{
   _mDpAndroidAutoLaunchStatus.vSetData(AndroidAutoLaunchStatus);
}


uint32 SpiMediaDataPoolConfig::getDpAndroidAutoLaunch()
{
   return _mDpAndroidAutoLaunchStatus.tGetData();
}


//!Android Auto AskOnConnect Setter/Getter handlers
void SpiMediaDataPoolConfig::setDpAndroidAutoAskOnConnect(const uint32 AndroidAutoAskOnConnectStatus)
{
   _mDpAndroidAutoAskOnConnect.vSetData(AndroidAutoAskOnConnectStatus);
}


uint32 SpiMediaDataPoolConfig::getDpAndroidAutoAskOnConnect()
{
   return _mDpAndroidAutoAskOnConnect.tGetData();
}
