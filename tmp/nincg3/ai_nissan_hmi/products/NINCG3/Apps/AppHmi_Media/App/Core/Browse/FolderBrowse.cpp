/**
*  @file   FolderBrowse.cpp
*  @author ECV - vma6cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#include "hall_std_if.h"
#include "FolderBrowse.h"
#include "Core/Utils/MediaUtils.h"
#include "Core/LanguageDefines.h"
#include "Core/Utils/MediaProxyUtility.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL_BROWSE
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::FolderBrowse::
#include "trcGenProj/Header/FolderBrowse.cpp.trc.h"
#endif

namespace App {
namespace Core {

static const char* const DATA_CONTEXT_LIST_CONTENT_EMPTY_BROWSER_ITEM    = "BrowserListText_Empty";


FolderBrowse::FolderBrowse() : _folderType(::MPlay_fi_types::T_e8_MPlayFileType__e8FT_FOLDER)
{
   ETG_I_REGISTER_FILE();

   _folderListBrowsingHistory.clear();
}


/**
 *  FolderBrowse::getDataProvider - Gets the ListDataProvider
 *  @param [in] requestedlistInfo - structure containing the requested list information
 *  @param [in] activeDeviceType - current active device type
 *  @return tSharedPtrDataProvider
 */
tSharedPtrDataProvider FolderBrowse::getDataProvider(RequestedListInfo& requestedlistInfo, uint32 /*activeDeviceType*/)
{
   requestListData(requestedlistInfo);
   return tSharedPtrDataProvider();
}


/**
 *  FolderBrowse::populateNextListOrPlayItem - Function to populate the next list or play based on the selected item
 *  @param [in] currentListId - current list Id from which the user selects an Item
 *  @param [in] selectedItem - selected Item
 *  @param [in] activeDeviceType - current active device type
 *  @return none
 */
void FolderBrowse::populateNextListOrPlayItem(uint32 /*currentListId*/, uint32 selectedItem, uint32 /*activeDeviceType*/, uint32 /*activeDeviceTag*/)
{
   ETG_TRACE_USR4(("FolderBrowse::populateNextListOrPlayItem"));

   if ((_iListFacade) && (!_iListFacade->isListWaitingToComplete()))
   {
      //initListStateMachine();
//      MediaDatabinding::getInstance().updateBrowseListStartIndex(0);
      _folderType = _folderListInfo._fileListItems[selectedItem].getE8FileType();
      _iListFacade->requestPlayOrSubFolderList(selectedItem);
   }

   ETG_TRACE_USR4(("FolderBrowse::populateNextListOrPlayItem %d", _folderType));
}


/**
 * updateListData - function called when mediaplayer get the file list from corresponding path .
 *
 * @param[in] _FolderListItemInfo
 * @return void
 */
void FolderBrowse::updateListData(void)
{
   ETG_TRACE_USR4(("updateFolderListData %d ", _folderListInfo._startIndex));
   updateFolderBrowsingHistory(_folderListInfo);
   uint32 QsIndex;
   Candera::UInt32 guiListIndex = 0;
   uint32 listSize = checkFolderUpVisibility() ? (_folderListInfo._totalListSize + 1) : (_folderListInfo._totalListSize);
   uint32 windowSize = checkFolderUpVisibility() ? (_folderListInfo._fileListItems.size() + 1) : (_folderListInfo._fileListItems.size());

   tSharedPtrDataProvider dataProvider = ListDataProvider::newBackEndInterface(_folderListInfo._listType + MEDIA_BASE_LIST_ID, _folderListInfo._startIndex,
                                         windowSize, listSize);  //MEDIA_BASE_LIST_ID is added to u32ListType to get the LIST id value
   tSharedPtrDataItemVector rowItems;
   //Highlight of track or folder for playing item is removed as requested from customer to same behaviour for folder and metadata browser.
   bool isHighlightSupport = false;
#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
   if (listSize > FLEX_LIST_MAX_QUICK_SEARCH)
   {
      MediaDatabinding::getInstance().updateQuickSearchAvailabeStatus(true);
   }
   else
   {
      MediaDatabinding::getInstance().updateQuickSearchAvailabeStatus(false);
   }
#endif

   if (checkFolderUpVisibility())
   {
      rowItems.clear();
      rowItems.push_back(DataItem::newIdentifierItem(_folderListInfo._listType + MEDIA_BASE_LIST_ID, guiListIndex, 0));
      rowItems.push_back(DataItem::newDataItem(FTS_FOLDER_UP, guiListIndex, "FolderBrowseIcons"));
      rowItems.push_back(DataItem::newDataItem(LANGUAGE_STRING(USB__FOLDER_BROWSE_Button_FolderUp, "Folder Up"),
                         guiListIndex, "FolderBrowserListText"));
      (*dataProvider)[guiListIndex] = DataItem::newDataItem(rowItems, guiListIndex, "Browser_Folder");
      ++guiListIndex;
   }

   for (Candera::UInt32 listSliceIndex = 0; guiListIndex < windowSize; ++guiListIndex, ++listSliceIndex)
   {
      rowItems.clear();
      rowItems.push_back(DataItem::newIdentifierItem(_folderListInfo._listType + MEDIA_BASE_LIST_ID, guiListIndex, 0));
      rowItems.push_back(DataItem::newDataItem(_folderListInfo._fileListItems[listSliceIndex].getE8FileType(), guiListIndex));
      rowItems.push_back(DataItem::newDataItem(_folderListInfo._fileListItems[listSliceIndex].getSFilename().c_str(), guiListIndex));
      rowItems.push_back(DataItem::newDataItem(isHighlightSupport, guiListIndex));
      (*dataProvider)[guiListIndex] = DataItem::newDataItem(rowItems, guiListIndex, getlistItemTemplate(guiListIndex));
   }
   if (_folderType == ::MPlay_fi_types::T_e8_MPlayFileType__e8FT_PLAYLIST)
   {
      QsIndex = 0;
      MediaDatabinding::getInstance().updateScrollBarSwitchIndex(QsIndex);
#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT)
      MediaDatabinding::getInstance().updateQuickSearchAvailabeStatus(false);
#elif defined(ABCSEARCH_SUPPORT)
      MediaDatabinding::getInstance().updateQuickSearchAvailabeStatus(true);
#endif
   }
   else
   {
      QsIndex = 1;
      MediaDatabinding::getInstance().updateScrollBarSwitchIndex(QsIndex);
   }
#ifdef QUICKSEARCH_SUPPORT
   uint32 activeDeviceType = MediaProxyUtility::getInstance().getHallActiveDeviceType();
   if (SOURCE_BT == activeDeviceType)
   {
      MediaDatabinding::getInstance().updateQuickSearchAvailabeStatus(false);
   }
#endif
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));
   //POST_MSG((COURIER_MESSAGE_NEW(ActivateFolderBrowsingMsg)(1, isCurrentFolderBrowserInRoot)));   // This message is posted to controller whether the user is in root folder
}


/**
 * updateListDataCDDA - update file name if CD track is not available .
 *
 * @param[in] _FolderListItemInfo
 * @return void
 *
 *
 *
 */
void FolderBrowse::updateListDataCDDA(void)
{
   ETG_TRACE_USR4((" updateListDataCDDA %d ", _folderListInfo._startIndex));

   Candera::UInt32 guiListIndex = 0;
   uint32 listSize = _folderListInfo._totalListSize;
   uint32 windowSize = _folderListInfo._fileListItems.size();
   std::string trackName = "";
   std::string iStr = "Track ";
   char indexInStr[MEDIA_HEADER_STRING_SIZE] = "";
   //Highlight of track or folder for playing item is removed as requested from customer to same behaviour for folder and metadata browser.
   bool isHighlightSupport = false;

   tSharedPtrDataProvider dataProvider = ListDataProvider::newBackEndInterface(_folderListInfo._listType + MEDIA_BASE_LIST_ID, _folderListInfo._startIndex,
                                         windowSize, listSize);  //MEDIA_BASE_LIST_ID is added to u32ListType to get the LIST id value
   tSharedPtrDataItemVector rowItems;

#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
   MediaDatabinding::getInstance().updateQuickSearchAvailabeStatus(false);
#endif
   for (Candera::UInt32 listSliceIndex = 0; guiListIndex < windowSize; ++guiListIndex, ++listSliceIndex)
   {
      rowItems.clear();
      MediaUtils::convertToString(indexInStr, listSliceIndex + 1);
      trackName.assign(iStr + indexInStr);
      rowItems.push_back(DataItem::newIdentifierItem(_folderListInfo._listType + MEDIA_BASE_LIST_ID, guiListIndex, 0));
      rowItems.push_back(DataItem::newDataItem(_folderListInfo._fileListItems[listSliceIndex].getE8FileType(), guiListIndex, "Browser_Folder"));
      if (_folderListInfo._fileListItems[listSliceIndex].getSFilename() == "")
      {
         rowItems.push_back(DataItem::newDataItem(trackName.c_str(), guiListIndex, "Browser_Folder"));
      }
      else
      {
         rowItems.push_back(DataItem::newDataItem(_folderListInfo._fileListItems[listSliceIndex].getSFilename().c_str(), guiListIndex, "Browser_Folder"));
      }
      rowItems.push_back(DataItem::newDataItem(isHighlightSupport, guiListIndex, "Browser_Folder"));

      (*dataProvider)[guiListIndex] = DataItem::newDataItem(rowItems, guiListIndex, "Browser_Folder");
   }
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));
   //POST_MSG((COURIER_MESSAGE_NEW(ActivateFolderBrowsingMsg)(1, isCurrentFolderBrowserInRoot)));   // This message is posted to controller whether the user is in root folder
}


/**
 * updateFolderBrowsingHistory - function is used to update the history in folder based browsing
 * @param[in] FolderBrowsingHistory& _FolderBrowsingHistoryInfo
 * @parm[out] none
 */
void FolderBrowse::updateFolderBrowsingHistory(const FolderListInfo& folderListItemInfo)
{
   bool isAvailableInHistory = false;
   std::vector<FolderListInfo>::iterator itr = _folderListBrowsingHistory.begin();
   while (itr != _folderListBrowsingHistory.end())
   {
      if (itr->_listHandle == folderListItemInfo._listHandle)
      {
         isAvailableInHistory = true;
         itr = _folderListBrowsingHistory.end();
      }
      else
      {
         ++itr;
      }
   }

   if (!isAvailableInHistory)
   {
      _folderListBrowsingHistory.push_back(folderListItemInfo);
      ETG_TRACE_USR4(("browsing history updated with windowsize:%d and list handle= %d", folderListItemInfo._fileListItems.size(), folderListItemInfo._listHandle));
   }
   else
   {
      ETG_TRACE_USR4(("browsing history already available in vector"));
   }
}


/**
 *  FolderBrowse::clearFolderBrowsingHistory - The function is used to remove and release the list handles in browsing history .
 *  The function will release/remove the list handle until it sees the currentlisthandle
 *  @param[in] currentListHandle[uint32]
 *  @return none
 */

void FolderBrowse::clearFolderBrowsingHistory(uint32 currentListHandle, std::string path)
{
   ETG_TRACE_USR4(("clearFolderBrowsingHistory is called %d %s", _folderListBrowsingHistory.size(), path.c_str()));
   std::vector<FolderListInfo>::reverse_iterator itr = _folderListBrowsingHistory.rbegin();
   FolderListInfo currentListInfo ;
   bool isCurrentListhandleAvailable = false;
   while (itr != _folderListBrowsingHistory.rend())
   {
      ETG_TRACE_USR4(("The List handle and path in the browsing history is %d %s", itr->_listHandle, itr->_folderPath.c_str()));
      if ((itr->_listHandle == currentListHandle) && (itr->_folderPath == path))           // the path is also check for inconsistency in middleware for cases like DRM
      {
         isCurrentListhandleAvailable = true;
         ETG_TRACE_USR4(("Current playing List handle is already available in the history"));
         break;
      }
      else
      {
         _iListFacade->releaseListHandle(itr->_listHandle);
         _folderListBrowsingHistory.pop_back();
         itr = _folderListBrowsingHistory.rbegin();	// the iterator reassigned to last value in the vector to avoid garbage value (this is done since pop back operation is carried )
      }
   }
   //_folderListBrowsingHistory.clear();
   if (isCurrentListhandleAvailable)
   {
      //TODO since the list handle is available in the history use the value from history and then request slice alone .The below code is unreachable till midw gives proper list handle
      ETG_TRACE_USR4(("Current playing list handle is from folde browsing and history size after pop operation %d ", _folderListBrowsingHistory.size()));
      _iListFacade->setCurrentStateMachineState(LIST_REQUEST_PROCESS_DONE);
      //_folderListBrowsingHistory.push_back(currentListInfo);
   }
   else
   {
      _iListFacade->setCurrentStateMachineState(IDLE);
      ETG_TRACE_USR4(("Current playing list handle is not from folder browsing and history size after pop operation %d ", _folderListBrowsingHistory.size()));
   }
}


/**
 *  FolderBrowse::populatePreviousList - Function to populate the previous (parent) list when user pressed Back key
 *  @return none
 */
void FolderBrowse::populatePreviousList(uint32 deviceTag)
{
   if (_iListFacade)
   {
      std::string currentFolderPath(_iListFacade->getCurrentFolderPath());
      std::string newFolderPath("");
      MediaUtils::goOneFolderUp(currentFolderPath, newFolderPath);
      _folderType = ::MPlay_fi_types::T_e8_MPlayFileType__e8FT_FOLDER;
      if (!newFolderPath.empty())
      {
         _iListFacade->setCurrentFolderPath(newFolderPath);
         populateListFromHistory(deviceTag);
      }
      MediaDatabinding::getInstance().updateFolderPath(_iListFacade->getCurrentFolderPath());
   }
}


/**
 *  FolderBrowse::populateListFromHistory - Function to populate the previous (parent) list with data from history
 *  @return none
 */
void FolderBrowse::populateListFromHistory(uint32 deviceTag)
{
   if (!_folderListBrowsingHistory.empty())
   {
      FolderListInfo historyInfo = _folderListBrowsingHistory.back();
      if (_iListFacade)
      {
         _iListFacade->releaseListHandle(historyInfo._listHandle);
      }
      _folderListBrowsingHistory.pop_back();
      requestListWithHistoryOrDefaultData(deviceTag);
//      MediaDatabinding::getInstance().updateBrowseListStartIndex(0);
   }
}


/**
 *  FolderBrowse::requestListWithHistoryOrDefaultData - Function to request for list with data from history or default data
 *  @return none
 */
void FolderBrowse::requestListWithHistoryOrDefaultData(uint32 deviceTag)
{
   RequestedListInfo requestedListInfo;
   ETG_TRACE_USR4(("The size of history is %d", _folderListBrowsingHistory.size()));
   bool isIndexingDone = MediaProxyUtility::getInstance().isIndexingDone(MediaProxyUtility::getInstance().getActiveDeviceTag());
   if (!_folderListBrowsingHistory.empty() && isIndexingDone)
   {
      FolderListInfo folderBrowsingHistoryInfo = _folderListBrowsingHistory.back();
      requestedListInfo._listType  = (LIST_ID_BROWSER_FOLDER_BROWSE % MEDIA_BASE_LIST_ID);
      requestedListInfo._startIndex = FLEX_LIST_START_INDEX; //folderBrowsingHistoryInfo._startIndex;
      requestedListInfo._bufferSize = FLEX_LIST_MAX_BUFFER_SIZE; //folderBrowsingHistoryInfo._fileListItems.size();
      // TODO: Start index and buffer size to be corrected for direct jumping
      requestedListInfo._deviceTag = deviceTag;
      ETG_TRACE_USR4(("_bufferSize size:%d", requestedListInfo._bufferSize));
      ETG_TRACE_USR4(("_start index:%d", requestedListInfo._startIndex));
      //initListStateMachine();
      _iListFacade->updateFolderListParameters(folderBrowsingHistoryInfo._listHandle, folderBrowsingHistoryInfo._totalListSize);
   }
   else
   {
      _iListFacade->setCurrentStateMachineState(IDLE);
      requestedListInfo._listType  = (LIST_ID_BROWSER_FOLDER_BROWSE % MEDIA_BASE_LIST_ID);
      requestedListInfo._startIndex = FLEX_LIST_START_INDEX;
      requestedListInfo._bufferSize = FLEX_LIST_MAX_BUFFER_SIZE;
      requestedListInfo._deviceTag = deviceTag;
   }
   _iListFacade->setFolderRequestType(FTS_FOLDER_UP);
   ETG_TRACE_USR4(("requestListWithHistoryOrDefaultData stidx:%d, buffsize:%d", requestedListInfo._startIndex, requestedListInfo._bufferSize));
   _iListFacade->requestParentFolderList(requestedListInfo);
}


/**
 * updateEmptyListData - Populates the list in a different template when the list is empty
 * @param[in] Current List Type
 * @parm[out] none
 * @return void
 */
void FolderBrowse::updateEmptyListData(uint32 _currentListType)
{
   ETG_TRACE_USR4(("Empty List Data"));

   FolderListInfo folderlistinfo;
   updateFolderBrowsingHistory(folderlistinfo);

   ListDataProviderBuilder listBuilder(_currentListType + MEDIA_BASE_LIST_ID, DATA_CONTEXT_LIST_CONTENT_EMPTY_BROWSER_ITEM);
   listBuilder.AddItem(0).AddData(LANGUAGE_STRING(METADATA_BROWSE_EmptyList, "The List is Empty"));
   tSharedPtrDataProvider dataProvider = listBuilder.CreateDataProvider();
   POST_MSG((COURIER_MESSAGE_NEW(ListDateProviderResMsg)(dataProvider)));
}


/**
 *  FolderBrowse::initializeListParameters - initializes the list parameters
 *  @return Return_Description
 */
void FolderBrowse::initializeListParameters()
{
   if (_iListFacade)
   {
      _iListFacade->setCurrentFolderPath("/");
      _iListFacade->setCurrentPlaylistPath("");
      _iListFacade->setFolderRequestType(FTS_FOLDER);
      std::vector<FolderListInfo>::iterator itr = _folderListBrowsingHistory.begin();
      while (itr != _folderListBrowsingHistory.end())
      {
         _iListFacade->releaseListHandle(itr->_listHandle);
         ++itr;
      }
   }
   _folderListBrowsingHistory.clear();
}


//#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
//
//void FolderBrowse::populateCurrentPlayingList(uint32 currentPlayingListHandle)
//{
//   if (_iListFacade)
//   {
//      _iListFacade->setCurrentStateMachineState(LIST_REQUEST_PROCESS_DONE);
//      _iListFacade->requestCurrentPlayingList(currentPlayingListHandle);
//   }
//}

//#endif


/**
 *  FolderBrowse::setWindowStartIndex - sets the window start index for folder browse
 *  @param[in] windowStartIndex
 *  @return none
 */
void FolderBrowse::setWindowStartIndex(uint32 windowStartIndex)
{
   _windowStartIndex = windowStartIndex;
}


void FolderBrowse::setFocussedIndex(int32 focussedIndex)
{
   ETG_TRACE_USR4(("FolderBrowse::setFocussedIndex:%d", focussedIndex));
   _focussedIndex = focussedIndex;
   _iListFacade->setFocussedIndexAndNextListId(focussedIndex, 0);
}


/**
 *  FolderBrowse::checkBrowsingHistory - tells the information about the browsing history in folder browsing .
 *  Based on information in history the folder path is updated and this function is used mostly for direct jumping when song is playing via meta data.
 *  @param[in] none
 *  @return none
 */
void FolderBrowse::checkBrowsingHistory()
{
   ETG_TRACE_USR4(("Check the history available %d", _folderListBrowsingHistory.empty()));
   if (_folderListBrowsingHistory.empty())
   {
      if (_iListFacade)
      {
         _iListFacade->setCurrentFolderPath("/");
         ETG_TRACE_USR4(("Setting path to root"));
         _iListFacade->setCurrentStateMachineState(IDLE);
      }
   }
   else
   {
      if (_iListFacade)
      {
         ETG_TRACE_USR4(("Folder path of the Last item in history %s", _folderListBrowsingHistory.back()._folderPath.c_str()));
         _iListFacade->setCurrentFolderPath(_folderListBrowsingHistory.back()._folderPath);
         _folderListBrowsingHistory.clear();
      }
   }
}


/**
 *  FolderBrowse::checkFolderUpVisibility - tells whether folder up is available in current window based on start index and location of the folder
 *  @param[in] none
 *  @return bool
 */
bool FolderBrowse::checkFolderUpVisibility()
{
   bool visibility = false;
#ifdef FOLDERUP_VISIBILITY_FOR_BROWSER
   if (_iListFacade)
   {
      if ((!_iListFacade->isRootFolder()) && (_folderListInfo._startIndex == 0))
      {
         visibility = true;
      }
      else
      {
         visibility = false;
      }
   }
#endif
   return visibility;
}


int32 FolderBrowse::getActiveItemIndex(int32 selectedIndex)
{
   if (checkFolderUpVisibility())
   {
      if ((selectedIndex -= 1) < 0)
      {
         selectedIndex = 0;
      }
   }

   return selectedIndex;
}


#if defined(QUICKSEARCH_SUPPORT) || defined(VERTICAL_KEYBOARD_SEARCH_SUPPORT) || defined(ABCSEARCH_SUPPORT)
uint32 FolderBrowse::getActiveQSIndex(uint32 absoluteIndex)
{
   if (!_iListFacade->isRootFolder())
   {
      absoluteIndex += 1;
   }
   return absoluteIndex;
}


#endif

/**
 *  FolderBrowse::updateFolderHistoryDuringIndexing - updates the list handle and list size in the browsing history during indexing
 *  @param[in] currentListHandle,listsize
 *  @return none
 */
void FolderBrowse::updateFolderHistoryDuringIndexing(uint32 currentListHandle, uint32 listsize)
{
   ETG_TRACE_USR4(("updateFolderHistoryDuringIndexing is called %d", _folderListBrowsingHistory.size()));
   std::vector<FolderListInfo>::reverse_iterator itr = _folderListBrowsingHistory.rbegin();
   FolderListInfo currentListInfo ;
   while (itr != _folderListBrowsingHistory.rend())
   {
      ETG_TRACE_USR4(("The List handle in the browsing history is %d", itr->_listHandle));
      if (itr->_listHandle == currentListHandle && itr->_totalListSize != listsize)
      {
         itr->_totalListSize = listsize;
         ETG_TRACE_USR4(("The list handle available in the history is updated with latest list size after indexing"));
         break;
      }
      ++itr;
   }
}


/**
 *  FolderBrowse::setFooterDetails - updates the footer text based on the focused item in the folder browser screens
 *  @param[in] focussedIndex
 *  @return none
 */
void FolderBrowse::setFooterDetails(uint32 focussedIndex)
{
   uint32 currentStartIndex = _folderListInfo._startIndex;
   uint32 currentFolderListSize = _folderListInfo._totalListSize;
   uint32 absoluteIndex = focussedIndex + currentStartIndex;

#ifdef FOLDERUP_VISIBILITY_FOR_BROWSER
   bool getFolderUpVisibility = checkFolderUpVisibility();
   bool isRootFolder = _iListFacade->isRootFolder();
   uint32 absoluteIndex = (getFolderUpVisibility || isRootFolder) ? absoluteIndex : absoluteIndex + 1;
   currentFolderListSize = isRootFolder ? currentFolderListSize : currentFolderListSize + 1;	// The value 1 is added if it is not a root folder because folder up is an extra item
#endif

   std::string footerText = MediaUtils::FooterTextForBrowser(absoluteIndex, currentFolderListSize);
   MediaDatabinding::getInstance().updateFooterInFolderBrowse(footerText.c_str());
   ETG_TRACE_USR4(("Footerdetails %d and FocussedIndex :%s", focussedIndex, footerText.c_str()));
}


const char* FolderBrowse::getlistItemTemplate(uint32 index)
{
   const char* listItemTemplate;

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R
   if (SOURCE_BT != MediaProxyUtility::getInstance().getHallActiveDeviceType())
   {
      if (index % 2)
      {
         listItemTemplate = "Browser_Folder";
      }
      else
      {
         listItemTemplate = "Browser_Folder_Even";
      }
   }
   else
   {
      if (index % 2)
      {
         listItemTemplate = "Browser_Folder_Even";
      }
      else
      {
         listItemTemplate = "Browser_Folder";
      }
   }
#else
   (void)index;
   listItemTemplate = "Browser_Folder";
#endif
   return listItemTemplate;
}


}
}
