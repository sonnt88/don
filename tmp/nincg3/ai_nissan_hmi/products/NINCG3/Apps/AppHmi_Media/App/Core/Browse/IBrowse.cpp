/**
*  @file   IBrowse.cpp
*  @author ECV - vma6cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#include "hall_std_if.h"
#include "IBrowse.h"
#include "MPlay_fi_typesConst.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL_BROWSE
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::IBrowse::
#include "trcGenProj/Header/IBrowse.cpp.trc.h"
#endif
using namespace MPlay_fi_types;

namespace App {
namespace Core {


IBrowse::IBrowse() :
   _isIndexingDone(false),
   _activeDeviceType(0),
   _iListFacade(NULL),
   _windowStartIndex(0),
   _focussedIndex(0)

{
   _sListItemText.clear();
}


IBrowse::~IBrowse()
{
   _iListFacade = NULL;
}


/**
 *  IBrowse::initListStateMachine -  Helper function to reset internal list statemachine of listInterfaceHandler
 *  @return none
 */
void IBrowse::initListStateMachine()
{
   if (_iListFacade)
   {
      _iListFacade->initStatemachine();
   }
}


/**
 *  IBrowse::requestListData -  Helper function to start processing of list in listInterfaceHandler
 *  @param [in] requestedlistInfo Parameter_Description
 *  @return Return_Description
 */
void IBrowse::requestListData(const RequestedListInfo& requestedlistInfo)
{
   if (_iListFacade)
   {
      _iListFacade->requestListData(requestedlistInfo);
   }
}


/**
 *  IBrowse::updateMetaDataListResult -  Helper function to save list result after processing  meta data browser list
 *  @param [in] listResultInfo - Structure containing the list result
 *  @return none
 */
void IBrowse::updateMetaDataListResult(const ListResultInfo& listResultInfo)
{
   _listResultInfo = listResultInfo;
}


/**
 *  IBrowse::updateFolderListResult - Helper function to save list result after processing folder browser list
 *  @param [in] folderListResult Parameter_Description
 *  @return Return_Description
 */
void IBrowse::updateFolderListResult(const FolderListInfo& folderListResult)
{
   _folderListInfo = folderListResult;
}


/**
 *  IBrowse::setIndexingDoneStatus - Helper function to save indexing completion status
 *  @param [in] isIndexingDone
 *  @return Return_Description
 */
void IBrowse::setIndexingDoneStatus(bool isIndexingDone)
{
   _isIndexingDone = isIndexingDone;
}


/**
 *  IBrowse::setListFacade - save the list interface class pointer
 *  @param [in] iListFacade
 *  @return Return_Description
 */
void IBrowse::setListFacade(IListFacade* iListFacade)
{
   _iListFacade = iListFacade;
}


/**
 *  IBrowse::setListItemText - function to set first list item
 *  @param [in] listItem
 *  @return Return_Description
 */
void IBrowse::setListItemText(std::vector<std::string> listItem)
{
   _sListItemText = listItem;
}


/**
 * getNodeIndexQuickSearch - Private helper function to get composite index,  quick search required or not in scroll bar
 * @param[in] uint32 Listtype
 * @parm[out] none
 * @return uint32 - return 1 if quick search is required otherwise 0
 */
uint32 IBrowse::getNodeIndexQuickSearch(uint32 Listtype) const
{
   uint32 retIndex = 0xFFFFFFFF;
   switch (Listtype)
   {
#ifndef ABCSEARCH_SUPPORT
      case LIST_ID_BROWSER_PLAYLIST:
#endif
      case LIST_ID_BROWSER_PLAYLIST_SONG:
      case LIST_ID_BROWSER_ALBUM_SONG:
      case LIST_ID_BROWSER_GENRE_ALBUM_SONG:
      case LIST_ID_BROWSER_GENRE_ARTIST_ALBUM_SONG:
      case LIST_ID_BROWSER_ARTIST_ALBUM_SONG:
      case LIST_ID_BROWSER_COMPOSER_ALBUM_SONG:
      case LIST_ID_BROWSER_BOOKTITLE_CHAPTER:
      case LIST_ID_BROWSER_PODCAST_EPISODE:
      {
         ETG_TRACE_USR4(("Quick Search not required for current list id %d", Listtype));
         retIndex = 0;
         break;
      }
      default:
      {
         ETG_TRACE_USR4(("Quick Search is required for current list id %d", Listtype));
         retIndex = 1;
         break;
      }
   }
   return retIndex;
}


void IBrowse::updateListDataCDDA()
{
   ETG_TRACE_USR4(("IBrowse::updateListDataCDDA"));
}


bool IBrowse::isAllAvailable(uint32 categoryType)
{
   bool isAllRequired = false;
#ifndef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R
   switch (categoryType)
   {
      case T_e8_MPlayCategoryType__e8CTY_ARTIST:
      case T_e8_MPlayCategoryType__e8CTY_ALBUM:
      case T_e8_MPlayCategoryType__e8CTY_GENRE:
      case T_e8_MPlayCategoryType__e8CTY_COMPOSER:
      {
         isAllRequired = true;
         break;
      }
      default:
      {
         isAllRequired = false;
         break;
      }
   }
#else
   (void)categoryType;
#endif
   return isAllRequired;
}


}
}
