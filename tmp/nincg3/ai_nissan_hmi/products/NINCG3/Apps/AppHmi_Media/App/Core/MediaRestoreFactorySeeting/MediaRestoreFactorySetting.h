/*
 * MediaRestoreFactorySetting.h
 *
 *  Created on: Mar 17, 2016
 *      Author: sla7cob
 */

#ifndef MEDIARESTOREFACTORYSETTING_H_
#define MEDIARESTOREFACTORYSETTING_H_

#include "Common/DefSetServiceBase/DefSetServiceBase.h"
#include "mplay_MediaPlayer_FIProxy.h"

namespace App {
namespace Core {
class MediaRestoreFactorySetting
   : public iDefSetServiceBase
   , public ::mplay_MediaPlayer_FI::ClearMediaPlayerDataCallbackIF
{
   public:
      MediaRestoreFactorySetting(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& mediaPlayerProxy);
      virtual ~MediaRestoreFactorySetting();
      virtual void onClearMediaPlayerDataError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::ClearMediaPlayerDataError >& /*error*/);
      virtual void onClearMediaPlayerDataResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::ClearMediaPlayerDataResult >& /*result*/);

      virtual void reqPrepareResponse();
      virtual void reqExecuteResponse();
      virtual void reqFinalizeResponse();

   private:
      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& _mediaPlayerProxy;
      DefSetServiceBase* _mdefSetServiceBase;
};


}
}


#endif /* MEDIARESTOREFACTORYSETTING_H_ */
