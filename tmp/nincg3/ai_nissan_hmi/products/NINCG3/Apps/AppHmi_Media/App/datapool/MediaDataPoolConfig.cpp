/**
 * @file MediaDataPoolConfig.cpp
 * @Author ECV1-put5cob
 * @Copyright (c) 2015 Robert Bosch Car Multimedia Gmbh
 * @addtogroup AppHmi_Media
 */

#include "hall_std_if.h"

#ifdef DP_DATAPOOL_ID
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_hmi_04_if.h"
#endif

#include "MediaDataPoolConfig.h"
#include "App/Core/MediaDefines.h" //@todo: remove once settings for scope2 is available.

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#endif

// Instantiate the static class object
MediaDataPoolConfig* MediaDataPoolConfig::_DpMedia = NULL;


// Constructor
MediaDataPoolConfig::MediaDataPoolConfig()
{
   DP_vCreateDatapool();
}

// Set LanguageIndex
void MediaDataPoolConfig::setLanguage(const uint8 LanguageIndexMedia)
{
   _DpLanguage.vSetData(LanguageIndexMedia);
}

// Get LanguageIndex
uint8 MediaDataPoolConfig::getLanguage()
{
   return _DpLanguage.tGetData();
}



