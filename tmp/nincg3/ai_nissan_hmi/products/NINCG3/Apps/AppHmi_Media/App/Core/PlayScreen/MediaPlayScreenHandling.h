/**
*  @file   MediaPlayScreenHandling.h
*  @author ECV - kng3kor
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef _MEDIA_PLAYSCREEN_HANDLING_H_
#define _MEDIA_PLAYSCREEN_HANDLING_H_

#include "Core/PlayScreen/IPlayScreen.h"
#include "MediaDatabinding.h"
#include "PlayMode.h"
#include "MetaData.h"
#include "asf/core/Timer.h"
#include "Common/NavigationServiceHandler/NavigationServiceHandler.h"

#include "mplay_MediaPlayer_FIProxy.h"
#include "MIDW_TUNERMASTER_FIProxy.h"


namespace App {
namespace Core {


class MediaSourceHandling;

class MediaPlayScreenHandling
   : public IPlayScreen
   , public ::mplay_MediaPlayer_FI::PlaytimeCallbackIF
   , public ::mplay_MediaPlayer_FI::NowPlayingCallbackIF
   , public ::mplay_MediaPlayer_FI::GetMediaObjectAlbumArtCallbackIF
   , public ::mplay_MediaPlayer_FI::GetMediaObjectAlbumArtInfoCallbackIF
   , public ::mplay_MediaPlayer_FI::RequestPlaybackActionCallbackIF
   , public ::mplay_MediaPlayer_FI::PlaybackModeCallbackIF
   , public ::mplay_MediaPlayer_FI::RepeatModeCallbackIF
   , public ::mplay_MediaPlayer_FI::RequestListInformationCallbackIF
   , public ::mplay_MediaPlayer_FI::PlaybackStateCallbackIF
   , public ::mplay_MediaPlayer_FI::CurrentFolderPathCallbackIF
#ifdef SEEK_SUPPORT
   , public ::mplay_MediaPlayer_FI::SeekToCallbackIF
#endif
#ifdef PHOTOPLAYER_SUPPORT
   , public ::mplay_MediaPlayer_FI::RequestSlideshowActionCallbackIF
   , public ::mplay_MediaPlayer_FI::SlideshowStateCallbackIF
   , public ::mplay_MediaPlayer_FI::SlideshowTimeCallbackIF
   , public ::mplay_MediaPlayer_FI::NowShowingCallbackIF
#endif
#ifdef TBT_NOTIFICATION_SUPPORT
   , public INavigationService
#endif
   , public ::MIDW_TUNERMASTER_FI::FID_TUNMSTR_S_ACTIVATE_ANNOUNCEMENTCallbackIF
   , public ::MIDW_TUNERMASTER_FI::FID_TUNMSTR_G_ANNO_BUTTON_STATUSCallbackIF
   , public ::asf::core::TimerCallbackIF
{
   public:
      virtual ~MediaPlayScreenHandling();
      MediaPlayScreenHandling(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy, ::boost::shared_ptr< ::MIDW_TUNERMASTER_FI::MIDW_TUNERMASTER_FIProxy >& pTunerMasterProxy);
      virtual void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);
      virtual void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);

      virtual void onPlaytimeError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::PlaytimeError >& /*error*/);
      virtual void onPlaytimeStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::PlaytimeStatus >& status);
      virtual void onNowPlayingError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::NowPlayingError >& /*error*/);
      virtual void onNowPlayingStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::NowPlayingStatus >& status);
      virtual void onGetMediaObjectAlbumArtInfoError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::GetMediaObjectAlbumArtInfoError >& /*error*/);
      virtual void onGetMediaObjectAlbumArtInfoResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::GetMediaObjectAlbumArtInfoResult >& result);
      virtual void onGetMediaObjectAlbumArtError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::GetMediaObjectAlbumArtError >& /*error*/);
      virtual void onGetMediaObjectAlbumArtResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::GetMediaObjectAlbumArtResult >& result);
      virtual void onRequestPlaybackActionResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestPlaybackActionResult >& result);
      virtual void onRequestPlaybackActionError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestPlaybackActionError >& /*error*/);
      virtual void onPlaybackModeError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::PlaybackModeError >& /*error*/);
      virtual void onPlaybackModeStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::PlaybackModeStatus >& status);
      virtual void onRepeatModeError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::RepeatModeError >& /*error*/);
      virtual void onRepeatModeStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::RepeatModeStatus >& status);
      virtual void onRequestListInformationError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestListInformationError >& /*error*/);
      virtual void onRequestListInformationResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestListInformationResult >& result);
      virtual void onPlaybackStateError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::PlaybackStateError >& /*error*/);
      virtual void onPlaybackStateStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::PlaybackStateStatus >& status);
      virtual void onCurrentFolderPathError(const ::boost::shared_ptr<mplay_MediaPlayer_FI:: Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< mplay_MediaPlayer_FI::CurrentFolderPathError >& /*error*/);
      virtual void onCurrentFolderPathStatus(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< mplay_MediaPlayer_FI::CurrentFolderPathStatus >& status);
#ifdef SEEK_SUPPORT
      virtual void onSeekToError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< mplay_MediaPlayer_FI::SeekToError >& error);
      virtual void onSeekToResult(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< mplay_MediaPlayer_FI::SeekToResult >& result);
#endif
#ifdef PHOTOPLAYER_SUPPORT
      virtual void onRequestSlideshowActionError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestSlideshowActionError >& error);
      virtual void onRequestSlideshowActionResult(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::RequestSlideshowActionResult >& result);
      virtual void onSlideshowStateError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::SlideshowStateError >& error);
      virtual void onSlideshowStateStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::SlideshowStateStatus >& status);
      virtual void onSlideshowTimeError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::SlideshowTimeError >& error);
      virtual void onSlideshowTimeStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::SlideshowTimeStatus >& status);
      virtual void onNowShowingError(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::NowShowingError >& error);
      virtual void onNowShowingStatus(const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& proxy, const ::boost::shared_ptr< ::mplay_MediaPlayer_FI::NowShowingStatus >& status);
#endif
      //TunerMaster FI for TA
      virtual void onFID_TUNMSTR_S_ACTIVATE_ANNOUNCEMENTError(const ::boost::shared_ptr< ::MIDW_TUNERMASTER_FI::MIDW_TUNERMASTER_FIProxy >& proxy, const boost::shared_ptr< ::MIDW_TUNERMASTER_FI::FID_TUNMSTR_S_ACTIVATE_ANNOUNCEMENTError >& error);
      virtual void onFID_TUNMSTR_S_ACTIVATE_ANNOUNCEMENTResult(const ::boost::shared_ptr< ::MIDW_TUNERMASTER_FI::MIDW_TUNERMASTER_FIProxy >& proxy, const boost::shared_ptr< ::MIDW_TUNERMASTER_FI::FID_TUNMSTR_S_ACTIVATE_ANNOUNCEMENTResult >& result);
      virtual void onFID_TUNMSTR_G_ANNO_BUTTON_STATUSError(const ::boost::shared_ptr< ::MIDW_TUNERMASTER_FI::MIDW_TUNERMASTER_FIProxy >& proxy, const boost::shared_ptr< ::MIDW_TUNERMASTER_FI::FID_TUNMSTR_G_ANNO_BUTTON_STATUSError >& error);
      virtual void onFID_TUNMSTR_G_ANNO_BUTTON_STATUSStatus(const ::boost::shared_ptr< ::MIDW_TUNERMASTER_FI::MIDW_TUNERMASTER_FIProxy >& proxy, const boost::shared_ptr< ::MIDW_TUNERMASTER_FI::FID_TUNMSTR_G_ANNO_BUTTON_STATUSStatus >& status);

      virtual void onExpired(asf::core::Timer& timer, boost::shared_ptr<asf::core::TimerPayload> data);
      static void TraceCmd_ActivateNextPrevious(::MPlay_fi_types::T_e8_MPlayAction nPlaybackId);
#ifdef SEEK_SUPPORT
      static void TraceCmd_SeekTo(unsigned int seektime);
#endif
      static void TraceCmd_ActivatePopup(int);
      static void TraceCmd_DeActivatePopup(int);
      virtual void requestNowPlayingGet();
      virtual void setPlayModeUpdatePending(bool isPlayModeUpdatePending);
      virtual void updateLoadingFilePopupStatus(bool status);
      virtual void getSongPlayFromFolderBrowseStatus(bool status);

#ifdef PHOTOPLAYER_SUPPORT
      bool onCourierMessage(const ResumePlayPhotoPlayerReqMsg& /*msg*/);
      bool onCourierMessage(const PausePhotoPlayerReqMsg& /*msg*/);
      bool onCourierMessage(const PrevPhotoPlayerReqMsg& /*msg*/);
      bool onCourierMessage(const NextPhotoPlayerReqMsg& /*msg*/);
      bool onCourierMessage(const StopPhotoPlayerReqMsg& /*msg*/);
      bool onCourierMessage(const FitScreenPhotoPlayerReqMsg& /*msg*/);
      bool onCourierMessage(const SettingPhotoPlayerReqMsg& /*msg*/);
#endif
      bool onCourierMessage(const SeekNextReqMsg& /*msg*/);
      bool onCourierMessage(const SeekPrevReqMsg& /*msg*/);
      bool onCourierMessage(const FastForwardStartReqMsg& /*msg*/);
      bool onCourierMessage(const FastForwardStopReqMsg& /*msg*/);
      bool onCourierMessage(const FastRewindStartReqMsg& /*msg*/);
      bool onCourierMessage(const FastRewindStopReqMsg& /*msg*/);
      bool onCourierMessage(const RepeatReqMsg& /*msg*/);
      bool onCourierMessage(const RandomReqMsg& /*msg*/);
      bool onCourierMessage(const PlayReqMsg& /*msg*/);
      bool onCourierMessage(const PauseReqMsg& /*msg*/);
      bool onCourierMessage(const TAReqMsg& /*msg*/);
      bool onCourierMessage(const PlayPauseReqMsg& /*msg*/);


#ifdef SEEK_SUPPORT
      bool onCourierBindingItemChange_MediaMetaDataItem(Courier::Request& request);
#endif
#ifdef ALBUM_ART_TOGGLE_SUPPORT
      bool onCourierMessage(const AlbumArtToggleReqMsg& /*msg*/);
#endif
#ifdef ENCODER_ROTATION_TRACK_CHANGE_SUPPORT
      bool onCourierMessage(const EncoderTrackChangeUpMsg& msg);
#endif
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
#ifdef SEEK_SUPPORT
      ON_COURIER_MESSAGE(Courier::UpdateModelMsg)
#endif
#ifdef PHOTOPLAYER_SUPPORT
      ON_COURIER_MESSAGE(ResumePlayPhotoPlayerReqMsg)
      ON_COURIER_MESSAGE(PausePhotoPlayerReqMsg)
      ON_COURIER_MESSAGE(PrevPhotoPlayerReqMsg)
      ON_COURIER_MESSAGE(NextPhotoPlayerReqMsg)
      ON_COURIER_MESSAGE(StopPhotoPlayerReqMsg)
      ON_COURIER_MESSAGE(FitScreenPhotoPlayerReqMsg)
      ON_COURIER_MESSAGE(SettingPhotoPlayerReqMsg)
#endif
      ON_COURIER_MESSAGE(SeekNextReqMsg)
      ON_COURIER_MESSAGE(SeekPrevReqMsg)
      ON_COURIER_MESSAGE(FastForwardStartReqMsg)
      ON_COURIER_MESSAGE(FastForwardStopReqMsg)
      ON_COURIER_MESSAGE(FastRewindStartReqMsg)
      ON_COURIER_MESSAGE(FastRewindStopReqMsg)
      ON_COURIER_MESSAGE(RepeatReqMsg)
      ON_COURIER_MESSAGE(RandomReqMsg)
      ON_COURIER_MESSAGE(PlayReqMsg)
      ON_COURIER_MESSAGE(PauseReqMsg)
      ON_COURIER_MESSAGE(TAReqMsg)
      ON_COURIER_MESSAGE(PlayPauseReqMsg)

#ifdef ALBUM_ART_TOGGLE_SUPPORT
      ON_COURIER_MESSAGE(AlbumArtToggleReqMsg)
#endif
#ifdef ENCODER_ROTATION_TRACK_CHANGE_SUPPORT
      ON_COURIER_MESSAGE(EncoderTrackChangeUpMsg)
#endif
      COURIER_MSG_MAP_DELEGATE_START()
#ifdef TBT_NOTIFICATION_SUPPORT
      COURIER_MSG_DELEGATE_TO_OBJ(_mPtrNavigationServiceHandler)
#endif
      COURIER_MSG_MAP_DELEGATE_END()
#ifdef SEEK_SUPPORT
      COURIER_BINDING_MAP_BEGIN()
      COURIER_BINDING_ITEM_CHANGE(MediaMetaDataItem)
      COURIER_BINDING_MAP_END()
#endif

   private:
      std::string oGetMonospaceDigit(char* pDestBuff, uint32 u32DestBufferSize);
      bool bUpdateNextPrevCounter(::MPlay_fi_types::T_e8_MPlayAction nPlaybackId);
      void vUpdateAlbumArt(const std::string& oStr, ::MPlay_fi_types::T_e8_MPlayNowPlayingState nowPlayingState);
      void triggerPendingNextPrevPlayBackAction();
      void postTtfisSkipMessage(::MPlay_fi_types::T_e8_MPlayAction playbackId);
      bool isPodcastAndAudiobookPlaying();
#ifdef SEEK_SUPPORT
      void postSeekmessage(unsigned int seektime);
#endif
      void postActivatePopup(int popupId);
      void postDeActivatePopup(int popupId);
      void drawAlbumArt();
      void updatePlayModeIfPending(uint32 listHandle);
      void updateMetaDataField(const ::MPlay_fi_types::T_MPlayMediaObject& oMediaObject);
      void handlePlayableStateOfCurrentTrack(::MPlay_fi_types::T_e8_MPlayPlayableStatus playableStatus);
#ifdef ENCODER_ROTATION_TRACK_CHANGE_SUPPORT
      bool isEncoderAvailableRegion();
      //For Turnbyturn updates
#endif
#ifdef TBT_NOTIFICATION_SUPPORT
      void onNextManeuverDetailsUpdate(const bool& isRGActive, const TBTManeuverInfoData& TBTManeuverInfo);
#endif
      MetaData _metaData;
      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& _mediaPlayerProxy;
      ::boost::shared_ptr< ::MIDW_TUNERMASTER_FI::MIDW_TUNERMASTER_FIProxy >& _tunerMasterFiProxy;

#ifdef TBT_NOTIFICATION_SUPPORT
      NavigationServiceHandler* _mPtrNavigationServiceHandler;
#endif

      bool _isNextPrevActionCompleted;
      signed int _nextPrevActionCounter;
      std::string _strAlbumArt;
      uint32 mutable _currentImageSize;
      PlayMode _playMode;
      ::MPlay_fi_types::T_e8_MPlayAction playPauseStatus;
      ::MPlay_fi_types::T_MPlayImageData _ImageData;
      bool _TAStatus;
      bool _isAlbumArtEnable;
      bool _isAlbumArtFetched;
      bool _isPlayModeUpdatePending;
      PlayingSelection _currentPlayingSelection;
      bool isLoadingFilePopupActive;
      ::Courier::DataItemContainer<MediaMetaDataDataBindingSource> _metadataProgressBar;
      static MediaPlayScreenHandling* _playScreenHandlingInstance;
      asf::core::Timer _errorfiletimer;
      asf::core::Timer _nextPrevBufferTimer;
      bool _isActualAlbumArtUpdated; //False: either default cover art active or wating for reply; True: Actual cover art for playing item tag is already updated
      bool _statusOfSongPlayingFromFolderBrowse;
#ifdef PHOTOPLAYER_SUPPORT
      uint8 _u8SlideshowState;
      uint32 _u32SlideshowTimeStatus;
#endif
};


}
}


#endif // _MEDIA_PLAYSCREEN_HANDLING_H_
