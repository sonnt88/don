/**
*  @file   MediaSettings.h
*  @author ECV - pkm8cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/
#include "hall_std_if.h"
#include "Core/MediaInfoSettings/MediaSettings.h"
#include "mplay_MediaPlayer_FIProxy.h"
#include "Core/Utils/MediaProxyUtility.h"
#include "Common/ListHandler/ListRegistry.h"
#include "CgiExtensions/ListDataProviderDistributor.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL_MEDIA_INFOSETTINGS
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::MediaSettings::
#include "trcGenProj/Header/MediaSettings.cpp.trc.h"
#endif


namespace App {
namespace Core {

static const char* const MEDIA_SETTINGS_LIST_BUTTON_SETTINGS_ITEM_EVEN  = "Settings_Even";
static const char* const MEDIA_SETTINGS_LIST_BUTTON_SETTINGS_ITEM_ODD  = "Settings_Odd";
#ifdef PHOTOPLAYER_SUPPORT
static const char* const MEDIA_SETTINGS_LIST_BUTTON_PHOTOPLAYER_SETTINGS_ITEM_EVEN  = "PhotoPlayer_Settings_Even";
static const char* const MEDIA_SETTINGS_LIST_BUTTON_PHOTOPLAYER_SETTINGS_ITEM_ODD   = "PhotoPlayer_Settings_Odd";
#endif

MediaSettings* MediaSettings::_theInstance = NULL;

/**
 * @Constructor
 */
MediaSettings::MediaSettings(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy)
   : _mediaPlayerProxy(pMediaPlayerProxy)
   , _videoViewMode(FULL)
#ifdef PHOTOPLAYER_SUPPORT
   , _u32SlideshowTimeStatus(3000)
   , _bAnimationEffect(false)
#endif
{
   ETG_I_REGISTER_FILE();
}


/**
 * @Destructor
 */
MediaSettings::~MediaSettings()
{
   if (_theInstance)
   {
      delete _theInstance;
      _theInstance = NULL;
   }
#ifdef PHOTOPLAYER_SUPPORT
   //_u32SlideshowTimeStatus = 3000;
   _bAnimationEffect = false;
#endif
}


void MediaSettings::createInstance(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy)
{
   if (_theInstance == NULL)
   {
      _theInstance = new MediaSettings(pMediaPlayerProxy);
   }
}


/**
 *  MediaSettings::getInstance - function to get singleton instance
 *  @return Singleton Instance
 */
MediaSettings* MediaSettings::getInstance()
{
   assert(_theInstance);
   return _theInstance;
}


/**
 *  MediaSettings::setViewMode - Function to set the ViewMode of Video Settings.
 *  @param [in] bool
 *  @return None
 */
void MediaSettings::setViewMode(bool viewMode)
{
   ETG_TRACE_USR4(("MediaSettings::setViewMode"));
   _videoViewMode = viewMode;
}


/**
 *  MediaSettings::getViewMode - Function to return the ViewMode of Video Settings.
 *  @param [in] None
 *  @return bool
 */
bool MediaSettings::getViewMode()
{
   ETG_TRACE_USR4(("MediaSettings::getViewMode"));
   return _videoViewMode;
}


/**
 *  MediaSettings::updateMediaPlayerSettings - Function to Change the MediaPlayer Settings.
 *  @param [in] _mediaPlayerSettings
 *  @return none
 */
void MediaSettings::updateMediaPlayerSettings()
{
   ETG_TRACE_USR4(("MediaSettings::updateMediaPlayerSettings"));

   bool viewMode = getViewMode();
   viewMode = (viewMode == FULL) ?  FIT : FULL;
   setViewMode(viewMode);
   MediaDatabinding::getInstance().updateVideoPlayerViewModeSettings(viewMode);
}


/**getMediaSettingsListDataProvider:
 * add list items to the List scene
 * @return  list data needed for respective scene
 */
tSharedPtrDataProvider MediaSettings::getMediaSettingsListDataProvider(uint8 infoSettings)
{
   ETG_TRACE_USR4(("getMediaSettingsListDataProvider - infoSettings: %d", infoSettings));
   //initializes the provider with the list id and the default data context

   ListDataProviderBuilder listBuilder(LIST_ID_INFORMATION_SETTINGS);

   switch (infoSettings)
   {
      case VIDEO_PLAYER_SETTINGS:
      {
         uint32 itemIdx = 0;
         listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForMediaSettings(itemIdx, infoSettings)).AddData("View mode");
         break;
      }
#ifdef PHOTOPLAYER_SUPPORT
      case PHOTO_PLAYER_SETTINGS:
      {
         uint32 itemIdx = 0;
         listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForMediaSettings(itemIdx, infoSettings)).AddData(getMediaSettingsDataInfoItemValue(itemIdx - 1, infoSettings)).AddDataBindingUpdater<PhotoPlayerSettingsDataBindingSource>(getCurrentPhotoSettingsData());
         listBuilder.AddItem(itemIdx, 0, getlistItemTemplateForMediaSettings(itemIdx, infoSettings)).AddData(getMediaSettingsDataInfoItemValue(itemIdx - 1, infoSettings)).AddDataBindingUpdater<PhotoPlayerSettingsDataBindingSource>(getCurrentPhotoSettingsData());

         break;
      }
#endif
      default:
         break;
   }

   return listBuilder.CreateDataProvider();
}


#ifdef PHOTOPLAYER_SUPPORT
/**
 * onSlideshowTimeError - function called when mediaplayer gets an error during the property SlideshowTime updated.
 *
 * @param[in] proxy
 * @param[in] error
 * @return void
 */
void MediaSettings::onSlideshowTimeError(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< mplay_MediaPlayer_FI::SlideshowTimeError >& /*error*/)
{
   _u32SlideshowTimeStatus = 0;
   ETG_TRACE_USR4(("onSlideshowTimeError"));
}


/**
 * onSlideshowTimeStatus - function called when mediaplayer gets an status during the property SlideshowTime updated.
 *
 * @param[in] proxy
 * @param[in] status
 * @return void
 */
void MediaSettings::onSlideshowTimeStatus(const ::boost::shared_ptr< mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& /*proxy*/, const ::boost::shared_ptr< mplay_MediaPlayer_FI::SlideshowTimeStatus >& status)
{
   _u32SlideshowTimeStatus = status->getU32Time();
   ETG_TRACE_USR4(("onSlideshowTimeStatus = %d", _u32SlideshowTimeStatus));
}


#endif


/**
 * getlistItemTemplateForMediaSettigs - function to get list template.
 * @param[in] index, infoSettings
 * @parm[out] index
 * @return const char - return listItemTemplate
 */
const char* MediaSettings::getlistItemTemplateForMediaSettings(uint32& index, uint8 infoSettings)
{
   ETG_TRACE_USR4(("getlistItemTemplateForMediaSettings - index: %d, infoSettings: %d", index, infoSettings));
   const char* listItemTemplate;

   switch (infoSettings)
   {
      case VIDEO_PLAYER_SETTINGS:
      {
         if (index % 2)
         {
            listItemTemplate = MEDIA_SETTINGS_LIST_BUTTON_SETTINGS_ITEM_ODD;
         }
         else
         {
            listItemTemplate = MEDIA_SETTINGS_LIST_BUTTON_SETTINGS_ITEM_EVEN;
         }
         break;
      }
#ifdef PHOTOPLAYER_SUPPORT
      case PHOTO_PLAYER_SETTINGS:
      {
         if (index % 2)
         {
            listItemTemplate = MEDIA_SETTINGS_LIST_BUTTON_PHOTOPLAYER_SETTINGS_ITEM_ODD;
         }
         else
         {
            listItemTemplate = MEDIA_SETTINGS_LIST_BUTTON_PHOTOPLAYER_SETTINGS_ITEM_EVEN;
         }
         break;
      }
#endif
      default:
      {
         listItemTemplate = "";
         break;
      }
   }

   ++index;

   return listItemTemplate;
}


/**
 * getMediaSettingsDataInfoItemValue - function to get information about setting item information.
 * @param[in] index, infoSettings
 * @parm[out] none
 * @return const char - return data information value
 */
Candera::String MediaSettings::getMediaSettingsDataInfoItemValue(uint8 index, uint8 infoSettings)
{
   ETG_TRACE_USR4(("getMediaSettingsDataInfoItemValue - index: %d, infoSettings: %d", index, infoSettings));
   std::string itemValue = "";
   switch (infoSettings)
   {
#ifdef PHOTOPLAYER_SUPPORT
      case PHOTO_PLAYER_SETTINGS:
      {
         if (index % 2)
         {
            itemValue = "Animation effect";
         }
         else
         {
            itemValue = "Slideshow setting";
         }
         break;
      }
      default:
         break;
   }
#endif

   ETG_TRACE_USR4(("getMediaSettingsDataInfoItemValue - itemValue: %s", itemValue.c_str()));
   return itemValue.c_str();
}


#ifdef PHOTOPLAYER_SUPPORT
/**
 * getMediaSettingsDataInfoItemValue - function to get photoplayer setting value.
 * @param[in] none
 * @parm[out] none
 * @return PhotoPlayerSettingsData
 */
PhotoPlayerSettingsData MediaSettings::getCurrentPhotoSettingsData()
{
   ETG_TRACE_USR4(("getCurrentPhotoSettingsData - _u32SlideshowTimeStatus: %d, _bAnimationEffect: %d", _u32SlideshowTimeStatus, _bAnimationEffect));

   PhotoPlayerSettingsData photoPlayerSettings = PhotoPlayerSettingsData();

   switch (_u32SlideshowTimeStatus)
   {
      case 1000:
      {
         photoPlayerSettings.mSlideShowSettingTime = 0;
         break;
      }
      case 3000:
      {
         photoPlayerSettings.mSlideShowSettingTime = 1;
         break;
      }
      case 5000:
      {
         photoPlayerSettings.mSlideShowSettingTime = 2;
         break;
      }
      default:
         _u32SlideshowTimeStatus = 1000;
         photoPlayerSettings.mSlideShowSettingTime = 0;
         break;
   }

   _mediaPlayerProxy->sendSlideshowTimeSet(*this, _u32SlideshowTimeStatus);

   // It will improve after animation is applied
   photoPlayerSettings.mAnimationEffectOnOff = _bAnimationEffect;

   return photoPlayerSettings;
}


#endif


/**
 * onCourierMessage -

 * @param[in] VideoModeBtnPressUpdMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaSettings::onCourierMessage(const VideoModeBtnPressUpdMsg& /*msg*/)
{
   ETG_TRACE_USR4(("MediaInfoSettingsHandling::VideoModeBtnPressUpdMsg courier msg update"));
   updateMediaPlayerSettings();
   ListDataInfo listDataInfo;
   listDataInfo.listId = LIST_ID_INFORMATION_SETTINGS;
   ListRegistry::s_getInstance().updateList(listDataInfo);
   return true;
}


#ifdef PHOTOPLAYER_SUPPORT
/**
 * onCourierMessage - Message from Statemachine when setting slideshow times.
 *
 * @param[in] SettingTimePhotoPlayerSlideShowReqMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaSettings::onCourierMessage(const SettingTimePhotoPlayerSlideShowReqMsg& /*oMsg*/)
{
   ETG_TRACE_USR4(("onCourierMessage - SettingTimePhotoPlayerSlideShowReqMsg - _u32SlideshowTimeStatus: %d", _u32SlideshowTimeStatus));

   switch (_u32SlideshowTimeStatus)
   {
      case 1000:
      {
         _u32SlideshowTimeStatus = 3000;
         break;
      }
      case 3000:
      {
         _u32SlideshowTimeStatus = 5000;
         break;
      }
      case 5000:
      {
         _u32SlideshowTimeStatus = 1000;
         break;
      }
      default:
         _u32SlideshowTimeStatus = 1000;
         break;
   }

   ListDataInfo listDataInfo;
   listDataInfo.listId = LIST_ID_INFORMATION_SETTINGS;
   ListRegistry::s_getInstance().updateList(listDataInfo);
   return true;
}


/**
 * onCourierMessage - Message from Statemachine when setting slide show animation on/off.
 *
 * @param[in] OnOffAnimationEffectPhotoPlayerReqMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool MediaSettings::onCourierMessage(const OnOffAnimationEffectPhotoPlayerReqMsg& /*oMsg*/)
{
   _bAnimationEffect = _bAnimationEffect ? false : true;

   ListDataInfo listDataInfo;
   listDataInfo.listId = LIST_ID_INFORMATION_SETTINGS;
   ListRegistry::s_getInstance().updateList(listDataInfo);
   return true;
}


#endif
}// namespace AppLogic
} // namespace App
