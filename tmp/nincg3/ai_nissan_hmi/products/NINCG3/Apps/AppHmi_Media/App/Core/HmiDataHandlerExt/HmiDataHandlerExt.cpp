/*
 * HmiDataHandlerExt.cpp
 *
 *  Created on: Nov 18, 2015
 *  Author: A-IVI MediaTeam
 */

#include "gui_std_if.h"
#include "HmiDataHandlerExt.h"
#include "AppBase/StartupSync/StartupSync.h"
#include "AppHmi_MasterBase/AudioInterface/AudioDefines.h"
#include "MediaDatabinding.h"

using namespace ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData;

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::HmiDataHandlerExt::
#include "trcGenProj/Header/HmiDataHandlerExt.cpp.trc.h"
#endif
HmiDataHandlerExt::HmiDataHandlerExt()
{
   _hmiDataHandler  = new HmiDataCommonHandler::HmiDataHandler();
   if (HmiDataHandler::GetInstance() != NULL)
   {
      HmiDataHandler::GetInstance()->vRegisterforUpdate(this);
   }
   StartupSync::getInstance().registerPropertyRegistrationIF(this);
   _mSrcId = 0;
   _mAppModeId = 0;
   ETG_I_REGISTER_FILE();
}


HmiDataHandlerExt::~HmiDataHandlerExt()
{
   if (HmiDataHandlerExt::GetInstance() != NULL)
   {
      HmiDataHandlerExt::GetInstance()->vUnRegisterforUpdate(this);
   }
   if (NULL != _hmiDataHandler)
   {
      delete _hmiDataHandler;
   }
   _hmiDataHandler = NULL;
}


void HmiDataHandlerExt::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if ((proxy == HmiDataCommonHandler::HmiDataHandler::_hmiDataProxy))
   {
      _hmiDataProxy->sendVisibleApplicationModeRegister(*this);
   }
}


void HmiDataHandlerExt::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if ((proxy == HmiDataCommonHandler::HmiDataHandler::_hmiDataProxy))
   {
      _hmiDataProxy->sendVisibleApplicationModeDeregisterAll();
   }
}


void HmiDataHandlerExt::handleSwipeDirectionPropertyUpdate(const uint8 /*swipeDirectionType*/)
{
   //As of now nothing to do..
}


void HmiDataHandlerExt::onSetApplicationModeError(const ::boost::shared_ptr<HmiDataProxy >& /*proxy*/, const ::boost::shared_ptr< SetApplicationModeError >& /*error*/)
{
}


void HmiDataHandlerExt::onSetApplicationModeResponse(const ::boost::shared_ptr< HmiDataProxy >& /*proxy*/, const ::boost::shared_ptr< SetApplicationModeResponse >& /*response*/)
{
}


bool HmiDataHandlerExt::onCourierMessage(const SetApplicationModeReqMsg& oMsg)
{
   HmiDataCommonHandler::HmiDataHandler::_hmiDataProxy->sendSetApplicationModeRequest(*this, APPID_APPHMI_MEDIA, oMsg.GetModeId());
   return true;
}


/**
 * getCurrentAudioSourceUpdate - to update current audio media source to _mSrcId
 * @param[in] srcId
 * @return void
 */
void HmiDataHandlerExt::getCurrentAudioSourceUpdate(int32 srcId)
{
   _mSrcId = srcId;
}


void HmiDataHandlerExt::onVisibleApplicationModeError(const ::boost::shared_ptr< HmiDataProxy >& /*proxy*/, const ::boost::shared_ptr< VisibleApplicationModeError >& /*error*/)
{
}


void HmiDataHandlerExt::onVisibleApplicationModeUpdate(const ::boost::shared_ptr< HmiDataProxy >& /*proxy*/, const ::boost::shared_ptr< VisibleApplicationModeUpdate >& update)
{
   ETG_TRACE_USR4(("HmiDataHandlerExt:onVisibleApplicationModeUpdate is called"));
   static bool isAuxKey = false;
   _mAppModeId = (int)update->getVisibleApplicationMode();
#if defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2)|| defined(VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R)
   App::Core::MediaDatabinding::getInstance().SetApplicationModeValue(_mAppModeId);
#endif
   if (_mAppModeId == APP_MODE_CAMERA)
   {
      if ((_mSrcId >= SRC_MEDIA_AUX) && (_mSrcId <= SRC_SPI_ENTERTAIN))
      {
         isAuxKey = true;
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
         POST_MSG((COURIER_MESSAGE_NEW(AuxKeyRegisterUpdMsg)(isAuxKey)));
#endif
      }
   }
   else
   {
      if (isAuxKey)
      {
         isAuxKey = false;
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
         POST_MSG((COURIER_MESSAGE_NEW(AuxKeyRegisterUpdMsg)(isAuxKey)));
#endif
      }
   }
}


uint8 HmiDataHandlerExt::getCurrentVisibleAppMode()
{
   uint8 visibleAppMode = 0xFF;
   if (HmiDataCommonHandler::HmiDataHandler::_hmiDataProxy->hasVisibleApplicationMode())
   {
      visibleAppMode = HmiDataCommonHandler::HmiDataHandler::_hmiDataProxy->getVisibleApplicationMode();
   }
   return visibleAppMode;
}
