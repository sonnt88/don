/**
*  @file   StaticBrowse.h
*  @author ECV - vma6cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef STATICBROWSE_H_
#define STATICBROWSE_H_

#include "IBrowse.h"

namespace App {
namespace Core {

class StaticBrowse : public IBrowse
{
   public:
      StaticBrowse();
      ~StaticBrowse() {}
      tSharedPtrDataProvider getDataProvider(RequestedListInfo& requestedlistInfo, uint32 activeDeviceType);
      void populateNextListOrPlayItem(uint32, uint32, uint32, uint32) {}
      void populatePreviousList(uint32) {}
      void updateListData() {}
      void initializeListParameters() {}
      void updateEmptyListData(uint32) {}
      void setWindowStartIndex(uint32);
      void setFocussedIndex(int32);
      void setFooterDetails(uint32) {}
#ifdef VERTICAL_KEYBOARD_SEARCH_SUPPORT
      void updateSearchKeyBoardListInfo(trSearchKeyboard_ListItem /*item*/) {}
      bool updateVerticalListToGuiList(const ButtonListItemUpdMsg& /*oMsg*/)
      {
         return true;
      }
      void SetActiveVerticalKeyLanguague() {}
      void ShowVerticalKeyInfo(bool /*pDefault*/) {}
#endif
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_MAP_DELEGATE_END()

   private:
      tSharedPtrDataProvider getBrowseListDataProvider(uint32 activeDeviceTag, uint32 activeDeviceType);
      bool isIndexingDone(uint32 activeDeviceTag);
      bool getIndexingStatusToEnableList();
      const char* getlistItemTemplateForStaticBrowse(uint32& index);
};


}
}


#endif /* STATICBROWSE_H_ */
