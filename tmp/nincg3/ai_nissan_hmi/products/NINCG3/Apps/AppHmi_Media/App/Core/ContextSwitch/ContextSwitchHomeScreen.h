/**
 *  @file   ContextSwitchHomeScreen.h
 *  @Author:put5cob (RBEI/ECV3)
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#ifndef CONTEXTSWITCHHOMESCREEN_H_
#define CONTEXTSWITCHHOMESCREEN_H_

#include "mplay_MediaPlayer_FIProxy.h"
#include "Proxy/provider/AppMedia_ContextProvider.h"

namespace App {
namespace Core {

class ContextSwitchHomeScreen
{
   public:
      /*Device info storage*/
      typedef struct trDeviceInfo
      {
         unsigned int DeviceTag;
         unsigned int DeviceType;
         std::string DeviceName;
      } TDeviceInfo;

      ContextSwitchHomeScreen(AppMedia_ContextProvider* poContextProvider);
      ~ContextSwitchHomeScreen();

      /**
       *  Member Function Declaration
       */
      void setDeviceInfoHomeScreen(::MPlay_fi_types::T_MPlayDeviceInfo deviceInfo);
      bool getHomeScreenSourceInfo(Courier::UInt32 ContextId, int32& SrcID, int32& SubSrcID);

   private:

      void UpdateDeviceDisconnection(TDeviceInfo Deviceinfo);
      void UpdateDeviceConnection(TDeviceInfo Deviceinfo);
      bool isDeviceAvailabe(TDeviceInfo Deviceinfo);

      void setDefaultConnection();
      void printAvailDevice();
      /**
       *  Member Variable Declaration
       */
      std::map<Courier::UInt32, TDeviceInfo> _mDeviceInfo;
      AppMedia_ContextProvider* _poContextProvider;
};


} // namespace AppLogic
} // namespace App
#endif /* CONTEXTSWITCHHOMESCREEN_H_ */
