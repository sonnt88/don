/*
 * @file       : DataCollector.cpp
 * @author     : CM-PJCB
 * @copyright  : (c) 2015 Robert Bosch Car Multimedia GmbH
 * @addtogroup : Media
 */
#include "hall_std_if.h"
#include "DataCollector.h"
#include "Core/LanguageDefines.h"
#include "Core/DeviceConnection/IDeviceConnection.h"

#include "mplay_MediaPlayer_FIProxy.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL_DATA_COLLECTOR
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::DataCollector::
#include "trcGenProj/Header/DataCollector.cpp.trc.h"
#endif

using namespace ::datacollector_main_fi;
using namespace MPlay_fi_types;

namespace App {
namespace Core {
static const std::string subString = ("$(1:BT_ICON)");
static const  std::string btIcon("\uf8af");

/**
 * Constructor of class DataCollector
 */
DataCollector::DataCollector(IDeviceConnection* _pdeviceConnection, SpiConnectionHandling* paraSpiConnectionHandling):
   _statusBarHandler(StatusBarCommonHandler::StatusBarHandler::GetInstance())
   , _currentActiveScreen(Media_Scenes_MEDIA_AUX__LINEIN_MAIN)
   , _metadataBrowserHeader("")
   , _sHeaderText_USB("")
   , _sHeaderText_IPOD("")
   , _ideviceConnection(*_pdeviceConnection)
   , _indexPercent(0)
   , _indexState(0)
   , _mpSpiConnectionHandling(paraSpiConnectionHandling)
{
   ETG_I_REGISTER_FILE();

   if (_statusBarHandler != NULL)
   {
      _statusBarHandler->vRegisterforUpdate(this);
   }

   {
      m_Source_MAP[SOURCE_CD] = ::datacollector_main_fi_types::T_e8_datacollector_Audio_Source__CD;
      m_Source_MAP[SOURCE_AUX] = ::datacollector_main_fi_types::T_e8_datacollector_Audio_Source__Pin_Aux;
      m_Source_MAP[SOURCE_BT] = ::datacollector_main_fi_types::T_e8_datacollector_Audio_Source__BT_Audio;
      m_Source_MAP[SOURCE_IPOD] = ::datacollector_main_fi_types::T_e8_datacollector_Audio_Source__iPod;
      m_Source_MAP[SOURCE_USB] = ::datacollector_main_fi_types::T_e8_datacollector_Audio_Source__USB;
      m_Source_MAP[SOURCE_SPI] = ::datacollector_main_fi_types::T_e8_datacollector_Audio_Source__None; //Initializing to None by default.To be mapped according to device type
   }
   {
      m_Action_MAP[NEXT_SONG] = ::datacollector_main_fi_types::T_e8_datacollector_Interrupt_Type__Next_Track;
      m_Action_MAP[PREV_SONG] = ::datacollector_main_fi_types::T_e8_datacollector_Interrupt_Type__Prev_Track;
      m_Action_MAP[FF_SONG] = ::datacollector_main_fi_types::T_e8_datacollector_Interrupt_Type__Start_FF;
      m_Action_MAP[RW_SONG] = ::datacollector_main_fi_types::T_e8_datacollector_Interrupt_Type__Start_RW;
      m_Action_MAP[FF_RW_STOP] = ::datacollector_main_fi_types::T_e8_datacollector_Interrupt_Type__Stop_FF_RW;
   }

   ETG_TRACE_USR4(("DataCollector:Object created"));
}


/**
 * Destructor of class DataCollector
 */
DataCollector::~DataCollector()
{
   ETG_TRACE_USR4(("DataCollector:Object Destroyed"));
   if (_statusBarHandler)
   {
      _statusBarHandler->vUnRegisterforUpdate(this);
      _statusBarHandler = NULL;
   }
   _mpSpiConnectionHandling = NULL;
}


/**
 * This funtion is used to get the get the startup trigger and initializes default value for header line.
 * based on the statup of the GUI default data collector function needs to be called.
 * @param[in] - msg       - GuiStartupFinishedUpdMsg,
 * @param[out] -returns bool
 */
//bool DataCollector::onCourierMessage(const GuiStartupFinishedUpdMsg& /*msg*/)
/*{
   this->vInitializeStatusLineDataDefault();
   this->setHeaderTextData(LANGUAGE_STRING(LINEIN__MAIN_Headline, "Aux"));
   this->setHeaderIconData(SRC_AUX);
   return false;
}*/
/**
 * This funtion is used to get header text for each scene
 * used for updating status line
 * @param[in] - int8 sHeaderIndex
 * @param[out] -returns std::string headertext
 */
Candera::String DataCollector::getHeaderText(uint32 sHeaderIndex) const
{
   Candera::String returnVal;
   switch (sHeaderIndex)
   {
      case Media_Scenes_MEDIA__AUX__USB_MAIN:
      {
         returnVal = _sHeaderText_USB.c_str();
         if (_sHeaderText_USB == "")
         {
            returnVal = LANGUAGE_STRING(USB__MAIN_Headline, "USB");
         }
#ifdef SHOW_DEVICE_NAME_IN_HEADER_SUPPORT
         std::string devicename = _ideviceConnection.getActiveDeviceName(sHeaderIndex);
         returnVal =  devicename.c_str();
#endif
      }
      break;
      case Media_Scenes_MEDIA__AUX__IPOD_MAIN:
      {
         returnVal = _sHeaderText_IPOD.c_str();
         if (_sHeaderText_IPOD == "")
         {
            returnVal = LANGUAGE_STRING(IPOD__MAIN_Headline, "IPOD");
         }
#ifdef SHOW_DEVICE_NAME_IN_HEADER_SUPPORT
         std::string devicename = _ideviceConnection.getActiveDeviceName(sHeaderIndex);
         returnVal =  devicename.c_str();
#endif
      }
      break;
      case Media_Scenes_MEDIA_AUX__BTAUDIO_BROWSER :
      {
         returnVal = LANGUAGE_STRING(BT_AUDIO_BTMENU , "BT Menu");
      }
      break;
      case Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN :
      case Media_Scenes_MEDIA__CD_CDMP3_MAIN :
      {
         std::string deviceName =  _ideviceConnection.getActiveDeviceName(sHeaderIndex);
         returnVal = deviceName.c_str();
      }
      break;
      case Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT:
      {
         returnVal = LANGUAGE_STRING(BTAUDIO__MAIN_Headline , "BLUETOOTH AUDIO");
      }
      break;
      case Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE:
      {
         std::string currentFolderPath = MediaDatabinding::getInstance().getCurrentFolderPath();
         std::string strName = returnVal.GetCString();
         MediaUtils::bExtractFileName(currentFolderPath, strName);
         returnVal = strName.c_str();
         if (strName == "")				// if the folder path is in root then display "USB"
         {
            if (MediaDatabinding::getInstance().getCurrentActiveMediaDevice() == SOURCE_BT)
            {
               std::string deviceName =  _ideviceConnection.getActiveDeviceName(Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN);
               returnVal = deviceName.c_str();
            }
            else if (MediaDatabinding::getInstance().getCurrentActiveMediaDevice() == SOURCE_USB)
            {
               returnVal = LANGUAGE_STRING(USB__MAIN_Headline , "USB");
            }
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2
            else if (MediaDatabinding::getInstance().getCurrentActiveMediaDevice() == SOURCE_CDMP3)
            {
               returnVal = LANGUAGE_STRING(CDMP3__MAIN_Menu , "CDROM");
            }
            else if (MediaDatabinding::getInstance().getCurrentActiveMediaDevice() == SOURCE_CD)
            {
               returnVal = LANGUAGE_STRING(CDMP3__MAIN_Menu , "CDDA");
            }
#endif
         }
      }
      break;
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
      case Spi_Spi_Scene_SETTINGS__CARPLAY:
      {
         returnVal = LANGUAGE_STRING(SETTINGS_CARPLAY_Headline, "SPI Settings");
      }
      break;
      case Spi_Spi_Scene_CARPLAY_TUTORIAL:
      {
         returnVal = LANGUAGE_STRING(SETTINGS_CARPLAY_TUTORIAL_Headline , "CarPlay Tutorial");
      }
      break;
#endif
      case Media_Scenes_MEDIA_AUX__BROWSER:
      {
         if (MediaDatabinding::getInstance().getCurrentActiveMediaDevice() == SOURCE_IPOD)
         {
            returnVal = LANGUAGE_STRING(IPOD__MAIN_Headline, "IPOD");
         }
         else if (MediaDatabinding::getInstance().getCurrentActiveMediaDevice() == SOURCE_USB)
         {
            returnVal = LANGUAGE_STRING(USB__BROWSE_Headline , "USB");
         }
         break;
      }
      case Media_Scenes_MEDIA__LOADING_MAIN:
      {
         returnVal = "";
         break;
      }
      case Media_Scenes_MEDIA_AUX__METADATA_BROWSE:
         returnVal = _metadataBrowserHeader;
         break;
      case Media_Scenes_MEDIA_AUX__LINEIN_MAIN:
      {
         returnVal = LANGUAGE_STRING(LINEIN__MAIN_Headline, "AUX");
         updateAuxInfo();
         break;
      }
      default :
      {
         returnVal = "";
         break;
      }
   }
   return returnVal;
}


/**
 * This funtion is used to get header icon for each scene
 * used for updating status line
 * @param[in] - int8 sHeaderIndex
 * @param[out] -returns enHeaderIcon
 */
enHeaderIcon DataCollector::getHeaderIcon(uint32 sHeaderIndex) const
{
   enHeaderIcon enIcon = UNDEF;
   switch (sHeaderIndex)
   {
      case Media_Scenes_MEDIA__CD_CDMP3_MAIN:
      {
         enIcon = SRC_CD;
      }
      break;
      case Media_Scenes_MEDIA__AUX__USB_MAIN :
      {
         enIcon = SRC_USB;
      }
      break;
      case Media_Scenes_MEDIA_AUX__USB_FOLDER_BROWSE :
      {
         uint32 currentActiveMediaDevice = MediaDatabinding::getInstance().getCurrentActiveMediaDevice();
         switch (currentActiveMediaDevice)
         {
            case SOURCE_USB:
            {
               enIcon = SRC_USB;
            }
            break;
            case SOURCE_CDMP3:
            case SOURCE_CD:
            {
               enIcon = SRC_CD;
            }
            break;
            case SOURCE_BT:
            {
               enIcon = SRC_PHONE;
            }
            break;
            default:
               break;
         }
      }
      break;
      case Media_Scenes_MEDIA__AUX__BTAUDIO_MAIN :
      case Media_Scenes_MEDIA_AUX__BTAUDIO_MAIN_DEFAULT :
      case Media_Scenes_MEDIA_AUX__BTAUDIO_BROWSER :
      {
         enIcon = SRC_PHONE;
      }
      break;
      case Media_Scenes_MEDIA__AUX__IPOD_MAIN:
      {
         enIcon = SRC_IPOD;
      }
      break;
#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE1
      case Spi_Spi_Scene_SETTINGS__CARPLAY:
      case Spi_Spi_Scene_CARPLAY_TUTORIAL:
      {
         enIcon = NAVI_SETTINGS;
      }
      break;
#endif
      case Media_Scenes_MEDIA_AUX__METADATA_BROWSE:
      case Media_Scenes_MEDIA_AUX__BROWSER:
      {
         if (MediaDatabinding::getInstance().getCurrentActiveMediaDevice() == SOURCE_IPOD)
         {
            enIcon = SRC_IPOD;
         }
         else if (MediaDatabinding::getInstance().getCurrentActiveMediaDevice() == SOURCE_USB)
         {
            enIcon = SRC_USB;
         }
         break;
      }
      case Media_Scenes_MEDIA__LOADING_MAIN:
      {
         enIcon = UNDEF;
         break;
      }
      case Media_Scenes_MEDIA_AUX__LINEIN_MAIN:
      {
         enIcon = SRC_AUX;
         break;
      }
      default :
      {
         enIcon = UNDEF;
         break;
      }
   }
   return enIcon;
}


/**
 * This funtion is used to update the AuxInfo in GUI.
 * @param[in] - int8 sHeaderIndex
 * @param[out] -returns enHeaderIcon
 */
void DataCollector::updateAuxInfo(void) const
{
   Candera::String auxLangInfo = LANGUAGE_STRING(MEDIA_LINEIN__MAIN_Message, "Your system also supports these audio components:Apple® devices via USB,USB players and sticks, smart phones via USB and $(1:BT_ICON) Bluetooth.");
   std::string strInfo = auxLangInfo.GetCString();
   if (strInfo.find(subString) != std::string::npos)
   {
      strInfo.replace(strInfo.find(subString), subString.length(), btIcon);
      MediaDatabinding::getInstance().updateAuxScreenInfo(strInfo.c_str());
   }
}


/**
 * This funtion is used to update every screen header based on scenename
 *
 * @param[in] - msg       - ScreenHeaderReq,
 * @param[out] -returns bool
 */
bool DataCollector::onCourierMessage(const ScreenHeaderReq& msg)
{
   _currentActiveScreen = msg.GetSceneName();								//this private data member is used during indexing .
   Candera::String sHeaderText = getHeaderText(_currentActiveScreen);
   this->setHeaderTextData(sHeaderText);
   enHeaderIcon headerIcon = getHeaderIcon(msg.GetSceneName());
   this->setHeaderIconData(headerIcon);
   return true;
}


/**
 * This funtion is used to update indexing percentage in the header of main screens.
 *
 *The courier message is posted whenever there is update in IndexingState property by the mediaplayer.
 * @param[in] - msg       - IndexingInfoUpdMsg,
 * @param[out] -returns bool
 */
bool DataCollector::onCourierMessage(const IndexingInfoUpdMsg& msg)
{
   _indexPercent = msg.GetIndexPercentage();
   _indexState = msg.GetIndexState();
   ETG_TRACE_USR4(("DataCollector: IndexingInfoUpdMsg:%d", _indexPercent));
   updateIndexingOnActiveScreen(_indexPercent, _indexState);

   return true;
}


void DataCollector::updateIndexingOnActiveScreen(int8 indexPercent, int8 indexState)
{
   char indexInStr[MEDIA_HEADER_STRING_SIZE] = "";
   std::string indexingPercentText;
   indexingPercentText.clear();
   Candera::String indexString = LANGUAGE_STRING(MEDIA__MAIN_Headline_Indexing, "Indexing: ");
   std::string strIndexString = indexString.GetCString();
   if ((indexState == T_e8_MPlayDeviceIndexedState__e8IDS_PARTIAL) && (indexPercent > MEDIA_MIN_INDEXING_PERCENTAGE)) 	// indexing info should be displayed only if indexing is ongoing ;it shouldnt be displayed if it is completed.
   {
      MediaUtils::convertToString(indexInStr, indexPercent);
      indexingPercentText += ("(" + strIndexString + indexInStr + "%)");
   }
   updateHeaderBasedOnActiveMedia(indexingPercentText);
   if (_currentActiveScreen == Media_Scenes_MEDIA__AUX__USB_MAIN)	// Updating the header binding variable is needed only for USB and IPod Main screen.
   {
      this->setHeaderTextData(_sHeaderText_USB.c_str());
   }
   else if (_currentActiveScreen == Media_Scenes_MEDIA__AUX__IPOD_MAIN)
   {
      this->setHeaderTextData(_sHeaderText_IPOD.c_str());
   }
}


void DataCollector::updateHeaderBasedOnActiveMedia(std::string indexPercentText)
{
   Candera::String headerText;
   _sHeaderText_USB.clear();
   _sHeaderText_IPOD.clear();
   std::string strIndexPercent = indexPercentText;
   if (MediaDatabinding::getInstance().getCurrentActiveMediaDevice() == SOURCE_USB)
   {
      headerText = LANGUAGE_STRING(USB__MAIN_Headline_USBIndexing, "USB");
      _sHeaderText_USB = headerText.GetCString();
      _sHeaderText_USB = _sHeaderText_USB.append(strIndexPercent);
      ETG_TRACE_USR4(("DataCollector: IndexingInfoUpdMsg received: %s ", _sHeaderText_USB.c_str()));
   }
   else if (MediaDatabinding::getInstance().getCurrentActiveMediaDevice() == SOURCE_IPOD)
   {
      headerText = LANGUAGE_STRING(IPOD__MAIN_Headline_IpodIndexing, "IPOD");
      _sHeaderText_IPOD = headerText.GetCString();
      _sHeaderText_IPOD = _sHeaderText_IPOD.append(strIndexPercent);
      ETG_TRACE_USR4(("DataCollector: IndexingInfoUpdMsg received: %s ", _sHeaderText_IPOD.c_str()));
   }
}


/**
 * This funtion is used to update Language change.
 *
 * @param[in] - msg       - LanguageChangeUpdMsg,
 * @param[out] -returns bool
 */
bool DataCollector::onCourierMessage(const LanguageChangeUpdMsg& /*msg*/)
{
   updateIndexingOnActiveScreen(_indexPercent, _indexState);
   updateAuxInfo();
   return true;
}


/**
 * This funtion is used to update metadata browse header info
 *
 * @param[in] - msg       - BrowserHeaderUpdMsg,
 * @param[out] -returns bool
 */
bool DataCollector::onCourierMessage(const BrowserHeaderUpdMsg& msg)
{
   _metadataBrowserHeader = msg.GetBrowserHeaderText().GetCString();
   ETG_TRACE_USR4(("DataCollector:BrowserHeaderUpdMsg:%s", _metadataBrowserHeader.GetCString()));
   return true;
}


/**
 *  updateHeaderPhoneSignalStrenghthInfo : Update the phone signal strength info to status line.
 *
 * @param[in]      :
 * @return         : void
 */
void DataCollector::updatePhoneSignalStrength(uint8 SignalStrength)
{
   ETG_TRACE_USR4(("updatePhoneSignalStrength Signal Strength :%d", SignalStrength));

   if (SignalStrength != UNDEFINED)
   {
      //Update Antenna Icon
      (*_headerAntennaIcon).mVisible = true;
      _headerAntennaIcon.MarkItemModified(ItemKey::HeaderAntennaIcon::VisibleItem);
      _headerAntennaIcon.SendUpdate(true);

      (*_headerAntennaIcon).mIndex = SignalStrength;
      _headerAntennaIcon.MarkItemModified(ItemKey::HeaderAntennaIcon::IndexItem);
      _headerAntennaIcon.SendUpdate(true);
   }
   else
   {
      //Update Antenna Icon
      (*_headerAntennaIcon).mVisible = false;
      _headerAntennaIcon.MarkItemModified(ItemKey::HeaderAntennaIcon::VisibleItem);
      _headerAntennaIcon.SendUpdate(true);
   }
}


/**
 *  updateHeaderPhoneBatteryInfo : Update the phone Battery info to status line.
 *
 * @param[in]      :
 * @return         : void
 */
void DataCollector::updateHeaderPhoneBatteryInfo(uint8 BatteryInfo)
{
   ETG_TRACE_USR4(("updateHeaderPhoneBatteryInfo Battery Info:%d", BatteryInfo));
   if (BatteryInfo != UNDEFINED)
   {
      //Update Battery Icon
      (*_headerBatteryIcon).mVisible = true;
      _headerBatteryIcon.MarkItemModified(ItemKey::HeaderBatteryIcon::VisibleItem);
      _headerBatteryIcon.SendUpdate(true);

      (*_headerBatteryIcon).mIndex = BatteryInfo;
      _headerBatteryIcon.MarkItemModified(ItemKey::HeaderBatteryIcon::IndexItem);
      _headerBatteryIcon.SendUpdate(true);
   }
   else
   {
      //Update Battery Icon
      (*_headerBatteryIcon).mVisible = false;
      _headerBatteryIcon.MarkItemModified(ItemKey::HeaderBatteryIcon::VisibleItem);
      _headerBatteryIcon.SendUpdate(true);
   }
}


/**
 *  updateHeaderPhoneBTConnectInfo : Update the BT connection status data to status line.
 *
 * @param[in]      :
 * @return         : void
 */
void DataCollector::updateHeaderPhoneBTConnectInfo(uint8 BTConnectInfo)
{
   ETG_TRACE_USR4(("updateHeaderPhoneBTConnectInfo BT Connect Info:%d", BTConnectInfo));

   if (BTConnectInfo != 0)
   {
      //Update BT Icon
      (*_headerBTicon).mVisible = true;
      _headerBTicon.MarkItemModified(ItemKey::HeaderBTicon::VisibleItem);
      _headerBTicon.SendUpdate(true);

      (*_headerBTicon).mIndex = BTConnectInfo;
      _headerBTicon.MarkItemModified(ItemKey::HeaderBTicon::IndexItem);
      _headerBTicon.SendUpdate(true);
   }
   else
   {
      (*_headerBTicon).mVisible = false;
      _headerBTicon.MarkItemModified(ItemKey::HeaderBTicon::VisibleItem);
      _headerBTicon.SendUpdate(true);
   }
}


/**
 *  updateHeaderTAInfo : Update the TA info data to status line.
 *
 * @param[in]      :
 * @return         : void
 */
void DataCollector::updateHeaderTAInfo(uint8 TAInfo)
{
   //Update TA Icon
   if (TAInfo != 0)
   {
      //setHeaderTAIconVisibility(true);
      (*_headerTAicon).mVisible = true;
      _headerTAicon.MarkItemModified(ItemKey::HeaderTAicon::VisibleItem);
      _headerTAicon.SendUpdate(true);

      (*_headerTAicon).mIndex = TAInfo;
      _headerTAicon.MarkItemModified(ItemKey::HeaderTAicon::IndexItem);
      _headerTAicon.SendUpdate(true);
   }
   else
   {
      //setHeaderTAIconVisibility(false);
      (*_headerTAicon).mVisible = false;
      _headerTAicon.MarkItemModified(ItemKey::HeaderTAicon::VisibleItem);
      _headerTAicon.SendUpdate(true);
   }
}


/**
 *  updateMeterConnectionInfo : Update Meter Connection Info to phone settings screen.
 *
 * @param[in]      :
 * @return         : void
 */
void DataCollector::updateStatusLineDefaultData()
{
   ETG_TRACE_USR4(("updateStatusLineDefaultData "));

   this->vInitializeStatusLineDataDefault();
   this->setHeaderTextData(LANGUAGE_STRING(LINEIN__MAIN_Headline, "Aux"));
   this->setHeaderIconData(SRC_AUX);
}


/**
 *  updateMeterConnectionInfo : Update Meter Connection Info to phone settings screen.
 *
 * @param[in]      :
 * @return         : void
 */
void DataCollector::updateMeterConnectionInfo(bool MeterConnectionStatus)
{
   ETG_TRACE_USR4(("updateMeterConnectionInfo Meter Unit Connected:%d", MeterConnectionStatus));
}


/**
 *  updateHeaderTimeInfo : Update the time info data to status line.
 *
 * @param[in]      :
 * @return         : void
 */
void DataCollector::updateHeaderTimeInfo(std::string TimeInfo)
{
   ETG_TRACE_USR4(("updateHeaderTimeInfo Time Info:%s", TimeInfo.c_str()));
   //Update the time info
   (*_headerTime).mVisible = true;
   _headerTime.MarkItemModified(ItemKey::HeaderTime::VisibleItem);
   _headerTime.SendUpdate(true);

   (*_headerTime).mText = TimeInfo.c_str();
   _headerTime.MarkItemModified(ItemKey::HeaderTime::TextItem);
   _headerTime.SendUpdate(true);
}


/**
 *  setHeaderIconData : Update the Header Icon data to status line.
 * @param[in]      : HeaderIconData - Header Icon Data
 * @return         : void
 */
void DataCollector::setHeaderIconData(enHeaderIcon headerIconData)
{
   ETG_TRACE_USR4(("DataCollector: setHeaderIconData:%d", ETG_CENUM(enHeaderIcon, (enHeaderIcon)headerIconData)));
   //Update Header Icon
   setHeaderIconVisibility(true);

   (*_headerIcon).mIndex = headerIconData;
   _headerIcon.MarkItemModified(ItemKey::HeaderIcon::IndexItem);
   _headerIcon.SendUpdate(true);
}


/**
 * setHeaderTextData : Update the Header Icon data to status line.
 * @param[in]      : HeaderTextData - Header Text Data
 * @return         : void
 */
void DataCollector::setHeaderTextData(const Candera::String& headerTextData)
{
   ETG_TRACE_USR4(("DataCollector: setHeaderTextData:%s", headerTextData.GetCString()));
   //Update Header Text
   setHeaderTextVisibility(true);

   (*_headerText).mText = headerTextData;
   _headerText.MarkItemModified(ItemKey::HeaderText::TextItem);
   _headerText.SendUpdate(true);
}


/**
 *  vInitializeStatusLineDataDefault : It initializes the default data required for the status line.
 *
 * @return         : void
 */
void DataCollector::vInitializeStatusLineDataDefault()
{
   ETG_TRACE_USR4(("DataCollector: vInitializeStatusLineDataDefault"));

   //Update Header Icon
   setHeaderIconVisibility(false);

   //Update Header Text
   setHeaderTextVisibility(false);

   //Update Antenna Icon
   setHeaderAntennaIconVisibility(false);

   //Update Battery Icon
   setHeaderBatteryIconVisibility(false);

   //Update TA Icon
   setHeaderTAIconVisibility(false);

   //Update BT Icon
   setHeaderBTIconVisibility(false);

   //Update SMS Icon
   setHeaderSMSIconVisibility(false);

   //Update SMS Icon
   setHeaderSMSAutoIconVisibility(false);

   //Update GraceNote
   setHeaderGraceNoteVisibility(false);

   //Update SMS Auto Icon
   setHeaderTCUIconVisibility(false);

   //Update IconDyn Data
   setHeaderIconDynDataVisibility(false);
}


void DataCollector::setHeaderIconVisibility(bool isVisible)
{
   (*_headerIcon).mVisible = isVisible;
   _headerIcon.MarkItemModified(ItemKey::HeaderIcon::VisibleItem);
   _headerIcon.SendUpdate(true);
}


void DataCollector::setHeaderTextVisibility(bool isVisible)
{
   (*_headerText).mVisible = isVisible;
   _headerText.MarkItemModified(ItemKey::HeaderText::VisibleItem);
   _headerText.SendUpdate(true);
}


void DataCollector::setHeaderAntennaIconVisibility(bool isVisible)
{
   (*_headerAntennaIcon).mVisible = isVisible;
   _headerAntennaIcon.MarkItemModified(ItemKey::HeaderAntennaIcon::VisibleItem);
   _headerAntennaIcon.SendUpdate(true);
}


void DataCollector::setHeaderBatteryIconVisibility(bool isVisible)
{
   (*_headerBatteryIcon).mVisible = isVisible;
   _headerBatteryIcon.MarkItemModified(ItemKey::HeaderBatteryIcon::VisibleItem);
   _headerBatteryIcon.SendUpdate(true);
}


void DataCollector::setHeaderTAIconVisibility(bool isVisible)
{
   (*_headerTAicon).mVisible = isVisible;
   _headerTAicon.MarkItemModified(ItemKey::HeaderTAicon::VisibleItem);
   _headerTAicon.SendUpdate(true);
}


void DataCollector::setHeaderBTIconVisibility(bool isVisible)
{
   (*_headerBTicon).mVisible = isVisible;
   _headerBTicon.MarkItemModified(ItemKey::HeaderBTicon::VisibleItem);
   _headerBTicon.SendUpdate(true);
}


void DataCollector::setHeaderSMSIconVisibility(bool isVisible)
{
   (*_headerSMSicon).mVisible = isVisible;
   _headerSMSicon.MarkItemModified(ItemKey::HeaderSMSicon::VisibleItem);
   _headerSMSicon.SendUpdate(true);
}


void DataCollector::setHeaderSMSAutoIconVisibility(bool isVisible)
{
   (*_headerSMSautoIcon).mVisible = isVisible;
   _headerSMSautoIcon.MarkItemModified(ItemKey::HeaderSMSautoIcon::VisibleItem);
   _headerSMSautoIcon.SendUpdate(true);
}


void DataCollector::setHeaderGraceNoteVisibility(bool isVisible)
{
   (*_headerGraceNote).mVisible = isVisible;
   _headerGraceNote.MarkItemModified(ItemKey::HeaderGraceNoteIcon::VisibleItem);
   _headerGraceNote.SendUpdate(true);
}


void DataCollector::setHeaderTCUIconVisibility(bool isVisible)
{
   (*_headerTCUicon).mVisible = isVisible;
   _headerTCUicon.MarkItemModified(ItemKey::HeaderTCUicon::VisibleItem);
   _headerTCUicon.SendUpdate(true);
}


void DataCollector::setHeaderIconDynDataVisibility(bool isVisible)
{
   (*_headerIconDynData).mVisible = isVisible;
   _headerIconDynData.MarkItemModified(ItemKey::HeaderTCUicon::VisibleItem);
   _headerIconDynData.SendUpdate(true);
}


/**
 * setHeaderTextData : send the notification to Meter about the SWC PREV and NEXT key Press.
 * @param[in]      	 : Interrupt Type
 *  @return          : void
 */

bool DataCollector::onCourierMessage(const InterruptToMeterMsg& msg)
{
   bool actionMode = msg.GetInterruptMode();
   uint8 actionType = msg.GetInterruptType();
   uint8 sourceType = MediaDatabinding::getInstance().getCurrentActiveMediaDevice();

   if (sourceType == SOURCE_SPI)
   {
      //!Map SPI source type for the device type connected
      vMapSpiSourcetype();
   }
   std::map<uint32, ::datacollector_main_fi_types::T_e8_datacollector_Audio_Source>::iterator it_Source = m_Source_MAP.find(sourceType);
   std::map<uint32, ::datacollector_main_fi_types::T_e8_datacollector_Interrupt_Type>::iterator it_Action = m_Action_MAP.find(actionType);
   ETG_TRACE_USR4(("DataCollector: onSetInterrupt_to_Meter Action Type : %d, Source Type : %d ", actionType, sourceType));
   if (it_Action != m_Action_MAP.end() && it_Source != m_Source_MAP.end() && _statusBarHandler != NULL)
   {
      bool isVolumeAvailable = MediaDatabinding::getInstance().getVolumeAvailablity();
      if (isVolumeAvailable)
      {
         //_dataCollectorProxy->sendSetInterrupt_to_MeterStart(*this, true, it_Source->second, it_Action->second);
         _statusBarHandler->setInterruptToMeter(it_Source->second, it_Action->second, actionMode);
      }
      else
      {
         ETG_TRACE_USR4(("Volume is zero or in mute state and information is not passed to meter"));
      }
   }
   else
   {
      ETG_TRACE_USR4(("DataCollector: onSetInterrupt_to_Meter Out of Bound Error "));
   }
   return true;
}


/**
 * vMapSpiSourcetype : Map the SPI device source type to map the  source notification to Meter about the SWC/HK PREV and NEXT key Press.
 * @param[in]      	 : None
 *  @return          : void
 */

void DataCollector::vMapSpiSourcetype()
{
   ETG_TRACE_USR4(("DataCollector: vMapSpiSourcetype is called.."));
   if (_mpSpiConnectionHandling != NULL)
   {
      if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_DIPO == _mpSpiConnectionHandling->getActiveDeviceCategory())
      {
         m_Source_MAP[SOURCE_SPI] = ::datacollector_main_fi_types::T_e8_datacollector_Audio_Source__CARPLAY;
      }
      else if (::midw_smartphoneint_fi_types::T_e8_DeviceCategory__DEV_TYPE_ANDROIDAUTO == _mpSpiConnectionHandling->getActiveDeviceCategory())
      {
         m_Source_MAP[SOURCE_SPI] = ::datacollector_main_fi_types::T_e8_datacollector_Audio_Source__ANDROID_AUTO;
      }
   }
}


} // namespace Core
} // namespace App
