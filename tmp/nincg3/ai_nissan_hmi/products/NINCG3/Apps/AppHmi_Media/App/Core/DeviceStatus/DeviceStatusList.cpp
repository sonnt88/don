/*
 * DeviceStatusList.cpp
 *
 *  Created on: Oct 23, 2015
 *      Author: IVI-Media Team
 */
#include "hall_std_if.h"
#include "Core/DeviceStatus/DeviceStatusList.h"
#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
#include "Core/DeviceConnection/IDeviceConnection.h"
#include "mplay_MediaPlayer_FIProxy.h"
#else
#include "Core/utest/stubs/MPlay_fi_types.h"
#endif

#include "Core/Utils/MediaProxyUtility.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::DeviceStatusList::
#include "trcGenProj/Header/DeviceStatusList.cpp.trc.h"
#endif


namespace App {
namespace Core {

#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
/**
 * @Constructor
 */
DeviceStatusList::DeviceStatusList(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy,
                                   IDeviceConnection* pDeviceConnection): _usbStatus("---"), _usbInfo("Unknown Medium"), _cdInfo("No Medium"),
   _cdStatus("-----"),
   _iDeviceConnection(*pDeviceConnection),
   _mediaPlayerProxy(pMediaPlayerProxy)
{
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_MEDIA_DEVICE_STATUS, this);
   ETG_I_REGISTER_FILE();
}


/**getListDataProvider :
 * call respective list data provider for list data
 * @param[in] <listId> unique list id
 * @return  list data needed for respective scene
 */
tSharedPtrDataProvider DeviceStatusList::getListDataProvider(const ListDataInfo& listDataInfo)
{
   ETG_TRACE_USR4(("DTMStatusDriveList:getListDataProvider is called"));
   switch (listDataInfo.listId)
   {
      case LIST_ID_MEDIA_DEVICE_STATUS:
      {
         ETG_TRACE_USR4(("DTMStatusDriveList:getDTMSystemDriveListDataProvider is called"));
         return getDeviceStatusListDataProvider();
      }
      default:
      {
         ETG_TRACE_USR4(("DTMStatusDriveList:Default : ERROR:Invalid List"));
         return tSharedPtrDataProvider();
      }
   }
}


/**getDeviceStatusListDataProvider:
 * add list items to the List scene
 * @return  list data needed for respective scene
 */
tSharedPtrDataProvider DeviceStatusList::getDeviceStatusListDataProvider()
{
   //initializes the provider with the list id and the default data context

   ListDataProviderBuilder listBuilder(LIST_ID_MEDIA_DEVICE_STATUS, DEVICE_STATUS_LIST_BUTTON_SYSTEM_DRIVER_ITEM);
   uint32 itemIdx = 0;
   listBuilder.AddItem(itemIdx++, 0, DEVICE_STATUS_LIST_BUTTON_SYSTEM_DRIVER_ITEM).AddData("CD Drive Status:").AddData(getServiceSystemStatusItemValue(Item_01));
   listBuilder.AddItem(itemIdx++, 0, DEVICE_STATUS_LIST_BUTTON_SYSTEM_DRIVER_ITEM).AddData("CD Drive Media:").AddData(getServiceSystemStatusItemValue(Item_02));
   listBuilder.AddItem(itemIdx++, 0, DEVICE_STATUS_LIST_BUTTON_SYSTEM_DRIVER_ITEM).AddData("USB Status:").AddData(getServiceSystemStatusItemValue(Item_03));
   listBuilder.AddItem(itemIdx++, 0, DEVICE_STATUS_LIST_BUTTON_SYSTEM_DRIVER_ITEM).AddData("USB Card Media:").AddData(getServiceSystemStatusItemValue(Item_04));

   return listBuilder.CreateDataProvider();
}


#else
DeviceStatusList::DeviceStatusList(): _usbStatus("---"), _usbInfo("Unknown Medium"), _cdInfo("No Medium"), _cdStatus("-----")
{
}


#endif


#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
/**
 * @Destructor
 */
DeviceStatusList::~DeviceStatusList()
{
   _mediaPlayerProxy.reset();
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_MEDIA_DEVICE_STATUS);
}


#else
/**
 * @Destructor
 */
DeviceStatusList::~DeviceStatusList()
{
}


#endif


/**getServiceSystemStatusItemValue-
 * get the value for Version List
 * @return  string-name for item
 */
Candera::String DeviceStatusList::getServiceSystemStatusItemValue(uint8 ItemIndex)
{
   ETG_TRACE_USR4(("DTMStatusDriveList:getServiceSystemStatusItemValue is called"));
   std::string itemValue = "";
   switch (ItemIndex)
   {
      case Item_01:
      {
         itemValue = updateCDStatus();
         break;
      }
      case Item_02:
      {
         itemValue = updateCDInfo();
         break;
      }
      case Item_03:
      {
         itemValue = updateUSBStatus() + "MB";
         break;
      }
      case Item_04:
      {
         itemValue = updateUSBInfo();
         break;
      }
      default:
      {
         itemValue = "USB + MB";
      }
      break;
   }
   return itemValue.c_str();
}


#ifdef VARIANT_S_FTR_ENABLE_UNITTEST

/**setUSBInfo() - set USB status
 * @param[in] std::string
 * @param[out] None
 */
void DeviceStatusList::setUSBStatus(std::string usbStatus)
{
   _usbStatus = usbStatus;
}


/**setUSBInfo() - set USB information (USB device type)
 * @param[in] std::string
 * @param[out] None
 */
void DeviceStatusList::setUSBInfo(std::string usbInfo)
{
   _usbInfo = usbInfo;
}


/**setUSBInfo() - set CD status
 * @param[in] std::string
 * @param[out] None
 */
void DeviceStatusList::setCDStatus(std::string cdStatus)
{
   _cdStatus = cdStatus;
}


/**setCDInfo() - set CD information (CD device type)
 * @param[in] std::string
 * @param[out] None
 */
void DeviceStatusList::setCDInfo(std::string cdInfo)
{
   _cdInfo = cdInfo;
}


#endif


#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
/**updateUSBStatus() -
 * get usb Size
 * @param[in] None
 * @param[out] std::string
 */
std::string DeviceStatusList::updateUSBStatus()
{
   if (!_iDeviceConnection.getConnectedDeviceSize(_usbStatus))
   {
      _usbStatus = "---" ;
   }
   ETG_TRACE_USR4(("getConnectedDeviceSize = %s ", _usbStatus.c_str()));
   return _usbStatus;
}


#else
/**updateUSBStatus() - get dummy value of usb Size for unit testing
 * @param[in] None
 * @param[out] std::string
 */
std::string DeviceStatusList::updateUSBStatus()
{
   return _usbStatus;
}


#endif


/**
 * updateUSBInfo() -
 * update usb info
 * @param[in] None
 * @param[out] std::string
 */
std::string DeviceStatusList::updateUSBInfo()
{
   uint32 deviceType = MediaProxyUtility::getInstance().getActiveDeviceType();
   switch (deviceType)
   {
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_USB:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_MTP:
      {
         _usbInfo = "Mass Storage";
         break;
      }
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPOD:
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPHONE:
      {
         _usbInfo = "iPod";
         break;
      }
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_UNKNOWN:
      {
         _usbInfo = "No Medium";
         break;
      }
      default:
      {
         ETG_TRACE_USR4(("No device is connected"));
         break;
      }
   }
   return _usbInfo;
}


/**updateCDStatus():
 * update CD status
 * @param[in] None
 * @param[out] std::string
 */
std::string DeviceStatusList::updateCDStatus()
{
   return _cdStatus;
}


/**updateCDInfo():
 * update CD info
 * @param[in] None
 * @param[out] std::string
 */
std::string DeviceStatusList::updateCDInfo()
{
   uint32 deviceType = MediaProxyUtility::getInstance().getActiveDeviceType();
   switch (deviceType)
   {
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA:
      {
         _cdInfo = "Medium CDDA";
         break;
      }
      case MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDROM:
      {
         _cdInfo = "Medium CDMp3";
         break;
      }
      default:
      {
         ETG_TRACE_USR4(("updateCDInfo()"));
         break;
      }
   }

   return _cdInfo;
}


#ifndef VARIANT_S_FTR_ENABLE_UNITTEST
/**
 * onCourierMessage -

 * @param[in] ListDateProviderReqMsg
 * @parm[out] none
 * @return true - message consumed
 */
bool DeviceStatusList::onCourierMessage(const ListDateProviderReqMsg& oMsg)
{
   if (oMsg.GetListId() == LIST_ID_MEDIA_DEVICE_STATUS)
   {
      ListDataInfo listDataInfo;
      listDataInfo.listId = oMsg.GetListId();
      listDataInfo.startIndex =  oMsg.GetStartIndex();
      listDataInfo.windowSize = oMsg.GetWindowElementSize();
      return ListRegistry::s_getInstance().updateList(listDataInfo);
   }
   return true;
}


#endif
}// namespace AppLogic
} // namespace App
