/**
*  @file   MediaBrowseHandling.h
*  @author ECV - chs4cob
*  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
*  @addtogroup AppHmi_media
*/

#ifndef MEDIABROWSEHANDLING_H
#define MEDIABROWSEHANDLING_H

#include "CgiExtensions/ListDataProviderDistributor.h"
#include "CourierTunnelService/CourierMessageReceiverStub.h"
#include "Common/ListHandler/ListRegistry.h"
#include "Common/QuickSearch/QuickSearch.h"
#include "mplay_MediaPlayer_FIProxy.h"
#include "Core/Utils/MediaUtils.h"
#include "BrowseFactory.h"
#include "BrowserSpeedLock.h"
#include "asf/core/Timer.h"
#include "Core/PlayScreen/IPlayScreen.h"

namespace App {
namespace Core {
class IBrowse;
class IPlayScreen;

class MediaBrowseHandling :
   public ListImplementation
#ifdef BT_BROWSE_SUPPORT
   , public ::asf::core::TimerCallbackIF
#endif
{
   public:
      MediaBrowseHandling(::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& pMediaPlayerProxy, IPlayScreen* pPlayScreen);
      tSharedPtrDataProvider getListDataProvider(const ListDataInfo& _ListInfo);
      void updateListData(const ListResultInfo& _ListResultInfo);
      void updateEmptyListData(uint32 _currentListType) const;
      void updateFolderListData(const FolderListInfo& _FolderListItemInfo) const;
      virtual ~MediaBrowseHandling();
      static void TraceCmd_EncoderRotation(enEncoder nRotationDirection);
      bool isMediaListId(unsigned int);
      uint32 updateStartIndex(uint32 startIndex);
      uint32 SetListIdForAll(uint32 ListType);
#ifdef QUICKSEARCH_SUPPORT
      void SetActiveQSLanguague();
#endif

      void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);
      void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/);

#ifdef BT_BROWSE_SUPPORT
      void handleBTBrowseIdleState();
      virtual void onExpired(asf::core::Timer& timer, boost::shared_ptr<asf::core::TimerPayload> data);
#endif
      bool onCourierMessage(const ButtonReactionMsg& oMsg);
      bool onCourierMessage(const BrowseBtnPressUpdMsg& oMsg);
      bool onCourierMessage(const MetaDataBrowserBtnPressUpdMsg& oMsg);
      bool onCourierMessage(const FolderBrowserBtnPressUpdMsg& oMsg);
#ifdef QUICKSEARCH_SUPPORT
      bool onCourierMessage(const QuickSerachBtnPressUpdMsg& /*oMsg*/);
      bool onCourierMessage(const EncoderPositiveRotationUpdMsg& /*oMsg*/);
      bool onCourierMessage(const EncoderNegativeRotationUpdMsg& /*oMsg*/);
      bool onCourierMessage(const EncoderPressUpdMsg& /*oMsg*/);
#endif
      bool onCourierMessage(const MetaDataBrowserBackBtnPressUpdMsg& /*oMsg*/);
      bool onCourierMessage(const IndexingCompleteUpdMsg& oMsg);
      bool onCourierMessage(const InitialiseFolderBrowserReqMsg& /*oMsg*/);
      bool onCourierMessage(const FolderUpBtnPressReqMsg& /*oMsg*/);
      bool onCourierMessage(const InitializeMediaParametersUpdMsg& /*oMsg*/);
      bool onCourierMessage(const Courier::StartupMsg& /*oMsg*/);
      bool onCourierMessage(const FocusChangedUpdMsg& oMsg);

      virtual bool onCourierMessage(const ListDateProviderReqMsg& oMsg);
      virtual bool onCourierMessage(const ListChangedUpdMsg& oMsg);
#ifdef BROWSEINFO_CURRENT_PLAYLIST
      bool onCourierMessage(const CurrentPlayingListReqMsg& /*oMsg*/);
#endif
#ifdef BT_BROWSE_SUPPORT
      bool onCourierMessage(const BTBrowseTimerStopReqMsg& /*oMsg*/);
      bool onCourierMessage(const InitialiseBTBrowseMsg& /*oMsg*/);
#endif
#ifdef VERTICAL_KEYBOARD_SEARCH_SUPPORT
      bool onCourierMessage(const VerticalKeyLanguageSwitch& oMsg);
      bool onCourierMessage(const VKBSearchListUpMsg& oMsg);
      bool onCourierMessage(const VKBSearchListDownMsg& oMsg);
      virtual bool onCourierMessage(const ButtonListItemUpdMsg& oMsg);
#endif

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R
      bool onCourierMessage(const PlayAllSongsReqMsg& /*oMsg*/);
      // bool onCourierMessage(const InitialisePictureBrowserReqMsg& /*oMsg*/);
#endif
#ifdef ABCSEARCH_SUPPORT
      /** Called for the match speller when a key is pressed */
      bool onCourierMessage(const SpellerKeyPressed& oMsg);
      bool onCourierMessage(const ABCSearchBtnPressUpdMsg& /*oMsg*/);
#endif
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_MEDIA_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_DUMMY_CASE(0)
      ON_COURIER_MESSAGE(BrowseBtnPressUpdMsg)
      ON_COURIER_MESSAGE(MetaDataBrowserBtnPressUpdMsg)
      ON_COURIER_MESSAGE(FolderBrowserBtnPressUpdMsg)
#ifdef QUICKSEARCH_SUPPORT
      ON_COURIER_MESSAGE(QuickSerachBtnPressUpdMsg)
      ON_COURIER_MESSAGE(EncoderPositiveRotationUpdMsg)
      ON_COURIER_MESSAGE(EncoderNegativeRotationUpdMsg)
      ON_COURIER_MESSAGE(EncoderPressUpdMsg)
#endif
      ON_COURIER_MESSAGE(IndexingCompleteUpdMsg)
      ON_COURIER_MESSAGE(MetaDataBrowserBackBtnPressUpdMsg)
      ON_COURIER_MESSAGE(InitialiseFolderBrowserReqMsg)
      ON_COURIER_MESSAGE(ListChangedUpdMsg)
      ON_COURIER_MESSAGE(FolderUpBtnPressReqMsg)
      ON_COURIER_MESSAGE(InitializeMediaParametersUpdMsg)
      ON_COURIER_MESSAGE(Courier::StartupMsg)
      ON_COURIER_MESSAGE(FocusChangedUpdMsg)

#ifdef BROWSEINFO_CURRENT_PLAYLIST
      ON_COURIER_MESSAGE(CurrentPlayingListReqMsg)
#endif
#ifdef BT_BROWSE_SUPPORT
      ON_COURIER_MESSAGE(BTBrowseTimerStopReqMsg)
      ON_COURIER_MESSAGE(InitialiseBTBrowseMsg)
#endif
#ifdef VERTICAL_KEYBOARD_SEARCH_SUPPORT
      ON_COURIER_MESSAGE(ButtonListItemUpdMsg)
      ON_COURIER_MESSAGE(VerticalKeyLanguageSwitch)
      ON_COURIER_MESSAGE(VKBSearchListUpMsg)
      ON_COURIER_MESSAGE(VKBSearchListDownMsg)
#endif

#ifdef VARIANT_S_FTR_ENABLE_AIVI_SCOPE2_1_R
      ON_COURIER_MESSAGE(PlayAllSongsReqMsg)
      //  ON_COURIER_MESSAGE(InitialisePictureBrowserReqMsg)
#endif
#ifdef ABCSEARCH_SUPPORT
      ON_COURIER_MESSAGE(ABCSearchBtnPressUpdMsg)
      ON_COURIER_MESSAGE(SpellerKeyPressed)
#endif

      COURIER_MSG_MAP_DELEGATE_DEF_BEGIN()
      COURIER_MSG_DELEGATE_TO_CLASS(ListImplementation)
      COURIER_MSG_MAP_DELEGATE_DEF_END()
      COURIER_MSG_DELEGATE_TO_OBJ(_browserSpeedLock)
      COURIER_MSG_MAP_DELEGATE_END()

   private:
      void updateStartIndex(uint32 listId, bool isRequired);
      void requestFolderList(const ListDataInfo& listInfo);
      void requestMetaDataList(const ListDataInfo& listInfo);
      void populateFolderBrowser(uint32, std::string);
#ifdef QUICKSEARCH_SUPPORT
      void getSearchChar(std::string& ostr);
#endif
#ifdef BROWSER_FOOTER_DEFAULT_VALUES_SUPPORT
      void updateFooterDetails(uint32, uint32);
#endif
      ::boost::shared_ptr< ::mplay_MediaPlayer_FI::Mplay_MediaPlayer_FIProxy >& _mediaPlayerProxy;
      IPlayScreen& _iPlayScreen;

      IListFacade* _iListFacade;
#ifdef QUICKSEARCH_SUPPORT
      QuickSearch* _QuickSearch;
      QuickSearch* _QuickSearchIPOD;
      QuickSearch* _QuickSearchNotIpod;
#endif
      IBrowse* _iBrowse;
      bool _isRootBrowserUp;
      uint32 _windowStartIndex;
      uint32 _currentStartIndex;
#ifdef QUICKSEARCH_SUPPORT
      uint32 _quickSearchItemIndex;
#endif
      std::string _currentString;
      std::string _nextPrevCharacter;
      uint32 _currentMetaDataListType;
      int32 _focussedIndex;
      BrowserSpeedLock* _browserSpeedLock;
      bool isMessageProcessed ;
      uint32 _currentListId;
      bool _defaultLanguage;
#ifdef BT_BROWSE_SUPPORT
      asf::core::Timer _btBrowseTimer;
#endif
};


}//end of namespace Core
}//end of namespace App


#endif /* MEDIABROWSEHANDLING_H_ */
