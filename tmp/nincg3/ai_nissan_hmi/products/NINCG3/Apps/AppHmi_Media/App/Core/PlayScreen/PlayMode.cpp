/**
 * @file Playmode.cpp
 * @author RBEI/ECV-AIVI_MediaTeam
 * @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 * @addtogroup AppHMI_Media
 */

#include "Core/PlayScreen/PlayMode.h"

namespace App
{
namespace Core
{


/**
 * Playmode - Constructor, initializes class members
 *
 * @param[in] none
 * @parm[out] none
 * @return none
 */
PlayMode::PlayMode()
{
   _isRepeatActive = false;
   _isRandomActive = false;
   _oRandomRepeat = "";
   _oRandom = "";
   _oRepeat = "";
}


/**
 * Playmode - destructor, deinitializes class members
 *
 * @param[in] none
 * @parm[out] none
 * @return none
 */
PlayMode::~PlayMode()
{
   // TODO Auto-generated destructor stub
}


/**
 * Playmode - updateRandomRepeatState
 * @param[in] randomStatus, repeatStatus, currentPlayingSelection
 * @parm[out] void
 **/
void PlayMode::updateRandomRepeatState(::MPlay_fi_types::T_e8_MPlayMode randomStatus, ::MPlay_fi_types::T_e8_MPlayRepeat repeatStatus, PlayingSelection currentPlayingSelection)
{
   bool isRepeatActive = false;
   bool isRandomActive = false;
   Candera::String oRandomRepeat("");
   Candera::String oRandom("");
   Candera::String oRepeat("");

   if (randomStatus == ::MPlay_fi_types::T_e8_MPlayMode__e8PBM_RANDOM)
   {
      isRandomActive = true;
      oRandom = LANGUAGE_STRING(USB__MAIN_FooterButton_Random , "Random ON");
   }
   else
   {
      isRandomActive = false;
   }

   if (repeatStatus ==::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_ONE)
   {
      isRepeatActive = true;
      oRandomRepeat = LANGUAGE_STRING(USB__MAIN_FooterButton_RepeatTrack, "Repeat Track");
      oRepeat = LANGUAGE_STRING(USB__MAIN_FooterButton_RepeatTrack, "Repeat Track");
   }
   else if (currentPlayingSelection == FOLDER_BASED_SELECTION)
   {
      if (repeatStatus ==::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS)
      {
         isRepeatActive = true;
         oRepeat = LANGUAGE_STRING(USB__MAIN_FooterButton_RepeatFolder, "Repeat Folder");
         if (isRandomActive)
         {
            oRandomRepeat = LANGUAGE_STRING(USB__MAIN_FooterButton_RandomFolder, "Random Folder");
         }
         else
         {
            oRandomRepeat = oRepeat;
         }
      }
      else if (repeatStatus ==::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_ALL)
      {
         isRepeatActive = false;
         if (isRandomActive)
         {
            oRandomRepeat = LANGUAGE_STRING(USB__MAIN_FooterButton_RandomAll, "Random All");
         }
      }
   }
   else if (currentPlayingSelection == METADATA_BASED_SELECTION)
   {
      if (repeatStatus ==::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS || repeatStatus ==::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_LIST
            || repeatStatus ==::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_NONE)
      {
         isRepeatActive = false;
         if (isRandomActive)
         {
            oRandomRepeat = LANGUAGE_STRING(USB__MAIN_FooterButton_RandomAll, "Random All");
         }
      }
   }

   _isRepeatActive  = isRepeatActive;
   _isRandomActive = isRandomActive;
   _oRandomRepeat  = oRandomRepeat;
   _oRandom = oRandom;
   _oRepeat = oRepeat;

}

/**
 * Playmode - getNextRepeatState
 * @param[in] _currentListHandleType
 * @parm[out] _repeatStatus
 **/
::MPlay_fi_types::T_e8_MPlayRepeat PlayMode::getNextRepeatState(PlayingSelection currentPlayingSelection, ::MPlay_fi_types::T_e8_MPlayRepeat repeatStatus)
{
   ::MPlay_fi_types::T_e8_MPlayRepeat nextRepeatStatus;
   if (currentPlayingSelection == FOLDER_BASED_SELECTION)
   {
      switch (repeatStatus)
      {
         case::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_ALL:
         {
            nextRepeatStatus =::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS;

            break;
         }
         case::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_ONE:
         {
            nextRepeatStatus =::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_ALL;
            break;
         }
         case::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_LIST_WITH_SUBLISTS:
         case::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_LIST:
         {

            nextRepeatStatus =::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_ONE;
            break;
         }
         default:
         {
            nextRepeatStatus =::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_ALL;
            break;
         }
      }
   }
   else//Meta data
   {
      if (repeatStatus ==::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_ONE)
      {
         nextRepeatStatus =::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_LIST;
      }
      else
      {
         nextRepeatStatus =::MPlay_fi_types::T_e8_MPlayRepeat__e8RPT_ONE;
      }

   }
   return nextRepeatStatus;
}


/**
 * Playmode - getNextRandomState
 * @param[in] _currentListHandleType
 * @parm[out] _randomStatus
 **/
::MPlay_fi_types::T_e8_MPlayMode PlayMode::getNextRandomState(::MPlay_fi_types::T_e8_MPlayMode randomStatus)
{
   ::MPlay_fi_types::T_e8_MPlayMode nextrandom;
   if (randomStatus ==::MPlay_fi_types::T_e8_MPlayMode__e8PBM_NORMAL)
   {
      nextrandom =::MPlay_fi_types::T_e8_MPlayMode__e8PBM_RANDOM;
   }
   else
   {
      nextrandom =::MPlay_fi_types::T_e8_MPlayMode__e8PBM_NORMAL;
   }
   return nextrandom;
}


} /* namespace Core */
} /* namespace App */
