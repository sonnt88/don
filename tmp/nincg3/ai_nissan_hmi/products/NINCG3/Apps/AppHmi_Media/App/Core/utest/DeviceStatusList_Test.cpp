/**
*  @file   <DeviceStatusList_Test.cpp>
*  @author <ECV> - <pul1hc>
*  @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
*  @addtogroup <AppHmi_media>
*  @{
*/

#include "DeviceStatusList_Test.h"


INSTANTIATE_TEST_CASE_P(
   DeviceStatusList_updateUSBInfo,
   DeviceStatusList_Test__updateUSBInfo_withValidParam,
   ::testing::Values
   (
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_USB, "Mass Storage"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_MTP, "Mass Storage"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPOD, "iPod"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPHONE, "iPod"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_UNKNOWN, "No Medium")
   )
);

TEST_P(DeviceStatusList_Test__updateUSBInfo_withValidParam, updateUSBInfo_supportedDeviceTypes_valid)
{
   std::string strExpectedUSBInfo = _deviceStatusList.updateUSBInfo();
   EXPECT_EQ(_strExpectedUSBInfo, strExpectedUSBInfo);
}


INSTANTIATE_TEST_CASE_P(
   DeviceStatusList_updateUSBInfo,
   DeviceStatusList_Test__updateUSBInfo_withInvalidParam,
   ::testing::Values
   (
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_SD, "Unknown Medium"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH, "Unknown Medium"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDROM, "Unknown Medium"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_FLASH, "Unknown Medium"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA, "Unknown Medium"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_UNSUPPORTED, "Unknown Medium"),
      make_tuple(11, "Unknown Medium"),
      make_tuple(-1, "Unknown Medium")
   )
);

TEST_P(DeviceStatusList_Test__updateUSBInfo_withInvalidParam, updateUSBInfo_unsupportedDeviceTypes_invalid)
{
   std::string strExpectedUSBInfo = _deviceStatusList.updateUSBInfo();
   EXPECT_EQ(_strExpectedUSBInfo, strExpectedUSBInfo);
}


INSTANTIATE_TEST_CASE_P(
   DeviceStatusList_updateCDInfo,
   DeviceStatusList_Test__updateCDInfo_withValidParam,
   ::testing::Values
   (
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA, "Medium CDDA"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDROM, "Medium CDMp3")
   )
);

TEST_P(DeviceStatusList_Test__updateCDInfo_withValidParam, updateCDInfo_supportedDeviceTypes_valid)
{
   std::string strExpectedCDInfo = _deviceStatusList.updateCDInfo();
   EXPECT_EQ(_strExpectedCDInfo, strExpectedCDInfo);
}


INSTANTIATE_TEST_CASE_P(
   DeviceStatusList_updateCDInfo,
   DeviceStatusList_Test__updateCDInfo_withInvalidParam,
   ::testing::Values
   (
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_UNKNOWN, "No Medium"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPHONE, "No Medium"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_SD, "No Medium"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_BLUETOOTH, "No Medium"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_MTP, "No Medium"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_USB, "No Medium"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_FLASH, "No Medium"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPOD, "No Medium"),
      make_tuple(MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_UNSUPPORTED, "No Medium"),
      make_tuple(11, "No Medium"),
      make_tuple(-1, "No Medium")
   )
);

TEST_P(DeviceStatusList_Test__updateCDInfo_withInvalidParam, updateCDInfo_unsupportedDeviceTypes_invalid)
{
   std::string strExpectedCDInfo = _deviceStatusList.updateCDInfo();
   EXPECT_EQ(_strExpectedCDInfo, strExpectedCDInfo);
}


INSTANTIATE_TEST_CASE_P(
   DeviceStatusList_getServiceSystemStatusItemValue,
   DeviceStatusList_Test__getServiceSystemStatusItemValue_withValidParam,
   ::testing::Values
   (
      make_tuple(Item_01, MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDROM, "650MB"),
      make_tuple(Item_02, MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDROM, "Medium CDMp3"),
      make_tuple(Item_03, MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_USB, "16384MB"),
      make_tuple(Item_04, MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_USB, "Mass Storage")
   )
);

TEST_P(DeviceStatusList_Test__getServiceSystemStatusItemValue_withValidParam, getServiceSystemStatusItemValue_inSupportedRangeListItems_valid)
{
   std::string cStrExpectedItemValue = _deviceStatusList.getServiceSystemStatusItemValue(_itemIndex).GetCString();
   EXPECT_EQ(_cStrExpectedItemValue, cStrExpectedItemValue);
}


INSTANTIATE_TEST_CASE_P(
   DeviceStatusList_getServiceSystemStatusItemValue,
   DeviceStatusList_Test__getServiceSystemStatusItemValue_withInvalidParam,
   ::testing::Values
   (
      make_tuple(-1, MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA, "USB + MB"),
      make_tuple(0, MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA, "USB + MB"),
      make_tuple(5, MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_CDDA, "USB + MB"),
      make_tuple(6, MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPOD, "USB + MB"),
      make_tuple(100, MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPOD, "USB + MB"),
      make_tuple(4294967296, MPlay_fi_types::T_e8_MPlayDeviceType__e8DTY_IPHONE, "USB + MB")
   )
);

TEST_P(DeviceStatusList_Test__getServiceSystemStatusItemValue_withInvalidParam, getServiceSystemStatusItemValue_outOfSupportedRangeListItems_invalid)
{
   std::string cStrExpectedItemValue = _deviceStatusList.getServiceSystemStatusItemValue(_itemIndex).GetCString();
   EXPECT_EQ(_cStrExpectedItemValue, cStrExpectedItemValue);
}
