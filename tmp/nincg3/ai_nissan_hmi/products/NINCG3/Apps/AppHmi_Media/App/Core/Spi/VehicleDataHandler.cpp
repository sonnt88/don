/**
 *  @file   Vehicledatahandler.cpp
 *  @author ECV - alc7kor
 *  @copyright (c) 2015 Robert Bosch Car Multimedia GmbH
 *  @addtogroup AppHmi_media
 */

#include "hall_std_if.h"

#include "VehicleDataHandler.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS         TR_CLASS_APPHMI_MEDIA_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_MEDIA
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_MEDIA_"
#define ETG_I_FILE_PREFIX                 App::Core::VehicleDataHandler::
#include "trcGenProj/Header/VehicleDataHandler.cpp.trc.h"
#endif

using namespace ::App::Core;
using namespace ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData;

namespace App {
namespace Core {
/*VehicleDataHandler destructor*/

VehicleDataHandler::~VehicleDataHandler()
{
   ETG_TRACE_USR4(("Spi VehicleDataHandler VehicleDataHandler destructor"));
   _mbAccessoryDisplayContext = false;
   if (_commonVehicle) // De register from  common
   {
      _commonVehicle->vUnRegisterforSpeedLock(this);
      _commonVehicle->vUnRegisterforHandBrake(this);
   }
   _spiVehicleinfoProxy.reset();
   _spiDimmingFiProxy.reset();
   _hmiDataProxy.reset();
   _spiServiceProxy.reset();
   _commonVehicle = NULL;
   _mSpiConnectionHandling = NULL;
}


/*VehicleDataHandler constructor*/

VehicleDataHandler::VehicleDataHandler(::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& pSpiVehicleinfoProxy, ::boost::shared_ptr< ::dimming_main_fi::Dimming_main_fiProxy >& pSpiDimmingfiproxy , SpiConnectionHandling* paraSpiConnectionHandling, ::VehicleDataCommonHandler::VehicleDataHandler* commonVehicle , ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& spiServiceProxy)
   : _mSpiConnectionHandling(paraSpiConnectionHandling), _spiVehicleinfoProxy(pSpiVehicleinfoProxy), _spiDimmingFiProxy(pSpiDimmingfiproxy), _hmiDataProxy(HmiDataProxy::createProxy("hmiDataServicePort", *this)), _commonVehicle(commonVehicle)
   , _spiServiceProxy(spiServiceProxy)
{
   ETG_TRACE_USR4(("Spi VehicleDataHandler VehicleDataHandler constructor"));
   StartupSync::getInstance().registerPropertyRegistrationIF(this);
   _mbAccessoryDisplayContext = false;
   if (_commonVehicle) // De register from common
   {
      _commonVehicle->vRegisterforSpeedLock(this);
      _commonVehicle->vRegisterforHandBrake(this);
   }
   _mParkBrakeInfo = T_e8_ParkBrake__PARK_BRAKE_NOT_ENGAGED;
   _mVehicleState = T_e8_VehicleState__PARK_MODE;
}


/* This function performs the upreg of the required properties.
 * @param[in]  - proxy: Pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - stateChange: Contains the current Service State
 * @param[out] - None
 * @return     - Void
 */

void VehicleDataHandler::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   ETG_TRACE_USR4(("Spi VehicleDataHandler registerProperties"));
   if (_spiVehicleinfoProxy && (proxy == _spiVehicleinfoProxy))
   {
      _spiVehicleinfoProxy->sendSpeedLimitUpReg(*this);
      _spiVehicleinfoProxy->sendSpeedUpReg(*this);
   }
   if (_spiDimmingFiProxy && (proxy == _spiDimmingFiProxy))
   {
      _spiDimmingFiProxy->sendDIM_INFO_DimmingModeUpReg(*this);
   }
   if (_hmiDataProxy && (proxy == _hmiDataProxy))
   {
      _hmiDataProxy->sendCurrentHMIStateRegister(*this);
      _hmiDataProxy->sendCurrentHMIStateGet(*this);
      _hmiDataProxy->sendVisibleApplicationModeRegister(*this);
      _hmiDataProxy->sendVisibleApplicationModeGet(*this);
   }
}


/* This function performs the Relupreg of the required properties.
 * @param[in]  - proxy: Pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - stateChange: Contains the current Service State
 * @param[out] - None
 * @return     - Void
 */

void VehicleDataHandler::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   ETG_TRACE_USR4(("Spi VehicleDataHandler DeregisterProperties"));
   if (_spiVehicleinfoProxy && (proxy == _spiVehicleinfoProxy))
   {
      _spiVehicleinfoProxy->sendSpeedLimitRelUpRegAll();
      _spiVehicleinfoProxy->sendSpeedRelUpRegAll();
   }
   if (_spiDimmingFiProxy && (proxy == _spiDimmingFiProxy))
   {
      _spiDimmingFiProxy->sendDIM_INFO_DimmingModeRelUpRegAll();
   }
   if (_hmiDataProxy && (proxy == _hmiDataProxy))
   {
      _hmiDataProxy->sendCurrentHMIStateDeregisterAll();
      _hmiDataProxy->sendVisibleApplicationModeDeregisterAll();
   }
}


/*
 * This function is called when the property update for SpeedLimit is received with error
 * @param[in]  - proxy: pointer to VEHICLE_MAIN_FIProxy
 * @param[in]  - error : pointer to VEHICLE_MAIN_FIProxy
 * @param[out] - None
 * @return     - Void
 */
void VehicleDataHandler:: onSpeedLimitError(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::SpeedLimitError >&/*error*/)
{
}


/*
 * This function is called when the property update for SpeedLimit is received
 * @param[in]  - proxy: pointer to VEHICLE_MAIN_FIProxy
 * @param[in]  - status : pointer to VEHICLE_MAIN_FIProxy
 * @param[out] - None
 * @return     - Void
 */
void VehicleDataHandler:: onSpeedLimitStatus(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::SpeedLimitStatus >& /*status*/)
{
}


/*
 * This function is called when the property update for DIM_INFO_DimmingMode is received with error
 * @param[in]  - proxy: pointer to Dimming_main_fiProxy
 * @param[in]  - error: pointer to Dimming_main_fiProxy
 * @param[out] - None
 * @return     - Void
 */
void VehicleDataHandler::onDIM_INFO_DimmingModeError(const ::boost::shared_ptr< ::dimming_main_fi::Dimming_main_fiProxy >&/*proxy*/, const ::boost::shared_ptr< ::dimming_main_fi:: DIM_INFO_DimmingModeError >&/* error*/)
{
   ETG_TRACE_USR4(("Spi VehicleDataHandler onDIM_INFO_DimmingModeError  Received ..."));
}


/*
 * This function is called when the property update for DIM_INFO_DimmingMode is received
 * @param[in]  - proxy: pointer to Dimming_main_fiProxy
 * @param[in]  - status: pointer to Dimming_main_fiProxy
 * @param[out] - None
 * @return     - Void
 */
void VehicleDataHandler::onDIM_INFO_DimmingModeStatus(const ::boost::shared_ptr< ::dimming_main_fi::Dimming_main_fiProxy >& proxy, const ::boost::shared_ptr< ::dimming_main_fi:: DIM_INFO_DimmingModeStatus >& status)
{
   if (proxy == _spiDimmingFiProxy)
   {
      ETG_TRACE_USR4(("Spi VehicleDataHandler onDIM_INFO_DimmingModeStatus  Received ...DimmingConfig = %d DimmingCar = %d hasCar = %d hasconfig = %d DimmingStatus = %d " , status->getConfig(), status->getCar(), status->hasCar(), status->hasConfig(), status->getStatus()));
      setDimmingModeStatus(status);
   }
}


/*
 * This function is a helper function to set the dimming mode status
 * @param[in]  - ::dimming_main_fi_types::T_e8_DIM_Mode eDim_Mode
 * @param[out] - None
 * @return     - Void
 */
void  VehicleDataHandler::setDimmingModeStatus(const ::boost::shared_ptr< ::dimming_main_fi:: DIM_INFO_DimmingModeStatus >& status)
{
   ETG_TRACE_USR4(("Spi VehicleDataHandler setDimmingModeStatus Enter ..."));
   enVehicle_Configuration eDimmingMode = Vehicle_Configuration__DAY_MODE;
   bool bSetDimmingMode = true;
   switch (status->getConfig())
   {
      case ::dimming_main_fi_types::T_e8_DIM_Mode__Day:
      {
         eDimmingMode = Vehicle_Configuration__DAY_MODE;
         ETG_TRACE_USR4(("Spi VehicleDataHandler::setDimmingModeStatus eDimmingMode = %d ", ETG_CENUM(enVehicle_Configuration, eDimmingMode)));
         break;
      }
      case ::dimming_main_fi_types::T_e8_DIM_Mode__Night:
      {
         eDimmingMode = Vehicle_Configuration__NIGHT_MODE;
         ETG_TRACE_USR4(("Spi VehicleDataHandler::setDimmingModeStatus eDimmingMode = %d ", ETG_CENUM(enVehicle_Configuration, eDimmingMode)));
         break;
      }
      case ::dimming_main_fi_types::T_e8_DIM_Mode__Auto:
      {
         ETG_TRACE_USR4(("Spi VehicleDataHandler::onDIM_INFO_DimmingModeStatus Auto"));
         ::dimming_main_fi_types::T_e8_DIM_Mode eDimModeSet = status->getCar();
         if (eDimModeSet == (::dimming_main_fi_types::T_e8_DIM_Mode__Night))
         {
            eDimmingMode = Vehicle_Configuration__NIGHT_MODE;
         }
         break;
      }
      default :
      {
         break;
      }
   }
   ETG_TRACE_USR4(("Spi VehicleDataHandler::setDimmingModeStatus:setVehicleConfiguration called :eDimmingMode = %d bSetDimmingMode = %d", ETG_CENUM(enVehicle_Configuration, eDimmingMode), bSetDimmingMode));
   _mSpiConnectionHandling->setVehicleConfiguration(eDimmingMode, bSetDimmingMode);
}


/*
 * This function is called when the property update for CurrentHMIState fails
 * @param[in]  - proxy: pointer to HMIDATASERVICE
 * @param[in]  - error: Pointer to HMIDATASERVICE
 * @param[out] - None
 * @return     - Void
 */
void  VehicleDataHandler::onCurrentHMIStateError(const ::boost::shared_ptr< HMIDATASERVICE::HmiDataProxy >& /*proxy*/, const ::boost::shared_ptr< HMIDATASERVICE::CurrentHMIStateError >& /*error*/)
{
}


/*
 * This function is called when the property update for CurrentHMIState update receive
 * @param[in]  - proxy: pointer to HMIDATASERVICE
 * @param[in]  - update: Pointer to HMIDATASERVICE
 * @param[out] - None
 * @return     - Void
 */
void  VehicleDataHandler::onCurrentHMIStateUpdate(const ::boost::shared_ptr< HMIDATASERVICE::HmiDataProxy >& proxy, const ::boost::shared_ptr< HMIDATASERVICE::CurrentHMIStateUpdate >& update)
{
   if (proxy == _hmiDataProxy)
   {
      ETG_TRACE_USR4(("Spi VehicleDataHandler::onCurrentHMIStateUpdate HMIState = %d", update->getCurrentHMIState()));
      enDisplayContext eDisplayContext = DISPLAY_CONTEXT_EMERGENCY;
      bool bDisplayflag = false;
      if (_mSpiConnectionHandling != 0)
      {
         _mSpiConnectionHandling->setCurrentHMIState(update->getCurrentHMIState());

         if (HMI_MODE_RVC_AVM == update->getCurrentHMIState())
         {
            ETG_TRACE_USR4(("Spi VehicleDataHandler::onCurrentHMIStateUpdate RVC"));
            bDisplayflag = true;
            _mbAccessoryDisplayContext = true;
            _mSpiConnectionHandling->setAccessoryDisplayContext(bDisplayflag, eDisplayContext);
            _mSpiConnectionHandling->setRVCStatus(true);
         }
         else if (_mbAccessoryDisplayContext == true)
         {
            _mbAccessoryDisplayContext = false;
            bDisplayflag = false;
            _mSpiConnectionHandling->setRVCStatus(false);
            _mSpiConnectionHandling->setAccessoryDisplayContext(bDisplayflag, eDisplayContext);
            ETG_TRACE_USR4(("Spi VehicleDataHandler setAccessoryDisplayContext Reset Emergency with display false"));
         }
      }
      else
      {
         ETG_TRACE_USR4(("_mSpiConnectionHandling is 0"));
      }
   }
}


/*
 * skn6kor: This function is called when property udpates for lockStateApplicationUpdate are received in CGIAppControlled from userinterfacectrl
 * @param[in]  - appLockStateMsg
 * @return     - bool
 */
bool VehicleDataHandler::onCourierMessage(const SpiLockApplicationUpdMsg& appLockStateMsg)
{
   int32 applicationId = appLockStateMsg.GetApplicationId();
   bool lockState = appLockStateMsg.GetLockState();

   ETG_TRACE_USR4(("onlockStateApplicationUpdate lockState = %d,applicationId=%d", lockState, applicationId));
   enDisplayContext eDisplayContext = DISPLAY_CONTEXT_EMERGENCY;
   bool bDisplayflag = false;
   if (_mSpiConnectionHandling != 0)
   {
      if (TRUE == lockState)
      {
#ifdef SPI_TEMPORARYCONTEXT_ONRVC_NCG3D_7788
         if (true == _mSpiConnectionHandling->bGetIsSpiVideoActive())
         {
            _mSpiConnectionHandling->vSetTemporaryContextOnRVC(true);
         }
#endif
         ETG_TRACE_USR4(("Spi VehicleDataHandler::onCurrentHMIStateUpdate RVC"));
         bDisplayflag = true;
         _mbAccessoryDisplayContext = true;
         _mSpiConnectionHandling->setAccessoryDisplayContext(bDisplayflag, eDisplayContext);
         _mSpiConnectionHandling->setRVCStatus(true);
      }
      else if (_mbAccessoryDisplayContext == true)
      {
         _mbAccessoryDisplayContext = false;
         bDisplayflag = false;
         _mSpiConnectionHandling->setRVCStatus(false);
         _mSpiConnectionHandling->setAccessoryDisplayContext(bDisplayflag, eDisplayContext);
         ETG_TRACE_USR4(("Spi VehicleDataHandler setAccessoryDisplayContext Reset Emergency with display false"));
      }
   }
   else
   {
      ETG_TRACE_USR4(("onlockStateApplicationUpdate _mSpiConnectionHandling is o"));
   }
   return true;
}


/*
 * This function is called when currentSpeedLockState update receive from Common
 * @param[in]  - State: pointer to SPEEDLOCK_ACTIVE/SPEEDLOCK_INACTIVE
 * @param[out] - None
 * @return     - Void
 */
void VehicleDataHandler::currentSpeedLockState(bool State)
{
   if (SPEEDLOCK_ACTIVE == State)
   {
      _mVehicleState = T_e8_VehicleState__DRIVE_MODE;
   }
   else
   {
      _mVehicleState = T_e8_VehicleState__PARK_MODE;
   }
   ETG_TRACE_USR4(("Spi VehicleDataHandler::currentSpeedLockState vehiclestate  = %d ", ETG_CENUM(enVehicle_Configuration, _mVehicleState)));
   if (_mSpiConnectionHandling != NULL)
   {
      _mSpiConnectionHandling->setVehicleMovementState(_mParkBrakeInfo, _mVehicleState);
   }
}


/*
 * This function is called when currentHandBrakeState update receive from Common
 * @param[in]  - State: hand break state
 * @param[out] - None
 * @return     - Void
 */
void VehicleDataHandler::currentHandBrakeState(uint8 State)
{
   ETG_TRACE_USR4(("Spi VehicleDataHandler::currentHandBrakeState  = %d ", ETG_CENUM(enVehicle_HandBrakeState, State)));
   switch (State)
   {
      case HAND_BRAKE_INFO_UNAVAILABLE :
      case HAND_BRAKE_NOT_ENGAGED :
      {
         _mParkBrakeInfo = T_e8_ParkBrake__PARK_BRAKE_NOT_ENGAGED; //@todo: hijacking unavailable case to make AAP work.
         break;
      }
      case HAND_BRAKE_ENGAGED :
      {
         _mParkBrakeInfo = T_e8_ParkBrake__PARK_BRAKE_ENGAGED;
         break;
      }
      default:
         break;
   }

   if (_mSpiConnectionHandling != NULL)
   {
      _mSpiConnectionHandling->setVehicleMovementState(_mParkBrakeInfo, _mVehicleState);
   }
}


/*
 * This function is called when the property update for Speed is received with error
 * @param[in]  - proxy: pointer to VEHICLE_MAIN_FIProxy
 * @param[in]  - error: pointer to speederror
 * @param[out] - None
 * @return     - Void
 */
void VehicleDataHandler:: onSpeedError(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& /*proxy*/, const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::SpeedError >& /*error*/)
{
   ETG_TRACE_USR4(("Spi VehicleDataHandler::onSpeedError received ..."));
}


/*
 * This function is called when the property update for Speed is received
 * @param[in]  - proxy: pointer to VEHICLE_MAIN_FIProxy
 * @param[in]  - status: pointer to speedstatus
 * @param[out] - None
 * @return     - Void
 */
void VehicleDataHandler:: onSpeedStatus(const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, const ::boost::shared_ptr< ::VEHICLE_MAIN_FI::SpeedStatus >& status)
{
   ETG_TRACE_USR4(("Spi VehicleDataHandler::onSpeedStatus speedvalue  = %d", status->getSpeedValue()));
   if (proxy == _spiVehicleinfoProxy)
   {
      if ((SPEED_INVALID_VALUE != status->getSpeedValue()) && _spiServiceProxy)
      {
         int32 i32speed = int32((status->getSpeedValue() * 5) / 18); //conversion from km/hr to cm/sec (speed*.01*100*1000)/3600
         ETG_TRACE_USR4(("Spi VehicleDataHandler::sendVehicleMechanicalSpeedStart speed in cm/sec = %d ", i32speed));
         _spiServiceProxy->sendVehicleMechanicalSpeedStart(*this , i32speed);
      }
   }
}


/*
 * Virtual function implemented to get an update of VehicleMechanicalSpee
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - error : pointer to VehicleMechanicalSpeedError
 * @param[out] - None
 * @return     - Void
 */
void VehicleDataHandler:: onVehicleMechanicalSpeedError(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& /*proxy*/, const ::boost::shared_ptr< ::midw_smartphoneint_fi::VehicleMechanicalSpeedError >& /*error*/)
{
   ETG_TRACE_USR4(("Spi VehicleDataHandler::onVehicleMechanicalSpeedError  received ... "));
}


/*
 * Virtual function implemented to get an update of onVehicleMechanicalSpeedResult
 * @param[in]  - proxy: pointer to the type Midw_smartphoneint_fiProxy
 * @param[in]  - status: Pointer to the onVehicleMechanicalSpeedResult
 * @param[out] - None
 * @return     - Void
 */
void VehicleDataHandler:: onVehicleMechanicalSpeedResult(const ::boost::shared_ptr< ::midw_smartphoneint_fi::Midw_smartphoneint_fiProxy >& proxy, const ::boost::shared_ptr< ::midw_smartphoneint_fi::VehicleMechanicalSpeedResult >& result)
{
   if (proxy == _spiServiceProxy)
   {
      ETG_TRACE_USR4(("Spi VehicleDataHandler::onVehicleMechanicalSpeedResult  ResponseCode = %d ErrorCode = %d", result->getResponseCode() , result->getErrorCode()));
   }
}


void VehicleDataHandler::onVisibleApplicationModeError(const ::boost::shared_ptr< HMIDATASERVICE::HmiDataProxy >& /*proxy*/, const ::boost::shared_ptr< HMIDATASERVICE::VisibleApplicationModeError >& /*error*/)
{
}


void VehicleDataHandler::onVisibleApplicationModeUpdate(const ::boost::shared_ptr< HMIDATASERVICE::HmiDataProxy >& proxy, const ::boost::shared_ptr< HMIDATASERVICE::VisibleApplicationModeGet >& update)
{
   if (proxy == _hmiDataProxy)
   {
      ETG_TRACE_USR4(("onVisibleApplicationModeUpdate =%d", update->getVisibleApplicationMode()));
      if (APP_MODE_SOURCE == update->getVisibleApplicationMode())
      {
         if (_mSpiConnectionHandling != NULL)
         {
            if ((_mSpiConnectionHandling->getCurrentActiveAudioSource() == SRC_SPI_ENTERTAIN) && (false == _mSpiConnectionHandling->bGetIsSpiVideoActive()))
            {
               ETG_TRACE_USR4((" VehicleDataHandler : LaunchApp sent with Now Playing"));
               // _mSpiConnectionHandling->vLaunchApp(_mSpiConnectionHandling->getActiveDeviceHandle(), (enDeviceCategory)_mSpiConnectionHandling->getActiveDeviceCategory(), DiPOAppType__DIPO_NOWPLAYING);
            }
         }
      }
   }
}


} //namespace Core
} //namespace app
