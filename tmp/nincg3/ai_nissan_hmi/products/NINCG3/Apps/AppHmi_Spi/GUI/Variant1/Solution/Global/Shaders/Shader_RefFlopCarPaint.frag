/**
  * This Fragment Shader supports:
  * - Mixes the two color components together based on the camera position and a user defined bias.
  * - Adding reflection from a cube map to the model.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

precision mediump float;

// Uniforms
uniform float u_ColorBias;             // How strong the mixture of the two colors is.
uniform float u_ReflectionIntensity;   // How strong the reflection is.
uniform samplerCube u_CubeMapTexture;  // Cube map - environment.

// Varying
varying float v_ColorBlend;   // The mix value of the two colors depending on the line of sight.
varying vec3 v_Normal;
varying vec3 v_LineOfSight;   // World space vector from camera position to vertex.

// Light values
varying vec4 v_Diffuse;       // Primary color.
varying vec4 v_Emissive;      // Secondary color.
varying vec4 v_Color;         // The precalculated ambient and specular from the vertex shader.

void main(void)
{  
   gl_FragColor = v_Color;
   
   float lerpValue = 0.0;
   
   // Nvidia graphics can not pow with 0!
   if(v_ColorBlend > 0.0) {
      lerpValue = clamp(pow(v_ColorBlend, u_ColorBias), 0.0, 1.0);
   }
    
   // Mix the two colors together.
   gl_FragColor += mix(v_Diffuse, v_Emissive, lerpValue);
   
   // Calcular the reflection.
   vec3 normal = normalize(v_Normal);
   vec3 lineOfSight = normalize(v_LineOfSight);
   
   // Get the reflection from the the environment map.
   vec3 r = reflect(normal, lineOfSight);
   vec4 reflection = textureCube(u_CubeMapTexture, normalize(r));
   reflection *= u_ReflectionIntensity;

   gl_FragColor += (gl_FragColor * clamp(reflection, 0.0, 1.0));

   gl_FragColor.a = v_Diffuse.a;
}
