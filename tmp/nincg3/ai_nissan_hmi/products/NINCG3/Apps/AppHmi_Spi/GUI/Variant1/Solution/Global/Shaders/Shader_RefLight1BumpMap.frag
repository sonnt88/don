
/**
  * This Fragment Shader supports:
  * - Assignment of Texture.
  * - Per Fragment lighting in tangent space,
  * - using normals from normal map.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE
precision mediump float;

/*
 * Structure holding all parameters for a material
 */
struct material
{
    mediump vec4 ambient;       // Ambient light component
    mediump vec4 diffuse;       // Diffuse light component
    mediump vec4 emissive;      // Emissive light component
    mediump vec4 specular;      // Specular light component
    mediump float shininess;    // Shininess (exponent for specular light)
};


/*
 * Uniforms
 */
uniform material u_Material;

/*
 * Global variables act as accumulators
 */
mediump vec4 g_ambient_color;
mediump vec4 g_diffuse_color;
mediump vec4 g_specular_color;

/* 
 * Varyings describing light 0.
 */
varying mediump vec4 v_lightState;
varying mediump vec3 v_light0SpotParameters; // x = SpotCosCutoff, y = SpotExponent, z = Range
varying mediump vec4 v_light0Attenuation;
varying mediump vec4 v_light0Ambient;
varying mediump vec4 v_light0Diffuse;
varying mediump vec4 v_light0Specular;

/*-----------------------------------------------------------------------------------
 * Function: DirectionalLight
 *
 * normal           - Normalized Normal vector
 * lightDirection   - Normalized direction from fragment to light.
 * halfVector       - Normalized Calculated HalfVector for lighting calculations.
 *
 * Note: All parameter must be in the same coordinate space.
 */
void directionalLight(in vec3 normal, in vec3 lightDirection, in vec3 halfVector)
{
    /* Ambient light component */
    g_ambient_color  += v_light0Ambient;

    /* Diffuse light component */
    mediump float diffuse = max(dot(normal, lightDirection), 0.0);
    g_diffuse_color  += v_light0Diffuse * diffuse;

    /* Specular light component */
    mediump float specular = dot(normal, halfVector);
    if (specular > 0.0) {
        specular = pow(specular, u_Material.shininess);
        g_specular_color += v_light0Specular * specular;
    }
}

/*-----------------------------------------------------------------------------------
 * Function: PointLight
 *
 * normal           - Normalized Normal vector
 * lightDirection   - UnNormalized direction from fragment to light.
 * halfVector       - Normalized Calculated HalfVector for lighting calculations.
 *
 * Note: All parameters must be in the same coordinate space
 */
void pointLight(in vec3 normal, in vec3 lightDirection, in vec3 halfVector)
{
    mediump float diffuse;    // diffuse light contribution
    mediump float specular;   // specular light contribution
    float pf;                 // power factor
    float attenuation = 1.0;  // computed attenuation factor
    float d;                  // distance from surface to light source

    // Compute distance between surface and light position.
    d = length(lightDirection);

    // out of range: no light
    if (d > v_light0SpotParameters.z) {
      return;
    }


    // Compute attenuation only if enabled (attenuation.w == 1.0)
    if( v_light0Attenuation.w == 1.0 ) {
        attenuation = 1.0 / (v_light0Attenuation.x + v_light0Attenuation.y * d + v_light0Attenuation.z * d * d);
    }

    /* Ambient light component */
    g_ambient_color  += v_light0Ambient * attenuation;

    /* Diffuse light component */
    diffuse  = max(dot(normal, normalize(lightDirection)), 0.0);
    g_diffuse_color  += v_light0Diffuse * diffuse * attenuation;

    /* Specular light component */
    specular = dot(normal, halfVector);
    if (specular > 0.0) {
        specular = pow(specular, u_Material.shininess);
        g_specular_color += v_light0Specular * specular * attenuation;
    }
}

/*-----------------------------------------------------------------------------------
 * Function: Spotlight
 *
 * normal               - Normalized Normal vector
 * lightDirection       - UnNormalized direction from fragment to light.
 * halfVector           - Normalized Calculated HalfVector for lighting calculations.
 * spotLightDirection   - Direction of spot light in the according coordinate space.
 *
 * Note: All parameter must be in the same coordinate space
 */
void spotLight(in vec3 normal, in vec3 lightDirection, in vec3 halfVector, in vec3 spotLightDirection)
{
    mediump float diffuse;   // diffuse light contribution
    mediump float specular;  // specular light contribution
    float spotDot;           // cosine of angle between spotlight
    float spotAttenuation;   // spotlight attenuation factor
    float d;                 // distance from surface to light source
    float attenuation = 1.0; // computed attenuation factor
    vec3 lightDir;           // Direction from surface to light position

    // Compute distance between surface and light position.
    d = length(lightDirection);

    // out of range: no light
    if (d > v_light0SpotParameters.z) {
      return;
    }

    // Normalize vector from surface to light position.
    lightDir = normalize(lightDirection);

    // See if point on surface is inside cone of illumination.
    spotDot = dot(lightDir, spotLightDirection);

    if (spotDot >= v_light0SpotParameters.x) {
        // We are inside the cone.

        // Compute attenuation only if enabled (attenuation.w == 1.0).
        if( v_light0Attenuation.w == 1.0 ) {
            attenuation = 1.0 / (v_light0Attenuation.x + v_light0Attenuation.y * d + v_light0Attenuation.z * d * d);
        }

        // Combine the spotlight and distance attenuation.
        spotAttenuation = pow(spotDot, v_light0SpotParameters.y);
        attenuation *= spotAttenuation;

        /* Ambient light component */
        g_ambient_color  += v_light0Ambient * attenuation;

        /* Diffuse light component */
        diffuse  = max(dot(normal, lightDir), 0.0);
        g_diffuse_color  += v_light0Diffuse * diffuse * attenuation;

        /* Specular light component */
        specular = dot(normal, halfVector);
        if (specular > 0.0) {
            specular = pow(specular, u_Material.shininess);
            g_specular_color += v_light0Specular * specular * attenuation;
        }
    }
}

uniform sampler2D u_Texture;    //Color map.
uniform sampler2D u_Texture1;   //Normal map.

/*
 * Varyings
 */
varying vec2 v_TexCoord;
/*
 * Directions for light computations in tangent space.
 */
varying  mediump vec3 v_light0DirectionTS;     //Direction of light 0 transformed in tangent space.
varying  mediump vec3 v_light0HalfplaneTS;     //Halfvector of light 0 transfomed in tangent space.
varying  mediump vec3 v_spotLight0DirectionTS; //Light direction used for spot light, transformed in tangent Space.

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{

    /* Get modified tangent space normal from normal map. */
    vec3 normal = normalize(2.0 * texture2D(u_Texture1, v_TexCoord).rgb - 1.0);

    g_ambient_color = vec4(0.0);
    g_diffuse_color = vec4(0.0);
    g_specular_color = vec4(0.0);

    gl_FragColor = u_Material.emissive;
    if (v_lightState.x > 0.5) {
        if (v_lightState.y > 0.5) {
            directionalLight(normal, normalize(v_light0DirectionTS), normalize(v_light0HalfplaneTS));
        }
        else if (v_lightState.z > 0.5) {
            pointLight(normal, v_light0DirectionTS, normalize(v_light0HalfplaneTS));
        }
        else if(v_lightState.w > 0.5) {
            spotLight(normal, v_light0DirectionTS, normalize(v_light0HalfplaneTS), v_spotLight0DirectionTS);
        }
    }

    g_ambient_color *= u_Material.ambient;
    g_diffuse_color *= u_Material.diffuse;
    g_specular_color *= u_Material.specular;

    gl_FragColor += g_ambient_color + g_diffuse_color + g_specular_color;
    gl_FragColor.a = u_Material.diffuse.a;
    gl_FragColor *= texture2D(u_Texture, v_TexCoord);
}
