/**
  * This Vertex Shader supports:
  * - Transformation into world space,
  * - Texturing refraction and reflection using one cubemap texture.
  *
  * Note: 
  * - Lighting has no influence.
  * - Only one level of refraction is supported. 
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

/*
 * Uniforms
 */
uniform mat4 u_MVPMatrix;       // Model-View-Projection Matrix
uniform mediump mat4 u_MMatrix;
uniform mediump mat3 u_NormalMMatrix3;
uniform mediump vec3 u_CamPosition;
uniform mediump float refractionRatio;

/*
 * Attributes
 */
attribute vec4 a_Position;
attribute vec3 a_Normal;

/*
 * Varyings
 */
varying mediump vec3 v_CubeTexCoord[2];
varying mediump float v_Ratio;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{
    /* Transform vertex into world space */
    gl_Position = u_MVPMatrix * a_Position;

    mediump vec3 view = normalize((u_MMatrix * a_Position).xyz - u_CamPosition);
    mediump vec3 normalWorld = normalize(u_NormalMMatrix3 * a_Normal);

    //Calculate the ratio of refraction over reflection
    if (refractionRatio <= 1.0) {
        //Use Christophe Schlick approximation of Fresel reflectance.
        float F = ((1.0-refractionRatio) * (1.0-refractionRatio)) / ((1.0+refractionRatio) * (1.0+refractionRatio));
        v_Ratio = F + (1.0 - F) * pow((1.0 - dot(-view, normalWorld)), 5.0);
    }
    else {
        //If refractionRatio is greater than one use equation described in refract GLSL specification to maximize 
        // refraction over reflection ratio at refract() treshold.
        // Note: refractionRatio is greater than one in the uncommon case when the viewer is placed inside a medium with a higher 
        // refractance ratio than the object it is looking at (e.g. viewing air form inside water).
        v_Ratio = pow(clamp(refractionRatio * refractionRatio * (1.0 - dot(normalWorld, view) * dot(normalWorld, view)),0.0, 1.0), 5.0);
        v_Ratio = clamp(v_Ratio, 0.0, 1.0);
    }

    v_CubeTexCoord[0] = refract(view, normalWorld, refractionRatio);
    v_CubeTexCoord[1] = reflect(view, normalWorld);
}
