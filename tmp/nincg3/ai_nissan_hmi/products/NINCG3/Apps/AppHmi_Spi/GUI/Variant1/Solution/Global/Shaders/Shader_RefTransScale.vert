/**
  * This Vertex Shader supports:
  * - Transformation into world space.
  * - Upscaling in direction of normals.
  * - Texture coordinates.
  *
  * Note: Lighting has no influence.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

/*
 * Uniforms
 */
uniform mat4 u_MVPMatrix;       // Model-View-Projection Matrix.
uniform mediump float u_Scale;  // Scale factor to manipulate vertices along normals.

/*
 * Attributes
 */
attribute vec4 a_Position;
attribute vec3 a_Normal;
attribute vec2 a_TextureCoordinate;

/*
 * Varyings
 */
varying mediump vec2 v_TexCoord;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{
    /* Scale up using passed uniform scale factor. */
    vec4 position = a_Position;
    position = position + vec4(u_Scale * normalize(a_Normal), 0.0);

    /* Transform vertex into world space */
    gl_Position = u_MVPMatrix * position;

    v_TexCoord = a_TextureCoordinate;
}
