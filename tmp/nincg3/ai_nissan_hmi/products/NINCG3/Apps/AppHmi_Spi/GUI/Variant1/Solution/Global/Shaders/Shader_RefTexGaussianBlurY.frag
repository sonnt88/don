/**
  * This fragment shader can be used for post process blurring effects.
  * The incoming texture is blurred in Y direction by using a Gaussian blur.
  * The step width of the Gaussian blur can be modified by changing u_blurFactor.
  *
  * This Fragment Shader supports:
  * - Blurring the incoming image in Y direction.
  * - Assignment of Texture.
  * - Blurring the image by using a configurable step width.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

precision mediump float;

/*
 * Uniforms
 */ 
uniform sampler2D u_Texture;
uniform mediump float u_blurFactor;

/*
 * Varyings
 */
varying mediump vec2 v_TexCoord;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{
    mediump vec4 color = vec4(0.0);

   // blur in Y direction
   color += texture2D(u_Texture, vec2(v_TexCoord.x, v_TexCoord.y - 4.0 * u_blurFactor)) * 0.05;
   color += texture2D(u_Texture, vec2(v_TexCoord.x, v_TexCoord.y - 3.0 * u_blurFactor)) * 0.09;
   color += texture2D(u_Texture, vec2(v_TexCoord.x, v_TexCoord.y - 2.0 * u_blurFactor)) * 0.12;
   color += texture2D(u_Texture, vec2(v_TexCoord.x, v_TexCoord.y - u_blurFactor)) * 0.15;
   color += texture2D(u_Texture, vec2(v_TexCoord.x, v_TexCoord.y)) * 0.16;
   color += texture2D(u_Texture, vec2(v_TexCoord.x, v_TexCoord.y + u_blurFactor)) * 0.15;
   color += texture2D(u_Texture, vec2(v_TexCoord.x, v_TexCoord.y + 2.0 * u_blurFactor)) * 0.12;
   color += texture2D(u_Texture, vec2(v_TexCoord.x, v_TexCoord.y + 3.0 * u_blurFactor)) * 0.09;
   color += texture2D(u_Texture, vec2(v_TexCoord.x, v_TexCoord.y + 4.0 * u_blurFactor)) * 0.05;

   gl_FragColor = color;
}
