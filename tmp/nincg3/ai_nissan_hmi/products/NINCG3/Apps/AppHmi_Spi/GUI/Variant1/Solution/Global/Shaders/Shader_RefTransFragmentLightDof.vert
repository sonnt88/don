/**
  * This vertex shader is used as part of a 
  * depth-of-field (DOF) simulation that implements
  * the technique proposed in http://developer.amd.com/media/gpu_assets/ShaderX2_Real-TimeDepthOfFieldSimulation.pdf.
  * Additionally to rendering the vertex buffer it calculates the current vertices depth value and stores it 
  * as varying to be further processed in the fragment shader.
  * For DOF a pixels depth value has to be stored in the first render pass, 
  * so other shaders that should contribute to a scene with DOF should implement 
  * something similar.  
  *
  * This Vertex Shader supports:
  * - First render pass of depth-of-field rendering: Rendering the scene.
  * - Transformation into world space,
  * - Texturing with one texture.
  * - Passing of position and normal for per fragment lighting.
  * - Scaled depth value passed as varying.
  *
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

/*
 * Uniforms
 */
uniform mat4 u_MVPMatrix;       // Model-View-Projection Matrix
uniform mat4 u_MMatrix;         // Model Matrix
uniform mat3 u_NormalMMatrix3;  // Model Matrix inversed transposed

uniform mediump float u_depthScale;

/*
 * Attributes
 */
attribute vec4 a_Position;
attribute vec3 a_Normal;
attribute vec2 a_TextureCoordinate;

/*
 * Varyings
 */
varying mediump vec3 v_Position;
varying mediump vec3 v_Normal;
varying mediump vec2 v_TexCoord;
varying mediump float v_depth;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{
    /* Transform vertex into world space */
    vec4 position = u_MVPMatrix * a_Position;
    gl_Position = position;
    v_TexCoord = a_TextureCoordinate;

    /* Here the actual depth value is passed to the fragment shader. */
    v_depth = position.z * u_depthScale;

    /* Transform vectors needed for lighting calculations. */
    v_Position = (u_MMatrix * a_Position).xyz;
    v_Normal =  normalize(u_NormalMMatrix3 * a_Normal);
}
