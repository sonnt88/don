/**
  * This Fragment Shader supports:
  * - Wraps a seamless texture multible times around the object.
  * - Adds the specular term depending on the carbon pattern.
  * - Adds reflection from a cube map to the object.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

precision mediump float;

// Uniforms
uniform float u_ReflectionIntensity;      // How strong the reflection is.
uniform vec2 u_Tile;                      // How many times the texture is wrapped around.
uniform sampler2D u_Texture;              // Carbon pattern.
uniform samplerCube u_CubeMapTexture1;    // Cube map - environment.

// Varyings
varying vec3 v_Normal;
varying vec3 v_LineOfSight;
varying vec4 v_Color;
varying vec4 v_Specular;
varying vec2 v_TexCoord;

void main(void)
{  
   gl_FragColor = v_Color;
   
   vec2 tile = vec2(1.0 / u_Tile.x, 1.0 / u_Tile.y);
   
   // Tile coords.
   vec2 sector = fract(v_TexCoord / tile);
   
   // Wrap tile position. 
   if (sector.x > 1.0) {
      sector.x = sector.x - 1.0;
   }
   
   if (sector.y > 1.0) {
      sector.y = sector.y - 1.0;
   }
   
   // Apply the carbon pattern to the object.
   gl_FragColor *= texture2D(u_Texture, sector);
   
   // Add the specular only where the carbon pattern is bright, in order to produce a typical carbon cross effect.
   gl_FragColor += clamp((v_Specular * texture2D(u_Texture, sector)), 0.0, 1.0);

   // Calculate the reflection.
   vec3 normal = normalize(v_Normal);
   vec3 lineOfSight = normalize(v_LineOfSight);

   // Get the reflection of the environment map.
   vec3 r = reflect(normal, lineOfSight);
   vec4 reflection = textureCube(u_CubeMapTexture1, normalize(r));
   reflection *= u_ReflectionIntensity;
   
   gl_FragColor += clamp(reflection, 0.0, 1.0);   
   
   gl_FragColor.a = v_Color.a;  
}
