/**
  * This Fragment Shader supports:
  * - Applying CubeMap texture using passed texture coordinates.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

/*
 * Uniforms
 */ 
uniform samplerCube u_CubeMapTexture;

/*
 * Varyings
 */
varying mediump vec3 v_CubeTexCoord;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{        
    gl_FragColor = textureCube(u_CubeMapTexture, v_CubeTexCoord);
}
