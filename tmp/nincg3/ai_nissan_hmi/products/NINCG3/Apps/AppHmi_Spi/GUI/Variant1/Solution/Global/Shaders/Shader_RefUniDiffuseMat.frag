/**
  * This Fragment Shader supports:
  * - Assignment of product of material's diffuse color and texture color to fragment color.
  * 
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

precision mediump float;

/*
 * Structure holding all parameters for a material
 * This uniform value is set by Candera automatically if a Material is applied to a Node.
 */ 
struct material
{    
    vec4  diffuse;  	// Diffuse light component 
};


/*
 * Uniforms
 */ 
uniform material u_Material;


/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{        
    gl_FragColor = u_Material.diffuse;
}
