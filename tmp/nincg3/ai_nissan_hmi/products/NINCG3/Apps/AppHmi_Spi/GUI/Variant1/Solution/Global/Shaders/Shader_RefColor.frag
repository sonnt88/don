/**
  * This Fragment Shader supports:
  * - Assignment of varying color to fragment color.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

varying mediump vec4 v_Color;

void main(void)
{    
    gl_FragColor = v_Color;
}
