/**
  * This Vertex Shader is for PointSprites only and supports:
  * - Transformation of point sprite into world space.
  *
  * Note: Precondition: Node must have set PointSpriteDefaultShaderParams in order to parametrize this Shader correctly
  * (For details see function Node::SetShaderParams). As usual for point sprites, lighting has no influence.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

uniform mat4 u_MVPMatrix;
uniform mediump float u_Size;

attribute vec4 a_Position;

void main()
{
    gl_Position = u_MVPMatrix * a_Position;
    gl_PointSize = u_Size;
}
