/**
  * This Fragment Shader supports:
  * - Using a texture to create a mask through discarding pixels (no value is set in target buffers), 
  * which can further be used to e.g. create a stencil mask for stereoscopic 3D.
  * - Creating screen space mask works best in conjunction with RefViewportSpace.vertp shader.
  * 
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

precision mediump float;

uniform sampler2D u_Texture;
varying vec2 v_TexCoord;

void main()
{
   vec4 color = texture2D(u_Texture, v_TexCoord);
   if(color.a < 0.5) {
       discard;
   }
   gl_FragColor = color;
}

