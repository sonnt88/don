/**
  * This Vertex Shader supports:
  * - Transformation into world space,
  * - Texturing reflection using one CubeMap texture.
  *
  * Note: Lighting has no influence.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

/*
 * Uniforms
 */
uniform mat4 u_MVPMatrix;       // Model-View-Projection Matrix
uniform mediump mat4 u_MMatrix;
uniform mediump mat3 u_NormalMMatrix3;
uniform mediump vec3 u_CamPosition;

/*
 * Attributes
 */
attribute vec4 a_Position;
attribute vec3 a_Normal;

/*
 * Varyings
 */
varying mediump vec3 v_CubeTexCoord;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{
    /* Transform vertex into world space */
    gl_Position = u_MVPMatrix * a_Position;

    mediump vec3 view = (u_MMatrix * a_Position).xyz - u_CamPosition;
    mediump vec3 normalWorld = normalize(u_NormalMMatrix3 * a_Normal);
    v_CubeTexCoord = reflect(view, normalWorld);
}
