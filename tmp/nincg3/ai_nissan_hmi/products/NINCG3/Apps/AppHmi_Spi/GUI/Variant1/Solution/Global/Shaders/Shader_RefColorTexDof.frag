
/**
  * This vertex shader is used as part of a 
  * depth-of-field (DOF) simulation that implements
  * the technique proposed in http://developer.amd.com/media/gpu_assets/ShaderX2_Real-TimeDepthOfFieldSimulation.pdf.
  * Additionally to writing the color of the fragment it encodes the depth of the fragment into its alpha channel.
  * For DOF a pixels depth value has to be stored in the first render pass, 
  * so other shaders that should contribute to a scene with DOF should implement 
  * something similar.  
  *
  * This Fragment Shader supports:
  * - First render pass of depth-of-field rendering: Rendering the scene.
  * - Assignment of Texture.
  * - Depth encoding into alpha channel.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

/*
 * Uniforms
 */ 
uniform sampler2D u_Texture;

/*
 * Varyings
 */
varying mediump float v_depth;
varying mediump vec2 v_TexCoord;
varying mediump vec4 v_Color;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{        
    mediump vec4 color = texture2D(u_Texture, v_TexCoord) * v_Color;
    /* Stores the incoming depth as fragments alpha value. */
    color.a = v_depth;
    gl_FragColor = color;
}
