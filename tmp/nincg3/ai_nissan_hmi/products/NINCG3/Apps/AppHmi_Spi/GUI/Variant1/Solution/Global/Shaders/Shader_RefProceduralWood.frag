/**
  * This Fragment Shader supports:
  * - Procedural generation of interpolated rings, that 
  *   make a wood-like appearance if the right colors are chosen.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

precision mediump float; 

/*
 * Uniforms
 */ 

uniform vec3 u_CustomWoodCenter;          // Grain center in model space.
uniform vec4 u_CustomWoodColor1;          // First color of wood.
uniform vec4 u_CustomWoodColor2;          // Second color of wood.
uniform float u_CustomWoodMultiplier;     // Multipliert to control density of annual rings-

/*
 * Varyings
 */
varying mediump vec4 v_Color;
varying mediump vec3 v_Position;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{        
    /* Calculate annual rings of procedural wood. */
    float distanceToCenter = length(v_Position.xyz - u_CustomWoodCenter);
    float weight = clamp(sin(distanceToCenter * u_CustomWoodMultiplier), 0.0, 1.0);
    vec4 diffuseColor = u_CustomWoodColor1 + weight * u_CustomWoodColor2;
    gl_FragColor = diffuseColor * v_Color;
    gl_FragColor.a = v_Color.a;
}
