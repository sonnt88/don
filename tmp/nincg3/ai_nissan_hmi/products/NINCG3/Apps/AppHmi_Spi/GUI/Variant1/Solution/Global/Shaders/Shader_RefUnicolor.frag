/**
  * This Fragment Shader supports:
  * - Assignment of uniform color to fragment color.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

uniform mediump vec4 u_Color;   // Color

void main(void)
{    
    gl_FragColor = u_Color;
}
