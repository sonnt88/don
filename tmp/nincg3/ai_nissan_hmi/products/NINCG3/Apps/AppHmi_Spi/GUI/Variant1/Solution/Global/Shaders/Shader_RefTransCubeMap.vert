/**
  * This Vertex Shader supports:
  * - Transformation,
  * - Texturing using one CubeMap texture, addressed directly by object space vertex coordinates.
  *
  * Note: Lighting has no influence.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

/*
 * Uniforms
 */
uniform mat4 u_MVPMatrix;       // Model-View-Projection Matrix

/*
 * Attributes
 */
attribute vec4 a_Position;

/*
 * Varyings
 */
varying mediump vec3 v_CubeTexCoord;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{
    /* Transform vertex into world space */
    gl_Position = u_MVPMatrix * a_Position;

    v_CubeTexCoord = a_Position.xyz;
}
