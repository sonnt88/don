/**
  * This Vertex Shader supports:
  * - Output of vertex coordinate as gl_Position.
  * - Uniforms for the adjustable offset of a
  *   screen aligned billboard.
  *   u_offsetLeft: left offset from 0.0 to 1.0
  *   u_offsetTop: top offset from 0.0 to 1.0
  *
  * This shader can be used to create screen aligned
  * billboards used for e.g. screen space effects.
  * The used billboard should be of size 0.0 to 1.0
  * with 1.0 being fullscreen.
  * The position of the billboard is adjustable with
  * the origin in the top, left corner.
  * Since the vertex position is passed to the
  * fragment shader without any matrix transformation
  * the incoming vertex buffer should already be in
  * homogenous screen coordinates.
  * Note: Overlapping Billboards (in screen space) would 
  * produce Z-fighting artifacts. Disabling depth test and
  * using render order as well as using depth bias
  * can be used to avoid them.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

/*
 * Uniforms
 */
uniform float u_offsetLeft;
uniform float u_offsetTop;

/*
 * Attributes
 */
attribute vec4 a_Position;
attribute vec2 a_TextureCoordinate;

/*
 * Varyings
 */
varying mediump vec2 v_TexCoord;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{
    vec4 pos = a_Position;
    /* scale the billboard to size 0.0 to 1.0 */
    pos.xy *= 2.0;
    /* set the origin to the top left */
    float width = abs(pos.x);
    float height = abs(pos.y);
    pos.x -= (width != 1.0) ? (1.0 - width) : 0.0;
    pos.y += (height != 1.0) ? (1.0 - height) : 0.0;
    /* offset from the new origin */
    pos.x += u_offsetLeft * 2.0;
    pos.y -= u_offsetTop * 2.0;

    gl_Position = pos;
    v_TexCoord = a_TextureCoordinate;
}
