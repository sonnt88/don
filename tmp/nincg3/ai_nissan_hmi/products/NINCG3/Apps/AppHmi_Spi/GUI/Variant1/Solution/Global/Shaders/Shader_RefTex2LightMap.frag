/**
  * This Fragment Shader supports:
  * - Lightmap Multitexturing: A Lightmap typically defines an illumination texture at unit 1 that is multiplied with texture at unit 0.
  * 
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

precision mediump float;

uniform sampler2D u_Texture;
uniform sampler2D u_Texture1;

varying vec2 v_TexCoord;

void main(void)
{   
    /* Multiply texture0 with lightmap texture1. */	
    gl_FragColor = texture2D(u_Texture, v_TexCoord) * texture2D(u_Texture1, v_TexCoord);
}
