/**
  * This Vertex Shader supports:
  * - Transformation,
  * - Texturing with one texture,
  * - Passing positions, normals, tangents and binormals in world space to fragment shader.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE
 
/*
 * Uniforms
 */
uniform mat4 u_MVPMatrix;       // Model-View-Projection Matrix
uniform mediump mat4 u_MMatrix;        // Model Matrix
uniform mediump mat3 u_NormalMMatrix3;        // Model Matrix inversed transposed

/*
 * Attributes
 */
attribute vec4 a_Position;
attribute vec2 a_TextureCoordinate;
attribute vec3 a_Normal;
attribute vec3 a_Tangent;
attribute vec3 a_BiNormal;

/*
 * Varyings
 */
varying mediump vec2 v_TexCoord;
varying mediump vec3 v_Position;
varying mediump vec3 v_Normal;
varying mediump vec3 v_Tangent;
varying mediump vec3 v_BiNormal;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{
   
    /* Transform vertex into world space */
    gl_Position = u_MVPMatrix * a_Position;
    v_TexCoord = a_TextureCoordinate;

     /* Transform vectors needed for lighting calculations. */
    v_Position = (u_MMatrix * a_Position).xyz;
    v_Normal =  normalize(u_NormalMMatrix3 * a_Normal);
    v_Tangent =  normalize(u_NormalMMatrix3 * a_Tangent);
    v_BiNormal =  normalize(u_NormalMMatrix3 * a_BiNormal);
}
