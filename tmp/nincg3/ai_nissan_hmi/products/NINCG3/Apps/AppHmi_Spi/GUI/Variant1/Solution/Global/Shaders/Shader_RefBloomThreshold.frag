/**
  * This fragment shader is used as part of a bloom effect.
  * At first the bright parts of the original rendering have to be extracted by using a brightness threshold.
  * Afterwards these extracted, bright parts will be blurred.
  * The last pass combines the blurred and the original image.
  *
  * This Fragment Shader supports:
  * - Assignment of Texture.
  * - First render pass of bloom rendering: Extracting bright parts by using a threshold.
  * - Adjustable brightness threshold.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

/*
 * Uniforms
 */
uniform sampler2D u_Texture;
uniform mediump float u_brightnessThreshold;

/*
 * Varyings
 */
varying mediump vec2 v_TexCoord;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{
    mediump vec4 color = texture2D(u_Texture, v_TexCoord);

    /* extract the bright colors in the scene by using a threshold */
    gl_FragColor = clamp((color - u_brightnessThreshold) / (1.0 - u_brightnessThreshold), 0.0, 1.0);
}
