/**
  * This fragment shader is used as part of a bloom effect.
  * At first the bright parts of the original rendering were extracted by using a brightness threshold.
  * Afterwards these extracted, bright parts were blurred.
  * Now, the last pass combines the blurred and the original image.
  *
  * This Fragment Shader supports:
  * - Assignment of Texture units 0 and 1.
  * - Third pass of bloom rendering: Combining original and bloomed images.
  * - Adjustable intensity and saturation of the original and bloomed images.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

precision mediump float; 

/*
 * Uniforms
 */
uniform sampler2D u_Texture;
uniform sampler2D u_Texture1;
uniform mediump float u_originalIntensity;
uniform mediump float u_bloomIntensity;
uniform mediump float u_originalSaturation;
uniform mediump float u_bloomSaturation;

/*
 * Varyings
 */
varying mediump vec2 v_TexCoord;

mediump vec4 adjustSaturation(mediump vec4 color, mediump float saturation)
{
    /* converting the incoming color to the corresponding grayscale value by using predefined values */
    mediump float grey = dot(color, vec4(0.3, 0.59, 0.11, 1.0));
    /* linearly interpolate between the grayscale and the color value by using the saturation */
    return mix(vec4(grey, grey, grey, grey), color, saturation);
}

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{
    mediump vec4 originalColor = texture2D(u_Texture, v_TexCoord);
    mediump vec4 bloomColor = texture2D(u_Texture1, v_TexCoord);

    originalColor = adjustSaturation(originalColor, u_originalSaturation) * u_originalIntensity;
    bloomColor = adjustSaturation(bloomColor, u_bloomSaturation) * u_bloomIntensity;

    /* darken the original scene where the bloom color is bright to avoid "overexposure" */
    originalColor *= (1.0 - clamp(bloomColor, 0.0, 1.0));

    gl_FragColor = originalColor + bloomColor;
}
