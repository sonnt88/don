/**
  * This vertex shader is used as part of a 
  * depth-of-field (DOF) simulation that implements
  * the technique proposed in http://developer.amd.com/media/gpu_assets/ShaderX2_Real-TimeDepthOfFieldSimulation.pdf.
  * This combines the incoming sharp and blurred texture using a parametrizable focus, along with a 
  * parametrizable focus range.
  *
  * This Fragment Shader supports:
  * - Third Pass of depth-of-field rendering: Combining sharp and blurred picture.
  * - Assignment of Texture units 0 and 1.
  * - Combining sharp and blurred images using focus and focus range.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

/*
 * Uniforms
 */ 
uniform sampler2D u_Texture;
uniform sampler2D u_Texture1;

uniform mediump float u_Range;
uniform mediump float u_Focus;

/*
 * Varyings
 */
varying mediump vec2 v_TexCoord;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{        
    mediump vec4 sharp = texture2D(u_Texture, v_TexCoord);
    mediump vec4 blurred = texture2D(u_Texture1, v_TexCoord);
 
    mediump float weight = clamp(u_Range * abs(u_Focus - sharp.a), 0.0, 1.0);
    gl_FragColor = mix(sharp, blurred, weight);
}
