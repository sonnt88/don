/**
  * This Fragment Shader supports:
  * - Mixes the two color components together based on the camera position and a user defined bias.
  * - Adding a noise to the spectral part of the light.
  * - Noise can be wrapped around the object multible times.
  * - Create the illusion of metal flakes in the car paint.
  * - Adding reflection from a cube map to the model.
  */
  
//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

precision mediump float;

// Uniforms
uniform float u_ColorBias;             // How strong the mixture of the two colors is.
uniform float u_ReflectionIntensity;   // How strong the reflection is.
uniform vec2 u_Tile;                   // How many times the texture is wrapped around.
uniform float u_FlakeIntensity;        // How bright the metal flakes are.
uniform float u_FlakeThreshold;        // Lower boundary of the metal flakes.
uniform sampler2D u_Texture;           // Noise for the metal flakes. 
uniform samplerCube u_CubeMapTexture1; // Cube map - environment.

// Varyings
varying float v_ColorBlend;   // The mix value of the two colors depending on the line of sight.
varying vec3 v_Normal;
varying vec3 v_LineOfSight;
varying vec2 v_TexCoord;

// The color and light values
varying vec4 v_Diffuse;       // Primary color.
varying vec4 v_Emissive;      // Secondary color.
varying vec4 v_Specular;      // Highlight component.
varying vec4 v_Color;         // The incoming ambient and specular color component of the final color.

void main(void)
{  
   gl_FragColor = v_Color;
   
   float lerpValue = 0.0;
   
   // Nvidia graphics can not pow with 0!
   if(v_ColorBlend > 0.0) {
      lerpValue = clamp(pow(v_ColorBlend, u_ColorBias), 0.0, 1.0);
   }
    
   // Mix the two colors together.
   gl_FragColor += mix(v_Diffuse, v_Emissive, lerpValue);
   
   // Calculate coordinates to wrap the texture multible times around the object.
   vec2 tile = vec2(1.0 / u_Tile.x, 1.0 / u_Tile.y);
   vec2 sector = fract(v_TexCoord / tile);
   
   // Wrap tile position.
   if (sector.x > 1.0) {
      sector.x = sector.x - 1.0;
   }
   
   if (sector.y > 1.0) {
      sector.y = sector.y - 1.0;
   }
   
   // Get a noise value from a single channel of the texture.
   float  flakeNoiseValue = pow(clamp(texture2D(u_Texture, sector).b, 0.0, 1.0), 2.0); 
   
   gl_FragColor.xyz += clamp((flakeNoiseValue * (v_Specular.xyz - u_FlakeThreshold)) * u_FlakeIntensity, 0.0, 1.0);
   
   vec3 normal = normalize(v_Normal);
   vec3 lineOfSight = normalize(v_LineOfSight);

   // Get the reflection of the environment map.
   vec3 r = reflect(normal, lineOfSight);
   vec4 reflection = textureCube(u_CubeMapTexture1, normalize(r));
   reflection *= u_ReflectionIntensity;
   
   gl_FragColor += clamp(reflection, 0.0, 1.0);
   
   gl_FragColor.a = v_Diffuse.a;
}
