/**
  * This Vertex Shader supports:
  * - Transformation,
  * - SphereMap Texture Generation.
  *
  * Note: The Node must have set SphereMapShaderParams in order to parametrize this Shader correctly
  * (For details see function Node::SetShaderParams).
  * Lighting has no influence.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE
//START INCLUDE RefTexturing.inc
/*
 * The include file TextureMapping.incv for vertex shaders implements various texture mapping helper functions.  
 * 
 * Following functions are provided:
 * sphereMap: Generates a texture coordinate in a 2D Texture known as Sphere Map
 */


/**
 * Function sphereMap generates a 2D texture coordinate to look up in a SphereMap image, 
 * which has been bound as a texture for environmental mapping. 
 * @param position	is the normalized coordinate of the vertex in eye space (Model-view space)
 * @param normal	is the normalized normal coordinate in eye space (Model-view space)
 * @returns			a vec2 texture coordinate
 */ 
vec2 sphereMap(vec3 position, vec3 normal)
{
	// Reflect incident eye vector. Since the computation is performed in eye coordinates, the eye is 
	// located at the origin and the direction vector is equal to the normalized vertex position. 
	vec3 reflection= reflect(position, normal);
	
	// Calculate 2D texture coordinate
	float m = 2.0 * sqrt( reflection.x * reflection.x + reflection.y * reflection.y + (reflection.z + 1.0) * (reflection.z + 1.0) );						  
	return (reflection / m + 0.5).xy;  
}
//END INCLUDE

/*
 * Uniforms
 */
uniform mat4 u_MVPMatrix;  // Model-View-Projection Matrix
uniform mediump mat4 u_MVMatrix;   // Model-View Matrix for Transformation positions into eye space
uniform mediump mat3 u_NormalMVMatrix3;  // Model-View Matrix for Transformation normals into eye space

/*
 * Attributes
 */
attribute vec4 a_Position; // Vertex position
attribute vec3 a_Normal;   // Normal vector of vertex

/*
 * Varyings
 */
varying mediump vec2 v_TexCoord;  // Generated 2D Texture coordinate

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{
    /* Transform vertex into world space */
    gl_Position = u_MVPMatrix * a_Position;

    /* Transform position and normal into eye space */
    vec4 position = u_MVMatrix * a_Position;
    vec3 normal= u_NormalMVMatrix3 * a_Normal;

    /* Retrieve sphere map texture coords */
    v_TexCoord = sphereMap(normalize(position.xyz), normalize(normal));
}
