/**
  * This Vertex Shader supports:
  * - Transformation,
  * - Texturing with one texture,
  * - Preparing light vectors in tangent space for per fragment lighting in fragment shader.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

/*
 * Structure holding all parameters of a light source
 */
struct light
{
    mediump int type;                // Type of Light (0: Ambient, 1: Diffuse, 2: Point, 3: Spot)
    mediump int enabled;             // Light has been enabled by application
    mediump vec3 position;           // Position of light in model space
    mediump vec3 direction;          // Normalized light direction in model space
    mediump vec3 halfplane;          // Normalized half-plane vector
    mediump float spotCosCutoff;     // Cosine of spot light cutoff angle
    mediump float spotExponent;      // Spotlight exponent
    mediump vec4 attenuation;        // Attenuation: constant, linear, quadratic, enabled (1.0 = true, 0.0 = false);
    mediump float range;             // Range for point and spot lights; intensity is 0 for positions farther away than range
    mediump vec4 ambient;    // Ambient light component
    mediump vec4 diffuse;    // Diffuse light component
    mediump vec4 specular;   // Specular light component
    mediump vec3 cameraLookAtVector; // Camera look at vector.
};

/*
 * Uniforms
 */
uniform light u_Light[1];
uniform mediump vec3 u_CamPosition;

/*
 * Uniforms
 */
uniform mat4 u_MVPMatrix;          // Model-View-Projection Matrix
uniform mediump mat4 u_MMatrix;           // Model-View Matrix
uniform mediump mat3 u_NormalMMatrix3; // Model-View Matrix inversed transposed

/*
 * Attributes
 */
attribute vec4 a_Position;
attribute vec2 a_TextureCoordinate;
attribute vec3 a_Normal;
attribute vec3 a_Tangent;
attribute vec3 a_BiNormal;

/*
 * Varyings
 */
varying mediump vec2 v_TexCoord;

/*
 * Directions for light computations in tangent space.
 */
varying mediump vec3 v_light0DirectionTS; //direction of light 0 transformed in tangent space.
varying mediump vec3 v_light0HalfplaneTS; //halfvector of light 0 transfomed in tangent space.

/* Spot light direction is also transformed into tangent space. */
varying mediump vec3 v_spotLight0DirectionTS;

/* Send needed parameters of light 1 to fragment shader. */
varying mediump vec4 v_lightState;
varying mediump vec3 v_light0SpotParameters; // x = SpotCosCutoff, y = SpotExponent, z = Range
varying mediump vec4 v_light0Attenuation;
varying mediump vec4 v_light0Ambient;
varying mediump vec4 v_light0Diffuse;
varying mediump vec4 v_light0Specular;


/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{

    /* Transform vertex into world space */
    gl_Position = u_MVPMatrix * a_Position;

    v_spotLight0DirectionTS = vec3(0.0);

    /* Pass texture coordinates to fragment shader. */
    v_TexCoord = a_TextureCoordinate;

    /* Transform normals, tangents and binormals into world space. */
    vec3 normal = normalize(u_NormalMMatrix3 * a_Normal);
    vec3 tangent = normalize(u_NormalMMatrix3 * a_Tangent);
    vec3 binormal = normalize(u_NormalMMatrix3 * a_BiNormal);

    /* Transform vertex position in world space. */
    vec3 position = (u_MMatrix * a_Position).xyz;

    vec3 eyeVec = u_CamPosition - position;
    /* Get light dir according to light type. */
    vec3 lightDir = vec3(0.0);
    vec3 halfplane = vec3(0.0);
    
    v_lightState = vec4(0.0);
    
    if (u_Light[0].enabled == 1) {
        int lightType = u_Light[0].type;
        v_lightState.x = 1.0;
        /* LightType 0 (ambient) skipped as ambient light has no relevant properties.*/
        if (lightType == 1) {
            /* Directional light. */
            lightDir = normalize(u_Light[0].direction);
            halfplane = eyeVec + lightDir;
            v_lightState.y = 1.0;
        }
        else if (lightType == 2) {
            /* Point light. */
            lightDir = u_Light[0].position - position;
            halfplane = eyeVec + lightDir;
            v_lightState.z = 1.0;
        }
        else if (lightType == 3) {
            /* Spot Light. */
            lightDir = u_Light[0].position - position;
            halfplane = eyeVec + lightDir;
            v_spotLight0DirectionTS.x = dot(u_Light[0].direction, tangent);
            v_spotLight0DirectionTS.y = dot(u_Light[0].direction, binormal);
            v_spotLight0DirectionTS.z = dot(u_Light[0].direction, normal);
            v_spotLight0DirectionTS = normalize(v_spotLight0DirectionTS);
            v_lightState.w = 1.0;
        }
    }

    /* Compute lightDir, halfplane and eye vector in tangent space. */
    v_light0DirectionTS.x = dot(lightDir, tangent);
    v_light0DirectionTS.y = dot(lightDir, binormal);
    v_light0DirectionTS.z = dot(lightDir, normal);

    v_light0HalfplaneTS.x = dot(halfplane, tangent);
    v_light0HalfplaneTS.y = dot(halfplane, binormal);
    v_light0HalfplaneTS.z = dot(halfplane, normal);

    /* Pass light properties to fragment shader. */
    v_light0SpotParameters.x = u_Light[0].spotCosCutoff;
    v_light0SpotParameters.y = u_Light[0].spotExponent;
    v_light0SpotParameters.z = u_Light[0].range;
    v_light0Attenuation = u_Light[0].attenuation;
    v_light0Ambient = u_Light[0].ambient;
    v_light0Diffuse = u_Light[0].diffuse;
    v_light0Specular = u_Light[0].specular;

}
