/**
  * This Vertex Shader is for PointSprites only and supports:
  * - Coloring,
  * - Texturing.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

precision mediump float;

struct material
{    
    vec4  diffuse;
};

uniform sampler2D u_Texture;
uniform material u_Material;

void main()
{
    vec4 texColor;
    texColor = texture2D(u_Texture, gl_PointCoord);
    gl_FragColor = texColor * u_Material.diffuse;
}
