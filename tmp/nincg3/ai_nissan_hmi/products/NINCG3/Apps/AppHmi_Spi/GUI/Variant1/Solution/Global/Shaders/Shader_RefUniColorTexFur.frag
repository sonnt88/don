/**
  * This Fragment Shader supports:
  * - Assignment of product of alpha mask, texture and fixed color.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

precision mediump float; 

/*
 * Uniforms
 */ 
uniform sampler2D u_Texture;        //Fur transparancies.
uniform sampler2D u_Texture1;       //Fur color map.
uniform mediump vec4 u_ColorScale;          //Fur color scale.

/*
 * Varyings
 */
varying vec2 v_TexCoord;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{   
    //Alpha component.
    vec4 alpha = texture2D(u_Texture, v_TexCoord);

    //Color component, scaled.
    vec4 color = u_ColorScale * texture2D(u_Texture1, v_TexCoord) * alpha;

    gl_FragColor = color;
}
