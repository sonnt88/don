/**
  * This vertex shader is used as part of a 
  * depth-of-field (DOF) simulation that implements
  * the technique proposed in http://developer.amd.com/media/gpu_assets/ShaderX2_Real-TimeDepthOfFieldSimulation.pdf.
  * This fragment shader blurs the incoming picture, simulating a circle of confusion (coc) of configurable size,
  * and represents the second pass of the algorithm.
  * For details, please see the paper cited above.
  *
  * This Fragment Shader supports:
  * - Second Pass of depth-of-field rendering: Blurring the first pass.
  * - Assignment of Texture.
  * - Blurring of result image simulating a configurable size circle of confusion.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

precision mediump float;

/*
 * Uniforms
 */ 
uniform sampler2D u_Texture;
uniform mediump float u_CocScale; //Scale of circle of confusion.

/*
 * Varyings
 */
varying mediump vec2 v_TexCoord;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{        
    mediump vec4 color = texture2D(u_Texture, v_TexCoord);

    /* Values for coc distribution taken from the paper cited above. */
    color += texture2D(u_Texture, v_TexCoord + u_CocScale * vec2(-0.326212, -0.405805));
    color += texture2D(u_Texture, v_TexCoord + u_CocScale * vec2(-0.840144, -0.073580));
    color += texture2D(u_Texture, v_TexCoord + u_CocScale * vec2(-0.695914,  0.457137));
    color += texture2D(u_Texture, v_TexCoord + u_CocScale * vec2(-0.203345,  0.620716));
    color += texture2D(u_Texture, v_TexCoord + u_CocScale * vec2( 0.962340, -0.194983));
    color += texture2D(u_Texture, v_TexCoord + u_CocScale * vec2( 0.473434, -0.480026));
    color += texture2D(u_Texture, v_TexCoord + u_CocScale * vec2( 0.519456,  0.767022));
    color += texture2D(u_Texture, v_TexCoord + u_CocScale * vec2( 0.185461, -0.893124));
    color += texture2D(u_Texture, v_TexCoord + u_CocScale * vec2( 0.507431,  0.064425));
    color += texture2D(u_Texture, v_TexCoord + u_CocScale * vec2( 0.896420,  0.412458));
    color += texture2D(u_Texture, v_TexCoord + u_CocScale * vec2(-0.321940, -0.932615));
    color += texture2D(u_Texture, v_TexCoord + u_CocScale * vec2(-0.791559, -0.597705));
 
    mediump float factor =  0.0769230; //~1.0/13.0
    color *= factor;
    gl_FragColor = color;
}
