/**
  * This Vertex Shader supports:
  * - Transformation into world space,
  * - Texturing with one texture.
  *
  * Note: Lighting has no influence.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

/*
 * Uniforms
 */
uniform mat4 u_MVPMatrix;       // Model-View-Projection Matrix

/*
 * Attributes
 */
attribute vec4 a_Position;
attribute vec2 a_TextureCoordinate;

/*
 * Varyings
 */
varying mediump vec2 v_TexCoord;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{
    /* Transform vertex into world space */
    gl_Position = u_MVPMatrix * a_Position;

    v_TexCoord = a_TextureCoordinate;
}
