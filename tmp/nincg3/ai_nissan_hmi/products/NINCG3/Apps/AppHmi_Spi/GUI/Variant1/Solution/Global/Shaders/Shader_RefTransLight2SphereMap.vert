/**
  * This Vertex Shader supports:
  * - Transformation,
  * - SphereMap Texture Generation.
  * - Two light sources with lighting in model space.
  *
  * Note: The Node must have set SphereMapShaderParams in order to parametrize this Shader correctly
  * (For details see function Node::SetShaderParams).
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE
//START INCLUDE RefLighting.inc
/*
 * Shader Include-File that implements ambient, directional, point, and spot light.
 * The lighting calcuations for the different light types are implement as
 * subroutines. The following functions can be used for either 
 * per pixel or per vertex lighting:
 *
 * - ambientLight()
 * - directionalLight()
 * - pointLight()
 * - spotLight()
 *
 * The following functions are used for calculation of
 * anisotropic lighting, and can only be used for per pixel lighting.
 * Here, Directional-, Point- and SpotLight calculations calculate the specular component 
 * anisotropic, according to Ward's SIGGRAPH 92 paper "Measuring and Modeling 
 * Anisotropic Reflection".
 *
 * - anisotropicAmbientLight()
 * - anisotropicDirectionalLight()
 * - anisotropicPointLight()
 * - anisotropicSpotLight()
 *
 * For details see the function headers.
 */

/*
 * Structure holding all parameters for a material
 */
struct material
{
    mediump vec4 ambient;       // Ambient light component
    mediump vec4 diffuse;       // Diffuse light component
    mediump vec4 emissive;      // Emissive light component
    mediump vec4 specular;      // Specular light componennt
    mediump float shininess;    // Shininess (exponent for specular light)
};

/*
 * Structure holding all parameters of a light source
 */
struct light
{
    mediump int type;                // Type of Light (0: Ambient, 1: Diffuse, 2: Point, 3: Spot)
    mediump int enabled;             // Light has been enabled by application
    mediump vec3 position;           // Position of light in model space
    mediump vec3 direction;          // Normalized light direction in model space
    mediump vec3 halfplane;          // Normalized half-plane vector
    mediump float spotCosCutoff;     // Cosine of spot light cutoff angle
    mediump float spotExponent;      // Spotlight exponent
    mediump vec4 attenuation;        // Attenuation: constant, linear, quadratic, enabled (1.0 = true, 0.0 = false);
    mediump float range;             // Range for point and spot lights; intensity is 0 for positions farther away than range
    mediump vec4 ambient;    // Ambient light component
    mediump vec4 diffuse;    // Diffuse light component
    mediump vec4 specular;   // Specular light component
    mediump vec3 cameraLookAtVector; // Camera look at vector.
};

/*
 * Uniforms
 */
uniform mediump float u_AlphaU;      // Distribution of specular light component in u-direction.
uniform mediump float u_AlphaV;      // Distribution of specular light component in v-direction.
uniform mediump vec3 u_CamPosition;  // Camera position in world space used to get eye vector from (per vertex or fragment position). 
uniform material u_Material;
uniform light u_Light[8];

/*
 * Global variables act as accumulators
 */
mediump vec4 g_ambient_color;
mediump vec4 g_diffuse_color;
mediump vec4 g_specular_color;

/*-----------------------------------------------------------------------------------
 * Function: AmbientLight
 *
 * idx    - Index into u_Light[] array (light number)
 */
void ambientLight(int idx)
{
    /* Ambient light component only */
    g_ambient_color  += u_Light[idx].ambient;
}

/*-----------------------------------------------------------------------------------
 * Function: DirectionalLight
 *
 * idx    			- Index into u_Light[] array (light number)
 * normal 			- Normalized Normal vector
 * lightDirection	- Normalized direction from fragment to light.
 * halfVector     	- Normalized Calculated HalfVector for lighting calculations.
 *
 * Note: All parameter must be in the same coordinate space
 */
void directionalLight(int idx, in vec3 normal, in vec3 lightDirection, in vec3 halfVector)
{
    /* Ambient light component */
    g_ambient_color  += u_Light[idx].ambient;

    /* Diffuse light component */
    mediump float diffuse = max(dot(normal, lightDirection), 0.0);
    g_diffuse_color  += u_Light[idx].diffuse * diffuse;

    /* Specular light component */
    mediump float specular = dot(normal, halfVector);
    if (specular > 0.0) {
        specular = pow(specular, u_Material.shininess);
        g_specular_color += u_Light[idx].specular * specular;
    }
}

/*-----------------------------------------------------------------------------------
 * Function: PointLight
 *
 * idx    			- Index into u_Light[] array (light number)
 * normal 			- Normalized Normal vector
 * lightDirection	- UnNormalized direction from fragment to light.
 * halfVector     	- Normalized Calculated HalfVector for lighting calculations.
 *
 * Note: All parameters must be in the same coordinate space
 */
void pointLight(int idx, in vec3 normal, in vec3 lightDirection, in vec3 halfVector)
{
    mediump float diffuse;    // diffuse light contribution
    mediump float specular;   // specular light contribution
    float pf;                 // power factor
    float attenuation = 1.0;  // computed attenuation factor
    float d;                  // distance from surface to light source

    // Compute distance between surface and light position
    d = length(lightDirection);

    // out of range: no light
    if (d > u_Light[idx].range) {
      return;
    }


    // Compute attenuation only if enabled (attenuation.w == 1.0)
    if( u_Light[idx].attenuation.w == 1.0 ) {
        attenuation = 1.0 / (u_Light[idx].attenuation.x + u_Light[idx].attenuation.y * d + u_Light[idx].attenuation.z * d * d);
    }

    /* Ambient light component */
    g_ambient_color  += u_Light[idx].ambient * attenuation;

    /* Diffuse light component */
    diffuse  = max(dot(normal, normalize(lightDirection)), 0.0);
    g_diffuse_color  += u_Light[idx].diffuse * diffuse * attenuation;

    /* Specular light component */
    specular = dot(normal, halfVector);
    if (specular > 0.0) {
        specular = pow(specular, u_Material.shininess);
        g_specular_color += u_Light[idx].specular * specular * attenuation;
    }
}

/*-----------------------------------------------------------------------------------
 * Function: Spotlight
 *
 * idx    				- Index into u_Light[idx][] array (light number)
 * normal 				- Normalized Normal vector
 * lightDirection 		- UnNormalized direction from fragment to light.
 * halfVector     		- Normalized Calculated HalfVector for lighting calculations.
 * spotLightDirection 	- Direction of spot light in the according coordinate space.
 *
 * Note: All parameter must be in the same coordinate space
 */
void spotLight(int idx, in vec3 normal, in vec3 lightDirection, in vec3 halfVector, in vec3 spotLightDirection)
{
    mediump float diffuse;   // diffuse light contribution
    mediump float specular;  // specular light contribution
    float spotDot;           // cosine of angle between spotlight
    float spotAttenuation;   // spotlight attenuation factor
    float d;                 // distance from surface to light source
    float attenuation = 1.0; // computed attenuation factor
    vec3 lightDir;           // Direction from surface to light position

    // Compute distance between surface and light position
    d = length(lightDirection);

    // out of range: no light
    if (d > u_Light[idx].range) {
      return;
    }

    // Normalize vector from surface to light position
    lightDir = normalize(lightDirection);

    // See if point on surface is inside cone of illumination
    spotDot = dot(lightDir, spotLightDirection);

    if (spotDot >= u_Light[idx].spotCosCutoff) {
        // We are inside the cone

        // Compute attenuation only if enabled (attenuation.w == 1.0)
        if( u_Light[idx].attenuation.w == 1.0 ) {
            attenuation = 1.0 / (u_Light[idx].attenuation.x + u_Light[idx].attenuation.y * d + u_Light[idx].attenuation.z * d * d);
        }

        // Combine the spotlight and distance attenuation.
        spotAttenuation = pow(spotDot, u_Light[idx].spotExponent);
        attenuation *= spotAttenuation;

        /* Ambient light component */
        g_ambient_color  += u_Light[idx].ambient * attenuation;

        /* Diffuse light component */
        diffuse  = max(dot(normal, lightDir), 0.0);
        g_diffuse_color  += u_Light[idx].diffuse * diffuse * attenuation;

        /* Specular light component */
        specular = dot(normal, halfVector);
        if (specular > 0.0) {
            specular = pow(specular, u_Material.shininess);
            g_specular_color += u_Light[idx].specular * specular * attenuation;
        }
    }
}

/*-----------------------------------------------------------------------------------
 * Function: anisotropicSpecularComponent
 *
 * normal       - Normalized Normal of geometry in world coordinates.
 * tangent      - Normalized Tangent of geometry in world coordinates.
 * binormal     - Normalized BiNormal of geometry in world coordinates.
 * halfVector   - Calculated HalfVector for lighting calculations.
 * camDirection - Direction from current fragment to camera.
 * dotLN        - Diffuse intensity of light.
 */
float anisotropicSpecularComponent(vec3 normal, vec3 tangent, vec3 binormal, vec3 halfVector, vec3 camDirection, float dotLN)
{ 
    mediump float specular = 0.0;
    
    float dotHN = dot(halfVector, normal);
    float dotVN = dot(camDirection, normal);
    float dotHTAlphaX = dot(halfVector, tangent) / u_AlphaU;
    float dotHBAlphaY = dot(halfVector, binormal) / u_AlphaV;

    specular = ( sqrt(max(0.0, dotLN / dotVN))) * 
        exp(-2.0 * (dotHTAlphaX * dotHTAlphaX + dotHBAlphaY * dotHBAlphaY) / (1.0 + dotHN));

    return clamp(specular, 0.0, 1.0);
}

/*-----------------------------------------------------------------------------------
 * Function: AnisotropicDirectionalLight
 *
 * idx            - Index into u_Light[] array (light number)
 * normal         - Normal vector
 * tangent        - Tangent vector
 * binormal       - BiNormal vector
 * lightDirection - Normalized direction from fragment to light.
 * halfVector     - Normalized Calculated HalfVector for lighting calculations.
 * camDirection   - Normalized Direction from fragment to camera.
 *
 * Note: All parameter must be in the same coordinate space
 */
void anisotropicDirectionalLight(int idx, in vec3 normal, in vec3 tangent, in vec3 binormal, in vec3 lightDirection, in vec3 halfVector, in vec3 camDirection)
{
    /* Ambient light component */
    g_ambient_color  += u_Light[idx].ambient;

    /* Diffuse light component */
    mediump float diffuse = dot(normal, lightDirection);
    
    /* Specular light component */
    if (diffuse > 0.0) {
        mediump float specular = anisotropicSpecularComponent(normal, tangent, binormal, halfVector, camDirection, diffuse);
        g_specular_color += u_Light[idx].specular * specular;
    }

    diffuse = max(diffuse, 0.0);
    g_diffuse_color  += u_Light[idx].diffuse * diffuse;
}

/*-----------------------------------------------------------------------------------
 * Function: AnisotropicPointLight
 *
 * idx            - Index into u_Light[] array (light number)
 * normal         - Normal vector
 * tangent        - Tangent vector
 * binormal       - BiNormal vector
 * lightDirection - UnNormalized direction from fragment to light.
 * halfVector     - Normalized Calculated HalfVector for lighting calculations.
 * camDirection   - Normalized Direction from fragment to camera.
 *
 * Note: All parameters must be in the same coordinate space
 */
void anisotropicPointLight(int idx, in vec3 normal, in vec3 tangent, in vec3 binormal, in vec3 lightDirection, in vec3 halfVector, in vec3 camDirection)
{
    mediump float diffuse;    // Diffuse light contribution.
    mediump float specular;   // Specular light contribution.
    float pf;                 // Power factor.
    float attenuation = 1.0;  // Computed attenuation factor.
    float d;                  // Distance from surface to light source.

    // Compute distance between surface and light position.
    d = length(lightDirection);

    // Out of range: no light.
    if (d > u_Light[idx].range) {
      return;
    }


    // Compute attenuation only if enabled (attenuation.w == 1.0).
    if( u_Light[idx].attenuation.w == 1.0 ) {
        attenuation = 1.0 / (u_Light[idx].attenuation.x + u_Light[idx].attenuation.y * d + u_Light[idx].attenuation.z * d * d);
    }

    /* Ambient light component. */
    g_ambient_color  += u_Light[idx].ambient * attenuation;

    /* Diffuse light component. */
    diffuse  = dot(normalize(lightDirection), normal);

    /* Specular light component. */
    if (diffuse >= 0.0) {
        mediump float specular = anisotropicSpecularComponent(normal, tangent, binormal, halfVector, camDirection, diffuse);
        g_specular_color += u_Light[idx].specular * specular * attenuation;
    }

    diffuse = max(diffuse, 0.0);
    g_diffuse_color  += u_Light[idx].diffuse * diffuse * attenuation;
}

/*-----------------------------------------------------------------------------------
 * Function: AnisotropicSpotlight
 *
 * idx                - Index into u_Light[] array (light number)
 * normal             - Normal vector
 * tangent            - Tangent vector
 * binormal           - BiNormal vector
 * lightDirection     - UnNormalized direction from fragment to light.
 * halfVector         - Normalized Calculated HalfVector for lighting calculations.
 * spotLightDirection - Direction of spot light in the according coordinate space.
 * camDirection       - Normalized Direction from fragment to camera.
 *
 * Note: All parameter must be in the same coordinate space
 */
void anisotropicSpotLight(int idx, in vec3 normal, in vec3 tangent, in vec3 binormal, in vec3 lightDirection, in vec3 halfVector, in vec3 spotLightDirection, in vec3 camDirection)
{
    mediump float diffuse;   // Diffuse light contribution.
    mediump float specular;  // Specular light contribution.
    float spotDot;           // Cosine of angle between spotlight.
    float spotAttenuation;   // Spotlight attenuation factor.
    float d;                 // Distance from surface to light source.
    float attenuation = 1.0; // Computed attenuation factor.
    vec3 lightDir;           // Direction from surface to light position.

    // Compute distance between surface and light position.
    d = length(lightDirection);

    // Out of range: no light.
    if (d > u_Light[idx].range) {
      return;
    }

    // Normalize vector from surface to light position.
    lightDir = normalize(lightDirection);

    // See if point on surface is inside cone of illumination.
    spotDot = dot(lightDir, spotLightDirection);

    if (spotDot >= u_Light[idx].spotCosCutoff) {
        // We are inside the cone.

        // Compute attenuation only if enabled (attenuation.w == 1.0).
        if( u_Light[idx].attenuation.w == 1.0 ) {
            attenuation = 1.0 / (u_Light[idx].attenuation.x + u_Light[idx].attenuation.y * d + u_Light[idx].attenuation.z * d * d);
        }

        // Combine the spotlight and distance attenuation.
        spotAttenuation = pow(spotDot, u_Light[idx].spotExponent);
        attenuation *= spotAttenuation;

        /* Ambient light component. */
        g_ambient_color  += u_Light[idx].ambient * attenuation;

        /* Diffuse light component. */
        diffuse = dot(normal, lightDir);
        g_diffuse_color  += u_Light[idx].diffuse * diffuse * attenuation;

        /* Specular light component. */
        if (diffuse >= 0.0) {
            mediump float specular = anisotropicSpecularComponent(normal, tangent, binormal, halfVector, camDirection, diffuse);
            g_specular_color += u_Light[idx].specular * specular * attenuation;
        }

        diffuse = max(diffuse, 0.0);
        g_diffuse_color  += u_Light[idx].diffuse * diffuse * attenuation;
    }
}
//END INCLUDE
//START INCLUDE RefTexturing.inc
/*
 * The include file TextureMapping.incv for vertex shaders implements various texture mapping helper functions.  
 * 
 * Following functions are provided:
 * sphereMap: Generates a texture coordinate in a 2D Texture known as Sphere Map
 */


/**
 * Function sphereMap generates a 2D texture coordinate to look up in a SphereMap image, 
 * which has been bound as a texture for environmental mapping. 
 * @param position	is the normalized coordinate of the vertex in eye space (Model-view space)
 * @param normal	is the normalized normal coordinate in eye space (Model-view space)
 * @returns			a vec2 texture coordinate
 */ 
vec2 sphereMap(vec3 position, vec3 normal)
{
	// Reflect incident eye vector. Since the computation is performed in eye coordinates, the eye is 
	// located at the origin and the direction vector is equal to the normalized vertex position. 
	vec3 reflection= reflect(position, normal);
	
	// Calculate 2D texture coordinate
	float m = 2.0 * sqrt( reflection.x * reflection.x + reflection.y * reflection.y + (reflection.z + 1.0) * (reflection.z + 1.0) );						  
	return (reflection / m + 0.5).xy;  
}
//END INCLUDE

/*
 * Uniforms
 */
uniform mat4 u_MVPMatrix;       // Model-View-Projection Matrix
uniform mediump mat4  u_MVMatrix;        // Model-View Matrix for Transformation positions into eye space
uniform mediump mat3 u_NormalMVMatrix3;       // Model-View Matrix for Transformation normals into eye space

/*
 * Attributes
 */
attribute vec4 a_Position;      // Vertex position
attribute vec3 a_Normal;        // Normal vector of vertex

/*
 * Varyings
 */
varying mediump vec4 v_Color;   // Lit material color
varying mediump vec2 v_TexCoord;        // Generated 2D Texture coordinate


/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{
    vec3 position = a_Position.xyz;

    /* Transform vertex into world space */
    gl_Position = u_MVPMatrix * a_Position;

    /* Transform position and normal into eye space */
    vec4 positionInEyeSpace = u_MVMatrix * a_Position;
    vec3 normalInEyeSpace= u_NormalMVMatrix3 * a_Normal;

    /* Retrieve sphere map texture coords */
    v_TexCoord = sphereMap(normalize(positionInEyeSpace.xyz), normalize(normalInEyeSpace));

     /* Clear the light intensity accumulators */
    g_ambient_color  = vec4(0.0);
    g_diffuse_color  = vec4(0.0);
    g_specular_color = vec4(0.0);


    /* Calculate light color coefficients for light index 0. */
    if (u_Light[0].enabled == 1) {
        int lightType = u_Light[0].type;
        if      (lightType == 0) { ambientLight(0); }
        else if (lightType == 1) { directionalLight(0, normalize(a_Normal), u_Light[0].direction, u_Light[0].halfplane ); }
        else if (lightType == 2) {
            vec3 lightDirection = u_Light[0].position - position;
            vec3 halfVector = lightDirection + u_Light[0].cameraLookAtVector;
            pointLight(0, normalize(a_Normal), lightDirection, normalize(halfVector)); 
        }
        else if (lightType == 3) { 
            vec3 lightDirection = u_Light[0].position - position;
            vec3 halfVector = lightDirection + u_Light[0].cameraLookAtVector;
            spotLight(0, normalize(a_Normal), lightDirection, normalize(halfVector), u_Light[0].direction);  
        }
    }

    /* Calculate light color coefficients for light index 1. */
    if (u_Light[1].enabled == 1) {
        int lightType = u_Light[1].type;
        if      (lightType == 0) { ambientLight(1); }
        else if (lightType == 1) { directionalLight(1, normalize(a_Normal), u_Light[1].direction, u_Light[1].halfplane ); }
        else if (lightType == 2) {
            vec3 lightDirection = u_Light[1].position - position;
            vec3 halfVector = lightDirection + u_Light[1].cameraLookAtVector;
            pointLight(1, normalize(a_Normal), lightDirection, normalize(halfVector)); 
        }
        else if (lightType == 3) { 
            vec3 lightDirection = u_Light[1].position - position;
            vec3 halfVector = lightDirection + u_Light[1].cameraLookAtVector;
            spotLight(1, normalize(a_Normal), lightDirection, normalize(halfVector), u_Light[1].direction);  
        }
    }

    /* Multiply light color coefficients with corresponding material components */
    v_Color = u_Material.emissive;
    v_Color += g_ambient_color * u_Material.ambient;
    v_Color += g_diffuse_color * u_Material.diffuse;
    v_Color += g_specular_color * u_Material.specular;

    /* Assign alpha value */
    v_Color.a = u_Material.diffuse.a;
}
