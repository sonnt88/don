/**
  * This Fragment Shader supports:
  * - Mixing two texture coordinates of a CubeMap texture with a custom ratio.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

/*
 * Uniforms
 */ 
uniform samplerCube u_CubeMapTexture;

/*
 * Varyings
 */
varying mediump vec3 v_CubeTexCoord[2];
varying mediump float v_Ratio;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{        
    gl_FragColor = (1.0 -  v_Ratio ) * textureCube(u_CubeMapTexture, v_CubeTexCoord[0]) + v_Ratio * textureCube(u_CubeMapTexture, v_CubeTexCoord[1]);
}
