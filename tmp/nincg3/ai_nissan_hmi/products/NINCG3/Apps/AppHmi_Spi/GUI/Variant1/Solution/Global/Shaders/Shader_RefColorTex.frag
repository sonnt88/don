/**
  * This Fragment Shader supports:
  * - Assignment of product of varying color and texture color to fragment color.
  */

//START INCLUDE RefEnvironment.inc
/*
 * Candera shader environment definitions.
 */

#ifndef GL_ES

// An OpenGL environment might not support precision qualifiers. Thus, define them to void.
#define lowp 
#define mediump 
#define highp 
#define precision

#endif
//END INCLUDE

precision mediump float; 

/*
 * Uniforms
 */ 
uniform sampler2D u_Texture;

/*
 * Varyings
 */
varying vec2 v_TexCoord;
varying vec4 v_Color;

/*---------------------------------- MAIN ------------------------------------*/
void main(void)
{        
    gl_FragColor = texture2D(u_Texture, v_TexCoord) * v_Color;
}
