/**
 * This is the class registered as 'Controller' at Courier message framework
 *
 * when there is no handling for a message in the statemachine, the message comes next here
 */

#include "gui_std_if.h"

#include "AppHmi_SpiStateMachine.h"
#include "CGIAppController.h"
