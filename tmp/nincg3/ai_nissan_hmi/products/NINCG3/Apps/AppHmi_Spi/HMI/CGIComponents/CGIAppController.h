#ifndef CGIAPPCONTROLLER_H
#define CGIAPPCONTROLLER_H

#include "Common/CGIAppController/CGIAppControllerProject.h"

class CGIAppController : public CGIAppControllerProject
{
   courier_messages:
      COURIER_MSG_MAP_BEGIN(0)
      COURIER_CASE_DUMMY_ENTRY()
      COURIER_MSG_MAP_END_DELEGATE(CGIAppControllerProject)
   public:
      using CGIAppControllerProject::onCourierMessage;
      CGIAppController(hmibase::services::hmiappctrl::ProxyHandler& proxyHandler) : CGIAppControllerProject(proxyHandler) {}
};
#endif // CGIAPPCONTROLLER_H
