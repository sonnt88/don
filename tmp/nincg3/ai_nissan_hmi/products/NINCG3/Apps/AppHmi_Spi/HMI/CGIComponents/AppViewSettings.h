
#if !defined(AppViewSettings_h)
#define AppViewSettings_h

#include "AppBase/ScreenBrokerClient/IAppViewSettings.h"
#include "AppUtils/Singleton.h"

class AppViewSettings : public IAppViewSettings
{
      //Lazy initialization of application view settings singleton
      HMIBASE_SINGLETON_CLASS(AppViewSettings)
};

#endif // AppViewSettings_h
