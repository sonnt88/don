#ifndef _CGIAPP_H
#define _CGIAPP_H

// enable this include and pass KeyMapping class as addition template parameter to GraphicAdapter
#include "HMI/CGIComponents/CGIAppViewFactory.h"
#include "HMI/CGIComponents/CGIAppController.h"
#include "AppHmi_SpiStateMachine.h"
#include "AppBase/CgiApplication.hpp"

class CGIApp : public CgiApplication<AppHmi_SpiStateMachineImpl>
{
   public:
      CGIApp(const char* assetFile, hmibase::services::hmiappctrl::ProxyHandler&);
      virtual ~CGIApp();

};

#endif /* _CGIAPP_H */
