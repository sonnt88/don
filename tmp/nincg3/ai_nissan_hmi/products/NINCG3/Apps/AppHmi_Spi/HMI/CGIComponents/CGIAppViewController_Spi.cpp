/**
 * receive messages subscribed with 'View' here, when it has not been consumed by any widget
 */

#include "gui_std_if.h"

#include "AppHmi_SpiStateMachine.h"
#include "CGIAppViewController_Spi.h"

const char* CGIAppViewController_MyDummyScene::_name = "M1#Scenes#DUMMY";
