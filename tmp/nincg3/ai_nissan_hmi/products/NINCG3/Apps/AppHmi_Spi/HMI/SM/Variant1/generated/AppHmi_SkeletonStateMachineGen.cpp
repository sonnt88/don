/*
 * Id:        AppHmi_SkeletonStateMachineGen.cpp
 *
 * Function:  VS System Source File.
 *
 * Generated: Mon Mar 07 15:16:30 2016
 *
 * Coder 7, 3, 2, 2426
 * 
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include "AppHmi_SkeletonStateMachineSEMLibB.h"


#include "AppHmi_SkeletonStateMachineGen.h"


#include "AppHmi_SkeletonStateMachineData.h"


/*
 * VS System data definition and initialization. Rule data format number: 0
 */
AppHmi_SkeletonStateMachine::VSDATA const AppHmi_SkeletonStateMachine::VS = 
{
  {
    0x20u, 0x00u, 0x05u, 0x06u
  },
  {
    0x00u
  },
  {
    0x00u, 0x01u, 0x01u, 0x01u, 0x01u, 0x01u, 0x01u, 0x01u, 
    0x01u, 0x01u, 0x01u, 0x01u, 0x01u, 0x01u, 0x01u, 0x01u, 
    0x01u, 0x01u, 0x01u, 0x01u, 0x01u, 0x01u, 0x01u, 0x01u, 
    0x01u, 0x01u
  },
  {
    'S', 'E', '_', 'R', 'E', 'S', 'E', 'T', '\0',
    'o', 'n', 'E', 'n', 'c', 'o', 'd', 'e', 'r', 'S', 't', 'a', 't', 'u', 's', 'C', 
    'h', 'a', 'n', 'g', 'e', 'd', '_', 'R', 'I', 'G', 'H', 'T', '_', 'E', 'N', 'C', '\0',
    'o', 'n', 'H', 'M', 'I', 'S', 'u', 'b', 'S', 't', 'a', 't', 'e', 'C', 'h', 'a', 
    'n', 'g', 'e', 'd', '_', 'P', 'H', 'O', 'N', 'E', '\0',
    'o', 'n', 'H', 'M', 'I', 'S', 'u', 'b', 'S', 't', 'a', 't', 'e', 'C', 'h', 'a', 
    'n', 'g', 'e', 'd', '_', 'C', 'L', 'O', 'C', 'K', '\0',
    'o', 'n', 'H', 'M', 'I', 'S', 'u', 'b', 'S', 't', 'a', 't', 'e', 'C', 'h', 'a', 
    'n', 'g', 'e', 'd', '_', 'O', 'N', '\0',
    'o', 'n', 'H', 'M', 'I', 'S', 'u', 'b', 'S', 't', 'a', 't', 'e', 'C', 'h', 'a', 
    'n', 'g', 'e', 'd', '_', 'N', 'O', '_', 'D', 'I', 'S', 'P', 'L', 'A', 'Y', '\0',
    'o', 'n', 'H', 'M', 'I', 'S', 'u', 'b', 'S', 't', 'a', 't', 'e', 'C', 'h', 'a', 
    'n', 'g', 'e', 'd', '_', 'O', 'F', 'F', '\0',
    'o', 'n', 'H', 'M', 'I', 'S', 'u', 'b', 'S', 't', 'a', 't', 'e', 'C', 'h', 'a', 
    'n', 'g', 'e', 'd', '_', 'S', 'T', 'A', 'N', 'D', 'B', 'Y', '\0',
    'o', 'n', 'H', 'M', 'I', 'S', 'u', 'b', 'S', 't', 'a', 't', 'e', 'C', 'h', 'a', 
    'n', 'g', 'e', 'd', '_', 'S', 'T', 'A', 'N', 'D', 'B', 'Y', '_', 'R', 'E', 'S', 
    'T', 'R', 'I', 'C', 'T', 'E', 'D', '\0',
    'o', 'n', 'H', 'K', 'S', 't', 'a', 't', 'u', 's', 'C', 'h', 'a', 'n', 'g', 'e', 
    'd', '_', 'H', 'K', '_', 'S', 'E', 'L', 'E', 'C', 'T', '_', 'U', 'P', '\0',
    'o', 'n', 'A', 'p', 'p', 'S', 't', 'a', 't', 'u', 's', 'C', 'h', 'a', 'n', 'g', 
    'e', 'd', '_', 'F', 'O', 'R', 'E', 'G', 'R', 'O', 'U', 'N', 'D', '\0',
    'o', 'n', 'A', 'p', 'p', 'S', 't', 'a', 't', 'u', 's', 'C', 'h', 'a', 'n', 'g', 
    'e', 'd', '_', 'B', 'A', 'C', 'K', 'G', 'R', 'O', 'U', 'N', 'D', '\0',
    'o', 'n', 'H', 'M', 'I', 'S', 'u', 'b', 'S', 't', 'a', 't', 'e', 'C', 'h', 'a', 
    'n', 'g', 'e', 'd', '_', 'S', 'E', 'C', 'U', 'R', 'E', '\0',
    'O', 'n', 'N', 'e', 'x', 't', 'L', 'e', 'v', 'e', 'l', 'T', 'r', 'a', 'n', 's', 
    'i', 't', 'i', 'o', 'n', 'F', 'i', 'n', 'i', 's', 'h', 'e', 'd', '\0',
    'O', 'n', 'P', 'r', 'e', 'v', 'i', 'o', 'u', 's', 'L', 'e', 'v', 'e', 'l', 'T', 
    'r', 'a', 'n', 's', 'i', 't', 'i', 'o', 'n', 'F', 'i', 'n', 'i', 's', 'h', 'e', 
    'd', '\0',
    'O', 'n', 'P', 'a', 'g', 'e', 'F', 'l', 'i', 'p', 'B', 'a', 'c', 'k', 'F', 'i', 
    'n', 'i', 's', 'h', 'e', 'd', '\0',
    'O', 'n', 'P', 'a', 'g', 'e', 'F', 'l', 'i', 'p', 'F', 'r', 'o', 'n', 't', 'F', 
    'i', 'n', 'i', 's', 'h', 'e', 'd', '\0',
    'O', 'n', 'Z', 'o', 'o', 'm', 'I', 'n', 'F', 'i', 'n', 'i', 's', 'h', 'e', 'd', '\0',
    'O', 'n', 'Z', 'o', 'o', 'm', 'O', 'u', 't', 'F', 'i', 'n', 'i', 's', 'h', 'e', 
    'd', '\0',
    'O', 'n', 'F', 'a', 'd', 'e', 'A', 'n', 'i', 'm', 'a', 't', 'i', 'o', 'n', 'F', 
    'i', 'n', 'i', 's', 'h', 'e', 'd', '\0',
    'O', 'n', 'D', 'u', 'a', 'l', 'V', 'i', 'e', 'w', 'L', 'e', 'f', 't', 'T', 'r', 
    'a', 'n', 's', 'i', 't', 'i', 'o', 'n', 'F', 'i', 'n', 'i', 's', 'h', 'e', 'd', '\0',
    'O', 'n', 'D', 'u', 'a', 'l', 'V', 'i', 'e', 'w', 'R', 'i', 'g', 'h', 't', 'T', 
    'r', 'a', 'n', 's', 'i', 't', 'i', 'o', 'n', 'F', 'i', 'n', 'i', 's', 'h', 'e', 
    'd', '\0',
    'o', 'n', 'S', 'u', 'b', 'S', 'u', 'r', 'f', 'a', 'c', 'e', 'U', 'p', 'd', 'M', 
    's', 'g', 'C', 'h', 'a', 'n', 'g', 'e', 'd', '_', 'F', 'O', 'R', 'E', 'G', 'R', 
    'O', 'U', 'N', 'D', '\0',
    'o', 'n', 'S', 'u', 'b', 'S', 'u', 'r', 'f', 'a', 'c', 'e', 'U', 'p', 'd', 'M', 
    's', 'g', 'C', 'h', 'a', 'n', 'g', 'e', 'd', '_', 'B', 'A', 'C', 'K', 'G', 'R', 
    'O', 'U', 'N', 'D', '\0',
    'o', 'n', 'A', 'p', 'p', 'S', 't', 'a', 't', 'u', 's', 'P', 'r', 'e', 'p', 'a', 
    'r', 'e', '_', 'B', 'A', 'C', 'K', 'G', 'R', 'O', 'U', 'N', 'D', '\0'
  },
  {
    0x0000u, 0x0009u, 0x002Au, 0x0045u, 0x0060u, 0x0078u, 0x0098u, 0x00B1u, 
    0x00CEu, 0x00F6u, 0x0115u, 0x0133u, 0x0151u, 0x016Du, 0x018Bu, 0x01ADu, 
    0x01C4u, 0x01DCu, 0x01EDu, 0x01FFu, 0x0217u, 0x0238u, 0x025Au, 0x027Fu, 
    0x02A4u
  },
  {
    'g', 'a', 'c', 'D', 'e', 'c', 'r', 'e', 'm', 'e', 'n', 't', 'A', 'c', 't', 'i', 
    'v', 'e', 'A', 'n', 'i', 'm', 'a', 't', 'i', 'o', 'n', 'C', 'o', 'u', 'n', 't', '\0',
    'g', 'a', 'c', 'P', 'o', 'p', 'u', 'p', 'F', 'i', 'l', 't', 'e', 'r', 'A', 'l', 
    'l', 'E', 'n', 'a', 'b', 'l', 'e', 'R', 'e', 'q', '\0',
    'g', 'a', 'c', 'P', 'o', 'p', 'u', 'p', 'F', 'i', 'l', 't', 'e', 'r', 'D', 'i', 
    's', 'a', 'b', 'l', 'e', 'R', 'e', 'q', '\0',
    'g', 'a', 'c', 'D', 'i', 's', 'p', 'l', 'a', 'y', 'F', 'o', 'o', 't', 'e', 'r', 
    'L', 'i', 'n', 'e', 'R', 'e', 'q', '\0',
    'g', 'a', 'c', 'H', 'i', 'd', 'e', 'F', 'o', 'o', 't', 'e', 'r', 'L', 'i', 'n', 
    'e', 'R', 'e', 'q', '\0'
  },
  {
    0x00u, 0x21u, 0x3Cu, 0x55u, 0x6Du
  }
};
