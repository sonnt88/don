#include "gui_std_if.h"

#include "CGIApp.h"

CGIApp::CGIApp(const char* assetFile, hmibase::services::hmiappctrl::ProxyHandler& proxyHandler)
   : CgiApplication<AppHmi_SpiStateMachineImpl>(assetFile, proxyHandler)
{
}

CGIApp::~CGIApp()
{
}

