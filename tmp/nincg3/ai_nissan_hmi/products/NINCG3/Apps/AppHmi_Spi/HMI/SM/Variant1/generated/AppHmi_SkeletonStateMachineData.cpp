/*
 * Id:        AppHmi_SkeletonStateMachineData.cpp
 *
 * Function:  VS System Data Source File.
 *
 * Generated: Mon Mar 07 15:16:30 2016
 *
 * Coder 7, 3, 2, 2426
 * 
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include "AppHmi_SkeletonStateMachineSEMLibB.h"


#include "AppHmi_SkeletonStateMachineData.h"


#include <stdarg.h>


/*
 * SEM Deduct Function.
 */
unsigned char AppHmi_SkeletonStateMachine::SEM_Deduct (SEM_EVENT_TYPE EventNo, ...)
{
  va_list ap;

  va_start(ap, EventNo);
  if (SEM.State == 0x00u /* STATE_SEM_NOT_INITIALIZED */)
  {
    return SES_NOT_INITIALIZED;
  }
  if (VS_NOF_EVENTS <= EventNo)
  {
    return (SES_RANGE_ERR);
  }
  switch (EventNo)
  {
  case 1:
    EventArgsVar.DB1.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 22:
    EventArgsVar.DB22.VS_INT32Var[0] = (VS_INT32) va_arg(ap, VS_INT32_VAARG);
    break;

  case 23:
    EventArgsVar.DB22.VS_INT32Var[0] = (VS_INT32) va_arg(ap, VS_INT32_VAARG);
    break;

  default:
    break;
  }
  SEM.EventNo = EventNo;
  SEM.DIt = 2;
  SEM.State = 0x02u; /* STATE_SEM_PREPARE */

  va_end(ap);
  return (SES_OKAY);
}


/*
 * Action Expressions Wrapper Function.
 */
VS_VOID AppHmi_SkeletonStateMachine::VSAction (SEM_ACTION_EXPRESSION_TYPE i)
{
  switch (i)
  {
  case 5:
    gacViewCreateReq(M1_Scenes_DUMMY);
    break;
  case 6:
    gacViewShowReq(M1_Scenes_DUMMY);
    break;

  default:
    break;
  }
}
