#ifndef _visualSTATE_APPHMI_SKELETONSTATEMACHINESEMBDEF_H
#define _visualSTATE_APPHMI_SKELETONSTATEMACHINESEMBDEF_H

/*
 * Id:        AppHmi_SkeletonStateMachineSEMBDef.h
 *
 * Function:  SEM Defines Header File.
 *
 * Generated: Mon Mar 07 15:16:30 2016
 *
 * Coder 7, 3, 2, 2426
 * 
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include "AppHmi_SkeletonStateMachineSEMTypes.h"


/*
 * Number of Identifiers.
 */
#define VS_NOF_ACTION_EXPRESSIONS        0X007u  /*   7 */
#define VS_NOF_ACTION_FUNCTIONS          0X005u  /*   5 */
#define VS_NOF_EVENT_GROUPS              0X000u  /*   0 */
#define VS_NOF_EVENTS                    0X019u  /*  25 */
#define VS_NOF_EXTERNAL_VARIABLES        0X000u  /*   0 */
#define VS_NOF_GUARD_EXPRESSIONS         0X000u  /*   0 */
#define VS_NOF_INSTANCES                 0X001u  /*   1 */
#define VS_NOF_INTERNAL_VARIABLES        0X000u  /*   0 */
#define VS_NOF_SIGNALS                   0X000u  /*   0 */
#define VS_NOF_STATE_MACHINES            0X000u  /*   0 */
#define VS_NOF_STATES                    0X000u  /*   0 */


/*
 * Functional expression type definitions
 */
typedef VS_BOOL (* VS_GUARDEXPR_TYPE) (VS_VOID);
typedef VS_VOID (* VS_ACTIONEXPR_TYPE) (VS_VOID);


#endif /* ifndef _visualSTATE_APPHMI_SKELETONSTATEMACHINESEMBDEF_H */
