#include "gui_std_if.h"
#include "CgiExtensions/PopupConfig.h"

//#include "CGIAppViewController_ABC.h"
//#include "CGIAppViewController_XYZ.h"
#include "ProjectBaseTypes.h"

POPUP_TABLE_BEGIN() // popup ID, modality, priority, presentation time, min. presentation time, validity period, close on superseded, close on app leave, surface ID, path in asset
//POPUP_TABLE_ENTRY(ScreenBroker::Modality::Application, 10, 0, 2000, 0, false, false, SURFACEID_NNN, CGIAppViewController_ABC")
//POPUP_TABLE_ENTRY(ScreenBroker::Modality::System, 10, 0, 2000, 0, false, false, SURFACEID_NNN, CGIAppViewController_XYZ)
POPUP_TABLE_ENTRY_DUMMY()
POPUP_TABLE_END()
