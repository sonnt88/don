#if !defined (_CGIAPPVIEWCONTROLLER_SPI_H)
#define _CGIAPPVIEWCONTROLLER_SPI_H

class CGIAppViewController_MyDummyScene : public Courier::ViewController
{
   private:
   courier_messages:

      COURIER_MSG_MAP_BEGIN(0)
      COURIER_DUMMY_CASE(0)
      COURIER_MSG_MAP_END()
   public:
      CGIAppViewController_MyDummyScene() {}
      virtual ~CGIAppViewController_MyDummyScene() {}
      static const char* _name;
      static Courier::ViewController* createInstance()
      {
         return COURIER_NEW(CGIAppViewController_MyDummyScene)();
      }

};

#endif // _CGIAPPVIEWCONTROLLER_SPI_H

