#include "gui_std_if.h"

#include "CGIAppViewFactory.h"
#include "AppHmi_SpiMessages.h"
// the views
#include "CGIAppViewController_Spi.h"


SCENE_MAPPING_BEGIN(aScenes)
SCENE_MAPPING_ENTRY_2D(CGIAppViewController_MyDummyScene),
                       SCENE_MAPPING_END()

                       CGIAppViewFactory::CGIAppViewFactory(): CGIAppViewFactoryBase(TABSIZE(aScenes), aScenes)
{

}

Courier::View* CGIAppViewFactory::Create(const Courier::Char* sViewName)
{
   return SceneMapping::createView(sViewName, aScenes, TABSIZE(aScenes), GetViewHandler());
}

void CGIAppViewFactory::Destroy(Courier::View* pView)
{
   SceneMapping::destroyView(aScenes, TABSIZE(aScenes), pView);
}

Courier::ViewController* CGIAppViewControllerFactory::Create(const Courier::Char* sViewName)
{
   return SceneMapping::createViewController(sViewName, aScenes, TABSIZE(aScenes));
}

void CGIAppViewControllerFactory::Destroy(Courier::ViewController* viewController)
{
   COURIER_UNUSED(viewController);
}

