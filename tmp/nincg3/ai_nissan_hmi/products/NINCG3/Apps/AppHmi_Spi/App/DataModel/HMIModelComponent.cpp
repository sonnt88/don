#include "gui_std_if.h"

#include "AppUtils/StrUtf8.h"
#include "AppHmi_SpiStateMachine.h"
#include "HMIModelComponent.h"
#include "hmi_trace_if.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_APPHMI_SPI_DM
#include "trcGenProj/Header/HMIModelComponent.cpp.trc.h"
#endif

HMIModelComponent::HMIModelComponent()  : m_DummyData()
{
}

HMIModelComponent::~HMIModelComponent()
{
}

bool HMIModelComponent::Init()
{
   return true;
}


/**
 * @brief   : Incomming message for HMI data model a message component
 * @param   :
 */
bool HMIModelComponent::onCourierMessage(const Courier::StartupMsg& /*oMsg*/)
{
   return true;
}


bool HMIModelComponent::onCourierMessage(const DummyChangedUpdMsg& /*oMsg*/)
{
//   ETG_TRACE_COMP(("OnNewMessage(const DummyUpd"));
//   Courier::UInt32 const&   dummy = oMsg.GetDummyValue();
//
//   m_DummyData.SetValue(ItemKey::Dummy::Dummy_2Item, dummy);
//   m_DummyData.MarkItemModified(ItemKey::Dummy::DummyValueItem);
//   m_DummyData.SendUpdate(true);
   return true;
}


