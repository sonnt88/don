/****************************************************************************
 * Copyright (C) Robert Bosch Car Multimedia GmbH, 2012
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ***************************************************************************/
#ifndef SpiHall_h
#define SpiHall_h

#include "CourierTunnelService/CourierMessageReceiverStub.h"
#include "AppBase/HallComponentBase.h"

#include "AppHmi_SpiMessages.h"

namespace App
{
namespace Core
{

class SpiHall
   : public HallComponentBase
{

   public:
      SpiHall();
      virtual ~SpiHall();

      // TimerCallbackIF
      virtual void onExpired(asf::core::Timer& timer, boost::shared_ptr<asf::core::TimerPayload> data);

   protected:
//      virtual bool onCourierMessage(const MyMessage& oMsg);

//      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_SPI_COURIER_PAYLOAD_MODEL_COMP)
//         ON_COURIER_MESSAGE(MyMessage)
//      COURIER_MSG_MAP_DELEGATE_START()
//         COURIER_MSG_DELEGATE_TO_CLASS(HallComponentBase)
//      COURIER_MSG_MAP_DELEGATE_END()

   private:

      DECLARE_CLASS_LOGGER ();
};

} // namespace Core
} // namespace App


#endif
