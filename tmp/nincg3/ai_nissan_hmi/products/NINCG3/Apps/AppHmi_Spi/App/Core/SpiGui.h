/****************************************************************************
 * Copyright (C) Robert Bosch Car Multimedia GmbH, 2013
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ***************************************************************************/
#ifndef SpiGui_h
#define SpiGui_h

#include "AppBase/GuiComponentBase.h"
#include "HMIAppCtrl/Proxy/ProxyHandler.h"

// datapool for persistent data
//#define DP_S_IMPORT_INTERFACE_FI
//#include "dp_hmi_01_if.h"

namespace App
{
namespace Core
{


class SpiGui :
   public GuiComponentBase
{
   public:
      SpiGui();
      virtual ~SpiGui();

      virtual void preRun();
      virtual void postRun();

      static void TraceCmd_NotProcessedMsg (const unsigned char* pcu8Data);

   protected:
      virtual unsigned int getDefaultTraceClass();
      virtual void setupCgiInstance();

   private:

      void PersistentValuesRead();
      void PersistentValuesWrite();
      //HmiAppSpi::PersMemVars _mvar;

      hmibase::services::hmiappctrl::ProxyHandler _hmiAppCtrlProxyHandler;
};


} // namespace Core
} // namespace App


#endif
