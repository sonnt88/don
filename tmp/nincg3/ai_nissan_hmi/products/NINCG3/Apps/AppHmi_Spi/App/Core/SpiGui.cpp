/****************************************************************************
 * Copyright (C) Robert Bosch Car Multimedia GmbH, 2013
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ***************************************************************************/
#include "gui_std_if.h"

#include "SpiGui.h"
#include "HMI/CGIComponents/AppViewSettings.h"
#include "HMI/CGIComponents/CGIApp.h"
#include "AppBase/IApplicationSettings.h"
#include "Common/AppBaseSettings/KeyMapping.h"

//////// TRACE IF ///////////////////////////////////////////////////
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SPI_MAIN
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SPI
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SPI_"
#define ETG_I_FILE_PREFIX                 App::Core::SpiGui::
#include "trcGenProj/Header/SpiGui.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN
//////// TRACE IF ///////////////////////////////////////////////////

//                  main surface, 	             video surface,  popup surfaces
DEFAULT_APPSETTINGS(SURFACEID_MAIN_SURFACE_SPI, SURFACEID_NONE, SURFACEID_TOP_POPUP_SURFACE_SPI, SURFACEID_CENTER_POPUP_SURFACE_SPI)

using namespace ::hmi;

namespace App
{
namespace Core
{


SpiGui::SpiGui() : GuiComponentBase(hmi::apps::appHmi_Spi, appSettings)
{
   ETG_I_REGISTER_FILE();
   ETG_I_REGISTER_CHN(TraceCmd_NotProcessedMsg);
   _hmiAppCtrlProxyHandler.setTraceId(TR_CLASS_APPHMI_SPI_APPCTRL_PROXY);
}


SpiGui::~SpiGui()
{

}

unsigned int SpiGui::getDefaultTraceClass()
{
   return TR_CLASS_APPHMI_SPI_MAIN;
}

void SpiGui::setupCgiInstance()
{
   std::string assetFileName = SystemConfiguration::getAssetFileName();
   setCgiApp(new CGIApp(assetFileName.c_str(), _hmiAppCtrlProxyHandler));
}

void SpiGui::preRun()
{
//	DP_vCreateDatapool();
   PersistentValuesRead();
}

void SpiGui::postRun()
{
   PersistentValuesWrite();
}

void SpiGui::PersistentValuesRead()
{
//   dp_tclhmiAppSpiPersMemVarsSpi dp;
//   _mvar = dp.tGetData();
}

void SpiGui::PersistentValuesWrite()
{
//   dp_tclhmiAppSpiPersMemVarsSpi dp;
//   dp.s32SetData(_mvar);
}


/**
 *sink for not processed ttfis input messages
 */
void SpiGui::TraceCmd_NotProcessedMsg (const unsigned char* data)
{
   if (data)
   {
      ETG_TRACE_FATAL(("TraceCmd_NotProcessedMsg() : 0x%*x", ETG_LIST_LEN(data[0]), ETG_LIST_PTR_T8(data) ));
   }
}

} // namespace Core
} // namespace App
