#if !defined(_HMI_MODEL_COMPONENT_H)
#define _HMI_MODEL_COMPONENT_H
#include "CgiExtensions/ListDataProviderDistributor.h"
#include "AppHmi_SpiStateMachine.h"
#include "CgiExtensions/DataBindingItem.hpp"

class HMIModelComponent : public Courier::ModelComponent
{
   private:
      //  Binding source instances
      DataBindingItem<DummyDataBindingSource>m_DummyData;

      // Incomming events ...
      virtual bool onCourierMessage(const Courier::StartupMsg& oMsg);
      virtual bool onCourierMessage(const DummyChangedUpdMsg& oMsg);


      COURIER_MSG_MAP_BEGIN(0)
      ON_COURIER_MESSAGE(Courier::StartupMsg)
      ON_COURIER_MESSAGE(DummyChangedUpdMsg)
      COURIER_MSG_MAP_END()

   public:
      HMIModelComponent();
      virtual ~HMIModelComponent();
      bool Init();
};

#endif // _HMI_MODEL_COMPONENT_H

