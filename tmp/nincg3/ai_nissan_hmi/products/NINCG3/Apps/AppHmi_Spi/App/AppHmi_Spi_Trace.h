#ifndef APPHMI_SPI_TRACE_H
#define APPHMI_SPI_TRACE_H

#ifndef HMI_TRACE_IF_INCLUDED
#error "Please include hmi_trace_if.h only"
#endif

// default trace set will be defined in Project_Trace.h by using the macro APPHMI_STANDARD_TRACE_SET
// define your own trace classe here, beginning with id TR_CLASSOFFSET_APPHMI_USER1 as offset to your base id,
// see hmi_trace_if.h

#endif // APPHMI_SPI_TRACE_H

