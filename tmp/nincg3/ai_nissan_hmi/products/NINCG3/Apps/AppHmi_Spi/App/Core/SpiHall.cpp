/****************************************************************************
 * Copyright (C) Robert Bosch Car Multimedia GmbH, 2012
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ***************************************************************************/
#include "hall_std_if.h"
#include "App/Core/SpiHall.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SPI_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SPI
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SPI_"
#define ETG_I_FILE_PREFIX                 App::Core::SpiHall::
#include "trcGenProj/Header/SpiHall.cpp.trc.h"
#endif

using namespace ::App::Core;
using namespace ::CourierTunnelService::CourierMessageReceiver;


namespace App
{
namespace Core
{

DEFINE_CLASS_LOGGER_AND_LEVEL ("App/Core", SpiHall, Info);


SpiHall::SpiHall()
   : HallComponentBase("", "App.Core.AppHmi_Spi", "/org/genivi/NodeStateManager/LifeCycleConsumer/AppHmi_Spi")
{
   ETG_I_REGISTER_FILE();
}


SpiHall::~SpiHall()
{
}



void SpiHall::onExpired(asf::core::Timer& /*timer*/, boost::shared_ptr<asf::core::TimerPayload> /*data*/)
{
//   ETG_TRACE_USR4(("SPIAPP : Received Timer expired\n"));
//   if (timer.getAct()  == Manualtune_timer.getAct())
//   {
//      if (data->getReason() == asf::core::TimerPayload_Reason__Completed)
//      {
//      }
//   }
//   else
//   {
//   }
}

} // namespace Core
} // namespace App

