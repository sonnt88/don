#!/bin/bash
dir=$(dirname $0)

umlFiles=$(find $dir -type f | xargs grep -l -E "^@startuml" | sort)
echo "converting PlantUML files ..."
for file in $umlFiles; do
    echo $file
done
java -jar $dir/../../../fc_sds_adapter/tools/plantuml/plantuml.jar $umlFiles

a2x --asciidoc-opts=--conf-file=./lang-en.conf --asciidoc-opts=--theme=flask --format=chunked  SdsHmiApplication.adoc
a2x --asciidoc-opts=--conf-file=./lang-en.conf --asciidoc-opts=--theme=flask --format=pdf      SdsHmiApplication.adoc
#a2x --asciidoc-opts=--conf-file=./Res/lang-en.conf --asciidoc-opts=--theme=flask --format=xhtml    SdsHmiApplication.adoc
asciidoc --theme=flask SdsHmiApplication.adoc
asciidoc --theme=flask index.adoc

# create archive
tar -cf ./SdsHmiApplication_docs.tar ./SdsHmiApplication.chunked ./SdsHmiApplication.pdf ./SdsHmiApplication.html ./index.html
find . -name '*.adoc' | xargs tar -uf ./SdsHmiApplication_docs.tar
find . -name '*.png' -o -name '*.jpg' | xargs tar -uf ./SdsHmiApplication_docs.tar
gzip -f ./SdsHmiApplication_docs.tar

