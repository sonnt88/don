==================== Please Follow the below Steps to Generate Documentation for AppHmi_Sds ==============================

1) Open the folder "/home/bee4kor/samba/views/nincg3/ai_nissan_hmi/products/NINCG3/Apps/AppHmi_Sds/Doc"  in Ubuntu/Linux.
2) "Double click" on the "build_docs.sh" script file.
3) Wait for few moments for Document Generation & the Terminal to close.
4) The Pdf document "SdsHmiApplication.pdf" will be  created.
5) Open "SdsHmiApplication.pdf" in Pdf reader to view documentation :) .

==========================================================================================================================
