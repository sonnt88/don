/*
 * Id:        AppHmi_SdsStateMachineSEMLibB.cpp
 *
 * Function:  Contains all API functions.
 *
 * Generated: Tue May 10 10:42:08 2016
 *
 * Coder 7, 3, 2, 2426
 * 
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include "AppHmi_SdsStateMachineSEMLibB.h"


/*
 * Conditional Compilation Definitions for the API only.
 */
#define SEM_RDHW_TYPE_1                    0
#define SEM_RDHW_TYPE_2                    1
#define SEM_RDHW_TYPE_3                    0
#define SEM_RDHW_WIDTH_16_BIT              0
#define SEM_RDHW_WIDTH_24_BIT              1
#define SEM_RDHW_WIDTH_32_BIT              0
#define SEM_RDHW_WIDTH_48_BIT              0
#define SEM_RDHW_WIDTH_64_BIT              0
#define SEM_RD_WIDTH_8_BIT                 1
#define SEM_RD_WIDTH_16_BIT                0
#define SEM_RD_WIDTH_32_BIT                0
#define SEM_RDFM_NUMBER                    1
#define SEM_EVENT_GROUP_INDEX              0
#define SEM_EVENT_GROUP_TABLE_INDEX        0
#define SEM_SIGNAL_QUEUE_ERROR_IF_FULL     1
#define SEM_SIGNAL_QUEUE_NO_ERROR_IF_FULL  0
#define SEM_RMN_ACTIONS                    11
#define SEM_RMN_GUARDS                     1
#define SEM_RMN_NEGATIVE_STATE_SYNCS       0
#define SEM_RMN_NEXT_STATES                8
#define SEM_RMN_POSITIVE_STATE_SYNCS       1
#define SEM_RMN_SIGNALS                    0
#define SEM_SIGNAL                         0
#define SEM_EXPL                           0
#define SEM_EXPL_ABS                       0
#define SEM_FORCE_STATE                    0
#define SEM_GET_OUTPUT_ALL                 0
#define SEM_INIT_ALL                       1
#define SEM_MACHINE                        0
#define SEM_NAME                           0
#define SEM_NAME_ABS                       1
#define SEM_NEXT_STATE_CHG                 1
#define SEM_SIGNAL_QUEUE_INFO              0
#define SEM_STATE                          1
#define SEM_STATE_ALL                      1
#define SEM_INIT_EXTERNAL_VARIABLES        0
#define SEM_INIT_INTERNAL_VARIABLES        0
#define VS_ACTION_EXPLS                    0
#define VS_ACTION_FUNCTION_NAMES           1
#define VS_EVENT_EXPLS                     0
#define VS_EVENT_NAMES                     1
#define VS_STATE_EXPLS                     0
#define VS_STATE_NAMES                     1


#ifdef VS_RUNTIME_INFO
AppHmi_SdsStateMachineVSRunTimeInfo volatile const VS_RUNTIME_INFO_EXTKW AppHmi_SdsStateMachinevsRunTimeInfo = 
{
  VS_SIGNATURE_VERSION,
  VS_SIGNATURE_CONTENT
};
#endif


const SEM_STATE_TYPE AppHmi_SdsStateMachine::StateUndefined = ((SEM_STATE_TYPE)-1L);

const SEM_EVENT_TYPE AppHmi_SdsStateMachine::EventUndefined = ((SEM_EVENT_TYPE)-1L);

const SEM_EVENT_TYPE AppHmi_SdsStateMachine::EventGroupUndefined = 0xFFu;

const SEM_EVENT_TYPE AppHmi_SdsStateMachine::EventTerminationId = ((SEM_EVENT_TYPE)-1L);

const SEM_ACTION_EXPRESSION_TYPE AppHmi_SdsStateMachine::ActionExpressionTerminationId = ((SEM_ACTION_EXPRESSION_TYPE)-1L);


#if (SEM_SIGNAL)
void AppHmi_SdsStateMachine::SEM_InitSignalQueue (void)
{
  SEM.SPut = 0u;
  SEM.SGet = 0u;
  SEM.SUsed = 0u;
}


unsigned char AppHmi_SdsStateMachine::SEM_SignalQueuePut (SEM_EVENT_TYPE SignalNo)
{
  unsigned char result;

  if (SEM.SUsed == 1u)
  {
    result = SES_SIGNAL_QUEUE_FULL;
  }
  else
  {
    SEM.SUsed++;
    SEM.SQueue[SEM.SPut] = SignalNo;
    if (++SEM.SPut == 1u)
    {
      SEM.SPut = 0u;
    }
    result = SES_OKAY;
  }
  return (result);
}


SEM_EVENT_TYPE AppHmi_SdsStateMachine::SEM_SignalQueueGet (void)
{
  SEM_EVENT_TYPE SignalNo = EventUndefined;

  if (SEM.SUsed)
  {
    SEM.SUsed--;
    SignalNo = SEM.SQueue[SEM.SGet];
    if (++SEM.SGet == 1u)
    {
      SEM.SGet = 0u;
    }
  }
  return (SignalNo);
}


#if (SEM_SIGNAL_QUEUE_INFO == 1)
void AppHmi_SdsStateMachine::SEM_SignalQueueInfo (SEM_SIGNAL_QUEUE_TYPE *NofSignals)
{
  *NofSignals = SEM.SUsed;
}
#endif
#endif


#if (SEM_INIT_ALL)
void AppHmi_SdsStateMachine::SEM_InitAll (void)
{
  SEM_Init();
#if (SEM_INIT_EXTERNAL_VARIABLES)
  SEM_InitExternalVariables();
#endif
#if (SEM_INIT_INTERNAL_VARIABLES)
  SEM_InitInternalVariables();
#endif
#if (SEM_SIGNAL)
  SEM_InitSignalQueue();
#endif
}
#endif


void AppHmi_SdsStateMachine::SEM_Init (void)
{
#ifdef VS_RUNTIME_INFO
  *AppHmi_SdsStateMachinevsRunTimeInfo.pSignatureVersion;
#endif
#if (VS_NOF_STATE_MACHINES != 0u)
  {
    SEM_STATE_MACHINE_TYPE i;
    for (i = 0u; i < VS_NOF_STATE_MACHINES; i++)
    {
      SEM.WSV[i] = StateUndefined;
      SEM.CSV[i] = StateUndefined;
    }
  }
#if (SEM_NEXT_STATE_CHG == 1)
  SEM.Chg = 0;
#endif
#endif
  SEM.State = STATE_SEM_INITIALIZED;
}


void AppHmi_SdsStateMachine::DeductChangeState (VS_VOID)
{
  SEM_STATE_MACHINE_TYPE i;
  for (i = 0u; i < VS_NOF_STATE_MACHINES; i++)
  {
    if (SEM.WSV[i] != StateUndefined)
    {
#if (SEM_NEXT_STATE_CHG == 1)
      if (SEM.CSV[i] != SEM.WSV[i])
      {
        SEM.Chg = 1;
      }
#endif
      SEM.CSV[i] = SEM.WSV[i];
      SEM.WSV[i] = StateUndefined;
    }
  }
}


unsigned char AppHmi_SdsStateMachine::SEM_GetOutput (SEM_ACTION_EXPRESSION_TYPE *ActionNo)
{
  for(;;)
  {
    switch (SEM.State)
    {
    case STATE_SEM_PREPARE:
      SEM.iFirstR = VS.AppHmi_SdsStateMachineRuleTableIndex[SEM.EventNo];
      SEM.iLastR = VS.AppHmi_SdsStateMachineRuleTableIndex[SEM.EventNo + 1];
      SEM.State = STATE_SEM_CONSULT;
      /* fall through */

    case STATE_SEM_CONSULT:
      while (SEM.iFirstR < SEM.iLastR)
      {
        SEM_INTERNAL_TYPE i;
        VS_UINT8 nNo;
        VS_UINT8 nPos;
        VS_UINT8 nNxt;
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        VS_UINT8 nNeg;
#endif
#if (SEM_RMN_SIGNALS)
        VS_UINT8 nSignal;
#endif
#if (SEM_RMN_GUARDS)
        VS_UINT8 nGuard;
#endif
        SEM_RULE_INDEX_TYPE iRI;

        iRI = VS.AppHmi_SdsStateMachineRuleIndex[SEM.iFirstR++];
#if (SEM_RD_WIDTH_8_BIT && SEM_RDHW_TYPE_1 && SEM_RDHW_WIDTH_16_BIT)
        i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
        nNxt = (unsigned char)(i & 0x0Fu);
        SEM.nAction = (unsigned char)(i >> 4u);
        i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
        nPos = (unsigned char)(i & 0x0Fu);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)(i >> 4u);
#endif
#endif
#if (SEM_RD_WIDTH_8_BIT && SEM_RDHW_TYPE_2 && SEM_RDHW_WIDTH_24_BIT)
        i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
        nPos = (unsigned char)(i & 0x0Fu);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)(i >> 4u);
#endif
        i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
#if (SEM_RMN_GUARDS)
        nGuard = (unsigned char)(i & 0x0Fu);
#endif
        nNxt = (unsigned char)(i >> 4u);
        i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
        SEM.nAction = (unsigned char)(i & 0x0Fu);
#if (SEM_RMN_SIGNALS)
        nSignal = (unsigned char)(i >> 4u);
#endif
#endif
#if (SEM_RD_WIDTH_8_BIT && SEM_RDHW_TYPE_1 && SEM_RDHW_WIDTH_32_BIT)
        SEM.nAction = (unsigned char)VS.AppHmi_SdsStateMachineRuleData[iRI++];
        nNxt = (unsigned char)VS.AppHmi_SdsStateMachineRuleData[iRI++];
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)VS.AppHmi_SdsStateMachineRuleData[iRI++];
#else
        iRI++;
#endif
        nPos = (unsigned char)VS.AppHmi_SdsStateMachineRuleData[iRI++];
#endif
#if (SEM_RD_WIDTH_8_BIT && SEM_RDHW_TYPE_2 && SEM_RDHW_WIDTH_48_BIT)
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)VS.AppHmi_SdsStateMachineRuleData[iRI++];
#else
        iRI++;
#endif
        nPos = (unsigned char)VS.AppHmi_SdsStateMachineRuleData[iRI++];
        nNxt = (unsigned char)VS.AppHmi_SdsStateMachineRuleData[iRI++];
#if (SEM_RMN_GUARDS)
        nGuard = (unsigned char)VS.AppHmi_SdsStateMachineRuleData[iRI];
#endif
        iRI++;
#if (SEM_RMN_SIGNALS)
        nSignal = (unsigned char)VS.AppHmi_SdsStateMachineRuleData[iRI];
#endif
        iRI++;
        SEM.nAction = (unsigned char)VS.AppHmi_SdsStateMachineRuleData[iRI++];
#endif
#if (SEM_RD_WIDTH_16_BIT && SEM_RDHW_TYPE_1 && SEM_RDHW_WIDTH_16_BIT)
        i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
        nPos = (unsigned char)(i & 0x0Fu);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)((i >> 4u) & 0x0Fu);
#endif
        nNxt = (unsigned char)((i >> 8u) & 0x0Fu);
        SEM.nAction = (unsigned char)((i >> 12u) & 0x0Fu);
#endif
#if (SEM_RD_WIDTH_16_BIT && SEM_RDHW_TYPE_3 && SEM_RDHW_WIDTH_32_BIT)
        i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
        nPos = (unsigned char)(i & 0x0Fu);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)((i >> 4u) & 0x0Fu);
#endif
#if (SEM_RMN_GUARDS)
        nGuard = (unsigned char)((i >> 8u) & 0x0Fu);
#endif
        nNxt = (unsigned char)(i >> 12u);
        i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
        SEM.nAction = (unsigned char)(i & 0x0Fu);
#if (SEM_RMN_SIGNALS)
        nSignal = (unsigned char)((i >> 4u) & 0x0Fu);
#endif
#endif
#if (SEM_RD_WIDTH_16_BIT && SEM_RDHW_TYPE_1 && SEM_RDHW_WIDTH_32_BIT)
        i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
        nNxt = (unsigned char)(i & 0x0FFu);
        SEM.nAction = (unsigned char)(i >> 8u);
        i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
        nPos = (unsigned char)(i & 0x0FFu);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)(i >> 8u);
#endif
#endif
#if (SEM_RD_WIDTH_16_BIT && SEM_RDHW_TYPE_2 && SEM_RDHW_WIDTH_48_BIT)
        i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
        nPos = (unsigned char)(i & 0x0FFu);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)(i >> 8u);
#endif
        i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
#if (SEM_RMN_GUARDS)
        nGuard = (unsigned char)(i & 0x0FFu);
#endif
        nNxt = (unsigned char)(i >> 8u);
        i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
        SEM.nAction = (unsigned char)(i & 0x0FFu);
#if (SEM_RMN_SIGNALS)
        nSignal = (unsigned char)(i >> 8u);
#endif
#endif
#if (SEM_RD_WIDTH_32_BIT && SEM_RDHW_TYPE_1 && SEM_RDHW_WIDTH_32_BIT)
        i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
        nPos = (unsigned char)(i & 0X0FFu);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)((i >> 8u) & 0X0FFu);
#endif
        nNxt = (unsigned char)((i >> 16u) & 0x0FFu);
        SEM.nAction = (unsigned char)((i >> 24u) & 0x0FFu);
#endif
#if (SEM_RD_WIDTH_32_BIT && SEM_RDHW_TYPE_3 && SEM_RDHW_WIDTH_64_BIT)
        i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
        nPos = (unsigned char)(i & 0x0FFu);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        nNeg = (unsigned char)((i >> 8u) & 0x0FFu);
#endif
#if (SEM_RMN_GUARDS)
        nGuard = (unsigned char)((i >> 16u) & 0x0FFu);
#endif
        nNxt = (unsigned char)(i >> 24u);
        i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
        SEM.nAction = (unsigned char)(i & 0x0FFu);
#if (SEM_RMN_SIGNALS)
        nSignal = (unsigned char)((i >> 8u) & 0x0FFu);
#endif
#endif
#if (VS_NOF_STATE_MACHINES != 0u)
        for (nNo = 0 ; nNo < nPos; nNo++)
        {
          SEM_STATE_TYPE sa;
          sa = (SEM_STATE_TYPE) VS.AppHmi_SdsStateMachineRuleData[iRI++];
          if (sa != SEM.CSV[VS.AppHmi_SdsStateMachineStateMachineIndex[sa]])
          {
            goto NextRule;
          }
        }
#if (SEM_RMN_NEGATIVE_STATE_SYNCS)
        for (nNo = 0; nNo < nNeg; nNo++)
        {
          SEM_STATE_TYPE sa;
          i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
          sa = SEM.CSV[VS.AppHmi_SdsStateMachineStateMachineIndex[i]];
          if ((sa == StateUndefined) || (sa == (SEM_STATE_TYPE) i))
          {
            goto NextRule;
          }
        }
#endif
#endif
#if (SEM_RMN_GUARDS)
        if (nGuard)
        {
          for (nNo = 0; nNo < nGuard; nNo++)
          {
            if (VSGuard(VS.AppHmi_SdsStateMachineRuleData[iRI++]) == VS_FALSE)
            {
              goto NextRule;
            }
          }
        }
#endif

#if (VS_NOF_STATE_MACHINES != 0u)
        for (nNo = 0; nNo < nNxt; nNo++)
        {
          SEM_STATE_TYPE sa;
          sa = (SEM_STATE_TYPE) VS.AppHmi_SdsStateMachineRuleData[iRI++];
          i = VS.AppHmi_SdsStateMachineStateMachineIndex[sa];
          if (SEM.WSV[i] == StateUndefined)
          {
            SEM.WSV[i] = sa;
          }
          else if (SEM.WSV[i] != sa)
          {
            SEM._iRI = iRI;
            return (SES_CONTRADICTION);
          }
        }
#endif
#if (SEM_RMN_SIGNALS)
        if (nSignal)
        {
          for (nNo = 0; nNo < nSignal; nNo++)
          {
            i = VS.AppHmi_SdsStateMachineRuleData[iRI++];
#if (SEM_SIGNAL_QUEUE_ERROR_IF_FULL)
            if (SEM_SignalQueuePut((SEM_EVENT_TYPE)i) == SES_SIGNAL_QUEUE_FULL)
            {
              SEM._iRI = iRI;
              return (SES_SIGNAL_QUEUE_FULL);
            }
#endif
#if (SEM_SIGNAL_QUEUE_NO_ERROR_IF_FULL)
            SEM_SignalQueuePut((SEM_EVENT_TYPE)i);
#endif
          }
        }
#endif
        if (SEM.nAction)
        {
          *ActionNo = (SEM_ACTION_EXPRESSION_TYPE)VS.AppHmi_SdsStateMachineRuleData[iRI];
          if (SEM.nAction > 1)
          {
            iRI++;
            SEM._iRI = iRI;
            SEM.nAction--;
            SEM.State = STATE_SEM_OUTPUT;
          }
          return (SES_FOUND);
        }
NextRule:
        ;
      }
      SEM.State = STATE_SEM_OKAY;
      return (SES_OKAY);

    case STATE_SEM_OUTPUT:
      if (SEM.nAction)
      {
        *ActionNo = (SEM_ACTION_EXPRESSION_TYPE) VS.AppHmi_SdsStateMachineRuleData[SEM._iRI++];
        SEM.nAction--;
        return (SES_FOUND);
      }
      SEM.State = STATE_SEM_CONSULT;
      break;

    case STATE_SEM_OKAY:
      return (SES_OKAY);

    default:
      return (SES_EMPTY);
    }
  }
}


#if (SEM_GET_OUTPUT_ALL == 1)
#if (SEM_SIGNAL)
#error SEM_GetOutputAll cannot be used when the Project contains signals.
#endif
unsigned char AppHmi_SdsStateMachine::SEM_GetOutputAll (SEM_ACTION_EXPRESSION_TYPE *ActionVector,
  SEM_ACTION_EXPRESSION_TYPE MaxSize)
{
  SEM_ACTION_EXPRESSION_TYPE i;
  unsigned char CC;

  for (i = 0; ((CC = SEM_GetOutput(&ActionVector[i])) == SES_FOUND) && i < MaxSize - 1; i++);

  if (CC == SES_OKAY)
  {
    ActionVector[i] = ActionExpressionTerminationId;
    return (SES_OKAY);
  }
  if (CC == SES_FOUND)
  {
    return (SES_BUFFER_OVERFLOW);
  }
  return (CC);
}
#endif


#if (SEM_NEXT_STATE_CHG == 1)
unsigned char AppHmi_SdsStateMachine::SEM_NextStateChg (void)
#else
unsigned char AppHmi_SdsStateMachine::SEM_NextState (void)
#endif
{
  unsigned char CC;
  SEM_ACTION_EXPRESSION_TYPE nAction;

  if (SEM.State != STATE_SEM_OKAY)
  {
    while ((CC = SEM_GetOutput (&nAction)) == SES_FOUND)
    {
      ;
    }
    if (CC != SES_OKAY)
    {
      return(CC);
    }
  }
  DeductChangeState();
  SEM.State = STATE_SEM_INITIALIZED;

#if (SEM_NEXT_STATE_CHG == 1)
  if (SEM.Chg)
  {
    SEM.Chg = 0;
    return (SES_FOUND);
  }
  return (SES_OKAY);
#else
  return (SES_OKAY);
#endif
}


#if (SEM_NAME == 1)
unsigned char AppHmi_SdsStateMachine::SEM_Name (unsigned char IdentType,
  SEM_EXPLANATION_TYPE IdentNo, char *Text, unsigned short MaxSize) const
{
  char c, *s;
  unsigned short i;

  if (!MaxSize)
  {
    return (SES_TEXT_TOO_LONG);
  }
  switch (IdentType)
  {
#if (VS_EVENT_NAMES != 0)
  case EVENT_TYPE:
    if (VS_NOF_EVENTS <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    s = (char *)VS.AppHmi_SdsStateMachineEventNames + VS.AppHmi_SdsStateMachineEventNamesIndex[IdentNo];
    break;
#endif
#if ((VS_STATE_NAMES != 0) && (VS_NOF_STATES != 0u))
  case STATE_TYPE:
    if (VS_NOF_STATES <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    s = (char *)VS.AppHmi_SdsStateMachineStateNames + VS.AppHmi_SdsStateMachineStateNamesIndex[IdentNo];
    break;
#endif
#if ((VS_ACTION_FUNCTION_NAMES != 0) && (VS_NOF_ACTION_FUNCTIONS != 0u))
  case ACTION_TYPE:
    if (VS_NOF_ACTION_FUNCTIONS <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    s = (char *)VS.AppHmi_SdsStateMachineActionNames + VS.AppHmi_SdsStateMachineActionNamesIndex[IdentNo];
    break;
#endif
  default:
    return (SES_TYPE_ERR);
  }
  for (i = 0; i < MaxSize; i++)
  {
    c = *s++;
    *Text++ = c;
    if (c == '\0')
    {
      return (SES_OKAY);
    }
  }
  Text--;
  *Text = '\0';
  return (SES_TEXT_TOO_LONG);
}
#endif


#if (SEM_NAME_ABS == 1)
unsigned char AppHmi_SdsStateMachine::SEM_NameAbs (unsigned char IdentType,
  SEM_EXPLANATION_TYPE IdentNo, char const **Text) const
{
  switch (IdentType)
  {
#if (VS_EVENT_NAMES != 0)
  case EVENT_TYPE:
    if (VS_NOF_EVENTS <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    *Text = (char const *)&VS.AppHmi_SdsStateMachineEventNames + VS.AppHmi_SdsStateMachineEventNamesIndex[IdentNo];
    break;
#endif
#if ((VS_STATE_NAMES != 0) && (VS_NOF_STATES != 0u))
  case STATE_TYPE:
    if (VS_NOF_STATES <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    *Text = (char const *)&VS.AppHmi_SdsStateMachineStateNames + VS.AppHmi_SdsStateMachineStateNamesIndex[IdentNo];
    break;
#endif
#if ((VS_ACTION_FUNCTION_NAMES != 0) && (VS_NOF_ACTION_FUNCTIONS != 0u))
  case ACTION_TYPE:
    if (VS_NOF_ACTION_FUNCTIONS <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    *Text = (char const *)&VS.AppHmi_SdsStateMachineActionNames + VS.AppHmi_SdsStateMachineActionNamesIndex[IdentNo];
    break;
#endif
  default:
    return (SES_TYPE_ERR);
  }
  return (SES_OKAY);
}
#endif


#if (SEM_EXPL == 1)
unsigned char AppHmi_SdsStateMachine::SEM_Expl (unsigned char IdentType, SEM_EXPLANATION_TYPE IdentNo,
  char *Text, unsigned short MaxSize) const
{
  char c, *s;
  unsigned short i;

  if (!MaxSize)
  {
    return (SES_TEXT_TOO_LONG);
  }
  switch (IdentType)
  {
#if (VS_EVENT_EXPLS != 0)
  case EVENT_TYPE:
    if (VS_NOF_EVENTS <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    s = (char *)VS.AppHmi_SdsStateMachineEventExpls + VS.AppHmi_SdsStateMachineEventExplsIndex[IdentNo];
    break;
#endif
#if ((VS_STATE_EXPLS != 0) && (VS_NOF_STATES != 0u))
  case STATE_TYPE:
    if (VS_NOF_STATES <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    s = (char *)VS.AppHmi_SdsStateMachineStateExpls + VS.AppHmi_SdsStateMachineStateExplsIndex[IdentNo];
    break;
#endif
#if ((VS_ACTION_EXPLS != 0) && (VS_NOF_ACTION_FUNCTIONS != 0u))
  case ACTION_TYPE:
    if (VS_NOF_ACTION_FUNCTIONS <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    s = (char *)VS.AppHmi_SdsStateMachineActionExpls + VS.AppHmi_SdsStateMachineActionExplsIndex[IdentNo];
    break;
#endif
  default:
    return (SES_TYPE_ERR);
  }

  for (i = 0; i < MaxSize; i++)
  {
    c = *s++;
    *Text++ = c;
    if (c == '\0')
    {
      return (SES_OKAY);
    }
  }
  Text--;
  *Text = '\0';
  return (SES_TEXT_TOO_LONG);
}
#endif


#if (SEM_EXPL_ABS == 1)
unsigned char AppHmi_SdsStateMachine::SEM_ExplAbs (unsigned char IdentType,
  SEM_EXPLANATION_TYPE IdentNo, char const **Text) const
{
  switch (IdentType)
  {
#if (VS_EVENT_EXPLS != 0)
  case EVENT_TYPE:
    if (VS_NOF_EVENTS <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    *Text = (char const *)&VS.AppHmi_SdsStateMachineEventExpls + VS.AppHmi_SdsStateMachineEventExplsIndex[IdentNo];
    break;
#endif
#if ((VS_STATE_EXPLS != 0) && (VS_NOF_STATES != 0u))
  case STATE_TYPE:
    if (VS_NOF_STATES <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    *Text = (char const *)&VS.AppHmi_SdsStateMachineStateExpls + VS.AppHmi_SdsStateMachineStateExplsIndex[IdentNo];
    break;
#endif
#if ((VS_ACTION_EXPLS != 0) && (VS_NOF_ACTION_FUNCTIONS != 0u))
  case ACTION_TYPE:
    if (VS_NOF_ACTION_FUNCTIONS <= IdentNo)
    {
      return (SES_RANGE_ERR);
    }
    *Text = (char const *)&VS.AppHmi_SdsStateMachineActionExpls + VS.AppHmi_SdsStateMachineActionExplsIndex[IdentNo];
    break;
#endif
  default:
    return (SES_TYPE_ERR);
  }
  return (SES_OKAY);
}
#endif


#if (SEM_STATE == 1)
unsigned char AppHmi_SdsStateMachine::SEM_State (SEM_STATE_MACHINE_TYPE StateMachineNo,
  SEM_STATE_TYPE *StateNo) const
{
  if (VS_NOF_STATE_MACHINES <= StateMachineNo)
  {
    return (SES_RANGE_ERR);
  }
#if (VS_NOF_STATE_MACHINES != 0u)
  *StateNo = SEM.CSV[StateMachineNo];
#else
  *StateNo = 0;
#endif
  return (SES_FOUND);
}
#endif


#if (SEM_STATE_ALL == 1)
unsigned char AppHmi_SdsStateMachine::SEM_StateAll (SEM_STATE_TYPE *StateVector,
  SEM_STATE_MACHINE_TYPE MaxSize) const
{
  SEM_STATE_MACHINE_TYPE i;

  if (VS_NOF_STATE_MACHINES > MaxSize)
  {
    return (SES_BUFFER_OVERFLOW);
  }

#if (VS_NOF_STATE_MACHINES != 0u)
  for (i = 0u; i < VS_NOF_STATE_MACHINES; i++)
  {
    StateVector[i] = SEM.CSV[i];
  }
#else
  for (i = 0u; i < VS_NOF_STATE_MACHINES; i++)
  {
    StateVector[i] = 0;
  }
#endif
  return (SES_FOUND);
}
#endif


#if (SEM_MACHINE == 1)
unsigned char AppHmi_SdsStateMachine::SEM_Machine (SEM_STATE_TYPE StateNo,
  SEM_STATE_MACHINE_TYPE *StateMachineNo) const
{
  if (VS_NOF_STATES <= StateNo)
  {
    return (SES_RANGE_ERR);
  }
#if (VS_NOF_STATE_MACHINES != 0u)
  *StateMachineNo = VS.AppHmi_SdsStateMachineStateMachineIndex[StateNo];
#else
  *StateMachineNo = 0;
#endif
  return (SES_FOUND);
}
#endif


#if (SEM_FORCE_STATE == 1)
unsigned char AppHmi_SdsStateMachine::SEM_ForceState (SEM_STATE_TYPE StateNo)
{
  if (VS_NOF_STATES <= StateNo)
  {
    return (SES_RANGE_ERR);
  }
#if (VS_NOF_STATE_MACHINES != 0u)
  SEM.CSV[VS.AppHmi_SdsStateMachineStateMachineIndex[StateNo]] = StateNo;
#endif
  return (SES_OKAY);
}
#endif
