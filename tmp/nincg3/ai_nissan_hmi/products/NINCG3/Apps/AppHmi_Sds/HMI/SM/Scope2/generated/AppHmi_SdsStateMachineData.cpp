/*
 * Id:        AppHmi_SdsStateMachineData.cpp
 *
 * Function:  VS System Data Source File.
 *
 * Generated: Tue May 10 10:42:08 2016
 *
 * Coder 7, 3, 2, 2426
 * 
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include "AppHmi_SdsStateMachineSEMLibB.h"


#include "AppHmi_SdsStateMachineData.h"


#include <stdarg.h>


/*
 * SEM Deduct Function.
 */
unsigned char AppHmi_SdsStateMachine::SEM_Deduct (SEM_EVENT_TYPE EventNo, ...)
{
  va_list ap;

  va_start(ap, EventNo);
  if (SEM.State == 0x00u /* STATE_SEM_NOT_INITIALIZED */)
  {
    return SES_NOT_INITIALIZED;
  }
  if (VS_NOF_EVENTS <= EventNo)
  {
    return (SES_RANGE_ERR);
  }
  switch (EventNo)
  {
  case 10:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB10.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB10.VS_UINT32Var[2] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 11:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 12:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 16:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 17:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 18:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 19:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 20:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 21:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 22:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 23:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 24:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 25:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 26:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 27:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 28:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 29:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 30:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 31:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 32:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 33:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 34:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 35:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 36:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 37:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 38:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 39:
    EventArgsVar.DB64.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 40:
    EventArgsVar.DB64.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 41:
    EventArgsVar.DB41.VS_BOOLVar[0] = (VS_BOOL) va_arg(ap, VS_INT);
    break;

  case 42:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 43:
    EventArgsVar.DB10.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 44:
    EventArgsVar.DB45.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 45:
    EventArgsVar.DB45.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 46:
    EventArgsVar.DB45.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 47:
    EventArgsVar.DB45.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 48:
    EventArgsVar.DB64.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 57:
    EventArgsVar.DB57.VS_UINT8Var[0] = (VS_UINT8) va_arg(ap, VS_INT);
    EventArgsVar.DB57.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB57.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB57.VS_UINT32Var[2] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 58:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB58.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 59:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB58.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 60:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 61:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 62:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB58.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 63:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB58.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 64:
    EventArgsVar.DB64.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    EventArgsVar.DB64.VS_INT8Var[1] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 65:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 66:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 67:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 68:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 69:
    EventArgsVar.DB64.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 70:
    EventArgsVar.DB64.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 71:
    EventArgsVar.DB64.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 72:
    EventArgsVar.DB64.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 73:
    EventArgsVar.DB64.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 74:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 75:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 76:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 77:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 78:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 79:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 80:
    EventArgsVar.DB58.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 81:
    EventArgsVar.DB81.VS_INT32Var[0] = (VS_INT32) va_arg(ap, VS_INT32_VAARG);
    break;

  case 82:
    EventArgsVar.DB81.VS_INT32Var[0] = (VS_INT32) va_arg(ap, VS_INT32_VAARG);
    break;

  default:
    break;
  }
  SEM.EventNo = EventNo;
  SEM.DIt = 2;
  SEM.State = 0x02u; /* STATE_SEM_PREPARE */

  va_end(ap);
  return (SES_OKAY);
}


/*
 * Guard Expression Functions.
 */
VS_BOOL AppHmi_SdsStateMachine::VSGuard (SEM_GUARD_EXPRESSION_TYPE i)
{
  switch (i)
  {
  case 0:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_LAYOUT_N);
  case 1:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_LAYOUT_R);
  case 2:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_LAYOUT_C);
  case 3:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_LAYOUT_L);
  case 4:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_SDS_Settings_Main);
  case 5:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_SDS_Help_1List);
  case 6:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_SDS_Help_3List);
  case 7:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_SDS_Settings_BestMatch);
  case 8:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_LAYOUT_M);
  case 9:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_LAYOUT_Q);
  case 10:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_LAYOUT_MAP);
  case 11:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_LAYOUT_MAIL);
  case 12:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_LAYOUT_S);
  case 13:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_LAYOUT_E);
  case 14:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_LAYOUT_T);
  case 15:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_LAYOUT_O);
  case 16:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_SDS_Test_Mode);
  case 17:
    return (VS_BOOL)(EventArgsVar.DB10.VS_UINT32Var[0] == SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
  case 18:
    return (VS_BOOL)(EventArgsVar.DB10.VS_UINT32Var[0] == SdsPopups_Popups_SDS_VR_ICPOP_AUDIO_UNAVAILABLE);
  case 19:
    return (VS_BOOL)(EventArgsVar.DB10.VS_UINT32Var[0] == SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
  case 20:
    return (VS_BOOL)(EventArgsVar.DB10.VS_UINT32Var[0] == SdsPopups_Popups_SDS_SESSION_UNAVAILABILITY);
  case 21:
    return (VS_BOOL)(EventArgsVar.DB10.VS_UINT32Var[0] == SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO);
  case 22:
    return (VS_BOOL)(EventArgsVar.DB10.VS_UINT32Var[0] == SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK);
  case 23:
    return (VS_BOOL)(EventArgsVar.DB45.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP);
  case 24:
    return (VS_BOOL)(EventArgsVar.DB64.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP);
  case 25:
    return (VS_BOOL)(EventArgsVar.DB10.VS_UINT32Var[0] == SdsPopups_Popups_SDS_VR_MPOP_EARLY_START);
  case 26:
    return (VS_BOOL)(EventArgsVar.DB10.VS_UINT32Var[0] == LIST_ID_SDS_Settings_Main && EventArgsVar.DB10.VS_UINT32Var[1] == Enum_LIST_ITEM_BEST_MATCH_LIST);
  case 27:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB58.VS_UINT32Var[1] == SdsPopups_Popups_SDS_VR_MPOP_EARLY_START);
  case 28:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB58.VS_UINT32Var[1] == SdsPopups_Popups_SDS_VR_ICPOP_AUDIO_UNAVAILABLE);
  case 29:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB58.VS_UINT32Var[1] == SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
  case 30:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB58.VS_UINT32Var[1] == SdsPopups_Popups_SDS_SESSION_UNAVAILABILITY);
  case 31:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB58.VS_UINT32Var[1] == SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO);
  case 32:
    return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB58.VS_UINT32Var[1] == SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK);
  }
  return (VS_BOOL)(EventArgsVar.DB58.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB58.VS_UINT32Var[1] == SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
}


/*
 * Action Expressions Wrapper Function.
 */
VS_VOID AppHmi_SdsStateMachine::VSAction (SEM_ACTION_EXPRESSION_TYPE i)
{
  switch (i)
  {
  case 0:
    Notify_Init_Finished();
    break;
  case 1:
    acPerform_BtnHelpRelease();
    break;
  case 2:
    acPerform_BtnSettingsRelease();
    break;
  case 4:
    acPerform_SkBackPressMsg();
    break;
  case 18:
    gacHideStatusLineReq(Enum_HEADER_TYPE_OTHER);
    break;
  case 19:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_R);
    break;
  case 20:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_R);
    break;
  case 21:
    gacViewDestroyReq(SdsPopups_Popups_SDS_VR_MPOP_EARLY_START);
    break;
  case 22:
    gacViewDestroyReq(SdsPopups_Popups_SDS_VR_ICPOP_AUDIO_UNAVAILABLE);
    break;
  case 23:
    gacViewDestroyReq(SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
    break;
  case 24:
    gacViewDestroyReq(SdsPopups_Popups_SDS_SESSION_UNAVAILABILITY);
    break;
  case 25:
    gacViewDestroyReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO);
    break;
  case 26:
    gacViewDestroyReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK);
    break;
  case 27:
    gacViewDestroyReq(SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
    break;
  case 28:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_N);
    break;
  case 29:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_N);
    break;
  case 30:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_C);
    break;
  case 31:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_C);
    break;
  case 32:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_R);
    break;
  case 33:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_R);
    break;
  case 34:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_L);
    break;
  case 35:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_L);
    break;
  case 36:
    gacViewHideReq(Sds_Scenes_SDS_Settings_Main);
    break;
  case 37:
    gacViewDestroyReq(Sds_Scenes_SDS_Settings_Main);
    break;
  case 38:
    gacViewHideReq(Sds_Scenes_SDS_Help_1List);
    break;
  case 39:
    gacViewDestroyReq(Sds_Scenes_SDS_Help_1List);
    break;
  case 40:
    gacViewHideReq(Sds_Scenes_SDS_Help_3List);
    break;
  case 41:
    gacViewDestroyReq(Sds_Scenes_SDS_Help_3List);
    break;
  case 42:
    gacViewHideReq(Sds_Scenes_SDS_Settings_BestMatch);
    break;
  case 43:
    gacViewDestroyReq(Sds_Scenes_SDS_Settings_BestMatch);
    break;
  case 44:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_M);
    break;
  case 45:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_M);
    break;
  case 46:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_Q);
    break;
  case 47:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_Q);
    break;
  case 48:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_S);
    break;
  case 49:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_S);
    break;
  case 50:
    gacViewHideReq(Sds_Scenes_SDS_Layout_Map);
    break;
  case 51:
    gacViewDestroyReq(Sds_Scenes_SDS_Layout_Map);
    break;
  case 52:
    gacViewHideReq(Sds_Scenes_SDS_Layout_Mail);
    break;
  case 53:
    gacViewDestroyReq(Sds_Scenes_SDS_Layout_Mail);
    break;
  case 54:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_E);
    break;
  case 55:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_E);
    break;
  case 56:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_T);
    break;
  case 57:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_T);
    break;
  case 58:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_O);
    break;
  case 59:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_O);
    break;
  case 60:
    gacViewHideReq(Sds_Scenes_SDS_TESTMODE);
    break;
  case 61:
    gacViewDestroyReq(Sds_Scenes_SDS_TESTMODE);
    break;
  case 62:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_N);
    break;
  case 63:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_N);
    break;
  case 64:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_C);
    break;
  case 65:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_C);
    break;
  case 66:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_L);
    break;
  case 67:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_L);
    break;
  case 68:
    gacViewCreateReq(Sds_Scenes_SDS_Settings_Main);
    break;
  case 69:
    gacViewShowReq(Sds_Scenes_SDS_Settings_Main);
    break;
  case 70:
    gacViewCreateReq(Sds_Scenes_SDS_Help_1List);
    break;
  case 71:
    gacViewShowReq(Sds_Scenes_SDS_Help_1List);
    break;
  case 72:
    gacViewCreateReq(Sds_Scenes_SDS_Help_3List);
    break;
  case 73:
    gacViewShowReq(Sds_Scenes_SDS_Help_3List);
    break;
  case 74:
    gacViewCreateReq(Sds_Scenes_SDS_Settings_BestMatch);
    break;
  case 75:
    gacViewShowReq(Sds_Scenes_SDS_Settings_BestMatch);
    break;
  case 76:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_M);
    break;
  case 77:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_M);
    break;
  case 78:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_Q);
    break;
  case 79:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_Q);
    break;
  case 80:
    gacViewCreateReq(Sds_Scenes_SDS_Layout_Map);
    break;
  case 81:
    gacViewShowReq(Sds_Scenes_SDS_Layout_Map);
    break;
  case 82:
    gacViewCreateReq(Sds_Scenes_SDS_Layout_Mail);
    break;
  case 83:
    gacViewShowReq(Sds_Scenes_SDS_Layout_Mail);
    break;
  case 84:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_S);
    break;
  case 85:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_S);
    break;
  case 86:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_E);
    break;
  case 87:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_E);
    break;
  case 88:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_T);
    break;
  case 89:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_T);
    break;
  case 90:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_O);
    break;
  case 91:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_O);
    break;
  case 92:
    gacViewCreateReq(Sds_Scenes_SDS_TESTMODE);
    break;
  case 93:
    gacViewShowReq(Sds_Scenes_SDS_TESTMODE);
    break;
  case 94:
    gacPopupCreateAndSBShowReq(SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
    break;
  case 95:
    gacPopupCreateAndSBShowReq(SdsPopups_Popups_SDS_VR_ICPOP_AUDIO_UNAVAILABLE);
    break;
  case 96:
    gacPopupCreateAndSBShowReq(SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
    break;
  case 97:
    gacPopupCreateAndSBShowReq(SdsPopups_Popups_SDS_SESSION_UNAVAILABILITY);
    break;
  case 98:
    gacPopupCreateAndSBShowReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO);
    break;
  case 99:
    gacPopupCreateAndSBShowReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK);
    break;
  case 100:
    acPerform_HkBackPressMsg(EventArgsVar.DB45.VS_INT8Var[0]);
    break;
  case 101:
    acPerform_HkPressMsg(EventArgsVar.DB64.VS_INT8Var[0]);
    break;
  case 102:
    acPerform_HkPressMsg(Enum_HARDKEYCODE_HK_SELECT);
    break;
  case 103:
    acPerform_UpdateContext(Enum_HMI_CONTEXT_BACKGROUND);
    break;
  case 104:
    gacPopupCreateAndSBShowReq(SdsPopups_Popups_SDS_VR_MPOP_EARLY_START);
    break;
  case 105:
    acPerform_UpdateSdsHmiAppMode(Enum_SDSHMI_MODE_BACKGROUND);
    break;
  case 106:
    acPerform_UpdateSdsHmiAppMode(Enum_SDSHMI_MODE_FOREGROUND);
    break;
  case 107:
    acPerform_BtnYesRelease(Enum_LIST_ITEM_AUDIO, EventArgsVar.DB58.VS_UINT32Var[0]);
    break;
  case 108:
    acPerform_BtnNoRelease(Enum_LIST_ITEM_AUDIO, EventArgsVar.DB58.VS_UINT32Var[0]);
    break;
  case 109:
    acPerform_BtnYesRelease(Enum_LIST_ITEM_PHONEBOOK, EventArgsVar.DB58.VS_UINT32Var[0]);
    break;
  case 110:
    acPerform_BtnNoRelease(Enum_LIST_ITEM_PHONEBOOK, EventArgsVar.DB58.VS_UINT32Var[0]);
    break;
  case 111:
    acPerform_SpeechRateUpdate(SPEECH_RATE_INCREMENT);
    break;
  case 112:
    acPerform_SpeechRateUpdate(SPEECH_RATE_DECREMENT);
    break;
  case 113:
    gacViewShowReq(SdsPopups_Popups_SDS_VR_MPOP_EARLY_START);
    break;
  case 114:
    gacViewHideReq(SdsPopups_Popups_SDS_VR_MPOP_EARLY_START);
    break;
  case 115:
    gacViewClearReq(SdsPopups_Popups_SDS_VR_MPOP_EARLY_START);
    break;
  case 116:
    gacPopupSBCloseReq(SdsPopups_Popups_SDS_VR_MPOP_EARLY_START);
    break;
  case 117:
    acPerform_HkPressMsg(EventArgsVar.DB64.VS_INT8Var[0]);
    break;
  case 118:
    gacViewShowReq(SdsPopups_Popups_SDS_VR_ICPOP_AUDIO_UNAVAILABLE);
    break;
  case 119:
    gacViewHideReq(SdsPopups_Popups_SDS_VR_ICPOP_AUDIO_UNAVAILABLE);
    break;
  case 120:
    gacViewClearReq(SdsPopups_Popups_SDS_VR_ICPOP_AUDIO_UNAVAILABLE);
    break;
  case 121:
    gacPopupSBCloseReq(SdsPopups_Popups_SDS_VR_ICPOP_AUDIO_UNAVAILABLE);
    break;
  case 122:
    acPerform_HkPressMsg(EventArgsVar.DB64.VS_INT8Var[0]);
    break;
  case 123:
    gacViewShowReq(SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
    break;
  case 124:
    gacViewHideReq(SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
    break;
  case 125:
    gacViewClearReq(SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
    break;
  case 126:
    gacPopupSBCloseReq(SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
    break;
  case 127:
    acPerform_HkPressMsg(EventArgsVar.DB64.VS_INT8Var[0]);
    break;
  case 128:
    gacViewShowReq(SdsPopups_Popups_SDS_SESSION_UNAVAILABILITY);
    break;
  case 129:
    gacViewHideReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO);
    break;
  case 130:
    gacViewClearReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO);
    break;
  case 131:
    gacPopupSBCloseReq(SdsPopups_Popups_SDS_SESSION_UNAVAILABILITY);
    break;
  case 132:
    acPerform_HkPressMsg(EventArgsVar.DB64.VS_INT8Var[0]);
    break;
  case 133:
    gacViewShowReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO);
    break;
  case 134:
    gacPopupSBCloseReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO);
    break;
  case 135:
    acPerform_HkPressMsg(EventArgsVar.DB64.VS_INT8Var[0]);
    break;
  case 136:
    gacViewShowReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK);
    break;
  case 137:
    gacViewHideReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK);
    break;
  case 138:
    gacViewClearReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK);
    break;
  case 139:
    gacPopupSBCloseReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK);
    break;
  case 140:
    acPerform_HkBackPressMsg(EventArgsVar.DB64.VS_INT8Var[0]);
    break;
  case 141:
    gacViewShowReq(SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
    break;
  case 142:
    gacViewHideReq(SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
    break;
  case 143:
    gacViewClearReq(SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
    break;
  case 144:
    gacPopupSBCloseReq(SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
    break;
  case 145:
    acPerform_HkPressMsg(EventArgsVar.DB64.VS_INT8Var[0]);
    break;

  default:
    break;
  }
}
