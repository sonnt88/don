//  -------------------------------------------------------------------------
//  Copyright (C) 2000 - 2011
//  Fujitsu Semiconductor Embedded Solutions Austria GmbH (FEAT)
//  All rights reserved.
//  -------------------------------------------------------------------------
//  This document contains proprietary information belonging to
//  Fujitsu Semiconductor Embedded Solutions Austria GmbH (FEAT).
//  Passing on and copying of this document, use and communication
//  of its contents is not permitted without prior written authorization.
//  -------------------------------------------------------------------------

#if !defined(AppViewSettings_h)
#define AppViewSettings_h

#include "AppBase/ScreenBrokerClient/IAppViewSettings.h"
#include "AppUtils/Singleton.h"


class AppViewSettings : public IAppViewSettings
{
      //Lazy initialization of application view settings singleton
      HMIBASE_SINGLETON_CLASS(AppViewSettings)
};

#endif // AppViewSettings_h
