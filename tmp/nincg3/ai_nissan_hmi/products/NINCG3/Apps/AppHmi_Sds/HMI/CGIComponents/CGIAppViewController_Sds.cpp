/**
 * receive messages subscribed with 'View' here, when it has not been consumed by any widget
 */

#include "gui_std_if.h"

#include "AppHmi_SdsStateMachine.h"
#include "CGIAppViewController_Sds.h"

//SDS Scenes

const char* CGIAppViewController_SDS_LAYOUT_N::_name = "Sds#Scenes#SDS_LAYOUT_N";
const char* CGIAppViewController_SDSDomain::_name = "Sds#Scenes#SDS__Layout_N";
const char* CGIAppViewController_SDS_LAYOUT_C::_name = "Sds#Scenes#SDS_LAYOUT_C";
const char* CGIAppViewController_SDS_LAYOUT_L::_name = "Sds#Scenes#SDS_LAYOUT_L";
const char* CGIAppViewController_SDS_LAYOUT_R::_name = "Sds#Scenes#SDS_LAYOUT_R";
const char* CGIAppViewController_SDS_Settings_Main::_name = "Sds#Scenes#SDS_Settings_Main";
const char* CGIAppViewController_SDS_Help_1List::_name = "Sds#Scenes#SDS_Help_1List";
const char* CGIAppViewController_SDS_Help_3List::_name = "Sds#Scenes#SDS_Help_3List";
const char* CGIAppViewController_SDS_LAYOUT_Q::_name = "Sds#Scenes#SDS_LAYOUT_Q";
const char* CGIAppViewController_SDS_Settings_BestMatch::_name = "Sds#Scenes#SDS_Settings_BestMatch";
const char* CGIAppViewController_SDS_LAYOUT_S::_name = "Sds#Scenes#SDS_LAYOUT_S";
const char* CGIAppViewController_SDS_LAYOUT_M::_name = "Sds#Scenes#SDS_LAYOUT_M";
const char* CGIAppViewController_SDS_Layout_Map::_name = "Sds#Scenes#SDS_Layout_Map";
const char* CGIAppViewController_SDS_Layout_Mail::_name = "Sds#Scenes#SDS_Layout_Mail";
const char* CGIAppViewController_SDS_LAYOUT_E::_name = "Sds#Scenes#SDS_LAYOUT_E";
const char* CGIAppViewController_SDS_LAYOUT_T::_name = "Sds#Scenes#SDS_LAYOUT_T";
const char* CGIAppViewController_SDS_LAYOUT_O::_name = "Sds#Scenes#SDS_LAYOUT_O";

//popups
const char* CGIAppViewController_SDS__MPOP_LANGUAGE_NOT_SUPPORTED::_name = "SdsPopups#Popups#SDS__MPOP_LANGUAGE_NOT_SUPPORTED";
const char* CGIAppViewController_SDS__ICPOP_BEST_MATCH_PHONEBOOK::_name = "SdsPopups#Popups#SDS__ICPOP_BEST_MATCH_PHONEBOOK";
const char* CGIAppViewController_SDS__ICPOP_BEST_MATCH_AUDIO::_name = "SdsPopups#Popups#SDS__ICPOP_BEST_MATCH_AUDIO";
const char* CGIAppViewController_SDS_SESSION_UNAVAILABILITY::_name = "SdsPopups#Popups#SDS_SESSION_UNAVAILABILITY";
const char* CGIAppViewController_SDS_VR_MPOP_VR_INIT::_name = "SdsPopups#Popups#SDS_VR_MPOP_VR_INIT";
const char* CGIAppViewController_SDS_VR_MPOP_EARLY_START::_name = "SdsPopups#Popups#SDS_VR_MPOP_EARLY_START";
const char* CGIAppViewController_SDS_VR_ICPOP_AUDIO_UNAVAILABLE::_name = "SdsPopups#Popups#SDS_VR_ICPOP_AUDIO_UNAVAILABLE";
