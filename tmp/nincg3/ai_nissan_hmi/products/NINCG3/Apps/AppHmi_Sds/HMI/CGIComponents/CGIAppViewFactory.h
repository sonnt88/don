#if !defined(CGI_APP_VIEW_FACTORY_H)
#define CGI_APP_VIEW_FACTORY_H

#include "CgiExtensions/CGIAppViewFactoryBase.h"
#include "CgiExtensions/SceneMapping.h"

class CGIAppViewFactory : public CGIAppViewFactoryBase
{
   public:
      CGIAppViewFactory();

      virtual Courier::View* Create(const Courier::Char* viewId);
      virtual void Destroy(Courier::View* view);
};


class CGIAppViewControllerFactory : public Courier::ViewControllerFactory
{
   public:
      virtual Courier::ViewController* Create(const Courier::Char* viewId);
      virtual void Destroy(Courier::ViewController* viewController);

};

#endif //CGI_APP_VIEW_FACTORY_H
