#ifndef _visualSTATE_APPHMI_SDSSTATEMACHINESEMBDEF_H
#define _visualSTATE_APPHMI_SDSSTATEMACHINESEMBDEF_H

/*
 * Id:        AppHmi_SdsStateMachineSEMBDef.h
 *
 * Function:  SEM Defines Header File.
 *
 * Generated: Tue May 10 10:42:08 2016
 *
 * Coder 7, 3, 2, 2426
 * 
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include "AppHmi_SdsStateMachineSEMTypes.h"


/*
 * Number of Identifiers.
 */
#define VS_NOF_ACTION_EXPRESSIONS        0X092u  /* 146 */
#define VS_NOF_ACTION_FUNCTIONS          0X012u  /*  18 */
#define VS_NOF_EVENT_GROUPS              0X000u  /*   0 */
#define VS_NOF_EVENTS                    0X054u  /*  84 */
#define VS_NOF_EXTERNAL_VARIABLES        0X000u  /*   0 */
#define VS_NOF_GUARD_EXPRESSIONS         0X022u  /*  34 */
#define VS_NOF_INSTANCES                 0X001u  /*   1 */
#define VS_NOF_INTERNAL_VARIABLES        0X000u  /*   0 */
#define VS_NOF_SIGNALS                   0X000u  /*   0 */
#define VS_NOF_STATE_MACHINES            0X008u  /*   8 */
#define VS_NOF_STATES                    0X026u  /*  38 */


/*
 * Functional expression type definitions
 */
typedef VS_BOOL (* VS_GUARDEXPR_TYPE) (VS_VOID);
typedef VS_VOID (* VS_ACTIONEXPR_TYPE) (VS_VOID);


#endif /* ifndef _visualSTATE_APPHMI_SDSSTATEMACHINESEMBDEF_H */
