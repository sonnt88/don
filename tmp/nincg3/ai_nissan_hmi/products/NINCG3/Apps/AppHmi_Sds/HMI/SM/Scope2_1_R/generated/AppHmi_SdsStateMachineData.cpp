/*
 * Id:        AppHmi_SdsStateMachineData.cpp
 *
 * Function:  VS System Data Source File.
 *
 * Generated: Tue May 10 10:44:01 2016
 *
 * Coder 7, 3, 2, 2426
 * 
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include "AppHmi_SdsStateMachineSEMLibB.h"


#include "AppHmi_SdsStateMachineData.h"


#include <stdarg.h>


/*
 * SEM Deduct Function.
 */
unsigned char AppHmi_SdsStateMachine::SEM_Deduct (SEM_EVENT_TYPE EventNo, ...)
{
  va_list ap;

  va_start(ap, EventNo);
  if (SEM.State == 0x00u /* STATE_SEM_NOT_INITIALIZED */)
  {
    return SES_NOT_INITIALIZED;
  }
  if (VS_NOF_EVENTS <= EventNo)
  {
    return (SES_RANGE_ERR);
  }
  switch (EventNo)
  {
  case 10:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB50.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB50.VS_UINT32Var[2] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 11:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 12:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 15:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 16:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 17:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 18:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 19:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 20:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 21:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 22:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 23:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 24:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 25:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 26:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 27:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 28:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 29:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 30:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 31:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 32:
    EventArgsVar.DB57.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 33:
    EventArgsVar.DB57.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 34:
    EventArgsVar.DB34.VS_BOOLVar[0] = (VS_BOOL) va_arg(ap, VS_INT);
    break;

  case 35:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 36:
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 37:
    EventArgsVar.DB61.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 38:
    EventArgsVar.DB61.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 39:
    EventArgsVar.DB61.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 40:
    EventArgsVar.DB61.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 41:
    EventArgsVar.DB61.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 50:
    EventArgsVar.DB50.VS_UINT8Var[0] = (VS_UINT8) va_arg(ap, VS_INT);
    EventArgsVar.DB50.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB50.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB50.VS_UINT32Var[2] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 51:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB51.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 52:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB51.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 53:
    EventArgsVar.DB56.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 54:
    EventArgsVar.DB56.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 55:
    EventArgsVar.DB56.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB56.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 56:
    EventArgsVar.DB56.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    EventArgsVar.DB56.VS_UINT32Var[1] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 57:
    EventArgsVar.DB57.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    EventArgsVar.DB57.VS_INT8Var[1] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 58:
    EventArgsVar.DB57.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 59:
    EventArgsVar.DB57.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 60:
    EventArgsVar.DB57.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 61:
    EventArgsVar.DB61.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 62:
    EventArgsVar.DB57.VS_INT8Var[0] = (VS_INT8) va_arg(ap, VS_INT);
    break;

  case 63:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 64:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 65:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 66:
    EventArgsVar.DB66.VS_INT32Var[0] = (VS_INT32) va_arg(ap, VS_INT32_VAARG);
    break;

  case 67:
    EventArgsVar.DB66.VS_INT32Var[0] = (VS_INT32) va_arg(ap, VS_INT32_VAARG);
    break;

  case 68:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 69:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 70:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 71:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 72:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 73:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 76:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 77:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 78:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 79:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 80:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 81:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 82:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  case 83:
    EventArgsVar.DB51.VS_UINT32Var[0] = (VS_UINT32) va_arg(ap, VS_UINT32_VAARG);
    break;

  default:
    break;
  }
  SEM.EventNo = EventNo;
  SEM.DIt = 2;
  SEM.State = 0x02u; /* STATE_SEM_PREPARE */

  va_end(ap);
  return (SES_OKAY);
}


/*
 * Guard Expression Functions.
 */
VS_BOOL AppHmi_SdsStateMachine::VSGuard (SEM_GUARD_EXPRESSION_TYPE i)
{
  switch (i)
  {
  case 0:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_LAYOUT_N);
  case 1:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_LAYOUT_R);
  case 2:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_LAYOUT_C);
  case 3:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_LAYOUT_L);
  case 4:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_SDS_Settings_Main);
  case 5:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_SDS_Help_1List);
  case 6:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_SDS_Help_3List);
  case 7:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_SDS_Settings_BestMatch);
  case 8:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_LAYOUT_M);
  case 9:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_LAYOUT_Q);
  case 10:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_LAYOUT_MAP);
  case 11:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_LAYOUT_MAIL);
  case 12:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_LAYOUT_S);
  case 13:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_LAYOUT_E);
  case 14:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_LAYOUT_T);
  case 15:
    return (VS_BOOL)(EventArgsVar.DB50.VS_UINT32Var[0] == SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
  case 16:
    return (VS_BOOL)(EventArgsVar.DB50.VS_UINT32Var[0] == SdsPopups_Popups_SDS_VR_ICPOP_AUDIO_UNAVAILABLE);
  case 17:
    return (VS_BOOL)(EventArgsVar.DB50.VS_UINT32Var[0] == SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
  case 18:
    return (VS_BOOL)(EventArgsVar.DB50.VS_UINT32Var[0] == SdsPopups_Popups_SDS_SESSION_UNAVAILABILITY);
  case 19:
    return (VS_BOOL)(EventArgsVar.DB50.VS_UINT32Var[0] == SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO);
  case 20:
    return (VS_BOOL)(EventArgsVar.DB50.VS_UINT32Var[0] == SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK);
  case 21:
    return (VS_BOOL)(EventArgsVar.DB61.VS_INT8Var[0] == Enum_hmibase_HARDKEYSTATE_UP);
  case 22:
    return (VS_BOOL)(EventArgsVar.DB50.VS_UINT32Var[0] == LIST_ID_SDS_Settings_Main && EventArgsVar.DB50.VS_UINT32Var[1] == Enum_LIST_ITEM_BEST_MATCH_LIST);
  case 23:
    return (VS_BOOL)(EventArgsVar.DB56.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB56.VS_UINT32Var[1] == SdsPopups_Popups_SDS_VR_ICPOP_AUDIO_UNAVAILABLE);
  case 24:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB51.VS_UINT32Var[1] == SdsPopups_Popups_SDS_VR_ICPOP_AUDIO_UNAVAILABLE);
  case 25:
    return (VS_BOOL)(EventArgsVar.DB56.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB56.VS_UINT32Var[1] == SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
  case 26:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB51.VS_UINT32Var[1] == SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
  case 27:
    return (VS_BOOL)(EventArgsVar.DB56.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB56.VS_UINT32Var[1] == SdsPopups_Popups_SDS_SESSION_UNAVAILABILITY);
  case 28:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB51.VS_UINT32Var[1] == SdsPopups_Popups_SDS_SESSION_UNAVAILABILITY);
  case 29:
    return (VS_BOOL)(EventArgsVar.DB56.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB56.VS_UINT32Var[1] == SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO);
  case 30:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB51.VS_UINT32Var[1] == SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO);
  case 31:
    return (VS_BOOL)(EventArgsVar.DB56.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB56.VS_UINT32Var[1] == SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK);
  case 32:
    return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB51.VS_UINT32Var[1] == SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK);
  case 33:
    return (VS_BOOL)(EventArgsVar.DB56.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB56.VS_UINT32Var[1] == SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
  }
  return (VS_BOOL)(EventArgsVar.DB51.VS_UINT32Var[0] == Enum_SURFACEID_CENTER_POPUP_SURFACE_SDS && EventArgsVar.DB51.VS_UINT32Var[1] == SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
}


/*
 * Action Expressions Wrapper Function.
 */
VS_VOID AppHmi_SdsStateMachine::VSAction (SEM_ACTION_EXPRESSION_TYPE i)
{
  switch (i)
  {
  case 0:
    Notify_Init_Finished();
    break;
  case 1:
    acPerform_SkBackPressMsg();
    break;
  case 18:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_R);
    break;
  case 19:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_R);
    break;
  case 20:
    gacViewDestroyReq(SdsPopups_Popups_SDS_VR_ICPOP_AUDIO_UNAVAILABLE);
    break;
  case 21:
    gacViewDestroyReq(SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
    break;
  case 22:
    gacViewDestroyReq(SdsPopups_Popups_SDS_SESSION_UNAVAILABILITY);
    break;
  case 23:
    gacViewDestroyReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO);
    break;
  case 24:
    gacViewDestroyReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK);
    break;
  case 25:
    gacViewDestroyReq(SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
    break;
  case 26:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_N);
    break;
  case 27:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_N);
    break;
  case 28:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_C);
    break;
  case 29:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_C);
    break;
  case 30:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_R);
    break;
  case 31:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_R);
    break;
  case 32:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_L);
    break;
  case 33:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_L);
    break;
  case 34:
    gacViewHideReq(Sds_Scenes_SDS_Settings_Main);
    break;
  case 35:
    gacViewDestroyReq(Sds_Scenes_SDS_Settings_Main);
    break;
  case 36:
    gacViewHideReq(Sds_Scenes_SDS_Help_1List);
    break;
  case 37:
    gacViewDestroyReq(Sds_Scenes_SDS_Help_1List);
    break;
  case 38:
    gacViewHideReq(Sds_Scenes_SDS_Help_3List);
    break;
  case 39:
    gacViewDestroyReq(Sds_Scenes_SDS_Help_3List);
    break;
  case 40:
    gacViewHideReq(Sds_Scenes_SDS_Settings_BestMatch);
    break;
  case 41:
    gacViewDestroyReq(Sds_Scenes_SDS_Settings_BestMatch);
    break;
  case 42:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_M);
    break;
  case 43:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_M);
    break;
  case 44:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_Q);
    break;
  case 45:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_Q);
    break;
  case 46:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_S);
    break;
  case 47:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_S);
    break;
  case 48:
    gacViewHideReq(Sds_Scenes_SDS_Layout_Map);
    break;
  case 49:
    gacViewDestroyReq(Sds_Scenes_SDS_Layout_Map);
    break;
  case 50:
    gacViewHideReq(Sds_Scenes_SDS_Layout_Mail);
    break;
  case 51:
    gacViewDestroyReq(Sds_Scenes_SDS_Layout_Mail);
    break;
  case 52:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_E);
    break;
  case 53:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_E);
    break;
  case 54:
    gacViewHideReq(Sds_Scenes_SDS_LAYOUT_T);
    break;
  case 55:
    gacViewDestroyReq(Sds_Scenes_SDS_LAYOUT_T);
    break;
  case 56:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_N);
    break;
  case 57:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_N);
    break;
  case 58:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_C);
    break;
  case 59:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_C);
    break;
  case 60:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_L);
    break;
  case 61:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_L);
    break;
  case 62:
    gacViewCreateReq(Sds_Scenes_SDS_Settings_Main);
    break;
  case 63:
    gacViewShowReq(Sds_Scenes_SDS_Settings_Main);
    break;
  case 64:
    gacViewCreateReq(Sds_Scenes_SDS_Help_1List);
    break;
  case 65:
    gacViewShowReq(Sds_Scenes_SDS_Help_1List);
    break;
  case 66:
    gacViewCreateReq(Sds_Scenes_SDS_Help_3List);
    break;
  case 67:
    gacViewShowReq(Sds_Scenes_SDS_Help_3List);
    break;
  case 68:
    gacViewCreateReq(Sds_Scenes_SDS_Settings_BestMatch);
    break;
  case 69:
    gacViewShowReq(Sds_Scenes_SDS_Settings_BestMatch);
    break;
  case 70:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_M);
    break;
  case 71:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_M);
    break;
  case 72:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_Q);
    break;
  case 73:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_Q);
    break;
  case 74:
    gacViewCreateReq(Sds_Scenes_SDS_Layout_Map);
    break;
  case 75:
    gacViewShowReq(Sds_Scenes_SDS_Layout_Map);
    break;
  case 76:
    gacViewCreateReq(Sds_Scenes_SDS_Layout_Mail);
    break;
  case 77:
    gacViewShowReq(Sds_Scenes_SDS_Layout_Mail);
    break;
  case 78:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_S);
    break;
  case 79:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_S);
    break;
  case 80:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_E);
    break;
  case 81:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_E);
    break;
  case 82:
    gacViewCreateReq(Sds_Scenes_SDS_LAYOUT_T);
    break;
  case 83:
    gacViewShowReq(Sds_Scenes_SDS_LAYOUT_T);
    break;
  case 84:
    gacPopupCreateAndSBShowReq(SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
    break;
  case 85:
    gacPopupCreateAndSBShowReq(SdsPopups_Popups_SDS_VR_ICPOP_AUDIO_UNAVAILABLE);
    break;
  case 86:
    gacPopupCreateAndSBShowReq(SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
    break;
  case 87:
    gacPopupCreateAndSBShowReq(SdsPopups_Popups_SDS_SESSION_UNAVAILABILITY);
    break;
  case 88:
    gacPopupCreateAndSBShowReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO);
    break;
  case 89:
    gacPopupCreateAndSBShowReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK);
    break;
  case 90:
    acPerform_HkBackPressMsg(EventArgsVar.DB61.VS_INT8Var[0]);
    break;
  case 91:
    acPerform_HkPressMsg(EventArgsVar.DB57.VS_INT8Var[0]);
    break;
  case 92:
    acPerform_HkPressMsg(Enum_HARDKEYCODE_HK_SELECT);
    break;
  case 93:
    acPerform_UpdateContext(Enum_HMI_CONTEXT_BACKGROUND);
    break;
  case 94:
    gacViewShowReq(SdsPopups_Popups_SDS_VR_ICPOP_AUDIO_UNAVAILABLE);
    break;
  case 95:
    gacViewHideReq(SdsPopups_Popups_SDS_VR_ICPOP_AUDIO_UNAVAILABLE);
    break;
  case 96:
    gacViewClearReq(SdsPopups_Popups_SDS_VR_ICPOP_AUDIO_UNAVAILABLE);
    break;
  case 97:
    gacViewShowReq(SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
    break;
  case 98:
    gacViewHideReq(SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
    break;
  case 99:
    gacViewClearReq(SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
    break;
  case 100:
    gacViewShowReq(SdsPopups_Popups_SDS_SESSION_UNAVAILABILITY);
    break;
  case 101:
    gacViewHideReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO);
    break;
  case 102:
    gacViewClearReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO);
    break;
  case 103:
    gacViewShowReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO);
    break;
  case 104:
    gacViewShowReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK);
    break;
  case 105:
    gacViewHideReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK);
    break;
  case 106:
    gacViewClearReq(SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK);
    break;
  case 107:
    gacViewShowReq(SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
    break;
  case 108:
    gacViewHideReq(SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
    break;
  case 109:
    gacViewClearReq(SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
    break;

  default:
    break;
  }
}
