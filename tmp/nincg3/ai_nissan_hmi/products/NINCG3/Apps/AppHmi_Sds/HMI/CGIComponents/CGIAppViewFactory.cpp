#include "gui_std_if.h"

#include "CGIAppViewFactory.h"
#include "AppHmi_SdsMessages.h"
// the views
#include "CGIAppViewController_Sds.h"
/*-------------------------------------*/
/**
 * RenderTargetInvalidatingViewScene is derived from ViewSceneBase.
 * This class is used to invalidate the scene
 *.
 *
 */
namespace Courier {
template<class ViewSceneBase> class RenderTargetInvalidatingViewScene : public ViewSceneBase
{
   public:
      RenderTargetInvalidatingViewScene(Courier::Bool managed = false) : ViewSceneBase(managed) { }
      virtual ~RenderTargetInvalidatingViewScene() { }

      virtual void Invalidate()
      {
         for (Courier::Int32 idx = 0; idx < ViewSceneBase::GetCameraPtrVector().Size(); idx++)
         {
            ViewSceneBase::GetViewHandler()->Invalidate(ViewSceneBase::GetCameraPtrVector()[idx]->GetRenderTarget());
         }
      }
};


}

SCENE_MAPPING_BEGIN(aScenes)
SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_LAYOUT_N),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDSDomain),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_LAYOUT_R),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_LAYOUT_C),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_LAYOUT_L),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_Settings_Main),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_Help_1List),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_Help_3List),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_LAYOUT_Q),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_Settings_BestMatch),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_LAYOUT_S),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_LAYOUT_M),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_Layout_Map),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_Layout_Mail),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_LAYOUT_E),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_LAYOUT_T),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_LAYOUT_O),
                       //popup
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS__MPOP_LANGUAGE_NOT_SUPPORTED),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS__ICPOP_BEST_MATCH_PHONEBOOK),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS__ICPOP_BEST_MATCH_AUDIO),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_SESSION_UNAVAILABILITY),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_VR_MPOP_VR_INIT),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_VR_MPOP_EARLY_START),
                       SCENE_MAPPING_ENTRY_2D(CGIAppViewController_SDS_VR_ICPOP_AUDIO_UNAVAILABLE),
                       SCENE_MAPPING_END()

                       CGIAppViewFactory::CGIAppViewFactory(): CGIAppViewFactoryBase(TABSIZE(aScenes), aScenes)
{
}


/**
 *  This method is used to create a new scene when it is called.
 * @param [in]= sViewName, type = Char*,<View name is passed as an input parameter>
 * @param [out] = View*, type = Courier::View, <the created view returned to calling method>
 *.@param [in, out] = none
 * @return = Courier::View*
 * @throws <NA>
 *
 * pre
 * post
 * bug = none
 */
Courier::View* CGIAppViewFactory::Create(const Courier::Char* sViewName)
{
   return SceneMapping::createView(sViewName, aScenes, TABSIZE(aScenes), GetViewHandler());
}


/**
 *  This method is used to destroy the scene when it is about to leave the state.
 * @param [in]= pView, type = Char*,<View name is passed as an input parameter>
 * @param [out] = void
 *.@param [in, out] = none
 * @return = none
 * @throws <NA>
 *
 * pre
 * post
 * bug = none
 */
void CGIAppViewFactory::Destroy(Courier::View* pView)
{
   SceneMapping::destroyView(aScenes, TABSIZE(aScenes), pView);
}


/**
 *  This method is used to create the view controller for the scene.
 * @param [in]= sViewName, type = Char*,<View name is passed as an input parameter>
 * @param [out] = Courier::ViewController
 *.@param [in, out] = none
 * @return = Courier::ViewController*
 * @throws <NA>
 *
 * pre
 * post
 * bug = none
 */
Courier::ViewController* CGIAppViewControllerFactory::Create(const Courier::Char* sViewName)
{
   return SceneMapping::createViewController(sViewName, aScenes, TABSIZE(aScenes));
}


/**
 *  This method is used to destroy the view controller .
 * @param [in]= viewController, type = Courier::ViewController*,<Pointer to view controller>
 * @param [out] = void
 *.@param [in, out] = none
 * @return = none
 * @throws <NA>
 *
 * pre
 * post
 * bug = none
 */
void CGIAppViewControllerFactory::Destroy(Courier::ViewController* viewController)
{
   COURIER_UNUSED(viewController);
}


/****************************EOF***********************/
