#if !defined (_CGIAPPVIEWCONTROLLER_SDS_H)
#define _CGIAPPVIEWCONTROLLER_SDS_H
#include "CgiExtensions/CourierMessageMapper.h"
/**
* tController is the template for the classes
* to be created for the newly added scene. Each
*.new scene added will have to add new declaration
* as shown below.
*/

#define SIMPLE_CONTROLLER(tController)\
class tController : public ViewControllerBase\
{\
   private:\
      courier_messages:\
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_SDS_COURIER_PAYLOAD_CTRL_COMP)\
      COURIER_DUMMY_CASE(0)\
      COURIER_MSG_MAP_END()\
   public:\
      tController() {}\
      virtual ~tController() {}\
      static const char* _name;\
      static Courier::ViewController* createInstance()\
      {\
         return COURIER_NEW(tController)();\
      }\
};


SIMPLE_CONTROLLER(CGIAppViewController_SDS_LAYOUT_N)
SIMPLE_CONTROLLER(CGIAppViewController_SDSDomain)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_LAYOUT_C)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_LAYOUT_L)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_LAYOUT_R)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_Settings_Main)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_Help_1List)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_Help_3List)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_LAYOUT_Q)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_Settings_BestMatch)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_LAYOUT_S)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_LAYOUT_M)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_Layout_Map)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_Layout_Mail)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_LAYOUT_E)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_LAYOUT_T)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_LAYOUT_O)

//popups
SIMPLE_CONTROLLER(CGIAppViewController_SDS__MPOP_LANGUAGE_NOT_SUPPORTED)
SIMPLE_CONTROLLER(CGIAppViewController_SDS__ICPOP_BEST_MATCH_PHONEBOOK)
SIMPLE_CONTROLLER(CGIAppViewController_SDS__ICPOP_BEST_MATCH_AUDIO)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_SESSION_UNAVAILABILITY)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_VR_MPOP_VR_INIT)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_VR_MPOP_EARLY_START)
SIMPLE_CONTROLLER(CGIAppViewController_SDS_VR_ICPOP_AUDIO_UNAVAILABLE)


#endif // _CGIAPPVIEWCONTROLLER_SDS_H
