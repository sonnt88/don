#ifndef CGIAPPCONTROLLER_H
#define CGIAPPCONTROLLER_H

#include "Common/CGIAppController/CGIAppControllerProject.h"
#include "HMIAppCtrl/Proxy/ProxyHandler.h"
#include "CgiExtensions/CGIAppControllerBase.h"
#include <BaseContract/generated/BaseMsgs.h>

namespace hmibase {
namespace services {
namespace hmiappctrl {
class ProxyHandler;
}


}
}


class CGIAppController : public CGIAppControllerProject
{
   courier_messages:
      COURIER_MSG_MAP_BEGIN(0)
      COURIER_DUMMY_CASE(0)
      ON_COURIER_MESSAGE(EncoderStatusChangedUpdMsg)
      ON_COURIER_MESSAGE(Courier::ViewPlaceholderResMsg)
      ON_COURIER_MESSAGE(RenderingCompleteMsg)
      ON_COURIER_MESSAGE(HKStatusChangedUpdMsg)
      COURIER_MSG_MAP_END_DELEGATE(CGIAppControllerProject)

   public:
      using CGIAppControllerProject::onCourierMessage;
      CGIAppController(hmibase::services::hmiappctrl::ProxyHandler& proxyHandler) : CGIAppControllerProject(proxyHandler) {}
      bool onCourierMessage(const EncoderStatusChangedUpdMsg& msg);
      bool onCourierMessage(const Courier::ViewPlaceholderResMsg& oMsg);
      bool onCourierMessage(const RenderingCompleteMsg& msg);
      bool onCourierMessage(const HKStatusChangedUpdMsg& msg);
      bool _isPlaceholdercreated;
};


#endif
