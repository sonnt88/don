#include "gui_std_if.h"
#include "CgiExtensions/PopupConfig.h"

#include "CGIAppViewController_Sds.h"

POPUP_TABLE_BEGIN()

//          popup ID, modality,                    prio, presentation time, min. presentation time, validity period, close on superseded, close on app leave, Surface Id, path in asset
POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  300, 5000, 500, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_SDS, CGIAppViewController_SDS__MPOP_LANGUAGE_NOT_SUPPORTED),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  204, 5000, 500, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_SDS, CGIAppViewController_SDS__ICPOP_BEST_MATCH_PHONEBOOK),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  204, 5000, 500, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_SDS, CGIAppViewController_SDS__ICPOP_BEST_MATCH_AUDIO),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,   32, 5000, 500, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_SDS, CGIAppViewController_SDS_SESSION_UNAVAILABILITY),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,   32, 5000, 500, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_SDS, CGIAppViewController_SDS_VR_MPOP_VR_INIT),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,   32, 5000, 500, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_SDS, CGIAppViewController_SDS_VR_MPOP_EARLY_START),
                  POPUP_TABLE_ENTRY(ScreenBroker::Modality::System,  180, 5000, 500, 0, false, false, SURFACEID_CENTER_POPUP_SURFACE_SDS, CGIAppViewController_SDS_VR_ICPOP_AUDIO_UNAVAILABLE),
                  POPUP_TABLE_END()
