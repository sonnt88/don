#ifndef _CGIAPP_H
#define _CGIAPP_H

#include "HMI/CGIComponents/CGIAppViewFactory.h"
#include "HMI/CGIComponents/CGIAppController.h"
#include "AppHmi_SdsStateMachine.h"
#include "AppBase/CgiApplication.hpp"
#include "Common/Focus/RnaiviFocus.h"

class CGIApp : public CgiApplication<AppHmi_SdsStateMachineImpl>
{
   public:
      CGIApp(const char* assetFile, hmibase::services::hmiappctrl::ProxyHandler&);
      CGIApp(const std::vector<std::string>& assetFiles, hmibase::services::hmiappctrl::ProxyHandler&);
      virtual ~CGIApp();
   private:
      virtual Focus::FManagerConfigurator* createFocusManagerConfigurator()
      {
         RnaiviFocus::setAppId(APPID_APPHMI_SDS);
         return RnaiviFocus::createConfigurator(mViewHandler);
      }
};


#endif /* _CGIAPP_H */
