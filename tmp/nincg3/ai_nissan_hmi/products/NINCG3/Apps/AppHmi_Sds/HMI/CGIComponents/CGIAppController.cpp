/**
 * This is the class registered as 'Controller' at Courier message framework
 *
 * when there is no handling for a message in the statemachine, the message comes next here
 */

#include "gui_std_if.h"

#include "AppHmi_SdsStateMachine.h"
#include "CGIAppController.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SDS_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SDS
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SDS_"
#define ETG_I_FILE_PREFIX                 App::Core::CGIAppController::
#include "trcGenProj/Header/CGIAppController.cpp.trc.h"
#endif


bool CGIAppController::onCourierMessage(const EncoderStatusChangedUpdMsg& msg)
{
   ETG_TRACE_USR4(("CGIAppController::onCourierMessage::EncoderStatusChangedUpdMsg"));
   CGIAppControllerBase::onCourierMessage(msg);
   if (msg.GetEncCode() == Enum_ENCCODE_RIGHT_ENCODER)
   {
      POST_MSG((COURIER_MESSAGE_NEW(EncoderRightRotatedStatusMsg)(msg.GetEncSteps())));
   }

   return false;
}


bool CGIAppController::onCourierMessage(const Courier::ViewPlaceholderResMsg& oMsg)
{
   ETG_TRACE_USR4(("CGIAppController::onCourierMessage::ViewPlaceholderResMsg"));

   if (oMsg.GetLoad() && oMsg.GetSuccess())
   {
      _isPlaceholdercreated = true;
   }
   else
   {
      _isPlaceholdercreated = false;
   }
   return true;
}


bool CGIAppController::onCourierMessage(const RenderingCompleteMsg& msg)
{
   ETG_TRACE_USR4(("CGIAppController:: onCourierMessage::RenderingCompleteMsg"));

   if ((msg.GetSurfaceId() == Enum_SURFACEID_MAIN_SURFACE_SDS) && !_isPlaceholdercreated)
   {
      Courier::Message* lpNewMessage =    COURIER_MESSAGE_NEW(Courier::ViewPlaceholderReqMsg)(Courier::ViewId(msg.GetViewName().   GetCString()), true);
      if (lpNewMessage != 0)
      {
         lpNewMessage->Post();
      }
   }
   CGIAppControllerProject::onCourierMessage(msg);

   return false;
}


bool CGIAppController::onCourierMessage(const HKStatusChangedUpdMsg& msg)
{
   ETG_TRACE_USR4(("CGIAppController::onCourierMessage::HKStatusChangedUpdMsg"));
   CGIAppControllerBase::onCourierMessage(msg);
   if ((msg.GetHKState() == Enum_hmibase_HARDKEYSTATE_UP) || (msg.GetHKState() == Enum_hmibase_HARDKEYSTATE_LONGUP))
   {
      POST_MSG((COURIER_MESSAGE_NEW(HKStatusChangedNotifyMsg)((Courier::UInt32)msg.GetHKCode(), msg.GetHKState())));
   }
   CGIAppControllerProject::onCourierMessage(msg);
   return false;
}
