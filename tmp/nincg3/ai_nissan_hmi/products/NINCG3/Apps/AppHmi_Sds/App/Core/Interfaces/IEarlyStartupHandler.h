/*
 * IEarlyStartupHandler.h
 *
 *  Created on: Apr 12, 2016
 *      Author: bee4kor
 */

#ifndef IEarlyStartupHandler_h
#define IEarlyStartupHandler_h

#include "App/Core/SdsHallTypes.h"


namespace App {
namespace Core {

class IEarlyStartupHandler
{
   public:
      virtual ~IEarlyStartupHandler() {};
      virtual enEarlyContextState startEarlySdsContext(unsigned int _srcContext, unsigned int _destContext) = 0 ;
      virtual bool isSdsMiddlewareAvailable() = 0;
      virtual bool isSdsAdapterAvailable() = 0;
};


}
}


#endif /* IEarlyStartupHandler_h */
