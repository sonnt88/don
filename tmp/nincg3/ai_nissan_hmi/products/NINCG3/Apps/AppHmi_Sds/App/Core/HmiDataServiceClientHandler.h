/*
 * HmiDataServiceClientHandler.h
 *
 *  Created on: Jan 4, 2016
 *      Author: vee4kor
 */

#ifndef HmiDataServiceClientHandler_h
#define HmiDataServiceClientHandler_h

#include "bosch/cm/ai/nissan/hmi/hmidataservice/HmiDataProxy.h"
#include "sds_gui_fi/SdsGuiServiceProxy.h"
#include "sds_gui_fi/PopUpServiceProxy.h"
#include "App/Core/SdsHallTypes.h"

namespace App {
namespace Core {

class ISdsAdapterRequestor;
class ISdsContextRequestor;

class HmiDataServiceClientHandler
   : public asf::core::ServiceAvailableIF
   , public ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::VisibleApplicationModeCallbackIF
   , public ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::SetApplicationModeCallbackIF
   , public sds_gui_fi::SdsGuiService::EventCallbackIF
   , public sds_gui_fi::PopUpService::SdsStatusCallbackIF
{
   public:

      virtual ~HmiDataServiceClientHandler();
      HmiDataServiceClientHandler(ISdsAdapterRequestor* poISdsAdapterRequestor,
                                  const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::HmiDataProxy >& hmiDataProxy,
                                  const ::boost::shared_ptr<sds_gui_fi::SdsGuiService::SdsGuiServiceProxy>& sdsGuiServiceProxy,
                                  const ::boost::shared_ptr<sds_gui_fi::PopUpService::PopUpServiceProxy>& popUpServiceProxy);

      virtual void onVisibleApplicationModeError(const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::HmiDataProxy >& proxy,
            const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::VisibleApplicationModeError >& error);

      virtual void onVisibleApplicationModeUpdate(const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::HmiDataProxy >& proxy,
            const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::VisibleApplicationModeUpdate >& update);

      virtual void onSetApplicationModeError(const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::HmiDataProxy >& proxy,
                                             const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::SetApplicationModeError >& error);

      virtual void onSetApplicationModeResponse(const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::HmiDataProxy >& proxy,
            const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::SetApplicationModeResponse >& response);

      virtual void onEventSignal(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy,
                                 const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::EventSignal >& signal);
      virtual void onEventError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy,
                                const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::EventError >& error);

      virtual void onSdsStatusError(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& proxy,
                                    const ::boost::shared_ptr< sds_gui_fi::PopUpService::SdsStatusError >& error);
      virtual void onSdsStatusUpdate(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& proxy,
                                     const ::boost::shared_ptr< sds_gui_fi::PopUpService::SdsStatusUpdate >& update);

      void onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange);

      void setContextRequestor(ISdsContextRequestor* poSdsContextRequestor);
   private:

      ISdsAdapterRequestor* _pSdsAdapterRequestor;
      ISdsContextRequestor* _pSdsContextRequestor;
      ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::HmiDataProxy > _hmiDataProxy;
      ::boost::shared_ptr<sds_gui_fi::SdsGuiService::SdsGuiServiceProxy> _sdsGuiServiceProxy;
      ::boost::shared_ptr<sds_gui_fi::PopUpService::PopUpServiceProxy> _popUpServiceProxy;
      ::sds_gui_fi::PopUpService::SpeechInputState _sdsStatus;

      void handleVisibleAppmode(unsigned int visibleAppmode);
      void setActiveApplicationmode(bool isVRActive);
      bool isHighPriorityAppActive(unsigned int visibleAppmode);
      bool isSDSActive();
};


} // namespace App
} // namespace Core

#endif
