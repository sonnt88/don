/**
 *  @file   <SdsHall.cpp>
 *  @author <ECV2> - <A-IVI>
 *  @copyright (c) <2014> Robert Bosch Car Multimedia GmbH
 *  @addtogroup <Apphmi_Sds>
 */


#include "hall_std_if.h"
#include "App/Core/SdsHall.h"
#include "App/Core/SdsDefines.h"
#include "App/Core/HmiDataServiceClientHandler.h"
#include "App/Core/ListHandler/ListHandler.h"
#include "App/Core/TTFisCommander/SdsTTfisTestCommandHandler.h"
#include "App/Core/SdsAdapter/GuiUpdater.h"
#include "App/Core/SdsAdapter/SdsEventHandler.h"
#include "App/Core/SdsAdapter/VRSettingsHandler.h"
#include "App/datapool/SDSDatapoolConfig.h"
#include "HmiTranslation_TextIds.h"
#include "proxy/clContextProxy.h"
#include "App/Core/VehicleHandler/Proxy/provider/HmiVehicleHandler.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SDS_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SDS
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SDS_"
#define ETG_I_FILE_PREFIX                 App::Core::SdsHall::
#include "trcGenProj/Header/SdsHall.cpp.trc.h"
#endif

namespace App {
namespace Core {


SdsHall::SdsHall()
   : HallComponentBase("", "App.Core.AppHmi_Sds", "/org/genivi/NodeStateManager/LifeCycleConsumer/AppHmi_Sds")
   , _guiPrevFocusIndex(INVALID_FOCUS_INDEX)
   , _guiFocusChanged(false)
   , _guiPrevEncSteps(0)
{
   ETG_TRACE_USR4(("SdsHall:SdsHall Constructor"));
   ETG_I_REGISTER_FILE();
   ////////////////////////////////////////////////////////Proxy Creation////////////////////////////////////////////////////////////////////////////////
   _sdsGuiServiceProxy		= sds_gui_fi::SdsGuiService::SdsGuiServiceProxy::createProxy("sdsGuiAppPort", *this);
   _popUpServiceProxy		= sds_gui_fi::PopUpService::PopUpServiceProxy::createProxy("sdsPopUpPort", *this);
   _settingsServiceProxy	= sds_gui_fi::SettingsService::SettingsServiceProxy::createProxy("sdsSettingsPort", *this);
   _sdsPhoneServiceProxy	= sds_gui_fi::SdsPhoneService::SdsPhoneServiceProxy::createProxy("sdsPhonePort", *this);
   _hmiDataProxy			= bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::HmiDataProxy::createProxy("hmiDataServicePort", *this);
   _pHmiInfoProxy = HmiInfoProxy::GetInstance();
   ///////////////////Default Service Initializations////////////////////////////////////////////////////////////////////////////////////////////////////
   StartupSync::getInstance().registerPropertyRegistrationIF(this);
   DefSetServiceBase::s_Intialize("AppHmiSdsServicePort");
   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   /////Courier Message Subscriptions to Model///////////////////////////////////////////////////////////////////////////////////////////////////////////
   FocusChangedUpdMsg::Subscribe(Courier::ComponentType::Model);
   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   /////////////////Instantiation of Handler Members/////////////////////////////////////////////////////////////////////////////////////////////////////
   _pHMIModelComponent = new(std::nothrow)HMIModelComponent();
   _pGuiUpdater = new(std::nothrow) GuiUpdater();
   _pVRSettingsHandler = new(std::nothrow)VRSettingsHandler(_settingsServiceProxy, _pGuiUpdater);
   _pSdsAdapterRequestor = new(std::nothrow) SdsAdapterRequestor(_sdsGuiServiceProxy);
   _sdsHmiLanguageHandler = new(std::nothrow) SdsHmiLanguageHandler();
   _pContextRequestor  = new(std::nothrow) AppSds_ContextRequestor();
   _pListHandler = new(std::nothrow)ListHandler(_pGuiUpdater, _pVRSettingsHandler, _pSdsAdapterRequestor);
   _pHmiVehicleHandler = new(std::nothrow)HmiVehicleHandler();

   if (_pHMIModelComponent && _pGuiUpdater && _pContextRequestor && _pListHandler)
   {
      _pEarlyStartupHandler = new(std::nothrow) EarlyStartupHandler(_pContextRequestor, _sdsGuiServiceProxy, _popUpServiceProxy, _settingsServiceProxy, _pListHandler);
      _pSdsAdapterProvider = new(std::nothrow) SdsAdapterProvider(_popUpServiceProxy, _settingsServiceProxy, _sdsPhoneServiceProxy, _pGuiUpdater, _pHMIModelComponent, _pListHandler);
   }

   _pSdsEventHandler = new(std::nothrow) SdsEventHandler(_sdsGuiServiceProxy, _pContextRequestor, _pListHandler);

   if (_pSdsAdapterRequestor && _pEarlyStartupHandler && _pListHandler)
   {
      _pHmiDataServiceClientHandler = new(std::nothrow) HmiDataServiceClientHandler(_pSdsAdapterRequestor, _hmiDataProxy, _sdsGuiServiceProxy, _popUpServiceProxy);
      _pContextProvider   = new(std::nothrow) AppSds_ContextProvider(_pSdsAdapterRequestor, _pEarlyStartupHandler, _pListHandler);
   }

   if (_pContextProvider && _pContextRequestor && _pSdsAdapterProvider && _pSdsAdapterRequestor && _pHmiDataServiceClientHandler)
   {
      _pContextProvider->setContextRequestor(_pContextRequestor);
      _pHmiDataServiceClientHandler->setContextRequestor(_pContextRequestor);

      clContextProxy::instance()->vSetApplication(APPID_APPHMI_SDS);
      clContextProxy::instance()->vSetProviderImpl(_pContextProvider);
      clContextProxy::instance()->vSetRequestorImpl(_pContextRequestor);

      _pSdsAdapterProvider->setContextRequestor(_pContextRequestor);
      _pSdsAdapterProvider->setContextProvider(_pContextProvider);
      _pSdsAdapterProvider->setEventHandler(_pSdsEventHandler);
      _pSdsTTfisTestCommandHandler = new(std::nothrow) SdsTTfisTestCommandHandler(_pSdsAdapterRequestor, _pContextRequestor, _pGuiUpdater, _pVRSettingsHandler, _pListHandler);
   }

   // TODO:abo6kor to handle in respective classes
   vRegisterObservers();
   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   ////////////////// SDS Reset Factory Settings //////////////////////////////////////////////////////////////////////////////////////////////////////////
   _pDefSetServiceBase = DefSetServiceBase::GetInstance();
   if (NULL != _pDefSetServiceBase)
   {
      _pDefSetServiceBase->vRegisterforUpdate(this);
   }
   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   registerServiceAvailableObservers();
}


SdsHall::~SdsHall()
{
   DELETE_AND_PURGE(_pSdsAdapterProvider);
   DELETE_AND_PURGE(_pSdsAdapterRequestor);
   DELETE_AND_PURGE(_pContextProvider);
   DELETE_AND_PURGE(_pContextRequestor);
   DELETE_AND_PURGE(_pHmiDataServiceClientHandler);
   DELETE_AND_PURGE(_pSdsTTfisTestCommandHandler);
   DELETE_AND_PURGE(_pGuiUpdater);
   DELETE_AND_PURGE(_pVRSettingsHandler);
   DELETE_AND_PURGE(_sdsHmiLanguageHandler);
   DELETE_AND_PURGE(_pEarlyStartupHandler);
   DELETE_AND_PURGE(_pListHandler);
   DELETE_AND_PURGE(_pHMIModelComponent);
   DELETE_AND_PURGE(_pHmiVehicleHandler);
   PURGE(_pHmiInfoProxy);
   DELETE_AND_PURGE(_pSdsEventHandler);

   if (NULL != _pDefSetServiceBase)
   {
      _pDefSetServiceBase->vUnRegisterforUpdate(this);
      _pDefSetServiceBase = NULL; //Do not delete this instance as its not created by SDS HALL.
   }
   DefSetServiceBase::s_Destrory();
}


void SdsHall::onExpired(asf::core::Timer& /*timer*/, boost::shared_ptr<asf::core::TimerPayload> /*data*/)
{
}


void SdsHall::registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange)
{
   ETG_TRACE_USR4(("SdsHall:registerProperties is called,ProxyAvail=%d,StateChange=%d", proxy->isAvailable(), stateChange.kCurrentState));

   if (_pListHandler)
   {
      _pListHandler->updateSubCommandList();
   }

   for (unsigned int i = 0; i < _proxyAvailabilityObservers.size(); i++)
   {
      if (_proxyAvailabilityObservers[i])
      {
         _proxyAvailabilityObservers[i]->onAvailable(proxy, stateChange);
      }
   }
}


void SdsHall::deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange)
{
   ETG_TRACE_USR4(("SdsHall:deregisterProperties is called,ProxyAvail=%d,StateChange=%d", proxy->isAvailable(), stateChange.kCurrentState));

   for (unsigned int i = 0; i < _proxyAvailabilityObservers.size(); i++)
   {
      if (_proxyAvailabilityObservers[i])
      {
         _proxyAvailabilityObservers[i]->onUnavailable(proxy, stateChange);
      }
   }
}


void SdsHall::addServiceAvailableObservers(asf::core::ServiceAvailableIF* obs)
{
   if (obs)
   {
      _proxyAvailabilityObservers.push_back(obs);
   }
}


void SdsHall::registerServiceAvailableObservers()
{
   addServiceAvailableObservers(_pHmiDataServiceClientHandler);
   addServiceAvailableObservers(_pSdsAdapterProvider);
   addServiceAvailableObservers(_pEarlyStartupHandler);
   addServiceAvailableObservers(_pVRSettingsHandler);
   addServiceAvailableObservers(_pSdsEventHandler);
}


unsigned int SdsHall::updateTestModeData(std::string _header, std::string _upperCase, std::string _lowerCase, std::string _number, std::string _sentence)
{
   Courier::DataItemContainer<SDS_Test_ModeDataBindingSource> testModeData;
   _header += "\n" + _upperCase + "\n" + _lowerCase + "\n" + _number + "\n" + _sentence;
   (*testModeData).mTextHeader = _header.c_str();

   testModeData.MarkItemModified(ItemKey::SDS_Test_ModeItem);
   testModeData.SendUpdate(true);
   if (_pSdsAdapterRequestor)
   {
      _pSdsAdapterRequestor->sendTestModeSentence(_sentence);
   }

   return true;
}


void SdsHall::handleHardKeyShortPress(unsigned int hardkeyType)


{
   ETG_TRACE_USR4(("handleHardKeyShortPress %d", hardkeyType));

   switch (hardkeyType)
   {
      case Enum_HARDKEYCODE_HK_AUX:
      case Enum_HARDKEYCODE_HK_AUDIO:
      case Enum_HARDKEYCODE_HK_PHONE:
      case Enum_HARDKEYCODE_HK_MEDIA:
      case Enum_HARDKEYCODE_HK_FM_AM:
      case Enum_HARDKEYCODE_HK_CD:
      case Enum_HARDKEYCODE_HK_SETTINGS:
      case Enum_HARDKEYCODE_HK_INFO:
      case Enum_HARDKEYCODE_HK_MENU:
      case Enum_HARDKEYCODE_HK_NAV:
      case Enum_HARDKEYCODE_HK_MAP:
      case Enum_HARDKEYCODE_HK_XMTUNER:
      case Enum_HARDKEYCODE_HK_CAMERA:
      case Enum_HARDKEYCODE_SWC_PHONE:
      case Enum_HARDKEYCODE_SWC_MENU_BACK:
      case Enum_HARDKEYCODE_SWC_SWITCH_AUDIO:
      {
         if (_pSdsAdapterRequestor)
         {
            _pSdsAdapterRequestor->sendStopSession();
         }
         break;
      }
      case Enum_HARDKEYCODE_HK_SELECT:
      {
         if (_pSdsAdapterRequestor && _guiPrevFocusIndex != INVALID_FOCUS_INDEX)
         {
            _pSdsAdapterRequestor->listItemSelected(_guiPrevFocusIndex);
            _guiPrevFocusIndex = INVALID_FOCUS_INDEX;
         }
         break;
      }

      default:
         ETG_TRACE_USR4(("Ignoring Hardkey %d", hardkeyType));
         break;
   }
}


void SdsHall::handleHardKeyLongPress(unsigned int hardkeyType)
{
   ETG_TRACE_USR4(("handleHardKeyLongPress %d", hardkeyType));
   switch (hardkeyType)
   {
      case Enum_HARDKEYCODE_SWC_PTT:
      case Enum_HARDKEYCODE_HK_ILLUM:
      case Enum_HARDKEYCODE_SWC_PHONE:
         if (_pSdsAdapterRequestor)
         {
            _pSdsAdapterRequestor->sendStopSession();
         }
         break;
      default:
         ETG_TRACE_USR4(("Ignoring Hardkey %d", hardkeyType));
         break;
   }
}


bool SdsHall::onCourierMessage(const HKStatusChangedNotifyMsg& msg)
{
   if (msg.GetHkState() == Enum_hmibase_HARDKEYSTATE_LONGUP)
   {
      handleHardKeyLongPress(msg.GetHkCode());
   }
   else if (msg.GetHkState() == Enum_hmibase_HARDKEYSTATE_UP)
   {
      handleHardKeyShortPress(msg.GetHkCode());
   }
   else
   {
      return false;
   }

   return true;
}


bool SdsHall::onCourierMessage(const HkBackPressMsg& /*msg*/)
{
   ETG_TRACE_USR4(("onCourierMessage::HkBackPressMsg"));

   if (_pSdsAdapterRequestor && _pContextProvider && _pContextRequestor && _pListHandler)
   {
      if (_pListHandler->getCurrentLayout() == SDS_Settings_Main)
      {
         _pContextProvider->contextSwitchComplete();
         _pContextRequestor->contextSwitch(APPID_APPHMI_SYSTEM, SYSTEM_CONTEXT_SETTINGS_MAIN);
      }
      else if (_pListHandler->getCurrentLayout() == SDS_Settings_BestMatch)
      {
         POST_MSG((COURIER_MESSAGE_NEW(ShowDomainScreen)(SDS_Settings_Main)));
      }
      else
      {
         _pSdsAdapterRequestor->backKeyPressed();
      }
   }
   return true;
}


bool SdsHall::onCourierMessage(const SkBackPressMsg& /*msg*/)
{
   ETG_TRACE_USR4(("onCourierMessage::SkBackPressMsg"));

   if (_pSdsAdapterRequestor && _pContextProvider && _pContextRequestor && _pListHandler)
   {
      if (_pListHandler->getCurrentLayout() == SDS_Settings_Main)
      {
         _pContextProvider->contextSwitchComplete();
         _pContextRequestor->contextSwitch(APPID_APPHMI_SYSTEM, SYSTEM_CONTEXT_SETTINGS_MAIN);
      }
      else
      {
         _pSdsAdapterRequestor->backKeyPressed();
      }
   }
   return true;
}


bool SdsHall::onCourierMessage(const nBestToggleMsg& msg)
{
   ETG_TRACE_USR4(("SdsHall::onCourierMessage - nBestToggleMsg  received "));
   if (_pSdsAdapterRequestor)
   {
      storeBestMatchPhonebooktodatapool((uint8) msg.GetNBestPhoneBookToggleType());
      storeBestMatchAudiotodatapool((uint8) msg.GetNBestAudioToggleType());

      return true;
   }
   else
   {
      ETG_TRACE_FATAL(("Error::_pSdsAdapterProvider is NULL"));
      return false;
   }
}


bool SdsHall::onCourierMessage(const FocusChangedUpdMsg& oMsg)
{
   ETG_TRACE_USR4(("SdsHall::onCourierMessage - FocusChangedUpdMsg"));
   ListProviderEventInfo info;
   if (ListProviderEventInfo::GetItemIdentifierInfo(oMsg.GetWidget(), info))
   {
      ETG_TRACE_USR4(("ButtonReactionMsg - LIST_ID_SDS_VL"));
      ETG_TRACE_USR4(("info.getListId() = %d", info.getListId()));
      ETG_TRACE_USR4(("info.getHdlRow() = %d", info.getHdlRow()));

      if (_guiPrevFocusIndex != info.getHdlRow())
      {
         ETG_TRACE_USR4(("Focus is Changed"));
         _guiFocusChanged = true;

         _guiPrevFocusIndex = info.getHdlRow();

         if (_pSdsAdapterRequestor)
         {
            _pSdsAdapterRequestor->focusMoved(info.getHdlRow());
         }
         return true;
      }
      else
      {
         ETG_TRACE_USR4(("Focus is Not Changed"));
         _guiFocusChanged = false;
      }
   }
   return false;
}


bool SdsHall::onCourierMessage(const EncoderRightRotatedStatusMsg& oMsg)
{
   ETG_TRACE_USR4(("SdsHall::onCourierMessage - EncoderRightRotatedStatusMsg , steps = %d", oMsg.GetEncSteps()));
   if (_guiPrevEncSteps == 0)
   {
      _guiPrevEncSteps = oMsg.GetEncSteps();
      return true;
   }
   else if (_pListHandler && _pListHandler->getCurrentLayout() != LAYOUT_R)
   {
      if (!_guiFocusChanged)
      {
         if ((oMsg.GetEncSteps() > 0) && (_guiPrevEncSteps > 0))
         {
            if (_pSdsAdapterRequestor)
            {
               _pSdsAdapterRequestor->requestNextPage();
            }
         }
         else if ((oMsg.GetEncSteps() < 0) && (_guiPrevEncSteps < 0))
         {
            if (_pSdsAdapterRequestor)
            {
               _pSdsAdapterRequestor->requestPrevPage();
            }
         }
      }
   }
   _guiPrevEncSteps = oMsg.GetEncSteps();
   return true;
}


bool SdsHall::onCourierMessage(const ListChangedUpdMsg& /*oMsg*/)
{
   ETG_TRACE_USR4(("SdsHall::ListChangedUpdMsg -   received "));
   return false;
}


uint8 SdsHall::getBestMatchPhonebookfromdatapool()
{
   uint8 bestmatchphonebook = SDSDataPoolConfig::getInstance()->getBestMatchPhonebook();
   ETG_TRACE_USR4(("SdsHall::getBestMatchPhonebookfromdatapool :%d", bestmatchphonebook));
   return bestmatchphonebook;
}


uint8 SdsHall::getBestMatchAudiofromdatapool()
{
   uint8 bestmatchAudio = SDSDataPoolConfig::getInstance()->getBestMatchAudio();
   ETG_TRACE_USR4(("SdsHall::getBestMatchAudiofromdatapool :%d", bestmatchAudio));
   return bestmatchAudio;
}


void SdsHall::storeBestMatchPhonebooktodatapool(uint8 bestmatchphonebook)
{
   SDSDataPoolConfig::getInstance()->setBestMatchPhonebook(bestmatchphonebook);
   ETG_TRACE_USR4(("SDSHall::storeBestMatchPhonebooktodatapool :%d", bestmatchphonebook));
}


void  SdsHall::storeBestMatchAudiotodatapool(uint8 bestmatchAudio)
{
   SDSDataPoolConfig::getInstance()->setBestMatchAudio(bestmatchAudio);
   ETG_TRACE_USR4(("SDSHall::storeBestMatchAudiotodatapool :%d", bestmatchAudio));
}


void SdsHall::reqPrepareResponse()
{
   ETG_TRACE_USR4(("SdsHall reqPrepareResponse"));

   if (_pDefSetServiceBase != NULL)
   {
      _pDefSetServiceBase->sendPrepareResponse(0, this);
   }
}


void SdsHall::reqExecuteResponse()
{
   ETG_TRACE_USR4(("SdsHall reqExecuteResponse"));

   if (_pDefSetServiceBase != NULL)
   {
      _pDefSetServiceBase->sendExecuteResponse(static_cast<int>(bSdsRestoreFactorySetting()), this);
   }
}


void SdsHall::reqFinalizeResponse()
{
   ETG_TRACE_USR4(("SdsHall reqFinalizeResponse"));

   if (_pDefSetServiceBase != NULL)
   {
      _pDefSetServiceBase->sendFinalizeResponse(0, this);
   }
}


bool SdsHall::bSdsRestoreFactorySetting()
{
   bool bResult = false ; //0 for success

   // TO DO: Restore SDS_Settings_BestMatch Screen.

   if (_pSdsAdapterRequestor)
   {
      _pSdsAdapterRequestor->restoreFactorySDSSettings();
   }
   return bResult;
}


void SdsHall::vRegisterObservers()
{
   if (_pHmiVehicleHandler)
   {
      _pHmiVehicleHandler->vRegisterObservers(_pSdsAdapterProvider);
   }

   if (_pSdsAdapterProvider)
   {
      _pSdsAdapterProvider->vRegisterObservers(_pHmiVehicleHandler);
   }
}


} // namespace Core
} // namespace App
