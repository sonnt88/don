#include "gui_std_if.h"

#include "AppUtils/StrUtf8.h"
#include "AppHmi_SdsStateMachine.h"
#include "HMIModelComponent.h"
#include "hmi_trace_if.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_APPHMI_SDS_DM
#include "trcGenProj/Header/HMIModelComponent.cpp.trc.h"
#endif

namespace App {
namespace Core {


HMIModelComponent::HMIModelComponent(): _sdsHmiActiveAppMode(SDSHMI_MODE_BACKGROUND)
{
}


HMIModelComponent::~HMIModelComponent()
{
}


bool HMIModelComponent::onCourierMessage(const UpdateSdsHmiAppMode& msg)
{
   ETG_TRACE_USR4(("HMIModelComponent::onCourierMessage-UpdateSdsHmiAppMode"));
   if (msg.GetAppMode() == SDSHMI_MODE_FOREGROUND)
   {
      ETG_TRACE_USR4(("Application Mode : SDSHMI_MODE_FOREGROUND"));
   }
   else
   {
      ETG_TRACE_USR4(("Application Mode : SDSHMI_MODE_BACKGROUND"));
   }

   if (_sdsHmiActiveAppMode != msg.GetAppMode())
   {
      _sdsHmiActiveAppMode = msg.GetAppMode();
      notifySdsHmiApplicationModeObservers();
   }

   return true;
}


void HMIModelComponent::registerSdsApplicationModeObserver(ActiveApplicationModeObserver* pActiveApplicationModeObserver)
{
   ETG_TRACE_USR4(("HMIModelComponent::registerSdsApplicationModeObserver()"));
   _appModeObserver.push_back(pActiveApplicationModeObserver);
}


void HMIModelComponent::notifySdsHmiApplicationModeObservers()
{
   ETG_TRACE_USR4(("HMIModelComponent::notifySdsHmiApplicationModeObservers = %d", _appModeObserver.size()));

   for (::std::vector< ActiveApplicationModeObserver* >::iterator iter = _appModeObserver.begin(); iter != _appModeObserver.end(); ++iter)
   {
      if (*iter != NULL)
      {
         (*iter)->sdsHmiActiveAppModeChanged(_sdsHmiActiveAppMode);
      }
   }
}


}
}
