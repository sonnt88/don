/*
 * SdsHmiLanguageHandler.cpp
 *
 *  Created on: Apr 4, 2016
 *      Author: heg6kor
 */
#include "App/Core/LanguageHandler/Proxy/provider/SdsHmiLanguageHandler.h"
#include "App/datapool/SDSDatapoolConfig.h"

#include "hmi_trace_if.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SDS_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SDS
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SDS_"
#define ETG_I_FILE_PREFIX                 App::Core::SdsHall::
#include "trcGenProj/Header/SdsHmiLanguageHandler.cpp.trc.h"
#endif
namespace App {
namespace Core {


SdsHmiLanguageHandler::SdsHmiLanguageHandler()
   : _languageHandler(VehicleDataCommonHandler::VehicleDataHandler::GetInstance())
{
   if (_languageHandler != NULL)
   {
      _languageHandler->vRegisterforUpdate(this);
   }
}


SdsHmiLanguageHandler::~SdsHmiLanguageHandler()
{
   if (_languageHandler)
   {
      _languageHandler->vUnRegisterforUpdate(this);
   }
   delete _languageHandler;
   _languageHandler = NULL;
}


uint8 SdsHmiLanguageHandler::readLanguageOnStartUp()
{
   uint8 languageIndex = SDSDataPoolConfig::getInstance()->getLanguageValue();
   ETG_TRACE_USR4(("SdsHmiLanguageHandler::readLanguageOnStartUp: languageIndex %d", languageIndex));
   return languageIndex;
}


void SdsHmiLanguageHandler::writeLanguageintoDP(uint8 languageIndex)
{
   ETG_TRACE_USR4(("SdsHmiLanguageHandler::writeLanguageintoDP : languageIndex %d", languageIndex));
   SDSDataPoolConfig::getInstance()->setLanguageValue(languageIndex);
}


} // namespace Core
} // namespace App
