/****************************************************************************
 * Copyright (C) Robert Bosch Car Multimedia GmbH, 2013
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ***************************************************************************/
#ifndef SdsGui_h
#define SdsGui_h

#include "AppBase/GuiComponentBase.h"
#include "HMIAppCtrl/Proxy/ProxyHandler.h"


namespace App
{
namespace Core
{


class SdsGui :
   public GuiComponentBase
{
   public:
      /**
      * SdsGui - Constructor
      */
      SdsGui();
      virtual ~SdsGui();

      virtual void preRun();
      virtual void postRun();

      /**
      * TraceCmd_NotProcessedMsg - Sink for not processed TTFIS input messages
      * @param[in] pcu8Data - String Containing Error Message
      * @return void
      */
      static void TraceCmd_NotProcessedMsg (const unsigned char* pcu8Data);

   protected:
      virtual unsigned int getDefaultTraceClass();
      virtual void setupCgiInstance();

      /**
      * @class_member - _hmiAppCtrlProxyHandler - The application control proxy handler object
      */
      hmibase::services::hmiappctrl::ProxyHandler _hmiAppCtrlProxyHandler;

   private:

      void PersistentValuesRead();
      void PersistentValuesWrite();

};


} // Namespace Core
} // Namespace App


#endif
