/**
 *  @file   <SmsHeaderVisibilityObserver.h>
 *  @author <ECV2> - <A-IVI>
 *  @copyright (c) <2014> Robert Bosch Car Multimedia GmbH
 *  @addtogroup <Apphmi_Sds>
 */

#ifndef SmsHeaderVisibilityObserver_h
#define SmsHeaderVisibilityObserver_h


namespace App {
namespace Core {

class SmsHeaderVisibilityObserver
{
   public:
      SmsHeaderVisibilityObserver();
      virtual ~SmsHeaderVisibilityObserver();
      virtual void vOnSmsHeaderVisibilityChanged(bool vehicleMovingSpeed) = 0;
};


}
}


#endif
