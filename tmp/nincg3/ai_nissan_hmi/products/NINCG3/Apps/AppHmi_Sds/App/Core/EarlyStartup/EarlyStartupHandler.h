/*
 * EarlyStartupHandler.h
 *
 *  Created on: Apr 12, 2016
 *      Author: bee4kor
 */

#ifndef EarlyStartupHandler_h
#define EarlyStartupHandler_h


#include "App/Core/Interfaces/IEarlyStartupHandler.h"
#include "CgiExtensions/ListDataProviderDistributor.h"
#include "sds_gui_fi/SdsGuiServiceProxy.h"
#include "sds_gui_fi/PopUpServiceProxy.h"
#include "sds_gui_fi/SettingsServiceProxy.h"


namespace App {
namespace Core {

class IHMIModelComponent;
class ISdsContextRequestor;
class ListHandler;

class EarlyStartupHandler
   : public asf::core::ServiceAvailableIF
   , public IEarlyStartupHandler
   , public sds_gui_fi::SdsGuiService::StartSessionContextCallbackIF
   , public sds_gui_fi::SdsGuiService::StopEarlyHandlingCallbackIF
   , public sds_gui_fi::PopUpService::SdsStatusCallbackIF
   , public sds_gui_fi::SdsGuiService::PttPressCallbackIF
   , public sds_gui_fi::SdsGuiService::StartEarlyHandlingCallbackIF
{
   public:
      EarlyStartupHandler(ISdsContextRequestor* pSdsContextRequestor,
                          const ::boost::shared_ptr<sds_gui_fi::SdsGuiService::SdsGuiServiceProxy>& sdsGuiServiceProxy,
                          const ::boost::shared_ptr<sds_gui_fi::PopUpService::PopUpServiceProxy>& popUpServiceProxy,
                          const ::boost::shared_ptr<sds_gui_fi::SettingsService::SettingsServiceProxy>& settingsServiceProxy,
                          ListHandler* pListHandler);

      virtual ~EarlyStartupHandler();

      void onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange);
      void onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange);

      enEarlyContextState startEarlySdsContext(unsigned int _srcContext, unsigned int _destContext);
      bool isSdsMiddlewareAvailable();
      bool isSdsAdapterAvailable();

   private:

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_SDS_COURIER_PAYLOAD_MODEL_COMP)
      ON_COURIER_MESSAGE(CloseEarlyStartSession)
      COURIER_MSG_MAP_END()
      bool onCourierMessage(const CloseEarlyStartSession& msg);

      ISdsContextRequestor* _pSdsContextRequestor;
      ListHandler* _pListHandler;

      ::boost::shared_ptr<sds_gui_fi::SdsGuiService::SdsGuiServiceProxy> _sdsGuiServiceProxy;
      ::boost::shared_ptr<sds_gui_fi::PopUpService::PopUpServiceProxy> _popUpServiceProxy;
      ::boost::shared_ptr<sds_gui_fi::SettingsService::SettingsServiceProxy> _settingsServiceProxy;
      bool _isSdsMiddlewareAvailable;
      enEarlySavedContext _earlySavedContext;
      unsigned int _earlyDestContext;

      void showVRSettingsScreen();
      void initiateEarlySdsContext();
      void requestEarlyVRSettingsContext();
      void saveEarlyContext(enEarlySavedContext earlyContext, unsigned int earlyDestContext);
      void handleEarlySavedContexts();
      enEarlyContextState handleEarlyVRShortcuts(unsigned int _srcContext, unsigned int _destContext);
      enEarlyContextState handleEarlyGeneralContexts(unsigned int _srcContext, unsigned int _destContext);
      enEarlyContextState handleEarlySettingsContexts(unsigned int _srcContext, unsigned int _destContext);
      sds_gui_fi::SdsGuiService::ContextType mapGui2AdapterShortcutContext(unsigned int _guiContext);
      void setInitialSdsMiddlewareAvailability(const ::boost::shared_ptr< sds_gui_fi::PopUpService::SdsStatusUpdate >& update);

      virtual void onStartSessionContextError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy,
                                              const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StartSessionContextError >& error) ;
      virtual void onStartSessionContextResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy,
            const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StartSessionContextResponse >& response);
      virtual void onSdsStatusError(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& proxy,
                                    const ::boost::shared_ptr< sds_gui_fi::PopUpService::SdsStatusError >& error);
      virtual void onSdsStatusUpdate(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& proxy,
                                     const ::boost::shared_ptr< sds_gui_fi::PopUpService::SdsStatusUpdate >& update);
      void onPttPressError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy,
                           const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::PttPressError >& error) ;
      void onPttPressResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy,
                              const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::PttPressResponse >& response) ;
      void onStartEarlyHandlingError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StartEarlyHandlingError >& error);

      void onStartEarlyHandlingResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StartEarlyHandlingResponse >& response);

      void onStopEarlyHandlingError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StopEarlyHandlingError >& error);

      void onStopEarlyHandlingResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StopEarlyHandlingResponse >& response);
};


}
}


#endif /* EarlyStartupHandler_h */
