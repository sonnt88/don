/**
 *  @file   <SdsSessionStartObserver.h>
 *  @author <ECV2> - <A-IVI>
 *  @copyright (c) <2014> Robert Bosch Car Multimedia GmbH
 *  @addtogroup <Apphmi_Sds>
 */

#ifndef SdsSessionStartObserver_h
#define SdsSessionStartObserver_h


namespace App {
namespace Core {

class SdsSessionStartObserver
{
   public:
      SdsSessionStartObserver();
      virtual ~SdsSessionStartObserver();
      virtual void onSDSSessionStarted() = 0;
      virtual void onSDSSessionEnd() = 0;
};


}
}


#endif
