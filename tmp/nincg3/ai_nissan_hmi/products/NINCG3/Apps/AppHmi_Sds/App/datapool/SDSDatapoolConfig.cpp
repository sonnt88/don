/****************************************************************************
 * Copyright (C) Robert Bosch Car Multimedia GmbH, 2015
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ***************************************************************************/


#include "hall_std_if.h"

#ifdef DP_DATAPOOL_ID
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_hmi_02_if.h"
#endif

#include "SDSDatapoolConfig.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#endif

// Instantiate the static class object
SDSDataPoolConfig* SDSDataPoolConfig::_DpSDS = NULL;


// Constructor
SDSDataPoolConfig::SDSDataPoolConfig()
{
   DP_vCreateDatapool();
}


SDSDataPoolConfig* SDSDataPoolConfig::getInstance()
{
   if (!_DpSDS)
   {
      _DpSDS = new SDSDataPoolConfig();
   }
   return _DpSDS;
}


void SDSDataPoolConfig::setBestMatchPhonebook(const uint8 bestMatchPhonebook)
{
   _DpBestMatchPhonebook.vSetData(bestMatchPhonebook);
}


uint8 SDSDataPoolConfig::getBestMatchPhonebook()
{
   return _DpBestMatchPhonebook.tGetData();
}


void SDSDataPoolConfig::setBestMatchAudio(const uint8 bestMatchAudio)
{
   _DpBestMatchAudio.vSetData(bestMatchAudio);
}


uint8 SDSDataPoolConfig::getBestMatchAudio()
{
   return _DpBestMatchAudio.tGetData();
}


void SDSDataPoolConfig::setLanguageValue(const uint8 languageIndex)
{
   _dpSystemLanguage.vSetData(languageIndex);
}


uint8 SDSDataPoolConfig::getLanguageValue()
{
   return _dpSystemLanguage.tGetData();
}
