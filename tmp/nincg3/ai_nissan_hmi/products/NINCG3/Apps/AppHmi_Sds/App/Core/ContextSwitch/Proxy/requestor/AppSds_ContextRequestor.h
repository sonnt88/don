///////////////////////////////////////////////////////////
//  AppSds_ContextRequestor.h
//  Implementation of the Class AppSds_ContextRequestor
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(AppSds_ContextRequestor_h)
#define AppSds_ContextRequestor_h

/**
 * interface class for context proxies to provide contexts to requestors
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 5:44:42 PM
 */
#include "App/Core/SdsHallTypes.h"
#include "App/Core/Interfaces/ISdsContextRequestor.h"
#include "clContextRequestor.h"
#include "CourierTunnelService/CourierMessageReceiverStub.h"
#include "CgiExtensions/ListDataProviderDistributor.h"

class AppSds_ContextRequestor : public clContextRequestor, public App::Core::ISdsContextRequestor
{
   public:
      AppSds_ContextRequestor();
      virtual ~AppSds_ContextRequestor();

      void contextSwitch(const enApplicationId appId, const enContextId destContextId);
      void contextSwitchSelf();

   protected:
      bool onCourierMessage(const ContextSwitchOutReqMsg& msg);

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_SDS_COURIER_PAYLOAD_MODEL_COMP)
      ON_COURIER_MESSAGE(ContextSwitchOutReqMsg)
      COURIER_MSG_MAP_END()

   private:
      void requestContextSwitch(const unsigned int appId, const unsigned int destContextId);
};


#endif // !defined(EA_FAF598AF_B4F9_4bb1_91CE_ABBA6F93217B__INCLUDED_)
