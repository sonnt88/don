/****************************************************************************
 * Copyright (C) Robert Bosch Car Multimedia GmbH, 2015
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ***************************************************************************/

#ifndef SDSDATAPOOLCONFIG_H_
#define SDSDATAPOOLCONFIG_H_


#include "asf/core/Types.h"
#ifdef DP_DATAPOOL_ID
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_hmi_02_if.h"
#include "dp_tclAppHmi_Sds.h"
#endif


class SDSDataPoolConfig
{
   public:
      /**
       * Destructor of class DataPoolConfig
       * @return None
       */
      ~SDSDataPoolConfig()
      {
         delete _DpSDS;
      }

      void setBestMatchPhonebook(const uint8 bestMatchPhonebook);
      uint8 getBestMatchPhonebook();
      void setBestMatchAudio(const uint8 bestMatchAudio);
      uint8 getBestMatchAudio();
      void setLanguageValue(const uint8 languageIndex);
      uint8 getLanguageValue();
      /**
       * Singleton Class. Method to retrieve the instance of the class
       * @return Returns instance of the class
       */
      static SDSDataPoolConfig* getInstance();
   private:

      // Instance of the class
      static SDSDataPoolConfig* _DpSDS;

      SDSDataPoolConfig();
      SDSDataPoolConfig(const SDSDataPoolConfig&);
      SDSDataPoolConfig& operator=(const SDSDataPoolConfig&);

      //DP Elements
      dp_tclAppHmi_SdsBestMatchPhonebook _DpBestMatchPhonebook;
      dp_tclAppHmi_SdsBestMatchAudio _DpBestMatchAudio;
      dp_tclAppHmi_SdsSystemLanguage _dpSystemLanguage;
};


#endif /* SDSDATAPOOLCONFIG_H_ */
