/*
 * ListHandler.h
 *
 *  Created on:
 *      Author:
 */

#ifndef ListHandler_h
#define ListHandler_h

#include "App/Core/Interfaces/IListHandler.h"
#include "App/Core/SdsHallTypes.h"
#include "App/Core/ContextSwitch/Proxy/provider/AppSds_ContextProvider.h"
#include "Common/ListHandler/ListRegistry.h"
#include "CourierTunnelService/CourierMessageReceiverStub.h"


namespace App {
namespace Core {

class GuiUpdater;
class VRSettingsHandler;
class SdsAdapterRequestor;

class ListHandler :
   public ListImplementation,
   public IListHandler
{
   public:
      ListHandler(GuiUpdater* guiUpdater, VRSettingsHandler* vrSettingsHandler, SdsAdapterRequestor* sdsAdapterRequestor);
      virtual ~ListHandler();

      void setShowScreenLayout(enSdsScreenLayoutType screenLayout);
      void updateSubCommandList();
      enSdsScreenLayoutType getCurrentLayout();
      std::vector <SelectableListItem>& getVectCmdString();
      std::vector <std::string>& getVectSubCmdString();
      std::vector <std::string>& getVectDescString();
      std::vector <std::string>& getVectDistString();
      std::vector <std::string>& getVectPriceAgeString();
      std::vector <std::string>& getVectPriceString();
      std::vector <std::string>& getVectDirectionString();

   private :

      tSharedPtrDataProvider getListDataProvider(const ListDataInfo& _ListInfo);
      tSharedPtrDataProvider getSDSList();
      tSharedPtrDataProvider getFuelPriceList();
      void initializeMembers();
      void initializeListTextFields();
      void initializeSubCommandList();
      void setCurrentLayout(enSdsScreenLayoutType screenLayout);
      void showScreenLayout(enSdsScreenLayoutType screenLayout);

      ////////////////////////////////////////////// Courier Message Mapping with Traces classes ///////////////////////////////////////////////////////////////////////////////////
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_SDS_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_DELEGATE_TO_CLASS(ListImplementation)
      COURIER_MSG_MAP_DELEGATE_END()
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      //////////The below onCourierMessage funtions are the various action perform & model subscribed courier messages from SM ////////////////////////////////////////////////////
      bool onCourierMessage(const ButtonReactionMsg& oMsg);
      /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      std::vector <SelectableListItem> vecCommandString;
      std::vector <std::string> vecDescString;
      std::vector <std::string> vecSubCommandString;
      std::vector <std::string> vecDistanceString;
      std::vector <std::string> vecPriceAgeString;
      std::vector <std::string> vecPriceString;
      std::vector <std::string> vecDirectionString;
      enSdsScreenLayoutType currentLayout;

      GuiUpdater* _pGuiUpdater;
      VRSettingsHandler* _pVRSettingsHandler;
      SdsAdapterRequestor* _pSdsAdapterRequestor;
};


}//namespace App
}//namespace Core
#endif /* ListHandler_h */
