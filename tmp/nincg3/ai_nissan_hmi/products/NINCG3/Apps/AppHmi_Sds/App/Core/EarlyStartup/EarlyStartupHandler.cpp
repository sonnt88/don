/*
 * EarlyStartupHandler.cpp
 *
 *  Created on: Apr 12, 2016
 *      Author: bee4kor
 */

#include "App/Core/EarlyStartup/EarlyStartupHandler.h"
#include "App/Core/Interfaces/IHMIModelComponent.h"
#include "App/Core/Interfaces/ISdsContextRequestor.h"
#include "App/Core/ListHandler/ListHandler.h"
#include "App/Core/SdsDefines.h"
#include "hmi_trace_if.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SDS_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SDS
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SDS_"
#define ETG_I_FILE_PREFIX                 App::Core::EarlyStartupHandler::
#include "trcGenProj/Header/EarlyStartupHandler.cpp.trc.h"
#endif

using namespace sds_gui_fi::SdsGuiService;

namespace App {
namespace Core {

EarlyStartupHandler::EarlyStartupHandler(
   ISdsContextRequestor* pSdsContextRequestor,
   const ::boost::shared_ptr<sds_gui_fi::SdsGuiService::SdsGuiServiceProxy>& sdsGuiServiceProxy,
   const ::boost::shared_ptr<sds_gui_fi::PopUpService::PopUpServiceProxy>& popUpServiceProxy,
   const ::boost::shared_ptr<sds_gui_fi::SettingsService::SettingsServiceProxy>& settingsServiceProxy,
   ListHandler* pListHandler)
   : _pSdsContextRequestor(pSdsContextRequestor)
   , _popUpServiceProxy(popUpServiceProxy)
   , _sdsGuiServiceProxy(sdsGuiServiceProxy)
   , _settingsServiceProxy(settingsServiceProxy)
   , _pListHandler(pListHandler)
   , _isSdsMiddlewareAvailable(false)
   , _earlyDestContext(SDS_CONTEXT_START)
   , _earlySavedContext(EARLY_CONTEXT_NOT_REQUESTED)
{
}


EarlyStartupHandler::~EarlyStartupHandler()
{
   PURGE(_pSdsContextRequestor);
   PURGE(_pListHandler);
}


enEarlyContextState EarlyStartupHandler::startEarlySdsContext(unsigned int srcContext, unsigned int destContext)
{
   enEarlyContextState earlyContextState = handleEarlyVRShortcuts(srcContext, destContext);
   if (earlyContextState != EARLY_CONTEXTSTATE_INVALIDATE)
   {
      return earlyContextState;
   }
   else
   {
      earlyContextState = handleEarlySettingsContexts(srcContext, destContext);
      if (earlyContextState != EARLY_CONTEXTSTATE_INVALIDATE)
      {
         return earlyContextState;
      }

      else
      {
         earlyContextState = handleEarlyGeneralContexts(srcContext, destContext);
         if (earlyContextState != EARLY_CONTEXTSTATE_INVALIDATE)
         {
            return earlyContextState;
         }
         else
         {
            return EARLY_CONTEXTSTATE_REJECT;
         }
      }
   }
}


enEarlyContextState EarlyStartupHandler::handleEarlyVRShortcuts(unsigned int _srcContext, unsigned int _destContext)
{
   ETG_TRACE_USR4(("EarlyStartupHandler::handleEarlyVRShortcuts"));

   if (_srcContext >= HOMESCREEN_CONTEXT_START && _srcContext <= HOMESCREEN_CONTEXT_END)
   {
      if (_destContext == SDS_CONTEXT_SETTINGS_MAIN)
      {
         requestEarlyVRSettingsContext();
      }
      else
      {
         saveEarlyContext(EARLY_CONTEXT_VR_SHORTCUTS, _destContext);
         initiateEarlySdsContext();
      }
      return EARLY_CONTEXTSTATE_COMPLETE;
   }

   return EARLY_CONTEXTSTATE_INVALIDATE;
}


enEarlyContextState EarlyStartupHandler::handleEarlySettingsContexts(unsigned int _srcContext, unsigned int _destContext)
{
   ETG_TRACE_USR4(("EarlyStartupHandler::handleEarlySettingsContexts"));

   if (_srcContext == DEFAULT_CONTEXT_BACK && _destContext == SDS_CONTEXT_SETTINGS_MAIN)
   {
      requestEarlyVRSettingsContext();
      return EARLY_CONTEXTSTATE_COMPLETE;
   }
   return EARLY_CONTEXTSTATE_INVALIDATE;
}


enEarlyContextState EarlyStartupHandler::handleEarlyGeneralContexts(unsigned int _srcContext, unsigned int _destContext)
{
   ETG_TRACE_USR4(("EarlyStartupHandler::handleGeneralContexts"));

   if ((_srcContext == 0) && (_destContext == SDS_CONTEXT_ROOT))
   {
      ETG_TRACE_USR4(("EarlyStartupHandler::handleEarlyGeneralContexts - SelfContext Request Accepted"));
      return EARLY_CONTEXTSTATE_ACCEPT;
   }

   if (_destContext == SDS_CONTEXT_ROOT)
   {
      ETG_TRACE_USR4(("EarlyStartupHandler::handleEarlyGeneralContexts - PTT Press Request"));
      saveEarlyContext(EARLY_CONTEXT_PTT_SHORTPRESS, _destContext);
      initiateEarlySdsContext();
      return EARLY_CONTEXTSTATE_COMPLETE;
   }

   return EARLY_CONTEXTSTATE_INVALIDATE;
}


void EarlyStartupHandler::requestEarlyVRSettingsContext()
{
   ETG_TRACE_USR4(("EarlyStartupHandler::requestEarlyVRSettingsContext()"));
   showVRSettingsScreen();
   // initiateEarlySdsContext() is not needed as VR_Settings only requires Sds Adapter to be Available
}


void EarlyStartupHandler::onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _popUpServiceProxy)
   {
      _popUpServiceProxy->sendSdsStatusRegister(*this);
      _popUpServiceProxy->sendSdsStatusGet(*this);
   }
}


void EarlyStartupHandler::onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _popUpServiceProxy)
   {
      _popUpServiceProxy->sendSdsStatusDeregisterAll();
   }
}


bool EarlyStartupHandler::isSdsMiddlewareAvailable()
{
   return _isSdsMiddlewareAvailable;
}


bool EarlyStartupHandler::onCourierMessage(const CloseEarlyStartSession& /*msg*/)
{
   _sdsGuiServiceProxy->sendStopEarlyHandlingRequest(*this);
   return true;
}


void EarlyStartupHandler::handleEarlySavedContexts()
{
   switch (_earlySavedContext)
   {
      case EARLY_CONTEXT_VR_SHORTCUTS:
      {
         _sdsGuiServiceProxy->sendStartSessionContextRequest(*this, mapGui2AdapterShortcutContext(_earlyDestContext));
      }
      break;
      case EARLY_CONTEXT_PTT_SHORTPRESS:
      {
         _sdsGuiServiceProxy->sendPttPressRequest(*this, KeyState__KEY_HK_MFL_PTT_SHORT);
      }
      break;
      case EARLY_CONTEXT_VR_SETTINGS:
      {
         showVRSettingsScreen();
      }
      break;
      default:
      {
         _earlySavedContext = EARLY_CONTEXT_NOT_REQUESTED;
      }
   }
   _earlySavedContext = EARLY_CONTEXT_NOT_REQUESTED;
}


void EarlyStartupHandler::saveEarlyContext(enEarlySavedContext earlyContext, unsigned int earlyDestContext)
{
   _earlySavedContext = earlyContext;
   _earlyDestContext = earlyDestContext;
}


sds_gui_fi::SdsGuiService::ContextType EarlyStartupHandler::mapGui2AdapterShortcutContext(unsigned int _guiContext)
{
   switch (_guiContext)
   {
      case SDS_CONTEXT_HOMESCREEN_CALL:
         return ContextType__SDS_CONTEXT_CALL;

      case SDS_CONTEXT_HOMESCREEN_DIALNUM:
         return ContextType__SDS_CONTEXT_DIALNUM;

      case SDS_CONTEXT_HOMESCREEN_CALLHIST:
         return ContextType__SDS_CONTEXT_CALLHIST;

      case SDS_CONTEXT_HOMESCREEN_QUICKDIAL:
         return ContextType__SDS_CONTEXT_QUICKDIAL;

      case SDS_CONTEXT_HOMESCREEN_READTEXT:
         return ContextType__SDS_CONTEXT_READTEXT;

      case SDS_CONTEXT_HOMESCREEN_SENDTEXT:
         return ContextType__SDS_CONTEXT_SENDTEXT;

      case SDS_CONTEXT_HOMESCREEN_STREETADDR:
         return ContextType__SDS_CONTEXT_STREETADDR;

      case SDS_CONTEXT_HOMESCREEN_POI:
         return ContextType__SDS_CONTEXT_POI;

      case SDS_CONTEXT_HOMESCREEN_POICAT:
         return ContextType__SDS_CONTEXT_POICAT;

      case SDS_CONTEXT_HOMESCREEN_INTERSECTION:
         return ContextType__SDS_CONTEXT_INTERSECTION;

      case SDS_CONTEXT_HOMESCREEN_CITYCENTER:
         return ContextType__SDS_CONTEXT_CITYCENTER;

      case SDS_CONTEXT_HOMESCREEN_PLAYALBUM:
         return ContextType__SDS_CONTEXT_PLAYALBUM;

      case SDS_CONTEXT_HOMESCREEN_PLAYARTIST:
         return ContextType__SDS_CONTEXT_PLAYARTIST;

      case SDS_CONTEXT_HOMESCREEN_PLAYSONG:
         return ContextType__SDS_CONTEXT_PLAYSONG;

      case SDS_CONTEXT_HOMESCREEN_PLAYLIST:
         return ContextType__SDS_CONTEXT_PLAYLIST;

      default:
         return ContextType__SDS_CONTEXT_CALL;
   }
}


void EarlyStartupHandler::onStartSessionContextError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StartSessionContextError >& /*error*/)
{
}


void EarlyStartupHandler::onStartSessionContextResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StartSessionContextResponse >& /*response*/)
{
}


void EarlyStartupHandler::onSdsStatusError(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& /* proxy */, const ::boost::shared_ptr< sds_gui_fi::PopUpService::SdsStatusError >& /* error */)
{
}


void EarlyStartupHandler::onPttPressError(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< PttPressError >& /*error*/)
{
}


void EarlyStartupHandler::onPttPressResponse(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< PttPressResponse >& /*response*/)
{
}


void EarlyStartupHandler::onStartEarlyHandlingError(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< StartEarlyHandlingError >& /*error*/)
{
}


void EarlyStartupHandler::onStartEarlyHandlingResponse(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< StartEarlyHandlingResponse >& /*response*/)
{
}


void EarlyStartupHandler::onStopEarlyHandlingError(const ::boost::shared_ptr< SdsGuiServiceProxy >&  /*proxy*/, const ::boost::shared_ptr< StopEarlyHandlingError >& /*error*/)
{
}


void EarlyStartupHandler::onStopEarlyHandlingResponse(const ::boost::shared_ptr< SdsGuiServiceProxy >&  /*proxy*/, const ::boost::shared_ptr< StopEarlyHandlingResponse >& /*response*/)
{
}


void EarlyStartupHandler::onSdsStatusUpdate(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& /* proxy */, const ::boost::shared_ptr< sds_gui_fi::PopUpService::SdsStatusUpdate >& update)
{
   ETG_TRACE_USR4(("EarlyStartupHandler::onSdsStatusUpdate = %d", update->getSdsStatus()));

   setInitialSdsMiddlewareAvailability(update);
   if ((_isSdsMiddlewareAvailable) && (_earlySavedContext != EARLY_CONTEXT_NOT_REQUESTED))
   {
      handleEarlySavedContexts();
   }
}


void EarlyStartupHandler::setInitialSdsMiddlewareAvailability(const ::boost::shared_ptr< sds_gui_fi::PopUpService::SdsStatusUpdate >& update)
{
   if ((!_isSdsMiddlewareAvailable) && (update->getSdsStatus() == sds_gui_fi::PopUpService::SpeechInputState__IDLE))
   {
      _isSdsMiddlewareAvailable = true;
   }
}


bool EarlyStartupHandler::isSdsAdapterAvailable()
{
   return (_sdsGuiServiceProxy->isAvailable() && _popUpServiceProxy->isAvailable() && _settingsServiceProxy->isAvailable());
}


void EarlyStartupHandler::showVRSettingsScreen()
{
   _pListHandler->setShowScreenLayout(SDS_Settings_Main);
   _pSdsContextRequestor->contextSwitchSelf();
}


void EarlyStartupHandler::initiateEarlySdsContext()
{
   ETG_TRACE_USR4(("EARLY_START_INIT"));

   if (_pSdsContextRequestor)
   {
      _pSdsContextRequestor->contextSwitchSelf();
   }
   _sdsGuiServiceProxy->sendStartEarlyHandlingRequest(*this);
}


}
}
