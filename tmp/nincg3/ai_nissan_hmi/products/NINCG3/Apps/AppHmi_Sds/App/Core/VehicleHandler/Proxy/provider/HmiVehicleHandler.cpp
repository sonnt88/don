/**
 *  @file   <HmiVehicleHandler.cpp>
 *  @author <ECV2> - <A-IVI>
 *  @copyright (c) <2014> Robert Bosch Car Multimedia GmbH
 *  @addtogroup <Apphmi_Sds>
 */

#include <App/Core/Observers/SmsHeaderVisibilityObserver.h>
#include <App/Core/VehicleHandler/Proxy/provider/HmiVehicleHandler.h>
#include "App/Core/SdsDefines.h"

namespace App {
namespace Core {


#include "hmi_trace_if.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SDS_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SDS
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SDS_"
#define ETG_I_FILE_PREFIX                 App::Core::SdsHmiVehicleHandler::
#include "trcGenProj/Header/HmiVehicleHandler.cpp.trc.h"
#endif


using namespace VEHICLE_MAIN_FI;


HmiVehicleHandler::~HmiVehicleHandler()
{
}


HmiVehicleHandler::HmiVehicleHandler()
{
   _vehicleProxy = VEHICLE_MAIN_FIProxy::createProxy("vehicleMainFiPort", *this);
}


void HmiVehicleHandler::onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   _vehicleProxyAvailable = proxy->isAvailable();
}


void HmiVehicleHandler::onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   _vehicleProxyAvailable = proxy->isAvailable();
}


void HmiVehicleHandler::registerProperties()
{
   ETG_TRACE_USR1(("register Vehicle Properties on SDS Session Active"));
   _vehicleProxy->sendSpeedUpReg(*this);
}


void HmiVehicleHandler::deregisterProperties() const
{
   ETG_TRACE_USR4(("deregister Vehicle Properties on SDS Session End"));
   _vehicleProxy->sendSpeedRelUpRegAll();
}


void HmiVehicleHandler::onSDSSessionStarted()
{
   registerProperties();
}


void HmiVehicleHandler::onSDSSessionEnd()
{
   deregisterProperties();
}


void HmiVehicleHandler::onSpeedError(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& /*proxy*/, const ::boost::shared_ptr< SpeedError >&/*error*/)
{
}


void HmiVehicleHandler::onSpeedStatus(const ::boost::shared_ptr< VEHICLE_MAIN_FIProxy >& /*proxy*/, const ::boost::shared_ptr<  SpeedStatus >& status)
{
   uint16 vehicleSpeedinKmph = status->getSpeedValue();
   bool vehicleMovingSpeed = true;
   if (vehicleSpeedinKmph < MAX_VEHICLE_SPEED_SMS_HEADER_VISIBILITY)
   {
      uint32 vehicleSpeed = vehicleSpeedinKmph / KMPH_TO_MILES_CONVERSION_VALUE;
      vehicleMovingSpeed = ((vehicleSpeed > MAX_VEHICLE_SPEED_IN_MILES) ? true : false);
      ETG_TRACE_USR1(("Current vehicleSpeed in miles per hour :%d", vehicleSpeed));
      vNotifySmsHeaderVisibilityToObservers(vehicleMovingSpeed);
   }
   else
   {
      ETG_TRACE_USR4(("Vehicle speed received from Vehicle is invalid"));
      vNotifySmsHeaderVisibilityToObservers(vehicleMovingSpeed);
   }
}


void HmiVehicleHandler:: vRegisterObservers(SmsHeaderVisibilityObserver* pSmsHeaderVisibilityObserver)
{
   if (pSmsHeaderVisibilityObserver != NULL)
   {
      _observers.push_back(pSmsHeaderVisibilityObserver);
   }
}


void HmiVehicleHandler::vNotifySmsHeaderVisibilityToObservers(bool vehicleMovingSpeed)
{
   std::vector<SmsHeaderVisibilityObserver*>::iterator iter = _observers.begin();
   while (iter != _observers.end())
   {
      if (*iter != NULL)
      {
         (*iter)->vOnSmsHeaderVisibilityChanged(vehicleMovingSpeed);
      }
      ++iter;
   }
}


}// namespace Core
}// namespace App
