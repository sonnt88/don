/*
 * ISdsContextProvider.h
 *
 *  Created on: Feb 5, 2016
 *      Author: bee4kor
 */

#ifndef ISdsContextProvider_h
#define ISdsContextProvider_h

namespace App {
namespace Core {

class ISdsContextProvider
{
   public:
      virtual ~ISdsContextProvider() {};
      virtual void contextSwitchBack() = 0;
      virtual void contextSwitchComplete() = 0;
};


}
}


#endif /* ISdsContextProvider_h */
