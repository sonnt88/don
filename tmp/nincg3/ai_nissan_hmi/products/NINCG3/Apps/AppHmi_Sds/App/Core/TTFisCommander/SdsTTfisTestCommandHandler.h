/*
 * SdsTTfisTestCommandHandler.h
 *
 *  Created on: Jan 26, 2016
 *      Author: bee4kor
 */

#ifndef SdsTTfisTestCommandHandler_h
#define SdsTTfisTestCommandHandler_h

#include "App/Core/SdsHallTypes.h"
#include "CgiExtensions/ListDataProviderDistributor.h"

namespace App {
namespace Core {

class ISdsAdapterRequestor;
class ISdsContextRequestor;
class IVRSettingsHandler;
class IListHandler;
class GuiUpdater;


class SdsTTfisTestCommandHandler
{
   public:
      SdsTTfisTestCommandHandler(ISdsAdapterRequestor* pAdapterRequestor,
                                 ISdsContextRequestor* pSdsContextRequestor,
                                 GuiUpdater* pGuiUpdater,
                                 IVRSettingsHandler* pVRSettingsHandler,
                                 IListHandler* pListHandler);
      virtual ~SdsTTfisTestCommandHandler();
      static void ttfisTestCommand(unsigned int param);
      void testCommandMethod(unsigned int param);

   private:
      ISdsAdapterRequestor* _pAdapterRequestor;
      ISdsContextRequestor* _pSdsContextRequestor;
      GuiUpdater* _pGuiUpdater;
      IVRSettingsHandler* _pVRSettingshandler;
      IListHandler* _pListHandler;

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_SDS_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_MSG_MAP_END()
};


}
}


#endif /* SdsTTfisTestCommandHandler_h */
