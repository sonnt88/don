/*
 * IHMIModelComponent.h
 *
 *  Created on: Apr 12, 2016
 *      Author: bee4kor
 */

#ifndef IHMIModelComponent_h
#define IHMIModelComponent_h

#include "App/Core/Observers/ActiveApplicationModeObserver.h"


namespace App {
namespace Core {

class IHMIModelComponent
{
   public:
      virtual ~IHMIModelComponent() {};
      virtual void registerSdsApplicationModeObserver(ActiveApplicationModeObserver* pActiveApplicationModeObserver) = 0 ;
};


}
}


#endif /* IHMIModelComponent_h */
