/*
 * ISdsAdapterRequestor.h
 *
 *  Created on: Feb 2, 2016
 *      Author: bee4kor
 */

#ifndef ISdsAdapterRequestor_h
#define ISdsAdapterRequestor_h

#include <string>


namespace App {
namespace Core {


class ISdsAdapterRequestor
{
   public:
      virtual ~ISdsAdapterRequestor() {};

      virtual void sendPttShortPress() = 0;
      virtual void backKeyPressed() = 0;
      virtual unsigned int focusMoved(unsigned int _guiFocusIndex) = 0;
      virtual unsigned int listItemSelected(unsigned int _guiSelectedListItemIndex) = 0;
      virtual unsigned int requestNextPage() = 0;
      virtual unsigned int requestPrevPage() = 0;
      virtual unsigned int sendShortcutRequest(unsigned int _shortcutType) = 0;
      virtual unsigned int sendTestModeSentence(std::string _textSentence) = 0;
      virtual void restoreFactorySDSSettings() = 0;
      virtual void sendStopSession() = 0;
      virtual void sendAbortSession() = 0;
      virtual void sendPauseSession() = 0;
      virtual void sendResumeSession() = 0;
      virtual void setHighPriorityAppStatus(bool highPriorityAppActive) = 0;
};


}
}


#endif /* ISdsAdapterRequestor_h */
