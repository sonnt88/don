/*
 * HmiDataServiceClientHandler.cpp
 *
 *  Created on: Jan 4, 2016
 *      Author: vee4kor
 */

#include "HmiDataServiceClientHandler.h"
#include "App/Core/Interfaces/ISdsAdapterRequestor.h"
#include "App/Core/Interfaces/ISdsContextRequestor.h"
#include "App/Core/SdsDefines.h"
#include "hmi_trace_if.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SDS_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SDS
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SDS_"
#define ETG_I_FILE_PREFIX                 App::Core::HmiDataServiceClientHandler::
#include "trcGenProj/Header/HmiDataServiceClientHandler.cpp.trc.h"
#endif


namespace App {
namespace Core {

#define ARRAY_SIZE(array)	(sizeof (array) / sizeof (array)[0])

static const enApplicationMode highPriorityApps[] = {APP_MODE_CAMERA, APP_MODE_SECURE, APP_MODE_AVM, APP_MODE_ECALL, APP_MODE_DOWNLOAD, APP_MODE_APPS_INCOMING_CALL, APP_MODE_PHONE_INCOMING_CALL, APP_MODE_OPERATOR, APP_MODE_SONAR };

/**************************************************************************//**
*
******************************************************************************/
HmiDataServiceClientHandler::HmiDataServiceClientHandler(ISdsAdapterRequestor* poISdsAdapterRequestor,
      const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::HmiDataProxy >& hmiDataProxy,
      const ::boost::shared_ptr<sds_gui_fi::SdsGuiService::SdsGuiServiceProxy>& sdsGuiServiceProxy,
      const ::boost::shared_ptr<sds_gui_fi::PopUpService::PopUpServiceProxy>& popUpServiceProxy)
   : _pSdsAdapterRequestor(poISdsAdapterRequestor)
   , _hmiDataProxy(hmiDataProxy)
   , _sdsGuiServiceProxy(sdsGuiServiceProxy)
   , _popUpServiceProxy(popUpServiceProxy)
   , _sdsStatus(sds_gui_fi::PopUpService::SpeechInputState__UNKNOWN)
{
   setActiveApplicationmode(false);
}


/**************************************************************************//**
*
******************************************************************************/
HmiDataServiceClientHandler::~HmiDataServiceClientHandler()
{
   PURGE(_pSdsAdapterRequestor);
   PURGE(_pSdsContextRequestor);
}


/**************************************************************************//**
*
******************************************************************************/
void HmiDataServiceClientHandler::onVisibleApplicationModeUpdate(const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::HmiDataProxy >& /*proxy*/,
      const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::VisibleApplicationModeUpdate >& update)
{
   ETG_TRACE_USR4(("HmiDataServiceClientHandler:onVisibleApplicationModeUpdate %d", update->getVisibleApplicationMode()));

   handleVisibleAppmode(update->getVisibleApplicationMode());
}


/**************************************************************************//**
*
******************************************************************************/
void HmiDataServiceClientHandler::onVisibleApplicationModeError(const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::HmiDataProxy >& /*proxy*/,
      const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::VisibleApplicationModeError >& /* error*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void HmiDataServiceClientHandler::onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   ETG_TRACE_USR1(("HmiDataServiceClientHandler::onAvailable"));

   if (proxy == _hmiDataProxy)
   {
      _hmiDataProxy->sendVisibleApplicationModeRegister(*this);
   }
   if (proxy == _popUpServiceProxy)
   {
      _popUpServiceProxy->sendSdsStatusRegister(*this);
   }
   if (proxy == _sdsGuiServiceProxy)
   {
      _sdsGuiServiceProxy->sendEventRegister(*this);
   }
}


/**************************************************************************//**
*
******************************************************************************/
void HmiDataServiceClientHandler::onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange*/)
{
   ETG_TRACE_USR4(("HmiDataServiceClientHandler::onUnavailable"));

   if (proxy == _hmiDataProxy)
   {
      _hmiDataProxy->sendVisibleApplicationModeDeregisterAll();
   }
   if (proxy == _popUpServiceProxy)
   {
      _popUpServiceProxy->sendSdsStatusDeregisterAll();
   }
   if (proxy == _sdsGuiServiceProxy)
   {
      _sdsGuiServiceProxy->sendEventDeregisterAll();
   }
}


/**************************************************************************//**
*
******************************************************************************/
void HmiDataServiceClientHandler::setActiveApplicationmode(bool isVRActive)
{
   if (isVRActive)
   {
      _hmiDataProxy->sendSetApplicationModeRequest(*this, APPID_APPHMI_SDS, APP_MODE_VR);
   }
   else
   {
      _hmiDataProxy->sendSetApplicationModeRequest(*this, APPID_APPHMI_SDS, APP_MODE_UTILITY);
   }
}


/**************************************************************************//**
*
******************************************************************************/
void HmiDataServiceClientHandler::onSetApplicationModeError(const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::HmiDataProxy >& /*proxy */,
      const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::SetApplicationModeError >& /* error */)
{
}


/**************************************************************************//**
*
******************************************************************************/
void HmiDataServiceClientHandler::onSetApplicationModeResponse(const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::HmiDataProxy >& /* proxy */,
      const ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::SetApplicationModeResponse >& /* response */)
{
}


/**************************************************************************//**
*
******************************************************************************/
void HmiDataServiceClientHandler::handleVisibleAppmode(unsigned int visibleAppmode)
{
   bool highPriorityAppStatus = isHighPriorityAppActive(visibleAppmode);
   if (_pSdsAdapterRequestor)
   {
      _pSdsAdapterRequestor->setHighPriorityAppStatus(highPriorityAppStatus);

      if (highPriorityAppStatus && isSDSActive())
      {
         _pSdsAdapterRequestor->sendPauseSession();
      }
      else if ((visibleAppmode == APP_MODE_SPCXVR) && isSDSActive())
      {
         _pSdsAdapterRequestor->sendAbortSession();
      }
      if ((_sdsStatus == sds_gui_fi::PopUpService::SpeechInputState__PAUSE) && (highPriorityAppStatus == false) && (APP_MODE_VR != visibleAppmode))
      {
         _pSdsAdapterRequestor->sendResumeSession();
      }
   }
}


/**************************************************************************//**
*
******************************************************************************/
bool HmiDataServiceClientHandler::isHighPriorityAppActive(unsigned int visibleAppmode)
{
   enApplicationMode applicationmode = static_cast<enApplicationMode>(visibleAppmode);
   bool highPriorityAppStatus = false;

   for (size_t i = 0; i < ARRAY_SIZE(highPriorityApps); i++)
   {
      if (highPriorityApps[i] == applicationmode)
      {
         highPriorityAppStatus = true;
      }
   }
   ETG_TRACE_USR4(("isHighPriorityAppActive %d", highPriorityAppStatus));
   return highPriorityAppStatus;
}


/**************************************************************************//**
*
******************************************************************************/
bool HmiDataServiceClientHandler::isSDSActive()
{
   bool sdsActiveStatus = false;
   if ((_sdsStatus == sds_gui_fi::PopUpService::SpeechInputState__LISTENING) || (_sdsStatus == sds_gui_fi::PopUpService::SpeechInputState__ACTIVE) || (_sdsStatus == sds_gui_fi::PopUpService::SpeechInputState__DIALOGOPEN))
   {
      sdsActiveStatus = true;
   }
   ETG_TRACE_USR4(("isSDSActive %d", sdsActiveStatus));
   return sdsActiveStatus;
}


/**************************************************************************//**
*
******************************************************************************/
void HmiDataServiceClientHandler::onSdsStatusError(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& /* proxy */, const ::boost::shared_ptr< sds_gui_fi::PopUpService::SdsStatusError >& /* error */)
{
}


/**************************************************************************//**
*
******************************************************************************/
void HmiDataServiceClientHandler::onSdsStatusUpdate(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& /* proxy */, const ::boost::shared_ptr< sds_gui_fi::PopUpService::SdsStatusUpdate >& update)
{
   _sdsStatus = update->getSdsStatus();
   ETG_TRACE_USR4(("sdsStatus %d", _sdsStatus));
}


/**************************************************************************//**
*
******************************************************************************/
void HmiDataServiceClientHandler::onEventError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::EventError >&/* error*/)
{
}


/**************************************************************************//**
*
******************************************************************************/
void HmiDataServiceClientHandler::onEventSignal(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::EventSignal >& signal)
{
   if (signal->getGuiEvent() == sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SDS_START_SESSION)
   {
      setActiveApplicationmode(true);
   }
   else if (signal->getGuiEvent() == sds_gui_fi::SdsGuiService::Event__SPEECH_DIALOG_SDS_STOP_SESSION)
   {
      setActiveApplicationmode(false);
   }
}


/**************************************************************************//**
*
******************************************************************************/
void HmiDataServiceClientHandler::setContextRequestor(ISdsContextRequestor* poSdsContextRequestor)
{
   _pSdsContextRequestor = poSdsContextRequestor;
}


} // namespace App
} // namespace Core
