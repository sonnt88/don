/*
 * SdsEventHandler.h
 *
 *  Created on: Apr 15, 2016
 *      Author: raa8hi
 */

#ifndef SdsEventHandler_h
#define SdsEventHandler_h

#include "sds_gui_fi/SdsGuiServiceProxy.h"
#include "CgiExtensions/ListDataProviderDistributor.h"


namespace App {
namespace Core {

class IListHandler;
class ISdsContextRequestor;


class SdsEventHandler
   : public asf::core::ServiceAvailableIF
   , public sds_gui_fi::SdsGuiService::EventCallbackIF
{
   public:
      SdsEventHandler(::boost::shared_ptr<sds_gui_fi::SdsGuiService::SdsGuiServiceProxy> sdsGuiServiceProxy,
                      ISdsContextRequestor* pSdsContextRequestor,
                      IListHandler* pListHandler);
      virtual ~SdsEventHandler();

      virtual void onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                               const asf::core::ServiceStateChange& stateChange);

      virtual void onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                                 const asf::core::ServiceStateChange& stateChange);

      virtual void onEventError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy,
                                const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::EventError >& error);

      virtual void onEventSignal(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy,
                                 const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::EventSignal >& signal);

      bool getBackTransitionFlag();

   private:

      bool handleGeneralEvents(unsigned int _event);
      bool handleSourceChangeEvents(unsigned int _event);
      bool handleSXMEvents(unsigned int _event);
      bool handleExternalSystemEvents(unsigned int _event);
      bool handleSDSPopups(unsigned int _event);
      bool handleNavigationEvents(unsigned int _event);

      void showScreenLayout(enSdsScreenLayoutType screenLayout);
      void showSDSPopup(unsigned int popupid);
      void playBeep();
      void contextSwitch(const enApplicationId appId, const enContextId destContextId);

      ::boost::shared_ptr<sds_gui_fi::SdsGuiService::SdsGuiServiceProxy> _sdsGuiServiceProxy;
      ISdsContextRequestor* _pSdsContextRequestor;
      IListHandler* _pListHandler;

      bool _backTransitflag;

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_SDS_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_MSG_MAP_END()
};


} // namespace Core
} // namespace App

#endif /* SdsEventHandler_h */
