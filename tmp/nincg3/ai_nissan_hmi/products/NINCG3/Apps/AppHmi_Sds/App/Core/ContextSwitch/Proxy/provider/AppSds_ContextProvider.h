///////////////////////////////////////////////////////////
//  AppSds_ContextProvider.h
//  Implementation of the Class AppSds_ContextProvider
//  Created on:      13-Jul-2015 5:44:42 PM
//  Original author: pad1cob
///////////////////////////////////////////////////////////

#if !defined(AppSds_ContextProvider_h)
#define AppSds_ContextProvider_h

/**
 * interface class for context proxies to provide contexts to requestors
 * @author pad1cob
 * @version 1.0
 * @created 13-Jul-2015 5:44:42 PM
 */

#include "App/Core/Interfaces/ISdsContextProvider.h"
#include "App/Core/SdsHallTypes.h"
#include "clContextProvider.h"
#include "CgiExtensions/ListDataProviderDistributor.h"


static const clContext aSupportedContexts[] =
{
#define SUPPORTED_CONTEXTS(context, contextType, priority, surfaceId,  viewName, sourceId)                  { context, contextType, priority, surfaceId,  viewName, sourceId },
#include "contexts.dat"
#undef SUPPORTED_CONTEXTS
};


namespace App {
namespace Core {
class ISdsAdapterRequestor;
class ISdsContextRequestor;
class IHmiDataServiceClientHandler;
class ListHandler;
class IEarlyStartupHandler;
}


}

class AppSds_ContextProvider
   : public clContextProvider
   , public App::Core::ISdsContextProvider
{
   public:

      AppSds_ContextProvider(App::Core::ISdsAdapterRequestor* poSdsAdapterRequestor, App::Core::IEarlyStartupHandler* pEarlyStartupHandler, App::Core::ListHandler* pListHandler);
      virtual ~AppSds_ContextProvider();

      void vSendContextTable()
      {
         ::std::vector<clContext> availableContexts;
         const int mapSize = sizeof aSupportedContexts / sizeof(clContext);
         for (int u32Index = 0; u32Index < mapSize; u32Index++)
         {
            availableContexts.push_back(aSupportedContexts[u32Index]);
         }
         storeAvailableContexts(availableContexts);
      }

      virtual bool bOnNewContext(uint32 sourceContextId, uint32 targetContextId);
      virtual void vOnBackRequestResponse(ContextState enState);

      void setContextRequestor(App::Core::ISdsContextRequestor* poSdsContextRequestor);

      ///////////////// ISdsContextProvider Interface Methods ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      void contextSwitchBack();
      void contextSwitchComplete();
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      bool onCourierMessage(const ContextSwitchInResMsg& msg);
      bool onCourierMessage(const ActiveRenderedView&);
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_SDS_COURIER_PAYLOAD_MODEL_COMP)
      ON_COURIER_MESSAGE(ContextSwitchInResMsg)
      ON_COURIER_MESSAGE(ActiveRenderedView)
      COURIER_MSG_MAP_END()
   private:
      App::Core::ISdsContextRequestor* _pSdsContextRequestor;
      App::Core::ISdsAdapterRequestor* _pSdsAdapterRequestor;
      App::Core::IEarlyStartupHandler* _pEarlyStartupHandler;
      App::Core::ListHandler* _pListHandler;

      /**
       * handleGeneralContexts - Handle Method for User PTT requests routed via PHONE HMI
       */
      bool handleGeneralContexts(unsigned int _srcContext, unsigned int _destContext);
      /**
      /**
       * handleSettingsContexts - Handle Methods for User VR_SETTINGS requests requested from SYSTEM_SETTINGS Application
       */
      bool handleSettingsContexts(unsigned int _srcContext, unsigned int _destContext);
      /**
       * handleShortcutContexts - Handle Method for User SDS Shortcut requests from HOMESCREEN HMI
       */
      bool handleShortcutContexts(unsigned int _srcContext, unsigned int _destContext);
      /**
       * handleEarlySdsContext - Handle Method for User Early SDS PTT requests
       */
      bool handleEarlySdsContext(unsigned int _srcContext, unsigned int _destContext);
      ContextState convertToServiceContextState(App::Core::enEarlyContextState earlyContextState);
      void requestVRSettingsContext();
};


#endif // !defined(EA_FAF598AF_B4F9_4bb1_91CE_ABBA6F93217B__INCLUDED_)
