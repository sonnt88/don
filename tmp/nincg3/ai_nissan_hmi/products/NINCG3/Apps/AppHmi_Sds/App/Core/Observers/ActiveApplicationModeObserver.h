/*
 * ActiveApplicationModeObserver.h
 *
 *  Created on: Apr 12, 2016
 *      Author: bee4kor
 */

#ifndef ActiveApplicationModeObserver_h
#define ActiveApplicationModeObserver_h


namespace App {
namespace Core {

class ActiveApplicationModeObserver
{
   public:
      virtual ~ActiveApplicationModeObserver() {};
      virtual void sdsHmiActiveAppModeChanged(unsigned int sdsHmiActiveAppMode) = 0;
};


}
}


#endif /* ActiveApplicationModeObserver_h */
