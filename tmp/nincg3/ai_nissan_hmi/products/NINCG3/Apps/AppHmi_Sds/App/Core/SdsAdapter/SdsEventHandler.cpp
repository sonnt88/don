/*
 * SdsEventHandler.cpp
 *
 *  Created on: Apr 15, 2016
 *      Author: raa8hi
 */

#include "App/Core/Interfaces/IListHandler.h"
#include "App/Core/Interfaces/ISdsContextRequestor.h"
#include "App/Core/SdsAdapter/SdsEventHandler.h"
#include "App/Core/SdsDefines.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SDS_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SDS
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SDS_"
#define ETG_I_FILE_PREFIX                 App::Core::SdsEventHandler::
#include "trcGenProj/Header/SdsEventHandler.cpp.trc.h"
#endif

using namespace sds_gui_fi::SdsGuiService;

namespace App {
namespace Core {


SdsEventHandler::SdsEventHandler(::boost::shared_ptr<sds_gui_fi::SdsGuiService::SdsGuiServiceProxy> sdsGuiServiceProxy,
                                 ISdsContextRequestor* pSdsContextRequestor,
                                 IListHandler* pListHandler)
   : _sdsGuiServiceProxy(sdsGuiServiceProxy)
   , _pSdsContextRequestor(pSdsContextRequestor)
   , _pListHandler(pListHandler)
   , _backTransitflag(true)
{
}


SdsEventHandler::~SdsEventHandler()
{
   _pSdsContextRequestor = NULL;
   _pListHandler = NULL;
}


void SdsEventHandler::onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                                  const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _sdsGuiServiceProxy)
   {
      _sdsGuiServiceProxy->sendEventRegister(*this);
   }
}


void SdsEventHandler::onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                                    const asf::core::ServiceStateChange& /*stateChange*/)
{
   if (proxy == _sdsGuiServiceProxy)
   {
      _sdsGuiServiceProxy->sendEventDeregisterAll();
   }
}


void SdsEventHandler::onEventError(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/,
                                   const ::boost::shared_ptr< EventError >& /*error*/)
{
}


void SdsEventHandler::onEventSignal(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/,
                                    const ::boost::shared_ptr< EventSignal >& signal)
{
   _backTransitflag = true; //reset flag on each event

   if (handleGeneralEvents(signal->getGuiEvent()))
   {
      return;
   }
   if (handleSourceChangeEvents(signal->getGuiEvent()))
   {
      return;
   }
   if (handleSXMEvents(signal->getGuiEvent()))
   {
      return;
   }
   if (handleExternalSystemEvents(signal->getGuiEvent()))
   {
      return;
   }
   if (handleSDSPopups(signal->getGuiEvent()))
   {
      return;
   }
   if (handleNavigationEvents(signal->getGuiEvent()))
   {
      return;
   }

   ETG_TRACE_USR4(("SdsEventHandler::onEventSignal - _backTransitflag = %d", _backTransitflag));
}


bool SdsEventHandler::getBackTransitionFlag()
{
   return _backTransitflag;
}


bool SdsEventHandler::handleGeneralEvents(unsigned int _event)
{
   switch (_event)
   {
      case Event__SPEECH_DIALOG_SDS_START_SESSION:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SDS_START_SESSION"));
      }
      break;
      case Event__SPEECH_DIALOG_SDS_STOP_SESSION:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SDS_STOP_SESSION"));
      }
      break;
      case Event__SPEECH_DIALOG_SELECT_PAIRED_DEVICE_LIST:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SELECT_PAIRED_DEVICE_LIST"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_PHONE, PHONE_CONTEXT_CONNECTION_MANAGER);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_TCU_VOICE_MENU:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_TCU_VOICE_MENU"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_TELEMATICS, TELEMATICS_CONTEXT_TELEMATICS_VOICE_MENU);
      }
      break;
      case Event__SPEECH_DIALOG_PHONE_GOTO_PAIRING:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_PHONE_GOTO_PAIRING"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_PHONE, PHONE_CONTEXT_CONNECTION_MANAGER);
      }
      break;
      case Event__SPEECH_DIALOG_SDS_PHONE_HANDSFREE_STATE:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SDS_PHONE_HANDSFREE_STATE"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_PHONE, PHONE_CONTEXT_ACTIVE_CALL_IN_HANDSFREE);
      }
      break;
      case Event__SPEECH_DIALOG_PLAY_CARPLAY_AUDIO:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_PLAY_CARPLAY_AUDIO"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_MEDIA, MEDIA_SPI_CONTEXT_CARPLAY_DIPO_MUSIC);
      }
      break;
      case Event__SPEECH_DIALOG_PLAY_ANDROID_AUTO_AUDIO:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_PLAY_ANDROID_AUTO_AUDIO"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_MEDIA, MEDIA_SPI_CONTEXT_ANDROID_AUTO_MUSIC_VIDEO);
      }
      break;
      case Event__SPEECH_DIALOG_PLAY_BEEP_AUDIO:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_PLAY_BEEP_AUDIO"));
         playBeep();
      }
      break;
      case Event__SPEECH_DIALOG_SDS_SESSION_ABORT:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SDS_SESSION_ABORT"));
         _backTransitflag = false;
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_VR_SETTINGS:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_VR_SETTINGS"));
         showScreenLayout(SDS_Settings_Main);
         _backTransitflag = false;
      }
      break;
      default :
      {
         return false;
      }
   }
   return true;
}


bool SdsEventHandler::handleSXMEvents(unsigned int _event)
{
   switch (_event)
   {
      case Event__SPEECH_DIALOG_SHOW_INFO_CURRENT_WEATHER_REPORT:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_INFO_CURRENT_WEATHER_REPORT"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_SXM, SXM_CONTEXT_CURRENT_WEATHER);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_INFO_WEATHER_5_DAY_FORECAST:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_INFO_WEATHER_5_DAY_FORECAST"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_SXM, SXM_CONTEXT_FIVEDAY_FORECAST);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_INFO_WEATHER_6_HOUR_FORECAST:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_INFO_WEATHER_6_HOUR_FORECAST"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_SXM, SXM_CONTEXT_HOURLY_FORECAST);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_INFO_MOVIE_FAV_THEATERS:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_INFO_MOVIE_FAV_THEATERS"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_SXM, SXM_CONTEXT_FAV_THEATERS);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_INFO_MOVIE_LIST_THEATERS:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_INFO_MOVIE_LIST_THEATERS"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_SXM, SXM_CONTEXT_LIST_THEATERS);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_INFO_MOVIE_LIST_MOVIE:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_INFO_MOVIE_LIST_MOVIE"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_SXM, SXM_CONTEXT_LIST_MOVIES);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_INFO_STOCK:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_INFO_STOCK"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_SXM, SXM_CONTEXT_STOCKS_MAIN);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_SPORTS_MOTORSPORTS:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_SPORTS_MOTORSPORTS"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_SXM, SXM_CONTEXT_SPORTS_MOTORSPORTS);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_SPORTS_GOLF:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_SPORTS_GOLF"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_SXM, SXM_CONTEXT_SPORTS_GOLF);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_SPORTS_ICEHOCKEY:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_SPORTS_ICEHOCKEY"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_SXM, SXM_CONTEXT_SPORTS_ICEHOCKEY);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_SPORTS_SOCCER:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_SPORTS_SOCCER"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_SXM, SXM_CONTEXT_SPORTS_SOCCER);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_SPORTS_BASEBALL:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_SPORTS_BASEBALL"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_SXM, SXM_CONTEXT_SPORTS_BASEBALL);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_SPORTS_BASKETBALL:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_SPORTS_BASKETBALL"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_SXM, SXM_CONTEXT_SPORTS_BASKETBALL);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_SPORTS_FOOTBALL:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_SPORTS_FOOTBALL"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_SXM, SXM_CONTEXT_SPORTS_FOOTBALL);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_SPORTS_FAVOURITES:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_SPORTS_FAVOURITES"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_SXM, SXM_CONTEXT_SPORTS_FAVORITE);
      }
      break;
      default :
      {
         return false;
      }
   }
   return true;
}


bool SdsEventHandler::handleSourceChangeEvents(unsigned int _event)
{
   switch (_event)
   {
      case Event__SPEECH_DIALOG_SDS_PLAY_AM:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SDS_PLAY_AM"));
         _backTransitflag = false;
      }
      break;
      case Event__SPEECH_DIALOG_SDS_PLAY_FM:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SDS_PLAY_FM"));
         _backTransitflag = false;
      }
      break;
      case Event__SPEECH_DIALOG_SDS_PLAY_XM:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SDS_PLAY_XM"));
         _backTransitflag = false;
      }
      break;
      default :
      {
         return false;
      }
   }
   return true;
}


bool SdsEventHandler::handleExternalSystemEvents(unsigned int _event)
{
   switch (_event)
   {
      case Event__SPEECH_DIALOG_SHOW_ENERGY_FLOW:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_ENERGY_FLOW"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_VEHICLE, VEHICLE_CONTEXT_HEV_MAIN);
      }
      break;
      default :
      {
         ETG_TRACE_ERR((" NO ExternalSystem Events"));
         return false;
      }
   }
   return true;
}


bool SdsEventHandler::handleNavigationEvents(unsigned int _event)
{
   switch (_event)
   {
      case Event__SPEECH_DIALOG_SHOW_INFO_TRAFFIC:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_INFO_TRAFFIC"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_NAVIGATION, NAVIGATION_CONTEXT_TRAFFIC_INFO);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_INFO_WEATHER_REPORT_MAP:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_INFO_WEATHER_REPORT_MAP"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_NAVIGATION, NAVIGATION_CONTEXT_WEATHER_MAP);
      }
      break;
      case Event__SPEECH_DIALOG_START_NAVIHOMEDESTINATION:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_START_NAVIHOMEDESTINATION"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_NAVIGATION, NAVIGATION_CONTEXT_HOME);
      }
      break;
      case Event__SPEECH_DIALOG_START_NAVIWORKDESTINATION:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_START_NAVIWORKDESTINATION"));
         _backTransitflag = false;
         contextSwitch(APPID_APPHMI_NAVIGATION, NAVIGATION_CONTEXT_WORK);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_ACTIVE"));
         _backTransitflag = false;
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_PASSIVE:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_NAVI_SOURCECHANGE_PASSIVE"));
         _backTransitflag = true;
      }
      break;
      default:
      {
         ETG_TRACE_ERR((" NO Navigation Events"));
         return false;
      }
   }
   return true;
}


bool SdsEventHandler::handleSDSPopups(unsigned int _event)
{
   switch (_event)
   {
      case Event__SPEECH_DIALOG_SHOW_POPUP_LANGUAGE_NOT_SUPPORTED:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_POPUP_LANGUAGE_NOT_SUPPORTED"));
         showSDSPopup(SdsPopups_Popups_SDS__MPOP_LANGUAGE_NOT_SUPPORTED);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_POPUP_LOADING_OPEN:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_POPUP_LOADING_OPEN"));
         showSDSPopup(SdsPopups_Popups_SDS_VR_MPOP_VR_INIT);
      }
      break;
      case Event__SPEECH_DIALOG_SHOW_POPUP_NODATACARRIER_OPEN:
      {
         ETG_TRACE_USR4(("Event__SPEECH_DIALOG_SHOW_POPUP_NODATACARRIER_OPEN"));
         showSDSPopup(VR_NODATACARRIER_POPUP);
      }
      break;
      default :
      {
         return false;
      }
   }
   return true;
}


void SdsEventHandler::showScreenLayout(enSdsScreenLayoutType screenLayout)
{
   ETG_TRACE_USR4(("SdsEventHandler::showScreenLayout"));
   if (_pListHandler)
   {
      _pListHandler->setShowScreenLayout(screenLayout);
   }
}


void SdsEventHandler::showSDSPopup(unsigned int popupid)
{
   ETG_TRACE_USR4(("SdsEventHandler::showSDSPopup %d", popupid));
   POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, popupid)));
}


void SdsEventHandler::playBeep()
{
   ETG_TRACE_USR4(("SdsEventHandler::playBeep"));
   POST_MSG((COURIER_MESSAGE_NEW(PlayBeepReqMsg)(BEEPTYPE_ROGER)));
}


void SdsEventHandler::contextSwitch(const enApplicationId appId, const enContextId destContextId)
{
   if (_pSdsContextRequestor)
   {
      _pSdsContextRequestor->contextSwitch(appId, destContextId);
   }
   else
   {
      ETG_TRACE_FATAL(("Error::_pSdsContextRequestor is NULL"));
   }
}


} // namespace Core
} // namespace App
