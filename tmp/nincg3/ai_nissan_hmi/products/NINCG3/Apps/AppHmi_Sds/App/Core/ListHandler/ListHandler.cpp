/*
 *ListHandler.cpp
 *
 *  Created on:
 *      Author:
 */

#include "App/Core/ListHandler/ListHandler.h"
#include "App/Core/SdsAdapter/Proxy/requestor/SdsAdapterRequestor.h"
#include "App/Core/SdsAdapter/GuiUpdater.h"
#include "App/Core/SdsAdapter/VRSettingsHandler.h"
#include "App/Core/SdsDefines.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SDS_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SDS
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SDS_"
#define ETG_I_FILE_PREFIX                 App::Core::ListHandler::
#include "trcGenProj/Header/ListHandler.cpp.trc.h"
#endif

namespace App {
namespace Core {

ListHandler::ListHandler(GuiUpdater* guiUpdater, VRSettingsHandler* vrSettingsHandler, SdsAdapterRequestor* sdsAdapterRequestor)
   : _pGuiUpdater(guiUpdater),
     _pVRSettingsHandler(vrSettingsHandler),
     currentLayout(LAYOUT_R),
     _pSdsAdapterRequestor(sdsAdapterRequestor)
{
   /////////////////////////////////////////////////Resgister SDS HMI List ID's////////////////////////////////////////////////////////////////////////////
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_SDS_VL, this);
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_SDS_Settings_Main, this);
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_SDS_Best_Match_List, this);
   ListRegistry::s_getInstance().addListImplementation(LIST_ID_SDS_VL_FUEL, this);
   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   ///////////////////Member Initializations/////////////////////////////////////////////////////////////////////////////////////////////////////////////
   initializeMembers();
   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}


tSharedPtrDataProvider ListHandler::getListDataProvider(const ListDataInfo& listDataInfo)
{
   ETG_TRACE_USR4(("ListHandler getListDataProvider %d", listDataInfo.listId));
   switch (listDataInfo.listId)
   {
      case LIST_ID_SDS_VL:
      {
         ETG_TRACE_USR4((" Case LIST_ID_SDS_VL"));
         return getSDSList();
      }
      case LIST_ID_SDS_Settings_Main:
      {
         ETG_TRACE_USR4((" Case LIST_ID_SDS_Settings_Main"));
         currentLayout = SDS_Settings_Main;
         return(_pVRSettingsHandler->getSettingsMainList());
      }
      case LIST_ID_SDS_Best_Match_List:
      {
         ETG_TRACE_USR4((" Case LIST_ID_SDS_Best_Match_List"));
         currentLayout = SDS_Settings_BestMatch;
         return _pVRSettingsHandler->getBestMatchList();
      }
      case LIST_ID_SDS_VL_FUEL:
      {
         return getFuelPriceList();
         break;
      }
      default:
      {
         return tSharedPtrDataProvider();
      }
   }
}


tSharedPtrDataProvider ListHandler::getSDSList()
{
   ETG_TRACE_USR4(("ListHandler::getSDSList()"));
   ListDataProviderBuilder listBuilder(LIST_ID_SDS_VL, DATA_CONTEXT_LIST_BTN_VL);

   switch (currentLayout)
   {
      case LAYOUT_R:
      {
         ETG_TRACE_USR4(("ListHandler::getSDSList() - currentLayout LAYOUT_R:"));
         for (unsigned int i = 0; (i < MAX_LIST_ITEMS_LAYOUT_R) && (i < vecCommandString.size()); i++)
         {
            listBuilder.AddItem(i, 0UL, DATA_CONTEXT_LIST_BTN_VL).AddData(vecCommandString.at(i).text.c_str()).AddData(vecCommandString.at(i).isSelectable);
         }
         if (_pGuiUpdater)
         {
            _pGuiUpdater->updateSubCommandList(vecSubCommandString);
         }
      }

      break;

      case LAYOUT_N:
      {
         ETG_TRACE_USR4(("ListHandler::getSDSList() - currentLayout LAYOUT_N:"));
         for (unsigned int i = 0; (i < MAX_LIST_ITEMS_LAYOUT_N) && (i < vecCommandString.size()); i++)
         {
            listBuilder.AddItem(i, 0UL, DATA_CONTEXT_LIST_BTN_VL).AddData(vecCommandString.at(i).text.c_str()).AddData(vecCommandString.at(i).isSelectable);
         }
      }
      break;

      case LAYOUT_L:
      {
         ETG_TRACE_USR4(("ListHandler::getSDSList() - currentLayout LAYOUT_L:"));
         for (unsigned int i = 0; (i < MAX_LIST_ITEMS_LAYOUT_L) && (i < vecCommandString.size()); i++)
         {
            listBuilder.AddItem(i, 0UL, DATA_CONTEXT_LIST_BTN_VL).AddData(vecCommandString.at(i).text.c_str()).AddData(vecDescString.at(i).c_str()).AddData(vecCommandString.at(i).isSelectable);
         }
      }
      break;

      case LAYOUT_T:
      {
         ETG_TRACE_USR4(("ListHandler::getSDSList() - currentLayout LAYOUT_T:"));
         for (unsigned int i = 0; (i < MAX_LIST_ITEMS_LAYOUT_T) && (i < vecCommandString.size()); i++)
         {
            listBuilder.AddItem(i, 0UL, DATA_CONTEXT_LIST_BTN_VL).AddData(vecCommandString.at(i).text.c_str()).AddData(vecCommandString.at(i).isSelectable);
         }
      }
      break;

      case LAYOUT_O:
      {
         ETG_TRACE_USR4(("ListHandler::getSDSList() - currentLayout LAYOUT_O:"));
         for (unsigned int i = 0; (i < MAX_LIST_ITEMS_LAYOUT_O) && (i < vecCommandString.size()); i++)
         {
            listBuilder.AddItem(i, 0UL, DATA_CONTEXT_LIST_BTN_VL).AddData(vecCommandString.at(i).text.c_str()).AddData(vecCommandString.at(i).isSelectable);
         }
      }
      break;

      case LAYOUT_C:
      {
         ETG_TRACE_USR4(("ListHandler::getSDSList() - currentLayout : LAYOUT_C"));
      }
      break;

      case LAYOUT_M:
      {
         ETG_TRACE_USR4(("ListHandler::getSDSList() - currentLayout LAYOUT_M:"));
         for (unsigned int i = 0; (i < MAX_LIST_ITEMS_LAYOUT_M) && (i < vecCommandString.size()); i++)
         {
            listBuilder.AddItem(i, 0UL, DATA_CONTEXT_LIST_BTN_VL).AddData(vecCommandString.at(i).text.c_str()).AddData(vecCommandString.at(i).isSelectable);
         }
      }
      break;

      case LAYOUT_S:
      {
         ETG_TRACE_USR4(("ListHandler::getSDSList() - currentLayout LAYOUT_S:"));
         for (unsigned int i = 0; (i < MAX_LIST_ITEMS_LAYOUT_S) && (i < vecCommandString.size()); i++)
         {
            listBuilder.AddItem(i, 0UL, DATA_CONTEXT_LIST_BTN_VL).AddData(vecCommandString.at(i).text.c_str()).AddData(vecCommandString.at(i).isSelectable);
         }
      }
      break;

      case LAYOUT_E :
      {
         ETG_TRACE_USR4(("ListHandler::getSDSList() - currentLayout LAYOUT_E :"));
         for (unsigned int i = 0; (i < MAX_LIST_ITEMS_LAYOUT_E) && (i < vecCommandString.size()); i++)
         {
            listBuilder.AddItem(0, 0UL, DATA_CONTEXT_LIST_BTN_FUEL_PRICES).AddData(vecCommandString.at(i).text.c_str()).AddData(vecDescString.at(i).c_str()) \
            .AddData(vecPriceAgeString.at(i).c_str()).AddData(vecPriceString.at(i).c_str()).AddData(vecDistanceString.at(i).c_str()) \
            .AddData((int)(strtol(vecDirectionString.at(i).c_str(), NULL, 10))).AddData(vecCommandString.at(i).isSelectable);
         }
      }
      break;

      case LAYOUT_Q :
      {
         ETG_TRACE_USR4(("ListHandler::getSDSList() - currentLayout LAYOUT_Q:"));
         for (unsigned int i = 0; (i < MAX_LIST_ITEMS_LAYOUT_Q) && (i < vecCommandString.size()); i++)
         {
            listBuilder.AddItem(i, 0UL, DATA_CONTEXT_LIST_BTN_POI).AddData(vecCommandString.at(i).text.c_str()).AddData(vecDescString.at(i).c_str()) \
            .AddData((int)(strtol(vecDirectionString.at(i).c_str(), NULL, 10))).AddData(vecDistanceString.at(i).c_str()).AddData(vecCommandString.at(i).isSelectable);
         }
      }
      break;

      default:
      {
         ETG_TRACE_USR4(("ListHandler::getSDSList() currentLayout : INVALID"));
      }
   }
   tSharedPtrDataProvider dataProvider = listBuilder.CreateDataProvider();
   return dataProvider;
}


tSharedPtrDataProvider ListHandler::getFuelPriceList()
{
   ETG_TRACE_USR4(("ListHandler::getFuelPriceList()"));
   ListDataProviderBuilder listBuilder_fuel(LIST_ID_SDS_VL_FUEL, DATA_CONTEXT_LIST_BTN_FUEL_PRICES);

   if (currentLayout == LAYOUT_E)
   {
      for (unsigned int i = 0; (i < MAX_LIST_ITEMS_LAYOUT_E) && (i < vecCommandString.size()); i++)
      {
         listBuilder_fuel.AddItem(0, 0UL, DATA_CONTEXT_LIST_BTN_FUEL_PRICES).AddData("1").AddData("Fuel1").AddData("Price").AddData("3 Days").AddData("1.6 mi").AddData(true);
      }
   }
   else
   {
      ETG_TRACE_USR4(("ListHandler::getFuelPriceList() currentLayout : INVALID"));
   }
   tSharedPtrDataProvider fuelListDataProvider = listBuilder_fuel.CreateDataProvider();
   return fuelListDataProvider;
}


bool ListHandler::onCourierMessage(const ButtonReactionMsg& oMsg)
{
   ETG_TRACE_USR4(("onCourierMessage ButtonReactionMsg -   received "));
   if ((oMsg.GetEnReaction() == enRelease))
   {
      ETG_TRACE_USR4(("ButtonReactionMsg Release - received "));

      if ((oMsg.GetSender() == Courier::Identifier("BtnNextPageSds/Button")))
      {
         ETG_TRACE_USR4(("BtnNextPage Pressed"));
         if (_pSdsAdapterRequestor)
         {
            _pSdsAdapterRequestor->requestNextPage();
         }
         return true;
      }

      if ((oMsg.GetSender() == Courier::Identifier("BtnPrevPageSds/Button")))
      {
         ETG_TRACE_USR4(("BtnPrevPage Pressed"));
         if (_pSdsAdapterRequestor)
         {
            _pSdsAdapterRequestor->requestPrevPage();
         }
         return true;
      }

      ETG_TRACE_USR4(("Handle Dynamic List"));

      ListProviderEventInfo info;
      if (ListProviderEventInfo::GetItemIdentifierInfo(oMsg.GetSender(), info) && (info.getListId() == LIST_ID_SDS_VL))
      {
         ETG_TRACE_USR4(("info.getListId() = %d ,info.getHdlRow() = %d", info.getListId(), info.getHdlRow()));
         if (_pSdsAdapterRequestor)
         {
            ETG_TRACE_USR4(("_pSdsAdapterRequestor->listItemSelected"));
            _pSdsAdapterRequestor->listItemSelected(info.getHdlRow());
         }
         POST_MSG((COURIER_MESSAGE_NEW(ButtonListItemUpdMsg)(info.getListId(), info.getHdlRow(), info.getHdlCol(), oMsg.GetEnReaction())));
         return true;
      }

      if (ListProviderEventInfo::GetItemIdentifierInfo(oMsg.GetSender(), info) && (info.getListId() == LIST_ID_SDS_Settings_Main))
      {
         ETG_TRACE_USR4(("ListHandler:ButtonReactionMsg Listid : %d, HdlRow : %d, hdlCol : %d", info.getListId(), info.getHdlRow(), info.getHdlCol()));
         _pVRSettingsHandler->processSettingsMainListItems(info, oMsg);
         POST_MSG((COURIER_MESSAGE_NEW(ButtonListItemUpdMsg)(info.getListId(), info.getHdlRow(), info.getHdlCol(), oMsg.GetEnReaction())));
         return true;
      }

      if (ListProviderEventInfo::GetItemIdentifierInfo(oMsg.GetSender(), info) && ((info.getListId() == LIST_ID_SDS_Best_Match_List)))
      {
         _pVRSettingsHandler->processBestMatchListItems(info, oMsg);
         POST_MSG((COURIER_MESSAGE_NEW(ButtonListItemUpdMsg)(info.getListId(), info.getHdlRow(), info.getHdlCol(), oMsg.GetEnReaction())));
         return true;
      }
   }
   return false;
}


std::vector <SelectableListItem>& ListHandler::getVectCmdString()
{
   return vecCommandString;
}


std::vector <std::string>& ListHandler::getVectSubCmdString()
{
   return vecSubCommandString;
}


std::vector <std::string>& ListHandler::getVectDescString()
{
   return vecDescString;
}


std::vector <std::string>& ListHandler::getVectDistString()
{
   return vecDistanceString;
}


std::vector <std::string>& ListHandler::getVectPriceAgeString()
{
   return vecPriceAgeString;
}


std::vector <std::string>& ListHandler::getVectPriceString()
{
   return vecPriceString;
}


std::vector <std::string>& ListHandler::getVectDirectionString()
{
   return vecDirectionString;
}


void ListHandler::setShowScreenLayout(enSdsScreenLayoutType screenLayout)
{
   setCurrentLayout(screenLayout);
   showScreenLayout(currentLayout);
}


void ListHandler::setCurrentLayout(enSdsScreenLayoutType screenLayout)
{
   currentLayout = screenLayout;
}


void ListHandler::showScreenLayout(enSdsScreenLayoutType screenLayout)
{
   ETG_TRACE_USR4(("POST_MSG( (COURIER_MESSAGE_NEW(ShowDomainScreen)()) )"));
   POST_MSG((COURIER_MESSAGE_NEW(ShowDomainScreen)(screenLayout)));
}


enSdsScreenLayoutType ListHandler::getCurrentLayout()
{
   return currentLayout;
}


void ListHandler::updateSubCommandList()
{
   if (_pGuiUpdater)
   {
      _pGuiUpdater->updateSubCommandList(vecSubCommandString);
   }
}


void ListHandler::initializeMembers()
{
   SelectableListItem selItem;
   //selItem.isSelectable = true;
   //selItem.text = "Command";
   vecCommandString		=  std::vector<SelectableListItem>(10, selItem);
   vecSubCommandString		= std::vector <std::string>(7,  "");
   vecDescString			= std::vector <std::string>(10, "Description");
   vecPriceAgeString		= std::vector <std::string>(10, "Today");
   vecPriceString			= std::vector <std::string>(10, "$7.7");
   vecDistanceString		= std::vector <std::string>(10, "1.5mi");
   vecDirectionString		= std::vector <std::string>(10, "4");

   initializeListTextFields();
   initializeSubCommandList();
}


void ListHandler::initializeListTextFields()
{
   vecCommandString.at(0).isSelectable = false;
   vecCommandString.at(1).isSelectable = false;
   vecCommandString.at(2).isSelectable = false;
   vecCommandString.at(3).isSelectable = false;
   vecCommandString.at(0).text = "Phone";
   vecCommandString.at(1).text = "Audio";
   vecCommandString.at(2).text = "Navigation";
   vecCommandString.at(3).text = "Information";
}


void ListHandler::initializeSubCommandList()
{
   vecSubCommandString.at(0) = "";
   vecSubCommandString.at(1) = "";
   vecSubCommandString.at(2) = "   The Voice Recognition";
   vecSubCommandString.at(3) = "   system is starting.";
   vecSubCommandString.at(4) = "   Please Wait.";
   vecSubCommandString.at(5) = "";
   vecSubCommandString.at(6) = "";
}


ListHandler::~ListHandler()
{
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_SDS_VL);
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_SDS_Settings_Main);
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_SDS_Best_Match_List);
   ListRegistry::s_getInstance().removeListImplementation(LIST_ID_SDS_VL_FUEL);

   DELETE_AND_PURGE(_pGuiUpdater);
   DELETE_AND_PURGE(_pVRSettingsHandler);
   DELETE_AND_PURGE(_pSdsAdapterRequestor);
}


} // Namespace Core
} // Namespace App
