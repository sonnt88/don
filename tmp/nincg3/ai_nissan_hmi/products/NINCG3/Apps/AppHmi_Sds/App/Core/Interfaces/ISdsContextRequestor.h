/*
 * ISdsContextRequestor.h
 *
 *  Created on: Feb 5, 2016
 *      Author: bee4kor
 */

#ifndef ISdsContextRequestor_h
#define ISdsContextRequestor_h

#include <ProjectBaseTypes.h>
#include <ProjectBaseContexts.h>


namespace App {
namespace Core {

class ISdsContextRequestor
{
   public:
      virtual ~ISdsContextRequestor() {};
      virtual void contextSwitch(const enApplicationId appId, const enContextId destContextId) = 0;
      virtual void contextSwitchSelf() = 0;
};


}
}


#endif /* ISdsContextRequestor_h */
