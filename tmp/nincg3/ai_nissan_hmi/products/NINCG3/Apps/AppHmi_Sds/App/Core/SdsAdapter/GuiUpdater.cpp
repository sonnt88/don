/****************************************************************************
 * Copyright (C) Robert Bosch Car Multimedia GmbH, 2016
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ***************************************************************************/

#include "App/Core/SdsAdapter/GuiUpdater.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SDS_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SDS
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SDS_"
#define ETG_I_FILE_PREFIX                 App::Core::GuiUpdater::
#include "trcGenProj/Header/GuiUpdater.cpp.trc.h"
#endif

namespace App {
namespace Core {


GuiUpdater::GuiUpdater()
{
}


GuiUpdater::~GuiUpdater()
{
}


void GuiUpdater::updateMicrophoneLevelData(unsigned int micLevel) const
{
//   ETG_TRACE_USR4(("GuiUpdater::updateMicrophoneLevelData, micLevel = %d", micLevel));

   Courier::DataItemContainer<SdsMicrophoneLevelDataBindingSource> _MicrophoneLevelData;
   (*_MicrophoneLevelData).mMicLevel = micLevel;
   _MicrophoneLevelData.MarkItemModified(ItemKey::SdsMicrophoneLevelItem);
   _MicrophoneLevelData.SendUpdate(true);
}


void GuiUpdater::updateHeaderSpeakingIconIndex(unsigned int index) const
{
   Courier::DataItemContainer<SdsHeaderIconIndexDataBindingSource> _headerIconIndexData;
   (*_headerIconIndexData).mIconIndex = index;
   _headerIconIndexData.MarkItemModified(ItemKey::SdsHeaderIconIndex::IconIndexItem);
   _headerIconIndexData.SendUpdate(true);
}


void GuiUpdater::updateHeaderTextData(std::string headerString)
{
   ETG_TRACE_USR4(("GuiUpdater::updateHeaderTextData, _headerString = %s", headerString.c_str()));
   (*_headerTextData).mText_1 = headerString.c_str();
   _headerTextData.MarkAllItemsModified();
   _headerTextData.SendUpdate(true);
}


void GuiUpdater::updateHelpLineData(std::string helpString)
{
   ETG_TRACE_USR4(("GuiUpdater::updateHelpLineData, helpString = %s", helpString.c_str()));
   (*_headerTextData).mText_3 = helpString.c_str();
}


void GuiUpdater::updateHelp3List(std::string headerString1, std::string headerString2, std::string headerString3) const
{
   Courier::DataItemContainer<SDS_Help_3ListDataBindingSource> help3ListData;
   (*help3ListData).mFlexListText = headerString1.c_str();
   (*help3ListData).mFlexListText_1 = headerString2.c_str();
   (*help3ListData).mFlexListText_2 = headerString3.c_str();

   help3ListData.MarkItemModified(ItemKey::SDS_Help_3ListItem);
   help3ListData.SendUpdate(true);
}


void GuiUpdater::updateHelpList(std::string headerString1) const
{
   Courier::DataItemContainer<SDS_Help_ListDataBindingSource> helpListData;
   (*helpListData).mFlexListText = headerString1.c_str();
   helpListData.MarkItemModified(ItemKey::SDS_Help_ListItem);
   helpListData.SendUpdate(true);
}


void GuiUpdater::updateInfoTextData(std::string infoString) const
{
//   ETG_TRACE_USR4(("GuiUpdater::updateInfoTextData, infoString = %s", infoString.c_str()));
   Courier::DataItemContainer<SDS_Info_ListDataBindingSource> infoListData;
   (*infoListData).mText_1 = infoString.c_str();
   infoListData.MarkAllItemsModified();
   infoListData.SendUpdate(true);
}


void GuiUpdater::updateSmsMessageContent(Candera::String smsMessage) const
{
   Courier::DataItemContainer<SdsLayout_SDataBindingSource> layout_S_Data;
   (*layout_S_Data).mSmsMessage = smsMessage;
   layout_S_Data.MarkAllItemsModified();
   layout_S_Data.SendUpdate(true);
}


void GuiUpdater::updateSubCommandList(const std::vector <std::string>& vecSubCommandString) const
{
   ETG_TRACE_USR4(("SdsHall::updateSubCommandList"));

   Courier::DataItemContainer<SubCommandTextDataBindingSource> _subCommandTextData;
   (*_subCommandTextData).mText_1_visible = false;
   (*_subCommandTextData).mText_2_visible = false;
   (*_subCommandTextData).mText_3_visible = false;
   (*_subCommandTextData).mText_4_visible = false;
   (*_subCommandTextData).mText_5_visible = false;
   (*_subCommandTextData).mText_6_visible = false;
   (*_subCommandTextData).mText_7_visible = false;
   unsigned int subCmdindex = 0;

   ETG_TRACE_USR4(("vecSubCommandString.size() = %d", vecSubCommandString.size()));

   if (vecSubCommandString.size())
   {
      if (subCmdindex < vecSubCommandString.size())
      {
         (*_subCommandTextData).mText_1 = vecSubCommandString.at(subCmdindex).c_str();
         (*_subCommandTextData).mText_1_visible = true;
         subCmdindex++;

         if (subCmdindex < vecSubCommandString.size())
         {
            (*_subCommandTextData).mText_2 = vecSubCommandString.at(subCmdindex).c_str();
            (*_subCommandTextData).mText_2_visible = true;
            subCmdindex++;

            if (subCmdindex < vecSubCommandString.size())
            {
               (*_subCommandTextData).mText_3 = vecSubCommandString.at(subCmdindex).c_str();
               (*_subCommandTextData).mText_3_visible = true;
               subCmdindex++;

               if (subCmdindex < vecSubCommandString.size())
               {
                  (*_subCommandTextData).mText_4 = vecSubCommandString.at(subCmdindex).c_str();
                  (*_subCommandTextData).mText_4_visible = true;
                  subCmdindex++;

                  if (subCmdindex < vecSubCommandString.size())
                  {
                     (*_subCommandTextData).mText_5 = vecSubCommandString.at(subCmdindex).c_str();
                     (*_subCommandTextData).mText_5_visible = true;
                     subCmdindex++;

                     if (subCmdindex < vecSubCommandString.size())
                     {
                        (*_subCommandTextData).mText_6 = vecSubCommandString.at(subCmdindex).c_str();
                        (*_subCommandTextData).mText_6_visible = true;
                        subCmdindex++;

                        if (subCmdindex < vecSubCommandString.size())
                        {
                           (*_subCommandTextData).mText_7 = vecSubCommandString.at(subCmdindex).c_str();
                           (*_subCommandTextData).mText_7_visible = true;
                           subCmdindex++;
                        }
                     }
                  }
               }
            }
         }
      }
   }
   else
   {
      return;
   }
   _subCommandTextData.MarkAllItemsModified();
   _subCommandTextData.SendUpdate(true);
}


bool GuiUpdater::updateDynamicList(unsigned int listId)
{
   ListDataInfo _listDataInfo;
   _listDataInfo.listId = listId;
   if ((ListRegistry::s_getInstance().updateList(_listDataInfo)) == true)
   {
      ETG_TRACE_USR4(("List ID - %d is updated", listId));
      return true;
   }
   else
   {
      ETG_TRACE_USR4(("ERROR::List ID - %d Update Failed", listId));
      return false;
   }
}


} // namespace Core
} // namespace App
