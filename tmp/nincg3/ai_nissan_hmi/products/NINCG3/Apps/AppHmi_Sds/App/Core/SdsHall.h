/**
 *  @file   <SdsHall.h>
 *  @copyright (c) <2014> Robert Bosch Car Multimedia GmbH
 *  @addtogroup <AppHmi_Sds>
 */


#ifndef SdsHall_h
#define SdsHall_h

#include "AppBase/HallComponentBase.h"
#include "App/Core/ContextSwitch/Proxy/requestor/AppSds_ContextRequestor.h"
#include "App/Core/ContextSwitch/Proxy/provider/AppSds_ContextProvider.h"
#include "App/Core/EarlyStartup/EarlyStartupHandler.h"
#include "App/Core/SdsAdapter/Proxy/provider/SdsAdapterProvider.h"
#include "App/Core/SdsAdapter/Proxy/requestor/SdsAdapterRequestor.h"
#include "App/Core/LanguageHandler/Proxy/provider/SdsHmiLanguageHandler.h"
#include "App/DataModel/HMIModelComponent.h"
#include "bosch/cm/ai/nissan/hmi/hmidataservice/HmiDataProxy.h"
#include "CgiExtensions/ListDataProviderDistributor.h"
#include "Common/DefSetServiceBase/DefSetServiceBase.h"
#include "Common/HmiInfo/Proxy/HmiInfoProxy.h"
#include "Common/ListHandler/ListRegistry.h"
#include "App/Core/SdsAdapter/VRSettingsHandler.h"


namespace App {
namespace Core {

class HmiDataServiceClientHandler;
class SdsTTfisTestCommandHandler;
class GuiUpdater;
class VRSettingsHandler;
class ListHandler;
class HmiVehicleHandler;
class SdsEventHandler;

class SdsHall
   : public HallComponentBase
   , public iDefSetServiceBase
{
   public:

      SdsHall();
      virtual ~SdsHall();
      /**
       * onExpired - To indicate receiving onExpired timer signal from State Machine
       * @param[in] asf::core::Timer object reference
       * @param[in] none
       * @parm[out] boost::shared_ptr<asf::core::TimerPayload> Payload Data
       * @return void
       */
      virtual void onExpired(asf::core::Timer& timer, boost::shared_ptr<asf::core::TimerPayload> data);
      /**
       * registerProperties - Registers to CCA/DBus server properties by calling appropriate "Upreg/SendRegister" method calls.
       * @param[in] ::boost::shared_ptr< asf::core::Proxy >& proxy - GUI Proxy handler
       * @param[in] asf::core::ServiceStateChange& stateChange - Contains the current Service state
       * @pre - Gets called by GUI StateMachine when the GUI App is initialized and ready
       * @return void
       */
      virtual void registerProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                                      const asf::core::ServiceStateChange& stateChange);
      /**
       * deregisterProperties - Deregisters  CCA/DBus server properties by calling appropriate "RelUpreg/SendDeRegister" method calls.
       * @param[in] ::boost::shared_ptr< asf::core::Proxy >& proxy - GUI Proxy handler
       * @param[in] asf::core::ServiceStateChange& stateChange - Contains the current Service state
       * @pre - Gets called by GUI StateMachine when the GUI App Shuts Down or quits
       * @return void
       */
      virtual void deregisterProperties(const ::boost::shared_ptr< asf::core::Proxy >& proxy,
                                        const asf::core::ServiceStateChange& stateChange);

      ////////////////////// virtual methods from iDefSetServiceBase to handle restore factory settings //////////////////////////////////////////////////////////////////
      void reqPrepareResponse();
      void reqExecuteResponse();
      void reqFinalizeResponse();
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////General Access Methods////////////////////////////////////////////////////////////////////////////////////////////////////////////
      unsigned int updateTestModeData(std::string _headerString1, std::string _headerString2, std::string _headerString3, std::string _headerString4, std::string _headerString5);
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   private:

      ////////////////////////////////////////////// Courier Message Mapping with Traces classes ///////////////////////////////////////////////////////////////////////////////////
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_SDS_COURIER_PAYLOAD_MODEL_COMP)
      ON_COURIER_MESSAGE(HkBackPressMsg)
      ON_COURIER_MESSAGE(SkBackPressMsg)
      ON_COURIER_MESSAGE(nBestToggleMsg)
      ON_COURIER_MESSAGE(FocusChangedUpdMsg)
      ON_COURIER_MESSAGE(ListChangedUpdMsg)
      ON_COURIER_MESSAGE(EncoderRightRotatedStatusMsg)
      ON_COURIER_MESSAGE(HKStatusChangedNotifyMsg)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_DELEGATE_TO_CLASS(HallComponentBase)
      COURIER_MSG_DELEGATE_TO_OBJ(_pSdsAdapterRequestor)
      COURIER_MSG_DELEGATE_TO_OBJ(_sdsHmiLanguageHandler)
      COURIER_MSG_DELEGATE_TO_OBJ(_pHMIModelComponent)
      COURIER_MSG_DELEGATE_TO_OBJ(_pVRSettingsHandler)
      COURIER_MSG_DELEGATE_TO_OBJ(_pHmiInfoProxy)
      COURIER_MSG_DELEGATE_TO_OBJ(_pListHandler)
      COURIER_MSG_MAP_DELEGATE_END()
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      ::boost::shared_ptr<sds_gui_fi::SdsGuiService::SdsGuiServiceProxy> _sdsGuiServiceProxy;
      ::boost::shared_ptr<sds_gui_fi::PopUpService::PopUpServiceProxy> _popUpServiceProxy;
      ::boost::shared_ptr<sds_gui_fi::SettingsService::SettingsServiceProxy> _settingsServiceProxy;
      ::boost::shared_ptr<sds_gui_fi::SdsPhoneService::SdsPhoneServiceProxy> _sdsPhoneServiceProxy;
      ::boost::shared_ptr< ::bosch::cm::ai::nissan::hmi::hmidataservice::HmiData::HmiDataProxy > _hmiDataProxy;

      DefSetServiceBase* _pDefSetServiceBase;
      AppSds_ContextProvider* _pContextProvider;
      AppSds_ContextRequestor* _pContextRequestor;
      SdsAdapterProvider* _pSdsAdapterProvider;
      SdsAdapterRequestor* _pSdsAdapterRequestor;
      HmiDataServiceClientHandler* _pHmiDataServiceClientHandler;
      SdsTTfisTestCommandHandler* _pSdsTTfisTestCommandHandler;
      GuiUpdater* _pGuiUpdater;
      SdsHmiLanguageHandler* _sdsHmiLanguageHandler;
      IHmiInfoProxy* _pHmiInfoProxy;
      EarlyStartupHandler* _pEarlyStartupHandler;
      HMIModelComponent* _pHMIModelComponent;
      VRSettingsHandler* _pVRSettingsHandler;
      ListHandler* _pListHandler;
      HmiVehicleHandler* _pHmiVehicleHandler;
      SdsEventHandler* _pSdsEventHandler;

      unsigned int _guiPrevFocusIndex;
      bool _guiFocusChanged;
      int _guiPrevEncSteps;
      unsigned int _guiHKState;

      std::vector<asf::core::ServiceAvailableIF*> _proxyAvailabilityObservers;
      void addServiceAvailableObservers(asf::core::ServiceAvailableIF* obs);
      void registerServiceAvailableObservers();

      //////////The below onCourierMessage funtions are the various action perform & model subscribed courier messages from SM ////////////////////////////////////////////////////
      bool onCourierMessage(const HkBackPressMsg& msg);
      bool onCourierMessage(const SkBackPressMsg& msg);
      bool onCourierMessage(const nBestToggleMsg& msg);
      bool onCourierMessage(const FocusChangedUpdMsg& oMsg);
      bool onCourierMessage(const ListChangedUpdMsg& oMsg);
      bool onCourierMessage(const EncoderRightRotatedStatusMsg& msg);
      bool onCourierMessage(const HKStatusChangedNotifyMsg& msg);
      /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      uint8 getBestMatchPhonebookfromdatapool();
      void storeBestMatchPhonebooktodatapool(uint8 bestmatchphonebook);
      uint8 getBestMatchAudiofromdatapool();
      void storeBestMatchAudiotodatapool(uint8 bestmatchAudio);
      bool bSdsRestoreFactorySetting();
      void handleHardKeyLongPress(unsigned int hardkeyType);
      void handleHardKeyShortPress(unsigned int hardkeyType);
      void vRegisterObservers();
};


}
}


#endif
