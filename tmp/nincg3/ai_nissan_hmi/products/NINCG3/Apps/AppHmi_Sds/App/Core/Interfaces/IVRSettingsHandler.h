/*
 * IVRSettingsHandler.h
 *
 *  Created on:
 *      Author:
 */

#ifndef IVRSettingsHandler_h
#define IVRSettingsHandler_h

#include "App/Core/SdsHallTypes.h"

namespace App {
namespace Core {


class IVRSettingsHandler
{
   public:
      virtual ~IVRSettingsHandler() {};
      virtual void sendBeepOnlyMode(bool mode) = 0;
      virtual void sendShortPrompt(bool mode) = 0;
      virtual void sendBestMatchAudio(bool bestmatchAudio) = 0;
      virtual void sendBestMatchPhoneBook(bool bestmatchPB) = 0;
      virtual void sendVoicePreference(voicePreferenceIndex enIndex) = 0;
};


}
}


#endif /* IVRSettingsHandler_h */
