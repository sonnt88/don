/*
 * SdsAdapterProvider.h
 *
 *  Created on: May 25, 2015
 *      Author: bee4kor
 */

#ifndef SdsAdapterProvider_h
#define SdsAdapterProvider_h

#include "App/Core/ListHandler/ListHandler.h"
#include "App/Core/Observers/ActiveApplicationModeObserver.h"
#include "App/Core/Observers/SmsHeaderVisibilityObserver.h"
#include "CgiExtensions/ListDataProviderDistributor.h"
#include "CourierTunnelService/CourierMessageReceiverStub.h"
#include "sds_gui_fi/PopUpServiceProxy.h"
#include "sds_gui_fi/SettingsServiceProxy.h"
#include "sds_gui_fi/SdsPhoneServiceProxy.h"


namespace App {
namespace Core {

class GuiUpdater;
class IHMIModelComponent;
class ISdsContextProvider;
class ISdsContextRequestor;
class SdsEventHandler;
class SdsSessionStartObserver;


class SdsAdapterProvider
   : public asf::core::ServiceAvailableIF
   , public sds_gui_fi::SdsPhoneService::SmsContentCallbackIF
   , public sds_gui_fi::PopUpService::MicrophoneLevelCallbackIF
   , public sds_gui_fi::PopUpService::PopupRequestCloseCallbackIF
   , public sds_gui_fi::PopUpService::PopupRequestCallbackIF
   , public ActiveApplicationModeObserver
   , public SmsHeaderVisibilityObserver
{
   public:
      SdsAdapterProvider(
         const ::boost::shared_ptr<sds_gui_fi::PopUpService::PopUpServiceProxy>& popUpServiceProxy,
         const ::boost::shared_ptr<sds_gui_fi::SettingsService::SettingsServiceProxy>& settingsServiceProxy,
         const ::boost::shared_ptr<sds_gui_fi::SdsPhoneService::SdsPhoneServiceProxy>& sdsPhoneServiceProxy,
         GuiUpdater* pGuiUpdater,
         IHMIModelComponent* pIHMIModelComponent,
         ListHandler* pListHandler);
      virtual ~SdsAdapterProvider();

      //////////////////////////////////////////Callbacks from SDS_Adapter///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      virtual void onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy, const asf::core::ServiceStateChange& stateChange);
      virtual void onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy, const asf::core::ServiceStateChange& stateChange);

      virtual void onPopupRequestSignal(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestSignal >& signal);
      virtual void onPopupRequestError(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestError >& error) ;

      virtual void onPopupRequestCloseSignal(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestCloseSignal >& signal);
      virtual void onPopupRequestCloseError(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestCloseError >& error);

      virtual void onSmsContentError(const ::boost::shared_ptr< sds_gui_fi::SdsPhoneService::SdsPhoneServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsPhoneService::SmsContentError >& error);
      virtual void onSmsContentSignal(const ::boost::shared_ptr< sds_gui_fi::SdsPhoneService::SdsPhoneServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsPhoneService::SmsContentSignal >& signal);

      virtual void onMicrophoneLevelError(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::PopUpService::MicrophoneLevelError >& error);
      virtual void onMicrophoneLevelUpdate(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::PopUpService::MicrophoneLevelUpdate >& update);

      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      /////////////////////////////// General Access Methods ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      void setContextRequestor(ISdsContextRequestor* poSdsContextRequestor);
      void setContextProvider(ISdsContextProvider* poSdsContextProvider);
      void setEventHandler(SdsEventHandler* pSdsEventHandler);
      void sdsHmiActiveAppModeChanged(unsigned int sdsHmiActiveAppMode);
      void vRegisterObservers(SdsSessionStartObserver* pSDSSessionObserver);

      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   private:
      ::boost::shared_ptr<sds_gui_fi::PopUpService::PopUpServiceProxy> _popUpServiceProxy;
      ::boost::shared_ptr<sds_gui_fi::SettingsService::SettingsServiceProxy> _settingsServiceProxy;
      ::boost::shared_ptr<sds_gui_fi::SdsPhoneService::SdsPhoneServiceProxy> _sdsPhoneServiceProxy;

      GuiUpdater* _pGuiUpdater;
      IHMIModelComponent* _pHMIModelComponent;
      ListHandler* _pListhandler;
      ISdsContextProvider* _pSdsContextProvider;
      ISdsContextRequestor* _pSdsContextRequestor;
      SdsEventHandler* _pSdsEventHandler;

      unsigned int _sdsHmiActiveAppMode;
      bool _isSmsHeaderVisible;
      std::vector<SdsSessionStartObserver*> _sdsSessionStartedObservers;

      void clearScreenData(unsigned char layoutType);
      enSdsScreenLayoutType getLayoutEnumValue(unsigned char layoutChar) const;
      unsigned char getLayout(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestSignal >& signal) const;
      void processHeaderData(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestSignal >& signal);
      unsigned int convertToTalkingHeadState(sds_gui_fi::PopUpService::SpeechInputState speechInputState);

      void processListData(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestSignal >& signal);
      void processLayout(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestSignal >& signal);
      void processSmsHeaderData(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestSignal >& signal);
      unsigned int percentageToValue(unsigned int percentage, unsigned int maxValue) const;

      void vNotifySDSSessionStartedToObservers();
      void vNotifySDSSessionEndToObesrvers();
      void vOnSmsHeaderVisibilityChanged(bool vehicleMovingSpeed);
};


} // Namespace Core
} // Namespace App


#endif /* SdsAdapterProvider_h */
