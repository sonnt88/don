/**
 *  @file   <SdsSessionStartObserver.cpp>
 *  @author <ECV2> - <A-IVI>
 *  @copyright (c) <2014> Robert Bosch Car Multimedia GmbH
 *  @addtogroup <Apphmi_Sds>
 */

#include "App/Core/Observers/SdsSessionStartObserver.h"


namespace App {
namespace Core {

SdsSessionStartObserver::~SdsSessionStartObserver()
{
}


SdsSessionStartObserver::SdsSessionStartObserver()
{
}


}// namespace Core
}// namespace App
