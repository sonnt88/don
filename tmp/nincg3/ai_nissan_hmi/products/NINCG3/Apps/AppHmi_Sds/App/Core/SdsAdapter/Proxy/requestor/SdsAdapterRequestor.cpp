/*
 * SdsAdapterRequestor.cpp
 *
 *  Created on: Feb 3, 2016
 *      Author: bee4kor
 */

#include "App/Core/SdsAdapter/Proxy/requestor/SdsAdapterRequestor.h"
#include "App/Core/SdsDefines.h"
#include "hmi_trace_if.h"


using namespace sds_gui_fi::SdsGuiService;


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SDS_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SDS
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SDS_"
#define ETG_I_FILE_PREFIX                 App::Core::SdsAdapterRequestor::
#include "trcGenProj/Header/SdsAdapterRequestor.cpp.trc.h"
#endif


namespace App {
namespace Core {


SdsAdapterRequestor::SdsAdapterRequestor(::boost::shared_ptr<sds_gui_fi::SdsGuiService::SdsGuiServiceProxy>& sdsGuiServProxy)
   : _sdsGuiServiceProxy(sdsGuiServProxy)
   , _highPriorityAppActive(false)
{
   ETG_TRACE_USR4(("SdsAdapterRequestor::Constructor"));
}


SdsAdapterRequestor::~SdsAdapterRequestor()
{
}


void SdsAdapterRequestor::sendPttShortPress()
{
   if (_highPriorityAppActive)
   {
      POST_MSG((COURIER_MESSAGE_NEW(PlayBeepReqMsg)(BEEPTYPE_ROGER)));
      ETG_TRACE_USR4(("sendPttShortPress HighPriority Application is active. Suppressing Ptt %d", _highPriorityAppActive));
   }
   else
   {
      ETG_TRACE_USR4(("sendPttShortPress HighPriority Application is not active. allowing Ptt %d", _highPriorityAppActive));
      _sdsGuiServiceProxy->sendPttPressRequest(*this, KeyState__KEY_HK_MFL_PTT_SHORT);
   }
}


void SdsAdapterRequestor::onPttPressError(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< PttPressError >& /*error*/)
{
}


void SdsAdapterRequestor::onPttPressResponse(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< PttPressResponse >& /*response*/)
{
}


void SdsAdapterRequestor::onTestModeUpdateError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::TestModeUpdateError >& /*error*/)
{
}


void SdsAdapterRequestor::onTestModeUpdateResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::TestModeUpdateResponse >& /*response*/)
{
}


void SdsAdapterRequestor::onStartSessionContextError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StartSessionContextError >& /*error*/)
{
}


void SdsAdapterRequestor::onStartSessionContextResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StartSessionContextResponse >& /*response*/)
{
}


void SdsAdapterRequestor::onManualOperationError(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< ManualOperationError >& /*error*/)
{
}


void SdsAdapterRequestor::onManualOperationResponse(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< ManualOperationResponse >& /*response*/)
{
}


void SdsAdapterRequestor::backKeyPressed()
{
   _sdsGuiServiceProxy->sendBackPressRequest(*this);
}


unsigned int SdsAdapterRequestor::sendTestModeSentence(std::string _textSentence)
{
   _sdsGuiServiceProxy->sendTestModeUpdateRequest(*this, _textSentence);
   return true;
}


unsigned int SdsAdapterRequestor::focusMoved(unsigned int _guiFocusIndex)
{
   ETG_TRACE_USR4(("SdsAdapterRequestor::focusMoved = %d", _guiFocusIndex));
   _sdsGuiServiceProxy->sendManualOperationRequest(*this, OperationType__MANUAL_INTERVENTION_ENCODER_FOCUS_MOVED, _guiFocusIndex);
   return true;
}


unsigned int SdsAdapterRequestor::listItemSelected(unsigned int _guiSelectedListItemIndex)
{
   ETG_TRACE_USR4(("SdsAdapterRequestor::listItemSelected = %d", _guiSelectedListItemIndex));
   _sdsGuiServiceProxy->sendManualOperationRequest(*this, OperationType__MANUAL_INTERVENTION_HAPTICAL_SELECTION, _guiSelectedListItemIndex);
   return true;
}


unsigned int SdsAdapterRequestor::requestNextPage()
{
   ETG_TRACE_USR4(("SdsAdapterRequestor::requestNextPage"));
   _sdsGuiServiceProxy->sendManualOperationRequest(*this, OperationType__MANUAL_INTERVENTION_NEXT_PAGE, false);
   return true;
}


unsigned int SdsAdapterRequestor::requestPrevPage()
{
   ETG_TRACE_USR4(("SdsAdapterRequestor::requestPrevPage"));
   _sdsGuiServiceProxy->sendManualOperationRequest(*this, OperationType__MANUAL_INTERVENTION_PREV_PAGE, false);
   return true;
}


unsigned int SdsAdapterRequestor::sendShortcutRequest(unsigned int _shortcutType)
{
   ETG_TRACE_USR4(("SdsAdapterRequestor::sendShortcutRequest"));
   _sdsGuiServiceProxy->sendStartSessionContextRequest(*this, mapGui2AdapterShortcutContext(_shortcutType));
   return true;
}


sds_gui_fi::SdsGuiService::ContextType SdsAdapterRequestor::mapGui2AdapterShortcutContext(unsigned int _guiContext)
{
   switch (_guiContext)
   {
      case SDS_CONTEXT_HOMESCREEN_CALL:
         return ContextType__SDS_CONTEXT_CALL;

      case SDS_CONTEXT_HOMESCREEN_DIALNUM:
         return ContextType__SDS_CONTEXT_DIALNUM;

      case SDS_CONTEXT_HOMESCREEN_CALLHIST:
         return ContextType__SDS_CONTEXT_CALLHIST;

      case SDS_CONTEXT_HOMESCREEN_QUICKDIAL:
         return ContextType__SDS_CONTEXT_QUICKDIAL;

      case SDS_CONTEXT_HOMESCREEN_READTEXT:
         return ContextType__SDS_CONTEXT_READTEXT;

      case SDS_CONTEXT_HOMESCREEN_SENDTEXT:
         return ContextType__SDS_CONTEXT_SENDTEXT;

      case SDS_CONTEXT_HOMESCREEN_STREETADDR:
         return ContextType__SDS_CONTEXT_STREETADDR;

      case SDS_CONTEXT_HOMESCREEN_POI:
         return ContextType__SDS_CONTEXT_POI;

      case SDS_CONTEXT_HOMESCREEN_POICAT:
         return ContextType__SDS_CONTEXT_POICAT;

      case SDS_CONTEXT_HOMESCREEN_INTERSECTION:
         return ContextType__SDS_CONTEXT_INTERSECTION;

      case SDS_CONTEXT_HOMESCREEN_CITYCENTER:
         return ContextType__SDS_CONTEXT_CITYCENTER;

      case SDS_CONTEXT_HOMESCREEN_PLAYALBUM:
         return ContextType__SDS_CONTEXT_PLAYALBUM;

      case SDS_CONTEXT_HOMESCREEN_PLAYARTIST:
         return ContextType__SDS_CONTEXT_PLAYARTIST;

      case SDS_CONTEXT_HOMESCREEN_PLAYSONG:
         return ContextType__SDS_CONTEXT_PLAYSONG;

      case SDS_CONTEXT_HOMESCREEN_PLAYLIST:
         return ContextType__SDS_CONTEXT_PLAYLIST;

      default:
         return ContextType__SDS_CONTEXT_CALL;
   }
}


void SdsAdapterRequestor::restoreFactorySDSSettings()
{
   //raa8hi: method RestoreFactorySettings was removed from sds_gui_fi.fidl
   //SdsAdapter doesn't need to have any specific handling for this;
   //instead, SdsAdaper will receive notifications in case of any settings changes

//   _sdsGuiServiceProxy->sendRestoreFactorySettingsRequest(*this);
}


bool SdsAdapterRequestor::onCourierMessage(const BtnSettingsRelease& /*msg*/)
{
   ETG_TRACE_USR4(("SdsAdapterRequestor::BtnSettingsRelease - received "));
   _sdsGuiServiceProxy->sendSettingsCommandRequest(*this);
   return true;
}


bool SdsAdapterRequestor::onCourierMessage(const BtnHelpRelease& /*msg*/)
{
   ETG_TRACE_USR4(("SdsAdapterRequestor::BtnHelpRelease - received "));
   _sdsGuiServiceProxy->sendHelpCommandRequest(*this);
   return true;
}


void SdsAdapterRequestor::onSettingsCommandError(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< SettingsCommandError >& /*error*/)
{
}


void SdsAdapterRequestor::onSettingsCommandResponse(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< SettingsCommandResponse >& /*response*/)
{
}


void SdsAdapterRequestor::sendStopSession()
{
   ETG_TRACE_USR4(("SdsAdapterRequestor::sendstopsession on HKPress "));
   _sdsGuiServiceProxy->sendStopSessionRequest(*this);
}


void SdsAdapterRequestor::sendAbortSession()
{
   ETG_TRACE_USR4(("SdsAdapterRequestor::sendAbortSession "));
   _sdsGuiServiceProxy->sendAbortSessionRequest(*this);
}


void SdsAdapterRequestor::sendPauseSession()
{
   ETG_TRACE_USR4(("SdsAdapterRequestor::sendPauseSession "));
   _sdsGuiServiceProxy->sendPauseSessionRequest(*this);
}


void SdsAdapterRequestor::sendResumeSession()
{
   ETG_TRACE_USR4(("SdsAdapterRequestor::sendResumeSession "));
   _sdsGuiServiceProxy->sendResumeSessionRequest(*this);
}


void SdsAdapterRequestor::setHighPriorityAppStatus(bool highPriorityAppActive)
{
   _highPriorityAppActive = highPriorityAppActive;
}


void SdsAdapterRequestor::onBackPressError(const ::boost::shared_ptr< SdsGuiServiceProxy >& /* proxy */, const ::boost::shared_ptr< BackPressError >& /* error */)
{
}


void SdsAdapterRequestor::onBackPressResponse(const ::boost::shared_ptr< SdsGuiServiceProxy >& /* proxy */, const ::boost::shared_ptr< BackPressResponse >& /*response */)
{
}


void SdsAdapterRequestor::onStopSessionError(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy */, const ::boost::shared_ptr< StopSessionError >& /*error */)
{
}


void SdsAdapterRequestor::onStopSessionResponse(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy */, const ::boost::shared_ptr< StopSessionResponse >& /*response*/)
{
}


void SdsAdapterRequestor::onAbortSessionError(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy */, const ::boost::shared_ptr< AbortSessionError >& /*error*/)
{
}


void SdsAdapterRequestor::onAbortSessionResponse(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy */, const ::boost::shared_ptr< AbortSessionResponse >& /*response*/)
{
}


void SdsAdapterRequestor::onPauseSessionError(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy */, const ::boost::shared_ptr<PauseSessionError >& /*error*/)
{
}


void SdsAdapterRequestor::onPauseSessionResponse(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< PauseSessionResponse >& /*response*/)
{
}


void SdsAdapterRequestor::onResumeSessionError(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< ResumeSessionError >& /*error*/)
{
}


void SdsAdapterRequestor::onResumeSessionResponse(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< ResumeSessionResponse >& /*response*/)
{
}


void SdsAdapterRequestor::onHelpCommandError(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< HelpCommandError >& /*error*/)
{
}


void SdsAdapterRequestor::onHelpCommandResponse(const ::boost::shared_ptr< SdsGuiServiceProxy >& /*proxy*/, const ::boost::shared_ptr< HelpCommandResponse >& /*response*/)
{
}


}//Core
}//App
