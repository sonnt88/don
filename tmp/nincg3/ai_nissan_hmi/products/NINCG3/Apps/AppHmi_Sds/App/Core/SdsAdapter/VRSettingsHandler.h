/*
 * VRSettingsHandler.h
 *
 *  Created on:
 *      Author:
 */

#ifndef VRSettingsHandler_h
#define VRSettingsHandler_h

#include "sds_gui_fi/SettingsServiceProxy.h"
#include "App/Core/Interfaces/IVRSettingsHandler.h"
#include "App/Core/SdsDefines.h"
#include "CgiExtensions/ListDataProviderDistributor.h"
#include "Common/ListHandler/ListRegistry.h"
#include "CourierTunnelService/CourierMessageReceiverStub.h"


namespace App {
namespace Core {

class GuiUpdater;

class VRSettingsHandler
   : public asf::core::ServiceAvailableIF
   , public sds_gui_fi::SettingsService::BeepOnlyModeCallbackIF
   , public sds_gui_fi::SettingsService::ShortPromptCallbackIF
   , public sds_gui_fi::SettingsService::BestMatchAudioCallbackIF
   , public sds_gui_fi::SettingsService::BestMatchPhoneBookCallbackIF
   , public sds_gui_fi::SettingsService::VoicePreferenceCallbackIF
   , public IVRSettingsHandler
{
   public:
      VRSettingsHandler(
         ::boost::shared_ptr<sds_gui_fi::SettingsService::SettingsServiceProxy>& settingsServiceProxy,
         GuiUpdater* pGuiUpdater);
      virtual ~VRSettingsHandler();

      //////////////////////////////////////////Callbacks from SDS_Adapter///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      virtual void onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange);
      virtual void onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange);

      virtual void onBeepOnlyModeError(const ::boost::shared_ptr< sds_gui_fi::SettingsService::SettingsServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SettingsService::BeepOnlyModeError >& error);
      virtual void onBeepOnlyModeUpdate(const ::boost::shared_ptr< sds_gui_fi::SettingsService::SettingsServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SettingsService::BeepOnlyModeUpdate >& update);

      virtual void onShortPromptError(const ::boost::shared_ptr< sds_gui_fi::SettingsService::SettingsServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SettingsService::ShortPromptError >& error);
      virtual void onShortPromptUpdate(const ::boost::shared_ptr< sds_gui_fi::SettingsService::SettingsServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SettingsService::ShortPromptUpdate >& update);

      virtual void onBestMatchAudioError(const ::boost::shared_ptr< sds_gui_fi::SettingsService::SettingsServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SettingsService::BestMatchAudioError >& error);
      virtual void onBestMatchAudioUpdate(const ::boost::shared_ptr< sds_gui_fi::SettingsService::SettingsServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SettingsService::BestMatchAudioUpdate >& update);

      virtual void onBestMatchPhoneBookError(const ::boost::shared_ptr< sds_gui_fi::SettingsService::SettingsServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SettingsService::BestMatchPhoneBookError >& error);
      virtual void onBestMatchPhoneBookUpdate(const ::boost::shared_ptr< sds_gui_fi::SettingsService::SettingsServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SettingsService::BestMatchPhoneBookUpdate >& update);

      virtual void onVoicePreferenceError(const ::boost::shared_ptr< sds_gui_fi::SettingsService::SettingsServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SettingsService::VoicePreferenceError >& error);
      virtual void onVoicePreferenceUpdate(const ::boost::shared_ptr< sds_gui_fi::SettingsService::SettingsServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SettingsService::VoicePreferenceUpdate >& update);

      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////General Access Methods////////////////////////////////////////////////////////////////////////////////////////////////////////////
      tSharedPtrDataProvider getSettingsMainList();
      tSharedPtrDataProvider getBestMatchList();
      Candera::String getItemTextBestMatch(const enBestMatchList Item) const;
      void initializeMembers();
      void processSettingsMainListItems(ListProviderEventInfo info, const ButtonReactionMsg& oMsg);
      void processBestMatchListItems(ListProviderEventInfo info, const ButtonReactionMsg& oMsg);
      void onToggleBeepOnlyModeButtonReactionStatus();
      void onToggleShortPromptButtonReactionStatus();
      void onToggleVoicePreferenceButtonReactionStatus();
      voicePreferenceIndex adapter2HallVoicePreferenceIndex(sds_gui_fi::SettingsService::ActiveSpeakerGender speakerGender);
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      ///////////////////////////////////////////Requests to SDS_Adapter//////////////////////////////////////////////////////////////////////////////////////////////////////////////
      void sendBeepOnlyMode(bool mode);
      void sendShortPrompt(bool mode);
      void sendBestMatchAudio(bool bestmatchAudio);
      void sendBestMatchPhoneBook(bool bestmatchPB);
      void sendVoicePreference(voicePreferenceIndex enIndex);
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////// Courier Message Mapping with Traces classes ///////////////////////////////////////////////////////////////////////////////////
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_SDS_COURIER_PAYLOAD_MODEL_COMP)
      ON_COURIER_MESSAGE(BtnYesRelease)
      ON_COURIER_MESSAGE(BtnNoRelease)
      COURIER_MSG_MAP_END()
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      //////////The below onCourierMessage funtions are the various action perform & model subscribed courier messages from SM ////////////////////////////////////////////////////
      bool onCourierMessage(const BtnYesRelease& msg);
      bool onCourierMessage(const BtnNoRelease& msg);
      /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   private:
      ::boost::shared_ptr<sds_gui_fi::SettingsService::SettingsServiceProxy> _settingsServiceProxy;
      GuiUpdater* _pGuiUpdater;

      bool _beepOnlyModeState;
      bool _bestMatchAudioState;
      bool _bestMatchPhoneBookState;
      bool _shortPromptState;
      std::vector <Candera::String> _vecVoicePreference;
      voicePreferenceIndex _voicePreferenceIndex;
};


} // Namespace Core
} // Namespace App


#endif /* VRSettingsHandler_h */
