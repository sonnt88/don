/*
 * SdsTTfisTestCommandHandler.cpp
 *
 *  Created on: Jan 26, 2016
 *      Author: bee4kor
 */

#include "App/Core/TTFisCommander/SdsTTfisTestCommandHandler.h"
#include "App/Core/Interfaces/ISdsAdapterRequestor.h"
#include "App/Core/Interfaces/ISdsContextRequestor.h"
#include "App/Core/Interfaces/IVRSettingsHandler.h"
#include "App/Core/Interfaces/IListHandler.h"
#include "App/Core/SdsAdapter/GuiUpdater.h"
#include "App/Core/SdsDefines.h"
#include "hmi_trace_if.h"
#include <sstream>


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SDS_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SDS
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SDS_"
#define ETG_I_FILE_PREFIX                 App::Core::SdsTTfisTestCommandHandler::
#include "trcGenProj/Header/SdsTTfisTestCommandHandler.cpp.trc.h"
#endif


namespace App {
namespace Core {

SdsTTfisTestCommandHandler* poSdsTTfisTestCommandHandler;

SdsTTfisTestCommandHandler::SdsTTfisTestCommandHandler(
   ISdsAdapterRequestor* pAdapterRequestor,
   ISdsContextRequestor* pSdsContextRequestor,
   GuiUpdater* pGuiUpdater,
   IVRSettingsHandler* pVRSettingshandler,
   IListHandler* pListHandler)
   :  _pAdapterRequestor(pAdapterRequestor)
   , _pSdsContextRequestor(pSdsContextRequestor)
   , _pGuiUpdater(pGuiUpdater)
   , _pVRSettingshandler(pVRSettingshandler)
   , _pListHandler(pListHandler)
{
   ETG_TRACE_USR4(("SdsTTfisTestCommandHandler::Constructor"));
   ETG_I_REGISTER_FILE();
   poSdsTTfisTestCommandHandler = this;
}


ETG_I_CMD_DEFINE((SdsTTfisTestCommandHandler::ttfisTestCommand, "ttfisTestCommand %d", unsigned int))
void SdsTTfisTestCommandHandler::ttfisTestCommand(unsigned int param)
{
   ETG_TRACE_USR4(("SdsTTfisTestCommandHandler: ttfisTestCommand received - %d", param));
   poSdsTTfisTestCommandHandler->testCommandMethod(param);
}


void SdsTTfisTestCommandHandler::testCommandMethod(unsigned int param)
{
   switch (param)
   {
      case 0:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Self Context Switch"));
         if (_pSdsContextRequestor)
         {
            _pSdsContextRequestor->contextSwitchSelf();
         }
         return;
      }

      case 1:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Show LAYOUT_R"));
         if (_pSdsContextRequestor)
         {
            _pListHandler->setShowScreenLayout(LAYOUT_R);
            _pSdsContextRequestor->contextSwitchSelf();
         }
         return;
      }

      case 2:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Show LAYOUT_N"));
         if (_pListHandler && _pSdsContextRequestor)
         {
            _pListHandler->setShowScreenLayout(LAYOUT_N);
            _pSdsContextRequestor->contextSwitchSelf();
         }
         return;
      }

      case 3:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Show LAYOUT_L"));
         if (_pListHandler && _pSdsContextRequestor)
         {
            _pListHandler->setShowScreenLayout(LAYOUT_L);
            _pSdsContextRequestor->contextSwitchSelf();
         }
         return;
      }

      case 4:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Show LAYOUT_C"));
         if (_pListHandler && _pSdsContextRequestor)
         {
            _pListHandler->setShowScreenLayout(LAYOUT_C);
            _pSdsContextRequestor->contextSwitchSelf();
         }
         return;
      }

      case 5:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Show LAYOUT_S"));
         if (_pListHandler && _pSdsContextRequestor)
         {
            _pListHandler->setShowScreenLayout(LAYOUT_S);
            _pSdsContextRequestor->contextSwitchSelf();
         }
         return;
      }

      case 6:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Show LAYOUT_M"));
         if (_pListHandler && _pSdsContextRequestor)
         {
            _pListHandler->setShowScreenLayout(LAYOUT_M);
            _pSdsContextRequestor->contextSwitchSelf();
         }
         return;
      }

      case 7:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Show LAYOUT_E"));
         if (_pListHandler && _pSdsContextRequestor)
         {
            _pListHandler->setShowScreenLayout(LAYOUT_E);
            _pSdsContextRequestor->contextSwitchSelf();
         }
         return;
      }

      case 8:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Show LAYOUT_Q"));
         if (_pListHandler && _pSdsContextRequestor)
         {
            _pListHandler->setShowScreenLayout(LAYOUT_Q);
            _pSdsContextRequestor->contextSwitchSelf();
         }
         return;
      }

      case 9:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Show LAYOUT_O"));
         if (_pListHandler && _pSdsContextRequestor)
         {
            _pListHandler->setShowScreenLayout(LAYOUT_O);
            _pSdsContextRequestor->contextSwitchSelf();
         }
         return;
      }

      case 10:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Show LAYOUT_T"));
         if (_pListHandler && _pSdsContextRequestor)
         {
            _pListHandler->setShowScreenLayout(LAYOUT_T);
            _pSdsContextRequestor->contextSwitchSelf();
         }
         return;
      }

      case 11:
      {
         if (_pListHandler && _pSdsContextRequestor)
         {
            _pListHandler->setShowScreenLayout(SDS_Settings_Main);
            _pSdsContextRequestor->contextSwitchSelf();
         }
         return;
      }

      case 110:
      {
         if (_pListHandler && _pSdsContextRequestor)
         {
            _pListHandler->setShowScreenLayout(SDS_Test_Mode);
            _pSdsContextRequestor->contextSwitchSelf();
         }
         return;
      }

      case 50:
      {
         ETG_TRACE_USR4(("Show SdsPopups_Popups_SDS_VR_MPOP_EARLY_START"));
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, SdsPopups_Popups_SDS_VR_MPOP_EARLY_START)));

         return;
      }

      case 51:
      {
         //////////////////// To Be Moved to Early SDS Startup Handler //////////////////////////////////////////////////////////////////////////////////////
         ETG_TRACE_USR4(("Early Sds Startup: Play File-/opt/bosch/base/bin/sds_earlystartup_en.mp3"));
         system("gst-launch -v filesrc location='/opt/bosch/base/bin/sds_earlystartup_en.mp3' ! decodebin2 ! audioconvert ! alsasink device='AdevEnt2Out'");
         ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         return;
      }

      case 55:
      {
         ETG_TRACE_USR4(("Show SdsPopups_Popups_SDS_VR_MPOP_VR_INIT"));
         POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, SdsPopups_Popups_SDS_VR_MPOP_VR_INIT)));

         return;
      }
      case 100:
      {
         ETG_TRACE_USR4(("_pAdapterRequestor->sendPttShortPress()"));
         if (_pAdapterRequestor)
         {
            _pAdapterRequestor->sendPttShortPress();
         }
         return;
      }
      case 12:
      {
         ETG_TRACE_USR4(("updateInfoTextData()"));
         std::stringstream headerStreamString;
         headerStreamString << "Say the Street";
         headerStreamString << std::endl;
         headerStreamString << "Say the Street";
         headerStreamString << std::endl;
         headerStreamString << "Say the Street";

         if (_pGuiUpdater)
         {
            _pGuiUpdater->updateInfoTextData(headerStreamString.str().c_str());
         }
         return;
      }
      case 13:
      {
         ETG_TRACE_USR4(("Update Headline & Helpline"));
         std::stringstream headerStreamString;
         headerStreamString << "Destination";
         headerStreamString << std::endl;
         headerStreamString << "Grand Central Station West Gate.";
         if (_pGuiUpdater)
         {
            _pGuiUpdater->updateHelpLineData("Say or select a category using the Steering Wheel Control.");
            _pGuiUpdater->updateHeaderTextData(headerStreamString.str().c_str());
         }
         return;
      }

      case 22:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: SDS_Settings_BestMatch"));
         if (_pListHandler && _pSdsContextRequestor)
         {
            _pListHandler->setShowScreenLayout(SDS_Settings_BestMatch);
            _pSdsContextRequestor->contextSwitchSelf();
         }
         return;
      }

      case 23:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Set Beep Only Mode-TRUE"));
         if (_pVRSettingshandler)
         {
            _pVRSettingshandler->sendBeepOnlyMode(true);
         }
         return;
      }

      case 24:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Set Beep Only Mode-FALSE"));
         if (_pVRSettingshandler)
         {
            _pVRSettingshandler->sendBeepOnlyMode(false);
         }
         return;
      }

      case 25:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Initialize TTFIS Test Vector Data"));
         if (_pListHandler)
         {
            _pListHandler->getVectCmdString().clear();
            _pListHandler->getVectSubCmdString().clear();
            _pListHandler->getVectDescString().clear();
            _pListHandler->getVectPriceAgeString().clear();
            _pListHandler->getVectPriceString().clear();
            _pListHandler->getVectDistString().clear();
            _pListHandler->getVectDirectionString().clear();

            SelectableListItem selItem;
            selItem.isSelectable = true;
            selItem.text = "Command";

            _pListHandler->getVectCmdString() 	= std::vector<SelectableListItem>(10, selItem);
            _pListHandler->getVectCmdString().at(0).isSelectable = false;
            _pListHandler->getVectSubCmdString()	= std::vector <std::string>(7, "SubCommand");
            _pListHandler->getVectDescString()	= std::vector <std::string>(10, "Description");
            _pListHandler->getVectPriceAgeString() = std::vector <std::string>(10, "Today");
            _pListHandler->getVectPriceString() = std::vector <std::string>(10, "$7.7");
            _pListHandler->getVectDistString() = std::vector <std::string>(10, "1.5mi");
            _pListHandler->getVectDirectionString() = std::vector <std::string>(10, "4");
         }
         return;
      }
      case 26:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Setshort prompt to TRUE"));
         if (_pVRSettingshandler)
         {
            _pVRSettingshandler->sendShortPrompt(true);
         }
         return;
      }

      case 27:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Setshort prompt to false"));
         if (_pVRSettingshandler)
         {
            _pVRSettingshandler->sendShortPrompt(false);
         }
         return;
      }

      case 28:
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Best match list for audio to true"));
         if (_pVRSettingshandler)
         {
            _pVRSettingshandler->sendBestMatchAudio(true);
         }
         return;
      case 30:
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Best match list for audio to false"));
         if (_pVRSettingshandler)
         {
            _pVRSettingshandler->sendBestMatchAudio(false);
         }
         return;
      case 31:
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Best match list for audio to true"));
         if (_pVRSettingshandler)
         {
            _pVRSettingshandler->sendBestMatchPhoneBook(false);
         }
         return;
      case 32:
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Voice Preference to Female"));
         if (_pVRSettingshandler)
         {
            _pVRSettingshandler->sendVoicePreference(INDEX_FEMALE);
         }
         return;
      case 33 :
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: Voice Preference to Male"));
         if (_pVRSettingshandler)
         {
            _pVRSettingshandler->sendVoicePreference(INDEX_MALE);
         }
         return;

      default:
      {
         ETG_TRACE_USR4(("SdsTTfisTestCommandHandler:: default case"));
         return;
      }
   }
}


SdsTTfisTestCommandHandler::~SdsTTfisTestCommandHandler()
{
   PURGE(_pAdapterRequestor);
   PURGE(_pSdsContextRequestor);
   PURGE(_pGuiUpdater);
   PURGE(_pVRSettingshandler);
   PURGE(_pListHandler);
}


}
}
