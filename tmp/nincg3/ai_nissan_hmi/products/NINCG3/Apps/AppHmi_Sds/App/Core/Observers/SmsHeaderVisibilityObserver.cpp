/**
 *  @file   <SmsHeaderVisibilityObserver.cpp>
 *  @author <ECV2> - <A-IVI>
 *  @copyright (c) <2014> Robert Bosch Car Multimedia GmbH
 *  @addtogroup <Apphmi_Sds>
 */

#include "App/Core/Observers/SmsHeaderVisibilityObserver.h"

namespace App {
namespace Core {


SmsHeaderVisibilityObserver:: ~SmsHeaderVisibilityObserver()
{
}


SmsHeaderVisibilityObserver::SmsHeaderVisibilityObserver()
{
}


}// Namespace Core
}// Namespace App
