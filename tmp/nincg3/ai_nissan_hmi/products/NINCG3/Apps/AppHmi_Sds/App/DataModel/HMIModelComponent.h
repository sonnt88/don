/**
*  @file   <HMIModelComponent.h>
*  @copyright (c) <2014> Robert Bosch Car Multimedia GmbH
*  @addtogroup <AppHmi_Sds>
*  @aGeneralInformation:  This class contains the various SDS HMI State Machine State & DataModel Variable Information to be used by Sds Hmi Model classes
*/

#if !defined(_HMI_MODEL_COMPONENT_H)
#define _HMI_MODEL_COMPONENT_H
#include "CgiExtensions/ListDataProviderDistributor.h"
#include "AppHmi_SdsStateMachine.h"
#include "App/Core/Interfaces/IHMIModelComponent.h"


namespace App {
namespace Core {

class HMIModelComponent
   : public IHMIModelComponent
{
   public:
      HMIModelComponent();
      virtual ~HMIModelComponent();

      COURIER_MSG_MAP_BEGIN(0)
      COURIER_DUMMY_CASE(0)
      ON_COURIER_MESSAGE(UpdateSdsHmiAppMode)
      COURIER_MSG_MAP_END()

      bool onCourierMessage(const UpdateSdsHmiAppMode& msg);
      void registerSdsApplicationModeObserver(ActiveApplicationModeObserver* pActiveApplicationModeObserver) ;

   private:
      unsigned int _sdsHmiActiveAppMode;
      std::vector<ActiveApplicationModeObserver*> _appModeObserver;
      void notifySdsHmiApplicationModeObservers();
};


}
}


#endif // _HMI_MODEL_COMPONENT_H
