/*
 * IListHandler.h
 *
 */

#ifndef IListHandler_h
#define IListHandler_h

#include "App/Core/SdsHallTypes.h"

namespace App {
namespace Core {


class IListHandler
{
   public:
      virtual ~IListHandler() {};
      virtual void setShowScreenLayout(enSdsScreenLayoutType screenLayout) = 0;
      virtual enSdsScreenLayoutType getCurrentLayout() = 0;
      virtual std::vector <SelectableListItem>& getVectCmdString() = 0;
      virtual std::vector <std::string>& getVectSubCmdString() = 0;
      virtual std::vector <std::string>& getVectDescString() = 0;
      virtual std::vector <std::string>& getVectDistString() = 0;
      virtual std::vector <std::string>& getVectPriceAgeString() = 0;
      virtual std::vector <std::string>& getVectPriceString() = 0;
      virtual std::vector <std::string>& getVectDirectionString() = 0;
};


}
}


#endif /* IListHandler_h */
