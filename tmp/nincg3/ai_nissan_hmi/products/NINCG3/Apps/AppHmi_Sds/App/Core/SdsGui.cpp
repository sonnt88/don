/****************************************************************************
 * Copyright (C) Robert Bosch Car Multimedia GmbH, 2013
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ***************************************************************************/
#include "gui_std_if.h"

#include "SdsGui.h"
#include "HMI/CGIComponents/AppViewSettings.h"
#include "HMI/CGIComponents/CGIApp.h"
#include "AppUtils/Trace/GuiInfo.h"
#include "AppBase/IApplicationSettings.h"
#include "Common/AppBaseSettings/KeyMapping.h"
#include "Common/AssetShaperUtils/AssetShaperUtil.h"

//////// TRACE IF ///////////////////////////////////////////////////
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SDS_MAIN
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SDS
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SDS_"
#define ETG_I_FILE_PREFIX                 App::Core::SdsGui::
#include "trcGenProj/Header/SdsGui.cpp.trc.h"
#endif // VARIANT_S_FTR_ENABLE_TRC_GEN

DEFAULT_APPSETTINGS(SURFACEID_MAIN_SURFACE_SDS, SURFACEID_NONE, SURFACEID_NONE, SURFACEID_NONE)

using namespace ::hmi;

namespace App {
namespace Core {


SdsGui::SdsGui() : GuiComponentBase(hmi::apps::appHmi_Sds, appSettings)
{
   ETG_I_REGISTER_FILE();
   ETG_I_REGISTER_CHN(TraceCmd_NotProcessedMsg);
   _hmiAppCtrlProxyHandler.setTraceId(TR_CLASS_APPHMI_SDS_APPCTRL_PROXY);
   ETG_TRACE_FATAL(("IN SdsGui Constructor"));
}


SdsGui::~SdsGui()
{
   hmibase::utils::trace::GuiInfo::setViewHandler(NULL);
}


unsigned int SdsGui::getDefaultTraceClass()
{
   ETG_TRACE_FATAL(("SdsGui::getDefaultTraceClass"));
   return TR_CLASS_APPHMI_SDS_MAIN;
}


void SdsGui::setupCgiInstance()
{
   ETG_TRACE_FATAL(("SdsGui::setupCgiInstance"));
   std::string assetFileName = Rnaivi::AssetShaperUtil::getDefaultAssetFileName();
   std::vector<std::string> assetFileNames = Rnaivi::AssetShaperUtil::getAssetFiles(assetFileName);
   setCgiApp(new CGIApp(assetFileNames, _hmiAppCtrlProxyHandler));

   //setCgiApp(new CGIApp(assetFileName.c_str(), _hmiAppCtrlProxyHandler));
}


void SdsGui::preRun()
{
   //PersistentValuesRead(); //TO be used when persistent values are needed
}


void SdsGui::postRun()
{
   //PersistentValuesWrite(); TO be used when persistent values are needed
}


void SdsGui::PersistentValuesRead()
{
}


void SdsGui::PersistentValuesWrite()
{
}


void SdsGui::TraceCmd_NotProcessedMsg(const unsigned char* data)
{
   if (data)
   {
      ETG_TRACE_FATAL(("TraceCmd_NotProcessedMsg() : 0x%*x", ETG_LIST_LEN(data[0]), ETG_LIST_PTR_T8(data)));
   }
}


} // namespace Core
} // namespace App
