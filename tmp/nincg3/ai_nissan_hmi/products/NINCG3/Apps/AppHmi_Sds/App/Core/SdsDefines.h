/**
* @file <SdsDefines.h>
* @author <bee4kor> <A-IVI>
* @copyright (c) <2015> Robert Bosch Car Multimedia GmbH
* @addtogroup <AppHmi_Sds>
*/


#ifndef SDSDEFINES_HEADER
#define SDSDEFINES_HEADER

namespace App {
namespace Core {


#define MAX_LIST_ITEMS_LAYOUT_N 10
#define MAX_LIST_ITEMS_LAYOUT_R 4
#define MAX_LIST_ITEMS_VL_SUBCMD 7
#define MAX_LIST_ITEMS_LAYOUT_C 2
#define MAX_LIST_ITEMS_LAYOUT_L 5
#define MAX_LIST_ITEMS_LAYOUT_M 5
#define MAX_LIST_ITEMS_LAYOUT_MAIL 4
#define MAX_LIST_ITEMS_LAYOUT_S 3
#define MAX_LIST_ITEMS_SDS_Settings_Main 6
#define MAX_LIST_ITEMS_LAYOUT_E 5
#define MAX_LIST_ITEMS_LAYOUT_T 3
#define MAX_LIST_ITEMS_LAYOUT_O 1
#define MAX_LIST_ITEMS_LAYOUT_Q 5
#define KMPH_TO_MILES_CONVERSION_VALUE 161
#define MAX_VEHICLE_SPEED_IN_MILES 5
#define MAX_VEHICLE_SPEED_SMS_HEADER_VISIBILITY 65535

#define DELETE_AND_PURGE(member)    if(member) delete member; member = NULL
#define PURGE(member)    member = NULL
#define BEEPTYPE_ROGER 2


#define TAGNAME_CMD "cmd"
#define TAGNAME_CMD_SIZE 3
#define TAGNAME_DESC "descr"
#define TAGNAME_DESC_SIZE 5
#define TAGNAME_SUBCMD "subCmd"
#define TAGNAME_SUBCMD_SIZE 6
#define TAGNAME_HELP "help"
#define TAGNAME_HELP_SIZE 4
#define TAGNAME_INFO "info"
#define TAGNAME_INFO_SIZE 4

#define TAGNAME_DIST "dist"
#define TAGNAME_DIST_SIZE 4
#define TAGNAME_DIR "dir"
#define TAGNAME_DIR_SIZE 3
#define TAGNAME_PRICE "price"
#define TAGNAME_PRICE_SIZE 5
#define TAGNAME_PRICE_AGE "priceU"
#define TAGNAME_PRICE_AGE_SIZE 6

#define MAX_MIC_LEVEL 14
}


}

#endif //SSDDEFINES_HEADER
