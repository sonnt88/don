/**
 *  @file   <HmiVehicleHandler.h>
 *  @author <ECV2> - <A-IVI>
 *  @copyright (c) <2014> Robert Bosch Car Multimedia GmbH
 *  @addtogroup <Apphmi_Sds>
 */

#ifndef HmiVehicleHandler_h
#define HmiVehicleHandler_h

#include "App/Core/SdsHallTypes.h"
#include "VEHICLE_MAIN_FIProxy.h"
#include "asf/core/Proxy.h"
#include "App/Core/Observers/SdsSessionStartObserver.h"
#include "App/Core/SdsDefines.h"
namespace App {
namespace Core {

class SmsHeaderVisibilityObserver;


class HmiVehicleHandler
   : public asf::core::ServiceAvailableIF
   , public SdsSessionStartObserver
   , public VEHICLE_MAIN_FI::SpeedCallbackIF
{
   public:
      virtual ~ HmiVehicleHandler();
      HmiVehicleHandler();

      virtual void onAvailable(const boost::shared_ptr<asf::core::Proxy>& proxy, const asf::core::ServiceStateChange& stateChange);
      virtual void onUnavailable(const boost::shared_ptr<asf::core::Proxy>& proxy, const asf::core::ServiceStateChange& stateChange);

      virtual void onSpeedError(const ::boost::shared_ptr<  VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, const ::boost::shared_ptr<  VEHICLE_MAIN_FI::SpeedError >& error);
      virtual void onSpeedStatus(const ::boost::shared_ptr<  VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy >& proxy, const ::boost::shared_ptr<  VEHICLE_MAIN_FI::SpeedStatus >& status);

      void vRegisterObservers(SmsHeaderVisibilityObserver* pSmsHeaderVisibilityObserver);

   private:
      void registerProperties();
      void deregisterProperties()const;
      void onSDSSessionStarted();
      void onSDSSessionEnd();
      void vNotifySmsHeaderVisibilityToObservers(bool vehicleMovingSpeed);
      bool _vehicleProxyAvailable;
      boost::shared_ptr< VEHICLE_MAIN_FI::VEHICLE_MAIN_FIProxy > _vehicleProxy;
      std::vector<SmsHeaderVisibilityObserver*> _observers;
};


}
}


#endif
