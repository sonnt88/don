/****************************************************************************
 * Copyright (C) Robert Bosch Car Multimedia GmbH, 2016
 * This software is property of Robert Bosch GmbH. Unauthorized
 * duplication and disclosure to third parties is prohibited.
 ***************************************************************************/

#ifndef GuiUpdater_h
#define GuiUpdater_h

#include "App/Core/SdsHallTypes.h"
#include "App/Core/ContextSwitch/Proxy/provider/AppSds_ContextProvider.h"
#include "Common/ListHandler/ListRegistry.h"
#include "CourierTunnelService/CourierMessageReceiverStub.h"

namespace App {
namespace Core {

class GuiUpdater
{
   public:
      GuiUpdater();
      virtual ~GuiUpdater();

      void updateMicrophoneLevelData(unsigned int micLevel) const;
      void updateHeaderSpeakingIconIndex(unsigned int index) const;
      void updateHeaderTextData(std::string headerString);
      void updateHelpLineData(std::string helpString);
      void updateHelp3List(std::string headerString1, std::string headerString2, std::string headerString3) const;
      void updateHelpList(std::string headerString1) const;
      void updateInfoTextData(std::string infoString) const;
      void updateSmsMessageContent(Candera::String smsMessage) const;
      void updateSubCommandList(const std::vector <std::string>& vecSubCommandString) const;
      bool updateDynamicList(unsigned int listId);

   private:
      Courier::DataItemContainer<SdsHeaderTextDataBindingSource> _headerTextData;
};


} // namespace Core
} // namespace App

#endif /* GuiUpdater_h */
