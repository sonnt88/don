/*
 * SdsHmiLanguageHandler.h
 *
 *  Created on: Apr 4, 2016
 *      Author: heg6kor
 */

#ifndef SdsHmiLanguageHandler_h
#define SdsHmiLanguageHandler_h

#include "CgiExtensions/ListDataProviderDistributor.h"
#include "App/Core/SdsHallTypes.h"
#include "VehicleDataHandler.h"


namespace App {
namespace Core {

class SdsHmiLanguageHandler : public VehicleDataCommonHandler::iLanguageUpdate
{
   public:
      SdsHmiLanguageHandler();
      virtual ~SdsHmiLanguageHandler();

      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_SDS_COURIER_PAYLOAD_MODEL_COMP)
      COURIER_MSG_MAP_DELEGATE_START()
      COURIER_MSG_DELEGATE_TO_OBJ(_languageHandler)
      COURIER_MSG_MAP_DELEGATE_END()

      //override the virtual functions to read and write DP language Specific to SDS HALL
      virtual uint8 readLanguageOnStartUp();
      virtual void writeLanguageintoDP(uint8);

   private:
      VehicleDataCommonHandler::VehicleDataHandler* _languageHandler;
};


} // namespace Core
} // namespace App

#endif /* SdsHmiLanguageHandler_h */
