///////////////////////////////////////////////////////////
//  AppSds_ContextRequestor.cpp
//  Implementation of the Class AppSds_ContextRequestor
//  Created on:      13-Jul-2015 11:44:47 AM
//  Original author: pad1cob
///////////////////////////////////////////////////////////


#include "AppSds_ContextRequestor.h"
#include "AppHmi_SdsMessages.h"

using namespace App::Core;
using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;


AppSds_ContextRequestor::AppSds_ContextRequestor()
{
}


AppSds_ContextRequestor::~AppSds_ContextRequestor()
{
}


bool AppSds_ContextRequestor::onCourierMessage(const ContextSwitchOutReqMsg& msg)
{
   requestContextSwitch(msg.GetSourceContextId(), msg.GetTargetContextId());
   return true;
}


void AppSds_ContextRequestor::requestContextSwitch(const unsigned int appId, const unsigned int destContextId)
{
   vRequestContext(appId, destContextId);
}


void AppSds_ContextRequestor::contextSwitch(const enApplicationId /*appId*/, const enContextId destContextId)
{
   requestContextSwitch(0, (unsigned int)destContextId);
}


void AppSds_ContextRequestor::contextSwitchSelf()
{
   requestContextSwitch(0, SDS_CONTEXT_ROOT);
}
