/*
 * SdsAdapterRequestor.h
 *
 *  Created on: Feb 3, 2016
 *      Author: bee4kor
 */

#ifndef SdsAdapterRequestor_h
#define SdsAdapterRequestor_h

#include "App/Core/Interfaces/ISdsAdapterRequestor.h"
#include "App/Core/SdsHallTypes.h"
#include "sds_gui_fi/SdsGuiServiceProxy.h"
#include "CourierTunnelService/CourierMessageReceiverStub.h"
#include "CgiExtensions/ListDataProviderDistributor.h"

namespace App {
namespace Core {


class SdsAdapterRequestor
   : public ISdsAdapterRequestor
   , public sds_gui_fi::SdsGuiService::PttPressCallbackIF
   , public sds_gui_fi::SdsGuiService::ManualOperationCallbackIF
   , public sds_gui_fi::SdsGuiService::StartSessionContextCallbackIF
   , public sds_gui_fi::SdsGuiService::TestModeUpdateCallbackIF
   , public sds_gui_fi::SdsGuiService::SettingsCommandCallbackIF
   , public sds_gui_fi::SdsGuiService::BackPressCallbackIF
   , public sds_gui_fi::SdsGuiService::StopSessionCallbackIF
   , public sds_gui_fi::SdsGuiService::AbortSessionCallbackIF
   , public sds_gui_fi::SdsGuiService::PauseSessionCallbackIF
   , public sds_gui_fi::SdsGuiService::ResumeSessionCallbackIF
   , public sds_gui_fi::SdsGuiService::HelpCommandCallbackIF
{
   public:
      SdsAdapterRequestor(::boost::shared_ptr<sds_gui_fi::SdsGuiService::SdsGuiServiceProxy>& sdsGuiServProxy);
      virtual ~SdsAdapterRequestor();

      ///////////////////////////////////////////Callbacks for Requests to SDS_Adapter/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      void onPttPressError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::PttPressError >& error) ;
      void onPttPressResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::PttPressResponse >& response) ;

      virtual void onManualOperationResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::ManualOperationResponse >& response);
      virtual void onManualOperationError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::ManualOperationError >& error);

      virtual void onStartSessionContextError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StartSessionContextError >& error) ;
      virtual void onStartSessionContextResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StartSessionContextResponse >& response);

      virtual void onTestModeUpdateError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::TestModeUpdateError >& error);
      virtual void onTestModeUpdateResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::TestModeUpdateResponse >& response);

      virtual void onSettingsCommandError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SettingsCommandError >& error);
      virtual void onSettingsCommandResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SettingsCommandResponse >& response);

      virtual void onBackPressError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::BackPressError >& error);
      virtual void onBackPressResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::BackPressResponse >& response);

      virtual void onStopSessionError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StopSessionError >& error);
      virtual void onStopSessionResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::StopSessionResponse >& response);

      virtual void onAbortSessionError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::AbortSessionError >& error);
      virtual void onAbortSessionResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::AbortSessionResponse >& response);

      virtual void onPauseSessionError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::PauseSessionError >& error);
      virtual void onPauseSessionResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::PauseSessionResponse >& response);

      virtual void onResumeSessionError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::ResumeSessionError >& error);
      virtual void onResumeSessionResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::ResumeSessionResponse >& response);

      virtual void onHelpCommandError(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::HelpCommandError >& error);
      virtual void onHelpCommandResponse(const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::SdsGuiServiceProxy >& proxy, const ::boost::shared_ptr< sds_gui_fi::SdsGuiService::HelpCommandResponse >& response);

      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////////////////////////// Courier Message Mapping with Traces classes ///////////////////////////////////////////////////////////////////////////////////
      COURIER_MSG_MAP_BEGIN(TR_CLASS_APPHMI_SDS_COURIER_PAYLOAD_MODEL_COMP)
      ON_COURIER_MESSAGE(BtnSettingsRelease)
      ON_COURIER_MESSAGE(BtnHelpRelease)
      COURIER_MSG_MAP_END()
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      //////////The below onCourierMessage funtions are the various action perform & model subscribed courier messages from SM ////////////////////////////////////////////////////
      bool onCourierMessage(const BtnSettingsRelease& msg);
      bool onCourierMessage(const BtnHelpRelease& msg);
      /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      ///////////////////////////////////////////Requests to SDS_Adapter//////////////////////////////////////////////////////////////////////////////////////////////////////////////

      void sendPttShortPress();
      void backKeyPressed();
      unsigned int focusMoved(unsigned int _guiFocusIndex);
      unsigned int listItemSelected(unsigned int _guiSelectedListItemIndex);
      unsigned int requestNextPage();
      unsigned int requestPrevPage();
      unsigned int sendShortcutRequest(unsigned int _shortcutType);
      unsigned int sendTestModeSentence(std::string _textSentence);
      void restoreFactorySDSSettings();
      void sendStopSession();
      void sendAbortSession();
      void sendPauseSession();
      void sendResumeSession();
      void setHighPriorityAppStatus(bool highPriorityAppActive);
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   private:
      sds_gui_fi::SdsGuiService::ContextType mapGui2AdapterShortcutContext(unsigned int _guiContext);

      ::boost::shared_ptr<sds_gui_fi::SdsGuiService::SdsGuiServiceProxy> _sdsGuiServiceProxy;
      bool _highPriorityAppActive;
};


}
}


#endif /* SDSADAPTERREQUESTOR_H_ */
