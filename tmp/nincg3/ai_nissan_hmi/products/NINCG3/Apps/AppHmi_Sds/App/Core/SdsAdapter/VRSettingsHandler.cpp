/*
 *VRSettingsHandler.cpp
 *
 *  Created on:
 *      Author:
 */

#include "App/Core/SdsAdapter/VRSettingsHandler.h"
#include "App/Core/SdsAdapter/GuiUpdater.h"
#include "App/Core/SdsHallTypes.h"
#include "HmiTranslation_TextIds.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SDS_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SDS
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SDS_"
#define ETG_I_FILE_PREFIX                 App::Core::VRSettingsHandler::
#include "trcGenProj/Header/VRSettingsHandler.cpp.trc.h"
#endif


using namespace sds_gui_fi::SettingsService;


namespace App {
namespace Core {


VRSettingsHandler::VRSettingsHandler(::boost::shared_ptr<SettingsServiceProxy>& settingsServiceProxy,
                                     GuiUpdater* pGuiUpdater)
   : _settingsServiceProxy(settingsServiceProxy)
   , _pGuiUpdater(pGuiUpdater)
   , _beepOnlyModeState(false)
   , _bestMatchAudioState(false)
   , _bestMatchPhoneBookState(false)
   , _shortPromptState(false)
   , _voicePreferenceIndex(INDEX_FEMALE)
{
   ETG_TRACE_USR4(("VRSettingsHandler::Constructor"));
   ///////////////////Member Initializations/////////////////////////////////////////////////////////////////////////////////////////////////////////////
   initializeMembers();
   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}


void VRSettingsHandler::onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange)
{
   if (proxy == _settingsServiceProxy)
   {
      ETG_TRACE_USR4(("VRSettingsHandler::onAvailable for _settingsServiceProxy, ProxyAvail=%d, StateChange=%d", proxy->isAvailable(), stateChange.kCurrentState));
      _settingsServiceProxy->sendBeepOnlyModeRegister(*this);
      _settingsServiceProxy->sendShortPromptRegister(*this);
      _settingsServiceProxy->sendBestMatchAudioRegister(*this);
      _settingsServiceProxy->sendBestMatchPhoneBookRegister(*this);
      _settingsServiceProxy->sendVoicePreferenceRegister(*this);
   }
}


void VRSettingsHandler::onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& stateChange)
{
   if (proxy == _settingsServiceProxy)
   {
      ETG_TRACE_USR4(("VRSettingsHandler::onUnavailable for _settingsServiceProxy, ProxyAvail=%d, StateChange=%d", proxy->isAvailable(), stateChange.kCurrentState));
      _settingsServiceProxy->sendBeepOnlyModeDeregisterAll();
      _settingsServiceProxy->sendShortPromptDeregisterAll();
      _settingsServiceProxy->sendBestMatchAudioDeregisterAll();
      _settingsServiceProxy->sendBestMatchPhoneBookDeregisterAll();
      _settingsServiceProxy->sendVoicePreferenceDeregisterAll();
   }
}


void VRSettingsHandler::processSettingsMainListItems(ListProviderEventInfo info, const ButtonReactionMsg& oMsg)
{
   ETG_TRACE_USR4(("VRSettingsHandler::processSettingsMainListItems"));

   switch (info.getHdlRow())
   {
      case LIST_ITEM_BEEP_ONLY_FOR_OPENING_PROMPT:
      {
         ETG_TRACE_USR4(("VRSettingsHandler::processSettingsMainListItems ::LIST_ITEM_BEEP_ONLY_FOR_OPENING_PROMPT"));
         onToggleBeepOnlyModeButtonReactionStatus();
      }
      break;

      case LIST_ITEM_SHORT_PROMPTS:
      {
         ETG_TRACE_USR4(("VRSettingsHandler::processSettingsMainListItems ::LIST_ITEM_SHORT_PROMPTS"));
         onToggleShortPromptButtonReactionStatus();
      }
      break;

      case LIST_ITEM_BEST_MATCH_LIST:
      {
         ETG_TRACE_USR4(("VRSettingsHandler::processSettingsMainListItems ::LIST_ITEM_BEST_MATCH_LIST"));
         POST_MSG((COURIER_MESSAGE_NEW(ButtonListItemUpdMsg)(info.getListId(), info.getHdlRow(), info.getHdlCol(), oMsg.GetEnReaction())));
      }
      break;

      case LIST_ITEM_VOICE_PREFERENCE :
      {
         ETG_TRACE_USR4(("VRSettingsHandler::processSettingsMainListItems ::LIST_ITEM_VOICE_PREFERENCE"));
         onToggleVoicePreferenceButtonReactionStatus();
      }
      break;

      default:
      {
         ETG_TRACE_USR4(("Default Case"));
      }
      break;
   }
}


void VRSettingsHandler::processBestMatchListItems(ListProviderEventInfo info, const ButtonReactionMsg& /*oMsg*/)
{
   ETG_TRACE_USR4(("SdsHall::processBestMatchListItems"));

   switch (info.getHdlRow())
   {
      case LIST_ITEM_AUDIO :
      {
         if (!_bestMatchAudioState)
         {
            POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_AUDIO)));
         }
         else
         {
            sendBestMatchAudio(false);
         }
      }
      break;

      case LIST_ITEM_PHONEBOOK :
      {
         if (!_bestMatchPhoneBookState)
         {
            POST_MSG((COURIER_MESSAGE_NEW(ActivatePopUpMsg)(1, SdsPopups_Popups_SDS__ICPOP_BEST_MATCH_PHONEBOOK)));
         }
         else
         {
            sendBestMatchPhoneBook(false);
         }
      }
      break;

      default:
      {
         ETG_TRACE_USR4(("Default Case"));
      }
      break;
   }
}


tSharedPtrDataProvider VRSettingsHandler::getSettingsMainList()
{
   ETG_TRACE_USR4(("SdsHall::getSettingsMainList()"));
   ListDataProviderBuilder listBuilder(LIST_ID_SDS_Settings_Main, DATA_CONTEXT_LIST_BTN_ON_OFF);

   listBuilder.AddItem((int)LIST_ITEM_BEEP_ONLY_FOR_OPENING_PROMPT, 0UL, DATA_CONTEXT_LIST_BTN_ON_OFF).AddData(Candera::String(TextId_New_VR_001)).AddData(!_beepOnlyModeState).AddData(true);
   listBuilder.AddItem((int)LIST_ITEM_SHORT_PROMPTS, 0UL, DATA_CONTEXT_LIST_BTN_ON_OFF).AddData(Candera::String(TextId_New_VR_002)).AddData(!_shortPromptState).AddData(true);
   listBuilder.AddItem((int)LIST_ITEM_BEST_MATCH_LIST, 0UL, DATA_CONTEXT_LIST_BTN_SETTINGS_TEXT).AddData(Candera::String(TextId_New_VR_003)).AddData(true);
   listBuilder.AddItem((int)LIST_ITEM_VOICE_PREFERENCE, 0UL, DATA_CONTEXT_LIST_BTN_SETTINGS_2_ITEM_TOGGLE).AddData("Voice Preference").AddData(_vecVoicePreference.at(_voicePreferenceIndex)).AddData(_voicePreferenceIndex);
   listBuilder.AddItem((int)LIST_ITEM_SPEECH_RATE, 0UL, DATA_CONTEXT_LIST_BTN_SETTINGS_MAIN_SPEECH_RATE).AddData("Speech Rate").AddData(true).AddData(true).AddData(true);

   tSharedPtrDataProvider dataProvider = listBuilder.CreateDataProvider();
   return dataProvider;
}


tSharedPtrDataProvider VRSettingsHandler::getBestMatchList()
{
   ETG_TRACE_USR4(("SdsHall::getBestMatchList()"));
   ListDataProviderBuilder listBuilder(LIST_ID_SDS_Best_Match_List, DATA_CONTEXT_LIST_BTN_SETTINGS_BEST_MATCH_AUDIO);

   listBuilder.AddItem((int)LIST_ITEM_AUDIO, 0UL, DATA_CONTEXT_LIST_BTN_SETTINGS_BEST_MATCH_AUDIO).AddData(true).AddData(getItemTextBestMatch(LIST_ITEM_AUDIO)).AddData(_bestMatchAudioState);
   listBuilder.AddItem((int)LIST_ITEM_PHONEBOOK, 0UL, DATA_CONTEXT_LIST_BTN_SETTINGS_BEST_MATCH_AUDIO).AddData(true).AddData(getItemTextBestMatch(LIST_ITEM_PHONEBOOK)).AddData(_bestMatchPhoneBookState);

   tSharedPtrDataProvider dataProvider = listBuilder.CreateDataProvider();
   return dataProvider;
}


Candera::String VRSettingsHandler::getItemTextBestMatch(const enBestMatchList Item) const
{
   Candera::String bestMatchListText;
   switch (Item)
   {
      case LIST_ITEM_AUDIO:
         bestMatchListText = "Audio";
         break;
      case LIST_ITEM_PHONEBOOK:
         bestMatchListText = "PhoneBook";
         break;
      default:
         bestMatchListText = "<invalid>";
         break;
   }
   ETG_TRACE_USR4(("Selected List item :%d", ETG_CENUM(enBestMatchList, (enBestMatchList)Item)));
   return bestMatchListText;
}


bool VRSettingsHandler::onCourierMessage(const BtnYesRelease& msg)
{
   ETG_TRACE_USR4(("SdsAdapterProvider::BtnYesRelease - received "));
   switch (msg.GetName())
   {
      case LIST_ITEM_AUDIO:
      {
         ETG_TRACE_USR4(("BEST MATCH_LIST_ITEM_AUDIO_YES"));
         sendBestMatchAudio(true);
      }
      break;
      case LIST_ITEM_PHONEBOOK:
      {
         ETG_TRACE_USR4(("BEST MATCH_LIST_ITEM_PHONEBOOK_YES"));
         sendBestMatchPhoneBook(true);
      }
      break;
      default:
      {
         ETG_TRACE_USR4(("Case Default"));
         return false;
      }
   }
   return true;
}


bool VRSettingsHandler::onCourierMessage(const BtnNoRelease& msg)
{
   ETG_TRACE_USR4(("SdsAdapterProvider::BtnNoRelease - received "));
   switch (msg.GetName())
   {
      case LIST_ITEM_AUDIO:
      {
         ETG_TRACE_USR4(("BEST MATCH_LIST_ITEM_AUDIO_NO"));
         sendBestMatchAudio(false);
      }
      break;
      case LIST_ITEM_PHONEBOOK:
      {
         ETG_TRACE_USR4(("BEST MATCH_LIST_ITEM_PHONEBOOK_NO"));
         sendBestMatchPhoneBook(false);
      }
      break;
      default:
      {
         ETG_TRACE_USR4(("Case Default"));
         return false;
      }
   }
   return true;
}


void VRSettingsHandler::onToggleBeepOnlyModeButtonReactionStatus()
{
   sendBeepOnlyMode(!_beepOnlyModeState);
}


void VRSettingsHandler::onToggleShortPromptButtonReactionStatus()
{
   sendShortPrompt(!_shortPromptState);
}


void VRSettingsHandler::onToggleVoicePreferenceButtonReactionStatus()
{
   if (_voicePreferenceIndex == INDEX_FEMALE)
   {
      sendVoicePreference(INDEX_MALE);
   }
   else
   {
      sendVoicePreference(INDEX_FEMALE);
   }
}


void VRSettingsHandler::sendBeepOnlyMode(bool mode)
{
   _settingsServiceProxy->sendBeepOnlyModeSet(mode);
}


void VRSettingsHandler::onBeepOnlyModeError(const ::boost::shared_ptr< SettingsServiceProxy >& /*proxy*/, const ::boost::shared_ptr< BeepOnlyModeError >& /*error*/)
{
   ETG_TRACE_USR4(("VRSettingsHandler::onBeepOnlyModeError"));
}


void VRSettingsHandler::onBeepOnlyModeUpdate(const ::boost::shared_ptr< SettingsServiceProxy >& /*proxy*/, const ::boost::shared_ptr< BeepOnlyModeUpdate >& update)
{
   ETG_TRACE_USR4(("VRSettingsHandler::onBeepOnlyModeUpdate BeepMode value =%d", update->getBeepOnlyMode()));
   _beepOnlyModeState = update->getBeepOnlyMode();
   _pGuiUpdater->updateDynamicList(LIST_ID_SDS_Settings_Main);
}


void VRSettingsHandler::sendShortPrompt(bool mode)
{
   _settingsServiceProxy->sendShortPromptSet(mode);
}


void VRSettingsHandler::onShortPromptError(const ::boost::shared_ptr< SettingsServiceProxy >& /*proxy*/, const ::boost::shared_ptr< ShortPromptError >& /*error*/)
{
   ETG_TRACE_USR4(("VRSettingsHandler::onShortPromptError"));
}


void VRSettingsHandler::onShortPromptUpdate(const ::boost::shared_ptr< SettingsServiceProxy >& /*proxy*/, const ::boost::shared_ptr< ShortPromptUpdate >& update)
{
   ETG_TRACE_USR4(("VRSettingsHandler::onShortPromptUpdate = %d", update->getShortPrompt()));
   _shortPromptState = update->getShortPrompt();
   _pGuiUpdater->updateDynamicList(LIST_ID_SDS_Settings_Main);
}


void VRSettingsHandler::sendBestMatchAudio(bool bestmatchAudio)
{
   _settingsServiceProxy->sendBestMatchAudioSet(bestmatchAudio);
}


void VRSettingsHandler::onBestMatchAudioError(const ::boost::shared_ptr< SettingsServiceProxy >& /*proxy*/, const ::boost::shared_ptr< BestMatchAudioError >& /*error*/)
{
}


void VRSettingsHandler::onBestMatchAudioUpdate(const ::boost::shared_ptr< SettingsServiceProxy >& /*proxy*/, const ::boost::shared_ptr< BestMatchAudioUpdate >& update)
{
   ETG_TRACE_USR4(("VRSettingsHandler::onBestMatchAudioUpdate = %d", update->getBestMatchAudio()));
   _bestMatchAudioState = update->getBestMatchAudio();
   _pGuiUpdater->updateDynamicList(LIST_ID_SDS_Best_Match_List);
}


void VRSettingsHandler::sendBestMatchPhoneBook(bool bestmatchPB)
{
   _settingsServiceProxy->sendBestMatchPhoneBookSet(bestmatchPB);
}


void VRSettingsHandler::onBestMatchPhoneBookError(const ::boost::shared_ptr< SettingsServiceProxy >& /*proxy*/, const ::boost::shared_ptr< BestMatchPhoneBookError >& /*error*/)
{
   ETG_TRACE_USR4(("VRSettingsHandler::onBestMatchPhoneBookError"));
}


void VRSettingsHandler::onBestMatchPhoneBookUpdate(const ::boost::shared_ptr< SettingsServiceProxy >& /*proxy*/, const ::boost::shared_ptr< BestMatchPhoneBookUpdate >& update)
{
   ETG_TRACE_USR4(("VRSettingsHandler::onBestMatchPhoneBookUpdate = %d", update->getBestMatchPhoneBook()));
   _bestMatchPhoneBookState = update->getBestMatchPhoneBook();
   _pGuiUpdater->updateDynamicList(LIST_ID_SDS_Best_Match_List);
}


void VRSettingsHandler::sendVoicePreference(voicePreferenceIndex enIndex)
{
   switch (enIndex)
   {
      case INDEX_FEMALE:
         ETG_TRACE_USR4(("_settingsServiceProxy->sendVoicePreferenceSet(ActiveSpeakerGender__FEMALE)"));
         _settingsServiceProxy->sendVoicePreferenceSet(ActiveSpeakerGender__FEMALE);
         return;
      case INDEX_MALE:
         ETG_TRACE_USR4(("_settingsServiceProxy->sendVoicePreferenceSet(ActiveSpeakerGender__MALE)"));
         _settingsServiceProxy->sendVoicePreferenceSet(ActiveSpeakerGender__MALE);
         return;
      default:
         ETG_TRACE_USR4(("Default case"));
         _settingsServiceProxy->sendVoicePreferenceSet(ActiveSpeakerGender__FEMALE);
         break;
   }
}


void VRSettingsHandler::onVoicePreferenceError(const ::boost::shared_ptr< SettingsServiceProxy >& /*proxy*/, const ::boost::shared_ptr< VoicePreferenceError >& /*error*/)
{
   ETG_TRACE_USR4(("VRSettingsHandler::onVoicePreferenceError"));
}


void VRSettingsHandler::onVoicePreferenceUpdate(const ::boost::shared_ptr< SettingsServiceProxy >& /*proxy*/, const ::boost::shared_ptr< VoicePreferenceUpdate >& update)
{
   ETG_TRACE_USR4(("VRSettingsHandler::onVoicePreferenceUpdate = %d", update->getVoicePreference()));
   _voicePreferenceIndex = adapter2HallVoicePreferenceIndex(update->getVoicePreference());
   _pGuiUpdater->updateDynamicList(LIST_ID_SDS_Settings_Main);
}


voicePreferenceIndex VRSettingsHandler::adapter2HallVoicePreferenceIndex(sds_gui_fi::SettingsService::ActiveSpeakerGender speakerGender)
{
   switch (speakerGender)
   {
      case sds_gui_fi::SettingsService::ActiveSpeakerGender__FEMALE :
         return INDEX_FEMALE;

      case sds_gui_fi::SettingsService::ActiveSpeakerGender__MALE :
         return INDEX_MALE;

      default :
         return INDEX_FEMALE;
   }
}


void VRSettingsHandler::initializeMembers()
{
   _vecVoicePreference.push_back("Female");
   _vecVoicePreference.push_back("Male");
}


VRSettingsHandler::~VRSettingsHandler()
{
   PURGE(_pGuiUpdater);
}


} // Namespace Core
} // Namespace App
