/*
 * SdsHallTypes.h
 *
 *  Created on: Jan 27, 2016
 *      Author: bee4kor
 */

#ifndef SdsHallTypes_h
#define SdsHallTypes_h

#include "asf/core/Types.h"
#include "ProjectBaseTypes.h"
#include "AppHmi_SdsStateMachine.h"
#include "hmi_trace_if.h"


namespace App {
namespace Core {


#define INVALID_FOCUS_INDEX 50000
struct SelectableListItem
{
   ::std::string text;
   bool isSelectable;
};


static const char* const DATA_CONTEXT_LIST_BTN_VL    									= "BTN_VL_SDS";
static const char* const DATA_CONTEXT_LIST_BTN_ON_OFF    								= "BtnSettings_onoff";
static const char* const DATA_CONTEXT_LIST_BTN_SETTINGS_TEXT  							= "BtnSettingsText";
static const char* const DATA_CONTEXT_LIST_BTN_SETTINGS_2_ITEM_TOGGLE  					= "BtnList_2ItemToggle_1";
static const char* const DATA_CONTEXT_LIST_BTN_SETTINGS_MAIN_SPEECH_RATE  				= "BtnSettingsIncrementSpeechRate";
static const char* const DATA_CONTEXT_LIST_BTN_SETTINGS_BEST_MATCH_AUDIO  				= "AudioList";
static const char* const DATA_CONTEXT_LIST_BTN_FUEL_PRICES    							= "BTN_VL_FUEL";
static const char* const DATA_CONTEXT_LIST_BTN_POI    									= "BTN_VL_POI";

enum voicePreferenceIndex
{
   INDEX_FEMALE = 0L,
   INDEX_MALE = 1L
};


enum sdsHmiVisibleAppMode
{
   MODE_FOREGROUND = 0L,
   MODE_BACKGROUND = 1L
};


enum enEarlySavedContext
{
   EARLY_CONTEXT_NOT_REQUESTED = 0L,
   EARLY_CONTEXT_PTT_SHORTPRESS,
   EARLY_CONTEXT_VR_SETTINGS,
   EARLY_CONTEXT_VR_SHORTCUTS
};


enum enEarlyContextState
{
   EARLY_CONTEXTSTATE_ACCEPT 		= 0L,
   EARLY_CONTEXTSTATE_REJECT 		= 1L,
   EARLY_CONTEXTSTATE_COMPLETE    	= 2L,
   EARLY_CONTEXTSTATE_INVALIDATE 	= 3L,
};


enum enTalkingHeadState
{
   TALKINGHEAD_STATE_PROMPTING 		= 0L,
   TALKINGHEAD_STATE_LISTENING         = 4L,
};


}
}


#endif /* SdsHallTypes_h */
