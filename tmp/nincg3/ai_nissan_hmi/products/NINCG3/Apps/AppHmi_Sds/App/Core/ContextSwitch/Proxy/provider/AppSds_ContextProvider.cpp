///////////////////////////////////////////////////////////
//  AppSds_ContextProvider.cpp
//  Implementation of the Class AppSds_ContextProvider
//  Created on:      13-Jul-2015 11:44:47 AM
//  Original author: pad1cob
///////////////////////////////////////////////////////////


#include "AppHmi_SdsMessages.h"
#include "AppSds_ContextProvider.h"
#include "App/Core/Interfaces/IEarlyStartupHandler.h"
#include "App/Core/Interfaces/ISdsAdapterRequestor.h"
#include "App/Core/Interfaces/ISdsContextRequestor.h"
#include "App/Core/ListHandler/ListHandler.h"
#include "App/Core/SdsDefines.h"

using namespace App::Core;
using namespace bosch::cm::ai::nissan::hmi::contextswitchservice::ContextSwitchTypes;


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SDS_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SDS
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SDS_"
#define ETG_I_FILE_PREFIX                 App::Core::AppSds_ContextProvider::
#include "trcGenProj/Header/AppSds_ContextProvider.cpp.trc.h"
#endif


AppSds_ContextProvider::AppSds_ContextProvider(ISdsAdapterRequestor* poSdsAdapterRequestor, IEarlyStartupHandler* pEarlyStartupHandler, ListHandler* pListHandler)
   :  _pSdsAdapterRequestor(poSdsAdapterRequestor), _pEarlyStartupHandler(pEarlyStartupHandler), _pListHandler(pListHandler)

{
   vSendContextTable();
}


AppSds_ContextProvider::~AppSds_ContextProvider()
{
   PURGE(_pSdsContextRequestor);
   PURGE(_pSdsAdapterRequestor);
   PURGE(_pEarlyStartupHandler);
   PURGE(_pListHandler);
}


bool AppSds_ContextProvider::onCourierMessage(const ContextSwitchInResMsg& msg)
{
   ContextState enState =  ContextState__INVALID;
   if (msg.GetResponseType() == CONTEXT_TRANSITION_BACK)
   {
      vSwitchBack();
      return true;
   }
   switch (msg.GetResponseType())
   {
      case CONTEXT_TRANSITION_DONE:
         enState = ContextState__ACCEPTED;
         break;
      case CONTEXT_TRANSITION_COMPLETE:
         enState = ContextState__COMPLETED;
         POST_MSG((COURIER_MESSAGE_NEW(ContextSwitchStateUpdateMsg)(1, CONTEXT_SWITCH_CLOSED)));
         break;
      case CONTEXT_TRANSITION_FAILED:
         enState = ContextState__REJECTED;
         break;
      default:
         break;
   }
   if (msg.GetSwitchId())
   {
      vOnNewContextRequestState(0, enState);
   }
   return true;
}


bool AppSds_ContextProvider::bOnNewContext(uint32 sourceContextId, uint32 targetContextId)
{
   ETG_TRACE_USR4(("AppSds_ContextProvider::bOnNewContext- sourceContextId = %d, targetContextId = %d", sourceContextId, targetContextId));

   /**
    Do Not Modify the Context Handling Sequence Without Testing - (1) handleEarlySdsContext  , (2) handleShortcutContexts , (3) handleSettingsContexts, (4) handleGeneralContexts
   **/

   if (handleEarlySdsContext(sourceContextId, targetContextId))
   {
      return true;
   }

   if (handleShortcutContexts(sourceContextId, targetContextId))
   {
      return true;
   }

   if (handleSettingsContexts(sourceContextId, targetContextId))
   {
      return true;
   }

   if (handleGeneralContexts(sourceContextId, targetContextId))
   {
      return true;
   }

   else
   {
      vOnNewContextRequestState(0, ContextState__REJECTED);
   }
   return false;
}


bool AppSds_ContextProvider::handleEarlySdsContext(unsigned int _srcContext, unsigned int _destContext)
{
   ETG_TRACE_USR4(("AppSds_ContextProvider::handleEarlySdsContext"));

   if (_pEarlyStartupHandler)
   {
      if (_pEarlyStartupHandler->isSdsAdapterAvailable())
      {
         if (!_pEarlyStartupHandler->isSdsMiddlewareAvailable())
         {
            ETG_TRACE_USR4(("SDS_STATUS_NOT_IDLE, Start Early Sds Context Handling"));

            enEarlyContextState earlyContextState = _pEarlyStartupHandler->startEarlySdsContext(_srcContext, _destContext);
            vOnNewContextRequestState(0, convertToServiceContextState(earlyContextState));
            return true;
         }
         else
         {
            ETG_TRACE_USR4(("SDS_STATUS_IDLE, Start Normal Context Handling"));
            return false;
         }
      }

      else
      {
         ETG_TRACE_USR4(("SDS_ADAPTER_UNAVAILABLE"));
         return true;
      }
   }
   return false;
}


ContextState AppSds_ContextProvider::convertToServiceContextState(enEarlyContextState earlyContextState)
{
   switch (earlyContextState)
   {
      case EARLY_CONTEXTSTATE_COMPLETE:
      {
         return ContextState__COMPLETED;
      }
      case EARLY_CONTEXTSTATE_ACCEPT:
      {
         return ContextState__ACCEPTED;
      }
      case EARLY_CONTEXTSTATE_REJECT:
      {
         return ContextState__REJECTED;
      }
      default:
      {
         return ContextState__REJECTED;
      }
   }
}


bool AppSds_ContextProvider::handleShortcutContexts(unsigned int _srcContext, unsigned int _destContext)
{
   ETG_TRACE_USR4(("AppSds_ContextProvider::handleShortcutContexts"));

   if (_srcContext >= HOMESCREEN_CONTEXT_START && _srcContext <= HOMESCREEN_CONTEXT_END)
   {
      if (_pSdsAdapterRequestor)
      {
         if (_destContext == SDS_CONTEXT_SETTINGS_MAIN)
         {
            requestVRSettingsContext();
         }
         else
         {
            _pSdsAdapterRequestor->sendShortcutRequest(_destContext);
         }
      }
      vOnNewContextRequestState(0, ContextState__COMPLETED);
      return true;
   }

   return false;
}


bool AppSds_ContextProvider::handleSettingsContexts(unsigned int _srcContext, unsigned int _destContext)
{
   ETG_TRACE_USR4(("AppSds_ContextProvider::handleSettingsContexts"));

   if (_srcContext == DEFAULT_CONTEXT_BACK && _destContext == SDS_CONTEXT_SETTINGS_MAIN)
   {
      requestVRSettingsContext();
      vOnNewContextRequestState(0, ContextState__COMPLETED);
      return true;
   }
   return false;
}


bool AppSds_ContextProvider::handleGeneralContexts(unsigned int _srcContext, unsigned int _destContext)
{
   ETG_TRACE_USR4(("AppSds_ContextProvider::handleGeneralContexts"));

   if ((_srcContext == 0) && (_destContext == SDS_CONTEXT_ROOT))
   {
      ETG_TRACE_USR4(("AppSds_ContextProvider::handleGeneralContexts - SelfContext Request Accepted"));
      vOnNewContextRequestState(0, ContextState__ACCEPTED);
      return true;
   }

   if (_destContext == SDS_CONTEXT_ROOT)
   {
      ETG_TRACE_USR4(("AppSds_ContextProvider::handleGeneralContexts - PTT Press Request"));

      if (_pSdsAdapterRequestor)
      {
         _pSdsAdapterRequestor->sendPttShortPress();
      }
      vOnNewContextRequestState(0, ContextState__COMPLETED);
      return true;
   }

   return false;
}


void AppSds_ContextProvider::requestVRSettingsContext()
{
   ETG_TRACE_USR4(("AppSds_ContextProvider::requestVRSettingsContext"));

   _pListHandler->setShowScreenLayout(SDS_Settings_Main);
   _pSdsContextRequestor->contextSwitchSelf();
}


bool AppSds_ContextProvider::onCourierMessage(const ActiveRenderedView& msg)
{
   vOnActiveRenderedView(msg.GetViewName().GetCString(), msg.GetSurfaceId());
   return false;
}


void AppSds_ContextProvider::vOnBackRequestResponse(ContextState /*enState */)
{
}


void AppSds_ContextProvider::contextSwitchBack()
{
   ETG_TRACE_USR4(("AppSds_ContextProvider::contextSwitchBack()"));
   vSwitchBack();
}


void AppSds_ContextProvider::contextSwitchComplete()
{
   ETG_TRACE_USR4(("AppSds_ContextProvider::contextSwitchComplete()"));
   vOnNewContextRequestState(0, ContextState__COMPLETED);
}


void AppSds_ContextProvider::setContextRequestor(App::Core::ISdsContextRequestor* poSdsContextRequestor)
{
   _pSdsContextRequestor = poSdsContextRequestor;
}
