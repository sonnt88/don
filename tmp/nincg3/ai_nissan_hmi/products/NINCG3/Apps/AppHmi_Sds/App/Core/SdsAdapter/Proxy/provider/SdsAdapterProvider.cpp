/*
 * SdsAdapterProvider.cpp
 *
 *  Created on: May 25, 2015
 *      Author: bee4kor
 */

#include "App/Core/Interfaces/ISdsContextProvider.h"
#include "App/Core/Interfaces/ISdsContextRequestor.h"
#include "App/Core/Interfaces/IHMIModelComponent.h"
#include "App/Core/Observers/SdsSessionStartObserver.h"
#include "App/Core/SdsAdapter/GuiUpdater.h"
#include "App/Core/SdsAdapter/Proxy/provider/SdsAdapterProvider.h"
#include "App/Core/SdsAdapter/SdsEventHandler.h"
#include "App/Core/SdsDefines.h"

#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS           TR_CLASS_APPHMI_SDS_HALL
#define ETG_I_TRACE_CHANNEL               TR_TTFIS_APPHMI_SDS
#define ETG_I_TTFIS_CMD_PREFIX            "APPHMI_SDS_"
#define ETG_I_FILE_PREFIX                 App::Core::SdsAdapterProvider::
#include "trcGenProj/Header/SdsAdapterProvider.cpp.trc.h"
#endif


using namespace sds_gui_fi::SdsGuiService;
using namespace sds_gui_fi::PopUpService;
using namespace sds_gui_fi::SettingsService;
using namespace sds_gui_fi::SdsPhoneService;


namespace App {
namespace Core {


SdsAdapterProvider::SdsAdapterProvider(const ::boost::shared_ptr<PopUpServiceProxy>& popUpServiceProxy,
                                       const ::boost::shared_ptr<SettingsServiceProxy>& settingsServiceProxy,
                                       const ::boost::shared_ptr<SdsPhoneServiceProxy>& sdsPhoneServiceProxy,
                                       GuiUpdater* pGuiUpdater,
                                       IHMIModelComponent* pHMIModelComponent,
                                       ListHandler* pListHandler)
   : _popUpServiceProxy(popUpServiceProxy)
   , _settingsServiceProxy(settingsServiceProxy)
   , _sdsPhoneServiceProxy(sdsPhoneServiceProxy)
   , _pGuiUpdater(pGuiUpdater)
   , _pHMIModelComponent(pHMIModelComponent)
   , _pListhandler(pListHandler)
   , _pSdsContextProvider(NULL)
   , _pSdsContextRequestor(NULL)
   , _pSdsEventHandler(NULL)
   , _sdsHmiActiveAppMode(SDSHMI_MODE_BACKGROUND)
   , _isSmsHeaderVisible(false)
{
   ETG_TRACE_USR4(("SdsAdapterProvider::Constructor"));
   if (_pHMIModelComponent)
   {
      ETG_TRACE_USR4(("_pHMIModelComponent->registerSdsApplicationModeObserver"));
      _pHMIModelComponent->registerSdsApplicationModeObserver(this);
   }
}


void SdsAdapterProvider::onAvailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange */)
{
   ETG_TRACE_USR1(("SdsAdapterProvider::onAvailable"));

   if (proxy == _sdsPhoneServiceProxy)
   {
      _sdsPhoneServiceProxy->sendSmsContentRegister(*this);
   }
   if (proxy == _popUpServiceProxy)
   {
      _popUpServiceProxy->sendMicrophoneLevelRegister(*this);
      _popUpServiceProxy->sendPopupRequestCloseRegister(*this);
      _popUpServiceProxy->sendPopupRequestRegister(*this);
   }
}


void SdsAdapterProvider::onUnavailable(const ::boost::shared_ptr< asf::core::Proxy >& proxy, const asf::core::ServiceStateChange& /*stateChange */)
{
   ETG_TRACE_USR1(("SdsAdapterProvider::onUnavailable "));

   if (proxy == _sdsPhoneServiceProxy)
   {
      _sdsPhoneServiceProxy->sendSmsContentDeregisterAll();
   }
   if (proxy == _popUpServiceProxy)
   {
      _popUpServiceProxy->sendMicrophoneLevelDeregisterAll();
      _popUpServiceProxy->sendPopupRequestCloseDeregisterAll();
      _popUpServiceProxy->sendPopupRequestDeregisterAll();
   }
}


void SdsAdapterProvider::onPopupRequestSignal(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestSignal >& signal)
{
   ETG_TRACE_USR4(("----------SdsAdapterProvider::onPopupRequestSignal -------------"));

   if (_pSdsContextRequestor)
   {
      if (_sdsHmiActiveAppMode == SDSHMI_MODE_BACKGROUND)
      {
         ETG_TRACE_USR4(("_sdsHmiActiveAppMode == SDSHMI_MODE_BACKGROUND"));//TBR
         vNotifySDSSessionStartedToObservers();
         _pSdsContextRequestor->contextSwitchSelf();
      }
      else
      {
         ETG_TRACE_USR4(("_sdsHmiActiveAppMode == SDSHMI_MODE_FOREGROUND / NO_SELF_CONTEXT_SWITCH"));//TBR
      }

      unsigned char layoutType = getLayout(signal);
      switch (layoutType)
      {
         case 'R':
         case 'N':
         case 'L':
         case 'C':
         case 'M':
         case 'E':
         case 'Q':
         case 'T':
         case 'O':
         {
            processListData(signal);
            processLayout(signal);
            processHeaderData(signal);
            return;
         }
         case 'S':
         {
            processSmsHeaderData(signal);
         }
         return;
         default:
         {
            ETG_TRACE_FATAL(("INVALID LAYOUT - NO LIST Processing Possible"));
         }
      }
   }
}


unsigned char SdsAdapterProvider::getLayout(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestSignal >& signal) const
{
   ETG_TRACE_USR4(("SdsAdapterProvider getLayout() = %s", (signal->getLayout().c_str())));
   unsigned char layoutType = *((const char*)(signal->getLayout().c_str()));
   ETG_TRACE_USR4(("char layoutType = %c", layoutType));
   return layoutType;
}


void SdsAdapterProvider::processHeaderData(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestSignal >& signal)
{
   ETG_TRACE_USR4(("SdsAdapterProvider::processHeaderData Listening State = %d ", signal->getSpeechInputState()));
   ETG_TRACE_USR4((" getHeader() = %s", signal->getHeader().c_str()));
   if (_pGuiUpdater)
   {
      _pGuiUpdater->updateHeaderSpeakingIconIndex(convertToTalkingHeadState(signal->getSpeechInputState()));
      _pGuiUpdater->updateHeaderTextData(signal->getHeader().c_str());
   }
}


unsigned int SdsAdapterProvider::convertToTalkingHeadState(sds_gui_fi::PopUpService::SpeechInputState speechInputState)
{
   switch (speechInputState)
   {
      case sds_gui_fi::PopUpService::SpeechInputState__LISTENING:
         return ((unsigned int) TALKINGHEAD_STATE_LISTENING);
      default:
         return ((unsigned int) TALKINGHEAD_STATE_PROMPTING);
   }
}


void SdsAdapterProvider::processListData(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestSignal >& signal)
{
   ETG_TRACE_USR4(("SdsAdapterProvider::processListData"));

   unsigned char layoutType = getLayout(signal);

   clearScreenData(layoutType);

   SelectableListItem selectableItem;

   ::std::vector< sds_gui_fi::PopUpService::TextField > textFieldVector;
   textFieldVector = signal->getTextFields();

   ETG_TRACE_USR4(("textFieldVector.size() = %d", textFieldVector.size()));

   for (::std::vector< TextField >::iterator iter = textFieldVector.begin(); iter != textFieldVector.end(); ++iter)
   {
      ETG_TRACE_USR4(("iter.getTagName() = %s", iter->getTagName().c_str()));
      ETG_TRACE_USR4(("iter.getString() = %s", iter->getString().c_str()));

      if (iter->getTagName().substr(0, TAGNAME_CMD_SIZE) == TAGNAME_CMD)
      {
         selectableItem.text = iter->getString();
         switch (iter->getAttrib())
         {
            case TextAttribute__NORMAL:
               selectableItem.isSelectable = true;
               break;
            case TextAttribute__COMMAND:
               selectableItem.isSelectable = true;
               break;
            case TextAttribute__GREYED_OUT:
               ETG_TRACE_USR4(("case TextAttribute__GREYED_OUT"));
               selectableItem.isSelectable = false;
               break;
            default:
               selectableItem.isSelectable = true;
               break;
         }
         if (_pListhandler)
         {
            _pListhandler->getVectCmdString().push_back(selectableItem);
         }
      }

      else if (layoutType == 'R')
      {
         if ((_pListhandler) && (iter->getTagName().substr(0, TAGNAME_SUBCMD_SIZE) == TAGNAME_SUBCMD))
         {
            _pListhandler->getVectSubCmdString().push_back(iter->getString());
         }
      }

      else if ((layoutType == 'L') || (layoutType == 'Q'))
      {
         if ((_pListhandler) && (iter->getTagName().substr(0, TAGNAME_DESC_SIZE) == TAGNAME_DESC))
         {
            _pListhandler->getVectDescString().push_back(iter->getString());
         }
      }

      else if (layoutType == 'E')
      {
         if ((_pListhandler) && (iter->getTagName().substr(0, TAGNAME_DESC_SIZE) == TAGNAME_DESC))
         {
            _pListhandler->getVectDescString().push_back(iter->getString());
         }
         else if ((_pListhandler) && (iter->getTagName().substr(0, TAGNAME_PRICE_AGE_SIZE) == TAGNAME_PRICE_AGE))
         {
            _pListhandler->getVectPriceAgeString().push_back(iter->getString());
         }
         else if ((_pListhandler) && (iter->getTagName().substr(0, TAGNAME_PRICE_SIZE) == TAGNAME_PRICE))
         {
            _pListhandler->getVectPriceString().push_back(iter->getString());
         }
         else if ((_pListhandler) && (iter->getTagName().substr(0, TAGNAME_DIST_SIZE) == TAGNAME_DIST))
         {
            _pListhandler->getVectDistString().push_back(iter->getString());
         }
         else if ((_pListhandler) && (iter->getTagName().substr(0, TAGNAME_DIR_SIZE) == TAGNAME_DIR))
         {
            _pListhandler->getVectDirectionString().push_back(iter->getString());
         }
      }
      else if (iter->getTagName().substr(0, TAGNAME_HELP_SIZE) == TAGNAME_HELP)
      {
         if (_pGuiUpdater)
         {
            _pGuiUpdater->updateHelpLineData(iter->getString());
         }
      }

      else if ((layoutType == 'T') || (layoutType == 'O') || (layoutType == 'M'))
      {
         if ((_pGuiUpdater) && (iter->getTagName().substr(0, TAGNAME_INFO_SIZE) == TAGNAME_INFO))
         {
            _pGuiUpdater->updateInfoTextData(iter->getString());
         }
      }
   }
}


void SdsAdapterProvider::clearScreenData(unsigned char layoutType)
{
   if (_pListhandler && _pGuiUpdater)
   {
      _pListhandler->getVectCmdString().clear();

      if ((layoutType == 'R'))
      {
         _pListhandler->getVectSubCmdString().clear();
      }

      if ((layoutType == 'M') || (layoutType == 'T') || (layoutType == 'O'))
      {
         _pGuiUpdater->updateInfoTextData("");
      }

      if ((layoutType == 'L') || (layoutType == 'E') || (layoutType == 'Q'))
      {
         _pListhandler->getVectDescString().clear();
      }

      if ((layoutType == 'E'))
      {
         _pListhandler->getVectPriceAgeString().clear();
         _pListhandler->getVectPriceString().clear();
         _pListhandler->getVectDistString().clear();
         _pListhandler->getVectDirectionString().clear();
      }
   }
}


void SdsAdapterProvider::processLayout(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestSignal >& signal)
{
   unsigned char layoutType = getLayout(signal);
   enSdsScreenLayoutType requestedLayoutType = getLayoutEnumValue(layoutType);

   ETG_TRACE_USR4(("SdsAdapterProvider::processLayout layoutType:%d, requestedLayoutType:%d", layoutType, requestedLayoutType));

   if (_pListhandler && _pGuiUpdater && SDS_LAYOUT_INVALID != requestedLayoutType)
   {
      enSdsScreenLayoutType previousLayoutType = _pListhandler->getCurrentLayout();

      ETG_TRACE_USR4(("Previous Layout = %d", previousLayoutType));

      if (requestedLayoutType != previousLayoutType)
      {
         _pListhandler->setShowScreenLayout(requestedLayoutType);
         ETG_TRACE_USR4(("Current Layout = %d", _pListhandler->getCurrentLayout()));
      }
      else
      {
         ETG_TRACE_USR4(("VALID LAYOUT UNCHANGED"));
         _pGuiUpdater->updateDynamicList(LIST_ID_SDS_VL);
      }
   }
}


enSdsScreenLayoutType SdsAdapterProvider::getLayoutEnumValue(unsigned char layoutChar) const
{
   switch (layoutChar)
   {
      case 'R':
         return LAYOUT_R;
      case 'N':
         return LAYOUT_N;
      case 'L':
         return LAYOUT_L;
      case 'C':
         return LAYOUT_C;
      case 'S':
         return LAYOUT_S;
      case 'M':
         return LAYOUT_M;
      case 'E':
         return LAYOUT_E;
      case 'Q':
         return LAYOUT_Q;
      case 'T':
         return LAYOUT_T;
      case 'O':
         return LAYOUT_O;

      default:
         return SDS_LAYOUT_INVALID;
   }
}


void SdsAdapterProvider::onPopupRequestCloseError(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestCloseError >&/* error*/)
{
}


void SdsAdapterProvider::onPopupRequestCloseSignal(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestCloseSignal >& /*signal*/)
{
   ETG_TRACE_USR4(("SdsAdapterProvider::onPopupRequestCloseSignal"));

   if (_pSdsContextProvider && _pSdsEventHandler)
   {
      vNotifySDSSessionEndToObesrvers();
      if (_pSdsEventHandler->getBackTransitionFlag())
      {
         _pSdsContextProvider->contextSwitchBack();
         _pSdsContextProvider->contextSwitchComplete();
      }
      else
      {
         _pSdsContextProvider->contextSwitchComplete();
      }
   }
}


void SdsAdapterProvider::onPopupRequestError(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopUpServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestError >&/* error*/)
{
}


SdsAdapterProvider::~SdsAdapterProvider()
{
   PURGE(_pSdsContextRequestor);
   PURGE(_pSdsContextProvider);
   PURGE(_pGuiUpdater);
   PURGE(_pHMIModelComponent);
   PURGE(_pListhandler);
   PURGE(_pSdsEventHandler);
}


void SdsAdapterProvider::onSmsContentError(const ::boost::shared_ptr< sds_gui_fi::SdsPhoneService::SdsPhoneServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_gui_fi::SdsPhoneService::SmsContentError >& /*error*/)
{
}


void SdsAdapterProvider::onSmsContentSignal(const ::boost::shared_ptr< sds_gui_fi::SdsPhoneService::SdsPhoneServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_gui_fi::SdsPhoneService::SmsContentSignal >& signal)
{
   ETG_TRACE_USR4(("SdsAdapterProvider::onSmsContentSignal and vehicleMovingOnSpeed[%d]" , _isSmsHeaderVisible));
   ETG_TRACE_USR4(("signal->getHeader().c_str() = %s ", signal->getHeader().c_str()));
   ETG_TRACE_USR4(("signal->getMessage.c_str() = %s", signal->getMessage().c_str()));
   if ((_pGuiUpdater) && (_isSmsHeaderVisible == false))
   {
      _pGuiUpdater->updateSmsMessageContent(signal->getMessage().c_str());
      _pGuiUpdater->updateHeaderTextData(signal->getHeader().c_str());
   }
}


void SdsAdapterProvider::onMicrophoneLevelError(const ::boost::shared_ptr< PopUpServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_gui_fi::PopUpService::MicrophoneLevelError >& /*error*/)
{
}


void SdsAdapterProvider::onMicrophoneLevelUpdate(const ::boost::shared_ptr< PopUpServiceProxy >& /*proxy*/, const ::boost::shared_ptr< sds_gui_fi::PopUpService::MicrophoneLevelUpdate >& update)
{
   if (_pGuiUpdater)
   {
      _pGuiUpdater->updateMicrophoneLevelData(percentageToValue(update->getMicrophoneLevel(), MAX_MIC_LEVEL));
   }
}


unsigned int SdsAdapterProvider::percentageToValue(unsigned int percentage, unsigned int maxValue) const
{
   unsigned int percentLimited = percentage < 100 ? percentage : 100;
   return ((percentLimited * maxValue) + 50) / 100;
}


void SdsAdapterProvider::setContextRequestor(ISdsContextRequestor* poSdsContextRequestor)
{
   _pSdsContextRequestor = poSdsContextRequestor;
}


void SdsAdapterProvider::setContextProvider(ISdsContextProvider* poSdsContextProvider)
{
   _pSdsContextProvider = poSdsContextProvider;
}


void SdsAdapterProvider::setEventHandler(SdsEventHandler* pSdsEventHandler)
{
   _pSdsEventHandler = pSdsEventHandler;
}


void SdsAdapterProvider::sdsHmiActiveAppModeChanged(unsigned int sdsHmiActiveAppMode)
{
   ETG_TRACE_USR4(("SdsAdapterProvider::sdsHmiActiveAppModeChanged"));

   _sdsHmiActiveAppMode = sdsHmiActiveAppMode;
}


void SdsAdapterProvider::vRegisterObservers(SdsSessionStartObserver* pSDSSessionObserver)
{
   if (pSDSSessionObserver != NULL)
   {
      _sdsSessionStartedObservers.push_back(pSDSSessionObserver);
   }
}


void SdsAdapterProvider::vNotifySDSSessionStartedToObservers()
{
   std::vector<SdsSessionStartObserver*>::iterator iter = _sdsSessionStartedObservers.begin();
   while (iter != _sdsSessionStartedObservers.end())
   {
      if (*iter != NULL)
      {
         (*iter)->onSDSSessionStarted();
      }
      ++iter;
   }
}


void  SdsAdapterProvider:: vNotifySDSSessionEndToObesrvers()
{
   std::vector<SdsSessionStartObserver*>::iterator iter = _sdsSessionStartedObservers.begin();
   while (iter != _sdsSessionStartedObservers.end())
   {
      if (*iter != NULL)
      {
         (*iter)->onSDSSessionEnd();
      }
      ++iter;
   }
}


void SdsAdapterProvider::vOnSmsHeaderVisibilityChanged(bool vehicleMovingSpeed)
{
   _isSmsHeaderVisible = vehicleMovingSpeed;
}


void SdsAdapterProvider::processSmsHeaderData(const ::boost::shared_ptr< sds_gui_fi::PopUpService::PopupRequestSignal >& signal)
{
   ETG_TRACE_USR4(("Read SMS,check vehicle Speed is less than 5mph :%s", _isSmsHeaderVisible == true ? "True" : "False"));
   if (_isSmsHeaderVisible == true)
   {
      processListData(signal);
      processLayout(signal);
      if (_pGuiUpdater)
      {
         ETG_TRACE_USR4(("speed is more than 5mph,SMS will  not displayed"));
         _pGuiUpdater->updateHeaderSpeakingIconIndex((unsigned int)signal->getSpeechInputState());
         _pGuiUpdater->updateHeaderTextData(" ");
      }
   }
   else
   {
      processListData(signal);
      processLayout(signal);
      processHeaderData(signal);
   }
}


} // Namespace Core
} // Namespace App
