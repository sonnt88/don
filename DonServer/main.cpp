#include "iostream"

#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

#define DEFAULT_BUFLEN 1024
#define DEFAULT_PORT "27015"

enum {
	CLIENT_PLAY = 0,
	CLIENT_VIEW
};
struct ClientThreadParam {
	short _clientType;
	SOCKET _clientSock;
	char *_msgData;
	unsigned _msgLen;
	bool _isUpdated;

	ClientThreadParam *_playParam;
};


void print_addr_inf(struct addrinfo *p) {
        char ipstr[INET6_ADDRSTRLEN];
        void *addr;
        char ipver[5];
        // get the pointer to the address itself,
        // different fields in IPv4 and IPv6:
        if (p->ai_family == AF_INET) { // IPv4
                struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
                addr = &(ipv4->sin_addr);
                strcpy(ipver, "IPv4");
        } else { // IPv6
                struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)p->ai_addr;
                addr = &(ipv6->sin6_addr);
                strcpy(ipver, "IPv6");
        }
        //convert the IP to a string and print it:
        inet_ntop(p->ai_family, addr, ipstr, sizeof ipstr);
		printf(" %s: %s\n", ipver, ipstr);
}

void
print_sockaddr(struct sockaddr* addr,const char *name)
{
    char addrbuf[128] ;

    addrbuf[0] = 0;
    if(addr->sa_family == AF_UNSPEC)
        return;
    switch(addr->sa_family) {
        case AF_INET:
            inet_ntop(addr->sa_family,&((struct sockaddr_in*)addr)->sin_addr,addrbuf,sizeof(addrbuf));
            break;
        case AF_INET6:
            inet_ntop(addr->sa_family,&((struct sockaddr_in6*)addr)->sin6_addr,addrbuf,sizeof(addrbuf));
            break;
        default:
            sprintf(addrbuf,"Unknown (%d)",(int)addr->sa_family);
            break;

    }
    printf("%-16s %s\n",name,addrbuf);
}

int getAddresses(char *host_name)
{
    //-----------------------------------------
    // Declare and initialize variables
    WSADATA wsaData;
    int iResult;

    DWORD dwError;
    int i = 0;

    struct hostent *remoteHost;
    /*char *host_name;*/
    struct in_addr addr;

    char **pAlias;

    // Validate the parameters
   /* if (argc != 2) {
        printf("usage: %s ipv4address\n", arg0);
        printf(" or\n");
        printf("       %s hostname\n", arg0);
        printf("  to return the host\n");
        printf("       %s 127.0.0.1\n", arg0);
        printf("  to return the IP addresses for a host\n");
        printf("       %s www.contoso.com\n", arg0);
        return 1;
    }*/
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed: %d\n", iResult);
        return 1;
    }

    //host_name = arg0;

// If the user input is an alpha name for the host, use gethostbyname()
// If not, get host by addr (assume IPv4)
    if (isalpha(host_name[0])) {        /* host address is a name */
        printf("Calling gethostbyname with %s\n", host_name);
        remoteHost = gethostbyname(host_name);
    } else {
        printf("Calling gethostbyaddr with %s\n", host_name);
        addr.s_addr = inet_addr(host_name);
        if (addr.s_addr == INADDR_NONE) {
            printf("The IPv4 address entered must be a legal address\n");
            return 1;
        } else
            remoteHost = gethostbyaddr((char *) &addr, 4, AF_INET);
    }

    if (remoteHost == NULL) {
        dwError = WSAGetLastError();
        if (dwError != 0) {
            if (dwError == WSAHOST_NOT_FOUND) {
                printf("Host not found\n");
                return 1;
            } else if (dwError == WSANO_DATA) {
                printf("No data record found\n");
                return 1;
            } else {
                printf("Function failed with error: %ld\n", dwError);
                return 1;
            }
        }
    } else {
        printf("Function returned:\n");
        printf("\tOfficial name: %s\n", remoteHost->h_name);
        for (pAlias = remoteHost->h_aliases; *pAlias != 0; pAlias++) {
            printf("\tAlternate name #%d: %s\n", ++i, *pAlias);
        }
        printf("\tAddress type: ");
        switch (remoteHost->h_addrtype) {
        case AF_INET:
            printf("AF_INET\n");
            break;
        case AF_INET6:
            printf("AF_INET6\n");
            break;
        case AF_NETBIOS:
            printf("AF_NETBIOS\n");
            break;
        default:
            printf(" %d\n", remoteHost->h_addrtype);
            break;
        }
        printf("\tAddress length: %d\n", remoteHost->h_length);

        if (remoteHost->h_addrtype == AF_INET) {
            while (remoteHost->h_addr_list[i] != 0) {
                addr.s_addr = *(u_long *) remoteHost->h_addr_list[i++];
                printf("\tIPv4 Address #%d: %s\n", i, inet_ntoa(addr));
            }
        } else if (remoteHost->h_addrtype == AF_INET6)
            printf("\tRemotehost is an IPv6 address\n");
    }

    return 0;
}

int doit()
{
    char ac[80];
    if (gethostname(ac, sizeof(ac)) == SOCKET_ERROR) {
        std::cerr << "Error " << WSAGetLastError() <<
                " when getting local host name." << std::endl;
        return 1;
    }
    std::cout << "Host name is " << ac << "." << std::endl;

#if 0
    struct hostent *phe = gethostbyname(ac);
    if (phe == 0) {
        std::cerr << "Yow! Bad host lookup." << std::endl;
        return 1;
    }

    for (int i = 0; phe->h_addr_list[i] != 0; ++i) {
        struct in_addr addr;
        memcpy(&addr, phe->h_addr_list[i], sizeof(struct in_addr));
        std::cout << "Address " << i << ": " << inet_ntoa(addr) << std::endl;
    }
#else
	getAddresses(ac);
#endif
    return 0;
}

DWORD WINAPI ComunicateToClient(LPVOID lpParam) {
	ClientThreadParam * params = (ClientThreadParam *)lpParam;
	do {
			char recvbuf[1024];
			int iResult = recv(params->_clientSock, recvbuf, 1024, 0);
			//int iResult = send(params->_connectSocket, params->_msgData, params->_msgLen, NULL);
			if (iResult == SOCKET_ERROR) {
				printf("received failed with error: %d\n", WSAGetLastError());
				closesocket(params->_clientSock);
				delete params;
				break;
			}

			if ( iResult > 0 ) {
				printf("Bytes Received: %d\n", iResult);
				recvbuf[iResult] = NULL;
				printf("%s", recvbuf);
				
#if 0
				iResult = send( params->_clientSock, recvbuf, iResult, 0 );
				if (iResult == SOCKET_ERROR) {
					printf("send failed with error: %d\n", WSAGetLastError());
					/*closesocket(ClientSocket);
					WSACleanup();
					return 1;*/
				}
				printf("Bytes sent: %d\n", iResult);
#endif
				if (params->_msgData) {
					delete[] params->_msgData;
					params->_msgData = NULL;
				}

				params->_msgData = new char[iResult + 1];
				std::fill(params->_msgData, params->_msgData + iResult + 1, NULL);
				strcpy(params->_msgData, recvbuf);
				params->_msgLen = iResult;
				params->_isUpdated = true;
				Sleep(1);
			}
			
	} while( true );

	return 0;
}

DWORD WINAPI ComunicateToClientAndroid(LPVOID lpParam) {
	ClientThreadParam * params = (ClientThreadParam *)lpParam;
	SOCKET clsock = params->_clientSock;
	ClientThreadParam *playParam = params->_playParam;
	do {
		if (playParam && playParam->_isUpdated) {
			int iResult = send(clsock, playParam->_msgData, playParam->_msgLen, NULL);
			if (iResult == SOCKET_ERROR) {
				printf("send failed with error: %d\n", WSAGetLastError());
				closesocket(params->_clientSock);
				delete params;
			}

			if ( iResult > 0 )
				printf("Bytes Sent: %d\n", iResult);

			playParam->_isUpdated = false;
		}
		Sleep(1);
	} while( true );

	return 0;
}

int __cdecl main(void) 
{
    WSADATA wsaData;

    int iResult;

    SOCKET ListenSocket = INVALID_SOCKET;
    SOCKET ClientSocket = INVALID_SOCKET;

    struct addrinfo *result = NULL;
    struct addrinfo hints;

    int iSendResult;
    char recvbuf[DEFAULT_BUFLEN];
    int recvbuflen = DEFAULT_BUFLEN;
	int nclient = 0;
    
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed with error: %d\n", iResult);
        return 1;
    }

#ifdef DEBUG_MODE
	doit();
#endif
    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    // Resolve the server address and port
    iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
    if ( iResult != 0 ) {
        printf("getaddrinfo failed with error: %d\n", iResult);
        WSACleanup();
        return 1;
    }

    // Create a SOCKET for connecting to server
    ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (ListenSocket == INVALID_SOCKET) {
        printf("socket failed with error: %ld\n", WSAGetLastError());
        freeaddrinfo(result);
        WSACleanup();
        return 1;
    }

	print_addr_inf(result);
    // Setup the TCP listening socket
    iResult = bind( ListenSocket, result->ai_addr, (int)result->ai_addrlen);
    if (iResult == SOCKET_ERROR) {
        printf("bind failed with error: %d\n", WSAGetLastError());
        freeaddrinfo(result);
        closesocket(ListenSocket);
        WSACleanup();
        return 1;
    }

    freeaddrinfo(result);

    iResult = listen(ListenSocket, SOMAXCONN);
    if (iResult == SOCKET_ERROR) {
        printf("listen failed with error: %d\n", WSAGetLastError());
        closesocket(ListenSocket);
        WSACleanup();
        return 1;
    }

    // Accept a client socket
	ClientThreadParam *playclientParam = NULL;
	while (true) {
		ClientSocket = accept(ListenSocket, NULL, NULL);
		if (ClientSocket == INVALID_SOCKET) {
			printf("accept failed with error: %d\n", WSAGetLastError());
			//closesocket(ListenSocket);
			//WSACleanup();
			//return 1;
		}

		if (nclient == 0) {
			playclientParam = new ClientThreadParam;
			playclientParam->_clientSock = ClientSocket;
			playclientParam->_clientType = CLIENT_PLAY;
			playclientParam->_isUpdated = false;
			playclientParam->_msgData = NULL;
			playclientParam->_msgLen = 0;
			playclientParam->_playParam = NULL;
			CreateThread(NULL, 0, ComunicateToClient, playclientParam, NULL, NULL);
		} else {
			ClientThreadParam *clientParam = new ClientThreadParam;
			clientParam->_clientSock = ClientSocket;
			clientParam->_clientType = CLIENT_VIEW;
			clientParam->_isUpdated = false;
			clientParam->_msgData = NULL;
			clientParam->_msgLen = 0;
			clientParam->_playParam = playclientParam;

			CreateThread(NULL, 0, ComunicateToClientAndroid, clientParam, NULL, NULL);
		}

		nclient++;

		std::cout << "number client:" << nclient;
	}
    // No longer need server socket
    closesocket(ListenSocket);

    // Receive until the peer shuts down the connection
    do {

        iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
        if (iResult > 0) {
            printf("Bytes received: %d\n", iResult);

        // Echo the buffer back to the sender
            iSendResult = send( ClientSocket, recvbuf, iResult, 0 );
            if (iSendResult == SOCKET_ERROR) {
                printf("send failed with error: %d\n", WSAGetLastError());
                closesocket(ClientSocket);
                WSACleanup();
                return 1;
            }
            printf("Bytes sent: %d\n", iSendResult);
        }
        else if (iResult == 0)
            printf("Connection closing...\n");
        else  {
            printf("recv failed with error: %d\n", WSAGetLastError());
            closesocket(ClientSocket);
            WSACleanup();
            return 1;
        }

    } while (iResult > 0);

    // shutdown the connection since we're done
    iResult = shutdown(ClientSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {
        printf("shutdown failed with error: %d\n", WSAGetLastError());
        closesocket(ClientSocket);
        WSACleanup();
        return 1;
    }

    // cleanup
    closesocket(ClientSocket);
    WSACleanup();

    return 0;
}