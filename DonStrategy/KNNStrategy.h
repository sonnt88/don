#ifndef __KNN_STRATEGY_H__
#define __KNN_STRATEGY_H__

/*
** K nearest neighbor strategy
*/

#include "StrategyBase.h"

class KNNStrategy: public StrategyBase {
public:
	static int const NUMBER_KNN;

public:
	KNNStrategy();
	KNNStrategy(GameController *gameCtrl);
	~KNNStrategy();

	/* 
	** virtual functions
	*/
	/*virtual*/ int makeDecision();
	/*virtual*/ void resetGameInfo();
	/*virtual*/ short getType();
	/*virtual*/ ostringstream dumInfo();

	/*
	** normal functions
	*/

protected:
	/*virtual*/ void updateGamewinner();
	/*virtual*/ void updateGameEnd();

private:
	float _baseProbBanker;
	float _baseProbPlayer;
};
#endif