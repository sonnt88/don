#include "BAStrategy.h"
#include "BASMoneyManagement.h"
#include "GameController.h"
#include "CommonDefine.h"
#include "GameInfo.h"
#include "GameStatistic.h"

using namespace std;

BAStrategy::BAStrategy(): StrategyBase() {
	initializeStateMachine();
}

BAStrategy::BAStrategy(GameController *gameCtrl):
				StrategyBase(gameCtrl)
{
	initializeStateMachine();
}

BAStrategy::~BAStrategy() {
	for (unsigned i = 0; i < SelectionState::TOTAL_STATE_SIZE; i++) {
		delete _selStates[i];
	}
}

void BAStrategy::initializeStateMachine()
{
	_selStates[SelectionState::SELECTION_STATE_A1] = new SelectionState_A1;
	_selStates[SelectionState::SELECTION_STATE_A2] = new SelectionState_A2;
	_selStates[SelectionState::SELECTION_STATE_B1] = new SelectionState_B1;
	_selStates[SelectionState::SELECTION_STATE_B2] = new SelectionState_B2;
	_selStates[SelectionState::SELECTION_STATE_B3] = new SelectionState_B3;

	_selStates[SelectionState::SELECTION_STATE_A1]->setPreState(nullptr);
	_selStates[SelectionState::SELECTION_STATE_A2]->setPreState(nullptr);
	_selStates[SelectionState::SELECTION_STATE_B1]->setPreState(nullptr);
	_selStates[SelectionState::SELECTION_STATE_B2]->setPreState(nullptr);
	_selStates[SelectionState::SELECTION_STATE_B3]->setPreState(nullptr);

	_currentState = _selStates[SelectionState::SELECTION_STATE_A1];
}

int BAStrategy::makeDecision()
{
	GameInfo *info = _gameController->getGameInfo();
	if (info->getPlayedRoundNumber() == 0) {
		return PB_UNDEFINED;
	}
	int lastWinner = _gameController->getGameStatistic()->lastWinner();
	if (lastWinner == TIE) {
		lastWinner = PLAYER;
	}

	return _currentState->makeDeciscion(lastWinner);
}

void BAStrategy::resetGameInfo()
{
	_selStates[SelectionState::SELECTION_STATE_A1]->setPreState(nullptr);
	_selStates[SelectionState::SELECTION_STATE_A2]->setPreState(nullptr);
	_selStates[SelectionState::SELECTION_STATE_B1]->setPreState(nullptr);
	_selStates[SelectionState::SELECTION_STATE_B2]->setPreState(nullptr);
	_selStates[SelectionState::SELECTION_STATE_B3]->setPreState(nullptr);

	_currentState = _selStates[SelectionState::SELECTION_STATE_A1];
}

short BAStrategy::getType()
{
	return STRATEGY_BACC_ATTACK_TYPE;
}

void BAStrategy::updateGamewinner()
{
	int isWin = _gameController->getGameInfo()->getIsCurrentWin();
	if (_gameController->getGameInfo()->hasBetted() &&
		isWin != DRAW) {
		bool bwin = isWin == WIN;
		jumpToNextState(bwin);
	}
}

void BAStrategy::updateGameEnd()
{
	_currentState = _selStates[SelectionState::SELECTION_STATE_A1];
}

SelectionState *BAStrategy::nextState(bool isWin)
{
	int stateType = _currentState->nextStateType(isWin);
	return _selStates[stateType];
}

void BAStrategy::jumpToNextState(bool isWin)
{
	SelectionState *nxtSt = nextState(isWin);
	nxtSt->setPreState(_currentState);
	_currentState = nxtSt;
}

ostringstream BAStrategy::dumInfo()
{
	ostringstream ostr;
	int lastWinner = _gameController->getGameStatistic()->lastWinner();
	ostr << "  Baccarat attack Strategy" << endl;
	ostr << "  Next Bet Selection: " << 
			SELECTED_BET_TO_STR(_currentState->makeDeciscion(lastWinner)) << endl;
	ostr << "  Current state:" << 
			_currentState->nameOfType(_currentState->getType()) << endl;

	return ostr;
}