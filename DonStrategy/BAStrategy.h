#ifndef __BA_STRATEGY_H__
#define __BA_STRATEGY_H__

#include "StrategyBase.h"
#include "SelectionState.h"

class BAStrategy: public StrategyBase {
public:
public:
	BAStrategy();
	BAStrategy(GameController *gameCtrl);
	~BAStrategy();

	/*
	** virtual functions
	*/
	/*virtual*/ int makeDecision();
	/*virtual*/ void resetGameInfo();
	/*virtual*/ short getType();
	/*virtual*/ ostringstream dumInfo();

	/*
	** normal functions
	*/
	void initializeStateMachine();
	SelectionState *nextState(bool isWin);
	void jumpToNextState(bool isWin);

protected:
	/*virtual*/ void updateGamewinner();
	/*virtual*/ void updateGameEnd();

private:
	SelectionState *_selStates[SelectionState::TOTAL_STATE_SIZE];
	SelectionState * _currentState;
};
#endif