#ifndef __GAME_INFO_H__
#define __GAME_INFO_H__

#include "vector"
#include "sstream"

class GameListener;

using namespace std;

class GameInfo {
public:
	GameInfo();
	~GameInfo();

	int getIsCurrentWin();
	int getLastWinner();
	int getSelectedWinner();
	int getNumberOfContinuousResult();
	int getPlayedRoundNumber();
	bool hasBetted();

	void setIsCurrentWin(int);
	void setLastWinner(int);
	void setSelectedWinner(int);
	void setHasBetted(bool);
	void setNumberOfContinuousResult(int);
	void setCurrentPlayedRoundNumber(int);

	void updateNewWinner(int winner);
	void resetGameInfo();

	void registerObserver(GameListener *listener);

	ostringstream dumpInfo();

private:
	/*
	** Data store game information
	*/
	int _isCurrentWin;  // Currently win the last round
	int _lastWinner;	// Last winner player/banker/tie
	int _selectedWinner;	// betted on winner or player
	int _playedRoundNumber;  // Current roun number was played
	int _numberOfContinuousResult; // current number of continuous result
	bool _hasbetted;

	/* List of observer objects */
	std::vector<GameListener *> _observers;
};

#endif