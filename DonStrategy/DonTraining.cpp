#include "DonTraining.h"
#include "RecordIO.h"
#include "DonKnowledge.h"
#include "CommonDefine.h"
#include "StrategyBase.h"
#include "iostream"

DonTraining::DonTraining(DonKnowledge *knowledge):
					_knowledge(knowledge)/*,
						_numSeries3(0),
						_numSeries4(0),
						_numSeries5(0),
						_numSeries6(0)*/
{
	std::fill(_counterSeries3, _counterSeries3 + 27, 0);
	std::fill(_counterSeries4, _counterSeries4 + 81, 0);
	std::fill(_counterSeries5, _counterSeries5 + 243, 0);
	std::fill(_counterSeries6, _counterSeries6 + 729, 0);
}

DonTraining::~DonTraining()
{

}

void DonTraining::doTrainingFromRecords()
{
	RecordIO::GameRCDInfo gamercd;
	RecordIO *rcdIo = RecordIO::getInstance();
	std::ifstream readStream;
	long rcdlength = rcdIo->openGameRecords(readStream);
	if (rcdlength <=0 ) {
		assert("failed to open game records");
		return;
	}

	// num busted
	int totalgames = 0;
	int numbusted = 0;
	int numReached = 0;
	float lostmoney = 0;
	float totalwon = 0;
	float minwon = FLT_MAX;
	int numnonwon = 0;
	float maxFinalLost = FLT_MAX;
	float maxLostOnlyPlayer = FLT_MAX;
	//int sumWin, sumLose;
	//int lostCont[20];

	while ((long)readStream.tellg() < rcdlength) {
#ifdef DEBUG_MODE
		long l1 = (long)readStream.tellg();
#endif
		rcdIo->readAGameInfo(readStream, gamercd);
		trainFromAGame(gamercd);
#ifdef DEBUG_MODE
		long length = (long)readStream.tellg();
		if (gamercd._numround < 45) {
			int bug = 0;
			//readStream.seekg(l1);
			//continue;
		}
#endif
		if (gamercd._strategyType != StrategyBase::STRATEGY_KNN_TYPE)
			continue;
		totalgames++;
#ifdef DEBUG_MODE
		if (totalgames > 73) {
			int bug = 0;
			//continue;
		}
		
		if (gamercd._numround > 75) {
			int vl = 1;
		}
		if (gamercd._wonMoney < -75) {
			int vl = 1;
		}
#endif
		if (gamercd._bustedRound > 1 /*&& 
			(gamercd._bustedRound < gamercd._reachedProfitRound ||
			gamercd._reachedProfitRound < 0)*/) {
			numbusted ++;
		} else if (gamercd._reachedProfitRound > 1) {
			numReached++;
		} else /*if (gamercd._wonMoney < 0)*/{
			lostmoney += gamercd._wonMoney;
		}

		if (gamercd._bustedRound > 1) totalwon -= 75;
		else 
			totalwon += gamercd._wonMoney;

		if (gamercd._maxWonMoney <3) {
			minwon = gamercd._maxWonMoney;
			numnonwon++;
		}

		_knowledge->analyzeWon();
		float finalWon = _knowledge->getFinalWon();
		float finalWonPlayerOnly = _knowledge->getFinalWonPlayerOnly();
		if (maxFinalLost > finalWon) {
			maxFinalLost = finalWon;
		}
		if (maxLostOnlyPlayer > finalWonPlayerOnly) {
			maxLostOnlyPlayer = finalWonPlayerOnly;
		}

		if (totalgames % 10 == 0) {
			std::cout << "moneyWon = " << totalwon << std::endl;
#if 0
			std::cout << "finalWon = " << finalWon << std::endl;
			std::cout << "finalWonPlayerOnly =" << finalWonPlayerOnly << std::endl;
#endif
		}
	}

	std::cout << "total games:" << totalgames <<endl;
	std::cout << "busted: " << numbusted<<endl;
	std::cout << "reached: " <<numReached<<endl;
	std::cout << "lost money: " << lostmoney<<endl;
	std::cout << "total won: " << totalwon<<endl;

	updateProbOfSeries3();
	updateProbOfSeries4();
	updateProbOfSeries5();
	updateProbOfSeries6();
#if 0
	_knowledge->analyzeWon();
	float finalWon = _knowledge->getFinalWon();
	float finalWonPlayerOnly = _knowledge->getFinalWonPlayerOnly();
	std::cout << "finalWon = " << finalWon << std::endl;
	std::cout << "finalWonPlayerOnly =" << finalWonPlayerOnly << std::endl;
#endif
	readStream.close();
}

void DonTraining::trainFromAGame(RecordIO::GameRCDInfo &gameInfo)
{
	int nround = gameInfo._numround;
	int nlostcont = 0;

	if (nround < 0) return;

	for (unsigned i = 0; i < (unsigned)nround; ++i)
	{
		RecordIO::RoundRCDInfo roundinfo = gameInfo._roundlist[i];
		roundinfo._winner;

		if (gameInfo._strategyType == StrategyBase::STRATEGY_KNN_TYPE) {
			if (roundinfo._result != TIE) {
				if (roundinfo._result == PLAYER) {
					_knowledge->updateNPlayer();
				} else if (roundinfo._result == BANKER) {
					_knowledge->updateNBanker();
				}

				if (roundinfo._winner == WIN) {
					_knowledge->updateNWon();
					if (roundinfo._result == PLAYER) {
						_knowledge->updateNWonPlayer();
					} else if (roundinfo._result == BANKER) {
						_knowledge->updateNWonBanker();
					} else {
						int vl = 1;
					}
					if (nlostcont > 0) {
						_knowledge->updateNLostContinuous(nlostcont - 1);
					}
					nlostcont = 0;
				} else if (roundinfo._winner == LOSE){
					_knowledge->updateNLost();
					if (roundinfo._result == PLAYER) {
						_knowledge->updateNLostPlayer();
					} else if (roundinfo._result == BANKER) {
						_knowledge->updateNLostBanker();
					} else {
						int vl = 1;
					}
					nlostcont++;
				}
			} else {
				// do nothing
			}
		}

		// Calc probability for 3 round
		if (i > 1) {
			//_numSeries3++;
			short x0 = gameInfo._roundlist[i - 2]._winner;
			short x1 = gameInfo._roundlist[i - 1]._winner;
			short x2 = roundinfo._winner;
			long ind = TRINARY_TO_DEC_3(x0, x1, x2);
			assert (ind < 27);
			_counterSeries3[ind]++;
		}

		if (i > 2) {
			//_numSeries4++;
			short x0 = gameInfo._roundlist[i - 3]._winner;
			short x1 = gameInfo._roundlist[i - 2]._winner;
			short x2 = gameInfo._roundlist[i - 1]._winner;
			short x3 = roundinfo._winner;
			long ind = TRINARY_TO_DEC_4(x0, x1, x2, x3);
			assert (ind < 81);
			_counterSeries4[ind]++;
		}

		if ( i > 3) {
			//_numSeries5++;
			short x0 = gameInfo._roundlist[i - 4]._winner;
			short x1 = gameInfo._roundlist[i - 3]._winner;
			short x2 = gameInfo._roundlist[i - 2]._winner;
			short x3 = gameInfo._roundlist[i - 1]._winner;
			short x4 = roundinfo._winner;
			long ind = TRINARY_TO_DEC_5(x0, x1, x2, x3, x4);
			assert (ind < 243);
			_counterSeries5[ind]++;
		}

		if (i > 4) {
			//_numSeries6++;
			short x0 = gameInfo._roundlist[i - 5]._winner;
			short x1 = gameInfo._roundlist[i - 4]._winner;
			short x2 = gameInfo._roundlist[i - 3]._winner;
			short x3 = gameInfo._roundlist[i - 2]._winner;
			short x4 = gameInfo._roundlist[i - 1]._winner;
			short x5 = roundinfo._winner;
			long ind = TRINARY_TO_DEC_6(x0, x1, x2, x3, x4, x5);
			assert (ind < 729);
			_counterSeries6[ind]++;
		}
	}

	_knowledge->updateNLostContinuous(nlostcont - 1);
}

void DonTraining::updateProbOfSeries3()
{
	long numSeries3 = 0;
	for (int i = 0; i < POW_3_3; ++i) {
		numSeries3+= _counterSeries3[i];
	}
	
	for (int i = 0; i < POW_3_3; ++i) {
		float prob = (float)_counterSeries3[i] / numSeries3;
		_knowledge->updateProbOfSeries3(i, prob);
	}
}

void DonTraining::updateProbOfSeries4()
{
	long numSeries4 = 0;
	for (int i = 0; i < POW_3_4; ++i) {
		numSeries4 += _counterSeries4[i];
	}

	for (int i = 0; i < POW_3_4; ++i) {
		float prob = (float)_counterSeries4[i] / numSeries4;
		_knowledge->updateProbOfSeries4(i, prob);
	}
}

void DonTraining::updateProbOfSeries5()
{
	long numSeries5 = 0;
	for (int i = 0; i < POW_3_5; ++i) {
		numSeries5 += _counterSeries5[i];
	}

	for (int i = 0; i < POW_3_5; ++i) {
		float prob = (float)_counterSeries5[i] / numSeries5;
		_knowledge->updateProbOfSeries5(i, prob);
	}
}

void DonTraining::updateProbOfSeries6()
{
	long numSeries6 = 0;
	for (int i = 0; i < POW_3_6; ++i) {
		numSeries6 += _counterSeries6[i];
	}

	for (int i = 0; i < POW_3_6; ++i) {
		float prob = (float)_counterSeries6[i] / numSeries6;
		_knowledge->updateProbOfSeries6(i, prob);
	}
}