#ifndef __RECORD_IO_H__
#define __RECORD_IO_H__

#include "fstream"
#include "vector"

class GameController;

/* + Game info:
		- num_rounds: .............................. 2 bytes (short)
		- final won money of game: ................. 4 bytes (float)
		- max betted money:......................... 4 bytes (float)
		- max needed money in bank roll:............ 4 bytes (float)
		- max won money in game:.................... 4 bytes (float)
		- max lost money in game:................... 4 bytes (float)
		- selection strategy type:.................. 1 bytes (char)
		- money management type:.................... 1 bytes
		- round busted.............................. 1 bytes
		- round reached profit...................... 1 bytes
		------------------------------------------------------------
										 used ===== 26 bytes
	  + reserve: 256 - 26 = 230 bytes

	  + Round info: total 4 bytes
			winner:................... 2 bits
			won/lost:................. 2 bits
			reserve:..................28 bits

	====> total bytes: (256 + nround * 4)
*/

class RecordIO {
public:
	struct RoundRCDInfo {
		unsigned _result:2;  // lowest bit
		unsigned _winner:2;
		unsigned _reserve:28;
	};

	struct GameRCDInfo {
		short _numround;
		float _wonMoney;
		float _maxBettedMoney;
		float _maxNeededMoney;
		float _maxWonMoney;
		float _maxLostMoney;
		char  _strategyType;
		char  _mmType;
		char  _bustedRound;
		char  _reachedProfitRound;
		std::vector<RoundRCDInfo> _roundlist;
	};
	
	~RecordIO();

private:
	RecordIO();

public:
	static RecordIO *getInstance();
	void writeGameRecord(GameController *);
	std::vector<RecordIO::GameRCDInfo> readGameRecords();
	std::vector<RecordIO::GameRCDInfo> readGameRecords(const std::string &filePath);
	void readAGameInfo(std::ifstream &fi, GameRCDInfo &gamercd);
	long openGameRecords(std::ifstream &readStream);

	void writeAGameInfoToFile(std::ofstream &fo, GameController *gamectrler);
	void writeARoundToFile(std::ofstream &fo, const RoundRCDInfo &round);
	void writeOverall();

public:
	const char *_gameInfoRecordName;
	const char *_overallRecordName;
private:
	static RecordIO *_recordIOInstance;
};
#endif