#ifndef __STRATEGY_BASE_H__
#define __STRATEGY_BASE_H__

#include "MoneyManagement.h"
#include "istream"
#include <sstream>

using namespace std;

class GameController;

class StrategyBase: public GameListener {

public:
	enum {
		STRATEGY_BASE_TYPE = -1,
		STRATEGY_BACC_ATTACK_TYPE,
		STRATEGY_KNN_TYPE,
		STRATEGY_NAIVE_BAYES_TYPE
	};
public:
	static StrategyBase *makeStrategy(short type);
	StrategyBase();
	StrategyBase(GameController *gameController);
	virtual ~StrategyBase();
	virtual int makeDecision() = 0;
	virtual void updateGameInfo(UpdateMessageType msg);
	virtual void resetGameInfo();
	virtual short getType();
	virtual ostringstream dumInfo() = 0;
	GameController *getGameController();
	void setGameController(GameController *gctrl);

protected:
	virtual void updateGamewinner(); // update info/status after end of round
	virtual void updateGameEnd();

protected:
	GameController *_gameController;
};

#endif