#include "GameSimulation.h"
#include "GameController.h"
#include "StrategyBase.h"
#include "MoneyManagement.h"
#include "RecordIO.h"

GameSimulation::GameSimulation()
{
	_gameController = new GameController();
}

GameSimulation::GameSimulation(short strategytype, short monmantype)
{
	_gameController->setStrategy(StrategyBase::makeStrategy(strategytype));
	_gameController->setMoneyManagement(MoneyManagement::makeMoneyManagement(monmantype));
}

GameSimulation::~GameSimulation()
{
	delete _gameController;
}

int GameSimulation::playAGame(const std::vector<short> &winnerList)
{
	unsigned nround = winnerList.size();

	for (unsigned i = 0; i < nround; ++i)
	{
		_gameController->updateNewWinner(winnerList[i]);
		_gameController->doBet();
		//_gameController->dumpGameInfo(false);
	}

	//_gameController->dumpGameInfo(true);
	return nround;
}

int GameSimulation::playAGame(const RecordIO::GameRCDInfo &gamercd)
{
	std::vector<short> winnerlist;
	unsigned nrounds = gamercd._roundlist.size();
	winnerlist.resize(nrounds);

	for (unsigned i = 0; i < nrounds; ++i)
	{
		winnerlist[i] = gamercd._roundlist[i]._winner;
	}
	
	return playAGame(winnerlist);
}

void GameSimulation::setStrategy(short strategytype)
{
	_gameController->setStrategy(
						StrategyBase::makeStrategy(strategytype));
}

void GameSimulation::setMoneyManagement(short monmantype)
{
	_gameController->setMoneyManagement(
						MoneyManagement::makeMoneyManagement(monmantype));
}

void GameSimulation::simulation(const std::string &gameData)
{
	RecordIO *rcd = RecordIO::getInstance();
	std::vector<RecordIO::GameRCDInfo> games = rcd->readGameRecords();
	for (auto it = games.begin(); it != games.end(); ++it)
	{
		playAGame(*it);
	}
}

void GameSimulation::resetGameInfo()
{
	_gameController->resetGameInfo();
}

float GameSimulation::getWonMoney()
{
	return _gameController->wonMoney();
}