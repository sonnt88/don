#ifndef __GAME_STATISTIC_H__
#define __GAME_STATISTIC_H__

#include "GameListener.h"

class GameInfo;

class GameStatistic: public GameListener {
public:
	GameStatistic(GameInfo *gameinf);
	~GameStatistic();

	/* virtual */ void updateGameInfo(UpdateMessageType msg);

	int getMaxNumberOfWonCont();
	int getMaxNumberOfLoseCont();
	float getMaxBetMoney();
	float getMaxMoneyNeededInBankroll();
	float getMaxMoneyWon();
	float getMaxMoneyLost();
	int getRoundBusted();
	int getRoundReachExpectedProfit();

	void setMaxNumberOfWonCont(int);
	void setMaxNumberOfLoseCont(int);
	void setMoneyManagement(MoneyManagement *mm);

	void resetGameInfo();
	void onBettedMoney(float money);
	const std::vector<short> &getWinloseList();
	const std::vector<int> &getOutcomeRounds();
	short numWonInLastNRounds(int nrounds);
	short numLostInLastNRounds(int nrounds);
	short calcNumWonRounds();
	short calcNumLostRounds();
	short lastWinner(bool skipTie = true);
	short numberOfLastContinuousResult(short result);
	short getLastNRoundWinner(short n, short *winner);

	ostringstream dumpInfo();

private:
	void updateGameWinner();
	void updateGameEnd();

private:
	int _maxNumberOfWonCont; // Max number won continuously
	int _maxNumberOfLoseCont; // Max number lose Continuously
	float _maxBettedMoney;
	float _maxMoneyNeededInBankRoll; // plus of losing money and next bet money
	float _maxMonneyWon;  // Max money has won
	float _maxMoneyLost;  // Max money has lost

	int _roundBusted; // round busted
	int _roundReachExpectedProfit; // round has reached expected profit

	std::vector<int> _outcomeRounds;  // result of played rounds
	std::vector<short> _winloseList;  // list of win/lose result

	GameInfo *_gameInfo;
	MoneyManagement *_moneymgmt;
};
#endif