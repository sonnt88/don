#include "RecordIO.h"
#include "GameController.h"
#include "GameStatistic.h"
#include "MoneyManagement.h"
#include "StrategyBase.h"
#include "CommonDefine.h"
#include "iostream"
#include "fstream"

#define GAME_RECORD_NAME "games.rcd"
#define OVERALL_RECORD_NAME "overall.rcd"

RecordIO * RecordIO::_recordIOInstance = NULL;

RecordIO::RecordIO():
				_gameInfoRecordName(GAME_RECORD_NAME),
				_overallRecordName(OVERALL_RECORD_NAME)
{

}

RecordIO::~RecordIO()
{

}

RecordIO *RecordIO::getInstance()
{
	if (_recordIOInstance == NULL) {
		_recordIOInstance = new RecordIO;
	}

	return _recordIOInstance;
}

void RecordIO::writeGameRecord(GameController *_gameController)
{
	ofstream fo(_gameInfoRecordName, ios::app | ios::binary);
	if (fo.is_open() == false) {
		assert("cannot open file to write");
	}

	writeAGameInfoToFile(fo, _gameController);
	fo.close();
}

std::vector<RecordIO::GameRCDInfo> RecordIO::readGameRecords()
{
	std::vector<RecordIO::GameRCDInfo> games;
	std::ifstream fi(GAME_RECORD_NAME, std::ios::binary);
	if (false == fi.is_open()) {
		std::cout << "can not open file to read"<< std::endl;
		return games;
	}

	int numGames = 0;
	fi.seekg (0, ios::end);
	long length = (long)fi.tellg();
	fi.seekg (0);

	while (fi.good() && fi.eof() == false &&
		   (long)fi.tellg() < length) {
		GameRCDInfo gamercd;
		readAGameInfo(fi, gamercd);
		games.push_back(gamercd);
		numGames++;
	}

	fi.close();

	return games;
}

std::vector<RecordIO::GameRCDInfo> RecordIO::readGameRecords(const std::string &filePath)
{
	std::vector<RecordIO::GameRCDInfo> games;
	std::ifstream fi(filePath.c_str(), std::ios::binary);
	if (false == fi.is_open()) {
		std::cout << "can not open file to read"<< std::endl;
		return games;
	}

	int numGames = 0;
	fi.seekg (0, ios::end);
	long length = (long)fi.tellg();
	fi.seekg (0);

	while (fi.good() && fi.eof() == false &&
		   (long)fi.tellg() < length) {
		GameRCDInfo gamercd;
		readAGameInfo(fi, gamercd);
		games.push_back(gamercd);
		numGames++;
	}

	fi.close();

	return games;
}

void RecordIO::readAGameInfo(std::ifstream &fi, GameRCDInfo &gamercd)
{
	short nrounds;
	float money;
	char roundinf[4];
	char type;
	short winner = 0;
	short result = 0;
	ostringstream ostr;

	fi.read((char *)&nrounds, 2);
	ostr << "rounds: " << nrounds << std::endl;
	gamercd._numround = nrounds;

	fi.read((char *)&money, 4);
	gamercd._wonMoney = money;
	ostr << "money won:....................." << money << std::endl;
	
	fi.read((char *)&money, 4);
	gamercd._maxBettedMoney = money;
	ostr << "max betted money:.............:" << money << std::endl;

	fi.read((char *)&money, 4);
	gamercd._maxNeededMoney = money;
	ostr << "max needed money in bank roll: " << money << std::endl;

	fi.read((char *)&money, 4);
	gamercd._maxWonMoney = money;
	ostr << "max won money in game:........ " << money << std::endl;

	fi.read((char *)&money, 4);
	gamercd._maxLostMoney = money;
	ostr << "max lost money in game:....... " << money << std::endl;

	fi.read(&type, 1);
	gamercd._strategyType = type;
	ostr << "strategy type:................ " << (int)type << std::endl;

	fi.read(&type, 1);
	gamercd._mmType = type;
	ostr << "money management type:........ " << (int)type << std::endl;

	fi.read(&type, 1);
	gamercd._bustedRound = type;
	ostr << "busted round:................. " << (int)type << std::endl;

	fi.read(&type, 1);
	gamercd._reachedProfitRound = type;
	ostr << "reached profit round:......... " << (int)type << std::endl;

	// jump
	for (unsigned i = 0; i < 230; ++i) {
		char c;
		fi.read(&c, 1);
	}

	ostr << "Rounds:" << std::endl;
	std::vector<short> roundoutcome(nrounds);
	std::vector<short> winloselist(nrounds);
	gamercd._roundlist.resize(nrounds);
	RoundRCDInfo rnd;
	for (short i = 0; i < nrounds; ++i) {
		fi.read(roundinf, 4);
		rnd = *(RoundRCDInfo*)(roundinf);
		winner = (0xc & ( roundinf[0])) >> 2;
		result = 0x03 & roundinf[0];
		roundoutcome[i] = winner;
		winloselist[i] = result;

		gamercd._roundlist[i] = rnd;
	}

	for (short i = 0; i < nrounds; ++i) {
		ostr << roundoutcome[i] << " ";
	}
	ostr << std::endl;

	for (short i = 0; i < nrounds; ++i) {
		ostr << winloselist[i] << " ";
	}
	ostr << std::endl;
	ostr << std::endl;

	//cout << ostr.str();
}

long RecordIO::openGameRecords(std::ifstream &readStream)
{
	readStream.open(GAME_RECORD_NAME, std::ios::binary);
	if (false == readStream.is_open()) {
		std::cout << "can not open file to read"<< std::endl;
		return 0;
	}

	readStream.seekg (0, ios::end);
	long length = (long)readStream.tellg();
	readStream.seekg (0);

	return length;
}

void RecordIO::writeAGameInfoToFile(ofstream &fo, GameController *gamectrler)
{
	/* + Game info:
		- num_rounds: .............................. 2 bytes (short)
		- final won money of game: ................. 4 bytes (float)
		- max betted money:......................... 4 bytes (float)
		- max needed money in bank roll:............ 4 bytes (float)
		- max won money in game:.................... 4 bytes (float)
		- max lost money in game:................... 4 bytes (float)
		- selection strategy type:.................. 1 bytes (char)
		- money management type:.................... 1 bytes
		- round busted.............................. 1 byte
		- round reached profit...................... 1 byte
		------------------------------------------------------------
										 used ===== 26 bytes
	  + reserve: 256 - 26 = 230 bytes
	  <Rounds>
	*/

	if (false == fo.is_open()) {
		assert("file is not opened");
		return;
	}

	GameStatistic *gameStatistic = gamectrler->getGameStatistic();
	const std::vector<int> &allRounds = gameStatistic->getOutcomeRounds();
	const std::vector<short> &winloseList = gameStatistic->getWinloseList();
	MoneyManagement *moneymgnt = gamectrler->getMoneyManagement();
	StrategyBase *strategy = gamectrler->getStragtegy();
	short nround = allRounds.size();
	float fnum;
	char type;
	const short NUM_RESERVE_BYTES = 230;
	char gamereserve[NUM_RESERVE_BYTES ];
	std::fill(gamereserve, gamereserve + NUM_RESERVE_BYTES, NULL);

	//assert(nround > 0 && nround == winloseList.size());
	if (nround < 50) return;
	/*
	** write game header
	*/
	/* num rounds */
	fo.write((char *)&nround, 2);
	
	/* money won */
	fnum = moneymgnt->getGameWon();
	fo.write((char *)&fnum, 4);

	/* max bet money */
	fnum = gameStatistic->getMaxBetMoney();
	fo.write((char *)&fnum, 4);

	/* max money needed in bankroll */
	fnum = gameStatistic->getMaxMoneyNeededInBankroll();
	fo.write((char *)&fnum, 4);

	/* max won money in game */
	fnum = gameStatistic->getMaxMoneyWon();
	fo.write((char *)&fnum, 4);

	/* max won money in game */
	fnum = gameStatistic->getMaxMoneyLost();
	fo.write((char *)&fnum, 4);

	/* strategy type */
	type = (char)strategy->getType();
	fo.write(&type, 1);

	/* money management type */
	type = (char)moneymgnt->getType();
	fo.write(&type, 1);

	/* busted round */
	type = (char)gameStatistic->getRoundBusted();
	fo.write(&type, 1);

	/* round has reached expected profit */
	type = (char)gameStatistic->getRoundReachExpectedProfit();
	fo.write(&type, 1);

	/* reserve bytes */
	fo.write(gamereserve, NUM_RESERVE_BYTES);

	/*
	** write rounds
	*/
	for (short i = 0; i < nround; ++i)
	{
		RoundRCDInfo iround;
		iround._winner = allRounds[i];
		iround._result = winloseList[i];

		writeARoundToFile(fo, iround);
	}
}

void RecordIO::writeARoundToFile(std::ofstream &fo, const RoundRCDInfo &round)
{
	/*
	  + Round info: total 4 bytes
			winner:................... 2 bits
			won/lost:................. 2 bits
			reserve:..................28 bits
	*/
	int buff = 0;
	int winner = round._winner;
	int resul = round._result;

	buff |= winner;
	buff <<= 2;
	buff |= resul;

	fo.write((char *)&buff, 4);
}