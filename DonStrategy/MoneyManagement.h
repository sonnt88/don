#ifndef __MONEY_MANAGEMENT_H__
#define __MONEY_MANAGEMENT_H__

#include "sstream"
#include "GameListener.h"

using namespace std;

class GameController;

class MoneyManagement: public GameListener {
public:
	enum {
		MONEY_MNGMNT_BASE_TYPE = -1,
		MONEY_MNGMNT_BACC_ATTACK_TYPE
	};

public:
	MoneyManagement();
	MoneyManagement(GameController *gameCtrl);
	virtual ~MoneyManagement();

	static MoneyManagement *makeMoneyManagement(short type);

	/*
	**	return next multiply times of money unit
	*/
	virtual float moneyForNextBet() = 0;
	virtual void updateGameInfo(UpdateMessageType msg);
	virtual void resetGameInfo();  // call to reset information after end of game
	virtual short getType();
	virtual ostringstream dumpInfo();

	void setBaseBet(int basemoney);
	void setGameController(GameController *gmctr);
	GameController *getGameController();
	int getBaseBet();
	float getExpectedProfit();
	float getGameWager();
	float getGameWon();
	float getGameBankroll();
	float getTotalWager();
	float getTotalWon();
	bool isGameBusted();
	bool isReachedExpectedProfit();

protected:
	virtual void updateGameWinner();
	virtual void updateGameEnd();

protected:
	int _baseBet;  // money unit
	float _expectedProfit;
	float _gameWager; // amount of money already wager for a game
	float _gameWon; // game amount won/lose: negative - lose; possitive - win
	float _gameBankroll; // bank roll for a game
	
	float _totalWager; // total amount of money already wager for all games
	float _totalWon;   // total amount of won/lose: negative - lose

	GameController *_gameController;
};
#endif