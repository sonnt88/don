#ifndef __GAME_SIMULATION_H__
#define __GAME_SIMULATION_H__

#include "vector"
#include "RecordIO.h"

class GameController;

class GameSimulation {
public:
	GameSimulation();
	GameSimulation(short strategytype, short monmantype);
	~GameSimulation();

	int playAGame(const std::vector<short> &winnerList);
	int playAGame(const RecordIO::GameRCDInfo &gamercd);
	void setStrategy(short strategytype);
	void setMoneyManagement(short monmantype);
	void simulation(const std::string &gameData); // simulation all games from data file name
	void resetGameInfo();
	GameController *getGameController() {return _gameController;};
	float getWonMoney();
private:
	GameController *_gameController;
};
#endif