#include "DonKnowledge.h"
#include "DonTraining.h"
#include "CommonDefine.h"

DonKnowledge* DonKnowledge::_knowledgeInstance = nullptr;

DonKnowledge::DonKnowledge()
{
	_training = new DonTraining(this);
	std::fill(_probOfSeries3, _probOfSeries3 + 27, 0.f);
	std::fill(_probOfSeries4, _probOfSeries4 + 81, 0.f);
	std::fill(_probOfSeries5, _probOfSeries5 + 243, 0.f);
	std::fill(_probOfSeries6, _probOfSeries6 + 729, 0.f);

	std::fill(_lostNcontinuous, _lostNcontinuous + MAX_LOST_CONTINUOUS, 0);

	_nWon = 0;
	_nLost = 0;
	_nWonBanker = 0;
	_nWonPlayer = 0;
	_nLostBanker = 0;
	_nLostPlayer = 0;
	_nBanker = 0;
	_nPlayer = 0;

	_finalWon = 0.;
	_finalWonPlayerOnly = 0.;
	// TODO: check here
	//_training->doTrainingFromRecords();
}

DonKnowledge::~DonKnowledge()
{
	if (_training)
		delete _training;
}

DonKnowledge *DonKnowledge::getInstance()
{
	if (nullptr == _knowledgeInstance) {
		_knowledgeInstance = new DonKnowledge;
	}

	return _knowledgeInstance;
}

float *DonKnowledge::getProbOfSeries3()
{
	return _probOfSeries3;
}

float *DonKnowledge::getProbOfSeries4()
{
	return _probOfSeries4;
}

float *DonKnowledge::getProbOfSeries5()
{
	return _probOfSeries5;
}

float *DonKnowledge::getProbOfSeries6()
{
	return _probOfSeries6;
}

float DonKnowledge::getProbOfSeries3(int ind)
{
	return _probOfSeries3[ind];
}

float DonKnowledge::getProbOfSeries4(int ind)
{
	return _probOfSeries4[ind];
}

float DonKnowledge::getProbOfSeries5(int ind)
{
	return _probOfSeries5[ind];
}

float DonKnowledge::getProbOfSeries6(int ind)
{
	return _probOfSeries6[ind];
}
	
void DonKnowledge::updateProbOfSeries3(long ind, float prob)
{
	assert(ind < POW_3_3);
	_probOfSeries3[ind] = prob;
}

void DonKnowledge::updateProbOfSeries4(long ind, float prob)
{
	assert(ind < POW_3_4);
	_probOfSeries4[ind] = prob;
}

void DonKnowledge::updateProbOfSeries5(long ind, float prob)
{
	assert(ind < POW_3_5);
	_probOfSeries5[ind] = prob;
}

void DonKnowledge::updateProbOfSeries6(long ind, float prob)
{
	assert(ind < POW_3_6);
	_probOfSeries6[ind] = prob;
}

void DonKnowledge::updateNLostContinuous(int ind)
{
	if (ind < 0)
		return;
	assert(ind >=0 && ind < MAX_LOST_CONTINUOUS);
	_lostNcontinuous[ind]++;
}

void DonKnowledge::trainingFromRecords()
{
	_training->doTrainingFromRecords();
}

void DonKnowledge::updateNWon()
{
	_nWon++;
}

void DonKnowledge::updateNLost()
{
	_nLost++;
}

void DonKnowledge::updateNWonBanker()
{
	_nWonBanker++;
}

void DonKnowledge::updateNWonPlayer()
{
	_nWonPlayer++;
}

void DonKnowledge::updateNBanker()
{
	_nBanker++;
}

void DonKnowledge::updateNLostBanker()
{
	_nLostBanker++;
}

void DonKnowledge::updateNLostPlayer()
{
	_nLostPlayer++;
}

void DonKnowledge::updateNPlayer()
{
	_nPlayer++;
}

float DonKnowledge::getFinalWon()
{
	return _finalWon;
}

float DonKnowledge::getFinalWonPlayerOnly()
{
	return _finalWonPlayerOnly;
}

void DonKnowledge::analyzeWon()
{
	unsigned int totalRound = 0; // number of won/lost rounds
	float wonBeforeCommission = 0.;
	float wonAfterCommission = 0.;
	float wonReturn = 0.;
	const float returnRate = 0.002f;
	
	totalRound = _nWon + _nLost;
	wonBeforeCommission = (float)(_nWon - _nLost);
	wonAfterCommission = _nWonBanker * 0.95f + _nWonPlayer - _nLost;
	_finalWon = wonAfterCommission + totalRound * returnRate;

	// 
	_finalWonPlayerOnly = (_nWonPlayer - _nLostBanker);
	_finalWonPlayerOnly += returnRate * (float)(_nWonPlayer + _nLostBanker);
}

void DonKnowledge::resetAll()
{
	_nWon = 0;
	_nLost = 0;
	_nWonBanker = 0;
	_nWonPlayer = 0;
	_nLostBanker = 0;
	_nLostPlayer = 0;
	_nBanker = 0;
	_nPlayer = 0;

	_finalWon = 0.;
	_finalWonPlayerOnly = 0.;
}