#include "KNNStrategy.h"
#include "CommonDefine.h"
#include "GameController.h"
#include "GameStatistic.h"
#include "GameInfo.h"

int const KNNStrategy::NUMBER_KNN = 6;

KNNStrategy::KNNStrategy():
				StrategyBase(),
				_baseProbBanker(0.56f),
				_baseProbPlayer(0.43f)
{

}

KNNStrategy::KNNStrategy(GameController *gameCtrl):
			StrategyBase(gameCtrl),
			_baseProbBanker(0.56f),
			_baseProbPlayer(0.43f)
{

}

KNNStrategy::~KNNStrategy()
{

}

int KNNStrategy::makeDecision()
{
	GameInfo *gameinfo = _gameController->getGameInfo();
	GameStatistic *gameStatis = _gameController->getGameStatistic();
	std::vector<int> outcomes;
	int n = gameinfo->getPlayedRoundNumber();
	if (n < NUMBER_KNN) {
		return PB_UNDEFINED;
	}

	float nnProbBanker = 0.0;
	float nnProbPlayer = 0.0;
	float step = 1.0f / NUMBER_KNN;
	outcomes = gameStatis->getOutcomeRounds();
	for (int i = 0; i < NUMBER_KNN; i++) {
		if (outcomes[n - 1 - i] == BANKER) {
			nnProbBanker += step;
		} else if (outcomes[n - 1 - i] == PLAYER) {
			nnProbPlayer += step;
		}
	}

	nnProbBanker = _baseProbBanker * nnProbBanker;
	nnProbPlayer = _baseProbPlayer * nnProbPlayer;

	if (nnProbBanker > nnProbPlayer)
		return BANKER;

	else return PLAYER;
}

void KNNStrategy::resetGameInfo()
{

}

short KNNStrategy::getType()
{
	return STRATEGY_KNN_TYPE;
}

ostringstream KNNStrategy::dumInfo()
{
	ostringstream ostr;
	ostr << "  KNN Strategy" << endl;
	ostr << "  Next Bet Selection: " << 
		SELECTED_BET_TO_STR(this->makeDecision()) << endl;

	return ostr;
}

void KNNStrategy::updateGamewinner()
{

}

void KNNStrategy::updateGameEnd()
{

}