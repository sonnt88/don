#ifndef __GAME_CONTROLLER_H__
#define __GAME_CONTROLLER_H__

#include <sstream>
#include "vector"

using namespace std;

class StrategyBase;
class MoneyManagement;
class GameInfo;
class GameStatistic;

enum UpdateMessageType {
	MSG_UPDATE_UNKNOWN = -1,
	MSG_UPDATE_WINNER,
	MSG_UPDATE_GAME_END
};

/*
** Observer and control game
*/
class GameController {

public:
	GameController();
	~GameController();

	GameInfo *getGameInfo();
	GameStatistic *getGameStatistic();
	StrategyBase *getStragtegy();
	MoneyManagement *getMoneyManagement();
	bool hasBusted();
	bool checkValidToBet();
	bool doBet();
	void setSelectedWinner(int);
	int selectWinner();
	void setLastWinner(int);
	void setStrategy(StrategyBase *strategy);
	void setMoneyManagement(MoneyManagement *moneym);

	// update functions
	void updateNewWinner(int winner);
	void onEndGame();
	void resetGameInfo();
	
	bool checkWin(int outcome);
	float monneyForBet();
	short numWinningInLastNRounds(int nrounds);
	short numPlayedRound();
	float wonMoney();
	float totalWager();
	ostringstream dumpGameInfo(bool showConsole = true);

private:
	GameInfo *_gameInfo;  // Store information of playing game
	GameStatistic *_gameStatistic;  // statistic for game
	StrategyBase *_strategy;  // Strategy betting
	MoneyManagement *_moneyManagement; // money management
};

#endif