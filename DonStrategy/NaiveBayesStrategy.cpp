#include "NaiveBayesStrategy.h"
#include "CommonDefine.h"
#include "DonKnowledge.h"
#include "GameStatistic.h"

NaiveBayesStrategy::NaiveBayesStrategy():
					StrategyBase(),
					_baseProbBanker(0.56f),
					_baseProbPlayer(0.43f)
{

}

NaiveBayesStrategy::NaiveBayesStrategy(GameController *gameCtrl):
					StrategyBase(gameCtrl),
					_baseProbBanker(0.56f),
					_baseProbPlayer(0.43f)
{
}

NaiveBayesStrategy::~NaiveBayesStrategy()
{

}

int NaiveBayesStrategy::makeDecision()
{
	short lastWinners[7];
	float prob_b_3series = 0.0f;
	float prob_p_3series = 0.0f;
	float prob_b_4series = 0.0f;
	float prob_p_4series = 0.0f;
	float prob_b_5series = 0.0f;
	float prob_p_5series = 0.0f;
	float prob_banker = 0.0f;
	float prob_player = 0.0f;
	
	DonKnowledge *knowledge = DonKnowledge::getInstance();
	GameInfo *gameinf = _gameController->getGameInfo();
	GameStatistic *gamestas = _gameController->getGameStatistic();
	short n = gamestas->getLastNRoundWinner(3, lastWinners);
	
	if (n > 0) {
		prob_b_3series = knowledge->getProbOfSeries4(
						 TRINARY_TO_DEC_4(lastWinners[0], lastWinners[1],
										  lastWinners[2], 1));
		prob_p_3series = knowledge->getProbOfSeries4(
						TRINARY_TO_DEC_4(lastWinners[0], lastWinners[1],
													lastWinners[2], 0));
	}
	
	n = gamestas->getLastNRoundWinner(4, lastWinners);
	if (n > 0) {
		prob_b_4series = knowledge->getProbOfSeries5(
						TRINARY_TO_DEC_5(lastWinners[0], lastWinners[1],
										 lastWinners[2], lastWinners[3], 1));
		prob_p_4series = knowledge->getProbOfSeries5(
						TRINARY_TO_DEC_5(lastWinners[0], lastWinners[1],
										 lastWinners[2], lastWinners[3], 0));
	}
									 
	n = gamestas->getLastNRoundWinner(5, lastWinners);
	if (n > 0) {
		prob_b_5series = knowledge->getProbOfSeries6(
						TRINARY_TO_DEC_6(lastWinners[0], lastWinners[1],
										 lastWinners[2], lastWinners[3],
										 lastWinners[4], 1));
		prob_p_5series = knowledge->getProbOfSeries6(
						TRINARY_TO_DEC_6(lastWinners[0], lastWinners[1],
										 lastWinners[2], lastWinners[3],
										 lastWinners[4], 0));
	}
	float scale = 1e4f;
	prob_banker = prob_b_3series * prob_b_4series * prob_b_5series * scale * _baseProbBanker;
	prob_player = prob_p_3series * prob_p_4series * prob_p_5series * scale * _baseProbPlayer;
	
	if (prob_banker > prob_player)
		return BANKER;
	else if (prob_banker < prob_player) 
		return PLAYER;
	else return PB_UNDEFINED;
}

void NaiveBayesStrategy::resetGameInfo()
{

}

short NaiveBayesStrategy::getType()
{
	return StrategyBase::STRATEGY_NAIVE_BAYES_TYPE;
}

ostringstream NaiveBayesStrategy::dumInfo()
{
	ostringstream ostr;
	ostr << "  NaiveBayes Strategy" << endl;
	ostr << "  Next Bet Selection: " << 
		SELECTED_BET_TO_STR(this->makeDecision()) << endl;

	return ostr;
}

void NaiveBayesStrategy::updateGamewinner()
{

}

void NaiveBayesStrategy::updateGameEnd()
{

}