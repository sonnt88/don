#ifndef __COMMON_DEFINE_H__
#define __COMMON_DEFINE_H__
#include <assert.h>

enum  {
	PB_UNDEFINED = -1,
	PLAYER,
	BANKER,
	TIE
};

enum {
	LOSE = 0,
	WIN,
	DRAW
};

#define SELECTED_BET_TO_STR(pb) ((pb) == PLAYER ? "Player" : \
						(pb) == BANKER ? "Banker" : \
						(pb) == TIE ? "TIE" : \
						"UNKNOWN")

#define SELECTED_BET_TO_LETTER(pb) ((pb) == PLAYER ? "P" : \
							(pb) == BANKER ? "B" : \
							(pb) == TIE ? "T" : \
							"U")

#define WIN_LOSE_TO_LETTER(wl) ((wl) == LOSE ? "L" :	\
						  (wl) == WIN ? "W" :	\
						  "D")

#define WIN_LOSE_TO_STR(wl) ((wl) == LOSE ? "LOSE" :	\
						(wl) == WIN ? "WIN" :	\
						"DRAW")

#define TRINARY_TO_DEC_3(x0, x1, x2) (x0 + x1 * 3 + x2 * 9)
#define TRINARY_TO_DEC_4(x0, x1, x2, x3) (x0 + x1 * 3 + x2 * 9 + x3 * 27)
#define TRINARY_TO_DEC_5(x0, x1, x2, x3, x4) (x0 + x1 * 3 + x2 * 9 \
												 + x3 * 27 + x4 * 81)
#define TRINARY_TO_DEC_6(x0, x1, x2, x3, x4, x5) (x0 + x1 * 3 + x2 * 9 \
										+ x3 * 27 + x4 * 81 + x5 * 243)
#define POW_3_3 27
#define POW_3_4 81
#define POW_3_5 243
#define POW_3_6 729

#define MAX_LOST_CONTINUOUS 30
#endif