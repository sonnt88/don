#ifndef __DON_KNOWLEDGE_H__
#define __DON_KNOWLEDGE_H__

#include "CommonDefine.h"

class DonTraining;

class DonKnowledge {
public:
	static DonKnowledge *getInstance();
	~DonKnowledge();

	float *getProbOfSeries3();
	float *getProbOfSeries4();
	float *getProbOfSeries5();
	float *getProbOfSeries6();

	float getProbOfSeries3(int ind);
	float getProbOfSeries4(int ind);
	float getProbOfSeries5(int ind);
	float getProbOfSeries6(int ind);
	float getFinalWon();
	float getFinalWonPlayerOnly();

	void updateProbOfSeries3(long ind, float prob);
	void updateProbOfSeries4(long ind, float prob);
	void updateProbOfSeries5(long ind, float prob);
	void updateProbOfSeries6(long ind, float prob);
	void updateNLostContinuous(int ind);
	void updateNWon();
	void updateNLost();
	void updateNWonBanker();
	void updateNWonPlayer();
	void updateNLostBanker();
	void updateNLostPlayer();
	void updateNBanker();
	void updateNPlayer();
	void analyzeWon();

	void trainingFromRecords();
	void resetAll();

private:
	DonKnowledge();

private:
	static DonKnowledge *_knowledgeInstance;
	DonTraining *_training;

	float _probOfSeries3[27];  // probability of 3 rounds
	float _probOfSeries4[81];  // probability of 4 rounds
	float _probOfSeries5[243]; // probability of 5 rounds
	float _probOfSeries6[729]; // probability of 6 rounds

	int _lostNcontinuous[MAX_LOST_CONTINUOUS];
	int _nWon;
	int _nLost;
	int _nPlayer;
	int _nBanker;
	int _nWonPlayer;
	int _nWonBanker;
	int _nLostPlayer;
	int _nLostBanker;

	float _finalWon;
	float _finalWonPlayerOnly;
};

#endif