#include "GameStatistic.h"
#include "GameInfo.h"
#include "MoneyManagement.h"
#include "CommonDefine.h"

GameStatistic::GameStatistic(GameInfo *gameinf):
				_gameInfo(gameinf) {
	resetGameInfo();
	_gameInfo->registerObserver(this);
}

GameStatistic::~GameStatistic() {

}

int GameStatistic::getMaxNumberOfWonCont() {
	return _maxNumberOfWonCont;
}

int GameStatistic::getMaxNumberOfLoseCont() {
	return _maxNumberOfWonCont;
}

float GameStatistic::getMaxBetMoney()
{
	return _maxBettedMoney;
}

float GameStatistic::getMaxMoneyNeededInBankroll()
{
	return _maxMoneyNeededInBankRoll;
}

float GameStatistic::getMaxMoneyWon()
{
	return _maxMonneyWon;
}

float GameStatistic::getMaxMoneyLost()
{
	return _maxMoneyLost;
}

int GameStatistic::getRoundBusted()
{
	return _roundBusted;
}

int GameStatistic::getRoundReachExpectedProfit()
{
	return _roundReachExpectedProfit;
}

void GameStatistic::setMaxNumberOfWonCont(int) {

}

void GameStatistic::setMaxNumberOfLoseCont(int) {

}

void GameStatistic::setMoneyManagement(MoneyManagement *mm)
{
	_moneymgmt = mm;
}

void GameStatistic::updateGameInfo(UpdateMessageType msg)
{
	switch (msg)
	{
	case MSG_UPDATE_WINNER:
		this->updateGameWinner();
		break;
	case MSG_UPDATE_GAME_END:
		this->updateGameEnd();
		break;
	}
}

void GameStatistic::resetGameInfo() 
{
	_maxNumberOfLoseCont = 0;
	_maxNumberOfWonCont = 0;
	_maxBettedMoney = 0;
	_maxMoneyNeededInBankRoll = 0;
	_maxMonneyWon = 0.0;
	_maxMoneyLost = 0.0;
	_roundBusted = -1;
	_roundReachExpectedProfit = -1;

	_winloseList.clear();
	_outcomeRounds.clear();
}

void GameStatistic::onBettedMoney(float money) 
{
	if (money > _maxBettedMoney) {
		_maxBettedMoney = money;
	}

	float won;
	float neededmoney;
	if (_moneymgmt) {
		won = _moneymgmt->getGameWon();
		neededmoney = won > 0 ? money : money - won;
		_maxMoneyNeededInBankRoll = neededmoney > _maxMoneyNeededInBankRoll ? neededmoney :
									_maxMoneyNeededInBankRoll;
	}
}

const std::vector<short> &GameStatistic::getWinloseList() {
	return _winloseList;
}

const std::vector<int> &GameStatistic::getOutcomeRounds() {
	return _outcomeRounds;
}

short GameStatistic::numWonInLastNRounds(int nrounds)
{
	short res = 0;
	int playedround = _winloseList.size();

	if (playedround < nrounds) nrounds = playedround;

	for (int i = 0; i < nrounds; ++i) {
		if (WIN == _winloseList[playedround - i - 1]) {
			res++;
		}
	}

	return res;
}

short GameStatistic::numLostInLastNRounds(int nrounds)
{
	short res = 0;
	int playedround = _winloseList.size();

	if (playedround < nrounds) nrounds = playedround;

	for (int i = 0; i < nrounds; ++i) {
		if (LOSE == _winloseList[playedround - i - 1]) {
			res++;
		}
	}

	return res;
}

short GameStatistic::calcNumWonRounds() {
	short numWon = 0;
	unsigned nround = _winloseList.size();
	for (unsigned i = 0; i < nround; ++i) {
		if (WIN == _winloseList[i]) {
			numWon++;
		}
	}

	return numWon;
}

short GameStatistic::calcNumLostRounds() {
	short numLost = 0;
	unsigned nround = _winloseList.size();
	for (unsigned i = 0; i < nround; ++i) {
		if (LOSE == _winloseList[i]) {
			numLost++;
		}
	}

	return numLost;
}

short GameStatistic::lastWinner(bool skipTie)
{
	if (_outcomeRounds.empty())
		return 0;

	if (skipTie) {
		for (int i = _outcomeRounds.size() - 1; i > -1; i--)
		{
			if (_outcomeRounds[i] != TIE)
				return _outcomeRounds[i];
		}
	}

	return _outcomeRounds.back();
}

short GameStatistic::numberOfLastContinuousResult(short result)
{
	int res = 0;

	if (_winloseList.empty())
		return 0;
	
	for (int i = _winloseList.size() - 1; i > -1; i--)
	{
		if (_winloseList[i] == DRAW) {
			continue;
		}

		if (_winloseList[i] == result) {
			res++;
		} else if (_winloseList[i] != result) {
			break;
		}
	}

	return res;
}

short GameStatistic::getLastNRoundWinner(short n, short *winners)
{
	if (n < 0) return 0;

	unsigned sz = _outcomeRounds.size();

	if (sz < (unsigned)n || n < 0) {
		return 0;
	}
	
	for (int i = n-1; i >= 0; --i) {
		winners[n - i - 1] = _outcomeRounds[sz - 1 - i];
	}
	
	return n;
}

void GameStatistic::updateGameWinner() {
	int isWin = _gameInfo->getIsCurrentWin();
	int winner = _gameInfo->getLastWinner();
	_outcomeRounds.push_back(winner);
	
	if (isWin == DRAW || false == _gameInfo->hasBetted()) {
		_winloseList.push_back(DRAW);
		return;
	}

	_winloseList.push_back(isWin);
	int numContResult = numberOfLastContinuousResult(isWin);//_gameInfo->getNumberOfContinuousResult();

	// win previous round
	if (isWin == WIN && numContResult > _maxNumberOfWonCont) {
		_maxNumberOfWonCont = numContResult;
	} else if (isWin == LOSE && numContResult > _maxNumberOfLoseCont) {
		_maxNumberOfLoseCont = numContResult;
	}

	float moneyWon = _moneymgmt->getGameWon();
	if (moneyWon > 0) {
		if (moneyWon > _maxMonneyWon)
			_maxMonneyWon = moneyWon;
	} else {
		if (moneyWon < _maxMoneyLost)
			_maxMoneyLost = moneyWon;
	}

	if (_roundBusted == -1 && _moneymgmt->isGameBusted()) {
		_roundBusted = _gameInfo->getPlayedRoundNumber();
	}

	if (_roundReachExpectedProfit == -1 && 
		_moneymgmt->isReachedExpectedProfit()) {
			_roundReachExpectedProfit = _gameInfo->getPlayedRoundNumber();
	}
}

void GameStatistic::updateGameEnd() 
{
	/* coming up */
}

ostringstream GameStatistic::dumpInfo() 
{
	ostringstream ostr;
	ostr << "  Max Lose continuous " << _maxNumberOfLoseCont << endl;
	ostr << "  Max Won continuous "  << _maxNumberOfWonCont  << endl;
	ostr << "  Max Betted money "    << _maxBettedMoney      << endl;
	ostr << "  Max Needed Money in Bankroll: " << _maxMoneyNeededInBankRoll << endl;
	ostr << "  Max Money Has Won: " << _maxMonneyWon << endl;
	ostr << "  Max Money Has Lost: " << _maxMoneyLost << endl;
	ostr << "  Round Busted: "    << _roundBusted << endl;
	ostr << "  Round reached expected profit: " << _roundReachExpectedProfit << endl;
	ostr << "  Number of lost rounds: " << calcNumLostRounds() << endl;
	ostr << "  Number of won rounds: " << calcNumWonRounds() << endl;
	for (unsigned i = 0; i < _outcomeRounds.size(); ++i) {
		ostr << SELECTED_BET_TO_LETTER(_outcomeRounds[i]) << " ";
	}
	ostr << endl;
	for (unsigned i = 0; i < _winloseList.size(); ++i) {
		ostr << WIN_LOSE_TO_LETTER(_winloseList[i]) << " ";
	}
	ostr<<endl;
	return ostr;
}