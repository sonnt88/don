#ifndef __DON_TRAINING_H__
#define __DON_TRAINING_H__

#include "RecordIO.h"

class DonKnowledge;

class DonTraining {
public:
	DonTraining(DonKnowledge *knowledge);
	~DonTraining();

	void doTrainingFromRecords();
	void trainFromAGame(RecordIO::GameRCDInfo &gameInfo);

	void updateProbOfSeries3();
	void updateProbOfSeries4();
	void updateProbOfSeries5();
	void updateProbOfSeries6();

private:
	DonKnowledge *_knowledge;

	long _counterSeries3[27];  // probability of 3 rounds
	long _counterSeries4[81];  // probability of 4 rounds
	long _counterSeries5[243]; // probability of 5 rounds
	long _counterSeries6[729]; // probability of 6 rounds
};

#endif