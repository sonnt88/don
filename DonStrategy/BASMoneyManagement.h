#ifndef __BAS_MONEY_MANAGEMENT_H__
#define __BAS_MONEY_MANAGEMENT_H__

#include "MoneyManagement.h"
#include "vector"

class BASMoneyManagement: public MoneyManagement {
	static const float AttachMoneySeries[];
	static const float RetrenchMoneySeries[];
public:
	enum BettingMode {
		BETTING_MODE_TRIGGER = 0,
		BETTING_MODE_ATTACK,
		BETTING_MODE_RETRENCH
	};

public:
		BASMoneyManagement();
		BASMoneyManagement(GameController *gameCtrl);
		/* virtual */~BASMoneyManagement();

		/*virtual*/ float moneyForNextBet();
		
		/* Update whenever betting win or lose*/
		/*virtual*/ void updateGameWinner();
		/*virtual*/ void resetGameInfo();
		/*virtual*/ short getType();
		/*virtual*/ ostringstream dumpInfo();

		BettingMode getCurrentMode();
		char *currentModeName();
		int getMoneyLevel();

private:

private:
	BettingMode _currentMode;
	int _moneyLevel;
};
#endif