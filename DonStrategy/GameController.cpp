#include "iostream"
#include "GameController.h"
#include "BASMoneyManagement.h"
#include "BAStrategy.h"
#include "GameInfo.h"
#include "GameStatistic.h"
#include "RecordIO.h"

GameController::GameController()
{
	_moneyManagement = nullptr;
	_strategy = nullptr;
	_gameInfo = new GameInfo;
	_gameStatistic = new GameStatistic(_gameInfo);

	resetGameInfo();
}

GameController::~GameController() 
{
	delete _moneyManagement;
	delete _strategy;
	delete _gameInfo;
	delete _gameStatistic;
}

GameInfo *GameController::getGameInfo()
{
	return _gameInfo;
}

GameStatistic *GameController::getGameStatistic() 
{
	return _gameStatistic;
}

bool GameController::doBet()
{
	// TODO: check if available for bet
	bool isValidTobet = checkValidToBet();
	_gameInfo->setHasBetted(isValidTobet);
	if (false == isValidTobet) {
		return false;
	}
	this->selectWinner();

	float money;
	assert(_moneyManagement);
	money = _moneyManagement->moneyForNextBet();
	assert(_gameStatistic);
	_gameStatistic->onBettedMoney(money);

	return true;
}


bool GameController::checkValidToBet()
{
	/*
	if (hasBusted()) {
		return false;
	}
	*/

	int decis = _strategy->makeDecision();
	if (decis == PB_UNDEFINED) {
		return false;
	}

	return true;
}

void GameController::setSelectedWinner(int sel)
{
	_gameInfo->setSelectedWinner(sel);
}

int GameController::selectWinner() {
	int nxtWinner = _strategy->makeDecision();
	this->setSelectedWinner(nxtWinner);

	return nxtWinner;
}

void GameController::setLastWinner(int winner) 
{
	_gameInfo->setLastWinner(winner);
}

bool GameController::checkWin(int winner) 
{
	return _gameInfo->getSelectedWinner() == winner;
}

StrategyBase *GameController::getStragtegy() 
{
	return _strategy;
}

MoneyManagement *GameController::getMoneyManagement() 
{
	return _moneyManagement;
}

bool GameController::hasBusted()
{
	// check won money
	return _moneyManagement->isGameBusted();
}

void GameController::setStrategy(StrategyBase *strategy) 
{
	if (nullptr != _strategy) {
		delete _strategy;
		_strategy = nullptr;
	}

	_strategy = strategy;

	if (strategy->getGameController() != this) {
		strategy->setGameController(this);
	}
}

void GameController::setMoneyManagement(MoneyManagement *moneym) 
{
	if (nullptr != _moneyManagement) {
		delete _moneyManagement;
		_moneyManagement = nullptr;
	}

	_gameStatistic->setMoneyManagement(moneym);
	_moneyManagement = moneym;

	if (moneym->getGameController() != this) {
		moneym->setGameController(this);
	}
}

void GameController::updateNewWinner(int winner)
{
	// gameinfo will send msg GAME_UPDATE_WINNER to observers
	_gameInfo->updateNewWinner(winner);
}

float GameController::monneyForBet()
{
	return _moneyManagement->moneyForNextBet();
}

short GameController::numWinningInLastNRounds(int nrounds)
{
	return _gameStatistic->numWonInLastNRounds(nrounds);
}

short GameController::numPlayedRound()
{
	if (_gameInfo) {
		return _gameInfo->getPlayedRoundNumber();
	}

	return 0;
}

float GameController::wonMoney()
{
	if (_moneyManagement) {
		return _moneyManagement->getTotalWon();
	}

	return 0.f;
}

float GameController::totalWager()
{
	if (_moneyManagement) {
		return _moneyManagement->getTotalWager();
	}

	return 0.f;
}

void GameController::resetGameInfo()
{
	_gameStatistic->resetGameInfo();
	_gameInfo->resetGameInfo();

	if (_moneyManagement)
		_moneyManagement->resetGameInfo();

	if (_strategy)
		_strategy->resetGameInfo();
}

ostringstream GameController::dumpGameInfo(bool showConsole)
{
	ostringstream ostr;

	MoneyManagement *mm =_moneyManagement;
	ostr << endl;
	ostr << "====================== Game Info =================" << endl;
	ostr << _gameInfo->dumpInfo().str();
	ostr << _strategy->dumInfo().str();
	ostr << _gameStatistic->dumpInfo().str();
	ostr << mm->dumpInfo().str();
	ostr << "=========== End of Info ==========================" << endl;
	ostr << endl;

	if (showConsole)
		cout << ostr.str();

	return ostr;
}