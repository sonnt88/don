#ifndef __SELECTION_STATE_H__
#define __SELECTION_STATE_H__

#include "StrategyBase.h"
#include "CommonDefine.h"
#include "assert.h"

class SelectionState {

public:
	enum {
		SELECTION_STATE_A1 = 0,
		SELECTION_STATE_A2,
		SELECTION_STATE_B1,
		SELECTION_STATE_B2,
		SELECTION_STATE_B3,
		TOTAL_STATE_SIZE
	};

public:
	SelectionState() {
		_preState = nullptr;
	};
	~SelectionState() {};

	void setPreState(SelectionState *state) {
		_preState = state;
	}

	SelectionState *getPreState() {
		return _preState;
	}

	virtual int makeDeciscion(int lastWin) = 0;
	virtual int nextStateType(bool win) = 0;
	virtual int getType() = 0;
	int selected() { return _selected; };
	char *nameOfType(int type) {
		switch (type) {
		case SELECTION_STATE_A1:
			return "A1";
			break;
		case SELECTION_STATE_A2:
			return "A2";
			break;
		case SELECTION_STATE_B1:
			return "B1";
			break;
		case SELECTION_STATE_B2:
			return "B2";
			break;
		case SELECTION_STATE_B3:
			return "B3";
			break;
		default:
			break;
		}

		return "UNKNOWN";
	}

protected:
	SelectionState *_preState;
	int _selected;
};

class SelectionState_A1: public SelectionState {
public:
	SelectionState_A1() {};
	~SelectionState_A1() {};

	virtual int makeDeciscion(int lastWin) {
		int preType = _preState == nullptr ? -1 : _preState->getType();
		if (SELECTION_STATE_A2 == preType) {
			_selected = lastWin;
		} else {
			if (lastWin == PLAYER) {
				_selected = BANKER;
			} else if (lastWin == BANKER){
				_selected = PLAYER;
			} else {
				_selected = BANKER;
			}
		}
		return _selected;
	}

	virtual int nextStateType(bool win) {
		if (win) {
			return SELECTION_STATE_A1;
		} else {
			return SELECTION_STATE_A2;
		}
	}

	virtual int getType() {
		return SELECTION_STATE_A1;
	}
private:
};

class SelectionState_A2: public SelectionState {
public:
	SelectionState_A2() {};
	~SelectionState_A2() {};

	virtual int makeDeciscion(int lastWin) {
		assert(_preState); // ensure that prestate is valid
		int preSelected = _preState->selected();
		_selected = preSelected;

		return _selected;
	}

	virtual int nextStateType(bool win) {
		if (win) {
			return SELECTION_STATE_A1;
		} else {
			return SELECTION_STATE_B1;
		}
	}

	virtual int getType() {
		return SELECTION_STATE_A2;
	}

private:
};

class SelectionState_B1: public SelectionState {
public:
	SelectionState_B1() {};
	~SelectionState_B1() {};

	virtual int makeDeciscion(int lastWin) {
		int preSelected = _preState->selected();
		if (preSelected == PLAYER) {
			_selected = BANKER;
		} else {
			_selected = PLAYER;
		}

		return _selected;
	}

	virtual int nextStateType(bool win) {
		if (win) {
			return SELECTION_STATE_A1;
		} else {
			return SELECTION_STATE_B2;
		}
	}

	virtual int getType() {
		return SELECTION_STATE_B1;
	}
private:
};

class SelectionState_B2: public SelectionState {
public:
	SelectionState_B2() {};
	~SelectionState_B2() {};

	virtual int makeDeciscion(int lastWin) {
		_selected = _preState->selected();
		return _selected;
	}

	virtual int nextStateType(bool win) {
		if (win) {
			if (SELECTION_STATE_B3 == _preState->getType())
				return SELECTION_STATE_A1;
			else return SELECTION_STATE_B1;
		} else {
			return SELECTION_STATE_B3;
		}
	}

	virtual int getType() {
		return SELECTION_STATE_B2;
	}
private:
};

class SelectionState_B3: public SelectionState {
public:
	SelectionState_B3() {};
	~SelectionState_B3() {};

	virtual int makeDeciscion(int lastWin) {
		_selected = _preState->selected();
		return _selected;
	}

	virtual int nextStateType(bool win) {
		if (win) {
			return SELECTION_STATE_B2;
		} else {
			return SELECTION_STATE_A1;
		}
	}

	virtual int getType() {
		return SELECTION_STATE_B3;
	}
private:
};

#endif