#include "MoneyManagement.h"
#include "GameController.h"
#include "GameInfo.h"
#include "CommonDefine.h"
#include "BASMoneyManagement.h"

MoneyManagement::MoneyManagement():
					_baseBet(100),
					_expectedProfit(10),
					_gameWager(0),
					_gameWon(0),
					_gameBankroll(75),
					_totalWager(0),
					_totalWon(0),
					_gameController(nullptr)
{

}

MoneyManagement::MoneyManagement(GameController *gameCtrl):
							_baseBet(100),
							_expectedProfit(10),
							_gameWager(0),
							_gameWon(0),
							_gameBankroll(75),
							_totalWager(0),
							_totalWon(0),
							_gameController(gameCtrl) {
	_gameController->getGameInfo()->registerObserver(this);
}
							

MoneyManagement::~MoneyManagement()
{

}

MoneyManagement *MoneyManagement::makeMoneyManagement(short type)
{
	switch (type) {
	case MONEY_MNGMNT_BACC_ATTACK_TYPE:
		return new BASMoneyManagement();

	default:
		return NULL;
	}
}

void MoneyManagement::updateGameWinner()
{
	int isWin = _gameController->getGameInfo()->getIsCurrentWin();
	if (isWin == DRAW)
		return;

	float bettedMoney = moneyForNextBet()/* * _baseBet*/; // must call first to get wager money
	_gameWager += bettedMoney;
	_totalWager += bettedMoney;

	if (LOSE == isWin) {
		bettedMoney = -bettedMoney;
	} else if (_gameController->getGameInfo()->getLastWinner() == BANKER){
		bettedMoney *= 0.95f;
	}

	_gameWon += bettedMoney;
	_totalWon += bettedMoney;
}

void MoneyManagement::updateGameEnd()
{
	resetGameInfo();
}

void MoneyManagement::updateGameInfo(UpdateMessageType msg)
{
	switch(msg) {
	case UpdateMessageType::MSG_UPDATE_WINNER:
		{
			if (_gameController->getGameInfo()->hasBetted())
				updateGameWinner();
		}
		break;

	case UpdateMessageType::MSG_UPDATE_GAME_END:
		updateGameEnd();
		break;

	default:
		break;
	}
}

void MoneyManagement::setBaseBet(int basemoney)
{
	_baseBet = basemoney;
}

void MoneyManagement::setGameController(GameController *gmctr)
{
	_gameController = gmctr;

	gmctr->getGameInfo()->registerObserver(this);
}

GameController *MoneyManagement::getGameController()
{
	return _gameController;
}

int MoneyManagement::getBaseBet()
{
	return _baseBet;
}

float MoneyManagement::getExpectedProfit()
{
	return _expectedProfit;
}

float MoneyManagement::getGameWager() {
	return _gameWager;
}

float MoneyManagement::getGameWon() {
	return _gameWon;
}

float MoneyManagement::getGameBankroll() {
	return _gameBankroll;
}

float MoneyManagement::getTotalWager() {
	return _totalWager;
}

float MoneyManagement::getTotalWon() {
	return _totalWon;
}

bool MoneyManagement::isGameBusted()
{
	if (_gameBankroll + _gameWon - moneyForNextBet() < 0.0f)
		return true;

	return false;
}

bool MoneyManagement::isReachedExpectedProfit()
{
	return _gameWon >= _expectedProfit;
}

void MoneyManagement::resetGameInfo() {
	_gameWon = 0;
	_gameWager = 0;
}

short MoneyManagement::getType()
{
	return MONEY_MNGMNT_BASE_TYPE;
}

ostringstream MoneyManagement::dumpInfo() {
	return ostringstream();
}