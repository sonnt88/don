// GameListener: Observer type

#ifndef __GAME_LISTENER_H__
#define __GAME_LISTENER_H__

#include "GameController.h"

class GameListener {
public:
	virtual void updateGameInfo(UpdateMessageType msg) = 0;
private:
};

#endif