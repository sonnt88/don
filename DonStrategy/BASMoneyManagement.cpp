#include "BASMoneyManagement.h"
#include "iostream"
#include "GameController.h"
#include "GameInfo.h"
#include "GameStatistic.h"
#include "CommonDefine.h"

#define MAX_MONEY_SERIES 11

const float BASMoneyManagement::AttachMoneySeries[MAX_MONEY_SERIES] = 
											{1, 1.5f, 2, 2.5f, 3, 4, 5, 6, 7, 8, 9};

const float BASMoneyManagement::RetrenchMoneySeries[MAX_MONEY_SERIES] = 
									/*{3, 5, 7, 10, 13, 18, 25, 30, 43, 70, 100};*/
									{3, 5, 7, 10, 13, 18, 13, 10, 7, 5, 3};

BASMoneyManagement::BASMoneyManagement():
					MoneyManagement(),
					_currentMode(BETTING_MODE_TRIGGER),
					_moneyLevel(-1) {
}

BASMoneyManagement::BASMoneyManagement(GameController *gameCtrl):
					MoneyManagement(gameCtrl),
					_currentMode(BETTING_MODE_TRIGGER),
					_moneyLevel(-1) {

}

BASMoneyManagement::~BASMoneyManagement() {

}

float BASMoneyManagement::moneyForNextBet()
{
	const float *moneySeries = 0;
	if (_currentMode == BETTING_MODE_TRIGGER) {
		return 2; // double of money unit
	} else if (_currentMode == BETTING_MODE_ATTACK) {
		moneySeries = BASMoneyManagement::AttachMoneySeries;
	} else if (_currentMode == BETTING_MODE_RETRENCH) {
		moneySeries = BASMoneyManagement::RetrenchMoneySeries;
	}

	if (_moneyLevel < MAX_MONEY_SERIES) {
		return moneySeries[_moneyLevel];
	} else {
		return moneySeries[MAX_MONEY_SERIES - 1];
	}

	return moneySeries[_moneyLevel];
}

void BASMoneyManagement::updateGameWinner()
{
	MoneyManagement::updateGameWinner();
	GameInfo *gameInfo = _gameController->getGameInfo();
	GameStatistic *gameStatistic = _gameController->getGameStatistic();
	assert(gameInfo && gameStatistic);

	int isWin = gameInfo->getIsCurrentWin();
	if (isWin == DRAW) {
		return;
	} else if (isWin == WIN) {
		if (_currentMode == BETTING_MODE_TRIGGER) {
			_currentMode = BETTING_MODE_ATTACK;
			_moneyLevel = 0;
		} else if (_currentMode == BETTING_MODE_ATTACK) {
			_moneyLevel++;
		} else if (_currentMode == BETTING_MODE_RETRENCH) {
			_moneyLevel--;
			if (gameInfo->getNumberOfContinuousResult() > 1 || 
				gameStatistic->numWonInLastNRounds(2) > 0) {
					if (_moneyLevel < 6)
						_moneyLevel--;
			}

			if (_moneyLevel < 0) {
				_currentMode = BETTING_MODE_TRIGGER;
			}
		}
	} else {
		if (_currentMode == BETTING_MODE_TRIGGER) {
			_currentMode = BETTING_MODE_RETRENCH;
			_moneyLevel = 0;
		} else if (_currentMode == BETTING_MODE_ATTACK){
			_moneyLevel = -1;
			_currentMode = BETTING_MODE_TRIGGER;
		} else if (_currentMode == BETTING_MODE_RETRENCH) {
			_moneyLevel++;
		}
	}
}

void BASMoneyManagement::resetGameInfo()
{
	MoneyManagement::resetGameInfo();
	_currentMode = BETTING_MODE_TRIGGER;
	_moneyLevel = 0;
}

short BASMoneyManagement::getType()
{
	return MONEY_MNGMNT_BACC_ATTACK_TYPE;
}

BASMoneyManagement::BettingMode BASMoneyManagement::getCurrentMode() {
	return _currentMode;
}

char *BASMoneyManagement::currentModeName()
{
	switch (_currentMode) {
	case BETTING_MODE_TRIGGER:
		return "Trigger";
		break;
	case BETTING_MODE_ATTACK:
		return "Attack";
		break;
	case BETTING_MODE_RETRENCH:
		return "Retrench";
		break;

	default:
		return "Unknown";
		break;
	}
}

int BASMoneyManagement::getMoneyLevel() {
	return _moneyLevel;
}

ostringstream BASMoneyManagement::dumpInfo()
{
	ostringstream ostr;

	ostr << "  Money: Attack mode: " << currentModeName() << endl;
	ostr << "  Money: level " << getMoneyLevel() << endl;
	ostr << "  Money: game wager:...." << getGameWager() << endl;
	ostr << "  Money: game won:......" << getGameWon() << endl;
	ostr << "  Money: game Bankroll:." << getGameBankroll() << endl;
	ostr << "  Money: Total Wager:..." << getTotalWager() << endl;
	ostr << "  Money: Total Won:....." << getTotalWon() << endl;
	ostr << "  Money: Next Bet money:" << moneyForNextBet() * getBaseBet() << endl;

	return ostr;
}