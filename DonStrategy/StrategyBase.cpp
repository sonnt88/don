#include "StrategyBase.h"
#include "GameController.h"
#include "GameInfo.h"
#include "CommonDefine.h"
#include "BAStrategy.h"
#include "KNNStrategy.h"
#include "NaiveBayesStrategy.h"

StrategyBase::StrategyBase():
					_gameController(nullptr) {

}

StrategyBase::StrategyBase(GameController *gameController):
					_gameController(gameController)
{
	// register to game infor
	_gameController->getGameInfo()->registerObserver(this);
}

StrategyBase::~StrategyBase() {

}

StrategyBase *StrategyBase::makeStrategy(short type)
{
	switch (type) {
	case STRATEGY_BACC_ATTACK_TYPE:
		return new BAStrategy();
		break;
	
	case STRATEGY_KNN_TYPE:
		return new KNNStrategy();
		break;

	case STRATEGY_NAIVE_BAYES_TYPE:
		return new NaiveBayesStrategy();
		break;

	default:
		return NULL;
	}
}

void StrategyBase::updateGamewinner()
{

}

void StrategyBase::updateGameEnd()
{

}

void StrategyBase::updateGameInfo(UpdateMessageType msg)
{
	switch(msg) {
	case MSG_UPDATE_WINNER:
		this->updateGamewinner();
		break;
	case MSG_UPDATE_GAME_END:
		this->updateGameEnd();
		break;

	default:
		break;
	}
}

void StrategyBase::resetGameInfo()
{

}

short StrategyBase::getType()
{
	return STRATEGY_BASE_TYPE;
}

GameController *StrategyBase::getGameController() {
	return _gameController;
}

void StrategyBase::setGameController(GameController *gctrl) {
	_gameController = gctrl;

	// register to listen gameinfo
	gctrl->getGameInfo()->registerObserver(this);
}