#include "GameInfo.h"
#include "CommonDefine.h"
#include "iostream"
#include "GameListener.h"

GameInfo::GameInfo() {
	resetGameInfo();
}

GameInfo::~GameInfo() {

}

int GameInfo::getIsCurrentWin()
{
	return _isCurrentWin;
}

int GameInfo::getLastWinner() {
	return _lastWinner;
}

int GameInfo::getSelectedWinner() {
	return _selectedWinner;
}

int GameInfo::getNumberOfContinuousResult() {
	return _numberOfContinuousResult;
}

int GameInfo::getPlayedRoundNumber() {
	return _playedRoundNumber;
}

bool GameInfo::hasBetted()
{
	return _hasbetted;
}

void GameInfo::setIsCurrentWin(int isWin) {
	_isCurrentWin = isWin;
}

void GameInfo::setLastWinner(int lstwinner) {
	_lastWinner = lstwinner;
}

void GameInfo::setSelectedWinner(int sel) {
	_selectedWinner = sel;
}

void GameInfo::setHasBetted(bool isBetted)
{
	_hasbetted = isBetted;
}

void GameInfo::setNumberOfContinuousResult(int conRes) {
	_numberOfContinuousResult = conRes;
}

void GameInfo::setCurrentPlayedRoundNumber(int playedRound) {
	_playedRoundNumber = playedRound;
}

void GameInfo::updateNewWinner(int winner)
{
	int preWin = _isCurrentWin;
	_lastWinner = winner;
	_isCurrentWin = 
					(_selectedWinner != TIE && winner == TIE ||
					 _selectedWinner == PB_UNDEFINED ||
					 _hasbetted == false) ? DRAW :
					 winner == _selectedWinner ? WIN :
					LOSE;
	
	if (_hasbetted == true && _isCurrentWin != DRAW) {
		if (_isCurrentWin == preWin) {
			_numberOfContinuousResult++;
		} else {
			_numberOfContinuousResult = 1;
		}
	}
	_playedRoundNumber++;

	// send event to observers
	// note: update for game statistic at last
	int nobser = _observers.size() - 1;
	for (int i = nobser; i >= 0; --i)
	{
		_observers[i]->updateGameInfo(MSG_UPDATE_WINNER);
	}
}

void GameInfo::resetGameInfo()
{
	_lastWinner = PB_UNDEFINED;
	_selectedWinner = PB_UNDEFINED;
	_isCurrentWin = DRAW;
	_numberOfContinuousResult = 0;
	_playedRoundNumber = 0;
	_hasbetted = false;
}

void GameInfo::registerObserver(GameListener *listener)
{
	_observers.push_back(listener);
}

ostringstream GameInfo::dumpInfo()
{
	ostringstream ostr;

	ostr << "  Win/Lose: " << WIN_LOSE_TO_STR(_isCurrentWin) << endl;
	ostr << "  Current Of continuous result: " << _numberOfContinuousResult << endl;
	ostr << "  Current played round: " << _playedRoundNumber << endl;
	//cout << ostr.str();

	return ostr;
}