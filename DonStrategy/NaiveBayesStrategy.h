#ifndef __NAIVE_BAYES_STRATEGY_H__
#define __NAIVE_BAYES_STRATEGY_H__

#include "StrategyBase.h"

class NaiveBayesStrategy: public StrategyBase {
public:

public:
	NaiveBayesStrategy();
	NaiveBayesStrategy(GameController *gameCtrl);
	~NaiveBayesStrategy();

	/* 
	** virtual functions
	*/
	/*virtual*/ int makeDecision();
	/*virtual*/ void resetGameInfo();
	/*virtual*/ short getType();
	/*virtual*/ ostringstream dumInfo();

	/*
	** normal functions
	*/

protected:
	/*virtual*/ void updateGamewinner();
	/*virtual*/ void updateGameEnd();

private:
	float _baseProbBanker;
	float _baseProbPlayer;
};
#endif