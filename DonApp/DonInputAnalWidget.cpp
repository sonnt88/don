#include "DonInputAnalWidget.h"
#include "qlineedit.h"
#include "qfiledialog.h"
#include "qsignalmapper.h"
#include "qgroupbox.h"
#include "qlayout.h"
#include "qslider.h"
#include "qlabel.h"
#include "qcombobox.h"
#include "qtextedit.h"

using namespace std;

DonInputAnalWidget::DonInputAnalWidget(DonAnalyzeWidget *parent):
						QWidget(parent)
{
	_analyzeWidget = parent;
	_filePath = QString("");
	this->sizeHint();
	this->setMinimumSize(300, 20);
	this->resize(300, 20);
	
	QLineEdit *filePathTxt = new QLineEdit(this);
	QPushButton *browseBtn = new QPushButton("Browse", this);
	filePathTxt->move(0, 20);
	filePathTxt->resize(200, 20);
	browseBtn->move(210, 20);
	int btnwidth = 75;
	int btnheight = 25;

	createSimulationBox();
	createInformationBox();

	QSignalMapper* signalMapper = new QSignalMapper (this) ;
	connect (browseBtn, SIGNAL(clicked()), signalMapper, SLOT(map())) ;
	signalMapper->setMapping (browseBtn, (QObject*)filePathTxt);

	connect(signalMapper, SIGNAL(mapped(QObject*)), this, SLOT(browse(QObject *)));
}

DonInputAnalWidget::~DonInputAnalWidget()
{
}

void DonInputAnalWidget::createSimulationBox()
{
	// Group box Simulation
	QGroupBox *simulationbox = new QGroupBox("Simulation", this);
	simulationbox->setMaximumSize(600, 300);
	simulationbox->move(0, 65);
	simulationbox->resize(260, 200);

	QLabel *strategyLabel = new QLabel("Strategy:", simulationbox);
	strategyLabel->resize(200, 20);
	strategyLabel->move(10, 20);

	_strategyCombox = new QComboBox(simulationbox);
	_strategyCombox->resize(120, 20);
	_strategyCombox->addItem("Baccarat Attack");
	_strategyCombox->addItem("KNN");
	_strategyCombox->setCurrentIndex(1);
	_strategyCombox->move(10, 40);

	QLabel *moneymgntLabel = new QLabel("Money managment:", simulationbox);
	moneymgntLabel->resize(200, 20);
	moneymgntLabel->move(10, 70);

	_moneymgntCombox = new QComboBox(simulationbox);
	_moneymgntCombox->addItem("BAS money mm");
	_moneymgntCombox->resize(120, 20);
	_moneymgntCombox->move(10, 90);

	_speedLabel = new QLabel("Speed:    ", simulationbox);
	_speedLabel->resize(200, 20);
	_speedLabel->move(10, 120);
	_slider = new QSlider(Qt::Orientation::Horizontal, simulationbox);
	_slider->setMaximum(100);
	_slider->setMinimum(1);
	_slider->resize(simulationbox->width() - 20, 20);
	(_slider)->move(10,140);
	_slider->setValue(60);
	_speedLabel->setText(QString("Speed: ") + QString::number(_slider->value()));

	QPushButton *startSimulationBtn = new QPushButton("Start", simulationbox);
	QPushButton *pauseSimulationBtn = new QPushButton("Pause", simulationbox);
	QPushButton *stopSimulationBtn = new QPushButton("Stop", simulationbox);
	(startSimulationBtn)->move(10, simulationbox->height() - 30);
	(pauseSimulationBtn)->move(90, simulationbox->height() - 30);
	(stopSimulationBtn)->move(170, simulationbox->height() - 30);

	connect(startSimulationBtn, SIGNAL(clicked()), SLOT(startSimulation()));
	connect(pauseSimulationBtn, SIGNAL(clicked()), SLOT(pauseSimulation()));
	connect(stopSimulationBtn, SIGNAL(clicked()), SLOT(stopSimulation()));
	connect(_slider, SIGNAL(valueChanged(int)), SLOT(speedValue(int)));
	connect(_strategyCombox, SIGNAL(currentIndexChanged(int)), this, SLOT(strategyChanged(int)));
	connect(_moneymgntCombox, SIGNAL(currentIndexChanged(int)), this, SLOT(moneymgntChanged(int)));
}

void DonInputAnalWidget::createInformationBox()
{
	// Group box Simulation
	QGroupBox *informationbox = new QGroupBox("Information", this);
	informationbox->setMaximumSize(600, 300);
	informationbox->move(0, 300);
	informationbox->resize(260, 200);

	QString text(
		"<b>Number of Games:</b> <br>"
		"Number of Rounds: <br>"
		"Round Won: <br>"
		"Round Lost:	<br>"
		"Total Wager: <br>"
		"Money Won: "
		);
	_informationlbl = new QTextEdit(text, informationbox);
	_informationlbl->setMaximumSize(200, 100);
	_informationlbl->move(10, 20);
	_informationlbl->setReadOnly(true);
	connect(_analyzeWidget, SIGNAL(updateInforSignal(const QString &)), this, SLOT(updateInformation(const QString &)));
}

void DonInputAnalWidget::updateInformation(const QString &text)
{
	//_informationlbl->clear();
	_informationlbl->setText(text);
	_informationlbl->update();
}

void DonInputAnalWidget::browse(QObject *lineEdit)
{
	QString directory = QFileDialog::getOpenFileName(this,
                            tr("Find Files"), QDir::currentPath());

    if (!directory.isEmpty()) {
		dynamic_cast<QLineEdit *>(lineEdit)->setText(directory);
		_filePath = directory;
    }
}

void DonInputAnalWidget::speedValue(int value)
{
	QString speed("Speed: ");
	speed.append(QString::number(value));
	_speedLabel->setText(speed);
}

void DonInputAnalWidget::strategyChanged(int index)
{
	int changed = 0;
}

void DonInputAnalWidget::moneymgntChanged(int index)
{
	int changed = 0;
}

void DonInputAnalWidget::startSimulation()
{
	_analyzeWidget->startSimulationThread();
}

void DonInputAnalWidget::pauseSimulation()
{

}

void DonInputAnalWidget::stopSimulation()
{

}

int DonInputAnalWidget::getSimSpeed()
{
	return _slider->value();
}

int DonInputAnalWidget::getStrategyType()
{
	return _strategyCombox->currentIndex();
}

int DonInputAnalWidget::getMoneyMgntType()
{
	return _moneymgntCombox->currentIndex();
}