#ifndef __GAME_PLAYING_V9_H__
#define __GAME_PLAYING_V9_H__

#include "GamePlaying.h"

class GamePlayingV9: public GamePlaying
{
public:
	GamePlayingV9();
	~GamePlayingV9();

	void handleGameWaitingBet(TableHandleBase::enTableState preTableState);
private:
};
#endif