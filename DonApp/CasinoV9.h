#ifndef __CASINO_V9_H__
#define __CASINO_V9_H__

#include "CasinoBase.h"

class CasinoV9: public CasinoBase {
public:
	CasinoV9();
	~CasinoV9();

	/*
	** override virtual functions 
	*/ 
	enCasinoState checkStateOfCasino();
	virtual enCasinoID getID();
	void playTable(TableHandleBase *table, 
				   GameController *gameController);
	void config();
	void monitorLobby(CasinoObserver *observer);
private:

};

#endif