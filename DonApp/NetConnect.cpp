#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include "NetConnect.h"


// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

void print_addr_inf(struct addrinfo *p) {
        char ipstr[INET6_ADDRSTRLEN];
        void *addr;
        char ipver[5];
        // get the pointer to the address itself,
        // different fields in IPv4 and IPv6:
        if (p->ai_family == AF_INET) { // IPv4
                struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
                addr = &(ipv4->sin_addr);
                strcpy(ipver, "IPv4");
        } else { // IPv6
                struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)p->ai_addr;
                addr = &(ipv6->sin6_addr);
                strcpy(ipver, "IPv6");
        }
        //convert the IP to a string and print it:
        inet_ntop(p->ai_family, addr, ipstr, sizeof ipstr);
		printf(" %s: %s\n", ipver, ipstr);
}

DWORD WINAPI ComunicateToServer(LPVOID lpParam) {
	ConnectThreadParam * params = (ConnectThreadParam *)lpParam;
	do {
		if (params->_isUpdated) {
			//int iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
			//Sleep(1);
			int iResult = send(params->_connectSocket, params->_msgData, params->_msgLen, NULL);
			if (iResult == SOCKET_ERROR) {
				printf("send failed with error: %d\n", WSAGetLastError());
			}

			if ( iResult > 0 )
				printf("Bytes Sent: %d\n", iResult);

			params->_isUpdated = false;
		}

		Sleep(100);
	} while( true );

	return 0;
}


int connetToHost(char *host, ConnectThreadParam *cnParam)
{
    WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    struct addrinfo *result = NULL,
                    *ptr = NULL,
                    hints;
    char *sendbuf = "this is a test";
    char recvbuf[DEFAULT_BUFLEN];
    int iResult;
    int recvbuflen = DEFAULT_BUFLEN;
    
    // Validate the parameters
    if (host == NULL) {
        printf("usage: %s server-name\n", "");
        return 1;
    }

    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed with error: %d\n", iResult);
        return 1;
    }

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
    iResult = getaddrinfo(host, DEFAULT_PORT, &hints, &result);
    if ( iResult != 0 ) {
        printf("getaddrinfo failed with error: %d\n", iResult);
        WSACleanup();
        return 1;
    }

    // Attempt to connect to an address until one succeeds
    for(ptr=result; ptr != NULL ;ptr=ptr->ai_next) {
		print_addr_inf(ptr);
        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, 
            ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) {
            printf("socket failed with error: %ld\n", WSAGetLastError());
            WSACleanup();
            return 1;
        }

        // Connect to server.
        iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) {
        printf("Unable to connect to server!\n");
        WSACleanup();
        return 1;
    }

	cnParam->_connectSocket = ConnectSocket;

    // Send an initial buffer
    //iResult = send( ConnectSocket, sendbuf, (int)strlen(sendbuf), 0 );
    //if (iResult == SOCKET_ERROR) {
    //    printf("send failed with error: %d\n", WSAGetLastError());
    //    closesocket(ConnectSocket);
    //    WSACleanup();
    //    return 1;
    //}

    //printf("Bytes Sent: %ld\n", iResult);

    //// shutdown the connection since no more data will be sent
    //iResult = shutdown(ConnectSocket, SD_SEND);
    //if (iResult == SOCKET_ERROR) {
    //    printf("shutdown failed with error: %d\n", WSAGetLastError());
    //    closesocket(ConnectSocket);
    //    WSACleanup();
    //    return 1;
    //}

	// Create new thread to comunicate with server
	// create thread 
	CreateThread(NULL, 0, ComunicateToServer, cnParam, NULL, NULL);

    // cleanup
    //closesocket(ConnectSocket);
    //WSACleanup();

    return 0;
}