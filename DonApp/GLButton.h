#ifndef __GL_BUTTON_H__
#define __GL_BUTTON_H__
#include "GLControlWidget.h"
class GLButton: public GLControlWidget
{
public:
	GLButton();
	GLButton(GLView *glview, GLControlWidget *parent);
	virtual ~GLButton();
	virtual void render();
	virtual void onFocused();
	virtual void onMouseMove();
	virtual void onMousePressed();
	virtual void onMouseReleased();
protected:
};
#endif