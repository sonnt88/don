#ifndef __UTILS_H__
#define __UTILS_H__

#include "WindowUtils.h"

namespace DonApp {
	namespace Utils {
		bool isCloseToWithColor(int red, int green, int blue, int tolerance = 90);
		bool isTwoEqualColor(int rgb1[3], int rgb2[3], int tolerance = 20);
		int getRGBAt(ScreenCoord scrcoord, int rgb[3]);
		bool isReachColor(int rgb[3]);
		void PrintRGB(int r, int g, int b);
		unsigned int __stdcall playTableThreadFunc(void *lparam);
	}
}
#endif