#include "GamePlayingV9.h"
#include "TableHandleV9.h"

GamePlayingV9::GamePlayingV9()
{
}

GamePlayingV9::~GamePlayingV9()
{
}

void GamePlayingV9::handleGameWaitingBet(TableHandleBase::enTableState preTableState)
{
	// check if this is the new game then store last game information
	bool isnwgm = dynamic_cast<TableHandleV9 *>(_tblHandle)->isEmptyAtRound0();
	if (isnwgm) {
		handleGameShuffling(preTableState);
	}

	GamePlaying::handleGameWaitingBet(preTableState);
}