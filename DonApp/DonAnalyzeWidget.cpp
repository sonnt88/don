#include "DonAnalyzeWidget.h"
#include "GLGraph.h"
#include "qdockwidget.h"
#include "qlayout.h"
#include "qlineedit.h"
#include "qpushbutton.h"
#include "IncludeLib.h"
#include "DonInputAnalWidget.h"
#include <iostream>
#include <process.h>
#include "Lock.h"

unsigned int __stdcall simulationThreadFunc(void *lparam) {
		if (NULL == lparam) {
			return 0;
		}
		
		DonAnalyzeWidget *analyzeWidget = static_cast<DonAnalyzeWidget *>(lparam);
		analyzeWidget->doSimulation();

		return 0;
}

DonAnalyzeWidget::DonAnalyzeWidget():_simthreadHdl(nullptr)
{
	createInputWidget();
	createGLGraph();
	QDockWidget *inputdockWidget = new QDockWidget(tr("Input"), this);
    inputdockWidget->setAllowedAreas(Qt::LeftDockWidgetArea |
                                Qt::RightDockWidgetArea|
								Qt::TopDockWidgetArea|
								Qt::BottomDockWidgetArea);
	this->addDockWidget(Qt::RightDockWidgetArea, inputdockWidget);
	inputdockWidget->setWidget(_inputWidget);
	setCentralWidget(_glGraph);

	_animationTimer.setSingleShot(false);
    connect(&_animationTimer, SIGNAL(timeout()), this, SLOT(animate()));
}

DonAnalyzeWidget::~DonAnalyzeWidget()
{
	if (_simthreadHdl)
		CloseHandle(_simthreadHdl);
}

void DonAnalyzeWidget::createInputWidget()
{
	_inputWidget = new DonInputAnalWidget(this);
}

void DonAnalyzeWidget::createGLGraph()
{
	_glGraph = new GLGraph();
}

void DonAnalyzeWidget::showEvent(QShowEvent *)
{
	_inputWidget->setMaximumHeight(10000);
}

void DonAnalyzeWidget::startSimulationThread()
{
	if (_simthreadHdl) {
		CloseHandle(_simthreadHdl);
	}
	_animationTimer.start(10);
	_simthreadHdl = (HANDLE)_beginthreadex(0, 0, &simulationThreadFunc, (void*)this, 0, 0);
}

void DonAnalyzeWidget::doSimulation()
{
	GameSimulation gamesim;
	int strategy = _inputWidget->getStrategyType();
	int moneymgnt = _inputWidget->getMoneyMgntType();
	gamesim.setStrategy(strategy);
	gamesim.setMoneyManagement(moneymgnt);

	// Get Records to simulate
	float totalwon = 0.f;
	float totalwager = 0.f;
	int numwon = 0;
	int numlost = 0;
	float maxNeedMoney = -FLT_MAX;
	int count = 0;
	float nwwon = 0.f;
	float nwwager = 0.f;
	float maxneed = 0.f;
	bool isplayed = false;
	int nwplayed = 0;
	float commision = 0.012;
	unsigned int numround = 0;
	std::vector<float> wonmoneylist;
	std::string filePath = _inputWidget->filePath().toStdString();
	std::vector<RecordIO::GameRCDInfo> games = RecordIO::getInstance()->readGameRecords(filePath);
	auto updateInfor = [&]() {
		char buff[512];
		
		sprintf(buff,   "Number of Games: %d<br>"
						"Number of Rounds: %d<br>"
						"Round Won: %d<br>"
						"Round Lost:	%d<br>"
						"Total Wager: %f<br>"
						"Money Won: %f<br>",
						count, numround, numwon, numlost,
						gamesim.getGameController()->getMoneyManagement()->getTotalWager(),
						totalwon);
		this->_infoStr = QString(buff);
	};
	{
		Lock guard(g_SimulationCritical);
		_glGraph->clearData();
		_glGraph->setAutoMove(true);
	}
	for (auto it = games.begin(); it != games.end(); ++it)
	{
		if (it->_strategyType != StrategyBase::STRATEGY_KNN_TYPE)
			continue;
		if (it->_numround > 75) {
			int potenbug = 1;
		}
		gamesim.resetGameInfo();
		gamesim.playAGame(*it);
		float won = gamesim.getGameController()->getMoneyManagement()->getGameWon();
		if (won != it->_wonMoney)
		{
			int bug = 0;
		}
		
		totalwon += won;
		if (maxNeedMoney < gamesim.getGameController()->getGameStatistic()->getMaxMoneyNeededInBankroll())
		{
			maxNeedMoney = gamesim.getGameController()->getGameStatistic()->getMaxMoneyNeededInBankroll();
		}
		numwon += gamesim.getGameController()->getGameStatistic()->calcNumWonRounds();
		numlost += gamesim.getGameController()->getGameStatistic()->calcNumLostRounds();
		numround += gamesim.getGameController()->getGameInfo()->getPlayedRoundNumber();
		if (count > 4) {
			float need = (it - 4)->_maxNeededMoney + (it - 3)->_maxNeededMoney +
							(it - 2)->_maxNeededMoney + (it - 1)->_maxNeededMoney +
							it->_maxNeededMoney;
			if (need > maxneed) maxneed = need;
		}
		if (count > 0) {
			if ((it - 1)->_wonMoney > 0) {
				nwwon += it->_wonMoney;
				nwwager += gamesim.getGameController()->getMoneyManagement()->getGameWager();
			}
		}

		count++;
		{
			Lock guard(g_SimulationCritical);
			float com = gamesim.getGameController()->getMoneyManagement()->getTotalWager() * 0.01;
			_glGraph->addNewData(totalwon);
		}
		// Sleep time according to speed
		int speed = _inputWidget->getSimSpeed() / 5;
		// update information
		if (speed < 5 || count % (speed / 3) == 0 )
			updateInfor();
		Sleep((20 - speed) * 25);
	}
	_animationTimer.stop();
	_glGraph->setAutoMove(false);
	updateInfor();
	emit updateInforSignal(_infoStr);
	gamesim.getGameController()->dumpGameInfo(true);
	//totalwon = gamesim.getWonMoney();
	std::cout <<"wonmoney: " << totalwon<<endl;
	std::cout << "final won: " << gamesim.getGameController()->getMoneyManagement()->getTotalWager() * 0.007 + totalwon << endl;
	std::cout << "nwwon: "   << nwwon << "; aftercom " << nwwon + 0.007 * nwwager << endl;
	std::cout << "nwwager: "   << nwwager << endl;
	std::cout << "nwmaxneed: " << maxneed << endl;
	std::cout << "numgames: " << count << endl;
	std::cout << "won: " << numwon << endl;
	std::cout << "lost: " << numlost << endl;
	std::cout << "max Need: " << maxNeedMoney << endl;
	std::cout << "total wager: " << gamesim.getGameController()->getMoneyManagement()->getTotalWager() << endl;
}

void DonAnalyzeWidget::animate()
{
	_inputWidget->updateInformation(_infoStr);
	_glGraph->update();
    update();
}