#include "GLButton.h"
#include "GLControlWidget.h"
#include <GLView.h>

GLButton::GLButton(GLView *glview, GLControlWidget *parent):
			GLControlWidget(glview, parent)
{
}

GLButton::GLButton(): GLControlWidget(nullptr, nullptr)
{
}

GLButton::~GLButton()
{
}

void GLButton::render()
{
	double top;
	double left;
	double width;
	double height;

	top = calcTop() * _glview->getZoomFactor();
	left = calcLeft() * _glview->getZoomFactor();
	width = _width * _glview->getZoomFactor();
	height = _height* _glview->getZoomFactor();

	glPushMatrix();
	glLoadIdentity();
	glTranslated(this->_glview->getCenterX(), this->_glview->getCenterY(), 0.0);
	glDisable(GL_DEPTH_TEST);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_QUADS);
		glVertex2d(left, top - height); // bottom left
		glVertex2d(left + width, top - height); // bottom right
		glVertex2d(left + width, top); // top right
		glVertex2d(left, top);// top left
	glEnd();
	glEnable(GL_DEPTH_TEST);
	glPopMatrix();
}

void GLButton::onFocused()
{}

void GLButton::onMouseMove()
{}

void GLButton::onMousePressed()
{}

void GLButton::onMouseReleased()
{}