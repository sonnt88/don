#include "iostream"
#include <vector>
#include <time.h>

using namespace std;

#define MAX_GEN_CHAIN 30
#define MAX_SOLUTIONS 20

typedef std::vector<short> ChromosomeType ;
short get_random(short min, short max) 
{
	/* Returns a random double between min and max */
	return (short)(max - min) * ( (float)rand() / (float)RAND_MAX ) + min;
}

// initialize chromosome solutions
void initializeChromosome(std::vector<ChromosomeType> &chormosomes) 
{
	for (unsigned i = 0; i < MAX_SOLUTIONS; ++i)
		cout << "random number = "<< get_random(1,13) << endl;
}

void geneticAlgorithm(int targetNumber)
{
	srand(time(NULL));
	cout <<"GENETIC ALGORITHM DEMO" << endl;
	std::vector<ChromosomeType> chromosomes;
	initializeChromosome(chromosomes);
	

	system("pause");
}