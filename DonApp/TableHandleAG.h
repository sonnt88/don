#ifndef __TABLE_HANDLE_AG_H__
#define __TABLE_HANDLE_AG_H__

#include "WindowUtils.h"
#include "TableHandleBase.h"

class TableHandleAG: public TableHandleBase {
public:
	TableHandleAG();
	~TableHandleAG();

	/*
	**	override virtual functions
	*/
	void config();
	int checkWinner();
	enTableState checkTableState();
	bool checkBigResult();
	bool isReach24Round();
	void doBet(int nlost);

private:
	
};
#endif