#include "TableHandleAG.h"
#include "Utils.h"

TableHandleAG::TableHandleAG()
{
	config();
}

TableHandleAG::~TableHandleAG()
{
}

void TableHandleAG::config()
{
	// inside a single table
	_tblconfig._coordBetStatus			= ScreenCoord(220,110);
	_tblconfig._coordTableStatus		= ScreenCoord(279,155);
	_tblconfig._coordSmallCard			= ScreenCoord(258,260);
	_tblconfig._coordBigCard1			= ScreenCoord(303, 380);
	_tblconfig._coordBigCard2			= ScreenCoord(506, 380);
	_tblconfig._coordPlayerWin			= ScreenCoord(310, 483);
	_tblconfig._coordBankerWin			= ScreenCoord(520, 483);
	_tblconfig._coordTieWin				= ScreenCoord(390,483);

	_tblconfig._coordMoney100			= ScreenCoord(1543, 880); 
	_tblconfig._coordMoney20			= ScreenCoord(1368, 880);
	_tblconfig._coordBigBet				= ScreenCoord(314, 741);
	_tblconfig._coordSmallBet			= ScreenCoord(1523, 750);
	_tblconfig._coordBetConfirm			= ScreenCoord(1023, 890);
}

TableHandleAG::enTableState TableHandleAG::checkTableState() {
	int rgb[3];
	int res = DonApp::Utils::getRGBAt(_tblconfig._coordBetStatus, rgb);

	if (rgb[0] < 30 && rgb[1] > 100 && rgb[2] < 30) {  // Wait bet
		return TABLE_WAITING_BET;
	} else if (rgb[0] > 200 && rgb[1] < 30 && rgb[2] < 30) {  // Stop Bet
		DonApp::Utils::getRGBAt(_tblconfig._coordTableStatus, rgb);
		if (rgb[0] > 200 && rgb[0] < 230 &&
			rgb[1] > 145 && rgb[1] < 175 &&
			rgb[2] > 100 && rgb[2] < 130) {  // Shuffling
				return TABLE_SHUFFLING;
		} else {
			if (checkWinner() != -1) { 
				return TABLE_SHOW_WINER;
			} else {
				return TABLE_OPEN_CARD;
			}
		}

	} else {
		//cout << "** Not on any table" << endl;
		return TABLE_INVALID;
	}
}

int TableHandleAG::checkWinner() {
	int rgb1[3], rgb2[3], rgb3[3], rgb4[3];

	DonApp::Utils::getRGBAt(_tblconfig._coordSmallCard, rgb4);
	if (rgb4[0] < 220 || rgb4[1] < 220 && rgb4[2] < 220) {
		//return -1;
	}
	DonApp::Utils::getRGBAt(_tblconfig._coordPlayerWin, rgb1);
	DonApp::Utils::getRGBAt(_tblconfig._coordBankerWin, rgb2);
	DonApp::Utils::getRGBAt(_tblconfig._coordTieWin, rgb3);

	if (rgb1[0] < 30 && rgb1[1] < 30 && rgb1[2] > 150) { // player win
		return 0;
	} else if (rgb2[0] > 150 && rgb2[1] < 30 && rgb2[2] < 30) { // banker win
		return 1;
	} else if (rgb3[0] < 60 && rgb3[1] > 100 && rgb3[2] < 30) { // tie
		return 2;
	}

	return -1;
}

bool TableHandleAG::checkBigResult() {
	int rgb1[3], rgb2[3];

	DonApp::Utils::getRGBAt(_tblconfig._coordBigCard1, rgb1);
	DonApp::Utils::getRGBAt(_tblconfig._coordBigCard2, rgb2);

	if (rgb1[0] > 200 && rgb1[1] > 200 && rgb1[2] > 200 ||
		rgb2[0] > 200 && rgb2[1] > 200 && rgb2[2] > 200) {  // check big
		return true;
	} else {
		return false;
	}
}

bool TableHandleAG::isReach24Round() {
#if 0
	int rgb[3];
	int white[3] = {255,255,255};
	//getRGBAt(g_coordRoundNumber24, rgb);
	DonApp::Utils::getRGBAt(_tblconfig._coordRoundNumber27, rgb);

	if (DonApp::Utils::isReachColor(rgb)) 
		return true;

	return false;
#endif
	return 0;
}

void TableHandleAG::doBet(int nlost) {
#if 0
	//return;
	int mm[] = {1, 3, 9, 27, 81, 250}; /*{1, 1, 4, 12, 36, 200}; */
				/*{1, 1, 5, 15, 45, 200};*/
				/*{2, 6, 18, 54, 162};*/

	int moneyBetUnit = 100;
	int rate = moneyBetUnit/100;
	g_beted = mm[nlost];
	cout <<"\n==================== DO BET ======================= \n";
	if (nlost < MAX_CONT_LOST && g_enableBet == true) {
		cout << "\n$$$$: do bet with money: " << mm[nlost] * moneyBetUnit << endl;	
		cout << " current number of lost = " << nlost;
		cout << endl;

		click(g_coordMoney100);
		//click(g_coordMoney20);
		Sleep(1000);
		for (int i = 0; i < mm[nlost] * rate; ++i)
		{
			Sleep(10);
			cout << "bet money: " << moneyBetUnit;
			click(g_coordBigBet);
		}

		Sleep(1000);
		for (int i = 0; i < 20; i++) {
			Sleep(200);
			click(g_coordBetConfirm);
		}
		
		g_hasBet = true;
	} else if (nlost >= MAX_CONT_LOST) {
		cout << "\nnumber of lost exeeded" << endl;
		g_hasBet = false;
	}
	cout <<"===================================================== \n";
#endif
}