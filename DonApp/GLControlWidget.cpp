#include "GlControlWidget.h"
#include "GLView.h"

GLControlWidget::GLControlWidget(GLView *glview, GLControlWidget *parent)
{
	_glview = glview;
	_parent = parent;
}

GLControlWidget::~GLControlWidget()
{
}

double GLControlWidget::calcTop() // for render in glview
{
	double top = _ypos;
	if (_parent)
	{
		top = _parent->calcTop() - _ypos;
	}

	return top;
}

double GLControlWidget::calcLeft() // for render in glview
{
	double left = _xpos;
	if (_parent) 
	{
		left = _xpos + _parent->calcLeft();
	}

	return left;
}

void GLControlWidget::setBoundingRect(double top, double left, double width, double height)
{
	_xpos = left;
	_ypos = top;
	_width = width;
	_height = height;
}

void GLControlWidget::drawRect()
{
	double top;
	double left;
	double width;
	double height;

	top = calcTop() * _glview->getZoomFactor();
	left = calcLeft() * _glview->getZoomFactor();
	width = _width * _glview->getZoomFactor();
	height = _height* _glview->getZoomFactor();

	glPushMatrix();
	glLoadIdentity();
	glTranslated(this->_glview->getCenterX(), this->_glview->getCenterY(), 0.0);
	glDisable(GL_DEPTH_TEST);
	glColor3f(1.0f, 1.0f, 0.0f);
	glBegin(GL_LINE_LOOP);
		glVertex2d(left, top - height); // bottom left
		glVertex2d(left + width, top - height); // bottom right
		glVertex2d(left + width, top); // top right
		glVertex2d(left, top);// top left
	glEnd();
	glEnable(GL_DEPTH_TEST);
	glPopMatrix();
}