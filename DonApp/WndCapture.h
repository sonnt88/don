#ifndef __WND_CAPTURE_H__
#define __WND_CAPTURE_H__

#include "WindowUtils.h"
#include "Lock.h"

class WndCapture {
public:
	~WndCapture();

	static WndCapture &getInstance();

	inline int getWndImgWidth() { return _wndImgWidth; }
	inline int getWndImgHeight() { return _wndImgHeight; }
	inline BYTE *getPixelsColor() { return _pixelsColor; }
	inline HWND getWebWnd() { return _webWnd; }
	void setWebWnd(HWND hwnd) { _webWnd = hwnd; }
	void capture();

private:
	WndCapture();

private:
	static WndCapture* volatile _wndCaptureInstance;

	int _wndImgWidth;
	int _wndImgHeight;
	BYTE *_pixelsColor;
	HWND _webWnd;
};

#endif