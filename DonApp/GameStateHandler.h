#ifndef __GAME_STATE_HANDLER_H__
#define __GAME_STATE_HANDLER_H__

class GameController;

enum enActParmType {
	WINNER_TYPE = 0
};

struct ActionParam {
	int _type;
	void *_value;
};

class GameStateHandler {
public:
	enum enGameStateHandlerType {
		GAME_WAITING_BET,
		GAME_SHUFFLING,
		GAME_SHOWING_WINER,
		TOTAL_STATE_SIZE
	};

public:
	GameStateHandler() {
		_preState = nullptr;
		_gameController = nullptr;
	}
	~GameStateHandler() {}
	void setPreState(GameStateHandler *state) {
		_preState = state;
	}
	GameStateHandler *getPreState() {
		return _preState;
	}
	void setGameController(GameController *gameCtrl) {
		_gameController = gameCtrl;
	}
	GameController *getGameController() {
		return _gameController;
	}
	virtual void doAction(ActionParam *param = nullptr) = 0;
	virtual enGameStateHandlerType nextState() = 0;
	virtual enGameStateHandlerType getType() = 0;

protected:
	GameStateHandler *_preState;
	GameController *_gameController;
};

class GameState_Shuffing: public GameStateHandler{
public:
	GameState_Shuffing(){};
	~GameState_Shuffing();
	virtual void doAction(ActionParam *param = nullptr);
	virtual enGameStateHandlerType nextState()
	{
		return GAME_WAITING_BET;
	}
	virtual enGameStateHandlerType getType()
	{ return GAME_SHUFFLING; }

private:
};

class GameState_WaitingBet: public GameStateHandler{
public:
	GameState_WaitingBet(){};
	~GameState_WaitingBet();
	virtual void doAction(ActionParam *param = nullptr);
	virtual enGameStateHandlerType nextState()
	{
		return GAME_SHOWING_WINER;
	}
	virtual enGameStateHandlerType getType()
	{ return GAME_WAITING_BET; }
private:

};

class GameState_ShowingWinner: public GameStateHandler{
public:
	GameState_ShowingWinner(){};
	~GameState_ShowingWinner();
	virtual void doAction(ActionParam *param = nullptr);
	virtual enGameStateHandlerType nextState()
	{
		return GAME_WAITING_BET;
	}
	virtual enGameStateHandlerType getType()
	{ return GAME_SHOWING_WINER; }
private:

};
#endif