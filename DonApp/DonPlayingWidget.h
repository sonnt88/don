#ifndef __DON_PLAYING_WIDGET_H__
#define __DON_PLAYING_WIDGET_H__
#include "qmainwindow.h"
#include <Windows.h>
#include <qtimer.h>
#include "CasinoObserver.h"

class GLGraph;
class DonInputPlayingWidget;
class CasinoBase;
class QTextEdit;

class DonPlayingWidget:public QMainWindow, 
					   public CasinoObserver				
{
		Q_OBJECT
public:
	DonPlayingWidget();
	~DonPlayingWidget();
	/* implement func of abstract class */
	virtual void onWonMoneyChanged(float newWonMoney);
	virtual void onInformationChanged(const QString &txt);

	//void showEvent(QShowEvent *);
	void DonPlayingWidget::startPlayingThread();
	void doPlaying();

public slots:
	void animate();

//signals:
	//void updateInforSignal(const QString &text);

private:
	//void createInputWidget();
	void createGLGraph();

private:
	GLGraph *_glGraph;
	DonInputPlayingWidget *_inputWidget;
	QTextEdit *_playingInfo;
	HANDLE _playingthreadHdl;
	CasinoBase *_casino;
	//QString _infoStr;
	QTimer _animationTimer;
};
#endif