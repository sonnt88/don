#ifndef KEVVOXTESTING_H
#define KEVVOXTESTING_H

#include <QtGui/QMainWindow>

class MyToolBar;
class GLGraph;
class InputWidget;

class DonMainWindow : public QMainWindow
{
	Q_OBJECT

public:
	DonMainWindow(QWidget *parent = 0, Qt::WFlags flags = 0);
	~DonMainWindow();
	GLGraph *getGlView() { return _glView;}
private:
	//Ui::DonMainWindowClass ui;
	GLGraph *_glView;
};

#endif // KEVVOXTESTING_H
