#ifndef __GAME_PLAYING_H__
#define __GAME_PLAYING_H__

#include "TableHandleBase.h"
#include "GameStateHandler.h"

class GameController;
class GameStateHandler;
class CasinoBase;

class GamePlaying {
public:
	GamePlaying();
	virtual ~GamePlaying();
	virtual void handleGameWaitingBet(TableHandleBase::enTableState preTableState);
	virtual void handleGameShowWinner(TableHandleBase::enTableState preTableState);
	virtual void handleGameShuffling(TableHandleBase::enTableState preTableState);

	TableHandleBase *getTableHandle();
	void setTableHandle(TableHandleBase *tablehdl);
	GameController *getGameController();
	void setGameController(GameController *gameCtrl);
	GameStateHandler *getGameStateHandler();
	void setGameStateHandler(GameStateHandler *gameStatehdl);
	CasinoBase *getCasino();
	void setCasino(CasinoBase *casino);
	void play();
	void showWinner(short winner);

protected:
	CasinoBase *_casino;
	TableHandleBase *_tblHandle;
	GameController *_gameController;
	GameStateHandler *_curStateHandler;
	TableHandleBase::enTableState _curTableState;
	GameStateHandler *_gameStateHandlers[GameStateHandler::TOTAL_STATE_SIZE];
};
#endif