#include "GameStateHandler.h"
#include "RecordIO.h"
#include "GameController.h"
#include "Lock.h"

void GameState_Shuffing::doAction(ActionParam *param)
{
	if (_gameController) {
		Lock guard(g_RecordIOCritical);
		RecordIO *rc = RecordIO::getInstance();
		rc->writeGameRecord(_gameController);
		_gameController->resetGameInfo();
	}
}

void GameState_WaitingBet::doAction(ActionParam *param)
{
	//Dobet here
	if (_gameController)
		_gameController->doBet();
}

void GameState_ShowingWinner::doAction(ActionParam *param)
{
	short winner = 0;
	if (param && param->_type == enActParmType::WINNER_TYPE) {
		winner = *(short *)param->_value;
	}

	if (_gameController) {
		_gameController->updateNewWinner(winner);
		_gameController->dumpGameInfo();
	}
}