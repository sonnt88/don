#include "DonPlayingWidget.h"
#include "DonInputPlayingWidget.h"
#include "qdockwidget.h"
#include "GLGraph.h"
#include "qmainwindow.h"
#include "qstring.h"
#include <process.h>
#include "IncludeLib.h"
#include "CasinoAG.h"
#include "CasinoBase.h"
#include "TableHandleAG.h"
#include "TableHandleBase.h"
#include "qtextedit.h"

unsigned int __stdcall playingThreadFunc(void *lparam) {
		if (NULL == lparam) {
			return 0;
		}
		
		DonPlayingWidget *playingWidget = static_cast<DonPlayingWidget *>(lparam);
		playingWidget->doPlaying();

		return 0;
}

DonPlayingWidget::DonPlayingWidget()
{
	createGLGraph();
	QDockWidget *inputdockWidget = new QDockWidget(tr("Input"), this);
    inputdockWidget->setAllowedAreas(Qt::LeftDockWidgetArea |
                                Qt::RightDockWidgetArea|
								Qt::TopDockWidgetArea|
								Qt::BottomDockWidgetArea);
	this->addDockWidget(Qt::RightDockWidgetArea, inputdockWidget);
	_inputWidget = new DonInputPlayingWidget(this);
	inputdockWidget->setWidget(_inputWidget);
	_playingInfo = new QTextEdit();
	//_playingInfo->setMaximumSize(200, 100);
	_playingInfo->setReadOnly(true);
	QDockWidget *infoDockWidget = new QDockWidget(tr("Information"), this);
    inputdockWidget->setAllowedAreas(Qt::LeftDockWidgetArea |
                                Qt::RightDockWidgetArea|
								Qt::TopDockWidgetArea|
								Qt::BottomDockWidgetArea);
	this->addDockWidget(Qt::BottomDockWidgetArea, infoDockWidget);
	infoDockWidget->setWidget(_playingInfo);


	setCentralWidget(_glGraph);
	_playingthreadHdl = nullptr;
	_animationTimer.setSingleShot(false);
    connect(&_animationTimer, SIGNAL(timeout()), this, SLOT(animate()));
}

DonPlayingWidget::~DonPlayingWidget()
{
	if (_glGraph) {
		delete _glGraph;
	}
}

void DonPlayingWidget::onWonMoneyChanged(float newWonMoney)
{
	_glGraph->addNewData(newWonMoney);
}

void DonPlayingWidget::onInformationChanged(const QString &txt)
{
	_playingInfo->append(txt);
}

void DonPlayingWidget::createGLGraph()
{
	_glGraph = new GLGraph();
	GraphConfig *config = _glGraph->getGraphConfig();
	config->_yInterval = 5;
	config->_xInterval = 5;
	_glGraph->setAutoMove(true);
	_glGraph->setZoomFactor(0.2);
}

void DonPlayingWidget::startPlayingThread()
{
	if (this->_playingthreadHdl) {
		CloseHandle(_playingthreadHdl);
	}
	_animationTimer.start(5000);
	_playingthreadHdl = (HANDLE)_beginthreadex(0, 0, &playingThreadFunc, (void*)this, 0, 0);
}

void DonPlayingWidget::doPlaying()
{
	int strategy = _inputWidget->getStrategyType();
	int moneymgnt = _inputWidget->getMoneyMgntType();

	// if mornitor
	_casino = new CasinoAG();
	if (0 == _inputWidget->getPlayingMode())
	{
		_casino->monitorLobby(this);
	} else {
		GameController *gameController = new GameController();
		StrategyBase *bccattack = new KNNStrategy(gameController);
		MoneyManagement *moneym = new BASMoneyManagement(gameController);
		TableHandleBase *tblHandle = new TableHandleAG();
		tblHandle->setTableNumber(4);
		//tblHandle->config();
		_casino->playTable(tblHandle, gameController);
	}
}

void DonPlayingWidget::animate()
{
	//_inputWidget->updateInformation(_infoStr);
	_glGraph->update();
    update();
}