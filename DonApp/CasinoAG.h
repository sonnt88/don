#ifndef __CASINO_AG_H__
#define __CASINO_AG_H__

#include "CasinoBase.h"
#include "WindowUtils.h"
#include <vector>

#define SQUARE_SIZE 23
#define NUMBER_TABLE 7
#define COLUM_SIZE 10
#define ROW_SIZE 6
#define MAX_NUMBER_GAME 24

class TableHandleBase;
class GameController;

class CasinoAG: public CasinoBase 
{
public:
	enum enGroup {
		GROUP_AGIN = 1,
		GROUP_AGQ,
		NUM_GROUP
	};

public:
	CasinoAG();
	virtual ~CasinoAG();

	/*
	** override virtual functions 
	*/ 
	enCasinoState checkStateOfCasino();
	virtual enCasinoID getID();
	void playTable(TableHandleBase *table, 
				   GameController *gameController);
	void config();
	void monitorLobby(CasinoObserver *observer);

	/* 
	** normal functions
	*/
	int getCurrentGroup();
	int findBestTblToPlay(int group);
	void outTable(int i);
	void joinTable(int i);
	void outTableForMornitor(unsigned short tableNumber);
	bool isTableAvailForMornitor(short tableNum);
	bool isTableShufflingLobby(short tableNum);
	int monitorTable(short tableNum, MonitorTableParam *param);
	void getRBGAtRound(short round, short table, int *rgb);
	bool isInLobby();

private:
	int _colorSlectedGroup[3];
	std::vector<TablePlayingHandle> _playingThreads;

	// additional configs
	ScreenCoord _coordAGINGroup;
	ScreenCoord _coordAGQGroup;
	ScreenCoord _coordShift;
};
#endif