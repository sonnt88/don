#include "CasinoAG.h"
#include "Utils.h"
#include "GamePlayingAG.h"
#include "GameController.h"
#include "RecordIO.h"
#include "BASMoneyManagement.h"
#include "KNNStrategy.h"
#include "CasinoObserver.h"

CasinoAG::CasinoAG() {
	config();
	_colorSlectedGroup[0] = 182;
	_colorSlectedGroup[1] = 149; 
	_colorSlectedGroup[2] = 122;
}

CasinoAG::~CasinoAG() {
}

void CasinoAG::config()
{
	_coordShift			= ScreenCoord(10, 18);

	// Lobby
	_config._AllTableBoard[0]		= ScreenCoord(1122, 218);
	_config._AllTableBoard[1]		= ScreenCoord(349, 447);
	_config._AllTableBoard[2]		= ScreenCoord(1122, 447);
	_config._AllTableBoard[3]		= ScreenCoord(349, 676);
	_config._AllTableBoard[4]		= ScreenCoord(1122, 676);
	_config._AllTableBoard[5]		= ScreenCoord(349, 905);
	_config._AllTableBoard[6]		= ScreenCoord(1122, 905);

	_config._coordEnterTbl[0]		= ScreenCoord(1750, 365);
	_config._coordEnterTbl[1]		= ScreenCoord(1000, 595);
	_config._coordEnterTbl[2]		= ScreenCoord(1750, 590);
	_config._coordEnterTbl[3]		= ScreenCoord(1000,825);
	_config._coordEnterTbl[4]		= ScreenCoord(1720, 825);
	_config._coordEnterTbl[5]		= ScreenCoord(1000,1055);
	_config._coordEnterTbl[6]		= ScreenCoord(1691,1061);

	_coordAGINGroup			= ScreenCoord(185, 450);
	_coordAGQGroup			= ScreenCoord(185,500);
	
	//SHIFT
	ScreenCoord shift = ScreenCoord(-54, 9);
	for (unsigned i = 0; i < 6; ++i)
	{
		_config._coordEnterTbl[i]+=shift;
	}
}	

CasinoBase::enCasinoState CasinoAG::checkStateOfCasino()
{
	return UNKNOWN_STATE;
}

CasinoBase::enCasinoID CasinoAG::getID()
{
	return CasinoBase::CASINO_AG;
}

int CasinoAG::getCurrentGroup() {
	return GROUP_AGQ;

	int rgb[3];
	DonApp::Utils::getRGBAt(_coordAGINGroup, rgb);
	if (DonApp::Utils::isTwoEqualColor(_colorSlectedGroup, rgb)) {
		return GROUP_AGIN;
	} else {
		DonApp::Utils::getRGBAt(_coordAGQGroup, rgb);
		if (DonApp::Utils::isTwoEqualColor(_colorSlectedGroup, rgb)) {
			return GROUP_AGQ;
		}
	}

	return 0;
}

void CasinoAG::outTable(int i) {
	HWND hwnd = WndCapture::getInstance().getWebWnd();
	switch (i) {
	case 1: 
		click(_coordAGINGroup, hwnd);
		break;
	case 2:
		click(_coordAGQGroup, hwnd);
		break;
	default:
		break;
	}
}

int CasinoAG::findBestTblToPlay(int group) {
	int numtbl;
	int bestTbl = 0;
	int minBlankId = INT_MAX;
	numtbl = group == 1 ? 5 : 6;
	for (int t = 0; t < numtbl; ++t) {
		int blankInd = 59;
		ScreenCoord iboard = _config._AllTableBoard[t];
		bool foundBlankInd = false;
		for (unsigned i = 0; i < COLUM_SIZE; i++) {
			for (unsigned k = 0; k < ROW_SIZE; ++k) {
				blankInd = i * ROW_SIZE + k;
				ScreenCoord checkCoord;
				checkCoord._x = iboard._x + SQUARE_SIZE * i + _coordShift._x;
				checkCoord._y = iboard._y + SQUARE_SIZE * k + _coordShift._y;
				int rgb[3];
				DonApp::Utils::getRGBAt(checkCoord, rgb);
				if (!DonApp::Utils::isReachColor(rgb)) {
					foundBlankInd = true;
					break;
				}
			}

			if (foundBlankInd) {
				break;
			}
		}

		if (blankInd < minBlankId) {
			minBlankId = blankInd;
			bestTbl = t;
		}
	}

	if (minBlankId < MAX_NUMBER_GAME)
		return bestTbl;
	else return -1;
}

void CasinoAG::joinTable(int i) 
{
	HWND hwnd = WndCapture::getInstance().getWebWnd();
	ScreenCoord enterBtnCoord = _config._coordEnterTbl[1];
	int rgbEnterBtn[3];
	DonApp::Utils::getRGBAt(enterBtnCoord, rgbEnterBtn);
	bool isRed = rgbEnterBtn[0] > 120 && rgbEnterBtn[1] < 30 && rgbEnterBtn[2] < 30;
	while (isRed) 
	{
		click(_config._coordEnterTbl[i], hwnd);
		Sleep(1000);
		DonApp::Utils::getRGBAt(enterBtnCoord, rgbEnterBtn);
		isRed = rgbEnterBtn[0] > 120 && rgbEnterBtn[1] < 30 && rgbEnterBtn[2] < 30;
	}
}

void CasinoAG::outTableForMornitor(unsigned short tableNumber)
{
	int group = 0;
	group = getCurrentGroup();
	outTable(group);
	joinTable(tableNumber);
}

void CasinoAG::playTable(TableHandleBase *table, 
				   GameController *gameController)
{
	HANDLE threadHdl = nullptr;
	TablePlayingHandle nwTablePlaying;

	GamePlaying *gamePlaying = new GamePlayingAG();
	gamePlaying->setCasino(this);
	gamePlaying->setTableHandle(table);
	gamePlaying->setGameController(gameController);

	threadHdl = (HANDLE)_beginthreadex(0, 0, &DonApp::Utils::playTableThreadFunc, (void*)gamePlaying, 0, 0);
	nwTablePlaying._gamePlaying = gamePlaying;
	nwTablePlaying._threadHdl = threadHdl;
	_playingThreads.push_back(nwTablePlaying);
}

void CasinoAG::monitorLobby(CasinoObserver *observer) 
{
	int count = 0;

	MonitorTableParam param0;
	MonitorTableParam param1;
	MonitorTableParam param2;
	MonitorTableParam param3;
	MonitorTableParam param4;
	MonitorTableParam param5;
	MonitorTableParam param6;
	std::vector<MonitorTableParam*> tableList;

	auto createGameController = [&]() -> GameController* {
		GameController *gameController = new GameController();
		StrategyBase *bccattack = new KNNStrategy(gameController);
		MoneyManagement *moneym = new BASMoneyManagement(gameController);
		gameController->setStrategy(bccattack);
		gameController->setMoneyManagement(moneym);

		return gameController;
	};
	
	param0._currentRound = 0;
	param0._currentState = 0;
	param0._gameCtrler = createGameController();
	tableList.push_back(&param0);

	param1._currentRound = 0;
	param1._currentState = 0;
	param1._gameCtrler = createGameController();
	tableList.push_back(&param1);

	param2._currentRound = 0;
	param2._currentState = 0;
	param2._gameCtrler = createGameController();
	tableList.push_back(&param2);

	param3._currentRound = 0;
	param3._currentState = 0;
	param3._gameCtrler = createGameController();
	tableList.push_back(&param3);
#ifdef DEBUG_MODE0
	param4._currentRound = 0;
	param4._currentState = 0;
	param4._gameCtrler = createGameController();
	tableList.push_back(&param4);

	param5._currentRound = 0;
	param5._currentState = 0;
	param5._gameCtrler = createGameController();
	tableList.push_back(&param5);

	param6._currentRound = 0;
	param6._currentState = 0;
	param6._gameCtrler = createGameController();
	tableList.push_back(&param6);
#endif
	
	while (true) 
	{
		Sleep(1000);
		if (false == isInLobby()) 
		{
			cout << "not in Lobby \n";
			continue;
		}
		
		float wonMoney = 0.f;
		float totalWager = 0.f;
		int updated = 0;
		for (unsigned i = 0; i < tableList.size(); ++i) 
		{
			if (monitorTable(i, tableList[i]))
			{
				updated = 1;
			}
			wonMoney += tableList[i]->_gameCtrler->wonMoney();
			totalWager += tableList[i]->_gameCtrler->totalWager();
		}

		if (updated)
		{
			observer->onWonMoneyChanged(wonMoney);
			observer->onInformationChanged("new information");
		}
		// show won money
		if (count == 10) {
			cout << endl << endl;

			cout << " >> >> won money: " <<	wonMoney << endl;
			cout << " >> >> total wager: " << totalWager;
			
			cout << endl << endl;
			count = 0;
		}
		count++;
	}
}

bool CasinoAG::isInLobby()
{
	int rgb[3];
	int black[3] = {35, 31, 31};
	int red[3] = {138, 0, 0};

	DonApp::Utils::getRGBAt(ScreenCoord(890, 600), rgb);
	if (false == DonApp::Utils::isTwoEqualColor(rgb, red)) 
		return false;
	DonApp::Utils::getRGBAt(ScreenCoord(1660, 600), rgb);
	if (false == DonApp::Utils::isTwoEqualColor(rgb, red)) 
		return false;
	DonApp::Utils::getRGBAt(ScreenCoord(890, 830), rgb);
	if (false == DonApp::Utils::isTwoEqualColor(rgb, red)) 
		return false;

	DonApp::Utils::getRGBAt(ScreenCoord(600, 430), rgb);
	if (false == DonApp::Utils::isTwoEqualColor(rgb, black)) 
		return false;

	return true;
}

bool CasinoAG::isTableAvailForMornitor(short tableNum)
{
	// check round 45th is played or not
	int bestTbl = 0;
	int minBlankId = INT_MAX;
	ScreenCoord iboard = _config._AllTableBoard[tableNum];

	int i = 7, k = 2; // round = 7 * 6 + 3 = 45
	ScreenCoord checkCoord;
	checkCoord._x = iboard._x + SQUARE_SIZE * i + _coordShift._x;
	checkCoord._y = iboard._y + SQUARE_SIZE * k + _coordShift._y;
	int rgb[3];
	DonApp::Utils::getRGBAt(checkCoord, rgb);
	if (DonApp::Utils::isReachColor(rgb)) 
	{
		return false;
	}

	return true;
}

bool CasinoAG::isTableShufflingLobby(short tableNum)
{
	int shufflingColor[3] = {128,128,128};
	int rgb[3];
	ScreenCoord iboard = _config._AllTableBoard[tableNum];
	iboard._x += _coordShift._x;
	iboard._y += _coordShift._y;
	DonApp::Utils::getRGBAt(iboard, rgb);
	bool isShuff = DonApp::Utils::isTwoEqualColor(shufflingColor, rgb);

	return isShuff;
}

int CasinoAG::monitorTable(short tableNum, MonitorTableParam *param)
{
	int blankColor[3] = {255,255,255};
	int bankerColor[3] = {196, 45, 42};
	int playerColor[3] = {55, 53, 243};
	int tieColor[3] = {54, 155, 3};
	int winner = 0;
	int rc = 0;

	int rgb[3];
	bool curState = 0; // 0 mean not started
	ScreenCoord iboard = _config._AllTableBoard[tableNum];

	bool isTableShuffling = isTableShufflingLobby(tableNum);
	if (isTableShuffling) {
		if (param->_gameCtrler) {
			Lock guard(g_RecordIOCritical);
			RecordIO *rc = RecordIO::getInstance();
			rc->writeGameRecord(param->_gameCtrler);
			param->_gameCtrler->resetGameInfo();
		}

		//reset param
		param->_currentRound = 0;
		return 0;
	} else if (param->_currentState == 0 &&
		false == isTableAvailForMornitor(tableNum)) {
		return 0;
	}

	param->_currentState = 1;

	while (1) {
		getRBGAtRound(param->_currentRound, tableNum, rgb);
		if (DonApp::Utils::isTwoEqualColor(rgb, blankColor) ||
			isTableShufflingLobby(tableNum)) {
			break;
		}

		if (DonApp::Utils::isTwoEqualColor(rgb, bankerColor)) {
			winner = 1;
		} else if (DonApp::Utils::isTwoEqualColor(rgb, playerColor)) {
			winner = 0;
		} else if (DonApp::Utils::isTwoEqualColor(rgb, tieColor)) {
			winner = 2;
		} else {
			cout <<"error detect winner at table "<< tableNum 
				 << ", round "<< param->_currentRound;
			cout << endl;
			Sleep (1000);
			continue;
		}

		param->_gameCtrler->updateNewWinner(winner);
		param->_gameCtrler->doBet(); // fake
		std::cout << "************ TABLE " << tableNum << "*************" << endl;
		param->_gameCtrler->dumpGameInfo();
		std::cout << "***************************************" << endl;
		param->_currentRound++;
		rc = 1;
	}

	return rc;
}

void CasinoAG::getRBGAtRound(short round, short table, int *rgb)
{
	int blankColor[3] = {255,255,255};
	ScreenCoord iboard = _config._AllTableBoard[table];
	ScreenCoord checkCoord;
	int i = round / 6;
	int k = round % 6;
	if (i > 8) { // for round >=55
		if (k == 0) {
			// check round + 1
			checkCoord._x = iboard._x + SQUARE_SIZE * 8 + _coordShift._x;
			checkCoord._y = iboard._y + SQUARE_SIZE * (k+1) + _coordShift._y;
			DonApp::Utils::getRGBAt(checkCoord, rgb);
			if (DonApp::Utils::isTwoEqualColor(blankColor, rgb)) { // next is blank
				checkCoord._x = iboard._x + SQUARE_SIZE * 8 + _coordShift._x;
				checkCoord._y = iboard._y + SQUARE_SIZE * (k) + _coordShift._y;
				DonApp::Utils::getRGBAt(checkCoord, rgb);
			} else { // next is not blank
				rgb[0] = 255;
				rgb[1] = 255;
				rgb[2] = 255;
			}
		} else {
			checkCoord._x = iboard._x + SQUARE_SIZE * 8 + _coordShift._x;
			checkCoord._y = iboard._y + SQUARE_SIZE * k + _coordShift._y;
			DonApp::Utils::getRGBAt(checkCoord, rgb);
		}
	} else {
		checkCoord._x = iboard._x + SQUARE_SIZE * i + _coordShift._x;
		checkCoord._y = iboard._y + SQUARE_SIZE * k + _coordShift._y;
		DonApp::Utils::getRGBAt(checkCoord, rgb);
	}
}