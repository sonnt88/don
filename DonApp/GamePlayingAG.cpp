#include "GamePlayingAG.h"
#include "GameController.h"
#include "CasinoAG.h"

GamePlayingAG::GamePlayingAG(): GamePlaying() 
{
}

GamePlayingAG::~GamePlayingAG()
{
}

void GamePlayingAG::handleGameWaitingBet(TableHandleBase::enTableState preTableState)
{
	GamePlaying::handleGameWaitingBet(preTableState);

	unsigned short playedRound = _gameController->numPlayedRound();
	if (playedRound % 4 == 0 && 
		preTableState != TableHandleBase::TABLE_INVALID) {
		dynamic_cast<CasinoAG *>(_casino)->
			outTableForMornitor(_tblHandle->getTableNumber());
	}
}