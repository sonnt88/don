#include "GamePlaying.h"
#include "GameController.h"
#include "KNNStrategy.h"
#include "BASMoneyManagement.h"
#include "CasinoAG.h"

GamePlaying::GamePlaying():
					_tblHandle(NULL),
					_gameController(NULL),
					_curStateHandler(NULL),
					_casino(NULL)
{
	_gameStateHandlers[GameStateHandler::GAME_SHUFFLING] = new GameState_Shuffing();
	_gameStateHandlers[GameStateHandler::GAME_WAITING_BET] = new GameState_WaitingBet();
	_gameStateHandlers[GameStateHandler::GAME_SHOWING_WINER] = new GameState_ShowingWinner();
}

GamePlaying::~GamePlaying()
{
	if (_tblHandle)
		delete _tblHandle;

	if (_gameController)
		delete _gameController;

	for (unsigned i = 0; i < GameStateHandler::TOTAL_STATE_SIZE; ++i)
	{
		delete _gameStateHandlers[i];
	}
}

GameController *GamePlaying::getGameController()
{
	return _gameController;
}

void GamePlaying::setGameController(GameController *gameCtrl)
{
	if (_gameController) {
		delete _gameController;
	}
	
	_gameController = gameCtrl;
	for (unsigned i = 0; i < GameStateHandler::TOTAL_STATE_SIZE; ++i)
	{
		_gameStateHandlers[i]->setGameController(_gameController);
	}
}

TableHandleBase *GamePlaying::getTableHandle()
{
	return _tblHandle;
}

void GamePlaying::setTableHandle(TableHandleBase *tablehdl)
{
	_tblHandle = tablehdl;
}

GameStateHandler *GamePlaying::getGameStateHandler()
{
	return _curStateHandler;
}

void GamePlaying::setGameStateHandler(GameStateHandler *gameStatehdl)
{
	_curStateHandler = gameStatehdl;
}

CasinoBase *GamePlaying::getCasino()
{
	return _casino;
}

void GamePlaying::setCasino(CasinoBase *casino)
{
	_casino = casino;
}

void GamePlaying::play()
{
	if (NULL == _tblHandle) {
		return;
	}
	TableHandleBase::enTableState preTableState = TableHandleBase::TABLE_INVALID;
	unsigned int count = 0;
	while (1) {
		Sleep(1000);
		_curTableState = _tblHandle->checkTableState();
		
		if (_curTableState == TableHandleBase::TABLE_INVALID) {
			if (_casino->getID() == CasinoBase::CASINO_AG) {
				dynamic_cast<CasinoAG *>(_casino)->outTableForMornitor(_tblHandle->getTableNumber());
			}
			preTableState = _curTableState;
			_tblHandle->showState(_curTableState);
			continue;
		}

		if (_curTableState == preTableState) {
			cout << "..";
			continue;
		}
		_tblHandle->showState(_curTableState);
		
		if (_curTableState == TableHandleBase::TABLE_SHUFFLING) {
			handleGameShuffling(preTableState);
		} else if (_curTableState == TableHandleBase::TABLE_WAITING_BET) {
			handleGameWaitingBet(preTableState);
		} else if (_curTableState == TableHandleBase::TABLE_SHOW_WINER) {
			handleGameShowWinner(preTableState);
			count++;
		}

		preTableState = _curTableState;
	}
}

void GamePlaying::handleGameWaitingBet(TableHandleBase::enTableState preTableState)
{
	_curStateHandler = _gameStateHandlers[GameStateHandler::GAME_WAITING_BET];
	_curStateHandler->doAction();
}

void GamePlaying::handleGameShowWinner(TableHandleBase::enTableState preTableState)
{
	short winner = _tblHandle->checkWinner();
	showWinner(winner);
	ActionParam act;
	act._type = enActParmType::WINNER_TYPE;
	act._value = (void*)&winner;
	_curStateHandler = _gameStateHandlers
						[GameStateHandler::GAME_SHOWING_WINER];
	_curStateHandler->doAction(&act);
}

void GamePlaying::handleGameShuffling(TableHandleBase::enTableState preTableState)
{
	_curStateHandler = _gameStateHandlers[GameStateHandler::GAME_SHUFFLING];
	_curStateHandler->doAction();
}

void GamePlaying::showWinner(short winner)
{
	cout << "======= RESULT [PLAYER - BANKER]========" << endl << endl;
	if (winner == 0) {
		cout << "$$$$$$$ PLAYER WIN $$$$$$$" << endl;
	} else if (winner == 1) {
		cout << "$$$$$$$ BANKER WIN $$$$$$$" << endl;
	} else if (winner == 2) {
		cout << "$$$$$$$ TIE WIN $$$$$$$" << endl;
	}
	cout << endl;
	cout <<"========================================="<< endl;
}