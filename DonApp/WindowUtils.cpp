#include <iostream>
#include <windows.h>
#include "string.h"
#include <tlhelp32.h>
#include <Windows.h>
#include <TlHelp32.h>
#include <tchar.h>
#include <Psapi.h>
#include "WindowUtils.h"

#define LOG_IMAGE_FOLDER  "E:\\Research\\Casino\\BS\\Image\\"


struct handle_data {
	unsigned long process_id;
	HWND best_handle;
};

BOOL is_main_window(HWND handle)
{   
	return GetWindow(handle, GW_OWNER) == (HWND)0 && IsWindowVisible(handle);
}


BOOL CALLBACK enum_windows_callback(HWND handle, LPARAM lParam)
{
	handle_data& data = *(handle_data*)lParam;
	unsigned long process_id = 0;
	GetWindowThreadProcessId(handle, &process_id);
	if (data.process_id != process_id || !is_main_window(handle)) {
		return TRUE;
	}

	data.best_handle = handle;
	return FALSE;   
}

BOOL CALLBACK EnumChildProc(HWND hwnd, LPARAM lParam) {
	std::cout << "hwnd_Child = " << hwnd << std::endl;

	return TRUE; // must return TRUE; If return is FALSE it stops the recursion
}

HWND find_main_window(unsigned long process_id)
{
	handle_data data;
	data.process_id = process_id;
	data.best_handle = 0;
	EnumWindows(enum_windows_callback, (LPARAM)&data);

	HWND hwnd = data.best_handle;

	return data.best_handle;
}	

HBITMAP GetScreenShot(bool isSave)
{
	int x1, y1, x2, y2, w, h;
	// get screen dimensions
	x1  = GetSystemMetrics(SM_XVIRTUALSCREEN);
	y1  = GetSystemMetrics(SM_YVIRTUALSCREEN);
	x2  = GetSystemMetrics(SM_CXVIRTUALSCREEN);
	y2  = GetSystemMetrics(SM_CYVIRTUALSCREEN);

	w   = x2 - x1;
	h   = y2;

	int xsrc = 260;
	int ysrc = 175;
	int width = 415;
	int height = 350;
	int xshift = 0;

	// copy screen to bitmap
	HDC     hScreen = GetDC(NULL);
	HDC     hDC     = CreateCompatibleDC(hScreen);
	HBITMAP hBitmap;
	if (isSave) {
		hBitmap = CreateCompatibleBitmap(hScreen, width, height);
	} else {
		hBitmap = CreateCompatibleBitmap(hScreen, w, h);
	}
	HGDIOBJ old_obj = SelectObject(hDC, hBitmap);

	if (isSave) {
		BOOL    bRet    = BitBlt(hDC, 0, 0, width, height, hScreen, xsrc+ xshift, ysrc, SRCCOPY);
	} else {
		BOOL    bRet    = BitBlt(hDC, 0, 0, w, h, hScreen, x1 + xshift, y1, SRCCOPY);
	}
	//save bitmap to clipboard
#if 0
	OpenClipboard(NULL);
	EmptyClipboard();
	SetClipboardData(CF_BITMAP, hBitmap);
	CloseClipboard();   
#endif
	/*saveBitmap(hScreen, hBitmap);*/

	// clean up
	SelectObject(hDC, old_obj);
	DeleteDC(hDC);
	ReleaseDC(NULL, hScreen);
	//DeleteObject(hBitmap);

	return hBitmap;
}

void saveScreenBitmap(const char *imgName) {

	HBITMAP hbmScreen = GetScreenShot(true);
	HDC hScreen = GetDC(NULL);

	BITMAP bmpScreen;
	// Get the BITMAP from the HBITMAP
	GetObject(hbmScreen,sizeof(BITMAP), &bmpScreen);

	BITMAPFILEHEADER   bmfHeader;    
	BITMAPINFOHEADER   bi;

	bi.biSize = sizeof(BITMAPINFOHEADER);    
	bi.biWidth = bmpScreen.bmWidth;    
	bi.biHeight = bmpScreen.bmHeight;  
	bi.biPlanes = 1;    
	bi.biBitCount = 32;    
	bi.biCompression = BI_RGB;    
	bi.biSizeImage = 0;  
	bi.biXPelsPerMeter = 0;    
	bi.biYPelsPerMeter = 0;    
	bi.biClrUsed = 0;    
	bi.biClrImportant = 0;

	DWORD dwBmpSize = ((bmpScreen.bmWidth * bi.biBitCount + 31) / 32) * 4 * bmpScreen.bmHeight;

	// Starting with 32-bit Windows, GlobalAlloc and LocalAlloc are implemented as wrapper functions that 
	// call HeapAlloc using a handle to the process's default heap. Therefore, GlobalAlloc and LocalAlloc 
	// have greater overhead than HeapAlloc.
	HANDLE hDIB = GlobalAlloc(GHND,dwBmpSize); 
	char *lpbitmap = (char *)GlobalLock(hDIB);    

	// Gets the "bits" from the bitmap and copies them into a buffer 
	// which is pointed to by lpbitmap.
	HDC hdcScreen = GetDC(NULL);
	GetDIBits(hdcScreen, hbmScreen, 0,
		(UINT)bmpScreen.bmHeight,
		lpbitmap,
		(BITMAPINFO *)&bi, DIB_RGB_COLORS);

	// A file is created, this is where we will save the screen capture.
	std::string str;
	str.append(LOG_IMAGE_FOLDER);
	str.append(imgName);
	str.append(".bmp");

	HANDLE hFile = CreateFile(LPCTSTR (str.c_str()),
		GENERIC_WRITE,
		0,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL, NULL);   

	// Add the size of the headers to the size of the bitmap to get the total file size
	DWORD dwSizeofDIB = dwBmpSize + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	//Offset to where the actual bitmap bits start.
	bmfHeader.bfOffBits = (DWORD)sizeof(BITMAPFILEHEADER) + (DWORD)sizeof(BITMAPINFOHEADER); 

	//Size of the file
	bmfHeader.bfSize = dwSizeofDIB; 

	//bfType must always be BM for Bitmaps
	bmfHeader.bfType = 0x4D42; //BM   

	DWORD dwBytesWritten = 0;
	WriteFile(hFile, (LPSTR)&bmfHeader, sizeof(BITMAPFILEHEADER), &dwBytesWritten, NULL);
	WriteFile(hFile, (LPSTR)&bi, sizeof(BITMAPINFOHEADER), &dwBytesWritten, NULL);
	WriteFile(hFile, (LPSTR)lpbitmap, dwBmpSize, &dwBytesWritten, NULL);

	//Unlock and Free the DIB from the heap
	GlobalUnlock(hDIB);    
	GlobalFree(hDIB);

	//Close the handle for the file that was created
	CloseHandle(hFile);

	//Clean up
	DeleteObject(hbmScreen);
}

HBITMAP GetScreenShot2(HWND hwnd) {
	RECT rc;
	//HWND hwnd = FindWindow(TEXT("Notepad"), NULL);    //the window can't be min
	if (hwnd == NULL)
	{
		std::cout << "it can't find any 'note' window" << std::endl;
		return 0;
	}
	GetClientRect(hwnd, &rc);
	//rc.right = 1920;
	//rc.bottom = 1080;

	//create
	HDC hdcScreen = GetDC(NULL);
	HDC hdc = CreateCompatibleDC(hdcScreen);
	HBITMAP hbmp = CreateCompatibleBitmap(hdcScreen, 
		rc.right - rc.left, rc.bottom - rc.top);
	SelectObject(hdc, hbmp);

	//Print to memory hdc
	PrintWindow(hwnd, hdc, /*PW_CLIENTONLY*/0);

#ifdef DEBUG_MODE0
	//copy to clipboard
	OpenClipboard(NULL);
	EmptyClipboard();
	SetClipboardData(CF_BITMAP, hbmp);
	CloseClipboard();
#endif
	//release
	DeleteDC(hdc);
	//DeleteObject(hbmp);
	ReleaseDC(NULL, hdcScreen);

	return hbmp;
}

int CaptureAnImage(HWND hWnd)
{
	HDC hdcScreen;
	HDC hdcWindow;
	HDC hdcMemDC = NULL;
	HBITMAP hbmScreen = NULL;
	BITMAP bmpScreen;

	// Retrieve the handle to a display device context for the client 
	// area of the window. 
	hdcScreen = GetDC(NULL);
	hdcWindow = GetDC(hWnd);

	// Create a compatible DC which is used in a BitBlt from the window DC
	hdcMemDC = CreateCompatibleDC(hdcWindow); 

	if(!hdcMemDC)
	{
		//// MessageBox(hWnd, L"CreateCompatibleDC has failed",L"Failed", MB_OK);
		goto done;
	}

	// Get the client area for size calculation
	RECT rcClient;
	GetClientRect(hWnd, &rcClient);

	//This is the best stretch mode
	SetStretchBltMode(hdcWindow,HALFTONE);

	//The source DC is the entire screen and the destination DC is the current window (HWND)
	if(!StretchBlt(hdcWindow, 
		0,0, 
		rcClient.right, rcClient.bottom, 
		hdcScreen, 
		0,0,
		GetSystemMetrics (SM_CXSCREEN),
		GetSystemMetrics (SM_CYSCREEN),
		SRCCOPY))
	{
		// MessageBox(hWnd, L"StretchBlt has failed",L"Failed", MB_OK);
		goto done;
	}

	// Create a compatible bitmap from the Window DC
	hbmScreen = CreateCompatibleBitmap(hdcWindow, rcClient.right-rcClient.left, rcClient.bottom-rcClient.top);

	if(!hbmScreen)
	{
		// MessageBox(hWnd, L"CreateCompatibleBitmap Failed",L"Failed", MB_OK);
		goto done;
	}

	// save bitmap to clipboard
#if 0
	OpenClipboard(NULL);
	EmptyClipboard();
	SetClipboardData(CF_BITMAP, hbmScreen);
	CloseClipboard();   
#endif

	// Select the compatible bitmap into the compatible memory DC.
	SelectObject(hdcMemDC,hbmScreen);

	// Bit block transfer into our compatible memory DC.
	if(!BitBlt(hdcMemDC, 
		0,0, 
		rcClient.right-rcClient.left, rcClient.bottom-rcClient.top, 
		hdcWindow, 
		0,0,
		SRCCOPY))
	{
		// MessageBox(hWnd, L"BitBlt has failed", L"Failed", MB_OK);
		goto done;
	}

	// Get the BITMAP from the HBITMAP
	GetObject(hbmScreen,sizeof(BITMAP),&bmpScreen);

	BITMAPFILEHEADER   bmfHeader;    
	BITMAPINFOHEADER   bi;

	bi.biSize = sizeof(BITMAPINFOHEADER);    
	bi.biWidth = bmpScreen.bmWidth;    
	bi.biHeight = bmpScreen.bmHeight;  
	bi.biPlanes = 1;    
	bi.biBitCount = 32;    
	bi.biCompression = BI_RGB;    
	bi.biSizeImage = 0;  
	bi.biXPelsPerMeter = 0;    
	bi.biYPelsPerMeter = 0;    
	bi.biClrUsed = 0;    
	bi.biClrImportant = 0;

	DWORD dwBmpSize = ((bmpScreen.bmWidth * bi.biBitCount + 31) / 32) * 4 * bmpScreen.bmHeight;

	// Starting with 32-bit Windows, GlobalAlloc and LocalAlloc are implemented as wrapper functions that 
	// call HeapAlloc using a handle to the process's default heap. Therefore, GlobalAlloc and LocalAlloc 
	// have greater overhead than HeapAlloc.
	HANDLE hDIB = GlobalAlloc(GHND,dwBmpSize); 
	char *lpbitmap = (char *)GlobalLock(hDIB);    

	// Gets the "bits" from the bitmap and copies them into a buffer 
	// which is pointed to by lpbitmap.
	GetDIBits(hdcWindow, hbmScreen, 0,
		(UINT)bmpScreen.bmHeight,
		lpbitmap,
		(BITMAPINFO *)&bi, DIB_RGB_COLORS);

	// A file is created, this is where we will save the screen capture.
	HANDLE hFile = CreateFile(LPCSTR("captureqwsx.bmp"),
		GENERIC_WRITE,
		0,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL, NULL);   

	// Add the size of the headers to the size of the bitmap to get the total file size
	DWORD dwSizeofDIB = dwBmpSize + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	//Offset to where the actual bitmap bits start.
	bmfHeader.bfOffBits = (DWORD)sizeof(BITMAPFILEHEADER) + (DWORD)sizeof(BITMAPINFOHEADER); 

	//Size of the file
	bmfHeader.bfSize = dwSizeofDIB; 

	//bfType must always be BM for Bitmaps
	bmfHeader.bfType = 0x4D42; //BM   

	DWORD dwBytesWritten = 0;
	WriteFile(hFile, (LPSTR)&bmfHeader, sizeof(BITMAPFILEHEADER), &dwBytesWritten, NULL);
	WriteFile(hFile, (LPSTR)&bi, sizeof(BITMAPINFOHEADER), &dwBytesWritten, NULL);
	WriteFile(hFile, (LPSTR)lpbitmap, dwBmpSize, &dwBytesWritten, NULL);

	//Unlock and Free the DIB from the heap
	GlobalUnlock(hDIB);    
	GlobalFree(hDIB);

	//Close the handle for the file that was created
	CloseHandle(hFile);

	//Clean up
done:
	DeleteObject(hbmScreen);
	DeleteObject(hdcMemDC);
	ReleaseDC(NULL,hdcScreen);
	ReleaseDC(hWnd,hdcWindow);

	return 0;
}

DWORD GetProcessByName(char *name)
{
	DWORD pid = 0;

	// Create toolhelp snapshot.
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 process;
	ZeroMemory(&process, sizeof(process));
	process.dwSize = sizeof(process);
	int count = 0;
	// Walkthrough all processes.
	if (Process32First(snapshot, &process))
	{
		do
		{
			// Compare process.szExeFile based on format of name, i.e., trim file path
			// trim .exe if necessary, etc.
			if (_stricmp(process.szExeFile, name) == 0)
			{		
				pid = process.th32ProcessID;
				break;
			/*	count ++;
				if (count == 4)
					break;*/
			}
		} while (Process32Next(snapshot, &process));
	}

	CloseHandle(snapshot);

	if (pid != 0)
	{
		OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
		return pid;
	}

	// Not found


	return NULL;
}

BYTE *GetImageData(HBITMAP &hBmp) {
	//getting the size of the picture
	BITMAP bm;
	GetObject(hBmp, sizeof(bm), &bm);
	int width(bm.bmWidth),
		height(bm.bmHeight);

	//creating a bitmapheader for getting the dibits
	BITMAPINFOHEADER bminfoheader;
	::ZeroMemory(&bminfoheader, sizeof(BITMAPINFOHEADER));
	bminfoheader.biSize        = sizeof(BITMAPINFOHEADER);
	bminfoheader.biWidth       = width;
	bminfoheader.biHeight      = -height;
	bminfoheader.biPlanes      = 1;
	bminfoheader.biBitCount    = 32;
	bminfoheader.biCompression = BI_RGB;

	bminfoheader.biSizeImage = width * 4 * height;
	bminfoheader.biClrUsed = 0;
	bminfoheader.biClrImportant = 0;

	//create a buffer and let the GetDIBits fill in the buffer
	BYTE* pPixels = new BYTE[(width * 4 * height)];
	HDC hDC = CreateCompatibleDC(0);
	if( !GetDIBits(hDC, hBmp, 0, height, pPixels, (BITMAPINFO*) &bminfoheader, DIB_RGB_COLORS)) // load pixel info 
	{ 
		//return if fails but first delete the resources
		DeleteObject(hBmp);
		delete [] pPixels; // delete the array of objects

		return false;
	}

	/*int x, y; // fill the x and y coordinate
	x = 310, y = 360;
	unsigned char r = pPixels[(width*y+x) * 4 + 2];
	unsigned char g = pPixels[(width*y+x) * 4 + 1];
	unsigned char b = pPixels[(width*y+x) * 4 + 0];*/ 

	//clean up the bitmap and buffer unless you still need it
	DeleteDC(hDC);
	DeleteObject(hBmp);
	return pPixels;
}

LRESULT CALLBACK LowLevelMouseProc(int nCode, WPARAM wParam, LPARAM lParam)
{

	if(wParam == WM_LBUTTONDOWN)

	{

		MSLLHOOKSTRUCT* mouseInfo = (MSLLHOOKSTRUCT*)lParam;

		HWND h = GetForegroundWindow();



		SetCapture(h);

		PostMessage(h, WM_LBUTTONDOWN, 0, MAKELPARAM(mouseInfo->pt.x, mouseInfo->pt.y));

		Sleep(5);

		PostMessage(h, WM_LBUTTONUP, 0, MAKELPARAM(mouseInfo->pt.x, mouseInfo->pt.y));

		ReleaseCapture();

		//Prevent the real mouseclick from being processed

		return 1;

	}



	return CallNextHookEx(NULL, nCode, wParam, lParam);

}

bool showInactiveWnd(HWND hwnd)
{
	Sleep(200);
	ANIMATIONINFO ai = {0};
	bool restoreAnimation = false;
	SystemParametersInfo(SPI_GETANIMATION, sizeof(ANIMATIONINFO), &ai, 0);

	if (ai.iMinAnimate != 0)
	{
		ai.iMinAnimate = 0;
		SystemParametersInfo(SPI_SETANIMATION, sizeof(ANIMATIONINFO), &ai, 0);
		restoreAnimation = true;
	}

	// optionally move the window off-screen, or
	// apply alpha using SetLayeredWindowAttributes()...

	COLORREF RRR = RGB(255, 0, 255);
	SetWindowLong(hwnd, GWL_EXSTYLE, GetWindowLong(hwnd, GWL_EXSTYLE) | WS_EX_LAYERED);
	SetLayeredWindowAttributes(hwnd, RRR, (BYTE)0, LWA_ALPHA);
	ShowWindow(hwnd, SW_SHOWNOACTIVATE);

	return restoreAnimation;
}

void rePlacementWnd(HWND hwnd, WINDOWPLACEMENT wp, bool restoreAnimation) {
	ANIMATIONINFO ai = {0};
	//ShowWindow(hwnd, SW_HIDE);
	//wp.showCmd = SW_HIDE;
	SetWindowPlacement(hwnd, &wp);
	// optionally remove alpha using SetLayeredWindowAttributes()...
	COLORREF RRR = RGB(255, 0, 255);
	SetWindowLong(hwnd, GWL_EXSTYLE, GetWindowLong(hwnd, GWL_EXSTYLE) | WS_EX_LAYERED);
	SetLayeredWindowAttributes(hwnd, RRR, (BYTE)255, LWA_ALPHA);
	if (restoreAnimation)
	{
		ai.iMinAnimate = 1;
		SystemParametersInfo(SPI_SETANIMATION, sizeof(ANIMATIONINFO), &ai, 0);
	}
}

void click (ScreenCoord &scrCoord, HWND hwnd) {
	static int count = 0;
	int xcoord = scrCoord._x ;
	WORD xc = scrCoord._x;
	WORD yc = scrCoord._y;
	WORD x = (65536 / (1920)) * xcoord; //convert to absolute coordinates
	WORD y = (65536 / 1080) * scrCoord._y;

	WINDOWPLACEMENT wp = {0};
	wp.length = sizeof(WINDOWPLACEMENT);
	GetWindowPlacement(hwnd, &wp);

	ANIMATIONINFO ai = {0};
	bool restoreAnimation = false;
	if (wp.showCmd == SW_SHOWMINIMIZED ||
		SW_HIDE == wp.showCmd)
	{
		restoreAnimation = showInactiveWnd(hwnd);
	}
		
	PostMessageA(hwnd, WM_LBUTTONDOWN, 0, MAKELPARAM(xc, yc));

	Sleep(5);

	PostMessageA(hwnd, WM_LBUTTONUP, 0, MAKELPARAM(xc, yc));

	if (wp.showCmd == SW_SHOWMINIMIZED ||
		SW_HIDE == wp.showCmd)
	{
		rePlacementWnd(hwnd, wp);
	}

	// move mouse to prevent sleep
	count++;
	if (count == 50) {
		count = 0;
		// move mouse for active event
		mouse_event(MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE, x, y, 0, 0);
	}
}