#ifndef __GL_CONTROL_WIDGET_H__
#define __GL_CONTROL_WIDGET_H__

class GLView;

class GLControlWidget {
public:
	GLControlWidget(GLView *container, GLControlWidget *parent);
	virtual ~GLControlWidget();
	virtual void render() = 0;
	virtual void onFocused() = 0;
	virtual void onMouseMove() = 0;
	virtual void onMousePressed() = 0;
	virtual void onMouseReleased() = 0;

	void getPos(double &x, double &y) { x = _xpos, y = _ypos;}
	double getXPos() {return _xpos;}
	double getYPos() {return _ypos;}
	double getWidth() { return _width;}
	double getHeight() { return _height;};
	void setXPos(double xpos) { _xpos = xpos;}
	void setYPos(double ypos) { _ypos = ypos;}
	void setWidth(double width) {  _width = width;}
	void setHeight(double height) {  _height = height;}
	void setBoundingRect(double top, double left, double width, double height);
	double calcTop(); // for render in glview
	double calcLeft(); // for render in glview
	void drawRect();

protected:
	GLControlWidget *_parent;
	GLView *_glview; // belong to container glview

	double _xpos; // x coord of top left
	double _ypos; // y coord of top left
	double _width;
	double _height;
};

#endif