#ifndef __GL_GRAPH_H__
#define __GL_GRAPH_H__

#include "GLView.h"

struct GraphConfig 
{
	unsigned short _xInterval;
	unsigned short _yInterval;
	float _axisColor[3];
	float _pointColor[3];
	float _dataLineColor[3];
	QFont _font;
};

class GLGraph: public GLView
{
public:
	
public:
	GLGraph();
	~GLGraph();
	virtual void paintGL();
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseReleaseEvent(QMouseEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void wheelEvent(QWheelEvent *event);

	void setXInterval(unsigned short x) { _graphConfig._xInterval = x;}
	GraphConfig *getGraphConfig() { return &_graphConfig; }
	void drawGraph();
	void drawAxisesWithNumber();
	void drawXAxis();
	void drawYAxis();
	void drawOptionButton();
	void drawSelectedObj();
	void drawMeasure();
	void highlightObject(short i);
	
	void setGraphData(const std::vector<float> &list) { _data = list;}
	void setAutoMove(bool automove) { _autoMove = automove; }
	bool autoMove() { return _autoMove; }
	void drawNetSquares();
	int selectObject();
	void addNewData(float data);
	void moveToLast();
	void clearData();
	double glRightMostWnd();
	double glLeftMostWnd();
	double glTopMostWnd();
	double glBottomMostWnd();

private:
	GraphConfig _graphConfig;
	std::vector<float> _data; // data of y axis
	double _startX; // start value of x axis or leftmost node
	double _startY; // start value of y axis or bottom node
	int _selectedObj;
	int _measureObj1; // first selected object of measure
	int _measureObj2; // second selected object of measure
	bool _autoMove; // auto move to last round/games
};
#endif