#include "TableHandleV9.h"
#include "Utils.h"
#include "WndCapture.h"

TableHandleV9::TableHandleV9(): TableHandleBase()
{
	//config(); // TODO: move for multiple tables
}

TableHandleV9::~TableHandleV9()
{
}

void TableHandleV9::config()
{
	ScreenCoord start = ScreenCoord(304, 238);
	_tblconfig._topleftPoint = start;

	if (1)
	{
		_tblconfig._topleftPoint = findTopleftTbl();
	}
	
	switch(_tableNumber)
	{
		case 1:
			_tblconfig._topleftPoint._x = start._x + 1082;
			break;
		case 2:
			_tblconfig._topleftPoint._y += 215;
			break;
		case 3:
			_tblconfig._topleftPoint._x = start._x + 1082;
			_tblconfig._topleftPoint._y += 215;
			break;
		case 4:
			_tblconfig._topleftPoint._y += 215 * 2;
			break;
		case 5:
			_tblconfig._topleftPoint._x = start._x + 1082;
			_tblconfig._topleftPoint._y += 215 * 2;
			break;
		case 6:
			break;
		default:
			break;
	}
	
	shiftAlongTopleft();
}

void TableHandleV9::shiftAlongTopleft()
{
	ScreenCoord start					= _tblconfig._topleftPoint;
	_tblconfig._coordBetStatus			= start + ScreenCoord(160, 180);
	_tblconfig._coordTableStatus		= start + ScreenCoord(90, 150);
	_tblconfig._coordSmallCard			= start + ScreenCoord(0,0);
	_tblconfig._coordBigCard1			= start + ScreenCoord(0,0);
	_tblconfig._coordBigCard2			= start + ScreenCoord(0,0);
	_tblconfig._coordPlayerWin			= start + ScreenCoord(30, 173);
	_tblconfig._coordBankerWin			= start + ScreenCoord(150, 173);
	_tblconfig._coordTieWin				= start + ScreenCoord(90, 173);

	_tblconfig._coordMoney100			= start + ScreenCoord(855, 775);
	_tblconfig._coordMoney20			= start + ScreenCoord(805,775);
	_tblconfig._coordBigBet				= start + ScreenCoord(0, 0);
	_tblconfig._coordSmallBet			= start + ScreenCoord(0, 0);
	_tblconfig._coordBetConfirm			= start + ScreenCoord(35, 190);
}

bool TableHandleV9::isEmptyAtRound0()
{
	int rgb[3];
	ScreenCoord round0Coord;
	round0Coord._x = _tblconfig._topleftPoint._x + 8;
	round0Coord._y = _tblconfig._topleftPoint._y + 40;
	DonApp::Utils::getRGBAt(round0Coord, rgb);

	if (isBankerColor(rgb) || isPlayerColor(rgb)) {
		return false;
	}

	return true;
}

bool TableHandleV9::isBankerColor(int *rgb)
{
	int bankerColor[3] = {255,12,11};
	return DonApp::Utils::isTwoEqualColor(bankerColor, rgb);
}

bool TableHandleV9::isPlayerColor(int *rgb)
{
	int playerColor[3] = {13,13,254};
	return DonApp::Utils::isTwoEqualColor(playerColor, rgb);
}

bool TableHandleV9::isTieColor(int *rgb)
{
	int tieColor[3] = {0,166,81};
	return DonApp::Utils::isTwoEqualColor(tieColor, rgb);
}

ScreenCoord TableHandleV9::findTopleftTbl()
{
	int width = WndCapture::getInstance().getWndImgWidth();
	int height = WndCapture::getInstance().getWndImgHeight();
	int rgb[3];
	int topleftColor[3] = {160,147,93};

	for (unsigned j = 0; j < height; j++)
		for (unsigned i = 0; i < width; ++i)
		{
			DonApp::Utils::getRGBAt(ScreenCoord(i, j), rgb);
			if (DonApp::Utils::isTwoEqualColor(rgb, topleftColor, 0))
			{
				return ScreenCoord(i, j);
			}
		}

	return ScreenCoord(0,0);
}

int TableHandleV9::checkWinner()
{
	int rgb[3];
	int winColor[3] = {255, 255, 255};

	// player win
	DonApp::Utils::getRGBAt(_tblconfig._coordPlayerWin, rgb);
	if (DonApp::Utils::isTwoEqualColor(rgb, winColor, 10))
	{
		return 0;
	}

	// banker win
	DonApp::Utils::getRGBAt(_tblconfig._coordBankerWin, rgb);
	if (DonApp::Utils::isTwoEqualColor(rgb, winColor, 10)) {
		return 1;
	}

	// tie win
	DonApp::Utils::getRGBAt(_tblconfig._coordTieWin, rgb);
	if (DonApp::Utils::isTwoEqualColor(rgb, winColor, 10)) {
		return 2;
	}

	return -1;
}

TableHandleBase::enTableState TableHandleV9::checkTableState()
{
	int waitbetcolor[3] = {169,138,53};
	int blackColor[3] = {0, 0, 0}; // for shuffling
	int rgb[3];

	int res = DonApp::Utils::getRGBAt(_tblconfig._coordBetStatus, rgb);

	if (DonApp::Utils::isTwoEqualColor(waitbetcolor, rgb, 10)) 
	{  // Wait bet
		return TABLE_WAITING_BET;
	}
	else 
	{  // Stop Bet
		int winner = checkWinner();
		DonApp::Utils::getRGBAt(_tblconfig._coordTableStatus, rgb);
#if 0
		if (DonApp::Utils::isTwoEqualColor(blackColor, rgb) &&
			(winner != -1))  {  // Shuffling
				return TABLE_SHUFFLING;
		} else {
#endif
			if (winner != -1) { 
				return TABLE_SHOW_WINER;
			} else {
				return TABLE_OPEN_CARD;
			}
//		}
	}
}

bool TableHandleV9::checkBigResult()
{
	return false;
}

void TableHandleV9::doBet(int nlost)
{

}