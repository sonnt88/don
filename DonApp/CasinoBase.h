#ifndef __CASINO_BASE_H__
#define __CASINO_BASE_H__

#include "WindowUtils.h"
#include "WndCapture.h"
#include "GamePlaying.h"
#include <process.h>
#include <vector>

class TableHandleBase;
class GamePlaying;
class GameController;
class CasinoObserver;

struct CasinoConfig {
	ScreenCoord _coordTbl1_Board;
	ScreenCoord _coordTbl2_Board;
	ScreenCoord _coordTbl3_Board;
	ScreenCoord _coordTbl4_Board;
	ScreenCoord _coordTbl5_Board;
	ScreenCoord _coordTbl6_Board;
	
	ScreenCoord _AllTableBoard[7];
	ScreenCoord _coordEnterTbl[7];
};

struct MonitorTableParam {
	GameController *_gameCtrler;
	short _currentState;
	short _currentRound;
};

struct TablePlayingHandle{
	GamePlaying *_gamePlaying;
	HANDLE _threadHdl;
};

class CasinoBase {
public:
	enum enCasinoID {
		CASINO_AG = 0,
		CASINO_V9
	};

	enum enCasinoState {
		UNKNOWN_STATE = 0,
		LOBBY_STATE,
		SINGLE_TABLE_STATE,
		MULTI_TABLE_STATE,
		DISCONNECTED_STATE
	};

public:
	CasinoBase();
	virtual ~CasinoBase();
	virtual enCasinoState checkStateOfCasino() = 0;
	virtual void config() = 0;
	virtual enCasinoID getID() = 0;
	virtual void playTable(TableHandleBase *, GameController *);
	virtual void monitorLobby(CasinoObserver *observer) {};

protected:
	CasinoConfig _config;
	std::vector<GamePlaying *> _gamePlayings; // games are playing

private:

};

#endif