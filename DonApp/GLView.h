#ifndef __GLVIEW_H__
#define __GLVIEW_H__

#include <QGLWidget>
#include <qtimer.h>

#define AXIS_LENGTH 30.0f

class QWheelEvent;

class GLView: public QGLWidget
{
	Q_OBJECT

public:
	GLView(QWidget *parent = 0);
	~GLView();

	QSize minimumSizeHint() const;
	QSize sizeHint() const;
	void viewOrtho();
	//void setRefControl(KevvoxTesting *ref);
	void viewCenter();
	void glPointAt(GLdouble glPoint[3], double winx, double winy, double winz = 0);
	void winPointAt(GLdouble glpoint[3], double win[2]);
	void calcViewCenter(double mx, double my, double prefactor);
	double getCenterX() { return centerX;}
	double getCenterY() { return centerY;}
	double getZoomFactor() { return m_factor; }
	void setZoomFactor(double factor) { m_factor = factor;}
	public slots:
		void setXRotation(int angle);
		void setYRotation(int angle);
		void setZRotation(int angle);
		void animate();

signals:
		void xRotationChanged(int angle);
		void yRotationChanged(int angle);
		void zRotationChanged(int angle);

protected:
	void initializeGL();
	virtual void paintGL();
	void resizeGL(int width, int height);
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void wheelEvent(QWheelEvent *event);
	void drawAxises();
	
protected:
	double xRot;
	double yRot;
	double zRot;
	float m_factor;
	double xTranslate;
	double yTranslate;
	double centerX;
	double centerY;

	QPointF lastPos;
	QColor qtGreen;
	QColor qtPurple;
	QTimer _animationTimer;
#ifdef DEBUG_MODE
	GLdouble _dbgPoint[3];
#endif
};

#endif