#ifndef __LOCK_H__
#define __LOCK_H__

#include "Windows.h"
extern CRITICAL_SECTION g_wndCaptureCritical;
extern CRITICAL_SECTION g_RecordIOCritical;
extern CRITICAL_SECTION g_SimulationCritical;

class Lock {
public:
	Lock(CRITICAL_SECTION &critical) {
		EnterCriticalSection(&critical);
		_critical = &critical;
	}

	~Lock() {
		LeaveCriticalSection(_critical);
	}
private:
	CRITICAL_SECTION *_critical;
};

class RefCntLock {
    private:
        static int count;
        static Lock* lock;

    public:
        RefCntLock(CRITICAL_SECTION& mutex) {

             // probably want to check that the mutex matches prior instances.
             if( !lock ) {
                  lock = new Lock(mutex);
                  count++;
             }
        }
        ~RefCntLock() {
             --count;
             if( count == 0 ) {
                 delete lock;
                 lock = NULL;
             }
        }
}; 
#endif