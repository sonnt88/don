#include "Utils.h"
#include "WndCapture.h"
#include "GamePlaying.h"

namespace DonApp {
  namespace Utils {
	bool isCloseToWithColor(int red, int green, int blue, int tolerance)
	{
		int sum = red + green + blue;
		return (255 * 3 - sum) < tolerance;
	}

	bool isTwoEqualColor(int rgb1[3], int rgb2[3], int tolerance)
	{
		if (labs(rgb1[0] - rgb2[0]) > tolerance ||
			labs(rgb1[1] - rgb2[1]) > tolerance ||
			labs(rgb1[2] - rgb2[2]) > tolerance
			)
		{
			return false;
		}

		return true;
	}

	int getRGBAt(ScreenCoord scrcoord, int rgb[3]) {
		//Guard for multithread accessing
		Lock lock(g_wndCaptureCritical);

		int width = WndCapture::getInstance().getWndImgWidth();
		int height = WndCapture::getInstance().getWndImgHeight();
		
		BYTE *pixelsColor = WndCapture::getInstance().getPixelsColor();
		
		int x = scrcoord._x;
		int y = scrcoord._y;
		if (x > width || y > height || NULL == pixelsColor) {
			return 0;
		}

		rgb[0] = pixelsColor[(width*y+x) * 4 + 2];
		rgb[1] = pixelsColor[(width*y+x) * 4 + 1];
		rgb[2] = pixelsColor[(width*y+x) * 4 + 0];

		return 1;
	}

	bool isReachColor(int rgb[3]) {
		int bankerColor[3] = {196, 45, 42};
		int playerColor[3] = {55, 53, 243};
		int tieColor[3] = {54, 155, 3};

		if (isTwoEqualColor(rgb, bankerColor) ||
			isTwoEqualColor(rgb, playerColor) ||
			isTwoEqualColor(rgb, tieColor)) {
				return true;
		}

		return false;
	}

	unsigned int __stdcall playTableThreadFunc(void *lparam) {
		if (NULL == lparam) {
			return 0;
		}

		GamePlaying *gamePlaying = static_cast<GamePlaying *>(lparam);
		gamePlaying->play();

		return 0;
	}

	void PrintRGB(int r, int g, int b) {
		std::cout << "R:" << (int)r 
				  << ", G:" << (int)g 
				  << ", B:" << (int)b << std::endl;
	}
  }
}