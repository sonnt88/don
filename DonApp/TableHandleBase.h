#ifndef __TABLE_HANDLE_BASE_H__
#define __TABLE_HANDLE_BASE_H__

#include "WindowUtils.h"

struct TableConfig {
	ScreenCoord _coordBetStatus;
	ScreenCoord _coordTableStatus;
	ScreenCoord _coordSmallCard;
	ScreenCoord _coordBigCard1;
	ScreenCoord _coordBigCard2;
	ScreenCoord _coordPlayerWin;
	ScreenCoord _coordBankerWin;
	ScreenCoord _coordTieWin;
	ScreenCoord _coordMoney100; 
	ScreenCoord _coordMoney20;
	ScreenCoord _coordBigBet;
	ScreenCoord _coordSmallBet;
	ScreenCoord _coordBetConfirm;
	ScreenCoord _topleftPoint;
};

class TableHandleBase {
public:
	enum enTableState {  // status of a game/table
		TABLE_INVALID = -1,
		TABLE_WAITING_BET,
		TABLE_SHUFFLING,
		TABLE_OPEN_CARD,
		TABLE_SHOW_WINER
	};

public:
	TableHandleBase();
	virtual ~TableHandleBase();

	virtual void config() = 0;
	virtual int checkWinner();
	virtual enTableState checkTableState();
	virtual bool checkBigResult();
	virtual void doBet(int nlost);
	
	void showState(enTableState curState);
	void setTableNumber(short tblNumber);
	short getTableNumber();
	void setTopleftPoint(const ScreenCoord &point);

protected:
	TableConfig _tblconfig;
	short _tableNumber;

private:
};

#endif