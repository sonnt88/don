#ifndef __CASINO_OBSERVER_H__
#define __CASINO_OBSERVER_H__
#include "qstring.h"

class CasinoObserver
{
public:
	virtual void onWonMoneyChanged(float newWonMoney) = 0;
	virtual void onInformationChanged(const QString &txt) = 0;
private:
};
#endif