#include "TableHandleBase.h"
#include <iostream>

TableHandleBase::TableHandleBase() 
{
}

TableHandleBase::~TableHandleBase() 
{
}
void TableHandleBase::setTableNumber(short tblNumber)
{
	_tableNumber = tblNumber;
}

short TableHandleBase::getTableNumber()
{
	return _tableNumber;
}

void TableHandleBase::setTopleftPoint(const ScreenCoord &point)
{
	_tblconfig._topleftPoint = point;
}

void TableHandleBase::showState(enTableState curState)
{
	std::cout << std::endl;
	switch (curState) {
	case TABLE_WAITING_BET:
		std::cout << " --> Waiting for bet: ";
		break;
	case TABLE_SHUFFLING:
		std::cout <<" --> SHUFFLING ";
		break;
	case TABLE_SHOW_WINER:
		std::cout <<" --> SHOW WINNER ";
		break;
	case TABLE_OPEN_CARD:
		std::cout <<" --> OPEN CARD ";
		break;
	case TABLE_INVALID:
		std::cout << " --> Not on any table";
		break;
	}

	std::cout << "\n";
}

int TableHandleBase::checkWinner()
{
	return 0;
}

TableHandleBase::enTableState TableHandleBase::checkTableState()
{
	return TABLE_INVALID;
}

bool TableHandleBase::checkBigResult()
{
	return false;
}

void TableHandleBase::doBet(int nlost)
{
}