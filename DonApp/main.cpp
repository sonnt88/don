#if 1
#include <iostream>
#include <windows.h>
#include "string.h"
#include <tlhelp32.h>
#include <Windows.h>
#include <TlHelp32.h>
#include <tchar.h>
#include <Psapi.h>
#include <cstring>
#include <string>
#include <fstream>
#include <sstream>
#include <ctime>
#include "GameController.h"
#include "GameStatistic.h"
#include "BAStrategy.h"
#include "KNNStrategy.h"
#include "NaiveBayesStrategy.h"
#include "BASMoneyManagement.h"
#include "RecordIO.h"
#include "DonKnowledge.h"
#include "WindowUtils.h"
#include "GameSimulation.h"
#include "NetConnect.h"
#include "WindowUtils.h"
#include "WndCapture.h"
#include "CasinoAG.h"
#include "CasinoBase.h"
#include "TableHandleBase.h"
#include "TableHandleAG.h"
#include "TableHandleV9.h"
#include "CasinoV9.h"
#include "GamePlaying.h"
#include <process.h>
#include <QtGui/QApplication>
#include "GLGraph.h"
#include "qmainwindow.h"
#include "qgraphicsview.h"
#include "DonMainWindow.h"

using namespace std;
#define DEFAULT_WIDTH 1858
#define DEFAULT_HEIGHT 1088

#define H_KEY 0x48
#define R_KEY 0x52
#define S_KEY 0x53
LONG WEB_TITLE_WND_STYLE = (WS_BORDER | WS_CAPTION| WS_THICKFRAME | WS_SYSMENU | WS_MAXIMIZEBOX | WS_MINIMIZEBOX);

ofstream g_logfile;
ofstream g_logfilePB;
ofstream g_logfileResult;

HWND g_mainWnd;
short g_keypressed;
HHOOK g_keyhook;
ostringstream g_fname;
ostringstream g_fnamePB; // for player banker outcome

const std::string currentDateTime() {
	time_t now = time(0);
	struct tm tstruct;
	char buf[80];
	tstruct = *localtime(&now);
	strftime(buf,sizeof(buf), "%Y-%m-%d.%X", &tstruct);

	string datestr = buf;
	char c = ':';
	char replace = '.';
	auto it = datestr.find(":");
	while(it != string::npos) {
		datestr.replace(it, 1, ".");
		it = datestr.find(":");
	}

	return datestr;
}

void updateLogFile() {
#ifdef LOG_BIG_SMALL
	g_logfile.close();
	g_logfile.open(g_fname.str(), ios::out | ios::app);
	if (g_logfile.is_open() == false) {
		int bug;
	}
#endif

	g_logfilePB.close();
	g_logfilePB.open(g_fnamePB.str(), ios::out | ios::app);
	if (g_logfilePB.is_open() == false) {
		assert("failed to open file");
	}
}

void openLogResult() {
	string datestr = currentDateTime();
	char c = ':';
	char replace = '.';
	auto it = datestr.find(":");
	while(it != string::npos) {
		datestr.replace(it, 1, ".");
		it = datestr.find(":");
	}
#ifdef LOG_BIG_SMALL
	g_fname.str("");
	g_fname.clear();
	g_fname << "E:\\Research\\Casino\\BS\\OU_"<<datestr;
	
	g_logfile.open(g_fname.str(), ios::out | ios::app);
	if (g_logfile.is_open() == false)
	{
		int bug = 0;
	}
#endif

	g_fnamePB.str("");
	g_fnamePB.clear();
	g_fnamePB << "E:\\Research\\Casino\\PB\\shoeBP_"<<datestr;

	g_logfilePB.open(g_fnamePB.str(), ios::out | ios::app);
	if (g_logfilePB.is_open() == false)
	{
		int bug = 0;
	}
}

/**
 *
 *  wParam, one of the: WM_KEYDOWN, WM_KEYUP, WM_SYSKEYDOWN, or WM_SYSKEYUP
    lParam: pointer to a KBDLLHOOKSTRUCT structure

    (*) "The hook procedure should process a message in less time than the
    data entry specified in the LowLevelHooksTimeout value in the following registry key: 
    HKEY_CURRENT_USER\Control Panel\Desktop 

    The value is in milliseconds. If the hook procedure does not 
    return during this interval, the system will pass the message to the next hook."

 *
 */
LRESULT CALLBACK
hook_proc( int code, WPARAM wParam, LPARAM lParam )
{
  static long ctrl_cnt = 0;
  static bool mmode = false;
  static DWORD time;

  KBDLLHOOKSTRUCT*  kbd = (KBDLLHOOKSTRUCT*)lParam;

  if (  code < 0
  ||   (kbd->flags & 0x10) // ignore injected events
     ) return CallNextHookEx( g_keyhook, code, wParam, lParam );

  long ret = 1; // by default I swallow the keys
  if (  mmode  ) { // macro mode is ON
    if (  WM_KEYDOWN == wParam  )
      //PostMessage(mainwnd, WM_MCR_ACCUM, kbd->vkCode, 0);
	  1;

    if (  WM_KEYUP == wParam  ) {
		g_keypressed = (SHORT)kbd->vkCode;
		switch (kbd->vkCode) {
		case VK_ESCAPE:
			mmode = false;
			exit(0);
			break;
		case H_KEY:
			break;
		case VK_RETURN:
			/* PostMessage(mainwnd, WM_MCR_EXEC, 0, 0);*/
			break;

		case VK_LCONTROL:
			mmode = false;
			break;
		}

	  
	}

    /* Which non printable keys allow passing? */
    switch( kbd->vkCode ) {
      case VK_LCONTROL:
      case VK_CAPITAL:
      case VK_LSHIFT:
      case VK_RSHIFT:
        ret = CallNextHookEx( g_keyhook, code, wParam, lParam );
    }
  }
  else { // macro mode is OFF
    /* Ctrl pressed */
    if (  kbd->vkCode == VK_LCONTROL && WM_KEYDOWN == wParam  ) {
      ctrl_cnt = 1;
      time = kbd->time;
    }

    /* Prevent ctrl combinations to activate macro mode */
    if (  kbd->vkCode != VK_LCONTROL  )
      ctrl_cnt = 0;

    /* Ctrl released */
    if (  ctrl_cnt == 1 && WM_KEYUP == wParam  ) {
      if (  kbd->time - time > 40  ) {
        mmode = true;
      }
    }

    ret = CallNextHookEx( g_keyhook, code, wParam, lParam ); // let it pass
  }

  return ret;
}

unsigned int __stdcall keyPressed(void*) {
	HWND webHwnd = WndCapture::getInstance().getWebWnd();
	while (true) {
		Sleep(50);
		// Fetch tab key state.
		SHORT spaceKeyState = GetAsyncKeyState( VK_SPACE );
		SHORT esc = GetAsyncKeyState( VK_ESCAPE );
		GetKeyState(VK_ESCAPE);
		if (GetForegroundWindow() != g_mainWnd &&
			GetForegroundWindow() != webHwnd)
			continue;
		// Test high bit - if set, key was down when GetAsyncKeyState was called.
		if( ( 1 << 16 ) & spaceKeyState )
		{
			// space key down... 
 			cout << "pressed Space key \n";
			//return;
		}

		if ((1 << 16) & esc) {
			LONG style = GetWindowLong(webHwnd, GWL_STYLE);
			style |= WEB_TITLE_WND_STYLE;
			SetWindowLong(webHwnd, GWL_STYLE, style);

			COLORREF RRR = RGB(255, 0, 255);
			SetWindowLong(webHwnd, GWL_EXSTYLE, GetWindowLong(webHwnd, GWL_EXSTYLE) | WS_EX_LAYERED);
			SetLayeredWindowAttributes(webHwnd, RRR, (BYTE)255, LWA_ALPHA);

			cout << "Exit program";
			Sleep(1000);
			exit(0);
		}

		if ((1 << 16) & GetAsyncKeyState(H_KEY)) {
			g_keypressed = H_KEY;
			COLORREF RRR = RGB(255, 0, 255);
			SetWindowLong(webHwnd, GWL_EXSTYLE, GetWindowLong(webHwnd, GWL_EXSTYLE) | WS_EX_LAYERED);
			SetLayeredWindowAttributes(webHwnd, RRR, (BYTE)0, LWA_ALPHA);
		}

		if ((1 << 16) & GetAsyncKeyState(S_KEY)) {
			g_keypressed = S_KEY;
			COLORREF RRR = RGB(255, 0, 255);
			SetWindowLong(webHwnd, GWL_EXSTYLE, GetWindowLong(webHwnd, GWL_EXSTYLE) | WS_EX_LAYERED);
			SetLayeredWindowAttributes(webHwnd, RRR, (BYTE)255, LWA_ALPHA);
		}
	}
}

unsigned int __stdcall captureWndThreadFunc(void *) {
	while (true) {
		Sleep(1000);
		WndCapture::getInstance().capture();
	}
}

void setOnTop()
{
	// GetConsoleWindow() => returns:
	// "handle to the window used by the console
	// associated with the calling process
	// or NULL
	// if there is no such associated console."
	g_mainWnd = GetConsoleWindow();

	// 
	if( g_mainWnd ){
		cout << endl << "Setting up associated console window ON TOP !";
		SetWindowPos(
			g_mainWnd, // window handle
			HWND_TOPMOST, // "handle to the window to precede
			// the positioned window in the Z order
			// OR one of the following:"
			// HWND_BOTTOM or HWND_NOTOPMOST or HWND_TOP or HWND_TOPMOST
			670, 10, // X, Y position of the window (in client coordinates)
			320, 370, // cx, cy => width & height of the window in pixels
			SWP_DRAWFRAME | SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW // The window sizing and positioning flags.
			);

		// OPTIONAL ! - SET WINDOW'S "SHOW STATE"
		ShowWindow(
			g_mainWnd, // window handle
			SW_NORMAL // how the window is to be shown
			// SW_NORMAL => "Activates and displays a window.
			// If the window is minimized or maximized,
			// the system restores it to its original size and position.
			// An application should specify this flag
			// when displaying the window for the first time."
			);
		cout << endl << "Done.";
	} else {
		cout << endl << "There is no console window associated with this app :(";
	}
}

void startup()
{
	DWORD pid = GetProcessByName("chrome.exe");
	HWND hWnd = find_main_window(pid);
#if 0
	WINDOWPLACEMENT wp = {0};
	wp.length = sizeof(WINDOWPLACEMENT);
	GetWindowPlacement(hWnd, &wp);
	if (wp.showCmd == SW_SHOWMAXIMIZED) {
		ShowWindow(hWnd, SW_SHOW);
	}
	wp.rcNormalPosition.left = 0;
	wp.rcNormalPosition.right = 1368 + 6;
	wp.rcNormalPosition.top = 0;
	wp.rcNormalPosition.bottom = 735;
	wp.showCmd = SW_SHOWNA;
	SetWindowPlacement(hWnd, &wp);
	//SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, /*1874*/1000, /*1096*/50, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW); 
#endif
	//showInactiveWnd(hWnd);
	RECT rect;
	GetWindowRect(hWnd, &rect);
	SetWindowPos(hWnd, HWND_BOTTOM, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_NOACTIVATE);

	LONG style = GetWindowLong(hWnd, GWL_STYLE);
	style &= ~WEB_TITLE_WND_STYLE;
	//style = ~(WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX);
	SetWindowLong(hWnd, GWL_STYLE, style);
	ShowWindow(hWnd, SW_SHOWNA);

	setOnTop();

	HANDLE keyPressedHandle, wndCapturehandle;
	WndCapture::getInstance().setWebWnd(hWnd);
	keyPressedHandle = (HANDLE)_beginthreadex(0, 0, &keyPressed, (void*)0, 0, 0);
	wndCapturehandle = (HANDLE)_beginthreadex(0, 0, &captureWndThreadFunc, (void*)0, 0, 0);

	Sleep(2000); // make sure captured wnd 
}

#define NETWORK_MODE 0
#define SIMULATION
#define QT_APP
extern void geneticAlgorithm(int targetNumber);

int main(int argc, char * argv[])
{
	// Initialize critical sections
	InitializeCriticalSection(&g_wndCaptureCritical);
	InitializeCriticalSection(&g_RecordIOCritical);
	InitializeCriticalSection(&g_SimulationCritical);
#ifdef QT_APP
	startup();
	QApplication a(argc, argv);
	//QGraphicsView w;
	DonMainWindow w;
	w.show();
#endif
#ifdef NETWORK_MODE0 // test socket
	ConnectThreadParam *connectParam = new ConnectThreadParam;
	connectParam->_connectSocket = 0;
	connectParam->_isUpdated = false;
	connectParam->_msgData = NULL;
	connectParam->_msgLen = 0;

	connetToHost("192.168.0.2", connectParam);
#endif
#ifdef SIMULATION
#if 0
	geneticAlgorithm(123);
	return 0;
#endif
	#if 0
		RecordIO::getInstance()->readGameRecords();
		DonKnowledge::getInstance()->trainingFromRecords();
	#else
		
		a.exec();
	#endif
		
	//system("pause");
	return 1;

#endif
	DWORD pid = GetProcessByName("chrome.exe");
	HWND hWnd = find_main_window(pid);
#if 0
	WINDOWPLACEMENT wp = {0};
	wp.length = sizeof(WINDOWPLACEMENT);
	GetWindowPlacement(hWnd, &wp);
	if (wp.showCmd == SW_SHOWMAXIMIZED) {
		ShowWindow(hWnd, SW_SHOW);
	}
	wp.rcNormalPosition.left = 0;
	wp.rcNormalPosition.right = 1368 + 6;
	wp.rcNormalPosition.top = 0;
	wp.rcNormalPosition.bottom = 735;
	wp.showCmd = SW_SHOWNA;
	SetWindowPlacement(hWnd, &wp);
	//SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, /*1874*/1000, /*1096*/50, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW); 
#endif
	//showInactiveWnd(hWnd);
	RECT rect;
	GetWindowRect(hWnd, &rect);
	SetWindowPos(hWnd, HWND_BOTTOM, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_NOACTIVATE);

	LONG style = GetWindowLong(hWnd, GWL_STYLE);
	style &= ~WEB_TITLE_WND_STYLE;
	//style = ~(WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX);
	SetWindowLong(hWnd, GWL_STYLE, style);
	ShowWindow(hWnd, SW_SHOWNA);

	setOnTop();

	GameController *gameController = new GameController();
	StrategyBase *bccattack = new KNNStrategy(gameController);
	MoneyManagement *moneym = new BASMoneyManagement(gameController);
	gameController->setStrategy(bccattack);
	gameController->setMoneyManagement(moneym);

	HANDLE keyPressedHandle, wndCapturehandle;
	WndCapture::getInstance().setWebWnd(hWnd);
	keyPressedHandle = (HANDLE)_beginthreadex(0, 0, &keyPressed, (void*)0, 0, 0);
	wndCapturehandle = (HANDLE)_beginthreadex(0, 0, &captureWndThreadFunc, (void*)0, 0, 0);

	Sleep(2000); // make sure captured wnd 
#if 1 // play a table v9
	CasinoBase *v9Casino = new CasinoV9();
	TableHandleBase *tblHandle = new TableHandleV9();
	tblHandle->setTableNumber(0);
	tblHandle->config();
	v9Casino->playTable(tblHandle, gameController);
#else // mornitor from lobby
	CasinoBase *agCasino = new CasinoAG();
	#if 1
		agCasino->monitorLobby();
	#else
		TableHandleBase *tblHandle = new TableHandleAG();
		tblHandle->setTableNumber(4);
		//tblHandle->config();
		agCasino->playTable(tblHandle, gameController);
	#endif
#endif
	WaitForSingleObject(keyPressedHandle, INFINITE);
	WaitForSingleObject(wndCapturehandle, INFINITE);

	CloseHandle(keyPressedHandle);
	CloseHandle(wndCapturehandle);

	// Delete critical sections
	DeleteCriticalSection(&g_wndCaptureCritical);
	DeleteCriticalSection(&g_RecordIOCritical);
	DeleteCriticalSection(&g_SimulationCritical);
	
	
	return 0;
}
#endif
