#ifndef __DON_ANALYZE_WIDGET_H__
#define __DON_ANALYZE_WIDGET_H__
#include "qmainwindow.h"
#include <Windows.h>
#include <qtimer.h>
class DonInputAnalWidget;

class GLGraph;

class DonAnalyzeWidget: public QMainWindow
{
	Q_OBJECT
public:
	DonAnalyzeWidget();
	~DonAnalyzeWidget();
	void showEvent(QShowEvent *);
	void startSimulationThread();
	void doSimulation();

public slots:
	void animate();

signals:
	void updateInforSignal(const QString &text);

private:
	void createInputWidget();
	void createGLGraph();

private:
	GLGraph *_glGraph;
	DonInputAnalWidget *_inputWidget;
	HANDLE _simthreadHdl;
	QString _infoStr;
	QTimer _animationTimer;
};
#endif