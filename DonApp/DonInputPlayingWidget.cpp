#include "DonInputPlayingWidget.h"
#include "DonPlayingWidget.h"
#include "qgroupbox.h"
#include "qlabel.h"
#include "qcombobox.h"
#include "qpushbutton.h"
#include "qlayout.h"
#include "qbuttongroup.h"
#include "qradiobutton.h"

DonInputPlayingWidget::DonInputPlayingWidget(DonPlayingWidget *parent):
		QWidget(parent)
{
	_playingWidget = parent;
	this->sizeHint();
	this->setMinimumSize(300, 20);
	this->resize(300, 20);
	int btnwidth = 75;
	int btnheight = 25;

	createOptionsBox();
	createInformationBox();
}

DonInputPlayingWidget::~DonInputPlayingWidget()
{
}

void DonInputPlayingWidget::createOptionsBox()
{
	// Group box Simulation
	QGroupBox *simulationbox = new QGroupBox("Options", this);
	simulationbox->setMaximumSize(600, 300);
	simulationbox->move(0, 65);
	simulationbox->resize(260, 300);

	QLabel *strategyLabel = new QLabel("Strategy:", simulationbox);
	strategyLabel->resize(200, 20);
	strategyLabel->move(10, 20);

	_strategyCombox = new QComboBox(simulationbox);
	_strategyCombox->resize(120, 20);
	_strategyCombox->addItem("Baccarat Attack");
	_strategyCombox->addItem("KNN");
	_strategyCombox->setCurrentIndex(1);
	_strategyCombox->move(10, 40);

	QLabel *moneymgntLabel = new QLabel("Money managment:", simulationbox);
	moneymgntLabel->resize(200, 20);
	moneymgntLabel->move(10, 70);

	_moneymgntCombox = new QComboBox(simulationbox);
	_moneymgntCombox->addItem("BAS money mm");
	_moneymgntCombox->resize(120, 20);
	_moneymgntCombox->move(10, 90);

	// Playing type
	QGroupBox *optionBox = new QGroupBox(tr("Playing Mode"), simulationbox);
	QHBoxLayout *optionLayout = new QHBoxLayout;

	_optionGroupBt = new QButtonGroup();
	_mornitorRadioBt = new QRadioButton(tr("Mornitoring"));
	_realPlayRadioBt = new QRadioButton(tr("Real Playing"));
	_optionGroupBt->addButton(_mornitorRadioBt);
	_optionGroupBt->addButton(_realPlayRadioBt);
	optionLayout->addWidget(_mornitorRadioBt);
	optionLayout->addWidget(_realPlayRadioBt);
	setPlayingMode(0);
	optionBox->setLayout(optionLayout);
	optionBox->setFixedSize(230, 65);
	optionBox->move(10, 120);
	//getSelectedOption();


	QPushButton *startSimulationBtn = new QPushButton("Start", simulationbox);
	QPushButton *pauseSimulationBtn = new QPushButton("Pause", simulationbox);
	QPushButton *stopSimulationBtn = new QPushButton("Stop", simulationbox);
	(startSimulationBtn)->move(10, simulationbox->height() - 30);
	(pauseSimulationBtn)->move(90, simulationbox->height() - 30);
	(stopSimulationBtn)->move(170, simulationbox->height() - 30);

	connect(startSimulationBtn, SIGNAL(clicked()), SLOT(startPlaying()));
	connect(pauseSimulationBtn, SIGNAL(clicked()), SLOT(pausePlaying()));
	connect(stopSimulationBtn, SIGNAL(clicked()), SLOT(stopSimulation()));
	connect(_strategyCombox, SIGNAL(currentIndexChanged(int)), this, SLOT(strategyChanged(int)));
	connect(_moneymgntCombox, SIGNAL(currentIndexChanged(int)), this, SLOT(moneymgntChanged(int)));
	connect(_mornitorRadioBt, SIGNAL(clicked()), this, SLOT(optionChecked()));
	connect(_realPlayRadioBt, SIGNAL(clicked()), this, SLOT(optionChecked()));
}

void DonInputPlayingWidget::createInformationBox()
{
}

int DonInputPlayingWidget::getStrategyType()
{
	return 0;
}

int DonInputPlayingWidget::getMoneyMgntType()
{
	return 0;
}

void DonInputPlayingWidget::startPlaying()
{
	_playingWidget->startPlayingThread();
}

void DonInputPlayingWidget::pausePlaying()
{
}

void DonInputPlayingWidget::strategyChanged(int index)
{
}

void DonInputPlayingWidget::moneymgntChanged(int index)
{
}

void DonInputPlayingWidget::updateInformation(const QString &text)
{
}

int DonInputPlayingWidget::getPlayingMode()
{
	int rc = 0;

	if (_mornitorRadioBt->isChecked()) {
		rc = 0;
	} else if (_realPlayRadioBt->isChecked()) {
		rc = 2;
	} else {
		rc = -1;
	}
	return rc;
}

void DonInputPlayingWidget::setPlayingMode(int opt)
{
	switch(opt) {
		case 0:
			_mornitorRadioBt->setChecked(true);
			break;
		case 1:
			_realPlayRadioBt->setChecked(true);
			break;
		default:
			break;
	}
}

void DonInputPlayingWidget::optionChecked()
{
	_mornitorRadioBt->setChecked(true);
}