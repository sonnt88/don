#ifndef __GAME_PLAYING_AG_H__
#define __GAME_PLAYING_AG_H__

#include "GamePlaying.h"

class GamePlayingAG: public GamePlaying
{
public:
	GamePlayingAG();
	~GamePlayingAG();
	void handleGameWaitingBet(TableHandleBase::enTableState preTableState);

private:
};
#endif