#include "CasinoV9.h"
#include "GameController.h"
#include "Utils.h"
#include "GamePlayingV9.h"

CasinoV9::CasinoV9(): CasinoBase()
{
}

CasinoV9::~CasinoV9()
{
}

CasinoBase::enCasinoState CasinoV9::checkStateOfCasino()
{
	return CasinoBase::MULTI_TABLE_STATE;
}

CasinoBase::enCasinoID CasinoV9::getID()
{
	return CasinoBase::CASINO_V9;
}

void CasinoV9::playTable(TableHandleBase *table, GameController *gmCtrler)
{
	HANDLE threadHdl = nullptr;
	TablePlayingHandle nwTablePlaying;

	GamePlaying *gamePlaying = new GamePlayingV9();
	gamePlaying->setCasino(this);
	gamePlaying->setTableHandle(table);
	gamePlaying->setGameController(gmCtrler);

	threadHdl = (HANDLE)_beginthreadex(0, 0, &DonApp::Utils::playTableThreadFunc, (void*)gamePlaying, 0, 0);
	nwTablePlaying._gamePlaying = gamePlaying;
	nwTablePlaying._threadHdl = threadHdl;
	//TODO: How to destroy memory for gameplaying???
	//_playingThreads.push_back(nwTablePlaying);
}

void CasinoV9::config()
{
	_config._AllTableBoard[0]		= ScreenCoord(304, 238);
	_config._AllTableBoard[1]		= ScreenCoord(1386, 238);
	_config._AllTableBoard[2]		= ScreenCoord(304, 453);
	_config._AllTableBoard[3]		= ScreenCoord(1386, 453);
	_config._AllTableBoard[4]		= ScreenCoord(304, 668);
	_config._AllTableBoard[5]		= ScreenCoord(1386, 668);
	_config._AllTableBoard[6]		= ScreenCoord(1122, 905); //???
}

void CasinoV9::monitorLobby(CasinoObserver *observer)
{
}