#include "GLView.h"

#include <QtGui>
#include <QtOpenGL>
#include "QWheelEvent"

#include <math.h>

#include "GLView.h"
#include "GL\glu.h"


#ifndef GL_MULTISAMPLE
#define GL_MULTISAMPLE  0x809D
#endif

GLView::GLView(QWidget *parent)
	: QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
	xRot = 0;
	yRot = 0;
	zRot = 0;
	xTranslate = 0;
	yTranslate = 0;
	m_factor = 1.0;
	centerX = 0.0;
	centerY = 0.0;

	qtGreen = QColor::fromCmykF(0.40, 0.0, 1.0, 0.0);
	qtPurple = QColor::fromRgb(50, 50, 50);//QColor::fromCmykF(0.39, 0.39, 0.0, 0.0);
	setMouseTracking(true);

	_animationTimer.setSingleShot(false);
    connect(&_animationTimer, SIGNAL(timeout()), this, SLOT(animate()));
    //_animationTimer.start(25);
}

GLView::~GLView()
{
}

QSize GLView::minimumSizeHint() const
{
	return QSize(50, 50);
}

QSize GLView::sizeHint() const
{
	return QSize(1000, 1000);
}


static void qNormalizeAngle(int &angle)
{
	while (angle < 0)
		angle += 360 * 16;
	while (angle > 360 * 16)
		angle -= 360 * 16;
}

void GLView::setXRotation(int angle)
{
	qNormalizeAngle(angle);
	if (angle != xRot) {
		xRot = angle;
		emit xRotationChanged(angle);
		updateGL();
	}
}

void GLView::setYRotation(int angle)
{
	qNormalizeAngle(angle);
	if (angle != yRot) {
		yRot = angle;
		emit yRotationChanged(angle);
		updateGL();
	}
}

void GLView::setZRotation(int angle)
{
	qNormalizeAngle(angle);
	if (angle != zRot) {
		zRot = angle;
		emit zRotationChanged(angle);
		updateGL();
	}
}

void GLView::initializeGL()
{
	qglClearColor(qtPurple);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glShadeModel(GL_SMOOTH);
	//glEnable(GL_LIGHTING);
	//glEnable(GL_LIGHT0);
	glEnable(GL_MULTISAMPLE);
	static GLfloat lightPosition[4] = { 200, 200.0, 200.0, 0.0 };
	//glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
}

void GLView::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	
	drawAxises();
	viewOrtho();

	glTranslatef(xTranslate*m_factor, yTranslate*m_factor, -10.0);
	glRotatef(xRot / 16.0, 1.0, 0.0, 0.0);
	glRotatef(yRot / 16.0, 0.0, 1.0, 0.0);
	glRotatef(zRot / 16.0, 0.0, 0.0, 1.0);
#ifdef DEBUG_MODE
	glColor3f(1.0, 0.0, 1.0);
	glPointSize(5.0);
	glBegin(GL_POINTS);
	glVertex3f(_dbgPoint[0], _dbgPoint[1], _dbgPoint[2]);
	glEnd();
#endif
}

void GLView::resizeGL(int width, int height)
{
	viewOrtho();
}

void GLView::viewOrtho()
{
	int side = qMin(width(), height());
	glViewport(0, 0, width(), height());
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(centerX -viewport[2]/2 * m_factor, centerX + viewport[2]/2 * m_factor, 
			centerY -viewport[3]/2 * m_factor, centerY + viewport[3]/2 * m_factor, -1000, 1500);

	glMatrixMode(GL_MODELVIEW);
}

void GLView::mousePressEvent(QMouseEvent *event)
{
	lastPos = event->posF();
#ifdef DEBUG_MODE
	glPointAt(_dbgPoint, lastPos.rx(), lastPos.ry());
#endif
}

void GLView::mouseMoveEvent(QMouseEvent *event)
{
	double dx = event->posF().rx() - lastPos.rx();
	double dy = event->posF().ry() - lastPos.ry();
	
	if (event->buttons() & Qt::LeftButton) {
		return;
		setXRotation(xRot + 8 * dy);
		setYRotation(yRot + 8 * dx);
	} else if (event->buttons() & Qt::RightButton) {
		xTranslate += dx;
		yTranslate += -dy;
	}
	lastPos = event->posF();
	update();
}

void GLView::calcViewCenter(double mx, double my, double prefactor)
{
	double width = this->width();
	double height = this->height();
	// flip mouse y axis so up is +y
    my = height - my;

    // convert mouse coords to (-1/2,-1/2)-(1/2, 1/2) box
    double x = ( mx / width ) - 0.5;
    double y = ( my / height ) - 0.5;
	double preX = ( x * width * prefactor );
    double preY = ( y * height * prefactor );

    double postX = ( x * width * m_factor );
    double postY = ( y * height * m_factor );

    // recenter
    centerX += ( preX - postX );
    centerY += ( preY - postY );
}

void GLView::wheelEvent(QWheelEvent *event)
{
	GLdouble p1[3];
	float prefactor = m_factor;
	glPointAt(p1, lastPos.rx(), lastPos.ry());
	
	double numDegrees = event->delta() / 8.0;
	double numSteps = numDegrees / 15.0;
	QPoint point = event->globalPos();
	if (event->orientation() == Qt::Horizontal) {
		//printf("%s", __FUNCTION__);
	} else {
		m_factor *= (float)pow( 2, ( -numSteps ) * 0.1f );
		//printf("%s", __FUNCTION__);
	}
	event->accept();
	calcViewCenter(lastPos.rx(), lastPos.ry(),prefactor);
	centerX -= xTranslate*(prefactor - m_factor);
	centerY -= yTranslate*(prefactor - m_factor);
	update();
}

void GLView::drawAxises()
{
	/* set view port */
	glViewport(0, 0, AXIS_LENGTH * 2, AXIS_LENGTH * 2);
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-viewport[2]/2 * 1, viewport[2]/2 * 1, 
			-viewport[3]/2 * 1, viewport[3]/2 * 1, -2000, 3000);
	glMatrixMode(GL_MODELVIEW);

	glPushMatrix();
	glLoadIdentity();
	//glTranslated(-width()/2 + 50.0, -height()/2 + 50.0, 0.0f);
	glRotatef(xRot/16, 1.0, 0.0, 0.0);
	glRotatef(yRot/16, 0.0, 1.0, 0.0);
	glRotatef(zRot/16, 0.0, 0.0, 1.0);

	/* x axis */
	glLineWidth(2.0f);
	glColor3d(1.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3d(0.0f, 0.0f, 0.0f);
	glVertex3d(AXIS_LENGTH, 0.0f, 0.0f);
	glEnd();

	/* y axis */
	glColor3f(0.0f, 1.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, AXIS_LENGTH, 0.0f);
	glEnd();

	/* z axis */ 
	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_LINES);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, AXIS_LENGTH);
	glEnd();
	glPopMatrix();
}

void GLView::viewCenter()
{
	xTranslate = 0;
	yTranslate = 0;

	m_factor = 1.0f;

	update();
}

void GLView::glPointAt(GLdouble glPoint[3], double winx, double winy, double winz)
{
	GLint viewport[4];
	double mvmatrix[16];
	double projmatrix[16];
	double realy;

	glGetIntegerv( GL_VIEWPORT, viewport );
	glGetDoublev( GL_MODELVIEW_MATRIX, mvmatrix );
	glGetDoublev( GL_PROJECTION_MATRIX, projmatrix );
	realy = viewport[3] - (GLint)winy;
	gluUnProject( (GLdouble) winx, (GLdouble) realy, 0.0, mvmatrix,
	projmatrix, viewport, (GLdouble*)&glPoint[0], (GLdouble*)&glPoint[1], (GLdouble*)&glPoint[2]);
}

void GLView::winPointAt(GLdouble glpoint[3], double win[2])
{
	GLint viewport[4];
	double mvmatrix[16];
	double projmatrix[16];
	GLdouble winx, winy, winz;

	glGetIntegerv( GL_VIEWPORT, viewport );
	glGetDoublev( GL_MODELVIEW_MATRIX, mvmatrix );
	glGetDoublev( GL_PROJECTION_MATRIX, projmatrix );
	//realy = viewport[3] - (GLint) winy - 1;
	gluProject( (GLdouble) glpoint[0], glpoint[1], glpoint[2], mvmatrix, projmatrix,
				viewport, &winx, &winy, &winz);

	win[0] = winx;
	win[1] = viewport[3] - winy;
}

void GLView::animate()
{
    //update();
}