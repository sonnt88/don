#include "GLGraph.h"
#include "stdio.h"
#include "Lock.h"
#include "GLButton.h"
#include "qevent.h"

GLGraph::GLGraph():GLView(NULL),
				_selectedObj(-1),
				_measureObj1(-1),
				_measureObj2(-1),
				_autoMove(false)
{
	_graphConfig._xInterval = 40;
	_graphConfig._yInterval = 30;

	_graphConfig._pointColor[0] = 0.0f;
	_graphConfig._pointColor[1] = 0.0f;
	_graphConfig._pointColor[2] = 0.0f;

	_graphConfig._axisColor[0] = 1.0f;
	_graphConfig._axisColor[1] = 1.0f;
	_graphConfig._axisColor[2] = 1.0f;

	_graphConfig._dataLineColor[0] = 1.0f;
	_graphConfig._dataLineColor[1] = .0f;
	_graphConfig._dataLineColor[2] = .0f;

	_graphConfig._font = QFont("MS Shell Dlg 2", 10, QFont::Normal);
	_data.push_back(0.f);
}

GLGraph::~GLGraph()
{
}

void GLGraph::paintGL()
{
	Lock simuguard(g_SimulationCritical);
	::GLView::paintGL();
	drawGraph();
}

void GLGraph::mousePressEvent(QMouseEvent *event)
{
	Lock sumuguard(g_SimulationCritical);
	GLView::mousePressEvent(event);
}

void GLGraph::mouseReleaseEvent(QMouseEvent *event)
{
	Lock sumuguard(g_SimulationCritical);
	if (_selectedObj == -1) return;
	if (event->button() == Qt::LeftButton) {
		if (_measureObj1 == -1) {
			_measureObj1 = _selectedObj;
		} else if (_measureObj1 == _selectedObj) {
			_measureObj1 = -1;
		} else if (_measureObj2 == -1) {
			_measureObj2 = _selectedObj;
		} else if (_measureObj2 == _selectedObj) {
			_measureObj2 = -1;
		} else {
			_measureObj1 = _selectedObj;
			_measureObj2 = -1;
		}

		if (_measureObj1 == _measureObj2) 
		{
			_measureObj1 = -1;
			_measureObj2 = -1;
		}
	}/* else if (event->buttons() & Qt::RightButton) {
	}*/
	update();
}

void GLGraph::mouseMoveEvent(QMouseEvent *event)
{
	Lock simuguard(g_SimulationCritical);
	GLView::mouseMoveEvent(event);
	this->selectObject();
}

void GLGraph::wheelEvent(QWheelEvent *event)
{
	Lock simuguard(g_SimulationCritical);
	GLView::wheelEvent(event);
}

void GLGraph::drawGraph()
{
	const std::vector<float> &wonmoneylist = _data;
	unsigned nround = wonmoneylist.size();
	glDisable(GL_DEPTH_TEST);
	drawNetSquares();
	glLineWidth(1.5f);
	glColor3fv(_graphConfig._dataLineColor);
	glBegin(GL_LINE_STRIP);
	for (unsigned i = 0; i < nround; ++i)
	{
		glVertex2d(i*_graphConfig._xInterval, wonmoneylist[i]);
	}
	glEnd();

	glPointSize(4.0f);
	glColor3fv(_graphConfig._pointColor);
	glBegin(GL_POINTS);
	for (unsigned i = 0; i < nround; ++i)
	{
		glVertex2d(i*_graphConfig._xInterval, wonmoneylist[i]);
	}
	glEnd();

	drawSelectedObj();
	drawMeasure();
	drawAxisesWithNumber();
	glEnable(GL_DEPTH_TEST);
	if (_autoMove) 
	{
		moveToLast();
	}
	//drawOptionButton();
}

void GLGraph::drawAxisesWithNumber()
{
	// Render Axis lines
	drawXAxis();
	drawYAxis();
	viewOrtho();
}

void GLGraph::drawXAxis()
{
	QFont qfont = _graphConfig._font;
	double MARGIN = 12 * m_factor;
	double XMARGIN = 12 * m_factor;
	double YMARGIN = 15 * m_factor;
	const double NEGATIVE_SHIFT_MARGIN = 3*m_factor;
	double xLength = width() * m_factor;
	double yLength = height() * m_factor;
	const double NODE_LENGTH = 6 * m_factor;
	const double MAX_INTERVAL = 100;
	const double MIN_INTERVAL = 20;
	GLint viewport[4];
	const double LENGTH = width();
	const double XWIDTH = 50;
	glViewport(0, 0, width(), XWIDTH);
	glGetIntegerv(GL_VIEWPORT, viewport);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(centerX -viewport[2]/2 * m_factor, centerX + viewport[2]/2 * m_factor, 
			-viewport[3]/2 * m_factor, viewport[3]/2 * m_factor, -2000, 3000);
	glMatrixMode(GL_MODELVIEW);

	glPushMatrix();
	glLoadIdentity();
	glTranslatef(xTranslate* m_factor, 0, 0);

	glLineWidth(1.0f);
	glColor3fv(_graphConfig._axisColor);
	glBegin(GL_LINES);
	glVertex2d(-INT_MAX, 0);
	glVertex2d(INT_MAX, 0);
	glEnd();
	
	// Render x Axis nodes
	double scale = ceill(m_factor);
	double inode = 0;
	unsigned steps = xLength / _graphConfig._xInterval;
	steps /= scale;
	double start_value = centerX -viewport[2]/2 * m_factor - xTranslate*m_factor;
	start_value = (start_value/_graphConfig._xInterval);
	start_value = ceil(start_value/scale);
	_startX = start_value;
	
	for (unsigned i = 0; i < steps; ++i)
	{
		inode = (start_value + i) * scale;
		QString text = QString::number(inode);
		if (i == 0) {
			QFontMetrics fmatrix(qfont);
			QRect square = fmatrix.boundingRect(text);
			XMARGIN = m_factor * square.width()/ 3.;
		}

		bool isSkipEven = inode > 1000 || inode < -1000;

		// for positive nodes
		double xpos = (inode * _graphConfig._xInterval);
		QString numstr;
		if (false == isSkipEven || (i%2) == 0)
		renderText(xpos - XMARGIN, -YMARGIN, 0, text, qfont);

		// render node
		glBegin(GL_LINES);
		glVertex2d(xpos, -NODE_LENGTH * 0.5);
		glVertex2d(xpos, NODE_LENGTH * 0.5);
		glEnd();
	}
	glPopMatrix();
}

void GLGraph::drawYAxis()
{
	double XMARGIN = 12 * m_factor;
	double YMARGIN = 5 * m_factor;
	double yLength = height() * m_factor;
	const double NODE_LENGTH = 3 * m_factor;
	const double XPOS_AXIS = -20 * m_factor;
	GLint viewport[4];
	QFont qfont = _graphConfig._font;

	const double YWIDTH = 50;
	const double LENGTH = height();
	glViewport(width() - YWIDTH, 0, YWIDTH, height());
	glGetIntegerv(GL_VIEWPORT, viewport);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-viewport[2]/2 * m_factor, viewport[2]/2 * m_factor, 
			centerY -viewport[3]/2 * m_factor, centerY + viewport[3]/2 * m_factor, -2000, 3000);
	glMatrixMode(GL_MODELVIEW);

	glPushMatrix();
	glLoadIdentity();
	glTranslatef(0, yTranslate * m_factor, 0);

	glLineWidth(1.0f);
	glColor3fv(_graphConfig._axisColor);
	glBegin(GL_LINES);
	glVertex2d(XPOS_AXIS, -INT_MAX);
	glVertex2d(XPOS_AXIS, INT_MAX);
	glEnd();

	// Render x Axis nodes
	glGetIntegerv(GL_VIEWPORT, viewport);
	double scale = ceill(m_factor);
	double inode = 0;
	double xpos = 0;
	unsigned short resolution = _graphConfig._yInterval;
	unsigned steps = yLength / _graphConfig._yInterval;
	steps /= scale;
	steps += 5;
	double start_value = centerY - viewport[3]/2 * m_factor - yTranslate*m_factor;
	start_value = (start_value/resolution);
	start_value = floorf(start_value / scale);
	_startY = start_value;
	QString text;
	for (unsigned i = 1; i < steps; ++i)
	{
		inode = (start_value + i) * scale * resolution;
		text = QString::number(inode);
		if (i == 1) 
		{
			QFontMetrics fmatrix(qfont);
			QRect square = fmatrix.boundingRect(text);
			XMARGIN = m_factor * (-7);
		}

		// render number
		double ypos = (inode);
		QString numstr;
		renderText(XPOS_AXIS - XMARGIN, ypos - YMARGIN, 0, text, qfont);

		// render node
		glBegin(GL_LINES);
		glVertex2d(XPOS_AXIS - NODE_LENGTH, ypos);
		glVertex2d(XPOS_AXIS + NODE_LENGTH, ypos);
		glEnd();
	}

	glPopMatrix();
}

void GLGraph::drawOptionButton()
{
#if 1
	GLButton *vlbtn = new GLButton(this, nullptr);
	vlbtn->setBoundingRect(30, 40, 50, 50);
	vlbtn->render();
	vlbtn->drawRect();

	GLButton *vlbtn2 = new GLButton(this, vlbtn);
	vlbtn2->setBoundingRect(10, 10, 10, 10);
	vlbtn2->render();
#else
	double width = 30;
	double height = 40;
	double xpos = 0;
	double ypos = 0;
	double points[12];
	double size = 8;

	width *= m_factor;
	height *= m_factor;
	xpos *= m_factor;
	ypos *= m_factor;
	size *= m_factor;

	points[0] = xpos;
	points[1] = ypos;
	points[2] = xpos + width;
	points[3] = ypos - height * 0.5;
	points[4] = xpos;
	points[5] = ypos - height;
	points[6] = xpos;
	points[7] = ypos - (height - size);
	points[8] = xpos + (width - size* width * 2/height);
	points[9] = ypos - height * 0.5;
	points[10] = xpos;
	points[11] = ypos - size;
	//glVertex2d(points[0], points[1]); //1
	//glVertex2d(points[2], points[3]); //2
	//glVertex2d(points[4], points[5]); //3
	//glVertex2d(points[6], points[7]); //4
	//glVertex2d(points[8], points[9]); //5
	//glVertex2d(points[10], points[11]); // 6

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glTranslated(centerX, centerY, 0.0);
	//glRotatef(90, 0, 0, 1.0); // up
	//glRotatef(180, 0, 0, 1.0); // left
	//glRotatef(270, 0, 0, 1.0); // right

	glDisable(GL_DEPTH_TEST);
	// arrow button
	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_TRIANGLES);
	glVertex2d(points[0], points[1]);
	glVertex2d(points[10], points[11]);
	glVertex2d(points[2], points[3]); //2

	glVertex2d(points[10], points[11]); // 6
	glVertex2d(points[8], points[9]); //5
	glVertex2d(points[2], points[3]); //2

	glVertex2d(points[8], points[9]); //5
	glVertex2d(points[6], points[7]); //4
	glVertex2d(points[2], points[3]); //2

	glVertex2d(points[2], points[3]); //2
	glVertex2d(points[6], points[7]); //4
	glVertex2d(points[4], points[5]); //3
	glEnd();
	glPopMatrix();
	glEnable(GL_DEPTH_TEST);
#endif
}

void GLGraph::drawSelectedObj()
{
	if (_selectedObj == -1) 
		return;

	double xval, yval;
	xval = _selectedObj*_graphConfig._xInterval;
	yval = _data[_selectedObj];

	highlightObject(_selectedObj);

	//render text
	char str[30];
	sprintf(str, "[%d; %.3f]", _selectedObj, yval);
	QString text(str);
	QFont qfont = _graphConfig._font;
	QFontMetrics fmatrix(qfont);
	QRect square = fmatrix.boundingRect(text);
	double w = (square.width() + 4) * m_factor;
	double h = square.height() * m_factor;
	// background
	glColor4f(0.0, 0.0, 0, 0.75f);
	glBegin(GL_QUADS);                    
	glVertex2d(xval,yval);
	glVertex2d(xval + w,yval);
	glVertex2d(xval + w, yval + h);
    glVertex2d(xval, yval + h);
	glEnd();

	yval += 4.*m_factor;
	xval += 2.*m_factor;
	glColor3f(1.0, 1.0, 1.0);
	renderText(xval, yval, 0, text, qfont);
}

void GLGraph::drawMeasure()
{
	// render measure points
	highlightObject(_measureObj1);
	highlightObject(_measureObj2);

	// check to render measure line
	if (_measureObj1 == -1 || _measureObj2 == -1) 
	{
		return;
	} 

	double xval1, yval1;
	double xval2, yval2;
	xval1 = _measureObj1*_graphConfig._xInterval;
	yval1 = _data[_measureObj1];
	xval2 = _measureObj2*_graphConfig._xInterval;
	yval2 = _data[_measureObj2];

	//render text
	double xval = xval1;
	double yval = yval2;
	char str[30];
	int obj1, obj2;
	if (_measureObj1 <= _measureObj2) {obj1 = _measureObj1; obj2 = _measureObj2;}
	else {obj1 = _measureObj2; obj2 = _measureObj1;}

	sprintf(str, "[%d-%d; %.3f]", obj1, obj2, _data[obj2] - _data[obj1]);
	QString text(str);
	QFont qfont = _graphConfig._font;
	QFontMetrics fmatrix(qfont);
	QRect square = fmatrix.boundingRect(text);
	double w = (square.width() + 4) * m_factor;
	double h = square.height() * m_factor;
	// lines
	glColor3f(1.0f, 1.0f, 0.0f);
	glBegin(GL_LINE_STRIP);
	glVertex2d(xval1, yval1);
	glVertex2d(xval1, yval2);
	glVertex2d(xval2, yval2);
	glEnd();
	xval -= w * 0.5;
	yval -= h * 0.5;
	// background
	glColor4f(0.0, 0.0, 0, 0.75f);
	glBegin(GL_QUADS);                    
	glVertex2d(xval,yval);
	glVertex2d(xval + w,yval);
	glVertex2d(xval + w, yval + h);
    glVertex2d(xval, yval + h);
	glEnd();

	yval += 4.*m_factor;
	xval += 2.*m_factor;
	glColor3f(1.0, 1.0, 1.0);
	renderText(xval, yval, 0, text, qfont);
}

void GLGraph::highlightObject(short i)
{
	if (i == -1) 
		return;

	double xval, yval;
	xval = i*_graphConfig._xInterval;
	yval = _data[i];

	// render point
	glPointSize(6.0f);
	glColor3f(0.0, 1.0, 0.0);
	glBegin(GL_POINTS);
	glVertex2d(xval, yval);
	glEnd();
}

void GLGraph::drawNetSquares()
{
	const double NODE_LENGTH = 3 * m_factor;
	const double XPOS_AXIS = -20 * m_factor;
	double xLength = width() * m_factor;
	double yLength = height() * m_factor;
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);
	
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	// vertical lines
	double inode = 0.;
	unsigned short resolution = 80;
	unsigned steps = (xLength / _graphConfig._xInterval) / m_factor;
	glLineWidth(0.25f);
	glColor3f(0.75f, 0.75f, 0.75f);
	glBegin(GL_LINES);
	double topY = centerY + viewport[3]/2 * m_factor;
	double bottomY = centerY - viewport[3]/2 * m_factor + 25 * m_factor;
	double rightX = centerX + viewport[2]/2 * m_factor - 50 * m_factor;
	double start = centerX - viewport[2]/2 * m_factor;
	for (unsigned i = 0; i < steps; i++)
	{
		double xpos = (start + i * resolution * m_factor);
		double ypos = 0;
		if (xpos > rightX)
			continue;
		glVertex2d(xpos, bottomY);
		glVertex2d(xpos, topY);
	}
	glEnd();

	// horizon lines
	inode = 0;
	steps = (yLength / _graphConfig._xInterval) / m_factor;
	start = centerY - viewport[3]/2 * m_factor;
	double xLeft = centerX - viewport[2]/2 * m_factor;
	double xRight = centerX + viewport[2]/2 * m_factor - 45 * m_factor;
	double yBottom = centerY - viewport[3]/2 * m_factor + 25 * m_factor;
	glBegin(GL_LINES);
	for (unsigned i = 0; i < steps; i++)
	{
		double ypos = (start + i * resolution * m_factor);
		if (ypos < yBottom)
			continue;
		glVertex2d(xLeft, ypos);
		glVertex2d(xRight, ypos);
	}
	glEnd();
	glPopMatrix();
}

int GLGraph::selectObject()
{
	double win[2];
	double size = 5.0;
	unsigned n = _data.size();
	//printf("\nlastPos: << x =%f y = %f",lastPos.rx(), lastPos.ry());
	
	for (unsigned i = 0; i < n; ++i)
	{
		double point[3] = {i*_graphConfig._xInterval, _data[i], 0.0};
		winPointAt(point, win);
		
		if ((win[0] - lastPos.rx() < size && win[0] - lastPos.rx() > -size) &&
			(win[1] - lastPos.ry() < size && win[1] - lastPos.ry() > -size))
		{
			_selectedObj = i;
			return i;
		}
	}
	_selectedObj = -1;
	return -1;
}

void GLGraph::addNewData(float data)
{
	{
		Lock simuguard(g_SimulationCritical);
		_data.push_back(data);
	}
	
}

void GLGraph::moveToLast()
{
	Lock guard(g_SimulationCritical);
	unsigned lastindex = _data.size() - 1;
	double xpos = lastindex*_graphConfig._xInterval;
	if (xpos > glRightMostWnd())
	{
		double delX = (xpos - glRightMostWnd())/m_factor;
		xTranslate -= delX;
	}
}

void GLGraph::clearData()
{
	Lock guard(g_SimulationCritical);
	_data.clear();
	_data.shrink_to_fit();
	_startX = 0;
	_startY = 0;
	_selectedObj = -1;
	_measureObj1 = -1;
	_measureObj2 = -1;
	_data.push_back(0.0f);
}

double GLGraph::glRightMostWnd()
{
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);
	return (centerX + viewport[2]/2 * m_factor - xTranslate*m_factor);
}

double GLGraph::glLeftMostWnd()
{
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);
	return (centerX - viewport[2]/2 * m_factor - xTranslate*m_factor);
}

double GLGraph::glTopMostWnd()
{
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);
	return (centerY + viewport[3]/2 * m_factor - yTranslate*m_factor);
}

double GLGraph::glBottomMostWnd()
{
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);
	return (centerY - viewport[3]/2 * m_factor - yTranslate*m_factor);
}