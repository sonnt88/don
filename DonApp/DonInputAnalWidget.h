#ifndef __DON_INPUT_ANAL_WIDGET_H__
#define __DON_INPUT_ANAL_WIDGET_H__

#include "qpushbutton.h"
#include "qtextedit.h"
#include "DonAnalyzeWidget.h"
#include "StrategyBase.h"
#include "MoneyManagement.h"

class QLineEdit;
class QSlider;
class QLabel;
class QComboBox;

class DonInputAnalWidget: public QWidget
{
	Q_OBJECT
public:
	DonInputAnalWidget(DonAnalyzeWidget *parent = 0);
	~DonInputAnalWidget();
	void createSimulationBox();
	void createInformationBox();
	int getSimSpeed();
	int getStrategyType();
	int getMoneyMgntType();

	QString filePath() { return _filePath; }
public slots:
	void startSimulation();
	void pauseSimulation();
	void stopSimulation();
	void browse(QObject *lineEdit);
	void speedValue(int value);
	void strategyChanged(int index);
	void moneymgntChanged(int index);
	void updateInformation(const QString &text);

signals:
	

private:
	DonAnalyzeWidget *_analyzeWidget;
	QString _filePath;
	QSlider *_slider;
	QLabel *_speedLabel;
	QComboBox *_strategyCombox;
	QComboBox *_moneymgntCombox;
	QTextEdit *_informationlbl;
};
#endif