#ifndef __DON_INPUT_PLAYING_WIDGET_H__
#define __DON_INPUT_PLAYING_WIDGET_H__

#include "qwidget.h"

class DonPlayingWidget;
class QComboBox;
class QTextEdit;
class QButtonGroup;
class QRadioButton;

class DonInputPlayingWidget: public QWidget
{
	Q_OBJECT
public:
	DonInputPlayingWidget(DonPlayingWidget *parent = 0);
	~DonInputPlayingWidget();
	void createOptionsBox();
	void createInformationBox();
	int getStrategyType();
	int getMoneyMgntType();
	int getPlayingMode();
	void setPlayingMode(int opt);

public slots:
	void startPlaying();
	void pausePlaying();
	void strategyChanged(int index);
	void moneymgntChanged(int index);
	void updateInformation(const QString &text);
	void optionChecked();

signals:

private:
	DonPlayingWidget *_playingWidget;
	QComboBox *_strategyCombox;
	QComboBox *_moneymgntCombox;
	QTextEdit *_informationlbl;

	QButtonGroup *_optionGroupBt;
	QRadioButton *_mornitorRadioBt;
	QRadioButton *_realPlayRadioBt;
};
#endif