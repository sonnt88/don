#ifndef __TABLE_HANDLE_V9_H__
#define __TABLE_HANDLE_V9_H__

#include "TableHandleBase.h"

class TableHandleV9: public TableHandleBase {
public:
	TableHandleV9();
	~TableHandleV9();

	/*
	**	override virtual functions
	*/
	void config();
	int checkWinner();
	enTableState checkTableState();
	bool checkBigResult();
	void doBet(int nlost);

	void shiftAlongTopleft();
	bool isEmptyAtRound0();
	bool isBankerColor(int *rgb);
	bool isPlayerColor(int *rgb);
	bool isTieColor(int *rgb);
	ScreenCoord findTopleftTbl();

private:
};
#endif