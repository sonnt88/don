#include "WndCapture.h"

WndCapture * volatile WndCapture::_wndCaptureInstance = NULL;

WndCapture::WndCapture():
						_wndImgWidth(0),
						_wndImgHeight(0),
						_pixelsColor(0)
{
}

WndCapture::~WndCapture()
{
	if (_pixelsColor) {
		delete _pixelsColor;
	}
}

WndCapture &WndCapture::getInstance()
{
	if (NULL == _wndCaptureInstance) {
		// lock before create
		Lock loc(g_wndCaptureCritical);
		if (NULL == _wndCaptureInstance) { // second check
			WndCapture* volatile temp =
				static_cast<WndCapture*>(operator new(sizeof(WndCapture)));
			temp->_pixelsColor = NULL;
			temp->_wndImgWidth = 0;
			temp->_wndImgHeight = 0;
            _wndCaptureInstance = temp;
		}
		
	}

	return *_wndCaptureInstance;
}

void WndCapture::capture() {
	HBITMAP hBmp;
	WINDOWPLACEMENT wp = {0};
	wp.length = sizeof(WINDOWPLACEMENT);
	GetWindowPlacement(_webWnd, &wp);
		
	ANIMATIONINFO ai = {0};
	bool restoreAnimation = false;
	if (wp.showCmd == SW_SHOWMINIMIZED ||
		SW_HIDE == wp.showCmd)
	{
		restoreAnimation = showInactiveWnd(_webWnd);
		Sleep(200);
	}

	hBmp = GetScreenShot2(_webWnd);

	if (wp.showCmd == SW_SHOWMINIMIZED ||
		SW_HIDE == wp.showCmd)
	{
		rePlacementWnd(_webWnd, wp);
	}
		
	BITMAP bm;
	GetObject(hBmp, sizeof(bm), &bm);
	int width(bm.bmWidth),
		height(bm.bmHeight);
	RECT rect;
	GetWindowRect(_webWnd, &rect);
	int w = rect.right - rect.left -16;
	int h = rect.bottom - rect.top - 8;
	if (width <  w|| height < h) {
			return;
	}

	_wndImgWidth = width;
	_wndImgHeight = height;

	//Guard for multithread accessing
	Lock lock(g_wndCaptureCritical);
	if (_pixelsColor) {
		delete[] _pixelsColor;
		_pixelsColor = NULL;
	}
	_pixelsColor = GetImageData(hBmp);
}