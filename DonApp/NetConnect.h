#ifndef __NET_CONNECT_H__
#define __NET_CONNECT_H__

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

struct ConnectThreadParam {
	SOCKET _connectSocket;
	bool _isUpdated;
	char *_msgData;
	unsigned _msgLen;
};

int connetToHost(char *host, ConnectThreadParam *cnParam);

#endif