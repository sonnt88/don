#include "DonTabWidget.h"
#include "DonAnalyzeWidget.h"
#include "DonPlayingWidget.h"

DonTabWidget::DonTabWidget(QWidget* parent):QTabWidget(parent)
{
	setParent(parent);
	_analyzeWidget = new DonAnalyzeWidget();
	_playingWidget = new DonPlayingWidget();
	setTabPosition(QTabWidget::TabPosition::North);
	//tabWidget->tabBar()->setStyle(new CustomTabStyle);
	addTab(_playingWidget, "Playing");
	addTab(_analyzeWidget, "Analyze");
	addTab(new QWidget, "Settings");

	setCurrentIndex(1);
}

DonTabWidget::~DonTabWidget()
{
}