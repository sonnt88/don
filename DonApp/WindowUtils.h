#ifndef __WINDOW_UTILS_H__
#define __WINDOW_UTILS_H__

#include <iostream>
#include <windows.h>
#include "string.h"
#include <tlhelp32.h>
#include <Windows.h>
#include <TlHelp32.h>
#include <tchar.h>
#include <Psapi.h>

struct ScreenCoord {
	ScreenCoord() {
		_x = 0;
		_y = 0;
	}
	ScreenCoord(int x, int y) {
		_x = x;
		_y = y;
	}

	ScreenCoord &scale(float xrate, float yrate) {
		_x *= xrate;
		_y *= yrate;

		return *this;
	}

	ScreenCoord &operator +=(const ScreenCoord & other)
	{
		_x += other._x;
		_y += other._y;

		return *this;
	}
	
	ScreenCoord &operator =(const ScreenCoord & other)
	{
		_x = other._x;
		_y = other._y;

		return *this;
	}

	int _x;
	int _y;
};

static ScreenCoord operator +(const ScreenCoord &p1, const ScreenCoord &p2)
{
	ScreenCoord sum;
	sum._x = p1._x + p2._x;
	sum._y = p1._y + p2._y;

	return sum;
}

BOOL is_main_window(HWND handle);
BOOL CALLBACK enum_windows_callback(HWND handle, LPARAM lParam);
HWND find_main_window(unsigned long process_id);
HBITMAP GetScreenShot(bool isSave = false);
void saveScreenBitmap(const char *imgName);
HBITMAP GetScreenShot2(HWND hwnd);
int CaptureAnImage(HWND hWnd);
DWORD GetProcessByName(char *name);
BYTE *GetImageData(HBITMAP &hBmp);
LRESULT CALLBACK LowLevelMouseProc(int nCode, WPARAM wParam, LPARAM lParam);
bool showInactiveWnd(HWND hwnd);
void rePlacementWnd(HWND hwnd, WINDOWPLACEMENT wp, bool restoreAnimation = true);
void click (ScreenCoord &scrCoord, HWND hwnd);
#endif