#include "DonMainWindow.h"
#include "GLGraph.h"
#include "qlayout.h"
#include "qdockwidget.h"
#include "qpushbutton.h"
#include "qtabbar.h"

#include <QtGui>
#include <QWidget>
#include <QDialog>
#include <QApplication>
#include <DonTabWidget.h>
#include "DonAnalyzeWidget.h"


DonMainWindow::DonMainWindow(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	_glView = new GLGraph();
	QDockWidget *dockWidget = new QDockWidget(tr("Demo Dock Widget"), this);
    dockWidget->setAllowedAreas(Qt::LeftDockWidgetArea |
                                Qt::RightDockWidgetArea|
								//Qt::TopDockWidgetArea|
								Qt::BottomDockWidgetArea);
	this->addDockWidget(Qt::BottomDockWidgetArea, dockWidget);
	//setDockOptions(DockOptions options)
	
	DonTabWidget *tabWidget =  new DonTabWidget();
	QVBoxLayout *mainLayout = new QVBoxLayout;
	mainLayout->addWidget(tabWidget);
	tabWidget->show();

	setCentralWidget(tabWidget);
}

DonMainWindow::~DonMainWindow()
{
}